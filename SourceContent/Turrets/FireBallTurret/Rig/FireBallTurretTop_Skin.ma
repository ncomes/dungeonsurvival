//Maya ASCII 2016 scene
//Name: FireBallTurretTop_Skin.ma
//Last modified: Fri, Nov 20, 2015 10:43:07 PM
//Codeset: 1252
file -rdi 1 -dns -rpr "FireBallTurretTop_Skeleton" -rfn "FireBallTurretTop_SkeletonRN"
		 -typ "mayaAscii" "C:/Users/Nathan/Documents/dungeonsurvival/SourceContent/Turrets/FireBallTurret/Rig/FireBallTurretTop_Skeleton.ma";
file -rdi 1 -dns -rpr "FireBallTurretTop" -rfn "FireBallTurretTopRN" -typ "mayaAscii"
		 "C:/Users/Nathan/Documents/dungeonsurvival/SourceContent/Turrets/FireBallTurret/Model/FireBallTurretTop.ma";
file -r -dns -rpr "FireBallTurretTop_Skeleton" -dr 1 -rfn "FireBallTurretTop_SkeletonRN"
		 -typ "mayaAscii" "C:/Users/Nathan/Documents/dungeonsurvival/SourceContent/Turrets/FireBallTurret/Rig/FireBallTurretTop_Skeleton.ma";
file -r -dns -rpr "FireBallTurretTop" -dr 1 -rfn "FireBallTurretTopRN" -typ "mayaAscii"
		 "C:/Users/Nathan/Documents/dungeonsurvival/SourceContent/Turrets/FireBallTurret/Model/FireBallTurretTop.ma";
requires maya "2016";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "A0A9E98F-435C-FC5A-8254-9E8DD3790DA1";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 730.12182814627408 466.72521657689617 625.45422204076624 ;
	setAttr ".r" -type "double3" -14.738352729617276 49.000000000013927 2.4239851453293745e-015 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "D82D9F57-4236-B0D0-2C9C-DAA448E4C268";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 1077.0431712779409;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "0768BFF1-4877-FA9C-4A83-BEBB9FA07570";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "615062DC-4401-89D2-53D1-82B6FA65B487";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "64440341-486C-77C2-495D-398E50507BB4";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "02EFD3A2-4407-8A13-C31E-F6ABBCF52A44";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "BC2D087D-4029-F1C8-F309-0C936AD49D18";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "6D7E6FF0-4932-C6FE-0D3F-3B94D9FFF4D1";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode fosterParent -n "FireBallTurretTopRNfosterParent1";
	rename -uid "56506AA7-4A3A-D36D-4D6A-ECBA2E2B4FAD";
createNode mesh -n "FireBallTurretTopShapeDeformed" -p "FireBallTurretTopRNfosterParent1";
	rename -uid "2CC405F3-4816-C8C5-0144-82A0FA2E7447";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75774329900741577 0.53110581636428833 ;
	setAttr ".uvst[0].uvsn" -type "string" "UVChannel_1";
	setAttr ".cuvs" -type "string" "UVChannel_1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vs" 10;
	setAttr ".vcs" 2;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "49CC68BC-4A2F-DD0A-90AB-44BDD13B13F2";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "883BB707-445A-EEE1-D59A-AEB0F97896AA";
createNode displayLayer -n "defaultLayer";
	rename -uid "1C79565D-4EC0-3AD4-1204-54A568083B26";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "1AF96139-420A-3A0C-5CE3-819CCC51C545";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "803A2401-4137-F35C-5445-D9926CB939C1";
	setAttr ".g" yes;
createNode reference -n "FireBallTurretTop_SkeletonRN";
	rename -uid "CE9606E5-460D-DAED-9A6F-7EA406FD4C61";
	setAttr -s 18 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"FireBallTurretTop_SkeletonRN"
		"FireBallTurretTop_SkeletonRN" 0
		"FireBallTurretTop_SkeletonRN" 43
		1 |ref_frame "lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		
		1 |ref_frame|root "lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		
		1 |ref_frame|root|pitch "lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		
		1 |ref_frame|root|pitch|gunBarrel "lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		
		2 "|ref_frame" "useObjectColor" " 1"
		2 "|ref_frame" "objectColor" " 0"
		2 "|ref_frame" "segmentScaleCompensate" " 1"
		2 "|ref_frame" "bindPose" " -type \"matrix\" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1"
		
		2 "|ref_frame" "lockInfluenceWeights" " 0"
		2 "|ref_frame|root" "useObjectColor" " 1"
		2 "|ref_frame|root" "objectColor" " 1"
		2 "|ref_frame|root" "bindPose" " -type \"matrix\" 1 0 0 0 0 1 0 0 0 0 1 0 0 135 0 1"
		
		2 "|ref_frame|root" "lockInfluenceWeights" " 0"
		2 "|ref_frame|root|pitch" "useObjectColor" " 1"
		2 "|ref_frame|root|pitch" "objectColor" " 2"
		2 "|ref_frame|root|pitch" "translate" " -type \"double3\" 0 102.87820434570312 1.1383209228515625"
		
		2 "|ref_frame|root|pitch" "rotate" " -type \"double3\" 0 0 0"
		2 "|ref_frame|root|pitch" "segmentScaleCompensate" " 1"
		2 "|ref_frame|root|pitch" "bindPose" " -type \"matrix\" 1 0 0 0 0 1 0 0 0 0 1 0 0 237.87820434570312 1.1383209228515625 1"
		
		2 "|ref_frame|root|pitch" "lockInfluenceWeights" " 0"
		2 "|ref_frame|root|pitch|gunBarrel" "useObjectColor" " 1"
		2 "|ref_frame|root|pitch|gunBarrel" "objectColor" " 3"
		2 "|ref_frame|root|pitch|gunBarrel" "translate" " -type \"double3\" 1.9620053137714422e-008 27.71868896484375 232.74354553222656"
		
		2 "|ref_frame|root|pitch|gunBarrel" "bindPose" " -type \"matrix\" 1 0 0 0 0 1 0 0 0 0 1 0 1.9620053137714422e-008 265.59689331054687 233.88186645507812 1"
		
		2 "|ref_frame|root|pitch|gunBarrel" "lockInfluenceWeights" " 1"
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame.message" "FireBallTurretTop_SkeletonRN.placeHolderList[1]" 
		""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame.bindPose" "FireBallTurretTop_SkeletonRN.placeHolderList[2]" 
		""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame|root.message" "FireBallTurretTop_SkeletonRN.placeHolderList[3]" 
		""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame|root.message" "FireBallTurretTop_SkeletonRN.placeHolderList[4]" 
		""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame|root.bindPose" "FireBallTurretTop_SkeletonRN.placeHolderList[5]" 
		""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame|root.worldMatrix" "FireBallTurretTop_SkeletonRN.placeHolderList[6]" 
		""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame|root.lockInfluenceWeights" 
		"FireBallTurretTop_SkeletonRN.placeHolderList[7]" ""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame|root.objectColorRGB" 
		"FireBallTurretTop_SkeletonRN.placeHolderList[8]" ""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame|root|pitch.message" "FireBallTurretTop_SkeletonRN.placeHolderList[9]" 
		""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame|root|pitch.bindPose" 
		"FireBallTurretTop_SkeletonRN.placeHolderList[10]" ""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame|root|pitch.worldMatrix" 
		"FireBallTurretTop_SkeletonRN.placeHolderList[11]" ""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame|root|pitch.lockInfluenceWeights" 
		"FireBallTurretTop_SkeletonRN.placeHolderList[12]" ""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame|root|pitch.objectColorRGB" 
		"FireBallTurretTop_SkeletonRN.placeHolderList[13]" ""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame|root|pitch|gunBarrel.message" 
		"FireBallTurretTop_SkeletonRN.placeHolderList[14]" ""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame|root|pitch|gunBarrel.bindPose" 
		"FireBallTurretTop_SkeletonRN.placeHolderList[15]" ""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame|root|pitch|gunBarrel.worldMatrix" 
		"FireBallTurretTop_SkeletonRN.placeHolderList[16]" ""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame|root|pitch|gunBarrel.lockInfluenceWeights" 
		"FireBallTurretTop_SkeletonRN.placeHolderList[17]" ""
		5 3 "FireBallTurretTop_SkeletonRN" "|ref_frame|root|pitch|gunBarrel.objectColorRGB" 
		"FireBallTurretTop_SkeletonRN.placeHolderList[18]" "";
lockNode -l 1 ;
createNode reference -n "FireBallTurretTopRN";
	rename -uid "94027D0A-4897-B1F2-D3A7-0AB022FB2680";
	setAttr -s 2 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"FireBallTurretTopRN"
		"FireBallTurretTopRN" 0
		"FireBallTurretTopRN" 23
		0 "|FireBallTurretTopRNfosterParent1|FireBallTurretTopShapeDeformed" "|FireBallTurretTop" 
		"-s -r "
		2 "|FireBallTurretTop|FireBallTurretTopShape" "intermediateObject" " 1"
		2 "|FireBallTurretTop|FireBallTurretTopShape" "vertexColorSource" " 2"
		5 3 "FireBallTurretTopRN" "|FireBallTurretTop|FireBallTurretTopShape.worldMesh" 
		"FireBallTurretTopRN.placeHolderList[1]" ""
		5 4 "FireBallTurretTopRN" "TurretSG.dagSetMembers" "FireBallTurretTopRN.placeHolderList[2]" 
		""
		8 "|FireBallTurretTop" "translateX"
		8 "|FireBallTurretTop" "translateY"
		8 "|FireBallTurretTop" "translateZ"
		8 "|FireBallTurretTop" "rotateX"
		8 "|FireBallTurretTop" "rotateY"
		8 "|FireBallTurretTop" "rotateZ"
		8 "|FireBallTurretTop" "scaleX"
		8 "|FireBallTurretTop" "scaleY"
		8 "|FireBallTurretTop" "scaleZ"
		9 "|FireBallTurretTop" "translateX"
		9 "|FireBallTurretTop" "translateY"
		9 "|FireBallTurretTop" "translateZ"
		9 "|FireBallTurretTop" "rotateX"
		9 "|FireBallTurretTop" "rotateY"
		9 "|FireBallTurretTop" "rotateZ"
		9 "|FireBallTurretTop" "scaleX"
		9 "|FireBallTurretTop" "scaleY"
		9 "|FireBallTurretTop" "scaleZ";
lockNode -l 1 ;
createNode script -n "uiConfigurationScriptNode2";
	rename -uid "585457D9-482E-9364-4829-0EB6A42FDAF4";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1240\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1240\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 19 100 -ps 2 81 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1240\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1240\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode2";
	rename -uid "14D5A680-459B-DBFE-4E5D-7382C5B048E6";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode skinCluster -n "skinCluster1";
	rename -uid "91A06F35-412D-85B0-B55D-85A7D61FACFC";
	setAttr -s 1057 ".wl";
	setAttr ".wl[0].w[1]"  1;
	setAttr ".wl[1].w[1]"  1;
	setAttr ".wl[2].w[1]"  1;
	setAttr ".wl[3].w[1]"  1;
	setAttr ".wl[4].w[1]"  1;
	setAttr ".wl[5].w[1]"  1;
	setAttr ".wl[6].w[1]"  1;
	setAttr ".wl[7].w[1]"  1;
	setAttr ".wl[8].w[1]"  1;
	setAttr ".wl[9].w[1]"  1;
	setAttr ".wl[10].w[1]"  1;
	setAttr ".wl[11].w[1]"  1;
	setAttr ".wl[12].w[1]"  1;
	setAttr ".wl[13].w[1]"  1;
	setAttr ".wl[14].w[1]"  1;
	setAttr ".wl[15].w[1]"  1;
	setAttr ".wl[16].w[1]"  1;
	setAttr ".wl[17].w[1]"  1;
	setAttr ".wl[18].w[1]"  1;
	setAttr ".wl[19].w[1]"  1;
	setAttr ".wl[20].w[1]"  1;
	setAttr ".wl[21].w[1]"  1;
	setAttr ".wl[22].w[1]"  1;
	setAttr ".wl[23].w[1]"  1;
	setAttr ".wl[24].w[1]"  1;
	setAttr ".wl[25].w[1]"  1;
	setAttr ".wl[26].w[1]"  1;
	setAttr ".wl[27].w[1]"  1;
	setAttr ".wl[28].w[1]"  1;
	setAttr ".wl[29].w[1]"  1;
	setAttr ".wl[30].w[1]"  1;
	setAttr ".wl[31].w[1]"  1;
	setAttr ".wl[32].w[1]"  1;
	setAttr ".wl[33].w[1]"  1;
	setAttr ".wl[34].w[1]"  1;
	setAttr ".wl[35].w[1]"  1;
	setAttr ".wl[36].w[1]"  1;
	setAttr ".wl[37].w[1]"  1;
	setAttr ".wl[38].w[1]"  1;
	setAttr ".wl[39].w[1]"  1;
	setAttr ".wl[40].w[1]"  1;
	setAttr ".wl[41].w[1]"  1;
	setAttr ".wl[42].w[1]"  1;
	setAttr ".wl[43].w[1]"  1;
	setAttr ".wl[44].w[1]"  1;
	setAttr ".wl[45].w[1]"  1;
	setAttr ".wl[46].w[1]"  1;
	setAttr ".wl[47].w[1]"  1;
	setAttr ".wl[48].w[1]"  1;
	setAttr ".wl[49].w[1]"  1;
	setAttr ".wl[50].w[1]"  1;
	setAttr ".wl[51].w[1]"  1;
	setAttr ".wl[52].w[1]"  1;
	setAttr ".wl[53].w[1]"  1;
	setAttr ".wl[54].w[1]"  1;
	setAttr ".wl[55].w[1]"  1;
	setAttr ".wl[56].w[1]"  1;
	setAttr ".wl[57].w[1]"  1;
	setAttr ".wl[58].w[1]"  1;
	setAttr ".wl[59].w[1]"  1;
	setAttr ".wl[60].w[1]"  1;
	setAttr ".wl[61].w[1]"  1;
	setAttr ".wl[62].w[1]"  1;
	setAttr ".wl[63].w[1]"  1;
	setAttr ".wl[64].w[1]"  1;
	setAttr ".wl[65].w[1]"  1;
	setAttr ".wl[66].w[1]"  1;
	setAttr ".wl[67].w[1]"  1;
	setAttr ".wl[68].w[1]"  1;
	setAttr ".wl[69].w[1]"  1;
	setAttr ".wl[70].w[1]"  1;
	setAttr ".wl[71].w[1]"  1;
	setAttr ".wl[72].w[1]"  1;
	setAttr ".wl[73].w[1]"  1;
	setAttr ".wl[74].w[1]"  1;
	setAttr ".wl[75].w[1]"  1;
	setAttr ".wl[76].w[1]"  1;
	setAttr ".wl[77].w[1]"  1;
	setAttr ".wl[78].w[1]"  1;
	setAttr ".wl[79].w[1]"  1;
	setAttr ".wl[80].w[1]"  1;
	setAttr ".wl[81].w[1]"  1;
	setAttr ".wl[82].w[1]"  1;
	setAttr ".wl[83].w[1]"  1;
	setAttr ".wl[84].w[1]"  1;
	setAttr ".wl[85].w[1]"  1;
	setAttr ".wl[86].w[1]"  1;
	setAttr ".wl[87].w[1]"  1;
	setAttr ".wl[88].w[1]"  1;
	setAttr ".wl[89].w[1]"  1;
	setAttr ".wl[90].w[1]"  1;
	setAttr ".wl[91].w[1]"  1;
	setAttr ".wl[92].w[1]"  1;
	setAttr ".wl[93].w[1]"  1;
	setAttr ".wl[94].w[1]"  1;
	setAttr ".wl[95].w[1]"  1;
	setAttr ".wl[96].w[1]"  1;
	setAttr ".wl[97].w[1]"  1;
	setAttr ".wl[98].w[1]"  1;
	setAttr ".wl[99].w[1]"  1;
	setAttr ".wl[100].w[1]"  1;
	setAttr ".wl[101].w[1]"  1;
	setAttr ".wl[102].w[1]"  1;
	setAttr ".wl[103].w[1]"  1;
	setAttr ".wl[104].w[1]"  1;
	setAttr ".wl[105].w[1]"  1;
	setAttr ".wl[106].w[1]"  1;
	setAttr ".wl[107].w[1]"  1;
	setAttr ".wl[108].w[1]"  1;
	setAttr ".wl[109].w[1]"  1;
	setAttr ".wl[110].w[1]"  1;
	setAttr ".wl[111].w[1]"  1;
	setAttr ".wl[112].w[1]"  1;
	setAttr ".wl[113].w[1]"  1;
	setAttr ".wl[114].w[1]"  1;
	setAttr ".wl[115].w[1]"  1;
	setAttr ".wl[116].w[1]"  1;
	setAttr ".wl[117].w[1]"  1;
	setAttr ".wl[118].w[1]"  1;
	setAttr ".wl[119].w[1]"  1;
	setAttr ".wl[120].w[1]"  1;
	setAttr ".wl[121].w[1]"  1;
	setAttr ".wl[122].w[1]"  1;
	setAttr ".wl[123].w[1]"  1;
	setAttr ".wl[124].w[1]"  1;
	setAttr ".wl[125].w[1]"  1;
	setAttr ".wl[126].w[1]"  1;
	setAttr ".wl[127].w[1]"  1;
	setAttr ".wl[128].w[1]"  1;
	setAttr ".wl[129].w[1]"  1;
	setAttr ".wl[130].w[1]"  1;
	setAttr ".wl[131].w[1]"  1;
	setAttr ".wl[132].w[1]"  1;
	setAttr ".wl[133].w[1]"  1;
	setAttr ".wl[134].w[1]"  1;
	setAttr ".wl[135].w[1]"  1;
	setAttr ".wl[136].w[1]"  1;
	setAttr ".wl[137].w[1]"  1;
	setAttr ".wl[138].w[1]"  1;
	setAttr ".wl[139].w[1]"  1;
	setAttr ".wl[140].w[1]"  1;
	setAttr ".wl[141].w[1]"  1;
	setAttr ".wl[142].w[1]"  1;
	setAttr ".wl[143].w[1]"  1;
	setAttr ".wl[144].w[1]"  1;
	setAttr ".wl[145].w[1]"  1;
	setAttr ".wl[146].w[1]"  1;
	setAttr ".wl[147].w[1]"  1;
	setAttr ".wl[148].w[1]"  1;
	setAttr ".wl[149].w[1]"  1;
	setAttr ".wl[150].w[1]"  1;
	setAttr ".wl[151].w[1]"  1;
	setAttr ".wl[152].w[1]"  1;
	setAttr ".wl[153].w[1]"  1;
	setAttr ".wl[154].w[1]"  1;
	setAttr ".wl[155].w[1]"  1;
	setAttr ".wl[156].w[1]"  1;
	setAttr ".wl[157].w[1]"  1;
	setAttr ".wl[158].w[1]"  1;
	setAttr ".wl[159].w[1]"  1;
	setAttr ".wl[160].w[1]"  1;
	setAttr ".wl[161].w[1]"  1;
	setAttr ".wl[162].w[1]"  1;
	setAttr ".wl[163].w[1]"  1;
	setAttr ".wl[164].w[1]"  1;
	setAttr ".wl[165].w[1]"  1;
	setAttr ".wl[166].w[1]"  1;
	setAttr ".wl[167].w[1]"  1;
	setAttr ".wl[168].w[1]"  1;
	setAttr ".wl[169].w[1]"  1;
	setAttr ".wl[170].w[1]"  1;
	setAttr ".wl[171].w[1]"  1;
	setAttr ".wl[172].w[1]"  1;
	setAttr ".wl[173].w[1]"  1;
	setAttr ".wl[174].w[1]"  1;
	setAttr ".wl[175].w[1]"  1;
	setAttr ".wl[176].w[1]"  1;
	setAttr ".wl[177].w[1]"  1;
	setAttr ".wl[178].w[1]"  1;
	setAttr ".wl[179].w[1]"  1;
	setAttr ".wl[180].w[1]"  1;
	setAttr ".wl[181].w[1]"  1;
	setAttr ".wl[182].w[1]"  1;
	setAttr ".wl[183].w[1]"  1;
	setAttr ".wl[184].w[1]"  1;
	setAttr ".wl[185].w[1]"  1;
	setAttr ".wl[186].w[1]"  1;
	setAttr ".wl[187].w[1]"  1;
	setAttr ".wl[188].w[2]"  1;
	setAttr ".wl[189].w[2]"  1;
	setAttr ".wl[190].w[2]"  1;
	setAttr ".wl[191].w[2]"  1;
	setAttr ".wl[192].w[2]"  1;
	setAttr ".wl[193].w[2]"  1;
	setAttr ".wl[194].w[2]"  1;
	setAttr ".wl[195].w[2]"  1;
	setAttr ".wl[196].w[2]"  1;
	setAttr ".wl[197].w[2]"  1;
	setAttr ".wl[198].w[2]"  1;
	setAttr ".wl[199].w[2]"  1;
	setAttr ".wl[200].w[2]"  1;
	setAttr ".wl[201].w[2]"  1;
	setAttr ".wl[202].w[2]"  1;
	setAttr ".wl[203].w[2]"  1;
	setAttr ".wl[204].w[2]"  1;
	setAttr ".wl[205].w[2]"  1;
	setAttr ".wl[206].w[2]"  1;
	setAttr ".wl[207].w[2]"  1;
	setAttr ".wl[208].w[2]"  1;
	setAttr ".wl[209].w[2]"  1;
	setAttr ".wl[210].w[2]"  1;
	setAttr ".wl[211].w[2]"  1;
	setAttr ".wl[212].w[2]"  1;
	setAttr ".wl[213].w[2]"  1;
	setAttr ".wl[214].w[3]"  1;
	setAttr ".wl[215].w[3]"  1;
	setAttr ".wl[216].w[3]"  1;
	setAttr ".wl[217].w[3]"  1;
	setAttr ".wl[218].w[3]"  1;
	setAttr ".wl[219].w[3]"  1;
	setAttr ".wl[220].w[3]"  1;
	setAttr ".wl[221].w[3]"  1;
	setAttr ".wl[222].w[3]"  1;
	setAttr ".wl[223].w[3]"  1;
	setAttr ".wl[224].w[3]"  1;
	setAttr ".wl[225].w[3]"  1;
	setAttr ".wl[226].w[3]"  1;
	setAttr ".wl[227].w[3]"  1;
	setAttr ".wl[228].w[3]"  1;
	setAttr ".wl[229].w[3]"  1;
	setAttr ".wl[230].w[3]"  1;
	setAttr ".wl[231].w[3]"  1;
	setAttr ".wl[232].w[3]"  1;
	setAttr ".wl[233].w[3]"  1;
	setAttr ".wl[234].w[3]"  1;
	setAttr ".wl[235].w[3]"  1;
	setAttr ".wl[236].w[3]"  1;
	setAttr ".wl[237].w[3]"  1;
	setAttr ".wl[238].w[3]"  1;
	setAttr ".wl[239].w[3]"  1;
	setAttr ".wl[240].w[3]"  1;
	setAttr ".wl[241].w[3]"  1;
	setAttr ".wl[242].w[3]"  1;
	setAttr ".wl[243].w[3]"  1;
	setAttr ".wl[244].w[3]"  1;
	setAttr ".wl[245].w[3]"  1;
	setAttr ".wl[246].w[3]"  1;
	setAttr ".wl[247].w[3]"  1;
	setAttr ".wl[248].w[3]"  1;
	setAttr ".wl[249].w[3]"  1;
	setAttr ".wl[250].w[3]"  1;
	setAttr ".wl[251].w[3]"  1;
	setAttr ".wl[252].w[3]"  1;
	setAttr ".wl[253].w[3]"  1;
	setAttr ".wl[254].w[3]"  1;
	setAttr ".wl[255].w[3]"  1;
	setAttr ".wl[256].w[3]"  1;
	setAttr ".wl[257].w[3]"  1;
	setAttr ".wl[258].w[3]"  1;
	setAttr ".wl[259].w[3]"  1;
	setAttr ".wl[260].w[3]"  1;
	setAttr ".wl[261].w[3]"  1;
	setAttr ".wl[262].w[3]"  1;
	setAttr ".wl[263].w[3]"  1;
	setAttr ".wl[264].w[3]"  1;
	setAttr ".wl[265].w[3]"  1;
	setAttr ".wl[266].w[3]"  1;
	setAttr ".wl[267].w[3]"  1;
	setAttr ".wl[268].w[2]"  1;
	setAttr ".wl[269].w[2]"  1;
	setAttr ".wl[270].w[2]"  1;
	setAttr ".wl[271].w[2]"  1;
	setAttr ".wl[272].w[2]"  1;
	setAttr ".wl[273].w[2]"  1;
	setAttr ".wl[274].w[3]"  1;
	setAttr ".wl[275].w[3]"  1;
	setAttr ".wl[276].w[2]"  1;
	setAttr ".wl[277].w[2]"  1;
	setAttr ".wl[278].w[3]"  1;
	setAttr ".wl[279].w[2]"  1;
	setAttr ".wl[280].w[3]"  1;
	setAttr ".wl[281].w[2]"  1;
	setAttr ".wl[282].w[3]"  1;
	setAttr ".wl[283].w[2]"  1;
	setAttr ".wl[284].w[2]"  1;
	setAttr ".wl[285].w[2]"  1;
	setAttr ".wl[286].w[2]"  1;
	setAttr ".wl[287].w[2]"  1;
	setAttr ".wl[288].w[2]"  1;
	setAttr ".wl[289].w[2]"  1;
	setAttr ".wl[290].w[2]"  1;
	setAttr ".wl[291].w[2]"  1;
	setAttr ".wl[292].w[2]"  1;
	setAttr ".wl[293].w[2]"  1;
	setAttr ".wl[294].w[2]"  1;
	setAttr ".wl[295].w[2]"  1;
	setAttr ".wl[296].w[2]"  1;
	setAttr ".wl[297].w[2]"  1;
	setAttr ".wl[298].w[2]"  1;
	setAttr ".wl[299].w[2]"  1;
	setAttr ".wl[300].w[2]"  1;
	setAttr ".wl[301].w[2]"  1;
	setAttr ".wl[302].w[2]"  1;
	setAttr ".wl[303].w[2]"  1;
	setAttr ".wl[304].w[2]"  1;
	setAttr ".wl[305].w[2]"  1;
	setAttr ".wl[306].w[2]"  1;
	setAttr ".wl[307].w[2]"  1;
	setAttr ".wl[308].w[2]"  1;
	setAttr ".wl[309].w[2]"  1;
	setAttr ".wl[310].w[2]"  1;
	setAttr ".wl[311].w[2]"  1;
	setAttr ".wl[312].w[2]"  1;
	setAttr ".wl[313].w[2]"  1;
	setAttr ".wl[314].w[2]"  1;
	setAttr ".wl[315].w[2]"  1;
	setAttr ".wl[316].w[2]"  1;
	setAttr ".wl[317].w[2]"  1;
	setAttr ".wl[318].w[2]"  1;
	setAttr ".wl[319].w[2]"  1;
	setAttr ".wl[320].w[2]"  1;
	setAttr ".wl[321].w[2]"  1;
	setAttr ".wl[322].w[2]"  1;
	setAttr ".wl[323].w[2]"  1;
	setAttr ".wl[324].w[2]"  1;
	setAttr ".wl[325].w[2]"  1;
	setAttr ".wl[326].w[2]"  1;
	setAttr ".wl[327].w[2]"  1;
	setAttr ".wl[328].w[2]"  1;
	setAttr ".wl[329].w[2]"  1;
	setAttr ".wl[330].w[2]"  1;
	setAttr ".wl[331].w[2]"  1;
	setAttr ".wl[332].w[2]"  1;
	setAttr ".wl[333].w[2]"  1;
	setAttr ".wl[334].w[2]"  1;
	setAttr ".wl[335].w[2]"  1;
	setAttr ".wl[336].w[2]"  1;
	setAttr ".wl[337].w[2]"  1;
	setAttr ".wl[338].w[2]"  1;
	setAttr ".wl[339].w[2]"  1;
	setAttr ".wl[340].w[2]"  1;
	setAttr ".wl[341].w[2]"  1;
	setAttr ".wl[342].w[2]"  1;
	setAttr ".wl[343].w[2]"  1;
	setAttr ".wl[344].w[2]"  1;
	setAttr ".wl[345].w[2]"  1;
	setAttr ".wl[346].w[2]"  1;
	setAttr ".wl[347].w[2]"  1;
	setAttr ".wl[348].w[2]"  1;
	setAttr ".wl[349].w[2]"  1;
	setAttr ".wl[350].w[2]"  1;
	setAttr ".wl[351].w[2]"  1;
	setAttr ".wl[352].w[2]"  1;
	setAttr ".wl[353].w[2]"  1;
	setAttr ".wl[354].w[2]"  1;
	setAttr ".wl[355].w[2]"  1;
	setAttr ".wl[356].w[2]"  1;
	setAttr ".wl[357].w[2]"  1;
	setAttr ".wl[358].w[2]"  1;
	setAttr ".wl[359].w[2]"  1;
	setAttr ".wl[360].w[2]"  1;
	setAttr ".wl[361].w[2]"  1;
	setAttr ".wl[362].w[2]"  1;
	setAttr ".wl[363].w[2]"  1;
	setAttr ".wl[364].w[2]"  1;
	setAttr ".wl[365].w[2]"  1;
	setAttr ".wl[366].w[2]"  1;
	setAttr ".wl[367].w[2]"  1;
	setAttr ".wl[368].w[2]"  1;
	setAttr ".wl[369].w[2]"  1;
	setAttr ".wl[370].w[2]"  1;
	setAttr ".wl[371].w[2]"  1;
	setAttr ".wl[372].w[2]"  1;
	setAttr ".wl[373].w[2]"  1;
	setAttr ".wl[374].w[2]"  1;
	setAttr ".wl[375].w[2]"  1;
	setAttr ".wl[376].w[2]"  1;
	setAttr ".wl[377].w[2]"  1;
	setAttr ".wl[378].w[2]"  1;
	setAttr ".wl[379].w[2]"  1;
	setAttr ".wl[380].w[2]"  1;
	setAttr ".wl[381].w[2]"  1;
	setAttr ".wl[382].w[2]"  1;
	setAttr ".wl[383].w[2]"  1;
	setAttr ".wl[384].w[2]"  1;
	setAttr ".wl[385].w[2]"  1;
	setAttr ".wl[386].w[2]"  1;
	setAttr ".wl[387].w[2]"  1;
	setAttr ".wl[388].w[2]"  1;
	setAttr ".wl[389].w[2]"  1;
	setAttr ".wl[390].w[2]"  1;
	setAttr ".wl[391].w[2]"  1;
	setAttr ".wl[392].w[2]"  1;
	setAttr ".wl[393].w[2]"  1;
	setAttr ".wl[394].w[2]"  1;
	setAttr ".wl[395].w[2]"  1;
	setAttr ".wl[396].w[2]"  1;
	setAttr ".wl[397].w[2]"  1;
	setAttr ".wl[398].w[2]"  1;
	setAttr ".wl[399].w[2]"  1;
	setAttr ".wl[400].w[2]"  1;
	setAttr ".wl[401].w[2]"  1;
	setAttr ".wl[402].w[2]"  1;
	setAttr ".wl[403].w[2]"  1;
	setAttr ".wl[404].w[2]"  1;
	setAttr ".wl[405].w[2]"  1;
	setAttr ".wl[406].w[2]"  1;
	setAttr ".wl[407].w[2]"  1;
	setAttr ".wl[408].w[2]"  1;
	setAttr ".wl[409].w[2]"  1;
	setAttr ".wl[410].w[2]"  1;
	setAttr ".wl[411].w[2]"  1;
	setAttr ".wl[412].w[2]"  1;
	setAttr ".wl[413].w[2]"  1;
	setAttr ".wl[414].w[2]"  1;
	setAttr ".wl[415].w[2]"  1;
	setAttr ".wl[416].w[2]"  1;
	setAttr ".wl[417].w[2]"  1;
	setAttr ".wl[418].w[2]"  1;
	setAttr ".wl[419].w[2]"  1;
	setAttr ".wl[420].w[2]"  1;
	setAttr ".wl[421].w[2]"  1;
	setAttr ".wl[422].w[2]"  1;
	setAttr ".wl[423].w[2]"  1;
	setAttr ".wl[424].w[2]"  1;
	setAttr ".wl[425].w[2]"  1;
	setAttr ".wl[426].w[2]"  1;
	setAttr ".wl[427].w[2]"  1;
	setAttr ".wl[428].w[2]"  1;
	setAttr ".wl[429].w[2]"  1;
	setAttr ".wl[430].w[2]"  1;
	setAttr ".wl[431].w[2]"  1;
	setAttr ".wl[432].w[2]"  1;
	setAttr ".wl[433].w[2]"  1;
	setAttr ".wl[434].w[2]"  1;
	setAttr ".wl[435].w[2]"  1;
	setAttr ".wl[436].w[2]"  1;
	setAttr ".wl[437].w[2]"  1;
	setAttr ".wl[438].w[2]"  1;
	setAttr ".wl[439].w[2]"  1;
	setAttr ".wl[440].w[2]"  1;
	setAttr ".wl[441].w[2]"  1;
	setAttr ".wl[442].w[2]"  1;
	setAttr ".wl[443].w[2]"  1;
	setAttr ".wl[444].w[2]"  1;
	setAttr ".wl[445].w[2]"  1;
	setAttr ".wl[446].w[2]"  1;
	setAttr ".wl[447].w[2]"  1;
	setAttr ".wl[448].w[2]"  1;
	setAttr ".wl[449].w[2]"  1;
	setAttr ".wl[450].w[2]"  1;
	setAttr ".wl[451].w[2]"  1;
	setAttr ".wl[452].w[2]"  1;
	setAttr ".wl[453].w[2]"  1;
	setAttr ".wl[454].w[2]"  1;
	setAttr ".wl[455].w[2]"  1;
	setAttr ".wl[456].w[2]"  1;
	setAttr ".wl[457].w[2]"  1;
	setAttr ".wl[458].w[2]"  1;
	setAttr ".wl[459].w[2]"  1;
	setAttr ".wl[460].w[2]"  1;
	setAttr ".wl[461].w[2]"  1;
	setAttr ".wl[462].w[2]"  1;
	setAttr ".wl[463].w[2]"  1;
	setAttr ".wl[464].w[2]"  1;
	setAttr ".wl[465].w[2]"  1;
	setAttr ".wl[466].w[2]"  1;
	setAttr ".wl[467].w[2]"  1;
	setAttr ".wl[468].w[2]"  1;
	setAttr ".wl[469].w[2]"  1;
	setAttr ".wl[470].w[2]"  1;
	setAttr ".wl[471].w[2]"  1;
	setAttr ".wl[472].w[2]"  1;
	setAttr ".wl[473].w[2]"  1;
	setAttr ".wl[474].w[2]"  1;
	setAttr ".wl[475].w[2]"  1;
	setAttr ".wl[476].w[2]"  1;
	setAttr ".wl[477].w[2]"  1;
	setAttr ".wl[478].w[2]"  1;
	setAttr ".wl[479].w[2]"  1;
	setAttr ".wl[480].w[2]"  1;
	setAttr ".wl[481].w[2]"  1;
	setAttr ".wl[482].w[2]"  1;
	setAttr ".wl[483].w[2]"  1;
	setAttr ".wl[484].w[2]"  1;
	setAttr ".wl[485].w[2]"  1;
	setAttr ".wl[486].w[2]"  1;
	setAttr ".wl[487].w[2]"  1;
	setAttr ".wl[488].w[2]"  1;
	setAttr ".wl[489].w[2]"  1;
	setAttr ".wl[490].w[2]"  1;
	setAttr ".wl[491].w[2]"  1;
	setAttr ".wl[492].w[2]"  1;
	setAttr ".wl[493].w[2]"  1;
	setAttr ".wl[494].w[2]"  1;
	setAttr ".wl[495].w[2]"  1;
	setAttr ".wl[496].w[2]"  1;
	setAttr ".wl[497].w[2]"  1;
	setAttr ".wl[498].w[2]"  1;
	setAttr ".wl[499].w[2]"  1;
	setAttr ".wl[500].w[2]"  1;
	setAttr ".wl[501].w[2]"  1;
	setAttr ".wl[502].w[2]"  1;
	setAttr ".wl[503].w[2]"  1;
	setAttr ".wl[504].w[2]"  1;
	setAttr ".wl[505].w[2]"  1;
	setAttr ".wl[506].w[2]"  1;
	setAttr ".wl[507].w[2]"  1;
	setAttr ".wl[508].w[2]"  1;
	setAttr ".wl[509].w[2]"  1;
	setAttr ".wl[510].w[2]"  1;
	setAttr ".wl[511].w[2]"  1;
	setAttr ".wl[512].w[2]"  1;
	setAttr ".wl[513].w[2]"  1;
	setAttr ".wl[514].w[2]"  1;
	setAttr ".wl[515].w[2]"  1;
	setAttr ".wl[516].w[2]"  1;
	setAttr ".wl[517].w[2]"  1;
	setAttr ".wl[518].w[2]"  1;
	setAttr ".wl[519].w[2]"  1;
	setAttr ".wl[520].w[2]"  1;
	setAttr ".wl[521].w[2]"  1;
	setAttr ".wl[522].w[2]"  1;
	setAttr ".wl[523].w[2]"  1;
	setAttr ".wl[524].w[2]"  1;
	setAttr ".wl[525].w[2]"  1;
	setAttr ".wl[526].w[2]"  1;
	setAttr ".wl[527].w[2]"  1;
	setAttr ".wl[528].w[2]"  1;
	setAttr ".wl[529].w[2]"  1;
	setAttr ".wl[530].w[2]"  1;
	setAttr ".wl[531].w[2]"  1;
	setAttr ".wl[532].w[2]"  1;
	setAttr ".wl[533].w[2]"  1;
	setAttr ".wl[534].w[2]"  1;
	setAttr ".wl[535].w[2]"  1;
	setAttr ".wl[536].w[2]"  1;
	setAttr ".wl[537].w[2]"  1;
	setAttr ".wl[538].w[2]"  1;
	setAttr ".wl[539].w[2]"  1;
	setAttr ".wl[540].w[2]"  1;
	setAttr ".wl[541].w[2]"  1;
	setAttr ".wl[542].w[2]"  1;
	setAttr ".wl[543].w[2]"  1;
	setAttr ".wl[544].w[2]"  1;
	setAttr ".wl[545].w[2]"  1;
	setAttr ".wl[546].w[2]"  1;
	setAttr ".wl[547].w[2]"  1;
	setAttr ".wl[548].w[2]"  1;
	setAttr ".wl[549].w[2]"  1;
	setAttr ".wl[550].w[2]"  1;
	setAttr ".wl[551].w[2]"  1;
	setAttr ".wl[552].w[2]"  1;
	setAttr ".wl[553].w[2]"  1;
	setAttr ".wl[554].w[2]"  1;
	setAttr ".wl[555].w[2]"  1;
	setAttr ".wl[556].w[2]"  1;
	setAttr ".wl[557].w[2]"  1;
	setAttr ".wl[558].w[2]"  1;
	setAttr ".wl[559].w[2]"  1;
	setAttr ".wl[560].w[2]"  1;
	setAttr ".wl[561].w[2]"  1;
	setAttr ".wl[562].w[2]"  1;
	setAttr ".wl[563].w[2]"  1;
	setAttr ".wl[564].w[2]"  1;
	setAttr ".wl[565].w[2]"  1;
	setAttr ".wl[566].w[2]"  1;
	setAttr ".wl[567].w[2]"  1;
	setAttr ".wl[568].w[2]"  1;
	setAttr ".wl[569].w[2]"  1;
	setAttr ".wl[570].w[2]"  1;
	setAttr ".wl[571].w[2]"  1;
	setAttr ".wl[572].w[2]"  1;
	setAttr ".wl[573].w[2]"  1;
	setAttr ".wl[574].w[2]"  1;
	setAttr ".wl[575].w[2]"  1;
	setAttr ".wl[576].w[2]"  1;
	setAttr ".wl[577].w[2]"  1;
	setAttr ".wl[578].w[2]"  1;
	setAttr ".wl[579].w[2]"  1;
	setAttr ".wl[580].w[2]"  1;
	setAttr ".wl[581].w[2]"  1;
	setAttr ".wl[582].w[2]"  1;
	setAttr ".wl[583].w[2]"  1;
	setAttr ".wl[584].w[2]"  1;
	setAttr ".wl[585].w[2]"  1;
	setAttr ".wl[586].w[2]"  1;
	setAttr ".wl[587].w[2]"  1;
	setAttr ".wl[588].w[2]"  1;
	setAttr ".wl[589].w[2]"  1;
	setAttr ".wl[590].w[2]"  1;
	setAttr ".wl[591].w[2]"  1;
	setAttr ".wl[592].w[2]"  1;
	setAttr ".wl[593].w[2]"  1;
	setAttr ".wl[594].w[2]"  1;
	setAttr ".wl[595].w[2]"  1;
	setAttr ".wl[596].w[2]"  1;
	setAttr ".wl[597].w[2]"  1;
	setAttr ".wl[598].w[2]"  1;
	setAttr ".wl[599].w[2]"  1;
	setAttr ".wl[600].w[2]"  1;
	setAttr ".wl[601].w[2]"  1;
	setAttr ".wl[602].w[2]"  1;
	setAttr ".wl[603].w[2]"  1;
	setAttr ".wl[604].w[2]"  1;
	setAttr ".wl[605].w[2]"  1;
	setAttr ".wl[606].w[2]"  1;
	setAttr ".wl[607].w[2]"  1;
	setAttr ".wl[608].w[2]"  1;
	setAttr ".wl[609].w[2]"  1;
	setAttr ".wl[610].w[2]"  1;
	setAttr ".wl[611].w[2]"  1;
	setAttr ".wl[612].w[2]"  1;
	setAttr ".wl[613].w[2]"  1;
	setAttr ".wl[614].w[2]"  1;
	setAttr ".wl[615].w[2]"  1;
	setAttr ".wl[616].w[2]"  1;
	setAttr ".wl[617].w[2]"  1;
	setAttr ".wl[618].w[2]"  1;
	setAttr ".wl[619].w[2]"  1;
	setAttr ".wl[620].w[2]"  1;
	setAttr ".wl[621].w[2]"  1;
	setAttr ".wl[622].w[2]"  1;
	setAttr ".wl[623].w[2]"  1;
	setAttr ".wl[624].w[2]"  1;
	setAttr ".wl[625].w[2]"  1;
	setAttr ".wl[626].w[2]"  1;
	setAttr ".wl[627].w[2]"  1;
	setAttr ".wl[628].w[2]"  1;
	setAttr ".wl[629].w[2]"  1;
	setAttr ".wl[630].w[2]"  1;
	setAttr ".wl[631].w[2]"  1;
	setAttr ".wl[632].w[2]"  1;
	setAttr ".wl[633].w[2]"  1;
	setAttr ".wl[634].w[2]"  1;
	setAttr ".wl[635].w[2]"  1;
	setAttr ".wl[636].w[2]"  1;
	setAttr ".wl[637].w[2]"  1;
	setAttr ".wl[638].w[2]"  1;
	setAttr ".wl[639].w[2]"  1;
	setAttr ".wl[640].w[2]"  1;
	setAttr ".wl[641].w[2]"  1;
	setAttr ".wl[642].w[2]"  1;
	setAttr ".wl[643].w[2]"  1;
	setAttr ".wl[644].w[2]"  1;
	setAttr ".wl[645].w[2]"  1;
	setAttr ".wl[646].w[2]"  1;
	setAttr ".wl[647].w[2]"  1;
	setAttr ".wl[648].w[2]"  1;
	setAttr ".wl[649].w[2]"  1;
	setAttr ".wl[650].w[2]"  1;
	setAttr ".wl[651].w[2]"  1;
	setAttr ".wl[652].w[2]"  1;
	setAttr ".wl[653].w[2]"  1;
	setAttr ".wl[654].w[2]"  1;
	setAttr ".wl[655].w[2]"  1;
	setAttr ".wl[656].w[2]"  1;
	setAttr ".wl[657].w[2]"  1;
	setAttr ".wl[658].w[2]"  1;
	setAttr ".wl[659].w[2]"  1;
	setAttr ".wl[660].w[2]"  1;
	setAttr ".wl[661].w[2]"  1;
	setAttr ".wl[662].w[2]"  1;
	setAttr ".wl[663].w[2]"  1;
	setAttr ".wl[664].w[2]"  1;
	setAttr ".wl[665].w[2]"  1;
	setAttr ".wl[666].w[2]"  1;
	setAttr ".wl[667].w[2]"  1;
	setAttr ".wl[668].w[2]"  1;
	setAttr ".wl[669].w[2]"  1;
	setAttr ".wl[670].w[2]"  1;
	setAttr ".wl[671].w[2]"  1;
	setAttr ".wl[672].w[2]"  1;
	setAttr ".wl[673].w[2]"  1;
	setAttr ".wl[674].w[2]"  1;
	setAttr ".wl[675].w[2]"  1;
	setAttr ".wl[676].w[2]"  1;
	setAttr ".wl[677].w[2]"  1;
	setAttr ".wl[678].w[2]"  1;
	setAttr ".wl[679].w[2]"  1;
	setAttr ".wl[680].w[2]"  1;
	setAttr ".wl[681].w[1]"  1;
	setAttr ".wl[682].w[1]"  1;
	setAttr ".wl[683].w[1]"  1;
	setAttr ".wl[684].w[1]"  1;
	setAttr ".wl[685].w[1]"  1;
	setAttr ".wl[686].w[1]"  1;
	setAttr ".wl[687].w[1]"  1;
	setAttr ".wl[688].w[1]"  1;
	setAttr ".wl[689].w[1]"  1;
	setAttr ".wl[690].w[1]"  1;
	setAttr ".wl[691].w[1]"  1;
	setAttr ".wl[692].w[1]"  1;
	setAttr ".wl[693].w[3]"  1;
	setAttr ".wl[694].w[3]"  1;
	setAttr ".wl[695].w[3]"  1;
	setAttr ".wl[696].w[3]"  1;
	setAttr ".wl[697].w[3]"  1;
	setAttr ".wl[698].w[3]"  1;
	setAttr ".wl[699].w[3]"  1;
	setAttr ".wl[700].w[3]"  1;
	setAttr ".wl[701].w[3]"  1;
	setAttr ".wl[702].w[3]"  1;
	setAttr ".wl[703].w[3]"  1;
	setAttr ".wl[704].w[3]"  1;
	setAttr ".wl[705].w[2]"  1;
	setAttr ".wl[706].w[2]"  1;
	setAttr ".wl[707].w[2]"  1;
	setAttr ".wl[708].w[2]"  1;
	setAttr ".wl[709].w[2]"  1;
	setAttr -s 2 ".wl[710].w[1:2]"  0.05 0.95;
	setAttr -s 2 ".wl[711].w[1:2]"  0.05 0.95;
	setAttr -s 2 ".wl[712].w[1:2]"  0.05 0.95;
	setAttr -s 2 ".wl[713].w[1:2]"  0.05 0.95;
	setAttr -s 2 ".wl[714].w[1:2]"  0.05 0.95;
	setAttr -s 2 ".wl[715].w[1:2]"  0.1 0.9;
	setAttr -s 2 ".wl[716].w[1:2]"  0.1 0.9;
	setAttr -s 2 ".wl[717].w[1:2]"  0.1 0.9;
	setAttr -s 2 ".wl[718].w[1:2]"  0.1 0.9;
	setAttr -s 2 ".wl[719].w[1:2]"  0.1 0.9;
	setAttr -s 2 ".wl[720].w[1:2]"  0.2 0.8;
	setAttr -s 2 ".wl[721].w[1:2]"  0.2 0.8;
	setAttr -s 2 ".wl[722].w[1:2]"  0.2 0.8;
	setAttr -s 2 ".wl[723].w[1:2]"  0.2 0.8;
	setAttr -s 2 ".wl[724].w[1:2]"  0.2 0.8;
	setAttr -s 2 ".wl[725].w[1:2]"  0.3 0.7;
	setAttr -s 2 ".wl[726].w[1:2]"  0.3 0.7;
	setAttr -s 2 ".wl[727].w[1:2]"  0.3 0.7;
	setAttr -s 2 ".wl[728].w[1:2]"  0.3 0.7;
	setAttr -s 2 ".wl[729].w[1:2]"  0.3 0.7;
	setAttr -s 2 ".wl[730].w[1:2]"  0.4 0.6;
	setAttr -s 2 ".wl[731].w[1:2]"  0.4 0.6;
	setAttr -s 2 ".wl[732].w[1:2]"  0.4 0.6;
	setAttr -s 2 ".wl[733].w[1:2]"  0.4 0.6;
	setAttr -s 2 ".wl[734].w[1:2]"  0.4 0.6;
	setAttr -s 2 ".wl[735].w[1:2]"  0.5 0.5;
	setAttr -s 2 ".wl[736].w[1:2]"  0.5 0.5;
	setAttr -s 2 ".wl[737].w[1:2]"  0.5 0.5;
	setAttr -s 2 ".wl[738].w[1:2]"  0.5 0.5;
	setAttr -s 2 ".wl[739].w[1:2]"  0.5 0.5;
	setAttr -s 2 ".wl[740].w[1:2]"  0.55 0.44999999999999996;
	setAttr -s 2 ".wl[741].w[1:2]"  0.55 0.44999999999999996;
	setAttr -s 2 ".wl[742].w[1:2]"  0.55 0.44999999999999996;
	setAttr -s 2 ".wl[743].w[1:2]"  0.55 0.44999999999999996;
	setAttr -s 2 ".wl[744].w[1:2]"  0.55 0.44999999999999996;
	setAttr -s 2 ".wl[745].w[1:2]"  0.6 0.4;
	setAttr -s 2 ".wl[746].w[1:2]"  0.6 0.4;
	setAttr -s 2 ".wl[747].w[1:2]"  0.6 0.4;
	setAttr -s 2 ".wl[748].w[1:2]"  0.6 0.4;
	setAttr -s 2 ".wl[749].w[1:2]"  0.6 0.4;
	setAttr -s 2 ".wl[750].w[1:2]"  0.7 0.30000000000000004;
	setAttr -s 2 ".wl[751].w[1:2]"  0.7 0.30000000000000004;
	setAttr -s 2 ".wl[752].w[1:2]"  0.7 0.30000000000000004;
	setAttr -s 2 ".wl[753].w[1:2]"  0.7 0.30000000000000004;
	setAttr -s 2 ".wl[754].w[1:2]"  0.7 0.30000000000000004;
	setAttr -s 2 ".wl[755].w[1:2]"  0.8 0.19999999999999996;
	setAttr -s 2 ".wl[756].w[1:2]"  0.8 0.19999999999999996;
	setAttr -s 2 ".wl[757].w[1:2]"  0.8 0.19999999999999996;
	setAttr -s 2 ".wl[758].w[1:2]"  0.8 0.19999999999999996;
	setAttr -s 2 ".wl[759].w[1:2]"  0.8 0.19999999999999996;
	setAttr -s 2 ".wl[760].w[1:2]"  0.85 0.15000000000000002;
	setAttr -s 2 ".wl[761].w[1:2]"  0.85 0.15000000000000002;
	setAttr -s 2 ".wl[762].w[1:2]"  0.85 0.15000000000000002;
	setAttr -s 2 ".wl[763].w[1:2]"  0.85 0.15000000000000002;
	setAttr -s 2 ".wl[764].w[1:2]"  0.85 0.15000000000000002;
	setAttr -s 2 ".wl[765].w[1:2]"  0.9 0.099999999999999978;
	setAttr -s 2 ".wl[766].w[1:2]"  0.9 0.099999999999999978;
	setAttr -s 2 ".wl[767].w[1:2]"  0.9 0.099999999999999978;
	setAttr -s 2 ".wl[768].w[1:2]"  0.9 0.099999999999999978;
	setAttr -s 2 ".wl[769].w[1:2]"  0.9 0.099999999999999978;
	setAttr ".wl[770].w[1]"  1;
	setAttr ".wl[771].w[1]"  1;
	setAttr ".wl[772].w[1]"  1;
	setAttr ".wl[773].w[1]"  1;
	setAttr ".wl[774].w[1]"  1;
	setAttr ".wl[775].w[1]"  1;
	setAttr ".wl[776].w[1]"  1;
	setAttr ".wl[777].w[1]"  1;
	setAttr ".wl[778].w[1]"  1;
	setAttr ".wl[779].w[1]"  1;
	setAttr ".wl[780].w[2]"  1;
	setAttr ".wl[781].w[2]"  1;
	setAttr ".wl[782].w[2]"  1;
	setAttr ".wl[783].w[2]"  1;
	setAttr ".wl[784].w[2]"  1;
	setAttr ".wl[785].w[2]"  1;
	setAttr ".wl[786].w[2]"  1;
	setAttr ".wl[787].w[2]"  1;
	setAttr -s 2 ".wl[788].w[1:2]"  0.10000000149011612 0.89999999850988388;
	setAttr -s 2 ".wl[789].w[1:2]"  0.10000000149011612 0.89999999850988388;
	setAttr -s 2 ".wl[790].w[1:2]"  0.073469392955303192 0.92653060704469681;
	setAttr -s 2 ".wl[791].w[1:2]"  0.046938776969909668 0.95306122303009033;
	setAttr -s 2 ".wl[792].w[1:2]"  0.1723032146692276 0.8276967853307724;
	setAttr -s 2 ".wl[793].w[1:2]"  0.10000000149011612 0.89999999850988388;
	setAttr -s 2 ".wl[794].w[1:2]"  0.20437318086624146 0.79562681913375854;
	setAttr -s 2 ".wl[795].w[1:2]"  0.16822157800197601 0.83177842199802399;
	setAttr -s 2 ".wl[796].w[1:2]"  0.30270960927009583 0.69729039072990417;
	setAttr -s 2 ".wl[797].w[1:2]"  0.28570312261581421 0.71429687738418579;
	setAttr -s 2 ".wl[798].w[1:2]"  0.3034386932849884 0.6965613067150116;
	setAttr -s 2 ".wl[799].w[1:2]"  0.32430049777030945 0.67569950222969055;
	setAttr -s 2 ".wl[800].w[1:2]"  0.41508477926254272 0.58491522073745728;
	setAttr -s 2 ".wl[801].w[1:2]"  0.41789001226425171 0.58210998773574829;
	setAttr -s 2 ".wl[802].w[1:2]"  0.38512289524078369 0.61487710475921631;
	setAttr -s 2 ".wl[803].w[1:2]"  0.40000000596046448 0.59999999403953552;
	setAttr -s 2 ".wl[804].w[1:2]"  0.44197872281074524 0.55802127718925476;
	setAttr -s 2 ".wl[805].w[1:2]"  0.43362003564834595 0.56637996435165405;
	setAttr -s 2 ".wl[806].w[1:2]"  0.4231095016002655 0.5768904983997345;
	setAttr -s 2 ".wl[807].w[1:2]"  0.30000001192092896 0.69999998807907104;
	setAttr -s 2 ".wl[808].w[1:2]"  0.51449114084243774 0.48550885915756226;
	setAttr -s 2 ".wl[809].w[1:2]"  0.44092240929603577 0.55907759070396423;
	setAttr -s 2 ".wl[810].w[1:2]"  0.38151559233665466 0.61848440766334534;
	setAttr -s 2 ".wl[811].w[1:2]"  0.38124093413352966 0.61875906586647034;
	setAttr -s 2 ".wl[812].w[1:2]"  0.45133012533187866 0.54866987466812134;
	setAttr -s 2 ".wl[813].w[1:2]"  0.44528788328170776 0.55471211671829224;
	setAttr -s 2 ".wl[814].w[1:2]"  0.35899367928504944 0.64100632071495056;
	setAttr -s 2 ".wl[815].w[1:2]"  0.34648069739341736 0.65351930260658264;
	setAttr -s 2 ".wl[816].w[1:2]"  0.33815017342567444 0.66184982657432556;
	setAttr -s 2 ".wl[817].w[1:2]"  0.40415108203887939 0.59584891796112061;
	setAttr -s 2 ".wl[818].w[1:2]"  0.38147345185279846 0.61852654814720154;
	setAttr -s 2 ".wl[819].w[1:2]"  0.31177887320518494 0.68822112679481506;
	setAttr -s 2 ".wl[820].w[1:2]"  0.33070984482765198 0.66929015517234802;
	setAttr -s 2 ".wl[821].w[1:2]"  0.36244702339172363 0.63755297660827637;
	setAttr -s 2 ".wl[822].w[1:2]"  0.36006030440330505 0.63993969559669495;
	setAttr -s 2 ".wl[823].w[1:2]"  0.33705511689186096 0.66294488310813904;
	setAttr -s 2 ".wl[824].w[1:2]"  0.32053521275520325 0.67946478724479675;
	setAttr -s 2 ".wl[825].w[1:2]"  0.33870482444763184 0.66129517555236816;
	setAttr -s 2 ".wl[826].w[1:2]"  0.34074008464813232 0.65925991535186768;
	setAttr -s 2 ".wl[827].w[1:2]"  0.3298475444316864 0.6701524555683136;
	setAttr -s 2 ".wl[828].w[1:2]"  0.33151388168334961 0.66848611831665039;
	setAttr -s 2 ".wl[829].w[1:2]"  0.33794540166854858 0.66205459833145142;
	setAttr -s 2 ".wl[830].w[1:2]"  0.34031957387924194 0.65968042612075806;
	setAttr -s 2 ".wl[831].w[1:2]"  0.33795782923698425 0.66204217076301575;
	setAttr -s 2 ".wl[832].w[1:2]"  0.33546280860900879 0.66453719139099121;
	setAttr -s 2 ".wl[833].w[1:2]"  0.32330277562141418 0.67669722437858582;
	setAttr -s 2 ".wl[834].w[1:2]"  0.32876282930374146 0.67123717069625854;
	setAttr -s 2 ".wl[835].w[1:2]"  0.3571891188621521 0.6428108811378479;
	setAttr -s 2 ".wl[836].w[1:2]"  0.30407604575157166 0.69592395424842834;
	setAttr -s 2 ".wl[837].w[1:2]"  0.30452519655227661 0.69547480344772339;
	setAttr -s 2 ".wl[838].w[1:2]"  0.26743119955062866 0.73256880044937134;
	setAttr -s 2 ".wl[839].w[1:2]"  0.20000000298023224 0.79999999701976776;
	setAttr -s 2 ".wl[840].w[1:2]"  0.23046755790710449 0.76953244209289551;
	setAttr -s 2 ".wl[841].w[1:2]"  0.29075750708580017 0.70924249291419983;
	setAttr -s 2 ".wl[842].w[1:2]"  0.22918561100959778 0.77081438899040222;
	setAttr -s 2 ".wl[843].w[1:2]"  0.10000000149011612 0.89999999850988388;
	setAttr -s 2 ".wl[844].w[1:2]"  0.18673279881477356 0.81326720118522644;
	setAttr -s 2 ".wl[845].w[1:2]"  0.30784010887145996 0.69215989112854004;
	setAttr -s 2 ".wl[846].w[1:2]"  0.27180624008178711 0.72819375991821289;
	setAttr -s 2 ".wl[847].w[1:2]"  0.20803768932819366 0.79196231067180634;
	setAttr -s 2 ".wl[848].w[1:2]"  0.23688481748104095 0.76311518251895905;
	setAttr -s 2 ".wl[849].w[1:2]"  0.3346860408782959 0.6653139591217041;
	setAttr -s 2 ".wl[850].w[1:2]"  0.33595529198646545 0.66404470801353455;
	setAttr -s 2 ".wl[851].w[1:2]"  0.28954973816871643 0.71045026183128357;
	setAttr -s 2 ".wl[852].w[1:2]"  0.34385389089584351 0.65614610910415649;
	setAttr -s 2 ".wl[853].w[1:2]"  0.38976505398750305 0.61023494601249695;
	setAttr -s 2 ".wl[854].w[1:2]"  0.39667594432830811 0.60332405567169189;
	setAttr -s 2 ".wl[855].w[1:2]"  0.37268748879432678 0.62731251120567322;
	setAttr -s 2 ".wl[856].w[1:2]"  0.40231281518936157 0.59768718481063843;
	setAttr -s 2 ".wl[857].w[1:2]"  0.41329693794250488 0.58670306205749512;
	setAttr -s 2 ".wl[858].w[1:2]"  0.44012922048568726 0.55987077951431274;
	setAttr -s 2 ".wl[859].w[1:2]"  0.42756745219230652 0.57243254780769348;
	setAttr -s 2 ".wl[860].w[1:2]"  0.44543707370758057 0.55456292629241943;
	setAttr -s 2 ".wl[861].w[1:2]"  0.44835153222084045 0.55164846777915955;
	setAttr -s 2 ".wl[862].w[1:2]"  0.49663126468658447 0.50336873531341553;
	setAttr -s 2 ".wl[863].w[1:2]"  0.46434161067008972 0.53565838932991028;
	setAttr -s 2 ".wl[864].w[1:2]"  0.4824923574924469 0.5175076425075531;
	setAttr -s 2 ".wl[865].w[1:2]"  0.48631250858306885 0.51368749141693115;
	setAttr -s 2 ".wl[866].w[1:2]"  0.59577399492263794 0.40422600507736206;
	setAttr -s 2 ".wl[867].w[1:2]"  0.51455581188201904 0.48544418811798096;
	setAttr -s 2 ".wl[868].w[1:2]"  0.51470690965652466 0.48529309034347534;
	setAttr -s 2 ".wl[869].w[1:2]"  0.49981677532196045 0.50018322467803955;
	setAttr -s 2 ".wl[870].w[1:2]"  0.64060616493225098 0.35939383506774902;
	setAttr -s 2 ".wl[871].w[1:2]"  0.56371188163757324 0.43628811836242676;
	setAttr -s 2 ".wl[872].w[1:2]"  0.53966492414474487 0.46033507585525513;
	setAttr -s 2 ".wl[873].w[1:2]"  0.51375299692153931 0.48624700307846069;
	setAttr -s 2 ".wl[874].w[1:2]"  0.62157636880874634 0.37842363119125366;
	setAttr -s 2 ".wl[875].w[1:2]"  0.59198266267776489 0.40801733732223511;
	setAttr -s 2 ".wl[876].w[1:2]"  0.67467206716537476 0.32532793283462524;
	setAttr -s 2 ".wl[877].w[1:2]"  0.67661792039871216 0.32338207960128784;
	setAttr -s 2 ".wl[878].w[1:2]"  0.70237314701080322 0.29762685298919678;
	setAttr -s 2 ".wl[879].w[1:2]"  0.71772420406341553 0.28227579593658447;
	setAttr -s 2 ".wl[880].w[1:2]"  0.82525026798248291 0.17474973201751709;
	setAttr -s 2 ".wl[881].w[1:2]"  0.81638133525848389 0.18361866474151611;
	setAttr -s 2 ".wl[882].w[1:2]"  0.82357126474380493 0.17642873525619507;
	setAttr -s 2 ".wl[883].w[1:2]"  0.84825617074966431 0.15174382925033569;
	setAttr -s 2 ".wl[884].w[1:2]"  0.93107432126998901 0.068925678730010986;
	setAttr -s 2 ".wl[885].w[1:2]"  0.92881792783737183 0.071182072162628174;
	setAttr -s 2 ".wl[886].w[1:2]"  0.93839806318283081 0.061601936817169189;
	setAttr -s 2 ".wl[887].w[1:2]"  0.94288110733032227 0.057118892669677734;
	setAttr -s 2 ".wl[888].w[1:2]"  0.9790688157081604 0.0209311842918396;
	setAttr -s 2 ".wl[889].w[1:2]"  0.98558986186981201 0.014410138130187988;
	setAttr ".wl[890].w[1]"  1;
	setAttr -s 2 ".wl[891].w[1:2]"  0.99103540182113647 0.0089645981788635254;
	setAttr -s 2 ".wl[892].w[1:2]"  0.99989998340606689 0.00010001659393310547;
	setAttr ".wl[893].w[1]"  1;
	setAttr ".wl[894].w[1]"  1;
	setAttr -s 2 ".wl[895].w[1:2]"  0.99909293651580811 0.00090706348419189453;
	setAttr ".wl[896].w[2]"  1;
	setAttr ".wl[897].w[2]"  1;
	setAttr ".wl[898].w[2]"  1;
	setAttr ".wl[899].w[2]"  1;
	setAttr ".wl[900].w[2]"  1;
	setAttr ".wl[901].w[2]"  1;
	setAttr ".wl[902].w[2]"  1;
	setAttr ".wl[903].w[2]"  1;
	setAttr ".wl[904].w[2]"  1;
	setAttr ".wl[905].w[2]"  1;
	setAttr ".wl[906].w[2]"  1;
	setAttr -s 2 ".wl[907].w[1:2]"  0.0448654405772686 0.9551345594227314;
	setAttr -s 2 ".wl[908].w[1:2]"  0.055281873792409897 0.9447181262075901;
	setAttr -s 2 ".wl[909].w[1:2]"  0.047080922871828079 0.95291907712817192;
	setAttr -s 2 ".wl[910].w[1:2]"  0.036669246852397919 0.96333075314760208;
	setAttr -s 2 ".wl[911].w[1:2]"  0.14615653455257416 0.85384346544742584;
	setAttr -s 2 ".wl[912].w[1:2]"  0.14977245032787323 0.85022754967212677;
	setAttr -s 2 ".wl[913].w[1:2]"  0.16077643632888794 0.83922356367111206;
	setAttr -s 2 ".wl[914].w[1:2]"  0.1622360497713089 0.8377639502286911;
	setAttr -s 2 ".wl[915].w[1:2]"  0.15446120500564575 0.84553879499435425;
	setAttr -s 2 ".wl[916].w[1:2]"  0.27111160755157471 0.72888839244842529;
	setAttr -s 2 ".wl[917].w[1:2]"  0.27597495913505554 0.72402504086494446;
	setAttr -s 2 ".wl[918].w[1:2]"  0.28411141037940979 0.71588858962059021;
	setAttr -s 2 ".wl[919].w[1:2]"  0.28604483604431152 0.71395516395568848;
	setAttr -s 2 ".wl[920].w[1:2]"  0.27798959612846375 0.72201040387153625;
	setAttr -s 2 ".wl[921].w[1:2]"  0.38847225904464722 0.61152774095535278;
	setAttr -s 2 ".wl[922].w[1:2]"  0.39414873719215393 0.60585126280784607;
	setAttr -s 2 ".wl[923].w[1:2]"  0.40967604517936707 0.59032395482063293;
	setAttr -s 2 ".wl[924].w[1:2]"  0.41874235868453979 0.58125764131546021;
	setAttr -s 2 ".wl[925].w[1:2]"  0.41110852360725403 0.58889147639274597;
	setAttr -s 2 ".wl[926].w[1:2]"  0.49319449067115784 0.50680550932884216;
	setAttr -s 2 ".wl[927].w[1:2]"  0.48038756847381592 0.51961243152618408;
	setAttr -s 2 ".wl[928].w[1:2]"  0.53404754400253296 0.46595245599746704;
	setAttr -s 2 ".wl[929].w[1:2]"  0.55015838146209717 0.44984161853790283;
	setAttr -s 2 ".wl[930].w[1:2]"  0.52901071310043335 0.47098928689956665;
	setAttr -s 2 ".wl[931].w[1:2]"  0.59407210350036621 0.40592789649963379;
	setAttr -s 2 ".wl[932].w[1:2]"  0.51110744476318359 0.48889255523681641;
	setAttr -s 2 ".wl[933].w[1:2]"  0.50986397266387939 0.49013602733612061;
	setAttr -s 2 ".wl[934].w[1:2]"  0.63399124145507813 0.36600875854492188;
	setAttr -s 2 ".wl[935].w[1:2]"  0.65659964084625244 0.34340035915374756;
	setAttr -s 2 ".wl[936].w[1:2]"  0.6240689754486084 0.3759310245513916;
	setAttr -s 2 ".wl[937].w[1:2]"  0.59930282831192017 0.40069717168807983;
	setAttr -s 2 ".wl[938].w[1:2]"  0.58358949422836304 0.41641050577163696;
	setAttr -s 2 ".wl[939].w[1:2]"  0.5890083909034729 0.4109916090965271;
	setAttr -s 2 ".wl[940].w[1:2]"  0.61036056280136108 0.38963943719863892;
	setAttr -s 2 ".wl[941].w[1:2]"  0.67410737276077271 0.32589262723922729;
	setAttr -s 2 ".wl[942].w[1:2]"  0.67890381813049316 0.32109618186950684;
	setAttr -s 2 ".wl[943].w[1:2]"  0.67577749490737915 0.32422250509262085;
	setAttr -s 2 ".wl[944].w[1:2]"  0.65464669466018677 0.34535330533981323;
	setAttr -s 2 ".wl[945].w[1:2]"  0.65177607536315918 0.34822392463684082;
	setAttr -s 2 ".wl[946].w[1:2]"  0.74754685163497925 0.25245314836502075;
	setAttr -s 2 ".wl[947].w[1:2]"  0.78012359142303467 0.21987640857696533;
	setAttr -s 2 ".wl[948].w[1:2]"  0.79610389471054077 0.20389610528945923;
	setAttr -s 2 ".wl[949].w[1:2]"  0.78303813934326172 0.21696186065673828;
	setAttr -s 2 ".wl[950].w[1:2]"  0.75885564088821411 0.24114435911178589;
	setAttr -s 2 ".wl[951].w[1:2]"  0.86734926700592041 0.13265073299407959;
	setAttr -s 2 ".wl[952].w[1:2]"  0.8769381046295166 0.1230618953704834;
	setAttr -s 2 ".wl[953].w[1:2]"  0.90044689178466797 0.099553108215332031;
	setAttr -s 2 ".wl[954].w[1:2]"  0.91146671772003174 0.088533282279968262;
	setAttr -s 2 ".wl[955].w[1:2]"  0.89044833183288574 0.10955166816711426;
	setAttr -s 2 ".wl[956].w[1:2]"  0.97268551588058472 0.027314484119415283;
	setAttr -s 2 ".wl[957].w[1:2]"  0.96373039484024048 0.036269605159759521;
	setAttr -s 2 ".wl[958].w[1:2]"  0.97051358222961426 0.029486417770385742;
	setAttr ".wl[959].w[1]"  1;
	setAttr -s 2 ".wl[960].w[1:2]"  0.99065053462982178 0.0093494653701782227;
	setAttr -s 2 ".wl[961].w[1:2]"  0.99813985824584961 0.0018601417541503908;
	setAttr -s 2 ".wl[962].w[1:2]"  0.99456602334976196 0.0054339766502380371;
	setAttr -s 2 ".wl[963].w[1:2]"  0.99377226829528809 0.0062277317047119141;
	setAttr ".wl[964].w[1]"  1;
	setAttr ".wl[965].w[1]"  1;
	setAttr ".wl[966].w[1]"  1;
	setAttr ".wl[967].w[1]"  1;
	setAttr ".wl[968].w[1]"  1;
	setAttr ".wl[969].w[1]"  1;
	setAttr ".wl[970].w[1]"  1;
	setAttr ".wl[971].w[2]"  1;
	setAttr ".wl[972].w[2]"  1;
	setAttr ".wl[973].w[2]"  1;
	setAttr ".wl[974].w[2]"  1;
	setAttr ".wl[975].w[2]"  1;
	setAttr ".wl[976].w[2]"  1;
	setAttr ".wl[977].w[2]"  1;
	setAttr ".wl[978].w[2]"  1;
	setAttr ".wl[979].w[2]"  1;
	setAttr ".wl[980].w[2]"  1;
	setAttr ".wl[981].w[2]"  1;
	setAttr ".wl[982].w[2]"  1;
	setAttr ".wl[983].w[2]"  1;
	setAttr ".wl[984].w[2]"  1;
	setAttr ".wl[985].w[2]"  1;
	setAttr ".wl[986].w[2]"  1;
	setAttr ".wl[987].w[2]"  1;
	setAttr ".wl[988].w[2]"  1;
	setAttr ".wl[989].w[2]"  1;
	setAttr ".wl[990].w[2]"  1;
	setAttr ".wl[991].w[2]"  1;
	setAttr ".wl[992].w[2]"  1;
	setAttr ".wl[993].w[2]"  1;
	setAttr ".wl[994].w[2]"  1;
	setAttr ".wl[995].w[2]"  1;
	setAttr ".wl[996].w[2]"  1;
	setAttr ".wl[997].w[2]"  1;
	setAttr ".wl[998].w[2]"  1;
	setAttr ".wl[999].w[2]"  1;
	setAttr ".wl[1000].w[2]"  1;
	setAttr ".wl[1001].w[2]"  1;
	setAttr ".wl[1002].w[2]"  1;
	setAttr ".wl[1003].w[2]"  1;
	setAttr ".wl[1004].w[2]"  1;
	setAttr ".wl[1005].w[2]"  1;
	setAttr ".wl[1006].w[2]"  1;
	setAttr ".wl[1007].w[2]"  1;
	setAttr ".wl[1008].w[2]"  1;
	setAttr ".wl[1009].w[2]"  1;
	setAttr ".wl[1010].w[2]"  1;
	setAttr ".wl[1011].w[2]"  1;
	setAttr ".wl[1012].w[2]"  1;
	setAttr ".wl[1013].w[2]"  1;
	setAttr ".wl[1014].w[2]"  1;
	setAttr ".wl[1015].w[2]"  1;
	setAttr ".wl[1016].w[2]"  1;
	setAttr ".wl[1017].w[2]"  1;
	setAttr ".wl[1018].w[2]"  1;
	setAttr ".wl[1019].w[2]"  1;
	setAttr ".wl[1020].w[2]"  1;
	setAttr ".wl[1021].w[2]"  1;
	setAttr ".wl[1022].w[2]"  1;
	setAttr ".wl[1023].w[2]"  1;
	setAttr ".wl[1024].w[2]"  1;
	setAttr ".wl[1025].w[2]"  1;
	setAttr ".wl[1026].w[2]"  1;
	setAttr ".wl[1027].w[2]"  1;
	setAttr ".wl[1028].w[2]"  1;
	setAttr ".wl[1029].w[2]"  1;
	setAttr ".wl[1030].w[2]"  1;
	setAttr ".wl[1031].w[2]"  1;
	setAttr ".wl[1032].w[2]"  1;
	setAttr ".wl[1033].w[2]"  1;
	setAttr ".wl[1034].w[2]"  1;
	setAttr ".wl[1035].w[2]"  1;
	setAttr ".wl[1036].w[2]"  1;
	setAttr ".wl[1037].w[2]"  1;
	setAttr ".wl[1038].w[2]"  1;
	setAttr ".wl[1039].w[2]"  1;
	setAttr ".wl[1040].w[2]"  1;
	setAttr ".wl[1041].w[2]"  1;
	setAttr ".wl[1042].w[2]"  1;
	setAttr ".wl[1043].w[2]"  1;
	setAttr ".wl[1044].w[2]"  1;
	setAttr ".wl[1045].w[2]"  1;
	setAttr ".wl[1046].w[2]"  1;
	setAttr ".wl[1047].w[2]"  1;
	setAttr ".wl[1048].w[2]"  1;
	setAttr ".wl[1049].w[2]"  1;
	setAttr ".wl[1050].w[2]"  1;
	setAttr ".wl[1051].w[2]"  1;
	setAttr ".wl[1052].w[2]"  1;
	setAttr ".wl[1053].w[2]"  1;
	setAttr ".wl[1054].w[2]"  1;
	setAttr ".wl[1055].w[2]"  1;
	setAttr ".wl[1056].w[2]"  1;
	setAttr -s 4 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
	setAttr ".pm[1]" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -135 -0 1;
	setAttr ".pm[2]" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -237.87820434570312 -1.1383209228515625 1;
	setAttr ".pm[3]" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -1.9620053137714422e-008 -265.59689331054687 -233.88186645507812 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 3 ".ma";
	setAttr -s 4 ".dpf[0:3]"  4 4 4 4;
	setAttr -s 3 ".lw";
	setAttr -s 3 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 4;
	setAttr ".ucm" yes;
	setAttr -s 3 ".ifcl";
	setAttr -s 3 ".ifcl";
createNode tweak -n "tweak1";
	rename -uid "994CBCE7-41E7-647D-AA90-FAAB5AC9EC0E";
createNode objectSet -n "skinCluster1Set";
	rename -uid "7C890F97-481C-3073-7B72-0C9C785FE0E8";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster1GroupId";
	rename -uid "681EE118-453B-5B6F-EEA5-E98511FC7AA0";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster1GroupParts";
	rename -uid "EA6B5D32-4CC6-5E96-8DDD-1D99815E98B5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet1";
	rename -uid "C8E93641-4D52-A661-F86B-8BB16AED9A55";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId2";
	rename -uid "F41B0C06-4DDA-ADE0-B240-479A1A230165";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "D83162A2-433A-97F9-BDDA-0E82BF6B2464";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose1";
	rename -uid "D97C75CA-4A28-30FA-0695-55B98A7F3176";
	setAttr -s 4 ".wm";
	setAttr -s 4 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 135 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 102.87820434570312 1.1383209228515625 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0 0 1.9620053137714422e-008
		 27.71868896484375 232.74354553222656 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1
		 1 1 yes;
	setAttr -s 4 ".m";
	setAttr -s 4 ".p";
	setAttr -s 4 ".g[0:3]" yes no no no;
	setAttr ".bp" yes;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 6 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 3 ".r";
select -ne :defaultTextureList1;
	setAttr -s 4 ".tx";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "FireBallTurretTop_SkeletonRN.phl[1]" "bindPose1.m[0]";
connectAttr "FireBallTurretTop_SkeletonRN.phl[2]" "bindPose1.wm[0]";
connectAttr "FireBallTurretTop_SkeletonRN.phl[3]" "skinCluster1.ptt";
connectAttr "FireBallTurretTop_SkeletonRN.phl[4]" "bindPose1.m[1]";
connectAttr "FireBallTurretTop_SkeletonRN.phl[5]" "bindPose1.wm[1]";
connectAttr "FireBallTurretTop_SkeletonRN.phl[6]" "skinCluster1.ma[1]";
connectAttr "FireBallTurretTop_SkeletonRN.phl[7]" "skinCluster1.lw[1]";
connectAttr "FireBallTurretTop_SkeletonRN.phl[8]" "skinCluster1.ifcl[1]";
connectAttr "FireBallTurretTop_SkeletonRN.phl[9]" "bindPose1.m[2]";
connectAttr "FireBallTurretTop_SkeletonRN.phl[10]" "bindPose1.wm[2]";
connectAttr "FireBallTurretTop_SkeletonRN.phl[11]" "skinCluster1.ma[2]";
connectAttr "FireBallTurretTop_SkeletonRN.phl[12]" "skinCluster1.lw[2]";
connectAttr "FireBallTurretTop_SkeletonRN.phl[13]" "skinCluster1.ifcl[2]";
connectAttr "FireBallTurretTop_SkeletonRN.phl[14]" "bindPose1.m[3]";
connectAttr "FireBallTurretTop_SkeletonRN.phl[15]" "bindPose1.wm[3]";
connectAttr "FireBallTurretTop_SkeletonRN.phl[16]" "skinCluster1.ma[3]";
connectAttr "FireBallTurretTop_SkeletonRN.phl[17]" "skinCluster1.lw[3]";
connectAttr "FireBallTurretTop_SkeletonRN.phl[18]" "skinCluster1.ifcl[3]";
connectAttr "FireBallTurretTopRN.phl[1]" "groupParts2.ig";
connectAttr "FireBallTurretTopShapeDeformed.iog" "FireBallTurretTopRN.phl[2]";
connectAttr "skinCluster1GroupId.id" "FireBallTurretTopShapeDeformed.iog.og[2].gid"
		;
connectAttr "skinCluster1Set.mwc" "FireBallTurretTopShapeDeformed.iog.og[2].gco"
		;
connectAttr "groupId2.id" "FireBallTurretTopShapeDeformed.iog.og[3].gid";
connectAttr "tweakSet1.mwc" "FireBallTurretTopShapeDeformed.iog.og[3].gco";
connectAttr "skinCluster1.og[0]" "FireBallTurretTopShapeDeformed.i";
connectAttr "tweak1.vl[0].vt[0]" "FireBallTurretTopShapeDeformed.twl";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "FireBallTurretTopRNfosterParent1.msg" "FireBallTurretTopRN.fp";
connectAttr "skinCluster1GroupParts.og" "skinCluster1.ip[0].ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1.ip[0].gi";
connectAttr "bindPose1.msg" "skinCluster1.bp";
connectAttr "groupParts2.og" "tweak1.ip[0].ig";
connectAttr "groupId2.id" "tweak1.ip[0].gi";
connectAttr "skinCluster1GroupId.msg" "skinCluster1Set.gn" -na;
connectAttr "FireBallTurretTopShapeDeformed.iog.og[2]" "skinCluster1Set.dsm" -na
		;
connectAttr "skinCluster1.msg" "skinCluster1Set.ub[0]";
connectAttr "tweak1.og[0]" "skinCluster1GroupParts.ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1GroupParts.gi";
connectAttr "groupId2.msg" "tweakSet1.gn" -na;
connectAttr "FireBallTurretTopShapeDeformed.iog.og[3]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "bindPose1.w" "bindPose1.p[0]";
connectAttr "bindPose1.m[0]" "bindPose1.p[1]";
connectAttr "bindPose1.m[1]" "bindPose1.p[2]";
connectAttr "bindPose1.m[2]" "bindPose1.p[3]";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of FireBallTurretTop_Skin.ma
