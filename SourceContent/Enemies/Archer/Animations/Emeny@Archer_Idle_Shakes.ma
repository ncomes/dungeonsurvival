//Maya ASCII 2016 scene
//Name: Emeny@Archer_Idle_Shakes.ma
//Last modified: Tue, Nov 24, 2015 12:18:30 PM
//Codeset: 1252
requires maya "2016";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "B580FF59-4288-5D53-48E5-65B483F7BC70";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 151.5595122843296 120.58960237969546 205.6742747294567 ;
	setAttr ".r" -type "double3" -17.138352729602357 31.799999999999866 9.3557536960512242e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "EAF762AA-4283-3EC1-412F-F0A1390009D9";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 291.76560210571114;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "AED45BBF-4DD8-2752-F4E3-AF806210C1FC";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "59DBE536-40D3-7806-E926-A4BBC0670544";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "AB460BF0-4C9E-C48C-2AC5-8BBFDC3675C8";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "DD35B4B5-4982-F67C-7B49-069923E6CB12";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "90683F10-4F28-6CB2-B267-6A80EB5BB60E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "7A748A4A-4389-5FAB-4555-59B5779405C5";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "ref_frame";
	rename -uid "2B54C921-4982-2303-3445-0AA78F0EBBEF";
	addAttr -ci true -sn "refFrame" -ln "refFrame" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "Character1_Hips" -p "ref_frame";
	rename -uid "5D585482-40BA-C30F-FBA1-78950EE82669";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -19.036789801921167 ;
	setAttr ".ssc" no;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftUpLeg" -p "Character1_Hips";
	rename -uid "89CADE6B-4898-623C-A378-D9839F9E7364";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 90.000000000000014 -12.849604412773132 43.257528613448208 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftLeg" -p "Character1_LeftUpLeg";
	rename -uid "7F03C68C-419C-3906-4C3B-83B160D582DB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -19.152297252867996 -13.416343813706657 -33.623675304480329 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFoot" -p "Character1_LeftLeg";
	rename -uid "6F10CD52-4623-6145-C742-328CC8654B49";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 25.817647449536953 51.040150860538553 -112.05775014674906 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftToeBase" -p "Character1_LeftFoot";
	rename -uid "3CC81385-4D43-5152-1FB2-59ADB3CB4036";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 43.727116820142825 -29.659481123592514 -4.6723850284209263 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle1" -p "Character1_LeftToeBase";
	rename -uid "BF6A96D1-4F23-BF75-4658-018CD3A87DE4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 165.43734692409092 -20.35908719617909 10.197796850486212 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle2" -p "Character1_LeftFootMiddle1";
	rename -uid "93361E2A-4523-1C5D-646F-528E4E9FC7D9";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 127.90699782306312 -44.626258206212555 -101.65660999147298 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightUpLeg" -p "Character1_Hips";
	rename -uid "492F61EA-4C42-BC66-84A4-E98E2D38487D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 133.25752861344816 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightLeg" -p "Character1_RightUpLeg";
	rename -uid "F664EC08-412C-7AE7-7C53-C095758E4F85";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -39.772585840344547 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFoot" -p "Character1_RightLeg";
	rename -uid "F4334327-4A2D-B349-22B6-1EA70BF06B2E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -53.712356932759 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightToeBase" -p "Character1_RightFoot";
	rename -uid "0B6D024F-4628-2AAB-0230-48B3988F4179";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 2.9245623946004821e-006 53.712356932758929 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle1" -p "Character1_RightToeBase";
	rename -uid "05A73876-4B93-1A2A-973D-9FBE7B98481B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -4.5380835579220506e-006 -2.7209092876563258e-005 
		39.772585840347155 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle2" -p "Character1_RightFootMiddle1";
	rename -uid "B4ED7F1A-4F46-1571-892B-77A3BB92AF48";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -2.8539668357113417e-005 0.00011485155389716205 -133.25752861351003 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine" -p "Character1_Hips";
	rename -uid "23A46CD5-422A-8609-3602-908C9BCF9A14";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 133.25752861344816 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine1" -p "Character1_Spine";
	rename -uid "BF0A5F6B-4E2C-3B0B-1EFD-F199426D776F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -34.34645244194413 -26.596597123422644 16.99576897690589 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftShoulder" -p "Character1_Spine1";
	rename -uid "90998608-4680-CE5A-522E-67A1CE4EFD62";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -166.39102833412767 21.832119962557094 -120.00622813437471 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftArm" -p "Character1_LeftShoulder";
	rename -uid "45202B68-4656-DFDA-56E6-4F99DDBFFC22";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -172.11387269090633 -44.88241940154802 -37.940590556196071 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftForeArm" -p "Character1_LeftArm";
	rename -uid "6F659C08-4E86-057E-AEEA-70903EBB1DF1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -84.022423618581243 -14.673598567163568 71.813326221989513 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHand" -p "Character1_LeftForeArm";
	rename -uid "FC6ADB04-4DFA-C6A2-8FF8-72962E19D290";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -24.404340271578391 -34.932697344706668 122.26498801320882 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr -k on ".liw" no;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb1" -p "Character1_LeftHand";
	rename -uid "05B39F4C-4C87-7BFD-6CF2-8381F8CF79D0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.1043965330995 61.333484361561069 1.8740740862626108 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb2" -p "Character1_LeftHandThumb1";
	rename -uid "1F3AD08F-4251-E949-E98E-85A2C6F7554F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -103.44292663098258 20.93424016453444 169.56041330531272 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb3" -p "Character1_LeftHandThumb2";
	rename -uid "567D90C5-43B5-A385-E3F0-87B739F7ECA1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 14.174484134960574 -59.672502756712539 -19.020394805426264 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex1" -p "Character1_LeftHand";
	rename -uid "92192574-4472-D374-E1D9-E2A73A679798";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -150.59621970507803 75.681392634287164 -124.28723884791442 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex2" -p "Character1_LeftHandIndex1";
	rename -uid "F1FCB789-4C85-E906-330A-D89C77445EA5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -41.093804604175162 -57.602285575728224 -69.242456746392662 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex3" -p "Character1_LeftHandIndex2";
	rename -uid "A32F7E4A-4021-5E62-7420-89B4B5251B92";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.108297963098213 8.7979835742630801 27.088028901385748 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing1" -p "Character1_LeftHand";
	rename -uid "72F6062D-4FE1-4E21-6101-4E9C5BCE6E1A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -141.42437885137375 63.919742192600388 -113.78475086453638 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing2" -p "Character1_LeftHandRing1";
	rename -uid "270BA144-4926-F2AF-4768-549A881AD63A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -35.368646977348 -76.384823071466059 -48.755963146359043 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing3" -p "Character1_LeftHandRing2";
	rename -uid "A6B9072C-4DDB-9117-5C69-8798D09A11C9";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 9.6312888734270121 39.523687300631337 15.3258182753707 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightShoulder" -p "Character1_Spine1";
	rename -uid "89A9438F-48DA-6EE7-A21A-DBB552447EED";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -164.54352138244408 34.957040538466451 -116.14754006477595 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightArm" -p "Character1_RightShoulder";
	rename -uid "0AE17A88-448D-FA18-2C1E-3288E3AFD946";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -173.75456654457099 -11.164646449014084 -45.537881339438307 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightForeArm" -p "Character1_RightArm";
	rename -uid "2DA7C41C-4018-A711-5125-DDA43190EEE3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 22.753833922449758 50.186429787105524 -101.53518723839815 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHand" -p "Character1_RightForeArm";
	rename -uid "5C72191E-44D4-524B-B6E2-8E8D5C183101";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 48.334026046900341 -3.7701083025157853 -29.882905994214536 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb1" -p "Character1_RightHand";
	rename -uid "5A678614-400A-0206-CDB9-47999557EFF3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb2" -p "Character1_RightHandThumb1";
	rename -uid "9E88DE4F-4E9B-4947-14C9-91BB21ACFE87";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb3" -p "Character1_RightHandThumb2";
	rename -uid "59523500-4769-0C05-4D8D-08B8666BD152";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex1" -p "Character1_RightHand";
	rename -uid "AB5A7082-43B9-DB66-5604-85BC81D85F7D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex2" -p "Character1_RightHandIndex1";
	rename -uid "C052C4B6-4CDC-D2A5-6F91-8F9262066761";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex3" -p "Character1_RightHandIndex2";
	rename -uid "A04413FE-4839-5466-784D-DDA12AA91039";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing1" -p "Character1_RightHand";
	rename -uid "7CF4AAC8-4C2D-DC70-DE3D-F8A760CF5C50";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing2" -p "Character1_RightHandRing1";
	rename -uid "7B93832E-40DB-C36E-9015-CBBC2F7EE138";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing3" -p "Character1_RightHandRing2";
	rename -uid "8AC7C4FA-41C5-B2AE-1514-D29F93828CE5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Neck" -p "Character1_Spine1";
	rename -uid "DAD23A6A-4822-D23B-7767-99BE200610BC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 48.229963067601958 30.670012388143238 129.35864775594831 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Head" -p "Character1_Neck";
	rename -uid "575EE688-4B42-ACCE-154A-2187ABE184E9";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -14.36476107543206 22.377462763663633 -109.58762475494268 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "eyes" -p "Character1_Head";
	rename -uid "2F918B10-483B-8618-5D64-EC922EB12813";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -157.21319586594478 -21.144487689504771 -3.4677530572789341 ;
	setAttr ".radi" 4.5;
	setAttr ".fbxID" 5;
createNode joint -n "jaw" -p "Character1_Head";
	rename -uid "07C4104D-4D50-7B15-4B01-C6A74C98F7AA";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -157.21319586594478 -21.144487689504771 -3.4677530572789341 ;
	setAttr ".radi" 0.5;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode animCurveTU -n "Character1_LeftHand_lockInfluenceWeights";
	rename -uid "F4DFE131-4464-901D-3888-BBA04529E123";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "jaw_lockInfluenceWeights";
	rename -uid "EFCD1FC3-4FF1-A3AD-F33B-6FA8332BCAA5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "Character1_Hips_translateX";
	rename -uid "21546B33-4B4F-1AE5-5E3C-EB8A7CC2370A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -0.50418257713317871 1 -0.57027363777160645
		 2 -0.6383020281791687 3 -0.70757967233657837 4 -0.7774665355682373 5 -0.84730982780456543
		 6 -0.91686195135116577 7 -0.98604780435562134 8 -1.0546904802322388 9 -1.1225231885910034
		 10 -1.1888502836227417 11 -1.2527761459350586 12 -1.3133012056350708 13 -1.3695251941680908
		 14 -1.4212276935577393 15 -1.4684387445449829 16 -1.466705322265625 17 -1.4739302396774292
		 18 -1.4895764589309692 19 -1.5103529691696167 20 -1.5330659151077271 21 -1.557591438293457
		 22 -1.5847244262695313 23 -1.6136481761932373 24 -1.6435074806213379 25 -1.6732450723648071
		 26 -1.7020406723022461 27 -1.7295185327529907 28 -1.7555186748504639 29 -1.7784824371337891
		 30 -1.7965859174728394 31 -1.799159049987793 32 -1.8047914505004883 33 -1.8142353296279907
		 34 -1.826738715171814 35 -1.8412297964096069 36 -1.8572641611099243 37 -1.8743720054626465
		 38 -1.8921457529067993 39 -1.9106402397155762 40 -1.9301841259002686 41 -1.9510432481765747
		 42 -1.9734212160110474 43 -1.9969589710235596 44 -2.0209550857543945 45 -2.0445091724395752
		 46 -2.0706865787506104 47 -2.1020157337188721 48 -2.1383833885192871 49 -2.1797480583190918
		 50 -2.2273082733154297 51 -2.2803077697753906 52 -2.3351707458496094 53 -2.3883199691772461
		 54 -2.4393613338470459 55 -2.4893078804016113 56 -2.5372233390808105 57 -2.5821607112884521
		 58 -2.6228549480438232 59 -2.6584732532501221 60 -2.6888461112976074 61 -2.7149124145507812
		 62 -2.7390754222869873 63 -2.7608115673065186 64 -2.7837135791778564 65 -2.8073797225952148
		 66 -2.8290250301361084 67 -2.8480930328369141 68 -2.864130973815918 69 -2.8766114711761475
		 70 -2.8855171203613281 71 -2.8911144733428955 72 -2.8935227394104004 73 -2.8927292823791504
		 74 -2.8881182670593262 75 -2.8787951469421387 76 -2.8648686408996582 77 -2.8261032104492187
		 78 -2.7663345336914062 79 -2.6896686553955078 80 -2.6002538204193115 81 -2.5033359527587891
		 82 -2.4023280143737793 83 -2.2980659008026123 84 -2.1916263103485107 85 -2.0873482227325439
		 86 -1.9902665615081787 87 -1.9036622047424316 88 -1.8310080766677856 89 -1.77583909034729
		 90 -1.7418215274810791 91 -1.658618688583374 92 -1.5521516799926758 93 -1.4267762899398804
		 94 -1.2853600978851318 95 -1.1334067583084106 96 -0.97689765691757202 97 -0.8194657564163208
		 98 -0.66438025236129761 99 -0.51636946201324463 100 -0.38047647476196289 101 -0.2629888653755188
		 102 -0.16857385635375977 103 -0.099100619554519653 104 -0.056463804095983505 105 -0.045604418963193893
		 106 -0.081382326781749725 107 -0.11581896990537643 108 -0.14898025989532471 109 -0.180643230676651
		 110 -0.21088063716888428 111 -0.24042542278766632 112 -0.27033475041389465 113 -0.30003237724304199
		 114 -0.32825061678886414 115 -0.3561311662197113 116 -0.38515621423721313 117 -0.41505062580108643
		 118 -0.44457662105560303 119 -0.47411969304084778 120 -0.50418257713317871;
createNode animCurveTL -n "Character1_Hips_translateY";
	rename -uid "C8C16EBB-4FFD-5634-C091-3EA8BB73F025";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 42.001785278320312 1 41.897136688232422
		 2 41.786663055419922 3 41.671749114990234 4 41.553993225097656 5 41.435188293457031
		 6 41.316596984863281 7 41.199089050292969 8 41.083797454833984 9 40.971866607666016
		 10 40.864723205566406 11 40.7642822265625 12 40.673000335693359 13 40.592987060546875
		 14 40.524662017822266 15 40.468471527099609 16 40.503799438476562 17 40.581089019775391
		 18 40.693729400634766 19 40.835010528564453 20 40.998767852783203 21 41.178802490234375
		 22 41.368637084960937 23 41.561973571777344 24 41.752506256103516 25 41.933780670166016
		 26 42.098644256591797 27 42.240287780761719 28 42.352870941162109 29 42.430519104003906
		 30 42.466560363769531 31 42.4532470703125 32 42.382583618164062 33 42.263175964355469
		 34 42.103919982910156 35 41.913173675537109 36 41.699516296386719 37 41.472183227539063
		 38 41.240459442138672 39 41.012718200683594 40 40.796657562255859 41 40.600196838378906
		 42 40.431259155273438 43 40.298557281494141 44 40.211563110351563 45 40.180427551269531
		 46 40.163890838623047 47 40.137100219726563 48 40.100154876708984 49 40.054080963134766
		 50 40.000690460205078 51 39.941780090332031 52 39.879188537597656 53 39.815322875976563
		 54 39.752182006835938 55 39.691154479980469 56 39.633918762207031 57 39.582111358642578
		 58 39.537540435791016 59 39.501167297363281 60 39.473819732666016 61 39.41461181640625
		 62 39.338836669921875 63 39.250576019287109 64 39.154293060302734 65 39.054332733154297
		 66 38.953983306884766 67 38.857292175292969 68 38.768562316894531 69 38.692493438720703
		 70 38.632667541503906 71 38.591945648193359 72 38.573562622070313 73 38.580997467041016
		 74 38.618640899658203 75 38.691581726074219 76 38.7373046875 77 38.865028381347656
		 78 39.061027526855469 79 39.311595916748047 80 39.604183197021484 81 39.9268798828125
		 82 40.267917633056641 83 40.615329742431641 84 40.957656860351562 85 41.283172607421875
		 86 41.580055236816406 87 41.836826324462891 88 42.041606903076172 89 42.182003021240234
		 90 42.245059967041016 91 42.196338653564453 92 42.150886535644531 93 42.110977172851562
		 94 42.07501220703125 95 42.041629791259766 96 42.011592864990234 97 41.986186981201172
		 98 41.964946746826172 99 41.947303771972656 100 41.934032440185547 101 41.926082611083984
		 102 41.923118591308594 103 41.924728393554687 104 41.930984497070313 105 41.942298889160156
		 106 41.957736968994141 107 41.970249176025391 108 41.980098724365234 109 41.987274169921875
		 110 41.991420745849609 111 41.993171691894531 112 41.994598388671875 113 41.997512817382813
		 114 42.000171661376953 115 42.000667572021484 116 41.999881744384766 117 41.999458312988281
		 118 41.999652862548828 119 42.000274658203125 120 42.001785278320312;
createNode animCurveTL -n "Character1_Hips_translateZ";
	rename -uid "D080FEAF-454D-440C-65C8-7FAD42927447";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -9.3845930099487305 1 -9.4116916656494141
		 2 -9.440913200378418 3 -9.4720029830932617 4 -9.5046415328979492 5 -9.53851318359375
		 6 -9.5727863311767578 7 -9.6064291000366211 8 -9.6385278701782227 9 -9.6682825088500977
		 10 -9.6953973770141602 11 -9.7198276519775391 12 -9.7416725158691406 13 -9.7609004974365234
		 14 -9.7766084671020508 15 -9.7876091003417969 16 -9.7870998382568359 17 -9.7865581512451172
		 18 -9.7864990234375 19 -9.7887039184570312 20 -9.7948617935180664 21 -9.8050098419189453
		 22 -9.8158683776855469 23 -9.8268909454345703 24 -9.8375711441040039 25 -9.8476219177246094
		 26 -9.8564596176147461 27 -9.8630409240722656 28 -9.8661336898803711 29 -9.866765022277832
		 30 -9.8643703460693359 31 -9.8653182983398437 32 -9.8584518432617187 33 -9.8440122604370117
		 34 -9.822601318359375 35 -9.79693603515625 36 -9.7683906555175781 37 -9.7383832931518555
		 38 -9.7082586288452148 39 -9.6787347793579102 40 -9.6501846313476562 41 -9.6230602264404297
		 42 -9.5978832244873047 43 -9.5758876800537109 44 -9.5587606430053711 45 -9.5484781265258789
		 46 -9.5500164031982422 47 -9.5528726577758789 48 -9.5560808181762695 49 -9.5586166381835938
		 50 -9.5574941635131836 51 -9.5538711547851562 52 -9.5502605438232422 53 -9.5492000579833984
		 54 -9.5513477325439453 55 -9.5539608001708984 56 -9.5571365356445313 57 -9.5609836578369141
		 58 -9.5659999847412109 59 -9.5720758438110352 60 -9.5782709121704102 61 -9.5831089019775391
		 62 -9.5916194915771484 63 -9.6025352478027344 64 -9.612818717956543 65 -9.6210212707519531
		 66 -9.6300878524780273 67 -9.6400718688964844 68 -9.6508922576904297 69 -9.6625251770019531
		 70 -9.6741466522216797 71 -9.6845369338989258 72 -9.6927061080932617 73 -9.6978445053100586
		 74 -9.6999740600585938 75 -9.6995115280151367 76 -9.697509765625 77 -9.7096109390258789
		 78 -9.7332277297973633 79 -9.7654743194580078 80 -9.8036022186279297 81 -9.8429374694824219
		 82 -9.8829660415649414 83 -9.9242706298828125 84 -9.9672355651855469 85 -10.010465621948242
		 86 -10.049315452575684 87 -10.082013130187988 88 -10.10653018951416 89 -10.120742797851562
		 90 -10.122356414794922 91 -10.107569694519043 92 -10.081966400146484 93 -10.048647880554199
		 94 -10.009154319763184 95 -9.9639854431152344 96 -9.9133672714233398 97 -9.8584613800048828
		 98 -9.8032598495483398 99 -9.7490272521972656 100 -9.6966972351074219 101 -9.644805908203125
		 102 -9.5961008071899414 103 -9.5546407699584961 104 -9.5244569778442383 105 -9.5079259872436523
		 106 -9.5055255889892578 107 -9.5013675689697266 108 -9.4957485198974609 109 -9.4892978668212891
		 110 -9.4822912216186523 111 -9.4742488861083984 112 -9.4643182754516602 113 -9.4545860290527344
		 114 -9.4453868865966797 115 -9.4358491897583008 116 -9.424774169921875 117 -9.4130525588989258
		 118 -9.4025411605834961 119 -9.3931369781494141 120 -9.3845930099487305;
createNode animCurveTU -n "Character1_Hips_scaleX";
	rename -uid "58EAC83E-49FF-22EB-0A22-1F9670EA516C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_Hips_scaleY";
	rename -uid "B42452CF-49AC-6CC3-902D-D4BE6CBE16F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_Hips_scaleZ";
	rename -uid "563E56C0-4409-3EA8-E071-F1951AE5D127";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Hips_rotateX";
	rename -uid "380026D8-4255-7FF1-9D90-2BB13FC58195";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -6.9407939910888672 1 -7.2634682655334482
		 2 -7.6127800941467285 3 -7.9823813438415527 4 -8.3658475875854492 5 -8.7566347122192383
		 6 -9.1480569839477539 7 -9.5334148406982422 8 -9.9059953689575195 9 -10.259091377258301
		 10 -10.586098670959473 11 -10.880438804626465 12 -11.135528564453125 13 -11.344762802124023
		 14 -11.501399993896484 15 -11.598575592041016 16 -11.562501907348633 17 -11.523119926452637
		 18 -11.481880187988281 19 -11.439582824707031 20 -11.396856307983398 21 -11.354724884033203
		 22 -11.305139541625977 23 -11.246271133422852 24 -11.176479339599609 25 -11.094453811645508
		 26 -10.999002456665039 27 -10.888860702514648 28 -10.762733459472656 29 -10.624221801757812
		 30 -10.470345497131348 31 -10.441956520080566 32 -10.377788543701172 33 -10.281233787536621
		 34 -10.154411315917969 35 -10.007028579711914 36 -9.8441591262817383 37 -9.6709661483764648
		 38 -9.4923477172851562 39 -9.3129587173461914 40 -9.1375036239624023 41 -8.9706916809082031
		 42 -8.8172397613525391 43 -8.6820106506347656 44 -8.569915771484375 45 -8.4858779907226563
		 46 -8.3958568572998047 47 -8.2784080505371094 48 -8.1396074295043945 49 -7.9854011535644531
		 50 -7.8139662742614746 51 -7.6351313591003418 52 -7.4579663276672363 53 -7.2914190292358398
		 54 -7.1451363563537598 55 -7.0157299041748047 56 -6.907597541809082 57 -6.8249702453613281
		 58 -6.7720241546630859 59 -6.752983570098877 60 -6.7721490859985352 61 -6.8728432655334473
		 62 -7.0499377250671387 63 -7.2854838371276855 64 -7.5601787567138672 65 -7.8565368652343759
		 66 -8.1778659820556641 67 -8.5144186019897461 68 -8.8560667037963867 69 -9.1920680999755859
		 70 -9.51116943359375 71 -9.8018712997436523 72 -10.052348136901855 73 -10.250561714172363
		 74 -10.384490966796875 75 -10.442222595214844 76 -10.500947952270508 77 -10.622156143188477
		 78 -10.798224449157715 79 -11.021747589111328 80 -11.284807205200195 81 -11.572649002075195
		 82 -11.879927635192871 83 -12.198727607727051 84 -12.518886566162109 85 -12.828418731689453
		 86 -13.106696128845215 87 -13.336908340454102 88 -13.501621246337891 89 -13.583442687988281
		 90 -13.565179824829102 91 -13.318645477294922 92 -13.004700660705566 93 -12.6385498046875
		 94 -12.230974197387695 95 -11.79240894317627 96 -11.331935882568359 97 -10.856784820556641
		 98 -10.382574081420898 99 -9.9197311401367187 100 -9.4783439636230469 101 -9.061309814453125
		 102 -8.6821708679199219 103 -8.354313850402832 104 -8.0911779403686523 105 -7.9065299034118661
		 106 -7.8789048194885263 107 -7.8414754867553702 108 -7.7951574325561532 109 -7.7410664558410645
		 110 -7.6802167892456064 111 -7.6132721900939941 112 -7.5407052040100107 113 -7.4716072082519531
		 114 -7.4031591415405273 115 -7.3319549560546884 116 -7.2539749145507821 117 -7.1717748641967773
		 118 -7.0914154052734375 119 -7.0140705108642578 120 -6.9407939910888672;
createNode animCurveTA -n "Character1_Hips_rotateY";
	rename -uid "2B10EF16-49AA-8B67-133E-77A669838BF2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 116 ".ktv[0:115]"  0 11.395454406738281 1 11.467273712158203
		 2 11.538300514221191 3 11.607551574707031 4 11.674221992492676 5 11.737870216369629
		 6 11.798617362976074 7 11.856795310974121 8 11.912900924682617 9 11.967589378356934
		 10 12.021430015563965 11 12.074977874755859 12 12.128781318664551 13 12.183475494384766
		 14 12.240135192871094 15 12.299857139587402 16 12.266983985900879 17 12.341482162475586
		 18 12.4771728515625 19 12.633760452270508 20 12.770896911621094 21 12.842432975769043
		 22 12.915918350219727 23 12.985006332397461 24 13.043282508850098 25 13.08428955078125
		 26 13.101664543151855 27 13.089201927185059 28 13.040855407714844 29 12.894787788391113
		 30 12.673904418945312 31 12.592543601989746 32 12.517946243286133 33 12.46781063079834
		 34 12.471723556518555 38 12.470170021057129 39 12.465963363647461 41 12.458022117614746
		 43 12.456954002380371 44 12.460634231567383 45 12.467625617980957 46 12.274270057678223
		 47 12.03087329864502 48 11.746525764465332 49 11.43034839630127 50 11.166680335998535
		 51 10.923571586608887 52 10.674857139587402 53 10.394316673278809 54 10.050230979919434
		 55 9.7276096343994141 56 9.435765266418457 57 9.1840553283691406 58 8.9815883636474609
		 59 8.8378095626831055 60 8.7625503540039063 61 8.7566747665405273 62 8.7179117202758789
		 63 8.7028636932373047 64 8.7811393737792969 65 9.00885009765625 66 9.2638177871704102
		 67 9.5339508056640625 68 9.8074827194213867 69 10.072919845581055 70 10.319577217102051
		 71 10.537359237670898 72 10.716385841369629 73 10.846930503845215 74 10.918949127197266
		 75 10.922138214111328 76 10.975652694702148 77 11.148440361022949 78 11.420745849609375
		 79 11.772814750671387 80 12.185007095336914 81 12.711625099182129 82 13.292513847351074
		 83 13.874392509460449 84 14.40526008605957 85 14.829009056091309 86 15.203207015991211
		 87 15.512657165527344 88 15.743080139160154 89 15.88071823120117 90 15.911773681640625
		 91 15.929214477539063 92 15.799077033996582 93 15.47103977203369 94 15.005369186401367
		 95 14.456900596618652 96 13.881313323974609 97 13.340826988220215 98 12.784344673156738
		 99 12.234391212463379 100 11.713624000549316 101 11.31893253326416 102 11.032394409179687
		 103 10.841875076293945 104 10.73509693145752 105 10.693850517272949 106 10.75883960723877
		 107 10.826547622680664 108 10.896481513977051 109 10.96807861328125 110 11.040882110595703
		 111 11.114656448364258 112 11.189327239990234 113 11.180917739868164 114 11.132041931152344
		 115 11.093905448913574 116 11.118036270141602 117 11.189720153808594 118 11.259745597839355
		 119 11.328253746032715 120 11.395454406738281;
createNode animCurveTA -n "Character1_Hips_rotateZ";
	rename -uid "CBD36569-42A6-6BA5-86C5-9185D27CA880";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -7.7957358360290527 1 -8.0919065475463867
		 2 -8.4300146102905273 3 -8.8002300262451172 4 -9.1925201416015625 5 -9.5970239639282227
		 6 -10.002847671508789 7 -10.398479461669922 8 -10.772683143615723 9 -11.114470481872559
		 10 -11.413986206054688 11 -11.661893844604492 12 -11.849117279052734 13 -11.96629810333252
		 14 -12.00224781036377 15 -11.945116996765137 16 -11.884553909301758 17 -11.720065116882324
		 18 -11.454577445983887 19 -11.093344688415527 20 -10.641452789306641 21 -10.100990295410156
		 22 -9.5106115341186523 23 -8.8856306076049805 24 -8.241572380065918 25 -7.5949268341064444
		 26 -6.9613561630249023 27 -6.3549394607543945 28 -5.7891693115234375 29 -5.261873722076416
		 30 -4.7996101379394531 31 -4.7108592987060547 32 -4.5563602447509766 33 -4.3520417213439941
		 34 -4.1167111396789551 35 -3.8429567813873291 36 -3.541254997253418 37 -3.2224798202514648
		 38 -2.8973050117492676 39 -2.5751485824584961 40 -2.2643897533416748 41 -1.9736027717590332
		 42 -1.7115451097488403 43 -1.4886229038238525 44 -1.3162144422531128 45 -1.206273078918457
		 46 -1.0496046543121338 47 -0.82898473739624023 48 -0.55443263053894043 49 -0.23568829894065857
		 50 0.097463764250278473 51 0.44277554750442505 52 0.79559803009033203 53 1.150987982749939
		 54 1.506906270980835 55 1.8342475891113279 56 2.1216206550598145 57 2.3576831817626953
		 58 2.5298252105712891 59 2.6270065307617187 60 2.640549898147583 61 2.5148427486419678
		 62 2.3211021423339844 63 2.0556671619415283 64 1.7078585624694824 65 1.2720154523849487
		 66 0.79014050960540771 67 0.27581170201301575 68 -0.25681611895561218 69 -0.79392772912979126
		 70 -1.3201364278793335 71 -1.8186491727828979 72 -2.2729270458221436 73 -2.6666979789733887
		 74 -2.9854414463043213 75 -3.2155930995941162 76 -3.6021220684051514 77 -4.2090511322021484
		 78 -5.0044693946838379 79 -5.9558558464050293 80 -7.0303258895874023 81 -8.2193927764892578
		 82 -9.4810667037963867 83 -10.773658752441406 84 -12.053328514099121 85 -13.270907402038574
		 86 -14.416386604309082 87 -15.454991340637207 88 -16.351194381713867 89 -17.069740295410156
		 90 -17.575183868408203 91 -17.407938003540039 92 -17.24476432800293 93 -17.056053161621094
		 94 -16.858943939208984 95 -16.667587280273438 96 -16.493553161621094 97 -16.347305297851563
		 98 -16.201263427734375 99 -16.051765441894531 100 -15.893736839294434 101 -15.742658615112305
		 102 -15.58261775970459 103 -15.400748252868654 104 -15.18483352661133 105 -14.920504570007324
		 106 -14.796248435974119 107 -14.57758903503418 108 -14.275169372558594 109 -13.900712013244629
		 110 -13.464957237243652 111 -12.976460456848145 112 -12.442821502685547 113 -11.85145378112793
		 114 -11.226269721984863 115 -10.593752861022949 116 -9.9791221618652344 117 -9.3894805908203125
		 118 -8.8224992752075195 119 -8.2880744934082031 120 -7.7957358360290527;
createNode animCurveTU -n "Character1_Hips_visibility";
	rename -uid "9750DFF7-4313-1E98-D667-0DB4DC3F1D02";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftUpLeg_translateX";
	rename -uid "FEDBCDE3-4E25-FD6E-7B1F-7A851654D86A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.784505844116211;
createNode animCurveTL -n "Character1_LeftUpLeg_translateY";
	rename -uid "265EB651-4AAD-9DEE-9026-0E8BAE913F45";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5774786561451037e-006;
createNode animCurveTL -n "Character1_LeftUpLeg_translateZ";
	rename -uid "80B7E0EC-4573-ACF9-69B0-C4BCE0C44891";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0264549255371094;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleX";
	rename -uid "B735110C-4530-3F05-DFE2-A2BCA9AE0C6C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleY";
	rename -uid "8E802421-4F01-8FBC-9A80-56BDF6D1E097";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleZ";
	rename -uid "B1E58451-4A74-B50B-A29A-1099E61073BD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateX";
	rename -uid "8D3A80D4-451F-65D3-B83E-E2B4FE3D1B83";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -4.9229340553283691 1 -4.8521475791931152
		 2 -4.8547658920288086 3 -4.9739551544189453 4 -5.2094006538391113 5 -5.5230660438537598
		 6 -5.8533353805541992 7 -6.1656641960144043 8 -6.4576559066772461 9 -6.7581233978271484
		 10 -7.0896010398864746 11 -7.4203562736511222 12 -7.6659364700317383 13 -7.7320237159729004
		 14 -7.6814150810241708 15 -7.635474681854248 16 -7.4680933952331543 17 -7.3958024978637695
		 18 -7.3220481872558594 19 -7.1589860916137695 20 -6.8088960647583008 21 -6.2222714424133301
		 22 -5.5539803504943848 23 -4.8376278877258301 24 -4.1348171234130859 25 -3.5749866962432861
		 26 -3.281141996383667 27 -3.1886799335479736 28 -3.0529773235321045 29 -2.6688482761383057
		 30 -2.1815571784973145 31 -1.904768705368042 32 -1.6922276020050049 33 -1.5975311994552612
		 34 -1.5712323188781738 35 -1.5097832679748535 36 -1.5117274522781372 37 -1.6575642824172974
		 38 -1.9874645471572876 39 -2.426400899887085 40 -2.8724441528320312 41 -3.2094125747680664
		 42 -3.3386893272399902 43 -3.3253505229949951 44 -3.2867496013641357 45 -3.3317656517028809
		 46 -3.4312911033630371 47 -3.7316334247589111 48 -4.0408987998962402 49 -4.0770063400268555
		 50 -3.7188565731048584 51 -3.088533878326416 52 -2.3946652412414551 53 -1.830353856086731
		 54 -1.3577860593795776 55 -0.976936936378479 56 -0.75757473707199097 57 -0.77724474668502808
		 58 -1.2343376874923706 59 -2.1080763339996338 60 -3.0836701393127441 61 -3.7999827861785893
		 62 -3.7658081054687496 63 -3.2897813320159912 64 -2.8939695358276367 65 -2.7386069297790527
		 66 -2.627840518951416 67 -2.6743795871734619 68 -2.9298689365386963 69 -3.3790347576141357
		 70 -3.8985333442687993 71 -4.3690733909606934 72 -4.7002701759338379 73 -4.84197998046875
		 74 -4.8417215347290039 75 -4.7381782531738281 76 -4.9218349456787109 77 -5.3099603652954102
		 78 -5.829740047454834 79 -6.3687844276428223 80 -6.8251819610595703 81 -7.3181118965148926
		 82 -7.9453263282775879 83 -8.738041877746582 84 -9.5643825531005859 85 -10.254534721374512
		 86 -10.905551910400391 87 -11.430944442749023 88 -11.729896545410156 89 -11.609336853027344
		 90 -11.121916770935059 91 -10.687363624572754 92 -10.41673469543457 93 -10.533438682556152
		 94 -10.821882247924805 95 -11.032586097717285 96 -10.834824562072754 97 -10.091878890991211
		 98 -9.052943229675293 99 -8.0972805023193359 100 -7.4226050376892081 101 -7.1309309005737305
		 102 -7.0522699356079102 103 -7.0814495086669922 104 -7.0978455543518066 105 -7.0519490242004395
		 106 -7.096959114074707 107 -7.0431427955627441 108 -6.8885078430175781 109 -6.6341443061828613
		 110 -6.4394197463989258 111 -6.4085078239440918 112 -6.4196944236755371 113 -6.1553878784179687
		 114 -5.6848506927490234 115 -5.1919064521789551 116 -4.8133726119995117 117 -4.5940604209899902
		 118 -4.5411615371704102 119 -4.6828665733337402 120 -4.9229340553283691;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateY";
	rename -uid "E2DF50EA-4DD4-9FF8-5492-AE969AA9FEF0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 21.506786346435547 1 22.570781707763672
		 2 23.750215530395508 3 24.920658111572266 4 25.949481964111328 5 26.715169906616211
		 6 27.281000137329102 7 27.792932510375977 8 28.400032043457031 9 29.215204238891602
		 10 30.109254837036136 11 30.868907928466797 12 31.282951354980469 13 31.134771347045902
		 14 30.553962707519528 15 29.86256217956543 16 29.388942718505859 17 29.344514846801754
		 18 29.504043579101566 19 29.583826065063473 20 29.327009201049801 21 28.749973297119144
		 22 28.023860931396484 23 27.10723876953125 24 25.958066940307617 25 24.417682647705078
		 26 22.618022918701172 27 20.943683624267578 28 19.730724334716797 29 18.985658645629883
		 30 18.533853530883789 31 18.695896148681641 32 18.91363525390625 33 19.137248992919922
		 34 19.481664657592773 35 19.966163635253906 36 20.426939010620117 37 20.680835723876953
		 38 20.602878570556641 39 20.310699462890625 40 19.978734970092773 41 19.78228759765625
		 42 19.857906341552734 43 20.094594955444336 44 20.288909912109375 45 20.229368209838867
		 46 19.624902725219727 47 18.640644073486328 48 17.646846771240234 49 16.988307952880859
		 50 16.898551940917969 51 17.180217742919922 52 17.563388824462891 53 17.741085052490234
		 54 17.681699752807617 55 17.553903579711914 56 17.28076171875 57 16.786321640014648
		 58 15.873608589172365 59 14.699155807495115 60 13.693245887756348 61 13.294995307922363
		 62 13.633570671081543 63 14.539636611938477 64 15.673934936523439 65 16.965433120727539
		 66 18.434041976928711 67 19.88298225402832 68 21.10150146484375 69 21.925521850585937
		 70 22.457221984863281 71 22.882888793945313 72 23.381847381591797 73 24.081453323364258
		 74 24.818193435668945 75 25.331081390380859 76 25.71998405456543 77 25.693500518798828
		 78 25.459962844848633 79 25.421493530273438 80 25.919246673583984 81 27.116748809814453
		 82 28.687013626098633 83 30.275955200195313 84 31.603044509887699 85 32.675323486328125
		 86 33.621513366699219 87 34.394359588623047 88 34.953189849853516 89 35.169528961181641
		 90 35.049598693847656 91 34.120349884033203 92 33.428737640380859 93 33.480278015136719
		 94 33.963962554931641 95 34.487350463867188 96 34.65631103515625 97 34.194404602050781
		 98 33.25689697265625 99 32.166343688964844 100 31.378044128417969 101 31.202249526977536
		 102 31.390151977539059 103 31.659990310668942 104 31.728605270385746 105 31.575340270996094
		 106 31.577337265014648 107 31.316020965576172 108 30.743135452270508 109 29.636590957641602
		 110 28.114406585693359 111 26.651268005371094 112 25.705230712890625 113 25.58087158203125
		 114 26.009201049804688 115 26.470865249633789 116 26.39619255065918 117 25.530668258666992
		 118 24.219566345214844 119 22.776716232299805 120 21.506786346435547;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateZ";
	rename -uid "1DC6630E-4099-3C19-E7AC-B18DD56AA8A9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -3.5466642379760742 1 -5.9881830215454102
		 2 -8.6081619262695312 3 -10.931483268737793 4 -12.469113349914551 5 -12.769772529602051
		 6 -12.181308746337891 7 -11.413848876953125 8 -11.19127082824707 9 -12.079622268676758
		 10 -13.585897445678711 11 -14.836137771606444 12 -14.931679725646974 13 -12.949726104736328
		 14 -9.5938606262207031 15 -6.3942222595214844 16 -4.7688021659851074 17 -5.6626834869384766
		 18 -8.0969991683959961 19 -10.832213401794434 20 -12.621627807617188 21 -13.389230728149414
		 22 -13.751194953918457 23 -13.425524711608887 24 -12.140357971191406 25 -9.0994806289672852
		 26 -4.7496447563171387 27 -0.64156985282897949 28 1.7279993295669556 29 2.1724555492401123
		 30 1.6598421335220337 31 0.43911603093147278 32 -1.1694556474685669 33 -2.854433536529541
		 34 -5.0595531463623047 35 -7.8965325355529794 36 -10.611837387084961 37 -12.453414916992188
		 38 -12.95203685760498 39 -12.5909423828125 40 -12.047366142272949 41 -11.994145393371582
		 42 -12.939656257629395 43 -14.403544425964355 44 -15.575525283813477 45 -15.647224426269531
		 46 -13.73961353302002 47 -10.585572242736816 48 -7.5649452209472665 49 -6.041724681854248
		 50 -6.8197293281555176 51 -9.0037708282470703 52 -11.469505310058594 53 -13.100185394287109
		 54 -13.833089828491211 55 -14.214055061340332 56 -13.976604461669922 57 -12.855236053466797
		 58 -10.136294364929199 59 -6.1757545471191406 60 -2.3096461296081543 61 0.11558134853839874
		 62 0.024379128590226173 63 -1.822939872741699 64 -3.9557847976684566 65 -6.1351208686828613
		 66 -8.6721811294555664 67 -11.00419807434082 68 -12.555129051208496 69 -12.876116752624512
		 70 -12.346574783325195 71 -11.630866050720215 72 -11.406102180480957 73 -12.197087287902832
		 74 -13.526554107666016 75 -14.562731742858887 76 -14.54622173309326 77 -12.58828067779541
		 78 -9.317596435546875 79 -6.1415834426879883 80 -4.4962558746337891 81 -5.2976727485656738
		 82 -7.6834926605224618 83 -10.489636421203613 84 -12.460299491882324 85 -13.460649490356445
		 86 -14.083942413330078 87 -13.986530303955078 88 -12.81321907043457 89 -9.5914545059204102
		 90 -4.7699398994445801 91 -0.30317226052284241 92 2.2185804843902588 93 -0.54553854465484619
		 94 -6.2466945648193359 95 -12.039501190185547 96 -15.047215461730955 97 -13.719284057617188
		 98 -9.8984947204589844 99 -5.8700051307678223 100 -3.8100278377532959 101 -4.6891436576843262
		 102 -7.1773843765258789 103 -10.022053718566895 104 -11.965320587158203 105 -12.931988716125488
		 106 -13.493748664855957 107 -13.344217300415039 108 -12.18412971496582 109 -8.9171915054321289
		 110 -4.1104011535644531 111 0.10028882324695587 112 1.6823571920394897 113 -0.95006459951400768
		 114 -6.3221020698547363 115 -11.754188537597656 116 -14.565189361572266 117 -13.681197166442871
		 118 -10.685057640075684 119 -6.8845438957214355 120 -3.5466642379760742;
createNode animCurveTU -n "Character1_LeftUpLeg_visibility";
	rename -uid "ED256F92-42A9-F09E-25C3-72A107A15F92";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftLeg_translateX";
	rename -uid "31F67261-42FE-4D49-E1DB-0DAC3C26E65F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.968876838684082;
createNode animCurveTL -n "Character1_LeftLeg_translateY";
	rename -uid "8B8790B0-4750-907E-F6EA-35967F9A0815";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.2585945129394531;
createNode animCurveTL -n "Character1_LeftLeg_translateZ";
	rename -uid "394872A6-428C-F768-78D7-FA8163EBCC3E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 18.197568893432617;
createNode animCurveTU -n "Character1_LeftLeg_scaleX";
	rename -uid "347F1530-4546-629E-0060-78A45DB1D9CE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_LeftLeg_scaleY";
	rename -uid "C2595152-4AB3-404C-C32F-09A33B79C74F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftLeg_scaleZ";
	rename -uid "660E763D-4165-5D6C-87CA-9DBB49A3D944";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftLeg_rotateX";
	rename -uid "83DA0596-4271-A7EE-F019-E0A636EE5F98";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 52.102336883544922 1 52.498855590820312
		 2 52.887279510498047 3 53.310665130615234 4 53.824127197265625 5 54.481712341308594
		 6 55.237434387207031 7 55.995674133300781 8 56.653694152832031 9 57.126777648925781
		 10 57.468696594238281 11 57.783187866210938 12 58.173923492431641 13 58.749855041503906
		 14 59.425987243652351 15 60.00469970703125 16 60.048641204833984 17 59.510856628417962
		 18 58.558525085449219 19 57.394199371337891 20 56.191783905029297 21 54.960281372070313
		 22 53.679801940917969 23 52.417110443115234 24 51.236240386962891 25 50.250045776367188
		 26 49.400581359863281 27 48.537643432617187 28 47.617996215820313 29 46.736259460449219
		 30 46.064926147460938 31 45.983116149902344 32 46.117458343505859 33 46.443241119384766
		 34 46.893192291259766 35 47.373100280761719 36 47.903667449951172 37 48.539421081542969
		 38 49.320507049560547 39 50.173332214355469 40 50.993175506591797 41 51.671630859375
		 42 52.11651611328125 43 52.352748870849609 44 52.451183319091797 45 52.485992431640625
		 46 52.638191223144531 47 52.924449920654297 48 53.176841735839844 49 53.243499755859375
		 50 53.072044372558594 51 52.749839782714844 52 52.394874572753906 53 52.131023406982422
		 54 51.952816009521484 55 51.829475402832031 56 51.800258636474609 57 51.902873992919922
		 58 52.22369384765625 59 52.709007263183594 60 53.202899932861328 61 53.804882049560547
		 62 54.246528625488281 63 54.585586547851563 64 54.9747314453125 65 55.439590454101563
		 66 55.862888336181641 67 56.296436309814453 68 56.800685882568359 69 57.418617248535149
		 70 58.077583312988281 71 58.663745880126946 72 59.058284759521484 73 59.164226531982429
		 74 59.02848815917968 75 58.741783142089844 76 58.832332611083977 77 59.060207366943359
		 78 59.361686706542969 79 59.559783935546868 80 59.458229064941406 81 58.975734710693366
		 82 58.259483337402344 83 57.490245819091797 84 56.828964233398438 85 56.282245635986328
		 86 55.826526641845703 87 55.52618408203125 88 55.447883605957031 89 55.728008270263672
		 90 56.351665496826172 91 57.127693176269531 92 57.699195861816399 93 57.596946716308601
		 94 57.113330841064453 95 56.597759246826172 96 56.382396697998047 97 56.663139343261719
		 98 57.213809967041016 99 57.778919219970703 100 58.076366424560547 101 57.981269836425781
		 102 57.644317626953125 103 57.222679138183594 104 56.864616394042969 105 56.555660247802734
		 106 56.326087951660156 107 56.139072418212891 108 56.036983489990234 109 56.162078857421875
		 110 56.447097778320313 111 56.621330261230469 112 56.418083190917969 113 55.622928619384766
		 114 54.494098663330078 115 53.376438140869141 116 52.590526580810547 117 52.261615753173828
		 118 52.175880432128906 119 52.168937683105469 120 52.102336883544922;
createNode animCurveTA -n "Character1_LeftLeg_rotateY";
	rename -uid "FB61CFB9-444F-B3DC-2C6E-F2A312D3B248";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 21.536457061767578 1 18.573993682861328
		 2 15.40744113922119 3 12.553844451904297 4 10.529463768005371 5 9.794306755065918
		 6 9.9763908386230469 7 10.341529846191406 8 10.150960922241211 9 8.8471708297729492
		 10 6.9611530303955078 11 5.3812875747680664 12 4.9875273704528809 13 6.6803245544433594
		 14 9.7996463775634766 15 12.861849784851074 16 14.565853118896483 17 13.741395950317383
		 18 11.377883911132812 19 8.7341642379760742 20 7.0432381629943848 21 6.3521928787231445
		 22 6.0491399765014648 23 6.4345393180847168 24 7.8073678016662598 25 11.019078254699707
		 26 15.636964797973631 27 20.036602020263672 28 22.609735488891602 29 23.162891387939453
		 30 22.741096496582031 31 21.472927093505859 32 19.834709167480469 33 18.143661499023438
		 34 15.916022300720215 35 13.029827117919922 36 10.271495819091797 37 8.4241018295288086
		 38 7.9793453216552725 39 8.43682861328125 40 9.0863132476806641 41 9.2094850540161133
		 42 8.2622556686401367 43 6.7586464881896973 44 5.5603947639465332 45 5.5218601226806641
		 46 7.6351966857910156 47 11.12641429901123 48 14.540055274963379 49 16.385284423828125
		 50 15.73512077331543 51 13.550854682922363 52 11.058369636535645 53 9.4683494567871094
		 54 8.8380098342895508 55 8.5613069534301758 56 8.9431638717651367 57 10.29233455657959
		 58 13.422983169555664 59 17.941371917724609 60 22.323280334472656 61 24.963014602661133
		 62 24.65656852722168 63 22.256559371948242 64 19.450649261474609 65 16.53272819519043
		 66 13.197229385375977 67 10.104621887207031 68 7.9084982872009277 69 7.1018586158752441
		 70 7.2647247314453125 71 7.676938533782959 72 7.6157565116882315 73 6.5413813591003418
		 74 4.9998931884765625 75 3.8907339572906494 76 3.8753023147583003 77 5.9044680595397949
		 78 9.3014755249023437 79 12.572023391723633 80 14.199749946594238 81 13.278656959533691
		 82 10.821122169494629 83 8.0842533111572266 84 6.2753763198852539 85 5.4339485168457031
		 86 4.9698319435119629 87 5.1832032203674316 88 6.3751707077026367 89 9.401911735534668
		 90 13.872878074645996 91 18.236200332641602 92 20.936943054199219 93 18.756567001342773
		 94 13.846543312072754 95 8.8514280319213867 96 6.4100446701049805 97 7.9795508384704581
		 98 11.96564769744873 99 16.301105499267578 100 18.815155029296875 101 18.409690856933594
		 102 16.335891723632813 103 13.833040237426758 104 12.15169620513916 105 11.346895217895508
		 106 10.776555061340332 107 10.930639266967773 108 12.13027286529541 109 15.530981063842773
		 110 20.598161697387695 111 25.108940124511719 112 26.857110977172852 113 24.11909294128418
		 114 18.476545333862305 115 12.786250114440918 116 9.8475399017333984 117 10.767335891723633
		 118 13.921762466430664 119 17.962274551391602 120 21.536457061767578;
createNode animCurveTA -n "Character1_LeftLeg_rotateZ";
	rename -uid "D14112A1-4F2A-F187-8309-EA83339ED98B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 17.270214080810547 1 17.96307373046875
		 2 18.634513854980469 3 19.233737945556641 4 19.730894088745117 5 20.122209548950195
		 6 20.460121154785156 7 20.790010452270508 8 21.118837356567383 9 21.421934127807617
		 10 21.672861099243164 11 21.87217903137207 12 22.041889190673828 13 22.228923797607422
		 14 22.457578659057617 15 22.681123733520508 16 22.666252136230469 17 22.39598274230957
		 18 21.985620498657227 19 21.553165435791016 20 21.127761840820313 21 20.667482376098633
		 22 20.175106048583984 23 19.614805221557617 24 18.9229736328125 25 17.946744918823242
		 26 16.65985107421875 27 15.268384933471678 28 14.117657661437988 29 13.393174171447754
		 30 13.035114288330078 31 13.288914680480957 32 13.775622367858887 33 14.385965347290041
		 34 15.176542282104492 35 16.096942901611328 36 16.963455200195313 37 17.636905670166016
		 38 18.066198348999023 39 18.359149932861328 40 18.619382858276367 41 18.909002304077148
		 42 19.24370002746582 43 19.54823112487793 44 19.744319915771484 45 19.762907028198242
		 46 19.554317474365234 47 19.235132217407227 48 18.92216682434082 49 18.717502593994141
		 50 18.705846786499023 51 18.823049545288086 52 18.982295989990234 53 19.076560974121094
		 54 19.082841873168945 55 19.065900802612305 56 18.996818542480469 57 18.848367691040039
		 58 18.556447982788086 59 18.183254241943359 60 17.877866744995117 61 17.926414489746094
		 62 18.273416519165039 63 18.798831939697266 64 19.369495391845703 65 19.951217651367188
		 66 20.492000579833984 67 20.955783843994141 68 21.334590911865234 69 21.647899627685547
		 70 21.924955368041992 71 22.167024612426758 72 22.342544555664062 73 22.411212921142578
		 74 22.384077072143555 75 22.293952941894531 76 22.328330993652344 77 22.379837036132812
		 78 22.441238403320313 79 22.45777702331543 80 22.355043411254883 81 22.128257751464844
		 82 21.863101959228516 83 21.631734848022461 84 21.444919586181641 85 21.274080276489258
		 86 21.125791549682617 87 20.992300033569336 88 20.867122650146484 89 20.745147705078125
		 90 20.696002960205078 91 20.796648025512695 92 20.94990348815918 93 21.041492462158203
		 94 21.102565765380859 95 21.180694580078125 96 21.246309280395508 97 21.266904830932617
		 98 21.278709411621094 99 21.313796997070313 100 21.331300735473633 101 21.296783447265625
		 102 21.227483749389648 103 21.152959823608398 104 21.075708389282227 105 20.97480583190918
		 106 20.902219772338867 107 20.798622131347656 108 20.652383804321289 109 20.435708999633789
		 110 20.161520004272461 111 19.860387802124023 112 19.54637336730957 113 19.2750244140625
		 114 19.179531097412109 115 19.239967346191406 116 19.234306335449219 117 18.953098297119141
		 118 18.459218978881836 119 17.85725212097168 120 17.270214080810547;
createNode animCurveTU -n "Character1_LeftLeg_visibility";
	rename -uid "804429F3-4BD6-92C1-9054-378A5A75BD90";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftFoot_translateX";
	rename -uid "BFC5DB84-435A-5CED-6163-2380AB4760B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5243232250213623;
createNode animCurveTL -n "Character1_LeftFoot_translateY";
	rename -uid "3D97D34D-4E3A-F368-CAED-36A97F04230B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.000351905822754;
createNode animCurveTL -n "Character1_LeftFoot_translateZ";
	rename -uid "0FF815D2-4E05-BAD8-3D77-1194C204BEF2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.967419624328613;
createNode animCurveTU -n "Character1_LeftFoot_scaleX";
	rename -uid "301C53DD-4137-B473-43C7-FE80DE6EE501";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFoot_scaleY";
	rename -uid "4A9E2B96-4D7F-A777-CBBF-29808A454D8C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleZ";
	rename -uid "1B3026D7-4D98-7EBB-6AE6-07B52C5CE399";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999964237213135;
createNode animCurveTA -n "Character1_LeftFoot_rotateX";
	rename -uid "E44A5B76-4D91-3361-4135-D6AB87AB7BD2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 41.134159088134766 1 40.245990753173828
		 2 39.209243774414063 3 38.250656127929688 4 37.645622253417969 5 37.66729736328125
		 6 38.152240753173828 7 38.724605560302734 8 38.998691558837891 9 38.663673400878906
		 10 37.961982727050781 11 37.3656005859375 12 37.389934539794922 13 38.556434631347656
		 14 40.427505493164062 15 42.131736755371094 16 42.891490936279297 17 42.203453063964844
		 18 40.577075958251953 19 38.65118408203125 20 37.126964569091797 21 36.094520568847656
		 22 35.243400573730469 23 34.741641998291016 24 34.736209869384766 25 35.605049133300781
		 26 37.030506134033203 27 38.240997314453125 28 38.704750061035156 29 38.51495361328125
		 30 38.127388000488281 31 37.680671691894531 32 37.202297210693359 33 36.779617309570313
		 34 36.179592132568359 35 35.294136047363281 36 34.428562164306641 37 33.963027954101563
		 38 34.177902221679688 39 34.838947296142578 40 35.5772705078125 41 36.012943267822266
		 42 35.840297698974609 43 35.285377502441406 44 34.788116455078125 45 34.817371368408203
		 46 36.000400543212891 47 37.862545013427734 48 39.592552185058594 49 40.514141082763672
		 50 40.272018432617188 51 39.3004150390625 52 38.142688751220703 53 37.412422180175781
		 54 37.180320739746094 55 37.137310028076172 56 37.429515838623047 57 38.193439483642578
		 58 39.778091430664062 59 41.889675140380859 60 43.782672882080078 61 45.010845184326172
		 62 45.182144165039063 63 44.590702056884766 64 43.806228637695312 65 42.906044006347656
		 66 41.727287292480469 67 40.561756134033203 68 39.769447326660156 69 39.661113739013672
		 70 40.035312652587891 71 40.496074676513672 72 40.63818359375 73 40.144222259521484
		 74 39.287189483642578 75 38.569469451904297 76 38.604438781738281 77 39.667865753173828
		 78 41.321231842041016 79 42.709316253662109 80 43.112464904785156 81 42.162151336669922
		 82 40.360843658447266 83 38.338497161865234 84 36.797187805175781 85 35.836402893066406
		 86 35.152397155761719 87 34.933052062988281 88 35.357982635498047 89 36.882682800292969
		 90 39.216922760009766 91 41.362960815429687 92 42.680393218994141 93 41.846450805664063
		 94 39.672885894775391 95 37.291461944580078 96 36.192596435546875 97 37.298561096191406
		 98 39.625812530517578 99 41.944206237792969 100 43.243927001953125 101 43.154281616210938
		 102 42.25030517578125 103 41.060863494873047 104 40.189350128173828 105 39.694122314453125
		 106 39.331233978271484 107 39.295204162597656 108 39.733909606933594 109 41.154426574707031
		 110 43.168586730957031 111 44.744709014892578 112 45.155109405517578 113 43.869453430175781
		 114 41.292804718017578 115 38.453781127929687 116 36.782596588134766 117 36.996898651123047
		 118 38.267704010009766 119 39.855304718017578 120 41.134159088134766;
createNode animCurveTA -n "Character1_LeftFoot_rotateY";
	rename -uid "8A06C346-4B0C-B9F5-1D76-EF8136D3E236";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -7.5173330307006836 1 -8.9785642623901367
		 2 -10.502828598022461 3 -11.84239387512207 4 -12.779974937438965 5 -13.135174751281738
		 6 -13.076123237609863 7 -12.921692848205566 8 -13.009499549865723 9 -13.599331855773926
		 10 -14.43638801574707 11 -15.117544174194336 12 -15.272854804992676 13 -14.501654624938963
		 14 -13.017735481262207 15 -11.468497276306152 16 -10.573819160461426 17 -10.909589767456055
		 18 -11.932479858398438 19 -12.967556953430176 20 -13.460526466369629 21 -13.450356483459473
		 22 -13.231235504150391 23 -12.698575973510742 24 -11.746879577636719 25 -10.014739990234375
		 26 -7.6292271614074707 27 -5.2919497489929199 28 -3.8209612369537354 29 -3.3739616870880127
		 30 -3.4623720645904541 31 -4.0713386535644531 32 -4.9096851348876953 33 -5.8153696060180664
		 34 -6.9945440292358398 35 -8.4712715148925781 36 -9.8808937072753906 37 -10.904668807983398
		 38 -11.360635757446289 39 -11.440131187438965 40 -11.411009788513184 41 -11.56406307220459
		 42 -12.124908447265625 43 -12.859333992004395 44 -13.398008346557617 45 -13.399872779846191
		 46 -12.498918533325195 47 -10.959067344665527 48 -9.361790657043457 49 -8.4404439926147461
		 50 -8.6944828033447266 51 -9.6470108032226562 52 -10.694502830505371 53 -11.314059257507324
		 54 -11.515558242797852 55 -11.569242477416992 56 -11.350510597229004 57 -10.721982955932617
		 58 -9.2899513244628906 59 -7.1316952705383301 60 -4.9170317649841309 61 -3.5576431751251221
		 62 -3.750935316085815 63 -5.0623493194580078 64 -6.570197582244873 65 -8.1104755401611328
		 66 -9.8217678070068359 67 -11.371872901916504 68 -12.474559783935547 69 -12.936077117919922
		 70 -12.947445869445801 71 -12.821093559265137 72 -12.885607719421387 73 -13.388043403625488
		 74 -14.061091423034668 75 -14.494656562805174 76 -14.505084991455078 77 -13.583352088928223
		 78 -11.954072952270508 79 -10.272550582885742 80 -9.3417091369628906 81 -9.6695404052734375
		 82 -10.705986022949219 83 -11.791996002197266 84 -12.395991325378418 85 -12.561625480651855
		 86 -12.571191787719727 87 -12.317626953125 88 -11.683871269226074 89 -10.270510673522949
		 90 -8.1443510055541992 91 -6.1635875701904297 92 -5.0060434341430664 93 -6.4979619979858398
		 94 -9.3675899505615234 95 -12.119307518005371 96 -13.600441932678223 97 -13.308064460754395
		 98 -11.849807739257813 99 -10.066895484924316 100 -9.0897417068481445 101 -9.5777912139892578
		 102 -10.862827301025391 103 -12.263632774353027 104 -13.162385940551758 105 -13.556347846984863
		 106 -13.732865333557129 107 -13.57850456237793 108 -12.941017150878906 109 -11.235271453857422
		 110 -8.6070919036865234 111 -6.1508522033691406 112 -5.1520776748657227 113 -6.6066255569458008
		 114 -9.4722261428833008 115 -12.113754272460937 116 -13.326671600341797 117 -12.8306884765625
		 118 -11.331038475036621 119 -9.3523054122924805 120 -7.5173330307006836;
createNode animCurveTA -n "Character1_LeftFoot_rotateZ";
	rename -uid "14A09DAA-4E1F-3CBA-C263-CD978B7562EA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 14.605794906616211 1 14.779473304748535
		 2 14.948989868164062 3 15.095928192138672 4 15.207614898681642 5 15.275721549987793
		 6 15.317499160766602 7 15.357171058654787 8 15.412047386169434 9 15.494352340698242
		 10 15.587072372436522 11 15.661887168884277 12 15.692254066467287 13 15.655856132507324
		 14 15.583218574523926 15 15.517072677612303 16 15.457291603088379 17 15.403275489807129
		 18 15.34977340698242 19 15.283652305603027 20 15.175620079040527 21 15.014333724975588
		 22 14.819791793823242 23 14.599648475646971 24 14.360891342163086 25 14.092800140380859
		 26 13.788161277770996 27 13.473411560058594 28 13.209657669067383 29 13.040260314941406
		 30 12.956546783447266 31 12.993769645690918 32 13.09050178527832 33 13.228961944580078
		 34 13.418249130249023 35 13.651164054870605 36 13.892911911010742 37 14.111422538757324
		 38 14.283991813659668 39 14.420773506164549 40 14.540536880493164 41 14.663718223571777
		 42 14.810127258300781 43 14.956252098083498 44 15.056787490844728 45 15.064476966857912
		 46 14.961457252502441 47 14.799121856689455 48 14.649923324584961 49 14.578858375549316
		 50 14.61872386932373 51 14.724732398986816 52 14.842756271362305 53 14.919631958007813
		 54 14.945618629455565 55 14.948505401611328 56 14.932520866394043 57 14.902994155883789
		 58 14.850153923034666 59 14.77496337890625 60 14.70042133331299 61 14.696713447570801
		 62 14.764867782592775 63 14.872304916381836 64 14.98899555206299 65 15.113138198852539
		 66 15.237216949462892 67 15.348855018615724 68 15.438966751098633 69 15.503617286682129
		 70 15.551058769226074 71 15.588686943054201 72 15.619023323059082 73 15.645837783813477
		 74 15.662832260131838 75 15.653697967529299 76 15.66203784942627 77 15.602934837341309
		 78 15.493560791015625 79 15.362226486206053 80 15.232035636901854 81 15.129347801208496
		 82 15.063694953918457 83 15.023801803588869 84 14.972292900085451 85 14.892536163330078
		 86 14.805121421813965 87 14.714068412780763 88 14.623532295227049 89 14.517558097839355
		 90 14.41089916229248 91 14.366722106933594 92 14.389809608459474 93 14.571066856384279
		 94 14.833982467651367 95 15.088771820068361 96 15.251906394958494 97 15.275951385498047
		 98 15.225510597229002 99 15.184394836425781 100 15.209131240844727 101 15.307838439941406
		 102 15.433516502380371 103 15.545804977416992 104 15.608133316040039 105 15.610320091247559
		 106 15.580039978027346 107 15.537796974182129 108 15.489152908325197 109 15.417999267578125
		 110 15.307747840881346 111 15.173330307006836 112 15.066749572753908 113 15.065217971801756
		 114 15.145291328430176 115 15.23433208465576 116 15.256874084472654 117 15.17453098297119
		 118 15.016061782836912 119 14.811547279357908 120 14.605794906616211;
createNode animCurveTU -n "Character1_LeftFoot_visibility";
	rename -uid "8108386C-45DB-0DF9-3C0A-F1929816CFB2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftToeBase_translateX";
	rename -uid "FC627C23-4A33-E454-DEFD-ED90B58AD239";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.585639476776123;
createNode animCurveTL -n "Character1_LeftToeBase_translateY";
	rename -uid "68D848C2-41C2-7FAA-386C-1A897F2DF641";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3360438346862793;
createNode animCurveTL -n "Character1_LeftToeBase_translateZ";
	rename -uid "4AFC53A8-4836-7F9F-9190-D8A44B013661";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.4897398948669434;
createNode animCurveTU -n "Character1_LeftToeBase_scaleX";
	rename -uid "F8AE9434-42AE-B13C-0D3B-79A46A581CCD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftToeBase_scaleY";
	rename -uid "E2B60100-4727-24A6-14BB-AE8BC27E61D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftToeBase_scaleZ";
	rename -uid "17A35CBE-4EB2-5CCB-1220-54B7A78E9478";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_LeftToeBase_rotateX";
	rename -uid "4066D988-4009-62BB-EA44-B49E7DC21C9C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.34498978621167e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateY";
	rename -uid "DD58CC54-40A1-892F-115B-2DB8EAA86512";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.6689547521764325e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateZ";
	rename -uid "19829F0C-4F8C-CF74-1FFC-D6B971AEAF51";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.9797616879202451e-009;
createNode animCurveTU -n "Character1_LeftToeBase_visibility";
	rename -uid "E5362E8D-47A5-4A04-074A-9A95C7C50368";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateX";
	rename -uid "7CF27F18-42C4-3EBF-7C5B-F3B33B86FB44";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7153444290161133;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateY";
	rename -uid "4F222D60-43E0-3739-5F54-7EB611D6F559";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.0020139217376709;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateZ";
	rename -uid "791F8D6B-4399-59C4-ACF5-62BFC17CBBA8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.1786108016967773;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleX";
	rename -uid "C95867EA-43C4-1E64-7E96-D1A752FDDD3E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleY";
	rename -uid "754730E2-4ABC-6F88-6D90-27808D667806";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999940395355225;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleZ";
	rename -uid "77D8E1A0-4EBA-4203-2E93-848764F53A44";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateX";
	rename -uid "03B40AAC-457A-14B8-44B9-B8A0F898B56B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.3304027858303016e-010;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateY";
	rename -uid "5F00E04B-4A36-BE2A-CE80-EAAA592E9251";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.528056126076649e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateZ";
	rename -uid "796B3552-4A40-2243-F85E-76A008B93F3E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.2206369772231938e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle1_visibility";
	rename -uid "82B0DC5A-4399-BBEC-11E8-99B16ECEB211";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateX";
	rename -uid "49028FEF-4772-B3E1-3D36-278DBADA1316";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4684751033782959;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateY";
	rename -uid "C47A7BA2-4C51-A3D9-7E57-8CB91E413034";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.1393222808837891;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateZ";
	rename -uid "FE9D1446-48F1-DD93-E655-FC9624BD2929";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.3014278411865234;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleX";
	rename -uid "101661CB-4C94-0F8D-A7A0-F38E8AC3DF7C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleY";
	rename -uid "6784DF7D-47BA-66FD-B3D5-538985AA0BEE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleZ";
	rename -uid "80D6A276-49D0-84FE-7EED-678C29367771";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateX";
	rename -uid "5EB0F009-455C-8444-05CE-689118B92BA2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2096632140412567e-008;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateY";
	rename -uid "3F4C0643-445B-26D5-2960-2A8F76B7FC63";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.4762588886965204e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateZ";
	rename -uid "60F2C367-4FD7-8810-4F67-B79E8A3D36F6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.1775266862155149e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle2_visibility";
	rename -uid "22D7892B-41F5-E562-E566-EB9CFEAD3A8F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightUpLeg_translateX";
	rename -uid "EDA696C6-402F-16CC-6474-59A72A78289C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.489771842956543;
createNode animCurveTL -n "Character1_RightUpLeg_translateY";
	rename -uid "A7E32AB4-403C-D9E8-3438-7F9274D1358A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.6505136489868164;
createNode animCurveTL -n "Character1_RightUpLeg_translateZ";
	rename -uid "999E5937-4A4B-5D2F-21BB-B6AFEC4D694A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0264549255371094;
createNode animCurveTU -n "Character1_RightUpLeg_scaleX";
	rename -uid "4B681E09-4D8A-806B-A8C9-2A998B4F08F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightUpLeg_scaleY";
	rename -uid "F13DCD2C-4039-441C-CA65-AEB888EDF850";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightUpLeg_scaleZ";
	rename -uid "49A65CD5-4893-D51A-290F-62B26ED26F34";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightUpLeg_rotateX";
	rename -uid "0B1C8716-47FB-4E4F-22F9-168DF3B2A99F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -31.774538040161133 1 -33.484523773193359
		 2 -35.447578430175781 3 -36.929916381835938 4 -37.186450958251953 5 -35.174648284912109
		 6 -31.610406875610352 7 -28.344141006469727 8 -27.136209487915039 9 -29.403802871704105
		 10 -33.935886383056641 11 -38.438373565673828 12 -40.486488342285156 13 -38.441013336181641
		 14 -33.932941436767578 15 -29.373893737792969 16 -27.069211959838867 17 -28.142454147338867
		 18 -31.195196151733398 19 -34.715038299560547 20 -37.136631011962891 21 -38.275646209716797
		 22 -38.872810363769531 23 -38.731685638427734 24 -37.661396026611328 25 -34.781002044677734
		 26 -30.525211334228516 27 -26.612522125244141 28 -24.646461486816406 29 -24.959079742431641
		 30 -26.475408554077148 31 -28.665973663330075 32 -30.999309539794918 33 -32.916416168212891
		 34 -34.723365783691406 35 -36.702659606933594 36 -38.201141357421875 37 -38.523941040039063
		 38 -36.566356658935547 39 -32.941753387451172 40 -29.543582916259762 41 -28.278886795043945
		 42 -30.757061004638668 43 -35.734485626220703 44 -40.700290679931641 45 -42.975772857666016
		 46 -40.780632019042969 47 -35.873405456542969 48 -30.863655090332028 49 -28.311428070068359
		 50 -29.482513427734375 51 -32.881832122802734 52 -36.839466094970703 53 -39.573333740234375
		 54 -40.843769073486328 55 -41.482425689697266 56 -41.239593505859375 57 -39.863986968994141
		 58 -36.309429168701172 59 -31.044843673706051 60 -26.101945877075195 61 -23.427230834960937
		 62 -25.044147491455078 63 -29.497005462646484 64 -33.338226318359375 65 -35.779426574707031
		 66 -38.016742706298828 67 -39.481349945068359 68 -39.615894317626953 69 -37.293144226074219
		 70 -33.180393218994141 71 -29.386693954467773 72 -27.922443389892578 73 -30.369070053100589
		 74 -35.282485961914063 75 -40.031021118164062 76 -41.910945892333984 77 -39.164897918701172
		 78 -33.720748901367188 79 -28.362277984619141 80 -25.610816955566406 81 -26.500545501708984
		 82 -29.436540603637699 83 -32.803138732910156 84 -35.041954040527344 85 -36.026546478271484
		 86 -36.525501251220703 87 -36.373779296875 88 -35.396209716796875 89 -32.812118530273438
		 90 -28.971689224243168 91 -25.322355270385742 92 -23.284734725952148 93 -25.802637100219727
		 94 -30.989906311035153 95 -36.316757202148438 96 -39.019058227539063 97 -37.345611572265625
		 98 -33.094066619873047 99 -28.682813644409183 100 -26.399349212646484 101 -27.26982307434082
		 102 -29.957321166992184 103 -33.098079681396484 104 -35.250301361083984 105 -36.232212066650391
		 106 -36.694046020507813 107 -36.463325500488281 108 -35.349544525146484 109 -32.308273315429688
		 110 -27.816814422607422 111 -23.903537750244141 112 -22.493234634399414 113 -25.166202545166016
		 114 -30.665679931640625 115 -36.515983581542969 116 -39.964679718017578 117 -39.792545318603516
		 118 -37.531051635742187 119 -34.441719055175781 120 -31.774538040161133;
createNode animCurveTA -n "Character1_RightUpLeg_rotateY";
	rename -uid "7722A3FD-473D-D27F-9793-BD94F5D13204";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 36.258934020996094 1 36.489658355712891
		 2 36.716228485107422 3 36.821460723876953 4 36.715118408203125 5 36.245429992675781
		 6 35.471488952636719 7 34.689640045166016 8 34.313228607177734 9 34.680027008056641
		 10 35.42352294921875 11 36.037551879882812 12 36.258453369140625 13 35.998237609863281
		 14 35.386157989501953 15 34.691093444824219 16 34.188549041748047 17 34.295635223388672
		 18 34.710250854492188 19 35.095272064208984 20 35.211536407470703 21 35.086933135986328
		 22 34.876758575439453 23 34.568305969238281 24 34.13775634765625 25 33.421257019042969
		 26 32.409839630126953 27 31.412103652954105 28 30.887722015380856 29 31.012615203857425
		 30 31.544681549072266 31 32.126251220703125 32 32.914180755615234 33 33.750587463378906
		 34 34.662643432617187 35 35.646461486816406 36 36.575958251953125 37 37.371028900146484
		 38 37.920639038085938 39 38.247440338134766 40 38.528865814208984 41 39.044475555419922
		 42 39.997795104980469 43 41.011810302734375 44 41.687156677246094 45 41.961566925048828
		 46 42.059577941894531 47 42.006862640380859 48 41.852329254150391 49 41.930244445800781
		 50 42.503238677978516 51 43.306636810302734 52 44.056720733642578 53 44.630073547363281
		 54 45.086593627929688 55 45.485969543457031 56 45.823173522949219 57 46.079364776611328
		 58 46.163806915283203 59 45.954734802246094 60 45.512130737304688 61 45.173820495605469
		 62 45.333957672119141 63 45.713756561279297 64 45.859149932861328 65 45.792221069335938
		 66 45.640308380126953 67 45.408077239990234 68 45.114574432373047 69 44.728485107421875
		 70 44.182773590087891 71 43.543567657470703 72 43.074012756347656 73 43.016609191894531
		 74 43.111186981201172 75 43.074565887451172 76 42.918216705322266 77 42.380863189697266
		 78 41.327991485595703 79 39.794319152832031 80 38.237567901611328 81 37.121982574462891
		 82 36.296108245849609 83 35.512107849121094 84 34.556251525878906 85 33.435649871826172
		 86 32.320693969726562 87 31.208139419555664 88 30.088865280151367 89 28.803928375244137
		 90 27.452365875244141 91 26.817489624023438 92 26.606794357299805 93 27.724897384643555
		 94 29.476694107055664 95 31.063901901245117 96 31.937894821166992 97 31.883668899536129
		 98 31.196796417236328 99 30.349954605102536 100 29.996067047119141 101 30.541305541992188
		 102 31.572257995605472 103 32.65655517578125 104 33.431732177734375 105 33.885574340820313
		 106 34.061988830566406 107 34.134979248046875 108 34.058586120605469 109 33.576713562011719
		 110 32.718856811523437 111 31.952714920043949 112 31.865013122558594 113 32.881599426269531
		 114 34.483776092529297 115 35.920822143554687 116 36.749439239501953 117 36.964706420898437
		 118 36.840980529785156 119 36.539196014404297 120 36.258934020996094;
createNode animCurveTA -n "Character1_RightUpLeg_rotateZ";
	rename -uid "425C9EA7-4B90-9A64-8BB1-F3A8AAF56B0B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -18.08891487121582 1 -18.596872329711914
		 2 -19.180866241455078 3 -19.540502548217773 4 -19.3553466796875 5 -18.180761337280273
		 6 -16.356870651245117 7 -14.694967269897461 8 -13.90992546081543 9 -14.583418846130369
		 10 -16.298379898071289 11 -18.130649566650391 12 -18.977224349975586 13 -18.061136245727539
		 14 -16.143447875976562 15 -14.338391304016113 16 -13.349512100219727 17 -13.894006729125977
		 18 -15.368033409118651 19 -17.13066291809082 20 -18.477567672729492 21 -19.296422958374023
		 22 -19.914999008178711 23 -20.242321014404297 24 -20.19566535949707 25 -19.411592483520508
		 26 -18.11798095703125 27 -17.037141799926758 28 -16.742038726806641 29 -17.28510856628418
		 30 -18.253007888793945 31 -19.204864501953125 32 -20.451215744018555 33 -21.744953155517578
		 34 -23.182306289672852 35 -24.848058700561523 36 -26.41529655456543 37 -27.503196716308594
		 38 -27.516021728515625 39 -26.678623199462891 40 -25.863542556762695 41 -25.971870422363281
		 42 -27.811561584472656 43 -30.841447830200195 44 -33.782630920410156 45 -35.15850830078125
		 46 -34.117992401123047 47 -31.738576889038086 48 -29.380176544189453 49 -28.339239120483398
		 50 -29.253248214721683 51 -31.372930526733402 52 -33.841529846191406 53 -35.68060302734375
		 54 -36.710262298583984 55 -37.371768951416016 56 -37.509731292724609 57 -36.967872619628906
		 58 -35.153934478759766 59 -32.329643249511719 60 -29.627466201782227 61 -28.083463668823242
		 62 -28.754747390747067 63 -30.918626785278317 64 -32.740379333496094 65 -33.782920837402344
		 66 -34.678371429443359 67 -35.111431121826172 68 -34.774494171142578 69 -33.047939300537109
		 70 -30.324365615844727 71 -27.7943115234375 72 -26.551551818847656 73 -27.434932708740234
		 74 -29.674238204956051 75 -31.836383819580078 76 -32.305206298828125 77 -29.915599822998043
		 78 -25.852655410766602 79 -21.743814468383789 80 -18.898891448974609 81 -17.706758499145508
		 82 -17.308748245239258 83 -16.950532913208008 84 -15.975674629211428 85 -14.413696289062498
		 86 -12.697017669677734 87 -10.854820251464844 88 -8.9377155303955078 89 -6.8102130889892578
		 90 -4.7577395439147949 91 -4.066582202911377 92 -3.792762279510498 93 -4.7893242835998535
		 94 -6.637939453125 95 -8.6187162399291992 96 -9.6984243392944336 97 -9.1599483489990234
		 98 -7.7007570266723642 99 -6.2588295936584473 100 -5.577946662902832 101 -5.9452004432678223
		 102 -6.9610466957092285 103 -8.2283668518066406 104 -9.2512445449829102 105 -9.9363298416137695
		 106 -10.246648788452148 107 -10.452542304992676 108 -10.46334171295166 109 -9.8855056762695313
		 110 -8.9156532287597656 111 -8.2657394409179687 112 -8.5165233612060547 113 -10.16486644744873
		 114 -12.945438385009766 115 -16.061260223388672 116 -18.343839645385742 117 -19.13862419128418
		 118 -19.015213012695313 119 -18.495149612426758 120 -18.08891487121582;
createNode animCurveTU -n "Character1_RightUpLeg_visibility";
	rename -uid "59055439-43E8-5C09-2D25-829C5E04F10A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightLeg_translateX";
	rename -uid "8DB3A34A-4C22-95FE-8E02-DFA6D505C257";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -18.197565078735352;
createNode animCurveTL -n "Character1_RightLeg_translateY";
	rename -uid "0298088B-4635-986B-027C-CDA6EAAB75B4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.1862249374389648;
createNode animCurveTL -n "Character1_RightLeg_translateZ";
	rename -uid "D3AD1AD7-4DF0-0CEB-85CF-75AD4E98BC75";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.5520458221435547;
createNode animCurveTU -n "Character1_RightLeg_scaleX";
	rename -uid "33411457-4937-23B7-834F-CAB0100B51FF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightLeg_scaleY";
	rename -uid "E1496A29-4394-8041-209D-7C9685686AC0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightLeg_scaleZ";
	rename -uid "8E22DC4E-4BD4-32A9-34C1-7FBDAF4239F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightLeg_rotateX";
	rename -uid "3DF9CA8A-4437-0F5A-B560-CEBED042D3CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 32.307552337646484 1 33.325393676757812
		 2 34.474395751953125 3 35.298030853271484 4 35.346412658691406 5 33.976348876953125
		 6 31.607435226440426 7 29.393186569213864 8 28.504222869873047 9 29.886018753051754
		 10 32.685474395751953 11 35.406742095947266 12 36.606826782226563 13 35.352909088134766
		 14 32.613479614257813 15 29.831100463867191 16 28.344669342041016 17 28.906288146972656
		 18 30.66352653503418 19 32.675594329833984 20 34.003238677978516 21 34.572578430175781
		 22 34.81036376953125 23 34.60186767578125 24 33.826866149902344 25 31.904325485229489
		 26 29.051225662231449 27 26.381181716918945 28 25.061016082763672 29 25.437801361083984
		 30 26.757846832275391 31 28.345310211181641 32 30.158084869384762 33 31.771156311035156
		 34 33.348209381103516 35 35.059722900390625 36 36.461357116699219 37 37.142250061035156
		 38 36.510063171386719 39 34.976318359375 40 33.617935180664062 41 33.465560913085938
		 42 35.289196014404297 43 38.229862213134766 44 40.928001403808594 45 42.132610321044922
		 46 41.240188598632812 47 39.079555511474609 48 36.903713226318359 49 35.996555328369141
		 50 36.945964813232422 51 38.943328857421875 52 41.132469177246094 53 42.7060546875
		 54 43.609870910644531 55 44.196788787841797 56 44.371829986572266 57 44.039237976074219
		 58 42.763179779052734 59 40.703529357910156 60 38.662773132324219 61 37.543956756591797
		 62 38.290634155273438 63 40.203468322753906 64 41.779472351074219 65 42.709308624267578
		 66 43.539321899414063 67 44.025485992431641 68 43.9254150390625 69 42.749073028564453
		 70 40.750450134277344 71 38.817550659179687 72 37.878787994384766 73 38.710628509521484
		 74 40.661754608154297 75 42.548614501953125 76 43.131927490234375 77 41.357078552246094
		 78 37.971256256103516 79 34.164680480957031 80 31.308324813842773 81 30.237144470214844
		 82 30.324508666992187 83 30.758077621459957 84 30.645635604858398 85 29.900426864624023
		 86 28.958150863647461 87 27.729198455810547 88 26.115301132202148 89 23.584310531616211
		 90 20.415977478027344 91 18.085691452026367 92 17.050834655761719 93 19.536811828613281
		 94 23.963827133178711 95 28.382854461669922 96 30.889961242675781 97 30.332574844360352
		 98 27.964130401611328 99 25.418256759643555 100 24.345676422119141 101 25.489055633544922
		 102 27.857412338256836 103 30.428974151611328 104 32.190128326416016 105 33.046775817871094
		 106 33.350414276123047 107 33.221565246582031 108 32.538944244384766 109 30.592191696166996
		 110 27.665214538574219 111 25.133283615112305 112 24.376520156860352 113 26.537073135375977
		 114 30.548925399780273 115 34.596698760986328 116 36.941162109375 117 36.956771850585938
		 118 35.672725677490234 119 33.866184234619141 120 32.307552337646484;
createNode animCurveTA -n "Character1_RightLeg_rotateY";
	rename -uid "A3A8BAB3-40E0-EA38-1797-D296FCB34A09";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -26.452003479003906 1 -26.616422653198242
		 2 -26.741437911987305 3 -26.922182083129883 4 -27.246662139892578 5 -27.847908020019531
		 6 -28.66617584228516 7 -29.475225448608398 8 -30.003129959106445 9 -30.024662017822266
		 10 -29.759723663330078 11 -29.556669235229496 12 -29.691631317138668 13 -30.329309463500973
		 14 -31.312395095825192 15 -32.370639801025391 16 -32.552829742431641 17 -32.163234710693359
		 18 -31.42069053649902 19 -30.562896728515625 20 -29.798414230346676 21 -29.142623901367191
		 22 -28.541311264038086 23 -28.037572860717773 24 -27.678915023803711 25 -27.61754035949707
		 26 -27.859601974487305 27 -28.200139999389648 28 -28.376144409179688 29 -28.29710578918457
		 30 -28.174488067626953 31 -27.952892303466797 32 -28.154325485229492 33 -28.796060562133786
		 34 -29.756155014038082 35 -30.905290603637695 36 -32.256809234619141 37 -33.813816070556641
		 38 -35.629043579101563 39 -37.580455780029297 40 -39.403572082519531 41 -40.811443328857422
		 42 -41.577228546142578 43 -41.873237609863281 44 -41.961380004882813 45 -42.018394470214844
		 46 -42.369140625 47 -43.076885223388672 48 -43.899589538574219 49 -44.532447814941406
		 50 -44.822189331054688 51 -44.935260772705078 52 -45.043880462646484 53 -45.280490875244141
		 54 -45.619770050048828 55 -45.990211486816406 56 -46.393741607666016 57 -46.835357666015625
		 58 -47.390613555908203 59 -48.037834167480469 60 -48.615184783935547 61 -49.0615234375
		 62 -49.081550598144531 63 -48.844886779785156 64 -48.70001220703125 65 -48.699237823486328
		 66 -48.693058013916016 67 -48.716789245605469 68 -48.803897857666016 69 -49.041904449462891
		 70 -49.390598297119141 71 -49.669708251953125 72 -49.653049468994141 73 -49.143081665039062
		 74 -48.296104431152344 75 -47.372993469238281 76 -46.756877899169922 77 -46.042774200439453
		 78 -45.182575225830078 79 -44.01904296875 80 -42.285305023193359 81 -39.827785491943359
		 82 -36.846969604492188 83 -33.598659515380859 84 -30.346168518066406 85 -27.181571960449219
		 86 -24.133893966674805 87 -21.359498977661133 88 -19.058862686157227 89 -17.579627990722656
		 90 -17.123859405517578 91 -18.323541641235352 92 -19.067739486694336 93 -18.768074035644531
		 94 -17.907752990722656 95 -17.036333084106445 96 -16.574878692626953 97 -16.729122161865234
		 98 -17.237176895141602 99 -17.793315887451172 100 -18.030399322509766 101 -17.807163238525391
		 102 -17.377002716064453 103 -16.988426208496094 104 -16.850933074951172 105 -16.970041275024414
		 106 -17.058195114135742 107 -17.382701873779297 108 -17.949165344238281 109 -18.891416549682617
		 110 -20.161245346069336 111 -21.449798583984375 112 -22.388999938964844 113 -22.642837524414063
		 114 -22.507944107055664 115 -22.440048217773438 116 -22.76490592956543 117 -23.524599075317383
		 118 -24.490942001342773 119 -25.520853042602539 120 -26.452003479003906;
createNode animCurveTA -n "Character1_RightLeg_rotateZ";
	rename -uid "C4B406B6-4619-DD61-C813-09900163FCAC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 2.6833248138427734 1 2.9219377040863037
		 2 3.2101366519927979 3 3.4059104919433594 4 3.3621230125427246 5 2.8725349903106689
		 6 2.0784287452697754 7 1.3433208465576172 8 1.0079708099365234 9 1.3484104871749878
		 10 2.1413233280181885 11 2.943673849105835 12 3.2663826942443848 13 2.7699244022369385
		 14 1.7813310623168945 15 0.78600090742111206 16 0.36266189813613892 17 0.60108101367950439
		 18 1.2322210073471069 19 1.9659241437911987 20 2.4960010051727295 21 2.7887270450592041
		 22 2.9730730056762695 23 3.0105123519897461 24 2.8641858100891113 25 2.3625192642211914
		 26 1.5938602685928345 27 0.89323943853378296 28 0.56010156869888306 29 0.65984433889389038
		 30 0.98407042026519775 31 1.4027401208877563 32 1.8074667453765869 33 2.092465877532959
		 34 2.3185064792633057 35 2.5571582317352295 36 2.6733288764953613 37 2.5183851718902588
		 38 1.8542470932006836 39 0.80841034650802612 40 -0.20884636044502258 41 -0.76231688261032104
		 42 -0.4603475034236908 43 0.45414036512374872 44 1.4367351531982422 45 1.8917392492294312
		 46 1.4127718210220337 47 0.30841958522796631 48 -0.83908283710479736 49 -1.4461700916290283
		 50 -1.2352170944213867 51 -0.53057724237442017 52 0.29972827434539795 53 0.86195337772369385
		 54 1.112191915512085 55 1.2161440849304199 56 1.1194455623626709 57 0.76674085855484009
		 58 -0.081304557621479034 59 -1.3282458782196045 60 -2.514693021774292 61 -3.2331879138946533
		 62 -2.9382429122924805 63 -1.9854654073715208 64 -1.2004297971725464 65 -0.77095699310302734
		 66 -0.37724897265434265 67 -0.15905629098415375 68 -0.25637423992156982 69 -0.94573962688446034
		 70 -2.0602319240570068 71 -3.0710268020629883 72 -3.4562010765075684 73 -2.7981066703796387
		 74 -1.4718595743179321 75 -0.15017487108707428 76 0.40287715196609497 77 -0.056297633796930313
		 78 -1.0304372310638428 79 -1.8585392236709593 80 -2.0028760433197021 81 -1.3554593324661255
		 82 -0.3333895206451416 83 0.7104305624961853 84 1.4784588813781738 85 1.959135055541992
		 86 2.2814311981201172 87 2.4291768074035645 88 2.3905143737792969 89 2.0534851551055908
		 90 1.4982764720916748 91 0.92462444305419933 92 0.64663559198379517 93 1.1176282167434692
		 94 2.0801918506622314 95 3.1351931095123291 96 3.7724318504333496 97 3.6307857036590576
		 98 3.030475378036499 99 2.4048192501068115 100 2.1471898555755615 101 2.4144394397735596
		 102 2.9826319217681885 103 3.6161034107208256 104 4.0489630699157715 105 4.2419705390930176
		 106 4.3066129684448242 107 4.2382311820983887 108 4.0034666061401367 109 3.4157674312591553
		 110 2.5681936740875244 111 1.8259631395339968 112 1.5197407007217407 113 1.9449537992477415
		 114 2.8988354206085205 115 3.9357049465179439 116 4.5226621627807617 117 4.417506217956543
		 118 3.9156966209411621 119 3.2580554485321045 120 2.6833248138427734;
createNode animCurveTU -n "Character1_RightLeg_visibility";
	rename -uid "CA510CBF-45F9-03C0-DBE5-6CBCE9053365";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightFoot_translateX";
	rename -uid "783BF188-42C1-419E-F870-8CB88815A674";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -18.25177001953125;
createNode animCurveTL -n "Character1_RightFoot_translateY";
	rename -uid "979EAE38-4C36-F079-E06E-D1AE4B18F7E1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.0794563293457031;
createNode animCurveTL -n "Character1_RightFoot_translateZ";
	rename -uid "9C420DE6-4E30-B25B-A61D-2D83B7FEB4F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5524176359176636;
createNode animCurveTU -n "Character1_RightFoot_scaleX";
	rename -uid "FD744110-4DB3-F1B8-234C-6B8590B6DE0D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFoot_scaleY";
	rename -uid "F695B0F6-4958-DE8E-42EB-3DADD65AAE55";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFoot_scaleZ";
	rename -uid "287D298D-4B45-8C80-7DD3-179690F67FFE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightFoot_rotateX";
	rename -uid "3A45B83E-463D-A909-4768-47A5649D76CC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -1.9345545768737795 1 -1.840928316116333
		 2 -1.6891236305236816 3 -1.6131969690322876 4 -1.7625943422317505 5 -2.3296453952789307
		 6 -3.1367204189300537 7 -3.8360612392425542 8 -4.1945853233337402 9 -4.0423140525817871
		 10 -3.4910094738006592 11 -2.8605613708496094 12 -2.6504089832305908 13 -3.2409794330596924
		 14 -4.2495937347412109 15 -5.172548770904541 16 -5.4253854751586914 17 -5.1927952766418457
		 18 -4.6433753967285156 19 -3.9531688690185551 20 -3.3792417049407959 21 -2.9723331928253174
		 22 -2.6552667617797852 23 -2.4757678508758545 24 -2.4737892150878906 25 -2.7951369285583496
		 26 -3.308375358581543 27 -3.7146995067596431 28 -3.8520698547363281 29 -3.7201695442199703
		 30 -3.4671852588653564 31 -3.1622729301452637 32 -2.9828040599822998 33 -2.9960184097290039
		 34 -3.144498348236084 35 -3.3152620792388916 36 -3.5968403816223145 37 -4.1129121780395508
		 38 -5.0716466903686523 39 -6.2993264198303223 40 -7.3976807594299316 41 -8.068389892578125
		 42 -8.0607337951660156 43 -7.4552574157714853 44 -6.6275591850280762 45 -6.2091264724731445
		 46 -6.6463899612426758 47 -7.6250529289245605 48 -8.5200376510620117 49 -8.9119577407836914
		 50 -8.7045698165893555 51 -8.0886249542236328 52 -7.2900362014770508 53 -6.6675496101379395
		 54 -6.3031606674194336 55 -6.0829706192016602 56 -6.0795001983642578 57 -6.3568453788757324
		 58 -7.136904239654541 59 -8.2350149154663086 60 -9.1777925491333008 61 -9.7192049026489258
		 62 -9.5108423233032227 63 -8.7770147323608398 64 -8.1584672927856445 65 -7.8977417945861816
		 66 -7.6780791282653809 67 -7.6214809417724618 68 -7.855626106262207 69 -8.6151304244995117
		 70 -9.6648159027099609 71 -10.510420799255371 72 -10.83515739440918 73 -10.420716285705566
		 74 -9.4151706695556641 75 -8.2369680404663086 76 -7.6946229934692374 77 -8.0867462158203125
		 78 -8.836639404296875 79 -9.2608470916748047 80 -9.0201644897460937 81 -8.1656103134155273
		 82 -6.9379787445068359 83 -5.5410251617431641 84 -4.2223691940307617 85 -3.0263519287109375
		 86 -1.9033819437026978 87 -0.92431437969207764 88 -0.17117039859294891 89 0.18382593989372253
		 90 0.13525678217411041 91 -0.53028357028961182 92 -0.90571129322052002 93 -0.67052191495895386
		 94 0.060103740543127067 95 1.0130115747451782 96 1.6693090200424194 97 1.602726936340332
		 98 1.1425693035125732 99 0.6989445686340332 100 0.57579612731933594 101 0.83457529544830322
		 102 1.3161393404006958 103 1.8508825302124023 104 2.2006831169128418 105 2.3139004707336426
		 106 2.3296265602111816 107 2.1653265953063965 108 1.7916103601455688 109 1.0424367189407349
		 110 0.054810799658298492 111 -0.81182277202606201 112 -1.3128635883331299 113 -1.2090483903884888
		 114 -0.58897602558135986 115 0.21693259477615356 116 0.62697845697402954 117 0.31369423866271973
		 118 -0.39778217673301697 119 -1.2300255298614502 120 -1.9345545768737795;
createNode animCurveTA -n "Character1_RightFoot_rotateY";
	rename -uid "C623F731-41F2-5B3D-22C2-19B3A8E5724F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 0.77543109655380249 1 0.80025798082351685
		 2 0.81287157535552979 3 0.83648949861526489 4 0.89535349607467662 5 1.0262463092803955
		 6 1.2146404981613159 7 1.4006166458129883 8 1.5165292024612427 9 1.5052196979522705
		 10 1.4078751802444458 11 1.3099793195724487 12 1.2958540916442871 13 1.421644926071167
		 14 1.6493847370147705 15 1.9019958972930908 16 1.972965359687805 17 1.9069721698760986
		 18 1.7547225952148438 19 1.5727075338363647 20 1.4192008972167969 21 1.3021131753921509
		 22 1.2047429084777832 23 1.1276508569717407 24 1.0728050470352173 25 1.0662974119186401
		 26 1.1140342950820923 27 1.1787439584732056 28 1.2149360179901123 29 1.2072223424911499
		 30 1.1889424324035645 31 1.1613616943359375 32 1.1905725002288818 33 1.2731807231903076
		 34 1.370792031288147 35 1.4580614566802979 36 1.5712611675262451 37 1.7532122135162354
		 38 2.0710368156433105 39 2.5002338886260986 40 2.9247767925262451 41 3.1874980926513672
		 42 3.1422262191772461 43 2.8861005306243896 44 2.6154253482818604 45 2.4919426441192627
		 46 2.6016683578491211 47 2.9077179431915283 48 3.2625560760498047 49 3.4581000804901123
		 50 3.3840706348419189 51 3.1549649238586426 52 2.9010889530181885 53 2.7362456321716309
		 54 2.6619458198547363 55 2.630634069442749 56 2.6442902088165283 57 2.7086184024810791
		 58 2.881760835647583 59 3.1804561614990234 60 3.510566234588623 61 3.7386944293975835
		 62 3.67915940284729 63 3.4467127323150635 64 3.2613427639007568 65 3.1395528316497803
		 66 3.0086119174957275 67 2.9247074127197266 68 2.9429693222045898 69 3.1574180126190186
		 70 3.5378363132476807 71 3.9192817211151127 72 4.090303897857666 73 3.8801610469818115
		 74 3.4546704292297363 75 3.0764970779418945 76 2.9703023433685303 77 3.1516313552856445
		 78 3.4758224487304687 79 3.7387716770172119 80 3.7353210449218754 81 3.4370124340057373
		 82 2.994732141494751 83 2.5153017044067383 84 2.0480425357818604 85 1.5770546197891235
		 86 1.094646692276001 87 0.60293859243392944 88 0.11493752896785736 89 -0.3559531569480896
		 90 -0.75671869516372681 91 -0.85218721628189087 92 -0.91695463657379161 93 -0.82814019918441772
		 94 -0.68417370319366455 95 -0.5749322772026062 96 -0.54973095655441284 97 -0.61354058980941772
		 98 -0.72933429479598999 99 -0.85320562124252319 100 -0.93633973598480214 101 -0.94424676895141613
		 102 -0.91406935453414917 103 -0.87276434898376465 104 -0.83184462785720825 105 -0.78106886148452759
		 106 -0.73411548137664795 107 -0.67306900024414063 108 -0.61020541191101074 109 -0.56139326095581055
		 110 -0.51198136806488037 111 -0.43603995442390442 112 -0.31668016314506531 113 -0.17740193009376526
		 114 -0.05314302071928978 115 0.054019976407289505 116 0.16543811559677124 117 0.29735180735588074
		 118 0.44872662425041199 119 0.61436796188354492 120 0.77543109655380249;
createNode animCurveTA -n "Character1_RightFoot_rotateZ";
	rename -uid "D214D972-434D-D92B-5101-D6A1D55CC014";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -0.61463683843612671 1 -1.2729642391204834
		 2 -2.0069034099578857 3 -2.5722413063049316 4 -2.7269241809844971 5 -2.1095578670501709
		 6 -0.90952587127685547 7 0.24849620461463931 8 0.67189872264862061 9 -0.22251115739345551
		 10 -1.9399247169494629 11 -3.5993950366973877 12 -4.3860640525817871 13 -3.766262531280518
		 14 -2.2313165664672852 15 -0.59407007694244385 16 0.31320580840110779 17 -0.05037819966673851
		 18 -1.133620023727417 19 -2.330359935760498 20 -3.083970308303833 21 -3.3870232105255127
		 22 -3.4937641620635986 23 -3.3499388694763184 24 -2.8995087146759033 25 -1.8317492008209229
		 26 -0.26463109254837036 27 1.1925156116485596 28 1.8912749290466309 29 1.6332526206970215
		 30 0.84579533338546753 31 -0.057572256773710251 32 -1.090576171875 33 -2.0363867282867432
		 34 -2.9849123954772949 35 -4.048072338104248 36 -4.9731826782226563 37 -5.4919099807739258
		 38 -5.1654033660888672 39 -4.1458964347839355 40 -3.1199789047241211 41 -2.8947782516479492
		 42 -4.1736712455749512 43 -6.3777995109558105 44 -8.4436922073364258 45 -9.3882579803466797
		 46 -8.7804594039916992 47 -7.1751670837402353 48 -5.5020971298217773 49 -4.8180389404296875
		 50 -5.655118465423584 51 -7.3795051574707022 52 -9.2784004211425781 53 -10.678730964660645
		 54 -11.543501853942871 55 -12.149604797363281 56 -12.40369701385498 57 -12.205671310424805
		 58 -11.14222526550293 59 -9.3139619827270508 60 -7.4509210586547852 61 -6.4070901870727539
		 62 -7.1628050804138184 63 -9.0365381240844727 64 -10.555109024047852 65 -11.41257381439209
		 66 -12.157388687133789 67 -12.579591751098633 68 -12.472156524658203 69 -11.387172698974609
		 70 -9.5330352783203125 71 -7.7370409965515137 72 -6.9040255546569824 73 -7.77504587173462
		 74 -9.6749639511108398 75 -11.454494476318359 76 -12.023320198059082 77 -10.551040649414062
		 78 -7.7658953666687021 79 -4.7668881416320801 80 -2.7059845924377441 81 -2.0844354629516602
		 82 -2.2219266891479492 83 -2.4297215938568115 84 -2.1457862854003906 85 -1.4281008243560791
		 86 -0.59237754344940186 87 0.35324612259864807 88 1.3937416076660156 89 2.7064461708068848
		 90 4.1452054977416992 91 5.3086366653442383 92 5.9414958953857422 93 4.9910316467285156
		 94 3.1687784194946289 95 1.3812267780303955 96 0.4459507167339325 97 0.80993545055389404
		 98 1.9491286277770996 99 3.1819124221801758 100 3.7716488838195801 101 3.3794095516204834
		 102 2.4403340816497803 103 1.4014357328414917 104 0.68138653039932251 105 0.29361504316329956
		 106 0.094899222254753113 107 0.063849151134490967 108 0.26511961221694946 109 1.0373132228851318
		 110 2.3096468448638916 111 3.4733645915985107 112 3.825535774230957 113 2.7274181842803955
		 114 0.6852724552154541 115 -1.3836686611175537 116 -2.6311399936676025 117 -2.758955717086792
		 118 -2.2257816791534424 119 -1.3796097040176392 120 -0.61463683843612671;
createNode animCurveTU -n "Character1_RightFoot_visibility";
	rename -uid "8FCFB02B-4719-9298-5BE7-50A9DE053DDD";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightToeBase_translateX";
	rename -uid "49060235-4C0A-0F8C-AB47-4BB6353A0EBB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2767767906188965;
createNode animCurveTL -n "Character1_RightToeBase_translateY";
	rename -uid "8C3C96B6-42D0-7348-AC23-6F80D42125D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.0139193534851074;
createNode animCurveTL -n "Character1_RightToeBase_translateZ";
	rename -uid "E4130AF3-4EDB-6AE0-A1BE-7B9FF7E601C1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8259720802307129;
createNode animCurveTU -n "Character1_RightToeBase_scaleX";
	rename -uid "70554988-4089-8677-C941-07B6288B9061";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightToeBase_scaleY";
	rename -uid "E78CC179-423B-9345-6830-08B1AE94E982";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightToeBase_scaleZ";
	rename -uid "1111FB97-4DF2-3727-EE56-01A0C76B26B6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightToeBase_rotateX";
	rename -uid "7823A082-453D-5A28-2037-989820DA7DB8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6433753791698109e-009;
createNode animCurveTA -n "Character1_RightToeBase_rotateY";
	rename -uid "C5AEC5DB-4CA5-A0F8-40DB-648AEE819A7B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.5016299555817899e-010;
createNode animCurveTA -n "Character1_RightToeBase_rotateZ";
	rename -uid "4CB160F1-4E2A-C984-E192-4197B47FD31A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.157942713409966e-010;
createNode animCurveTU -n "Character1_RightToeBase_visibility";
	rename -uid "D96BE0FF-4187-323D-3A2D-6E83D8718B25";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateX";
	rename -uid "94E1F73C-42A9-E52A-195E-5FB5ED11A596";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.0018006421159952879;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateY";
	rename -uid "E543BF17-40FE-D468-89DE-928AC9BC2638";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.0064689838327467442;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateZ";
	rename -uid "BDAE2B76-45F1-68AD-F987-6899A46AC2A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.6805553436279297;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleX";
	rename -uid "8B1AF29F-4537-C4F3-9610-13905A20BB23";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleY";
	rename -uid "DF8CD4F7-4DB7-5777-C554-87BD7969F175";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleZ";
	rename -uid "D3668C3F-40CF-8F9C-262F-3C835BF6B83C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateX";
	rename -uid "04E91012-415F-446B-C994-1ABF5B7ACDAA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.5084543854918593e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateY";
	rename -uid "032EF2CA-47D4-2169-8EF7-B9845DC6F39D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.2911350028873585e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateZ";
	rename -uid "069C4A2F-474E-4D3F-81B1-0FB3ADE77DE8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.6928585269693208e-011;
createNode animCurveTU -n "Character1_RightFootMiddle1_visibility";
	rename -uid "8B6EE015-4F45-DC32-8CF1-87BA631BBF73";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateX";
	rename -uid "D2BF55A2-4C65-CF48-CE69-DCB4EA9112AB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.5860795176413376e-006;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateY";
	rename -uid "E3EAC2DE-41DF-6F9F-534B-629C72B3F0D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.3309863662652788e-007;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateZ";
	rename -uid "902E29CF-47CF-825A-31C4-BD9060F7F5CE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.0234856605529785;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleX";
	rename -uid "6BCC082D-41B2-F16E-0FEC-10A015EDFC92";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleY";
	rename -uid "050AE832-472B-83A7-8899-A695F950E6A2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleZ";
	rename -uid "336E66DC-4DE1-B8B6-F361-D6ACF3BD9C12";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateX";
	rename -uid "9294A171-40A7-8ACE-1D7B-768E5D38DFF9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.2731548016993202e-009;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateY";
	rename -uid "F6ADFD7C-4EA1-176C-AB32-109BD5E92A0B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8101667720316073e-008;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateZ";
	rename -uid "48942CFA-4935-8DAD-3983-F894B4FFC68F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.5538285081979666e-010;
createNode animCurveTU -n "Character1_RightFootMiddle2_visibility";
	rename -uid "9CCE1033-4FFE-0CC6-9CAF-9A86CF0023E6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_Spine_translateX";
	rename -uid "22CCD2A7-4FDD-1264-0B7C-EB84D8A679FD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6075620651245117;
createNode animCurveTL -n "Character1_Spine_translateY";
	rename -uid "9F5ABC20-488A-5A76-F6A3-BF9616084D55";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.353469848632812;
createNode animCurveTL -n "Character1_Spine_translateZ";
	rename -uid "FD5E21C1-4BC8-CD6F-09AA-67A25ADA770A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2061522006988525;
createNode animCurveTU -n "Character1_Spine_scaleX";
	rename -uid "9CD14603-4C25-0410-0CEE-4D987A814183";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine_scaleY";
	rename -uid "C29F77E0-499B-0091-AA7A-4FAEC8781E35";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine_scaleZ";
	rename -uid "FB6525E4-4FE0-E284-165D-87AFCD2A2287";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_Spine_rotateX";
	rename -uid "21EEEFC4-4EDF-81C1-10E3-9CA23F66A62C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -4.2090029716491699 1 -4.2508859634399414
		 2 -4.2908878326416016 3 -4.3297367095947266 4 -4.3681316375732422 5 -4.4066381454467773
		 6 -4.445411205291748 7 -4.4845776557922363 8 -4.5243287086486816 9 -4.5649223327636719
		 10 -4.6069707870483398 11 -4.6512603759765625 12 -4.6986699104309082 13 -4.7500057220458984
		 14 -4.8055014610290527 15 -4.8651719093322754 16 -4.9352631568908691 17 -4.9856610298156738
		 18 -5.0077676773071289 19 -5.0376520156860352 20 -5.1113090515136719 21 -5.219693660736084
		 22 -5.3277254104614258 23 -5.4326720237731934 24 -5.5318307876586914 25 -5.6224865913391113
		 26 -5.7018890380859375 27 -5.7671351432800293 28 -5.8150997161865234 29 -5.86419677734375
		 30 -5.9198336601257324 31 -5.9644746780395508 32 -5.9796695709228516 33 -5.9478244781494141
		 34 -5.8789396286010742 35 -5.7962222099304199 36 -5.701880931854248 37 -5.5981693267822266
		 38 -5.4871487617492676 39 -5.3705534934997559 40 -5.2498278617858887 41 -5.1264595985412598
		 42 -5.0019655227661133 43 -4.8783187866210938 44 -4.7577366828918457 45 -4.6425538063049316
		 46 -4.5169901847839355 47 -4.366175651550293 48 -4.1942543983459473 49 -4.0053586959838867
		 50 -3.7711846828460693 51 -3.4903428554534912 52 -3.208153247833252 53 -2.9712386131286621
		 54 -2.7799360752105713 55 -2.6073648929595947 56 -2.4605662822723389 57 -2.3468174934387207
		 58 -2.2739431858062744 59 -2.2492024898529053 60 -2.2788677215576172 61 -2.3595998287200928
		 62 -2.5364749431610107 63 -2.7978196144104004 64 -3.0451817512512207 65 -3.2676801681518555
		 66 -3.5167596340179443 67 -3.7885849475860596 68 -4.0794148445129395 69 -4.3861198425292969
		 70 -4.7053604125976562 71 -5.0335025787353516 72 -5.3673176765441895 73 -5.7039532661437988
		 74 -6.0414047241210937 75 -6.3781604766845703 76 -6.7574300765991211 77 -7.2121062278747559
		 78 -7.7263770103454599 79 -8.284210205078125 80 -8.8694858551025391 81 -9.4348258972167969
		 82 -9.9596548080444336 83 -10.466572761535645 84 -10.977287292480469 85 -11.469488143920898
		 86 -11.896073341369629 87 -12.241230964660645 88 -12.488993644714355 89 -12.623188018798828
		 90 -12.627747535705566 91 -12.478713989257813 92 -12.184259414672852 93 -11.79531192779541
		 94 -11.338669776916504 95 -10.79830265045166 96 -10.157447814941406 97 -9.4421072006225586
		 98 -8.7036781311035156 99 -7.961977481842041 100 -7.2364268302917489 101 -6.5145416259765625
		 102 -5.8097162246704102 103 -5.1797690391540527 104 -4.6831870079040527 105 -4.3337283134460449
		 106 -4.0834932327270508 107 -3.8962609767913818 108 -3.7650222778320312 109 -3.6827783584594722
		 110 -3.6425378322601318 111 -3.6371264457702632 112 -3.6590795516967773 113 -3.7335872650146484
		 114 -3.8604016304016118 115 -3.9943056106567383 116 -4.0898838043212891 117 -4.1468424797058105
		 118 -4.1904706954956055 119 -4.2136096954345703 120 -4.2090029716491699;
createNode animCurveTA -n "Character1_Spine_rotateY";
	rename -uid "BB58AEC3-4D7D-2F2A-6270-7FA43F56E685";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -12.116277694702148 1 -12.09735107421875
		 2 -12.079145431518555 3 -12.061172485351563 4 -12.042956352233887 5 -12.024513244628906
		 6 -12.006045341491699 7 -11.987362861633301 8 -11.968302726745605 9 -11.948709487915039
		 10 -11.928412437438965 11 -11.907275199890137 12 -11.885201454162598 13 -11.862066268920898
		 14 -11.83762264251709 15 -11.811668395996094 16 -11.782950401306152 17 -11.794613838195801
		 18 -11.856613159179688 19 -11.91627311706543 20 -11.920767784118652 21 -11.87993049621582
		 22 -11.83934211730957 23 -11.799894332885742 24 -11.762484550476074 25 -11.728288650512695
		 26 -11.69814395904541 27 -11.672524452209473 28 -11.652023315429688 29 -11.607327461242676
		 30 -11.527853965759277 31 -11.432694435119629 32 -11.343226432800293 33 -11.290454864501953
		 34 -11.266111373901367 35 -11.242806434631348 36 -11.222930908203125 37 -11.209280967712402
		 38 -11.205124855041504 39 -11.21392822265625 40 -11.238627433776855 41 -11.282188415527344
		 42 -11.347597122192383 43 -11.437984466552734 44 -11.556623458862305 45 -11.706926345825195
		 46 -11.910005569458008 47 -12.177474021911621 48 -12.498172760009766 49 -12.861002922058105
		 50 -13.299442291259766 51 -13.811186790466309 52 -14.331475257873535 53 -14.795662879943848
		 54 -15.201340675354006 55 -15.581362724304199 56 -15.924061775207518 57 -16.217700958251953
		 58 -16.450614929199219 59 -16.610883712768555 60 -16.686458587646484 61 -16.674549102783203
		 62 -16.512819290161133 63 -16.221181869506836 64 -15.936402320861816 65 -15.678954124450684
		 66 -15.383850097656248 67 -15.060068130493166 68 -14.716501235961914 69 -14.362499237060547
		 70 -14.007437705993652 71 -13.660133361816406 72 -13.329401969909668 73 -13.024011611938477
		 74 -12.752784729003906 75 -12.524544715881348 76 -12.317795753479004 77 -12.108439445495605
		 78 -11.897756576538086 79 -11.687161445617676 80 -11.478352546691895 81 -11.318737030029297
		 82 -11.220376014709473 83 -11.131433486938477 84 -11.000005722045898 85 -10.838543891906738
		 86 -10.696588516235352 87 -10.578051567077637 88 -10.486838340759277 89 -10.427008628845215
		 90 -10.402210235595703 91 -10.422382354736328 92 -10.49001407623291 93 -10.551950454711914
		 94 -10.591533660888672 95 -10.656780242919922 96 -10.795801162719727 97 -10.992185592651367
		 98 -11.193161964416504 99 -11.392395973205566 100 -11.584139823913574 101 -11.807834625244141
		 102 -12.066769599914551 103 -12.301918983459473 104 -12.455148696899414 105 -12.531375885009766
		 106 -12.580928802490234 107 -12.614103317260742 108 -12.632923126220703 109 -12.639501571655273
		 110 -12.635693550109863 111 -12.623167037963867 112 -12.603761672973633 113 -12.534817695617676
		 114 -12.408778190612793 115 -12.280556678771973 116 -12.205521583557129 117 -12.176571846008301
		 118 -12.150810241699219 119 -12.130067825317383 120 -12.116277694702148;
createNode animCurveTA -n "Character1_Spine_rotateZ";
	rename -uid "064E2C40-4868-2A1C-3612-2489C43E8D2A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 119 ".ktv[0:118]"  0 5.6667900085449219 1 5.7390222549438477
		 2 5.814511775970459 3 5.893712043762207 4 5.9768338203430176 5 6.0637168884277344
		 6 6.1520485877990723 7 6.2390260696411133 8 6.3223366737365723 9 6.4000997543334961
		 10 6.4724526405334473 11 6.540492057800293 12 6.6058382987976074 13 6.6696267127990723
		 14 6.7296566963195801 15 6.7825803756713867 16 6.832371711730957 17 6.8608951568603516
		 18 6.8671994209289551 19 6.8691849708557129 20 6.8840632438659668 21 6.9086112976074219
		 22 6.9290966987609863 23 6.9456229209899902 24 6.95855712890625 25 6.9684567451477051
		 26 6.9753971099853516 29 6.9742765426635742 30 6.9794940948486328 31 6.9978151321411133
		 32 7.0098667144775391 33 7.0071253776550293 34 6.9913253784179687 35 6.9694275856018066
		 36 6.9423723220825195 37 6.9120621681213379 38 6.8788156509399414 39 6.8397965431213379
		 40 6.7911720275878906 41 6.7294921875 42 6.6516585350036621 43 6.5575704574584961
		 44 6.4489541053771973 45 6.3286447525024414 46 6.1775269508361816 47 5.9806451797485352
		 48 5.7433609962463379 49 5.4707884788513184 50 5.1560487747192383 51 4.8072233200073242
		 52 4.453514575958252 53 4.1251506805419922 54 3.8293299674987789 55 3.5599160194396973
		 56 3.3269143104553223 57 3.1404063701629639 58 3.0110254287719727 59 2.9478836059570313
		 60 2.9582183361053467 61 3.0353784561157227 62 3.1864993572235107 63 3.4037480354309082
		 64 3.641185998916626 65 3.8918368816375737 66 4.175783634185791 67 4.4905624389648437
		 68 4.8332662582397461 69 5.2003164291381836 70 5.5845460891723633 71 5.9779791831970215
		 72 6.3735151290893555 73 6.7648015022277832 74 7.1487922668457031 75 7.5240907669067374
		 76 7.9443120956420898 77 8.4514884948730469 78 9.0258302688598633 79 9.6463851928710937
		 80 10.292238235473633 81 10.934666633605957 82 11.559792518615723 83 12.166105270385742
		 84 12.749326705932617 85 13.289802551269531 86 13.762934684753418 87 14.154322624206543
		 88 14.448450088500977 89 14.628543853759766 90 14.677539825439453 91 14.56259822845459
		 92 14.31095027923584 93 13.944121360778809 94 13.484498023986816 95 12.943637847900391
		 96 12.329536437988281 97 11.661169052124023 98 10.96506404876709 99 10.258282661437988
		 100 9.5569543838500977 101 8.8665828704833984 102 8.2043647766113281 103 7.6064772605896005
		 104 7.1115732192993164 105 6.7389011383056641 106 6.462611198425293 107 6.2412295341491699
		 108 6.0692405700683594 109 5.9416179656982422 110 5.8525028228759766 111 5.7939496040344238
		 112 5.7564306259155273 113 5.744781494140625 114 5.7588706016540527 115 5.7796030044555664
		 116 5.7867445945739746 117 5.7777137756347656 118 5.759584903717041 119 5.7251825332641602
		 120 5.6667900085449219;
createNode animCurveTU -n "Character1_Spine_visibility";
	rename -uid "BDAB4C7D-4925-199E-31EB-70948A58DF7B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_Spine1_translateX";
	rename -uid "4F5D88DB-472E-DCF8-730F-9B9203F801DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.930038452148437;
createNode animCurveTL -n "Character1_Spine1_translateY";
	rename -uid "F5D9FBFB-4768-4398-ECEE-7D939AE64AC9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1661763191223145;
createNode animCurveTL -n "Character1_Spine1_translateZ";
	rename -uid "774FFC43-4831-9C11-632C-00AE616FE81C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.40121376514434814;
createNode animCurveTU -n "Character1_Spine1_scaleX";
	rename -uid "E879AAD0-4DB3-B357-C85B-DE8D23F385DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_Spine1_scaleY";
	rename -uid "4A838D7E-4916-899C-4F77-F6804854A6AA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine1_scaleZ";
	rename -uid "0D16A53B-4987-DCEA-82B2-D8AB0141ED4F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_Spine1_rotateX";
	rename -uid "5B66F345-4A69-FB6B-BC12-439EC7FB6B88";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -26.060359954833984 1 -25.608798980712891
		 2 -25.103160858154297 3 -24.683303833007813 4 -24.490749359130859 5 -24.458221435546875
		 6 -24.426721572875977 7 -24.398059844970703 8 -24.374057769775391 9 -24.356544494628906
		 10 -24.347440719604492 11 -24.348743438720703 12 -24.362522125244141 13 -24.390769958496094
		 14 -24.435131072998047 15 -24.497159957885742 16 -24.589397430419922 17 -24.768642425537109
		 18 -25.038335800170898 19 -25.330266952514648 20 -25.578672409057617 21 -25.78718376159668
		 22 -25.998653411865234 23 -26.206424713134766 24 -26.403940200805664 25 -26.637643814086914
		 26 -26.926412582397461 27 -27.220315933227539 28 -27.456148147583008 29 -27.650171279907227
		 30 -27.831361770629883 31 -27.94865608215332 32 -27.952348709106445 33 -27.792367935180664
		 34 -27.328678131103516 35 -26.615867614746094 36 -25.899477005004883 37 -25.431846618652344
		 38 -25.169452667236328 39 -24.906295776367188 40 -24.649024963378906 41 -24.404306411743164
		 42 -24.178808212280273 43 -23.979469299316406 44 -23.813423156738281 45 -23.68794059753418
		 46 -23.598728179931641 47 -23.534183502197266 48 -23.490823745727539 49 -23.465288162231445
		 50 -23.506034851074219 51 -23.618492126464844 52 -23.737058639526367 53 -23.801280975341797
		 54 -23.819978713989258 55 -23.83854866027832 56 -23.853843688964844 57 -23.862676620483398
		 58 -23.925058364868164 59 -24.076087951660156 60 -24.26234245300293 61 -24.401544570922852
		 62 -24.519208908081055 63 -24.625551223754883 64 -24.562925338745117 65 -24.184907913208008
		 66 -23.621597290039063 67 -23.083259582519531 68 -22.790924072265625 69 -22.701238632202148
		 70 -22.624021530151367 71 -22.562414169311523 72 -22.519580841064453 73 -22.498706817626953
		 74 -22.503253936767578 75 -22.536870956420898 76 -22.605108261108398 77 -22.708829879760742
		 78 -22.84208869934082 79 -22.998945236206055 80 -23.1737060546875 81 -23.405050277709961
		 82 -23.691549301147461 83 -23.973321914672852 84 -24.201452255249023 85 -24.385444641113281
		 86 -24.560155868530273 87 -24.721889495849609 88 -24.866781234741211 89 -25.119802474975586
		 90 -25.518758773803711 91 -25.923160552978516 92 -26.177787780761719 93 -26.024469375610352
		 94 -25.664037704467773 95 -25.3201904296875 96 -25.183401107788086 97 -25.202919006347656
		 98 -25.214275360107422 99 -25.218105316162109 100 -25.215290069580078 101 -25.249774932861328
		 102 -25.332635879516602 103 -25.413753509521484 104 -25.443593978881836 105 -25.43140983581543
		 106 -25.416374206542969 107 -25.394878387451172 108 -25.368015289306641 109 -25.442104339599609
		 110 -25.636960983276367 111 -25.821220397949219 112 -25.850702285766602 113 -25.609128952026367
		 114 -25.214052200317383 115 -24.863382339477539 116 -24.734231948852539 117 -24.898597717285156
		 118 -25.246713638305664 119 -25.668983459472656 120 -26.060359954833984;
createNode animCurveTA -n "Character1_Spine1_rotateY";
	rename -uid "68D9529E-4A6E-EE20-8409-8DB35143322B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -4.7502837181091309 1 -4.6953449249267578
		 2 -4.6482839584350586 3 -4.6611394882202148 4 -4.7735638618469238 5 -4.9497418403625488
		 6 -5.1263923645019531 7 -5.2994351387023926 8 -5.4649243354797363 9 -5.6190299987792969
		 10 -5.7584099769592285 11 -5.8799753189086914 12 -5.9808197021484375 13 -6.0579013824462891
		 14 -6.1072940826416016 15 -6.1248259544372559 16 -6.101874828338623 17 -6.1568222045898437
		 18 -6.325690746307373 19 -6.4696545600891113 20 -6.4406833648681641 21 -6.2666888236999512
		 22 -6.0821409225463867 23 -5.8936076164245605 24 -5.7077136039733887 25 -5.5304088592529297
		 26 -5.361814022064209 27 -5.2055559158325195 28 -5.070061206817627 29 -4.9772558212280273
		 30 -4.9413571357727051 31 -4.9508757591247559 32 -4.970604419708252 33 -4.9688010215759277
		 34 -4.8797531127929687 35 -4.7417774200439453 36 -4.6650772094726562 37 -4.7199015617370605
		 38 -4.8597359657287598 39 -5.0034422874450684 40 -5.1464152336120605 41 -5.2841534614562988
		 42 -5.4122633934020996 43 -5.5271291732788086 44 -5.6256732940673828 45 -5.7051610946655273
		 46 -5.7705574035644531 47 -5.8289566040039062 48 -5.8800888061523438 49 -5.9237222671508789
		 50 -6.0795817375183105 51 -6.3754181861877441 52 -6.6705012321472168 53 -6.8176164627075195
		 54 -6.8395905494689941 55 -6.8599481582641602 56 -6.8794217109680176 57 -6.8987336158752441
		 58 -6.9500188827514648 59 -7.0377664566040039 60 -7.119072437286377 61 -7.1387643814086914
		 62 -7.0402688980102539 63 -6.8718376159667969 64 -6.6969623565673828 65 -6.4976134300231934
		 66 -6.2870378494262695 67 -6.132758617401123 68 -6.0682320594787598 69 -6.0599584579467773
		 70 -6.0650534629821777 71 -6.0862894058227539 72 -6.1267094612121582 73 -6.1895651817321777
		 74 -6.2789812088012695 75 -6.3994951248168945 76 -6.5692963600158691 77 -6.7987799644470215
		 78 -7.0782699584960937 79 -7.3975300788879395 80 -7.7461223602294922 81 -8.2324609756469727
		 82 -8.8755397796630859 83 -9.5246953964233398 84 -10.019847869873047 85 -10.370656967163086
		 86 -10.68975830078125 87 -10.96673583984375 88 -11.190762519836426 89 -11.374635696411133
		 90 -11.500885963439941 91 -11.52719783782959 92 -11.443553924560547 93 -11.152462005615234
		 94 -10.718607902526855 95 -10.265803337097168 96 -9.9315910339355469 97 -9.6940250396728516
		 98 -9.4404668807983398 99 -9.1766939163208008 100 -8.9079198837280273 101 -8.7638311386108398
		 102 -8.7769279479980469 103 -8.8032808303833008 104 -8.6929397583007813 105 -8.4739227294921875
		 106 -8.264826774597168 107 -8.0499362945556641 108 -7.8296265602111808 109 -7.6327376365661621
		 110 -7.4552278518676767 111 -7.2491812705993643 112 -6.9685111045837402 113 -6.5505824089050293
		 114 -6.0311594009399414 115 -5.5155777931213379 116 -5.1216144561767578 117 -4.911902904510498
		 118 -4.8243899345397949 119 -4.7938423156738281 120 -4.7502837181091309;
createNode animCurveTA -n "Character1_Spine1_rotateZ";
	rename -uid "BE460C23-4DEA-C9AF-B35C-77B42D7D9931";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 19.853261947631836 1 21.065361022949219
		 2 22.371719360351563 3 23.496463775634766 4 24.163330078125 5 24.509805679321289
		 6 24.85687255859375 7 25.203378677368164 8 25.548330307006836 9 25.890871047973633
		 10 26.230852127075195 11 26.568460464477539 12 26.904054641723633 13 27.237819671630859
		 14 27.568737030029297 15 27.895349502563477 16 28.238162994384766 17 28.419120788574219
		 18 28.394071578979492 19 28.38471794128418 20 28.609109878540039 21 29.019063949584961
		 22 29.417818069458008 23 29.79734992980957 24 30.149806976318359 25 31.122903823852543
		 26 32.821765899658203 27 34.423091888427734 28 35.099830627441406 29 34.528385162353516
		 30 33.218498229980469 31 31.584074020385742 32 30.041461944580075 33 29.030023574829098
		 34 28.98786735534668 35 29.629093170166016 36 30.305568695068363 37 30.36663818359375
		 38 29.897781372070316 39 29.428447723388672 40 28.972105026245117 41 28.542465209960938
		 42 28.153469085693359 43 27.820196151733398 44 27.558443069458008 45 27.384445190429688
		 46 27.299089431762695 47 27.285667419433594 48 27.332220077514648 49 27.426687240600586
		 50 27.364528656005859 51 27.09764289855957 52 26.848655700683594 53 26.838903427124023
		 54 27.018732070922852 55 27.184658050537109 56 27.326204299926758 57 27.432920455932617
		 58 28.161680221557617 59 29.612304687500004 60 30.936063766479496 61 31.293661117553711
		 62 29.609304428100586 63 26.778776168823242 64 24.923391342163086 65 24.878501892089844
		 66 25.608356475830078 67 26.416799545288086 68 26.608343124389648 69 26.258140563964844
		 70 25.907915115356445 71 25.562538146972656 72 25.227207183837891 73 24.907428741455078
		 74 24.609916687011719 75 24.342044830322266 76 24.084451675415039 77 23.815855026245117
		 78 23.541337966918945 79 23.265514373779297 80 22.992851257324219 81 22.534168243408203
		 82 21.858188629150391 83 21.204381942749023 84 20.809465408325195 85 20.637857437133789
		 86 20.500804901123047 87 20.402923583984375 88 20.348215103149414 89 20.886613845825195
		 90 22.176609039306641 91 23.643985748291016 92 24.716829299926758 93 24.62922477722168
		 94 23.980373382568359 95 23.344663619995117 96 23.288064956665039 97 23.716484069824219
		 98 24.151514053344727 99 24.578929901123047 100 24.983974456787109 101 25.162220001220703
		 102 25.063039779663086 103 24.901697158813477 104 24.89161491394043 105 24.97999382019043
		 106 24.98634147644043 107 24.927753448486328 108 24.811756134033203 109 25.23951530456543
		 110 26.352758407592773 111 27.468717575073242 112 27.901824951171875 113 27.388298034667969
		 114 26.336807250976563 115 25.066776275634766 116 23.892501831054687 117 22.864999771118164
		 118 21.838546752929687 119 20.829225540161133 120 19.853261947631836;
createNode animCurveTU -n "Character1_Spine1_visibility";
	rename -uid "61421DD2-4C38-14C3-23FC-04BA68607B38";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftShoulder_translateX";
	rename -uid "9B4F2D40-4B30-8CB6-D8D8-4FBFF9663E37";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.0842323303222656;
createNode animCurveTL -n "Character1_LeftShoulder_translateY";
	rename -uid "B167701A-4E12-8BF1-C3B2-56B70B5EEC26";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.604855537414551;
createNode animCurveTL -n "Character1_LeftShoulder_translateZ";
	rename -uid "477467CC-4F9A-C7A2-0A58-FA857EBFE2F9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6404602527618408;
createNode animCurveTU -n "Character1_LeftShoulder_scaleX";
	rename -uid "63F2DBE4-45D8-AE5A-5BFE-66A28B8F391F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftShoulder_scaleY";
	rename -uid "3D5ADFE3-4DC0-6E2A-14DC-48AEF7715660";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftShoulder_scaleZ";
	rename -uid "73BC86C2-47BB-F59F-5172-2696BCCEE8B1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftShoulder_rotateX";
	rename -uid "3B148C13-4A2A-6024-6372-0586FAF9020F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 5.2854747772216797 1 5.1516294479370117
		 2 4.9967584609985352 3 4.8873686790466309 4 4.8888134956359863 5 5.1119556427001953
		 6 5.4854373931884766 7 5.8062543869018555 8 5.8780755996704102 9 5.5155782699584961
		 10 4.8694467544555664 11 4.247563362121582 12 3.9677081108093262 13 3.9677081108093262
		 14 3.9677081108093262 15 3.9677081108093262 16 3.9169962406158447 17 3.6059617996215816
		 18 3.0370380878448486 19 2.4536492824554443 20 2.0895950794219971 21 2.1191079616546631
		 22 2.3374550342559814 23 2.4509541988372803 24 2.2682335376739502 25 1.9114228487014773
		 26 1.5927621126174927 27 1.3189514875411987 28 1.0943336486816406 29 0.91715049743652344
		 30 0.77737218141555786 31 0.66850268840789795 32 0.6200411319732666 33 0.66760444641113281
		 34 0.7962268590927124 35 0.96211642026901256 36 1.2304614782333374 37 1.7636628150939941
		 38 2.4359922409057617 39 3.0366897583007813 40 3.5990288257598873 41 3.951891422271729
		 42 3.9118530750274663 43 3.5541815757751465 44 3.124539852142334 45 2.9139432907104492
		 46 2.9027130603790283 47 2.871429443359375 48 2.823333740234375 49 2.7616152763366699
		 50 2.5472908020019531 51 2.1642601490020752 52 1.8019636869430544 53 1.6605640649795532
		 54 1.9041669368743896 55 2.3544952869415283 56 2.737788200378418 57 2.8466758728027344
		 58 2.7636203765869141 59 2.6905896663665771 60 2.6295104026794434 61 2.5848021507263184
		 62 2.578160285949707 63 2.5991601943969727 64 2.6013190746307373 65 2.5428900718688965
		 66 2.4522602558135986 67 2.3850686550140381 68 2.3940732479095459 69 2.552621603012085
		 70 2.7952032089233398 71 2.9759914875030518 72 2.9667160511016846 73 2.6484642028808594
		 74 2.1036038398742676 75 1.5249420404434204 76 1.1160297393798828 77 0.83295714855194092
		 78 0.49985894560813909 79 0.13716703653335571 80 -0.23542504012584686 81 -0.71277791261672974
		 82 -1.3033523559570313 83 -1.8471357822418213 84 -2.1541409492492676 85 -2.0578973293304443
		 86 -1.7528365850448608 87 -1.4728126525878906 88 -1.3142836093902588 89 -1.1403830051422119
		 90 -0.72726500034332275 91 -0.14095154404640198 92 0.50549966096878052 93 1.0677416324615479
		 94 1.4713549613952637 95 1.823933959007263 96 2.2801480293273926 97 2.8238792419433594
		 98 3.3064942359924316 99 3.7212746143341064 100 4.0656580924987793 101 4.1475367546081543
		 102 3.9242248535156246 103 3.6585750579833984 104 3.6535212993621831 105 4.192924976348877
		 106 5.0875396728515625 107 5.9333043098449707 108 6.3055319786071777 109 6.2859463691711426
		 110 6.2448415756225586 111 6.1822781562805176 112 6.0983819961547852 113 5.7908296585083008
		 114 5.2455596923828125 115 4.7197213172912598 116 4.4382166862487793 117 4.481813907623291
		 118 4.7106871604919434 119 5.0130410194396973 120 5.2854747772216797;
createNode animCurveTA -n "Character1_LeftShoulder_rotateY";
	rename -uid "F671F0AB-44AF-F379-5619-4A9E5D13E75E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -21.089040756225586 1 -20.660287857055664
		 2 -20.146926879882812 3 -19.801828384399414 4 -19.878149032592773 5 -20.825067520141602
		 6 -22.381519317626953 7 -23.734210968017578 8 -24.068586349487305 9 -22.603618621826172
		 10 -19.96101188659668 11 -17.430936813354492 12 -16.301752090454102 13 -16.301752090454102
		 14 -16.301752090454102 15 -16.301752090454102 16 -16.097616195678711 17 -14.667189598083494
		 18 -11.936795234680176 19 -9.0447921752929687 20 -7.1302523612976074 21 -7.1776986122131348
		 22 -8.441462516784668 23 -9.4556093215942383 24 -8.7345829010009766 25 -6.633507251739502
		 26 -4.5336451530456543 27 -2.477818489074707 28 -0.50860458612442017 29 1.2682857513427734
		 30 2.791485071182251 31 4.6287274360656738 32 6.9420170783996582 33 9.1527910232543945
		 34 10.815861701965332 35 11.217284202575684 36 7.4245209693908691 37 0.17387595772743225
		 38 -6.0388226509094238 39 -10.016191482543945 40 -13.406439781188965 41 -15.396659851074219
		 42 -15.212254524230959 43 -13.494531631469727 44 -11.551647186279297 45 -10.68426513671875
		 46 -10.703573226928711 47 -10.568535804748535 48 -10.29887866973877 49 -9.9143285751342773
		 50 -8.5847997665405273 51 -6.2159981727600098 52 -3.9302077293396001 53 -2.8457825183868408
		 54 -3.9218187332153325 55 -6.3834419250488281 56 -8.7384757995605469 57 -9.4818439483642578
		 58 -8.9502229690551758 59 -8.499812126159668 60 -8.1501026153564453 61 -7.8776216506957999
		 62 -7.7858638763427734 63 -7.8288445472717285 64 -7.7261133193969727 65 -7.2553739547729501
		 66 -6.5981683731079102 67 -6.0804743766784668 68 -6.0284786224365234 69 -6.904780387878418
		 70 -8.405156135559082 71 -9.7036952972412109 72 -9.9712009429931641 73 -8.4115438461303711
		 74 -5.6516714096069336 75 -2.9945368766784668 76 -1.7392978668212891 77 -1.616586446762085
		 78 -1.4930740594863892 79 -1.3774312734603882 80 -1.2775970697402954 81 -0.34125018119812012
		 82 1.5414177179336548 83 3.2485249042510986 84 3.6612341403961177 85 1.8201245069503784
		 86 -1.4442936182022095 87 -4.6355123519897461 88 -6.3056154251098633 89 -6.8787212371826172
		 90 -7.8181829452514657 91 -9.0720348358154297 92 -10.494945526123047 93 -11.359108924865723
		 94 -11.50019645690918 95 -11.722878456115723 96 -12.819083213806152 97 -14.608798027038572
		 98 -16.366178512573242 99 -18.056154251098633 100 -19.643901824951172 101 -20.251094818115234
		 102 -19.733039855957031 103 -19.15092658996582 104 -19.560991287231445 105 -21.859336853027344
		 106 -25.246002197265625 107 -28.256303787231445 108 -29.419692993164063 109 -29.081838607788086
		 110 -28.584754943847656 111 -27.954740524291992 112 -27.218114852905273 113 -25.712491989135742
		 114 -23.3292236328125 115 -20.92875862121582 116 -19.375986099243164 117 -19.053777694702148
		 118 -19.499818801879883 119 -20.312261581420898 120 -21.089040756225586;
createNode animCurveTA -n "Character1_LeftShoulder_rotateZ";
	rename -uid "7242E0E3-437D-1C72-0112-8EBB4D1FBFF0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -3.372546911239624 1 -3.1346375942230225
		 2 -2.8722271919250488 3 -2.6635439395904541 4 -2.5846447944641113 5 -2.7664775848388672
		 6 -3.1399369239807129 7 -3.4688136577606201 8 -3.4968466758728027 9 -2.9766764640808105
		 10 -2.1320996284484863 11 -1.3731335401535034 12 -1.0476516485214233 13 -1.0476516485214233
		 14 -1.0476516485214233 15 -1.0476516485214233 16 -0.92672264575958263 17 -0.36369767785072327
		 18 0.61777794361114502 19 1.6965818405151367 20 2.6052000522613525 21 3.1284353733062744
		 22 3.4358129501342773 23 3.8696248531341557 24 4.7771363258361816 25 6.0386428833007812
		 26 7.3041276931762695 27 8.551666259765625 28 9.7587566375732422 29 10.821896553039551
		 30 11.684608459472656 31 12.711506843566895 32 14.057193756103516 33 15.441255569458008
		 34 16.566879272460937 35 16.992399215698242 36 15.189856529235838 37 11.809211730957031
		 38 9.2945346832275391 39 8.0621519088745117 40 7.0590195655822754 41 6.4792056083679199
		 42 6.5348677635192871 43 7.0419516563415527 44 7.6179409027099609 45 7.908647060394288
		 46 7.9811134338378906 47 8.1194801330566406 48 8.313899040222168 49 8.5546035766601562
		 50 9.0283317565917969 51 9.7365093231201172 52 10.408501625061035 53 10.802290916442871
		 54 10.704021453857422 55 10.26638126373291 56 9.8279094696044922 57 9.7636480331420898
		 58 9.9908390045166016 59 10.164932250976562 60 10.275508880615234 61 10.35863208770752
		 62 10.244574546813965 63 9.9512100219726563 64 9.820622444152832 65 9.9916362762451172
		 66 10.278936386108398 67 10.540075302124023 68 10.634171485900879 69 10.438055038452148
		 70 10.03827953338623 71 9.6142396926879883 72 9.3512563705444336 73 9.425724983215332
		 74 9.6608705520629883 75 9.7294216156005859 76 9.246830940246582 77 8.2362680435180664
		 78 7.0072951316833496 79 5.6111812591552734 80 4.0995655059814453 81 2.6696677207946777
		 82 1.3695225715637207 83 0.057235229760408408 84 -1.3689670562744141 85 -2.9837033748626709
		 86 -4.8074665069580078 87 -6.6391558647155762 88 -8.0239334106445313 89 -8.8202323913574219
		 90 -9.0638885498046875 91 -8.9068584442138672 92 -8.6418981552124023 93 -8.1082572937011719
		 94 -7.259497642517089 95 -6.3110823631286621 96 -5.5088067054748535 97 -4.8517313003540039
		 98 -4.1690049171447754 99 -3.4757113456726074 100 -2.7884631156921387 101 -1.8927867412567139
		 102 -0.77894353866577148 103 0.21879242360591888 104 0.78267592191696167 105 0.61429917812347412
		 106 -0.11957462131977081 107 -0.96303862333297741 108 -1.3528296947479248 109 -1.3392984867095947
		 110 -1.3760985136032104 111 -1.4531644582748413 112 -1.5605162382125854 113 -1.4119458198547363
		 114 -0.97907817363739014 115 -0.60893267393112183 116 -0.60004311800003052 117 -1.0618585348129272
		 118 -1.7976440191268921 119 -2.629075288772583 120 -3.372546911239624;
createNode animCurveTU -n "Character1_LeftShoulder_visibility";
	rename -uid "ED2AB6D8-48EB-320F-C48C-9EB15C3556DA";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftArm_translateX";
	rename -uid "628E509D-46C0-74CD-CFFB-8AA0E1EE8031";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 9.2092180252075195;
createNode animCurveTL -n "Character1_LeftArm_translateY";
	rename -uid "9BD89743-48CB-2475-5AE9-0F9A8282A790";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.4172050952911377;
createNode animCurveTL -n "Character1_LeftArm_translateZ";
	rename -uid "B001B98F-4499-2518-08A2-31AE30B9CCB1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.69030004739761353;
createNode animCurveTU -n "Character1_LeftArm_scaleX";
	rename -uid "A38C6992-4389-7D1F-80B4-23A1B620A19D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleY";
	rename -uid "2A5CF6D6-4A9A-92C6-A629-40A75A278C17";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleZ";
	rename -uid "CF838496-4F1E-6B17-788A-A193CA3FF820";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_LeftArm_rotateX";
	rename -uid "0A39C93F-4789-DC37-1EA0-42A990D1425C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 59.225181579589844 1 62.213188171386719
		 2 65.315048217773438 3 68.117660522460938 4 70.280311584472656 5 71.866676330566406
		 6 73.0357666015625 7 73.633377075195313 8 73.485366821289063 9 71.92681884765625
		 10 69.236312866210937 11 66.638389587402344 12 65.339447021484375 13 64.977134704589844
		 14 64.393852233886719 15 63.567588806152344 16 62.494083404541016 17 60.026264190673828
		 18 56.345840454101563 19 53.064998626708984 20 51.1861572265625 21 51.343612670898438
		 22 52.719749450683594 23 53.836288452148438 24 52.957515716552734 25 50.568046569824219
		 26 48.266342163085938 27 46.097812652587891 28 44.105934143066406 29 41.490627288818359
		 30 38.048625946044922 31 34.284267425537109 32 31.242959976196286 33 30.184171676635742
		 34 31.89189338684082 35 36.100070953369141 36 44.888900756835938 37 55.310081481933594
		 38 60.908134460449226 39 62.56207275390625 40 63.438953399658203 41 63.788311004638679
		 42 63.589565277099602 43 62.955669403076172 44 62.252899169921882 45 61.693950653076165
		 46 61.165145874023437 47 60.459892272949219 48 59.563152313232415 49 58.462200164794922
		 50 56.652725219726563 51 54.149421691894531 52 51.658432006835938 53 49.751022338867188
		 54 48.886501312255859 55 48.772556304931641 56 48.740131378173828 57 47.919586181640625
		 58 46.513923645019531 59 45.391010284423828 60 44.602565765380859 61 44.12158203125
		 62 42.097549438476563 63 38.856834411621094 64 37.446521759033203 65 38.812694549560547
		 66 41.314754486083984 67 43.955184936523437 68 45.872596740722656 69 46.845928192138672
		 70 47.328239440917969 71 47.579933166503906 72 47.865516662597656 73 48.228073120117188
		 74 48.584373474121094 75 48.906452178955078 76 49.093463897705078 77 49.127670288085938
		 78 49.028488159179688 79 48.784603118896484 80 48.381473541259766 81 47.530994415283203
		 82 46.159671783447266 83 44.638008117675781 84 43.384246826171875 85 42.849006652832031
		 86 42.104606628417969 87 40.457637786865234 88 38.323486328125 89 37.020702362060547
		 90 38.168991088867188 91 41.05291748046875 92 44.415599822998047 93 47.46307373046875
		 94 49.874351501464844 95 52.171710968017578 96 54.828449249267578 97 57.627605438232422
		 98 60.050765991210938 99 62.090934753417969 100 63.768013000488288 101 64.683296203613281
		 102 64.906654357910156 103 65.04388427734375 104 65.568336486816406 105 66.903884887695312
		 106 68.848381042480469 107 70.849418640136719 108 72.063262939453125 109 72.485679626464844
		 110 72.853965759277344 111 73.151153564453125 112 73.357810974121094 113 72.939895629882813
		 114 71.535087585449219 115 69.52276611328125 116 67.450233459472656 117 65.5853271484375
		 118 63.684497833251946 119 61.627384185791016 120 59.225181579589844;
createNode animCurveTA -n "Character1_LeftArm_rotateY";
	rename -uid "EC5DE709-410A-C5D8-8355-1699030EE96C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -69.777976989746094 1 -69.116806030273437
		 2 -68.399909973144531 3 -67.672691345214844 4 -67.005050659179688 5 -66.406730651855469
		 6 -65.860763549804687 7 -65.417839050292969 8 -65.133102416992187 9 -65.0980224609375
		 10 -65.217597961425781 11 -65.303367614746094 12 -65.248123168945313 13 -65.073554992675781
		 14 -64.840187072753906 15 -64.537422180175781 16 -64.170814514160156 17 -63.226398468017585
		 18 -61.550090789794922 19 -59.799865722656257 20 -58.763008117675788 21 -59.170738220214837
		 22 -60.546867370605476 23 -61.875514984130859 24 -62.06121826171875 25 -61.237064361572266
		 26 -60.340129852294915 27 -59.374202728271484 28 -58.344951629638665 29 -57.329841613769531
		 30 -56.289730072021484 31 -54.616165161132813 32 -52.134880065917969 33 -49.504108428955078
		 34 -47.372047424316406 35 -46.179531097412109 36 -46.897918701171875 37 -48.203498840332031
		 38 -48.566379547119141 39 -48.187591552734375 40 -47.721019744873047 41 -47.244457244873047
		 42 -46.841346740722656 43 -46.565559387207031 44 -46.416309356689453 45 -46.378990173339844
		 46 -46.438568115234375 47 -46.575290679931641 48 -46.780361175537109 49 -47.043983459472656
		 50 -46.786891937255859 51 -45.864665985107422 52 -44.974170684814453 53 -44.893344879150391
		 54 -46.363372802734375 55 -48.919052124023438 56 -51.468341827392578 57 -52.755580902099609
		 58 -52.897998809814453 59 -52.890220642089844 60 -52.700733184814453 61 -52.265266418457031
		 62 -51.751476287841797 63 -51.044975280761719 64 -49.966510772705078 65 -48.612384796142578
		 66 -47.046623229980469 67 -45.325912475585937 68 -43.612869262695312 69 -42.067958831787109
		 70 -40.674278259277344 71 -39.393630981445313 72 -38.177558898925781 73 -36.962802886962891
		 74 -35.915454864501953 75 -35.278789520263672 76 -35.286159515380859 77 -35.875110626220703
		 78 -36.808475494384766 79 -38.011730194091797 80 -39.410625457763672 81 -40.26763916015625
		 82 -40.404506683349609 83 -40.594696044921875 84 -41.635292053222656 85 -44.251121520996094
		 86 -48.288093566894531 87 -52.812732696533203 88 -56.087188720703125 89 -57.770637512207031
		 90 -58.443534851074219 91 -58.217441558837884 92 -57.620727539062493 93 -56.159797668457031
		 94 -53.865932464599609 95 -51.441802978515625 96 -49.453449249267578 97 -47.788089752197266
		 98 -46.07763671875 99 -44.403995513916016 100 -42.846343994140625 101 -41.006229400634766
		 102 -38.929172515869141 103 -37.279033660888672 104 -36.637416839599609 105 -37.453441619873047
		 106 -39.319355010986328 107 -41.469383239746094 108 -43.167270660400391 109 -44.545055389404297
		 110 -46.179355621337891 111 -48.032779693603516 112 -50.066623687744141 113 -51.925861358642578
		 114 -53.530284881591797 115 -55.237373352050781 116 -57.420818328857429 117 -60.269023895263672
		 118 -63.49140548706054 119 -66.773323059082031 120 -69.777976989746094;
createNode animCurveTA -n "Character1_LeftArm_rotateZ";
	rename -uid "30B2DB2E-4A27-C931-5F71-FCB3081E0020";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -77.858016967773438 1 -80.288505554199219
		 2 -82.760536193847656 3 -85.068626403808594 4 -87.076850891113281 5 -89.173309326171875
		 6 -91.314071655273438 7 -92.76702880859375 8 -92.788398742675781 9 -90.207138061523438
		 10 -85.700126647949219 11 -81.297042846679688 12 -79.051094055175781 13 -78.443466186523438
		 14 -77.579277038574219 15 -76.4329833984375 16 -75.005271911621094 17 -71.807052612304688
		 18 -66.991439819335937 19 -62.647323608398445 20 -60.239326477050788 21 -60.806915283203125
		 22 -63.212436676025398 23 -65.330589294433594 24 -64.764450073242188 25 -62.139263153076165
		 26 -59.626018524169929 27 -57.258998870849609 28 -55.066120147705078 29 -52.253421783447266
		 30 -48.582931518554688 31 -44.310592651367188 32 -40.329502105712891 33 -38.003105163574219
		 34 -38.200035095214844 35 -40.969459533691406 36 -48.833065032958984 37 -60.442714691162116
		 38 -69.142379760742187 39 -74.1298828125 40 -78.320335388183594 41 -81.183792114257813
		 42 -81.951492309570313 43 -81.045112609863281 44 -79.677566528320313 45 -78.981155395507813
		 46 -78.824630737304688 47 -78.374717712402344 48 -77.668342590332031 49 -76.746177673339844
		 50 -74.705986022949219 51 -71.572608947753906 52 -68.678291320800781 53 -67.158538818359375
		 54 -67.9010009765625 55 -70.124885559082031 56 -72.343063354492188 57 -72.93389892578125
		 58 -72.251396179199219 59 -71.748611450195313 60 -71.419754028320312 61 -71.216751098632813
		 62 -69.309272766113281 63 -65.992561340332031 64 -64.317916870117187 65 -65.201858520507812
		 66 -67.083602905273438 67 -69.186897277832031 68 -70.844841003417969 69 -72.195266723632812
		 70 -73.509956359863281 71 -74.4105224609375 72 -74.512077331542969 73 -73.204666137695313
		 74 -70.861099243164062 75 -68.517868041992188 76 -67.099624633789063 77 -66.314544677734375
		 78 -65.277854919433594 79 -64.036506652832031 80 -62.636207580566399 81 -60.403800964355476
		 82 -57.305835723876946 83 -54.367019653320313 84 -52.590950012207031 85 -52.871658325195313
		 86 -54.293788909912109 87 -55.541690826416016 88 -55.902008056640625 89 -56.350898742675781
		 90 -58.518947601318359 91 -61.914344787597656 92 -65.796737670898438 93 -69.068161010742187
		 94 -71.321151733398438 95 -73.484230041503906 96 -76.510017395019531 97 -80.16729736328125
		 98 -83.558403015136719 99 -86.652122497558594 100 -89.427947998046875 101 -90.911895751953125
		 102 -90.999916076660156 103 -90.906318664550781 104 -91.762474060058594 105 -94.56768798828125
		 106 -98.569450378417969 107 -102.22177124023437 108 -103.70276641845703 109 -103.28719329833984
		 110 -102.63587188720703 111 -101.77594757080078 112 -100.73063659667969 113 -98.582221984863281
		 114 -94.966178894042969 115 -90.833213806152344 116 -87.235443115234375 117 -84.615486145019531
		 118 -82.428581237792969 119 -80.311050415039063 120 -77.858016967773438;
createNode animCurveTU -n "Character1_LeftArm_visibility";
	rename -uid "D5EF01F8-4240-D28E-E107-139E636859E5";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftForeArm_translateX";
	rename -uid "F30B0E47-459C-980C-354D-3F86AF32A18C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.0427684783935547;
createNode animCurveTL -n "Character1_LeftForeArm_translateY";
	rename -uid "2DA3EDAB-4630-9470-07A7-039285813D30";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.323208808898926;
createNode animCurveTL -n "Character1_LeftForeArm_translateZ";
	rename -uid "B296591D-4C51-2A2A-FDC9-A9B624E5D635";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 16.678167343139648;
createNode animCurveTU -n "Character1_LeftForeArm_scaleX";
	rename -uid "104C1D67-4E17-83E2-4C6F-318D72DA29FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftForeArm_scaleY";
	rename -uid "82A5F62D-45D8-B7F3-B5B3-25B8737A41A3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftForeArm_scaleZ";
	rename -uid "B66A6661-4C22-EC04-9726-8A81EEBDAF27";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftForeArm_rotateX";
	rename -uid "E638A074-43D0-D607-A2D1-7293DE38364E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 120 ".ktv[0:119]"  0 -16.628414154052734 1 -16.922506332397461
		 2 -17.297195434570313 3 -17.593208312988281 4 -17.67156982421875 5 -17.255962371826172
		 6 -16.519950866699219 7 -15.985273361206056 8 -16.138347625732422 9 -17.439788818359375
		 10 -19.446495056152344 11 -21.286657333374023 12 -22.149625778198242 13 -22.237831115722656
		 14 -22.248226165771484 15 -22.148551940917969 16 -21.875295639038086 17 -21.148645401000977
		 18 -19.97050666809082 19 -18.674365997314453 20 -17.578359603881836 21 -16.891855239868164
		 22 -16.43792724609375 23 -15.975243568420412 24 -15.280778884887694 25 -14.413260459899902
		 26 -13.585160255432129 27 -12.818268775939941 28 -12.13438892364502 29 -11.516050338745117
		 30 -10.959463119506836 31 -10.661338806152344 32 -10.819210052490234 33 -11.365728378295898
		 34 -12.124550819396973 35 -12.945898056030273 36 -14.537525177001955 37 -16.736709594726562
		 38 -17.825536727905273 39 -17.997596740722656 40 -18.65553092956543 41 -19.967473983764648
		 42 -21.641994476318359 43 -22.981100082397461 44 -23.612886428833008 45 -23.784173965454102
		 46 -23.88671875 47 -23.94310188293457 48 -23.879812240600586 49 -23.596586227416992
		 50 -22.818109512329102 51 -21.632268905639648 52 -20.381082534790039 53 -19.251157760620117
		 54 -18.068538665771484 55 -16.732809066772461 56 -15.552021980285645 57 -14.991922378540039
		 58 -14.989122390747072 59 -15.131755828857422 60 -15.43763542175293 61 -15.95018196105957
		 62 -18.376846313476563 63 -22.216667175292969 64 -24.847429275512695 65 -25.678176879882812
		 66 -25.868473052978516 67 -25.982559204101563 68 -26.55305290222168 69 -27.665695190429688
		 70 -28.849742889404297 71 -29.891839981079098 72 -30.725637435913089 73 -31.447071075439453
		 74 -32.104240417480469 75 -32.599880218505859 76 -32.839572906494141 77 -32.894199371337891
		 78 -32.848884582519531 79 -32.584148406982422 80 -31.984806060791016 81 -30.786197662353516
		 82 -29.161327362060547 83 -27.526357650756836 84 -26.120378494262695 85 -25.005155563354492
		 86 -23.244373321533203 87 -20.259845733642578 88 -16.618776321411133 89 -13.410388946533203
		 90 -11.828685760498047 91 -11.57542896270752 92 -11.574233055114746 93 -12.118538856506348
		 94 -13.115678787231445 95 -14.078774452209473 96 -14.720757484436035 97 -15.19552421569824
		 98 -15.728777885437012 99 -16.277618408203125 100 -16.798002243041992 101 -16.993480682373047
		 102 -16.789737701416016 103 -16.484159469604492 104 -16.400358200073242 105 -16.749811172485352
		 106 -17.340984344482422 107 -17.9173583984375 108 -18.176206588745117 109 -18.176206588745117
		 111 -18.176206588745117 112 -18.176206588745117 113 -18.218795776367188 114 -18.287618637084961
		 115 -18.300430297851563 116 -18.187215805053711 117 -17.904699325561523 118 -17.502235412597656
		 119 -17.052886962890625 120 -16.628414154052734;
createNode animCurveTA -n "Character1_LeftForeArm_rotateY";
	rename -uid "3C3E1632-4524-076E-9F56-61A84C21C8DF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 120 ".ktv[0:119]"  0 -4.6888589859008789 1 -2.3425664901733398
		 2 0.32090601325035095 3 2.55483078956604 4 3.5627236366271973 5 2.564551830291748
		 6 0.19686700403690338 7 -2.2978169918060303 8 -3.8254961967468262 9 -4.1851587295532227
		 10 -3.8232276439666752 11 -2.814828634262085 12 -1.5321191549301147 13 0.5274193286895752
		 14 3.3541903495788574 15 5.6947298049926758 16 6.3304562568664551 17 4.0611324310302734
		 18 -0.1423620879650116 19 -4.2166557312011719 20 -6.0986909866333008 21 -4.3228459358215332
		 22 -0.29399728775024414 23 3.7329833507537837 24 5.4890379905700684 25 5.3719077110290527
		 26 5.276461124420166 27 5.2022538185119629 28 5.1475400924682617 29 4.487816333770752
		 30 3.0779879093170166 31 1.8546983003616335 32 1.4081177711486816 33 1.8669592142105103
		 34 3.6597650051116943 35 6.2493152618408203 36 7.5525231361389169 37 7.1722779273986816
		 38 6.3991966247558594 39 6.4380459785461426 40 7.157649040222168 41 9.3346796035766602
		 42 12.956060409545898 43 17.12353515625 44 21.153743743896484 45 24.179981231689453
		 46 27.1256103515625 47 30.684598922729492 48 33.638843536376953 49 34.768932342529297
		 50 32.860233306884766 51 28.894815444946289 52 24.950923919677734 53 23.113773345947266
		 54 24.881322860717773 55 28.841493606567379 56 32.738491058349609 57 34.350135803222656
		 58 34.090858459472656 59 33.861255645751953 60 33.682731628417969 61 33.534698486328125
		 62 31.515325546264648 63 27.716562271118164 64 25.384363174438477 65 26.179452896118164
		 66 28.476760864257813 67 30.773399353027347 68 31.438009262084964 69 29.359245300292972
		 70 25.540889739990234 71 21.492990493774414 72 18.755426406860352 73 17.905801773071289
		 74 18.149213790893555 75 18.878921508789063 76 19.213708877563477 77 19.676158905029297
		 78 20.666572570800781 79 21.067106246948242 80 19.755741119384766 81 15.595357894897461
		 82 9.6046228408813477 83 3.9190015792846675 84 0.72017043828964233 85 1.664879322052002
		 86 5.8748712539672852 87 11.178431510925293 88 14.755562782287598 89 16.354156494140625
		 90 16.865589141845703 91 16.317298889160156 92 15.517388343811035 93 13.418759346008301
		 94 9.847294807434082 95 6.1548113822937012 96 3.6813454627990723 97 3.5228500366210937
		 98 4.8338656425476074 99 6.0450387001037598 100 5.5824060440063477 101 2.1943097114562988
		 102 -2.927696704864502 103 -7.6919951438903809 104 -10.000859260559082 105 -8.3334226608276367
		 106 -4.1877646446228027 107 0.018898965790867805 108 1.9472029209136963 109 1.9472029209136963
		 111 1.9472029209136963 112 1.9472029209136963 113 0.89855307340621948 114 -1.4722893238067627
		 115 -4.0145726203918457 116 -5.6359376907348633 117 -5.9954862594604492 118 -5.7035284042358398
		 119 -5.1424746513366699 120 -4.6888589859008789;
createNode animCurveTA -n "Character1_LeftForeArm_rotateZ";
	rename -uid "FE9A9735-49FA-EB01-761B-75890B6D3C98";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 120 ".ktv[0:119]"  0 25.404184341430664 1 27.50950813293457
		 2 29.89914512634277 3 31.847400665283203 4 32.660511016845703 5 31.40546989440918
		 6 28.624898910522461 7 25.91862678527832 8 25.017221450805664 9 27.228767395019531
		 10 31.335157394409183 11 35.288414001464844 12 37.094272613525391 13 37.059555053710937
		 14 36.820110321044922 15 36.460479736328125 16 36.037185668945313 17 35.530738830566406
		 18 34.870765686035156 19 34.141937255859375 20 33.670173645019531 21 34.232799530029297
		 22 35.621524810791016 23 36.78814697265625 24 36.65380859375 25 35.506610870361328
		 26 34.412689208984375 27 33.400470733642578 28 32.498390197753906 29 30.392221450805664
		 30 26.517612457275391 31 22.322269439697266 32 19.162200927734375 33 18.041681289672852
		 34 19.828981399536133 35 23.509357452392578 36 28.2222900390625 37 32.212909698486328
		 38 32.743610382080078 39 30.517377853393551 40 28.130239486694336 41 27.257112503051758
		 42 29.209602355957031 43 32.937313079833984 44 36.540866851806641 45 37.985939025878906
		 46 37.469047546386719 47 36.736553192138672 48 35.972446441650391 49 35.364601135253906
		 50 34.967815399169922 51 34.645793914794922 52 34.421600341796875 53 34.575302124023438
		 54 35.899276733398437 55 38.302352905273438 56 40.695301055908203 57 41.675239562988281
		 58 41.504585266113281 59 41.480567932128906 60 41.628673553466797 61 41.980819702148438
		 62 38.663063049316406 63 32.37384033203125 64 29.015319824218754 65 30.269964218139652
		 66 33.396602630615234 67 36.765552520751953 68 38.715934753417969 69 38.218215942382812
		 70 36.489906311035156 71 35.108692169189453 72 35.455169677734375 73 38.608360290527344
		 74 43.398326873779297 75 47.777622222900391 76 49.839115142822266 77 49.848747253417969
		 78 49.488227844238281 79 49.073554992675781 80 48.896026611328125 81 49.081497192382813
		 82 49.249664306640625 83 49.081024169921875 84 48.567626953125 85 48.273712158203125
		 86 46.831188201904297 87 43.496109008789063 88 39.068344116210938 89 34.840724945068359
		 90 32.776424407958984 91 32.261760711669922 92 32.019393920898437 93 30.556985855102539
		 94 27.644847869873047 95 25.04295539855957 96 24.337095260620117 97 25.208175659179688
		 98 26.211759567260742 99 27.208498001098633 100 28.119329452514648 101 28.861803054809574
		 102 29.493228912353516 103 30.198041915893555 104 31.257242202758789 105 33.254974365234375
		 106 35.889591217041016 107 38.182834625244141 108 39.151390075683594 109 39.151390075683594
		 111 39.151390075683594 112 39.151390075683594 113 38.174900054931641 114 35.859725952148438
		 115 33.119163513183594 116 30.856145858764648 117 29.286247253417969 118 27.939964294433594
		 119 26.691623687744141 120 25.404184341430664;
createNode animCurveTU -n "Character1_LeftForeArm_visibility";
	rename -uid "645144AC-4B5C-7C7B-606D-B8B4DCFFF6F2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHand_translateX";
	rename -uid "A8C929A6-4F7B-E537-E6B5-5F933EFAD7B7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.29200950264930725;
createNode animCurveTL -n "Character1_LeftHand_translateY";
	rename -uid "48EB163D-4A3A-46B2-AC7E-769FCE2B7C0B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -14.295157432556152;
createNode animCurveTL -n "Character1_LeftHand_translateZ";
	rename -uid "E3CD9BBF-48BD-9768-D3AA-F2BDB8D17A22";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6319241523742676;
createNode animCurveTU -n "Character1_LeftHand_scaleX";
	rename -uid "49FCC544-404E-1032-D4F1-7E82FCCBF5FB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHand_scaleY";
	rename -uid "27180AE7-4083-F19A-C4D1-92A1809B9E21";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHand_scaleZ";
	rename -uid "CC66AD20-49FD-907B-6BBA-F08D14EE8A7C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHand_rotateX";
	rename -uid "07946AB3-4A29-AF68-56E2-0E8F227B702A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 119 ".ktv[0:118]"  0 -7.0680442149750888e-005 1 0.075452148914337158
		 2 0.0821223184466362 3 0.16916520893573761 4 0.51487630605697632 5 1.505278468132019
		 6 2.9575881958007812 7 4.1596498489379883 8 4.3839926719665527 9 3.0386612415313721
		 10 0.79911661148071289 11 -1.2746127843856812 12 -2.4174914360046387 13 -2.4795706272125244
		 14 -1.9690297842025755 15 -1.2180577516555786 16 -0.54637372493743896 17 0.12504011392593384
		 18 0.95683687925338745 19 1.6978312730789185 20 2.0603747367858887 21 1.7142175436019897
		 22 0.78305214643478394 23 -0.2503456175327301 24 -0.74682283401489258 25 -0.74682313203811646
		 27 -0.74682515859603882 28 -0.74682551622390747 29 -0.4789948463439942 30 0.038211967796087265
		 31 0.34891232848167419 32 0.36915606260299683 33 0.30416318774223328 34 0.21843937039375305
		 35 0.1247844398021698 36 0.50750887393951416 37 1.7312815189361572 38 4.0682816505432129
		 39 7.2858953475952157 40 10.76589298248291 41 13.095536231994629 42 12.773689270019531
		 43 10.383998870849609 44 7.4201455116271973 45 5.2313694953918457 46 4.2486624717712402
		 47 3.8754487037658687 48 3.8286643028259273 49 3.8097026348114014 50 4.1977453231811523
		 51 5.1010165214538574 52 5.7157344818115234 53 5.218653678894043 54 2.6907672882080078
		 55 -1.2381604909896851 56 -4.9403362274169922 57 -6.6942977905273438 58 -6.9253835678100586
		 59 -7.1576581001281738 60 -7.4041795730590811 61 -7.7204275131225586 62 -4.948768138885498
		 63 -0.28804430365562439 64 2.1486670970916748 65 2.0415027141571045 66 0.88728249073028564
		 67 -0.48981216549873352 68 -0.9273032546043396 69 0.75055223703384399 70 3.8320355415344243
		 71 6.7001328468322754 72 7.5917758941650391 73 5.4212331771850586 74 1.6964855194091797
		 75 -1.745625376701355 76 -3.6723656654357906 77 -4.0103411674499512 78 -3.5407173633575439
		 79 -2.6242630481719971 80 -1.688936710357666 81 -0.74050253629684448 82 0.18692766129970551
		 83 0.74251377582550049 84 0.77066147327423096 85 0.20203270018100739 86 -0.60890358686447144
		 87 -1.4654508829116821 88 -1.9936151504516599 89 -2.08036208152771 90 -2.1164252758026123
		 91 -2.0762217044830322 92 -2.0219440460205078 93 -1.8323785066604616 94 -1.7292803525924683
		 95 -2.0013675689697266 96 -2.333669900894165 97 -2.2701456546783447 98 -1.9135648012161255
		 99 -1.4311378002166748 100 -0.94523590803146362 101 -0.31425496935844421 102 0.48977500200271612
		 103 1.19844651222229 104 1.5249956846237183 105 1.1408940553665161 106 0.16821713745594025
		 107 -0.90238189697265625 108 -1.4148670434951782 109 -1.4148672819137573 111 -1.4148693084716797
		 112 -1.4148696660995483 113 -1.5084803104400635 114 -1.8627293109893797 115 -2.4736320972442627
		 116 -2.8914351463317871 117 -2.6302902698516846 118 -1.8825373649597168 119 -0.9069637656211853
		 120 -7.0681307988706976e-005;
createNode animCurveTA -n "Character1_LeftHand_rotateY";
	rename -uid "CEBEBFCA-472C-909C-76C3-E6A87DA3DCFE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 119 ".ktv[0:118]"  0 2.6529707319866702e-009 1 1.1068836450576782
		 2 2.2715506553649902 3 3.3454349040985107 4 4.1647858619689941 5 4.852726936340332
		 6 5.3894619941711426 7 5.4592485427856445 8 4.8306117057800293 9 2.8613753318786621
		 10 -0.24713613092899325 11 -3.3229789733886719 12 -4.9413185119628906 13 -4.2752633094787598
		 14 -2.1419413089752197 15 0.50905019044876099 16 2.7174818515777588 17 4.297762393951416
		 18 5.7060141563415527 19 6.9798378944396973 20 8.1895780563354492 21 9.5486297607421875
		 22 10.97545337677002 23 12.094520568847656 24 12.543742179870605 25 12.543742179870605
		 27 12.543744087219238 28 12.543744087219238 29 11.254157066345215 30 8.1517829895019531
		 31 4.9084587097167969 32 2.9790129661560059 33 3.0861682891845703 34 5.4938340187072754
		 35 9.2454261779785156 36 13.784893035888672 37 18.382905960083008 38 22.483676910400391
		 39 26.565593719482422 40 30.516111373901367 41 33.907176971435547 42 35.990802764892578
		 43 36.616115570068359 44 36.474170684814453 45 36.525531768798828 46 37.885944366455078
		 47 40.331214904785156 48 42.948535919189453 49 44.828533172607422 50 45.821224212646484
		 51 46.440032958984375 52 46.809005737304688 53 47.076854705810547 54 47.482711791992188
		 55 47.847599029541016 56 47.810462951660156 57 47.235622406005859 58 46.457569122314453
		 59 45.871944427490234 60 45.526145935058594 61 45.473983764648438 62 42.709732055664063
		 63 37.277568817138672 64 34.067752838134766 65 34.777477264404297 66 37.047286987304688
		 67 39.779674530029297 68 41.907787322998047 69 43.38555908203125 70 44.517528533935547
		 71 44.891204833984375 72 44.283767700195313 73 42.055980682373047 74 38.274551391601563
		 75 34.002193450927734 76 30.203413009643558 77 27.492832183837891 78 25.357271194458008
		 79 23.152318954467773 80 20.220603942871094 81 16.674673080444336 82 13.247599601745605
		 83 10.242710113525391 84 7.9709539413452148 85 6.8501548767089844 86 8.0594472885131836
		 87 11.453289985656738 88 15.325995445251465 89 18.50117301940918 90 19.993803024291992
		 91 20.062910079956055 92 19.932565689086914 93 17.070646286010742 94 10.929244041442871
		 95 4.4851126670837402 96 0.53839260339736938 97 0.086235523223876953 98 1.5589761734008789
		 99 3.7588834762573247 100 5.4502444267272949 101 6.4135737419128418 102 7.2978148460388175
		 103 8.1637639999389648 104 9.1034088134765625 105 10.349909782409668 106 11.748491287231445
		 107 12.839907646179199 108 13.276498794555664 109 13.276498794555664 111 13.276500701904297
		 112 13.276500701904297 113 10.794031143188477 114 5.227811336517334 115 -0.63018429279327393
		 116 -4.1624875068664551 117 -4.5637845993041992 118 -3.3438210487365723 119 -1.4981869459152222
		 120 1.4137788717505373e-009;
createNode animCurveTA -n "Character1_LeftHand_rotateZ";
	rename -uid "514DF6B3-4ED7-D029-CFAD-4191E2057D09";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 119 ".ktv[0:118]"  0 -0.0001194584765471518 1 -0.88433665037155151
		 2 -1.8912743330001833 3 -2.5840339660644531 4 -2.5277564525604248 5 -1.1469722986221313
		 6 1.1745432615280151 7 3.4076805114746094 8 4.5066747665405273 9 3.944338321685791
		 10 2.4479873180389404 11 0.75799649953842163 12 -0.55770421028137207 13 -1.5233209133148193
		 14 -2.4127740859985352 15 -3.1044845581054687 16 -3.5176599025726318 17 -3.3549528121948242
		 18 -2.7424688339233398 19 -2.2812864780426025 20 -2.5818169116973877 21 -4.3578715324401855
		 22 -7.1184072494506836 23 -9.6646909713745117 24 -10.784210205078125 25 -10.784210205078125
		 27 -10.784209251403809 28 -10.784209251403809 29 -9.6625833511352539 30 -6.9199647903442383
		 31 -4.1267004013061523 32 -2.502699613571167 33 -2.5309998989105225 34 -4.5203533172607422
		 35 -7.3265051841735831 36 -9.3477163314819336 37 -9.7428550720214844 38 -8.5005083084106445
		 39 -6.504763126373291 40 -4.4022951126098633 41 -3.430994987487793 42 -4.5840659141540527
		 43 -7.2085461616516122 44 -10.19700813293457 45 -12.54999828338623 46 -14.167795181274414
		 47 -15.55904006958008 48 -16.720888137817383 49 -17.647016525268555 50 -17.892353057861328
		 51 -17.534393310546875 52 -17.417856216430664 53 -18.41630744934082 54 -21.555768966674805
		 55 -26.145004272460938 56 -30.384695053100582 57 -32.420917510986328 58 -32.763259887695312
		 59 -33.113525390625 60 -33.485904693603516 61 -33.960044860839844 62 -30.121452331542965
		 63 -22.912397384643555 64 -18.660694122314453 65 -18.948104858398437 66 -21.075038909912109
		 67 -23.597318649291992 68 -24.846963882446289 69 -23.685325622558594 70 -21.042768478393555
		 71 -18.335664749145508 72 -17.113521575927734 73 -18.111095428466797 74 -20.065088272094727
		 75 -21.789077758789063 76 -22.420806884765625 77 -22.008321762084961 78 -21.024040222167969
		 79 -19.436067581176758 80 -17.286352157592773 81 -14.433429718017578 82 -11.181462287902832
		 83 -8.288578987121582 84 -6.5084261894226074 85 -6.7118706703186035 86 -9.0538969039916992
		 87 -12.438345909118652 88 -14.976052284240723 89 -16.281633377075195 90 -16.840402603149414
		 91 -16.764604568481445 92 -16.600990295410156 93 -14.910016059875488 94 -11.316508293151855
		 95 -7.3681688308715811 96 -4.7319884300231934 97 -4.2484068870544434 98 -4.9365091323852539
		 99 -5.9172759056091309 100 -6.3699631690979004 101 -5.8982110023498535 102 -5.0226626396179199
		 103 -4.3603782653808594 104 -4.5368094444274902 105 -6.2820677757263184 106 -9.0646743774414062
		 107 -11.630594253540039 108 -12.758606910705566 109 -12.758606910705566 111 -12.75860595703125
		 112 -12.75860595703125 113 -11.493575096130371 114 -8.4780874252319336 115 -4.9066524505615234
		 116 -2.2134068012237549 117 -1.025876522064209 118 -0.56425350904464722 119 -0.36549603939056396
		 120 -0.00011945827282033861;
createNode animCurveTU -n "Character1_LeftHand_visibility";
	rename -uid "B10DF98D-4286-0A17-1033-008359C76090";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateX";
	rename -uid "2BA9F34E-4FD8-C7F5-3DA8-C48F986A07EE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1364259719848633;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateY";
	rename -uid "743DD7D6-41A7-F7AC-8F05-9499726E6596";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4241994619369507;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateZ";
	rename -uid "B1DFD497-4095-96CC-4561-6785B4C4613E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.7211661338806152;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleX";
	rename -uid "E1D881F1-4D50-1344-C639-A99E4BBA67A2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleY";
	rename -uid "7980BCEC-449A-2EF5-59D0-D99C74FD9CB1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleZ";
	rename -uid "4B67E263-4F29-7511-E675-1488549E66DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateX";
	rename -uid "E55603F0-41E4-BE7A-87A1-1C887A40940A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -28.371952056884766 1 -28.702636718750004
		 2 -29.031478881835941 3 -29.342346191406246 4 -29.620529174804688 5 -29.986867904663082
		 6 -30.401269912719727 7 -30.630796432495117 8 -30.456029891967773 9 -29.521295547485355
		 10 -27.906753540039062 11 -26.170333862304688 12 -25.112600326538086 13 -25.20367431640625
		 14 -25.993515014648438 15 -27.003162384033203 16 -27.8079833984375 17 -28.434759140014648
		 18 -29.058656692504883 19 -29.550798416137695 20 -29.799394607543945 21 -29.753774642944339
		 22 -29.489337921142582 23 -29.084440231323239 24 -28.621927261352539 25 -27.985811233520508
		 26 -27.150711059570313 27 -26.375560760498047 28 -25.961071014404297 29 -26.033761978149414
		 30 -26.39765739440918 31 -26.882612228393555 32 -27.328039169311523 33 -27.797225952148437
		 34 -28.377801895141602 35 -29.0699462890625 36 -29.897317886352539 37 -30.929998397827145
		 38 -32.212570190429688 39 -33.605564117431641 40 -34.922969818115234 41 -35.936267852783203
		 42 -36.256767272949219 43 -35.920978546142578 44 -35.436496734619141 45 -35.576248168945313
		 46 -36.995616912841797 47 -39.149223327636719 48 -41.192806243896484 49 -42.496696472167969
		 50 -43.03985595703125 51 -43.195957183837891 52 -43.031932830810547 53 -42.599132537841797
		 54 -41.58966064453125 55 -39.997756958007813 56 -38.411712646484375 57 -37.569137573242188
		 58 -37.969684600830078 59 -39.130989074707031 60 -40.402927398681641 61 -41.229061126708984
		 62 -41.339290618896484 63 -41.084278106689453 64 -40.900516510009766 65 -40.939716339111328
		 66 -41.032054901123047 67 -41.141815185546875 68 -41.233814239501953 69 -41.307594299316406
		 70 -41.378997802734375 71 -41.437816619873047 72 -41.473930358886719 73 -41.461715698242187
		 74 -41.414249420166016 75 -41.379638671875 76 -41.247791290283203 77 -40.929981231689453
		 78 -40.424331665039063 79 -39.698570251464844 80 -38.734771728515625 81 -37.644527435302734
		 82 -36.47998046875 83 -35.173072814941406 84 -33.686176300048828 85 -31.945127487182617
		 86 -30.126106262207031 87 -28.541053771972656 88 -27.462699890136719 89 -26.924440383911133
		 90 -26.803407669067383 91 -26.989301681518555 92 -27.351799011230469 93 -28.080902099609375
		 94 -29.065078735351559 95 -29.900680541992188 96 -30.269096374511719 97 -29.986906051635742
		 98 -29.253116607666012 99 -28.373466491699219 100 -27.725955963134766 101 -27.408504486083984
		 102 -27.240978240966797 103 -27.231689453125 104 -27.384822845458984 105 -27.818784713745117
		 106 -28.471456527709961 107 -29.092376708984375 108 -29.466270446777347 109 -29.539688110351559
		 110 -29.439247131347653 111 -29.235149383544918 112 -28.998193740844727 113 -28.668535232543945
		 114 -28.225082397460938 115 -27.810028076171875 116 -27.579755783081055 117 -27.621257781982422
		 118 -27.840049743652344 119 -28.125541687011719 120 -28.371952056884766;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateY";
	rename -uid "81FC1A0D-435B-F89B-3FD9-6B85D9866C21";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 26.454395294189453 1 26.129133224487305
		 2 25.793222427368164 3 25.463550567626953 4 25.157943725585937 5 24.739204406738281
		 6 24.241439819335938 7 23.953866958618164 8 24.173629760742188 9 25.268146514892578
		 10 26.892034530639648 11 28.345073699951172 12 29.110315322875977 13 29.047601699829098
		 14 28.478796005249023 15 27.681324005126953 16 26.982101440429688 17 26.393550872802734
		 18 25.764881134033203 19 25.23552131652832 20 24.955883026123047 21 25.007844924926758
		 22 25.303354263305664 23 25.737911224365234 24 26.209648132324219 25 26.819238662719727
		 26 27.557584762573242 27 28.186792373657227 28 28.503072738647461 29 28.448572158813477
		 30 28.169549942016602 31 27.781000137329102 32 27.406293869018555 33 27.559919357299805
		 34 28.611394882202148 35 30.353759765624996 36 32.554927825927734 37 34.965461730957031
		 38 37.377315521240234 39 39.714473724365234 40 41.920673370361328 41 43.957061767578125
		 42 45.866794586181641 43 47.416603088378906 44 48.15496826171875 45 48.082534790039063
		 46 47.308357238769531 47 45.98651123046875 48 44.540702819824219 49 43.503170013427734
		 50 43.040904998779297 51 42.904544830322266 52 43.047782897949219 53 43.417407989501953
		 54 44.23516845703125 55 45.410842895507813 56 46.460678100585937 57 46.974575042724609
		 58 46.733860015869141 59 45.998519897460938 60 45.123912811279297 61 44.513149261474609
		 62 44.428928375244141 63 44.622764587402344 64 44.760292053222656 65 44.731105804443359
		 66 44.662029266357422 67 44.579334259033203 68 44.509525299072266 69 44.453212738037109
		 70 44.398429870605469 71 44.353092193603516 72 44.325160980224609 73 44.334613800048828
		 74 44.37127685546875 75 44.397937774658203 76 44.139450073242187 77 43.369365692138672
		 78 42.185550689697266 79 40.712955474853516 80 39.072147369384766 81 37.27630615234375
		 82 35.386531829833984 83 33.576789855957031 84 32.000545501708984 85 30.837036132812504
		 86 30.007705688476562 87 29.321413040161136 88 28.657754898071289 89 28.100309371948242
		 90 27.845809936523438 91 27.692848205566406 92 27.38580322265625 93 26.7308349609375
		 94 25.758171081542969 95 24.839450836181641 96 24.403116226196289 97 24.739158630371094
		 98 25.559427261352539 99 26.452932357788086 100 27.056167602539063 101 27.336694717407227
		 102 27.480928421020508 103 27.488851547241211 104 27.357240676879883 105 26.972299575805664
		 106 26.357799530029297 107 25.729593276977539 108 25.328681945800781 109 25.247823715209961
		 110 25.358264923095703 111 25.578607559204102 112 25.827810287475586 113 26.163244247436523
		 114 26.595012664794922 115 26.980247497558594 116 27.186553955078125 117 27.149751663208008
		 118 26.952966690063477 119 26.689016342163086 120 26.454395294189453;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateZ";
	rename -uid "52D45632-4F68-5A13-8E84-E3B47FAE9FC4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -6.3497772216796875 1 -7.3396215438842773
		 2 -8.3411054611206055 3 -9.3047761917114258 4 -10.182120323181152 5 -11.360874176025391
		 6 -12.729382514953613 7 -13.504921913146973 8 -12.913219451904297 9 -9.8674640655517578
		 10 -4.9842691421508789 11 -0.12270411103963853 12 2.6901795864105225 13 2.4517357349395752
		 14 0.35446673631668091 15 -2.4121296405792236 16 -4.6981544494628906 17 -6.5365076065063477
		 18 -8.4246826171875 19 -9.9608163833618164 20 -10.754196166992188 21 -10.607678413391113
		 22 -9.7665328979492187 23 -8.5040855407714844 24 -7.0964903831481934 25 -5.2142124176025391
		 26 -2.8254754543304443 27 -0.68029874563217163 28 0.44170892238616943 29 0.24611574411392209
		 30 -0.74057894945144653 31 -2.0762219429016113 32 -3.3255503177642822 33 -4.4175376892089844
		 34 -5.3929553031921387 35 -6.3924875259399414 36 -7.6162614822387695 37 -9.2833051681518555
		 38 -11.456606864929199 39 -13.811034202575684 40 -15.998006820678709 41 -17.65202522277832
		 42 -18.191005706787109 43 -17.731773376464844 44 -17.0313720703125 45 -17.243429183959961
		 46 -19.417667388916016 47 -22.797451019287109 48 -26.1143798828125 49 -28.299367904663086
		 50 -29.227991104125977 51 -29.497047424316406 52 -29.214361190795898 53 -28.473623275756836
		 54 -26.773189544677734 55 -24.160259246826172 56 -21.627969741821289 57 -20.307561874389648
		 58 -20.933263778686523 59 -22.76837158203125 60 -24.818010330200195 61 -26.174350738525391
		 62 -26.356952667236328 63 -25.935113906860352 64 -25.632421493530273 65 -25.696905136108398
		 66 -25.848981857299805 67 -26.030109405517578 68 -26.182220458984375 69 -26.304409027099609
		 70 -26.422828674316406 71 -26.520502090454102 72 -26.580533981323242 73 -26.560224533081055
		 74 -26.481355667114258 75 -26.42388916015625 76 -26.243501663208008 77 -25.828298568725586
		 78 -25.174066543579102 79 -24.219528198242188 80 -22.911026000976562 81 -21.426790237426758
		 82 -19.842533111572266 83 -17.967283248901367 84 -15.619217872619629 85 -12.484630584716797
		 86 -8.8679485321044922 87 -5.5191574096679687 88 -3.1944959163665771 89 -2.0690615177154541
		 90 -1.8563857078552248 91 -2.3734235763549805 92 -3.3928415775299072 93 -5.4919214248657227
		 94 -8.4444475173950195 95 -11.081049919128418 96 -12.288610458374023 97 -11.360998153686523
		 98 -9.0264081954956055 99 -6.354273796081543 100 -4.4615044593811035 101 -3.5536956787109375
		 102 -3.0795760154724121 103 -3.0533804893493652 104 -3.4864709377288818 105 -4.729377269744873
		 106 -6.6458797454833984 107 -8.5285444259643555 108 -9.6937999725341797 109 -9.9256362915039062
		 110 -9.6087236404418945 111 -8.9705343246459961 112 -8.238917350769043 113 -7.2367658615112305
		 114 -5.915377140045166 115 -4.7040615081787109 116 -4.0418620109558105 117 -4.1607108116149902
		 118 -4.7908987998962402 119 -5.6227126121520996 120 -6.3497772216796875;
createNode animCurveTU -n "Character1_LeftHandThumb1_visibility";
	rename -uid "2C6E7464-4165-828A-8257-56890AD27DB0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateX";
	rename -uid "EB578530-44C5-8E54-C986-F8A650E1591E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.8329257965087891;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateY";
	rename -uid "C46A33FC-4FBF-A84D-5B23-3881E249CCF1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.094426639378070831;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateZ";
	rename -uid "AF70F994-4A4C-C026-D918-54A12DD0CABC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5438374280929565;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleX";
	rename -uid "51319589-4EDC-5AC3-4E73-0D97A81D2802";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleY";
	rename -uid "220BA60E-44EB-4285-33FE-8882874FF595";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleZ";
	rename -uid "B6E7A3FA-4A16-B81C-2921-298B27A756C8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateX";
	rename -uid "E8ACA0AF-47FD-BA9B-5D51-9A985726CD4E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 10.355228424072266 1 10.696617126464844
		 2 11.04798412322998 3 11.392062187194824 4 11.710687637329102 5 12.147290229797363
		 6 12.667214393615723 7 12.968461990356445 8 12.738178253173828 9 11.595808029174805
		 10 9.8933200836181641 11 8.3233661651611328 12 7.4597401618957511 13 7.5318555831909171
		 14 8.1747913360595703 15 9.0492477416992187 16 9.7978010177612305 17 10.419196128845215
		 18 11.077588081359863 19 11.629818916320801 20 11.921328544616699 21 11.867157936096191
		 22 11.559103012084961 23 11.105754852294922 24 10.61223316192627 25 9.9703969955444336
		 26 9.1827478408813477 27 8.4981279373168945 28 8.1477212905883789 29 8.2084493637084961
		 30 8.5170974731445313 31 8.9413299560546875 32 9.3453226089477539 33 9.5872783660888672
		 34 9.5867061614990234 35 9.4354629516601562 36 9.2471809387207031 37 9.1336765289306641
		 38 9.1480875015258789 39 9.2228507995605469 40 9.2872905731201172 41 9.2703037261962891
		 42 9.0478343963623047 43 8.6956148147583008 44 8.437291145324707 45 8.4912271499633789
		 46 9.0574512481689453 47 9.9908914566040039 48 10.981344223022461 49 11.681331634521484
		 50 11.991745948791504 51 12.083221435546875 52 11.987130165100098 53 11.738965034484863
		 54 11.188118934631348 55 10.388110160827637 56 9.6599569320678711 57 9.2965812683105469
		 58 9.4674625396728516 59 9.9825544357299805 60 10.584558486938477 61 11.000019073486328
		 62 11.057064056396484 63 10.925689697265625 64 10.83229923248291 65 10.852133750915527
		 66 10.899041175842285 67 10.95514965057373 68 11.002471923828125 69 11.040621757507324
		 70 11.077710151672363 71 11.10838794708252 72 11.127281188964844 73 11.12088680267334
		 74 11.096083641052246 75 11.078042984008789 76 11.091886520385742 77 11.160853385925293
		 78 11.266345024108887 79 11.37047290802002 80 11.434810638427734 81 11.497090339660645
		 82 11.561725616455078 83 11.539522171020508 84 11.342148780822754 85 10.841087341308594
		 86 10.132461547851562 87 9.4536142349243164 88 9.0228004455566406 89 8.8666257858276367
		 90 8.8709754943847656 91 9.0367870330810547 92 9.3672895431518555 93 10.063859939575195
		 94 11.084595680236816 95 12.042733192443848 96 12.498167037963867 97 12.147336959838867
		 98 11.292049407958984 99 10.356766700744629 100 9.7191162109375 101 9.4198875427246094
		 102 9.2652091979980469 103 9.2566938400268555 104 9.3978900909423828 105 9.8082046508789062
		 106 10.456756591796875 107 11.114439010620117 108 11.532696723937988 109 11.616994857788086
		 110 11.501853942871094 111 11.272036552429199 112 11.011847496032715 113 10.66087532043457
		 114 10.20717716217041 115 9.7997684478759766 116 9.5802860260009766 117 9.6195144653320312
		 118 9.8287172317504883 119 10.108020782470703 120 10.355228424072266;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateY";
	rename -uid "AFA5D431-4487-64F4-0790-2AB46D5BBFAE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 20.610536575317383 1 21.506881713867188
		 2 22.417585372924805 3 23.297515869140625 4 24.101678848266602 5 25.186664581298828
		 6 26.452770233154297 7 27.173309326171875 8 26.623371124267578 9 23.812934875488281
		 10 19.380186080932617 11 15.057770729064941 12 12.597503662109375 13 12.804925918579102
		 14 14.638347625732424 15 17.082077026367188 16 19.123300552368164 17 20.779340744018555
		 18 22.49376106262207 19 23.8985595703125 20 24.627597808837891 21 24.49278450012207
		 22 23.720394134521484 23 22.566154479980469 24 21.286367416381836 25 19.586868286132813
		 26 17.449676513671875 27 15.548969268798828 28 14.561754226684569 29 14.733510017395018
		 30 15.602140426635742 31 16.783821105957031 32 17.895280838012695 33 18.554206848144531
		 34 18.552656173706055 35 18.141365051269531 36 17.626552581787109 37 17.314729690551758
		 38 17.354379653930664 39 17.559806823730469 40 17.736482620239258 41 17.689945220947266
		 42 17.078176498413086 43 16.101160049438477 44 15.37824535369873 45 15.529617309570313
		 46 17.104707717895508 47 19.641733169555664 48 22.245796203613281 49 24.028022766113281
		 50 24.802389144897461 51 25.028684616088867 52 24.790948867797852 53 24.17254638671875
		 54 22.77739143371582 55 20.697357177734375 56 18.751125335693359 57 17.761922836303711
		 58 18.228553771972656 59 19.619419097900391 60 21.213899612426758 61 22.293983459472656
		 62 22.440959930419922 63 22.101991653442383 64 21.859991073608398 65 21.911460876464844
		 66 22.033027648925781 67 22.178152084350586 68 22.300310134887695 69 22.398628234863281
		 70 22.494073867797852 71 22.572916030883789 72 22.621427536010742 73 22.605012893676758
		 74 22.541305541992188 75 22.494930267333984 76 22.530521392822266 77 22.707540512084961
		 78 22.977390289306641 79 23.242654800415039 80 23.406007766723633 81 23.56373405456543
		 82 23.727008819580078 83 23.670969009399414 84 23.17060661315918 85 21.882802963256836
		 86 20.019657135009766 87 18.190830230712891 88 17.009071350097656 89 16.57679557800293
		 90 16.588861465454102 91 17.047687530517578 92 17.955316543579102 93 19.836753845214844
		 94 22.511777877807617 95 24.928628921508789 96 26.044227600097656 97 25.186779022216797
		 98 23.042972564697266 99 20.614599227905273 100 18.911062240600586 101 18.098894119262695
		 102 17.675981521606445 103 17.652641296386719 104 18.03887939453125 105 19.151317596435547
		 106 20.878274917602539 107 22.588459014892578 108 23.653730392456055 109 23.866287231445313
		 110 23.575780868530273 111 22.991918563842773 112 22.324483871459961 113 21.413564682006836
		 114 20.218355178833008 115 19.128599166870117 116 18.535236358642578 117 18.641607284545898
		 118 19.206535339355469 119 19.954544067382812 120 20.610536575317383;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateZ";
	rename -uid "0D9D1AE2-471A-0966-4253-F8B9A41E8D55";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -8.5256719589233398 1 -8.4085025787353516
		 2 -8.282684326171875 3 -8.1544370651245117 4 -8.0313138961791992 5 -7.8559341430664062
		 6 -7.6372804641723624 7 -7.5058398246765146 8 -7.6066274642944336 9 -8.0761842727661133
		 10 -8.6760797500610352 11 -9.1146993637084961 12 -9.3061180114746094 13 -9.291529655456543
		 14 -9.1502084732055664 15 -8.9261226654052734 16 -8.7059993743896484 17 -8.5041017532348633
		 18 -8.2718448638916016 19 -8.0629558563232422 20 -7.9476532936096191 21 -7.9693388938903809
		 22 -8.0904073715209961 23 -8.2614974975585938 24 -8.4379329681396484 25 -8.6516399383544922
		 26 -8.8887500762939453 27 -9.0715780258178711 28 -9.1565628051757812 29 -9.1422567367553711
		 30 -9.0668106079101562 31 -8.9557285308837891 32 -8.8421249389648438 33 -8.7704906463623047
		 34 -8.7706632614135742 35 -8.8157501220703125 36 -8.8704166412353516 37 -8.902583122253418
		 38 -8.8985319137573242 39 -8.8773622512817383 40 -8.8589076995849609 41 -8.8637914657592773
		 42 -8.926513671875 43 -9.0211019515991211 44 -9.0867547988891602 45 -9.0733089447021484
		 46 -8.9238500595092773 47 -8.6450958251953125 48 -8.3069496154785156 49 -8.0428314208984375
		 50 -7.9192891120910645 51 -7.8821463584899893 52 -7.9211540222167969 53 -8.0201864242553711
		 54 -8.231048583984375 55 -8.5146064758300781 56 -8.7484531402587891 57 -8.856231689453125
		 58 -8.8062982559204102 59 -8.6477603912353516 60 -8.4475183486938477 61 -8.3001680374145508
		 62 -8.2793636322021484 63 -8.3270702362060547 64 -8.360539436340332 65 -8.3534622192382812
		 66 -8.3366584777832031 67 -8.3164358139038086 68 -8.2992763519287109 69 -8.285374641418457
		 70 -8.2718000411987305 71 -8.2605276107788086 72 -8.2535667419433594 73 -8.2559242248535156
		 74 -8.2650537490844727 75 -8.2716779708862305 76 -8.2665958404541016 77 -8.2411594390869141
		 78 -8.2018671035766602 79 -8.1626291275024414 80 -8.1381597518920898 81 -8.1143112182617187
		 82 -8.0893926620483398 83 -8.0979719161987305 84 -8.1733465194702148 85 -8.3574056625366211
		 86 -8.5993871688842773 87 -8.810394287109375 88 -8.9334287643432617 89 -8.9759035110473633
		 90 -8.974736213684082 91 -8.9295692443847656 92 -8.8357324600219727 93 -8.621647834777832
		 94 -8.2692737579345703 95 -7.898627281188964 96 -7.7095236778259277 97 -7.855914592742919
		 98 -8.1922225952148437 99 -8.5251550674438477 100 -8.7303380966186523 101 -8.8203344345092773
		 102 -8.8652524948120117 103 -8.8676939010620117 104 -8.8267889022827148 105 -8.7027606964111328
		 106 -8.4913539886474609 107 -8.2582998275756836 108 -8.1006059646606445 109 -8.0679492950439453
		 110 -8.1124811172485352 111 -8.1997337341308594 112 -8.2958660125732422 113 -8.4210052490234375
		 114 -8.5749063491821289 115 -8.7053871154785156 116 -8.7725982666015625 117 -8.7607450485229492
		 118 -8.6963605880737305 119 -8.6073417663574219 120 -8.5256719589233398;
createNode animCurveTU -n "Character1_LeftHandThumb2_visibility";
	rename -uid "28B87F13-4230-377D-45D7-AC88D8D02F25";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateX";
	rename -uid "AA5F6E34-4D14-49F6-5CEF-3F8AD8D2C90F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7747817039489746;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateY";
	rename -uid "F2A909AF-42AD-A286-C9B7-998B9ADF72E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3182404041290283;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateZ";
	rename -uid "378D3F47-4236-EE57-AC88-51AD70BC9BC1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.26324462890625;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleX";
	rename -uid "90974BA9-475A-0584-7D4C-DCBD050E2C7B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleY";
	rename -uid "09BC4C52-42B4-8F30-1DCF-A285EF576B1B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleZ";
	rename -uid "1CFF2390-48CA-D8EA-7982-3F9097E5DBF3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateX";
	rename -uid "8FBD5BC7-4871-E053-C998-A1B8529964B3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.0658578604488866e-007;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateY";
	rename -uid "D401A51B-47EF-17D7-9613-49AEE2C50C31";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.5785892995081667e-008;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateZ";
	rename -uid "1CF485AE-461C-B5FB-5B49-A9BB106721EE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.0537909506401775e-007;
createNode animCurveTU -n "Character1_LeftHandThumb3_visibility";
	rename -uid "2E8D4ADA-4E30-28ED-4EF7-568973FD2C57";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateX";
	rename -uid "0188C95C-4CAF-E7D5-EEBA-5DA73E45C713";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.854037284851074;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateY";
	rename -uid "015AF825-46F8-6294-187B-5D805446B730";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2616198062896729;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateZ";
	rename -uid "133F1A18-4C9C-5612-032D-6BB247C2FB47";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.944699764251709;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleX";
	rename -uid "942286E7-4D7F-C806-F052-43A12C468F29";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleY";
	rename -uid "E266DE9C-4B3D-3A99-AFB7-C383AACB1E65";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleZ";
	rename -uid "09DBF337-4634-BB66-F3FF-F0AC858E8F23";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateX";
	rename -uid "D8C9DDB9-4970-3388-FB38-92B7BCFE31E6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 2.1236224174499512 1 2.4104506969451904
		 2 2.7102711200714111 3 3.0080804824829102 4 3.2872986793518066 5 3.6748137474060059
		 6 4.1429004669189453 7 4.4170279502868652 8 4.2072949409484863 9 3.186262845993042
		 10 1.7431187629699707 11 0.52493995428085327 12 -0.087871193885803223 13 -0.03842468187212944
		 14 0.41638731956481934 15 1.0727108716964722 16 1.6655850410461426 17 2.1770164966583252
		 18 2.7357351779937744 19 3.2161331176757813 20 3.4735803604125977 21 3.4255506992340088
		 22 3.1540665626525879 23 2.7599914073944092 24 2.3391296863555908 25 1.8059775829315186
		 26 1.1764551401138306 27 0.65421676635742188 28 0.39674606919288635 29 0.44086894392967224
		 30 0.66835039854049683 31 0.98950767517089833 32 1.303989052772522 33 0.93867117166519154
		 34 -0.23873874545097351 35 -1.5217310190200806 36 -3.2289333343505859 37 -4.9731836318969727
		 38 -5.2941551208496094 39 -3.3584244251251221 40 0.88405287265777588 41 7.6976213455200204
		 42 16.048551559448242 43 23.270315170288086 44 26.009635925292969 45 26.194847106933594
		 46 28.145317077636719 47 31.369600296020508 48 34.769569396972656 49 37.138572692871094
		 50 38.176300048828125 51 38.480377197265625 52 38.160938262939453 53 37.331905364990234
		 54 35.473056793212891 55 32.737930297851562 56 30.226882934570316 57 28.971218109130859
		 58 29.561708450317383 59 31.340835571289059 60 33.412590026855469 61 34.833217620849609
		 62 35.027503967285156 63 34.579761505126953 64 34.260848999023437 65 34.328620910644531
		 66 34.488815307617188 67 34.680255889892578 68 34.841575622558594 69 34.971523284912109
		 70 35.097770690917969 71 35.202129364013672 72 35.266368865966797 73 35.24462890625
		 74 35.1602783203125 75 35.098903656005859 76 32.373264312744141 77 25.699935913085938
		 78 17.566585540771484 79 10.127117156982422 80 4.4416046142578125 81 0.69934260845184326
		 82 -1.4379051923751831 83 -2.4507260322570801 84 -0.61241650581359863 85 2.5331799983978271
		 86 2.0491869449615479 87 1.2822352647781372 88 1.0057040452957153 89 1.2298330068588257
		 90 1.4403668642044067 91 1.4706078767776489 92 1.511935830116272 93 1.7144098281860352
		 94 2.145869255065918 95 2.6431403160095215 96 2.9048967361450195 97 2.660400390625
		 98 2.0844943523406982 99 1.4779210090637207 100 1.1105154752731323 101 1.0053750276565552
		 102 1.0129513740539551 103 1.1201539039611816 104 1.3136665821075439 105 1.6740094423294067
		 106 2.208444356918335 107 2.7674756050109863 108 3.1309294700622559 109 3.2048661708831787
		 110 3.1039333343505859 111 2.9037423133850098 112 2.679229736328125 113 2.38020920753479
		 114 2.0006835460662842 115 1.6671780347824097 116 1.4905655384063721 117 1.5219694375991821
		 118 1.6906353235244751 119 1.9188563823699951 120 2.1236224174499512;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateY";
	rename -uid "F642DA1C-40CA-A5C5-95C8-BB9ED874FF51";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 6.7255492210388184 1 7.3314361572265634
		 2 7.9438138008117667 3 8.5323257446289062 4 9.0673608779907227 5 9.7848596572875977
		 6 10.615564346313477 7 11.085054397583008 8 10.726943016052246 9 8.8755626678466797
		 10 5.8889503479003906 11 2.9077317714691162 12 1.1842409372329712 13 1.3302320241928101
		 14 2.6151995658874512 15 4.3117465972900391 16 5.7135734558105469 17 6.8398885726928711
		 18 7.9948849678039551 19 8.9324760437011719 20 9.4157867431640625 21 9.3265848159790039
		 22 8.8140182495117188 23 8.043400764465332 24 7.1826672554016104 25 6.029879093170166
		 26 4.5652613639831543 27 3.249626636505127 28 2.5617208480834961 29 2.6816201210021973
		 30 3.2865910530090332 31 4.1057243347167969 32 4.8719544410705566 33 4.6234545707702637
		 34 3.3169317245483398 35 2.3426282405853271 36 1.485115647315979 37 1.3860491514205933
		 38 4.7796182632446289 39 12.604892730712891 40 22.622329711914063 41 32.439735412597656
		 42 39.872684478759766 43 44.06573486328125 44 45.319313049316406 45 45.364578247070313
		 46 45.806747436523438 47 46.405136108398437 48 46.867431640625 49 47.092067718505859
		 50 47.165946960449219 51 47.184799194335938 52 47.164962768554688 53 47.106952667236328
		 54 46.942340850830078 55 46.611438751220703 56 46.211395263671875 57 45.975429534912109
		 58 46.089443206787109 59 46.400501251220703 60 46.703044891357422 61 46.874496459960938
		 62 46.895709991455078 63 46.846019744873047 64 46.808891296386719 65 46.816902160644531
		 66 46.835578918457031 67 46.857418060302734 68 46.875419616699219 69 46.889652252197266
		 70 46.903247833251953 71 46.914318084716797 72 46.921058654785156 73 46.918781280517578
		 74 46.909896850585938 75 46.903369903564453 76 46.118011474609375 77 43.454479217529297
		 78 38.355537414550781 79 30.835824966430664 80 21.656974792480469 81 12.118470191955566
		 82 3.552642822265625 83 -2.9762444496154785 84 1.3323402404785156 85 7.5846118927001962
		 86 5.6964049339294434 87 2.2776226997375488 88 -1.3285739421844482 89 -4.0228991508483887
		 90 -5.0253171920776367 91 -4.5973124504089355 92 -3.6540458202362065 93 -1.8656836748123169
		 94 0.60884791612625122 95 3.0148415565490723 96 4.6182198524475098 97 4.94012451171875
		 98 4.3977575302124023 99 3.6247189044952393 100 3.2842738628387451 101 3.4707539081573486
		 102 3.8104658126831055 103 4.2812023162841797 104 4.8599786758422852 105 5.7327127456665039
		 106 6.9068512916564941 107 8.0583438873291016 108 8.7696619033813477 109 8.9110298156738281
		 110 8.7177715301513672 111 8.3282966613769531 112 7.881361961364747 113 7.2685041427612314
		 114 6.4594907760620117 115 5.7171940803527832 116 5.3112106323242188 117 5.3840823173522949
		 118 5.7704248428344727 119 6.2801952362060547 120 6.7255492210388184;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateZ";
	rename -uid "DFBEE86D-4CC9-A11C-B87F-73B0D813859B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 1.4951497316360474 1 2.2061383724212646
		 2 2.9332258701324463 3 3.6404416561126713 4 4.2909622192382812 5 5.1752667427062988
		 6 6.2172417640686035 7 6.815269947052002 8 6.3584995269775391 9 4.0569143295288086
		 10 0.52635854482650757 11 -2.8184959888458252 12 -4.6872925758361816 13 -4.5305891036987305
		 14 -3.1387124061584473 15 -1.2626705169677734 16 0.32508444786071777 17 1.6287044286727905
		 18 2.9942634105682373 19 4.1262640953063965 20 4.7186393737792969 21 4.6088376045227051
		 22 3.9820156097412109 23 3.0523042678833008 24 2.0308065414428711 25 0.68854385614395142
		 26 -0.97820013761520386 27 -2.4425766468048096 28 -3.197113037109375 29 -3.066119909286499
		 30 -2.4018235206604004 31 -1.4930222034454346 32 -0.63252198696136475 33 -1.3194969892501831
		 34 -3.7318522930145268 35 -5.9005537033081055 36 -7.897207736968995 37 -9.224125862121582
		 38 -7.1168932914733887 39 -0.46677207946777349 40 9.4594688415527344 41 21.897548675537109
		 42 35.072849273681641 43 45.700057983398438 44 49.706153869628906 45 49.919265747070313
		 46 52.159702301025391 47 55.849899291992188 48 59.727218627929687 49 62.422786712646491
		 50 63.602436065673828 51 63.947994232177727 52 63.584976196289063 53 62.642604827880866
		 54 60.528110504150391 55 57.41180419921875 56 54.543758392333984 57 53.106418609619141
		 58 53.782630920410156 59 55.817039489746094 60 58.181144714355469 61 59.799697875976555
		 62 60.020915985107422 63 59.511062622070313 64 59.147804260253906 65 59.225009918212898
		 66 59.407478332519531 67 59.625514984130859 68 59.809215545654304 69 59.957180023193359
		 70 60.100914001464837 71 60.219718933105469 72 60.2928466796875 73 60.268096923828132
		 74 60.172077178955085 75 60.102203369140618 76 56.253875732421875 77 46.616195678710938
		 78 34.247066497802734 79 21.780214309692383 80 10.651504516601563 81 1.4562674760818481
		 82 -5.646763801574707 83 -10.53783130645752 84 -4.4646453857421875 85 2.5056788921356201
		 86 -1.5650113821029663 87 -7.9621248245239258 88 -14.468325614929197 89 -19.375165939331055
		 90 -21.264518737792969 91 -20.660331726074219 92 -19.25956916809082 93 -16.722846984863281
		 94 -13.250182151794434 95 -9.7202043533325195 96 -7.0320754051208496 97 -5.8052630424499512
		 98 -5.5483660697937012 99 -5.5210862159729004 100 -5.0110893249511719 101 -3.9760422706604004
		 102 -2.8817627429962158 103 -1.7937960624694824 104 -0.77649450302124023 105 0.34702017903327942
		 106 1.7070527076721191 107 3.0701930522918701 108 3.9280955791473393 109 4.100121021270752
		 110 3.8650805950164799 111 3.3942923545837402 112 2.8586723804473877 113 2.1319088935852051
		 114 1.1854652166366577 115 0.32923367619514465 116 -0.13441306352615356 117 -0.051425881683826447
		 118 0.39026385545730591 119 0.97761368751525879 120 1.4951497316360474;
createNode animCurveTU -n "Character1_LeftHandIndex1_visibility";
	rename -uid "E5BFD1E0-49DC-B3EA-7623-749E96056F30";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateX";
	rename -uid "929A35F9-4BEE-2BCC-98C0-6980348C7DCA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.1721835136413574;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateY";
	rename -uid "947C5051-4AE2-28D0-E08B-AF841B209768";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.9652466773986816;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateZ";
	rename -uid "8126D3F9-4B7B-D26B-6446-A2A488B65F3C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.8871781826019287;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleX";
	rename -uid "71773DD9-46C0-DC69-E005-3FBF339CEB6D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleY";
	rename -uid "3A789558-4104-D128-2433-888D3190C3B1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleZ";
	rename -uid "1C628248-46C2-7BB7-01B3-8BAB5C7C868C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateX";
	rename -uid "F364A465-4588-80EB-FF56-74A887FB6A8C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 7.2087359428405762 1 7.4099459648132324
		 2 7.6094198226928711 3 7.7973628044128418 4 7.9649724960327148 5 8.1847743988037109
		 6 8.4319686889648438 7 8.5681285858154297 8 8.4645042419433594 9 7.9052486419677743
		 10 6.9247374534606934 11 5.8567948341369629 12 5.2013416290283203 13 5.257906436920166
		 14 5.7474579811096191 15 6.3703994750976563 16 6.8643093109130859 17 7.2469968795776367
		 18 7.6258764266967765 19 7.9230132102966309 20 8.0724296569824219 21 8.0450477600097656
		 22 7.8859987258911142 23 7.6414837837219238 24 7.3608927726745605 25 6.9730734825134277
		 26 6.4611387252807617 27 5.9835729598999023 28 5.727384090423584 29 5.7723522186279297
		 30 5.9972143173217773 31 6.2962040901184082 32 6.5700817108154297 33 7.057990550994873
		 34 7.9044704437255859 35 8.9336137771606445 36 9.9474859237670898 37 10.723069190979004
		 38 11.02104663848877 39 10.670552253723145 40 9.6836214065551758 41 8.2895870208740234
		 42 6.9566607475280762 43 6.0971369743347168 44 5.8830327987670898 45 5.8346152305603027
		 46 5.3161411285400391 47 4.4243912696838379 48 3.4358153343200684 49 2.7162325382232666
		 50 2.392672061920166 51 2.2968697547912598 52 2.3975005149841309 53 2.6563458442687988
		 54 3.2248430252075195 55 4.0326213836669922 56 4.7454304695129395 57 5.0918617248535156
		 58 4.9297699928283691 59 4.4325408935546875 60 3.836463451385498 61 3.4168198108673096
		 62 3.3587234020233154 63 3.4923522472381592 64 3.5869803428649902 65 3.5669090747833252
		 66 3.5193850994110107 67 3.4624385833740234 68 3.4143238067626953 69 3.3754804134368896
		 70 3.3376696109771729 71 3.3063602447509766 72 3.2870616912841797 73 3.2935948371887207
		 74 3.3189218044281006 75 3.3373301029205322 76 3.6045503616333008 77 4.3051700592041016
		 78 5.310943603515625 79 6.476712703704834 80 7.6413459777832031 81 8.6070117950439453
		 82 9.2638664245605469 83 9.6058416366577148 84 9.6350116729736328 85 7.2701220512390146
		 86 5.2649879455566406 87 3.3845582008361816 88 1.9330028295516968 89 1.0366355180740356
		 90 0.73438334465026855 91 0.87450462579727173 92 1.2271965742111206 93 1.8638743162155151
		 94 2.729994535446167 95 3.6290638446807861 96 4.3829236030578613 97 4.8546652793884277
		 98 5.0830497741699219 99 5.185206413269043 100 5.3525400161743164 101 5.640561580657959
		 102 5.9393930435180664 103 6.2414288520812988 104 6.5340156555175781 105 6.8709187507629395
		 106 7.2693419456481934 107 7.6462860107421875 108 7.8720993995666504 109 7.9163231849670419
		 110 7.855811595916748 111 7.7326269149780265 112 7.5892581939697266 113 7.3892240524291992
		 114 7.1191873550415039 115 6.8655600547790527 116 6.7245149612426758 117 6.7499518394470215
		 118 6.8839321136474609 119 7.0584344863891602 120 7.2087359428405762;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateY";
	rename -uid "E4F5C526-44D6-5A06-F82E-BD8C471BBD56";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -8.152409553527832 1 -8.5030345916748047
		 2 -8.8624734878540039 3 -9.2127742767333984 4 -9.5354557037353516 5 -9.9746026992797852
		 6 -10.49244213104248 7 -10.789682388305664 8 -10.562654495239258 9 -9.419316291809082
		 10 -7.6763100624084482 11 -6.0531697273254395 12 -5.1654210090637207 13 -5.2392139434814453
		 14 -5.8999214172363281 15 -6.8035316467285156 16 -7.5776724815368652 17 -8.2181997299194336
		 18 -8.8926830291748047 19 -9.4537248611450195 20 -9.7477836608886719 21 -9.6932592391967773
		 22 -9.3821592330932617 23 -8.9214134216308594 24 -8.4164848327636719 25 -7.7558646202087402
		 26 -6.9416666030883789 27 -6.2336215972900391 28 -5.8720192909240723 29 -5.9346237182617187
		 30 -6.2532186508178711 31 -6.6918745040893555 32 -7.1098723411560059 33 -7.8970685005187979
		 34 -9.417811393737793 35 -11.636131286621094 36 -14.528291702270508 37 -18.039892196655273
		 38 -21.998098373413086 39 -26.027669906616211 40 -29.734758377075195 41 -32.775562286376953
		 42 -34.846508026123047 43 -35.939765930175781 44 -36.189888000488281 45 -36.245349884033203
		 46 -36.815589904785156 47 -37.706340789794922 48 -38.582313537597656 49 -39.157783508300781
		 50 -39.401409149169922 51 -39.471851348876953 52 -39.397838592529297 53 -39.20355224609375
		 54 -38.756072998046875 55 -38.066295623779297 56 -37.397686004638672 57 -37.049705505371094
		 58 -37.214523315429687 59 -37.698661804199219 60 -38.240043640136719 61 -38.598136901855469
		 62 -38.646308898925781 63 -38.535007476806641 64 -38.455108642578125 65 -38.472129821777344
		 66 -38.512271881103516 67 -38.560073852539063 68 -38.600212097167969 69 -38.632450103759766
		 70 -38.663684844970703 71 -38.689445495605469 72 -38.705276489257813 73 -38.699920654296875
		 74 -38.679122924804688 75 -38.663967132568359 76 -38.529468536376953 77 -38.139179229736328
		 78 -37.443473815917969 79 -36.370273590087891 80 -34.862567901611328 81 -32.973762512207031
		 82 -30.766510009765625 83 -28.231836318969727 84 -21.298837661743164 85 -14.768177032470701
		 86 -12.825132369995117 87 -11.834352493286133 88 -11.573712348937988 89 -11.659379005432129
		 90 -11.610245704650879 91 -11.408857345581055 92 -11.300021171569824 93 -11.464759826660156
		 94 -11.896560668945313 95 -12.299341201782227 96 -12.314351081848145 97 -11.66310977935791
		 98 -10.553033828735352 99 -9.3390216827392578 100 -8.3944864273071289 101 -7.7982101440429696
		 102 -7.3883342742919922 103 -7.1850051879882821 104 -7.2070512771606445 105 -7.5884175300598145
		 106 -8.2568111419677734 107 -8.9302692413330078 108 -9.3554143905639648 109 -9.4407539367675781
		 110 -9.3241596221923828 111 -9.0907840728759766 112 -8.8255825042724609 113 -8.4663867950439453
		 114 -7.9999947547912589 115 -7.5797052383422852 116 -7.3528838157653809 117 -7.3934392929077157
		 118 -7.6096034049987793 119 -7.8978123664855957 120 -8.152409553527832;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateZ";
	rename -uid "BD2BE74E-4FCE-74A4-3657-B5B54B496CC7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 22.938976287841797 1 23.771453857421875
		 2 24.617425918579102 3 25.435056686401367 4 26.182579040527344 5 27.191728591918945
		 6 28.370428085327148 7 29.041864395141602 8 28.52935791015625 9 25.914134979248047
		 10 21.796371459960937 11 17.779850006103516 12 15.489598274230955 13 15.682851791381836
		 14 17.389690399169922 15 19.661670684814453 16 21.55780029296875 17 23.095745086669922
		 18 24.688196182250977 19 25.993736267089844 20 26.671646118164063 21 26.5462646484375
		 22 25.828109741210938 23 24.755455017089844 24 23.566640853881836 25 21.988315582275391
		 26 20.003219604492188 27 18.236650466918945 28 17.318429946899414 29 17.478221893310547
		 30 18.286092758178711 31 19.384521484375 32 20.417190551757812 33 22.327983856201172
		 34 25.910652160644531 35 30.935251235961914 36 37.241298675537109 37 44.702617645263672
		 38 53.091102600097656 39 61.883518218994141 40 70.493064880371094 41 78.201698303222656
		 42 83.96417236328125 43 87.237197875976563 44 88.013206481933594 45 88.186775207519531
		 46 90.004570007324219 47 92.978500366210937 48 96.092178344726563 49 98.260086059570313
		 50 99.211578369140625 51 99.490737915039063 52 99.197479248046875 53 98.437225341796875
		 54 96.735633850097656 55 94.233108520507813 56 91.927940368652344 57 90.769454956054688
		 58 91.314849853515625 59 92.952095031738281 60 94.850776672363281 61 96.150398254394531
		 62 96.328094482421875 63 95.9185791015625 64 95.626869201660156 65 95.688865661621094
		 66 95.83538818359375 67 96.010490417480469 68 96.158042907714844 69 96.276893615722656
		 70 96.392364501953125 71 96.487815856933594 72 96.54656982421875 73 96.526687622070313
		 74 96.449539184570313 75 96.393402099609375 76 95.363471984863281 77 92.571510314941406
		 78 88.301773071289062 79 82.804359436035156 80 76.340522766113281 81 69.373252868652344
		 82 62.221595764160149 83 54.91015625 84 38.028499603271484 85 22.121963500976563
		 86 17.401681900024414 87 15.043471336364748 88 14.688449859619142 89 15.489919662475586
		 90 16.190515518188477 91 16.758157730102539 92 17.801115036010742 93 19.818609237670898
		 94 22.622613906860352 95 25.183176040649414 96 26.486705780029297 97 25.895378112792969
		 98 24.090824127197266 99 22.036031723022461 100 20.672590255737305 101 20.134346008300781
		 102 19.931718826293945 103 20.060190200805664 104 20.516353607177734 105 21.583820343017578
		 106 23.187627792358398 107 24.776178359985352 108 25.766143798828125 109 25.963735580444336
		 110 25.693687438964844 111 25.15106201171875 112 24.53093147277832 113 23.684782028198242
		 114 22.574764251708984 115 21.562723159790039 116 21.011629104614258 117 21.11042594909668
		 118 21.635103225708008 119 22.329771041870117 120 22.938976287841797;
createNode animCurveTU -n "Character1_LeftHandIndex2_visibility";
	rename -uid "3146467B-4E22-FCC1-7781-148E2534E6FC";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateX";
	rename -uid "EED4C379-4179-D21D-F87A-BE879A281F78";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.5761528015136719;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateY";
	rename -uid "75B72336-45B6-0195-BD68-CCB5AA7DE450";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8397388458251953;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateZ";
	rename -uid "56D4067D-46CA-2D77-7619-A1AD35A03F30";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3069263696670532;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleX";
	rename -uid "55D38D81-440F-C25C-EFC8-55A3413AABA0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleY";
	rename -uid "0FA0E1F5-4B9C-72C5-3794-C59E94E2B545";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleZ";
	rename -uid "91BFC5AE-4123-5588-024E-ED92ADED6456";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateX";
	rename -uid "8560746F-4590-8A74-C4BE-079E9CF6F62B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2473842875569972e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateY";
	rename -uid "611AE3A7-4A7D-89E7-7C1B-11B14A7EF239";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.4367076889575401e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateZ";
	rename -uid "408FBF61-4457-D77F-AC0F-F88864EE1440";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.2412148104631342e-007;
createNode animCurveTU -n "Character1_LeftHandIndex3_visibility";
	rename -uid "B4E92765-4DCB-CB13-1A4D-D7BBADC3F3A8";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandRing1_translateX";
	rename -uid "3EC7EF4E-41C2-5DC7-08F9-FAA2A8D0A6B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4602031707763672;
createNode animCurveTL -n "Character1_LeftHandRing1_translateY";
	rename -uid "C330E152-474E-84DB-B868-2893AF40A5FA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.5015654563903809;
createNode animCurveTL -n "Character1_LeftHandRing1_translateZ";
	rename -uid "22662CE9-417D-C2CA-5C1A-888F12E0F337";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.6178884506225586;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleX";
	rename -uid "7CF5D81B-49AF-34BE-862C-7BA0E2BACF92";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleY";
	rename -uid "F27CDBE3-41BF-7F46-3D13-2FA1B4948BBD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleZ";
	rename -uid "C1D5407A-4F27-AB68-36BE-708839423915";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateX";
	rename -uid "B4E67027-4CD0-7279-F65A-43B9099373B7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 3.1731135845184326 1 3.3038613796234131
		 2 3.4447753429412842 3 3.5887103080749516 4 3.7269906997680664 5 3.9238421916961665
		 6 4.1686367988586426 7 4.3152756690979004 8 4.2028741836547852 9 3.6765961647033696
		 10 3.0063822269439697 11 2.5358448028564453 12 2.3469903469085693 13 2.3607220649719238
		 14 2.4996273517608643 15 2.7340338230133057 16 2.9734194278717041 17 3.1971366405487061
		 18 3.4569323062896729 19 3.6914532184600826 20 3.8209285736083984 21 3.7965836524963383
		 22 3.6606214046478271 23 3.4685392379760742 24 3.2709665298461914 25 3.0333654880523682
		 26 2.7742002010345459 27 2.5803694725036621 28 2.4931924343109131 29 2.5076992511749268
		 30 2.5853259563446045 31 2.7023906707763672 32 2.5068936347961426 33 1.911445736885071
		 34 1.3446335792541504 35 1.0926276445388794 36 1.4302263259887695 37 2.1060831546783447
		 38 1.5310040712356567 39 0.72141975164413452 40 4.1810054779052734 41 13.648677825927734
		 42 24.035085678100586 43 28.010189056396484 44 27.562332153320313 45 27.65583610534668
		 46 28.637256622314453 47 30.248594284057617 48 31.937501907348636 49 33.111122131347656
		 50 33.625003814697266 51 33.775596618652344 52 33.617397308349609 53 33.206859588623047
		 54 32.286167144775391 55 30.929250717163086 56 29.678865432739254 57 29.051179885864258
		 58 29.346588134765625 59 30.234268188476563 60 31.264333724975586 61 31.96905517578125
		 62 32.065361022949219 63 31.843397140502933 64 31.685245513916012 65 31.718858718872074
		 66 31.798299789428707 67 31.893222808837891 68 31.973199844360348 69 32.037612915039063
		 70 32.100189208984375 71 32.151908874511719 72 32.183746337890625 73 32.172969818115234
		 74 32.131168365478516 75 32.100749969482422 76 32.124092102050781 77 32.240280151367188
		 78 32.417655944824219 79 29.998134613037109 80 23.482294082641602 81 15.520775794982908
		 82 9.1579666137695313 83 5.6915802955627441 84 4.1452722549438477 85 3.3007025718688965
		 86 2.9114401340484619 87 2.7832410335540771 88 2.726210355758667 89 2.6809241771697998
		 90 2.682164192199707 91 2.7303421497344971 92 2.8315579891204834 93 3.0665414333343506
		 94 3.4598162174224854 95 3.875950813293457 96 4.0878615379333496 97 3.9238638877868652
		 98 3.5462841987609863 99 3.1736886501312256 100 2.9466664791107178 101 2.8482909202575684
		 102 2.7995693683624268 103 2.7969300746917725 104 2.8412725925445557 105 2.9769840240478516
		 106 3.211345911026001 107 3.4721260070800781 108 3.6491670608520508 109 3.6858453750610352
		 110 3.6358294486999512 111 3.5378518104553223 112 3.429995059967041 113 3.2898833751678467
		 114 3.1183722019195557 115 2.9740934371948242 116 2.9003562927246094 117 2.9133257865905762
		 118 2.9840304851531982 119 3.0823893547058105 120 3.1731135845184326;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateY";
	rename -uid "353C6047-41E2-D8F8-E9C6-888901992BD2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 7.5687365531921396 1 8.1183404922485352
		 2 8.6752986907958984 3 9.2119636535644531 4 9.701080322265625 5 10.358860015869141
		 6 11.123142242431641 7 11.556412696838379 8 11.22584056854248 9 9.525609016418457
		 10 6.8121886253356934 11 4.1377005577087402 12 2.6064796447753906 13 2.7357649803161621
		 14 3.8770384788513179 15 5.3931326866149902 16 6.6539325714111328 17 7.6723446846008301
		 18 8.7218160629272461 19 9.577662467956543 20 10.020237922668457 21 9.9384822845458984
		 22 9.469334602355957 23 8.7660150527954102 24 7.9832582473754892 25 6.9394435882568359
		 26 5.6205973625183105 27 4.4427437782287598 28 3.8294198513031006 29 3.9361948966979976
		 30 4.4757499694824219 31 5.2084565162658691 32 4.8138623237609863 33 2.5800011157989502
		 34 -0.70923078060150146 35 -3.905606746673584 36 -8.6891202926635742 37 -13.24065113067627
		 38 -11.066812515258789 39 0.56488543748855591 40 15.611384391784668 41 26.628671646118164
		 42 30.020736694335938 43 29.796438217163086 44 29.653800964355472 45 29.684148788452145
		 46 29.984756469726563 47 30.409730911254883 48 30.768016815185547 49 30.966655731201168
		 50 31.040981292724609 51 31.061321258544918 52 31.039937973022457 53 30.981081008911133
		 54 30.831264495849609 55 30.564605712890625 56 30.268974304199219 57 30.101919174194332
		 58 30.182121276855469 59 30.406316757202152 60 30.635618209838867 61 30.77388954162598
		 62 30.791629791259766 63 30.750326156616211 64 30.719997406005856 65 30.726507186889648
		 66 30.741754531860355 67 30.759725570678711 68 30.774658203125004 69 30.78654670715332
		 70 30.797977447509766 71 30.80733680725098 72 30.813060760498047 73 30.811126708984375
		 74 30.803594589233402 75 30.798080444335941 76 30.802312850952152 77 30.823146820068363
		 78 30.854183197021484 79 31.239772796630859 80 30.924137115478519 81 27.974582672119141
		 82 22.668527603149414 83 17.577112197875977 84 13.997152328491211 85 10.836170196533203
		 86 8.1785039901733398 87 6.2841806411743164 88 5.3479394912719727 89 5.0802044868469238
		 90 5.0876808166503906 91 5.3718452453613281 92 5.9331936836242676 93 7.0932121276855469
		 94 8.7328176498413086 95 10.202655792236328 96 10.876931190490723 97 10.358930587768555
		 98 9.0568742752075195 99 7.5712313652038574 100 6.5231094360351563 101 6.0218954086303711
		 102 5.7605443000793457 103 5.7461133003234863 104 5.9848208427429199 105 6.6711978912353516
		 106 7.7330465316772452 107 8.7796306610107422 108 9.4287872314453125 109 9.5580453872680664
		 110 9.3813619613647461 111 9.025752067565918 112 8.6184320449829102 113 8.0611886978149414
		 114 7.3278446197509766 115 6.6571989059448242 116 6.2912917137145996 117 6.3569240570068359
		 118 6.7052211761474609 119 7.1656618118286142 120 7.5687365531921396;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateZ";
	rename -uid "48227F65-40F4-42AC-05A8-119BBC1DF89D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 22.875423431396484 1 23.658853530883789
		 2 24.45787239074707 3 25.232982635498047 4 25.94416618347168 5 26.908220291137695
		 6 28.040197372436523 7 28.687971115112305 8 28.193330764770508 9 25.68848991394043
		 10 21.804550170898437 11 18.077028274536133 12 15.973946571350098 13 16.150857925415039
		 14 17.717706680297852 15 19.81669807434082 16 21.581577301025391 17 23.022745132446289
		 18 24.524848937988281 19 25.764270782470703 20 26.410802841186523 21 26.291069030761719
		 22 25.606622695922852 23 24.588525772094727 24 23.465850830078125 25 21.984096527099609
		 26 20.133682250976563 27 18.498300552368164 28 17.652128219604492 29 17.799201965332031
		 30 18.543935775756836 31 19.559768676757812 32 19.073469161987305 33 16.241683959960938
		 34 12.279200553894043 35 8.7182092666625977 36 3.8960275650024414 37 -0.30806136131286621
		 38 2.9457767009735107 39 16.869268417358398 40 38.237239837646484 41 64.269523620605469
		 42 86.905189514160156 43 95.433158874511719 44 94.568565368652344 45 94.749366760253906
		 46 96.638053894042969 47 99.707206726074219 48 102.88942718505859 49 105.08422088623047
		 50 106.04175567626953 51 106.32200622558594 52 106.02759552001953 53 105.26275634765625
		 54 103.54271697998047 55 100.99349212646484 56 98.626136779785156 57 97.42999267578125
		 58 97.993629455566406 59 99.680076599121094 60 101.62477111816406 61 102.94860076904297
		 62 103.12912750244141 63 102.71290588378906 64 102.41605377197266 65 102.47917175292969
		 66 102.62828826904297 67 102.80638122558594 68 102.95636749267578 69 103.07712554931641
		 70 103.19439697265625 71 103.29129791259766 72 103.35092926025391 73 103.33074951171875
		 74 103.25244140625 75 103.19544982910156 76 103.23918151855469 77 103.45680999755859
		 78 103.78880310058594 79 98.557426452636719 80 84.879417419433594 81 67.787055969238281
		 82 52.407230377197266 83 41.998561859130859 84 35.410781860351563 85 29.6902961730957
		 86 24.874284744262695 87 21.400901794433594 88 19.753789901733398 89 19.381559371948242
		 90 19.391942977905273 91 19.787063598632813 92 20.570285797119141 93 22.201358795166016
		 94 24.540695190429688 95 26.678461074829102 96 27.674081802368164 97 26.908323287963867
		 98 25.008440017700195 99 22.878969192504883 100 21.397512435913086 101 20.694389343261719
		 102 20.329002380371094 103 20.30885124206543 104 20.642505645751953 105 21.605886459350586
		 106 23.109136581420898 107 24.608146667480469 108 25.547672271728516 109 25.735706329345703
		 110 25.478763580322266 111 24.963434219360352 112 24.376043319702148 113 23.577159881591797
		 114 22.533533096313477 115 21.586175918579102 116 21.071907043457031 117 21.164022445678711
		 118 21.653802871704102 119 22.303840637207031 120 22.875423431396484;
createNode animCurveTU -n "Character1_LeftHandRing1_visibility";
	rename -uid "525096D1-4D0C-F83C-202D-46A97FE79B56";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandRing2_translateX";
	rename -uid "0E81FC5D-488D-1622-842C-4BA1E1989525";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.9237353801727295;
createNode animCurveTL -n "Character1_LeftHandRing2_translateY";
	rename -uid "869A1D16-4DD8-3A4C-FD62-C7967126C4EA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.0094923973083496;
createNode animCurveTL -n "Character1_LeftHandRing2_translateZ";
	rename -uid "D325E787-40B9-8A30-84FA-CE9AC8B83C86";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8221585750579834;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleX";
	rename -uid "66EB5811-4800-A466-E17B-F3AB2F2787E4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleY";
	rename -uid "89EEE7A3-410A-5832-01B8-5FA0C517F60F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleZ";
	rename -uid "3C8FAAE2-435C-13E7-B007-5F84A72E9B4C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateX";
	rename -uid "52210DF9-4FAA-EBDC-342E-7BB72549316F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 9.7664651870727539 1 10.329328536987305
		 2 10.89936637878418 3 11.448322296142578 4 11.948405265808105 5 12.620622634887695
		 6 13.401326179504395 7 13.843782424926758 8 13.506208419799805 9 11.769023895263672
		 10 8.9910392761230469 11 6.2431497573852539 12 4.6646561622619629 13 4.798090934753418
		 14 5.9747252464294434 15 7.5344104766845712 16 8.8287372589111328 17 9.8726015090942383
		 18 10.94696044921875 19 11.822238922119141 20 12.274608612060547 21 12.191055297851563
		 22 11.711489677429199 23 10.992179870605469 24 10.191020965576172 25 9.1215229034423828
		 26 7.7680993080139151 27 6.5571341514587402 28 5.9256758689880371 29 6.0356531143188477
		 30 6.5910987854003906 31 7.3446216583251953 32 5.8269758224487305 33 1.3395060300827026
		 34 -2.4589712619781494 35 -2.2365736961364746 36 -2.5163829326629639 37 3.4856564998626709
		 38 11.08668041229248 39 19.723087310791016 40 28.99468994140625 41 36.480110168457031
		 42 40.956161499023438 43 42.267383575439453 44 42.038719177246094 45 42.086856842041016
		 46 42.579597473144531 47 43.341358184814453 48 44.080955505371094 49 44.561492919921875
		 50 44.763584136962891 51 44.821861267089844 52 44.760627746582031 53 44.599521636962891
		 54 44.226509094238281 55 43.646430969238281 56 43.078498840332031 57 42.780750274658203
		 58 42.921958923339844 59 43.334835052490234 60 43.793113708496094 61 44.094226837158203
		 62 44.134609222412109 63 44.041259765625 64 43.974151611328125 65 43.988456726074219
		 66 44.022174835205078 67 44.062297821044922 68 44.095966339111328 69 44.122993469238281
		 70 44.149166107177734 71 44.170742034912109 72 44.183998107910156 73 44.179515838623047
		 74 44.162097930908203 75 44.149402618408203 76 44.159145355224609 77 44.207489013671875
		 78 44.280784606933594 79 43.805381774902344 80 42.241180419921875 81 39.597969055175781
		 82 35.909397125244141 83 31.236734390258789 84 25.767860412597656 85 19.769901275634766
		 86 13.972079277038574 87 9.4960794448852539 88 7.4879713058471689 89 7.2127881050109854
		 90 7.2204742431640625 91 7.5125365257263184 92 8.0891208648681641 93 9.2791643142700195
		 94 10.958215713500977 95 12.461019515991211 96 13.149859428405762 97 12.620695114135742
		 98 11.28971004486084 99 9.7690210342407227 100 8.6945428848266602 101 8.1801862716674805
		 102 7.9118361473083496 103 7.8970155715942392 104 8.142125129699707 105 8.8464450836181641
		 106 9.9347782135009766 107 11.006110191345215 108 11.67003345489502 109 11.80218505859375
		 110 11.621541976928711 111 11.257877349853516 112 10.841178894042969 113 10.270813941955566
		 114 9.5196437835693359 115 8.832087516784668 116 8.4566917419433594 117 8.5240392684936523
		 118 8.8813409805297852 119 9.3534259796142578 120 9.7664651870727539;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateY";
	rename -uid "D63CB798-43F7-A409-0499-4CB74A8BF9FD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -1.8647100925445559 1 -2.0409247875213623
		 2 -2.2273974418640137 3 -2.4146668910980225 4 -2.591895580291748 5 -2.8401951789855957
		 6 -3.1432352066040039 7 -3.3220667839050293 8 -3.1851601600646973 9 -2.5275921821594238
		 10 -1.6346931457519531 11 -0.93613868951797485 12 -0.61529481410980225 13 -0.64018183946609497
		 14 -0.87748616933822632 15 -1.2419832944869995 16 -1.5884037017822266 17 -1.8973400592803955
		 18 -2.2433338165283203 19 -2.5465829372406006 20 -2.7109375 21 -2.6801865100860596
		 22 -2.50714111328125 23 -2.2585275173187256 24 -1.996899485588074 25 -1.6723719835281372
		 26 -1.3015578985214233 27 -1.0068897008895874 28 -0.86695069074630737 29 -0.89065152406692505
		 30 -1.0146816968917847 31 -1.1945570707321167 32 -0.84592050313949585 33 -0.12642720341682434
		 34 0.19177430868148804 35 0.31992241740226746 36 0.15517976880073547 37 -0.41317200660705566
		 38 -2.2904453277587891 39 -6.2115178108215332 40 -13.021933555603027 41 -21.247100830078125
		 42 -27.977060317993164 43 -30.348028182983402 44 -29.917545318603519 45 -30.007532119750977
		 46 -30.94865798950195 47 -32.4825439453125 48 -34.079402923583984 49 -35.185039520263672
		 50 -35.668598175048828 51 -35.810268402099609 52 -35.66143798828125 53 -35.275142669677734
		 54 -34.408115386962891 55 -33.127170562744141 56 -31.941587448120117 57 -31.343900680541992
		 58 -31.625427246093754 59 -32.468955993652344 60 -33.443950653076172 61 -34.109161376953125
		 62 -34.199974060058594 63 -33.990634918212891 64 -33.841411590576172 65 -33.873134613037109
		 66 -33.948089599609375 67 -34.037639617919922 68 -34.113067626953125 69 -34.173812866210938
		 70 -34.232810974121094 71 -34.281570434570313 72 -34.311580657958984 73 -34.301425933837891
		 74 -34.262016296386719 75 -34.233341217041016 76 -34.255348205566406 77 -34.3648681640625
		 78 -34.532020568847656 79 -33.470653533935547 80 -30.298303604125973 81 -25.738988876342773
		 82 -20.505641937255859 83 -15.178638458251951 84 -10.295655250549316 85 -6.238560676574707
		 86 -3.3748888969421387 87 -1.7828351259231567 88 -1.2302995920181274 89 -1.1621171236038208
		 90 -1.1639970541000366 91 -1.236473560333252 92 -1.385523796081543 93 -1.7184456586837769
		 94 -2.2471108436584473 95 -2.7801899909973145 96 -3.0438907146453857 97 -2.8402223587036133
		 98 -2.359778881072998 99 -1.8654927015304565 100 -1.5506129264831543 101 -1.4097921848297119
		 102 -1.3388484716415405 103 -1.3349806070327759 104 -1.3996249437332153 105 -1.5934231281280518
		 106 -1.9165841341018677 107 -2.2632184028625488 108 -2.4924571514129639 109 -2.5394177436828613
		 110 -2.475337028503418 111 -2.3488395214080811 112 -2.2079916000366211 113 -2.0222411155700684
		 114 -1.7898992300033569 115 -1.5893527269363403 116 -1.4847012758255005 117 -1.5032256841659546
		 118 -1.6033364534378052 119 -1.7403602600097656 120 -1.8647100925445559;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateZ";
	rename -uid "3D52FBF3-4A88-30AD-A900-3A98EA09FCC2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 12.489145278930664 1 13.216601371765137
		 2 13.954545974731445 3 14.666414260864258 4 15.315996170043947 5 16.190895080566406
		 6 17.209613800048828 7 17.78828239440918 8 17.346694946289062 9 15.08286666870117
		 10 11.488808631896973 11 7.9589457511901847 12 5.9401178359985352 13 6.1105637550354004
		 14 7.615236759185791 15 9.6149587631225586 16 11.279688835144043 17 12.626230239868164
		 18 14.016216278076172 19 15.152012825012209 20 15.740302085876463 21 15.631575584411623
		 22 15.008123397827148 23 14.074817657470703 24 13.037741661071777 25 11.65699577331543
		 26 9.9151487350463867 27 8.3612194061279297 28 7.5524497032165527 29 7.6932377815246582
		 30 8.4047489166259766 31 9.3712749481201172 32 7.4261226654052734 33 1.7027676105499268
		 34 -3.3345100879669189 35 -3.5472347736358643 36 -3.2794604301452637 37 4.435600757598877
		 38 14.197311401367188 39 25.588678359985352 40 38.552074432373047 41 50.213031768798828
		 42 58.331573486328132 43 61.02825927734375 44 60.543033599853509 45 60.644607543945313
		 46 61.702453613281257 47 63.411212921142571 48 65.174423217773438 49 66.38861083984375
		 50 66.918464660644531 51 67.073600769042969 52 66.910629272460938 53 66.487388610839844
		 54 65.535865783691406 55 64.124626159667969 56 62.810523986816406 57 62.144443511962898
		 58 62.458515167236328 59 63.396148681640625 60 64.474357604980469 61 65.207160949707031
		 62 65.307052612304687 63 65.076744079589844 64 64.912452697753906 65 64.947380065917969
		 66 65.0299072265625 67 65.128463745117187 68 65.211463928222656 69 65.278274536132812
		 70 65.343162536621094 71 65.396774291992188 72 65.429763793945313 73 65.418601989746094
		 74 65.375274658203125 75 65.343742370605469 76 65.367942810058594 77 65.48834228515625
		 78 65.672004699707031 79 64.503814697265625 80 60.972301483154297 81 55.721027374267578
		 82 49.259624481201172 83 41.886253356933594 84 33.914382934570312 85 25.651773452758789
		 86 17.956260681152344 87 12.140097618103027 88 9.5553216934204102 89 9.2020635604858398
		 90 9.2119274139404297 91 9.5868663787841797 92 10.327787399291992 93 11.860261917114258
		 94 14.030800819396973 95 15.98298454284668 96 16.881162643432617 97 16.190988540649414
		 98 14.460604667663576 99 12.492445945739746 100 11.10684871673584 101 10.44489860534668
		 102 10.099869728088379 103 10.080820083618164 104 10.39594841003418 105 11.30250072479248
		 106 12.706555366516113 107 14.092872619628906 108 14.954274177551268 109 15.125953674316408
		 110 14.891298294067383 111 14.419313430786133 112 13.87916088104248 113 13.140922546386719
		 114 12.17050838470459 115 11.284004211425781 116 10.800642967224121 117 10.887328147888184
		 118 11.347456932067871 119 11.956048965454102 120 12.489145278930664;
createNode animCurveTU -n "Character1_LeftHandRing2_visibility";
	rename -uid "2E9E39D4-4DB9-623F-E330-38A59050FD8A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandRing3_translateX";
	rename -uid "BC5AACB6-4136-FC17-9717-5EB6582860AC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6098947525024414;
createNode animCurveTL -n "Character1_LeftHandRing3_translateY";
	rename -uid "7F5E6DC3-4480-23FB-FDD4-26A7B48FA67B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2952473163604736;
createNode animCurveTL -n "Character1_LeftHandRing3_translateZ";
	rename -uid "EB168465-4C2B-849C-DBC8-34A9F597431E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3554067611694336;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleX";
	rename -uid "7A0D0FBA-4E58-34B0-F859-6FA560424EA5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleY";
	rename -uid "306C8DA3-4263-E69E-B67F-2582BD8B3F1A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleZ";
	rename -uid "CC828800-4A77-6206-5DED-F59F0DCA43A2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateX";
	rename -uid "83B00A7E-4547-A156-EB8D-36B6B5C118A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.6608505499534658e-008;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateY";
	rename -uid "3F76A625-4747-B57D-2114-AAA7ADB14535";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.3796992460775073e-007;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateZ";
	rename -uid "0741EDB8-4336-DC2D-4B15-779061C7CE78";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8264045681680727e-007;
createNode animCurveTU -n "Character1_LeftHandRing3_visibility";
	rename -uid "77B7DA0F-4F70-579A-AF1E-EE86BB5D098D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightShoulder_translateX";
	rename -uid "6A7D66B9-48CF-6298-A5BC-2AB212D99008";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 14.332768440246582;
createNode animCurveTL -n "Character1_RightShoulder_translateY";
	rename -uid "B5E7B66B-452D-63A5-7439-DE8428340FA9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5798094272613525;
createNode animCurveTL -n "Character1_RightShoulder_translateZ";
	rename -uid "C64B0875-450C-DDEC-E93F-BE9AAA0959C1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1086856126785278;
createNode animCurveTU -n "Character1_RightShoulder_scaleX";
	rename -uid "8AC6282F-4FAB-EDDF-597C-D89F7DF89DA7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightShoulder_scaleY";
	rename -uid "3C6C19DE-4A44-3D89-59DF-6281433063A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightShoulder_scaleZ";
	rename -uid "D4DF1F9F-4C4F-4FCC-D2B4-60ADD6542112";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightShoulder_rotateX";
	rename -uid "1A672D78-45A7-4CE2-334E-2B9645FE0AF4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 120 ".ktv[0:119]"  0 -1.920072078704834 1 -1.8306792974472046
		 2 -1.7573060989379883 3 -1.6973785161972046 4 -1.6446936130523682 5 -1.5814369916915894
		 6 -1.5125253200531006 7 -1.4587373733520508 8 -1.4369790554046631 9 -1.4369790554046631
		 11 -1.4369790554046631 12 -1.4369790554046631 13 -1.4022045135498047 14 -1.3222231864929199
		 15 -1.2330710887908936 16 -1.12091064453125 17 -0.94415706396102905 18 -0.70510411262512207
		 19 -0.43089559674263 20 -0.1435723751783371 21 0.13183806836605072 22 0.38953691720962524
		 23 0.63721466064453125 24 0.88466948270797729 25 1.0897865295410156 26 1.2212014198303223
		 27 1.323053240776062 28 1.4586660861968994 29 1.6447979211807251 30 1.8137457370758057
		 31 1.9650800228118896 32 2.1136751174926758 33 2.2286777496337891 34 2.3130712509155273
		 35 2.3846611976623535 36 2.4382898807525635 37 2.4835226535797119 38 2.5481348037719727
		 39 2.6253228187561035 40 2.6870365142822266 41 2.7075521945953369 42 2.7032628059387207
		 43 2.7094736099243164 44 2.7285797595977783 45 2.7629737854003906 46 2.8640384674072266
		 47 3.0419483184814453 48 3.2372872829437256 49 3.390261173248291 50 3.4998185634613037
		 51 3.5921506881713863 52 3.6497008800506587 53 3.6568777561187749 54 3.5959596633911133
		 55 3.4773101806640625 56 3.3274595737457275 57 3.1748130321502686 58 2.9907205104827881
		 59 2.7643172740936279 60 2.5615394115447998 61 2.4699625968933105 62 2.5971889495849609
		 63 2.8097200393676758 64 3.0860874652862549 65 3.4033889770507812 66 3.7369492053985591
		 67 4.0599570274353027 68 4.343235969543457 69 4.5975065231323242 70 4.8237495422363281
		 71 4.9774608612060547 72 5.0171761512756348 73 4.9561395645141602 74 4.8293704986572266
		 75 4.6312656402587891 76 4.3433990478515625 77 4.0404105186462402 78 3.758737325668335
		 79 3.4337449073791504 80 3.0108809471130371 81 2.5112235546112061 82 1.9829483032226563
		 83 1.4269100427627563 84 0.84882992506027222 85 0.2509993314743042 86 -0.3449980616569519
		 87 -0.90774184465408325 88 -1.4071567058563232 89 -1.8566646575927734 90 -2.2655320167541504
		 91 -2.5980870723724365 92 -2.8076364994049072 93 -2.7442286014556885 94 -2.5273704528808594
		 95 -2.2735664844512939 96 -2.0632474422454834 97 -1.8976426124572754 98 -1.7401286363601685
		 99 -1.5903966426849365 100 -1.4509103298187256 101 -1.3194174766540527 102 -1.2060288190841675
		 103 -1.1295406818389893 104 -1.1049755811691284 105 -1.1507425308227539 106 -1.2490538358688354
		 107 -1.3657526969909668 108 -1.4802736043930054 109 -1.6151479482650757 110 -1.7928107976913452
		 111 -1.9640573263168333 112 -2.043677806854248 113 -1.9391804933547971 114 -1.7277588844299316
		 115 -1.5372413396835327 116 -1.4369790554046631 117 -1.4433891773223877 118 -1.5380040407180786
		 119 -1.7127088308334351 120 -1.920072078704834;
createNode animCurveTA -n "Character1_RightShoulder_rotateY";
	rename -uid "250B37C4-4FC6-F4E4-2ED5-B489F74A74AB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 119 ".ktv[0:118]"  0 -22.21434211730957 1 -20.907203674316406
		 2 -19.571617126464844 3 -18.291534423828125 4 -17.150947570800781 5 -16.052238464355469
		 6 -14.990810394287111 7 -14.19013786315918 8 -13.873610496520996 9 -13.873610496520996
		 11 -13.873610496520996 12 -13.873610496520996 13 -13.560927391052246 14 -12.815829277038574
		 15 -11.927727699279785 16 -10.89467716217041 17 -9.4098386764526367 18 -7.4498291015625
		 19 -5.3331055641174316 20 -3.3783161640167236 21 -1.7717465162277222 22 -0.4565537571907044
		 23 0.61333030462265015 24 1.4847729206085205 25 1.8851397037506106 26 1.7434420585632324
		 27 1.4104256629943848 28 1.2373933792114258 29 1.1307181119918823 30 0.73358845710754395
		 31 0.30752721428871155 32 0.1648794412612915 33 0.33810150623321533 34 1.124643087387085
		 35 2.5143249034881592 36 4.0781440734863281 37 5.3871030807495117 38 6.4806766510009766
		 39 7.5362000465393075 40 8.331995964050293 41 8.6466236114501953 42 8.6467399597167969
		 44 8.6460552215576172 45 8.6451330184936523 46 9.0040197372436523 47 9.8857736587524414
		 48 10.994941711425781 49 12.036969184875488 50 13.072199821472168 51 14.192328453063965
		 52 15.150825500488281 53 15.701451301574707 54 15.7303524017334 55 15.365505218505859
		 56 14.723345756530762 57 13.92048168182373 58 12.720221519470215 59 11.129217147827148
		 60 9.630925178527832 61 8.8019065856933594 62 9.2089376449584961 63 10.029497146606445
		 64 11.15534782409668 65 12.478483200073242 66 13.891383171081543 67 15.287192344665529
		 68 16.559804916381836 69 17.947458267211914 70 19.478464126586914 71 20.733152389526367
		 72 21.292699813842773 73 21.26799201965332 74 21.031116485595703 75 20.554487228393555
		 76 19.787527084350586 77 19.029090881347656 78 18.435426712036133 79 17.762371063232422
		 80 16.764701843261719 81 15.550926208496094 82 14.262777328491211 83 12.704418182373047
		 84 10.680831909179687 85 8.1302213668823242 86 5.2344250679016113 87 2.1649909019470215
		 88 -0.90595179796218872 89 -4.1964006423950195 90 -7.6334514617919922 91 -10.725908279418945
		 92 -13.008580207824707 93 -13.278176307678223 94 -12.370267868041992 95 -11.08908748626709
		 96 -10.239178657531738 97 -10.020809173583984 98 -10.00485897064209 99 -10.05119514465332
		 100 -10.019784927368164 101 -9.7668695449829102 102 -9.3978815078735352 103 -9.1465492248535156
		 104 -9.2464427947998047 105 -9.7986392974853516 106 -10.710329055786133 107 -11.901368141174316
		 108 -13.243130683898926 109 -15.102239608764648 110 -17.413894653320313 111 -19.401590347290039
		 112 -20.289016723632812 113 -19.285717010498047 114 -17.000028610229492 115 -14.755050659179689
		 116 -13.873610496520996 117 -14.921607971191404 118 -17.122636795043945 119 -19.784439086914063
		 120 -22.21434211730957;
createNode animCurveTA -n "Character1_RightShoulder_rotateZ";
	rename -uid "8C5797DB-487A-7230-A86F-30881FD8C55C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 119 ".ktv[0:118]"  1 10.844829559326172 2 10.846290588378906
		 3 10.858080863952637 4 10.880935668945312 5 10.920797348022461 6 10.973194122314453
		 7 11.019622802734375 8 11.039607048034668 9 11.039607048034668 11 11.039607048034668
		 12 11.039607048034668 13 11.093389511108398 14 11.216280937194824 15 11.350037574768066
		 16 11.512486457824707 17 11.761414527893066 18 12.121200561523438 19 12.587015151977539
		 20 13.146868705749512 21 13.777326583862305 22 14.457571983337401 23 15.169704437255859
		 24 15.89069652557373 25 16.534273147583008 26 17.067129135131836 27 17.537178039550781
		 28 17.984470367431641 29 18.37775993347168 30 18.600807189941406 31 18.732244491577148
		 32 18.880666732788086 33 19.004072189331055 34 19.100107192993164 35 19.200502395629883
		 36 19.310375213623047 37 19.425086975097656 38 19.553720474243164 39 19.691488265991211
		 40 19.803234100341797 41 19.851350784301758 42 19.853219985961914 43 19.850515365600586
		 44 19.842193603515625 45 19.827215194702148 46 19.895736694335938 47 20.086837768554687
		 48 20.321296691894531 49 20.514322280883789 50 20.655355453491211 51 20.781932830810547
		 52 20.873983383178711 53 20.909191131591797 54 20.871988296508789 55 20.773698806762695
		 56 20.632778167724609 57 20.466514587402344 58 20.226112365722656 59 19.926729202270508
		 60 19.666053771972656 61 19.560661315917969 62 19.727987289428711 63 20.009384155273438
		 64 20.37713623046875 65 20.802801132202148 66 21.255363464355469 67 21.699966430664063
		 68 22.097341537475586 69 22.454402923583984 70 22.773033142089844 71 22.993085861206055
		 72 23.044275283813477 73 22.933618545532227 74 22.706504821777344 75 22.351547241210938
		 76 21.846559524536133 77 21.285032272338867 78 20.7213134765625 79 20.093931198120117
		 80 19.345806121826172 81 18.501487731933594 82 17.627405166625977 83 16.736791610717773
		 84 15.847546577453615 85 14.981189727783203 86 14.166243553161621 87 13.427535057067871
		 88 12.779829025268555 89 12.194572448730469 90 11.687605857849121 91 11.291007995605469
		 92 11.014287948608398 93 10.947824478149414 94 11.022519111633301 95 11.168144226074219
		 96 11.289649963378906 97 11.354483604431152 98 11.404690742492676 99 11.43963623046875
		 100 11.460770606994629 101 11.464158058166504 102 11.445391654968262 103 11.404110908508301
		 104 11.338034629821777 105 11.240821838378906 106 11.113739013671875 107 10.966856956481934
		 108 10.808721542358398 109 10.595615386962891 110 10.350498199462891 111 10.16561222076416
		 112 10.111187934875488 113 10.247265815734863 114 10.529170989990234 115 10.843409538269043
		 116 11.039607048034668 117 11.052194595336914 118 10.976844787597656 119 10.890892028808594
		 120 10.851101875305176;
createNode animCurveTU -n "Character1_RightShoulder_visibility";
	rename -uid "5FB2D937-4740-8BBE-E5FA-46B348CD4D6A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightArm_translateX";
	rename -uid "7EE70755-4674-698E-1DF6-E3AD90239691";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.8354010581970215;
createNode animCurveTL -n "Character1_RightArm_translateY";
	rename -uid "34AF6E3C-4A4E-1C2C-EE02-F882946CF72E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1704782247543335;
createNode animCurveTL -n "Character1_RightArm_translateZ";
	rename -uid "8D7B74D4-4A46-8C7A-7BFC-09B244981275";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.3259291648864746;
createNode animCurveTU -n "Character1_RightArm_scaleX";
	rename -uid "F7057B92-465C-9BAF-D78C-7A92612B3B66";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightArm_scaleY";
	rename -uid "F22BB502-44A2-C161-BE93-44B727B71EEC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightArm_scaleZ";
	rename -uid "63EE62B3-4157-18E0-BE2F-51B0E87FACF4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightArm_rotateX";
	rename -uid "A23FE110-4931-E874-2470-FE8C2538C2C5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 7.9758634567260742 1 17.312616348266602
		 2 30.924890518188477 3 46.369255065917969 4 56.319026947021484 5 55.539485931396484
		 6 49.668563842773438 7 45.168910980224609 8 44.687564849853516 9 49.316440582275391
		 10 57.127826690673828 11 64.418785095214844 12 66.499252319335937 13 60.715835571289063
		 14 52.357990264892578 15 46.455364227294922 16 44.910663604736328 17 48.666961669921875
		 18 56.081367492675781 19 64.49481201171875 20 70.24102783203125 21 71.863227844238281
		 22 71.213966369628906 23 69.340858459472656 24 67.315200805664062 25 64.419410705566406
		 26 60.902816772460938 27 58.235363006591804 28 56.897987365722656 29 56.628562927246094
		 30 57.408203124999993 31 60.516765594482422 32 68.328628540039063 33 80.876968383789063
		 34 94.699005126953125 35 105.22061157226562 36 111.13919830322266 37 114.17364501953125
		 38 113.76777648925781 39 109.73836517333984 40 104.33394622802734 41 101.54434967041016
		 42 104.72315979003906 43 110.40098571777344 44 114.06462097167969 45 112.95944213867187
		 46 106.67289733886719 47 95.861495971679688 48 85.77606201171875 49 81.787750244140625
		 50 85.71783447265625 51 93.505935668945313 52 100.67235565185547 53 104.22948455810547
		 54 104.10052490234375 55 101.97381591796875 56 98.333534240722656 57 93.825027465820313
		 58 87.2745361328125 59 79.109100341796875 60 72.314102172851562 61 68.833702087402344
		 62 71.180488586425781 63 76.805091857910156 64 86.096176147460938 65 98.21783447265625
		 66 110.28422546386719 67 119.36360168457033 68 124.56746673583984 69 124.87212371826173
		 70 120.68827056884766 71 114.40074157714844 72 110.44718170166016 73 111.78682708740234
		 74 115.3526611328125 75 118.08885955810547 76 118.05890655517578 77 112.74398040771484
		 78 102.05876922607422 79 89.160430908203125 80 79.801277160644531 81 77.040679931640625
		 82 77.715362548828125 83 77.994010925292969 84 74.480461120605469 85 66.536872863769531
		 86 56.955883026123047 87 47.846149444580078 88 40.845726013183594 89 35.52362060546875
		 90 31.52543830871582 91 29.322799682617188 92 28.348110198974609 93 30.168621063232422
		 94 35.241626739501953 95 45.164485931396484 96 55.735118865966797 97 55.238803863525391
		 98 48.490837097167969 99 43.296905517578125 100 42.162071228027344 101 45.398750305175781
		 102 51.295589447021484 103 57.463954925537109 104 60.51654052734375 105 58.882160186767585
		 106 54.872756958007813 107 49.911762237548828 108 45.425979614257813 109 40.817771911621094
		 110 36.45123291015625 111 33.582012176513672 112 32.632892608642578 113 35.108180999755859
		 114 42.134777069091797 115 54.176059722900391 116 64.920501708984375 117 62.663459777832031
		 118 47.786376953125 119 26.357208251953125 120 7.9758634567260742;
createNode animCurveTA -n "Character1_RightArm_rotateY";
	rename -uid "8FBE3CFC-4CD6-28BF-387C-A2BF979C3F3B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 70.84698486328125 1 73.649795532226562
		 2 76.121002197265625 3 77.164558410644531 4 76.605628967285156 5 74.763473510742188
		 6 71.943748474121094 7 68.830406188964844 8 66.676651000976563 9 66.423027038574219
		 10 66.966438293457031 11 66.991447448730469 12 65.966804504394531 13 63.366558074951179
		 14 58.970680236816406 15 53.946952819824219 16 50.6474609375 17 50.455791473388672
		 18 51.716205596923828 19 52.636009216308594 20 52.488746643066406 21 51.951961517333984
		 22 51.51458740234375 23 50.890022277832031 24 49.732345581054688 25 46.869560241699219
		 26 42.564197540283203 27 39.104190826416016 28 39.205287933349609 29 44.081756591796875
		 30 51.871921539306641 31 60.099868774414063 32 67.389968872070312 33 71.692726135253906
		 34 71.996421813964844 35 69.48480224609375 36 66.36260986328125 37 64.458587646484375
		 38 64.638290405273438 39 65.894111633300781 40 66.972732543945313 41 67.118049621582031
		 42 66.114555358886719 43 64.156806945800781 44 61.890125274658203 45 60.388736724853516
		 46 60.946125030517585 47 61.222831726074219 48 59.982799530029297 49 58.092430114746094
		 50 56.40765380859375 51 54.345928192138672 52 51.854225158691406 53 50.013965606689453
		 54 49.781101226806641 55 50.622779846191406 56 51.785778045654297 57 52.484691619873047
		 58 52.156970977783203 59 50.590198516845703 60 48.575477600097656 61 47.772872924804688
		 62 50.75384521484375 63 54.766040802001953 64 58.444217681884766 65 60.506477355957031
		 66 60.479236602783196 67 59.090190887451165 68 57.655250549316406 69 57.803459167480469
		 70 59.33140563964843 71 60.529422760009766 72 60.495380401611328 73 59.142009735107422
		 74 56.893119812011719 75 54.616249084472656 76 53.650009155273437 77 55.200626373291016
		 78 57.322662353515625 79 57.816329956054695 80 56.809207916259766 81 55.776580810546875
		 82 55.095909118652344 83 54.679447174072266 84 54.330425262451172 85 53.667301177978516
		 86 52.17193603515625 87 49.484210968017578 88 45.903331756591797 89 40.913768768310547
		 90 35.168277740478516 91 29.858875274658203 92 27.835027694702148 93 35.170944213867188
		 94 47.702983856201172 95 59.192996978759759 96 64.620780944824219 97 63.765117645263679
		 98 59.147052764892585 99 53.101638793945312 100 49.316505432128906 101 49.424003601074219
		 102 51.127700805664063 103 52.785804748535156 104 53.479583740234375 105 53.44940185546875
		 106 52.543281555175781 107 51.038791656494141 108 48.788120269775391 109 44.380565643310547
		 110 38.128402709960938 111 33.089557647705078 112 32.523605346679688 113 39.177692413330078
		 114 50.261428833007812 115 60.458675384521484 116 66.462745666503906 117 69.940086364746094
		 118 72.185295104980469 119 72.193817138671875 120 70.84698486328125;
createNode animCurveTA -n "Character1_RightArm_rotateZ";
	rename -uid "444F721F-4BA1-01C9-0F26-5F8E45092FD3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 17.512258529663086 1 27.167373657226562
		 2 41.313957214355469 3 57.137413024902344 4 66.84381103515625 5 64.710273742675781
		 6 56.768253326416016 7 50.247505187988281 8 48.373931884765625 9 52.486045837402344
		 10 60.315818786621101 11 67.69793701171875 12 69.351173400878906 13 62.159667968750007
		 14 52.043010711669922 15 44.828460693359375 16 42.545986175537109 17 46.230289459228516
		 18 54.074337005615234 19 63.007022857666016 20 68.639434814453125 21 69.244392395019531
		 22 67.156661987304688 23 63.63566589355468 24 59.918643951416016 25 54.637248992919922
		 26 48.377079010009766 27 43.958023071289063 28 42.890602111816406 29 45.391498565673828
		 30 50.610904693603516 31 58.551544189453118 32 71.37091064453125 33 87.98541259765625
		 34 104.33718872070312 35 116.29465484619141 36 122.79087066650391 37 125.90393829345703
		 38 124.73436737060547 39 119.0895462036133 40 112.09037780761719 41 108.30759429931641
		 42 111.27783966064453 43 117.07238006591798 44 120.62133026123048 45 118.48464202880859
		 46 110.35030364990234 47 97.287208557128906 48 85.396705627441406 49 80.464454650878906
		 50 84.484542846679688 51 93.172492980957031 52 101.59208679199219 53 105.94812774658203
		 54 105.86248016357422 55 103.44879913330078 56 99.283363342285156 57 94.062881469726563
		 58 86.164939880371094 59 76.337547302246094 60 68.436393737792969 61 64.818183898925781
		 62 68.598121643066406 63 76.369712829589844 64 88.429435729980469 65 103.76117706298828
		 66 119.12996673583983 67 131.04953002929687 68 137.89985656738281 69 136.94053649902344
		 70 128.84132385253906 71 118.15283203125 72 111.12362670898438 73 111.2095947265625
		 74 114.4215087890625 75 117.11608123779297 76 116.28906249999999 77 108.45497131347656
		 78 94.49554443359375 79 78.748664855957031 80 67.606315612792969 81 64.384048461914063
		 82 65.70050048828125 83 67.269004821777344 84 64.903968811035156 85 57.616664886474616
		 86 48.500640869140625 87 39.702968597412109 88 32.911979675292969 89 27.462858200073242
		 90 23.118947982788086 91 20.548881530761719 92 19.474014282226562 93 21.354759216308594
		 94 27.811885833740234 95 40.577033996582031 96 53.509304046630859 97 52.959072113037109
		 98 44.803012847900391 99 38.390972137451172 100 37.073719024658203 101 41.265132904052734
		 102 48.785247802734375 103 56.729976654052734 104 60.939304351806648 105 59.554229736328132
		 106 54.934879302978516 107 49.152565002441406 108 43.813835144042969 109 38.120021820068359
		 110 32.759456634521484 111 29.416662216186523 112 28.297122955322266 113 30.59797286987305
		 114 38.461780548095703 115 53.015434265136719 116 66.819633483886719 117 66.988128662109375
		 118 54.076850891113281 119 34.327018737792969 120 17.512258529663086;
createNode animCurveTU -n "Character1_RightArm_visibility";
	rename -uid "0462798E-44C7-9951-B2E1-F487D991A1AE";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightForeArm_translateX";
	rename -uid "E8EF8948-44CD-040C-F408-47B6511AA3A7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.753132820129395;
createNode animCurveTL -n "Character1_RightForeArm_translateY";
	rename -uid "49D48343-406A-D236-E804-83B6B66F0F45";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.862128257751465;
createNode animCurveTL -n "Character1_RightForeArm_translateZ";
	rename -uid "7AD0C931-4E2E-3FDE-3FDC-E787D46604FD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.671875953674316;
createNode animCurveTU -n "Character1_RightForeArm_scaleX";
	rename -uid "186BF9B7-4FEE-C46A-8C2D-1DBF41EDCC9C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightForeArm_scaleY";
	rename -uid "87AF3BAA-46FB-CEC8-AC32-3A9F6B8AA9CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightForeArm_scaleZ";
	rename -uid "78A5A11A-4202-C501-33C3-0491D4AE5F20";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightForeArm_rotateX";
	rename -uid "2986E116-4230-EE2D-6A01-D5A5665C4ADC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 89.779289245605469 1 106.36314392089844
		 2 119.06248474121094 3 127.31622314453125 4 131.87861633300781 5 133.4755859375 6 133.18356323242187
		 7 131.75204467773437 8 129.99691772460938 9 127.95816040039062 10 125.41699218750001
		 11 122.83811187744141 12 120.51857757568358 13 118.1952362060547 14 115.65266418457031
		 15 113.20327758789063 16 110.89073944091797 17 109.2550048828125 18 108.07234191894531
		 19 107.17491149902344 20 106.4522705078125 21 106.03060913085937 22 105.89905548095703
		 23 105.82253265380859 24 105.67439270019531 25 105.68205261230469 26 105.89205169677734
		 27 106.01654815673828 28 105.85944366455078 29 104.88109588623047 30 102.87857055664062
		 31 100.67741394042969 32 99.40435791015625 33 101.66184997558594 34 110.66764068603516
		 35 121.44945526123047 36 128.78346252441406 37 132.20382690429687 38 132.52963256835937
		 39 131.03910827636719 40 128.39729309082031 41 125.34449005126953 42 121.79122924804689
		 43 117.44770050048828 44 112.95600891113281 45 108.88059997558594 46 105.43414306640625
		 47 101.87677001953125 48 98.570808410644531 49 95.979667663574219 50 94.410202026367188
		 51 93.589263916015625 52 93.168464660644531 53 92.908950805664063 54 92.868133544921875
		 55 93.108184814453125 56 93.528678894042969 57 94.184516906738281 58 95.044853210449219
		 59 96.171783447265625 60 97.7841796875 61 99.315231323242188 62 102.36608123779297
		 63 106.45130920410156 64 111.27361297607422 65 116.41871643066408 66 121.37411499023436
		 67 125.59567260742189 68 128.57644653320312 69 129.33807373046875 70 127.88999938964844
		 71 125.25736236572267 72 122.4867401123047 73 119.60227203369141 74 116.27629852294923
		 75 112.95269775390625 76 109.82546997070312 77 106.79743194580078 78 103.72299194335937
		 79 100.93997955322266 80 98.842636108398438 81 97.6187744140625 82 97.030441284179688
		 83 96.868125915527344 84 96.971992492675781 85 97.381965637207031 86 98.038116455078125
		 87 98.727630615234375 88 99.402542114257813 89 100.14363098144531 90 101.03890991210937
		 91 101.83758544921875 92 102.50736236572266 93 103.59242248535156 94 105.50244903564453
		 95 107.57763671875 96 108.33329772949219 97 106.30309295654297 98 102.17351531982422
		 99 97.452423095703125 100 93.866836547851562 101 91.972831726074219 102 90.928459167480469
		 103 90.376998901367188 104 90.043891906738281 105 89.948417663574219 106 90.515571594238281
		 107 91.176261901855469 108 91.980155944824219 109 93.060134887695312 110 94.334571838378906
		 111 95.612350463867187 112 96.794906616210937 113 98.295669555664063 114 100.33695220947266
		 115 102.41705322265625 116 103.54791259765625 117 102.70647430419922 118 99.886016845703125
		 119 95.338821411132813 120 89.779289245605469;
createNode animCurveTA -n "Character1_RightForeArm_rotateY";
	rename -uid "A492E559-42C2-4BB2-0DB3-D5B77BC7C4E0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 54.524368286132813 1 48.585258483886719
		 2 39.8016357421875 3 30.637758255004883 4 23.530288696289063 5 19.919698715209961
		 6 18.914194107055664 7 19.087514877319336 8 18.956148147583008 9 18.353551864624023
		 10 17.912059783935547 11 17.410167694091797 12 16.790016174316406 13 16.348087310791016
		 14 16.137697219848633 15 15.831207275390627 16 15.703768730163574 17 14.307047843933105
		 18 12.010034561157227 19 10.027388572692871 20 9.6074085235595703 21 11.710369110107422
		 22 15.452782630920408 23 19.319990158081055 24 21.785144805908203 25 21.396389007568359
		 26 19.221534729003906 27 17.57853889465332 28 18.882829666137695 29 24.535144805908203
		 30 32.749176025390625 31 41.142448425292969 32 47.914813995361328 33 51.300006866455078
		 34 48.788703918457031 35 40.566669464111328 36 30.591594696044918 37 23.787763595581055
		 38 21.96491813659668 39 22.806173324584961 40 24.82145881652832 41 26.441980361938477
		 42 27.375972747802734 43 28.152469635009766 44 28.571401596069336 45 28.714298248291019
		 46 28.702524185180668 47 28.875825881958008 48 28.821409225463867 49 28.125028610229492
		 50 26.135196685791016 51 23.267219543457031 52 20.685577392578125 53 19.564210891723633
		 54 20.819414138793945 55 23.61359977722168 56 26.489120483398437 57 27.960285186767578
		 58 27.122074127197266 59 24.865129470825195 60 22.382301330566406 61 21.160463333129883
		 62 21.727092742919922 63 22.422229766845703 64 22.88487434387207 65 22.856557846069336
		 66 22.239994049072266 67 21.115119934082031 68 19.699474334716797 69 18.487688064575195
		 70 17.774192810058594 71 17.390354156494141 72 17.085939407348633 73 16.827537536621094
		 74 16.572717666625977 75 16.214803695678711 76 16.351682662963867 77 16.448017120361328
		 78 16.499771118164063 79 16.247591018676758 80 15.406241416931151 81 13.398314476013184
		 82 10.590743064880371 83 8.1016340255737305 84 7.0734891891479492 85 8.4461355209350586
		 86 11.468269348144531 87 14.739206314086914 88 16.833574295043945 89 17.10002326965332
		 90 16.390314102172852 91 15.858176231384279 92 16.103448867797852 93 17.443960189819336
		 94 19.223583221435547 95 20.903789520263672 96 22.369377136230469 97 24.001993179321289
		 98 25.65730094909668 99 26.723270416259766 100 26.962947845458984 101 25.927146911621094
		 102 23.886787414550781 103 21.982650756835937 104 21.352323532104492 105 22.881301879882812
		 106 25.185836791992187 107 27.696578979492188 108 28.952730178833008 109 27.933387756347656
		 110 25.571573257446289 111 23.336833953857422 112 22.749368667602539 113 24.259527206420898
		 114 26.919471740722656 115 30.266530990600586 116 34.0697021484375 117 38.685779571533203
		 118 44.084701538085937 119 49.564441680908203 120 54.524368286132813;
createNode animCurveTA -n "Character1_RightForeArm_rotateZ";
	rename -uid "0530AF6B-491F-3ED8-5803-66AA17C81CFF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 44.045482635498047 1 58.494255065917969
		 2 68.3106689453125 3 73.505424499511719 4 75.846641540527344 5 77.122451782226563
		 6 78.036491394042969 7 78.287139892578125 8 77.586463928222656 9 75.515739440917969
		 10 72.279914855957031 11 68.772560119628906 12 65.907791137695312 13 63.982944488525391
		 14 62.464584350585945 15 61.099945068359368 16 59.421516418457024 17 57.879043579101563
		 18 56.678123474121094 19 55.768592834472656 20 54.973335266113281 21 54.152595520019531
		 22 53.318347930908203 23 52.489536285400391 24 51.670417785644531 25 50.832141876220703
		 26 50.064594268798828 27 49.493453979492188 28 49.159713745117188 29 48.673133850097656
		 30 47.847755432128906 31 47.292049407958984 32 47.940296173095703 33 51.520668029785156
		 34 60.124794006347663 35 68.766975402832031 36 73.350234985351563 37 75.225273132324219
		 38 76.415275573730469 39 77.287994384765625 40 77.420372009277344 41 76.525794982910156
		 42 74.162673950195313 43 70.461090087890625 44 66.402503967285156 45 63.009490966796875
		 46 60.723152160644531 47 58.809822082519531 48 57.122756958007813 49 55.646507263183594
		 50 54.69256591796875 51 54.342727661132813 52 54.181976318359375 53 53.723968505859375
		 54 52.645427703857422 55 51.232643127441406 56 49.914398193359375 57 49.084526062011719
		 58 48.610370635986328 59 48.376697540283203 60 48.71783447265625 61 50.037681579589844
		 62 53.412891387939453 63 57.964904785156257 64 63.383365631103523 65 69.275054931640625
		 66 75.18359375 67 80.652107238769531 68 85.277778625488281 69 88.692398071289062
		 70 90.898704528808594 71 92.0386962890625 72 92.32318115234375 73 91.325973510742187
		 74 88.966049194335938 75 86.111640930175781 76 82.873672485351563 77 80.726402282714844
		 78 79.134254455566406 79 77.820159912109375 80 76.588752746582031 81 75.551918029785156
		 82 74.840614318847656 83 74.237220764160156 84 73.470748901367188 85 72.387413024902344
		 86 71.157073974609375 87 69.954666137695313 88 68.850509643554688 89 67.303947448730469
		 90 65.425933837890625 91 63.513004302978523 92 62.894721984863281 93 64.861381530761719
		 94 68.79730224609375 95 72.703811645507813 96 74.322616577148437 97 72.489387512207031
		 98 68.431800842285156 99 63.538688659667962 100 59.512298583984368 101 57.042243957519531
		 102 55.430049896240234 103 54.233016967773438 104 52.913871765136719 105 51.111789703369141
		 106 50.023883819580078 107 48.940773010253906 108 48.288055419921875 109 48.010124206542969
		 110 47.883556365966797 111 48.050758361816406 112 48.695114135742188 113 50.625988006591797
		 114 53.7481689453125 115 56.867725372314453 116 58.480720520019531 117 57.496398925781243
		 118 54.355098724365234 119 49.562530517578125 120 44.045482635498047;
createNode animCurveTU -n "Character1_RightForeArm_visibility";
	rename -uid "031529CA-4456-C025-F47C-73910610B3D0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHand_translateX";
	rename -uid "86B0E130-438F-593A-FC67-399DCE179410";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7167695760726929;
createNode animCurveTL -n "Character1_RightHand_translateY";
	rename -uid "EB82AED7-4181-E248-833D-9598280103B6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.314067840576172;
createNode animCurveTL -n "Character1_RightHand_translateZ";
	rename -uid "F62E0141-41B2-5954-626C-51B3D062B54B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4444055557250977;
createNode animCurveTU -n "Character1_RightHand_scaleX";
	rename -uid "D5E991B9-4269-1086-1ABC-8A84297BD01E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightHand_scaleY";
	rename -uid "186ABF2F-40A2-836B-355C-3DAFE54CF6FB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightHand_scaleZ";
	rename -uid "7835C1A0-404B-4E89-2854-AFBBA015E110";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightHand_rotateX";
	rename -uid "1AEAC655-49EE-B53B-5D68-77BE246A862A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -2.0374476909637451 1 -5.0462698936462402
		 2 -8.4244670867919922 3 -10.989419937133789 4 -11.541519165039063 5 -8.8798599243164062
		 6 -3.9952414035797119 7 1.08162522315979 8 4.4256978034973145 9 4.9873628616333008
		 10 3.9806411266326909 11 2.925886869430542 12 3.3352298736572266 13 6.2340044975280762
		 14 10.586082458496094 15 14.772443771362305 16 17.117532730102539 17 16.69366455078125
		 18 14.56312370300293 19 12.008376121520996 20 10.289666175842285 21 9.5960264205932617
		 22 9.3219671249389648 23 9.5429220199584961 24 10.353707313537598 25 12.648058891296387
		 26 16.134712219238281 27 19.134599685668945 28 19.978876113891602 29 18.209297180175781
		 30 14.967581748962401 31 11.000470161437988 32 7.049675464630127 33 3.8458244800567631
		 34 0.88534098863601685 35 -2.1372416019439697 36 -4.2023444175720215 37 -4.2870745658874512
		 38 -1.27130126953125 39 3.9458296298980717 40 9.4193286895751953 41 13.274907112121582
		 42 14.487121582031252 43 14.155331611633301 44 13.635404586791992 45 14.357715606689451
		 46 17.386531829833984 47 21.789821624755859 48 26.005954742431641 49 28.425756454467773
		 50 28.152292251586914 51 26.232913970947266 52 23.938562393188477 53 22.516639709472656
		 54 22.136838912963867 55 22.182981491088867 56 22.741483688354492 57 23.9425048828125
		 58 26.614364624023438 59 30.457052230834961 60 33.930583953857422 61 35.563770294189453
		 62 33.435947418212891 63 29.808403015136715 64 25.294626235961914 65 20.541454315185547
		 66 16.204212188720703 67 12.921956062316895 68 11.313777923583984 69 13.230278015136719
		 70 18.385284423828125 71 24.251958847045898 72 28.292757034301758 73 29.324798583984371
		 74 28.703197479248043 75 27.705940246582031 76 27.890726089477539 77 30.469316482543945
		 78 34.514266967773438 79 38.498081207275391 80 40.839668273925781 81 40.646804809570313
		 82 38.926036834716797 83 36.906436920166016 84 35.796257019042969 85 35.795852661132812
		 86 36.263080596923828 87 37.171653747558594 88 38.560314178466797 89 41.401878356933594
		 90 45.349586486816406 91 48.13421630859375 92 47.640453338623047 93 42.161579132080078
		 94 33.20904541015625 95 23.951557159423828 96 17.563682556152344 97 15.901608467102049
		 98 17.116355895996094 99 18.826208114624023 100 18.645112991333008 101 15.648847579956053
		 102 11.420392990112305 103 7.3350968360900879 104 4.7502694129943848 105 3.956932544708252
		 106 4.0151081085205078 107 4.5802907943725586 108 5.7120475769042969 109 8.2893514633178711
		 110 11.990869522094727 111 15.072084426879883 112 15.795122146606445 113 12.77235221862793
		 114 7.2571606636047363 115 1.4634304046630859 116 -2.4391281604766846 117 -3.6730918884277339
		 118 -3.4496681690216064 119 -2.6110329627990723 120 -2.0374476909637451;
createNode animCurveTA -n "Character1_RightHand_rotateY";
	rename -uid "0B948941-4912-9545-A585-63B6737B6561";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 5.4970507621765137 1 6.7957258224487305
		 2 8.3360080718994141 3 9.6497879028320312 4 10.205869674682617 5 9.6059703826904297
		 6 8.0421161651611328 7 6.0983896255493164 8 4.6496834754943848 9 4.1513476371765137
		 10 3.9982054233551025 11 3.6544325351715088 12 3.0655081272125244 13 2.2535042762756348
		 14 1.2348872423171997 15 0.20654530823230743 16 -0.42351081967353821 17 -0.4054737389087677
		 18 -0.075903914868831635 19 0.26077452301979065 20 0.42642301321029663 21 0.45270198583602905
		 22 0.45852595567703242 23 0.37943130731582642 24 0.11812685430049896 25 -0.53117024898529053
		 26 -1.3496602773666382 27 -1.9261655807495119 28 -2.1735811233520508 29 -2.0532608032226562
		 30 -1.4720172882080078 31 -0.39771115779876709 32 0.9961211085319519 33 2.3205177783966064
		 34 3.5172665119171143 35 4.6913471221923828 36 5.5263328552246094 37 5.680854320526123
		 38 4.7542357444763184 39 2.8821902275085449 40 0.77727735042572021 41 -0.58572804927825928
		 42 -0.6172904372215271 43 0.037270147353410721 44 0.60321754217147827 45 0.72017025947570801
		 46 0.25226214528083801 47 -0.61737579107284546 48 -1.5482701063156128 49 -2.0449898242950439
		 50 -1.8393441438674925 51 -1.3171473741531372 52 -0.83218222856521606 53 -0.63996690511703491
		 54 -0.83793401718139648 55 -1.2483024597167969 56 -1.7367128133773804 57 -2.178828239440918
		 58 -2.5593194961547852 59 -2.7963182926177979 60 -2.7875275611877441 61 -2.7684063911437988
		 62 -2.8976147174835205 63 -2.8014161586761475 64 -2.2839858531951904 65 -1.30332350730896
		 66 -0.020295975729823112 67 1.2269679307937622 68 1.999474048614502 69 1.3383604288101196
		 70 -0.76067221164703369 71 -3.0249886512756348 72 -4.0897669792175293 73 -3.2611443996429443
		 74 -1.4300472736358643 75 0.28669184446334839 76 1.1740878820419312 77 0.99985551834106456
		 78 0.20854805409908295 79 -0.68132680654525757 80 -1.0778145790100098 81 -0.70329493284225464
		 82 0.0056306584738194942 83 0.65093213319778442 84 0.89412987232208252 85 0.51684939861297607
		 86 -0.26463931798934937 87 -1.1501815319061279 88 -1.8080228567123411 89 -2.1392862796783447
		 90 -2.1203384399414062 91 -1.7499138116836548 92 -1.4331502914428711 93 -1.1878898143768311
		 94 -0.61895060539245605 95 0.15816377103328705 96 0.57628893852233887 97 0.17801357805728912
		 98 -0.69723802804946899 99 -1.5344111919403076 100 -1.8733344078063963 101 -1.555867075920105
		 102 -0.96388769149780285 103 -0.39576512575149536 104 0.015591429546475409 105 0.35425260663032532
		 106 0.71160638332366943 107 0.97380805015563954 108 0.96934992074966431 109 0.45838671922683721
		 110 -0.33137086033821106 111 -0.87554258108139038 112 -0.8673940896987915 113 -0.1406518965959549
		 114 0.90396535396575928 115 1.8083056211471558 116 2.5193881988525391 117 3.2464005947113037
		 118 4.0418591499328613 119 4.7800502777099609 120 5.4970507621765137;
createNode animCurveTA -n "Character1_RightHand_rotateZ";
	rename -uid "AE929A3C-40FF-A1C6-5DC9-A996B6AF4E1B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 29.545639038085934 1 31.066926956176758
		 2 32.578819274902344 3 33.972358703613281 4 35.288196563720703 5 36.948097229003906
		 6 38.667930603027344 7 39.530441284179688 8 38.94189453125 9 36.243209838867188 10 31.996967315673828
		 11 27.652034759521484 12 24.642797470092773 13 23.510915756225586 14 23.248807907104492
		 15 23.061603546142578 16 22.405271530151367 17 21.161972045898438 18 19.688724517822266
		 19 18.327272415161133 20 17.511602401733398 21 17.842912673950195 22 18.995326995849609
		 23 20.070322036743164 24 20.162281036376953 25 18.56005859375 26 15.871697425842285
		 27 13.279352188110352 28 12.087181091308594 29 12.699319839477539 30 14.294054985046387
		 31 16.264083862304687 32 18.044448852539062 33 19.266412734985352 34 19.979804992675781
		 35 20.380683898925781 36 20.531368255615234 37 20.567657470703125 38 20.865013122558594
		 39 21.097007751464844 40 20.449909210205078 41 18.482217788696289 42 14.65269947052002
		 43 9.4667148590087891 44 4.2832889556884766 45 0.51143461465835571 46 -1.3101094961166382
		 47 -2.1530442237854004 48 -2.7317612171173096 49 -3.4677848815917969 50 -4.4252333641052246
		 51 -5.3371086120605469 52 -5.9475536346435547 53 -5.8902335166931152 54 -4.596592903137207
		 55 -2.385838508605957 56 -0.17321689426898956 57 1.1136623620986938 58 0.78344666957855225
		 59 -0.5610690712928772 60 -1.8392940759658813 61 -1.8957427740097046 62 0.34748435020446777
		 63 3.482349157333374 64 7.1162056922912598 65 10.845403671264648 66 14.322162628173828
		 67 17.310310363769531 68 19.679298400878906 69 21.474575042724609 70 22.439586639404297
		 71 22.145977020263672 72 20.73237419128418 73 17.909526824951172 74 13.775360107421875
		 75 9.408961296081543 76 6.1642160415649414 77 4.6630616188049316 78 4.0289325714111328
		 79 3.6389238834381104 80 3.1775987148284912 81 2.6415395736694336 82 2.2022597789764404
		 83 2.0061845779418945 84 2.3208169937133789 85 3.6790955066680913 86 5.8020777702331543
		 87 7.7881813049316397 88 8.7353076934814453 89 8.0096368789672852 90 6.2001872062683105
		 91 4.5285458564758301 92 4.1449732780456543 93 5.4735589027404785 94 7.5624485015869141
		 95 9.4460611343383789 96 10.765933036804199 97 11.703802108764648 98 12.383852005004883
		 99 12.775468826293945 100 13.143441200256348 101 13.434906005859375 102 13.473503112792969
		 103 13.561251640319824 104 14.11943531036377 105 15.717226028442385 106 18.097759246826172
		 107 20.456666946411133 108 21.870309829711914 109 21.887483596801758 110 20.974674224853516
		 111 19.709409713745117 112 18.832315444946289 113 18.231897354125977 114 17.440694808959961
		 115 16.868215560913086 116 17.259185791015625 117 19.246341705322266 118 22.422595977783203
		 119 26.100536346435547 120 29.545639038085934;
createNode animCurveTU -n "Character1_RightHand_visibility";
	rename -uid "56FCFED4-4C8B-7537-C337-A686B93E2BCF";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandThumb1_translateX";
	rename -uid "FF3BDDC7-41DD-EE94-0F1E-16877629700F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5741705894470215;
createNode animCurveTL -n "Character1_RightHandThumb1_translateY";
	rename -uid "62E08588-4888-4A96-9953-78A6A3CFFE7F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4327812194824219;
createNode animCurveTL -n "Character1_RightHandThumb1_translateZ";
	rename -uid "8548FDD7-4A5A-368C-D346-4FB519DD23C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8196568489074707;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleX";
	rename -uid "321D4249-4282-18CA-3535-9C89244F9C1D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleY";
	rename -uid "96DBFA42-46BB-E2FF-5B2B-3791CF120261";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleZ";
	rename -uid "8C828310-4E3F-9C45-63C4-2C8848181E20";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateX";
	rename -uid "DFBEE68E-4B0E-F3AE-2E76-CEB501CEE4F1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 38.973472595214844 1 39.184715270996094
		 2 39.405689239501953 3 39.625232696533203 4 39.831020355224609 5 40.116420745849609
		 6 40.460651397705078 7 40.661907196044922 8 40.507953643798828 9 39.756565093994141
		 10 38.693702697753906 11 37.806552886962891 12 37.370536804199219 13 37.405300140380859
		 14 37.728603363037109 15 38.203208923339844 16 38.636791229248047 17 39.012779235839844
		 18 39.424461364746094 19 39.778579711914063 20 39.968250274658203 21 39.932876586914062
		 22 39.732837677001953 23 39.442344665527344 24 39.132167816162109 25 38.739871978759766
		 26 38.278827667236328 27 37.899703979492188 28 37.714527130126953 29 37.746158599853516
		 30 37.909904479980469 31 38.142662048339844 32 38.3719482421875 33 38.512565612792969
		 34 38.512229919433594 35 38.424057006835938 36 38.315597534179688 37 38.250942230224609
		 38 38.259117126464844 39 38.301692962646484 40 38.338581085205078 41 38.328838348388672
		 42 38.202411651611328 43 38.006801605224609 44 37.867099761962891 45 37.89599609375
		 46 38.207836151123047 47 38.752182006835937 48 39.363521575927734 49 39.811965942382813
		 50 40.014324188232422 51 40.074314117431641 52 40.011302947998047 53 39.849391937255859
		 54 39.494747161865234 55 38.993659973144531 56 38.555259704589844 57 38.343914031982422
		 58 38.442638397216797 59 38.747173309326172 60 39.114982604980469 61 39.375328063964844
		 62 39.411445617675781 63 39.328392028808594 64 39.269638061523438 65 39.282096862792969
		 66 39.311603546142578 67 39.346977233886719 68 39.376876831054688 69 39.401027679443359
		 70 39.424541473388672 71 39.444015502929688 72 39.456024169921875 73 39.451957702636719
		 74 39.436203002929687 75 39.424751281738281 76 39.433536529541016 77 39.477382659912109
		 78 39.544677734375 79 39.611370086669922 80 39.6527099609375 81 39.692813873291016
		 82 39.734531402587891 83 39.720188140869141 84 39.593204498291016 85 39.275157928466797
		 86 38.837635040283203 87 38.434589385986328 88 38.188323974609375 89 38.101058959960937
		 90 38.103473663330078 91 38.196193695068359 92 38.384616851806641 93 38.796138763427734
		 94 39.428909301757813 95 40.047740936279297 96 40.3482666015625 97 40.116451263427734
		 98 39.561115264892578 99 38.974414825439453 100 38.590160369873047 101 38.415027618408203
		 102 38.325920104980469 103 38.321044921875 104 38.402297973632813 105 38.642971038818359
		 106 39.035919189453125 107 39.447860717773438 108 39.715782165527344 109 39.770275115966797
		 110 39.695884704589844 111 39.548316955566406 112 39.382808685302734 113 39.162433624267578
		 114 38.883007049560547 115 38.637958526611328 116 38.508468627929687 117 38.531475067138672
		 118 38.655174255371094 119 38.822830200195313 120 38.973472595214844;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateY";
	rename -uid "8EDA8488-47B9-CF1E-B055-C9AC312A0056";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 27.536184310913086 1 26.976785659790039
		 2 26.410211563110352 3 25.864543914794922 4 25.367410659790039 5 24.699077606201172
		 6 23.922769546508789 7 23.482746124267578 8 23.81846809387207 9 25.545740127563477
		 10 28.306787490844727 11 31.037548065185547 12 32.606643676757813 13 32.473979949951172
		 14 31.304338455200192 15 29.754291534423828 16 28.468074798583984 17 27.430704116821289
		 18 26.362905502319336 19 25.492835998535156 20 25.043102264404297 21 25.126171112060547
		 22 25.602933883666992 23 26.317956924438477 24 27.11424446105957 25 28.177118301391602
		 26 29.522066116333008 27 30.725494384765621 28 31.353090286254883 29 31.243780136108398
		 30 30.691738128662109 31 29.94289398193359 32 29.240905761718746 33 28.82585334777832
		 34 28.82682991027832 35 29.085800170898434 36 29.410417556762699 37 29.607286453247074
		 38 29.582242965698239 39 29.452541351318359 40 29.341058731079105 41 29.370418548583984
		 42 29.756755828857422 43 30.37519645690918 44 30.833906173706055 45 30.73777961730957
		 46 29.739986419677738 47 28.142711639404297 48 26.516946792602539 49 25.412881851196289
		 50 24.935462951660156 51 24.796215057373047 52 24.942506790161133 53 25.323671340942383
		 54 26.18687629699707 55 27.481925964355469 56 28.701980590820316 57 29.325008392333984
		 58 29.030874252319332 59 28.156703948974609 60 27.159442901611328 61 26.487001419067383
		 62 26.395694732666016 63 26.606344223022461 64 26.756889343261719 65 26.724861145019531
		 66 26.649232864379883 67 26.558994293212891 68 26.483070373535156 69 26.421987533569336
		 70 26.362710952758789 71 26.313760757446289 72 26.283647537231445 73 26.29383659362793
		 74 26.333385467529297 75 26.362180709838867 76 26.340082168579102 77 26.230213165283203
		 78 26.062860488891602 79 25.898513793945312 80 25.797388076782227 81 25.699802398681641
		 82 25.598844528198242 83 25.633489608764648 84 25.943134307861328 85 26.742692947387695
		 86 27.905876159667969 87 29.054636001586918 88 29.800439834594727 89 30.073904037475582
		 90 30.066267013549801 91 29.776029586791992 92 29.203056335449215 93 28.02046012878418
		 94 26.351718902587891 95 24.857767105102539 96 24.172834396362305 97 24.699007034301758
		 98 26.022211074829102 99 27.53364372253418 100 28.60142707824707 101 29.112560272216797
		 102 29.379228591918949 103 29.393955230712887 104 29.150382995605465 105 28.450475692749023
		 106 27.36890983581543 107 26.304111480712891 108 25.644145965576172 109 25.512773513793945
		 110 25.692350387573242 111 26.053855895996094 112 26.468050003051758 113 27.034940719604492
		 114 27.781476974487305 115 28.464744567871094 116 28.837789535522464 117 28.770862579345703
		 118 28.415800094604492 119 27.946659088134766 120 27.536184310913086;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateZ";
	rename -uid "7B6D78AF-4419-C441-E729-31B7CEECEDAA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -17.683155059814453 1 -18.332584381103516
		 2 -18.992015838623047 3 -19.628946304321289 4 -20.210983276367187 5 -20.996404647827148
		 6 -21.913490295410156 7 -22.435832977294922 8 -22.037132263183594 9 -20.001993179321289
		 10 -16.790786743164063 11 -13.640802383422852 12 -11.831796646118164 13 -11.984897613525391
		 14 -13.333408355712891 15 -15.119603157043459 16 -16.604293823242188 17 -17.805498123168945
		 18 -19.047161102294922 19 -20.063966751098633 20 -20.59166145324707 21 -20.494073867797852
		 22 -19.935014724731445 23 -19.099567413330078 24 -18.172859191894531 25 -16.940786361694336
		 26 -15.387396812438967 27 -14.000335693359375 28 -13.277233123779297 29 -13.403185844421387
		 30 -14.039226531982422 31 -14.902174949645998 32 -15.71175003051758 33 -16.190893173217773
		 34 -16.189765930175781 35 -15.890755653381348 36 -15.516176223754883 37 -15.289112091064451
		 38 -15.317993164062498 39 -15.467585563659666 40 -15.596193313598633 41 -15.562321662902832
		 42 -15.116761207580566 43 -14.403951644897461 44 -13.875429153442383 45 -13.986180305480957
		 46 -15.136096000671388 47 -16.980594635009766 48 -18.867647171020508 49 -20.157669067382813
		 50 -20.718193054199219 51 -20.882022857666016 52 -20.70991325378418 53 -20.262277603149414
		 54 -19.252475738525391 55 -17.746082305908203 56 -16.333978652954102 57 -15.614707946777344
		 58 -15.954160690307615 59 -16.964405059814453 60 -18.120361328125 61 -18.902534484863281
		 62 -19.008937835693359 63 -18.763532638549805 64 -18.588306427001953 65 -18.625574111938477
		 66 -18.713598251342773 67 -18.818672180175781 68 -18.907114028930664 69 -18.978292465209961
		 70 -19.047388076782227 71 -19.104463577270508 72 -19.139579772949219 73 -19.127696990966797
		 74 -19.081579208374023 75 -19.048007965087891 76 -19.073772430419922 77 -19.201913833618164
		 78 -19.397241592407227 79 -19.589239120483398 80 -19.70747184753418 81 -19.821628570556641
		 82 -19.939804077148438 83 -19.899242401123047 84 -19.537092208862305 85 -18.604822158813477
		 86 -17.254751205444336 87 -15.926729202270508 88 -15.06639575958252 89 -14.751165390014648
		 90 -14.759968757629395 91 -15.094540596008303 92 -15.755428314208984 93 -17.122085571289063
		 94 -19.060203552246094 95 -20.809585571289063 96 -21.61747932434082 97 -20.996488571166992
		 98 -19.444711685180664 99 -17.686100006103516 100 -16.450160980224609 101 -15.859867095947267
		 102 -15.552156448364258 103 -15.535165786743164 104 -15.816213607788086 105 -16.624635696411133
		 106 -17.877191543579102 107 -19.115713119506836 108 -19.88676643371582 109 -20.040609359741211
		 110 -19.830348968505859 111 -19.407756805419922 112 -18.924615859985352 113 -18.264995574951172
		 114 -17.398843765258789 115 -16.60814094543457 116 -16.177106857299805 117 -16.254405975341797
		 118 -16.664728164672852 119 -17.207527160644531 120 -17.683155059814453;
createNode animCurveTU -n "Character1_RightHandThumb1_visibility";
	rename -uid "7C58D4EF-427A-2FB0-9BF2-10B1D66A4808";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandThumb2_translateX";
	rename -uid "7309AF76-4D05-9E7F-4148-66B08C7CFCE8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7422792911529541;
createNode animCurveTL -n "Character1_RightHandThumb2_translateY";
	rename -uid "D73E58FD-405B-1BAF-8D07-C2A6B8A27753";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5767025947570801;
createNode animCurveTL -n "Character1_RightHandThumb2_translateZ";
	rename -uid "6CE4B438-4ACA-49D0-0C14-7C988A602879";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0428388118743896;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleX";
	rename -uid "4FC2EAFB-4B47-1D16-FEED-17A02D657864";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleY";
	rename -uid "DDAB278E-4CCB-8A38-2DCF-169CC9ECEF50";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleZ";
	rename -uid "B4CCC94E-44AA-909A-BAE6-68B75F3526DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateX";
	rename -uid "33C91AA1-43B1-DE88-63FA-6E85617C5CA1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 3.8401813507080074 1 3.2411699295043945
		 2 2.6373317241668701 3 2.0583305358886719 4 1.5328781604766846 5 0.82933950424194336
		 6 0.015894483774900436 7 -0.44355940818786621 8 -0.093113347887992859 9 1.7211489677429199
		 10 4.6702375411987305 11 7.6630244255065918 12 9.4238061904907227 13 9.2736625671386719
		 14 7.9601411819457999 15 6.2460598945617676 16 4.8447189331054687 17 3.7270097732543945
		 18 2.5870380401611328 19 1.6652717590332031 20 1.1910927295684814 21 1.2785674333572388
		 22 1.7815825939178467 23 2.5392701625823975 24 3.3880972862243652 25 4.5301523208618164
		 26 5.9917025566101074 27 7.3166241645812988 28 8.0145320892333984 29 7.8926191329956055
		 30 7.2792263031005859 31 6.453089714050293 32 5.6845583915710449 33 5.2327275276184082
		 34 5.2337884902954102 35 5.5154910087585449 36 5.869631290435791 37 6.0849733352661133
		 38 6.0575556755065918 39 5.9156713485717773 40 5.7938661575317383 41 5.8259305953979492
		 42 6.2487635612487793 43 6.9291963577270508 44 7.4368319511413574 45 7.3302402496337891
		 46 6.2303743362426758 47 4.493011474609375 48 2.7508742809295654 49 1.580862283706665
		 50 1.0778188705444336 51 0.9314005970954895 52 1.0852278470993042 53 1.4867368936538696
		 54 2.400057315826416 55 3.7819526195526123 56 5.0982379913330078 57 5.7763433456420898
		 58 5.4556832313537598 59 4.508115291595459 60 3.4364452362060547 61 2.7190086841583252
		 62 2.6218962669372559 63 2.8460495471954346 64 3.0064787864685059 65 2.9723308086395264
		 66 2.8917343616485596 67 2.7956295013427734 68 2.7148253917694092 69 2.6498532295227051
		 70 2.5868308544158936 71 2.5348095893859863 72 2.5028183460235596 73 2.5136423110961914
		 74 2.5556631088256836 75 2.586266040802002 76 2.5627791881561279 77 2.4460651874542236
		 78 2.2684805393218994 79 2.094304084777832 80 1.9872362613677979 81 1.8839937448501585
		 82 1.7772601842880249 83 1.8138775825500488 84 2.1415731906890869 85 2.9913420677185059
		 86 4.237668514251709 87 5.4815526008605957 88 6.2966790199279785 89 6.5971441268920898
		 90 6.5887398719787598 91 6.269899845123291 92 5.643275260925293 93 4.3611359596252441
		 94 2.5751461982727051 95 0.99610525369644165 96 0.27750825881958008 97 0.82926523685455322
		 98 2.2253808975219727 99 3.8374547958374023 100 4.9891843795776367 101 5.5446414947509766
		 102 5.8355550765991211 103 5.8516445159912109 104 5.5858554840087891 105 4.8256688117980957
		 106 3.6607599258422847 107 2.5245592594146729 108 1.8251452445983889 109 1.6863274574279785
		 110 1.8761141300201414 111 2.2589306831359863 112 2.6988451480865479 113 3.3033108711242676
		 114 4.1037683486938477 115 4.8411149978637695 116 5.245697021484375 117 5.1730055809020996
		 118 4.7881388664245605 119 4.2815995216369629 120 3.8401813507080074;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateY";
	rename -uid "590AA23E-448D-CC46-7344-23BCF6EC9801";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 4.2117342948913574 1 3.5800738334655762
		 2 2.933302640914917 3 2.303673267364502 4 1.7242541313171387 5 0.93651348352432251
		 6 0.0086946878582239151 7 -0.52340269088745117 8 -0.11702391505241394 9 1.9327384233474731
		 10 5.0707073211669922 11 8.0117073059082031 12 9.6297006607055664 13 9.4949245452880859
		 14 8.2904996871948242 15 6.6495347023010254 16 5.2488613128662109 17 4.0931525230407715
		 18 2.8789787292480469 19 1.8709642887115479 20 1.3432697057723999 21 1.4410840272903442
		 22 1.9994528293609621 23 2.8273181915283203 24 3.7359251976013179 25 4.927070140838623
		 26 6.3992705345153809 27 7.683690071105957 28 8.3412809371948242 29 8.2273492813110352
		 30 7.6480846405029288 31 6.8519377708435059 32 6.0947260856628418 33 5.6420483589172363
		 34 5.6431179046630859 35 5.9259943962097168 36 6.2785401344299316 37 6.4912447929382324
		 38 6.4642329216003418 39 6.3241219520568848 40 6.2034029960632324 41 6.2352209091186523
		 42 6.6521854400634766 43 7.3130068778991699 44 7.7978830337524414 45 7.6966443061828604
		 46 6.6341524124145508 47 4.8888969421386719 48 3.0556871891021729 49 1.7774831056594849
		 50 1.2162929773330688 51 1.0516371726989746 52 1.2246090173721313 53 1.6730095148086548
		 54 2.6764013767242432 55 4.1507663726806641 56 5.5062308311462402 57 6.1860032081604004
		 58 5.8661184310913086 59 4.9044251441955566 60 3.7870800495147705 61 3.0213761329650879
		 62 2.916637659072876 63 3.1579990386962891 64 3.3298921585083008 65 3.2933638095855713
		 66 3.2070209980010986 67 3.1038298606872559 68 3.0168697834014893 69 2.94681715965271
		 70 2.8787550926208496 71 2.8224911689758301 72 2.7878537178039551 73 2.7995760440826416
		 74 2.8450543880462646 75 2.8781445026397705 76 2.8527510166168213 77 2.7263364791870117
		 78 2.5332703590393066 79 2.3430624008178711 80 2.2257239818572998 81 2.1122775077819824
		 82 1.9946850538253784 83 2.0350635051727295 84 2.3947656154632568 85 3.3137049674987793
		 86 4.6254324913024902 87 5.8920292854309082 88 6.6991305351257324 89 6.992088794708252
		 90 6.9839277267456055 91 6.6729016304016113 92 6.0535974502563477 93 4.7530508041381836
		 94 2.866124153137207 95 1.1244744062423706 96 0.30908039212226868 97 0.9364296793937682
		 98 2.4862818717956543 99 4.2088823318481445 100 5.3957366943359375 101 5.9551429748535156
		 102 6.2447657585144043 103 6.2607169151306152 104 5.9963135719299316 105 5.2294507026672363
		 106 4.0235714912414551 107 2.8113956451416016 108 2.0474801063537598 109 1.8942521810531616
		 110 2.1036064624786377 111 2.5228629112243652 112 2.9996504783630371 113 3.646061897277832
		 114 4.4865565299987793 115 5.2451896667480469 116 5.6551198959350586 117 5.5817975997924805
		 118 5.1911821365356445 119 4.6708874702453613 120 4.2117342948913574;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateZ";
	rename -uid "4FC972A3-495E-8D24-EACC-15ADEBAB3460";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 2.4896585941314697 1 2.0631957054138184
		 2 1.6366516351699829 3 1.2309134006500244 4 0.86554479598999023 5 0.38071724772453308
		 6 -0.17338907718658447 7 -0.48319533467292786 8 -0.2471025288105011 9 0.99614065885543834
		 10 3.0859062671661377 11 5.283073902130127 12 6.6073002815246582 13 6.4935402870178223
		 14 5.5049777030944824 15 4.2339301109313965 16 3.2120010852813721 17 2.4088380336761475
		 18 1.6012800931930542 19 0.95734381675720215 20 0.62937641143798828 21 0.68970668315887451
		 22 1.03813636302948 23 1.5677070617675781 24 2.1674971580505371 25 2.9848582744598389
		 26 4.0472507476806641 27 5.0251851081848145 28 5.5456695556640625 29 5.4544920921325684
		 30 4.9973969459533691 31 4.3862524032592773 32 3.8225224018096924 33 3.493333101272583
		 34 3.4941043853759766 35 3.6991488933563232 36 3.9578437805175777 37 4.1156449317932129
		 38 4.0955328941345215 39 3.9915504455566406 40 3.9024124145507812 41 3.9258658885955815
		 42 4.2359166145324707 43 4.7378191947937012 44 5.1145763397216797 45 5.0353050231933594
		 46 4.2224030494689941 47 2.9580957889556885 48 1.7165950536727905 49 0.89879566431045521
		 50 0.55136972665786743 51 0.45073384046554565 52 0.55646783113479614 53 0.83359259366989136
		 54 1.4699885845184326 55 2.4480609893798828 56 3.3956763744354248 57 3.889599084854126
		 58 3.6555614471435547 59 2.968977689743042 60 2.2018623352050781 61 1.6941466331481934
		 62 1.6257932186126709 63 1.7836997509002686 64 1.8970066308975222 65 1.8728687763214111
		 66 1.8159414529800415 67 1.7481400966644287 68 1.6912007331848145 69 1.6454622745513916
		 70 1.6011346578598022 71 1.5645734071731567 72 1.5421024560928345 73 1.5497040748596191
		 74 1.5792263746261597 75 1.6007373332977295 76 1.5842273235321045 77 1.5022625923156738
		 78 1.3778018951416016 79 1.2560272216796875 80 1.1813187599182129 81 1.1093860864639282
		 82 1.0351320505142212 83 1.0605939626693726 84 1.2890461683273315 85 1.8863065242767334
		 86 2.7744290828704834 87 3.6744117736816406 88 4.2711424827575684 89 4.4924402236938477
		 90 4.4862408638000488 91 4.2514533996582031 92 3.7923762798309326 93 2.8631675243377686
		 94 1.5929204225540161 95 0.49517920613288874 96 0.004046089481562376 97 0.38066622614860535
		 98 1.3476415872573853 99 2.4877109527587891 100 3.3166005611419678 101 3.7204048633575439
		 102 3.932907342910767 103 3.9446802139282227 104 3.7504677772521977 105 3.1982212066650391
		 106 2.361579418182373 107 1.5573722124099731 108 1.06843101978302 109 0.97195965051651001
		 110 1.1038999557495117 111 1.3711173534393311 112 1.6799470186233521 113 2.1072845458984375
		 114 2.6783428192138672 115 3.2093937397003174 116 3.5027589797973633 117 3.4499483108520508
		 118 3.1710829734802246 119 2.8059871196746826 120 2.4896588325500488;
createNode animCurveTU -n "Character1_RightHandThumb2_visibility";
	rename -uid "0BA55F31-40C1-7414-0EC7-EBB2101CA304";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandThumb3_translateX";
	rename -uid "725A1FC8-4C3A-BFE9-FA0D-A3A1483ECEE2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.588839054107666;
createNode animCurveTL -n "Character1_RightHandThumb3_translateY";
	rename -uid "18E7E493-445B-6D03-79FE-0F91EF9EB94C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.13479287922382355;
createNode animCurveTL -n "Character1_RightHandThumb3_translateZ";
	rename -uid "1C35B927-4EC1-1B68-2774-2FB6C814E482";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.4058942794799805;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleX";
	rename -uid "8B2E07E7-4A28-4322-F3B3-8E86CA87CDF4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleY";
	rename -uid "6F0D2C45-45D8-F6B9-D01D-E499B4237F36";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleZ";
	rename -uid "3B3E0D10-4DDC-10A6-95F6-C8897EC6B0F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateX";
	rename -uid "7241D5C4-4DBD-16C4-042D-93A29A9C8B7A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3596758208223036e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateY";
	rename -uid "78F84A37-4D4B-3800-F52D-C5A809CD5D39";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.8065517072900548e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateZ";
	rename -uid "CD014B94-4495-A555-FD09-91A38742D9A3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9521345368266338e-007;
createNode animCurveTU -n "Character1_RightHandThumb3_visibility";
	rename -uid "8125FA94-49CE-AA8A-DEE6-3EB142F7AB9B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandIndex1_translateX";
	rename -uid "29B4023B-4A9F-13AB-D477-89B37EDA643A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.25625163316726685;
createNode animCurveTL -n "Character1_RightHandIndex1_translateY";
	rename -uid "E9CAC1AD-46AF-1A13-FCBD-3A848BDA0B0B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.892666816711426;
createNode animCurveTL -n "Character1_RightHandIndex1_translateZ";
	rename -uid "D7704100-472C-6A7C-B8F5-2D9640F4C0A3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.533165454864502;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleX";
	rename -uid "36F0E689-434C-C660-7CA2-29847D3782E6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleY";
	rename -uid "C003A53A-4B7D-2337-7A42-CBADD2F7DCC6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleZ";
	rename -uid "79CB2D37-4342-6483-B795-408C136D9C02";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateX";
	rename -uid "39BBB38E-4354-F3D4-ECB9-C5B99C5D391F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -1.158916711807251 1 -1.2291446924209595
		 2 -1.2983725070953369 3 -1.3632069826126099 4 -1.4206775426864624 5 -1.4954959154129028
		 6 -1.5788098573684692 7 -1.6242835521697998 8 -1.5897043943405151 9 -1.4002387523651123
		 10 -1.0591776371002197 11 -0.67884796857833862 12 -0.44220781326293945 13 -0.46270787715911865
		 14 -0.63951998949050903 15 -0.8626895546913147 16 -1.0378690958023071 17 -1.1723001003265381
		 18 -1.3040652275085449 19 -1.4063225984573364 20 -1.4573358297348022 21 -1.4480090141296387
		 22 -1.3936413526535034 23 -1.3094613552093506 24 -1.2120593786239624 25 -1.0762002468109131
		 26 -0.89499890804290771 27 -0.72436690330505371 28 -0.63228946924209595 29 -0.64847707748413086
		 30 -0.72926032543182373 31 -0.83622860908508301 32 -0.93371999263763439 33 -0.99007272720336925
		 34 -0.98994123935699463 35 -0.95489221811294545 36 -0.91042858362197876 37 -0.88317775726318359
		 38 -0.88665622472763062 39 -0.90461575984954823 40 -0.91997826099395752 41 -0.91593915224075328
		 42 -0.86234492063522339 43 -0.77484840154647827 44 -0.70861351490020752 45 -0.72258704900741577
		 46 -0.86468827724456787 47 -1.080701470375061 48 -1.2854787111282349 49 -1.4154846668243408
		 50 -1.4693562984466553 51 -1.4847973585128784 52 -1.4685719013214111 53 -1.4256600141525269
		 54 -1.325128436088562 55 -1.165809154510498 56 -1.0067030191421509 57 -0.92218399047851551
		 58 -0.96235692501068115 59 -1.0788701772689819 60 -1.2064142227172852 61 -1.2857357263565063
		 62 -1.2871090173721313 63 -1.2458256483078003 64 -1.2060443162918091 65 -1.1840529441833496
		 66 -1.1633363962173462 67 -1.1408584117889404 68 -1.1134662628173828 69 -1.0816025733947754
		 70 -1.0475366115570068 71 -1.0107294321060181 72 -0.97061216831207264 73 -0.92476910352706909
		 74 -0.87524127960205078 75 -0.828421950340271 76 -0.78633862733840942 77 -0.75101423263549805
		 78 -0.71953809261322021 79 -0.68428593873977661 80 -0.63731086254119873 81 -0.58884900808334351
		 82 -0.5413813591003418 83 -0.47419917583465576 84 -0.36091908812522888 85 -0.17024214565753937
		 86 0.08462873101234436 87 0.35015463829040527 88 0.54405409097671509 89 0.63852190971374512
		 90 0.65811938047409058 91 0.60633963346481323 92 0.4947346448898316 93 0.26664096117019653
		 94 -0.044945668429136276 95 -0.31654727458953857 96 -0.44766479730606074 97 -0.38107812404632568
		 98 -0.18049941956996918 99 0.059257913380861282 100 0.22638802230358124 101 0.29269492626190186
		 102 0.30975842475891113 103 0.27330651879310608 104 0.18049362301826477 105 -0.0033110834192484617
		 106 -0.25363394618034363 107 -0.49226182699203491 108 -0.65952771902084351 109 -0.74358505010604858
		 110 -0.78165334463119507 111 -0.79408925771713257 112 -0.7999919056892395 113 -0.78465855121612549
		 114 -0.74382990598678589 115 -0.71265155076980591 116 -0.72731781005859375 117 -0.80528515577316284
		 118 -0.92097717523574818 119 -1.0468724966049194 120 -1.158916711807251;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateY";
	rename -uid "C63FA738-4F90-0FDD-17EF-DEB0D60E7953";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 1.6353100538253784 1 1.764832615852356
		 2 1.8976894617080688 3 2.0272367000579834 4 2.1466224193572998 5 2.3091666698455811
		 6 2.5009214878082275 7 2.6110208034515381 8 2.5269267559051514 9 2.1036477088928223
		 10 1.4595745801925659 11 0.86194664239883423 12 0.53633618354797363 13 0.5633617639541626
		 14 0.80566585063934326 15 1.1379020214080811 16 1.4231874942779541 17 1.6596071720123291
		 18 1.9088587760925291 19 2.1163790225982666 20 2.2252035140991211 21 2.2050225734710693
		 22 2.0899002552032471 23 1.9194818735122681 24 1.7328532934188843 25 1.4889276027679443
		 26 1.1887675523757935 27 0.92825448513031006 28 0.79542303085327148 29 0.81840890645980835
		 30 0.93545740842819203 31 1.0968010425567627 32 1.2507294416427612 33 1.342958927154541
		 34 1.3427407741546631 35 1.2850894927978516 36 1.2133221626281738 37 1.1700677871704102
		 38 1.1755588054656982 39 1.2040499448776245 40 1.2286098003387451 41 1.2221355438232422
		 42 1.1373635530471802 43 1.0032975673675537 44 0.90515935420990001 45 0.92563319206237793
		 46 1.1410269737243652 47 1.4967306852340698 48 1.8725323677062988 49 2.1356484889984131
		 50 2.2514071464538574 51 2.2853960990905762 52 2.2496907711029053 53 2.1571884155273437
		 54 1.9505230188369751 55 1.6478010416030884 56 1.3706597089767456 57 1.2321507930755615
		 58 1.2972877025604248 59 1.4935567378997803 60 1.722360372543335 61 1.8730392456054685
		 62 1.8757078647613528 63 1.7963589429855347 64 1.7216678857803345 65 1.6810911893844604
		 66 1.6433141231536865 67 1.6028028726577759 68 1.5540882349014282 69 1.4982949495315552
		 70 1.4396476745605469 71 1.3774003982543945 72 1.3108264207839966 73 1.2363051176071167
		 74 1.1575721502304077 75 1.0847654342651367 76 1.0206092596054077 77 0.96766149997711182
		 78 0.92115509510040283 79 0.86980050802230835 80 0.80253070592880249 81 0.73447412252426147
		 82 0.66908591985702515 83 0.57860314846038818 84 0.43120986223220825 85 0.1965814083814621
		 86 -0.093581005930900574 87 -0.37092801928520203 88 -0.55921375751495361 89 -0.64694654941558838
		 90 -0.66483175754547119 91 -0.61733943223953247 92 -0.51238727569580078 93 -0.28623253107070923
		 94 0.050792038440704346 95 0.37515529990196228 96 0.54350847005844116 97 0.45698234438896174
		 98 0.2088003009557724 99 -0.065802954137325287 100 -0.24460169672966003 101 -0.3128984272480011
		 102 -0.33024123311042786 103 -0.29307734966278076 104 -0.19646929204463959 105 0.0037151398137211795
		 106 0.29721763730049133 107 0.60269945859909058 108 0.83418381214141846 109 0.95662939548492432
		 110 1.0135425329208374 111 1.0323406457901001 112 1.0412989854812622 113 1.0180772542953491
		 114 0.95699441432952881 115 0.91106408834457397 116 0.9325920343399049 117 1.0493459701538086
		 118 1.2302130460739136 119 1.4385144710540771 120 1.6353100538253784;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateZ";
	rename -uid "2F933A18-43B4-C523-6A5B-ACB91C59F9C8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 12.797025680541992 1 13.734759330749512
		 2 14.688142776489258 3 15.609960556030272 4 16.453014373779297 5 17.591463088989258
		 6 18.921539306640625 7 19.679311752319336 8 19.100900650024414 9 16.150238037109375
		 10 11.510767936706543 11 6.9985308647155762 12 4.4334864616394043 13 4.6496777534484863
		 14 6.5611147880554199 15 9.1106252670288086 16 11.242329597473145 17 12.973580360412598
		 18 14.767917633056642 19 16.240015029907227 20 17.004701614379883 21 16.863256454467773
		 22 16.053215026855469 23 14.843738555908201 24 13.504010200500488 25 11.726773262023926
		 26 9.4943618774414062 27 7.5108809471130362 28 6.4812431335449219 29 6.6603546142578125
		 30 7.566347599029541 31 8.7993278503417969 32 9.9596138000488281 33 10.647784233093262
		 34 10.646162986755371 35 10.216590881347656 36 9.6790237426757812 37 9.3534832000732422
		 38 9.3948755264282227 39 9.6093387603759766 40 9.7938013076782227 41 9.7452106475830078
		 42 9.1065549850463867 43 8.0869598388671875 44 7.3327941894531259 45 7.4906930923461923
		 46 9.1342487335205078 47 11.784116744995117 48 14.508253097534178 49 16.375770568847656
		 50 17.188116073608398 51 17.425624847412109 52 17.176111221313477 53 16.527339935302734
		 54 15.06499671936035 55 12.887829780578613 56 10.853487014770508 57 9.8203649520874023
		 58 10.307647705078125 59 11.760794639587402 60 13.428184509277344 61 14.511876106262205
		 62 14.530972480773926 63 13.961751937866211 64 13.423182487487793 65 13.12944221496582
		 66 12.855221748352051 67 12.560337066650391 68 12.20460033416748 69 11.795608520507813
		 70 11.36385440826416 71 10.903474807739258 72 10.408608436584473 73 9.8515214920043945
		 74 9.2592182159423828 75 8.7079648971557617 76 8.2192983627319336 77 7.8138904571533212
		 78 7.4561839103698739 79 7.0593857765197754 80 6.536679744720459 81 6.004368782043457
		 82 5.4895100593566895 83 4.7713279724121094 84 3.5864417552947998 85 1.6587057113647461
		 86 -0.80496364831924438 87 -3.255612850189209 88 -4.9813838005065918 89 -5.8047966957092285
		 90 -5.9742655754089355 91 -5.5254788398742676 92 -4.5470767021179199 93 -2.4963440895080566
		 94 0.43264585733413696 95 3.1306960582733154 96 4.4909238815307617 97 3.7950134277343746
		 98 1.7604405879974365 99 -0.56493675708770752 100 -2.1267504692077637 101 -2.7343127727508545
		 102 -2.8896129131317139 103 -2.5573325157165527 104 -1.7023142576217651 105 0.031747471541166306
		 106 2.492107629776001 107 4.9632482528686523 108 6.783052921295166 109 7.7291727066040039
		 110 8.1652994155883789 111 8.3088579177856445 112 8.3771867752075195 113 8.199951171875
		 114 7.7319760322570801 115 7.3783631324768075 116 7.544292926788331 117 8.4385223388671875
		 118 9.8058290481567383 119 11.355491638183594 120 12.797025680541992;
createNode animCurveTU -n "Character1_RightHandIndex1_visibility";
	rename -uid "47798ED7-48AE-13E0-1EE3-38A89E952DC1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandIndex2_translateX";
	rename -uid "23A17EA1-480D-9A5F-59DC-489A7583D9EC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.3959341049194336;
createNode animCurveTL -n "Character1_RightHandIndex2_translateY";
	rename -uid "E565FDB5-41F3-809B-CA33-EC86E43723A8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8172028064727783;
createNode animCurveTL -n "Character1_RightHandIndex2_translateZ";
	rename -uid "39AC1907-4C15-485D-7F97-0C90A2122658";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.52152138948440552;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleX";
	rename -uid "39299173-46C0-A60F-4B63-CABFF73CF2C8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleY";
	rename -uid "4220F256-44D9-4ABB-CD22-4FB97C00BA4A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleZ";
	rename -uid "CAB900C8-4C37-4C37-6F65-9B9192B8A083";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateX";
	rename -uid "9C28A574-4A26-9A45-E320-F2B71A2211B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 11.788410186767578 1 12.152862548828125
		 2 12.518521308898926 3 12.867411613464355 4 13.182487487792969 5 13.60190486907959
		 6 14.08311939239502 7 14.353043556213377 8 14.147286415100098 9 13.069768905639648
		 10 11.280740737915039 11 9.4282197952270508 12 8.3248424530029297 13 8.4192609786987305
		 14 9.2426586151123047 15 10.309293746948242 16 11.173656463623047 17 11.857393264770508
		 18 12.548895835876465 19 13.103243827819824 20 13.386603355407715 21 13.334426879882813
		 22 13.033546447753906 23 12.577732086181641 24 12.06362247467041 25 11.366623878479004
		 26 10.466728210449219 27 9.6442155838012695 28 9.2086591720581055 29 9.28485107421875
		 30 9.6675128936767578 31 10.180980682373047 32 10.656524658203125 33 10.93508243560791
		 34 10.934429168701172 35 10.760847091674805 36 10.542202949523926 37 10.409025192260742
		 38 10.425991058349609 39 10.513744354248047 40 10.589019775390625 41 10.569209098815918
		 42 10.307619094848633 43 9.8853445053100586 44 9.5693044662475586 45 9.6357345581054687
		 46 10.319008827209473 47 11.389381408691406 48 12.449902534484863 49 13.153778076171875
		 50 13.454102516174316 51 13.541240692138672 52 13.449689865112305 53 13.210082054138184
		 54 12.661706924438477 55 11.823909759521484 56 11.017843246459961 57 10.599843978881836
		 58 10.797725677490234 59 11.380127906799316 60 12.034235000610352 61 12.46916675567627
		 62 12.527844429016113 63 12.392333984375 64 12.295193672180176 65 12.31588077545166
		 66 12.364684104919434 67 12.422837257385254 68 12.471695899963379 69 12.510957717895508
		 70 12.549020767211914 71 12.580425262451172 72 12.599730491638184 73 12.593199729919434
		 74 12.567837715148926 75 12.549362182617188 76 12.596786499023438 77 12.757925987243652
		 78 12.999188423156738 79 13.268231391906738 80 13.514501571655273 81 13.763083457946777
		 82 14.006028175354004 83 14.147475242614746 84 14.142873764038086 85 13.863863945007324
		 86 13.356438636779785 87 12.860292434692383 88 12.6090087890625 89 12.638632774353027
		 90 12.817471504211426 91 13.151700019836426 92 13.650400161743164 93 14.496220588684084
		 94 15.58808422088623 95 16.535541534423828 96 17.011924743652344 97 16.827714920043945
		 98 16.198596954345703 99 15.438299179077147 100 14.913644790649414 101 14.702150344848633
		 102 14.610002517700195 103 14.622601509094237 104 14.723789215087891 105 15.007076263427734
		 106 15.450160980224609 107 15.862698554992676 108 16.033134460449219 109 15.896870613098145
		 110 15.572582244873047 111 15.128851890563965 112 14.637706756591797 113 14.038719177246094
		 114 13.310973167419434 115 12.60187816619873 116 12.07508373260498 117 11.827156066894531
		 118 11.769527435302734 119 11.793361663818359 120 11.788410186767578;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateY";
	rename -uid "18CADAE1-4238-E90A-2323-50B6CD27F33D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -15.640760421752931 1 -16.272445678710938
		 2 -16.917953491210937 3 -17.545166015625 4 -18.121376037597656 5 -18.903314590454102
		 6 -19.822301864624023 7 -20.348409652709961 8 -19.946664810180664 9 -17.914152145385742
		 10 -14.77963924407959 11 -11.810263633728027 12 -10.160307884216309 13 -10.298254013061523
		 14 -11.526876449584961 15 -13.189920425415039 16 -14.600720405578613 17 -15.759445190429687
		 18 -16.972114562988281 19 -17.975564956665039 20 -18.499755859375 21 -18.402647018432617
		 22 -17.847816467285156 23 -17.023611068725586 24 -16.116704940795898 25 -14.923811912536621
		 26 -13.442548751831055 27 -12.143229484558105 28 -11.475215911865234 29 -11.591096878051758
		 30 -12.179343223571777 31 -12.985417366027832 32 -13.749638557434082 33 -14.205439567565918
		 34 -14.204362869262695 35 -13.919625282287598 36 -13.564332008361816 37 -13.349735260009766
		 38 -13.376996994018555 39 -13.518360137939453 40 -13.640095710754395 41 -13.608015060424805
		 42 -13.187243461608887 43 -12.518932342529297 44 -12.027370452880859 45 -12.130090713500977
		 46 -13.205455780029297 47 -14.962116241455078 48 -16.795904159545898 49 -18.068479537963867
		 50 -18.625782012939453 51 -18.789140701293945 52 -18.617528915405273 53 -18.172292709350586
		 54 -17.174005508422852 55 -15.70178699493408 56 -14.342043876647949 57 -13.657636642456055
		 58 -13.97991943359375 59 -14.946535110473635 60 -16.065570831298828 61 -16.830123901367188
		 62 -16.934568405151367 63 -16.693838119506836 64 -16.522289276123047 65 -16.558752059936523
		 66 -16.644924163818359 67 -16.747882843017578 68 -16.834621429443359 69 -16.904478073120117
		 70 -16.97233772277832 71 -17.028423309326172 72 -17.062946319580078 73 -17.051263809204102
		 74 -17.00593376159668 75 -16.972946166992188 76 -17.05767822265625 77 -17.347127914428711
		 78 -17.785011291503906 79 -18.279829025268555 80 -18.738931655883789 81 -19.208471298217773
		 82 -19.673463821411133 83 -19.947029113769531 84 -19.938095092773438 85 -19.400623321533203
		 86 -18.443580627441406 87 -17.532253265380859 88 -17.079549789428711 89 -17.13261604309082
		 90 -17.454692840576172 91 -18.064651489257813 92 -18.994852066040039 93 -20.630678176879883
		 94 -22.860553741455078 95 -24.916847229003906 96 -25.998497009277344 97 -25.576213836669922
		 98 -24.171655654907227 99 -22.546209335327148 100 -21.4666748046875 101 -21.040641784667969
		 102 -20.856607437133789 103 -20.881715774536133 104 -21.083995819091797 105 -21.656528472900391
		 106 -22.571001052856445 107 -23.444255828857422 108 -23.811454772949219 109 -23.517574310302734
		 110 -22.827892303466797 111 -21.905511856079102 112 -20.911838531494141 113 -19.736501693725586
		 114 -18.35908317565918 115 -17.066789627075195 116 -16.136667251586914 117 -15.707372665405272
		 118 -15.608343124389648 119 -15.649266242980957 120 -15.640760421752931;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateZ";
	rename -uid "0177C2C3-495E-F4F6-A290-D8A2B0641ED0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 15.306336402893066 1 15.825138092041016
		 2 16.350088119506836 3 16.855335235595703 4 17.315481185913086 5 17.934047698974609
		 6 18.652841567993164 7 19.060569763183594 8 18.749462127685547 9 17.150428771972656
		 10 14.590578079223633 11 12.038952827453613 12 10.557773590087891 13 10.683525085449219
		 14 11.78801155090332 15 13.241514205932617 16 14.440581321716309 17 15.404206275939941
		 18 16.393901824951172 19 17.199394226074219 20 17.615631103515625 21 17.538747787475586
		 22 17.097492218017578 23 16.435525894165039 24 15.697705268859863 25 14.71112060546875
		 26 13.458423614501953 27 12.332046508789063 28 11.742120742797852 29 11.845005035400391
		 30 12.363723754882812 31 13.065201759338379 32 13.720776557922363 33 14.107582092285156
		 34 14.106672286987305 35 13.865390777587891 36 13.56263542175293 37 13.37884521484375
		 38 13.402233123779297 39 13.523322105407715 40 13.627354621887207 41 13.599961280822754
		 42 13.23921012878418 43 12.660531044006348 44 12.230270385742188 45 12.320512771606445
		 46 13.254879951477051 47 14.743096351623533 48 16.251226425170898 49 17.273395538330078
		 50 17.715253829956055 51 17.844137191772461 52 17.708734512329102 53 17.355960845947266
		 54 16.556911468505859 55 15.356682777404783 56 14.222915649414063 57 13.642326354980469
		 58 13.916583061218262 59 14.730093955993654 60 15.655800819396973 61 16.278966903686523
		 62 16.363534927368164 63 16.168413162231445 64 16.0289306640625 65 16.05860710144043
		 66 16.128677368164063 67 16.212276458740234 68 16.28260612487793 69 16.339179992675781
		 70 16.394081115722656 71 16.439414978027344 72 16.467300415039063 73 16.457864761352539
		 74 16.421239852905273 75 16.394573211669922 76 16.463045120239258 77 16.696306228637695
		 78 17.047325134277344 79 17.441362380981445 80 17.80455207824707 81 18.173681259155273
		 82 18.537002563476562 83 18.74974250793457 84 18.742807388305664 85 18.32408332824707
		 86 17.571165084838867 87 16.844974517822266 88 16.480701446533203 89 16.523527145385742
		 90 16.782737731933594 91 17.270347595214844 92 18.006032943725586 93 19.278238296508789
		 94 20.973115921020508 95 22.501653671264648 96 23.29435920715332 97 22.985734939575195
		 98 21.951164245605469 99 20.7366943359375 100 19.918645858764648 101 19.593069076538086
		 102 19.451930999755859 103 19.471202850341797 104 19.626276016235352 105 20.063222885131836
		 106 20.755369186401367 107 21.410106658935547 108 21.683712005615234 109 21.464817047119141
		 110 20.948589324951172 111 20.252365112304688 112 19.494325637817383 113 18.586095809936523
		 114 17.504226684570313 115 16.470403671264648 116 15.714056015014648 117 15.361289024353027
		 118 15.279571533203125 119 15.313356399536133 120 15.306336402893066;
createNode animCurveTU -n "Character1_RightHandIndex2_visibility";
	rename -uid "6A8D21E1-444E-F177-0464-01BF978C9B47";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandIndex3_translateX";
	rename -uid "0DE81482-4F21-70B9-6548-AEAB7467AA57";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6282989978790283;
createNode animCurveTL -n "Character1_RightHandIndex3_translateY";
	rename -uid "D61F9C01-4A4B-7801-B39D-E6BC97793D17";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.2120883464813232;
createNode animCurveTL -n "Character1_RightHandIndex3_translateZ";
	rename -uid "7AF92178-4E4A-EDE2-14B9-3584812E6BFF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7757487297058105;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleX";
	rename -uid "78163972-480A-3F85-4458-E795865B7909";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleY";
	rename -uid "B923CB8D-471F-2B28-746E-D58EB6ADF2BF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleZ";
	rename -uid "97B5F13B-425A-6563-16B8-7792E685D778";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateX";
	rename -uid "23DC8CD4-4763-3B1C-182D-D18692FF9A7B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.7431861932855099e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateY";
	rename -uid "DB4C5B14-4CCD-CAD0-6F45-01B652E08FE7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0663291760693028e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateZ";
	rename -uid "95C0EE56-4A9B-44F2-A3CB-1D9981F3102B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8196882933807501e-007;
createNode animCurveTU -n "Character1_RightHandIndex3_visibility";
	rename -uid "CEFA3EDE-4C34-5683-05E5-DC85D514B468";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandRing1_translateX";
	rename -uid "4F264E00-42FF-9E9C-F945-22BAC898894A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8543868064880371;
createNode animCurveTL -n "Character1_RightHandRing1_translateY";
	rename -uid "759EB2E4-4623-AA9E-6353-E4B65D7760AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2156095504760742;
createNode animCurveTL -n "Character1_RightHandRing1_translateZ";
	rename -uid "5CC44FEC-4749-164A-F4A6-62BAEF8F0D64";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2236912250518799;
createNode animCurveTU -n "Character1_RightHandRing1_scaleX";
	rename -uid "852E6140-4B80-1726-55E2-01AF226AA193";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandRing1_scaleY";
	rename -uid "E59ADF15-494B-CBF9-FE8C-B7AA523921FF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing1_scaleZ";
	rename -uid "879B7AEB-45B5-A621-02BB-479576F2A7E9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandRing1_rotateX";
	rename -uid "284DF990-4F58-4F57-33D3-ECBD9BD13508";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 118 ".ktv[0:117]"  0 -9.2360963821411133 1 -9.3277769088745117
		 2 -9.4170780181884766 3 -9.4996519088745117 4 -9.5719032287597656 5 -9.6644887924194336
		 6 -9.7653579711914062 7 -9.8192834854125977 8 -9.7783546447753906 9 -9.5463151931762695
		 10 -9.1041879653930664 11 -8.5861167907714844 12 -8.2541027069091797 13 -8.2831134796142578
		 14 -8.5313940048217773 15 -8.8392324447631836 16 -9.0757665634155273 17 -9.2536487579345703
		 18 -9.4243707656860352 19 -9.5539445877075195 20 -9.6174869537353516 21 -9.6059284210205078
		 22 -9.538029670715332 23 -9.4312763214111328 24 -9.3055696487426758 25 -9.1268348693847656
		 26 -8.8832244873046875 27 -8.6492147445678711 28 -8.5213174819946289 29 -8.5438785552978516
		 30 -8.6559810638427734 31 -8.8030910491943359 32 -8.9357337951660156 33 -9.0117273330688477
		 34 -9.0115509033203125 35 -8.9643449783325195 36 -8.9041748046875 37 -8.8671455383300781
		 38 -8.8718786239624023 39 -8.8962860107421875 40 -8.9171237945556641 41 -8.9116487503051758
		 42 -8.8387613296508789 43 -8.7188663482666016 44 -8.6274051666259766 45 -8.6467485427856445
		 46 -8.8419580459594727 47 -9.1328134536743164 48 -9.4005308151245117 49 -9.5654134750366211
		 50 -9.6323432922363281 51 -9.6513595581054687 52 -9.6313753128051758 53 -9.5781230926513672
		 54 -9.4512844085693359 55 -9.2451400756835937 56 -9.0340538024902344 57 -8.9201126098632812
		 58 -8.9744157791137695 59 -9.1303834915161133 60 -9.2982215881347656 61 -9.405186653137207
		 62 -9.4193181991577148 63 -9.3865728378295898 64 -9.3628635406494141 65 -9.3679285049438477
		 66 -9.3798437118530273 67 -9.393977165222168 68 -9.405797004699707 69 -9.4152593612670898
		 70 -9.4244012832641602 71 -9.4319210052490234 75 -9.4244823455810547 76 -9.4278802871704102
		 77 -9.4446916580200195 78 -9.4700345993041992 79 -9.4946098327636719 80 -9.5095787048339844
		 81 -9.5239105224609375 82 -9.5386228561401367 83 -9.5335874557495117 84 -9.4879684448242187
		 85 -9.3651103973388672 86 -9.173619270324707 87 -8.9700632095336914 88 -8.8304224014282227
		 89 -8.7777690887451172 90 -8.7792501449584961 91 -8.8350849151611328 92 -8.9427385330200195
		 93 -9.1539535522460937 94 -9.4260921478271484 95 -9.6429815292358398 96 -9.7336578369140625
		 97 -9.6644983291625977 98 -9.4761409759521484 99 -9.2365207672119141 100 -9.0520563125610352
		 101 -8.9594268798828125 102 -8.910003662109375 103 -8.9072532653808594 104 -8.9524631500244141
		 105 -9.0788812637329102 106 -9.2638731002807617 107 -9.4333992004394531 108 -9.5320358276367187
		 109 -9.5510730743408203 110 -9.5250005722045898 111 -9.4713888168334961 112 -9.4081277847290039
		 113 -9.3184070587158203 114 -9.1948099136352539 115 -9.0763559341430664 116 -9.0095672607421875
		 117 -9.0216588973999023 118 -9.0850086212158203 119 -9.1666364669799805 120 -9.2360963821411133;
createNode animCurveTA -n "Character1_RightHandRing1_rotateY";
	rename -uid "29CA9C1B-4B36-C12D-436A-B88D2110B841";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 4.393496036529541 1 4.6349263191223145
		 2 4.8818645477294922 3 5.1219820976257324 4 5.3426971435546875 5 5.6423540115356445
		 6 5.9946446418762207 7 6.196342945098877 8 6.042323112487793 9 5.2633094787597656
		 10 4.0647940635681152 11 2.9363946914672852 12 2.3139274120330811 13 2.3658185005187988
		 14 2.8292167186737061 15 3.4595620632171631 16 3.9965670108795166 17 4.4388389587402344
		 18 4.902592658996582 19 5.2868351936340332 20 5.4876847267150879 21 5.4504714012145996
		 22 5.2378983497619629 23 4.9223027229309082 24 4.5753812789916992 25 4.1197896003723145
		 26 3.5555851459503174 27 3.0624532699584961 28 2.8096909523010254 29 2.8534970283508301
		 30 3.0761337280273438 31 3.381880521774292 32 3.6723923683166508 33 3.8459270000457759
		 34 3.8455171585083012 35 3.7370884418487549 36 3.6018965244293213 37 3.5202994346618652
		 38 3.5306627750396729 39 3.5844123363494873 40 3.6307148933410645 41 3.6185116767883296
		 42 3.4585452079772949 43 3.2048506736755371 44 3.0185739994049072 45 3.0574760437011719
		 46 3.4654650688171387 47 4.1344032287597656 48 4.8351597785949707 49 5.3224315643310547
		 50 5.5359821319580078 51 5.5985918045043945 52 5.5328192710876465 53 5.3622055053710938
		 54 4.9798707962036133 55 4.4168095588684082 56 3.8979716300964351 57 3.6373879909515376
		 58 3.7600429058074951 59 4.1284589767456055 60 4.5558333396911621 61 4.8482546806335449
		 62 4.8882236480712891 63 4.7961068153381348 64 4.7304792404174805 65 4.7444272041320801
		 66 4.7773928642272949 67 4.8167843818664551 68 4.8499751091003418 69 4.8767080307006836
		 70 4.9026784896850586 71 4.9241447448730469 72 4.9373583793640137 73 4.9328866004943848
		 74 4.9155364036560059 75 4.9029111862182617 76 4.912600040435791 77 4.9608249664306641
		 78 5.0344548225402832 79 5.1069684028625488 80 5.1516890525817871 81 5.1949167251586914
		 82 5.2397141456604004 83 5.2243332862854004 84 5.0872602462768555 85 4.7366600036621094
		 86 4.2352404594421387 87 3.7501091957092285 88 3.4405293464660645 89 3.3280794620513916
		 90 3.3312125205993652 91 3.4505951404571533 92 3.6881635189056401 93 4.1864018440246582
		 94 4.9074974060058594 95 5.5708985328674316 96 5.8806695938110352 97 5.6423859596252441
		 98 5.0523710250854492 99 4.3945870399475098 100 3.940305233001709 101 3.7259132862091064
		 102 3.6148507595062256 103 3.6087327003479004 104 3.7101278305053706 105 4.0040011405944824
		 106 4.4654407501220703 107 4.9283771514892578 108 5.2196030616760254 109 5.2779664993286133
		 110 5.1982202529907227 111 5.0384225845336914 112 4.8565454483032227 113 4.6097164154052734
		 114 4.2883777618408203 115 3.9979732036590581 116 3.8409175872802734 117 3.8690161705017094
		 118 4.0186581611633301 119 4.2178463935852051 120 4.393496036529541;
createNode animCurveTA -n "Character1_RightHandRing1_rotateZ";
	rename -uid "76B6BB6C-4407-191D-6EFA-D28319BDA2E2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 29.838140487670898 1 30.746522903442379
		 2 31.670083999633789 3 32.5631103515625 4 33.379886627197266 5 34.48297119140625
		 6 35.771953582763672 7 36.506446838378906 8 35.945796966552734 9 33.086540222167969
		 10 28.592132568359375 11 24.220195770263672 12 21.733627319335938 13 21.943256378173828
		 14 23.796245574951172 15 26.266874313354492 16 28.332088470458984 17 30.009168624877933
		 18 31.747365951538086 19 33.17352294921875 20 33.914417266845703 21 33.777370452880859
		 22 32.992542266845703 23 31.820816040039063 24 30.522994995117188 25 28.801382064819339
		 26 26.638669967651367 27 24.716732025146484 28 23.718832015991211 29 23.892435073852539
		 30 24.770484924316406 31 25.965253829956055 32 27.089424133300781 33 27.756120681762695
		 34 27.754550933837891 35 27.338386535644531 36 26.817581176757813 37 26.502178192138672
		 38 26.542280197143555 39 26.750064849853516 40 26.928781509399414 41 26.881704330444336
		 42 26.262929916381836 43 25.274986267089844 44 24.544147491455078 45 24.697168350219727
		 46 26.289762496948242 47 28.856931686401364 48 31.495817184448239 49 33.305049896240234
		 50 34.092136383056641 51 34.322273254394531 52 34.080501556396484 53 33.451900482177734
		 54 32.035160064697266 55 29.926101684570313 56 27.95539665222168 57 26.95451545715332
		 58 27.426601409912109 59 28.834339141845703 60 30.449542999267578 61 31.544698715209961
		 62 31.693796157836914 63 31.349958419799805 64 31.104537963867184 65 31.156730651855469
		 66 31.280014038085938 67 31.427204132080075 68 31.551118850708008 69 31.650856018066406
		 70 31.747686386108398 71 31.827680587768558 72 31.876901626586911 73 31.860244750976563
		 74 31.795604705810543 75 31.748554229736328 76 31.784662246704102 77 31.964279174804688
		 78 32.238140106201172 79 32.507411956787109 80 32.673267364501953 81 32.833431243896484
		 82 32.999259948730469 83 32.942340850830078 84 32.434268951416016 85 31.127668380737305
		 86 29.239624023437496 87 27.388433456420898 88 26.193044662475586 89 25.75590705871582
		 90 25.768108367919922 91 26.232097625732422 92 27.15015983581543 93 29.054405212402347
		 94 31.765645980834964 95 34.220516204833984 96 35.355812072753906 97 34.483089447021484
		 98 32.304710388183594 99 29.842258453369137 100 28.117267608642578 101 27.295417785644531
		 102 26.867578506469727 103 26.843969345092773 104 27.234699249267578 105 28.360448837280273
		 106 30.109415054321293 107 31.843446731567383 108 32.924831390380859 109 33.140735626220703
		 110 32.845664978027344 111 32.252883911132813 112 31.575637817382809 113 30.651926040649414
		 114 29.440866470336911 115 28.337451934814453 116 27.736923217773438 117 27.844566345214844
		 118 28.416343688964844 119 29.173686981201168 120 29.838140487670898;
createNode animCurveTU -n "Character1_RightHandRing1_visibility";
	rename -uid "531B26EC-4800-8F00-4DFB-E08653523D27";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandRing2_translateX";
	rename -uid "3BB00C4D-4BCA-E46F-1FA3-9496E6385A01";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.062777042388916;
createNode animCurveTL -n "Character1_RightHandRing2_translateY";
	rename -uid "08BE58E5-44B7-BFE9-5752-118A6A72B2E1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.4930424690246582;
createNode animCurveTL -n "Character1_RightHandRing2_translateZ";
	rename -uid "BDC40780-4245-1B43-8B6D-F8A5E1AB845D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.86127841472625732;
createNode animCurveTU -n "Character1_RightHandRing2_scaleX";
	rename -uid "D528AF87-42F8-7757-AF50-C7A595B08C7F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandRing2_scaleY";
	rename -uid "067575FE-4555-AC9C-2892-6E99C9CD0752";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing2_scaleZ";
	rename -uid "C2C036AA-4E15-BDB7-2891-5490C77C4642";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandRing2_rotateX";
	rename -uid "C72E26DF-4429-52D1-BCA4-82B13E9D292B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 20.286947250366211 1 20.511837005615234
		 2 20.734979629516602 3 20.945363998413086 4 21.13306999206543 5 21.379262924194336
		 6 21.656038284301758 7 21.808355331420898 8 21.692447662353516 9 21.066179275512695
		 10 19.969900131225586 11 18.781896591186523 12 18.055780410766602 13 18.118366241455078
		 14 18.660629272460937 15 19.352415084838867 16 19.902498245239258 17 20.329694747924805
		 18 20.753396987915039 19 21.086074829101562 20 21.253429412841797 21 21.222759246826172
		 22 21.044620513916016 23 20.770864486694336 24 20.456993103027344 25 20.023826599121094
		 26 19.453365325927734 27 18.922588348388672 28 18.638372421264648 29 18.688234329223633
		 30 18.937732696533203 31 19.693645477294922 32 20.980634689331055 33 22.283782958984375
		 34 23.257720947265625 35 23.804540634155273 36 23.982975006103516 37 23.015037536621094
		 38 22.588743209838867 39 23.707956314086914 40 23.945838928222656 41 22.817171096801758
		 42 20.459230422973633 43 17.387767791748047 44 14.495562553405762 45 12.885673522949219
		 46 13.321444511413574 47 15.221257209777832 48 17.636594772338867 49 19.789516448974609
		 50 21.362035751342773 51 22.351594924926758 52 22.758754730224609 53 22.753219604492187
		 54 22.436800003051758 55 21.809127807617188 56 21.033287048339844 57 20.376211166381836
		 58 20.090322494506836 59 20.148990631103516 60 20.438899993896484 61 20.705015182495117
		 62 20.740634918212891 63 20.658269882202148 64 20.599002838134766 65 20.611639022827148
		 66 20.64141845703125 67 20.676841735839844 68 20.706550598144531 69 20.730390548706055
		 70 20.753473281860352 71 20.772493362426758 72 20.784177780151367 73 20.78022575378418
		 74 20.764871597290039 75 20.753679275512695 76 20.563791275024414 77 20.065305709838867
		 78 19.322834014892578 79 18.399303436279297 80 17.380062103271484 81 16.441522598266602
		 82 15.764276504516602 83 15.456278800964354 84 17.927375793457031 85 20.564285278320313
		 86 21.494369506835937 87 22.312778472900391 88 22.955802917480469 89 23.333503723144531
		 90 23.37799072265625 91 23.135501861572266 92 22.679885864257813 93 22.124828338623047
		 94 21.525856018066406 95 20.751773834228516 96 19.611551284790039 97 17.947593688964844
		 98 16.02203369140625 99 14.36163330078125 100 13.908506393432617 101 14.809527397155762
		 102 16.319541931152344 103 17.910882949829102 104 19.17535400390625 105 19.909870147705078
		 106 20.354663848876953 107 20.776237487792969 108 21.029054641723633 109 21.078580856323242
		 110 21.010814666748047 111 20.872886657714844 112 20.712417602539063 113 20.488668441772461
		 114 20.186927795410156 115 19.903894424438477 116 19.746658325195313 117 19.775005340576172
		 118 19.924383163452148 119 20.119096755981445 120 20.286947250366211;
createNode animCurveTA -n "Character1_RightHandRing2_rotateY";
	rename -uid "A34B7115-4D5D-EAD0-CC09-318D2807CBDF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -34.944370269775391 1 -35.645191192626953
		 2 -36.359352111816406 3 -37.0513916015625 4 -37.685565948486328 5 -38.543792724609375
		 6 -39.549053192138672 7 -40.122951507568359 8 -39.684814453125 9 -37.457668304443359
		 10 -33.985782623291016 11 -30.649698257446289 12 -28.773405075073242 13 -28.930946350097656
		 14 -30.328651428222656 15 -32.205886840820312 16 -33.786136627197266 17 -35.076194763183594
		 18 -36.419185638427734 19 -37.525230407714844 20 -38.101200103759766 21 -37.994590759277344
		 22 -37.384674072265625 23 -36.476058959960938 24 -35.472587585449219 25 -34.146537780761719
		 26 -32.489662170410156 27 -31.026294708251953 28 -30.270076751708988 29 -30.40145301818848
		 30 -31.067100524902344 31 -33.176124572753906 32 -37.169307708740234 33 -42.02899169921875
		 34 -46.822242736816406 35 -50.949748992919922 36 -56.675506591796875 37 -62.970455169677741
		 38 -64.271697998046875 39 -59.719005584716797 40 -52.809650421142578 41 -44.449771881103516
		 42 -35.479610443115234 43 -27.136659622192383 44 -20.82611083984375 45 -17.745439529418945
		 46 -18.553369522094727 47 -22.306821823120117 48 -27.737060546875 49 -33.454544067382813
		 50 -38.482692718505859 51 -42.317584991455078 52 -44.165348052978516 53 -44.138679504394531
		 54 -42.687179565429687 55 -40.125904083251953 56 -37.346389770507813 57 -35.220367431640625
		 58 -34.346019744873047 59 -34.523208618164062 60 -35.415889739990234 61 -36.262302398681641
		 62 -36.377708435058594 63 -36.111629486083984 64 -35.921844482421875 65 -35.962192535400391
		 66 -36.057529449462891 67 -36.17138671875 68 -36.267269134521484 69 -36.344463348388672
		 70 -36.419429779052734 71 -36.481372833251953 72 -36.519493103027344 73 -36.506591796875
		 74 -36.456535339355469 75 -36.420101165771484 76 -35.809738159179688 77 -34.270805358886719
		 78 -32.123233795166016 79 -29.64756011962891 80 -27.118228912353516 81 -24.944816589355469
		 82 -23.456514358520508 83 -22.799854278564453 84 -28.452518463134766 85 -35.811298370361328
		 86 -38.956321716308594 87 -42.151802062988281 88 -45.149059295654297 89 -47.284931182861328
		 90 -47.566902160644531 91 -46.115894317626953 92 -43.789939880371094 93 -41.370197296142578
		 94 -39.070507049560547 95 -36.413902282714844 96 -32.939792633056641 97 -28.502838134765625
		 98 -24.015575408935547 99 -20.559431076049805 100 -19.671648025512695 101 -21.459199905395508
		 102 -24.672065734863281 103 -28.411529541015625 104 -31.714488983154297 105 -33.807903289794922
		 106 -35.153488159179687 107 -36.493583679199219 108 -37.332103729248047 109 -37.499763488769531
		 110 -37.270648956298828 111 -36.810825347900391 112 -36.286247253417969 113 -35.572135925292969
		 114 -34.638389587402344 115 -33.790252685546875 116 -33.329750061035156 117 -33.412235260009766
		 118 -33.850803375244141 119 -34.432788848876953 120 -34.944370269775391;
createNode animCurveTA -n "Character1_RightHandRing2_rotateZ";
	rename -uid "D9248F6E-4FE1-B8C9-E551-8189CF1AF8BF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 29.386693954467773 1 29.873292922973629
		 2 30.369016647338871 3 30.849456787109375 4 31.289962768554687 5 31.886758804321289
		 6 32.587261199951172 7 32.988132476806641 8 32.682022094726563 9 31.131626129150387
		 10 28.720527648925785 11 26.389913558959961 12 25.065093994140625 13 25.176847457885742
		 14 26.164115905761719 15 27.480161666870117 16 28.581655502319336 17 29.478242874145504
		 18 30.410549163818363 19 31.178560256958004 20 31.578872680664063 21 31.504749298095707
		 22 31.0809211730957 23 30.450027465820313 24 29.753471374511719 25 28.832309722900387
		 26 27.678314208984375 27 26.65437126159668 28 26.122882843017578 29 26.215347290039063
		 30 26.683000564575195 31 28.156980514526367 32 30.931341171264645 33 34.326416015625
		 34 37.769199371337891 35 40.884918212890625 36 45.61981201171875 37 51.838211059570312
		 38 53.339260101318359 39 48.447463989257813 40 42.358478546142578 41 36.047859191894531
		 42 29.758344650268555 43 23.897476196289063 44 19.244001388549805 45 16.851545333862305
		 46 17.488433837890625 47 20.362173080444336 48 24.327251434326172 49 28.350883483886719
		 50 31.844236373901371 51 34.530185699462891 52 35.844013214111328 53 35.824924468994141
		 54 34.791664123535156 55 32.990200042724609 56 31.054327011108398 57 29.578353881835934
		 58 28.970979690551761 59 29.094118118286133 60 29.714109420776364 61 30.301651000976563
		 62 30.381759643554691 63 30.197065353393558 64 30.065332412719727 65 30.093339920043945
		 66 30.159513473510742 67 30.238544464111332 68 30.305097579956055 69 30.358682632446289
		 70 30.410718917846676 71 30.453716278076168 72 30.480178833007816 73 30.471223831176754
		 74 30.436473846435547 75 30.411184310913086 76 29.987514495849609 77 28.918701171874996
		 78 27.422412872314453 79 25.683933258056641 80 23.884256362915039 81 22.311908721923828
		 82 21.218107223510742 83 20.730476379394531 84 24.83714485168457 85 29.988599777221683
		 86 32.173999786376953 87 34.4130859375 88 36.551155090332031 89 38.109931945800781
		 90 38.318500518798828 91 37.252429962158203 92 35.575668334960938 93 33.862476348876953
		 94 32.253559112548828 95 30.406881332397461 96 27.992290496826172 97 24.872919082641602
		 98 21.630769729614258 99 19.040576934814453 100 18.358583450317383 101 19.72437858581543
		 102 22.112575531005859 103 24.807994842529297 104 27.136587142944336 105 28.596797943115234
		 106 29.531917572021488 107 30.462194442749023 108 31.044404983520508 109 31.160867691040039
		 110 31.001722335815433 111 30.682424545288082 112 30.318271636962891 113 29.822578430175781
		 114 29.17414665222168 115 28.584518432617188 116 28.263988494873047 117 28.32142448425293
		 118 28.626644134521484 119 29.031282424926754 120 29.386693954467773;
createNode animCurveTU -n "Character1_RightHandRing2_visibility";
	rename -uid "53796DFE-4310-0FB7-F1A4-C1B9CEA57878";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandRing3_translateX";
	rename -uid "FCB9762B-4EFC-E409-EAA4-B6B861FCC118";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.5035178661346436;
createNode animCurveTL -n "Character1_RightHandRing3_translateY";
	rename -uid "A83DEFEC-4A9A-ADE6-1B84-8699D7050BAB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.9153993129730225;
createNode animCurveTL -n "Character1_RightHandRing3_translateZ";
	rename -uid "18A3CC68-43CC-A500-1211-E1A94575D764";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.6898331642150879;
createNode animCurveTU -n "Character1_RightHandRing3_scaleX";
	rename -uid "44EF8E06-4FC2-76C2-E158-5286B096678C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing3_scaleY";
	rename -uid "EE9F81B2-411F-6FC7-8B53-A4A313F41FB7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing3_scaleZ";
	rename -uid "597DF1DF-414D-AEDE-E1CB-39A1C8232BE2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandRing3_rotateX";
	rename -uid "1568B8B6-4AD9-2B05-420B-9D8FA10C3023";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.0564198166539427e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateY";
	rename -uid "0C6997F5-40C1-0D91-8B1E-E799896A6307";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.7061626067516045e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateZ";
	rename -uid "FF2DFC36-4E08-CE70-73B4-77A56AD6ED8C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.9649631610718643e-007;
createNode animCurveTU -n "Character1_RightHandRing3_visibility";
	rename -uid "0FF534A4-453B-20EE-C03A-109BCB55A5A3";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_Neck_translateX";
	rename -uid "E86EAAA3-4B50-5674-8B2E-AF91615AF25B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 12.791294097900391;
createNode animCurveTL -n "Character1_Neck_translateY";
	rename -uid "DEF5451A-4616-2802-57D0-2695D3F54650";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.3580474853515625;
createNode animCurveTL -n "Character1_Neck_translateZ";
	rename -uid "10205C5D-4C6B-A851-DF93-B5A34E3A1FD0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.6322288513183594;
createNode animCurveTU -n "Character1_Neck_scaleX";
	rename -uid "1DE494E8-43AF-EB84-DE09-88B859EFBE79";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_Neck_scaleY";
	rename -uid "3E1AB371-41C1-2F45-FC58-E1A819551CB2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Neck_scaleZ";
	rename -uid "3718BFBC-4F7E-4F94-A56A-55B576356C1E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Neck_rotateX";
	rename -uid "404854C8-43B3-2F87-87BE-24B6ED4E24AF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 9.5831975936889648 1 10.375999450683594
		 2 11.310990333557129 3 12.033904075622559 4 12.189940452575684 5 11.260674476623535
		 6 9.5681352615356445 7 8.0148811340332031 8 7.4960689544677734 9 8.6414556503295898
		 10 10.833954811096191 11 13.091142654418945 12 14.421754837036131 13 14.19573974609375
		 14 13.051509857177734 15 11.894915580749512 16 11.394411087036133 17 12.161992073059082
		 18 13.618253707885742 19 14.661716461181639 20 14.185807228088379 21 11.394640922546387
		 22 7.177769660949707 23 2.9468927383422852 24 0.089347712695598602 25 -0.50378084182739258
		 26 0.36212927103042603 27 1.5140606164932251 28 1.7712788581848145 29 0.82382535934448242
		 30 -0.50037068128585815 31 -1.942294716835022 32 -3.4602632522583008 33 -4.5712089538574219
		 34 -4.853248119354248 35 -4.6167922019958496 36 -4.4791660308837891 37 -5.0589137077331543
		 38 -6.8826532363891602 39 -9.4453840255737305 40 -11.822649955749512 41 -13.098799705505371
		 42 -12.620376586914063 43 -10.960955619812012 44 -9.0531854629516602 45 -7.8382167816162109
		 46 -8.3126602172851562 47 -9.5258846282958984 48 -10.597047805786133 49 -10.649284362792969
		 50 -8.9751291275024414 51 -6.2206830978393555 52 -3.5487184524536133 53 -2.1348609924316406
		 54 -2.8569467067718506 55 -4.9284653663635254 56 -7.035733699798584 57 -7.8761329650878906
		 58 -6.6399974822998047 59 -4.2005863189697266 60 -1.7925128936767576 61 -0.86096155643463135
		 62 -2.284320592880249 63 -5.0180010795593262 64 -6.8728432655334473 65 -6.9696407318115234
		 66 -6.3498334884643555 67 -5.7197260856628418 68 -5.7869977951049805 69 -7.1264448165893555
		 70 -9.2469711303710937 71 -11.261397361755371 72 -12.29083251953125 73 -11.719328880310059
		 74 -10.160724639892578 75 -8.5882902145385742 76 -7.2036480903625488 77 -6.8407778739929199
		 78 -6.9361796379089355 79 -6.6631379127502441 80 -5.1981916427612305 81 -1.8872243165969849
		 82 2.5667648315429687 83 6.9412970542907715 84 9.9984302520751953 85 10.797691345214844
		 86 10.072175979614258 87 9.0856180191040039 88 9.0892953872680664 89 10.72463321685791
		 90 13.09730339050293 91 15.327882766723633 92 16.781721115112305 93 16.421262741088867
		 94 15.186703681945803 95 13.649958610534668 96 12.382055282592773 97 11.260327339172363
		 98 10.068856239318848 99 9.2374868392944336 100 9.1962099075317383 101 10.600809097290039
		 102 13.046464920043945 103 15.361725807189941 104 16.360248565673828 105 15.160593986511229
		 106 12.689148902893066 107 10.191060066223145 108 9.0093059539794922 109 9.9550352096557617
		 110 12.167909622192383 111 14.471458435058592 112 15.678897857666016 113 15.264166831970215
		 114 13.93482494354248 115 12.349661827087402 116 11.163918495178223 117 10.551729202270508
		 118 10.168180465698242 119 9.8872509002685547 120 9.5831975936889648;
createNode animCurveTA -n "Character1_Neck_rotateY";
	rename -uid "BA9F1664-4251-3A9E-1042-8EBB64AC2658";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -8.7274017333984375 1 -8.7948598861694336
		 2 -8.8805980682373047 3 -8.9211454391479492 4 -8.8537483215332031 5 -8.5860652923583984
		 6 -8.1814088821411133 7 -7.8077902793884277 8 -7.63006591796875 9 -7.7648973464965811
		 10 -8.0970029830932617 11 -8.4456996917724609 12 -8.6320686340332031 13 -8.5401935577392578
		 14 -8.2909145355224609 15 -8.058135986328125 16 -7.9426670074462882 17 -8.0650644302368164
		 18 -8.3257780075073242 19 -8.5179262161254883 20 -8.416132926940918 21 -7.8375635147094727
		 22 -6.9428801536560059 23 -6.0189671516418457 24 -5.377985954284668 25 -5.2440876960754395
		 26 -5.4513521194458008 27 -5.7374191284179687 28 -5.8327341079711914 29 -5.6652488708496094
		 30 -5.4233036041259766 31 -4.9988441467285156 32 -4.6209583282470703 33 -4.387300968170166
		 34 -4.3805837631225586 35 -4.5222077369689941 36 -4.6692676544189453 37 -4.6778011322021484
		 38 -4.4215612411499023 39 -3.9947638511657719 40 -3.5866899490356445 41 -3.3960645198822021
		 42 -3.5690100193023682 43 -3.9621870517730713 44 -4.3484797477722168 45 -4.5080204010009766
		 46 -4.4638423919677734 47 -4.2523555755615234 48 -4.0795464515686035 49 -4.1548519134521484
		 50 -4.6491212844848633 51 -5.3967766761779785 52 -6.1115059852600098 53 -6.516657829284668
		 54 -6.4084773063659668 55 -5.9718837738037109 56 -5.5145483016967773 57 -5.3497858047485352
		 58 -5.6728248596191406 59 -6.2665953636169434 60 -6.8372740745544434 61 -7.1895675659179679
		 62 -6.9714107513427734 63 -6.4199838638305664 64 -6.0320801734924316 65 -6.0158877372741699
		 66 -6.14044189453125 67 -6.2539410591125488 68 -6.2053565979003906 69 -5.8728256225585937
		 70 -5.3633661270141602 71 -4.8716320991516113 72 -4.599604606628418 73 -4.696098804473877
		 74 -5.0233678817749023 75 -5.3613743782043457 76 -5.5930233001708984 77 -5.4641227722167969
		 78 -5.1232175827026367 79 -4.7859005928039551 80 -4.6652369499206543 81 -4.9268255233764648
		 82 -5.4099850654602051 83 -5.8495759963989258 84 -6.0019211769104004 85 -5.7039608955383301
		 86 -5.1508383750915527 87 -4.6271061897277832 88 -4.3932681083679199 89 -4.5741453170776367
		 90 -4.9915571212768555 91 -5.4580254554748535 92 -5.8980450630187988 93 -6.0883955955505371
		 94 -6.1939263343811035 95 -6.3054585456848145 96 -6.5132346153259277 97 -6.7713627815246582
		 98 -7.019780158996582 99 -7.3278822898864737 100 -7.7658090591430655 101 -8.4464740753173828
		 102 -9.2676448822021484 103 -9.9786767959594727 104 -10.326991081237793 105 -10.118395805358887
		 106 -9.6222305297851562 107 -9.1114082336425781 108 -8.8545074462890625 109 -9.016444206237793
		 110 -9.4260416030883789 111 -9.8518552780151367 112 -10.061182022094727 113 -9.9496793746948242
		 114 -9.657954216003418 115 -9.3176803588867187 116 -9.0603542327880859 117 -8.9230031967163086
		 118 -8.8382940292358398 119 -8.7813949584960937 120 -8.7274017333984375;
createNode animCurveTA -n "Character1_Neck_rotateZ";
	rename -uid "7B163E66-4714-B3A8-1E15-069C44113C29";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 0.27945694327354431 1 -0.0033648286480456591
		 2 -0.32150349020957947 3 -0.62687289714813232 4 -0.87154746055603027 5 -0.98224836587905884
		 6 -0.98078984022140503 7 -0.9751090407371521 8 -1.0988640785217285 9 -1.4545348882675171
		 10 -1.9400027990341184 11 -2.3995652198791504 12 -2.6979079246520996 13 -2.7492303848266602
		 14 -2.633256196975708 15 -2.478344202041626 16 -2.4099535942077637 17 -2.5716962814331055
		 18 -2.8389768600463867 19 -3.0144567489624023 20 -2.9494078159332275 21 -2.5691597461700439
		 22 -2.0102889537811279 23 -1.4602410793304443 24 -1.1000514030456543 25 -1.0255038738250732
		 26 -1.0967923402786255 27 -1.1680848598480225 28 -1.1511629819869995 29 -1.0567288398742676
		 30 -0.97092455625534069 31 -0.80638480186462402 32 -0.60128355026245117 33 -0.41327518224716187
		 34 -0.28807768225669861 35 -0.1953757256269455 36 -0.083029963076114655 37 0.091196693480014801
		 38 0.3531515896320343 39 0.66648775339126587 40 0.96845757961273193 41 1.1828130483627319
		 42 1.2542121410369873 43 1.2328814268112183 44 1.1837837696075439 45 1.1462740898132324
		 46 1.200898289680481 47 1.2984886169433594 48 1.3855212926864624 49 1.3972481489181519
		 50 1.2728506326675415 51 1.0694257020950317 52 0.88264203071594238 53 0.78865116834640503
		 54 0.8465690016746521 55 1.0196037292480469 56 1.2171344757080078 57 1.3105117082595825
		 58 1.2035319805145264 59 0.98604279756546021 60 0.78278493881225586 61 0.71457701921463013
		 62 0.81754511594772339 63 1.0231404304504395 64 1.1530768871307373 65 1.1205724477767944
		 66 1.0231362581253052 67 0.92246657609939564 68 0.87444502115249634 69 0.92004549503326416
		 70 1.0382212400436401 71 1.1787093877792358 72 1.272057056427002 73 1.268965482711792
		 74 1.2346246242523193 75 1.2550100088119507 76 1.1870523691177368 77 1.1945503950119019
		 78 1.2632554769515991 79 1.3525495529174805 80 1.390605092048645 81 1.2866393327713013
		 82 1.0919985771179199 83 0.93117082118988037 84 0.93488442897796631 85 1.2227907180786133
		 86 1.7291483879089355 87 2.2745969295501709 88 2.6148104667663574 89 2.5789327621459961
		 90 2.2822577953338623 91 1.9338570833206175 92 1.6349673271179199 93 1.5394076108932495
		 94 1.5194873809814453 95 1.5028122663497925 96 1.4111027717590332 97 1.2694388628005981
		 98 1.1273716688156128 99 0.93373382091522228 100 0.63441306352615356 101 0.14040401577949524
		 102 -0.45743212103843689 103 -0.95772957801818837 104 -1.1859041452407837 105 -1.0148969888687134
		 106 -0.64339888095855713 107 -0.24082221090793607 108 -0.027259010821580887 109 -0.14697565138339996
		 110 -0.45184150338172907 111 -0.74868452548980713 112 -0.87168127298355103 113 -0.75859212875366211
		 114 -0.51236176490783691 115 -0.22857837378978729 116 -0.009251810610294342 117 0.11211010813713074
		 118 0.18705503642559052 119 0.2359110414981842 120 0.27945694327354431;
createNode animCurveTU -n "Character1_Neck_visibility";
	rename -uid "2EE21706-4DB5-6B88-8C39-ED97E1E3E442";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_Head_translateX";
	rename -uid "FDCD63BF-4069-9CCC-4683-7E8CBF554DAD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.627285957336426;
createNode animCurveTL -n "Character1_Head_translateY";
	rename -uid "4EC5AE64-4838-390D-6148-F39C7EE63961";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.64882755279541;
createNode animCurveTL -n "Character1_Head_translateZ";
	rename -uid "8EB2FC19-4E86-52C6-CCBA-8EBBC21B96D2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.1324095726013184;
createNode animCurveTU -n "Character1_Head_scaleX";
	rename -uid "30CEC742-4E37-527D-9793-0DA2A5D981DE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Head_scaleY";
	rename -uid "87989ACB-4689-4F00-18CD-889919C87A1F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Head_scaleZ";
	rename -uid "1E985A60-4AF7-67B8-2B68-79984A7ED244";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_Head_rotateX";
	rename -uid "2A30EB99-420B-8C21-ADD4-D9B2AE01399B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 5.3095211982727051 1 5.3453793525695801
		 2 5.3809070587158203 3 5.422755241394043 4 5.4753513336181641 5 5.5459194183349609
		 6 5.6312003135681152 7 5.7159829139709473 8 5.7783117294311523 9 5.8033418655395508
		 10 5.8125576972961426 11 5.8297562599182129 12 5.8634791374206543 13 5.9118590354919434
		 14 5.9693140983581543 15 6.0288572311401367 16 6.0752983093261719 17 6.0929121971130371
		 18 6.0945215225219727 19 6.0973119735717773 20 6.1090874671936035 21 6.1431698799133301
		 22 6.2126221656799316 23 6.3066473007202148 24 6.3776321411132812 25 6.3748540878295898
		 26 6.3229207992553711 27 6.2688512802124023 28 6.2534985542297363 29 6.2913374900817871
		 30 6.3652582168579102 31 6.4630246162414551 32 6.5654993057250977 33 6.6473546028137207
		 34 6.6822562217712402 35 6.6849226951599121 36 6.6891684532165527 37 6.7282929420471191
		 38 6.8342876434326172 39 6.9910168647766113 40 7.1509284973144531 41 7.2498788833618173
		 42 7.2370681762695321 43 7.1587367057800293 44 7.083742618560791 45 7.0658073425292969
		 46 7.134119987487793 47 7.2559599876403809 48 7.3810343742370597 49 7.4512252807617187
		 50 7.4225430488586417 51 7.3474888801574716 52 7.2961826324462891 53 7.3122777938842782
		 54 7.4189248085021982 55 7.5872025489807129 56 7.7641077041625977 57 7.8737058639526367
		 58 7.8617582321166983 59 7.7864575386047372 60 7.7172064781188956 61 7.7104945182800293
		 62 7.8147187232971191 63 7.9985785484313965 64 8.1641721725463867 65 8.2436599731445313
		 66 8.2812442779541016 67 8.3103036880493164 68 8.3638629913330078 69 8.4725818634033203
		 70 8.6146078109741211 71 8.7327737808227539 72 8.7493410110473633 73 8.6039133071899414
		 74 8.3470869064331055 75 8.0522031784057617 76 7.7489109039306641 77 7.444232940673829
		 78 7.1108670234680176 79 6.7148780822753906 80 6.2266998291015625 81 5.6381702423095703
		 82 5.01092529296875 83 4.405667781829834 84 3.8602874279022217 85 3.404052734375
		 86 3.0323619842529297 87 2.7232739925384521 88 2.4445099830627441 89 2.1825723648071289
		 90 1.9821056127548218 91 1.8853594064712522 92 1.913304805755615 93 2.0812981128692627
		 94 2.3441650867462158 95 2.67043137550354 96 3.030911922454834 97 3.4165186882019043
		 98 3.8206326961517334 99 4.2198338508605957 100 4.591280460357666 101 4.9103226661682129
		 102 5.1849617958068848 103 5.4312710762023926 104 5.6435065269470215 105 5.8051257133483887
		 106 5.9260916709899902 107 6.021815299987793 108 6.0680022239685059 109 6.0443325042724609
		 110 5.9780898094177246 111 5.8996338844299316 112 5.822573184967041 113 5.7490425109863281
		 114 5.6791095733642578 115 5.6135621070861816 116 5.5461935997009277 117 5.4744386672973633
		 118 5.4069633483886719 119 5.3498044013977051 120 5.3095211982727051;
createNode animCurveTA -n "Character1_Head_rotateY";
	rename -uid "C609E3A0-44D7-5D11-2394-C280DE00A5C2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 11.555663108825684 1 12.380659103393555
		 2 13.398021697998047 3 14.241464614868164 4 14.544805526733398 5 13.777573585510254
		 6 12.245626449584961 7 10.837047576904297 8 10.43941593170166 9 11.675359725952148
		 10 13.908376693725586 11 16.137153625488281 12 17.360498428344727 13 16.939872741699219
		 14 15.499301910400391 15 13.931391716003418 16 13.107930183410645 17 13.760477066040039
		 18 15.26841926574707 19 16.489873886108398 20 16.283012390136719 21 13.810713768005371
		 22 9.9144420623779297 23 5.9701142311096191 24 3.3529024124145508 25 2.9460239410400391
		 26 3.8982560634613033 27 4.9824261665344238 28 4.970940113067627 29 3.5191104412078857
		 30 1.4300804138183594 31 -0.8745388388633728 32 -2.9965379238128662 33 -4.4773063659667969
		 34 -4.9203009605407715 35 -4.6733870506286621 36 -4.394683837890625 37 -4.742246150970459
		 38 -6.2814569473266602 39 -8.5379409790039063 40 -10.610587120056152 41 -11.599411964416504
		 42 -10.868841171264648 43 -9.0382585525512695 44 -7.0908780097961426 45 -6.0104470252990723
		 46 -6.3780937194824219 47 -7.5209197998046866 48 -8.5422039031982422 49 -8.5461235046386719
		 50 -6.8099589347839355 51 -3.9958992004394531 52 -1.2862849235534668 53 0.13606841862201691
		 54 -0.60732656717300415 55 -2.7199218273162842 56 -4.8727822303771973 57 -5.7389373779296875
		 58 -4.4901676177978516 59 -2.0158064365386963 60 0.43349301815032959 61 1.5818836688995361
		 62 0.17216788232326508 63 -2.7109842300415039 64 -4.8259263038635254 65 -5.244408130645752
		 66 -4.9817929267883301 67 -4.7142157554626465 68 -5.1174864768981934 69 -6.7336564064025879
		 70 -9.0360565185546875 71 -11.098069190979004 72 -11.994219779968262 73 -11.06148624420166
		 74 -8.8861923217773437 75 -6.4179224967956543 76 -4.5891227722167969 77 -4.0156435966491699
		 78 -4.0968031883239746 79 -3.9619100093841553 80 -2.7371292114257813 81 0.28183543682098389
		 82 4.4104299545288086 83 8.4379568099975586 84 11.149353981018066 85 11.632868766784668
		 86 10.662205696105957 87 9.5505790710449219 88 9.6129474639892578 89 11.546304702758789
		 90 14.469056129455568 91 17.388017654418945 92 19.345781326293945 93 19.316179275512695
		 94 18.271251678466797 95 16.808181762695313 96 15.523815155029297 97 14.313107490539551
		 98 12.976402282714844 99 11.967540740966797 100 11.739405632019043 101 12.967878341674805
		 102 15.254144668579102 103 17.440086364746094 104 18.369770050048828 105 17.193525314331055
		 106 14.693945884704588 107 12.176059722900391 108 10.985199928283691 109 11.938071250915527
		 110 14.165430068969727 111 16.48332405090332 112 17.707176208496094 113 17.31330680847168
		 114 16.003219604492188 115 14.429812431335449 116 13.246341705322266 117 12.627128601074219
		 118 12.224905014038086 119 11.910744667053223 120 11.555663108825684;
createNode animCurveTA -n "Character1_Head_rotateZ";
	rename -uid "47EF6D2A-4A10-0808-8572-029C0D08CD1F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 5.2613430023193359 1 5.1776738166809082
		 2 5.105745792388916 3 5.0120987892150879 4 4.8617210388183594 5 4.6016764640808105
		 6 4.2617201805114746 7 3.9322390556335449 8 3.7072005271911621 9 3.6565837860107422
		 10 3.7207720279693604 11 3.7997918128967285 12 3.7888655662536621 13 3.6175763607025142
		 14 3.3528995513916016 15 3.0934746265411377 16 2.9329385757446289 17 2.9489142894744873
		 18 3.0740678310394287 19 3.1813733577728271 20 3.140873908996582 21 2.8579037189483643
		 22 2.427196741104126 23 1.998684763908386 24 1.7227644920349121 25 1.6999318599700928
		 26 1.8344789743423462 27 1.9879169464111328 28 2.0251979827880859 29 1.9091355800628662
		 30 1.7265384197235107 31 1.5282456874847412 32 1.3630450963973999 33 1.2824963331222534
		 34 1.3337013721466064 35 1.4755866527557373 36 1.628756046295166 37 1.7133052349090576
		 38 1.6574716567993164 39 1.5092663764953613 40 1.3731608390808105 41 1.365267276763916
		 42 1.5726171731948853 43 1.9119460582733154 44 2.2524616718292236 45 2.4699864387512207
		 46 2.4964251518249512 47 2.4170606136322021 48 2.343505859375 49 2.3953874111175537
		 50 2.6718375682830811 51 3.0835578441619873 52 3.4760897159576416 53 3.7004098892211919
		 54 3.6421914100646973 55 3.3956849575042725 56 3.1305534839630127 57 3.0322399139404297
		 58 3.2248857021331787 59 3.5851767063140869 60 3.9392490386962886 61 4.117619514465332
		 62 3.9504935741424556 63 3.5744588375091553 64 3.2957947254180908 65 3.2556924819946289
		 66 3.3129348754882813 67 3.3686454296112061 68 3.3217916488647461 69 3.0878863334655762
		 70 2.7384119033813477 71 2.4118549823760986 72 2.2574827671051025 73 2.3810834884643555
		 74 2.6775403022766113 75 2.9896931648254395 76 3.1761581897735596 77 3.1628963947296143
		 78 3.0549027919769287 79 2.9777159690856934 80 3.0317566394805908 81 3.2642092704772949
		 82 3.5641157627105713 83 3.8077826499938965 84 3.9203016757965088 85 3.8857378959655757
		 86 3.7982773780822754 87 3.7445452213287354 88 3.7749686241149907 89 3.8934128284454346
		 90 4.047142505645752 91 4.2073297500610352 92 4.3546295166015625 93 4.4523935317993164
		 94 4.5213780403137207 95 4.5712404251098633 96 4.6225261688232422 97 4.6683239936828613
		 98 4.6935896873474121 99 4.7276878356933594 100 4.8126254081726074 101 5.0209617614746094
		 102 5.3374381065368652 103 5.6577587127685547 104 5.8497281074523926 105 5.7977461814880371
		 106 5.5778346061706543 107 5.3391427993774414 108 5.2321557998657227 109 5.3494381904602051
		 110 5.596954345703125 111 5.8456134796142578 112 5.9679970741271973 113 5.9118142127990723
		 114 5.7587099075317383 115 5.581876277923584 116 5.4491696357727051 117 5.3766961097717285
		 118 5.3292570114135742 119 5.2946510314941406 120 5.2613430023193359;
createNode animCurveTU -n "Character1_Head_visibility";
	rename -uid "15CD3689-4F8C-EDF3-CC0F-DFAD91BB1E90";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "eyes_translateX";
	rename -uid "8B1BABE9-4759-7BB9-9767-60933F67E0A9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 30.947149276733398;
createNode animCurveTL -n "eyes_translateY";
	rename -uid "2501A181-4783-FD6B-F603-B29ADB38B6FF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.0992542810490704e-007;
createNode animCurveTL -n "eyes_translateZ";
	rename -uid "552D867B-457C-388D-BCEE-05A41A5DB47C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.121469657048256e-013;
createNode animCurveTU -n "eyes_scaleX";
	rename -uid "1EC83E49-49C5-7FCA-3F84-C5BC58FCE3BB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleY";
	rename -uid "DB470893-41FA-B762-12E9-B18F92ABD901";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleZ";
	rename -uid "DE66E6B1-4BDA-DD47-8B20-E48BD47A61EC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "eyes_rotateX";
	rename -uid "27BC5492-4D76-FEFD-0327-D786B30601E0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "eyes_rotateY";
	rename -uid "320A7F50-44C2-84B1-4FE4-90A60085C8B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "eyes_rotateZ";
	rename -uid "393E5110-4D6B-9F56-A88D-54894C71E6D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "eyes_visibility";
	rename -uid "8B15F9A5-4A73-4D52-6B2C-2AA0F0B08BCA";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "jaw_translateX";
	rename -uid "64BD4084-440E-10A6-C6F5-65A3FF3571D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.07279486209154129;
createNode animCurveTL -n "jaw_translateY";
	rename -uid "D6E5CE0F-4A8C-20B7-A8F2-A2B99F31E55F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.25670051574707;
createNode animCurveTL -n "jaw_translateZ";
	rename -uid "17B34DE1-49CC-6638-0CCB-8BB982D38855";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.2238426557523781e-007;
createNode animCurveTU -n "jaw_scaleX";
	rename -uid "99C3A2BF-48BA-457B-9E85-449B19AF1235";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleY";
	rename -uid "FA9BAE1F-468A-6E67-885C-77B08499489C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleZ";
	rename -uid "2B709259-42ED-2DFF-4FD3-2E8F54340210";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "jaw_rotateX";
	rename -uid "61781B8B-415E-690E-046E-94A8A71B619A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -52.716041564941406 1 -51.38946533203125
		 2 -49.901596069335938 3 -48.4786376953125 4 -47.361724853515625 5 -46.797073364257813
		 6 -47.061004638671875 7 -47.969268798828125 8 -49.094406127929688 9 -50.054946899414062
		 10 -50.497344970703125 11 -50.252960205078125 12 -49.537002563476562 13 -48.57818603515625
		 14 -47.656295776367188 15 -47.124740600585938 16 -47.1527099609375 17 -47.524429321289063
		 18 -48.064254760742188 19 -48.599929809570313 20 -48.973190307617188 21 -49.116531372070313
		 22 -49.157928466796875 23 -49.234634399414063 24 -49.462722778320313 25 -49.9171142578125
		 26 -50.78826904296875 27 -51.949996948242187 28 -53.026588439941406 29 -53.725311279296875
		 30 -53.785118103027344 31 -52.796806335449219 32 -50.825225830078125 33 -48.363449096679688
		 34 -45.9801025390625 35 -44.361679077148438 36 -43.9705810546875 37 -44.488479614257812
		 38 -45.490493774414063 39 -46.459365844726563 40 -46.874282836914063 41 -46.55938720703125
		 42 -45.7724609375 43 -44.769027709960938 44 -43.852310180664063 45 -43.389068603515625
		 46 -43.538253784179688 47 -44.067047119140625 48 -44.766204833984375 49 -45.441848754882813
		 50 -45.925735473632813 51 -46.140884399414063 52 -46.220703125 53 -46.313934326171875
		 54 -46.5482177734375 55 -47.0421142578125 56 -48.024734497070313 57 -49.359954833984375
		 58 -50.6773681640625 59 -51.691299438476563 60 -52.083038330078125 61 -51.503829956054688
		 62 -50.0986328125 63 -48.313247680664062 64 -46.66943359375 65 -45.78338623046875
		 66 -46.028091430664062 67 -47.000152587890625 68 -48.220046997070313 69 -49.269439697265625
		 70 -49.754928588867188 71 -49.51043701171875 72 -48.783157348632813 73 -47.80889892578125
		 74 -46.87567138671875 75 -46.34991455078125 76 -46.416015625 77 -46.863128662109375
		 78 -47.496856689453125 79 -48.134674072265625 80 -48.6163330078125 81 -48.880126953125
		 82 -49.058212280273438 83 -49.280670166015625 84 -49.645416259765625 85 -50.204025268554687
		 86 -51.085540771484375 87 -51.858230590820313 88 -52.128494262695313 89 -51.884780883789063
		 90 -51.01898193359375 91 -49.47125244140625 92 -47.62506103515625 93 -45.95001220703125
		 94 -44.558929443359375 95 -43.3468017578125 96 -42.1490478515625 97 -40.735336303710938
		 98 -39.293869018554687 99 -38.123046875 100 -37.627029418945313 101 -38.121307373046875
		 102 -39.315719604492188 103 -40.751373291015625 104 -42.076095581054688 105 -43.040847778320313
		 106 -43.574066162109375 107 -43.889968872070313 108 -44.235214233398438 109 -45.087203979492188
		 110 -46.70611572265625 111 -48.8851318359375 112 -51.11199951171875 113 -52.981620788574219
		 114 -53.705436706542969 115 -54.104965209960938 116 -54.129264831542969 117 -53.894744873046875
		 118 -53.507545471191406 119 -53.075714111328125 120 -52.716041564941406;
createNode animCurveTA -n "jaw_rotateY";
	rename -uid "DDB3FA46-4771-5ECA-B28B-1582333B0F0D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -120.80289459228517 1 -120.55987548828125
		 2 -120.22948455810547 3 -119.95140075683595 4 -119.86224365234374 5 -120.09637451171875
		 6 -120.9054718017578 7 -122.19786071777342 8 -123.58930969238283 9 -124.69168090820313
		 10 -125.11661529541016 11 -124.5543212890625 12 -123.2700958251953 13 -121.7377471923828
		 14 -120.42115783691408 15 -119.77429962158205 16 -120.07334136962892 17 -121.00544738769531
		 18 -122.14952850341797 19 -123.0852737426758 20 -123.39236450195312 21 -122.73474884033203
		 22 -121.39367675781251 23 -119.87374114990234 24 -118.67996978759766 25 -118.31884765625
		 26 -119.20272827148437 27 -121.00051879882813 28 -123.10140991210937 29 -124.88661193847655
		 30 -125.73398590087891 31 -125.35767364501955 32 -124.23070526123048 33 -122.89479064941405
		 34 -121.87847137451172 35 -121.68858337402344 36 -122.58834838867186 37 -124.14849090576172
		 38 -125.82876586914062 39 -127.18642425537109 40 -127.80372619628906 41 -127.36882019042969
		 42 -126.21266174316405 43 -124.79962158203125 44 -123.58915710449219 45 -123.03205871582031
		 46 -123.40288543701172 47 -124.395263671875 48 -125.58446502685548 49 -126.54476928710937
		 50 -126.84957885742187 51 -126.16288757324219 52 -124.77122497558594 53 -123.18980407714844
		 54 -121.93228149414062 55 -121.50004577636719 56 -122.28945159912111 57 -123.95899963378906
		 58 -125.87680053710937 59 -127.36021423339844 60 -127.83047485351562 61 -126.94291687011719
		 62 -125.14494323730467 63 -123.03898620605469 64 -121.21546936035156 65 -120.25971984863281
		 66 -120.53280639648437 67 -121.69491577148439 68 -123.19815826416014 69 -124.46873474121094
		 70 -124.99498748779298 71 -124.45732116699219 72 -123.18999481201172 73 -121.65950775146484
		 74 -120.33416748046875 75 -119.67543029785158 76 -119.96652221679686 77 -120.9080810546875
		 78 -122.07339477539062 79 -123.03353881835936 80 -123.359619140625 81 -122.71095275878905
		 82 -121.37525939941408 83 -119.86598968505859 84 -118.69517517089842 85 -118.37072753906251
		 86 -119.21764373779297 87 -120.47421264648437 88 -121.38681793212892 89 -121.71405792236327
		 90 -121.22276306152342 91 -120.11085510253905 92 -118.97972869873045 93 -118.14670562744141
		 94 -117.51252746582031 95 -116.82081604003906 96 -115.85665893554686 97 -114.57304382324219
		 98 -113.28623962402344 99 -112.30084991455078 100 -111.90962982177734 101 -112.37206268310547
		 102 -113.48424530029297 103 -114.84144592285158 104 -116.0326690673828 105 -116.63848876953126
		 106 -116.31857299804689 107 -115.33740997314453 108 -114.19235229492187 109 -113.65019226074219
		 110 -114.36937713623047 111 -116.57662963867187 112 -119.7631378173828 113 -123.14592742919922
		 114 -124.92650604248047 115 -125.81185913085937 116 -125.60290527343751 117 -124.69862365722656
		 118 -123.39797210693358 119 -121.9999542236328 120 -120.80289459228517;
createNode animCurveTA -n "jaw_rotateZ";
	rename -uid "AEB6F60A-4E1F-4C8B-222F-E9B80BCD8368";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -44.638351440429687 1 -45.339828491210937
		 2 -46.134872436523438 3 -46.898941040039062 4 -47.49127197265625 5 -47.7672119140625
		 6 -47.534576416015625 7 -46.92144775390625 8 -46.242019653320313 9 -45.746658325195313
		 10 -45.642578125 11 -46.062911987304688 12 -46.893081665039062 13 -47.955642700195313
		 14 -49.002899169921875 15 -49.6917724609375 16 -49.871017456054687 17 -49.742034912109375
		 18 -49.44671630859375 19 -49.113632202148438 20 -48.843841552734375 21 -48.65093994140625
		 22 -48.45147705078125 23 -48.188995361328125 24 -47.806243896484375 25 -47.27197265625
		 26 -46.4552001953125 27 -45.466400146484375 28 -44.601943969726563 29 -44.039886474609375
		 30 -43.890579223632812 31 -44.095108032226563 32 -44.48712158203125 33 -44.969482421875
		 34 -45.3355712890625 35 -45.219619750976562 36 -44.36541748046875 37 -43.128936767578125
		 38 -41.992660522460938 39 -41.207489013671875 40 -40.895736694335938 41 -41.202590942382813
		 42 -41.964462280273438 43 -42.967132568359375 44 -43.930572509765625 45 -44.490097045898438
		 46 -44.498641967773437 47 -44.183609008789063 48 -43.726364135742187 49 -43.280242919921875
		 50 -42.959915161132813 51 -42.8001708984375 52 -42.712417602539063 53 -42.622406005859375
		 54 -42.456985473632813 55 -42.134048461914062 56 -41.474472045898438 57 -40.59930419921875
		 58 -39.8675537109375 59 -39.636962890625 60 -40.018447875976563 61 -41.126556396484375
		 62 -42.830459594726563 63 -44.828277587890625 64 -46.717437744140625 65 -47.980377197265625
		 66 -48.24847412109375 67 -47.745208740234375 68 -46.973907470703125 69 -46.404693603515625
		 70 -46.276077270507813 71 -46.712158203125 72 -47.565505981445313 73 -48.646224975585937
		 74 -49.7008056640625 75 -50.377761840820312 76 -50.508834838867188 77 -50.303207397460938
		 78 -49.919677734375 79 -49.492691040039063 80 -49.122650146484375 81 -48.81634521484375
		 82 -48.488754272460938 83 -48.090301513671875 84 -47.58184814453125 85 -46.952774047851562
		 86 -46.109954833984375 87 -45.51617431640625 88 -45.545425415039063 89 -46.10870361328125
		 90 -47.180877685546875 91 -48.686431884765625 92 -50.258285522460937 93 -51.480636596679688
		 94 -52.369613647460937 95 -53.1944580078125 96 -54.158607482910156 97 -55.439163208007813
		 98 -56.817939758300781 99 -57.951728820800781 100 -58.379829406738281 101 -57.804634094238281
		 102 -56.565071105957031 103 -55.104461669921875 104 -53.745368957519531 105 -52.689369201660156
		 106 -51.9661865234375 107 -51.406723022460938 108 -50.840805053710938 109 -49.997970581054688
		 110 -48.727828979492188 111 -47.140731811523438 112 -45.628082275390625 113 -44.470062255859375
		 114 -43.904815673828125 115 -43.595245361328125 116 -43.553863525390625 117 -43.711318969726563
		 118 -43.999557495117188 119 -44.341506958007813 120 -44.638351440429687;
createNode animCurveTU -n "jaw_visibility";
	rename -uid "99C223ED-4BD0-F8C1-F3A5-1398F95BE350";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 70 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "F1A1C447-4F03-FDE6-B2BD-B99C31ABBF05";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "7F570A82-4A97-1DD8-79C6-658E0B88F450";
createNode displayLayer -n "defaultLayer";
	rename -uid "22A02E70-484C-CB23-9C1B-C0BE1891A500";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "DCE40516-4FDD-300B-95C1-1CA084AE1FFB";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "B65EE597-48E9-9094-3897-A28692B61FE8";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "E487D178-466E-6AF6-6E79-46B7FB5EF032";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1636\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1636\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1233\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1233\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 19 100 -ps 2 81 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1233\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1233\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "04A2B52C-4DBE-AB5B-96D0-81B2DEBC8EA4";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 120 -ast 0 -aet 250 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 0;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
connectAttr "ref_frame.s" "Character1_Hips.is";
connectAttr "Character1_Hips_scaleX.o" "Character1_Hips.sx";
connectAttr "Character1_Hips_scaleY.o" "Character1_Hips.sy";
connectAttr "Character1_Hips_scaleZ.o" "Character1_Hips.sz";
connectAttr "Character1_Hips_translateX.o" "Character1_Hips.tx";
connectAttr "Character1_Hips_translateY.o" "Character1_Hips.ty";
connectAttr "Character1_Hips_translateZ.o" "Character1_Hips.tz";
connectAttr "Character1_Hips_rotateX.o" "Character1_Hips.rx";
connectAttr "Character1_Hips_rotateY.o" "Character1_Hips.ry";
connectAttr "Character1_Hips_rotateZ.o" "Character1_Hips.rz";
connectAttr "Character1_Hips_visibility.o" "Character1_Hips.v";
connectAttr "Character1_Hips.s" "Character1_LeftUpLeg.is";
connectAttr "Character1_LeftUpLeg_scaleX.o" "Character1_LeftUpLeg.sx";
connectAttr "Character1_LeftUpLeg_scaleY.o" "Character1_LeftUpLeg.sy";
connectAttr "Character1_LeftUpLeg_scaleZ.o" "Character1_LeftUpLeg.sz";
connectAttr "Character1_LeftUpLeg_translateX.o" "Character1_LeftUpLeg.tx";
connectAttr "Character1_LeftUpLeg_translateY.o" "Character1_LeftUpLeg.ty";
connectAttr "Character1_LeftUpLeg_translateZ.o" "Character1_LeftUpLeg.tz";
connectAttr "Character1_LeftUpLeg_rotateX.o" "Character1_LeftUpLeg.rx";
connectAttr "Character1_LeftUpLeg_rotateY.o" "Character1_LeftUpLeg.ry";
connectAttr "Character1_LeftUpLeg_rotateZ.o" "Character1_LeftUpLeg.rz";
connectAttr "Character1_LeftUpLeg_visibility.o" "Character1_LeftUpLeg.v";
connectAttr "Character1_LeftUpLeg.s" "Character1_LeftLeg.is";
connectAttr "Character1_LeftLeg_scaleX.o" "Character1_LeftLeg.sx";
connectAttr "Character1_LeftLeg_scaleY.o" "Character1_LeftLeg.sy";
connectAttr "Character1_LeftLeg_scaleZ.o" "Character1_LeftLeg.sz";
connectAttr "Character1_LeftLeg_translateX.o" "Character1_LeftLeg.tx";
connectAttr "Character1_LeftLeg_translateY.o" "Character1_LeftLeg.ty";
connectAttr "Character1_LeftLeg_translateZ.o" "Character1_LeftLeg.tz";
connectAttr "Character1_LeftLeg_rotateX.o" "Character1_LeftLeg.rx";
connectAttr "Character1_LeftLeg_rotateY.o" "Character1_LeftLeg.ry";
connectAttr "Character1_LeftLeg_rotateZ.o" "Character1_LeftLeg.rz";
connectAttr "Character1_LeftLeg_visibility.o" "Character1_LeftLeg.v";
connectAttr "Character1_LeftLeg.s" "Character1_LeftFoot.is";
connectAttr "Character1_LeftFoot_scaleX.o" "Character1_LeftFoot.sx";
connectAttr "Character1_LeftFoot_scaleY.o" "Character1_LeftFoot.sy";
connectAttr "Character1_LeftFoot_scaleZ.o" "Character1_LeftFoot.sz";
connectAttr "Character1_LeftFoot_translateX.o" "Character1_LeftFoot.tx";
connectAttr "Character1_LeftFoot_translateY.o" "Character1_LeftFoot.ty";
connectAttr "Character1_LeftFoot_translateZ.o" "Character1_LeftFoot.tz";
connectAttr "Character1_LeftFoot_rotateX.o" "Character1_LeftFoot.rx";
connectAttr "Character1_LeftFoot_rotateY.o" "Character1_LeftFoot.ry";
connectAttr "Character1_LeftFoot_rotateZ.o" "Character1_LeftFoot.rz";
connectAttr "Character1_LeftFoot_visibility.o" "Character1_LeftFoot.v";
connectAttr "Character1_LeftFoot.s" "Character1_LeftToeBase.is";
connectAttr "Character1_LeftToeBase_scaleX.o" "Character1_LeftToeBase.sx";
connectAttr "Character1_LeftToeBase_scaleY.o" "Character1_LeftToeBase.sy";
connectAttr "Character1_LeftToeBase_scaleZ.o" "Character1_LeftToeBase.sz";
connectAttr "Character1_LeftToeBase_translateX.o" "Character1_LeftToeBase.tx";
connectAttr "Character1_LeftToeBase_translateY.o" "Character1_LeftToeBase.ty";
connectAttr "Character1_LeftToeBase_translateZ.o" "Character1_LeftToeBase.tz";
connectAttr "Character1_LeftToeBase_rotateX.o" "Character1_LeftToeBase.rx";
connectAttr "Character1_LeftToeBase_rotateY.o" "Character1_LeftToeBase.ry";
connectAttr "Character1_LeftToeBase_rotateZ.o" "Character1_LeftToeBase.rz";
connectAttr "Character1_LeftToeBase_visibility.o" "Character1_LeftToeBase.v";
connectAttr "Character1_LeftToeBase.s" "Character1_LeftFootMiddle1.is";
connectAttr "Character1_LeftFootMiddle1_scaleX.o" "Character1_LeftFootMiddle1.sx"
		;
connectAttr "Character1_LeftFootMiddle1_scaleY.o" "Character1_LeftFootMiddle1.sy"
		;
connectAttr "Character1_LeftFootMiddle1_scaleZ.o" "Character1_LeftFootMiddle1.sz"
		;
connectAttr "Character1_LeftFootMiddle1_translateX.o" "Character1_LeftFootMiddle1.tx"
		;
connectAttr "Character1_LeftFootMiddle1_translateY.o" "Character1_LeftFootMiddle1.ty"
		;
connectAttr "Character1_LeftFootMiddle1_translateZ.o" "Character1_LeftFootMiddle1.tz"
		;
connectAttr "Character1_LeftFootMiddle1_rotateX.o" "Character1_LeftFootMiddle1.rx"
		;
connectAttr "Character1_LeftFootMiddle1_rotateY.o" "Character1_LeftFootMiddle1.ry"
		;
connectAttr "Character1_LeftFootMiddle1_rotateZ.o" "Character1_LeftFootMiddle1.rz"
		;
connectAttr "Character1_LeftFootMiddle1_visibility.o" "Character1_LeftFootMiddle1.v"
		;
connectAttr "Character1_LeftFootMiddle1.s" "Character1_LeftFootMiddle2.is";
connectAttr "Character1_LeftFootMiddle2_translateX.o" "Character1_LeftFootMiddle2.tx"
		;
connectAttr "Character1_LeftFootMiddle2_translateY.o" "Character1_LeftFootMiddle2.ty"
		;
connectAttr "Character1_LeftFootMiddle2_translateZ.o" "Character1_LeftFootMiddle2.tz"
		;
connectAttr "Character1_LeftFootMiddle2_scaleX.o" "Character1_LeftFootMiddle2.sx"
		;
connectAttr "Character1_LeftFootMiddle2_scaleY.o" "Character1_LeftFootMiddle2.sy"
		;
connectAttr "Character1_LeftFootMiddle2_scaleZ.o" "Character1_LeftFootMiddle2.sz"
		;
connectAttr "Character1_LeftFootMiddle2_rotateX.o" "Character1_LeftFootMiddle2.rx"
		;
connectAttr "Character1_LeftFootMiddle2_rotateY.o" "Character1_LeftFootMiddle2.ry"
		;
connectAttr "Character1_LeftFootMiddle2_rotateZ.o" "Character1_LeftFootMiddle2.rz"
		;
connectAttr "Character1_LeftFootMiddle2_visibility.o" "Character1_LeftFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_RightUpLeg.is";
connectAttr "Character1_RightUpLeg_scaleX.o" "Character1_RightUpLeg.sx";
connectAttr "Character1_RightUpLeg_scaleY.o" "Character1_RightUpLeg.sy";
connectAttr "Character1_RightUpLeg_scaleZ.o" "Character1_RightUpLeg.sz";
connectAttr "Character1_RightUpLeg_translateX.o" "Character1_RightUpLeg.tx";
connectAttr "Character1_RightUpLeg_translateY.o" "Character1_RightUpLeg.ty";
connectAttr "Character1_RightUpLeg_translateZ.o" "Character1_RightUpLeg.tz";
connectAttr "Character1_RightUpLeg_rotateX.o" "Character1_RightUpLeg.rx";
connectAttr "Character1_RightUpLeg_rotateY.o" "Character1_RightUpLeg.ry";
connectAttr "Character1_RightUpLeg_rotateZ.o" "Character1_RightUpLeg.rz";
connectAttr "Character1_RightUpLeg_visibility.o" "Character1_RightUpLeg.v";
connectAttr "Character1_RightUpLeg.s" "Character1_RightLeg.is";
connectAttr "Character1_RightLeg_scaleX.o" "Character1_RightLeg.sx";
connectAttr "Character1_RightLeg_scaleY.o" "Character1_RightLeg.sy";
connectAttr "Character1_RightLeg_scaleZ.o" "Character1_RightLeg.sz";
connectAttr "Character1_RightLeg_translateX.o" "Character1_RightLeg.tx";
connectAttr "Character1_RightLeg_translateY.o" "Character1_RightLeg.ty";
connectAttr "Character1_RightLeg_translateZ.o" "Character1_RightLeg.tz";
connectAttr "Character1_RightLeg_rotateX.o" "Character1_RightLeg.rx";
connectAttr "Character1_RightLeg_rotateY.o" "Character1_RightLeg.ry";
connectAttr "Character1_RightLeg_rotateZ.o" "Character1_RightLeg.rz";
connectAttr "Character1_RightLeg_visibility.o" "Character1_RightLeg.v";
connectAttr "Character1_RightLeg.s" "Character1_RightFoot.is";
connectAttr "Character1_RightFoot_scaleX.o" "Character1_RightFoot.sx";
connectAttr "Character1_RightFoot_scaleY.o" "Character1_RightFoot.sy";
connectAttr "Character1_RightFoot_scaleZ.o" "Character1_RightFoot.sz";
connectAttr "Character1_RightFoot_translateX.o" "Character1_RightFoot.tx";
connectAttr "Character1_RightFoot_translateY.o" "Character1_RightFoot.ty";
connectAttr "Character1_RightFoot_translateZ.o" "Character1_RightFoot.tz";
connectAttr "Character1_RightFoot_rotateX.o" "Character1_RightFoot.rx";
connectAttr "Character1_RightFoot_rotateY.o" "Character1_RightFoot.ry";
connectAttr "Character1_RightFoot_rotateZ.o" "Character1_RightFoot.rz";
connectAttr "Character1_RightFoot_visibility.o" "Character1_RightFoot.v";
connectAttr "Character1_RightFoot.s" "Character1_RightToeBase.is";
connectAttr "Character1_RightToeBase_scaleX.o" "Character1_RightToeBase.sx";
connectAttr "Character1_RightToeBase_scaleY.o" "Character1_RightToeBase.sy";
connectAttr "Character1_RightToeBase_scaleZ.o" "Character1_RightToeBase.sz";
connectAttr "Character1_RightToeBase_translateX.o" "Character1_RightToeBase.tx";
connectAttr "Character1_RightToeBase_translateY.o" "Character1_RightToeBase.ty";
connectAttr "Character1_RightToeBase_translateZ.o" "Character1_RightToeBase.tz";
connectAttr "Character1_RightToeBase_rotateX.o" "Character1_RightToeBase.rx";
connectAttr "Character1_RightToeBase_rotateY.o" "Character1_RightToeBase.ry";
connectAttr "Character1_RightToeBase_rotateZ.o" "Character1_RightToeBase.rz";
connectAttr "Character1_RightToeBase_visibility.o" "Character1_RightToeBase.v";
connectAttr "Character1_RightToeBase.s" "Character1_RightFootMiddle1.is";
connectAttr "Character1_RightFootMiddle1_scaleX.o" "Character1_RightFootMiddle1.sx"
		;
connectAttr "Character1_RightFootMiddle1_scaleY.o" "Character1_RightFootMiddle1.sy"
		;
connectAttr "Character1_RightFootMiddle1_scaleZ.o" "Character1_RightFootMiddle1.sz"
		;
connectAttr "Character1_RightFootMiddle1_translateX.o" "Character1_RightFootMiddle1.tx"
		;
connectAttr "Character1_RightFootMiddle1_translateY.o" "Character1_RightFootMiddle1.ty"
		;
connectAttr "Character1_RightFootMiddle1_translateZ.o" "Character1_RightFootMiddle1.tz"
		;
connectAttr "Character1_RightFootMiddle1_rotateX.o" "Character1_RightFootMiddle1.rx"
		;
connectAttr "Character1_RightFootMiddle1_rotateY.o" "Character1_RightFootMiddle1.ry"
		;
connectAttr "Character1_RightFootMiddle1_rotateZ.o" "Character1_RightFootMiddle1.rz"
		;
connectAttr "Character1_RightFootMiddle1_visibility.o" "Character1_RightFootMiddle1.v"
		;
connectAttr "Character1_RightFootMiddle1.s" "Character1_RightFootMiddle2.is";
connectAttr "Character1_RightFootMiddle2_translateX.o" "Character1_RightFootMiddle2.tx"
		;
connectAttr "Character1_RightFootMiddle2_translateY.o" "Character1_RightFootMiddle2.ty"
		;
connectAttr "Character1_RightFootMiddle2_translateZ.o" "Character1_RightFootMiddle2.tz"
		;
connectAttr "Character1_RightFootMiddle2_scaleX.o" "Character1_RightFootMiddle2.sx"
		;
connectAttr "Character1_RightFootMiddle2_scaleY.o" "Character1_RightFootMiddle2.sy"
		;
connectAttr "Character1_RightFootMiddle2_scaleZ.o" "Character1_RightFootMiddle2.sz"
		;
connectAttr "Character1_RightFootMiddle2_rotateX.o" "Character1_RightFootMiddle2.rx"
		;
connectAttr "Character1_RightFootMiddle2_rotateY.o" "Character1_RightFootMiddle2.ry"
		;
connectAttr "Character1_RightFootMiddle2_rotateZ.o" "Character1_RightFootMiddle2.rz"
		;
connectAttr "Character1_RightFootMiddle2_visibility.o" "Character1_RightFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_Spine.is";
connectAttr "Character1_Spine_scaleX.o" "Character1_Spine.sx";
connectAttr "Character1_Spine_scaleY.o" "Character1_Spine.sy";
connectAttr "Character1_Spine_scaleZ.o" "Character1_Spine.sz";
connectAttr "Character1_Spine_translateX.o" "Character1_Spine.tx";
connectAttr "Character1_Spine_translateY.o" "Character1_Spine.ty";
connectAttr "Character1_Spine_translateZ.o" "Character1_Spine.tz";
connectAttr "Character1_Spine_rotateX.o" "Character1_Spine.rx";
connectAttr "Character1_Spine_rotateY.o" "Character1_Spine.ry";
connectAttr "Character1_Spine_rotateZ.o" "Character1_Spine.rz";
connectAttr "Character1_Spine_visibility.o" "Character1_Spine.v";
connectAttr "Character1_Spine.s" "Character1_Spine1.is";
connectAttr "Character1_Spine1_scaleX.o" "Character1_Spine1.sx";
connectAttr "Character1_Spine1_scaleY.o" "Character1_Spine1.sy";
connectAttr "Character1_Spine1_scaleZ.o" "Character1_Spine1.sz";
connectAttr "Character1_Spine1_translateX.o" "Character1_Spine1.tx";
connectAttr "Character1_Spine1_translateY.o" "Character1_Spine1.ty";
connectAttr "Character1_Spine1_translateZ.o" "Character1_Spine1.tz";
connectAttr "Character1_Spine1_rotateX.o" "Character1_Spine1.rx";
connectAttr "Character1_Spine1_rotateY.o" "Character1_Spine1.ry";
connectAttr "Character1_Spine1_rotateZ.o" "Character1_Spine1.rz";
connectAttr "Character1_Spine1_visibility.o" "Character1_Spine1.v";
connectAttr "Character1_Spine1.s" "Character1_LeftShoulder.is";
connectAttr "Character1_LeftShoulder_scaleX.o" "Character1_LeftShoulder.sx";
connectAttr "Character1_LeftShoulder_scaleY.o" "Character1_LeftShoulder.sy";
connectAttr "Character1_LeftShoulder_scaleZ.o" "Character1_LeftShoulder.sz";
connectAttr "Character1_LeftShoulder_translateX.o" "Character1_LeftShoulder.tx";
connectAttr "Character1_LeftShoulder_translateY.o" "Character1_LeftShoulder.ty";
connectAttr "Character1_LeftShoulder_translateZ.o" "Character1_LeftShoulder.tz";
connectAttr "Character1_LeftShoulder_rotateX.o" "Character1_LeftShoulder.rx";
connectAttr "Character1_LeftShoulder_rotateY.o" "Character1_LeftShoulder.ry";
connectAttr "Character1_LeftShoulder_rotateZ.o" "Character1_LeftShoulder.rz";
connectAttr "Character1_LeftShoulder_visibility.o" "Character1_LeftShoulder.v";
connectAttr "Character1_LeftShoulder.s" "Character1_LeftArm.is";
connectAttr "Character1_LeftArm_scaleX.o" "Character1_LeftArm.sx";
connectAttr "Character1_LeftArm_scaleY.o" "Character1_LeftArm.sy";
connectAttr "Character1_LeftArm_scaleZ.o" "Character1_LeftArm.sz";
connectAttr "Character1_LeftArm_translateX.o" "Character1_LeftArm.tx";
connectAttr "Character1_LeftArm_translateY.o" "Character1_LeftArm.ty";
connectAttr "Character1_LeftArm_translateZ.o" "Character1_LeftArm.tz";
connectAttr "Character1_LeftArm_rotateX.o" "Character1_LeftArm.rx";
connectAttr "Character1_LeftArm_rotateY.o" "Character1_LeftArm.ry";
connectAttr "Character1_LeftArm_rotateZ.o" "Character1_LeftArm.rz";
connectAttr "Character1_LeftArm_visibility.o" "Character1_LeftArm.v";
connectAttr "Character1_LeftArm.s" "Character1_LeftForeArm.is";
connectAttr "Character1_LeftForeArm_scaleX.o" "Character1_LeftForeArm.sx";
connectAttr "Character1_LeftForeArm_scaleY.o" "Character1_LeftForeArm.sy";
connectAttr "Character1_LeftForeArm_scaleZ.o" "Character1_LeftForeArm.sz";
connectAttr "Character1_LeftForeArm_translateX.o" "Character1_LeftForeArm.tx";
connectAttr "Character1_LeftForeArm_translateY.o" "Character1_LeftForeArm.ty";
connectAttr "Character1_LeftForeArm_translateZ.o" "Character1_LeftForeArm.tz";
connectAttr "Character1_LeftForeArm_rotateX.o" "Character1_LeftForeArm.rx";
connectAttr "Character1_LeftForeArm_rotateY.o" "Character1_LeftForeArm.ry";
connectAttr "Character1_LeftForeArm_rotateZ.o" "Character1_LeftForeArm.rz";
connectAttr "Character1_LeftForeArm_visibility.o" "Character1_LeftForeArm.v";
connectAttr "Character1_LeftForeArm.s" "Character1_LeftHand.is";
connectAttr "Character1_LeftHand_lockInfluenceWeights.o" "Character1_LeftHand.liw"
		;
connectAttr "Character1_LeftHand_scaleX.o" "Character1_LeftHand.sx";
connectAttr "Character1_LeftHand_scaleY.o" "Character1_LeftHand.sy";
connectAttr "Character1_LeftHand_scaleZ.o" "Character1_LeftHand.sz";
connectAttr "Character1_LeftHand_translateX.o" "Character1_LeftHand.tx";
connectAttr "Character1_LeftHand_translateY.o" "Character1_LeftHand.ty";
connectAttr "Character1_LeftHand_translateZ.o" "Character1_LeftHand.tz";
connectAttr "Character1_LeftHand_rotateX.o" "Character1_LeftHand.rx";
connectAttr "Character1_LeftHand_rotateY.o" "Character1_LeftHand.ry";
connectAttr "Character1_LeftHand_rotateZ.o" "Character1_LeftHand.rz";
connectAttr "Character1_LeftHand_visibility.o" "Character1_LeftHand.v";
connectAttr "Character1_LeftHand.s" "Character1_LeftHandThumb1.is";
connectAttr "Character1_LeftHandThumb1_scaleX.o" "Character1_LeftHandThumb1.sx";
connectAttr "Character1_LeftHandThumb1_scaleY.o" "Character1_LeftHandThumb1.sy";
connectAttr "Character1_LeftHandThumb1_scaleZ.o" "Character1_LeftHandThumb1.sz";
connectAttr "Character1_LeftHandThumb1_translateX.o" "Character1_LeftHandThumb1.tx"
		;
connectAttr "Character1_LeftHandThumb1_translateY.o" "Character1_LeftHandThumb1.ty"
		;
connectAttr "Character1_LeftHandThumb1_translateZ.o" "Character1_LeftHandThumb1.tz"
		;
connectAttr "Character1_LeftHandThumb1_rotateX.o" "Character1_LeftHandThumb1.rx"
		;
connectAttr "Character1_LeftHandThumb1_rotateY.o" "Character1_LeftHandThumb1.ry"
		;
connectAttr "Character1_LeftHandThumb1_rotateZ.o" "Character1_LeftHandThumb1.rz"
		;
connectAttr "Character1_LeftHandThumb1_visibility.o" "Character1_LeftHandThumb1.v"
		;
connectAttr "Character1_LeftHandThumb1.s" "Character1_LeftHandThumb2.is";
connectAttr "Character1_LeftHandThumb2_scaleX.o" "Character1_LeftHandThumb2.sx";
connectAttr "Character1_LeftHandThumb2_scaleY.o" "Character1_LeftHandThumb2.sy";
connectAttr "Character1_LeftHandThumb2_scaleZ.o" "Character1_LeftHandThumb2.sz";
connectAttr "Character1_LeftHandThumb2_translateX.o" "Character1_LeftHandThumb2.tx"
		;
connectAttr "Character1_LeftHandThumb2_translateY.o" "Character1_LeftHandThumb2.ty"
		;
connectAttr "Character1_LeftHandThumb2_translateZ.o" "Character1_LeftHandThumb2.tz"
		;
connectAttr "Character1_LeftHandThumb2_rotateX.o" "Character1_LeftHandThumb2.rx"
		;
connectAttr "Character1_LeftHandThumb2_rotateY.o" "Character1_LeftHandThumb2.ry"
		;
connectAttr "Character1_LeftHandThumb2_rotateZ.o" "Character1_LeftHandThumb2.rz"
		;
connectAttr "Character1_LeftHandThumb2_visibility.o" "Character1_LeftHandThumb2.v"
		;
connectAttr "Character1_LeftHandThumb2.s" "Character1_LeftHandThumb3.is";
connectAttr "Character1_LeftHandThumb3_translateX.o" "Character1_LeftHandThumb3.tx"
		;
connectAttr "Character1_LeftHandThumb3_translateY.o" "Character1_LeftHandThumb3.ty"
		;
connectAttr "Character1_LeftHandThumb3_translateZ.o" "Character1_LeftHandThumb3.tz"
		;
connectAttr "Character1_LeftHandThumb3_scaleX.o" "Character1_LeftHandThumb3.sx";
connectAttr "Character1_LeftHandThumb3_scaleY.o" "Character1_LeftHandThumb3.sy";
connectAttr "Character1_LeftHandThumb3_scaleZ.o" "Character1_LeftHandThumb3.sz";
connectAttr "Character1_LeftHandThumb3_rotateX.o" "Character1_LeftHandThumb3.rx"
		;
connectAttr "Character1_LeftHandThumb3_rotateY.o" "Character1_LeftHandThumb3.ry"
		;
connectAttr "Character1_LeftHandThumb3_rotateZ.o" "Character1_LeftHandThumb3.rz"
		;
connectAttr "Character1_LeftHandThumb3_visibility.o" "Character1_LeftHandThumb3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandIndex1.is";
connectAttr "Character1_LeftHandIndex1_scaleX.o" "Character1_LeftHandIndex1.sx";
connectAttr "Character1_LeftHandIndex1_scaleY.o" "Character1_LeftHandIndex1.sy";
connectAttr "Character1_LeftHandIndex1_scaleZ.o" "Character1_LeftHandIndex1.sz";
connectAttr "Character1_LeftHandIndex1_translateX.o" "Character1_LeftHandIndex1.tx"
		;
connectAttr "Character1_LeftHandIndex1_translateY.o" "Character1_LeftHandIndex1.ty"
		;
connectAttr "Character1_LeftHandIndex1_translateZ.o" "Character1_LeftHandIndex1.tz"
		;
connectAttr "Character1_LeftHandIndex1_rotateX.o" "Character1_LeftHandIndex1.rx"
		;
connectAttr "Character1_LeftHandIndex1_rotateY.o" "Character1_LeftHandIndex1.ry"
		;
connectAttr "Character1_LeftHandIndex1_rotateZ.o" "Character1_LeftHandIndex1.rz"
		;
connectAttr "Character1_LeftHandIndex1_visibility.o" "Character1_LeftHandIndex1.v"
		;
connectAttr "Character1_LeftHandIndex1.s" "Character1_LeftHandIndex2.is";
connectAttr "Character1_LeftHandIndex2_scaleX.o" "Character1_LeftHandIndex2.sx";
connectAttr "Character1_LeftHandIndex2_scaleY.o" "Character1_LeftHandIndex2.sy";
connectAttr "Character1_LeftHandIndex2_scaleZ.o" "Character1_LeftHandIndex2.sz";
connectAttr "Character1_LeftHandIndex2_translateX.o" "Character1_LeftHandIndex2.tx"
		;
connectAttr "Character1_LeftHandIndex2_translateY.o" "Character1_LeftHandIndex2.ty"
		;
connectAttr "Character1_LeftHandIndex2_translateZ.o" "Character1_LeftHandIndex2.tz"
		;
connectAttr "Character1_LeftHandIndex2_rotateX.o" "Character1_LeftHandIndex2.rx"
		;
connectAttr "Character1_LeftHandIndex2_rotateY.o" "Character1_LeftHandIndex2.ry"
		;
connectAttr "Character1_LeftHandIndex2_rotateZ.o" "Character1_LeftHandIndex2.rz"
		;
connectAttr "Character1_LeftHandIndex2_visibility.o" "Character1_LeftHandIndex2.v"
		;
connectAttr "Character1_LeftHandIndex2.s" "Character1_LeftHandIndex3.is";
connectAttr "Character1_LeftHandIndex3_translateX.o" "Character1_LeftHandIndex3.tx"
		;
connectAttr "Character1_LeftHandIndex3_translateY.o" "Character1_LeftHandIndex3.ty"
		;
connectAttr "Character1_LeftHandIndex3_translateZ.o" "Character1_LeftHandIndex3.tz"
		;
connectAttr "Character1_LeftHandIndex3_scaleX.o" "Character1_LeftHandIndex3.sx";
connectAttr "Character1_LeftHandIndex3_scaleY.o" "Character1_LeftHandIndex3.sy";
connectAttr "Character1_LeftHandIndex3_scaleZ.o" "Character1_LeftHandIndex3.sz";
connectAttr "Character1_LeftHandIndex3_rotateX.o" "Character1_LeftHandIndex3.rx"
		;
connectAttr "Character1_LeftHandIndex3_rotateY.o" "Character1_LeftHandIndex3.ry"
		;
connectAttr "Character1_LeftHandIndex3_rotateZ.o" "Character1_LeftHandIndex3.rz"
		;
connectAttr "Character1_LeftHandIndex3_visibility.o" "Character1_LeftHandIndex3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandRing1.is";
connectAttr "Character1_LeftHandRing1_scaleX.o" "Character1_LeftHandRing1.sx";
connectAttr "Character1_LeftHandRing1_scaleY.o" "Character1_LeftHandRing1.sy";
connectAttr "Character1_LeftHandRing1_scaleZ.o" "Character1_LeftHandRing1.sz";
connectAttr "Character1_LeftHandRing1_translateX.o" "Character1_LeftHandRing1.tx"
		;
connectAttr "Character1_LeftHandRing1_translateY.o" "Character1_LeftHandRing1.ty"
		;
connectAttr "Character1_LeftHandRing1_translateZ.o" "Character1_LeftHandRing1.tz"
		;
connectAttr "Character1_LeftHandRing1_rotateX.o" "Character1_LeftHandRing1.rx";
connectAttr "Character1_LeftHandRing1_rotateY.o" "Character1_LeftHandRing1.ry";
connectAttr "Character1_LeftHandRing1_rotateZ.o" "Character1_LeftHandRing1.rz";
connectAttr "Character1_LeftHandRing1_visibility.o" "Character1_LeftHandRing1.v"
		;
connectAttr "Character1_LeftHandRing1.s" "Character1_LeftHandRing2.is";
connectAttr "Character1_LeftHandRing2_scaleX.o" "Character1_LeftHandRing2.sx";
connectAttr "Character1_LeftHandRing2_scaleY.o" "Character1_LeftHandRing2.sy";
connectAttr "Character1_LeftHandRing2_scaleZ.o" "Character1_LeftHandRing2.sz";
connectAttr "Character1_LeftHandRing2_translateX.o" "Character1_LeftHandRing2.tx"
		;
connectAttr "Character1_LeftHandRing2_translateY.o" "Character1_LeftHandRing2.ty"
		;
connectAttr "Character1_LeftHandRing2_translateZ.o" "Character1_LeftHandRing2.tz"
		;
connectAttr "Character1_LeftHandRing2_rotateX.o" "Character1_LeftHandRing2.rx";
connectAttr "Character1_LeftHandRing2_rotateY.o" "Character1_LeftHandRing2.ry";
connectAttr "Character1_LeftHandRing2_rotateZ.o" "Character1_LeftHandRing2.rz";
connectAttr "Character1_LeftHandRing2_visibility.o" "Character1_LeftHandRing2.v"
		;
connectAttr "Character1_LeftHandRing2.s" "Character1_LeftHandRing3.is";
connectAttr "Character1_LeftHandRing3_translateX.o" "Character1_LeftHandRing3.tx"
		;
connectAttr "Character1_LeftHandRing3_translateY.o" "Character1_LeftHandRing3.ty"
		;
connectAttr "Character1_LeftHandRing3_translateZ.o" "Character1_LeftHandRing3.tz"
		;
connectAttr "Character1_LeftHandRing3_scaleX.o" "Character1_LeftHandRing3.sx";
connectAttr "Character1_LeftHandRing3_scaleY.o" "Character1_LeftHandRing3.sy";
connectAttr "Character1_LeftHandRing3_scaleZ.o" "Character1_LeftHandRing3.sz";
connectAttr "Character1_LeftHandRing3_rotateX.o" "Character1_LeftHandRing3.rx";
connectAttr "Character1_LeftHandRing3_rotateY.o" "Character1_LeftHandRing3.ry";
connectAttr "Character1_LeftHandRing3_rotateZ.o" "Character1_LeftHandRing3.rz";
connectAttr "Character1_LeftHandRing3_visibility.o" "Character1_LeftHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_RightShoulder.is";
connectAttr "Character1_RightShoulder_scaleX.o" "Character1_RightShoulder.sx";
connectAttr "Character1_RightShoulder_scaleY.o" "Character1_RightShoulder.sy";
connectAttr "Character1_RightShoulder_scaleZ.o" "Character1_RightShoulder.sz";
connectAttr "Character1_RightShoulder_translateX.o" "Character1_RightShoulder.tx"
		;
connectAttr "Character1_RightShoulder_translateY.o" "Character1_RightShoulder.ty"
		;
connectAttr "Character1_RightShoulder_translateZ.o" "Character1_RightShoulder.tz"
		;
connectAttr "Character1_RightShoulder_rotateX.o" "Character1_RightShoulder.rx";
connectAttr "Character1_RightShoulder_rotateY.o" "Character1_RightShoulder.ry";
connectAttr "Character1_RightShoulder_rotateZ.o" "Character1_RightShoulder.rz";
connectAttr "Character1_RightShoulder_visibility.o" "Character1_RightShoulder.v"
		;
connectAttr "Character1_RightShoulder.s" "Character1_RightArm.is";
connectAttr "Character1_RightArm_scaleX.o" "Character1_RightArm.sx";
connectAttr "Character1_RightArm_scaleY.o" "Character1_RightArm.sy";
connectAttr "Character1_RightArm_scaleZ.o" "Character1_RightArm.sz";
connectAttr "Character1_RightArm_translateX.o" "Character1_RightArm.tx";
connectAttr "Character1_RightArm_translateY.o" "Character1_RightArm.ty";
connectAttr "Character1_RightArm_translateZ.o" "Character1_RightArm.tz";
connectAttr "Character1_RightArm_rotateX.o" "Character1_RightArm.rx";
connectAttr "Character1_RightArm_rotateY.o" "Character1_RightArm.ry";
connectAttr "Character1_RightArm_rotateZ.o" "Character1_RightArm.rz";
connectAttr "Character1_RightArm_visibility.o" "Character1_RightArm.v";
connectAttr "Character1_RightArm.s" "Character1_RightForeArm.is";
connectAttr "Character1_RightForeArm_scaleX.o" "Character1_RightForeArm.sx";
connectAttr "Character1_RightForeArm_scaleY.o" "Character1_RightForeArm.sy";
connectAttr "Character1_RightForeArm_scaleZ.o" "Character1_RightForeArm.sz";
connectAttr "Character1_RightForeArm_translateX.o" "Character1_RightForeArm.tx";
connectAttr "Character1_RightForeArm_translateY.o" "Character1_RightForeArm.ty";
connectAttr "Character1_RightForeArm_translateZ.o" "Character1_RightForeArm.tz";
connectAttr "Character1_RightForeArm_rotateX.o" "Character1_RightForeArm.rx";
connectAttr "Character1_RightForeArm_rotateY.o" "Character1_RightForeArm.ry";
connectAttr "Character1_RightForeArm_rotateZ.o" "Character1_RightForeArm.rz";
connectAttr "Character1_RightForeArm_visibility.o" "Character1_RightForeArm.v";
connectAttr "Character1_RightForeArm.s" "Character1_RightHand.is";
connectAttr "Character1_RightHand_scaleX.o" "Character1_RightHand.sx";
connectAttr "Character1_RightHand_scaleY.o" "Character1_RightHand.sy";
connectAttr "Character1_RightHand_scaleZ.o" "Character1_RightHand.sz";
connectAttr "Character1_RightHand_translateX.o" "Character1_RightHand.tx";
connectAttr "Character1_RightHand_translateY.o" "Character1_RightHand.ty";
connectAttr "Character1_RightHand_translateZ.o" "Character1_RightHand.tz";
connectAttr "Character1_RightHand_rotateX.o" "Character1_RightHand.rx";
connectAttr "Character1_RightHand_rotateY.o" "Character1_RightHand.ry";
connectAttr "Character1_RightHand_rotateZ.o" "Character1_RightHand.rz";
connectAttr "Character1_RightHand_visibility.o" "Character1_RightHand.v";
connectAttr "Character1_RightHand.s" "Character1_RightHandThumb1.is";
connectAttr "Character1_RightHandThumb1_scaleX.o" "Character1_RightHandThumb1.sx"
		;
connectAttr "Character1_RightHandThumb1_scaleY.o" "Character1_RightHandThumb1.sy"
		;
connectAttr "Character1_RightHandThumb1_scaleZ.o" "Character1_RightHandThumb1.sz"
		;
connectAttr "Character1_RightHandThumb1_translateX.o" "Character1_RightHandThumb1.tx"
		;
connectAttr "Character1_RightHandThumb1_translateY.o" "Character1_RightHandThumb1.ty"
		;
connectAttr "Character1_RightHandThumb1_translateZ.o" "Character1_RightHandThumb1.tz"
		;
connectAttr "Character1_RightHandThumb1_rotateX.o" "Character1_RightHandThumb1.rx"
		;
connectAttr "Character1_RightHandThumb1_rotateY.o" "Character1_RightHandThumb1.ry"
		;
connectAttr "Character1_RightHandThumb1_rotateZ.o" "Character1_RightHandThumb1.rz"
		;
connectAttr "Character1_RightHandThumb1_visibility.o" "Character1_RightHandThumb1.v"
		;
connectAttr "Character1_RightHandThumb1.s" "Character1_RightHandThumb2.is";
connectAttr "Character1_RightHandThumb2_scaleX.o" "Character1_RightHandThumb2.sx"
		;
connectAttr "Character1_RightHandThumb2_scaleY.o" "Character1_RightHandThumb2.sy"
		;
connectAttr "Character1_RightHandThumb2_scaleZ.o" "Character1_RightHandThumb2.sz"
		;
connectAttr "Character1_RightHandThumb2_translateX.o" "Character1_RightHandThumb2.tx"
		;
connectAttr "Character1_RightHandThumb2_translateY.o" "Character1_RightHandThumb2.ty"
		;
connectAttr "Character1_RightHandThumb2_translateZ.o" "Character1_RightHandThumb2.tz"
		;
connectAttr "Character1_RightHandThumb2_rotateX.o" "Character1_RightHandThumb2.rx"
		;
connectAttr "Character1_RightHandThumb2_rotateY.o" "Character1_RightHandThumb2.ry"
		;
connectAttr "Character1_RightHandThumb2_rotateZ.o" "Character1_RightHandThumb2.rz"
		;
connectAttr "Character1_RightHandThumb2_visibility.o" "Character1_RightHandThumb2.v"
		;
connectAttr "Character1_RightHandThumb2.s" "Character1_RightHandThumb3.is";
connectAttr "Character1_RightHandThumb3_translateX.o" "Character1_RightHandThumb3.tx"
		;
connectAttr "Character1_RightHandThumb3_translateY.o" "Character1_RightHandThumb3.ty"
		;
connectAttr "Character1_RightHandThumb3_translateZ.o" "Character1_RightHandThumb3.tz"
		;
connectAttr "Character1_RightHandThumb3_scaleX.o" "Character1_RightHandThumb3.sx"
		;
connectAttr "Character1_RightHandThumb3_scaleY.o" "Character1_RightHandThumb3.sy"
		;
connectAttr "Character1_RightHandThumb3_scaleZ.o" "Character1_RightHandThumb3.sz"
		;
connectAttr "Character1_RightHandThumb3_rotateX.o" "Character1_RightHandThumb3.rx"
		;
connectAttr "Character1_RightHandThumb3_rotateY.o" "Character1_RightHandThumb3.ry"
		;
connectAttr "Character1_RightHandThumb3_rotateZ.o" "Character1_RightHandThumb3.rz"
		;
connectAttr "Character1_RightHandThumb3_visibility.o" "Character1_RightHandThumb3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandIndex1.is";
connectAttr "Character1_RightHandIndex1_scaleX.o" "Character1_RightHandIndex1.sx"
		;
connectAttr "Character1_RightHandIndex1_scaleY.o" "Character1_RightHandIndex1.sy"
		;
connectAttr "Character1_RightHandIndex1_scaleZ.o" "Character1_RightHandIndex1.sz"
		;
connectAttr "Character1_RightHandIndex1_translateX.o" "Character1_RightHandIndex1.tx"
		;
connectAttr "Character1_RightHandIndex1_translateY.o" "Character1_RightHandIndex1.ty"
		;
connectAttr "Character1_RightHandIndex1_translateZ.o" "Character1_RightHandIndex1.tz"
		;
connectAttr "Character1_RightHandIndex1_rotateX.o" "Character1_RightHandIndex1.rx"
		;
connectAttr "Character1_RightHandIndex1_rotateY.o" "Character1_RightHandIndex1.ry"
		;
connectAttr "Character1_RightHandIndex1_rotateZ.o" "Character1_RightHandIndex1.rz"
		;
connectAttr "Character1_RightHandIndex1_visibility.o" "Character1_RightHandIndex1.v"
		;
connectAttr "Character1_RightHandIndex1.s" "Character1_RightHandIndex2.is";
connectAttr "Character1_RightHandIndex2_scaleX.o" "Character1_RightHandIndex2.sx"
		;
connectAttr "Character1_RightHandIndex2_scaleY.o" "Character1_RightHandIndex2.sy"
		;
connectAttr "Character1_RightHandIndex2_scaleZ.o" "Character1_RightHandIndex2.sz"
		;
connectAttr "Character1_RightHandIndex2_translateX.o" "Character1_RightHandIndex2.tx"
		;
connectAttr "Character1_RightHandIndex2_translateY.o" "Character1_RightHandIndex2.ty"
		;
connectAttr "Character1_RightHandIndex2_translateZ.o" "Character1_RightHandIndex2.tz"
		;
connectAttr "Character1_RightHandIndex2_rotateX.o" "Character1_RightHandIndex2.rx"
		;
connectAttr "Character1_RightHandIndex2_rotateY.o" "Character1_RightHandIndex2.ry"
		;
connectAttr "Character1_RightHandIndex2_rotateZ.o" "Character1_RightHandIndex2.rz"
		;
connectAttr "Character1_RightHandIndex2_visibility.o" "Character1_RightHandIndex2.v"
		;
connectAttr "Character1_RightHandIndex2.s" "Character1_RightHandIndex3.is";
connectAttr "Character1_RightHandIndex3_translateX.o" "Character1_RightHandIndex3.tx"
		;
connectAttr "Character1_RightHandIndex3_translateY.o" "Character1_RightHandIndex3.ty"
		;
connectAttr "Character1_RightHandIndex3_translateZ.o" "Character1_RightHandIndex3.tz"
		;
connectAttr "Character1_RightHandIndex3_scaleX.o" "Character1_RightHandIndex3.sx"
		;
connectAttr "Character1_RightHandIndex3_scaleY.o" "Character1_RightHandIndex3.sy"
		;
connectAttr "Character1_RightHandIndex3_scaleZ.o" "Character1_RightHandIndex3.sz"
		;
connectAttr "Character1_RightHandIndex3_rotateX.o" "Character1_RightHandIndex3.rx"
		;
connectAttr "Character1_RightHandIndex3_rotateY.o" "Character1_RightHandIndex3.ry"
		;
connectAttr "Character1_RightHandIndex3_rotateZ.o" "Character1_RightHandIndex3.rz"
		;
connectAttr "Character1_RightHandIndex3_visibility.o" "Character1_RightHandIndex3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandRing1.is";
connectAttr "Character1_RightHandRing1_scaleX.o" "Character1_RightHandRing1.sx";
connectAttr "Character1_RightHandRing1_scaleY.o" "Character1_RightHandRing1.sy";
connectAttr "Character1_RightHandRing1_scaleZ.o" "Character1_RightHandRing1.sz";
connectAttr "Character1_RightHandRing1_translateX.o" "Character1_RightHandRing1.tx"
		;
connectAttr "Character1_RightHandRing1_translateY.o" "Character1_RightHandRing1.ty"
		;
connectAttr "Character1_RightHandRing1_translateZ.o" "Character1_RightHandRing1.tz"
		;
connectAttr "Character1_RightHandRing1_rotateX.o" "Character1_RightHandRing1.rx"
		;
connectAttr "Character1_RightHandRing1_rotateY.o" "Character1_RightHandRing1.ry"
		;
connectAttr "Character1_RightHandRing1_rotateZ.o" "Character1_RightHandRing1.rz"
		;
connectAttr "Character1_RightHandRing1_visibility.o" "Character1_RightHandRing1.v"
		;
connectAttr "Character1_RightHandRing1.s" "Character1_RightHandRing2.is";
connectAttr "Character1_RightHandRing2_scaleX.o" "Character1_RightHandRing2.sx";
connectAttr "Character1_RightHandRing2_scaleY.o" "Character1_RightHandRing2.sy";
connectAttr "Character1_RightHandRing2_scaleZ.o" "Character1_RightHandRing2.sz";
connectAttr "Character1_RightHandRing2_translateX.o" "Character1_RightHandRing2.tx"
		;
connectAttr "Character1_RightHandRing2_translateY.o" "Character1_RightHandRing2.ty"
		;
connectAttr "Character1_RightHandRing2_translateZ.o" "Character1_RightHandRing2.tz"
		;
connectAttr "Character1_RightHandRing2_rotateX.o" "Character1_RightHandRing2.rx"
		;
connectAttr "Character1_RightHandRing2_rotateY.o" "Character1_RightHandRing2.ry"
		;
connectAttr "Character1_RightHandRing2_rotateZ.o" "Character1_RightHandRing2.rz"
		;
connectAttr "Character1_RightHandRing2_visibility.o" "Character1_RightHandRing2.v"
		;
connectAttr "Character1_RightHandRing2.s" "Character1_RightHandRing3.is";
connectAttr "Character1_RightHandRing3_translateX.o" "Character1_RightHandRing3.tx"
		;
connectAttr "Character1_RightHandRing3_translateY.o" "Character1_RightHandRing3.ty"
		;
connectAttr "Character1_RightHandRing3_translateZ.o" "Character1_RightHandRing3.tz"
		;
connectAttr "Character1_RightHandRing3_scaleX.o" "Character1_RightHandRing3.sx";
connectAttr "Character1_RightHandRing3_scaleY.o" "Character1_RightHandRing3.sy";
connectAttr "Character1_RightHandRing3_scaleZ.o" "Character1_RightHandRing3.sz";
connectAttr "Character1_RightHandRing3_rotateX.o" "Character1_RightHandRing3.rx"
		;
connectAttr "Character1_RightHandRing3_rotateY.o" "Character1_RightHandRing3.ry"
		;
connectAttr "Character1_RightHandRing3_rotateZ.o" "Character1_RightHandRing3.rz"
		;
connectAttr "Character1_RightHandRing3_visibility.o" "Character1_RightHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_Neck.is";
connectAttr "Character1_Neck_scaleX.o" "Character1_Neck.sx";
connectAttr "Character1_Neck_scaleY.o" "Character1_Neck.sy";
connectAttr "Character1_Neck_scaleZ.o" "Character1_Neck.sz";
connectAttr "Character1_Neck_translateX.o" "Character1_Neck.tx";
connectAttr "Character1_Neck_translateY.o" "Character1_Neck.ty";
connectAttr "Character1_Neck_translateZ.o" "Character1_Neck.tz";
connectAttr "Character1_Neck_rotateX.o" "Character1_Neck.rx";
connectAttr "Character1_Neck_rotateY.o" "Character1_Neck.ry";
connectAttr "Character1_Neck_rotateZ.o" "Character1_Neck.rz";
connectAttr "Character1_Neck_visibility.o" "Character1_Neck.v";
connectAttr "Character1_Neck.s" "Character1_Head.is";
connectAttr "Character1_Head_scaleX.o" "Character1_Head.sx";
connectAttr "Character1_Head_scaleY.o" "Character1_Head.sy";
connectAttr "Character1_Head_scaleZ.o" "Character1_Head.sz";
connectAttr "Character1_Head_translateX.o" "Character1_Head.tx";
connectAttr "Character1_Head_translateY.o" "Character1_Head.ty";
connectAttr "Character1_Head_translateZ.o" "Character1_Head.tz";
connectAttr "Character1_Head_rotateX.o" "Character1_Head.rx";
connectAttr "Character1_Head_rotateY.o" "Character1_Head.ry";
connectAttr "Character1_Head_rotateZ.o" "Character1_Head.rz";
connectAttr "Character1_Head_visibility.o" "Character1_Head.v";
connectAttr "Character1_Head.s" "eyes.is";
connectAttr "eyes_translateX.o" "eyes.tx";
connectAttr "eyes_translateY.o" "eyes.ty";
connectAttr "eyes_translateZ.o" "eyes.tz";
connectAttr "eyes_scaleX.o" "eyes.sx";
connectAttr "eyes_scaleY.o" "eyes.sy";
connectAttr "eyes_scaleZ.o" "eyes.sz";
connectAttr "eyes_rotateX.o" "eyes.rx";
connectAttr "eyes_rotateY.o" "eyes.ry";
connectAttr "eyes_rotateZ.o" "eyes.rz";
connectAttr "eyes_visibility.o" "eyes.v";
connectAttr "Character1_Head.s" "jaw.is";
connectAttr "jaw_lockInfluenceWeights.o" "jaw.liw";
connectAttr "jaw_translateX.o" "jaw.tx";
connectAttr "jaw_translateY.o" "jaw.ty";
connectAttr "jaw_translateZ.o" "jaw.tz";
connectAttr "jaw_scaleX.o" "jaw.sx";
connectAttr "jaw_scaleY.o" "jaw.sy";
connectAttr "jaw_scaleZ.o" "jaw.sz";
connectAttr "jaw_rotateX.o" "jaw.rx";
connectAttr "jaw_rotateY.o" "jaw.ry";
connectAttr "jaw_rotateZ.o" "jaw.rz";
connectAttr "jaw_visibility.o" "jaw.v";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Emeny@Archer_Idle_Shakes.ma
