//Maya ASCII 2016 scene
//Name: Emeny@Archer_Run.ma
//Last modified: Tue, Nov 24, 2015 12:21:07 PM
//Codeset: 1252
requires maya "2016";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "A3AFFF36-4A72-323F-AC14-72B1EB85B7F9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 330.26970055769959 215.04892576124709 240.4391486136347 ;
	setAttr ".r" -type "double3" -22.538352729602359 51.400000000000006 2.5490132216529868e-015 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "2EC53C28-4165-30C6-8EB9-668DF48D254B";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 461.67060446268999;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "4FAF4BAE-4BB7-FC60-8268-D6A3081C4BF3";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "2B364A9F-434A-AC87-4F5C-AEB9FECFA6BD";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "F161605B-4C1B-5353-2DAD-18ABED3C54EF";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "EA500D99-4209-3563-C2FF-4D92BAFD6220";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "8AF5EFEE-46FE-B999-D5F2-C88707F5D963";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "C288DD64-4541-8FF2-9EAD-19997CC59750";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "ref_frame";
	rename -uid "91E481AA-488E-C3EB-1F23-CDB6AD4A8AD2";
	addAttr -ci true -sn "refFrame" -ln "refFrame" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "Character1_Hips" -p "ref_frame";
	rename -uid "48B2000D-40AE-8F8F-3889-25A50CE2B057";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -19.036789801921167 ;
	setAttr ".ssc" no;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftUpLeg" -p "Character1_Hips";
	rename -uid "C6134D94-41C8-4EFA-B521-5FBAF78D0508";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 90.000000000000014 -12.849604412773132 43.257528613448208 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftLeg" -p "Character1_LeftUpLeg";
	rename -uid "CB9DB95A-4171-671E-411D-34B09C550CF6";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -19.152297252867996 -13.416343813706657 -33.623675304480329 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFoot" -p "Character1_LeftLeg";
	rename -uid "65075BFE-4AFA-102D-37CC-248F2CE1787E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 25.817647449536953 51.040150860538553 -112.05775014674906 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftToeBase" -p "Character1_LeftFoot";
	rename -uid "D2D696F6-4997-74EA-0281-1D84B519CF50";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 43.727116820142825 -29.659481123592514 -4.6723850284209263 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle1" -p "Character1_LeftToeBase";
	rename -uid "4A1F1092-4D27-0E22-AA9B-F99FB2C0D7E1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 165.43734692409092 -20.35908719617909 10.197796850486212 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle2" -p "Character1_LeftFootMiddle1";
	rename -uid "9F512D79-472D-A3D1-9405-EFB733FFA5A7";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 127.90699782306312 -44.626258206212555 -101.65660999147298 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightUpLeg" -p "Character1_Hips";
	rename -uid "207CF2AB-40A6-DC9F-49AA-36B2C0693FEE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 133.25752861344816 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightLeg" -p "Character1_RightUpLeg";
	rename -uid "0BDF8041-4E97-E05F-6F1B-FC83C7CCD924";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -39.772585840344547 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFoot" -p "Character1_RightLeg";
	rename -uid "94C60005-4DEE-AA14-5A41-41B008B03CBC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -53.712356932759 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightToeBase" -p "Character1_RightFoot";
	rename -uid "002882CC-4A3F-6F09-D300-FEA4F706C03D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 2.9245623946004821e-006 53.712356932758929 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle1" -p "Character1_RightToeBase";
	rename -uid "62DA391D-4E22-5E6F-3140-AA8CEB1F5643";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -4.5380835579220506e-006 -2.7209092876563258e-005 
		39.772585840347155 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle2" -p "Character1_RightFootMiddle1";
	rename -uid "EEC43BE3-43A0-72D4-EE9B-CE80AC1CD734";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -2.8539668357113417e-005 0.00011485155389716205 -133.25752861351003 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine" -p "Character1_Hips";
	rename -uid "B171A95D-473A-D7BC-5954-2B91325DBEF7";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 133.25752861344816 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine1" -p "Character1_Spine";
	rename -uid "2BB85223-4E48-EAC4-1427-6489B84304BA";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -34.34645244194413 -26.596597123422644 16.99576897690589 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftShoulder" -p "Character1_Spine1";
	rename -uid "74B4AA2E-4B61-127A-4036-4A99844D67A7";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -166.39102833412767 21.832119962557094 -120.00622813437471 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftArm" -p "Character1_LeftShoulder";
	rename -uid "26B6C873-490A-29E1-5439-C0AC4A4412D7";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -172.11387269090633 -44.88241940154802 -37.940590556196071 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftForeArm" -p "Character1_LeftArm";
	rename -uid "C44B3C84-462C-A255-08AF-9AB8BB6CC55B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -84.022423618581243 -14.673598567163568 71.813326221989513 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHand" -p "Character1_LeftForeArm";
	rename -uid "60DF333B-4C10-C8C5-8F71-FAB7DA09C724";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -24.404340271578391 -34.932697344706668 122.26498801320882 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr -k on ".liw" no;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb1" -p "Character1_LeftHand";
	rename -uid "5F305552-435E-405F-3461-3D92AD48B345";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.1043965330995 61.333484361561069 1.8740740862626108 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb2" -p "Character1_LeftHandThumb1";
	rename -uid "FE045AC0-4974-6430-BA30-32B8918A7277";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -103.44292663098258 20.93424016453444 169.56041330531272 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb3" -p "Character1_LeftHandThumb2";
	rename -uid "431DE4BB-4869-83C5-0552-9984653B66C6";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 14.174484134960574 -59.672502756712539 -19.020394805426264 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex1" -p "Character1_LeftHand";
	rename -uid "2692C457-42FA-C758-7440-A794C1D8F809";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -150.59621970507803 75.681392634287164 -124.28723884791442 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex2" -p "Character1_LeftHandIndex1";
	rename -uid "C95BE882-43F9-E3B4-14E4-269EF533A80D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -41.093804604175162 -57.602285575728224 -69.242456746392662 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex3" -p "Character1_LeftHandIndex2";
	rename -uid "86D76EFC-4D9C-47D3-190E-B09EB2CC3AE5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.108297963098213 8.7979835742630801 27.088028901385748 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing1" -p "Character1_LeftHand";
	rename -uid "61B998D6-495F-1616-4CDA-6C8088714A1C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -141.42437885137375 63.919742192600388 -113.78475086453638 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing2" -p "Character1_LeftHandRing1";
	rename -uid "2992BC2A-4639-D4E5-C810-B9B9453CDFD9";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -35.368646977348 -76.384823071466059 -48.755963146359043 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing3" -p "Character1_LeftHandRing2";
	rename -uid "FA6AF1AD-43AE-EF3F-3D66-0D90E71ECBD8";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 9.6312888734270121 39.523687300631337 15.3258182753707 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightShoulder" -p "Character1_Spine1";
	rename -uid "C7E8C2CC-4913-1FB0-86A5-2BBE2188F640";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -164.54352138244408 34.957040538466451 -116.14754006477595 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightArm" -p "Character1_RightShoulder";
	rename -uid "E818C3CF-4479-CD16-5262-2F95F4DF0ACE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -173.75456654457099 -11.164646449014084 -45.537881339438307 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightForeArm" -p "Character1_RightArm";
	rename -uid "12C92F54-4828-F9C2-0137-878B8BD8C31E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 22.753833922449758 50.186429787105524 -101.53518723839815 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHand" -p "Character1_RightForeArm";
	rename -uid "E7C1B742-45A8-1C19-1707-6A80CD16964F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 48.334026046900341 -3.7701083025157853 -29.882905994214536 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb1" -p "Character1_RightHand";
	rename -uid "D319FB2A-4839-1E24-E8BF-A39B06E5C616";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb2" -p "Character1_RightHandThumb1";
	rename -uid "CBF68EFB-4FDF-DBE7-FE96-06A1867E450F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb3" -p "Character1_RightHandThumb2";
	rename -uid "B7E753F8-40F4-9CE6-AB61-3FBABAF0C0F0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex1" -p "Character1_RightHand";
	rename -uid "C10A0107-4DC1-D258-54FE-519E14AC2B1C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex2" -p "Character1_RightHandIndex1";
	rename -uid "208BCF53-4163-B299-F8EE-D4BF8CB570BC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex3" -p "Character1_RightHandIndex2";
	rename -uid "B293CB9F-4808-3210-FEE6-FCA0FF83A54A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing1" -p "Character1_RightHand";
	rename -uid "8BB7834E-4758-8102-D5B5-A6BE00C920BD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing2" -p "Character1_RightHandRing1";
	rename -uid "98DC10DF-4527-0A2C-3D51-10BBD0FDC50D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing3" -p "Character1_RightHandRing2";
	rename -uid "8F69A6B7-4BFF-C912-4071-F1A3F6759A12";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Neck" -p "Character1_Spine1";
	rename -uid "BB520C16-4601-3163-9FF1-BFA4F864653D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 48.229963067601958 30.670012388143238 129.35864775594831 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Head" -p "Character1_Neck";
	rename -uid "402FD8DD-4A53-4FD8-0D21-8296ABB06BB5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -14.36476107543206 22.377462763663633 -109.58762475494268 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "eyes" -p "Character1_Head";
	rename -uid "5637DB9C-44F9-0439-8251-75B82E077805";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -157.21319586594478 -21.144487689504771 -3.4677530572789341 ;
	setAttr ".radi" 4.5;
	setAttr ".fbxID" 5;
createNode joint -n "jaw" -p "Character1_Head";
	rename -uid "E730FD1A-4647-949C-8D44-019996AEA7D2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -157.21319586594478 -21.144487689504771 -3.4677530572789341 ;
	setAttr ".radi" 0.5;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode animCurveTU -n "Character1_LeftHand_lockInfluenceWeights";
	rename -uid "C38CCC5D-47A0-F0B7-94D1-2880D5A5645A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "jaw_lockInfluenceWeights";
	rename -uid "6F201041-43FD-A956-C445-94852CA412AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "Character1_Hips_translateX";
	rename -uid "BFB00A77-4C41-35A0-E660-409491347699";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 2.1608636379241943 1 0.62440860271453857
		 2 -0.79900211095809937 3 -1.9245802164077759 4 -2.6884782314300537 5 -3.1370973587036133
		 6 -3.27402663230896 7 -2.8956716060638428 8 -2.234846830368042 9 -1.8610837459564209
		 10 -1.7234811782836914 11 -1.2529376745223999 12 -0.50646919012069702 13 0.52227234840393066
		 14 1.606900691986084 15 2.533635139465332 16 3.3636324405670166 17 4.041447639465332
		 18 4.5012998580932617 19 4.7584586143493652 20 4.7839202880859375 21 4.5247678756713867
		 22 3.9685425758361816 23 3.1129024028778076 24 2.1608636379241943;
createNode animCurveTL -n "Character1_Hips_translateY";
	rename -uid "D4657CB7-43DE-8A00-C126-C2B77B1341AB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 47.693233489990234 1 47.502082824707031
		 2 46.108291625976563 3 43.553241729736328 4 40.778251647949219 5 37.962841033935547
		 6 35.266036987304688 7 33.795650482177734 8 34.818881988525391 9 37.930171966552734
		 10 42.290969848632813 11 46.173786163330078 12 47.917385101318359 13 47.197776794433594
		 14 45.476322174072266 15 43.04888916015625 16 40.711856842041016 17 38.890830993652344
		 18 37.26104736328125 19 36.134902954101563 20 36.884757995605469 21 39.653553009033203
		 22 43.223976135253906 23 46.287754058837891 24 47.693233489990234;
createNode animCurveTL -n "Character1_Hips_translateZ";
	rename -uid "54801141-41C5-C929-FC68-BCB786C48F5D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -7.6826419830322266 1 -7.7820930480957031
		 2 -7.1517963409423828 3 -5.3848047256469727 4 -4.1974740028381348 5 -4.6699423789978027
		 6 -5.4931201934814453 7 -6.3696126937866211 8 -6.8145065307617188 9 -6.6183524131774902
		 10 -6.6865949630737305 11 -7.182075023651123 12 -7.8141670227050781 13 -7.9955549240112305
		 14 -7.2996406555175781 15 -5.1131601333618164 16 -3.6369497776031494 17 -3.8756699562072754
		 18 -4.2556929588317871 19 -4.3634815216064453 20 -4.4909133911132812 21 -5.0461206436157227
		 22 -6.1181449890136719 23 -7.2471299171447754 24 -7.6826419830322266;
createNode animCurveTU -n "Character1_Hips_scaleX";
	rename -uid "3FF73722-4169-88E3-BDA3-A0B7FC534A90";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_Hips_scaleY";
	rename -uid "57F66BFF-4EDA-F962-18A1-8E9525622572";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_Hips_scaleZ";
	rename -uid "14580084-4483-FAFA-8F5A-4C918E5F03A8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Hips_rotateX";
	rename -uid "6DF03BB7-4872-8139-6522-A280C7DBB91D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 28.791807174682621 1 26.072944641113281
		 2 23.742277145385742 3 22.272920608520508 4 21.179145812988281 5 19.51197624206543
		 6 18.001548767089844 7 18.034845352172852 8 20.511829376220703 9 24.438774108886719
		 10 28.597986221313477 11 31.768842697143558 12 33.338314056396484 13 34.904655456542969
		 14 36.359878540039063 15 38.280216217041016 16 39.791103363037109 17 39.998371124267578
		 18 39.36541748046875 19 38.670501708984375 20 38.107322692871094 21 36.4373779296875
		 22 33.411727905273438 23 30.089214324951172 24 28.791807174682621;
createNode animCurveTA -n "Character1_Hips_rotateY";
	rename -uid "19367A6F-48DE-B7C2-5348-5BABF9EF8DB6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 16.857933044433594 1 20.766271591186523
		 2 24.290660858154297 3 27.36949348449707 4 29.145402908325195 5 28.287689208984375
		 6 25.695520401000977 7 22.53870964050293 8 19.006851196289063 9 14.105201721191406
		 10 8.6031494140625 11 3.7080576419830322 12 0.50313073396682739 13 -3.7192068099975586
		 14 -7.424588680267334 15 -10.013589859008789 16 -11.233242988586426 17 -11.773425102233887
		 18 -11.442469596862793 19 -9.5657453536987305 20 -4.7782702445983887 21 2.5910775661468506
		 22 10.103445053100586 23 15.31759548187256 24 16.857933044433594;
createNode animCurveTA -n "Character1_Hips_rotateZ";
	rename -uid "24B7A3B1-47BC-1EFB-77EB-6B808F0E215A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -25.604698181152344 1 -24.814371109008789
		 2 -23.796701431274414 3 -21.714853286743164 4 -19.815620422363281 5 -20.4315185546875
		 6 -21.887712478637695 7 -21.151882171630859 8 -16.550216674804687 9 -9.5598564147949219
		 10 -1.4472141265869141 11 5.9800796508789062 12 11.316466331481934 13 12.21247386932373
		 14 11.326709747314453 15 8.7739171981811523 16 6.458397388458252 17 7.1091256141662598
		 18 9.0622272491455078 19 9.1744041442871094 20 4.8215689659118652 21 -3.0832276344299316
		 22 -12.668259620666504 23 -21.244888305664062 24 -25.604698181152344;
createNode animCurveTU -n "Character1_Hips_visibility";
	rename -uid "E06D3277-4EE0-0F26-96EB-A0A1EB3DBE68";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftUpLeg_translateX";
	rename -uid "C2BA7120-4A2C-B235-9A09-71B5BAA1D19E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.784505844116211;
createNode animCurveTL -n "Character1_LeftUpLeg_translateY";
	rename -uid "6190CFAF-4870-F636-5C55-1F89A3B475FA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5774786561451037e-006;
createNode animCurveTL -n "Character1_LeftUpLeg_translateZ";
	rename -uid "52BCD34B-4C96-A530-12F8-D9B0E2493778";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0264549255371094;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleX";
	rename -uid "D181A308-4837-A32A-FC11-F98400685C43";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleY";
	rename -uid "D44C2F59-4FC8-9497-96D1-8992ED7DADBC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleZ";
	rename -uid "A28B26C1-4321-4715-286B-F9909AB180B3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateX";
	rename -uid "55BACAC1-40F9-ED51-E156-20BCA2F1CBEF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 24.833084106445313 1 23.269622802734375
		 2 26.817586898803711 3 33.084007263183594 4 34.826053619384766 5 30.616872787475582
		 6 21.127737045288086 7 -12.356738090515137 8 -72.305274963378906 9 -78.423202514648438
		 10 -73.491058349609375 11 -75.874771118164063 12 -79.852134704589844 13 -67.909011840820313
		 14 -50.646430969238281 15 -49.24383544921875 16 -47.842304229736328 17 -40.113670349121094
		 18 -28.497215270996094 19 -16.884178161621094 20 -4.5306911468505859 21 8.3785266876220703
		 22 12.30034065246582 23 15.385432243347168 24 24.833084106445313;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateY";
	rename -uid "FD5C4C57-4896-75D1-5B39-4D87FE582B11";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 0.5466761589050293 1 1.0423343181610107
		 2 -3.4532294273376465 3 -10.172624588012695 4 -10.869820594787598 5 4.8039751052856445
		 6 33.350105285644531 7 60.040599822998047 8 51.212978363037109 9 16.471059799194336
		 10 -14.40328788757324 11 -33.6885986328125 12 -40.657073974609375 13 -34.074939727783203
		 14 -25.386943817138672 15 -30.347324371337887 16 -37.900936126708984 17 -43.118038177490234
		 18 -45.90478515625 19 -42.209426879882813 20 -27.788545608520508 21 -15.444699287414551
		 22 -4.4233684539794922 23 3.5732948780059814 24 0.54667645692825317;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateZ";
	rename -uid "FD1F85D5-43EE-28DD-C775-75BE80666087";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 20.829233169555664 1 23.837299346923828
		 2 28.656881332397461 3 33.286613464355469 4 33.23516845703125 5 25.141372680664063
		 6 17.056854248046875 7 -7.867241382598877 8 -64.586273193359375 9 -72.599617004394531
		 10 -65.087211608886719 11 -54.069370269775391 12 -42.809150695800781 13 -38.121181488037109
		 14 -29.442596435546875 15 -35.204704284667969 16 -44.919044494628906 17 -52.238838195800781
		 18 -59.714588165283196 19 -62.113105773925788 20 -46.51507568359375 21 -22.596195220947266
		 22 -6.7241005897521973 23 7.3138985633850107 24 20.829231262207031;
createNode animCurveTU -n "Character1_LeftUpLeg_visibility";
	rename -uid "96F40E68-4921-8503-5262-7FBDC87F5026";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftLeg_translateX";
	rename -uid "F91E1E9E-4308-818C-83C9-D8B353BE6D93";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.968876838684082;
createNode animCurveTL -n "Character1_LeftLeg_translateY";
	rename -uid "AB627465-43A8-BA9F-CD1C-7E9A34C34EBE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.2585945129394531;
createNode animCurveTL -n "Character1_LeftLeg_translateZ";
	rename -uid "42C5B979-4CF0-760F-4E98-CBA657197BA4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 18.197568893432617;
createNode animCurveTU -n "Character1_LeftLeg_scaleX";
	rename -uid "5950BFE4-4080-31E1-4FCD-BAACDDCDF5B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_LeftLeg_scaleY";
	rename -uid "D8ACB372-4732-F6F3-2F65-8EB0F35BB6B1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftLeg_scaleZ";
	rename -uid "3006F277-4214-56F1-FF6A-1A83B0016C3A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftLeg_rotateX";
	rename -uid "BB4B6697-4CF8-50CA-ACB5-53ADAA510DA8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 15.206339836120605 1 26.948692321777344
		 2 29.48466682434082 3 24.886604309082031 4 31.901336669921879 5 60.578475952148445
		 6 100.26361846923828 7 135.21588134765625 8 145.19160461425781 9 138.36491394042969
		 10 121.22991943359375 11 103.1527099609375 12 86.380775451660156 13 56.212364196777344
		 14 13.444362640380859 15 22.346752166748047 16 42.376346588134766 17 50.922035217285156
		 18 58.126022338867195 19 65.123573303222656 20 58.552165985107422 21 32.483432769775391
		 22 23.793539047241211 23 24.553251266479492 24 15.206339836120605;
createNode animCurveTA -n "Character1_LeftLeg_rotateY";
	rename -uid "4571FB4E-48F2-3523-3517-E0AAA98C06A8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 17.425779342651367 1 14.602261543273928
		 2 10.015671730041504 3 5.3018589019775391 4 -1.2477599382400513 5 -21.191505432128906
		 6 -41.320781707763672 7 -40.170501708984375 8 -33.056999206542969 9 -30.055315017700192
		 10 -25.665151596069336 11 -14.254782676696777 12 3.8535926342010498 13 13.724509239196777
		 14 9.111943244934082 15 7.1251287460327148 16 5.7099103927612305 17 2.2473578453063965
		 18 -3.5364212989807129 19 -9.2302885055541992 20 -9.6572847366333008 21 0.31700709462165833
		 22 8.8176059722900391 23 16.029170989990234 24 17.425779342651367;
createNode animCurveTA -n "Character1_LeftLeg_rotateZ";
	rename -uid "FB787F94-4FAE-F447-7A9F-04AAB45C7A3F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -7.6551027297973633 1 3.7021899223327637
		 2 7.7780900001525879 3 8.2369852066040039 4 14.849159240722658 5 22.230779647827148
		 6 2.6842560768127441 7 -25.395963668823242 8 -27.376266479492188 9 -8.4638261795043945
		 10 12.63573169708252 11 28.052291870117187 12 34.182132720947266 13 20.242893218994141
		 14 -2.5655965805053711 15 5.1771044731140137 16 15.720831871032715 17 20.10418701171875
		 18 23.148910522460938 19 24.745487213134766 20 24.1796875 21 16.570884704589844 22 7.2912631034851065
		 23 2.117164134979248 24 -7.655102252960206;
createNode animCurveTU -n "Character1_LeftLeg_visibility";
	rename -uid "C223D924-4AD7-D98A-9571-80951F304690";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFoot_translateX";
	rename -uid "6EFE98D4-4F2F-617A-E40A-F0AAC90E6511";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5243232250213623;
createNode animCurveTL -n "Character1_LeftFoot_translateY";
	rename -uid "03106EA6-4113-5CF4-EC54-3BA496F4AF6D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.000351905822754;
createNode animCurveTL -n "Character1_LeftFoot_translateZ";
	rename -uid "825DC35A-45C9-BC71-B132-86AF04D86764";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.967419624328613;
createNode animCurveTU -n "Character1_LeftFoot_scaleX";
	rename -uid "FD2AA638-4DA4-5D4D-6C81-C089E4C4255B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFoot_scaleY";
	rename -uid "10B091B7-4223-D057-60BA-1698E76BE8B3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleZ";
	rename -uid "A0F8F39D-476D-E62A-B528-28ACCDA30E34";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999964237213135;
createNode animCurveTA -n "Character1_LeftFoot_rotateX";
	rename -uid "92751F14-4DEE-D2E5-8D84-FD87570FF418";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 13.552981376647949 1 14.993997573852539
		 2 13.126706123352051 3 8.5219860076904297 4 9.1426076889038086 5 12.043539047241211
		 6 1.1362117528915405 7 -13.128898620605469 8 -26.282943725585937 9 -32.190776824951172
		 10 -22.746984481811523 11 -8.3448238372802734 12 -0.18587188422679901 13 7.2763261795043936
		 14 4.3183374404907227 15 -2.967888355255127 16 -10.136229515075684 17 -11.72718620300293
		 18 -11.580548286437988 19 -12.058420181274414 20 -3.4868850708007813 21 11.107083320617676
		 22 14.632430076599121 23 16.509899139404297 24 13.552981376647949;
createNode animCurveTA -n "Character1_LeftFoot_rotateY";
	rename -uid "855A7005-44E9-CCB9-FAC3-FFBA92A42E7C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 12.882519721984863 1 14.897757530212402
		 2 17.275726318359375 3 19.986812591552734 4 17.840705871582031 5 4.6490664482116699
		 6 -6.813807487487793 7 -3.3800151348114014 8 1.4919930696487427 9 2.0085585117340088
		 10 4.7942876815795898 11 7.0102128982543945 12 6.1039352416992188 13 -5.2581405639648438
		 14 -11.852577209472656 15 -14.0147705078125 16 -16.863307952880859 17 -23.371437072753906
		 18 -32.859535217285156 19 -42.350284576416016 20 -45.662078857421875 21 -30.505659103393555
		 22 -14.045658111572266 23 -0.080650709569454193 24 12.882518768310547;
createNode animCurveTA -n "Character1_LeftFoot_rotateZ";
	rename -uid "39F82C71-4AD9-E6E7-4494-D6AB85634D43";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -1.0398316383361816 1 -2.8255155086517334
		 2 -6.5578327178955078 3 -12.077658653259277 4 -12.262598037719727 5 -4.9818968772888184
		 6 -1.3917032480239868 7 -7.1610531806945801 8 -18.181182861328125 9 -25.561458587646484
		 10 -24.610561370849609 11 -15.438332557678224 12 -3.4367601871490479 13 6.7294244766235352
		 14 8.2398138046264648 15 7.2970867156982431 16 8.6894416809082031 17 15.100833892822264
		 18 24.399309158325195 19 34.83233642578125 20 38.435920715332031 21 25.988903045654297
		 22 17.186534881591797 23 9.354243278503418 24 -1.0398316383361816;
createNode animCurveTU -n "Character1_LeftFoot_visibility";
	rename -uid "18C98852-432C-A1AA-A52D-3998C88D305F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftToeBase_translateX";
	rename -uid "EDBB476F-46BC-E8D7-D50C-C58DAFC9C28C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.585639476776123;
createNode animCurveTL -n "Character1_LeftToeBase_translateY";
	rename -uid "526FD1BF-4EBE-A9B1-4063-3C80F436601F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3360438346862793;
createNode animCurveTL -n "Character1_LeftToeBase_translateZ";
	rename -uid "CEAEACAF-4731-3055-01B5-6196C4DD13A2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.4897398948669434;
createNode animCurveTU -n "Character1_LeftToeBase_scaleX";
	rename -uid "F12B4B7C-4476-5D4B-E028-7D8D40764B16";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftToeBase_scaleY";
	rename -uid "30E7E9B3-429D-EB0E-FA11-7A929CC4C891";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftToeBase_scaleZ";
	rename -uid "711753EE-42E7-12ED-1052-9D911C057263";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_LeftToeBase_rotateX";
	rename -uid "8B849148-42FE-C507-87F6-19845BC79165";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -5.0923287808757323e-009 1 -1.6386831998825073
		 2 -3.1954116821289062 3 -4.9410810470581055 4 -7.1521525382995605 5 -10.316228866577148
		 6 -14.163131713867188 7 -17.815431594848633 8 -21.158056259155273 9 -23.347087860107422
		 10 -25.411558151245117 11 -26.665166854858398 12 -22.538900375366211 13 -1.8247648477554321
		 14 16.546342849731445 15 9.7219476699829102 16 3.0129330141903665e-009 17 2.2140571598328052e-009
		 18 1.2141544436516938e-009 19 -1.837724417086406e-011 20 8.9983940124511719 21 17.716512680053711
		 22 14.519119262695311 23 7.3534240722656241 24 -5.0791517658410612e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateY";
	rename -uid "B91F586C-4A29-0031-FF90-1E835768948A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 4.4197210335994441e-009 1 0.49729025363922125
		 2 0.94135540723800648 3 1.4064735174179077 4 1.9458096027374268 5 2.6210343837738037
		 6 3.2893509864807129 7 3.7695133686065678 8 4.077721118927002 9 4.2117772102355957
		 10 4.289128303527832 11 4.3128681182861328 12 4.1685242652893066 13 0.55182474851608276
		 14 -6.7206845283508301 15 -3.5780160427093506 16 5.0226303116573945e-009 17 4.9536663659921487e-009
		 18 5.0952135843829183e-009 19 5.0762509751223206e-009 20 -3.2749555110931396 21 -7.3100967407226571
		 22 -5.7341189384460449 23 -2.607811450958252 24 4.4584602676422946e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateZ";
	rename -uid "15F266E7-474D-A033-184B-738F316EDF27";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -1.2970492457853311e-009 1 -0.97955632209777832
		 2 -1.9182976484298704 3 -2.9797582626342773 4 -4.3364028930664062 5 -6.2987289428710938
		 6 -8.7123003005981445 7 -11.025821685791016 8 -13.156852722167969 9 -14.55731201171875
		 10 -15.880324363708496 11 -16.684318542480469 12 -14.0399169921875 13 -1.0913598537445068
		 14 9.2382068634033203 15 5.5931539535522461 16 -1.9110744098327359e-009 17 -1.853961206776944e-009
		 18 -1.7426163845257747e-009 19 -1.7573741351029071e-009 20 5.1916418075561523 21 9.834843635559082
		 22 8.1839065551757813 23 4.2691774368286133 24 -1.2108657410081491e-009;
createNode animCurveTU -n "Character1_LeftToeBase_visibility";
	rename -uid "5BABD1F8-415C-C3D0-C9F6-ECA59E2A7CE6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateX";
	rename -uid "8E08C04C-47B3-E683-0BE1-D7ABE0FE2BAB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7153444290161133;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateY";
	rename -uid "5F90DD59-4818-6E82-4C7F-9A9F67CB90B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.0020139217376709;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateZ";
	rename -uid "52B22091-417F-9DB3-F4E9-179FF4E022A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.1786108016967773;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleX";
	rename -uid "41F9A19A-4052-1FFA-7916-02942F26EA05";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleY";
	rename -uid "83C30158-4645-52D3-E716-0EA0E770EB3A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999940395355225;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleZ";
	rename -uid "799C1EA4-4958-ABDF-D0FF-92BC786F4ACD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateX";
	rename -uid "E33E35C3-4136-04A6-C8CD-6D9E02B655D6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1672057986800155e-008;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateY";
	rename -uid "46D5D564-4081-C792-D374-70847CE8032F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3806514154168781e-008;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateZ";
	rename -uid "523EC9FC-4467-0393-FA82-FE8EEB5F4099";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.7920254431232934e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle1_visibility";
	rename -uid "1B5FE018-4C84-981F-6B50-1892CB32D7A0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateX";
	rename -uid "F333034C-49B3-C43C-9124-3F8FC2B13C93";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4684751033782959;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateY";
	rename -uid "94801B70-4565-FC36-2616-5687CE582F6E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.1393222808837891;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateZ";
	rename -uid "FF4A1681-4588-9930-FEA0-0DA8B95EF069";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.3014278411865234;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleX";
	rename -uid "06BE032F-4C70-671A-CE27-8FA04CB69AA6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleY";
	rename -uid "473CFE7B-4B0D-53CF-2BA5-5FAFC5586409";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleZ";
	rename -uid "9346850E-4AE3-45A1-3876-EE94F4475069";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateX";
	rename -uid "655B00DD-409E-AC5F-3ED6-DEA80C148A9D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.7159814902356629e-008;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateY";
	rename -uid "4CDD2703-420B-FAE6-6EB9-BBAD8B6E9F30";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3259433551127131e-008;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateZ";
	rename -uid "451D1845-407C-E1BC-D3D2-1D9F279F96FA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3138356059225771e-008;
createNode animCurveTU -n "Character1_LeftFootMiddle2_visibility";
	rename -uid "32AFB5DA-469A-852D-0EB1-55919D32255A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightUpLeg_translateX";
	rename -uid "AC69C305-4649-EF02-E892-40802641B75C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.489771842956543;
createNode animCurveTL -n "Character1_RightUpLeg_translateY";
	rename -uid "6E3E65C1-40B4-A774-5962-E9A8AA76A7AC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.6505136489868164;
createNode animCurveTL -n "Character1_RightUpLeg_translateZ";
	rename -uid "0B38ADF4-48AA-D58C-B6F8-BD800959B7FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0264549255371094;
createNode animCurveTU -n "Character1_RightUpLeg_scaleX";
	rename -uid "A1DD2FC7-410F-7CA5-595A-FE8F37E64DC0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightUpLeg_scaleY";
	rename -uid "644F2812-4527-AF69-DAAB-3FBBED28AD44";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightUpLeg_scaleZ";
	rename -uid "438B8E99-48ED-EA5C-426D-D0B68956AB5A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightUpLeg_rotateX";
	rename -uid "5D108511-4F06-4636-0F9D-DBA009431BFD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 94.388694763183594 1 56.400814056396484
		 2 28.441839218139648 3 32.304611206054688 4 49.416461944580078 5 48.790142059326172
		 6 34.67364501953125 7 3.9457335472106938 8 -24.886432647705078 9 -32.381362915039063
		 10 -29.321800231933594 11 -20.36268424987793 12 -11.251681327819824 13 -6.4089655876159668
		 14 -5.8779196739196777 15 -8.7201871871948242 16 -5.1291408538818359 17 2.5232141017913818
		 18 9.9079732894897461 19 11.000944137573242 20 8.5354652404785156 21 29.128307342529293
		 22 76.054534912109375 23 88.454353332519531 24 94.388694763183594;
createNode animCurveTA -n "Character1_RightUpLeg_rotateY";
	rename -uid "0D95A5B8-4970-728E-72DB-5397367A2E24";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 52.253551483154297 1 51.522689819335938
		 2 37.376380920410156 3 37.038257598876953 4 47.086509704589844 5 53.481559753417969
		 6 60.115947723388672 7 60.668708801269531 8 37.030925750732422 9 7.4938063621521005
		 10 -7.6116933822631845 11 -15.637523651123045 12 -17.346080780029297 13 -21.049867630004883
		 14 -30.517465591430661 15 -41.088798522949219 16 -36.312263488769531 17 -18.669975280761719
		 18 9.0588550567626953 19 38.969608306884766 20 60.440387725830078 21 74.315330505371094
		 22 69.701324462890625 23 59.400764465332031 24 52.253551483154297;
createNode animCurveTA -n "Character1_RightUpLeg_rotateZ";
	rename -uid "4989ED93-4DB1-1B1E-8E4E-6A8174D6E652";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 109.49939727783203 1 77.278739929199219
		 2 55.222198486328125 3 52.375579833984375 4 64.552154541015625 5 73.182464599609375
		 6 71.489227294921875 7 48.764907836914063 8 23.495063781738281 9 17.822292327880859
		 10 12.817177772521973 11 3.405808687210083 12 -5.6788501739501953 13 -11.101240158081055
		 14 -13.623500823974609 15 -14.217791557312012 16 -18.83745002746582 17 -25.446147918701172
		 18 -32.510410308837891 19 -38.343574523925781 20 -34.904834747314453 21 2.2791321277618408
		 22 67.469551086425781 23 93.134262084960938 24 109.49939727783203;
createNode animCurveTU -n "Character1_RightUpLeg_visibility";
	rename -uid "B42EA249-4003-D5C8-7E55-079287909CB9";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightLeg_translateX";
	rename -uid "116C63B9-4913-D32C-9E3E-45A2CB39890F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -18.197565078735352;
createNode animCurveTL -n "Character1_RightLeg_translateY";
	rename -uid "6D175DCA-42A2-A494-4B64-0083C76DE86D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.1862249374389648;
createNode animCurveTL -n "Character1_RightLeg_translateZ";
	rename -uid "51BF66EC-47B2-CE3E-45ED-B3B0519BCF85";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.5520458221435547;
createNode animCurveTU -n "Character1_RightLeg_scaleX";
	rename -uid "5A3D400D-42C5-F102-3F97-2183C54C41D2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightLeg_scaleY";
	rename -uid "57F809CE-4564-9ED1-65FE-C5A4E12657AF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightLeg_scaleZ";
	rename -uid "9EF93D08-4393-CE32-C1DF-A4B91BAE0103";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightLeg_rotateX";
	rename -uid "9996A49D-4CB9-9220-DBC6-C48C5D036646";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 55.205593109130859 1 10.038251876831055
		 2 -2.7772889137268066 3 -9.120610237121582 4 -4.4032249450683594 5 9.4774761199951172
		 6 29.351945877075199 7 46.308971405029297 8 45.555030822753906 9 33.157741546630859
		 10 19.89680290222168 11 11.91018009185791 12 13.508709907531738 13 18.912469863891602
		 14 21.604852676391602 15 24.119710922241211 16 32.737979888916016 17 46.804672241210937
		 18 91.640953063964844 19 40.40155029296875 20 37.269332885742188 21 13.557601928710937
		 22 -28.73040771484375 23 -90.069015502929687 24 -124.79440307617187;
createNode animCurveTA -n "Character1_RightLeg_rotateY";
	rename -uid "D2242ABE-4C49-E491-4CE0-C19A7EEF8A04";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -71.682464599609375 1 -39.344738006591797
		 2 6.3604264259338379 3 5.1702351570129395 4 -28.251632690429688 5 -44.784431457519531
		 6 -56.471519470214844 7 -64.527519226074219 8 -58.122791290283196 9 -28.347755432128906
		 10 -8.16522216796875 11 -3.9734890460968018 12 -13.898998260498047 13 -23.194538116455078
		 14 -23.604438781738281 15 -15.260045051574705 16 -28.029211044311523 17 -56.941886901855469
		 18 -83.333267211914063 19 -100.22053527832031 20 -106.80597686767578 21 -107.91898345947266
		 22 -103.23310089111328 23 -102.96395874023437 24 -108.31753540039062;
createNode animCurveTA -n "Character1_RightLeg_rotateZ";
	rename -uid "8F362F61-4346-EE9F-BC77-02AFA7A2EB92";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -45.204486846923828 1 -5.3270053863525391
		 2 -0.47342801094055176 3 -0.88891202211380005 4 -4.3402152061462402 5 -7.7257809638977042
		 6 -9.973297119140625 7 -9.5221614837646484 8 -0.8107830286026001 9 4.8708739280700684
		 10 3.345806360244751 11 1.8871685266494751 12 1.7321786880493164 13 2.986513614654541
		 14 4.5535621643066406 15 5.1135797500610352 16 5.5364642143249512 17 2.274735689163208
		 18 -30.83567047119141 19 28.012252807617188 20 24.616104125976562 21 33.48736572265625
		 22 60.67842102050782 23 108.94700622558594 24 134.79551696777344;
createNode animCurveTU -n "Character1_RightLeg_visibility";
	rename -uid "D5631824-497E-4267-C65A-CA9459CF2D80";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFoot_translateX";
	rename -uid "822FCCEA-47F0-3A5B-C912-7698FDD78362";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -18.25177001953125;
createNode animCurveTL -n "Character1_RightFoot_translateY";
	rename -uid "F52151D5-4563-8AFE-EA4E-6686252E1CB0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.0794563293457031;
createNode animCurveTL -n "Character1_RightFoot_translateZ";
	rename -uid "1879F868-4A0A-C590-CB55-78994724B0A2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5524176359176636;
createNode animCurveTU -n "Character1_RightFoot_scaleX";
	rename -uid "1EC048DB-4A5A-80F3-5186-B1B0D61392A7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFoot_scaleY";
	rename -uid "21DA0390-4D40-0196-DA7F-92B5422BAFEC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFoot_scaleZ";
	rename -uid "506E93B5-4E01-0CB2-1B18-FB93B5A6008A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightFoot_rotateX";
	rename -uid "7946F001-4964-B748-DE63-849856B10861";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 7.6339087486267081 1 -7.0044660568237305
		 2 -7.4604763984680176 3 2.7922148704528809 4 2.8174562454223633 5 -8.1231365203857422
		 6 -23.452447891235352 7 -33.808174133300781 8 -42.084663391113281 9 -32.675857543945313
		 10 -10.040696144104004 11 8.5468244552612305 12 13.741185188293457 13 13.186801910400391
		 14 14.759032249450684 15 20.1363525390625 16 18.314304351806641 17 14.743801116943359
		 18 19.920019149780273 19 32.364013671875 20 29.311666488647464 21 19.376317977905273
		 22 13.358517646789551 23 9.7568941116333008 24 7.6339087486267081;
createNode animCurveTA -n "Character1_RightFoot_rotateY";
	rename -uid "182A2CA3-4222-48EA-C46A-3EB5DBFF6C30";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -4.4583425521850586 1 3.4601707458496094
		 2 3.4939241409301758 3 0.16539880633354187 4 1.2090922594070435 5 6.1961684226989746
		 6 6.4790763854980469 7 1.585644006729126 8 1.3072978258132935 9 7.4442615509033203
		 10 7.2318787574768075 11 -0.53607666492462158 12 -7.2154345512390146 13 -8.5960760116577148
		 14 -8.7009906768798828 15 -10.263429641723633 16 -8.30316162109375 17 -5.9702811241149902
		 18 -6.86688232421875 19 -10.171005249023437 20 -8.0925579071044922 21 -6.3272929191589355
		 22 -6.4518580436706543 23 -6.0117068290710449 24 -4.4583425521850586;
createNode animCurveTA -n "Character1_RightFoot_rotateZ";
	rename -uid "DEC928E2-425E-A39C-0010-51B37C0034FF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 8.22857666015625 1 1.8731366395950317
		 2 -3.9794654846191402 3 -8.8703527450561523 4 -9.5968599319458008 5 -16.222827911376953
		 6 -29.808832168579102 7 -43.006801605224609 8 -44.744831085205078 9 -27.58428955078125
		 10 -10.008971214294434 11 1.2551126480102539 12 7.4685750007629403 13 8.9358654022216797
		 14 8.2289190292358398 15 7.458746910095214 16 3.7665491104125972 17 -2.2578434944152832
		 18 -6.8103842735290527 19 -8.2691001892089844 20 -11.923093795776367 21 -12.081414222717285
		 22 -1.9481048583984377 23 7.4080681800842285 24 8.22857666015625;
createNode animCurveTU -n "Character1_RightFoot_visibility";
	rename -uid "A079F68C-45E6-467A-EE2E-DFA2844A626A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightToeBase_translateX";
	rename -uid "0A60505D-4578-57C7-0714-80AE3C1DECD2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2767767906188965;
createNode animCurveTL -n "Character1_RightToeBase_translateY";
	rename -uid "3A28540F-489B-4999-6CD2-E79A6D7E4321";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.0139193534851074;
createNode animCurveTL -n "Character1_RightToeBase_translateZ";
	rename -uid "BAFAEA02-4EF7-FE07-C963-08BE79096813";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8259720802307129;
createNode animCurveTU -n "Character1_RightToeBase_scaleX";
	rename -uid "9B7EBADB-4A40-DC36-C600-57ACA18FD035";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightToeBase_scaleY";
	rename -uid "608A9957-4CEE-393E-6C00-3EB904170595";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightToeBase_scaleZ";
	rename -uid "8C9EF414-45BB-19CC-616A-D99630BCD97D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightToeBase_rotateX";
	rename -uid "695FDF55-49DD-5F7A-3182-CEB30559F689";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 7.6131577491760263 1 -1.6664829254150391
		 2 -8.2680683135986328 3 -4.3630352020263672 4 1.2515609382202086e-010 5 1.3113457542068829e-010
		 6 1.7114618056091047e-010 7 3.889362720777001e-010 8 -2.5869646072387695 9 -5.3134932518005371
		 10 -4.1769647598266602 11 -1.9105042219161987 12 -1.9451489308153214e-009 13 1.198772668838501
		 14 2.241976261138916 15 3.1531858444213867 16 3.9519889354705815 17 4.4918231964111328
		 18 4.7853279113769531 19 5.1018085479736328 20 5.5572843551635742 21 6.0503292083740234
		 22 6.5556011199951172 23 7.0779390335083008 24 7.6131577491760263;
createNode animCurveTA -n "Character1_RightToeBase_rotateY";
	rename -uid "545B58E9-4AE3-38DA-6A7F-C29C4C60B8E0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -25.456724166870117 1 5.9656009674072266
		 2 27.326610565185547 3 15.288829803466797 4 6.1162303000017459e-010 5 8.5226581347797037e-010
		 6 1.1269648547695965e-009 7 1.3594017023166316e-009 8 9.2122488021850586 9 18.405048370361328
		 10 14.666628837585451 11 6.8311023712158203 12 9.7683261479630801e-010 13 -4.2990942001342773
		 14 -8.0015077590942383 15 -11.180377006530762 16 -13.909395217895508 17 -15.717251777648924
		 18 -16.686565399169922 19 -17.72038459777832 20 -19.186565399169922 21 -20.743492126464844
		 22 -22.305021286010742 23 -23.881776809692383 24 -25.456724166870117;
createNode animCurveTA -n "Character1_RightToeBase_rotateZ";
	rename -uid "60BABAD5-43B7-EF7E-AC32-81A84F4D6DDD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -1.7220641374588013 1 -0.08684123307466507
		 2 -2.0132172107696533 3 -0.58587419986724854 4 6.2170841808040223e-009 5 5.6500200074083295e-009
		 6 5.1246367149815342e-009 7 4.4951766753342781e-009 8 -0.20845586061477661 9 -0.86143982410430908
		 10 -0.53778469562530518 11 -0.11403587460517883 12 1.2850076558379442e-009 13 -0.044996649026870728
		 14 -0.15682369470596313 15 -0.3087044358253479 16 -0.48225983977317805 17 -0.62029767036437988
		 18 -0.70219540596008301 19 -0.79580354690551758 20 -0.93998980522155773 21 -1.1083546876907349
		 22 -1.2937546968460083 23 -1.4986525774002075 24 -1.7220641374588013;
createNode animCurveTU -n "Character1_RightToeBase_visibility";
	rename -uid "90C7FD02-476B-C9A7-9919-429B0F8AD1C3";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateX";
	rename -uid "FB3A9311-4763-2A76-A4B7-039BBCA7C678";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.0018006421159952879;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateY";
	rename -uid "7F606958-4A23-3B59-9A0E-CFA6A7AA4DD1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.0064689838327467442;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateZ";
	rename -uid "DF357918-4574-DE4E-C19A-1DB16D2A172C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.6805553436279297;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleX";
	rename -uid "76EDF8DB-451F-F7C7-F521-C89311A5361E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleY";
	rename -uid "9283D994-45B9-0E75-7EC2-C58C459DEF95";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleZ";
	rename -uid "54174F4D-4786-D454-2BC5-8381843EDE74";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateX";
	rename -uid "CCC70418-4834-511A-D925-37960FCC379F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.4023223410551964e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateY";
	rename -uid "DC52FC1B-45AB-D5F1-A3D6-548B79F1D8AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.975656832049367e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateZ";
	rename -uid "2D8355D2-42CC-7CDB-DE1B-5BA1B5DCB216";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.4133428284994807e-009;
createNode animCurveTU -n "Character1_RightFootMiddle1_visibility";
	rename -uid "E560B4F5-4ABE-B7BD-182D-CF88DAE570D6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateX";
	rename -uid "247D3C99-4A8B-7D2E-76FF-858566DA52BE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.5860742880468024e-006;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateY";
	rename -uid "3743205F-49A1-0BE5-45D0-C49183F8641B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.3312830889117322e-007;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateZ";
	rename -uid "FD77DB81-4869-437E-A94F-C1A36C6EB045";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.0234856605529785;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleX";
	rename -uid "DED81A13-4DE8-825A-6B6B-7AB1AD0B1998";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleY";
	rename -uid "6F9C6A6D-4567-EE54-79F8-84B217C39A71";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleZ";
	rename -uid "E24B6269-4F1A-FCB0-9D5D-8F86937AE48A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateX";
	rename -uid "20D716D4-43D3-E4B9-43B2-2FB61E23A8A1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.7389297779502044e-009;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateY";
	rename -uid "BA08E451-4CEB-D12C-8560-CB8FD02F16DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.3679765339184087e-009;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateZ";
	rename -uid "851BA45F-4EA6-BD48-C7A5-42925800DC65";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.6323046025945587e-008;
createNode animCurveTU -n "Character1_RightFootMiddle2_visibility";
	rename -uid "A8B4A8B1-45CD-1908-6999-C091BA607D2A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Spine_translateX";
	rename -uid "5603FBBC-4133-7E2D-5731-F0828AB0A571";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6075620651245117;
createNode animCurveTL -n "Character1_Spine_translateY";
	rename -uid "E9FFD43B-4F96-37EA-2B04-7DA3C41EF20F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.353469848632812;
createNode animCurveTL -n "Character1_Spine_translateZ";
	rename -uid "6598B223-467C-7A4B-8979-1AAD80253273";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2061522006988525;
createNode animCurveTU -n "Character1_Spine_scaleX";
	rename -uid "448607CE-46C3-B4A0-776E-4882FD77815E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine_scaleY";
	rename -uid "EDCB88A8-4C3A-4296-7532-19A934BB7228";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine_scaleZ";
	rename -uid "41B129D7-4794-298E-F48A-4285C49D4A66";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_Spine_rotateX";
	rename -uid "FE6D4CCA-40A4-EF42-2BE9-6AACE0A4E45F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -22.825103759765625 1 -24.484357833862305
		 2 -26.310297012329102 3 -27.840696334838867 4 -28.56675910949707 5 -27.578458786010742
		 6 -25.101264953613281 7 -22.346664428710937 8 -19.897111892700195 9 -17.010793685913086
		 10 -13.854913711547852 11 -10.844760894775391 12 -8.0614414215087891 13 -5.6431150436401367
		 14 -3.9703717231750493 15 -3.0034525394439697 16 -2.6393001079559326 17 -2.8648276329040527
		 18 -3.9781310558319092 19 -6.0897207260131836 20 -9.2320051193237305 21 -13.435202598571777
		 22 -17.875696182250977 23 -21.37725830078125 24 -22.825103759765625;
createNode animCurveTA -n "Character1_Spine_rotateY";
	rename -uid "65650C8C-4E68-A1D1-E67D-78B3BBE612EB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -3.4567282199859619 1 -2.7082149982452393
		 2 -1.8426418304443362 3 -0.8498263955116272 4 -0.032103400677442551 5 0.29353612661361694
		 6 0.082062467932701111 7 -0.95963722467422485 8 -2.4697914123535156 9 -4.3498177528381348
		 10 -6.7702102661132813 11 -9.0813865661621094 12 -10.862785339355469 13 -11.959624290466309
		 14 -12.79351806640625 15 -13.724053382873535 16 -14.143498420715332 17 -13.256985664367676
		 18 -11.602174758911133 19 -10.050312995910645 20 -8.6483755111694336 21 -6.9482102394104004
		 22 -5.279510498046875 23 -3.9476971626281734 24 -3.4567282199859619;
createNode animCurveTA -n "Character1_Spine_rotateZ";
	rename -uid "0129A277-4369-8CAB-FDAC-C6AF06045DF5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 18.064292907714844 1 18.912126541137695
		 2 19.752119064331055 3 20.475936889648437 4 21.231645584106445 5 22.999050140380859
		 6 24.925886154174805 7 24.71336555480957 8 21.245590209960938 9 15.200511932373047
		 10 7.1588010787963867 11 -0.77283453941345215 12 -7.0848555564880371 13 -11.278913497924805
		 14 -13.540547370910645 15 -12.629972457885742 16 -11.436867713928223 17 -13.574678421020508
		 18 -16.656839370727539 19 -16.905044555664063 20 -11.803595542907715 21 -3.027726411819458
		 22 6.8627147674560547 23 14.767317771911623 24 18.064292907714844;
createNode animCurveTU -n "Character1_Spine_visibility";
	rename -uid "07B35E65-49CB-0F35-5E0C-D3A148371318";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Spine1_translateX";
	rename -uid "CC3045AB-4EEF-291D-5C10-22963CB0F26C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.930038452148437;
createNode animCurveTL -n "Character1_Spine1_translateY";
	rename -uid "E16E5087-419A-4FFF-1830-2AB71C6C0535";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1661763191223145;
createNode animCurveTL -n "Character1_Spine1_translateZ";
	rename -uid "2536E7CE-4CCB-E1F8-9862-79B41702B249";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.40121376514434814;
createNode animCurveTU -n "Character1_Spine1_scaleX";
	rename -uid "805EF7B0-418C-542C-88AE-4BBA9E192274";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_Spine1_scaleY";
	rename -uid "7FD49709-45BF-5BBD-1B98-84B92EB818E5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine1_scaleZ";
	rename -uid "35B209CE-481C-C4A1-34E5-9E97E1859651";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_Spine1_rotateX";
	rename -uid "9D3E6DC8-41AE-B96D-4AB1-ADAD02C86F0F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -32.654312133789063 1 -29.824827194213867
		 2 -26.812488555908203 3 -23.562370300292969 4 -20.505834579467773 5 -18.208988189697266
		 6 -16.1373291015625 7 -13.685319900512695 8 -10.652616500854492 9 -6.8659992218017578
		 10 -2.8043718338012695 11 1.4630054235458374 12 5.5313286781311035 13 8.5012674331665039
		 14 10.604114532470703 15 13.165329933166504 16 14.048806190490723 17 11.223353385925293
		 18 6.0622220039367676 19 0.099146008491516113 20 -6.8558850288391113 21 -15.236477851867678
		 22 -23.501602172851563 23 -29.872386932373047 24 -32.654312133789063;
createNode animCurveTA -n "Character1_Spine1_rotateY";
	rename -uid "95025849-443C-D97F-7AF8-7FA0F9A14146";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 7.635685920715332 1 10.831697463989258
		 2 14.363937377929689 3 17.089315414428711 4 17.982952117919922 5 16.081710815429688
		 6 12.05925178527832 7 7.5619525909423819 8 2.9904346466064453 9 -1.9891248941421509
		 10 -6.8978357315063477 11 -10.995489120483398 12 -13.162985801696777 13 -11.765644073486328
		 14 -8.7720203399658203 15 -5.7708263397216797 16 -3.4802794456481934 17 -3.3029906749725342
		 18 -3.8277044296264648 19 -3.7715377807617187 20 -2.4908785820007324 21 0.11487851291894913
		 22 3.4356744289398193 23 6.4598021507263184 24 7.635685920715332;
createNode animCurveTA -n "Character1_Spine1_rotateZ";
	rename -uid "BC0AED79-4616-4BDD-17F4-7380081792D1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 27.640596389770508 1 28.722654342651364
		 2 30.464982986450199 3 31.940885543823242 4 31.626976013183597 5 27.862361907958984
		 6 21.893712997436523 7 15.913576126098633 8 10.170682907104492 9 3.7972378730773926
		 10 -3.095466136932373 11 -9.3315753936767578 12 -13.971776962280273 13 -15.403282165527344
		 14 -14.928824424743651 15 -14.595763206481935 16 -12.829971313476563 17 -8.6540422439575195
		 18 -3.1883845329284668 19 2.4588348865509033 20 8.8622837066650391 21 16.025596618652344
		 22 22.198600769042969 23 26.201656341552734 24 27.640596389770508;
createNode animCurveTU -n "Character1_Spine1_visibility";
	rename -uid "FB417CAF-464C-CC1C-A805-D69493D31ECE";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftShoulder_translateX";
	rename -uid "2A7698DA-42F6-4755-4BC4-5FA80F0A3484";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.0842323303222656;
createNode animCurveTL -n "Character1_LeftShoulder_translateY";
	rename -uid "E3F355EB-4913-4873-A191-978C7E57AB50";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.604855537414551;
createNode animCurveTL -n "Character1_LeftShoulder_translateZ";
	rename -uid "E1FC64AA-4BCE-5AB7-C0DC-7F8F428E0A7D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6404602527618408;
createNode animCurveTU -n "Character1_LeftShoulder_scaleX";
	rename -uid "7EF7FE4C-413D-FDA4-2271-FA838FE0C8EF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftShoulder_scaleY";
	rename -uid "556E5614-4569-631A-AD0C-87A97664BEAA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftShoulder_scaleZ";
	rename -uid "1CE58472-4FD1-DA41-B6A9-DEA96F1F82C9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftShoulder_rotateX";
	rename -uid "5DA6C7CF-4854-4F31-3B5D-DD89DD8BF811";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 2.0791878700256348 1 1.9949859380722044
		 2 1.9386401176452639 3 1.8588443994522097 4 1.7415686845779419 5 1.6235396862030029
		 6 1.7394298315048218 7 1.5550373792648315 8 1.4286659955978394 9 2.0233633518218994
		 10 3.4040877819061279 11 4.9722299575805664 12 5.7756423950195313 13 5.9559545516967773
		 14 6.1241359710693359 15 6.049065113067627 16 5.5398235321044922 17 4.7955589294433594
		 18 4.7851705551147461 19 5.367401123046875 20 5.1287002563476562 21 4.1351556777954102
		 22 3.2049605846405029 23 2.4654340744018555 24 2.0791878700256348;
createNode animCurveTA -n "Character1_LeftShoulder_rotateY";
	rename -uid "809833B9-4E93-7FCC-A868-B285BB368EBD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -25.164213180541992 1 -26.427658081054687
		 2 -28.15278434753418 3 -30.079864501953125 4 -31.947528839111328 5 -36.356315612792969
		 6 -39.886856079101563 7 -38.945137023925781 8 -36.605865478515625 9 -33.654712677001953
		 10 -30.916383743286133 11 -29.098989486694336 12 -28.733539581298828 13 -29.179071426391598
		 14 -29.801620483398437 15 -31.093315124511715 16 -33.565292358398437 17 -39.535324096679688
		 18 -43.942508697509766 19 -37.504680633544922 20 -30.190980911254883 21 -28.335048675537109
		 22 -26.431367874145508 23 -25.156707763671875 24 -25.164213180541992;
createNode animCurveTA -n "Character1_LeftShoulder_rotateZ";
	rename -uid "FE6D81A0-40EB-9AE2-183A-6B9A6D382BD9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 3.4980306625366211 1 3.2392950057983398
		 2 3.4218893051147461 3 3.801680326461792 4 4.119288444519043 5 1.9531968832015991
		 6 -0.40978765487670898 7 1.7114033699035645 8 6.8102641105651855 9 13.031688690185547
		 10 18.89190673828125 11 23.249719619750977 12 25.117752075195313 13 25.408058166503906
		 14 25.439765930175781 15 24.838502883911133 16 23.232803344726563 17 18.921682357788086
		 18 15.077627182006834 19 15.601941108703613 20 15.508427619934082 21 12.165563583374023
		 22 8.419011116027832 23 5.2133331298828125 24 3.4980306625366211;
createNode animCurveTU -n "Character1_LeftShoulder_visibility";
	rename -uid "614E94F7-49A2-0427-AC53-02A01B2A7DD2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftArm_translateX";
	rename -uid "D6D0BDC5-4685-6011-0CBE-F5B86D34EAE6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 9.2092180252075195;
createNode animCurveTL -n "Character1_LeftArm_translateY";
	rename -uid "DBBDFDDE-4F24-FD73-27BA-A1B6102CF67D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.4172050952911377;
createNode animCurveTL -n "Character1_LeftArm_translateZ";
	rename -uid "7CD53F89-49F3-39BF-AA37-A590973E9DA7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.69030004739761353;
createNode animCurveTU -n "Character1_LeftArm_scaleX";
	rename -uid "A5CE3D00-4B4A-35D7-253F-E7A02BADDE85";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleY";
	rename -uid "CD3E2CE1-409D-3477-B98D-A3B27784BA4B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleZ";
	rename -uid "08D76CB5-4C9A-555E-5523-8697D04342B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_LeftArm_rotateX";
	rename -uid "160EAE70-4642-B8A4-DBED-399B969ECE63";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -9.3013887405395508 1 -9.6844549179077148
		 2 -9.8211002349853516 3 -9.3629493713378906 4 -7.8709330558776864 5 -1.0576268434524536
		 6 6.7504472732543945 7 11.555301666259766 8 16.456977844238281 9 21.233476638793945
		 10 25.663484573364258 11 29.815402984619144 12 34.139911651611328 13 37.7022705078125
		 14 39.054721832275391 15 38.580528259277344 16 36.748054504394531 17 34.682235717773437
		 18 30.664360046386719 19 18.792903900146484 20 5.9378914833068848 21 -1.9187325239181521
		 22 -6.4886713027954102 23 -8.7526998519897461 24 -9.3013887405395508;
createNode animCurveTA -n "Character1_LeftArm_rotateY";
	rename -uid "7435FD61-4E44-E5BC-62F5-38B3D66C5A5E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 4.5642642974853516 1 1.2903517484664917
		 2 -1.8177353143692017 3 -4.5686259269714355 4 -6.7393093109130859 5 -12.320204734802246
		 6 -21.195131301879883 7 -29.788259506225586 8 -38.253261566162109 9 -45.975727081298828
		 10 -52.388702392578125 11 -57.028427124023438 12 -59.539722442626946 13 -58.830577850341797
		 14 -55.411651611328125 15 -51.250148773193359 16 -48.1905517578125 17 -46.910606384277344
		 18 -44.888736724853516 19 -37.252433776855469 20 -25.720857620239258 21 -15.966061592102053
		 22 -7.3109240531921378 23 -0.28137323260307312 24 4.5642642974853516;
createNode animCurveTA -n "Character1_LeftArm_rotateZ";
	rename -uid "C5D0D1D6-4A8B-798F-1E68-8C9EAFA17A98";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -62.602993011474602 1 -64.448654174804687
		 2 -66.351974487304688 3 -67.950668334960937 4 -69.072235107421875 5 -73.34014892578125
		 6 -78.700736999511719 7 -78.844154357910156 8 -76.985984802246094 9 -74.087211608886719
		 10 -71.0758056640625 11 -69.117408752441406 12 -69.739532470703125 13 -71.709541320800781
		 14 -72.479804992675781 15 -72.271324157714844 16 -71.407478332519531 17 -74.783409118652344
		 18 -75.264472961425781 19 -62.368007659912102 20 -53.999248504638672 21 -55.858180999755859
		 22 -59.480792999267578 23 -62.325798034667976 24 -62.602993011474602;
createNode animCurveTU -n "Character1_LeftArm_visibility";
	rename -uid "0CC5EF71-437A-9D3A-7721-E58AB39DB901";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftForeArm_translateX";
	rename -uid "6727E275-400E-7AE1-5A43-36BB23078ED8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.0427684783935547;
createNode animCurveTL -n "Character1_LeftForeArm_translateY";
	rename -uid "1D7D812E-419B-1714-3A3A-399BF9BBD269";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.323208808898926;
createNode animCurveTL -n "Character1_LeftForeArm_translateZ";
	rename -uid "C68CA1EF-44B3-D0CC-8077-7A917E0141B2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 16.678167343139648;
createNode animCurveTU -n "Character1_LeftForeArm_scaleX";
	rename -uid "51ECC9E3-4538-A9B3-D437-589039116CDE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftForeArm_scaleY";
	rename -uid "0B9F3BAE-40CB-9FBC-158D-CFA30454D811";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftForeArm_scaleZ";
	rename -uid "C77B269B-4F47-D53D-D713-5F82A5B40D51";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftForeArm_rotateX";
	rename -uid "ED339434-4D65-7FA2-E324-D099401C0D11";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -30.450496673583988 1 -34.858234405517578
		 2 -38.318416595458984 3 -40.994285583496094 4 -43.025058746337891 5 -44.425128936767578
		 6 -44.460971832275391 7 -43.251941680908203 8 -41.333755493164063 9 -38.609798431396484
		 10 -34.974094390869141 11 -30.34220123291016 12 -24.688411712646484 13 -17.812437057495117
		 14 -10.522765159606934 15 -4.3499655723571777 16 -0.49924245476722717 17 1.5907005071640015
		 18 3.0455880165100098 19 4.0348248481750488 20 4.5729618072509766 21 0.1102999821305275
		 22 -11.255388259887695 23 -23.847332000732422 24 -30.450496673583988;
createNode animCurveTA -n "Character1_LeftForeArm_rotateY";
	rename -uid "3DC64092-477D-54DF-52B1-DB8CC6646C59";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -2.8856675624847412 1 -1.4835071563720703
		 2 -0.29018604755401611 3 0.82673037052154541 4 2.011481761932373 5 4.5809593200683594
		 6 6.0913329124450684 7 4.905817985534668 8 3.1945514678955078 9 1.1537137031555176
		 10 -0.97183221578598022 11 -2.8882865905761719 12 -4.2591571807861328 13 -4.7315335273742676
		 14 -3.8843238353729244 15 -2.02144455909729 16 -0.26341387629508972 17 0.90684229135513295
		 18 1.820054292678833 19 2.4903542995452881 20 2.8725941181182861 21 0.061337713152170181
		 22 -4.0335116386413574 23 -4.3835263252258301 24 -2.8856675624847412;
createNode animCurveTA -n "Character1_LeftForeArm_rotateZ";
	rename -uid "75A9D81F-4251-6299-771E-3EB75397317A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 46.134529113769531 1 53.525283813476562
		 2 60.253784179687507 3 65.779136657714844 4 69.519027709960938 5 70.469146728515625
		 6 69.523124694824219 7 67.589164733886719 8 64.245765686035156 9 59.522911071777344
		 10 53.447250366210937 11 46.055931091308594 12 37.421085357666016 13 27.166986465454102
		 14 16.268589019775391 15 6.8482818603515625 16 0.79670560359954834 17 -2.5708794593811035
		 18 -4.9589085578918457 19 -6.6064143180847168 20 -7.5115833282470712 21 -0.17852741479873657
		 22 17.371318817138672 23 36.161800384521484 24 46.134529113769531;
createNode animCurveTU -n "Character1_LeftForeArm_visibility";
	rename -uid "A715AEF1-48D4-E80C-7B27-AD99A21075EB";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHand_translateX";
	rename -uid "BA9550A2-4B65-013B-730D-E89F8851002E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.29200950264930725;
createNode animCurveTL -n "Character1_LeftHand_translateY";
	rename -uid "B79B3AE3-4A64-BF56-DF51-BFBBEF8F10A1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -14.295157432556152;
createNode animCurveTL -n "Character1_LeftHand_translateZ";
	rename -uid "0D28840E-4087-3CA1-F343-98A6D0D85C36";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6319241523742676;
createNode animCurveTU -n "Character1_LeftHand_scaleX";
	rename -uid "3B23C4D2-4436-A198-5159-0A9B91FAD01C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHand_scaleY";
	rename -uid "E447380A-4C57-7E24-FC8B-488E8C5F26FA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHand_scaleZ";
	rename -uid "2B33FE33-4FE1-A75C-DF94-599B93BE3811";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHand_rotateX";
	rename -uid "5959B46E-4363-A31C-2362-B78915B47C24";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  0 -7.0816669904161245e-005 1 2.2232139110565186
		 2 4.6916379928588867 3 7.3466787338256827 4 10.064920425415039 5 13.282389640808105
		 6 14.284409523010254 7 12.071635246276855 8 8.9600839614868164 9 5.6560745239257812
		 10 2.7651722431182861 11 0.75483518838882446 12 -7.0789232267998159e-005 13 -7.0791596954222769e-005;
createNode animCurveTA -n "Character1_LeftHand_rotateY";
	rename -uid "C191CBE7-45A4-3EA8-C46F-5B91189033D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  0 5.3468447447357903e-008 1 3.0660994052886963
		 2 6.0332083702087402 3 8.5147323608398437 4 10.155788421630859 5 9.8366203308105469
		 6 8.5537843704223633 7 7.5542144775390616 8 6.1135530471801758 9 4.2633471488952637
		 10 2.2815277576446533 11 0.66364538669586182 12 7.3684830681486346e-008 13 8.013473973278451e-008;
createNode animCurveTA -n "Character1_LeftHand_rotateZ";
	rename -uid "F1EEDD6E-4B54-BFBF-2B3F-7CB5574669A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  0 -0.00011949140753131358 1 2.251936674118042
		 2 4.5755820274353027 3 7.1822638511657706 4 10.226243019104004 5 15.673404693603516
		 6 18.73979377746582 7 16.311540603637695 8 12.467415809631348 9 8.1095352172851562
		 10 4.0801606178283691 11 1.1397452354431152 12 -0.00011946534505113958 13 -0.00011946552695007996;
createNode animCurveTU -n "Character1_LeftHand_visibility";
	rename -uid "FA3CC6A2-4B06-E926-2772-A69ABC90DE3C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateX";
	rename -uid "0D1CFA6A-40CB-2F3F-B1F3-F699F04D8B8C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1364259719848633;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateY";
	rename -uid "E50283DE-4897-DD7A-00DE-E1A4DD7F2A3E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4241994619369507;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateZ";
	rename -uid "03B0E30C-4D29-A34B-7907-86872D5C9A3D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.7211661338806152;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleX";
	rename -uid "EFC12B7F-4D5F-D260-716F-83A52FE31664";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleY";
	rename -uid "6894316D-47C4-7159-E178-5A9A623E0A90";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleZ";
	rename -uid "0C55A3A6-46C4-B062-4C94-559A9E6B3691";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateX";
	rename -uid "A1511315-467B-323E-9CE1-D592A962D0A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -35.857170104980469;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateY";
	rename -uid "E1A098B9-4CDA-4432-28CA-428E4E03F9E4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 58.664314270019524;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateZ";
	rename -uid "A58E9138-464E-B288-90D2-8983D8D4264D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.1192960739135742;
createNode animCurveTU -n "Character1_LeftHandThumb1_visibility";
	rename -uid "064C39A3-47BE-FA43-E455-92A276CC66F4";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateX";
	rename -uid "2CF9499E-4FF2-ABCD-CB10-C1A96574DF49";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.8329257965087891;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateY";
	rename -uid "BDE6AD3F-4AC7-74BA-3453-C58B5A1AF626";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.094426639378070831;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateZ";
	rename -uid "0CB7A99C-417D-7B1F-994E-3A880885646F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5438374280929565;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleX";
	rename -uid "8BE98A3B-4CDE-C045-F00E-62A7C2357E54";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleY";
	rename -uid "4B151293-4915-1D20-6235-EF8ABD59CCFE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleZ";
	rename -uid "D95B7E05-48EE-EE50-6908-94B90B84FF47";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateX";
	rename -uid "C94E218C-4AEC-AD91-C659-08B6EAF27E1E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.0229120254516602;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateY";
	rename -uid "FD0A3D64-402A-C1A0-E339-14A3B883768B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 55.072174072265625;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateZ";
	rename -uid "9319ADA3-4B86-E590-3657-A1928EA0DF39";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -27.805004119873047;
createNode animCurveTU -n "Character1_LeftHandThumb2_visibility";
	rename -uid "32677049-4EDE-BD0A-EA2D-26810A61A1CC";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateX";
	rename -uid "1497D494-4B1A-B8DE-7E80-C3938EF1F2B4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7747817039489746;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateY";
	rename -uid "11DDBD61-4A7D-659F-B1A4-41AAFB799D71";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3182404041290283;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateZ";
	rename -uid "7B1AEDBC-415F-3224-79C4-63AC5D06955A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.26324462890625;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleX";
	rename -uid "1051479E-453E-54D3-F117-28A34EAAA8D2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleY";
	rename -uid "B6F5363A-444C-84B1-EDD7-799BCE34FB79";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleZ";
	rename -uid "49A69BD8-421E-9D9F-18D9-099E7218877A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateX";
	rename -uid "CDC13088-4749-6BF6-22CC-5E8ED6A4FD0E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7086763882143714e-007;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateY";
	rename -uid "30D82750-4A0E-231C-901C-0EA710EDB906";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.878145318798488e-007;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateZ";
	rename -uid "0D17AE66-4B19-47DD-E451-01AE061504F2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7402099672381155e-007;
createNode animCurveTU -n "Character1_LeftHandThumb3_visibility";
	rename -uid "00F3CAA1-429B-44BC-74AE-0C977C1076D9";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateX";
	rename -uid "A7D9A88C-4E95-9EFF-7F83-E8BD7903D777";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.854037284851074;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateY";
	rename -uid "0E5676EA-450E-F52E-5E32-598E7B8DB92A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2616198062896729;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateZ";
	rename -uid "C3DCC320-4461-96D0-A28D-BEBC10376ADF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.944699764251709;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleX";
	rename -uid "DB928B02-44CE-C83B-0C6E-C99AE49EBCAD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleY";
	rename -uid "B66788F7-4655-8EE3-FAA6-1FAB5CF51A9E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleZ";
	rename -uid "0B7F5713-40E8-FD49-C514-E48021FF447A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateX";
	rename -uid "7A55D54C-4C53-6750-4103-F8877EA8EB02";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 60.010890960693366;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateY";
	rename -uid "D45673C1-4241-CBDA-C3D7-5CB7F50AC3D6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 33.363674163818359;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateZ";
	rename -uid "1DD0C7E3-47F9-283D-61AD-22A5D3CDCCB0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 86.987724304199219;
createNode animCurveTU -n "Character1_LeftHandIndex1_visibility";
	rename -uid "37DADB9F-4E3E-B226-91F3-0D953934F25F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateX";
	rename -uid "266BD471-448B-3EF8-D37F-9E98BFA6DEDE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.1721835136413574;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateY";
	rename -uid "FBAFFFC9-40BC-9AE9-B11A-3799933971A2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.9652466773986816;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateZ";
	rename -uid "4B82436E-495F-9758-EAE6-B982C877B5C7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.8871781826019287;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleX";
	rename -uid "9C8C9A5C-4005-461B-6F22-76915220B402";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleY";
	rename -uid "0E35809B-4D9F-24FC-E774-1C8499A19619";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleZ";
	rename -uid "6F917B74-48BA-18E6-49D2-F7BFB6F2BA08";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateX";
	rename -uid "4F0B81A0-461B-B2FE-FF38-B587AAB108A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.7885370254516602;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateY";
	rename -uid "8891793F-4730-0114-8355-BEB23C0E5362";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -33.620094299316406;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateZ";
	rename -uid "B9DA87BD-45D4-4FE7-1FED-6488FCAB1E7B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 80.491020202636719;
createNode animCurveTU -n "Character1_LeftHandIndex2_visibility";
	rename -uid "6AE9BD39-4B90-55DD-68B5-59BB32230009";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateX";
	rename -uid "4FE3D1F2-4365-C0E9-D7FB-91A48DF4E669";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.5761528015136719;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateY";
	rename -uid "9F17A21A-4A1A-B02D-BDD0-56A3160C09E2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8397388458251953;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateZ";
	rename -uid "53B78267-47DD-98E5-67F1-788F9775D615";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3069263696670532;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleX";
	rename -uid "F17615DA-45EF-1FE1-2BDA-60B24030E90F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleY";
	rename -uid "4586131B-425A-335C-E326-48AD1E77C744";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleZ";
	rename -uid "8E8DD9D1-44DD-F715-DDEE-549CD4B16EF3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateX";
	rename -uid "37BD8B3D-41B0-6ED9-09B1-778C7F619316";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0655053301888984e-006;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateY";
	rename -uid "63B20AB0-4D46-8A82-9043-409147EB1977";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.019683054437337e-006;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateZ";
	rename -uid "EE546390-4B96-8D51-D7CE-059628E00BD4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.567951231772895e-007;
createNode animCurveTU -n "Character1_LeftHandIndex3_visibility";
	rename -uid "A2D0E555-4F4E-AE9F-6A0B-36A1381905A8";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing1_translateX";
	rename -uid "3FDEE151-4807-34F0-6331-19AF56243DEA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4602031707763672;
createNode animCurveTL -n "Character1_LeftHandRing1_translateY";
	rename -uid "D7C334E8-4E02-F9D6-300A-9B86D712A3B2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.5015654563903809;
createNode animCurveTL -n "Character1_LeftHandRing1_translateZ";
	rename -uid "4ED595B5-4597-B56B-F4D0-3586FC5B3697";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.6178884506225586;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleX";
	rename -uid "04289A73-49FD-2215-12D9-019DBC314DFF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleY";
	rename -uid "BAEA5B10-4197-1620-EF83-5682E304DEC3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleZ";
	rename -uid "B297B290-402B-6BC7-8AAA-B18395871D21";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateX";
	rename -uid "6B037CC6-4399-DCEF-8167-FAB546E5D209";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 32.34600830078125;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateY";
	rename -uid "7D66F185-45A2-330D-7EEB-20BBCD869E2E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 35.989974975585938;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateZ";
	rename -uid "46047D15-4DA1-1670-0648-97BDFF89FA89";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 90.74188232421875;
createNode animCurveTU -n "Character1_LeftHandRing1_visibility";
	rename -uid "4FC3B0B3-45A0-0ED1-568B-16BC8ADF51CB";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing2_translateX";
	rename -uid "7A4BC6AE-4D78-68E0-40CF-249E3D8B8CB5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.9237353801727295;
createNode animCurveTL -n "Character1_LeftHandRing2_translateY";
	rename -uid "1893D1A7-4D72-B1E1-9F45-7697A09DD095";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.0094923973083496;
createNode animCurveTL -n "Character1_LeftHandRing2_translateZ";
	rename -uid "34BAEBE6-4911-48E1-31CB-DFA185BC0C61";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8221585750579834;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleX";
	rename -uid "848CE2CC-435F-A57F-FCC7-7EB5F518D91C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleY";
	rename -uid "31A67750-4525-125E-EE00-48A2B0CE138E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleZ";
	rename -uid "FF31EF9D-4981-C9CD-9313-0DA91477E99F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateX";
	rename -uid "D9AE5134-4DAA-6089-D682-46ABC3F7BB14";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 42.815162658691406;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateY";
	rename -uid "9DA75851-4478-03F8-6325-4DB62B3F0BAF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -31.412193298339847;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateZ";
	rename -uid "B5E7B8DD-4EC5-2581-E01D-6F9C21D10E76";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 62.220684051513665;
createNode animCurveTU -n "Character1_LeftHandRing2_visibility";
	rename -uid "C8BD6D91-447C-2AC3-1391-E2BE91297087";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing3_translateX";
	rename -uid "157D99DE-4ABD-FB8D-015B-5994EA4F9DCE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6098947525024414;
createNode animCurveTL -n "Character1_LeftHandRing3_translateY";
	rename -uid "9AAC0130-4969-36E0-C90A-DAB1D3BDF84C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2952473163604736;
createNode animCurveTL -n "Character1_LeftHandRing3_translateZ";
	rename -uid "0D2BAE5B-41DE-7950-DA1F-55AF38652788";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3554067611694336;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleX";
	rename -uid "8838AAAB-4EA1-4B81-6577-2193A77A3DC5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleY";
	rename -uid "58C6356A-41E9-8B4A-E1A2-EEA93E873E58";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleZ";
	rename -uid "AE0FC44E-4AF8-2ECF-E347-04888B7C397E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateX";
	rename -uid "987284B2-427D-777E-2529-ECA88B39D58E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4959583722884418e-006;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateY";
	rename -uid "1E6C25F2-43C6-CED1-F45B-C29579D6A465";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.0730574331319076e-006;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateZ";
	rename -uid "79F68578-4246-A7FA-4DAA-FCA7A36479DE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.2444955877508619e-007;
createNode animCurveTU -n "Character1_LeftHandRing3_visibility";
	rename -uid "B31B3C02-464E-7AED-1F15-419C70520697";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightShoulder_translateX";
	rename -uid "9B6207D5-4510-9A90-AA83-BC9A46608248";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 14.332768440246582;
createNode animCurveTL -n "Character1_RightShoulder_translateY";
	rename -uid "6D93423B-4B5A-6164-3B0B-1EBF079E99D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5798094272613525;
createNode animCurveTL -n "Character1_RightShoulder_translateZ";
	rename -uid "AD714734-443B-32A3-57EA-8E9876C7B3D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1086856126785278;
createNode animCurveTU -n "Character1_RightShoulder_scaleX";
	rename -uid "FA51D1CE-474C-5DB6-E9A6-519426952C10";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightShoulder_scaleY";
	rename -uid "D6109BEA-4F4F-D1B5-7E5F-EE86BA6B44EC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightShoulder_scaleZ";
	rename -uid "B9EB1BF4-4C38-04C5-1F86-56B34B8F6E81";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightShoulder_rotateX";
	rename -uid "65EA2F34-4031-BC59-DBD9-CABAF1D5F2A1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 17.548374176025391 1 17.611936569213867
		 2 18.023601531982422 3 19.147432327270508 4 21.348468780517578 5 27.937881469726563
		 6 33.9677734375 7 28.16650390625 8 21.053937911987305 9 21.235027313232422 10 22.657974243164063
		 11 24.730224609375 12 26.642221450805664 13 28.039291381835938 14 29.130403518676758
		 15 29.893363952636719 16 30.300025939941406 17 32.466167449951172 18 34.231906890869141
		 19 32.693885803222656 20 29.382110595703121 21 25.343597412109375 22 21.508827209472656
		 23 18.674491882324219 24 17.548374176025391;
createNode animCurveTA -n "Character1_RightShoulder_rotateY";
	rename -uid "01BD3CB6-4799-E350-0FE1-7783A20FA856";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -13.575275421142578 1 -13.073760032653809
		 2 -12.519068717956543 3 -11.588126182556152 4 -9.960963249206543 5 -3.9351074695587163
		 6 1.8050469160079954 7 -8.1663722991943359 8 -19.310348510742188 9 -21.838884353637695
		 10 -22.701919555664063 11 -22.505464553833008 12 -21.692197799682617 13 -20.584743499755859
		 14 -19.186328887939453 15 -17.427499771118164 16 -15.252397537231447 17 -11.269762992858887
		 18 -7.1547036170959473 19 -6.1853971481323242 20 -6.8889594078063965 21 -8.7457094192504883
		 22 -11.068076133728027 23 -12.996823310852051 24 -13.575275421142578;
createNode animCurveTA -n "Character1_RightShoulder_rotateZ";
	rename -uid "71F31247-4507-032F-567E-BF846BA78B30";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 22.027389526367188 1 21.515289306640625
		 2 21.270380020141602 3 21.384372711181641 4 21.969173431396484 5 23.544570922851563
		 6 25.450399398803711 7 25.224416732788086 8 29.169635772705078 9 33.475490570068359
		 10 36.736793518066406 11 38.866840362548828 12 39.916919708251953 13 40.225700378417969
		 14 40.076316833496094 15 39.426246643066406 16 38.221187591552734 17 37.032691955566406
		 18 36.036201477050781 19 34.308841705322266 20 31.589618682861328 21 28.466989517211914
		 22 25.528261184692383 23 23.252313613891602 24 22.027389526367188;
createNode animCurveTU -n "Character1_RightShoulder_visibility";
	rename -uid "73E8B907-4144-5D15-3A7E-7BA00300C71E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightArm_translateX";
	rename -uid "D733762D-40D8-3874-CB81-28BBA48C59BE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.8354010581970215;
createNode animCurveTL -n "Character1_RightArm_translateY";
	rename -uid "05C34D8A-43DF-BB52-C769-FB8D71976808";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1704782247543335;
createNode animCurveTL -n "Character1_RightArm_translateZ";
	rename -uid "E26455F9-4CE7-C8C3-0D3C-7BB408361ADC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.3259291648864746;
createNode animCurveTU -n "Character1_RightArm_scaleX";
	rename -uid "833DBC9E-4DBF-A3A0-49DD-8C9877742166";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightArm_scaleY";
	rename -uid "F5D2079D-4BA1-0379-AAD7-B594615CA631";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightArm_scaleZ";
	rename -uid "3283D42A-4CC7-06AA-4711-3FB6A3A46089";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightArm_rotateX";
	rename -uid "6A86518D-44EE-3965-5891-B9A397553A92";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 20.037164688110352 1 18.312002182006836
		 2 13.972700119018555 3 8.9772729873657227 4 5.1308703422546387 5 5.9012064933776855
		 6 7.8174004554748535 7 -10.316378593444824 8 -32.212028503417969 9 -49.120883941650391
		 10 -61.340908050537109 11 -69.608627319335937 12 -74.371391296386719 13 -76.159622192382813
		 14 -76.145286560058594 15 -75.423507690429688 16 -75.141242980957031 17 -70.58062744140625
		 18 -55.193641662597656 19 -39.1390380859375 20 -19.957983016967773 21 -3.3023333549499512
		 22 8.4352493286132812 23 15.878156661987306 24 20.037164688110352;
createNode animCurveTA -n "Character1_RightArm_rotateY";
	rename -uid "6B263845-4948-08BC-A9B6-48A2CB2AA323";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 45.628002166748047 1 44.010536193847656
		 2 42.921745300292969 3 42.513179779052734 4 42.809097290039063 5 48.776359558105469
		 6 53.394500732421875 7 46.257381439208984 8 37.061565399169922 9 32.780532836914063
		 10 28.156827926635742 11 24.741735458374023 12 23.421293258666992 13 24.063175201416016
		 14 25.85304069519043 15 28.294145584106445 16 30.828710556030273 17 40.575439453125
		 18 52.944019317626953 19 57.657344818115234 20 57.861946105957038 21 54.571422576904297
		 22 49.989097595214844 23 46.376346588134766 24 45.628002166748047;
createNode animCurveTA -n "Character1_RightArm_rotateZ";
	rename -uid "39704684-490B-E143-573C-AF96C2EA42A9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 9.36138916015625 1 11.410529136657715
		 2 12.856328964233398 3 14.151203155517578 4 15.532073974609377 5 18.649372100830078
		 6 21.510198593139648 7 5.0674490928649902 8 -14.787100791931152 9 -25.346847534179688
		 10 -31.114198684692383 11 -33.772018432617188 12 -34.765350341796875 13 -35.285743713378906
		 14 -35.875385284423828 15 -36.658107757568359 16 -37.850502014160156 17 -39.329696655273438
		 18 -33.746822357177734 19 -24.35429573059082 20 -12.025079727172852 21 -1.6223412752151489
		 22 4.8691701889038086 23 8.1958160400390625 24 9.36138916015625;
createNode animCurveTU -n "Character1_RightArm_visibility";
	rename -uid "F33FDA2F-4706-42B0-504F-8A9BC6D1EBB5";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightForeArm_translateX";
	rename -uid "13A8025C-4EFE-B54A-F372-C08C2644A809";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.753132820129395;
createNode animCurveTL -n "Character1_RightForeArm_translateY";
	rename -uid "181DDB12-4715-92CF-5168-9594CF650F2A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.862128257751465;
createNode animCurveTL -n "Character1_RightForeArm_translateZ";
	rename -uid "E56C26A2-453C-9112-2DD8-619D02AA39B8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.671875953674316;
createNode animCurveTU -n "Character1_RightForeArm_scaleX";
	rename -uid "F499D8D9-4E4B-B950-98C7-F699DE2F36CD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightForeArm_scaleY";
	rename -uid "68FA0B82-4F03-4C31-0C62-50B5FE0BD655";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightForeArm_scaleZ";
	rename -uid "2C1D4BDE-40B5-FB2F-CD6B-928F89954C90";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightForeArm_rotateX";
	rename -uid "4475F974-4DAD-3CF2-E099-2CB625F89FA0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 4.3292136192321777 1 4.2040185928344727
		 2 3.0058403015136719 3 1.3562666177749634 4 0.46180433034896851 5 0.36849182844161987
		 6 0.26460146903991699 7 -0.42656180262565613 8 -0.50945442914962769 9 0.38167569041252136
		 10 -0.41077473759651184 11 -5.8494915962219238 12 -12.519842147827148 13 -15.95261764526367
		 14 -19.661523818969727 15 -23.003591537475586 16 -25.21824836730957 17 -23.566743850708008
		 18 -19.694334030151367 19 -15.708967208862305 20 -10.714812278747559 21 -5.4786586761474609
		 22 -0.79228115081787109 23 2.6543800830841064 24 4.3292136192321777;
createNode animCurveTA -n "Character1_RightForeArm_rotateY";
	rename -uid "46785CF5-440B-7026-E852-8B9A3D4767C8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 21.688676834106445 1 13.983924865722656
		 2 6.1801857948303223 3 0.027317361906170845 4 -2.9913959503173828 5 -3.9429905414581299
		 6 -4.9571728706359863 7 -12.848021507263184 8 -14.712862014770506 9 -17.8394775390625
		 10 -15.70296096801758 11 -13.015432357788086 12 -13.22596549987793 13 -16.059427261352539
		 14 -17.928678512573242 15 -18.907295227050781 16 -18.932558059692383 17 -14.234536170959473
		 18 -8.7614479064941406 19 -1.7630535364151001 20 5.3895959854125977 21 11.616983413696289
		 22 16.398401260375977 23 19.709020614624023 24 21.688676834106445;
createNode animCurveTA -n "Character1_RightForeArm_rotateZ";
	rename -uid "29B21208-415A-39CF-456B-339A3E615531";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -34.540847778320312 1 -25.430257797241211
		 2 -16.442705154418945 3 -9.5597295761108398 4 -7.0651946067810059 5 -7.6755871772766104
		 6 -8.204833984375 7 -12.699062347412109 8 -13.234668731689453 9 -24.209966659545898
		 10 -43.986923217773438 11 -64.292900085449219 12 -78.646247863769531 13 -84.992691040039063
		 14 -90.496955871582031 15 -94.623519897460938 16 -96.933113098144531 17 -93.962333679199219
		 18 -87.53314208984375 19 -80.481521606445313 20 -70.617530822753906 21 -59.081409454345696
		 22 -47.716464996337891 23 -38.773239135742188 24 -34.540847778320312;
createNode animCurveTU -n "Character1_RightForeArm_visibility";
	rename -uid "506B98C1-46B2-A685-75B6-9398B5C6D59C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHand_translateX";
	rename -uid "75FA1B54-4F70-B228-4EE2-D78EC3FBCFC3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7167695760726929;
createNode animCurveTL -n "Character1_RightHand_translateY";
	rename -uid "B49FBA1D-46E7-1827-3E57-BCAC91315CAB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.314067840576172;
createNode animCurveTL -n "Character1_RightHand_translateZ";
	rename -uid "90B0F069-49ED-2054-2FAE-2CAF922DB7C3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4444055557250977;
createNode animCurveTU -n "Character1_RightHand_scaleX";
	rename -uid "A974C31A-45C5-DAF9-8FC3-6FB442E9E3D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightHand_scaleY";
	rename -uid "A85FEEB7-4404-C488-20A0-2698421383D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightHand_scaleZ";
	rename -uid "CCC8DE94-4FD5-CEAB-BCDC-D08B31005ED3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightHand_rotateX";
	rename -uid "E53B856B-4A20-0E21-2A29-AB82ECED484C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -4.9092583656311035 1 -4.253262996673584
		 2 -3.534224271774292 3 -2.7666492462158203 4 -1.9650555849075317 5 -1.1439710855484009
		 6 -0.3179323673248291 7 -13.002492904663086 8 -29.586967468261719 9 -25.873462677001953
		 10 -19.179609298706055 11 -11.573879241943359 12 -5.1309995651245117 13 0.061281673610210426
		 14 4.9085278511047363 15 8.8605003356933594 16 11.373315811157227 17 8.8608188629150391
		 18 4.6158266067504883 19 2.8187558650970459 20 1.0995916128158569 21 -0.53612810373306274
		 22 -2.0849082469940186 23 -3.5438904762268066 24 -4.9092583656311035;
createNode animCurveTA -n "Character1_RightHand_rotateY";
	rename -uid "1666AC95-4282-952B-D91D-34B39E8E8CAA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 3.1439776420593262 1 3.1439776420593262
		 2 3.1389346122741699 3 3.1320691108703613 4 3.123316764831543 5 3.1127288341522217
		 6 3.1004793643951416 7 4.8126330375671387 8 7.008455753326416 9 7.6383523941040039
		 10 7.6953897476196289 11 7.1934213638305673 12 6.5503396987915039 13 6.0573387145996094
		 14 5.5253520011901855 15 4.9876298904418945 16 4.5509047508239746 17 4.9814505577087402
		 18 5.4155926704406738 19 5.1460394859313965 20 4.7807493209838867 21 4.3694243431091309
		 22 3.9485743045806889 23 3.5394604206085205 24 3.1473755836486816;
createNode animCurveTA -n "Character1_RightHand_rotateZ";
	rename -uid "44CC6EC5-453E-2799-215E-6CB735177959";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 19.464714050292969 1 19.488006591796875
		 2 19.512004852294922 3 19.535820007324219 4 19.55866813659668 5 19.579891204833984
		 6 19.598979949951172 7 23.026748657226563 8 23.119386672973633 9 24.733325958251953
		 10 26.737205505371094 11 28.682056427001957 12 30.264646530151367 13 31.640720367431641
		 14 32.9022216796875 15 33.879497528076172 16 34.417224884033203 17 33.937412261962891
		 18 32.344398498535156 19 30.402070999145508 20 28.322246551513672 21 26.143548965454102
		 22 23.908229827880859 23 21.664104461669922 24 19.464714050292969;
createNode animCurveTU -n "Character1_RightHand_visibility";
	rename -uid "CB511C5A-48E4-0DD8-F621-48A1BB277DF6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb1_translateX";
	rename -uid "90E91D1F-4419-1281-C1A5-17840AB9D9F4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5741705894470215;
createNode animCurveTL -n "Character1_RightHandThumb1_translateY";
	rename -uid "02F8D605-45FA-7DC7-B3AC-6F92FE050986";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4327812194824219;
createNode animCurveTL -n "Character1_RightHandThumb1_translateZ";
	rename -uid "22078430-416F-47F1-F650-359626B85FB2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8196568489074707;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleX";
	rename -uid "C146CA38-4601-91F5-A42E-CFBDE37F31B9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleY";
	rename -uid "1AFA1F84-49FC-A11F-9F9A-76A58C134BC2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleZ";
	rename -uid "04165FF0-4EE7-D181-8351-2D8AD5CD92AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateX";
	rename -uid "255FA7A5-479C-7F28-392B-D9877C5B25F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 57.314090728759766;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateY";
	rename -uid "9A862559-48E0-4EFF-2D2D-B4BD42ABCAAB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.127543449401855;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateZ";
	rename -uid "E5743AB3-4873-C9DE-BA5F-88AB84CCB1B2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.751087188720703;
createNode animCurveTU -n "Character1_RightHandThumb1_visibility";
	rename -uid "FD627F7C-40C5-D4E4-FD8D-D8896D971AA1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb2_translateX";
	rename -uid "AA9E30BB-4691-C527-3A1F-928C1796507E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7422792911529541;
createNode animCurveTL -n "Character1_RightHandThumb2_translateY";
	rename -uid "23D3C1B0-4D70-3C78-EA5F-DF8E52FA3311";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5767025947570801;
createNode animCurveTL -n "Character1_RightHandThumb2_translateZ";
	rename -uid "2BC8AC52-40E6-2363-DCFA-19A81DA133B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0428388118743896;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleX";
	rename -uid "E432E07D-4E75-8C0D-CD72-8793902EF630";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleY";
	rename -uid "492CD840-4BBD-C4C0-8A68-5FAB17876E9B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleZ";
	rename -uid "E6E9F680-4E59-7BEC-3952-9D9A35B469E3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateX";
	rename -uid "0B90C4C4-4D37-6F77-32F2-3EA3115F7C2B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.273929595947266;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateY";
	rename -uid "81D1FA82-4724-BC5C-14F9-6582067AAAAC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -25.162002563476563;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateZ";
	rename -uid "7731A672-49E0-4021-8CF7-BE8891E6A647";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.82732087373733521;
createNode animCurveTU -n "Character1_RightHandThumb2_visibility";
	rename -uid "6DE7DD2E-48EC-E4B8-6EBC-24873BA7F7A7";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb3_translateX";
	rename -uid "5F3B5076-40ED-BDAC-D24E-8E982DFE5EF2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.588839054107666;
createNode animCurveTL -n "Character1_RightHandThumb3_translateY";
	rename -uid "A64F61BA-44F8-952A-3C4B-BE8A8003202C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.13479287922382355;
createNode animCurveTL -n "Character1_RightHandThumb3_translateZ";
	rename -uid "85F438CA-4933-04A7-9207-13BDB38CCBAC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.4058942794799805;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleX";
	rename -uid "3C1913DE-4F00-855A-6EFA-668313983E03";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleY";
	rename -uid "3FC98038-4187-84BA-F05B-A49D473D6174";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleZ";
	rename -uid "219B14E4-4933-5907-3BE4-A5BBD69A9531";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateX";
	rename -uid "E1085ABD-4AE7-570A-BF2E-B9A7931E7E0A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.0888210151024396e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateY";
	rename -uid "14196CFB-47BA-C84C-698B-46B352642DA3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.9615803138513002e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateZ";
	rename -uid "9CF60E38-489B-A2F9-CB02-3583569869FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.7458253864788276e-007;
createNode animCurveTU -n "Character1_RightHandThumb3_visibility";
	rename -uid "2A82ACD8-4CF2-66CC-E706-D9BA797876C2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex1_translateX";
	rename -uid "CCA9DCD5-40C0-1B22-FEE1-4C9D4D33A81D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.25625163316726685;
createNode animCurveTL -n "Character1_RightHandIndex1_translateY";
	rename -uid "4DE328A9-4DE8-2448-2A5F-8C8C61F5DD9E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.892666816711426;
createNode animCurveTL -n "Character1_RightHandIndex1_translateZ";
	rename -uid "F0614487-41DC-0208-1E07-C5941F836FE4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.533165454864502;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleX";
	rename -uid "E0DD2100-4B81-F520-86F2-DBA3E4C23554";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleY";
	rename -uid "7915E1D0-42D6-DF48-D47C-F4BA706ACDA4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleZ";
	rename -uid "139FB169-4B41-70B4-9F38-1BB0705C5D02";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateX";
	rename -uid "A8AD7AD6-4F12-F66F-78F0-FC8B84255C79";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2556072473526001;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateY";
	rename -uid "6D45182E-4145-B869-995C-BD80344B4397";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.129959106445312;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateZ";
	rename -uid "E9EC74BD-40C8-980D-5A33-8C84AF6E8C88";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 69.348670959472656;
createNode animCurveTU -n "Character1_RightHandIndex1_visibility";
	rename -uid "28883C54-41B0-AEBA-CF8F-F68AF8F215A3";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex2_translateX";
	rename -uid "9F900B87-45C2-7B6F-6F09-32B3308AE3A9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.3959341049194336;
createNode animCurveTL -n "Character1_RightHandIndex2_translateY";
	rename -uid "B5CEC434-436B-F38C-E82C-32ACE0383918";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8172028064727783;
createNode animCurveTL -n "Character1_RightHandIndex2_translateZ";
	rename -uid "92F386F2-4D7F-7EA7-8E94-3AA57F169239";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.52152138948440552;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleX";
	rename -uid "1B54273B-49EF-62E2-1189-B389947ADB1A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleY";
	rename -uid "DD496E83-434B-37A3-EB4F-979B558BCC09";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleZ";
	rename -uid "ECBFF851-46C1-A8D9-954D-07B72D7B7CC4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateX";
	rename -uid "1832233D-4DAF-5115-C37F-A49D9AABF69C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.230602264404295;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateY";
	rename -uid "12AF0071-4A12-AA67-8B4C-079297851FB8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -73.199981689453125;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateZ";
	rename -uid "4BEFA074-4B30-729B-2D32-1FA488B237F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 68.75714111328125;
createNode animCurveTU -n "Character1_RightHandIndex2_visibility";
	rename -uid "CA71EEFC-4F0A-53BC-0BBA-779FB9217742";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex3_translateX";
	rename -uid "08D97053-424C-EEB3-96C9-82AA1138E539";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6282989978790283;
createNode animCurveTL -n "Character1_RightHandIndex3_translateY";
	rename -uid "1AC6CAFE-48C3-C16E-C4ED-AFA117F8C86C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.2120883464813232;
createNode animCurveTL -n "Character1_RightHandIndex3_translateZ";
	rename -uid "85ECDAE2-417F-BBA6-A7F6-409F7A20BE02";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7757487297058105;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleX";
	rename -uid "7CFAAC59-439F-B2D1-6401-C6B46C28CEDC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleY";
	rename -uid "B036D83F-413D-E9A7-3114-7C9CE0E8CFEC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleZ";
	rename -uid "FBB4C4DA-4BAD-7882-6B48-FF9A0A8AF4E0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateX";
	rename -uid "DE534163-449C-CB35-85D6-8D874409EBEC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.0127865241192922e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateY";
	rename -uid "A4E56312-4389-4C9F-0FDD-5CBE7537617D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.4825916966619843e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateZ";
	rename -uid "2A16C332-42EF-8A23-C51E-C5B4A73684B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.1826871804696566e-007;
createNode animCurveTU -n "Character1_RightHandIndex3_visibility";
	rename -uid "4B69FB53-48E3-AF48-C214-FE94A9C4D035";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing1_translateX";
	rename -uid "E079332D-4A77-E475-86BD-21A2571BF25E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8543868064880371;
createNode animCurveTL -n "Character1_RightHandRing1_translateY";
	rename -uid "60816745-444D-729B-ED9F-869EAB800608";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2156095504760742;
createNode animCurveTL -n "Character1_RightHandRing1_translateZ";
	rename -uid "82F4DF1A-494C-9DDF-6737-F4A3723D61C4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2236912250518799;
createNode animCurveTU -n "Character1_RightHandRing1_scaleX";
	rename -uid "40D03D0D-40DD-0C73-1742-1A9A7C09B810";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandRing1_scaleY";
	rename -uid "882643E2-4F4C-B042-56EB-93A91F913F1C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing1_scaleZ";
	rename -uid "8BD4556F-465A-BBF5-C2FB-308D67D2DB51";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandRing1_rotateX";
	rename -uid "ACD606C5-4545-19E7-E0EC-E29A90C6E29B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.4753284454345703;
createNode animCurveTA -n "Character1_RightHandRing1_rotateY";
	rename -uid "87B306BA-4986-686E-9813-62A13BEFFA3E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.397130966186525;
createNode animCurveTA -n "Character1_RightHandRing1_rotateZ";
	rename -uid "13C6684E-4048-C04E-3241-A0ADF0B40F99";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 69.758003234863281;
createNode animCurveTU -n "Character1_RightHandRing1_visibility";
	rename -uid "4FBF59A6-40C7-0C5B-119F-AAA9062F2E66";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing2_translateX";
	rename -uid "D45635D4-4841-2F21-DC24-7A87521CBD70";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.062777042388916;
createNode animCurveTL -n "Character1_RightHandRing2_translateY";
	rename -uid "BA734D2D-473D-D0E5-0557-EFB486F744FD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.4930424690246582;
createNode animCurveTL -n "Character1_RightHandRing2_translateZ";
	rename -uid "928751D8-4CC9-E130-5D56-928024CCA2CA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.86127841472625732;
createNode animCurveTU -n "Character1_RightHandRing2_scaleX";
	rename -uid "ABCE958A-4A3B-0DAD-89A0-949ECF2AD3F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandRing2_scaleY";
	rename -uid "933F08C5-49BE-4749-9EA4-05B78F1D1F20";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing2_scaleZ";
	rename -uid "BBB70BFD-4686-BBE8-7621-F88CC693577C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandRing2_rotateX";
	rename -uid "75C0F0F3-40CB-3E4E-0309-C8BF385B1EB1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 14.933642387390137;
createNode animCurveTA -n "Character1_RightHandRing2_rotateY";
	rename -uid "58C7E1A4-4E68-8CDB-435B-CBBB82A8FE70";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -73.483367919921875;
createNode animCurveTA -n "Character1_RightHandRing2_rotateZ";
	rename -uid "7DB0D1DB-4762-8AC1-AB7F-01AED9BAFC91";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 68.698143005371094;
createNode animCurveTU -n "Character1_RightHandRing2_visibility";
	rename -uid "3E01221A-4653-2F16-C0C7-5BBAA35AB988";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing3_translateX";
	rename -uid "7D366EF0-496C-0546-509B-AF9D09BE6AB8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.5035178661346436;
createNode animCurveTL -n "Character1_RightHandRing3_translateY";
	rename -uid "93F34F5A-42F0-942C-F89A-A389E26B040C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.9153993129730225;
createNode animCurveTL -n "Character1_RightHandRing3_translateZ";
	rename -uid "05D076BD-40D3-2C08-AB56-D1A0FE816071";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.6898331642150879;
createNode animCurveTU -n "Character1_RightHandRing3_scaleX";
	rename -uid "CA27E4CD-4966-2EC4-DACD-C3A872CCD99F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing3_scaleY";
	rename -uid "A8D67F19-4B6D-B9DE-6E4D-FAA5B1CB89CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing3_scaleZ";
	rename -uid "2A9E6D84-47ED-632D-2297-E7A9F250A24D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandRing3_rotateX";
	rename -uid "DFD0F3D8-432E-FC3E-60E4-DC8E03FB646A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.3114324676498654e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateY";
	rename -uid "92200EC9-4FD7-941F-B561-E18CE09587B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.3115232750114956e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateZ";
	rename -uid "712A0CA3-45B2-5CC4-D548-5BA40A0983D2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.1999332489031073e-007;
createNode animCurveTU -n "Character1_RightHandRing3_visibility";
	rename -uid "3639B0A2-4773-75BC-B56E-AA8E343A5A8C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Neck_translateX";
	rename -uid "1F750F0C-4DD3-B0C4-9FA7-FFB7EFDE5DF3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 12.791294097900391;
createNode animCurveTL -n "Character1_Neck_translateY";
	rename -uid "0E12F351-461F-F169-919D-A1B3730FF377";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.3580474853515625;
createNode animCurveTL -n "Character1_Neck_translateZ";
	rename -uid "12E98C42-4ACA-63F4-5441-24876E028931";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.6322288513183594;
createNode animCurveTU -n "Character1_Neck_scaleX";
	rename -uid "E5802647-45C3-0AC1-DD9F-C7B2F0480498";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_Neck_scaleY";
	rename -uid "29F94F5E-48E6-B5E2-4C36-5E8E636BB0F6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Neck_scaleZ";
	rename -uid "99C32921-4F79-EEFE-A411-5990B5D21390";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Neck_rotateX";
	rename -uid "59F78889-470B-5C8F-99F9-A9B93FE83E0D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -16.374589920043945 1 -14.894152641296387
		 2 -13.30938720703125 3 -12.044380187988281 4 -11.181281089782715 5 -9.9879446029663086
		 6 -8.2419662475585938 7 -6.1533370018005371 8 -3.6213271617889404 9 -0.50847393274307251
		 10 2.7040154933929443 11 5.1936688423156738 12 5.9240460395812988 13 5.714076042175293
		 14 4.9855608940124512 15 4.5010719299316406 16 3.534435510635376 17 1.7673219442367554
		 18 -0.32992389798164368 19 -2.6676197052001953 20 -5.6515707969665527 21 -9.314173698425293
		 22 -12.82327938079834 23 -15.353951454162598 24 -16.374589920043945;
createNode animCurveTA -n "Character1_Neck_rotateY";
	rename -uid "1EF70A59-4363-A9B8-9EB6-7F93A9520FE8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -36.515621185302734 1 -35.590133666992187
		 2 -34.706298828125 3 -33.653533935546875 4 -32.305816650390625 5 -30.151102066040039
		 6 -26.847217559814453 7 -22.900415420532227 8 -18.988079071044922 9 -15.303924560546877
		 10 -11.896457672119141 11 -8.8501091003417969 12 -6.3937640190124512 13 -5.6176276206970215
		 14 -5.3325023651123047 15 -5.2519116401672363 16 -5.8268771171569824 17 -8.0877647399902344
		 18 -11.50052547454834 19 -15.652268409729004 20 -20.701854705810547 21 -26.360479354858398
		 22 -31.601591110229496 23 -35.369739532470703 24 -36.515621185302734;
createNode animCurveTA -n "Character1_Neck_rotateZ";
	rename -uid "E565D024-4856-D043-20EF-44A19D8D1610";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 4.8554172515869141 1 3.3596982955932617
		 2 1.5316125154495239 3 0.34196269512176514 4 -0.050852395594120026 5 0.25935620069503784
		 6 1.2653230428695679 7 2.5411250591278076 8 4.8134517669677734 9 8.2708988189697266
		 10 12.07420539855957 11 15.371041297912598 12 17.299919128417969 13 16.897916793823242
		 14 15.264448165893553 15 13.557321548461914 16 12.209662437438965 17 11.426353454589844
		 18 10.655228614807129 19 9.9040069580078125 20 9.0934925079345703 21 8.0840396881103516
		 22 6.9708352088928223 23 5.8318190574645996 24 4.8554172515869141;
createNode animCurveTU -n "Character1_Neck_visibility";
	rename -uid "97E3ED9D-40AA-09C8-7FB3-009157F126AE";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Head_translateX";
	rename -uid "3D4E3AC6-4CAC-6DC4-E97F-68B732B1DC2C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.627285957336426;
createNode animCurveTL -n "Character1_Head_translateY";
	rename -uid "601F3102-4530-8E2E-5FFB-7A8F2A6F352F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.64882755279541;
createNode animCurveTL -n "Character1_Head_translateZ";
	rename -uid "01A8BCA7-49FD-B0D0-F6EA-DC86DA3D184B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.1324095726013184;
createNode animCurveTU -n "Character1_Head_scaleX";
	rename -uid "D920D454-4EDC-08A1-CDD3-EB8458B93B98";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Head_scaleY";
	rename -uid "6B6D336E-45BE-0547-7982-BBA84D2B6C61";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Head_scaleZ";
	rename -uid "1E4B0B12-4836-0E9A-A8F2-49932DFB04FF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_Head_rotateX";
	rename -uid "AE31474F-4862-1C94-1713-F4A7D57FBEC7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 35.65313720703125 1 34.526725769042969
		 2 34.091373443603516 3 33.249790191650391 4 31.306209564208984 5 27.926862716674805
		 6 23.373527526855469 7 18.713335037231445 8 14.173210144042969 9 9.2602119445800781
		 10 4.2336835861206055 11 -0.55571955442428589 12 -4.7313971519470215 13 -5.830416202545166
		 14 -5.0645198822021484 15 -3.6671123504638672 16 -1.5563864707946777 17 1.1305398941040039
		 18 4.5809049606323242 19 8.5943460464477539 20 13.543275833129883 21 19.505035400390625
		 22 25.735891342163086 23 31.429719924926758 24 35.65313720703125;
createNode animCurveTA -n "Character1_Head_rotateY";
	rename -uid "009B272C-41DE-077B-11A5-968163D1B0F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -10.598852157592773 1 -10.706690788269043
		 2 -10.554043769836426 3 -10.154911041259766 4 -9.7356576919555664 5 -8.785426139831543
		 6 -7.4838666915893555 7 -6.4639110565185547 8 -5.3579401969909668 9 -3.4210741519927979
		 10 -0.83861422538757324 11 1.7576688528060913 12 3.3371469974517822 13 3.9922053813934331
		 14 3.7677335739135747 15 3.9832000732421871 16 3.8830139636993404 17 2.9994065761566162
		 18 1.5456689596176147 19 -0.091537885367870331 20 -2.0931015014648437 21 -4.6917152404785156
		 22 -7.3181729316711417 23 -9.3775777816772461 24 -10.598852157592773;
createNode animCurveTA -n "Character1_Head_rotateZ";
	rename -uid "08FBBAB1-4B10-17B4-40BB-A7BE5003CB94";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 12.660550117492676 1 10.127373695373535
		 2 7.6285762786865243 3 6.1436076164245605 4 5.6324787139892578 5 4.8392815589904785
		 6 3.889866828918457 7 4.091522216796875 8 6.3160915374755859 9 9.7060823440551758
		 10 13.339070320129395 11 16.317672729492188 12 17.933547973632812 13 17.418380737304688
		 14 15.965401649475098 15 14.533227920532225 16 13.391849517822266 17 13.261131286621094
		 18 13.859211921691895 19 14.299825668334961 20 14.435111045837402 21 14.315109252929687
		 22 13.781017303466797 23 13.052800178527832 24 12.660550117492676;
createNode animCurveTU -n "Character1_Head_visibility";
	rename -uid "6B1C6409-43E3-4A55-E3F5-D383B8566658";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "eyes_translateX";
	rename -uid "D62EC093-48A6-9B76-E2D5-90996E9E541B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 30.947149276733398;
createNode animCurveTL -n "eyes_translateY";
	rename -uid "B6037C29-43DB-476E-A8DD-FA97450D2F29";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.0992542810490704e-007;
createNode animCurveTL -n "eyes_translateZ";
	rename -uid "32A894B5-4704-B210-89A2-F48B4C042023";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.121469657048256e-013;
createNode animCurveTU -n "eyes_scaleX";
	rename -uid "E6D8230D-4368-E73C-3B4A-4690D0411B94";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleY";
	rename -uid "000E2AE0-47B0-C473-B03F-C3904100E79A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleZ";
	rename -uid "EE4A98AC-4900-1BB4-C3A3-3B925708F16C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "eyes_rotateX";
	rename -uid "8E2B0756-4515-6B3C-1921-259A6209CEFA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 2.1526193618774414 1 2.2433967590332031
		 2 2.331958532333374 3 2.4216279983520508 4 2.5157287120819092 5 2.6175839900970459
		 6 2.7405831813812256 7 2.8825104236602783 8 3.0232820510864258 9 3.1428139209747314
		 10 3.221022367477417 11 3.255906343460083 12 3.2608551979064941 13 3.23887038230896
		 14 3.1929526329040527 15 3.1261036396026611 16 3.0431046485900879 17 2.9483089447021484
		 18 2.8440759181976318 19 2.7327654361724854 20 2.6167361736297607 21 2.4983477592468262
		 22 2.3799591064453125 23 2.263930082321167 24 2.1526193618774414;
createNode animCurveTA -n "eyes_rotateY";
	rename -uid "32484241-4B07-2B90-1F51-12BD47F786E7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -1.5553674697875977 1 -1.6622269153594971
		 2 -1.764867901802063 3 -1.8696181774139404 4 -1.9828052520751953 5 -2.1107571125030518
		 6 -2.2766342163085937 7 -2.4762187004089355 8 -2.6747686862945557 9 -2.8375427722930908
		 10 -2.9297997951507568 11 -2.9366292953491211 12 -2.8811919689178467 13 -2.7858536243438721
		 14 -2.6729798316955566 15 -2.5649359226226807 16 -2.4638237953186035 17 -2.3575263023376465
		 18 -2.2470803260803223 19 -2.1335232257843018 20 -2.0178921222686768 21 -1.90122389793396
		 22 -1.7845556735992432 23 -1.6689245700836182 24 -1.5553674697875977;
createNode animCurveTA -n "eyes_rotateZ";
	rename -uid "661652DB-4548-3BE8-903C-199853B94245";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -3.9976339340209956 1 -4.0132813453674316
		 2 -4.0348138809204102 3 -4.0534043312072754 4 -4.0602221488952637 5 -4.0464410781860352
		 6 -3.994328498840332 7 -3.9097697734832764 8 -3.8193626403808594 9 -3.7497034072875977
		 10 -3.7273895740509033 11 -3.7746589183807369 12 -3.8737804889678951 13 -3.9913969039916992
		 14 -4.0941495895385742 15 -4.1486821174621582 16 -4.1604118347167969 17 -4.158775806427002
		 18 -4.1464476585388184 19 -4.1261005401611328 20 -4.1004071235656738 21 -4.0720405578613281
		 22 -4.0436744689941406 23 -4.0179810523986816 24 -3.9976339340209956;
createNode animCurveTU -n "eyes_visibility";
	rename -uid "6DCAC8C7-424A-577A-2984-969FFBF9662F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "jaw_translateX";
	rename -uid "F3F63E2B-4C55-2288-5108-92A2F560FC04";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.07279486209154129;
createNode animCurveTL -n "jaw_translateY";
	rename -uid "E158BEAE-497B-DA0A-268C-1FA373C26B42";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.25670051574707;
createNode animCurveTL -n "jaw_translateZ";
	rename -uid "D6F7B24B-4119-43FF-4E8E-FCB32A58DB7F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.2238426557523781e-007;
createNode animCurveTU -n "jaw_scaleX";
	rename -uid "FD8B086D-4745-F467-BDE7-2E9A74355B32";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleY";
	rename -uid "2235468B-4F21-9B70-D092-9384E26D5DE3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleZ";
	rename -uid "9AAF186F-4935-A58E-5877-6F8C746BC417";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "jaw_rotateX";
	rename -uid "53F0853C-473B-E8D7-0076-0A827ED95AE9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -48.89996337890625 1 -49.409698486328125
		 2 -49.933547973632813 3 -49.92730712890625 4 -49.635009765625 5 -49.664840698242187
		 6 -50.02734375 7 -50.727890014648438 8 -51.198623657226563 9 -50.562042236328125
		 10 -49.39813232421875 11 -48.800674438476562 12 -48.622161865234375 13 -49.044845581054688
		 14 -49.6116943359375 15 -49.55853271484375 16 -49.23583984375 17 -48.941650390625
		 18 -48.568527221679688 19 -48.135772705078125 20 -47.595916748046875 21 -47.204376220703125
		 22 -47.80560302734375 23 -48.44744873046875 24 -48.89996337890625;
createNode animCurveTA -n "jaw_rotateY";
	rename -uid "B9792335-48B1-8EED-6DCD-2484052B503E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -120.58699035644531 1 -120.66830444335939
		 2 -120.73927307128905 3 -120.73784637451172 4 -120.69905853271483 5 -120.70381927490234
		 6 -120.76945495605469 7 -120.90985107421876 8 -121.028564453125 9 -120.87687683105469
		 10 -120.6331024169922 11 -120.60601806640625 12 -120.66609191894531 13 -120.68508911132814
		 14 -120.69688415527342 15 -120.69473266601561 16 -120.67415618896484 17 -120.65868377685548
		 18 -120.64454650878908 19 -120.6240692138672 20 -120.56507873535155 21 -120.4628143310547
		 22 -120.45870971679687 23 -120.51919555664064 24 -120.58699035644531;
createNode animCurveTA -n "jaw_rotateZ";
	rename -uid "043951C9-4086-E929-3E50-E2936968A16E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 -45.085540771484375 1 -45.081207275390625
		 2 -45.059585571289063 3 -45.07666015625 4 -45.076705932617187 5 -45.0496826171875
		 6 -44.76690673828125 7 -44.117919921875 8 -43.053375244140625 9 -42.435409545898437
		 10 -43.522247314453125 11 -44.597183227539063 12 -45.182785034179688 13 -45.174392700195313
		 14 -45.09405517578125 15 -45.078720092773437 16 -45.261093139648438 17 -45.58636474609375
		 18 -46.11669921875 19 -46.686630249023438 20 -47.0426025390625 21 -46.956939697265625
		 22 -46.0994873046875 23 -45.43902587890625 24 -45.085540771484375;
createNode animCurveTU -n "jaw_visibility";
	rename -uid "915CDF24-4091-DBD0-0BAF-8EACB7E8779F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "222733BA-4663-BB90-B2C9-9B957AEBD82D";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "109E976B-498B-349C-7722-B9A22FC2E444";
createNode displayLayer -n "defaultLayer";
	rename -uid "8556C09C-41FF-97BC-0997-DABF24C1427E";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "C979AD4C-4646-142A-1D36-E38CC46CC127";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "3B4E9AAD-4D15-9361-D3AE-17AFB587ABCF";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "78460FDF-49C3-005A-9284-9181B42CD357";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1636\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1636\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1132\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1132\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 26 100 -ps 2 74 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1132\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1132\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "078CCC75-4340-973C-2C8D-2BA9869CD48F";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 24 -ast 0 -aet 250 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 0;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
connectAttr "ref_frame.s" "Character1_Hips.is";
connectAttr "Character1_Hips_scaleX.o" "Character1_Hips.sx";
connectAttr "Character1_Hips_scaleY.o" "Character1_Hips.sy";
connectAttr "Character1_Hips_scaleZ.o" "Character1_Hips.sz";
connectAttr "Character1_Hips_translateX.o" "Character1_Hips.tx";
connectAttr "Character1_Hips_translateY.o" "Character1_Hips.ty";
connectAttr "Character1_Hips_translateZ.o" "Character1_Hips.tz";
connectAttr "Character1_Hips_rotateX.o" "Character1_Hips.rx";
connectAttr "Character1_Hips_rotateY.o" "Character1_Hips.ry";
connectAttr "Character1_Hips_rotateZ.o" "Character1_Hips.rz";
connectAttr "Character1_Hips_visibility.o" "Character1_Hips.v";
connectAttr "Character1_Hips.s" "Character1_LeftUpLeg.is";
connectAttr "Character1_LeftUpLeg_scaleX.o" "Character1_LeftUpLeg.sx";
connectAttr "Character1_LeftUpLeg_scaleY.o" "Character1_LeftUpLeg.sy";
connectAttr "Character1_LeftUpLeg_scaleZ.o" "Character1_LeftUpLeg.sz";
connectAttr "Character1_LeftUpLeg_translateX.o" "Character1_LeftUpLeg.tx";
connectAttr "Character1_LeftUpLeg_translateY.o" "Character1_LeftUpLeg.ty";
connectAttr "Character1_LeftUpLeg_translateZ.o" "Character1_LeftUpLeg.tz";
connectAttr "Character1_LeftUpLeg_rotateX.o" "Character1_LeftUpLeg.rx";
connectAttr "Character1_LeftUpLeg_rotateY.o" "Character1_LeftUpLeg.ry";
connectAttr "Character1_LeftUpLeg_rotateZ.o" "Character1_LeftUpLeg.rz";
connectAttr "Character1_LeftUpLeg_visibility.o" "Character1_LeftUpLeg.v";
connectAttr "Character1_LeftUpLeg.s" "Character1_LeftLeg.is";
connectAttr "Character1_LeftLeg_scaleX.o" "Character1_LeftLeg.sx";
connectAttr "Character1_LeftLeg_scaleY.o" "Character1_LeftLeg.sy";
connectAttr "Character1_LeftLeg_scaleZ.o" "Character1_LeftLeg.sz";
connectAttr "Character1_LeftLeg_translateX.o" "Character1_LeftLeg.tx";
connectAttr "Character1_LeftLeg_translateY.o" "Character1_LeftLeg.ty";
connectAttr "Character1_LeftLeg_translateZ.o" "Character1_LeftLeg.tz";
connectAttr "Character1_LeftLeg_rotateX.o" "Character1_LeftLeg.rx";
connectAttr "Character1_LeftLeg_rotateY.o" "Character1_LeftLeg.ry";
connectAttr "Character1_LeftLeg_rotateZ.o" "Character1_LeftLeg.rz";
connectAttr "Character1_LeftLeg_visibility.o" "Character1_LeftLeg.v";
connectAttr "Character1_LeftLeg.s" "Character1_LeftFoot.is";
connectAttr "Character1_LeftFoot_scaleX.o" "Character1_LeftFoot.sx";
connectAttr "Character1_LeftFoot_scaleY.o" "Character1_LeftFoot.sy";
connectAttr "Character1_LeftFoot_scaleZ.o" "Character1_LeftFoot.sz";
connectAttr "Character1_LeftFoot_translateX.o" "Character1_LeftFoot.tx";
connectAttr "Character1_LeftFoot_translateY.o" "Character1_LeftFoot.ty";
connectAttr "Character1_LeftFoot_translateZ.o" "Character1_LeftFoot.tz";
connectAttr "Character1_LeftFoot_rotateX.o" "Character1_LeftFoot.rx";
connectAttr "Character1_LeftFoot_rotateY.o" "Character1_LeftFoot.ry";
connectAttr "Character1_LeftFoot_rotateZ.o" "Character1_LeftFoot.rz";
connectAttr "Character1_LeftFoot_visibility.o" "Character1_LeftFoot.v";
connectAttr "Character1_LeftFoot.s" "Character1_LeftToeBase.is";
connectAttr "Character1_LeftToeBase_scaleX.o" "Character1_LeftToeBase.sx";
connectAttr "Character1_LeftToeBase_scaleY.o" "Character1_LeftToeBase.sy";
connectAttr "Character1_LeftToeBase_scaleZ.o" "Character1_LeftToeBase.sz";
connectAttr "Character1_LeftToeBase_translateX.o" "Character1_LeftToeBase.tx";
connectAttr "Character1_LeftToeBase_translateY.o" "Character1_LeftToeBase.ty";
connectAttr "Character1_LeftToeBase_translateZ.o" "Character1_LeftToeBase.tz";
connectAttr "Character1_LeftToeBase_rotateX.o" "Character1_LeftToeBase.rx";
connectAttr "Character1_LeftToeBase_rotateY.o" "Character1_LeftToeBase.ry";
connectAttr "Character1_LeftToeBase_rotateZ.o" "Character1_LeftToeBase.rz";
connectAttr "Character1_LeftToeBase_visibility.o" "Character1_LeftToeBase.v";
connectAttr "Character1_LeftToeBase.s" "Character1_LeftFootMiddle1.is";
connectAttr "Character1_LeftFootMiddle1_scaleX.o" "Character1_LeftFootMiddle1.sx"
		;
connectAttr "Character1_LeftFootMiddle1_scaleY.o" "Character1_LeftFootMiddle1.sy"
		;
connectAttr "Character1_LeftFootMiddle1_scaleZ.o" "Character1_LeftFootMiddle1.sz"
		;
connectAttr "Character1_LeftFootMiddle1_translateX.o" "Character1_LeftFootMiddle1.tx"
		;
connectAttr "Character1_LeftFootMiddle1_translateY.o" "Character1_LeftFootMiddle1.ty"
		;
connectAttr "Character1_LeftFootMiddle1_translateZ.o" "Character1_LeftFootMiddle1.tz"
		;
connectAttr "Character1_LeftFootMiddle1_rotateX.o" "Character1_LeftFootMiddle1.rx"
		;
connectAttr "Character1_LeftFootMiddle1_rotateY.o" "Character1_LeftFootMiddle1.ry"
		;
connectAttr "Character1_LeftFootMiddle1_rotateZ.o" "Character1_LeftFootMiddle1.rz"
		;
connectAttr "Character1_LeftFootMiddle1_visibility.o" "Character1_LeftFootMiddle1.v"
		;
connectAttr "Character1_LeftFootMiddle1.s" "Character1_LeftFootMiddle2.is";
connectAttr "Character1_LeftFootMiddle2_translateX.o" "Character1_LeftFootMiddle2.tx"
		;
connectAttr "Character1_LeftFootMiddle2_translateY.o" "Character1_LeftFootMiddle2.ty"
		;
connectAttr "Character1_LeftFootMiddle2_translateZ.o" "Character1_LeftFootMiddle2.tz"
		;
connectAttr "Character1_LeftFootMiddle2_scaleX.o" "Character1_LeftFootMiddle2.sx"
		;
connectAttr "Character1_LeftFootMiddle2_scaleY.o" "Character1_LeftFootMiddle2.sy"
		;
connectAttr "Character1_LeftFootMiddle2_scaleZ.o" "Character1_LeftFootMiddle2.sz"
		;
connectAttr "Character1_LeftFootMiddle2_rotateX.o" "Character1_LeftFootMiddle2.rx"
		;
connectAttr "Character1_LeftFootMiddle2_rotateY.o" "Character1_LeftFootMiddle2.ry"
		;
connectAttr "Character1_LeftFootMiddle2_rotateZ.o" "Character1_LeftFootMiddle2.rz"
		;
connectAttr "Character1_LeftFootMiddle2_visibility.o" "Character1_LeftFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_RightUpLeg.is";
connectAttr "Character1_RightUpLeg_scaleX.o" "Character1_RightUpLeg.sx";
connectAttr "Character1_RightUpLeg_scaleY.o" "Character1_RightUpLeg.sy";
connectAttr "Character1_RightUpLeg_scaleZ.o" "Character1_RightUpLeg.sz";
connectAttr "Character1_RightUpLeg_translateX.o" "Character1_RightUpLeg.tx";
connectAttr "Character1_RightUpLeg_translateY.o" "Character1_RightUpLeg.ty";
connectAttr "Character1_RightUpLeg_translateZ.o" "Character1_RightUpLeg.tz";
connectAttr "Character1_RightUpLeg_rotateX.o" "Character1_RightUpLeg.rx";
connectAttr "Character1_RightUpLeg_rotateY.o" "Character1_RightUpLeg.ry";
connectAttr "Character1_RightUpLeg_rotateZ.o" "Character1_RightUpLeg.rz";
connectAttr "Character1_RightUpLeg_visibility.o" "Character1_RightUpLeg.v";
connectAttr "Character1_RightUpLeg.s" "Character1_RightLeg.is";
connectAttr "Character1_RightLeg_scaleX.o" "Character1_RightLeg.sx";
connectAttr "Character1_RightLeg_scaleY.o" "Character1_RightLeg.sy";
connectAttr "Character1_RightLeg_scaleZ.o" "Character1_RightLeg.sz";
connectAttr "Character1_RightLeg_translateX.o" "Character1_RightLeg.tx";
connectAttr "Character1_RightLeg_translateY.o" "Character1_RightLeg.ty";
connectAttr "Character1_RightLeg_translateZ.o" "Character1_RightLeg.tz";
connectAttr "Character1_RightLeg_rotateX.o" "Character1_RightLeg.rx";
connectAttr "Character1_RightLeg_rotateY.o" "Character1_RightLeg.ry";
connectAttr "Character1_RightLeg_rotateZ.o" "Character1_RightLeg.rz";
connectAttr "Character1_RightLeg_visibility.o" "Character1_RightLeg.v";
connectAttr "Character1_RightLeg.s" "Character1_RightFoot.is";
connectAttr "Character1_RightFoot_scaleX.o" "Character1_RightFoot.sx";
connectAttr "Character1_RightFoot_scaleY.o" "Character1_RightFoot.sy";
connectAttr "Character1_RightFoot_scaleZ.o" "Character1_RightFoot.sz";
connectAttr "Character1_RightFoot_translateX.o" "Character1_RightFoot.tx";
connectAttr "Character1_RightFoot_translateY.o" "Character1_RightFoot.ty";
connectAttr "Character1_RightFoot_translateZ.o" "Character1_RightFoot.tz";
connectAttr "Character1_RightFoot_rotateX.o" "Character1_RightFoot.rx";
connectAttr "Character1_RightFoot_rotateY.o" "Character1_RightFoot.ry";
connectAttr "Character1_RightFoot_rotateZ.o" "Character1_RightFoot.rz";
connectAttr "Character1_RightFoot_visibility.o" "Character1_RightFoot.v";
connectAttr "Character1_RightFoot.s" "Character1_RightToeBase.is";
connectAttr "Character1_RightToeBase_scaleX.o" "Character1_RightToeBase.sx";
connectAttr "Character1_RightToeBase_scaleY.o" "Character1_RightToeBase.sy";
connectAttr "Character1_RightToeBase_scaleZ.o" "Character1_RightToeBase.sz";
connectAttr "Character1_RightToeBase_translateX.o" "Character1_RightToeBase.tx";
connectAttr "Character1_RightToeBase_translateY.o" "Character1_RightToeBase.ty";
connectAttr "Character1_RightToeBase_translateZ.o" "Character1_RightToeBase.tz";
connectAttr "Character1_RightToeBase_rotateX.o" "Character1_RightToeBase.rx";
connectAttr "Character1_RightToeBase_rotateY.o" "Character1_RightToeBase.ry";
connectAttr "Character1_RightToeBase_rotateZ.o" "Character1_RightToeBase.rz";
connectAttr "Character1_RightToeBase_visibility.o" "Character1_RightToeBase.v";
connectAttr "Character1_RightToeBase.s" "Character1_RightFootMiddle1.is";
connectAttr "Character1_RightFootMiddle1_scaleX.o" "Character1_RightFootMiddle1.sx"
		;
connectAttr "Character1_RightFootMiddle1_scaleY.o" "Character1_RightFootMiddle1.sy"
		;
connectAttr "Character1_RightFootMiddle1_scaleZ.o" "Character1_RightFootMiddle1.sz"
		;
connectAttr "Character1_RightFootMiddle1_translateX.o" "Character1_RightFootMiddle1.tx"
		;
connectAttr "Character1_RightFootMiddle1_translateY.o" "Character1_RightFootMiddle1.ty"
		;
connectAttr "Character1_RightFootMiddle1_translateZ.o" "Character1_RightFootMiddle1.tz"
		;
connectAttr "Character1_RightFootMiddle1_rotateX.o" "Character1_RightFootMiddle1.rx"
		;
connectAttr "Character1_RightFootMiddle1_rotateY.o" "Character1_RightFootMiddle1.ry"
		;
connectAttr "Character1_RightFootMiddle1_rotateZ.o" "Character1_RightFootMiddle1.rz"
		;
connectAttr "Character1_RightFootMiddle1_visibility.o" "Character1_RightFootMiddle1.v"
		;
connectAttr "Character1_RightFootMiddle1.s" "Character1_RightFootMiddle2.is";
connectAttr "Character1_RightFootMiddle2_translateX.o" "Character1_RightFootMiddle2.tx"
		;
connectAttr "Character1_RightFootMiddle2_translateY.o" "Character1_RightFootMiddle2.ty"
		;
connectAttr "Character1_RightFootMiddle2_translateZ.o" "Character1_RightFootMiddle2.tz"
		;
connectAttr "Character1_RightFootMiddle2_scaleX.o" "Character1_RightFootMiddle2.sx"
		;
connectAttr "Character1_RightFootMiddle2_scaleY.o" "Character1_RightFootMiddle2.sy"
		;
connectAttr "Character1_RightFootMiddle2_scaleZ.o" "Character1_RightFootMiddle2.sz"
		;
connectAttr "Character1_RightFootMiddle2_rotateX.o" "Character1_RightFootMiddle2.rx"
		;
connectAttr "Character1_RightFootMiddle2_rotateY.o" "Character1_RightFootMiddle2.ry"
		;
connectAttr "Character1_RightFootMiddle2_rotateZ.o" "Character1_RightFootMiddle2.rz"
		;
connectAttr "Character1_RightFootMiddle2_visibility.o" "Character1_RightFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_Spine.is";
connectAttr "Character1_Spine_scaleX.o" "Character1_Spine.sx";
connectAttr "Character1_Spine_scaleY.o" "Character1_Spine.sy";
connectAttr "Character1_Spine_scaleZ.o" "Character1_Spine.sz";
connectAttr "Character1_Spine_translateX.o" "Character1_Spine.tx";
connectAttr "Character1_Spine_translateY.o" "Character1_Spine.ty";
connectAttr "Character1_Spine_translateZ.o" "Character1_Spine.tz";
connectAttr "Character1_Spine_rotateX.o" "Character1_Spine.rx";
connectAttr "Character1_Spine_rotateY.o" "Character1_Spine.ry";
connectAttr "Character1_Spine_rotateZ.o" "Character1_Spine.rz";
connectAttr "Character1_Spine_visibility.o" "Character1_Spine.v";
connectAttr "Character1_Spine.s" "Character1_Spine1.is";
connectAttr "Character1_Spine1_scaleX.o" "Character1_Spine1.sx";
connectAttr "Character1_Spine1_scaleY.o" "Character1_Spine1.sy";
connectAttr "Character1_Spine1_scaleZ.o" "Character1_Spine1.sz";
connectAttr "Character1_Spine1_translateX.o" "Character1_Spine1.tx";
connectAttr "Character1_Spine1_translateY.o" "Character1_Spine1.ty";
connectAttr "Character1_Spine1_translateZ.o" "Character1_Spine1.tz";
connectAttr "Character1_Spine1_rotateX.o" "Character1_Spine1.rx";
connectAttr "Character1_Spine1_rotateY.o" "Character1_Spine1.ry";
connectAttr "Character1_Spine1_rotateZ.o" "Character1_Spine1.rz";
connectAttr "Character1_Spine1_visibility.o" "Character1_Spine1.v";
connectAttr "Character1_Spine1.s" "Character1_LeftShoulder.is";
connectAttr "Character1_LeftShoulder_scaleX.o" "Character1_LeftShoulder.sx";
connectAttr "Character1_LeftShoulder_scaleY.o" "Character1_LeftShoulder.sy";
connectAttr "Character1_LeftShoulder_scaleZ.o" "Character1_LeftShoulder.sz";
connectAttr "Character1_LeftShoulder_translateX.o" "Character1_LeftShoulder.tx";
connectAttr "Character1_LeftShoulder_translateY.o" "Character1_LeftShoulder.ty";
connectAttr "Character1_LeftShoulder_translateZ.o" "Character1_LeftShoulder.tz";
connectAttr "Character1_LeftShoulder_rotateX.o" "Character1_LeftShoulder.rx";
connectAttr "Character1_LeftShoulder_rotateY.o" "Character1_LeftShoulder.ry";
connectAttr "Character1_LeftShoulder_rotateZ.o" "Character1_LeftShoulder.rz";
connectAttr "Character1_LeftShoulder_visibility.o" "Character1_LeftShoulder.v";
connectAttr "Character1_LeftShoulder.s" "Character1_LeftArm.is";
connectAttr "Character1_LeftArm_scaleX.o" "Character1_LeftArm.sx";
connectAttr "Character1_LeftArm_scaleY.o" "Character1_LeftArm.sy";
connectAttr "Character1_LeftArm_scaleZ.o" "Character1_LeftArm.sz";
connectAttr "Character1_LeftArm_translateX.o" "Character1_LeftArm.tx";
connectAttr "Character1_LeftArm_translateY.o" "Character1_LeftArm.ty";
connectAttr "Character1_LeftArm_translateZ.o" "Character1_LeftArm.tz";
connectAttr "Character1_LeftArm_rotateX.o" "Character1_LeftArm.rx";
connectAttr "Character1_LeftArm_rotateY.o" "Character1_LeftArm.ry";
connectAttr "Character1_LeftArm_rotateZ.o" "Character1_LeftArm.rz";
connectAttr "Character1_LeftArm_visibility.o" "Character1_LeftArm.v";
connectAttr "Character1_LeftArm.s" "Character1_LeftForeArm.is";
connectAttr "Character1_LeftForeArm_scaleX.o" "Character1_LeftForeArm.sx";
connectAttr "Character1_LeftForeArm_scaleY.o" "Character1_LeftForeArm.sy";
connectAttr "Character1_LeftForeArm_scaleZ.o" "Character1_LeftForeArm.sz";
connectAttr "Character1_LeftForeArm_translateX.o" "Character1_LeftForeArm.tx";
connectAttr "Character1_LeftForeArm_translateY.o" "Character1_LeftForeArm.ty";
connectAttr "Character1_LeftForeArm_translateZ.o" "Character1_LeftForeArm.tz";
connectAttr "Character1_LeftForeArm_rotateX.o" "Character1_LeftForeArm.rx";
connectAttr "Character1_LeftForeArm_rotateY.o" "Character1_LeftForeArm.ry";
connectAttr "Character1_LeftForeArm_rotateZ.o" "Character1_LeftForeArm.rz";
connectAttr "Character1_LeftForeArm_visibility.o" "Character1_LeftForeArm.v";
connectAttr "Character1_LeftForeArm.s" "Character1_LeftHand.is";
connectAttr "Character1_LeftHand_lockInfluenceWeights.o" "Character1_LeftHand.liw"
		;
connectAttr "Character1_LeftHand_scaleX.o" "Character1_LeftHand.sx";
connectAttr "Character1_LeftHand_scaleY.o" "Character1_LeftHand.sy";
connectAttr "Character1_LeftHand_scaleZ.o" "Character1_LeftHand.sz";
connectAttr "Character1_LeftHand_translateX.o" "Character1_LeftHand.tx";
connectAttr "Character1_LeftHand_translateY.o" "Character1_LeftHand.ty";
connectAttr "Character1_LeftHand_translateZ.o" "Character1_LeftHand.tz";
connectAttr "Character1_LeftHand_rotateX.o" "Character1_LeftHand.rx";
connectAttr "Character1_LeftHand_rotateY.o" "Character1_LeftHand.ry";
connectAttr "Character1_LeftHand_rotateZ.o" "Character1_LeftHand.rz";
connectAttr "Character1_LeftHand_visibility.o" "Character1_LeftHand.v";
connectAttr "Character1_LeftHand.s" "Character1_LeftHandThumb1.is";
connectAttr "Character1_LeftHandThumb1_scaleX.o" "Character1_LeftHandThumb1.sx";
connectAttr "Character1_LeftHandThumb1_scaleY.o" "Character1_LeftHandThumb1.sy";
connectAttr "Character1_LeftHandThumb1_scaleZ.o" "Character1_LeftHandThumb1.sz";
connectAttr "Character1_LeftHandThumb1_translateX.o" "Character1_LeftHandThumb1.tx"
		;
connectAttr "Character1_LeftHandThumb1_translateY.o" "Character1_LeftHandThumb1.ty"
		;
connectAttr "Character1_LeftHandThumb1_translateZ.o" "Character1_LeftHandThumb1.tz"
		;
connectAttr "Character1_LeftHandThumb1_rotateX.o" "Character1_LeftHandThumb1.rx"
		;
connectAttr "Character1_LeftHandThumb1_rotateY.o" "Character1_LeftHandThumb1.ry"
		;
connectAttr "Character1_LeftHandThumb1_rotateZ.o" "Character1_LeftHandThumb1.rz"
		;
connectAttr "Character1_LeftHandThumb1_visibility.o" "Character1_LeftHandThumb1.v"
		;
connectAttr "Character1_LeftHandThumb1.s" "Character1_LeftHandThumb2.is";
connectAttr "Character1_LeftHandThumb2_scaleX.o" "Character1_LeftHandThumb2.sx";
connectAttr "Character1_LeftHandThumb2_scaleY.o" "Character1_LeftHandThumb2.sy";
connectAttr "Character1_LeftHandThumb2_scaleZ.o" "Character1_LeftHandThumb2.sz";
connectAttr "Character1_LeftHandThumb2_translateX.o" "Character1_LeftHandThumb2.tx"
		;
connectAttr "Character1_LeftHandThumb2_translateY.o" "Character1_LeftHandThumb2.ty"
		;
connectAttr "Character1_LeftHandThumb2_translateZ.o" "Character1_LeftHandThumb2.tz"
		;
connectAttr "Character1_LeftHandThumb2_rotateX.o" "Character1_LeftHandThumb2.rx"
		;
connectAttr "Character1_LeftHandThumb2_rotateY.o" "Character1_LeftHandThumb2.ry"
		;
connectAttr "Character1_LeftHandThumb2_rotateZ.o" "Character1_LeftHandThumb2.rz"
		;
connectAttr "Character1_LeftHandThumb2_visibility.o" "Character1_LeftHandThumb2.v"
		;
connectAttr "Character1_LeftHandThumb2.s" "Character1_LeftHandThumb3.is";
connectAttr "Character1_LeftHandThumb3_translateX.o" "Character1_LeftHandThumb3.tx"
		;
connectAttr "Character1_LeftHandThumb3_translateY.o" "Character1_LeftHandThumb3.ty"
		;
connectAttr "Character1_LeftHandThumb3_translateZ.o" "Character1_LeftHandThumb3.tz"
		;
connectAttr "Character1_LeftHandThumb3_scaleX.o" "Character1_LeftHandThumb3.sx";
connectAttr "Character1_LeftHandThumb3_scaleY.o" "Character1_LeftHandThumb3.sy";
connectAttr "Character1_LeftHandThumb3_scaleZ.o" "Character1_LeftHandThumb3.sz";
connectAttr "Character1_LeftHandThumb3_rotateX.o" "Character1_LeftHandThumb3.rx"
		;
connectAttr "Character1_LeftHandThumb3_rotateY.o" "Character1_LeftHandThumb3.ry"
		;
connectAttr "Character1_LeftHandThumb3_rotateZ.o" "Character1_LeftHandThumb3.rz"
		;
connectAttr "Character1_LeftHandThumb3_visibility.o" "Character1_LeftHandThumb3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandIndex1.is";
connectAttr "Character1_LeftHandIndex1_scaleX.o" "Character1_LeftHandIndex1.sx";
connectAttr "Character1_LeftHandIndex1_scaleY.o" "Character1_LeftHandIndex1.sy";
connectAttr "Character1_LeftHandIndex1_scaleZ.o" "Character1_LeftHandIndex1.sz";
connectAttr "Character1_LeftHandIndex1_translateX.o" "Character1_LeftHandIndex1.tx"
		;
connectAttr "Character1_LeftHandIndex1_translateY.o" "Character1_LeftHandIndex1.ty"
		;
connectAttr "Character1_LeftHandIndex1_translateZ.o" "Character1_LeftHandIndex1.tz"
		;
connectAttr "Character1_LeftHandIndex1_rotateX.o" "Character1_LeftHandIndex1.rx"
		;
connectAttr "Character1_LeftHandIndex1_rotateY.o" "Character1_LeftHandIndex1.ry"
		;
connectAttr "Character1_LeftHandIndex1_rotateZ.o" "Character1_LeftHandIndex1.rz"
		;
connectAttr "Character1_LeftHandIndex1_visibility.o" "Character1_LeftHandIndex1.v"
		;
connectAttr "Character1_LeftHandIndex1.s" "Character1_LeftHandIndex2.is";
connectAttr "Character1_LeftHandIndex2_scaleX.o" "Character1_LeftHandIndex2.sx";
connectAttr "Character1_LeftHandIndex2_scaleY.o" "Character1_LeftHandIndex2.sy";
connectAttr "Character1_LeftHandIndex2_scaleZ.o" "Character1_LeftHandIndex2.sz";
connectAttr "Character1_LeftHandIndex2_translateX.o" "Character1_LeftHandIndex2.tx"
		;
connectAttr "Character1_LeftHandIndex2_translateY.o" "Character1_LeftHandIndex2.ty"
		;
connectAttr "Character1_LeftHandIndex2_translateZ.o" "Character1_LeftHandIndex2.tz"
		;
connectAttr "Character1_LeftHandIndex2_rotateX.o" "Character1_LeftHandIndex2.rx"
		;
connectAttr "Character1_LeftHandIndex2_rotateY.o" "Character1_LeftHandIndex2.ry"
		;
connectAttr "Character1_LeftHandIndex2_rotateZ.o" "Character1_LeftHandIndex2.rz"
		;
connectAttr "Character1_LeftHandIndex2_visibility.o" "Character1_LeftHandIndex2.v"
		;
connectAttr "Character1_LeftHandIndex2.s" "Character1_LeftHandIndex3.is";
connectAttr "Character1_LeftHandIndex3_translateX.o" "Character1_LeftHandIndex3.tx"
		;
connectAttr "Character1_LeftHandIndex3_translateY.o" "Character1_LeftHandIndex3.ty"
		;
connectAttr "Character1_LeftHandIndex3_translateZ.o" "Character1_LeftHandIndex3.tz"
		;
connectAttr "Character1_LeftHandIndex3_scaleX.o" "Character1_LeftHandIndex3.sx";
connectAttr "Character1_LeftHandIndex3_scaleY.o" "Character1_LeftHandIndex3.sy";
connectAttr "Character1_LeftHandIndex3_scaleZ.o" "Character1_LeftHandIndex3.sz";
connectAttr "Character1_LeftHandIndex3_rotateX.o" "Character1_LeftHandIndex3.rx"
		;
connectAttr "Character1_LeftHandIndex3_rotateY.o" "Character1_LeftHandIndex3.ry"
		;
connectAttr "Character1_LeftHandIndex3_rotateZ.o" "Character1_LeftHandIndex3.rz"
		;
connectAttr "Character1_LeftHandIndex3_visibility.o" "Character1_LeftHandIndex3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandRing1.is";
connectAttr "Character1_LeftHandRing1_scaleX.o" "Character1_LeftHandRing1.sx";
connectAttr "Character1_LeftHandRing1_scaleY.o" "Character1_LeftHandRing1.sy";
connectAttr "Character1_LeftHandRing1_scaleZ.o" "Character1_LeftHandRing1.sz";
connectAttr "Character1_LeftHandRing1_translateX.o" "Character1_LeftHandRing1.tx"
		;
connectAttr "Character1_LeftHandRing1_translateY.o" "Character1_LeftHandRing1.ty"
		;
connectAttr "Character1_LeftHandRing1_translateZ.o" "Character1_LeftHandRing1.tz"
		;
connectAttr "Character1_LeftHandRing1_rotateX.o" "Character1_LeftHandRing1.rx";
connectAttr "Character1_LeftHandRing1_rotateY.o" "Character1_LeftHandRing1.ry";
connectAttr "Character1_LeftHandRing1_rotateZ.o" "Character1_LeftHandRing1.rz";
connectAttr "Character1_LeftHandRing1_visibility.o" "Character1_LeftHandRing1.v"
		;
connectAttr "Character1_LeftHandRing1.s" "Character1_LeftHandRing2.is";
connectAttr "Character1_LeftHandRing2_scaleX.o" "Character1_LeftHandRing2.sx";
connectAttr "Character1_LeftHandRing2_scaleY.o" "Character1_LeftHandRing2.sy";
connectAttr "Character1_LeftHandRing2_scaleZ.o" "Character1_LeftHandRing2.sz";
connectAttr "Character1_LeftHandRing2_translateX.o" "Character1_LeftHandRing2.tx"
		;
connectAttr "Character1_LeftHandRing2_translateY.o" "Character1_LeftHandRing2.ty"
		;
connectAttr "Character1_LeftHandRing2_translateZ.o" "Character1_LeftHandRing2.tz"
		;
connectAttr "Character1_LeftHandRing2_rotateX.o" "Character1_LeftHandRing2.rx";
connectAttr "Character1_LeftHandRing2_rotateY.o" "Character1_LeftHandRing2.ry";
connectAttr "Character1_LeftHandRing2_rotateZ.o" "Character1_LeftHandRing2.rz";
connectAttr "Character1_LeftHandRing2_visibility.o" "Character1_LeftHandRing2.v"
		;
connectAttr "Character1_LeftHandRing2.s" "Character1_LeftHandRing3.is";
connectAttr "Character1_LeftHandRing3_translateX.o" "Character1_LeftHandRing3.tx"
		;
connectAttr "Character1_LeftHandRing3_translateY.o" "Character1_LeftHandRing3.ty"
		;
connectAttr "Character1_LeftHandRing3_translateZ.o" "Character1_LeftHandRing3.tz"
		;
connectAttr "Character1_LeftHandRing3_scaleX.o" "Character1_LeftHandRing3.sx";
connectAttr "Character1_LeftHandRing3_scaleY.o" "Character1_LeftHandRing3.sy";
connectAttr "Character1_LeftHandRing3_scaleZ.o" "Character1_LeftHandRing3.sz";
connectAttr "Character1_LeftHandRing3_rotateX.o" "Character1_LeftHandRing3.rx";
connectAttr "Character1_LeftHandRing3_rotateY.o" "Character1_LeftHandRing3.ry";
connectAttr "Character1_LeftHandRing3_rotateZ.o" "Character1_LeftHandRing3.rz";
connectAttr "Character1_LeftHandRing3_visibility.o" "Character1_LeftHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_RightShoulder.is";
connectAttr "Character1_RightShoulder_scaleX.o" "Character1_RightShoulder.sx";
connectAttr "Character1_RightShoulder_scaleY.o" "Character1_RightShoulder.sy";
connectAttr "Character1_RightShoulder_scaleZ.o" "Character1_RightShoulder.sz";
connectAttr "Character1_RightShoulder_translateX.o" "Character1_RightShoulder.tx"
		;
connectAttr "Character1_RightShoulder_translateY.o" "Character1_RightShoulder.ty"
		;
connectAttr "Character1_RightShoulder_translateZ.o" "Character1_RightShoulder.tz"
		;
connectAttr "Character1_RightShoulder_rotateX.o" "Character1_RightShoulder.rx";
connectAttr "Character1_RightShoulder_rotateY.o" "Character1_RightShoulder.ry";
connectAttr "Character1_RightShoulder_rotateZ.o" "Character1_RightShoulder.rz";
connectAttr "Character1_RightShoulder_visibility.o" "Character1_RightShoulder.v"
		;
connectAttr "Character1_RightShoulder.s" "Character1_RightArm.is";
connectAttr "Character1_RightArm_scaleX.o" "Character1_RightArm.sx";
connectAttr "Character1_RightArm_scaleY.o" "Character1_RightArm.sy";
connectAttr "Character1_RightArm_scaleZ.o" "Character1_RightArm.sz";
connectAttr "Character1_RightArm_translateX.o" "Character1_RightArm.tx";
connectAttr "Character1_RightArm_translateY.o" "Character1_RightArm.ty";
connectAttr "Character1_RightArm_translateZ.o" "Character1_RightArm.tz";
connectAttr "Character1_RightArm_rotateX.o" "Character1_RightArm.rx";
connectAttr "Character1_RightArm_rotateY.o" "Character1_RightArm.ry";
connectAttr "Character1_RightArm_rotateZ.o" "Character1_RightArm.rz";
connectAttr "Character1_RightArm_visibility.o" "Character1_RightArm.v";
connectAttr "Character1_RightArm.s" "Character1_RightForeArm.is";
connectAttr "Character1_RightForeArm_scaleX.o" "Character1_RightForeArm.sx";
connectAttr "Character1_RightForeArm_scaleY.o" "Character1_RightForeArm.sy";
connectAttr "Character1_RightForeArm_scaleZ.o" "Character1_RightForeArm.sz";
connectAttr "Character1_RightForeArm_translateX.o" "Character1_RightForeArm.tx";
connectAttr "Character1_RightForeArm_translateY.o" "Character1_RightForeArm.ty";
connectAttr "Character1_RightForeArm_translateZ.o" "Character1_RightForeArm.tz";
connectAttr "Character1_RightForeArm_rotateX.o" "Character1_RightForeArm.rx";
connectAttr "Character1_RightForeArm_rotateY.o" "Character1_RightForeArm.ry";
connectAttr "Character1_RightForeArm_rotateZ.o" "Character1_RightForeArm.rz";
connectAttr "Character1_RightForeArm_visibility.o" "Character1_RightForeArm.v";
connectAttr "Character1_RightForeArm.s" "Character1_RightHand.is";
connectAttr "Character1_RightHand_scaleX.o" "Character1_RightHand.sx";
connectAttr "Character1_RightHand_scaleY.o" "Character1_RightHand.sy";
connectAttr "Character1_RightHand_scaleZ.o" "Character1_RightHand.sz";
connectAttr "Character1_RightHand_translateX.o" "Character1_RightHand.tx";
connectAttr "Character1_RightHand_translateY.o" "Character1_RightHand.ty";
connectAttr "Character1_RightHand_translateZ.o" "Character1_RightHand.tz";
connectAttr "Character1_RightHand_rotateX.o" "Character1_RightHand.rx";
connectAttr "Character1_RightHand_rotateY.o" "Character1_RightHand.ry";
connectAttr "Character1_RightHand_rotateZ.o" "Character1_RightHand.rz";
connectAttr "Character1_RightHand_visibility.o" "Character1_RightHand.v";
connectAttr "Character1_RightHand.s" "Character1_RightHandThumb1.is";
connectAttr "Character1_RightHandThumb1_scaleX.o" "Character1_RightHandThumb1.sx"
		;
connectAttr "Character1_RightHandThumb1_scaleY.o" "Character1_RightHandThumb1.sy"
		;
connectAttr "Character1_RightHandThumb1_scaleZ.o" "Character1_RightHandThumb1.sz"
		;
connectAttr "Character1_RightHandThumb1_translateX.o" "Character1_RightHandThumb1.tx"
		;
connectAttr "Character1_RightHandThumb1_translateY.o" "Character1_RightHandThumb1.ty"
		;
connectAttr "Character1_RightHandThumb1_translateZ.o" "Character1_RightHandThumb1.tz"
		;
connectAttr "Character1_RightHandThumb1_rotateX.o" "Character1_RightHandThumb1.rx"
		;
connectAttr "Character1_RightHandThumb1_rotateY.o" "Character1_RightHandThumb1.ry"
		;
connectAttr "Character1_RightHandThumb1_rotateZ.o" "Character1_RightHandThumb1.rz"
		;
connectAttr "Character1_RightHandThumb1_visibility.o" "Character1_RightHandThumb1.v"
		;
connectAttr "Character1_RightHandThumb1.s" "Character1_RightHandThumb2.is";
connectAttr "Character1_RightHandThumb2_scaleX.o" "Character1_RightHandThumb2.sx"
		;
connectAttr "Character1_RightHandThumb2_scaleY.o" "Character1_RightHandThumb2.sy"
		;
connectAttr "Character1_RightHandThumb2_scaleZ.o" "Character1_RightHandThumb2.sz"
		;
connectAttr "Character1_RightHandThumb2_translateX.o" "Character1_RightHandThumb2.tx"
		;
connectAttr "Character1_RightHandThumb2_translateY.o" "Character1_RightHandThumb2.ty"
		;
connectAttr "Character1_RightHandThumb2_translateZ.o" "Character1_RightHandThumb2.tz"
		;
connectAttr "Character1_RightHandThumb2_rotateX.o" "Character1_RightHandThumb2.rx"
		;
connectAttr "Character1_RightHandThumb2_rotateY.o" "Character1_RightHandThumb2.ry"
		;
connectAttr "Character1_RightHandThumb2_rotateZ.o" "Character1_RightHandThumb2.rz"
		;
connectAttr "Character1_RightHandThumb2_visibility.o" "Character1_RightHandThumb2.v"
		;
connectAttr "Character1_RightHandThumb2.s" "Character1_RightHandThumb3.is";
connectAttr "Character1_RightHandThumb3_translateX.o" "Character1_RightHandThumb3.tx"
		;
connectAttr "Character1_RightHandThumb3_translateY.o" "Character1_RightHandThumb3.ty"
		;
connectAttr "Character1_RightHandThumb3_translateZ.o" "Character1_RightHandThumb3.tz"
		;
connectAttr "Character1_RightHandThumb3_scaleX.o" "Character1_RightHandThumb3.sx"
		;
connectAttr "Character1_RightHandThumb3_scaleY.o" "Character1_RightHandThumb3.sy"
		;
connectAttr "Character1_RightHandThumb3_scaleZ.o" "Character1_RightHandThumb3.sz"
		;
connectAttr "Character1_RightHandThumb3_rotateX.o" "Character1_RightHandThumb3.rx"
		;
connectAttr "Character1_RightHandThumb3_rotateY.o" "Character1_RightHandThumb3.ry"
		;
connectAttr "Character1_RightHandThumb3_rotateZ.o" "Character1_RightHandThumb3.rz"
		;
connectAttr "Character1_RightHandThumb3_visibility.o" "Character1_RightHandThumb3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandIndex1.is";
connectAttr "Character1_RightHandIndex1_scaleX.o" "Character1_RightHandIndex1.sx"
		;
connectAttr "Character1_RightHandIndex1_scaleY.o" "Character1_RightHandIndex1.sy"
		;
connectAttr "Character1_RightHandIndex1_scaleZ.o" "Character1_RightHandIndex1.sz"
		;
connectAttr "Character1_RightHandIndex1_translateX.o" "Character1_RightHandIndex1.tx"
		;
connectAttr "Character1_RightHandIndex1_translateY.o" "Character1_RightHandIndex1.ty"
		;
connectAttr "Character1_RightHandIndex1_translateZ.o" "Character1_RightHandIndex1.tz"
		;
connectAttr "Character1_RightHandIndex1_rotateX.o" "Character1_RightHandIndex1.rx"
		;
connectAttr "Character1_RightHandIndex1_rotateY.o" "Character1_RightHandIndex1.ry"
		;
connectAttr "Character1_RightHandIndex1_rotateZ.o" "Character1_RightHandIndex1.rz"
		;
connectAttr "Character1_RightHandIndex1_visibility.o" "Character1_RightHandIndex1.v"
		;
connectAttr "Character1_RightHandIndex1.s" "Character1_RightHandIndex2.is";
connectAttr "Character1_RightHandIndex2_scaleX.o" "Character1_RightHandIndex2.sx"
		;
connectAttr "Character1_RightHandIndex2_scaleY.o" "Character1_RightHandIndex2.sy"
		;
connectAttr "Character1_RightHandIndex2_scaleZ.o" "Character1_RightHandIndex2.sz"
		;
connectAttr "Character1_RightHandIndex2_translateX.o" "Character1_RightHandIndex2.tx"
		;
connectAttr "Character1_RightHandIndex2_translateY.o" "Character1_RightHandIndex2.ty"
		;
connectAttr "Character1_RightHandIndex2_translateZ.o" "Character1_RightHandIndex2.tz"
		;
connectAttr "Character1_RightHandIndex2_rotateX.o" "Character1_RightHandIndex2.rx"
		;
connectAttr "Character1_RightHandIndex2_rotateY.o" "Character1_RightHandIndex2.ry"
		;
connectAttr "Character1_RightHandIndex2_rotateZ.o" "Character1_RightHandIndex2.rz"
		;
connectAttr "Character1_RightHandIndex2_visibility.o" "Character1_RightHandIndex2.v"
		;
connectAttr "Character1_RightHandIndex2.s" "Character1_RightHandIndex3.is";
connectAttr "Character1_RightHandIndex3_translateX.o" "Character1_RightHandIndex3.tx"
		;
connectAttr "Character1_RightHandIndex3_translateY.o" "Character1_RightHandIndex3.ty"
		;
connectAttr "Character1_RightHandIndex3_translateZ.o" "Character1_RightHandIndex3.tz"
		;
connectAttr "Character1_RightHandIndex3_scaleX.o" "Character1_RightHandIndex3.sx"
		;
connectAttr "Character1_RightHandIndex3_scaleY.o" "Character1_RightHandIndex3.sy"
		;
connectAttr "Character1_RightHandIndex3_scaleZ.o" "Character1_RightHandIndex3.sz"
		;
connectAttr "Character1_RightHandIndex3_rotateX.o" "Character1_RightHandIndex3.rx"
		;
connectAttr "Character1_RightHandIndex3_rotateY.o" "Character1_RightHandIndex3.ry"
		;
connectAttr "Character1_RightHandIndex3_rotateZ.o" "Character1_RightHandIndex3.rz"
		;
connectAttr "Character1_RightHandIndex3_visibility.o" "Character1_RightHandIndex3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandRing1.is";
connectAttr "Character1_RightHandRing1_scaleX.o" "Character1_RightHandRing1.sx";
connectAttr "Character1_RightHandRing1_scaleY.o" "Character1_RightHandRing1.sy";
connectAttr "Character1_RightHandRing1_scaleZ.o" "Character1_RightHandRing1.sz";
connectAttr "Character1_RightHandRing1_translateX.o" "Character1_RightHandRing1.tx"
		;
connectAttr "Character1_RightHandRing1_translateY.o" "Character1_RightHandRing1.ty"
		;
connectAttr "Character1_RightHandRing1_translateZ.o" "Character1_RightHandRing1.tz"
		;
connectAttr "Character1_RightHandRing1_rotateX.o" "Character1_RightHandRing1.rx"
		;
connectAttr "Character1_RightHandRing1_rotateY.o" "Character1_RightHandRing1.ry"
		;
connectAttr "Character1_RightHandRing1_rotateZ.o" "Character1_RightHandRing1.rz"
		;
connectAttr "Character1_RightHandRing1_visibility.o" "Character1_RightHandRing1.v"
		;
connectAttr "Character1_RightHandRing1.s" "Character1_RightHandRing2.is";
connectAttr "Character1_RightHandRing2_scaleX.o" "Character1_RightHandRing2.sx";
connectAttr "Character1_RightHandRing2_scaleY.o" "Character1_RightHandRing2.sy";
connectAttr "Character1_RightHandRing2_scaleZ.o" "Character1_RightHandRing2.sz";
connectAttr "Character1_RightHandRing2_translateX.o" "Character1_RightHandRing2.tx"
		;
connectAttr "Character1_RightHandRing2_translateY.o" "Character1_RightHandRing2.ty"
		;
connectAttr "Character1_RightHandRing2_translateZ.o" "Character1_RightHandRing2.tz"
		;
connectAttr "Character1_RightHandRing2_rotateX.o" "Character1_RightHandRing2.rx"
		;
connectAttr "Character1_RightHandRing2_rotateY.o" "Character1_RightHandRing2.ry"
		;
connectAttr "Character1_RightHandRing2_rotateZ.o" "Character1_RightHandRing2.rz"
		;
connectAttr "Character1_RightHandRing2_visibility.o" "Character1_RightHandRing2.v"
		;
connectAttr "Character1_RightHandRing2.s" "Character1_RightHandRing3.is";
connectAttr "Character1_RightHandRing3_translateX.o" "Character1_RightHandRing3.tx"
		;
connectAttr "Character1_RightHandRing3_translateY.o" "Character1_RightHandRing3.ty"
		;
connectAttr "Character1_RightHandRing3_translateZ.o" "Character1_RightHandRing3.tz"
		;
connectAttr "Character1_RightHandRing3_scaleX.o" "Character1_RightHandRing3.sx";
connectAttr "Character1_RightHandRing3_scaleY.o" "Character1_RightHandRing3.sy";
connectAttr "Character1_RightHandRing3_scaleZ.o" "Character1_RightHandRing3.sz";
connectAttr "Character1_RightHandRing3_rotateX.o" "Character1_RightHandRing3.rx"
		;
connectAttr "Character1_RightHandRing3_rotateY.o" "Character1_RightHandRing3.ry"
		;
connectAttr "Character1_RightHandRing3_rotateZ.o" "Character1_RightHandRing3.rz"
		;
connectAttr "Character1_RightHandRing3_visibility.o" "Character1_RightHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_Neck.is";
connectAttr "Character1_Neck_scaleX.o" "Character1_Neck.sx";
connectAttr "Character1_Neck_scaleY.o" "Character1_Neck.sy";
connectAttr "Character1_Neck_scaleZ.o" "Character1_Neck.sz";
connectAttr "Character1_Neck_translateX.o" "Character1_Neck.tx";
connectAttr "Character1_Neck_translateY.o" "Character1_Neck.ty";
connectAttr "Character1_Neck_translateZ.o" "Character1_Neck.tz";
connectAttr "Character1_Neck_rotateX.o" "Character1_Neck.rx";
connectAttr "Character1_Neck_rotateY.o" "Character1_Neck.ry";
connectAttr "Character1_Neck_rotateZ.o" "Character1_Neck.rz";
connectAttr "Character1_Neck_visibility.o" "Character1_Neck.v";
connectAttr "Character1_Neck.s" "Character1_Head.is";
connectAttr "Character1_Head_scaleX.o" "Character1_Head.sx";
connectAttr "Character1_Head_scaleY.o" "Character1_Head.sy";
connectAttr "Character1_Head_scaleZ.o" "Character1_Head.sz";
connectAttr "Character1_Head_translateX.o" "Character1_Head.tx";
connectAttr "Character1_Head_translateY.o" "Character1_Head.ty";
connectAttr "Character1_Head_translateZ.o" "Character1_Head.tz";
connectAttr "Character1_Head_rotateX.o" "Character1_Head.rx";
connectAttr "Character1_Head_rotateY.o" "Character1_Head.ry";
connectAttr "Character1_Head_rotateZ.o" "Character1_Head.rz";
connectAttr "Character1_Head_visibility.o" "Character1_Head.v";
connectAttr "Character1_Head.s" "eyes.is";
connectAttr "eyes_translateX.o" "eyes.tx";
connectAttr "eyes_translateY.o" "eyes.ty";
connectAttr "eyes_translateZ.o" "eyes.tz";
connectAttr "eyes_scaleX.o" "eyes.sx";
connectAttr "eyes_scaleY.o" "eyes.sy";
connectAttr "eyes_scaleZ.o" "eyes.sz";
connectAttr "eyes_rotateX.o" "eyes.rx";
connectAttr "eyes_rotateY.o" "eyes.ry";
connectAttr "eyes_rotateZ.o" "eyes.rz";
connectAttr "eyes_visibility.o" "eyes.v";
connectAttr "Character1_Head.s" "jaw.is";
connectAttr "jaw_lockInfluenceWeights.o" "jaw.liw";
connectAttr "jaw_translateX.o" "jaw.tx";
connectAttr "jaw_translateY.o" "jaw.ty";
connectAttr "jaw_translateZ.o" "jaw.tz";
connectAttr "jaw_scaleX.o" "jaw.sx";
connectAttr "jaw_scaleY.o" "jaw.sy";
connectAttr "jaw_scaleZ.o" "jaw.sz";
connectAttr "jaw_rotateX.o" "jaw.rx";
connectAttr "jaw_rotateY.o" "jaw.ry";
connectAttr "jaw_rotateZ.o" "jaw.rz";
connectAttr "jaw_visibility.o" "jaw.v";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Emeny@Archer_Run.ma
