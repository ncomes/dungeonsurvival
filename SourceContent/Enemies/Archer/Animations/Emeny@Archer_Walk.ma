//Maya ASCII 2016 scene
//Name: Emeny@Archer_Walk.ma
//Last modified: Tue, Nov 24, 2015 12:22:13 PM
//Codeset: 1252
requires maya "2016";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "767DA16A-43F7-1C64-ED62-588999439610";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 291.21252415515704 89.350284915224833 310.15912854455212 ;
	setAttr ".r" -type "double3" -2.7383527296025028 42.600000000000108 -2.7005224048541255e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "DB616B3C-4895-12D1-C60D-AD8555A98792";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 446.54793738602081;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "7F9E26C8-4AF4-ADD1-5FF1-FC84881D370F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "41048BAB-40FC-F525-0831-2A80B779413C";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "A9DBC61B-438E-CE2D-43A1-2293BFAFB9D6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "78674104-4771-82D9-1C14-BB8C3807C46D";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "2BD6890F-4CE4-187B-A0A0-5E86DF8E9192";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "20E5C13A-46E9-CB50-BF2A-C093A0F2E03B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "ref_frame";
	rename -uid "9C09C70D-43CF-4E06-06AF-8A87BFA07087";
	addAttr -ci true -sn "refFrame" -ln "refFrame" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "Character1_Hips" -p "ref_frame";
	rename -uid "39F8DF47-4203-7FD3-C108-B1A2C4D2B721";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -19.036789801921167 ;
	setAttr ".ssc" no;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftUpLeg" -p "Character1_Hips";
	rename -uid "DAA49DCC-410F-DCC2-8B6E-78B5CCDBFE0F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 90.000000000000014 -12.849604412773132 43.257528613448208 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftLeg" -p "Character1_LeftUpLeg";
	rename -uid "CEF2080F-49AA-E718-7A28-8C84310490E2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -19.152297252867996 -13.416343813706657 -33.623675304480329 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFoot" -p "Character1_LeftLeg";
	rename -uid "9097433A-4F00-25BF-52E5-24BA4284F229";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 25.817647449536953 51.040150860538553 -112.05775014674906 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftToeBase" -p "Character1_LeftFoot";
	rename -uid "0D6D4ADC-48EB-7BBA-63B7-ED8BB6719666";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 43.727116820142825 -29.659481123592514 -4.6723850284209263 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle1" -p "Character1_LeftToeBase";
	rename -uid "84CFCC09-450F-86CD-2878-58A2A4A2DA4F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 165.43734692409092 -20.35908719617909 10.197796850486212 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle2" -p "Character1_LeftFootMiddle1";
	rename -uid "EB11AD84-439F-E96B-B66B-71ABE985BF79";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 127.90699782306312 -44.626258206212555 -101.65660999147298 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightUpLeg" -p "Character1_Hips";
	rename -uid "777703EA-4CDE-E385-9193-B394DD3FA0DD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 133.25752861344816 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightLeg" -p "Character1_RightUpLeg";
	rename -uid "700C9CA1-4EC4-4B8D-EFE3-63A7A965DF48";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -39.772585840344547 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFoot" -p "Character1_RightLeg";
	rename -uid "6EFD76A5-4DC1-78ED-6980-D7BAABD18F2C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -53.712356932759 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightToeBase" -p "Character1_RightFoot";
	rename -uid "0864CDC1-4DF7-0372-7001-4D8B82E39852";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 2.9245623946004821e-006 53.712356932758929 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle1" -p "Character1_RightToeBase";
	rename -uid "8922437F-473F-B743-8771-CB8F0E0D03A5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -4.5380835579220506e-006 -2.7209092876563258e-005 
		39.772585840347155 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle2" -p "Character1_RightFootMiddle1";
	rename -uid "E07B1326-4227-F009-22FE-58A160409E70";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -2.8539668357113417e-005 0.00011485155389716205 -133.25752861351003 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine" -p "Character1_Hips";
	rename -uid "AFC82AB4-43D4-E0FA-8C85-739E22CDC90A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 133.25752861344816 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine1" -p "Character1_Spine";
	rename -uid "3F003965-413C-358B-EBAD-DA8A6175B04F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -34.34645244194413 -26.596597123422644 16.99576897690589 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftShoulder" -p "Character1_Spine1";
	rename -uid "28E9AA13-472F-76A7-C9FC-01B87B422878";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -166.39102833412767 21.832119962557094 -120.00622813437471 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftArm" -p "Character1_LeftShoulder";
	rename -uid "0E386D7F-4F56-F97F-7292-0BA6E4A1A6D5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -172.11387269090633 -44.88241940154802 -37.940590556196071 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftForeArm" -p "Character1_LeftArm";
	rename -uid "FE5938B2-4C7E-1ABC-339E-308C20DA4679";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -84.022423618581243 -14.673598567163568 71.813326221989513 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHand" -p "Character1_LeftForeArm";
	rename -uid "A3393D3D-4C7E-0C42-60F2-608D17B8C1DB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -24.404340271578391 -34.932697344706668 122.26498801320882 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr -k on ".liw" no;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb1" -p "Character1_LeftHand";
	rename -uid "89F1A614-45A7-A638-845C-DA9DEE98938F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.1043965330995 61.333484361561069 1.8740740862626108 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb2" -p "Character1_LeftHandThumb1";
	rename -uid "FBC16291-4D55-E321-1615-E59E1A858B03";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -103.44292663098258 20.93424016453444 169.56041330531272 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb3" -p "Character1_LeftHandThumb2";
	rename -uid "EE4A8755-4770-7DEA-ADC5-21A791890715";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 14.174484134960574 -59.672502756712539 -19.020394805426264 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex1" -p "Character1_LeftHand";
	rename -uid "07F4921B-4B59-E327-6ED8-37A98E276F95";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -150.59621970507803 75.681392634287164 -124.28723884791442 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex2" -p "Character1_LeftHandIndex1";
	rename -uid "B5CE748A-4768-5A93-9269-55821E9F7E7E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -41.093804604175162 -57.602285575728224 -69.242456746392662 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex3" -p "Character1_LeftHandIndex2";
	rename -uid "21DD90D4-4CF0-C65B-3D26-9CB735E11CAF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.108297963098213 8.7979835742630801 27.088028901385748 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing1" -p "Character1_LeftHand";
	rename -uid "38CEAE35-43C4-3F94-1E36-688F366ABC87";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -141.42437885137375 63.919742192600388 -113.78475086453638 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing2" -p "Character1_LeftHandRing1";
	rename -uid "CF0E5837-4B26-E0DE-051A-CD9011947CC8";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -35.368646977348 -76.384823071466059 -48.755963146359043 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing3" -p "Character1_LeftHandRing2";
	rename -uid "4CD10F81-4972-04AB-3559-65BCE635334E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 9.6312888734270121 39.523687300631337 15.3258182753707 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightShoulder" -p "Character1_Spine1";
	rename -uid "400814D6-4563-39B6-EE1C-B6BC9CF12D84";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -164.54352138244408 34.957040538466451 -116.14754006477595 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightArm" -p "Character1_RightShoulder";
	rename -uid "F997ECAD-46D4-4428-43B6-BC9280BCA72C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -173.75456654457099 -11.164646449014084 -45.537881339438307 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightForeArm" -p "Character1_RightArm";
	rename -uid "6C2AE6B4-4FFC-97BB-0233-649387A561C8";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 22.753833922449758 50.186429787105524 -101.53518723839815 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHand" -p "Character1_RightForeArm";
	rename -uid "60154598-4187-66FC-799D-109FD323DF12";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 48.334026046900341 -3.7701083025157853 -29.882905994214536 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb1" -p "Character1_RightHand";
	rename -uid "8B90DA71-443C-2F3F-2253-4A9D999A635C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb2" -p "Character1_RightHandThumb1";
	rename -uid "742B4F8A-4782-F559-3B2C-86BF7D94EA29";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb3" -p "Character1_RightHandThumb2";
	rename -uid "888EFF57-42AF-8481-D5D5-6EBCD372EABB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex1" -p "Character1_RightHand";
	rename -uid "0E7E04B1-4F0B-85A4-7103-20973303C037";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex2" -p "Character1_RightHandIndex1";
	rename -uid "238A4296-4391-658E-9BBE-73B83839AFCB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex3" -p "Character1_RightHandIndex2";
	rename -uid "F9E9E513-4DCE-91D6-B904-3A8EA6D64711";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing1" -p "Character1_RightHand";
	rename -uid "C51F8974-4564-5A1A-FD24-0C9EFE16CF85";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing2" -p "Character1_RightHandRing1";
	rename -uid "DC5E67C1-4BD7-E670-6BCD-94ADFD13A122";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing3" -p "Character1_RightHandRing2";
	rename -uid "0E0E14DD-4414-E82F-DEE2-9F8A39831ECE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Neck" -p "Character1_Spine1";
	rename -uid "6692398A-4FF4-E7C2-4D98-5886C2274A6F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 48.229963067601958 30.670012388143238 129.35864775594831 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Head" -p "Character1_Neck";
	rename -uid "75B75532-49B3-BAC7-9433-65ACFD8F25FD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -14.36476107543206 22.377462763663633 -109.58762475494268 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "eyes" -p "Character1_Head";
	rename -uid "53D837F4-4048-A2EC-1C55-4D9B275C535B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -157.21319586594478 -21.144487689504771 -3.4677530572789341 ;
	setAttr ".radi" 4.5;
	setAttr ".fbxID" 5;
createNode joint -n "jaw" -p "Character1_Head";
	rename -uid "52404E97-470D-DA41-E304-1DB736475B51";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -157.21319586594478 -21.144487689504771 -3.4677530572789341 ;
	setAttr ".radi" 0.5;
	setAttr -k on ".liw" no;
	setAttr ".fbxID" 5;
createNode animCurveTU -n "Character1_LeftHand_lockInfluenceWeights";
	rename -uid "B3F52EAF-4775-2D5C-FC35-B88B25E4EC45";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "jaw_lockInfluenceWeights";
	rename -uid "10055419-4A5E-431B-2061-8B92AE3AE962";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "Character1_Hips_translateX";
	rename -uid "0E8BCF5F-4532-E196-FCBC-A29F93AC8016";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 0.49226203560829163 1 0.51415866613388062
		 2 0.50766944885253906 3 0.48205935955047607 4 0.46413296461105347 5 0.47185239195823669
		 6 0.51122468709945679 7 0.5790330171585083 8 0.64821743965148926 9 0.62736451625823975
		 10 0.48885300755500793 11 0.30982264876365662 12 0.14261592924594879 13 -0.0028340970166027546
		 14 -0.11674776673316956 15 -0.17962616682052612 16 -0.16742855310440063 17 -0.1074349582195282
		 18 -0.038115166127681732 19 0.02856622077524662 20 0.076803535223007202 21 0.10044661164283752
		 22 0.10372169315814972 23 0.11477463692426682 24 0.17832276225090027 25 0.2813965380191803
		 26 0.38299256563186646 27 0.45144668221473694 28 0.4866025447845459 29 0.49660050868988037
		 30 0.49226203560829163;
createNode animCurveTL -n "Character1_Hips_translateY";
	rename -uid "7EF5ED41-41A4-2A54-750F-8EACCE01D856";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 46.406150817871094 1 44.904800415039063
		 2 44.145614624023438 3 44.260410308837891 4 44.723873138427734 5 45.401748657226563
		 6 46.171432495117187 7 46.884315490722656 8 47.462081909179688 9 47.980575561523438
		 10 48.47900390625 11 48.799392700195313 12 48.782554626464844 13 48.355262756347656
		 14 47.489204406738281 15 46.181015014648437 16 44.298854827880859 17 43.30938720703125
		 18 43.749488830566406 19 44.449024200439453 20 45.262325286865234 21 46.087066650390625
		 22 46.839633941650391 23 47.455501556396484 24 48.015338897705078 25 48.450180053710938
		 26 48.684520721435547 27 48.598602294921875 28 48.149375915527344 29 47.391284942626953
		 30 46.406150817871094;
createNode animCurveTL -n "Character1_Hips_translateZ";
	rename -uid "0B45A1AC-44D3-C179-AEBF-52BA19B8096B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -0.89092963933944702 1 -0.97207927703857422
		 2 -0.96274477243423462 3 -0.93853926658630371 4 -0.96021425724029541 5 -1.027268648147583
		 6 -1.0537137985229492 7 -1.0028814077377319 8 -0.9000285267829895 9 -0.76414138078689575
		 10 -0.62975245714187622 11 -0.59082335233688354 12 -0.64247608184814453 13 -0.71289461851119995
		 14 -0.77512538433074951 15 -0.79469913244247437 16 -0.56737691164016724 17 -0.349516361951828
		 18 -0.38447782397270203 19 -0.50373589992523193 20 -0.68310922384262085 21 -0.82635438442230225
		 22 -0.86139822006225586 23 -0.79601234197616577 24 -0.69732403755187988 25 -0.6054721474647522
		 26 -0.5637967586517334 27 -0.57273030281066895 28 -0.62138158082962036 29 -0.71868991851806641
		 30 -0.89092963933944702;
createNode animCurveTU -n "Character1_Hips_scaleX";
	rename -uid "B3CBF51A-484E-4B58-597C-27BAD488BC3E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_Hips_scaleY";
	rename -uid "221DAC98-4DEF-8690-2EA0-0DA796A93259";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_Hips_scaleZ";
	rename -uid "9FFAE623-413C-CAE8-D079-3CA8CFB48AD2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Hips_rotateX";
	rename -uid "77913924-4D37-F929-8DE6-D49E40F530AB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 20.434270858764648 1 20.57783317565918
		 2 20.701648712158203 3 20.738786697387695 4 20.592790603637695 5 20.360090255737305
		 6 20.17066764831543 7 20.17326545715332 8 20.560636520385742 9 21.72715950012207
		 10 23.512958526611328 11 25.344058990478516 12 27.391582489013672 13 29.769800186157227
		 14 31.733371734619141 15 32.537040710449219 16 31.951644897460941 17 30.715660095214847
		 18 29.084096908569336 19 27.015487670898438 20 24.725397109985352 21 22.429409027099609
		 22 20.361431121826172 23 18.79273796081543 24 17.910228729248047 25 17.737678527832031
		 26 17.861927032470703 27 18.351261138916016 28 19.255756378173828 29 20.113739013671875
		 30 20.434270858764648;
createNode animCurveTA -n "Character1_Hips_rotateY";
	rename -uid "1E0C97E3-41C6-9D24-4D35-9188C4C91B55";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 2.9650623798370361 1 1.6364583969116211
		 2 -0.42415454983711243 3 -2.7324047088623047 4 -5.2550816535949707 5 -7.8429031372070321
		 6 -10.428853034973145 7 -12.92753791809082 8 -15.2353458404541 9 -17.371181488037109
		 10 -19.329225540161133 11 -20.97161865234375 12 -22.384605407714844 13 -23.559831619262695
		 14 -24.300878524780273 15 -24.471897125244141 16 -24.049917221069336 17 -23.055513381958008
		 18 -21.620138168334961 19 -19.976615905761719 20 -18.193134307861328 21 -16.28532600402832
		 22 -14.238332748413086 23 -12.040512084960937 24 -9.5602149963378906 25 -6.7504925727844238
		 26 -4.0547394752502441 27 -1.5387195348739624 28 0.75532376766204834 29 2.3915517330169678
		 30 2.9650623798370361;
createNode animCurveTA -n "Character1_Hips_rotateZ";
	rename -uid "07557B92-47FE-16C3-D2F9-EEB9A0114AC4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -3.9715399742126465 1 -1.9340668916702268
		 2 0.35448566079139709 3 2.8720312118530273 4 5.6682977676391602 5 8.4489297866821289
		 6 10.953534126281738 7 12.891695022583008 8 13.956585884094238 9 13.969452857971191
		 10 13.252652168273926 11 12.190122604370117 12 10.748225212097168 13 8.7845277786254883
		 14 6.6463003158569336 15 4.6658501625061035 16 2.9043042659759521 17 1.2366266250610352
		 18 -0.48475256562232977 19 -2.3364479541778564 20 -4.1463828086853027 21 -5.7466464042663574
		 22 -6.9635500907897949 23 -7.6653633117675781 24 -8.0389318466186523 25 -8.2923250198364258
		 26 -8.2944784164428711 27 -7.7749209403991699 28 -6.7116599082946777 29 -5.3371410369873047
		 30 -3.9715399742126465;
createNode animCurveTU -n "Character1_Hips_visibility";
	rename -uid "60E6F227-4E8B-9AF2-3036-0983962A6917";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftUpLeg_translateX";
	rename -uid "6115A6F6-4800-C9E7-24DE-47A3530D0F0B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.784505844116211;
createNode animCurveTL -n "Character1_LeftUpLeg_translateY";
	rename -uid "2ABD03EF-4A53-097A-25FE-0483D73FFCED";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5774786561451037e-006;
createNode animCurveTL -n "Character1_LeftUpLeg_translateZ";
	rename -uid "84738608-4EB7-D3B3-8E6C-2483313482FB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0264549255371094;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleX";
	rename -uid "ABF31855-4BB8-C68B-00DE-4F9063141F3A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleY";
	rename -uid "960E1BD2-4832-E091-D991-1BAC05858B9A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleZ";
	rename -uid "24C0B9CB-4FC3-5419-6664-D0B8AFB71A87";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateX";
	rename -uid "9555B15A-42F6-4FF9-9052-CCB97B482ADE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 20.221408843994141 1 23.068191528320312
		 2 31.320091247558597 3 38.088203430175781 4 35.314182281494141 5 25.252559661865234
		 6 12.243221282958984 7 -1.14964759349823 8 -11.701901435852051 9 -18.159248352050781
		 10 -21.122045516967773 11 -20.900350570678711 12 -18.881475448608398 13 -16.508787155151367
		 14 -14.965937614440918 15 -16.173135757446289 16 -17.107147216796875 17 -15.235466957092283
		 18 -12.433177947998047 19 -8.9042949676513672 20 -4.9039340019226074 21 -0.4577099084854126
		 22 4.3980712890625 23 9.3316574096679687 24 13.655242919921875 25 16.463775634765625
		 26 18.263912200927734 27 18.897930145263672 28 18.528472900390625 29 18.551830291748047
		 30 20.221408843994141;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateY";
	rename -uid "9E3F3100-4263-554E-1318-3BBB06D29EA6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 4.544621467590332 1 5.7159981727600098
		 2 4.2536721229553223 3 2.4691441059112549 4 4.352534294128418 5 7.4951128959655762
		 6 9.1441926956176758 7 9.0623178482055664 8 7.8641424179077148 9 5.5035114288330078
		 10 2.1174921989440918 11 -1.1840101480484009 12 -3.8106496334075932 13 -5.4571552276611328
		 14 -6.0342655181884766 15 -5.3126654624938965 16 -5.2265462875366211 17 -4.9274172782897949
		 18 -2.6611032485961914 19 0.59191715717315674 20 4.4108219146728516 21 8.2690114974975586
		 22 11.503790855407715 23 13.401537895202637 24 13.784961700439453 25 13.301939010620117
		 26 12.434646606445313 27 11.23831844329834 28 9.6392831802368164 29 7.4753055572509766
		 30 4.544621467590332;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateZ";
	rename -uid "149157A9-44FA-69CB-515E-368F141BD514";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -18.619810104370117 1 -18.108697891235352
		 2 -16.243587493896484 3 -16.003143310546875 4 -18.931730270385742 5 -24.036436080932617
		 6 -30.648740768432617 7 -38.435081481933594 8 -46.137859344482422 9 -52.625869750976562
		 10 -57.141647338867188 11 -58.995159149169922 12 -58.864696502685547 13 -58.020603179931648
		 14 -57.899578094482422 15 -60.394687652587898 16 -69.61822509765625 17 -76.950942993164062
		 18 -76.81109619140625 19 -75.559783935546875 20 -73.260581970214844 21 -69.903152465820313
		 22 -65.531715393066406 23 -60.373600006103516 24 -54.285839080810547 25 -48.067829132080078
		 26 -41.760929107666016 27 -35.651161193847656 28 -29.938405990600586 29 -24.335451126098633
		 30 -18.619810104370117;
createNode animCurveTU -n "Character1_LeftUpLeg_visibility";
	rename -uid "E8CDE9DB-4E31-A940-9913-36A70BD2A7A4";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftLeg_translateX";
	rename -uid "74872075-46D9-2943-B60A-4191E9E3D42F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.968876838684082;
createNode animCurveTL -n "Character1_LeftLeg_translateY";
	rename -uid "4A539511-48C6-EF31-872E-269A63B7A7C5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.2585945129394531;
createNode animCurveTL -n "Character1_LeftLeg_translateZ";
	rename -uid "0F100C93-4F0C-6956-51D6-D09DCC431A77";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 18.197568893432617;
createNode animCurveTU -n "Character1_LeftLeg_scaleX";
	rename -uid "0235BE58-4836-079D-7B2E-98856952F453";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_LeftLeg_scaleY";
	rename -uid "6679F045-4C4E-0A99-2C27-C6A0EB47D4F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftLeg_scaleZ";
	rename -uid "1F2A382F-4BC9-2408-E17D-E8B2ED5296B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftLeg_rotateX";
	rename -uid "0449229F-492E-DD57-7D3F-F3AC2AF7FE78";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 9.4331493377685547 1 18.818069458007812
		 2 15.896364212036133 3 11.297143936157227 4 21.829242706298828 5 41.787757873535156
		 6 62.031620025634766 7 77.212562561035156 8 84.285263061523437 9 82.847503662109375
		 10 74.736907958984375 11 62.932590484619134 12 49.723251342773437 13 37.019474029541016
		 14 28.107686996459961 15 28.810161590576172 16 36.236850738525391 17 39.013916015625
		 18 36.283859252929688 19 32.597396850585938 20 28.07342529296875 21 22.752138137817383
		 22 16.747791290283203 23 10.463072776794434 24 5.0052299499511719 25 1.9795444011688235
		 26 1.1141564846038818 27 2.5936758518218994 28 5.7475566864013672 29 8.580841064453125
		 30 9.4331493377685547;
createNode animCurveTA -n "Character1_LeftLeg_rotateY";
	rename -uid "1D302777-48DD-088F-491E-2CB0EAA93972";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -6.2625017166137695 1 -9.4597454071044922
		 2 -12.583696365356445 3 -15.33790111541748 4 -21.209606170654297 5 -28.7540397644043
		 6 -33.328392028808594 7 -34.462417602539062 8 -33.5343017578125 9 -31.05230712890625
		 10 -26.910358428955078 11 -21.379718780517578 12 -15.370085716247559 13 -10.284060478210449
		 14 -7.6529030799865714 15 -7.8850631713867196 16 -16.431451797485352 17 -23.073381423950195
		 18 -24.905933380126953 19 -26.549930572509766 20 -27.938911437988281 21 -28.889108657836911
		 22 -29.113189697265629 23 -28.34672737121582 24 -26.141799926757812 25 -23.329206466674805
		 26 -20.335781097412109 27 -17.545627593994141 28 -14.558435440063475 29 -10.762289047241211
		 30 -6.2625017166137695;
createNode animCurveTA -n "Character1_LeftLeg_rotateZ";
	rename -uid "9F28DF74-44B2-1C90-7BAD-57A6334E03C6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 13.197159767150879 1 16.73046875 2 18.746858596801758
		 3 21.660011291503906 4 24.902826309204102 5 23.875049591064453 6 18.882152557373047
		 7 14.098668098449707 8 12.360404014587402 9 14.093814849853516 10 17.408073425292969
		 11 19.763715744018555 12 19.787775039672852 13 17.625499725341797 14 15.222580909729004
		 15 15.427578926086426 16 22.413457870483398 17 27.719289779663086 18 29.703393936157223
		 19 31.58390998840332 20 33.323741912841797 21 34.912014007568359 22 36.256782531738281
		 23 37.061237335205078 24 36.211353302001953 25 33.569473266601563 26 29.660680770874027
		 27 25.101835250854492 28 20.663702011108398 29 16.767604827880859 30 13.197159767150879;
createNode animCurveTU -n "Character1_LeftLeg_visibility";
	rename -uid "57EDBB13-48CF-4A05-426D-2FA2D6D424B1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFoot_translateX";
	rename -uid "BE7F609E-40D6-EB89-B939-1CA91BA11626";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5243232250213623;
createNode animCurveTL -n "Character1_LeftFoot_translateY";
	rename -uid "DBDDBCEB-4180-D8C8-65A3-FD8CD677663B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.000351905822754;
createNode animCurveTL -n "Character1_LeftFoot_translateZ";
	rename -uid "139142E0-492A-7F52-34ED-52AD9F491560";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.967419624328613;
createNode animCurveTU -n "Character1_LeftFoot_scaleX";
	rename -uid "84EF8BDE-49C3-48B7-FFE3-B3A9A2712835";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFoot_scaleY";
	rename -uid "05B1E1E4-468E-BA30-18E6-39AE42E1E37C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleZ";
	rename -uid "3CB84513-43C7-A980-B05B-88BB65CE524C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999964237213135;
createNode animCurveTA -n "Character1_LeftFoot_rotateX";
	rename -uid "9EA9524C-40A5-3AD9-C6B8-83943E630EAE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 12.189690589904785 1 13.639747619628906
		 2 12.973031044006348 3 9.7932271957397461 4 6.3005805015563965 5 2.6369988918304443
		 6 -0.6987464427947998 7 -3.9767909049987793 8 -8.0365571975708008 9 -9.6654958724975586
		 10 -5.338047981262207 11 4.0391111373901367 12 13.480838775634766 13 18.862277984619141
		 14 19.999549865722656 15 17.997829437255859 16 10.031761169433594 17 1.4154127836227417
		 18 0.60830080509185791 19 0.086686842143535614 20 -0.042015179991722107 21 0.32858061790466309
		 22 1.2370508909225464 23 2.5912144184112549 24 4.1571221351623535 25 5.4207019805908203
		 26 6.4159111976623535 27 7.205136775970459 28 8.2304048538208008 29 9.9154186248779297
		 30 12.189690589904785;
createNode animCurveTA -n "Character1_LeftFoot_rotateY";
	rename -uid "DC74C172-4815-3050-B5BA-228F4AB2BE4E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -11.513916015625 1 -10.280867576599121
		 2 -3.631816148757935 3 4.4832339286804199 4 7.4808149337768546 5 8.4423675537109375
		 6 11.193419456481934 7 16.197795867919922 8 21.347410202026367 9 23.89497184753418
		 10 22.352605819702148 11 14.019062042236328 12 -0.53918009996414185 13 -14.387524604797362
		 14 -22.179313659667969 15 -24.371179580688477 16 -20.874652862548828 17 -15.994897842407225
		 18 -16.980674743652344 19 -17.925748825073242 20 -18.750003814697266 21 -19.427663803100586
		 22 -19.904909133911133 23 -20.125970840454102 24 -19.46044921875 25 -18.062229156494141
		 26 -16.331567764282227 27 -14.923501014709473 28 -13.995444297790527 29 -12.996345520019531
		 30 -11.513916015625;
createNode animCurveTA -n "Character1_LeftFoot_rotateZ";
	rename -uid "41D3E85E-41CC-D915-AC1F-7889179A6E5F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 9.9259395599365234 1 10.272825241088867
		 2 7.2018623352050781 3 2.1748452186584473 4 -1.2627110481262207 5 -4.6347613334655762
		 6 -9.5438203811645508 7 -15.723120689392088 8 -21.533306121826172 9 -22.965604782104492
		 10 -17.375446319580078 11 -5.6395792961120605 12 6.8330144882202148 13 15.17379093170166
		 14 18.69244384765625 15 18.693744659423828 16 13.654502868652344 17 8.2345895767211914
		 18 9.343745231628418 19 10.718203544616699 20 12.035634994506836 21 13.102696418762207
		 22 13.80645751953125 23 14.108756065368652 24 13.660552978515625 25 12.735540390014648
		 26 11.651494979858398 27 10.814674377441406 28 10.382840156555176 29 10.14342212677002
		 30 9.9259395599365234;
createNode animCurveTU -n "Character1_LeftFoot_visibility";
	rename -uid "BC23682E-4F24-2E17-4D51-9C9CEC8981CF";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftToeBase_translateX";
	rename -uid "DE612FB8-4B60-5A30-918B-FA8EF0A1FA5B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.585639476776123;
createNode animCurveTL -n "Character1_LeftToeBase_translateY";
	rename -uid "0A1EDA66-4D64-02D2-6FA5-1D9BE9A0FDF8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3360438346862793;
createNode animCurveTL -n "Character1_LeftToeBase_translateZ";
	rename -uid "BBDEAE33-462B-F81D-CED2-DD98D7B58864";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.4897398948669434;
createNode animCurveTU -n "Character1_LeftToeBase_scaleX";
	rename -uid "1D427E0A-4371-4997-285C-A487C5325B57";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftToeBase_scaleY";
	rename -uid "92CE01BD-46AF-7B15-1058-7FAC83801C49";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftToeBase_scaleZ";
	rename -uid "8A5C2ACA-46B8-506B-3206-C481EC84C610";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_LeftToeBase_rotateX";
	rename -uid "69D475CC-4EA4-4DB4-94BA-22B00ACECD5A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 28 ".ktv[0:27]"  0 18.154415130615234 1 14.473560333251953
		 2 10.74396800994873 3 6.4238615036010742 4 0.42800605297088623 5 -6.9654860496520996
		 6 -14.163870811462402 7 -19.455774307250977 8 -21.113149642944336 9 -18.764253616333008
		 10 -13.824007034301758 11 -7.2999520301818848 12 -0.20158815383911133 13 6.5164995193481445
		 14 11.990928649902344 15 15.429092407226562 16 8.3755989074707031 17 -1.0467078315201661e-009
		 18 -8.0679857239474018e-010 22 9.4840961473163787e-011 23 9.3177528193155723e-011
		 24 0.70954471826553345 25 2.6115891933441162 26 5.3611612319946289 27 8.6151294708251953
		 28 12.040892601013184 29 15.321282386779783 30 18.154415130615234;
createNode animCurveTA -n "Character1_LeftToeBase_rotateY";
	rename -uid "C86C6F71-4813-79F6-FDB0-69B89ED6CFDC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 28 ".ktv[0:27]"  0 -7.5343475341796875 1 -5.7124319076538086
		 2 -4.0160236358642578 3 -2.2442646026611328 4 -0.1349056214094162 5 1.902447462081909
		 6 3.289484977722168 7 3.9364299774169926 8 4.0744314193725586 9 3.8697490692138672
		 10 3.2371644973754883 11 1.9798898696899414 12 0.062832407653331757 13 -2.2800760269165039
		 14 -4.5661983489990234 15 -6.171506404876709 16 -3.018794059753418 17 -5.3807607258704593e-009
		 18 -5.1066924022791227e-009 22 -4.518483809334839e-009 23 -4.5202486198547831e-009
		 24 -0.22482101619243625 25 -0.85578823089599609 26 -1.8406525850296018 27 -3.1167843341827393
		 28 -4.5885801315307617 29 -6.119196891784668 30 -7.5343475341796875;
createNode animCurveTA -n "Character1_LeftToeBase_rotateZ";
	rename -uid "31C704BA-4C57-D003-CFEA-E48BACC30896";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 28 ".ktv[0:27]"  0 10.055798530578613 1 8.1599340438842773
		 2 6.155707836151123 3 3.7422177791595459 4 0.25432726740837097 5 -4.2213644981384277
		 6 -8.7127523422241211 7 -12.070237159729004 8 -13.128138542175293 9 -11.62958812713623
		 10 -8.4984588623046875 11 -4.4275269508361816 12 -0.1199958100914955 13 3.7949068546295166
		 14 6.8345060348510742 15 8.6603221893310547 16 4.8439416885375977 17 1.913781133566772e-009
		 18 1.6153083315373351e-009 22 9.1242163824389877e-010 23 6.458370505058042e-010 24 0.42124542593955994
		 25 1.5412775278091431 26 3.1349442005157471 27 4.9779009819030762 28 6.8615341186523438
		 29 8.6041545867919922 30 10.055798530578613;
createNode animCurveTU -n "Character1_LeftToeBase_visibility";
	rename -uid "DC8B8E8C-43E7-4FD1-3E89-63BFEF2AC51B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateX";
	rename -uid "B24A067C-44C0-C706-E910-F8A47C4D52D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7153444290161133;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateY";
	rename -uid "8EFA2CBE-4206-FBE3-E807-34A29D61FDC3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.0020139217376709;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateZ";
	rename -uid "6EF70AF2-4FA9-B2B7-B984-9CB4865C9FF5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.1786108016967773;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleX";
	rename -uid "E61E34A2-49A8-2CBA-F7F2-3A973BD5E861";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleY";
	rename -uid "AD02F966-4C3E-15A9-D1BE-75A185E4A95E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999940395355225;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleZ";
	rename -uid "E327E671-43F6-C923-8674-11BCDD3472D5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateX";
	rename -uid "E9E99D00-41F4-3B15-B20B-FC80B67DF7C6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.7036491085682428e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateY";
	rename -uid "1628CA03-4301-DC54-5C09-A29019633FAF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.1577731236848194e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateZ";
	rename -uid "6390D507-4A7C-AF91-E458-B394699FDB6D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.7791316177002727e-010;
createNode animCurveTU -n "Character1_LeftFootMiddle1_visibility";
	rename -uid "B6023337-463A-4327-1A4E-949932A20B9B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateX";
	rename -uid "81B2268D-4896-1D13-A59A-D9B6EAF5D7A9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4684751033782959;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateY";
	rename -uid "C481312D-49FB-67B2-2FA6-BA94E0A9E750";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.1393222808837891;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateZ";
	rename -uid "3AB118A5-452F-101F-6E07-C39DFF749775";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.3014278411865234;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleX";
	rename -uid "12786A05-4380-F9DF-8F86-09A25240C326";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleY";
	rename -uid "53477779-49EF-C98C-7F5E-DAB6F538248E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleZ";
	rename -uid "848A8202-4042-DE88-B6C0-8DBA7A347D17";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateX";
	rename -uid "94877105-42CC-973E-C0DD-7098A2B123A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.6574187818655446e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateY";
	rename -uid "1AA9A110-468A-12FA-3BD5-3A8AEB6F12D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 9.565636283070944e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateZ";
	rename -uid "284CA7D5-4024-85E6-EA30-AABBC6D08CDC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.3993032267385388e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle2_visibility";
	rename -uid "EF597292-47F7-C284-3163-0A9F8DC17530";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightUpLeg_translateX";
	rename -uid "D5E131DA-422B-C8D2-4944-9AB4157881F2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.489771842956543;
createNode animCurveTL -n "Character1_RightUpLeg_translateY";
	rename -uid "884BCBCA-486D-2F9D-3F58-A5A4DB652F08";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.6505136489868164;
createNode animCurveTL -n "Character1_RightUpLeg_translateZ";
	rename -uid "58DDB4B3-4852-6776-D1A2-96A481B05670";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0264549255371094;
createNode animCurveTU -n "Character1_RightUpLeg_scaleX";
	rename -uid "5053EC0E-4CD7-7447-05C8-77A4D19777CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightUpLeg_scaleY";
	rename -uid "69F4846E-4151-33AD-BA4F-D7BD2C86D108";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightUpLeg_scaleZ";
	rename -uid "40C2281E-42B9-ADCE-03F7-D5A42736AEF6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightUpLeg_rotateX";
	rename -uid "419DE237-41DE-FD12-D3D2-308DD137CE18";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -4.8291869163513184 1 -11.879966735839844
		 2 -19.190567016601563 3 -18.927160263061523 4 -16.426498413085937 5 -12.646759986877441
		 6 -8.7999477386474609 7 -5.7370748519897461 8 -3.9971232414245605 9 -3.1581325531005859
		 10 -2.6796948909759521 11 -2.4669938087463379 12 -1.8844382762908938 13 -0.65918564796447754
		 14 0.83465075492858887 15 2.1603202819824219 16 2.6032702922821045 17 0.96489715576171875
		 18 -2.5516536235809326 19 -5.5262212753295898 20 -6.7227740287780762 21 -6.6577606201171875
		 22 -5.3953299522399902 23 -3.3950846195220947 24 -2.2698531150817871 25 -1.3125356435775757
		 26 -0.93686890602111805 27 -1.0856412649154663 28 -1.6099158525466919 29 -2.7756094932556152
		 30 -4.8291869163513184;
createNode animCurveTA -n "Character1_RightUpLeg_rotateY";
	rename -uid "B6A322E4-4AF2-7F27-C1D8-8BAFBCF587B2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 49.079830169677734 1 57.49754714965821
		 2 60.866939544677734 3 59.830638885498047 4 57.410942077636719 5 53.914951324462891
		 6 49.614608764648438 7 45.027324676513672 8 40.619766235351562 9 36.326004028320313
		 10 31.940362930297855 11 27.912050247192383 12 25.135456085205078 13 23.806020736694336
		 14 23.150552749633789 15 21.893871307373047 16 18.645288467407227 17 12.982891082763672
		 18 8.9112663269042969 19 11.053295135498047 20 18.912176132202148 21 29.579025268554687
		 22 40.745132446289063 23 49.155597686767578 24 53.013702392578125 25 55.158329010009766
		 26 55.584125518798828 27 55.097946166992188 28 54.254467010498047 29 52.591220855712891
		 30 49.079830169677734;
createNode animCurveTA -n "Character1_RightUpLeg_rotateZ";
	rename -uid "803EA2FD-4831-5979-639B-308455AA13E6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 15.249423027038574 1 8.9103431701660156
		 2 1.368348240852356 3 -1.9926736354827883 4 -4.104766845703125 5 -5.5590968132019043
		 6 -7.0769743919372559 7 -8.8983926773071289 8 -10.778635025024414 9 -12.10038948059082
		 10 -13.036233901977539 11 -14.01167106628418 12 -14.767461776733397 13 -15.131308555603029
		 14 -15.582396507263184 15 -16.524311065673828 16 -18.376918792724609 17 -20.230833053588867
		 18 -21.124900817871094 19 -21.118558883666992 20 -20.118799209594727 21 -17.708986282348633
		 22 -13.42303466796875 23 -8.204930305480957 24 -3.175929069519043 25 1.6801949739456177
		 26 5.800788402557373 27 8.9941730499267578 28 11.468229293823242 29 13.387909889221191
		 30 15.249423027038574;
createNode animCurveTU -n "Character1_RightUpLeg_visibility";
	rename -uid "B6CF0F32-4CC8-D832-0D0D-8B80E76FEBCA";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightLeg_translateX";
	rename -uid "CBCA68A8-4697-F38B-BF99-A4B6B0E70986";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -18.197565078735352;
createNode animCurveTL -n "Character1_RightLeg_translateY";
	rename -uid "36A34F93-429C-EF32-B9E0-CC92EB338B9F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.1862249374389648;
createNode animCurveTL -n "Character1_RightLeg_translateZ";
	rename -uid "38AA1673-4A6F-BD7C-B5F7-3E8A75BD4CEC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.5520458221435547;
createNode animCurveTU -n "Character1_RightLeg_scaleX";
	rename -uid "1673CF5F-4FB8-E7AD-D8DD-40A9DDE734D6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightLeg_scaleY";
	rename -uid "707CE5B5-419B-AF0A-E268-6B85372C3A69";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightLeg_scaleZ";
	rename -uid "D7F1B945-421B-FEC3-FC95-5BB8264C2A78";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightLeg_rotateX";
	rename -uid "4F60C010-4CCE-3116-96D1-D89E14B3378C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 36.802539825439453 1 44.836433410644531
		 2 49.486557006835938 3 51.272758483886719 4 51.718406677246094 5 50.796207427978516
		 6 48.854331970214844 7 46.556217193603516 8 44.617290496826172 9 42.747623443603516
		 10 40.437507629394531 11 38.183914184570313 12 36.538902282714844 13 35.692745208740234
		 14 35.571392059326172 15 36.073986053466797 16 39.837326049804688 17 44.685451507568359
		 18 47.354869842529297 19 53.274394989013672 20 66.052543640136719 21 87.382858276367188
		 22 110.34203338623047 23 117.14733123779297 24 102.12068176269531 25 78.940444946289063
		 26 57.667755126953132 27 44.598762512207031 28 38.164470672607422 29 36.043434143066406
		 30 36.802539825439453;
createNode animCurveTA -n "Character1_RightLeg_rotateY";
	rename -uid "97C77B8E-4AC9-097F-DAA8-F693CB9D9765";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -9.4681797027587891 1 -23.771577835083008
		 2 -31.199844360351566 3 -33.644962310791016 4 -33.939048767089844 5 -32.811534881591797
		 6 -30.877561569213864 7 -28.736555099487301 8 -26.492692947387695 9 -23.461877822875977
		 10 -19.612628936767578 11 -16.419401168823242 12 -15.498297691345215 13 -17.568977355957031
		 14 -22.720001220703125 15 -30.107709884643555 16 -38.992519378662109 17 -42.571243286132813
		 18 -42.360691070556641 19 -47.932872772216797 20 -58.772747039794929 21 -67.709075927734375
		 22 -70.600822448730469 23 -70.573554992675781 24 -70.705375671386719 25 -68.900894165039063
		 26 -62.933361053466804 27 -53.323383331298828 28 -41.291202545166016 29 -27.041727066040039
		 30 -9.4681797027587891;
createNode animCurveTA -n "Character1_RightLeg_rotateZ";
	rename -uid "B6A1BFC2-43A1-47D4-F4FD-F18316CB8390";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 5.8453736305236816 1 5.6378588676452637
		 2 4.3534417152404785 3 2.2782065868377686 4 0.18420664966106415 5 -1.7206437587738037
		 6 -3.2299222946166992 7 -4.210235595703125 8 -4.4635701179504395 9 -3.8623943328857422
		 10 -2.74928879737854 11 -1.7638380527496338 12 -1.3344196081161499 13 -1.5803797245025635
		 14 -2.6610062122344971 15 -4.7976040840148926 16 -8.916468620300293 17 -11.593491554260254
		 18 -11.157055854797363 19 -13.42808723449707 20 -22.869157791137695 21 -43.225082397460938
		 22 -67.700675964355469 23 -77.328529357910156 24 -65.874176025390625 25 -45.096630096435547
		 26 -24.924467086791992 27 -11.570548057556152 28 -3.3910086154937744 29 2.0205428600311279
		 30 5.8453736305236816;
createNode animCurveTU -n "Character1_RightLeg_visibility";
	rename -uid "3B767B56-49C9-FEFC-46FB-5D8B5F65470F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFoot_translateX";
	rename -uid "78A92700-46BB-B915-3DC5-5B924A4F48D6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -18.25177001953125;
createNode animCurveTL -n "Character1_RightFoot_translateY";
	rename -uid "D8F866C3-4162-3E5A-C4DC-E6B2F50444F7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.0794563293457031;
createNode animCurveTL -n "Character1_RightFoot_translateZ";
	rename -uid "120D3199-4515-EAAA-8A17-CC9F7FCDC211";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5524176359176636;
createNode animCurveTU -n "Character1_RightFoot_scaleX";
	rename -uid "9024FC70-4F33-10DD-3430-A29A0DDFB63E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFoot_scaleY";
	rename -uid "FBCDBB68-477C-3392-227A-81AA79FE1412";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFoot_scaleZ";
	rename -uid "6C6FBDB8-484D-94B6-6D92-58A27370CAC0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightFoot_rotateX";
	rename -uid "6C1747F8-4588-D695-744E-26B222AA67F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -17.734699249267578 1 -6.2828731536865234
		 2 0.52112889289855957 3 -3.796479225158691 4 -7.875098228454589 5 -11.585518836975098
		 6 -14.775655746459961 7 -17.30584716796875 8 -18.914653778076172 9 -19.461931228637695
		 10 -19.13237190246582 11 -18.46092414855957 12 -18.05085563659668 13 -18.21458625793457
		 14 -19.152740478515625 15 -20.712549209594727 16 -24.095525741577148 17 -22.931297302246094
		 18 -13.207225799560547 19 -1.9832000732421875 20 7.2471318244934073 21 16.266670227050781
		 22 25.464553833007813 23 33.523189544677734 24 36.913604736328125 25 32.923309326171875
		 26 23.031894683837891 27 9.7004518508911133 28 -3.5711333751678467 29 -13.410196304321289
		 30 -17.734699249267578;
createNode animCurveTA -n "Character1_RightFoot_rotateY";
	rename -uid "A37D5A8C-49D9-B728-57C3-B6AC8E49FC44";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 5.7691550254821777 1 2.2261068820953369
		 2 1.4116907119750977 3 2.5465993881225586 4 3.5753154754638672 5 4.6454281806945801
		 6 5.8008251190185547 7 6.9446806907653809 8 7.8368320465087891 9 8.3146600723266602
		 10 8.4462766647338867 11 8.3752250671386719 12 8.2873773574829102 13 8.3228111267089844
		 14 8.6104059219360352 15 9.2015504837036133 16 10.704544067382812 17 10.972580909729004
		 18 7.6858725547790527 19 3.3551969528198242 20 -0.16490732133388519 21 -3.607542991638184
		 22 -7.2790327072143555 23 -10.304744720458984 24 -10.641195297241211 25 -8.4716367721557617
		 26 -4.2534337043762207 27 0.84571307897567749 28 4.8300790786743164 29 6.4065184593200684
		 30 5.7691550254821777;
createNode animCurveTA -n "Character1_RightFoot_rotateZ";
	rename -uid "30FBA348-45A7-CC91-AC9A-45B3D705955D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -12.574819564819336 1 -18.220497131347656
		 2 -22.135255813598633 3 -22.51335334777832 4 -21.520744323730469 5 -19.516530990600586
		 6 -17.019598007202148 7 -14.629624366760254 8 -12.79843807220459 9 -11.326501846313477
		 10 -9.771815299987793 11 -8.2716550827026367 12 -7.1099333763122559 13 -6.3678550720214844
		 14 -5.8502345085144043 15 -5.0311827659606934 16 -3.4233813285827637 17 -0.51722168922424316
		 18 1.6179966926574707 19 1.1930423974990845 20 -1.1283273696899414 21 -3.100994348526001
		 22 -3.7968070507049561 23 -3.9583101272583012 24 -4.3603396415710449 25 -3.691092729568481
		 26 -3.2054710388183594 27 -4.3507647514343262 28 -7.3859639167785653 29 -10.869622230529785
		 30 -12.574819564819336;
createNode animCurveTU -n "Character1_RightFoot_visibility";
	rename -uid "E38926F4-40AC-41BD-5774-4AB7281D10D5";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightToeBase_translateX";
	rename -uid "6CAE2933-44A4-0DF0-F48D-95874A3712F7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2767767906188965;
createNode animCurveTL -n "Character1_RightToeBase_translateY";
	rename -uid "EF076805-4F84-CB8A-9BA8-D481F09A330B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.0139193534851074;
createNode animCurveTL -n "Character1_RightToeBase_translateZ";
	rename -uid "604BC48D-45C6-3054-1A76-09B0DECABB7F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8259720802307129;
createNode animCurveTU -n "Character1_RightToeBase_scaleX";
	rename -uid "339DE30C-4E7D-7C3E-D1AB-C8AFC54A7AEB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightToeBase_scaleY";
	rename -uid "3149E583-4134-9B65-C1CC-73A4F464AC01";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightToeBase_scaleZ";
	rename -uid "ACB3BBD4-414D-AE1A-F8A3-7DB204FCAD91";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightToeBase_rotateX";
	rename -uid "FBD76EA9-44BB-38AC-5113-428FC8BEE204";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 28 ".ktv[0:27]"  0 -3.645404577255249 1 -1.3471393585205078
		 2 -4.8920862916190799e-009 3 -4.8119916939981522e-009 7 -3.9966931986157306e-009
		 8 -3.8694638604397369e-009 9 -0.29628223180770874 10 -1.071480393409729 11 -2.1612794399261475
		 12 -3.4090526103973389 13 -4.6538114547729492 14 -5.7110905647277832 15 -6.3628678321838379
		 16 -6.0807328224182129 17 -4.8590259552001953 18 -3.2406978607177734 19 -1.1018733978271484
		 20 1.5838499069213867 21 4.2963805198669434 22 6.4291372299194336 23 7.1009092330932617
		 24 6.3683123588562012 25 5.0835943222045898 26 3.4608039855957031 27 1.6660431623458862
		 28 -0.18177185952663422 29 -1.9868209362030029 30 -3.645404577255249;
createNode animCurveTA -n "Character1_RightToeBase_rotateY";
	rename -uid "68A5FF7C-4795-36CE-D087-F380D84D479D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 28 ".ktv[0:27]"  0 12.869156837463379 1 4.8287034034729004
		 2 -5.0191739653371314e-010 3 -4.7790765789201828e-010 7 4.2447642623066878e-010 8 5.4597959486812897e-010
		 9 1.0644955635070801 10 3.8441183567047119 11 7.7172155380249015 12 12.061060905456543
		 13 16.253532409667969 14 19.675775527954102 15 21.713621139526367 16 20.838531494140625
		 17 16.928472518920898 18 11.48231315612793 19 3.9528791904449467 20 -5.6717567443847656
		 21 -15.066286087036133 22 -21.917398452758789 23 -23.950130462646484 24 -21.730222702026367
		 25 -17.661140441894531 26 -12.238345146179199 27 -5.9640040397644043 28 0.65313911437988281
		 29 7.1011414527893066 30 12.869156837463379;
createNode animCurveTA -n "Character1_RightToeBase_rotateZ";
	rename -uid "5C9F37C5-4072-8465-DB6E-0C82333313A0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -0.41126266121864319 1 -0.056802581995725625
		 2 -1.0395007077335094e-010 3 -2.9506005277957215e-011 8 3.6022962390802604e-010 9 -0.0027521257288753986
		 10 -0.035957880318164825 11 -0.14578863978385925 12 -0.36024639010429382 13 -0.66491103172302246
		 14 -0.99116218090057373 15 -1.2215315103530884 16 -1.1191533803939819 17 -0.72351264953613281
		 18 -0.32590445876121521 19 -0.038028180599212646 20 -0.078466489911079407 21 -0.56842637062072754
		 22 -1.246157169342041 23 -1.507969856262207 24 -1.2235467433929443 25 -0.79027527570724487
		 26 -0.37114420533180237 27 -0.086800731718540192 28 -0.0010392847470939159 29 -0.12329282611608505
		 30 -0.41126266121864319;
createNode animCurveTU -n "Character1_RightToeBase_visibility";
	rename -uid "1608F91A-4FB9-AF85-6632-E8ADCCEA595B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateX";
	rename -uid "555D431B-4DBE-B94E-0DCA-638BBADF04CA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.0018006421159952879;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateY";
	rename -uid "D4C8A0FF-405C-5994-1D0B-C88C2799249E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.0064689838327467442;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateZ";
	rename -uid "4809D701-4B78-7A7B-EB09-D7971862C859";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.6805553436279297;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleX";
	rename -uid "253D307C-42F8-E17D-DB10-03B018431348";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleY";
	rename -uid "B9DE88CA-4F4E-C9A1-DB05-D0998093ADDE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleZ";
	rename -uid "2A006B96-4D37-AC7B-5B3A-0E80594894F9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateX";
	rename -uid "3BFA3B73-464D-E059-CD87-76AB143A84E7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.0899128224464221e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateY";
	rename -uid "49E1895E-4210-69D0-A5A4-4F8DBF7E30F1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.4574489633837402e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateZ";
	rename -uid "10D344B7-436C-4DEE-74B8-B797649E2666";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.3333195899371049e-010;
createNode animCurveTU -n "Character1_RightFootMiddle1_visibility";
	rename -uid "2D02AFC2-445D-FB80-0BF4-16B42E0C2F42";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateX";
	rename -uid "34F50F8C-423D-F052-6DC2-C393EDE268F5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.5860786081466358e-006;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateY";
	rename -uid "BB9164F7-4865-8244-7925-15B6AF19C0CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.3302309172286186e-007;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateZ";
	rename -uid "88C62461-4BF7-571D-70D4-A389A1B8C703";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.0234856605529785;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleX";
	rename -uid "F81F1D1D-440A-20BD-A3F9-A2BE52C961FB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleY";
	rename -uid "FE4BCB32-488A-899D-F916-0BBCD5F674CC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleZ";
	rename -uid "F3A7EC47-4730-9995-0C31-F4A103C875CE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateX";
	rename -uid "CC01A232-40D9-C052-06EA-54B4883FF645";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.1069282963899241e-010;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateY";
	rename -uid "C835F29F-4463-9666-94B7-7EB6B4CEDE1B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2920493947253817e-008;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateZ";
	rename -uid "38007FB8-48A5-105B-C69E-518BCFCC6E60";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7239195626572725e-009;
createNode animCurveTU -n "Character1_RightFootMiddle2_visibility";
	rename -uid "51E6419E-46C9-5C65-BD9C-9CB65A2BB649";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Spine_translateX";
	rename -uid "09958A85-48AF-741C-20EC-888BAB2A2C84";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6075620651245117;
createNode animCurveTL -n "Character1_Spine_translateY";
	rename -uid "6E25D76C-4298-09F3-7514-0CA20AF8C82E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.353469848632812;
createNode animCurveTL -n "Character1_Spine_translateZ";
	rename -uid "E22B0C5C-483E-8A9A-ADAB-149EB19C288D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2061522006988525;
createNode animCurveTU -n "Character1_Spine_scaleX";
	rename -uid "8061412D-42E7-571B-45A8-F6BE0E332B83";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine_scaleY";
	rename -uid "97384A8F-44B0-18DC-2BA7-928E0D4FB526";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine_scaleZ";
	rename -uid "394FEB61-4CE3-48B3-F279-A2BE268C6D7C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_Spine_rotateX";
	rename -uid "8DB18A4B-4D16-BCF1-5BAD-E2812AFEB448";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -9.8376579284667969 1 -9.5925960540771484
		 2 -9.2539539337158203 3 -8.7830629348754883 4 -8.1992998123168945 5 -7.4982190132141113
		 6 -6.6314454078674316 7 -5.5815715789794922 8 -4.3558964729309082 9 -2.915825366973877
		 10 -1.326576828956604 11 0.22504360973834991 12 1.5987892150878906 13 2.6765127182006836
		 14 3.3110125064849854 15 3.3398177623748779 16 2.8706071376800537 17 2.1077449321746826
		 18 1.0456526279449463 19 -0.23290273547172546 20 -1.6309064626693726 21 -3.071774959564209
		 22 -4.4817996025085449 23 -5.7872376441955566 24 -6.9355020523071289 25 -7.9149656295776367
		 26 -8.7071533203125 27 -9.3090734481811523 28 -9.7176141738891602 29 -9.9120073318481445
		 30 -9.8376579284667969;
createNode animCurveTA -n "Character1_Spine_rotateY";
	rename -uid "F956D4FE-49A4-64EF-0289-3EBD8DDF756B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 0.023656571283936501 1 -0.17624126374721527
		 2 -0.58702212572097778 3 -1.1839399337768555 4 -1.9174009561538696 5 -2.7292256355285645
		 6 -3.5362076759338379 7 -4.2588329315185547 8 -4.858673095703125 9 -5.2784886360168457
		 10 -5.5305576324462891 11 -5.6778039932250977 12 -5.7611846923828125 13 -5.8126497268676758
		 14 -5.8674278259277344 15 -5.9673647880554199 16 -6.2563128471374512 17 -6.7731232643127441
		 18 -7.3937082290649414 19 -8.010594367980957 20 -8.5194358825683594 21 -8.8066825866699219
		 22 -8.7641668319702148 23 -8.3266754150390625 24 -7.3709535598754883 25 -5.9492101669311523
		 26 -4.3098626136779785 27 -2.6710441112518311 28 -1.2435680627822876 29 -0.25720903277397156
		 30 0.023656571283936501;
createNode animCurveTA -n "Character1_Spine_rotateZ";
	rename -uid "EF4DC048-458A-BB90-AE93-D69843DBA81E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 1.914801239967346 1 -0.70395803451538086
		 2 -3.4503374099731445 3 -6.0938081741333008 4 -8.4220705032348633 5 -10.279662132263184
		 6 -11.60087776184082 7 -12.305474281311035 8 -12.317699432373047 9 -11.825094223022461
		 10 -11.073387145996094 11 -10.08329963684082 12 -8.7775402069091797 13 -7.0720429420471191
		 14 -5.0078530311584473 15 -2.5477714538574219 16 0.1247428208589554 17 2.7194724082946777
		 18 5.0830464363098145 19 7.0686931610107422 20 8.5918111801147461 21 9.6210994720458984
		 22 10.125524520874023 23 10.134055137634277 24 9.7983198165893555 25 9.2282733917236328
		 26 8.4057435989379883 27 7.2938928604125977 28 5.8466782569885254 29 4.0450663566589355
		 30 1.914801239967346;
createNode animCurveTU -n "Character1_Spine_visibility";
	rename -uid "3806F84B-4884-95D7-2523-4585EE817EB7";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Spine1_translateX";
	rename -uid "6AFD6996-4AF3-FCC7-FC17-49B87E6DD5DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.930038452148437;
createNode animCurveTL -n "Character1_Spine1_translateY";
	rename -uid "1FBAE2E6-4D35-1586-51FC-8CBCEAF9C8E2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1661763191223145;
createNode animCurveTL -n "Character1_Spine1_translateZ";
	rename -uid "A0933122-4718-EBFC-2E47-DBB70866F611";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.40121376514434814;
createNode animCurveTU -n "Character1_Spine1_scaleX";
	rename -uid "0D47F4D8-4681-B249-188F-21BFE74E75EF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_Spine1_scaleY";
	rename -uid "8136D5D7-481B-43F5-3541-1EB3A4CEDDB5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine1_scaleZ";
	rename -uid "DA89C1D6-42A7-C3EF-3487-F5848F572155";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_Spine1_rotateX";
	rename -uid "5C0E2D5B-4FAA-C9F2-276E-BBB3BF09BD88";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -33.848346710205078 1 -33.780223846435547
		 2 -33.380638122558594 3 -32.630641937255859 4 -31.50151252746582 5 -29.937597274780273
		 6 -27.843801498413086 7 -25.119318008422852 8 -21.690374374389648 9 -17.742362976074219
		 10 -13.682454109191895 11 -9.7580404281616211 12 -6.1837892532348633 13 -3.1615710258483887
		 14 -0.92177426815032959 15 0.29995715618133545 16 0.73758482933044434 17 0.71165835857391357
		 18 0.10900916904211044 19 -1.1723155975341797 20 -3.2100591659545898 21 -6.045708179473877
		 22 -9.6906137466430664 23 -14.124889373779297 24 -18.777849197387695 25 -22.992870330810547
		 26 -26.65391731262207 27 -29.665103912353516 28 -31.931859970092773 29 -33.357830047607422
		 30 -33.848346710205078;
createNode animCurveTA -n "Character1_Spine1_rotateY";
	rename -uid "217134C3-436D-3805-27EC-AE9175023363";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 2.0392954349517822 1 2.0028853416442871
		 2 1.8374869823455811 3 1.4573643207550049 4 0.79224056005477905 5 -0.20496852695941925
		 6 -1.5373274087905884 7 -3.1874842643737793 8 -5.1481227874755859 9 -7.1807789802551278
		 10 -9.0101490020751953 11 -10.59343433380127 12 -11.917121887207031 13 -12.969675064086914
		 14 -13.74260425567627 15 -14.247074127197266 16 -14.628677368164061 17 -15.0194091796875
		 18 -15.367938995361326 19 -15.575175285339357 20 -15.512399673461914 21 -15.02237129211426
		 22 -13.936267852783203 23 -12.102517127990723 24 -9.6490764617919922 25 -6.9513673782348633
		 26 -4.267052173614502 27 -1.8265242576599123 28 0.1654374897480011 29 1.5168523788452148
		 30 2.0392954349517822;
createNode animCurveTA -n "Character1_Spine1_rotateZ";
	rename -uid "B49605C5-492B-A2E1-2BCB-7B894BF27CE1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 24.692970275878906 1 24.377233505249023
		 2 23.568778991699219 3 22.238357543945313 4 20.346858978271484 5 17.860076904296875
		 6 14.735824584960939 7 10.898969650268555 8 6.218416690826416 9 0.71301692724227905
		 10 -5.2684006690979004 11 -11.350702285766602 12 -17.077505111694336 13 -21.976995468139648
		 14 -25.625511169433594 15 -27.581686019897461 16 -28.165864944458008 17 -27.949619293212891
		 18 -26.88441276550293 19 -24.922346115112305 20 -22.004886627197266 21 -18.071977615356445
		 22 -13.096047401428223 23 -7.0752382278442383 24 -0.47344183921813959 25 5.9899024963378906
		 26 11.944440841674805 27 17.065891265869141 28 21.070810317993164 29 23.700933456420898
		 30 24.692970275878906;
createNode animCurveTU -n "Character1_Spine1_visibility";
	rename -uid "639046C0-48D8-7DF8-0EB3-509CB4FD50F4";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftShoulder_translateX";
	rename -uid "97B4347A-4DB2-3D77-B4DD-91B3EA2B9228";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.0842323303222656;
createNode animCurveTL -n "Character1_LeftShoulder_translateY";
	rename -uid "D087EBEC-4071-1D0F-BDFB-6885CFD11A75";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.604855537414551;
createNode animCurveTL -n "Character1_LeftShoulder_translateZ";
	rename -uid "8798974D-4D9B-D07D-90D3-41BCBAAECAB8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6404602527618408;
createNode animCurveTU -n "Character1_LeftShoulder_scaleX";
	rename -uid "DE47C2A1-4B6A-D933-417C-B3858CDBEA63";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftShoulder_scaleY";
	rename -uid "CD50C195-4F00-25B3-4E08-5DBB99BE4074";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftShoulder_scaleZ";
	rename -uid "871E3FD1-4D04-118C-E9D9-AF972B230817";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftShoulder_rotateX";
	rename -uid "8E4109EB-4BA7-99DA-E281-DC87DC245479";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -2.6812968254089355 1 -1.891088128089905
		 2 -1.0750534534454346 3 -0.31246861815452576 4 0.34633144736289978 5 0.88005024194717407
		 6 1.291088342666626 7 1.5948545932769775 8 1.8033788204193117 9 1.875164866447449
		 10 1.7525585889816284 11 1.3970291614532471 12 0.80245667695999146 13 0.033463999629020691
		 14 -0.75798845291137695 15 -1.339667797088623 16 -1.6488829851150513 17 -1.8116024732589722
		 18 -1.847404479980469 19 -1.7856227159500122 20 -1.6621972322463989 21 -1.5175142288208008
		 22 -1.3953958749771118 23 -1.3433815240859985 24 -1.5600224733352661 25 -1.8311735391616821
		 26 -2.1064112186431885 27 -2.3454310894012451 28 -2.523381233215332 29 -2.6324901580810547
		 30 -2.6812968254089355;
createNode animCurveTA -n "Character1_LeftShoulder_rotateY";
	rename -uid "2C54CB0E-4F3B-9DF8-BD5A-FF83D6BBE72C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 7.8043885231018066 1 6.2636194229125977
		 2 4.4072084426879883 3 2.4025795459747314 4 0.42721462249755859 5 -1.3342647552490234
		 6 -2.6937370300292969 7 -3.4626064300537109 8 -3.4535958766937256 9 -2.2984685897827148
		 10 -0.0093146273866295815 11 3.0227706432342529 12 6.4032859802246094 13 9.7445001602172852
		 14 12.677220344543457 15 14.856408119201662 16 16.371942520141602 17 17.58460807800293
		 18 18.567239761352539 19 19.390003204345703 20 20.121608734130859 21 20.830253601074219
		 22 21.584112167358398 23 22.451303482055664 24 21.549554824829102 25 19.918865203857422
		 26 17.757411956787109 27 15.264729499816896 28 12.64142894744873 29 10.088166236877441
		 30 7.8043885231018066;
createNode animCurveTA -n "Character1_LeftShoulder_rotateZ";
	rename -uid "1D53E2CE-4B99-C547-675F-95B4C345D43F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -3.4346048831939697 1 -1.5091153383255005
		 2 0.58554142713546753 3 2.7271633148193359 4 4.8042855262756348 5 6.7119588851928711
		 6 8.3468246459960937 7 9.6039209365844727 8 10.376479148864746 9 10.434800148010254
		 10 9.7846794128417969 11 8.6623697280883789 12 7.2920017242431632 13 5.8995604515075684
		 14 4.734306812286377 15 4.0823788642883301 16 3.9320342540740967 17 4.0323147773742676
		 18 4.3313446044921875 19 4.7730746269226074 20 5.2986526489257813 21 5.8475303649902344
		 22 6.3580207824707031 23 6.7671012878417969 24 5.9821009635925293 25 4.7686548233032227
		 26 3.2566866874694824 27 1.5684397220611572 28 -0.18203583359718323 29 -1.8858095407485962
		 30 -3.4346048831939697;
createNode animCurveTU -n "Character1_LeftShoulder_visibility";
	rename -uid "3BE240AF-4C70-DDB4-9AB9-B0AE39C43779";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftArm_translateX";
	rename -uid "DDFB8E72-4FD4-51A9-D70E-D890EBE704C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 9.2092180252075195;
createNode animCurveTL -n "Character1_LeftArm_translateY";
	rename -uid "80F6E366-4B28-DA5B-63E5-2D886961E313";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.4172050952911377;
createNode animCurveTL -n "Character1_LeftArm_translateZ";
	rename -uid "F1937BE7-4E97-60D0-F451-36AF9D766938";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.69030004739761353;
createNode animCurveTU -n "Character1_LeftArm_scaleX";
	rename -uid "A41C8F63-4CE4-B9AE-D035-5E8AFAA05DC2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleY";
	rename -uid "EE9052AC-4002-D4E5-BBB8-31BE86F8B79B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleZ";
	rename -uid "6148262C-421F-AA30-8A63-EF9427ACD434";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_LeftArm_rotateX";
	rename -uid "3E31C3E4-4527-9D9B-F877-D4A4BB2CE354";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 35.883331298828125 1 36.832927703857422
		 2 37.981651306152344 3 39.265434265136719 4 40.604480743408203 5 41.902793884277344
		 6 43.05194091796875 7 43.938987731933594 8 44.458377838134766 9 44.414699554443359
		 10 43.847888946533203 11 43.017219543457031 12 42.135887145996094 13 41.338897705078125
		 14 40.675998687744141 15 40.134414672851563 16 39.614093780517578 17 39.023017883300781
		 18 38.355030059814453 19 37.611789703369141 20 36.795269012451172 21 35.902908325195313
		 22 34.925563812255859 23 33.848365783691406 24 33.576633453369141 25 33.627006530761719
		 26 33.916660308837891 27 34.362937927246094 28 34.886749267578125 29 35.415252685546875
		 30 35.883331298828125;
createNode animCurveTA -n "Character1_LeftArm_rotateY";
	rename -uid "AAD0ED5E-48E9-717E-B5F0-B795727A043B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -13.139154434204102 1 -14.228398323059082
		 2 -15.457996368408205 3 -16.721506118774414 4 -17.905586242675781 5 -18.887832641601562
		 6 -19.538150787353516 7 -19.724458694458008 8 -19.32307243347168 9 -18.104558944702148
		 10 -16.176910400390625 11 -13.926360130310059 12 -11.680819511413574 13 -9.6890602111816406
		 14 -8.1376218795776367 15 -7.1858959197998056 16 -6.7560176849365234 17 -6.6239275932312012
		 18 -6.7324624061584473 19 -7.02801513671875 20 -7.459291934967041 21 -7.9764304161071786
		 22 -8.5302915573120117 23 -9.0722360610961914 24 -9.6381912231445313 25 -10.237249374389648
		 26 -10.85808277130127 27 -11.48343563079834 28 -12.090344429016113 29 -12.651824951171875
		 30 -13.139154434204102;
createNode animCurveTA -n "Character1_LeftArm_rotateZ";
	rename -uid "E2ED1A35-45E9-DCAA-6149-77BDFF6AEF36";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -42.218467712402344 1 -40.980480194091797
		 2 -39.549911499023438 3 -38.063499450683594 4 -36.648429870605469 5 -35.419193267822266
		 6 -34.479621887207031 7 -33.932247161865234 8 -33.895214080810547 9 -34.702785491943359
		 10 -36.466285705566406 11 -38.960830688476563 12 -41.870594024658203 13 -44.785778045654297
		 14 -47.240169525146484 15 -48.753608703613281 16 -49.335132598876953 17 -49.377582550048828
		 18 -48.984470367431641 19 -48.262252807617188 20 -47.3203125 21 -46.270961761474609
		 22 -45.229072570800781 23 -44.311817169189453 24 -43.706714630126953 25 -43.198738098144531
		 26 -42.803585052490234 27 -42.526519775390625 28 -42.357391357421875 29 -42.269004821777344
		 30 -42.218467712402344;
createNode animCurveTU -n "Character1_LeftArm_visibility";
	rename -uid "D9A4ABC2-4B82-4938-AE8A-ED895CCF3759";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftForeArm_translateX";
	rename -uid "A46B9674-41D9-3442-7AAB-30A2E92A03CA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.0427684783935547;
createNode animCurveTL -n "Character1_LeftForeArm_translateY";
	rename -uid "038670CD-4314-61D0-47CA-DC912B823D57";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.323208808898926;
createNode animCurveTL -n "Character1_LeftForeArm_translateZ";
	rename -uid "31226792-4235-BB56-A699-E0A487AEBC0C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 16.678167343139648;
createNode animCurveTU -n "Character1_LeftForeArm_scaleX";
	rename -uid "510A6164-4909-8331-A53F-FAA8A5049826";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftForeArm_scaleY";
	rename -uid "05656CE6-46C7-9B7C-D9E8-C695D8D33103";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftForeArm_scaleZ";
	rename -uid "73082FDD-4D6D-7902-9159-22A5AE96D06A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftForeArm_rotateX";
	rename -uid "F698C580-4CD2-1083-72BA-C7AA199382D5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -16.260894775390625 1 -17.165321350097656
		 2 -18.219003677368164 3 -19.333408355712891 4 -20.418975830078125 5 -21.385194778442383
		 6 -22.141134262084961 7 -22.59672737121582 8 -22.664627075195313 9 -22.179904937744141
		 10 -21.160009384155273 11 -19.772220611572266 12 -18.157825469970703 13 -16.462491989135742
		 14 -14.876339912414549 15 -13.647560119628906 16 -12.791049003601074 17 -12.118846893310547
		 18 -11.582220077514648 19 -11.123032569885254 20 -10.684928894042969 21 -10.21973991394043
		 22 -9.6904029846191406 23 -9.0724248886108398 24 -9.4312505722045898 25 -10.199275970458984
		 26 -11.265982627868652 27 -12.516526222229004 28 -13.838151931762695 29 -15.121846199035646
		 30 -16.260894775390625;
createNode animCurveTA -n "Character1_LeftForeArm_rotateY";
	rename -uid "6D7C60D7-4EF6-6682-FB7F-42A6112C8A26";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 3.5950589179992676 1 3.635046005249023
		 2 3.9402189254760742 3 4.3709726333618164 4 4.7791256904602051 5 5.0083084106445312
		 6 4.895876407623291 7 4.2757887840270996 8 2.9823176860809326 9 0.33775511384010315
		 10 -3.7593109607696533 11 -8.5978364944458008 12 -13.463784217834473 13 -17.655969619750977
		 14 -20.502243041992188 15 -21.367259979248047 16 -20.278114318847656 17 -17.878252029418945
		 18 -14.476184844970703 19 -10.376118659973145 20 -5.8792715072631836 21 -1.2848995923995972
		 22 3.109260082244873 23 7.0072250366210938 24 8.0585813522338867 25 8.1371364593505859
		 26 7.5229549407958993 27 6.4937243461608887 28 5.3218579292297363 29 4.2712764739990234
		 30 3.5950589179992676;
createNode animCurveTA -n "Character1_LeftForeArm_rotateZ";
	rename -uid "3CA71FA5-4A4B-AA53-BC9F-4EA3772220F9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 24.956369400024414 1 26.174676895141602
		 2 27.585029602050781 3 29.058551788330078 4 30.478696823120117 5 31.742420196533203
		 6 32.756591796875 7 33.429519653320313 8 33.657566070556641 9 33.251705169677734
		 10 32.130264282226563 11 30.3411865234375 12 28.003416061401367 13 25.352352142333984
		 14 22.759017944335938 15 20.707120895385742 16 19.281538009643555 17 18.207208633422852
		 18 17.438224792480469 19 16.909347534179688 20 16.550165176391602 21 16.291559219360352
		 22 16.06556510925293 23 15.800567626953123 24 16.435487747192383 25 17.434141159057617
		 26 18.719423294067383 27 20.218812942504883 28 21.842287063598633 29 23.471015930175781
		 30 24.956369400024414;
createNode animCurveTU -n "Character1_LeftForeArm_visibility";
	rename -uid "2BF02E2C-428D-1A2C-ACD7-60A2CF25F39A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHand_translateX";
	rename -uid "75A049DD-4A64-8460-4576-6998484A99E0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.29200950264930725;
createNode animCurveTL -n "Character1_LeftHand_translateY";
	rename -uid "5FA5F19A-4B34-8C56-413B-CB83C26880E4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -14.295157432556152;
createNode animCurveTL -n "Character1_LeftHand_translateZ";
	rename -uid "22925E3A-4661-CE54-2117-7AA284A3AC9C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6319241523742676;
createNode animCurveTU -n "Character1_LeftHand_scaleX";
	rename -uid "4579127C-4D29-8085-5402-FC8CB91476BD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHand_scaleY";
	rename -uid "4D66B902-49EA-C75B-DACD-A0A1F5849ABC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHand_scaleZ";
	rename -uid "3A4FEB8E-4E2A-EDD4-F965-E8802C5236EE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHand_rotateX";
	rename -uid "96265AE8-4A9C-7169-7ACE-DEB583E4602A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 3.7465405464172363 1 3.1170008182525635
		 2 2.4069576263427734 3 1.6683061122894287 4 0.95219457149505626 5 0.30881965160369873
		 6 -0.21270598471164703 7 -0.5643344521522522 8 -0.69888448715209961 9 -0.50295263528823853
		 10 0.022297242656350136 11 0.75334388017654419 12 1.5692442655563354 13 2.3519759178161621
		 14 2.9862585067749023 15 3.3584394454956055 16 3.4874248504638672 17 3.487694263458252
		 18 3.3882510662078857 19 3.2178514003753662 20 3.0055487155914307 21 2.7810478210449219
		 22 2.5748684406280518 23 2.4183082580566406 24 2.4837212562561035 25 2.6233620643615723
		 26 2.8178870677947998 27 3.0476508140563965 28 3.2926583290100098 29 3.5325367450714111
		 30 3.7465405464172363;
createNode animCurveTA -n "Character1_LeftHand_rotateY";
	rename -uid "52241D07-4335-4433-39E7-17BB99D19694";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 10.100974082946777 1 9.8600053787231445
		 2 9.5896759033203125 3 9.2964706420898437 4 8.9910125732421875 5 8.6880912780761719
		 6 8.4059934616088867 7 8.1651630401611328 8 7.9862132072448722 9 7.8905048370361319
		 10 7.8692111968994132 11 7.8941264152526847 12 7.9404945373535156 13 7.9916543960571289
		 14 8.0399370193481445 15 8.0841588973999023 16 8.1376876831054687 17 8.2112255096435547
		 18 8.300480842590332 19 8.4005422592163086 20 8.506382942199707 21 8.6131906509399414
		 22 8.716557502746582 23 8.8124589920043945 24 8.962733268737793 25 9.135746955871582
		 26 9.3242816925048828 27 9.5213193893432617 28 9.7203044891357422 29 9.9152917861938477
		 30 10.100974082946777;
createNode animCurveTA -n "Character1_LeftHand_rotateZ";
	rename -uid "30C5FACF-4F6B-C644-FF63-2BA146178FCD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -0.8326345682144165 1 -1.8169145584106445
		 2 -2.9421565532684326 3 -4.1198616027832031 4 -5.2627692222595215 5 -6.285041332244873
		 6 -7.102208137512207 7 -7.6308073997497567 8 -7.7876901626586914 9 -7.3643064498901358
		 10 -6.3520817756652832 11 -4.9666147232055664 12 -3.4244060516357422 13 -1.9442012310028076
		 14 -0.74703317880630493 15 -0.055417761206626892 16 0.15618930757045746 17 0.1025412455201149
		 18 -0.15526789426803589 19 -0.55602288246154785 20 -1.0384640693664551 21 -1.5413315296173096
		 22 -2.0033926963806152 23 -2.3634352684020996 24 -2.3450691699981689 25 -2.2053580284118652
		 26 -1.9768819808959961 27 -1.6923421621322632 28 -1.3846460580825806 29 -1.0869439840316772
		 30 -0.8326345682144165;
createNode animCurveTU -n "Character1_LeftHand_visibility";
	rename -uid "6B36B66E-4217-D34B-589B-A7B7AFDC77AD";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateX";
	rename -uid "C2BA8616-4E25-FB78-CC83-F984F05E9303";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1364259719848633;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateY";
	rename -uid "5E8525BB-40E8-4467-45F7-778804BB54C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4241994619369507;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateZ";
	rename -uid "992E2818-4850-92AE-7C2C-21872634ECEC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.7211661338806152;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleX";
	rename -uid "CBD0235F-46A7-AB88-F93B-14AF55D04B93";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleY";
	rename -uid "35541E02-4289-09D5-E89C-B89D2386A5C9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleZ";
	rename -uid "42025406-407D-E7F8-21CA-CD92CAAC76C2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateX";
	rename -uid "8F96EEA0-4E01-1C4B-EAE9-9DACEC1723FC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -35.857170104980469 8 -35.857170104980469
		 9 -36.847332000732422 10 -39.254642486572266 11 -42.286052703857422 12 -45.344192504882813
		 13 -47.986289978027344 14 -49.839694976806641 15 -50.542793273925781 16 -50.542793273925781
		 22 -50.542793273925781 23 -50.542793273925781 24 -50.0626220703125 25 -48.750095367431641
		 26 -46.780735015869141 27 -44.320659637451172 28 -41.540355682373047 29 -38.634170532226563
		 30 -35.857170104980469;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateY";
	rename -uid "C6BA2C59-4E79-2353-F703-C4A168498DC8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 58.664314270019524 8 58.664314270019524
		 9 57.167980194091797 10 53.306102752685547 11 48.021495819091797 12 42.261356353759766
		 13 36.974189758300781 14 33.107616424560547 15 31.608404159545895 16 31.608404159545895
		 22 31.608404159545895 23 31.608404159545895 24 32.634174346923828 25 35.395965576171875
		 26 39.420486450195313 27 44.234134674072266 28 49.362648010253906 29 54.331008911132813
		 30 58.664314270019524;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateZ";
	rename -uid "5E46F57B-4919-1450-ACCD-24A5420E4CC3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -8.1192960739135742 8 -8.1192960739135742
		 9 -9.3833293914794922 10 -12.515852928161621 11 -16.583425521850586 12 -20.830314636230469
		 13 -24.620189666748047 14 -27.348793029785156 15 -28.399675369262695 16 -28.399675369262695
		 22 -28.399675369262695 23 -28.399675369262695 24 -27.681026458740234 25 -25.73748779296875
		 26 -22.876680374145508 27 -19.392539978027344 28 -15.569900512695314 29 -11.700316429138184
		 30 -8.1192960739135742;
createNode animCurveTU -n "Character1_LeftHandThumb1_visibility";
	rename -uid "57522DBB-4815-3361-888D-7B8E693187D0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateX";
	rename -uid "93309BC1-4466-44A8-F0E5-F3B62A91C043";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.8329257965087891;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateY";
	rename -uid "31DCEB34-432A-F260-8D2B-52B9B8731F52";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.094426639378070831;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateZ";
	rename -uid "BFED6829-45E4-9481-29A3-82B9EDF7099D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5438374280929565;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleX";
	rename -uid "528DC74E-4C66-A630-482E-2889C59B647D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleY";
	rename -uid "D71F90CB-4CD2-B415-A5CE-9B8099906DDD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleZ";
	rename -uid "1A718C3B-4E81-0407-8401-44896E1626AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateX";
	rename -uid "1848209C-40CA-C500-655A-81B0FE44263F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -7.0229120254516602 8 -7.0229120254516602
		 9 -6.7025456428527832 10 -5.9324612617492676 11 -4.9172277450561523 12 -3.7571616172790527
		 13 -2.5746865272521973 14 -1.6066923141479492 15 -1.2025917768478394 16 -1.2025917768478394
		 22 -1.2025917768478394 23 -1.2025917768478394 24 -1.4809564352035522 25 -2.1916141510009766
		 26 -3.1394524574279785 27 -4.1661014556884766 28 -5.1760444641113281 29 -6.1317081451416016
		 30 -7.0229120254516602;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateY";
	rename -uid "C02B6409-4674-7DF7-8295-088B509DEABC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 55.072174072265625 8 55.072174072265625
		 9 52.416179656982422 10 45.566673278808594 11 36.205158233642578 12 26.01707649230957
		 13 16.683845520019531 14 9.8729000091552734 15 7.2361116409301749 16 7.2361116409301749
		 22 7.2361116409301749 23 7.2361116409301749 24 9.0399551391601562 25 13.902127265930176
		 26 20.999671936035156 27 29.504304885864258 28 38.579734802246094 29 47.383762359619141
		 30 55.072174072265625;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateZ";
	rename -uid "FDEFEE90-4516-59C7-BFC0-80806D9C16DD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -27.805004119873047 8 -27.805004119873047
		 9 -27.617288589477539 10 -27.266168594360352 11 -27.015508651733398 12 -26.971563339233398
		 13 -27.11750602722168 14 -27.339643478393555 15 -27.454065322875977 16 -27.454065322875977
		 22 -27.454065322875977 23 -27.454065322875977 24 -27.373994827270508 25 -27.19590950012207
		 26 -27.027988433837891 27 -26.962278366088867 28 -27.058181762695312 29 -27.343198776245117
		 30 -27.805004119873047;
createNode animCurveTU -n "Character1_LeftHandThumb2_visibility";
	rename -uid "7EFBD15A-40B3-7F70-6292-5E90F0333F96";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateX";
	rename -uid "80D10064-4076-06B3-F495-FE84094FF210";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7747817039489746;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateY";
	rename -uid "F79D7BD8-4579-556D-D97D-858E58591B44";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3182404041290283;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateZ";
	rename -uid "025ECA5F-4738-7F5D-0E3A-98B208199B6B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.26324462890625;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleX";
	rename -uid "A1CDD6CE-4778-0DA9-F02D-5BAEDD981444";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleY";
	rename -uid "21A8BF99-41B6-8961-1CA4-9CAD29214412";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleZ";
	rename -uid "76B757FF-4616-753D-7A98-1BB699751A6E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateX";
	rename -uid "22A70A75-42FE-CE43-9572-3AB26681554D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4541859627570375e-007;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateY";
	rename -uid "DA7B73F7-425A-2222-2A81-17A177568A92";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.4806861271572416e-008;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateZ";
	rename -uid "969CDE30-40CF-9450-111D-E0ABF80FFAE0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.9505185485450056e-008;
createNode animCurveTU -n "Character1_LeftHandThumb3_visibility";
	rename -uid "0CE9E644-4E86-3D98-2617-B7B5F310534B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateX";
	rename -uid "3184DC41-41A7-18D4-D001-DEB04B63B5DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.854037284851074;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateY";
	rename -uid "79F03D15-4806-2AED-E53E-868611CFFE5C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2616198062896729;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateZ";
	rename -uid "F4D1CF95-4D53-4D75-EE02-8EA180915AF5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.944699764251709;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleX";
	rename -uid "EEA2D6CC-4205-69FB-2D7A-42B33411D38D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleY";
	rename -uid "385C5C79-46FF-1CAD-9B7A-4BB926CF6B8F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleZ";
	rename -uid "0F5F1918-484D-2A90-D43F-FD8DF084697E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateX";
	rename -uid "F5DA108D-4B44-31C5-4EBD-5F8E80D7C19B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 60.010890960693366 8 60.010890960693366
		 9 58.740180969238288 10 55.448810577392578 11 50.930912017822266 12 46.014144897460938
		 13 41.534114837646484 14 38.291267395019531 15 37.043502807617188 16 37.043502807617188
		 22 37.043502807617188 23 37.043502807617188 24 37.896610260009766 25 40.206470489501953
		 26 43.601463317871094 27 47.695304870605469 28 52.077888488769531 29 56.323654174804688
		 30 60.010890960693366;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateY";
	rename -uid "00ACA806-4527-7FD5-49E3-43A53BE7A911";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 33.363674163818359 8 33.363674163818359
		 9 33.415081024169922 10 33.471641540527344 11 33.371746063232422 12 33.032016754150391
		 13 32.512851715087891 14 32.011215209960938 15 31.789722442626953 16 31.789722442626953
		 22 31.789722442626953 23 31.789722442626953 24 31.942878723144528 25 32.320339202880859
		 26 32.777351379394531 27 33.175205230712891 28 33.416416168212891 29 33.467323303222656
		 30 33.363674163818359;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateZ";
	rename -uid "E1B31D72-49FD-1D01-7675-D1AF7507ACC9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 86.987724304199219 8 86.987724304199219
		 9 85.703529357910156 10 82.393089294433594 11 77.883926391601562 12 73.019180297851563
		 13 68.622734069824219 14 65.461021423339844 15 64.248985290527344 16 64.248985290527344
		 22 64.248985290527344 23 64.248985290527344 24 65.077400207519531 25 67.326240539550781
		 26 70.6473388671875 27 74.677764892578125 28 79.0250244140625 29 83.270835876464844
		 30 86.987724304199219;
createNode animCurveTU -n "Character1_LeftHandIndex1_visibility";
	rename -uid "72B30528-47D7-E2D7-867E-449B50059C86";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateX";
	rename -uid "CD2598B8-4E38-0E15-B255-1999571EBAB7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.1721835136413574;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateY";
	rename -uid "A0C30E7A-4BD7-9A1B-AA2A-BBA272C75A38";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.9652466773986816;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateZ";
	rename -uid "04D80BBE-4A80-973D-A386-20A45267BDA7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.8871781826019287;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleX";
	rename -uid "6E70F0ED-4231-A4EE-9A0F-E19298BC2B70";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleY";
	rename -uid "12BABDB6-4BCB-9272-A2BE-C48009394B12";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleZ";
	rename -uid "189DAC7C-4255-CBA5-577F-A4A90088CEFF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateX";
	rename -uid "8ABADCA9-4FC7-62FB-C17D-33975A6F86D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 7.7885360717773429 8 7.7885360717773429
		 9 7.5761332511901847 10 6.994755744934082 11 6.1206469535827637 12 5.063075065612793
		 13 3.9953603744506836 14 3.1557519435882568 15 2.8169076442718506 16 2.8169076442718506
		 22 2.8169076442718506 23 2.8169076442718506 24 3.0495471954345703 25 3.6586391925811768
		 26 4.5009341239929199 27 5.4376611709594727 28 6.3511471748352051 29 7.1537413597106934
		 30 7.7885360717773429;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateY";
	rename -uid "161E24E7-411F-0885-E831-B48513DF14E6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -33.620090484619141 8 -33.620090484619141
		 9 -33.951988220214844 10 -34.794342041015625 11 -35.911808013916016 12 -37.079238891601563
		 13 -38.099624633789062 14 -38.812046051025391 15 -39.080127716064453 16 -39.080127716064453
		 22 -39.080127716064453 23 -39.080127716064453 24 -38.897209167480469 25 -38.393997192382813
		 26 -37.633907318115234 27 -36.685676574707031 28 -35.632244110107422 29 -34.572830200195313
		 30 -33.620094299316406;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateZ";
	rename -uid "94DEC939-45DC-AA46-6E8D-93A3FE836B26";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 80.491020202636719 8 80.491020202636719
		 9 81.412452697753906 10 83.812454223632812 11 87.151123046875 12 90.866752624511719
		 13 94.350959777832031 14 96.944892883300781 15 97.961204528808594 16 97.961204528808594
		 22 97.961204528808594 23 97.961204528808594 24 97.26519775390625 25 95.405014038085937
		 26 92.729934692382813 27 89.584930419921875 28 86.297630310058594 29 83.172340393066406
		 30 80.491020202636719;
createNode animCurveTU -n "Character1_LeftHandIndex2_visibility";
	rename -uid "DAF3A05E-42FF-EF59-83B5-AD97B51C8BE7";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateX";
	rename -uid "7A15D0DD-4EE0-72E2-C13C-B8B1441B6B22";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.5761528015136719;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateY";
	rename -uid "11C1577A-4A62-3717-70DA-09AB805DC63B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8397388458251953;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateZ";
	rename -uid "2C736A9A-4717-9C48-C467-629865B0AF9A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3069263696670532;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleX";
	rename -uid "E484E29D-4F0F-6F43-9BA8-70B5F319CC66";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleY";
	rename -uid "FDD987E3-4D0A-A20B-BD83-43991E332CA6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleZ";
	rename -uid "E5BE2A64-4917-63DF-1480-F7B7EE12E346";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateX";
	rename -uid "96785AB9-4DCF-1F0F-C26E-A580318D1878";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.8664206891116919e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateY";
	rename -uid "40EA4FFB-40F1-18B1-873B-49A8ED1E4AD7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.0388266831523651e-008;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateZ";
	rename -uid "242CA4DC-4A47-0A72-8D06-D6B6CE2F6162";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7101699256727443e-007;
createNode animCurveTU -n "Character1_LeftHandIndex3_visibility";
	rename -uid "DB6D2840-4556-900D-8B35-C88C8BFE5C2D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing1_translateX";
	rename -uid "91308B93-42A4-187C-7472-F9A557C45A3C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4602031707763672;
createNode animCurveTL -n "Character1_LeftHandRing1_translateY";
	rename -uid "7C5AB880-4EC5-9060-D782-C6938548554E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.5015654563903809;
createNode animCurveTL -n "Character1_LeftHandRing1_translateZ";
	rename -uid "43B0A4F4-47A5-94DD-3E80-249C56498A69";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.6178884506225586;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleX";
	rename -uid "4E10CEF2-4C6F-B4B4-FC4D-4DBB4B0115B8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleY";
	rename -uid "F16FF569-4071-F03F-CCC6-90A6CBA38567";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleZ";
	rename -uid "3BB917FF-41BB-456C-5860-F9B6D1ED9103";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateX";
	rename -uid "E0E8871C-49F9-0C44-C4A8-2BB92C7D474C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 32.34600830078125 8 32.34600830078125
		 9 31.881191253662109 10 30.687959671020508 11 29.079851150512695 12 27.376791000366211
		 13 25.875144958496094 14 24.822017669677734 15 24.425020217895508 16 24.425020217895508
		 22 24.425020217895508 23 24.425020217895508 24 24.695941925048828 25 25.440366744995117
		 26 26.561695098876953 27 27.95305061340332 28 29.484495162963867 29 31.00349235534668
		 30 32.34600830078125;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateY";
	rename -uid "4EEFED01-4C45-9E0C-3C77-DEAE3BDFA75B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 35.989974975585938 8 35.989974975585938
		 9 35.865089416503906 10 35.484447479248047 11 34.828094482421875 12 33.936740875244141
		 13 32.960693359375 14 32.153865814208984 15 31.820384979248047 16 31.820384979248047
		 22 31.820384979248047 23 31.820384979248047 24 32.049789428710938 25 32.640720367431641
		 26 33.430801391601563 27 34.262451171875 28 35.009407043457031 29 35.593650817871094
		 30 35.989974975585938;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateZ";
	rename -uid "F158E4AE-47A6-DD35-038A-2A9C11E631C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 90.74188232421875 8 90.74188232421875
		 9 89.549201965332031 10 86.484832763671875 11 82.331024169921875 12 77.869026184082031
		 13 73.8465576171875 14 70.955451965332031 15 69.846832275390625 16 69.846832275390625
		 22 69.846832275390625 23 69.846832275390625 24 70.604598999023438 25 72.66107177734375
		 26 75.698249816894531 27 79.3885498046875 28 83.380279541015625 29 87.295982360839844
		 30 90.74188232421875;
createNode animCurveTU -n "Character1_LeftHandRing1_visibility";
	rename -uid "BA9EA629-4AAD-D7B9-4A36-1D8F860428D0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing2_translateX";
	rename -uid "F25B9A02-4C14-6AEF-6BE6-7ABA197FA522";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.9237353801727295;
createNode animCurveTL -n "Character1_LeftHandRing2_translateY";
	rename -uid "A9F87A94-4101-B3B0-7E54-78B0DE479B8F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.0094923973083496;
createNode animCurveTL -n "Character1_LeftHandRing2_translateZ";
	rename -uid "1E7633E4-4898-5476-4F1A-D2AC904D8163";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8221585750579834;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleX";
	rename -uid "A8815DFD-42D2-4D12-810F-9E8A1D59AE2D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleY";
	rename -uid "600D8A49-4DA9-E484-1DA7-51B1528AA8CA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleZ";
	rename -uid "176F78EE-4228-71EB-9BD1-C3B754B423B2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateX";
	rename -uid "D1BE050F-4133-8C07-2263-EBBE35618EAD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 42.815162658691406 8 42.815162658691406
		 9 43.067024230957031 10 43.695632934570313 11 44.504810333251953 12 45.317008972167969
		 13 45.994842529296875 14 46.447052001953125 15 46.612030029296875 16 46.612030029296875
		 22 46.612030029296875 23 46.612030029296875 24 46.499790191650391 25 46.183994293212891
		 26 45.689533233642578 27 45.047271728515625 28 44.305137634277344 29 43.531833648681641
		 30 42.815162658691406;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateY";
	rename -uid "76D6044F-42A4-B3F7-22AE-9E9F6CD29D13";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -31.412193298339847 8 -31.412193298339847
		 9 -31.918266296386719 10 -33.232913970947266 11 -35.051475524902344 12 -37.058265686035156
		 13 -38.921131134033203 14 -40.294998168945313 15 -40.830120086669922 16 -40.830120086669922
		 22 -40.830120086669922 23 -40.830120086669922 24 -40.463844299316406 25 -39.480785369873047
		 26 -38.056846618652344 27 -36.368186950683594 28 -34.587856292724609 29 -32.882820129394531
		 30 -31.412193298339847;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateZ";
	rename -uid "8D2F0C0C-4C86-48E5-E265-67A2268E5D8E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 62.220687866210945 8 62.220687866210945
		 9 62.784584045410163 10 64.241424560546875 11 66.242149353027344 12 68.438873291015625
		 13 70.476524353027344 14 71.983528137207031 15 72.572357177734375 16 72.572357177734375
		 22 72.572357177734375 23 72.572357177734375 24 72.169189453125 25 71.089729309082031
		 26 69.53076171875 27 67.684181213378906 28 65.733329772949219 29 63.854488372802734
		 30 62.220687866210945;
createNode animCurveTU -n "Character1_LeftHandRing2_visibility";
	rename -uid "249F30D5-4BC0-B3E0-1F26-D5B5D5056528";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing3_translateX";
	rename -uid "75586C4A-4A75-891F-F798-8FBA86A4666E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6098947525024414;
createNode animCurveTL -n "Character1_LeftHandRing3_translateY";
	rename -uid "FCD22A42-40D1-E0F3-E31D-0EB1E4D7DEBC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2952473163604736;
createNode animCurveTL -n "Character1_LeftHandRing3_translateZ";
	rename -uid "B6465A59-427C-445A-EA22-DAB309C28790";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3554067611694336;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleX";
	rename -uid "04FDE087-42AD-1BE9-1B92-4AA487449F6A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleY";
	rename -uid "E269EE10-4A5C-A174-3D56-FC91E3C1C636";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleZ";
	rename -uid "E6A0A341-4FE5-A84A-EFD1-09A644E7A158";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateX";
	rename -uid "9F7D57A0-41CC-559F-D50F-378047526210";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8578125466083293e-007;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateY";
	rename -uid "EE4395F7-42AA-911D-1AD6-F7BDCE1A41E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.6230032429120911e-007;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateZ";
	rename -uid "6305BCE8-4CB4-182B-27B1-C7BBE4835DA4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5322412139084918e-007;
createNode animCurveTU -n "Character1_LeftHandRing3_visibility";
	rename -uid "8F58804F-4F2E-31A2-2874-33ABF01FB697";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightShoulder_translateX";
	rename -uid "3ED51DE2-4611-E843-12D4-48A62EEC71A3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 14.332768440246582;
createNode animCurveTL -n "Character1_RightShoulder_translateY";
	rename -uid "E49DD642-4B00-0B61-A6F6-F5805EDAE1EB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5798094272613525;
createNode animCurveTL -n "Character1_RightShoulder_translateZ";
	rename -uid "9B3F2B45-41DB-949C-11DF-A6812377944C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1086856126785278;
createNode animCurveTU -n "Character1_RightShoulder_scaleX";
	rename -uid "7B027451-4DA7-708C-3A1C-C589767B9F15";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightShoulder_scaleY";
	rename -uid "7DB64477-4089-F8B7-42E1-DB838AB0CAEA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightShoulder_scaleZ";
	rename -uid "6DCEA7A4-4B52-F940-DC29-E3905F9068DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightShoulder_rotateX";
	rename -uid "8C98B7ED-406B-5207-11C9-12B9E7563E54";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -1.9403674602508547 1 -2.5764157772064209
		 2 -3.1605088710784912 3 -3.5876588821411128 4 -3.8166460990905762 5 -3.8788528442382812
		 6 -3.8369994163513179 7 -3.7076582908630371 8 -3.4907362461090088 9 -3.1248197555541992
		 10 -2.5493326187133789 11 -1.7874096632003784 12 -0.91550749540328991 13 -0.066614389419555664
		 14 0.58228707313537598 15 0.83844316005706787 16 0.83844316005706787 22 0.83844316005706787
		 23 0.83844316005706787 24 0.71370893716812134 25 0.38540554046630859 26 -0.073288820683956146
		 27 -0.59109330177307129 28 -1.1056290864944458 29 -1.5673493146896362 30 -1.9403674602508547;
createNode animCurveTA -n "Character1_RightShoulder_rotateY";
	rename -uid "D40A6643-465B-2481-23E6-76A35A90A4CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -15.086688041687013 1 -17.760372161865234
		 2 -20.655773162841797 3 -23.191537857055664 4 -24.769563674926758 5 -25.157436370849609
		 6 -24.683160781860352 7 -23.588447570800781 8 -22.118940353393555 9 -20.15815544128418
		 10 -17.651971817016602 11 -14.936413764953615 12 -12.332124710083008 13 -10.137006759643555
		 14 -8.6261157989501953 15 -8.0629043579101562 16 -8.0629043579101562 22 -8.0629043579101562
		 23 -8.0629043579101562 24 -8.3199262619018555 25 -9.0157871246337891 26 -10.039632797241211
		 27 -11.279044151306152 28 -12.616668701171875 29 -13.928959846496582 30 -15.086688041687013;
createNode animCurveTA -n "Character1_RightShoulder_rotateZ";
	rename -uid "A95E1DF8-4C00-119E-2901-37942620C24D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -2.7853682041168213 1 -4.482572078704834
		 2 -6.2816858291625977 3 -7.8467478752136239 4 -8.8414068222045898 5 -9.1863393783569336
		 6 -9.0576982498168945 7 -8.4970216751098633 8 -7.5502691268920898 9 -6.115302562713623
		 10 -4.2237176895141602 11 -2.1186010837554932 12 -0.039140403270721436 13 1.7694351673126221
		 14 3.0497334003448486 15 3.5355179309844971 16 3.5355179309844971 22 3.5355179309844971
		 23 3.5355179309844971 24 3.2932724952697754 25 2.642493724822998 26 1.6976151466369629
		 27 0.57202571630477905 28 -0.62302470207214355 29 -1.7782604694366455 30 -2.7853682041168213;
createNode animCurveTU -n "Character1_RightShoulder_visibility";
	rename -uid "66F27047-4CC8-A3B3-D810-C5A2BB506A0E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightArm_translateX";
	rename -uid "DF468A12-4BDA-AF1E-891C-28AEDC6D6C9B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.8354010581970215;
createNode animCurveTL -n "Character1_RightArm_translateY";
	rename -uid "C1CF86CA-4FBD-E6AE-47DF-E0BE37C3E520";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1704782247543335;
createNode animCurveTL -n "Character1_RightArm_translateZ";
	rename -uid "5D451016-4A8A-06E5-761E-B9AF44731D62";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.3259291648864746;
createNode animCurveTU -n "Character1_RightArm_scaleX";
	rename -uid "C251DD7A-49E4-8E96-807E-C7B014933D61";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightArm_scaleY";
	rename -uid "E3843AE2-4304-4742-A381-E38569DF74EB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightArm_scaleZ";
	rename -uid "1BF133A1-4278-7C9A-3DAD-31A3E65C0776";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightArm_rotateX";
	rename -uid "CDDCE809-42C1-CA58-7A13-A1A9D845B618";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 31.438161849975582 1 29.071704864501957
		 2 26.833459854125977 3 25.321830749511719 4 24.887477874755859 5 25.718973159790039
		 6 27.2117919921875 7 28.546457290649414 8 29.06488037109375 9 28.826416015625 10 28.332632064819336
		 11 27.693321228027344 12 27.009164810180664 13 26.373895645141602 14 25.876686096191406
		 15 25.603219985961914 16 25.534910202026367 17 25.579673767089844 18 25.703716278076172
		 19 25.867843627929688 20 26.031108856201172 21 26.154441833496094 22 26.204364776611328
		 23 26.156877517700195 24 26.4937744140625 25 26.997200012207031 26 27.676340103149414
		 27 28.518558502197266 28 29.480962753295895 29 30.488193511962894 30 31.438161849975582;
createNode animCurveTA -n "Character1_RightArm_rotateY";
	rename -uid "A451B366-4940-D94B-52A8-65896B548096";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 23.139822006225586 1 21.157011032104492
		 2 18.696002960205078 3 16.396568298339844 4 15.069498062133791 5 15.543364524841309
		 6 17.430795669555664 7 19.643989562988281 8 21.026208877563477 9 21.492782592773438
		 10 21.767059326171875 11 21.906862258911133 12 21.96879768371582 13 21.999643325805664
		 14 22.032115936279297 15 22.084434509277344 16 22.150629043579102 17 22.209091186523438
		 18 22.243804931640625 19 22.238710403442383 20 22.180231094360352 21 22.059915542602539
		 22 21.876758575439453 23 21.638847351074219 24 21.941402435302734 25 22.283229827880859
		 26 22.620832443237305 27 22.909725189208984 28 23.108207702636719 29 23.187007904052734
		 30 23.139822006225586;
createNode animCurveTA -n "Character1_RightArm_rotateZ";
	rename -uid "0687B10B-4776-182E-1CCF-15968933233F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 37.001468658447266 1 31.955373764038082
		 2 26.737403869628906 3 21.765277862548828 4 17.297998428344727 5 13.283003807067871
		 6 9.7304115295410156 7 6.8465723991394043 8 4.5477628707885742 9 2.7409830093383789
		 10 1.4122978448867798 11 0.49450141191482544 12 -0.074633978307247162 13 -0.35524213314056396
		 14 -0.40596893429756165 15 -0.28442472219467163 16 0.097917750477790833 17 0.87665963172912598
		 18 2.0600550174713135 19 3.6512219905853267 20 5.6495199203491211 21 8.0522336959838867
		 22 10.856492042541504 23 14.061264038085937 24 16.96919059753418 25 20.082469940185547
		 26 23.373819351196289 27 26.800176620483398 28 30.289707183837891 29 33.736106872558594
		 30 37.001468658447266;
createNode animCurveTU -n "Character1_RightArm_visibility";
	rename -uid "0D89A521-4938-BB2D-5E5B-A58FF97F372B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightForeArm_translateX";
	rename -uid "1F003C18-418E-F28E-7ED3-E39718256E8B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.753132820129395;
createNode animCurveTL -n "Character1_RightForeArm_translateY";
	rename -uid "74E50A85-4D6A-4C57-0CF8-99865EB5D5B7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.862128257751465;
createNode animCurveTL -n "Character1_RightForeArm_translateZ";
	rename -uid "C69F5519-43A6-8CB9-9E41-DFA115271FDC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.671875953674316;
createNode animCurveTU -n "Character1_RightForeArm_scaleX";
	rename -uid "56DFB371-4206-033E-0CC6-D4A05794C2C5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightForeArm_scaleY";
	rename -uid "3AD7B4DB-4407-0BF9-DF7C-DCAA2F162487";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightForeArm_scaleZ";
	rename -uid "3C6DBD99-44D6-1E09-4070-E1A3EA57CECA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightForeArm_rotateX";
	rename -uid "88587F9E-45E3-0D57-1BE9-3DB45A211ABF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 2.9452180862426758 1 1.7590850591659546
		 2 0.40325593948364258 3 -0.84169238805770874 4 -1.5934063196182251 5 -1.4724433422088623
		 6 -0.65415078401565552 7 0.43008285760879517 8 1.3065569400787354 9 1.8876686096191408
		 10 2.3977830410003662 11 2.8143424987792969 12 3.125938892364502 13 3.3379909992218018
		 14 3.4747953414916992 15 3.5775063037872314 16 3.5346066951751709 17 3.2024576663970947
		 18 2.616553783416748 19 1.8481179475784302 20 0.99071586132049572 21 0.14816747605800629
		 22 -0.57412374019622803 23 -1.0758051872253418 24 -1.0118842124938965 25 -0.60271799564361572
		 26 0.037027981132268906 27 0.78915917873382568 28 1.556989312171936 29 2.2813651561737061
		 30 2.9452180862426758;
createNode animCurveTA -n "Character1_RightForeArm_rotateY";
	rename -uid "8092B85C-4882-0C45-FFC1-0583ABCA1E21";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 7.898810386657714 1 3.1574044227600098
		 2 -1.8792623281478882 3 -6.7599854469299316 4 -11.050278663635254 5 -14.547872543334961
		 6 -17.443378448486328 7 -19.901418685913086 8 -22.093273162841797 9 -24.085239410400391
		 10 -25.801834106445313 11 -27.22178840637207 12 -28.32737922668457 13 -29.101442337036133
		 14 -29.524291992187504 15 -29.571531295776367 16 -28.784564971923825 17 -26.883792877197266
		 18 -24.1025390625 19 -20.671653747558594 20 -16.825353622436523 21 -12.80429744720459
		 22 -8.8562602996826172 23 -5.2353191375732422 24 -2.8327987194061279 25 -0.7028118371963501
		 26 1.2001216411590576 27 2.9316866397857666 28 4.564873218536377 29 6.1883044242858887
		 30 7.898810386657714;
createNode animCurveTA -n "Character1_RightForeArm_rotateZ";
	rename -uid "4779AAF2-40C0-FC35-E931-60AB205B8E13";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -27.065401077270508 1 -23.979639053344727
		 2 -20.555431365966797 3 -17.430219650268555 4 -15.32037353515625 5 -14.608816146850588
		 6 -15.02607250213623 7 -16.315649032592773 8 -18.119421005249023 9 -20.313652038574219
		 10 -22.938728332519531 11 -25.808801651000977 12 -28.745004653930664 13 -31.581592559814453
		 14 -34.168907165527344 15 -36.37176513671875 16 -38.398296356201172 17 -40.459915161132813
		 18 -42.456104278564453 19 -44.309909820556641 20 -45.954849243164062 21 -47.32049560546875
		 22 -48.320644378662109 23 -48.846046447753906 24 -47.670875549316406 25 -45.36749267578125
		 26 -42.209762573242188 27 -38.487861633300781 28 -34.509201049804688 29 -30.593498229980472
		 30 -27.065401077270508;
createNode animCurveTU -n "Character1_RightForeArm_visibility";
	rename -uid "A147299A-4086-AEC8-AB82-D4801638A716";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHand_translateX";
	rename -uid "48FF0310-490B-E1D2-F51C-0AB7DF39E235";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7167695760726929;
createNode animCurveTL -n "Character1_RightHand_translateY";
	rename -uid "5B68640E-43F0-5036-E236-B4A474CFA11A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.314067840576172;
createNode animCurveTL -n "Character1_RightHand_translateZ";
	rename -uid "86248AED-49DC-C920-7834-408890B17E2F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4444055557250977;
createNode animCurveTU -n "Character1_RightHand_scaleX";
	rename -uid "97B3B49D-4195-C83A-107C-E7AAE36E4AE4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightHand_scaleY";
	rename -uid "BF2D81A9-4972-CE1A-DEFF-879766225925";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightHand_scaleZ";
	rename -uid "52665263-42D4-6D3C-10B9-28914764F988";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightHand_rotateX";
	rename -uid "A3EFE296-4195-9DFF-F2C3-66A017EBAD54";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -3.1234824657440186 1 -3.0720920562744141
		 2 -3.034398078918457 3 -3.0053374767303467 4 -2.9796991348266602 5 -2.9521334171295166
		 6 -2.9171631336212158 7 -2.8691976070404053 8 -2.8025462627410889 9 -1.941582202911377
		 10 0.13298505544662476 11 2.8891434669494629 12 5.8070755004882813 13 8.3916645050048828
		 14 10.175334930419922 15 10.709682464599609 16 10.209977149963379 17 9.2349491119384766
		 18 7.8509702682495108 19 6.1196584701538086 20 4.0959968566894531 21 1.8268009424209597
		 22 -0.64646917581558228 23 -3.2774922847747803 24 -4.058377742767334 25 -4.4582014083862305
		 26 -4.5314121246337891 27 -4.3369622230529785 28 -3.9555554389953613 29 -3.5010321140289307
		 30 -3.1234824657440186;
createNode animCurveTA -n "Character1_RightHand_rotateY";
	rename -uid "7E102E4E-45AE-5948-D211-31AD8120B887";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -1.1953624486923218 1 -0.7209550142288208
		 2 -0.24198509752750397 3 0.24008992314338681 4 0.72354155778884888 5 1.2063575983047485
		 6 1.6862331628799438 7 2.1605613231658936 8 2.6264297962188721 9 3.0422887802124023
		 10 3.3500072956085205 11 3.565922737121582 12 3.7271332740783691 13 3.8571572303771973
		 14 3.9530782699584961 15 4.001255989074707 16 3.9842691421508785 17 3.9228389263153081
		 18 3.8773617744445801 19 3.8863027095794678 20 3.9478652477264404 21 4.0116772651672363
		 22 3.9793474674224858 23 3.7121953964233398 24 3.3471355438232422 25 2.7792623043060303
		 26 2.0571861267089844 27 1.2417882680892944 28 0.39637288451194763 29 -0.42612537741661072
		 30 -1.1953624486923218;
createNode animCurveTA -n "Character1_RightHand_rotateZ";
	rename -uid "40B71B72-4DE5-E4B2-ABEC-0496618B7C8E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -1.1815009117126465 1 0.02898880839347839
		 2 1.2489736080169678 3 2.4762599468231201 4 3.7087466716766357 5 4.944434642791748
		 6 6.1814351081848145 7 7.4179644584655762 8 8.6523456573486328 9 9.5695924758911133
		 10 9.9661178588867187 11 9.9889554977416992 12 9.7906379699707031 13 9.5493383407592773
		 14 9.4679536819458008 15 9.7578563690185547 16 10.609524726867676 17 11.907516479492188
		 18 13.393342018127441 19 14.809934616088869 20 15.908011436462402 21 16.453170776367188
		 22 16.232242584228516 23 15.05689525604248 24 13.862320899963379 25 12.000726699829102
		 26 9.6490077972412109 27 6.9760050773620605 28 4.1534104347229004 29 1.3660602569580078
		 30 -1.1815009117126465;
createNode animCurveTU -n "Character1_RightHand_visibility";
	rename -uid "6172E28D-4E74-A557-DDE5-D1B9C6DABC86";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb1_translateX";
	rename -uid "6ED395CE-4DEC-2116-D23C-46AF4834CC8E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5741705894470215;
createNode animCurveTL -n "Character1_RightHandThumb1_translateY";
	rename -uid "10B589C9-4ACA-04A1-A41B-2E8035F60A39";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4327812194824219;
createNode animCurveTL -n "Character1_RightHandThumb1_translateZ";
	rename -uid "3474AC75-4FBD-E042-C934-F68824B2D94E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8196568489074707;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleX";
	rename -uid "34D3A0CE-42EA-9650-7D3D-CBA4C0A9BBB4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleY";
	rename -uid "D9539154-4DC7-D675-0407-34A988A10875";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleZ";
	rename -uid "A18088F0-4C0F-1292-2459-90B4494C8646";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateX";
	rename -uid "43641602-4D42-8627-B1A2-35923B40FC37";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 57.314090728759766 8 57.314090728759766
		 9 56.862846374511719 10 55.877830505371094 11 54.953041076660156 12 54.500595092773437
		 13 54.582199096679688 14 54.928424835205078 15 55.124469757080078 16 55.124469757080078
		 22 55.124469757080078 23 55.124469757080078 24 54.986698150634766 25 54.695175170898438
		 26 54.486705780029297 27 54.590919494628906 28 55.141330718994141 29 56.113887786865234
		 30 57.314090728759766;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateY";
	rename -uid "C17E51BE-456B-DB88-887A-CFA349326E79";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 13.127543449401855 8 13.127543449401855
		 9 13.198640823364258 10 13.399381637573242 11 13.675023078918457 12 13.901430130004883
		 13 13.964169502258301 14 13.88017463684082 15 13.811562538146973 16 13.811562538146973
		 22 13.811562538146973 23 13.811562538146973 24 13.860834121704102 25 13.945049285888672
		 26 13.957213401794434 27 13.838061332702637 28 13.608199119567871 29 13.344630241394043
		 30 13.127543449401855;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateZ";
	rename -uid "55C31E44-46AB-D5EB-A65A-69AF54E1493E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 13.751087188720703 8 13.751087188720703
		 9 15.338658332824707 10 19.477733612060547 11 25.229764938354492 12 31.593252182006839
		 13 37.490921020507813 14 41.817245483398437 15 43.493629455566406 16 43.493629455566406
		 22 43.493629455566406 23 43.493629455566406 24 42.346813201904297 25 39.256584167480469
		 26 34.757614135742188 27 29.404706954956051 28 23.761213302612305 29 18.373600006103516
		 30 13.751087188720703;
createNode animCurveTU -n "Character1_RightHandThumb1_visibility";
	rename -uid "44BE771B-415B-6881-E7B1-BE97F96BDD72";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb2_translateX";
	rename -uid "19246CA3-4C4D-CCAD-3B5E-71A75D7F0FB7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7422792911529541;
createNode animCurveTL -n "Character1_RightHandThumb2_translateY";
	rename -uid "C1FD1D85-4F68-071C-4C7A-A190583DA5D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5767025947570801;
createNode animCurveTL -n "Character1_RightHandThumb2_translateZ";
	rename -uid "7D797ABE-4848-324B-6791-CBB5A5B0EF2B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0428388118743896;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleX";
	rename -uid "1FB46371-493A-32A9-18A1-86913CF8C716";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleY";
	rename -uid "59ED1797-49D6-8D52-F144-068D999BF059";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleZ";
	rename -uid "45591A97-457C-7F19-12C2-0EB35AD2ED9F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateX";
	rename -uid "B54228AD-4B9A-84EC-50DF-44BDC40B6EDD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 13.273929595947266 8 13.273929595947266
		 9 11.285896301269531 10 6.0909590721130371 11 -1.2578722238540649 12 -9.7923345565795898
		 13 -18.397548675537109 14 -25.388433456420898 15 -28.308244705200195 16 -28.308244705200195
		 22 -28.308244705200195 23 -28.308244705200195 24 -26.296375274658203 25 -21.166244506835938
		 26 -14.301273345947266 27 -6.7897830009460449 28 0.6415441632270813 29 7.4807038307189941
		 30 13.273929595947266;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateY";
	rename -uid "0425D906-4E32-F4B2-1CFE-F29FB86F83D1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -25.162002563476563 8 -25.162002563476563
		 9 -26.788148880004883 10 -31.003322601318356 11 -36.790447235107422 12 -43.071067810058594
		 13 -48.740764617919922 14 -52.774112701416016 15 -54.299861907958984 16 -54.299861907958984
		 22 -54.299861907958984 23 -54.299861907958984 24 -53.258609771728516 25 -50.401885986328125
		 26 -46.134292602539063 27 -40.927642822265625 28 -35.321758270263672 29 -29.88262939453125
		 30 -25.162002563476563;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateZ";
	rename -uid "137F5BDF-4ABE-700C-0F49-DEB47DB18A15";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -0.82732135057449341 8 -0.82732135057449341
		 9 -1.0987701416015625 10 -1.5937032699584961 11 -1.7376594543457031 12 -1.0441519021987915
		 13 0.61179035902023315 14 2.6459290981292725 15 3.6686680316925044 16 3.6686680316925044
		 22 3.6686680316925044 23 3.6686680316925044 24 2.9533751010894775 25 1.3455767631530762
		 26 -0.29572403430938721 27 -1.3957189321517944 28 -1.7648465633392334 29 -1.4923301935195923
		 30 -0.82732129096984863;
createNode animCurveTU -n "Character1_RightHandThumb2_visibility";
	rename -uid "A462ECF2-47D3-B0D7-EBF7-6A8CBEDC8DB8";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb3_translateX";
	rename -uid "D2E163F2-46D2-4176-054E-C2BF72E487F1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.588839054107666;
createNode animCurveTL -n "Character1_RightHandThumb3_translateY";
	rename -uid "F076AF09-4BD6-ACFA-E6D9-4ABF81CC1B94";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.13479287922382355;
createNode animCurveTL -n "Character1_RightHandThumb3_translateZ";
	rename -uid "8571015C-4D40-570D-8677-16B2B6424A17";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.4058942794799805;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleX";
	rename -uid "E0E93295-46F6-C93F-2FCB-34882344CC6B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleY";
	rename -uid "298812A8-4535-0554-9CFD-A79726D09325";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleZ";
	rename -uid "82681169-4D67-A330-DC92-3896F272A7C5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateX";
	rename -uid "ACA5CE23-4605-4BC3-7DFB-9DA0C9994C80";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.2444496506941505e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateY";
	rename -uid "9397EE5D-4F24-EB09-9768-219DC9DB3694";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.0051656798095802e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateZ";
	rename -uid "CB87ED7F-46B1-C476-1741-FB950F52282C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.4242117181478875e-010;
createNode animCurveTU -n "Character1_RightHandThumb3_visibility";
	rename -uid "672B59D2-4DB2-2206-1999-909F4C11A04A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex1_translateX";
	rename -uid "BBD0FD62-400F-FAF7-10E7-E3952681A155";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.25625163316726685;
createNode animCurveTL -n "Character1_RightHandIndex1_translateY";
	rename -uid "850A72DF-41B1-08F0-45B3-8EB13BBB5109";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.892666816711426;
createNode animCurveTL -n "Character1_RightHandIndex1_translateZ";
	rename -uid "A56F8319-4F87-7BEF-3A26-449E974F5FD7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.533165454864502;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleX";
	rename -uid "36E67F03-49BE-B208-5EF0-89AF2D5FC9B3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleY";
	rename -uid "F001F5A9-4646-5C4F-E542-1585E5D749F2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleZ";
	rename -uid "8064B8C7-42C6-5813-38B1-9FA6382989EF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateX";
	rename -uid "36BF8A33-4890-0825-C02E-60957E4CA043";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -1.2556071281433105 8 -1.2556071281433105
		 9 -1.066382884979248 10 -0.565498948097229 11 0.14836694300174713 12 0.96220707893371571
		 13 1.740056037902832 14 2.3264071941375732 15 2.5575330257415771 16 2.5575330257415771
		 22 2.5575330257415771 23 2.5575330257415771 24 2.3991715908050537 25 1.9776598215103149
		 26 1.376619815826416 27 0.67941498756408691 28 -0.035845022648572922 29 -0.70017147064208984
		 30 -1.2556071281433105;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateY";
	rename -uid "5873CD06-4383-51B0-9C8B-73889B8A8189";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 10.129959106445312 8 10.129959106445312
		 9 9.9078550338745117 10 9.3254842758178711 11 8.5052404403686523 12 7.577301025390625
		 13 6.6916775703430176 14 6.0221567153930664 15 5.7574014663696289 16 5.7574014663696289
		 22 5.7574014663696289 23 5.7574014663696289 24 5.9388642311096191 25 6.4206857681274414
		 26 7.1056308746337891 27 7.8992948532104492 28 8.7161035537719727 29 9.4813747406005859
		 30 10.129959106445312;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateZ";
	rename -uid "4B2C6DBE-4669-A095-72FD-3A9A7F1324BF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 69.348670959472656 8 69.348670959472656
		 9 70.5616455078125 10 73.683952331542969 11 77.9381103515625 12 82.550880432128906
		 13 86.761558532714844 14 89.825790405273438 15 91.010292053222656 16 91.010292053222656
		 22 91.010292053222656 23 91.010292053222656 24 90.200065612792969 25 88.013885498046875
		 26 84.816215515136719 27 80.973960876464844 28 76.860458374023437 29 72.856437683105469
		 30 69.348670959472656;
createNode animCurveTU -n "Character1_RightHandIndex1_visibility";
	rename -uid "2CDE7015-4BD1-1F7B-9ED4-86ADD6EFFA18";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex2_translateX";
	rename -uid "D426D2E8-485A-0A95-3ED8-68A2BE679D44";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.3959341049194336;
createNode animCurveTL -n "Character1_RightHandIndex2_translateY";
	rename -uid "6DA2F714-46AB-7012-0644-2F919FE00FAF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8172028064727783;
createNode animCurveTL -n "Character1_RightHandIndex2_translateZ";
	rename -uid "7151C07A-4A34-8428-3E2F-F4B64D74C458";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.52152138948440552;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleX";
	rename -uid "351B7AFA-4B8F-4BEB-5C97-D6982FA71B86";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleY";
	rename -uid "34ACFC48-4E92-0BFB-0233-459DE910CE39";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleZ";
	rename -uid "3CDB639C-4107-61B5-3DDE-19BBA0345C03";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateX";
	rename -uid "0CC99FFC-4C7A-FE26-5764-2EA0B71E9223";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 15.230603218078615 8 15.230603218078615
		 9 16.141597747802734 10 18.134546279907227 11 20.189414978027344 12 21.768196105957031
		 13 22.766750335693359 14 23.28477668762207 15 23.444889068603516 16 23.444889068603516
		 22 23.444889068603516 23 23.444889068603516 24 23.337640762329102 25 22.997552871704102
		 26 22.350742340087891 27 21.292608261108398 28 19.730470657348633 29 17.651447296142578
		 30 15.230603218078615;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateY";
	rename -uid "253B5AA5-4CEA-788E-5807-66B6DBF3E286";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -73.199981689453125 8 -73.199981689453125
		 9 -72.58209228515625 10 -70.960746765136719 11 -68.69378662109375 12 -66.178024291992188
		 13 -63.840908050537109 14 -62.12006759643554 15 -61.450847625732429 16 -61.450847625732429
		 22 -61.450847625732429 23 -61.450847625732429 24 -61.908836364746094 25 -63.139495849609375
		 26 -64.924934387207031 27 -67.043853759765625 28 -69.273460388183594 29 -71.394401550292969
		 30 -73.199981689453125;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateZ";
	rename -uid "0C4C5046-4FC6-313D-6E9A-E29619E9D3FF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 68.75714111328125 8 68.75714111328125
		 9 67.309333801269531 10 63.927761077880859 11 59.963207244873047 12 56.287250518798828
		 13 53.346652984619141 14 51.396686553955078 15 50.678485870361328 16 50.678485870361328
		 22 50.678485870361328 23 50.678485870361328 24 51.167758941650391 25 52.532627105712891
		 26 54.663406372070313 27 57.483131408691406 28 60.907981872558594 29 64.779899597167969
		 30 68.75714111328125;
createNode animCurveTU -n "Character1_RightHandIndex2_visibility";
	rename -uid "EFECF253-4595-0391-050B-06B2DDC9C71D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex3_translateX";
	rename -uid "F4526678-445A-B23C-843B-BB8A2AEA4818";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6282989978790283;
createNode animCurveTL -n "Character1_RightHandIndex3_translateY";
	rename -uid "DFB35EBD-4894-22FA-7BAE-18948A30E9A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.2120883464813232;
createNode animCurveTL -n "Character1_RightHandIndex3_translateZ";
	rename -uid "D53CA0DE-477C-7C3E-7CF8-14AEEA1CE425";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7757487297058105;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleX";
	rename -uid "58905789-4657-6F31-9964-168B8C99F91A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleY";
	rename -uid "A00047E6-4526-C1AB-06C5-0C95C1884877";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleZ";
	rename -uid "A495A433-41F7-7471-0755-D08E541FB069";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateX";
	rename -uid "D9232942-4FF9-D918-C912-7F88BBA6C94A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.0895308832914452e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateY";
	rename -uid "F14F662E-421C-0348-3732-92A528BDAA1D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.0906300569076848e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateZ";
	rename -uid "C8E53200-4303-BBC2-70C9-FEAECAEBD24D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.9188413463998586e-007;
createNode animCurveTU -n "Character1_RightHandIndex3_visibility";
	rename -uid "F3E4A5BF-44AE-EB9F-7BC3-D08DF4A2078A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing1_translateX";
	rename -uid "9FCEF4FF-47F3-CD3B-6DD0-39B0D8B0E85A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8543868064880371;
createNode animCurveTL -n "Character1_RightHandRing1_translateY";
	rename -uid "74BC324D-4AAA-F9FF-0561-84AFEF7C6D15";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2156095504760742;
createNode animCurveTL -n "Character1_RightHandRing1_translateZ";
	rename -uid "5E352E30-4E98-2EE9-086D-1EBD0CDB5294";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2236912250518799;
createNode animCurveTU -n "Character1_RightHandRing1_scaleX";
	rename -uid "77445650-4D44-595D-F40F-7699A2B7EC82";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandRing1_scaleY";
	rename -uid "9752B8EA-4D07-6C9A-BA8F-13B50DCD3F7D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing1_scaleZ";
	rename -uid "D990748D-48BF-EDAC-6D48-8FA00C28B8B1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandRing1_rotateX";
	rename -uid "B90743CE-49EF-DB52-74D4-9D871F166B58";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -9.4753284454345703 8 -9.4753284454345703
		 9 -9.2732028961181641 10 -8.7369060516357422 11 -7.9680609703063956 12 -7.0826740264892578
		 13 -6.2252798080444336 14 -5.5705170631408691 15 -5.3102340698242187 16 -5.3102340698242187
		 22 -5.3102340698242187 23 -5.3102340698242187 24 -5.4887113571166992 25 -5.9608855247497559
		 26 -6.6273775100708008 27 -7.3915591239929199 28 -8.1670694351196289 29 -8.8813114166259766
		 30 -9.4753284454345703;
createNode animCurveTA -n "Character1_RightHandRing1_rotateY";
	rename -uid "4EBB2E90-43CD-93D9-AFD5-3EBEC0FD40FF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 15.397130966186525 8 15.397130966186525
		 9 15.851300239562988 10 17.019504547119141 11 18.608356475830078 12 20.325881958007813
		 13 21.887266159057617 14 23.018718719482422 15 23.454843521118164 16 23.454843521118164
		 22 23.454843521118164 23 23.454843521118164 24 23.156604766845703 25 22.350215911865234
		 26 21.166772842407227 27 19.739463806152344 28 18.20625114440918 29 16.710037231445313
		 30 15.397130966186525;
createNode animCurveTA -n "Character1_RightHandRing1_rotateZ";
	rename -uid "AE73B53F-4F55-9CF2-2EFD-75B405868C48";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 69.758003234863281 8 69.758003234863281
		 9 70.699798583984375 10 73.134559631347656 11 76.479217529296875 12 80.146949768066406
		 13 83.538017272949219 14 86.034843444824219 15 87.007095336914063 16 87.007095336914063
		 22 87.007095336914063 23 87.007095336914063 24 86.34161376953125 25 84.555335998535156
		 26 81.965858459472656 27 78.887886047363281 28 75.628738403320313 29 72.48773193359375
		 30 69.758003234863281;
createNode animCurveTU -n "Character1_RightHandRing1_visibility";
	rename -uid "95E44A01-4F06-29EA-FE39-CC91B16A0AC1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing2_translateX";
	rename -uid "44A444BC-4561-C5E6-17F2-3ABFBA1F9269";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.062777042388916;
createNode animCurveTL -n "Character1_RightHandRing2_translateY";
	rename -uid "793D77B8-4360-BD76-0578-CF935DF10478";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.4930424690246582;
createNode animCurveTL -n "Character1_RightHandRing2_translateZ";
	rename -uid "107AAD0B-4D62-E57A-D9A5-CC8D3D01C6DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.86127841472625732;
createNode animCurveTU -n "Character1_RightHandRing2_scaleX";
	rename -uid "DFF30796-4A79-ACE6-C28C-70AD3CB02D2C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandRing2_scaleY";
	rename -uid "9B4ACFDC-4BF1-B5CC-37DE-4EB213F58567";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing2_scaleZ";
	rename -uid "BA1C6EA4-47C4-89A6-9E67-2A8E0799C209";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandRing2_rotateX";
	rename -uid "1DE292FB-4342-545A-ABC0-39BB96F6CA07";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 14.933643341064453 8 14.933643341064453
		 9 15.86990261077881 10 17.914699554443359 11 20.017988204956055 12 21.630653381347656
		 13 22.64959716796875 14 23.178302764892578 15 23.341846466064453 16 23.341846466064453
		 22 23.341846466064453 23 23.341846466064453 24 23.232290267944336 25 22.885112762451172
		 26 22.225135803222656 27 21.145126342773438 28 19.548669815063477 29 17.419469833374023
		 30 14.933643341064453;
createNode animCurveTA -n "Character1_RightHandRing2_rotateY";
	rename -uid "4B4F76B8-4F0D-8697-C216-F7AA741401CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -73.483367919921875 8 -73.483367919921875
		 9 -72.864669799804688 10 -71.240486145019531 11 -68.968528747558594 12 -66.44647216796875
		 13 -64.103134155273437 14 -62.377578735351563 15 -61.706504821777337 16 -61.706504821777337
		 22 -61.706504821777337 23 -61.706504821777337 24 -62.165763854980469 25 -63.399810791015625
		 26 -65.190078735351563 27 -67.314521789550781 28 -69.549560546875 29 -71.674980163574219
		 30 -73.483367919921875;
createNode animCurveTA -n "Character1_RightHandRing2_rotateZ";
	rename -uid "E6736069-4492-BD56-CCBE-A4B17F8C22F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 68.698143005371094 8 68.698143005371094
		 9 67.227127075195313 10 63.799083709716797 11 59.793647766113288 12 56.092254638671875
		 13 53.139259338378906 14 51.184589385986328 15 50.465305328369141 16 50.465305328369141
		 22 50.465305328369141 23 50.465305328369141 24 50.955280303955078 25 52.322944641113281
		 26 54.460742950439453 27 57.295154571533203 28 60.746864318847656 29 64.661911010742187
		 30 68.698143005371094;
createNode animCurveTU -n "Character1_RightHandRing2_visibility";
	rename -uid "0C01BFE8-45DB-C4F6-6ACF-0CB0ED552BE9";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing3_translateX";
	rename -uid "2A1C24CA-4810-6F4E-7F03-9AA35CFB5AA5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.5035178661346436;
createNode animCurveTL -n "Character1_RightHandRing3_translateY";
	rename -uid "BD472D65-4C81-FF01-C453-45910B38159C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.9153993129730225;
createNode animCurveTL -n "Character1_RightHandRing3_translateZ";
	rename -uid "5AB403DB-4FEA-EE31-3BD0-4B816C49F842";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.6898331642150879;
createNode animCurveTU -n "Character1_RightHandRing3_scaleX";
	rename -uid "C3B21B8F-4435-FDAC-684C-7D8617C20437";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing3_scaleY";
	rename -uid "59589E38-4B7B-7C6E-EA63-0D9CACFD3822";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing3_scaleZ";
	rename -uid "D0603652-441F-F427-69C8-B287264B6A82";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandRing3_rotateX";
	rename -uid "C2DF4629-4FB2-8124-112E-F296296D7FC1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.9143814351282344e-008;
createNode animCurveTA -n "Character1_RightHandRing3_rotateY";
	rename -uid "5919C83F-4EEB-DDDA-2092-87A4CDE25711";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.5359698219726857e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateZ";
	rename -uid "79925A3E-4E44-D6A7-54B9-859A411C42C3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.1828363944441662e-007;
createNode animCurveTU -n "Character1_RightHandRing3_visibility";
	rename -uid "6969039B-49AD-09C7-86E3-3AB35459E0FC";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Neck_translateX";
	rename -uid "843B01C4-4621-B4A2-3C12-B48F41FE62DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 12.791294097900391;
createNode animCurveTL -n "Character1_Neck_translateY";
	rename -uid "626B35D4-428A-BE7E-8BFA-63B11814D5B4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.3580474853515625;
createNode animCurveTL -n "Character1_Neck_translateZ";
	rename -uid "9164F172-4C28-2AFF-0CE8-CD8831B481E3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.6322288513183594;
createNode animCurveTU -n "Character1_Neck_scaleX";
	rename -uid "257D341C-45FE-4B45-5FB2-5B98292C4A48";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_Neck_scaleY";
	rename -uid "1F1DB20A-4EF8-6BFE-5D41-D587FCBE8F8E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Neck_scaleZ";
	rename -uid "28C2EBFB-49CE-8601-5393-068C8DA9B5FF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Neck_rotateX";
	rename -uid "5E374C31-4DD4-BA97-60F2-4BA37A346AF8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 1.3273299932479858 1 1.066335916519165
		 2 0.56129223108291626 3 -0.08182041347026825 4 -0.7557443380355835 5 -1.3549840450286865
		 6 -1.7782888412475586 7 -1.9302732944488528 8 -1.7219895124435425 9 -1.1683671474456787
		 10 -0.40700283646583557 11 0.46028509736061096 12 1.3294112682342529 13 2.0936791896820068
		 14 2.6435129642486572 15 2.8675644397735596 16 2.8143763542175293 17 2.612076997756958
		 18 2.2692441940307617 19 1.7977012395858765 20 1.215096116065979 21 0.5463530421257019
		 22 -0.17580053210258484 23 -0.91081476211547852 24 -1.3330096006393433 25 -1.2344924211502075
		 26 -0.77217799425125122 27 -0.10944268852472305 28 0.58636641502380371 29 1.1453169584274292
		 30 1.3273299932479858;
createNode animCurveTA -n "Character1_Neck_rotateY";
	rename -uid "AAFE7902-44AB-0AB3-6239-13A26F5A8DE7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -26.627052307128906 1 -26.513193130493164
		 2 -26.124271392822266 3 -25.468542098999023 4 -24.554296493530273 5 -23.392080307006836
		 6 -21.996015548706055 7 -20.384357452392578 8 -18.579414367675781 9 -16.457542419433594
		 10 -14.027911186218262 11 -11.515995979309082 12 -9.1481838226318359 13 -7.1511054039001465
		 14 -5.751129150390625 15 -5.1740751266479492 16 -5.180239200592041 17 -5.4133787155151367
		 18 -5.9243226051330566 19 -6.7638454437255859 20 -7.9819822311401367 21 -9.6272449493408203
		 22 -11.745811462402344 23 -14.380908966064455 24 -17.101577758789063 25 -19.523723602294922
		 26 -21.691350936889648 27 -23.653312683105469 28 -25.462245941162109 29 -27.174114227294922
		 30 -26.627052307128906;
createNode animCurveTA -n "Character1_Neck_rotateZ";
	rename -uid "5A79971F-47CE-FF2B-8FD9-D0A880E2DC7E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -3.9364175796508789 1 -3.8343772888183594
		 2 -3.5305807590484619 3 -3.0574424266815186 4 -2.4499711990356445 5 -1.7417232990264893
		 6 -0.96066689491271984 7 -0.12524576485157013 8 0.75925189256668091 9 1.9174541234970093
		 10 3.4335305690765381 11 5.1041102409362793 12 6.7248506546020508 13 8.0891704559326172
		 14 8.9880084991455078 15 9.2103977203369141 16 8.75439453125 17 7.8355693817138663
		 18 6.5693812370300293 19 5.0716280937194824 20 3.4573032855987549 21 1.838919997215271
		 22 0.3241502046585083 23 -0.98730885982513439 24 -1.980415344238281 25 -2.6125779151916504
		 26 -3.0255560874938965 27 -3.3433403968811035 28 -3.6811847686767574 29 -4.1501283645629883
		 30 -3.9364175796508789;
createNode animCurveTU -n "Character1_Neck_visibility";
	rename -uid "E58D6B24-47EB-9C01-CC33-BCB3A0E4CB8E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Head_translateX";
	rename -uid "22B31C5F-4AF4-97F0-73C5-F2BEFD5510A9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.627285957336426;
createNode animCurveTL -n "Character1_Head_translateY";
	rename -uid "F26B8DFC-4C58-9741-A2B9-AFBE31574F6E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.64882755279541;
createNode animCurveTL -n "Character1_Head_translateZ";
	rename -uid "C5FDDB57-4E88-BB2A-0500-5A9A9E6C5E07";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.1324095726013184;
createNode animCurveTU -n "Character1_Head_scaleX";
	rename -uid "3D69D490-4BF2-A7A3-B03A-8AB892EFEE94";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Head_scaleY";
	rename -uid "10B472A2-4160-2615-7AAA-23A1003377CC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Head_scaleZ";
	rename -uid "6E833A15-4357-E1A1-A25B-2789E8594222";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_Head_rotateX";
	rename -uid "8235751E-4468-C025-EA46-7AAB2A2197F9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 25.630889892578125 1 24.622369766235352
		 2 23.710271835327148 3 22.83442497253418 4 21.934541702270508 5 20.950223922729492
		 6 19.820920944213867 7 18.48588752746582 8 16.884088516235352 9 14.718406677246096
		 10 11.935733795166016 11 8.8412866592407227 12 5.7413220405578613 13 2.9432332515716553
		 14 0.75531852245330811 15 -0.5136035680770874 16 -0.95925283432006847 17 -0.91975361108779907
		 18 -0.44977635145187378 19 0.39598581194877625 20 1.5627835988998413 21 2.9957787990570068
		 22 4.6400094032287598 23 6.4403491020202637 24 8.6064233779907227 25 11.387116432189941
		 26 14.55394172668457 27 17.878263473510742 28 21.131582260131836 29 24.085832595825195
		 30 25.630889892578125;
createNode animCurveTA -n "Character1_Head_rotateY";
	rename -uid "60F9FAB8-4411-9648-9F9E-A0B015B65B37";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 3.3671762943267822 1 3.0393824577331543
		 2 2.6600837707519531 3 2.2568464279174805 4 1.8568211793899534 5 1.4867415428161621
		 6 1.1728998422622681 7 0.94110316038131725 8 0.81661486625671387 9 0.84355306625366211
		 10 1.0157041549682617 11 1.2852262258529663 12 1.6086853742599487 13 1.9467898607254031
		 14 2.2631971836090088 15 2.5227506160736084 16 2.753676176071167 17 3.0022103786468506
		 18 3.2593414783477783 19 3.5155444145202637 20 3.7609574794769287 21 3.9855351448059082
		 22 4.179196834564209 23 4.3319797515869141 24 4.4333786964416504 25 4.4618353843688965
		 26 4.4229669570922852 27 4.3250823020935059 28 4.1804914474487305 29 4.00616455078125
		 30 3.3671762943267822;
createNode animCurveTA -n "Character1_Head_rotateZ";
	rename -uid "EE9329BE-4860-7D4B-CC97-B29E81AE9732";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 7.8320965766906738 1 7.7931637763977042
		 2 7.7385783195495605 3 7.6775288581848136 4 7.6194562911987296 5 7.5741229057312012
		 6 7.5516643524169931 7 7.5626235008239737 8 7.6179733276367187 9 7.7982258796691895
		 10 8.1275510787963867 11 8.530573844909668 12 8.9283123016357422 13 9.2377586364746094
		 14 9.3723878860473633 15 9.2438411712646484 16 8.7713098526000977 17 8.0027713775634766
		 18 7.0442929267883301 19 6.0028257369995117 20 4.9857616424560547 21 4.1005425453186035
		 22 3.4543278217315674 23 3.1537435054779053 24 3.2595810890197754 25 3.7804205417633057
		 26 4.5824918746948242 27 5.528559684753418 28 6.478024959564209 29 7.2876434326171884
		 30 7.8320965766906738;
createNode animCurveTU -n "Character1_Head_visibility";
	rename -uid "5F050FB0-47CF-48AE-D883-D1AE82FA91FA";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "eyes_translateX";
	rename -uid "C44971AE-404D-DE81-C6E2-E38E947C2F41";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 30.947149276733398;
createNode animCurveTL -n "eyes_translateY";
	rename -uid "2E2018CC-4A4F-CF5C-4971-E5A8FA42057D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.0992542810490704e-007;
createNode animCurveTL -n "eyes_translateZ";
	rename -uid "0D47751A-4BF6-36A4-8BEA-D3B1DEE8B8FF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.121469657048256e-013;
createNode animCurveTU -n "eyes_scaleX";
	rename -uid "CE1B10D3-436F-C845-D5C3-5087323B3F84";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleY";
	rename -uid "86AAFC71-4650-5BEA-C623-F19253D09FF3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleZ";
	rename -uid "44185F42-4E40-B8F6-1291-C6B1C7101F96";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "eyes_rotateX";
	rename -uid "ACEBF97E-45BC-13DC-B442-90A87D1D05AD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 2.7428603172302246 1 2.8199145793914795
		 2 2.9188971519470215 3 3.0233616828918457 4 3.1168622970581055 5 3.1829524040222168
		 6 3.2051854133605957 7 3.1671159267425537 8 3.0279381275177002 9 2.7892985343933105
		 10 2.4984269142150879 11 2.2025525569915771 12 1.9489059448242187 13 1.7847167253494263
		 14 1.7035021781921387 15 1.6626330614089966 16 1.6551183462142944 17 1.6739674806594849
		 18 1.7121895551681519 19 1.7627938985824585 20 1.8187898397445679 21 1.8819999694824219
		 22 1.957465648651123 23 2.0430238246917725 24 2.1365125179290771 25 2.2357680797576904
		 26 2.3386285305023193 27 2.4429304599761963 28 2.5465116500854492 29 2.6472091674804687
		 30 2.7428603172302246;
createNode animCurveTA -n "eyes_rotateY";
	rename -uid "F57D4C8D-4BFC-590E-099F-37A7D029BA7D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -2.5976543426513672 1 -2.936553955078125
		 2 -3.3185405731201172 3 -3.7112989425659184 4 -4.0825133323669434 5 -4.399869441986084
		 6 -4.6310510635375977 7 -4.7437424659729004 8 -4.669703483581543 9 -4.414555549621582
		 10 -4.0567765235900879 11 -3.6748435497283931 12 -3.3472342491149902 13 -3.1524267196655273
		 14 -3.0958199501037598 15 -3.1133723258972168 16 -3.1793975830078125 17 -3.2682108879089355
		 18 -3.3541259765625 19 -3.4114577770233154 20 -3.4145205020904541 21 -3.3725669384002686
		 22 -3.3139350414276123 23 -3.2415685653686523 24 -3.1584100723266602 25 -3.0674035549163818
		 26 -2.9714915752410889 27 -2.8736176490783691 28 -2.7767248153686523 29 -2.6837558746337891
		 30 -2.5976543426513672;
createNode animCurveTA -n "eyes_rotateZ";
	rename -uid "89B4059A-481A-4F67-4DFC-C8A82ED7764D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -6.7038145065307617 1 -6.3878231048583984
		 2 -6.0399107933044434 3 -5.6840181350708008 4 -5.344085693359375 5 -5.0440559387207031
		 6 -4.807868480682373 7 -4.6594653129577637 8 -4.6291360855102539 9 -4.7086639404296875
		 10 -4.8642263412475586 11 -5.0620036125183105 12 -5.2681732177734375 13 -5.448915958404541
		 14 -5.6241955757141113 15 -5.8262295722961426 16 -6.0395770072937012 17 -6.2487969398498535
		 18 -6.438448429107666 19 -6.5930895805358887 20 -6.6972799301147461 21 -6.7571959495544434
		 22 -6.7922368049621582 23 -6.8067917823791504 24 -6.8052501678466797 25 -6.7920026779174805
		 26 -6.7714381217956543 27 -6.7479476928710938 28 -6.7259202003479004 29 -6.7097458839416504
		 30 -6.7038145065307617;
createNode animCurveTU -n "eyes_visibility";
	rename -uid "D9997BF1-4611-A2AA-6D8E-278A5966B91B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "jaw_translateX";
	rename -uid "76B1BABC-48A0-9565-F7DB-89801BFDE42F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.07279486209154129;
createNode animCurveTL -n "jaw_translateY";
	rename -uid "C36B4AEB-40A5-343F-1EAE-AEA710276DA3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.25670051574707;
createNode animCurveTL -n "jaw_translateZ";
	rename -uid "1E024EFD-4271-8C51-7F48-8E9EE45E8DAF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.2238426557523781e-007;
createNode animCurveTU -n "jaw_scaleX";
	rename -uid "A58FE533-4305-D549-C55E-BBB15868DB0E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleY";
	rename -uid "BDA6FD8E-47C7-7953-BB28-BA9AF858D454";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleZ";
	rename -uid "13E1EEFA-4D2B-1EE0-4B6C-799EFE5652CD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "jaw_rotateX";
	rename -uid "28F847F8-433B-5AA2-3ADA-D28B02F5D8D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -53.662673950195313 1 -53.498191833496094
		 2 -53.332000732421875 3 -53.262443542480469 4 -53.355552673339844 5 -53.635398864746094
		 6 -54.004837036132813 7 -54.328781127929688 8 -54.543418884277344 9 -54.557991027832031
		 10 -54.398292541503906 11 -54.147911071777344 12 -53.912651062011719 13 -53.747695922851563
		 14 -53.589935302734375 15 -53.486923217773437 16 -53.428306579589844 17 -53.410781860351563
		 18 -53.416854858398437 19 -53.359901428222656 20 -53.189369201660156 21 -52.888633728027344
		 22 -52.47161865234375 23 -52.095069885253906 24 -51.980636596679688 25 -52.104713439941406
		 26 -52.412452697753906 27 -52.79180908203125 28 -53.137763977050781 29 -53.413619995117188
		 30 -53.662673950195313;
createNode animCurveTA -n "jaw_rotateY";
	rename -uid "A47C982B-4492-DAF6-3B60-4FB12443995D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -120.64627838134766 1 -120.58468627929689
		 2 -120.52737426757811 3 -120.50959777832033 4 -120.54820251464842 5 -120.64750671386719
		 6 -120.77838134765626 7 -120.90205383300781 8 -120.99254608154298 9 -121.00223541259764
		 10 -120.93489074707031 11 -120.83425903320312 12 -120.74890136718751 13 -120.69567871093749
		 14 -120.65065002441406 15 -120.62233734130859 16 -120.60567474365233 17 -120.5965576171875
		 18 -120.58607482910155 19 -120.54144287109376 20 -120.43946075439453 21 -120.27888488769531
		 22 -120.08050537109376 23 -119.92774200439453 24 -119.90987396240234 25 -119.99595642089842
		 26 -120.14176940917969 27 -120.29930877685548 28 -120.43490600585937 29 -120.54209899902342
		 30 -120.64627838134766;
createNode animCurveTA -n "jaw_rotateZ";
	rename -uid "35E8E1AC-4A8C-9E64-53E1-16B5C01011AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -45.0921630859375 1 -45.158950805664063
		 2 -45.238983154296875 3 -45.28387451171875 4 -45.259185791015625 5 -45.104278564453125
		 6 -44.793411254882813 7 -44.360977172851562 8 -43.915618896484375 9 -43.668228149414062
		 10 -43.714508056640625 11 -44.032623291015625 12 -44.447433471679688 13 -44.78717041015625
		 14 -45.077178955078125 15 -45.225799560546875 16 -45.2452392578125 17 -45.240737915039062
		 18 -45.303665161132813 19 -45.525054931640625 20 -45.903030395507812 21 -46.350723266601563
		 22 -46.778762817382813 23 -47.00628662109375 24 -46.893234252929688 25 -46.559249877929687
		 26 -46.130020141601563 27 -45.728729248046875 28 -45.42938232421875 29 -45.23516845703125
		 30 -45.0921630859375;
createNode animCurveTU -n "jaw_visibility";
	rename -uid "AB8DFE9F-483B-65DC-38DE-5CA431155471";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "16DED533-44BB-09BB-F911-95A619DBB65B";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "98E2F8A4-43D1-AB07-71B2-62B6206C6D07";
createNode displayLayer -n "defaultLayer";
	rename -uid "917DB1CC-40C7-876F-79CC-8F9CDA7A66C0";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "147CB244-4B49-6BFB-1421-16BCA6C248B0";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "31F7E45D-4834-1E77-6E7C-6CB1986F987B";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "FA4C13CC-4C62-08D2-7C81-0ABEBA478DA2";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1636\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1636\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1305\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1305\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 14 100 -ps 2 86 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1305\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1305\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "BF2A541F-415E-F4E3-83B3-528A0B9ECF86";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 30 -ast 0 -aet 250 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 0;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
connectAttr "ref_frame.s" "Character1_Hips.is";
connectAttr "Character1_Hips_scaleX.o" "Character1_Hips.sx";
connectAttr "Character1_Hips_scaleY.o" "Character1_Hips.sy";
connectAttr "Character1_Hips_scaleZ.o" "Character1_Hips.sz";
connectAttr "Character1_Hips_translateX.o" "Character1_Hips.tx";
connectAttr "Character1_Hips_translateY.o" "Character1_Hips.ty";
connectAttr "Character1_Hips_translateZ.o" "Character1_Hips.tz";
connectAttr "Character1_Hips_rotateX.o" "Character1_Hips.rx";
connectAttr "Character1_Hips_rotateY.o" "Character1_Hips.ry";
connectAttr "Character1_Hips_rotateZ.o" "Character1_Hips.rz";
connectAttr "Character1_Hips_visibility.o" "Character1_Hips.v";
connectAttr "Character1_Hips.s" "Character1_LeftUpLeg.is";
connectAttr "Character1_LeftUpLeg_scaleX.o" "Character1_LeftUpLeg.sx";
connectAttr "Character1_LeftUpLeg_scaleY.o" "Character1_LeftUpLeg.sy";
connectAttr "Character1_LeftUpLeg_scaleZ.o" "Character1_LeftUpLeg.sz";
connectAttr "Character1_LeftUpLeg_translateX.o" "Character1_LeftUpLeg.tx";
connectAttr "Character1_LeftUpLeg_translateY.o" "Character1_LeftUpLeg.ty";
connectAttr "Character1_LeftUpLeg_translateZ.o" "Character1_LeftUpLeg.tz";
connectAttr "Character1_LeftUpLeg_rotateX.o" "Character1_LeftUpLeg.rx";
connectAttr "Character1_LeftUpLeg_rotateY.o" "Character1_LeftUpLeg.ry";
connectAttr "Character1_LeftUpLeg_rotateZ.o" "Character1_LeftUpLeg.rz";
connectAttr "Character1_LeftUpLeg_visibility.o" "Character1_LeftUpLeg.v";
connectAttr "Character1_LeftUpLeg.s" "Character1_LeftLeg.is";
connectAttr "Character1_LeftLeg_scaleX.o" "Character1_LeftLeg.sx";
connectAttr "Character1_LeftLeg_scaleY.o" "Character1_LeftLeg.sy";
connectAttr "Character1_LeftLeg_scaleZ.o" "Character1_LeftLeg.sz";
connectAttr "Character1_LeftLeg_translateX.o" "Character1_LeftLeg.tx";
connectAttr "Character1_LeftLeg_translateY.o" "Character1_LeftLeg.ty";
connectAttr "Character1_LeftLeg_translateZ.o" "Character1_LeftLeg.tz";
connectAttr "Character1_LeftLeg_rotateX.o" "Character1_LeftLeg.rx";
connectAttr "Character1_LeftLeg_rotateY.o" "Character1_LeftLeg.ry";
connectAttr "Character1_LeftLeg_rotateZ.o" "Character1_LeftLeg.rz";
connectAttr "Character1_LeftLeg_visibility.o" "Character1_LeftLeg.v";
connectAttr "Character1_LeftLeg.s" "Character1_LeftFoot.is";
connectAttr "Character1_LeftFoot_scaleX.o" "Character1_LeftFoot.sx";
connectAttr "Character1_LeftFoot_scaleY.o" "Character1_LeftFoot.sy";
connectAttr "Character1_LeftFoot_scaleZ.o" "Character1_LeftFoot.sz";
connectAttr "Character1_LeftFoot_translateX.o" "Character1_LeftFoot.tx";
connectAttr "Character1_LeftFoot_translateY.o" "Character1_LeftFoot.ty";
connectAttr "Character1_LeftFoot_translateZ.o" "Character1_LeftFoot.tz";
connectAttr "Character1_LeftFoot_rotateX.o" "Character1_LeftFoot.rx";
connectAttr "Character1_LeftFoot_rotateY.o" "Character1_LeftFoot.ry";
connectAttr "Character1_LeftFoot_rotateZ.o" "Character1_LeftFoot.rz";
connectAttr "Character1_LeftFoot_visibility.o" "Character1_LeftFoot.v";
connectAttr "Character1_LeftFoot.s" "Character1_LeftToeBase.is";
connectAttr "Character1_LeftToeBase_scaleX.o" "Character1_LeftToeBase.sx";
connectAttr "Character1_LeftToeBase_scaleY.o" "Character1_LeftToeBase.sy";
connectAttr "Character1_LeftToeBase_scaleZ.o" "Character1_LeftToeBase.sz";
connectAttr "Character1_LeftToeBase_translateX.o" "Character1_LeftToeBase.tx";
connectAttr "Character1_LeftToeBase_translateY.o" "Character1_LeftToeBase.ty";
connectAttr "Character1_LeftToeBase_translateZ.o" "Character1_LeftToeBase.tz";
connectAttr "Character1_LeftToeBase_rotateX.o" "Character1_LeftToeBase.rx";
connectAttr "Character1_LeftToeBase_rotateY.o" "Character1_LeftToeBase.ry";
connectAttr "Character1_LeftToeBase_rotateZ.o" "Character1_LeftToeBase.rz";
connectAttr "Character1_LeftToeBase_visibility.o" "Character1_LeftToeBase.v";
connectAttr "Character1_LeftToeBase.s" "Character1_LeftFootMiddle1.is";
connectAttr "Character1_LeftFootMiddle1_scaleX.o" "Character1_LeftFootMiddle1.sx"
		;
connectAttr "Character1_LeftFootMiddle1_scaleY.o" "Character1_LeftFootMiddle1.sy"
		;
connectAttr "Character1_LeftFootMiddle1_scaleZ.o" "Character1_LeftFootMiddle1.sz"
		;
connectAttr "Character1_LeftFootMiddle1_translateX.o" "Character1_LeftFootMiddle1.tx"
		;
connectAttr "Character1_LeftFootMiddle1_translateY.o" "Character1_LeftFootMiddle1.ty"
		;
connectAttr "Character1_LeftFootMiddle1_translateZ.o" "Character1_LeftFootMiddle1.tz"
		;
connectAttr "Character1_LeftFootMiddle1_rotateX.o" "Character1_LeftFootMiddle1.rx"
		;
connectAttr "Character1_LeftFootMiddle1_rotateY.o" "Character1_LeftFootMiddle1.ry"
		;
connectAttr "Character1_LeftFootMiddle1_rotateZ.o" "Character1_LeftFootMiddle1.rz"
		;
connectAttr "Character1_LeftFootMiddle1_visibility.o" "Character1_LeftFootMiddle1.v"
		;
connectAttr "Character1_LeftFootMiddle1.s" "Character1_LeftFootMiddle2.is";
connectAttr "Character1_LeftFootMiddle2_translateX.o" "Character1_LeftFootMiddle2.tx"
		;
connectAttr "Character1_LeftFootMiddle2_translateY.o" "Character1_LeftFootMiddle2.ty"
		;
connectAttr "Character1_LeftFootMiddle2_translateZ.o" "Character1_LeftFootMiddle2.tz"
		;
connectAttr "Character1_LeftFootMiddle2_scaleX.o" "Character1_LeftFootMiddle2.sx"
		;
connectAttr "Character1_LeftFootMiddle2_scaleY.o" "Character1_LeftFootMiddle2.sy"
		;
connectAttr "Character1_LeftFootMiddle2_scaleZ.o" "Character1_LeftFootMiddle2.sz"
		;
connectAttr "Character1_LeftFootMiddle2_rotateX.o" "Character1_LeftFootMiddle2.rx"
		;
connectAttr "Character1_LeftFootMiddle2_rotateY.o" "Character1_LeftFootMiddle2.ry"
		;
connectAttr "Character1_LeftFootMiddle2_rotateZ.o" "Character1_LeftFootMiddle2.rz"
		;
connectAttr "Character1_LeftFootMiddle2_visibility.o" "Character1_LeftFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_RightUpLeg.is";
connectAttr "Character1_RightUpLeg_scaleX.o" "Character1_RightUpLeg.sx";
connectAttr "Character1_RightUpLeg_scaleY.o" "Character1_RightUpLeg.sy";
connectAttr "Character1_RightUpLeg_scaleZ.o" "Character1_RightUpLeg.sz";
connectAttr "Character1_RightUpLeg_translateX.o" "Character1_RightUpLeg.tx";
connectAttr "Character1_RightUpLeg_translateY.o" "Character1_RightUpLeg.ty";
connectAttr "Character1_RightUpLeg_translateZ.o" "Character1_RightUpLeg.tz";
connectAttr "Character1_RightUpLeg_rotateX.o" "Character1_RightUpLeg.rx";
connectAttr "Character1_RightUpLeg_rotateY.o" "Character1_RightUpLeg.ry";
connectAttr "Character1_RightUpLeg_rotateZ.o" "Character1_RightUpLeg.rz";
connectAttr "Character1_RightUpLeg_visibility.o" "Character1_RightUpLeg.v";
connectAttr "Character1_RightUpLeg.s" "Character1_RightLeg.is";
connectAttr "Character1_RightLeg_scaleX.o" "Character1_RightLeg.sx";
connectAttr "Character1_RightLeg_scaleY.o" "Character1_RightLeg.sy";
connectAttr "Character1_RightLeg_scaleZ.o" "Character1_RightLeg.sz";
connectAttr "Character1_RightLeg_translateX.o" "Character1_RightLeg.tx";
connectAttr "Character1_RightLeg_translateY.o" "Character1_RightLeg.ty";
connectAttr "Character1_RightLeg_translateZ.o" "Character1_RightLeg.tz";
connectAttr "Character1_RightLeg_rotateX.o" "Character1_RightLeg.rx";
connectAttr "Character1_RightLeg_rotateY.o" "Character1_RightLeg.ry";
connectAttr "Character1_RightLeg_rotateZ.o" "Character1_RightLeg.rz";
connectAttr "Character1_RightLeg_visibility.o" "Character1_RightLeg.v";
connectAttr "Character1_RightLeg.s" "Character1_RightFoot.is";
connectAttr "Character1_RightFoot_scaleX.o" "Character1_RightFoot.sx";
connectAttr "Character1_RightFoot_scaleY.o" "Character1_RightFoot.sy";
connectAttr "Character1_RightFoot_scaleZ.o" "Character1_RightFoot.sz";
connectAttr "Character1_RightFoot_translateX.o" "Character1_RightFoot.tx";
connectAttr "Character1_RightFoot_translateY.o" "Character1_RightFoot.ty";
connectAttr "Character1_RightFoot_translateZ.o" "Character1_RightFoot.tz";
connectAttr "Character1_RightFoot_rotateX.o" "Character1_RightFoot.rx";
connectAttr "Character1_RightFoot_rotateY.o" "Character1_RightFoot.ry";
connectAttr "Character1_RightFoot_rotateZ.o" "Character1_RightFoot.rz";
connectAttr "Character1_RightFoot_visibility.o" "Character1_RightFoot.v";
connectAttr "Character1_RightFoot.s" "Character1_RightToeBase.is";
connectAttr "Character1_RightToeBase_scaleX.o" "Character1_RightToeBase.sx";
connectAttr "Character1_RightToeBase_scaleY.o" "Character1_RightToeBase.sy";
connectAttr "Character1_RightToeBase_scaleZ.o" "Character1_RightToeBase.sz";
connectAttr "Character1_RightToeBase_translateX.o" "Character1_RightToeBase.tx";
connectAttr "Character1_RightToeBase_translateY.o" "Character1_RightToeBase.ty";
connectAttr "Character1_RightToeBase_translateZ.o" "Character1_RightToeBase.tz";
connectAttr "Character1_RightToeBase_rotateX.o" "Character1_RightToeBase.rx";
connectAttr "Character1_RightToeBase_rotateY.o" "Character1_RightToeBase.ry";
connectAttr "Character1_RightToeBase_rotateZ.o" "Character1_RightToeBase.rz";
connectAttr "Character1_RightToeBase_visibility.o" "Character1_RightToeBase.v";
connectAttr "Character1_RightToeBase.s" "Character1_RightFootMiddle1.is";
connectAttr "Character1_RightFootMiddle1_scaleX.o" "Character1_RightFootMiddle1.sx"
		;
connectAttr "Character1_RightFootMiddle1_scaleY.o" "Character1_RightFootMiddle1.sy"
		;
connectAttr "Character1_RightFootMiddle1_scaleZ.o" "Character1_RightFootMiddle1.sz"
		;
connectAttr "Character1_RightFootMiddle1_translateX.o" "Character1_RightFootMiddle1.tx"
		;
connectAttr "Character1_RightFootMiddle1_translateY.o" "Character1_RightFootMiddle1.ty"
		;
connectAttr "Character1_RightFootMiddle1_translateZ.o" "Character1_RightFootMiddle1.tz"
		;
connectAttr "Character1_RightFootMiddle1_rotateX.o" "Character1_RightFootMiddle1.rx"
		;
connectAttr "Character1_RightFootMiddle1_rotateY.o" "Character1_RightFootMiddle1.ry"
		;
connectAttr "Character1_RightFootMiddle1_rotateZ.o" "Character1_RightFootMiddle1.rz"
		;
connectAttr "Character1_RightFootMiddle1_visibility.o" "Character1_RightFootMiddle1.v"
		;
connectAttr "Character1_RightFootMiddle1.s" "Character1_RightFootMiddle2.is";
connectAttr "Character1_RightFootMiddle2_translateX.o" "Character1_RightFootMiddle2.tx"
		;
connectAttr "Character1_RightFootMiddle2_translateY.o" "Character1_RightFootMiddle2.ty"
		;
connectAttr "Character1_RightFootMiddle2_translateZ.o" "Character1_RightFootMiddle2.tz"
		;
connectAttr "Character1_RightFootMiddle2_scaleX.o" "Character1_RightFootMiddle2.sx"
		;
connectAttr "Character1_RightFootMiddle2_scaleY.o" "Character1_RightFootMiddle2.sy"
		;
connectAttr "Character1_RightFootMiddle2_scaleZ.o" "Character1_RightFootMiddle2.sz"
		;
connectAttr "Character1_RightFootMiddle2_rotateX.o" "Character1_RightFootMiddle2.rx"
		;
connectAttr "Character1_RightFootMiddle2_rotateY.o" "Character1_RightFootMiddle2.ry"
		;
connectAttr "Character1_RightFootMiddle2_rotateZ.o" "Character1_RightFootMiddle2.rz"
		;
connectAttr "Character1_RightFootMiddle2_visibility.o" "Character1_RightFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_Spine.is";
connectAttr "Character1_Spine_scaleX.o" "Character1_Spine.sx";
connectAttr "Character1_Spine_scaleY.o" "Character1_Spine.sy";
connectAttr "Character1_Spine_scaleZ.o" "Character1_Spine.sz";
connectAttr "Character1_Spine_translateX.o" "Character1_Spine.tx";
connectAttr "Character1_Spine_translateY.o" "Character1_Spine.ty";
connectAttr "Character1_Spine_translateZ.o" "Character1_Spine.tz";
connectAttr "Character1_Spine_rotateX.o" "Character1_Spine.rx";
connectAttr "Character1_Spine_rotateY.o" "Character1_Spine.ry";
connectAttr "Character1_Spine_rotateZ.o" "Character1_Spine.rz";
connectAttr "Character1_Spine_visibility.o" "Character1_Spine.v";
connectAttr "Character1_Spine.s" "Character1_Spine1.is";
connectAttr "Character1_Spine1_scaleX.o" "Character1_Spine1.sx";
connectAttr "Character1_Spine1_scaleY.o" "Character1_Spine1.sy";
connectAttr "Character1_Spine1_scaleZ.o" "Character1_Spine1.sz";
connectAttr "Character1_Spine1_translateX.o" "Character1_Spine1.tx";
connectAttr "Character1_Spine1_translateY.o" "Character1_Spine1.ty";
connectAttr "Character1_Spine1_translateZ.o" "Character1_Spine1.tz";
connectAttr "Character1_Spine1_rotateX.o" "Character1_Spine1.rx";
connectAttr "Character1_Spine1_rotateY.o" "Character1_Spine1.ry";
connectAttr "Character1_Spine1_rotateZ.o" "Character1_Spine1.rz";
connectAttr "Character1_Spine1_visibility.o" "Character1_Spine1.v";
connectAttr "Character1_Spine1.s" "Character1_LeftShoulder.is";
connectAttr "Character1_LeftShoulder_scaleX.o" "Character1_LeftShoulder.sx";
connectAttr "Character1_LeftShoulder_scaleY.o" "Character1_LeftShoulder.sy";
connectAttr "Character1_LeftShoulder_scaleZ.o" "Character1_LeftShoulder.sz";
connectAttr "Character1_LeftShoulder_translateX.o" "Character1_LeftShoulder.tx";
connectAttr "Character1_LeftShoulder_translateY.o" "Character1_LeftShoulder.ty";
connectAttr "Character1_LeftShoulder_translateZ.o" "Character1_LeftShoulder.tz";
connectAttr "Character1_LeftShoulder_rotateX.o" "Character1_LeftShoulder.rx";
connectAttr "Character1_LeftShoulder_rotateY.o" "Character1_LeftShoulder.ry";
connectAttr "Character1_LeftShoulder_rotateZ.o" "Character1_LeftShoulder.rz";
connectAttr "Character1_LeftShoulder_visibility.o" "Character1_LeftShoulder.v";
connectAttr "Character1_LeftShoulder.s" "Character1_LeftArm.is";
connectAttr "Character1_LeftArm_scaleX.o" "Character1_LeftArm.sx";
connectAttr "Character1_LeftArm_scaleY.o" "Character1_LeftArm.sy";
connectAttr "Character1_LeftArm_scaleZ.o" "Character1_LeftArm.sz";
connectAttr "Character1_LeftArm_translateX.o" "Character1_LeftArm.tx";
connectAttr "Character1_LeftArm_translateY.o" "Character1_LeftArm.ty";
connectAttr "Character1_LeftArm_translateZ.o" "Character1_LeftArm.tz";
connectAttr "Character1_LeftArm_rotateX.o" "Character1_LeftArm.rx";
connectAttr "Character1_LeftArm_rotateY.o" "Character1_LeftArm.ry";
connectAttr "Character1_LeftArm_rotateZ.o" "Character1_LeftArm.rz";
connectAttr "Character1_LeftArm_visibility.o" "Character1_LeftArm.v";
connectAttr "Character1_LeftArm.s" "Character1_LeftForeArm.is";
connectAttr "Character1_LeftForeArm_scaleX.o" "Character1_LeftForeArm.sx";
connectAttr "Character1_LeftForeArm_scaleY.o" "Character1_LeftForeArm.sy";
connectAttr "Character1_LeftForeArm_scaleZ.o" "Character1_LeftForeArm.sz";
connectAttr "Character1_LeftForeArm_translateX.o" "Character1_LeftForeArm.tx";
connectAttr "Character1_LeftForeArm_translateY.o" "Character1_LeftForeArm.ty";
connectAttr "Character1_LeftForeArm_translateZ.o" "Character1_LeftForeArm.tz";
connectAttr "Character1_LeftForeArm_rotateX.o" "Character1_LeftForeArm.rx";
connectAttr "Character1_LeftForeArm_rotateY.o" "Character1_LeftForeArm.ry";
connectAttr "Character1_LeftForeArm_rotateZ.o" "Character1_LeftForeArm.rz";
connectAttr "Character1_LeftForeArm_visibility.o" "Character1_LeftForeArm.v";
connectAttr "Character1_LeftForeArm.s" "Character1_LeftHand.is";
connectAttr "Character1_LeftHand_lockInfluenceWeights.o" "Character1_LeftHand.liw"
		;
connectAttr "Character1_LeftHand_scaleX.o" "Character1_LeftHand.sx";
connectAttr "Character1_LeftHand_scaleY.o" "Character1_LeftHand.sy";
connectAttr "Character1_LeftHand_scaleZ.o" "Character1_LeftHand.sz";
connectAttr "Character1_LeftHand_translateX.o" "Character1_LeftHand.tx";
connectAttr "Character1_LeftHand_translateY.o" "Character1_LeftHand.ty";
connectAttr "Character1_LeftHand_translateZ.o" "Character1_LeftHand.tz";
connectAttr "Character1_LeftHand_rotateX.o" "Character1_LeftHand.rx";
connectAttr "Character1_LeftHand_rotateY.o" "Character1_LeftHand.ry";
connectAttr "Character1_LeftHand_rotateZ.o" "Character1_LeftHand.rz";
connectAttr "Character1_LeftHand_visibility.o" "Character1_LeftHand.v";
connectAttr "Character1_LeftHand.s" "Character1_LeftHandThumb1.is";
connectAttr "Character1_LeftHandThumb1_scaleX.o" "Character1_LeftHandThumb1.sx";
connectAttr "Character1_LeftHandThumb1_scaleY.o" "Character1_LeftHandThumb1.sy";
connectAttr "Character1_LeftHandThumb1_scaleZ.o" "Character1_LeftHandThumb1.sz";
connectAttr "Character1_LeftHandThumb1_translateX.o" "Character1_LeftHandThumb1.tx"
		;
connectAttr "Character1_LeftHandThumb1_translateY.o" "Character1_LeftHandThumb1.ty"
		;
connectAttr "Character1_LeftHandThumb1_translateZ.o" "Character1_LeftHandThumb1.tz"
		;
connectAttr "Character1_LeftHandThumb1_rotateX.o" "Character1_LeftHandThumb1.rx"
		;
connectAttr "Character1_LeftHandThumb1_rotateY.o" "Character1_LeftHandThumb1.ry"
		;
connectAttr "Character1_LeftHandThumb1_rotateZ.o" "Character1_LeftHandThumb1.rz"
		;
connectAttr "Character1_LeftHandThumb1_visibility.o" "Character1_LeftHandThumb1.v"
		;
connectAttr "Character1_LeftHandThumb1.s" "Character1_LeftHandThumb2.is";
connectAttr "Character1_LeftHandThumb2_scaleX.o" "Character1_LeftHandThumb2.sx";
connectAttr "Character1_LeftHandThumb2_scaleY.o" "Character1_LeftHandThumb2.sy";
connectAttr "Character1_LeftHandThumb2_scaleZ.o" "Character1_LeftHandThumb2.sz";
connectAttr "Character1_LeftHandThumb2_translateX.o" "Character1_LeftHandThumb2.tx"
		;
connectAttr "Character1_LeftHandThumb2_translateY.o" "Character1_LeftHandThumb2.ty"
		;
connectAttr "Character1_LeftHandThumb2_translateZ.o" "Character1_LeftHandThumb2.tz"
		;
connectAttr "Character1_LeftHandThumb2_rotateX.o" "Character1_LeftHandThumb2.rx"
		;
connectAttr "Character1_LeftHandThumb2_rotateY.o" "Character1_LeftHandThumb2.ry"
		;
connectAttr "Character1_LeftHandThumb2_rotateZ.o" "Character1_LeftHandThumb2.rz"
		;
connectAttr "Character1_LeftHandThumb2_visibility.o" "Character1_LeftHandThumb2.v"
		;
connectAttr "Character1_LeftHandThumb2.s" "Character1_LeftHandThumb3.is";
connectAttr "Character1_LeftHandThumb3_translateX.o" "Character1_LeftHandThumb3.tx"
		;
connectAttr "Character1_LeftHandThumb3_translateY.o" "Character1_LeftHandThumb3.ty"
		;
connectAttr "Character1_LeftHandThumb3_translateZ.o" "Character1_LeftHandThumb3.tz"
		;
connectAttr "Character1_LeftHandThumb3_scaleX.o" "Character1_LeftHandThumb3.sx";
connectAttr "Character1_LeftHandThumb3_scaleY.o" "Character1_LeftHandThumb3.sy";
connectAttr "Character1_LeftHandThumb3_scaleZ.o" "Character1_LeftHandThumb3.sz";
connectAttr "Character1_LeftHandThumb3_rotateX.o" "Character1_LeftHandThumb3.rx"
		;
connectAttr "Character1_LeftHandThumb3_rotateY.o" "Character1_LeftHandThumb3.ry"
		;
connectAttr "Character1_LeftHandThumb3_rotateZ.o" "Character1_LeftHandThumb3.rz"
		;
connectAttr "Character1_LeftHandThumb3_visibility.o" "Character1_LeftHandThumb3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandIndex1.is";
connectAttr "Character1_LeftHandIndex1_scaleX.o" "Character1_LeftHandIndex1.sx";
connectAttr "Character1_LeftHandIndex1_scaleY.o" "Character1_LeftHandIndex1.sy";
connectAttr "Character1_LeftHandIndex1_scaleZ.o" "Character1_LeftHandIndex1.sz";
connectAttr "Character1_LeftHandIndex1_translateX.o" "Character1_LeftHandIndex1.tx"
		;
connectAttr "Character1_LeftHandIndex1_translateY.o" "Character1_LeftHandIndex1.ty"
		;
connectAttr "Character1_LeftHandIndex1_translateZ.o" "Character1_LeftHandIndex1.tz"
		;
connectAttr "Character1_LeftHandIndex1_rotateX.o" "Character1_LeftHandIndex1.rx"
		;
connectAttr "Character1_LeftHandIndex1_rotateY.o" "Character1_LeftHandIndex1.ry"
		;
connectAttr "Character1_LeftHandIndex1_rotateZ.o" "Character1_LeftHandIndex1.rz"
		;
connectAttr "Character1_LeftHandIndex1_visibility.o" "Character1_LeftHandIndex1.v"
		;
connectAttr "Character1_LeftHandIndex1.s" "Character1_LeftHandIndex2.is";
connectAttr "Character1_LeftHandIndex2_scaleX.o" "Character1_LeftHandIndex2.sx";
connectAttr "Character1_LeftHandIndex2_scaleY.o" "Character1_LeftHandIndex2.sy";
connectAttr "Character1_LeftHandIndex2_scaleZ.o" "Character1_LeftHandIndex2.sz";
connectAttr "Character1_LeftHandIndex2_translateX.o" "Character1_LeftHandIndex2.tx"
		;
connectAttr "Character1_LeftHandIndex2_translateY.o" "Character1_LeftHandIndex2.ty"
		;
connectAttr "Character1_LeftHandIndex2_translateZ.o" "Character1_LeftHandIndex2.tz"
		;
connectAttr "Character1_LeftHandIndex2_rotateX.o" "Character1_LeftHandIndex2.rx"
		;
connectAttr "Character1_LeftHandIndex2_rotateY.o" "Character1_LeftHandIndex2.ry"
		;
connectAttr "Character1_LeftHandIndex2_rotateZ.o" "Character1_LeftHandIndex2.rz"
		;
connectAttr "Character1_LeftHandIndex2_visibility.o" "Character1_LeftHandIndex2.v"
		;
connectAttr "Character1_LeftHandIndex2.s" "Character1_LeftHandIndex3.is";
connectAttr "Character1_LeftHandIndex3_translateX.o" "Character1_LeftHandIndex3.tx"
		;
connectAttr "Character1_LeftHandIndex3_translateY.o" "Character1_LeftHandIndex3.ty"
		;
connectAttr "Character1_LeftHandIndex3_translateZ.o" "Character1_LeftHandIndex3.tz"
		;
connectAttr "Character1_LeftHandIndex3_scaleX.o" "Character1_LeftHandIndex3.sx";
connectAttr "Character1_LeftHandIndex3_scaleY.o" "Character1_LeftHandIndex3.sy";
connectAttr "Character1_LeftHandIndex3_scaleZ.o" "Character1_LeftHandIndex3.sz";
connectAttr "Character1_LeftHandIndex3_rotateX.o" "Character1_LeftHandIndex3.rx"
		;
connectAttr "Character1_LeftHandIndex3_rotateY.o" "Character1_LeftHandIndex3.ry"
		;
connectAttr "Character1_LeftHandIndex3_rotateZ.o" "Character1_LeftHandIndex3.rz"
		;
connectAttr "Character1_LeftHandIndex3_visibility.o" "Character1_LeftHandIndex3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandRing1.is";
connectAttr "Character1_LeftHandRing1_scaleX.o" "Character1_LeftHandRing1.sx";
connectAttr "Character1_LeftHandRing1_scaleY.o" "Character1_LeftHandRing1.sy";
connectAttr "Character1_LeftHandRing1_scaleZ.o" "Character1_LeftHandRing1.sz";
connectAttr "Character1_LeftHandRing1_translateX.o" "Character1_LeftHandRing1.tx"
		;
connectAttr "Character1_LeftHandRing1_translateY.o" "Character1_LeftHandRing1.ty"
		;
connectAttr "Character1_LeftHandRing1_translateZ.o" "Character1_LeftHandRing1.tz"
		;
connectAttr "Character1_LeftHandRing1_rotateX.o" "Character1_LeftHandRing1.rx";
connectAttr "Character1_LeftHandRing1_rotateY.o" "Character1_LeftHandRing1.ry";
connectAttr "Character1_LeftHandRing1_rotateZ.o" "Character1_LeftHandRing1.rz";
connectAttr "Character1_LeftHandRing1_visibility.o" "Character1_LeftHandRing1.v"
		;
connectAttr "Character1_LeftHandRing1.s" "Character1_LeftHandRing2.is";
connectAttr "Character1_LeftHandRing2_scaleX.o" "Character1_LeftHandRing2.sx";
connectAttr "Character1_LeftHandRing2_scaleY.o" "Character1_LeftHandRing2.sy";
connectAttr "Character1_LeftHandRing2_scaleZ.o" "Character1_LeftHandRing2.sz";
connectAttr "Character1_LeftHandRing2_translateX.o" "Character1_LeftHandRing2.tx"
		;
connectAttr "Character1_LeftHandRing2_translateY.o" "Character1_LeftHandRing2.ty"
		;
connectAttr "Character1_LeftHandRing2_translateZ.o" "Character1_LeftHandRing2.tz"
		;
connectAttr "Character1_LeftHandRing2_rotateX.o" "Character1_LeftHandRing2.rx";
connectAttr "Character1_LeftHandRing2_rotateY.o" "Character1_LeftHandRing2.ry";
connectAttr "Character1_LeftHandRing2_rotateZ.o" "Character1_LeftHandRing2.rz";
connectAttr "Character1_LeftHandRing2_visibility.o" "Character1_LeftHandRing2.v"
		;
connectAttr "Character1_LeftHandRing2.s" "Character1_LeftHandRing3.is";
connectAttr "Character1_LeftHandRing3_translateX.o" "Character1_LeftHandRing3.tx"
		;
connectAttr "Character1_LeftHandRing3_translateY.o" "Character1_LeftHandRing3.ty"
		;
connectAttr "Character1_LeftHandRing3_translateZ.o" "Character1_LeftHandRing3.tz"
		;
connectAttr "Character1_LeftHandRing3_scaleX.o" "Character1_LeftHandRing3.sx";
connectAttr "Character1_LeftHandRing3_scaleY.o" "Character1_LeftHandRing3.sy";
connectAttr "Character1_LeftHandRing3_scaleZ.o" "Character1_LeftHandRing3.sz";
connectAttr "Character1_LeftHandRing3_rotateX.o" "Character1_LeftHandRing3.rx";
connectAttr "Character1_LeftHandRing3_rotateY.o" "Character1_LeftHandRing3.ry";
connectAttr "Character1_LeftHandRing3_rotateZ.o" "Character1_LeftHandRing3.rz";
connectAttr "Character1_LeftHandRing3_visibility.o" "Character1_LeftHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_RightShoulder.is";
connectAttr "Character1_RightShoulder_scaleX.o" "Character1_RightShoulder.sx";
connectAttr "Character1_RightShoulder_scaleY.o" "Character1_RightShoulder.sy";
connectAttr "Character1_RightShoulder_scaleZ.o" "Character1_RightShoulder.sz";
connectAttr "Character1_RightShoulder_translateX.o" "Character1_RightShoulder.tx"
		;
connectAttr "Character1_RightShoulder_translateY.o" "Character1_RightShoulder.ty"
		;
connectAttr "Character1_RightShoulder_translateZ.o" "Character1_RightShoulder.tz"
		;
connectAttr "Character1_RightShoulder_rotateX.o" "Character1_RightShoulder.rx";
connectAttr "Character1_RightShoulder_rotateY.o" "Character1_RightShoulder.ry";
connectAttr "Character1_RightShoulder_rotateZ.o" "Character1_RightShoulder.rz";
connectAttr "Character1_RightShoulder_visibility.o" "Character1_RightShoulder.v"
		;
connectAttr "Character1_RightShoulder.s" "Character1_RightArm.is";
connectAttr "Character1_RightArm_scaleX.o" "Character1_RightArm.sx";
connectAttr "Character1_RightArm_scaleY.o" "Character1_RightArm.sy";
connectAttr "Character1_RightArm_scaleZ.o" "Character1_RightArm.sz";
connectAttr "Character1_RightArm_translateX.o" "Character1_RightArm.tx";
connectAttr "Character1_RightArm_translateY.o" "Character1_RightArm.ty";
connectAttr "Character1_RightArm_translateZ.o" "Character1_RightArm.tz";
connectAttr "Character1_RightArm_rotateX.o" "Character1_RightArm.rx";
connectAttr "Character1_RightArm_rotateY.o" "Character1_RightArm.ry";
connectAttr "Character1_RightArm_rotateZ.o" "Character1_RightArm.rz";
connectAttr "Character1_RightArm_visibility.o" "Character1_RightArm.v";
connectAttr "Character1_RightArm.s" "Character1_RightForeArm.is";
connectAttr "Character1_RightForeArm_scaleX.o" "Character1_RightForeArm.sx";
connectAttr "Character1_RightForeArm_scaleY.o" "Character1_RightForeArm.sy";
connectAttr "Character1_RightForeArm_scaleZ.o" "Character1_RightForeArm.sz";
connectAttr "Character1_RightForeArm_translateX.o" "Character1_RightForeArm.tx";
connectAttr "Character1_RightForeArm_translateY.o" "Character1_RightForeArm.ty";
connectAttr "Character1_RightForeArm_translateZ.o" "Character1_RightForeArm.tz";
connectAttr "Character1_RightForeArm_rotateX.o" "Character1_RightForeArm.rx";
connectAttr "Character1_RightForeArm_rotateY.o" "Character1_RightForeArm.ry";
connectAttr "Character1_RightForeArm_rotateZ.o" "Character1_RightForeArm.rz";
connectAttr "Character1_RightForeArm_visibility.o" "Character1_RightForeArm.v";
connectAttr "Character1_RightForeArm.s" "Character1_RightHand.is";
connectAttr "Character1_RightHand_scaleX.o" "Character1_RightHand.sx";
connectAttr "Character1_RightHand_scaleY.o" "Character1_RightHand.sy";
connectAttr "Character1_RightHand_scaleZ.o" "Character1_RightHand.sz";
connectAttr "Character1_RightHand_translateX.o" "Character1_RightHand.tx";
connectAttr "Character1_RightHand_translateY.o" "Character1_RightHand.ty";
connectAttr "Character1_RightHand_translateZ.o" "Character1_RightHand.tz";
connectAttr "Character1_RightHand_rotateX.o" "Character1_RightHand.rx";
connectAttr "Character1_RightHand_rotateY.o" "Character1_RightHand.ry";
connectAttr "Character1_RightHand_rotateZ.o" "Character1_RightHand.rz";
connectAttr "Character1_RightHand_visibility.o" "Character1_RightHand.v";
connectAttr "Character1_RightHand.s" "Character1_RightHandThumb1.is";
connectAttr "Character1_RightHandThumb1_scaleX.o" "Character1_RightHandThumb1.sx"
		;
connectAttr "Character1_RightHandThumb1_scaleY.o" "Character1_RightHandThumb1.sy"
		;
connectAttr "Character1_RightHandThumb1_scaleZ.o" "Character1_RightHandThumb1.sz"
		;
connectAttr "Character1_RightHandThumb1_translateX.o" "Character1_RightHandThumb1.tx"
		;
connectAttr "Character1_RightHandThumb1_translateY.o" "Character1_RightHandThumb1.ty"
		;
connectAttr "Character1_RightHandThumb1_translateZ.o" "Character1_RightHandThumb1.tz"
		;
connectAttr "Character1_RightHandThumb1_rotateX.o" "Character1_RightHandThumb1.rx"
		;
connectAttr "Character1_RightHandThumb1_rotateY.o" "Character1_RightHandThumb1.ry"
		;
connectAttr "Character1_RightHandThumb1_rotateZ.o" "Character1_RightHandThumb1.rz"
		;
connectAttr "Character1_RightHandThumb1_visibility.o" "Character1_RightHandThumb1.v"
		;
connectAttr "Character1_RightHandThumb1.s" "Character1_RightHandThumb2.is";
connectAttr "Character1_RightHandThumb2_scaleX.o" "Character1_RightHandThumb2.sx"
		;
connectAttr "Character1_RightHandThumb2_scaleY.o" "Character1_RightHandThumb2.sy"
		;
connectAttr "Character1_RightHandThumb2_scaleZ.o" "Character1_RightHandThumb2.sz"
		;
connectAttr "Character1_RightHandThumb2_translateX.o" "Character1_RightHandThumb2.tx"
		;
connectAttr "Character1_RightHandThumb2_translateY.o" "Character1_RightHandThumb2.ty"
		;
connectAttr "Character1_RightHandThumb2_translateZ.o" "Character1_RightHandThumb2.tz"
		;
connectAttr "Character1_RightHandThumb2_rotateX.o" "Character1_RightHandThumb2.rx"
		;
connectAttr "Character1_RightHandThumb2_rotateY.o" "Character1_RightHandThumb2.ry"
		;
connectAttr "Character1_RightHandThumb2_rotateZ.o" "Character1_RightHandThumb2.rz"
		;
connectAttr "Character1_RightHandThumb2_visibility.o" "Character1_RightHandThumb2.v"
		;
connectAttr "Character1_RightHandThumb2.s" "Character1_RightHandThumb3.is";
connectAttr "Character1_RightHandThumb3_translateX.o" "Character1_RightHandThumb3.tx"
		;
connectAttr "Character1_RightHandThumb3_translateY.o" "Character1_RightHandThumb3.ty"
		;
connectAttr "Character1_RightHandThumb3_translateZ.o" "Character1_RightHandThumb3.tz"
		;
connectAttr "Character1_RightHandThumb3_scaleX.o" "Character1_RightHandThumb3.sx"
		;
connectAttr "Character1_RightHandThumb3_scaleY.o" "Character1_RightHandThumb3.sy"
		;
connectAttr "Character1_RightHandThumb3_scaleZ.o" "Character1_RightHandThumb3.sz"
		;
connectAttr "Character1_RightHandThumb3_rotateX.o" "Character1_RightHandThumb3.rx"
		;
connectAttr "Character1_RightHandThumb3_rotateY.o" "Character1_RightHandThumb3.ry"
		;
connectAttr "Character1_RightHandThumb3_rotateZ.o" "Character1_RightHandThumb3.rz"
		;
connectAttr "Character1_RightHandThumb3_visibility.o" "Character1_RightHandThumb3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandIndex1.is";
connectAttr "Character1_RightHandIndex1_scaleX.o" "Character1_RightHandIndex1.sx"
		;
connectAttr "Character1_RightHandIndex1_scaleY.o" "Character1_RightHandIndex1.sy"
		;
connectAttr "Character1_RightHandIndex1_scaleZ.o" "Character1_RightHandIndex1.sz"
		;
connectAttr "Character1_RightHandIndex1_translateX.o" "Character1_RightHandIndex1.tx"
		;
connectAttr "Character1_RightHandIndex1_translateY.o" "Character1_RightHandIndex1.ty"
		;
connectAttr "Character1_RightHandIndex1_translateZ.o" "Character1_RightHandIndex1.tz"
		;
connectAttr "Character1_RightHandIndex1_rotateX.o" "Character1_RightHandIndex1.rx"
		;
connectAttr "Character1_RightHandIndex1_rotateY.o" "Character1_RightHandIndex1.ry"
		;
connectAttr "Character1_RightHandIndex1_rotateZ.o" "Character1_RightHandIndex1.rz"
		;
connectAttr "Character1_RightHandIndex1_visibility.o" "Character1_RightHandIndex1.v"
		;
connectAttr "Character1_RightHandIndex1.s" "Character1_RightHandIndex2.is";
connectAttr "Character1_RightHandIndex2_scaleX.o" "Character1_RightHandIndex2.sx"
		;
connectAttr "Character1_RightHandIndex2_scaleY.o" "Character1_RightHandIndex2.sy"
		;
connectAttr "Character1_RightHandIndex2_scaleZ.o" "Character1_RightHandIndex2.sz"
		;
connectAttr "Character1_RightHandIndex2_translateX.o" "Character1_RightHandIndex2.tx"
		;
connectAttr "Character1_RightHandIndex2_translateY.o" "Character1_RightHandIndex2.ty"
		;
connectAttr "Character1_RightHandIndex2_translateZ.o" "Character1_RightHandIndex2.tz"
		;
connectAttr "Character1_RightHandIndex2_rotateX.o" "Character1_RightHandIndex2.rx"
		;
connectAttr "Character1_RightHandIndex2_rotateY.o" "Character1_RightHandIndex2.ry"
		;
connectAttr "Character1_RightHandIndex2_rotateZ.o" "Character1_RightHandIndex2.rz"
		;
connectAttr "Character1_RightHandIndex2_visibility.o" "Character1_RightHandIndex2.v"
		;
connectAttr "Character1_RightHandIndex2.s" "Character1_RightHandIndex3.is";
connectAttr "Character1_RightHandIndex3_translateX.o" "Character1_RightHandIndex3.tx"
		;
connectAttr "Character1_RightHandIndex3_translateY.o" "Character1_RightHandIndex3.ty"
		;
connectAttr "Character1_RightHandIndex3_translateZ.o" "Character1_RightHandIndex3.tz"
		;
connectAttr "Character1_RightHandIndex3_scaleX.o" "Character1_RightHandIndex3.sx"
		;
connectAttr "Character1_RightHandIndex3_scaleY.o" "Character1_RightHandIndex3.sy"
		;
connectAttr "Character1_RightHandIndex3_scaleZ.o" "Character1_RightHandIndex3.sz"
		;
connectAttr "Character1_RightHandIndex3_rotateX.o" "Character1_RightHandIndex3.rx"
		;
connectAttr "Character1_RightHandIndex3_rotateY.o" "Character1_RightHandIndex3.ry"
		;
connectAttr "Character1_RightHandIndex3_rotateZ.o" "Character1_RightHandIndex3.rz"
		;
connectAttr "Character1_RightHandIndex3_visibility.o" "Character1_RightHandIndex3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandRing1.is";
connectAttr "Character1_RightHandRing1_scaleX.o" "Character1_RightHandRing1.sx";
connectAttr "Character1_RightHandRing1_scaleY.o" "Character1_RightHandRing1.sy";
connectAttr "Character1_RightHandRing1_scaleZ.o" "Character1_RightHandRing1.sz";
connectAttr "Character1_RightHandRing1_translateX.o" "Character1_RightHandRing1.tx"
		;
connectAttr "Character1_RightHandRing1_translateY.o" "Character1_RightHandRing1.ty"
		;
connectAttr "Character1_RightHandRing1_translateZ.o" "Character1_RightHandRing1.tz"
		;
connectAttr "Character1_RightHandRing1_rotateX.o" "Character1_RightHandRing1.rx"
		;
connectAttr "Character1_RightHandRing1_rotateY.o" "Character1_RightHandRing1.ry"
		;
connectAttr "Character1_RightHandRing1_rotateZ.o" "Character1_RightHandRing1.rz"
		;
connectAttr "Character1_RightHandRing1_visibility.o" "Character1_RightHandRing1.v"
		;
connectAttr "Character1_RightHandRing1.s" "Character1_RightHandRing2.is";
connectAttr "Character1_RightHandRing2_scaleX.o" "Character1_RightHandRing2.sx";
connectAttr "Character1_RightHandRing2_scaleY.o" "Character1_RightHandRing2.sy";
connectAttr "Character1_RightHandRing2_scaleZ.o" "Character1_RightHandRing2.sz";
connectAttr "Character1_RightHandRing2_translateX.o" "Character1_RightHandRing2.tx"
		;
connectAttr "Character1_RightHandRing2_translateY.o" "Character1_RightHandRing2.ty"
		;
connectAttr "Character1_RightHandRing2_translateZ.o" "Character1_RightHandRing2.tz"
		;
connectAttr "Character1_RightHandRing2_rotateX.o" "Character1_RightHandRing2.rx"
		;
connectAttr "Character1_RightHandRing2_rotateY.o" "Character1_RightHandRing2.ry"
		;
connectAttr "Character1_RightHandRing2_rotateZ.o" "Character1_RightHandRing2.rz"
		;
connectAttr "Character1_RightHandRing2_visibility.o" "Character1_RightHandRing2.v"
		;
connectAttr "Character1_RightHandRing2.s" "Character1_RightHandRing3.is";
connectAttr "Character1_RightHandRing3_translateX.o" "Character1_RightHandRing3.tx"
		;
connectAttr "Character1_RightHandRing3_translateY.o" "Character1_RightHandRing3.ty"
		;
connectAttr "Character1_RightHandRing3_translateZ.o" "Character1_RightHandRing3.tz"
		;
connectAttr "Character1_RightHandRing3_scaleX.o" "Character1_RightHandRing3.sx";
connectAttr "Character1_RightHandRing3_scaleY.o" "Character1_RightHandRing3.sy";
connectAttr "Character1_RightHandRing3_scaleZ.o" "Character1_RightHandRing3.sz";
connectAttr "Character1_RightHandRing3_rotateX.o" "Character1_RightHandRing3.rx"
		;
connectAttr "Character1_RightHandRing3_rotateY.o" "Character1_RightHandRing3.ry"
		;
connectAttr "Character1_RightHandRing3_rotateZ.o" "Character1_RightHandRing3.rz"
		;
connectAttr "Character1_RightHandRing3_visibility.o" "Character1_RightHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_Neck.is";
connectAttr "Character1_Neck_scaleX.o" "Character1_Neck.sx";
connectAttr "Character1_Neck_scaleY.o" "Character1_Neck.sy";
connectAttr "Character1_Neck_scaleZ.o" "Character1_Neck.sz";
connectAttr "Character1_Neck_translateX.o" "Character1_Neck.tx";
connectAttr "Character1_Neck_translateY.o" "Character1_Neck.ty";
connectAttr "Character1_Neck_translateZ.o" "Character1_Neck.tz";
connectAttr "Character1_Neck_rotateX.o" "Character1_Neck.rx";
connectAttr "Character1_Neck_rotateY.o" "Character1_Neck.ry";
connectAttr "Character1_Neck_rotateZ.o" "Character1_Neck.rz";
connectAttr "Character1_Neck_visibility.o" "Character1_Neck.v";
connectAttr "Character1_Neck.s" "Character1_Head.is";
connectAttr "Character1_Head_scaleX.o" "Character1_Head.sx";
connectAttr "Character1_Head_scaleY.o" "Character1_Head.sy";
connectAttr "Character1_Head_scaleZ.o" "Character1_Head.sz";
connectAttr "Character1_Head_translateX.o" "Character1_Head.tx";
connectAttr "Character1_Head_translateY.o" "Character1_Head.ty";
connectAttr "Character1_Head_translateZ.o" "Character1_Head.tz";
connectAttr "Character1_Head_rotateX.o" "Character1_Head.rx";
connectAttr "Character1_Head_rotateY.o" "Character1_Head.ry";
connectAttr "Character1_Head_rotateZ.o" "Character1_Head.rz";
connectAttr "Character1_Head_visibility.o" "Character1_Head.v";
connectAttr "Character1_Head.s" "eyes.is";
connectAttr "eyes_translateX.o" "eyes.tx";
connectAttr "eyes_translateY.o" "eyes.ty";
connectAttr "eyes_translateZ.o" "eyes.tz";
connectAttr "eyes_scaleX.o" "eyes.sx";
connectAttr "eyes_scaleY.o" "eyes.sy";
connectAttr "eyes_scaleZ.o" "eyes.sz";
connectAttr "eyes_rotateX.o" "eyes.rx";
connectAttr "eyes_rotateY.o" "eyes.ry";
connectAttr "eyes_rotateZ.o" "eyes.rz";
connectAttr "eyes_visibility.o" "eyes.v";
connectAttr "Character1_Head.s" "jaw.is";
connectAttr "jaw_lockInfluenceWeights.o" "jaw.liw";
connectAttr "jaw_translateX.o" "jaw.tx";
connectAttr "jaw_translateY.o" "jaw.ty";
connectAttr "jaw_translateZ.o" "jaw.tz";
connectAttr "jaw_scaleX.o" "jaw.sx";
connectAttr "jaw_scaleY.o" "jaw.sy";
connectAttr "jaw_scaleZ.o" "jaw.sz";
connectAttr "jaw_rotateX.o" "jaw.rx";
connectAttr "jaw_rotateY.o" "jaw.ry";
connectAttr "jaw_rotateZ.o" "jaw.rz";
connectAttr "jaw_visibility.o" "jaw.v";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Emeny@Archer_Walk.ma
