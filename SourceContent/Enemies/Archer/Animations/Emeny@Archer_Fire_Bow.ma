//Maya ASCII 2016 scene
//Name: Emeny@Archer_Fire_Bow.ma
//Last modified: Tue, Nov 24, 2015 12:14:41 PM
//Codeset: 1252
requires maya "2016";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "BE8441D1-4CEB-ED79-2A59-8694E768BAC4";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 158.65568138440042 174.02560343581001 217.03442113198747 ;
	setAttr ".r" -type "double3" -21.938352729602375 36.2 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "EDEC8A93-4C86-81E0-34E0-988232AC761C";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 320.49222836597789;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "ECF0B976-45EC-7836-4290-CD9FBC25DF86";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "A85E061D-46B4-B3B0-3215-F58FBAFD8B0A";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "402048AE-4D4B-4404-A549-D28F3F6E0CBC";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "2366E74D-43DD-64EA-A3B3-6091411376B0";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "551157C5-40CB-A6E8-740A-60BE154A7D1F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "D683FB99-41B2-DE52-FBF0-C183C988B4C9";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "ref_frame";
	rename -uid "209157AE-43B9-67F5-C467-DEAA16C158D2";
	addAttr -ci true -sn "refFrame" -ln "refFrame" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "Character1_Hips" -p "ref_frame";
	rename -uid "43649F2C-45FF-4F64-BFBA-87ABA4A1F613";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -19.036789801921167 ;
	setAttr ".ssc" no;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftUpLeg" -p "Character1_Hips";
	rename -uid "FD971B6E-4764-F16D-3A0D-D1BE37AB4184";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 90.000000000000014 -12.849604412773132 43.257528613448208 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftLeg" -p "Character1_LeftUpLeg";
	rename -uid "DC9E82AF-43AF-F8AF-50A3-C5A4703B06A5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -19.152297252867996 -13.416343813706657 -33.623675304480329 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFoot" -p "Character1_LeftLeg";
	rename -uid "FF7169C8-41E5-9645-754A-9CAC00C75B09";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 25.817647449536953 51.040150860538553 -112.05775014674906 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftToeBase" -p "Character1_LeftFoot";
	rename -uid "0E7EC5B4-47ED-B5F7-B046-3587386FF4AE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 43.727116820142825 -29.659481123592514 -4.6723850284209263 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle1" -p "Character1_LeftToeBase";
	rename -uid "05EED4AF-4EA9-7ABF-E89D-03B14E8B9C9D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 165.43734692409092 -20.35908719617909 10.197796850486212 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle2" -p "Character1_LeftFootMiddle1";
	rename -uid "754B4924-4855-ADD5-69D2-EDA5665405BB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 127.90699782306312 -44.626258206212555 -101.65660999147298 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightUpLeg" -p "Character1_Hips";
	rename -uid "D378943E-495B-6842-F04D-AA9720AB6A16";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 133.25752861344816 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightLeg" -p "Character1_RightUpLeg";
	rename -uid "3256FEA0-47C1-2DC7-FC6E-E29D57AEF1BB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -39.772585840344547 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFoot" -p "Character1_RightLeg";
	rename -uid "821F5DBF-41D8-587D-902B-DF9C86F8F3F2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -53.712356932759 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightToeBase" -p "Character1_RightFoot";
	rename -uid "5AB5DB88-4B98-0AFA-17CA-6FAAFDE85C9C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 2.9245623946004821e-006 53.712356932758929 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle1" -p "Character1_RightToeBase";
	rename -uid "AC82DC67-4C5C-9EC2-C686-E08D36F5BDD7";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -4.5380835579220506e-006 -2.7209092876563258e-005 
		39.772585840347155 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle2" -p "Character1_RightFootMiddle1";
	rename -uid "89F18E20-4E44-72CC-6F0C-06B6E40891AA";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -2.8539668357113417e-005 0.00011485155389716205 -133.25752861351003 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine" -p "Character1_Hips";
	rename -uid "3C60781D-4C37-C1D7-F971-279BCBE784DF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 133.25752861344816 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine1" -p "Character1_Spine";
	rename -uid "AAC8EEF2-4336-E014-692E-519EAC8F0986";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -34.34645244194413 -26.596597123422644 16.99576897690589 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftShoulder" -p "Character1_Spine1";
	rename -uid "66BBB544-48EB-4FCD-6E84-AFBA4BD89E4E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -166.39102833412767 21.832119962557094 -120.00622813437471 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftArm" -p "Character1_LeftShoulder";
	rename -uid "E085A7F7-4282-1165-819D-74985B891470";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -172.11387269090633 -44.88241940154802 -37.940590556196071 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftForeArm" -p "Character1_LeftArm";
	rename -uid "617DE52D-4F91-1516-8F68-C7AF10C5B0F2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -84.022423618581243 -14.673598567163568 71.813326221989513 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHand" -p "Character1_LeftForeArm";
	rename -uid "5B0D4AAF-48F2-9E50-8298-3AA43BF633CA";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -24.404340271578391 -34.932697344706668 122.26498801320882 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr -k on ".liw" no;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb1" -p "Character1_LeftHand";
	rename -uid "3EFE6AB6-471D-E3F4-9BB6-0DB9F80168DE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.1043965330995 61.333484361561069 1.8740740862626108 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb2" -p "Character1_LeftHandThumb1";
	rename -uid "6EDBEBE4-4E23-2761-2A6E-5289A666A7AC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -103.44292663098258 20.93424016453444 169.56041330531272 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb3" -p "Character1_LeftHandThumb2";
	rename -uid "488FCFF6-48A7-A36F-2A33-4BA3BBC7C34E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 14.174484134960574 -59.672502756712539 -19.020394805426264 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex1" -p "Character1_LeftHand";
	rename -uid "15A1A208-4628-038D-E472-A09AD17F25FC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -150.59621970507803 75.681392634287164 -124.28723884791442 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex2" -p "Character1_LeftHandIndex1";
	rename -uid "B30B5C0D-4D3D-771B-F8E4-80855CF62A56";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -41.093804604175162 -57.602285575728224 -69.242456746392662 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex3" -p "Character1_LeftHandIndex2";
	rename -uid "CF283CD4-4660-B7B9-B98D-6E932DD9A5E3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.108297963098213 8.7979835742630801 27.088028901385748 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing1" -p "Character1_LeftHand";
	rename -uid "0E7ECEF9-4B59-0AE4-27E4-D0944154AFEE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -141.42437885137375 63.919742192600388 -113.78475086453638 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing2" -p "Character1_LeftHandRing1";
	rename -uid "DE8AE5C0-4895-4035-C959-AD8865847F83";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -35.368646977348 -76.384823071466059 -48.755963146359043 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing3" -p "Character1_LeftHandRing2";
	rename -uid "77C1D8EA-4EEF-AEC8-29D6-0894654D8F39";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 9.6312888734270121 39.523687300631337 15.3258182753707 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightShoulder" -p "Character1_Spine1";
	rename -uid "94DAD387-49E6-4215-AE69-6FA8FCA3A640";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -164.54352138244408 34.957040538466451 -116.14754006477595 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightArm" -p "Character1_RightShoulder";
	rename -uid "A7C95BAC-4FC7-3E95-0A15-8A821FD9F512";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -173.75456654457099 -11.164646449014084 -45.537881339438307 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightForeArm" -p "Character1_RightArm";
	rename -uid "3B8168B3-4AF9-534F-6513-A8B338A4CAD1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 22.753833922449758 50.186429787105524 -101.53518723839815 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHand" -p "Character1_RightForeArm";
	rename -uid "96DBD70C-4C97-7789-2C05-49BB050000FD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 48.334026046900341 -3.7701083025157853 -29.882905994214536 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb1" -p "Character1_RightHand";
	rename -uid "72B2398B-4DFC-5111-944D-BAB0065D2ADE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb2" -p "Character1_RightHandThumb1";
	rename -uid "EBF6BCDD-4BE9-44DF-BF36-E5B54971F4D8";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb3" -p "Character1_RightHandThumb2";
	rename -uid "2EE932E2-442E-8E29-3304-A0BBAABED976";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex1" -p "Character1_RightHand";
	rename -uid "7C128619-4FDB-1EDE-7D98-B59C64589E71";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex2" -p "Character1_RightHandIndex1";
	rename -uid "43384F88-42B8-CA56-EBA7-C484CAC979AB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex3" -p "Character1_RightHandIndex2";
	rename -uid "2661C804-4562-D2F2-9920-44AF3945BA22";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing1" -p "Character1_RightHand";
	rename -uid "FCC9B80B-4AB2-CE32-D6FA-8CBCC661B01F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing2" -p "Character1_RightHandRing1";
	rename -uid "976D791E-4576-A287-D0B0-C8B5332ADDF1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing3" -p "Character1_RightHandRing2";
	rename -uid "AB4D2172-412D-A9AE-47C2-538486017E98";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Neck" -p "Character1_Spine1";
	rename -uid "810B5D70-453E-14D3-5DB8-9CA415B9B429";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 48.229963067601958 30.670012388143238 129.35864775594831 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Head" -p "Character1_Neck";
	rename -uid "EB670C67-4506-BB78-654D-94B63D16FB61";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -14.36476107543206 22.377462763663633 -109.58762475494268 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "eyes" -p "Character1_Head";
	rename -uid "330CFDC6-43C7-BDD6-A6AA-ECB9A9159107";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -157.21319586594478 -21.144487689504771 -3.4677530572789341 ;
	setAttr ".radi" 4.5;
	setAttr ".fbxID" 5;
createNode joint -n "jaw" -p "Character1_Head";
	rename -uid "8644B8C3-4E89-FD16-8F02-A988926383C6";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -157.21319586594478 -21.144487689504771 -3.4677530572789341 ;
	setAttr ".radi" 0.5;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode animCurveTU -n "Character1_LeftHand_lockInfluenceWeights";
	rename -uid "D253DBE6-4696-208C-CD98-7486825590A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "jaw_lockInfluenceWeights";
	rename -uid "7879F764-4083-9748-4C5D-AF98D32DAF53";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "Character1_Hips_translateX";
	rename -uid "91EAA31F-43EC-7372-F74D-45860CB1F7F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -0.47656354308128357 1 0.35794743895530701
		 2 1.3105388879776001 3 2.2064337730407715 4 2.8697364330291748 5 3.1279046535491943
		 6 2.4573037624359131 7 1.0378566980361938 8 -0.19888037443161011 9 -0.60375958681106567
		 10 -0.41867125034332275 11 -0.15566246211528778 12 0.099830061197280884 13 0.43433526158332825
		 14 0.79062885046005249 15 1.1114281415939331 16 1.3395262956619263 17 1.4177988767623901
		 18 1.3391793966293335 19 1.1557950973510742 20 0.89923971891403198 21 0.60112088918685913
		 22 0.29304751753807068 23 0.0065932208672165871 24 -0.22668793797492981 25 -0.37524157762527466
		 26 -0.41801556944847107 27 -0.37474760413169861 28 -0.27496874332427979 29 -0.14822365343570709
		 30 -0.024153769016265869 31 0.067460373044013977 32 0.13302768766880035 33 0.19557489454746246
		 34 0.25037750601768494 35 0.2929726243019104 36 0.38969519734382629 37 0.47226256132125854
		 38 0.32570821046829224 39 -0.12215840816497803 40 -0.76768606901168823 41 -1.3759015798568726
		 42 -1.6981234550476074 43 -1.7311569452285767 44 -1.6191352605819702 45 -1.320609450340271
		 46 -0.85508793592453003 47 -0.62190341949462891 48 -0.47656354308128357;
createNode animCurveTL -n "Character1_Hips_translateY";
	rename -uid "40167AAF-4F8F-9B45-A4CF-959443919586";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 41.952247619628906 1 43.163543701171875
		 2 44.499095916748047 3 45.82318115234375 4 46.988174438476563 5 47.830478668212891
		 6 47.487369537353516 7 46.58746337890625 8 46.022560119628906 9 45.622760772705078
		 10 44.877277374267578 11 44.460796356201172 12 44.415607452392578 13 44.518478393554687
		 14 44.699783325195313 15 44.889633178710938 16 45.01715087890625 17 45.010814666748047
		 18 44.863033294677734 19 44.633174896240234 20 44.350349426269531 21 44.043182373046875
		 22 43.739757537841797 23 43.467758178710938 24 43.254585266113281 25 43.127532958984375
		 26 43.135967254638672 27 43.272853851318359 28 43.480998992919922 29 43.702804565429687
		 30 43.879695892333984 31 43.951896667480469 32 43.860477447509766 33 43.638713836669922
		 34 43.364227294921875 35 43.112751007080078 36 42.836444854736328 37 42.551132202148438
		 38 42.431404113769531 39 42.510498046875 40 42.716518402099609 41 42.9300537109375
		 42 42.744686126708984 43 42.437076568603516 44 42.484733581542969 45 42.531341552734375
		 46 42.088809967041016 47 41.943496704101563 48 41.952247619628906;
createNode animCurveTL -n "Character1_Hips_translateZ";
	rename -uid "CA5FB17B-47C8-C9B2-5297-ED885AE287E1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -9.4437532424926758 1 -10.717351913452148
		 2 -12.202180862426758 3 -13.572798728942871 4 -14.506007194519043 5 -14.692144393920898
		 6 -13.057366371154785 7 -9.8290834426879883 8 -7.2630348205566406 9 -6.2559447288513184
		 10 -5.7319378852844238 11 -5.5568008422851563 12 -5.5789008140563965 13 -5.7786970138549805
		 14 -6.0650420188903809 15 -6.3467288017272949 16 -6.5321779251098633 17 -6.5295209884643555
		 18 -6.3543882369995117 19 -6.0956268310546875 20 -5.7713336944580078 21 -5.3994956016540527
		 22 -4.997948169708252 23 -4.5843973159790039 24 -4.1764249801635742 25 -3.7915198802947998
		 26 -3.3883230686187744 27 -2.9370160102844238 28 -2.4724628925323486 29 -2.029949426651001
		 30 -1.6454740762710571 31 -1.3558585643768311 32 -1.2372043132781982 33 -1.2805459499359131
		 34 -1.3955748081207275 35 -1.4930006265640259 36 -1.5076704025268555 37 -1.5354769229888916
		 38 -1.7371939420700073 39 -2.0399279594421387 40 -2.3754642009735107 41 -2.8045897483825684
		 42 -3.6646182537078857 43 -4.4564003944396973 44 -5.3505411148071289 45 -6.3901200294494629
		 46 -7.3099350929260254 47 -8.4397954940795898 48 -9.4437532424926758;
createNode animCurveTU -n "Character1_Hips_scaleX";
	rename -uid "9A6EC3C9-4E42-1BEA-6F9A-6BA263367CFF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_Hips_scaleY";
	rename -uid "3E7F2DA8-4DAF-8C69-AB4E-BC84ABCC629A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_Hips_scaleZ";
	rename -uid "7357B676-4D96-C062-23EF-1AB4B66C32DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Hips_rotateX";
	rename -uid "D2C1D917-45E1-B907-2D10-A287E3C84E25";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -7.2707223892211914 1 -3.5408270359039307
		 2 0.21871067583560944 3 3.9852583408355708 4 7.6883277893066406 5 11.118149757385254
		 6 14.380654335021971 7 17.148721694946289 8 19.189882278442383 9 20.346315383911133
		 10 21.094114303588867 11 21.473936080932617 12 21.640420913696289 13 21.520606994628906
		 14 21.223505020141602 15 20.857580184936523 16 20.525829315185547 17 20.325944900512695
		 18 20.250694274902344 19 20.219415664672852 20 20.217819213867188 21 20.231849670410156
		 22 20.247602462768555 23 20.251131057739258 24 20.2283935546875 25 20.165210723876953
		 26 20.052051544189453 27 19.900619506835938 28 19.728551864624023 29 19.553194046020508
		 30 19.391117095947266 31 19.257623672485352 32 19.345880508422852 33 19.588081359863281
		 34 19.587644577026367 35 18.913612365722656 36 17.553430557250977 37 15.206035614013672
		 38 11.716597557067871 39 8.1785898208618164 40 4.9785952568054199 41 2.3403570652008057
		 42 0.21769902110099792 43 -1.4041991233825684 44 -2.6244330406188965 45 -3.6854057312011719
		 46 -4.7554116249084473 47 -5.9449858665466309 48 -7.2707223892211914;
createNode animCurveTA -n "Character1_Hips_rotateY";
	rename -uid "60A67A25-400D-B6EB-F191-479FFF3012C8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 9.9845380783081055 1 1.7321869134902954
		 2 -7.0323367118835449 3 -15.783937454223633 4 -23.922601699829102 5 -30.860033035278324
		 6 -36.103931427001953 7 -40.22369384765625 8 -43.504421234130859 9 -45.595901489257813
		 10 -46.394149780273438 11 -46.763065338134766 12 -46.878646850585938 13 -46.621402740478516
		 14 -46.143936157226563 15 -45.598682403564453 16 -45.138698577880859 17 -44.917682647705078
		 18 -44.918991088867188 19 -45.012248992919922 20 -45.180931091308594 21 -45.408527374267578
		 22 -45.678497314453125 23 -45.974319458007813 24 -46.279472351074219 25 -46.577461242675781
		 26 -46.85577392578125 27 -47.132068634033203 28 -47.435070037841797 29 -47.793674468994141
		 30 -48.237274169921875 31 -48.795967102050781 32 -49.521030426025391 33 -50.397209167480469
		 34 -51.357616424560547 35 -52.335651397705078 36 -54.764614105224609 37 -57.191200256347656
		 38 -55.230827331542969 39 -48.982421875 40 -39.418125152587891 41 -29.160671234130859
		 42 -20.826488494873047 43 -14.466593742370604 44 -8.9695301055908203 45 -4.1384119987487793
		 46 0.31668496131896973 47 5.0464506149291992 48 9.9845380783081055;
createNode animCurveTA -n "Character1_Hips_rotateZ";
	rename -uid "6D76E4DB-4EAC-559B-60EE-829459A96C6B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -7.327415943145752 1 -6.1643190383911133
		 2 -5.5487308502197266 3 -5.5192184448242188 4 -6.0151033401489258 5 -6.8076329231262207
		 6 -8.1491813659667969 7 -9.2191534042358398 8 -9.833552360534668 9 -10.052831649780273
		 10 -10.44640064239502 11 -10.636542320251465 12 -10.709575653076172 13 -10.628444671630859
		 14 -10.457178115844727 15 -10.259392738342285 16 -10.092955589294434 17 -10.009984970092773
		 18 -9.9913425445556641 19 -9.9861001968383789 20 -9.9964685440063477 21 -10.024980545043945
		 22 -10.07441520690918 23 -10.14753532409668 24 -10.247006416320801 25 -10.375344276428223
		 26 -10.566302299499512 27 -10.82801342010498 28 -11.124533653259277 29 -11.419412612915039
		 30 -11.675063133239746 31 -11.852343559265137 32 -12.081024169921875 33 -12.345785140991211
		 34 -12.368635177612305 35 -11.834865570068359 36 -10.560868263244629 37 -8.4035568237304687
		 38 -5.8104948997497559 39 -3.9099712371826167 40 -3.0839684009552002 41 -3.1405580043792725
		 42 -3.1349825859069824 43 -3.2832446098327637 44 -4.0110793113708496 45 -4.8563642501831055
		 46 -5.8492822647094727 47 -6.5862312316894531 48 -7.327415943145752;
createNode animCurveTU -n "Character1_Hips_visibility";
	rename -uid "3598ADA7-460F-C16D-F1A6-DCAFE229A52F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftUpLeg_translateX";
	rename -uid "969A2612-40CD-D042-560E-2E9B20DDCF00";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.784505844116211;
createNode animCurveTL -n "Character1_LeftUpLeg_translateY";
	rename -uid "04F8454D-4C77-02DC-0E96-C38882E4A125";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5774786561451037e-006;
createNode animCurveTL -n "Character1_LeftUpLeg_translateZ";
	rename -uid "EC8BC4FD-49B2-0712-6E95-0F81B769917F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0264549255371094;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleX";
	rename -uid "CDD8812C-4F8E-DE58-0F3D-59A0C3959D12";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleY";
	rename -uid "7CA9A4B4-4EE9-AD77-8655-BB914AB2C33D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleZ";
	rename -uid "7E3700C3-41C8-E80C-588D-63B536B7C94B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateX";
	rename -uid "E4FE51DF-4982-0D0F-CBC9-74B91027EDBE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -3.0650556087493896 1 3.234363317489624
		 2 10.067215919494629 3 17.894321441650391 4 26.902633666992187 5 37.667865753173828
		 6 15.522025108337402 7 -2.3108251094818115 8 3.5511338710784912 9 22.263839721679687
		 10 17.250499725341797 11 14.847636222839354 12 14.005587577819824 13 13.119876861572266
		 14 12.299560546875 15 11.629755020141602 16 11.117403984069824 17 10.715512275695801
		 18 10.366806030273437 19 10.087718963623047 20 9.9581117630004883 21 10.032597541809082
		 22 10.336382865905762 23 10.868144035339355 24 11.605198860168457 25 12.511024475097656
		 26 13.728543281555176 27 15.323831558227539 28 17.14732551574707 29 19.044900894165039
		 30 20.845783233642578 31 22.360157012939453 32 23.204305648803711 33 23.502262115478516
		 34 23.803424835205078 35 24.608415603637695 36 27.75147819519043 37 31.219818115234375
		 38 30.058210372924805 39 24.616195678710937 40 15.475894927978516 41 5.1384077072143555
		 42 -4.2586636543273926 43 -11.034287452697754 44 -15.346817016601562 45 -18.309345245361328
		 46 -39.598995208740234 47 -26.43925666809082 48 -3.0650556087493896;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateY";
	rename -uid "6A0A1644-4054-78E2-7C0C-EFA0EFB46D19";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 20.715211868286133 1 17.421470642089844
		 2 14.749125480651857 3 13.091050148010254 4 12.024271965026855 5 10.203447341918945
		 6 20.626142501831055 7 31.119956970214844 8 30.751863479614258 9 24.584829330444336
		 10 26.408960342407227 11 27.000349044799805 12 26.911291122436523 13 26.607488632202148
		 14 26.180540084838867 15 25.735525131225586 16 25.412534713745117 17 25.37580680847168
		 18 25.646736145019531 19 26.09233283996582 20 26.641231536865234 21 27.227752685546875
		 22 27.792295455932617 23 28.280447006225586 24 28.642429351806641 25 28.832582473754886
		 26 28.781963348388672 27 28.498647689819336 28 28.055164337158203 29 27.528167724609375
		 30 27.003213882446289 31 26.577140808105469 32 26.45329475402832 33 26.566442489624023
		 34 26.615585327148437 35 26.307521820068359 36 24.73979377746582 37 22.785976409912109
		 38 23.112693786621094 39 25.236600875854492 40 27.55084228515625 41 28.112953186035156
		 42 26.29847526550293 43 23.288625717163086 44 19.693876266479492 45 15.566055297851561
		 46 18.093828201293945 47 23.807559967041016 48 20.715211868286133;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateZ";
	rename -uid "B2627055-4B77-DF3E-0C9B-EA86B8706372";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -2.845961332321167 1 -7.4164223670959473
		 2 -13.69339656829834 3 -21.094911575317383 4 -28.072402954101563 5 -32.712409973144531
		 6 -37.229373931884766 7 -41.255603790283203 8 -37.525718688964844 9 -30.020645141601559
		 10 -31.500179290771481 11 -32.287441253662109 12 -32.646488189697266 13 -33.118881225585937
		 14 -33.608020782470703 15 -34.030364990234375 16 -34.343917846679687 17 -34.537197113037109
		 18 -34.639732360839844 19 -34.676418304443359 20 -34.620803833007813 21 -34.457233428955078
		 22 -34.183933258056641 23 -33.812732696533203 24 -33.367382049560547 25 -32.880638122558594
		 26 -32.319759368896484 27 -31.66533279418945 28 -30.96858024597168 29 -30.277090072631836
		 30 -29.637830734252933 31 -29.096559524536133 32 -28.741619110107422 33 -28.534191131591797
		 34 -28.342170715332031 35 -28.061298370361328 36 -27.228670120239258 37 -26.47100830078125
		 38 -26.843732833862305 39 -28.452949523925781 40 -31.861789703369144 41 -36.522304534912109
		 42 -41.146434783935547 43 -44.518444061279297 44 -46.62274169921875 45 -47.881160736083984
		 46 -51.747123718261719 47 -30.702199935913086 48 -2.845961332321167;
createNode animCurveTU -n "Character1_LeftUpLeg_visibility";
	rename -uid "6D524BDD-4492-3BD4-0A28-439F18B60A0D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftLeg_translateX";
	rename -uid "F890F9A2-4AA2-C005-3C0D-118DADCE49B6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.968876838684082;
createNode animCurveTL -n "Character1_LeftLeg_translateY";
	rename -uid "86639A6E-4461-8364-2161-A99633A0221C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.2585945129394531;
createNode animCurveTL -n "Character1_LeftLeg_translateZ";
	rename -uid "1BD884A3-49AA-94D8-AD55-01B84CB64844";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 18.197568893432617;
createNode animCurveTU -n "Character1_LeftLeg_scaleX";
	rename -uid "1BD7FAE4-41A1-0BC5-7C9D-76B9EE885478";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_LeftLeg_scaleY";
	rename -uid "56E2FB76-4FD8-C5ED-4E21-F69A3E57DA7A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftLeg_scaleZ";
	rename -uid "1D5CFD26-47ED-8521-E218-AFAD58A08B42";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftLeg_rotateX";
	rename -uid "67DF8954-4EFE-78F2-1FF8-D5A904E888B3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 51.663539886474609 1 44.718601226806641
		 2 36.036308288574219 3 25.927352905273438 4 14.407956123352051 5 0.46056178212165833
		 6 40.993961334228516 7 62.109344482421868 8 38.857810974121094 9 -1.0766217708587646
		 10 10.169177055358887 11 15.12278938293457 12 16.319787979125977 13 16.464271545410156
		 14 15.945913314819338 15 15.180099487304688 16 14.693350791931152 17 15.089878082275391
		 18 16.388261795043945 19 18.064699172973633 20 19.886058807373047 21 21.663633346557617
		 22 23.256351470947266 23 24.562326431274414 24 25.507795333862305 25 26.033987045288086
		 26 25.959749221801758 27 25.282873153686523 28 24.209388732910156 29 22.965993881225586
		 30 21.825626373291016 31 21.113412857055664 32 21.355447769165039 33 22.366363525390625
		 34 23.43470573425293 35 23.914636611938477 36 22.814949035644531 37 21.181520462036133
		 38 21.810842514038086 39 23.723054885864258 40 25.648036956787109 41 26.244674682617188
		 42 26.734073638916016 43 26.917839050292969 44 24.375848770141602 45 20.89453125
		 46 64.661247253417969 47 66.131294250488281 48 51.663539886474609;
createNode animCurveTA -n "Character1_LeftLeg_rotateY";
	rename -uid "10C6FB61-49E6-0FC5-8647-F8B1EA712342";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 22.35516357421875 1 24.68644905090332
		 2 25.13068962097168 3 23.044858932495117 4 18.706449508666992 5 11.728130340576172
		 6 20.036901473999023 7 5.3688430786132813 8 6.3205556869506836 9 2.5506150722503662
		 10 5.8497881889343262 11 7.0406112670898437 12 7.4141559600830078 13 7.408191204071044
		 14 7.1534109115600586 15 6.7850799560546875 16 6.461878776550293 17 6.3539886474609375
		 18 6.4317498207092285 19 6.5293550491333008 20 6.6046838760375977 21 6.6372385025024414
		 22 6.6256599426269531 23 6.5814237594604492 24 6.5224413871765137 25 6.4676532745361328
		 26 6.4165987968444824 27 6.3684344291687012 28 6.3324103355407715 29 6.3183040618896484
		 30 6.3467721939086914 31 6.4547176361083984 32 6.7933416366577148 33 7.3261942863464355
		 34 7.8134517669677743 35 8.0266227722167969 36 8.5931768417358398 37 8.8596944808959961
		 38 6.893040657043457 39 2.551560640335083 40 -3.9090788364410405 41 -10.795487403869629
		 42 -16.366144180297852 43 -20.573513031005859 44 -23.550102233886719 45 -25.72532844543457
		 46 -26.03026008605957 47 -10.437411308288574 48 22.35516357421875;
createNode animCurveTA -n "Character1_LeftLeg_rotateZ";
	rename -uid "C474F37A-4BCC-1967-8E53-49B8294E9D07";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 16.853425979614258 1 11.601533889770508
		 2 5.0552563667297363 3 -2.049898624420166 4 -9.5046253204345703 5 -18.345535278320313
		 6 10.413148880004883 7 23.616865158081055 8 14.056530952453613 9 -5.4145565032958984
		 10 -0.57335710525512695 11 1.5950109958648682 12 2.0248706340789795 13 2.1224923133850098
		 14 2.0215675830841064 15 1.8719004392623904 16 1.8665045499801634 17 2.2222387790679932
		 18 2.956742525100708 19 3.8748109340667725 20 4.859856128692627 21 5.8168768882751465
		 22 6.6743464469909668 23 7.3803725242614737 24 7.8971972465515146 25 8.1943511962890625
		 26 8.1907129287719727 27 7.8867878913879395 28 7.3717007637023935 29 6.7436008453369141
		 30 6.1228971481323242 31 5.6573667526245117 32 5.5299797058105469 33 5.6880607604980469
		 34 5.9272952079772949 35 6.0436019897460938 36 5.0226526260375977 37 3.8588428497314458
		 38 5.7066850662231445 39 9.796539306640625 40 14.846175193786621 41 19.361288070678711
		 42 22.739696502685547 43 25.138435363769531 44 27.367242813110352 45 29.672935485839844
		 46 20.949161529541016 47 23.346002578735352 48 16.853425979614258;
createNode animCurveTU -n "Character1_LeftLeg_visibility";
	rename -uid "200A45D4-45C2-4092-7DEC-569780D19D36";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftFoot_translateX";
	rename -uid "9C7CB3A6-4633-B05C-D810-3FBA432E70D2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5243232250213623;
createNode animCurveTL -n "Character1_LeftFoot_translateY";
	rename -uid "57FE7063-40CC-B5D7-E6D8-9B8251266289";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.000351905822754;
createNode animCurveTL -n "Character1_LeftFoot_translateZ";
	rename -uid "14FA6C7C-4767-0BB6-B24D-5C882947954B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.967419624328613;
createNode animCurveTU -n "Character1_LeftFoot_scaleX";
	rename -uid "75F18465-44F8-36A9-A7A5-9B8E64CDA621";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFoot_scaleY";
	rename -uid "1198B197-471F-66F0-9CC4-66B6D95B3267";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleZ";
	rename -uid "6D960A89-4A83-459C-41D0-B2B88C6700B6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999964237213135;
createNode animCurveTA -n "Character1_LeftFoot_rotateX";
	rename -uid "DBCC24DD-450F-4712-8601-B4AF2AB45CF6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 41.507350921630859 1 39.27130126953125
		 2 35.608547210693359 3 30.933593750000004 4 25.725852966308594 5 19.759893417358398
		 6 29.283226013183597 7 29.207519531250004 8 24.615564346313477 9 12.769744873046875
		 10 18.643527984619141 11 20.854883193969727 12 21.097898483276367 13 20.430206298828125
		 14 19.26024055480957 15 18.003568649291992 16 17.114170074462891 17 17.069244384765625
		 18 17.855384826660156 19 19.041172027587891 20 20.464471817016602 21 21.984712600708008
		 22 23.482128143310547 23 24.85270881652832 24 26.002937316894531 25 26.84446907043457
		 26 27.300840377807617 27 27.430601119995117 28 27.346321105957031 29 27.166263580322266
		 30 27.023126602172852 31 27.068004608154297 32 27.502761840820313 33 28.218212127685547
		 34 28.902833938598636 35 29.253961563110348 36 29.43892860412598 37 29.263076782226566
		 38 28.157926559448242 39 25.767055511474609 40 21.641239166259766 41 16.295793533325195
		 42 11.02459716796875 43 6.3925571441650391 44 1.766517162322998 45 -2.7755720615386963
		 46 -3.552264928817749 47 18.294961929321289 48 41.507350921630859;
createNode animCurveTA -n "Character1_LeftFoot_rotateY";
	rename -uid "C841AB27-4C9A-6628-EA23-948DF52A8B53";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -7.194810390472413 1 -5.5061860084533691
		 2 -3.6811277866363521 3 -1.5807756185531616 4 1.3490440845489502 5 5.6440258026123047
		 6 5.0247530937194824 7 9.7843255996704102 8 12.863494873046875 9 19.221216201782227
		 10 15.397326469421388 11 13.619187355041504 12 13.10984992980957 13 12.914824485778809
		 14 12.907487869262695 15 12.963668823242188 16 12.936064720153809 17 12.65706729888916
		 18 12.132705688476562 19 11.51035213470459 20 10.837430000305176 21 10.152300834655762
		 22 9.4839591979980469 23 8.8544597625732422 24 8.281611442565918 25 7.7820444107055664
		 26 7.3507647514343271 27 6.983609676361084 28 6.6974549293518066 29 6.4957270622253418
		 30 6.3600306510925293 31 6.2501673698425293 32 6.1139216423034668 33 5.9735732078552246
		 34 5.8849778175354004 35 5.868649959564209 36 6.435917854309082 37 7.1174407005310059
		 38 6.285710334777832 39 4.3608269691467285 40 2.1335926055908203 41 0.90161091089248668
		 42 0.74827075004577637 43 0.86870920658111572 44 2.1582009792327881 45 3.7651081085205078
		 46 -5.4094200134277344 47 -14.62071418762207 48 -7.194810390472413;
createNode animCurveTA -n "Character1_LeftFoot_rotateZ";
	rename -uid "76426799-4F65-E101-7295-83BDF282064D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 14.614688873291016 1 13.229243278503418
		 2 11.65534496307373 3 9.9345788955688477 4 7.7786355018615723 5 4.4656271934509277
		 6 7.6421952247619638 7 6.6329460144042969 8 3.5450925827026367 9 -4.1506609916687012
		 10 0.2824999988079071 11 1.9729354381561277 12 2.2785356044769287 13 2.0541276931762695
		 14 1.5260747671127319 15 0.92430949211120594 16 0.51608705520629883 17 0.59474486112594604
		 18 1.1387856006622314 19 1.8757171630859375 20 2.7131168842315674 21 3.5763843059539795
		 22 4.4071950912475586 23 5.158782958984375 24 5.7916693687438965 25 6.2702789306640625
		 26 6.576317310333252 27 6.7413735389709473 28 6.8035392761230469 29 6.8048748970031738
		 30 6.7983427047729492 31 6.8501033782958984 32 7.0474424362182617 33 7.3426470756530762
		 34 7.6084604263305673 35 7.7352442741394043 36 7.6263775825500497 37 7.3568191528320313
		 38 7.2244133949279785 39 7.0297021865844727 40 6.4550638198852539 41 5.2007246017456055
		 42 3.5031344890594482 43 1.819154739379883 44 -0.60898822546005249 45 -3.3972752094268799
		 46 1.8922476768493655 47 9.226043701171875 48 14.614688873291016;
createNode animCurveTU -n "Character1_LeftFoot_visibility";
	rename -uid "F0B632E5-47C3-07B6-AD44-BABF9505C935";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftToeBase_translateX";
	rename -uid "38E895A6-427E-C220-FC55-978CCA2EF626";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.585639476776123;
createNode animCurveTL -n "Character1_LeftToeBase_translateY";
	rename -uid "BA0FB4DB-4558-E907-E212-20AA410F7BC8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3360438346862793;
createNode animCurveTL -n "Character1_LeftToeBase_translateZ";
	rename -uid "3831BB7A-4817-8516-3B8A-1CBC441116CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.4897398948669434;
createNode animCurveTU -n "Character1_LeftToeBase_scaleX";
	rename -uid "8DD1E888-45B6-1646-A45B-D8B95B8D5E12";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftToeBase_scaleY";
	rename -uid "687DD013-4FAD-83D6-09E1-41B64206C9AD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftToeBase_scaleZ";
	rename -uid "1923352F-4688-F481-6D83-A3BD27AC908E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_LeftToeBase_rotateX";
	rename -uid "A3F133AE-4E70-175E-F180-4FB33BC81BB3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2782757902461981e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateY";
	rename -uid "9F6BC809-45A4-1884-7875-EE8CF40D0440";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.5058064512251121e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateZ";
	rename -uid "7A1CC5D2-4AB3-EC8A-99B3-C4B4ACF4E00D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.7303040656411213e-009;
createNode animCurveTU -n "Character1_LeftToeBase_visibility";
	rename -uid "77641E62-41EA-AAE3-45DC-CC9A7C36E852";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateX";
	rename -uid "A9E53990-485F-C093-C286-5B8F25137267";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7153444290161133;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateY";
	rename -uid "3CFD1DE4-4471-6E1F-F536-2F98FDBC8D6C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.0020139217376709;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateZ";
	rename -uid "9AA83F9A-4F20-66BC-7B39-76B4505D9637";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.1786108016967773;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleX";
	rename -uid "BE79DE38-41CB-BCC7-4C46-EBA08B483DB8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleY";
	rename -uid "7FDC9421-4BE8-6C93-1A90-568875E3F59D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999940395355225;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleZ";
	rename -uid "87923C2F-48CE-7781-252F-1F8849C94FFD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateX";
	rename -uid "47BAD987-4901-512F-BAE6-DEACF312744E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.5825255529384776e-010;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateY";
	rename -uid "674E7B30-4C47-D3F5-27DC-96A7BEE81DFA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.3760111655946048e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateZ";
	rename -uid "F00B0F24-4189-1786-159F-34AC38349624";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2730552612083557e-008;
createNode animCurveTU -n "Character1_LeftFootMiddle1_visibility";
	rename -uid "A36D7D82-4C18-2192-85A5-60BFB6DACDF8";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateX";
	rename -uid "E80E1FDB-4FBF-A0EE-401C-5F8A4A13A582";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4684751033782959;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateY";
	rename -uid "ED11C6C8-47DD-8986-8099-8EA156670354";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.1393222808837891;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateZ";
	rename -uid "BAE6A74F-4EA5-839E-7775-A6A0DDE0F7A0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.3014278411865234;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleX";
	rename -uid "08B8BB01-4356-D03D-8FF4-60A7820627FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleY";
	rename -uid "2C65CC0A-4421-94C7-47E4-6F95CB11B01A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleZ";
	rename -uid "87DEED71-43ED-FED7-C9F7-41AEE5A2F8E6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateX";
	rename -uid "BC7A1109-4CF1-BE01-145B-D1BDDFBDDBA5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.9311034549218675e-008;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateY";
	rename -uid "09C42A7A-4D3A-0704-7359-CC8BD96484A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5914608297862287e-008;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateZ";
	rename -uid "E28CB264-49F9-7562-EE84-46BA2D95FEC8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.4659944065681429e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle2_visibility";
	rename -uid "972A60E6-4B1F-6A99-6CF4-31A72888182B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightUpLeg_translateX";
	rename -uid "4E79DB86-419A-E580-0608-66AEE0C95C80";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.489771842956543;
createNode animCurveTL -n "Character1_RightUpLeg_translateY";
	rename -uid "DF0FFC8B-481F-7759-F954-4BAB69CFAFD2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.6505136489868164;
createNode animCurveTL -n "Character1_RightUpLeg_translateZ";
	rename -uid "F3803A3D-4F50-C0D9-84CD-FE98C61BF21A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0264549255371094;
createNode animCurveTU -n "Character1_RightUpLeg_scaleX";
	rename -uid "11EEF2BC-4C70-DCB5-71D2-06884ADE57D4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightUpLeg_scaleY";
	rename -uid "C8AB47AA-4BA2-8918-58D3-5F8418E18F27";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightUpLeg_scaleZ";
	rename -uid "9EC0E551-4C6B-60B6-12C1-16AD7688FC2B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightUpLeg_rotateX";
	rename -uid "24963B89-4D99-8BCB-537B-7D900EE2EABE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -31.869653701782227 1 -37.206008911132813
		 2 -45.079765319824219 3 -52.900741577148438 4 -58.763854980468743 5 -61.244747161865234
		 6 -57.816658020019524 7 -51.224288940429688 8 -47.036182403564453 9 -45.459743499755859
		 10 -43.947486877441406 11 -43.178474426269531 12 -43.069107055664063 13 -43.294368743896484
		 14 -43.697601318359375 15 -44.12945556640625 16 -44.450229644775391 17 -44.527835845947266
		 18 -44.399547576904297 19 -44.216251373291016 20 -44.015979766845703 21 -43.830718994140625
		 22 -43.685096740722656 23 -43.596328735351562 24 -43.575450897216797 25 -43.630573272705078
		 26 -43.834663391113281 27 -44.209716796875 28 -44.690723419189453 29 -45.194473266601563
		 30 -45.597862243652344 31 -45.730777740478516 32 -45.387832641601563 33 -44.669303894042969
		 34 -43.816753387451172 35 -42.970333099365234 36 -41.402889251708984 37 -39.68939208984375
		 38 -39.736370086669922 39 -42.153377532958984 40 -47.558155059814453 41 -54.929264068603516
		 42 -38.622180938720703 43 -16.274694442749023 44 0.27851581573486328 45 -0.33320000767707825
		 46 -6.6760306358337402 47 -18.853631973266602 48 -31.869653701782227;
createNode animCurveTA -n "Character1_RightUpLeg_rotateY";
	rename -uid "992179D5-4F9E-50C7-5E1B-DDA0BA414683";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 35.922103881835937 1 33.225517272949219
		 2 27.109474182128906 3 19.717105865478516 4 13.940883636474609 5 12.113659858703613
		 6 12.083156585693359 7 9.3014717102050781 8 6.5746569633483887 9 6.7936134338378906
		 10 8.3649501800537109 11 9.5727224349975586 12 10.206282615661621 13 10.68753719329834
		 14 11.006246566772461 15 11.163846015930176 16 11.178623199462891 17 11.078070640563965
		 18 10.91165828704834 19 10.692127227783203 20 10.389323234558105 21 9.9843606948852539
		 22 9.4711036682128906 23 8.8542499542236328 24 8.1459827423095703 25 7.3610520362854004
		 26 6.3373217582702637 27 5.0064444541931152 28 3.5189957618713379 29 2.0420722961425781
		 30 0.78335005044937134 31 0.0020757985766977072 32 0.15160943567752838 33 1.0811736583709717
		 34 2.1720855236053467 35 2.8604211807250977 36 3.9520618915557861 37 4.8305144309997559
		 38 2.5089054107666016 39 -2.9295523166656494 40 -9.8590068817138672 41 -15.276802062988281
		 42 5.5353131294250488 43 35.729942321777344 44 35.369052886962891 45 25.924617767333984
		 46 29.331716537475586 47 33.384654998779297 48 35.922103881835937;
createNode animCurveTA -n "Character1_RightUpLeg_rotateZ";
	rename -uid "03AE3E13-4B20-0F28-43B6-EA8554882DAB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -18.133783340454102 1 -15.021705627441406
		 2 -13.545505523681641 3 -12.806228637695312 4 -11.696563720703125 5 -10.253173828125
		 6 -12.068775177001953 7 -14.79390239715576 8 -16.742284774780273 9 -19.138870239257813
		 10 -21.768156051635742 11 -23.263736724853516 12 -23.70147705078125 13 -23.666940689086914
		 14 -23.347700119018555 15 -22.933870315551758 16 -22.631038665771484 17 -22.656476974487305
		 18 -23.03748893737793 19 -23.60584831237793 20 -24.286142349243164 21 -25.014305114746094
		 22 -25.736217498779297 23 -26.403202056884766 24 -26.967292785644531 25 -27.376800537109375
		 26 -27.512462615966797 27 -27.357950210571289 28 -27.01622200012207 29 -26.621191024780273
		 30 -26.365856170654297 31 -26.512413024902344 32 -27.260379791259766 33 -28.485544204711914
		 34 -30.008701324462894 35 -31.744741439819336 36 -35.077850341796875 37 -38.679019927978516
		 38 -38.790279388427734 39 -34.884990692138672 40 -26.688161849975586 41 -15.62311840057373
		 42 -15.850540161132814 43 -4.7315521240234375 44 0.78894740343093872 45 -2.779583215713501
		 46 -5.9836325645446777 47 -11.553922653198242 48 -18.133783340454102;
createNode animCurveTU -n "Character1_RightUpLeg_visibility";
	rename -uid "E439BC9F-4F9A-ABD7-C0A8-A09A3AF3EEA6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightLeg_translateX";
	rename -uid "D60CB852-4649-9BCB-0924-9793051D04C8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -18.197565078735352;
createNode animCurveTL -n "Character1_RightLeg_translateY";
	rename -uid "2BB4304C-4778-59B6-2A4C-75A3D7E707C2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.1862249374389648;
createNode animCurveTL -n "Character1_RightLeg_translateZ";
	rename -uid "0BBD064E-4D35-1CD0-627A-7EBD1EC35673";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.5520458221435547;
createNode animCurveTU -n "Character1_RightLeg_scaleX";
	rename -uid "1340D9DD-4F0E-16C7-E49D-B2A75659CFE0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightLeg_scaleY";
	rename -uid "AA082A8A-4A9A-7D42-CCE1-57A8F9CC9781";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightLeg_scaleZ";
	rename -uid "213ECD8D-4E53-D410-D8AA-8F93F54CD5FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightLeg_rotateX";
	rename -uid "D96D50D3-4737-12F9-B2B6-FC9C7970A0DD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 33.260894775390625 1 26.931921005249023
		 2 17.587146759033203 3 6.6826329231262207 4 -2.0846028327941895 5 -3.6159591674804683
		 6 0.57261133193969727 7 2.0814006328582764 8 3.0491266250610352 9 5.5009803771972656
		 10 8.1181783676147461 11 9.605433464050293 12 9.9803838729858398 13 9.7354440689086914
		 14 9.1096153259277344 15 8.3487377166748047 16 7.7201623916625977 17 7.5099644660949707
		 18 7.777496337890625 19 8.337397575378418 20 9.110626220703125 21 10.0159912109375
		 22 10.964313507080078 23 11.855134963989258 24 12.577795028686523 25 13.015935897827148
		 26 12.954692840576172 27 12.405782699584961 28 11.599026679992676 29 10.768181800842285
		 30 10.166230201721191 31 10.087106704711914 32 10.903284072875977 33 12.458995819091797
		 34 14.261549949645996 35 15.828193664550781 36 18.48271369934082 37 20.969112396240234
		 38 19.311681747436523 39 13.188136100769043 40 2.6378796100616455 41 -10.216503143310547
		 42 5.2910561561584473 43 51.656982421875 44 20.327835083007812 45 22.183673858642578
		 46 23.989124298095703 47 28.809495925903317 48 33.260894775390625;
createNode animCurveTA -n "Character1_RightLeg_rotateY";
	rename -uid "C7C6FCDF-479A-C6E8-0929-18B72459A2BE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -26.233663558959961 1 -28.974010467529293
		 2 -28.360744476318359 3 -23.30937385559082 4 -14.346705436706545 5 -5.1094498634338379
		 6 -8.6346282958984375 7 -15.230809211730955 8 -17.317764282226563 9 -19.837318420410156
		 10 -24.775583267211914 11 -27.523891448974609 12 -27.989236831665039 13 -27.699762344360352
		 14 -26.952106475830078 15 -26.063261032104492 16 -25.40301513671875 17 -25.37408447265625
		 18 -26.061029434204102 19 -27.130182266235352 20 -28.350860595703125 21 -29.529291152954098
		 22 -30.512041091918949 23 -31.179103851318356 24 -31.433183670043945 25 -31.185070037841797
		 26 -30.031158447265625 27 -27.880180358886719 28 -25.103305816650391 29 -22.137493133544922
		 30 -19.56324577331543 31 -18.132808685302734 32 -18.704521179199219 33 -20.880342483520508
		 34 -23.638725280761719 35 -26.193140029907227 36 -29.50168418884277 37 -32.815837860107422
		 38 -33.562286376953125 39 -31.181005477905273 40 -25.326826095581055 41 -16.967655181884766
		 42 -57.951877593994148 43 -73.723556518554688 44 -57.512165069580078 45 -29.623533248901371
		 46 -30.193510055541992 47 -28.396272659301758 48 -26.233663558959961;
createNode animCurveTA -n "Character1_RightLeg_rotateZ";
	rename -uid "2EF894D9-48F4-07D7-3609-9A9395DE4238";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 2.9731273651123047 1 0.85799700021743774
		 2 -0.90044844150543213 3 -1.4445425271987915 4 -1.0395221710205078 5 -0.48955032229423529
		 6 -0.35938817262649536 7 -0.80723816156387329 8 -0.95509093999862671 9 -1.0400760173797607
		 10 -1.5449976921081543 11 -1.9014445543289182 12 -1.9527765512466428 13 -1.9219361543655396
		 14 -1.844152569770813 15 -1.7582805156707764 16 -1.7046757936477661 17 -1.7227375507354736
		 18 -1.8227790594100952 19 -1.9703973531723025 20 -2.1353039741516113 21 -2.2871134281158447
		 22 -2.3975107669830322 23 -2.4431138038635254 24 -2.408097505569458 25 -2.2849915027618408
		 26 -2.0153961181640625 27 -1.6126651763916016 28 -1.1723370552062988 29 -0.7706141471862793
		 30 -0.46095475554466242 31 -0.27621927857398987 32 -0.24502985179424283 33 -0.34322535991668701
		 34 -0.52754253149032593 35 -0.75519371032714844 36 -1.0082405805587769 37 -1.3632913827896118
		 38 -1.8839433193206787 39 -2.2608301639556885 40 -2.1889772415161133 41 -1.6963615417480469
		 42 -16.793636322021484 43 -68.270797729492187 44 -15.474991798400881 45 -0.32471823692321777
		 46 -0.077810592949390411 47 1.4277395009994507 48 2.9731273651123047;
createNode animCurveTU -n "Character1_RightLeg_visibility";
	rename -uid "BF21463D-4926-C434-E172-8C92350E974F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightFoot_translateX";
	rename -uid "A1BB9D08-4928-9647-FCE3-9783936F1264";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -18.25177001953125;
createNode animCurveTL -n "Character1_RightFoot_translateY";
	rename -uid "9753B123-42D7-41BE-47E0-D2926B2981C2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.0794563293457031;
createNode animCurveTL -n "Character1_RightFoot_translateZ";
	rename -uid "E022D4AE-498D-4A88-EF0C-6099C3A9FFFF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5524176359176636;
createNode animCurveTU -n "Character1_RightFoot_scaleX";
	rename -uid "55743FF8-4564-BAAC-9E1D-12BB8515FE31";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFoot_scaleY";
	rename -uid "1C59DDEE-4239-FD15-AD20-548C8DB77CAB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFoot_scaleZ";
	rename -uid "16B7C498-49CA-1259-EF91-A1A26CBB9AF0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightFoot_rotateX";
	rename -uid "969361E4-40EA-4A94-C7B2-52B4650DECAD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -1.3250502347946167 1 -7.308112144470214
		 2 -14.156337738037109 3 -15.57863712310791 4 -10.845859527587891 5 -5.7473092079162598
		 6 -8.1437826156616211 7 -12.297336578369141 8 -14.213871002197266 9 -15.775517463684082
		 10 -17.885013580322266 11 -18.864713668823242 12 -18.751287460327148 13 -18.194696426391602
		 14 -17.401885986328125 15 -16.585697174072266 16 -15.983534812927244 17 -15.850736618041992
		 18 -16.242191314697266 19 -16.953342437744141 20 -17.850322723388672 21 -18.810869216918945
		 22 -19.721061706542969 23 -20.470392227172852 24 -20.948543548583984 25 -21.043964385986328
		 26 -20.497718811035156 27 -19.302181243896484 28 -17.724273681640625 29 -16.047573089599609
		 30 -14.596941947937013 31 -13.756014823913574 32 -13.904561042785645 33 -14.841631889343263
		 34 -16.105148315429688 35 -17.28849983215332 36 -18.618392944335938 37 -19.948003768920898
		 38 -20.823843002319336 39 -20.630388259887695 40 -18.263698577880859 41 -13.894355773925781
		 42 -26.851118087768555 43 -20.236892700195313 44 -8.9185962677001953 45 -4.8700604438781738
		 46 -4.7890934944152832 47 -3.1429493427276611 48 -1.3250502347946167;
createNode animCurveTA -n "Character1_RightFoot_rotateY";
	rename -uid "E30B9840-4AB6-972A-C39B-3A9E04BAB6C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 0.60343074798583984 1 2.4779098033905029
		 2 4.4324641227722168 3 5.2755980491638184 4 3.7330925464630127 5 1.8267496824264526
		 6 3.0442540645599365 7 4.902766227722168 8 5.9063553810119629 9 6.7648777961730957
		 10 7.918109416961669 11 8.4394550323486328 12 8.3537569046020508 13 8.0236616134643555
		 14 7.5716938972473145 15 7.1219906806945801 16 6.8089442253112793 17 6.7742247581481934
		 18 7.0544266700744629 19 7.5271320343017587 20 8.0950565338134766 21 8.6730146408081055
		 22 9.1914215087890625 23 9.5954513549804687 24 9.8415384292602539 25 9.8908405303955078
		 26 9.6288375854492187 27 9.0329065322875977 28 8.1992340087890625 29 7.2581067085266113
		 30 6.4041748046875 31 5.8991990089416504 32 6.0070285797119141 33 6.5600810050964355
		 34 7.234100341796875 35 7.8508348464965829 36 8.5171566009521484 37 9.1661100387573242
		 38 9.8267621994018555 39 10.179740905761719 40 9.2356939315795898 41 6.4500679969787598
		 42 14.143429756164551 43 7.9731287956237793 44 2.8787455558776855 45 1.7335008382797241
		 46 1.8600056171417236 47 1.2468693256378174 48 0.60343074798583984;
createNode animCurveTA -n "Character1_RightFoot_rotateZ";
	rename -uid "2B97D2D9-450D-BA85-FB7B-38B95844AEF5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -1.2137231826782227 1 -6.4503107070922852
		 2 -5.3470139503479004 3 1.4247640371322632 4 7.0401535034179687 5 7.0323696136474609
		 6 7.3642711639404288 7 11.495229721069336 8 14.694601058959961 9 15.281189918518068
		 10 15.568839073181151 11 15.53806686401367 12 15.358436584472656 13 15.128973007202148
		 14 14.905551910400389 15 14.743975639343262 16 14.704292297363283 17 14.851640701293944
		 18 15.157704353332521 19 15.53120803833008 20 15.942231178283693 21 16.364316940307617
		 22 16.779356002807617 23 17.180225372314453 24 17.570528030395508 25 17.960784912109375
		 26 18.454338073730469 27 19.088420867919922 28 19.767099380493164 29 20.395267486572266
		 30 20.884044647216797 31 21.145620346069336 32 20.964351654052734 33 20.353561401367188
		 34 19.587282180786133 35 18.989961624145508 36 17.926612854003906 37 16.990755081176758
		 38 18.485601425170898 39 22.386381149291992 40 27.712242126464844 41 31.80302810668945
		 42 23.649440765380859 43 9.0824050903320312 44 -0.082664676010608673 45 1.7317407131195068
		 46 1.9285949468612671 47 0.15676748752593994 48 -1.2137231826782227;
createNode animCurveTU -n "Character1_RightFoot_visibility";
	rename -uid "B6BD5E22-455B-0267-9399-4EB5D6222D9A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightToeBase_translateX";
	rename -uid "6CDE083B-404B-1EDF-590A-CBA66C4A295E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2767767906188965;
createNode animCurveTL -n "Character1_RightToeBase_translateY";
	rename -uid "D794A3C8-4AFF-3425-1AE6-C080EA7E72B7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.0139193534851074;
createNode animCurveTL -n "Character1_RightToeBase_translateZ";
	rename -uid "81D86A75-4AB9-9F99-B213-19BA2A600CC5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8259720802307129;
createNode animCurveTU -n "Character1_RightToeBase_scaleX";
	rename -uid "E125F821-486D-BFBC-6321-209009146C9A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightToeBase_scaleY";
	rename -uid "0E9099FB-40D5-057C-4F5C-ABADAB1AB3C2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightToeBase_scaleZ";
	rename -uid "EBA3D047-4512-6C26-6698-CBA2B973250C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightToeBase_rotateX";
	rename -uid "27232B7C-4B90-36D7-63DB-A69999E388EC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.0243666177275372e-009;
createNode animCurveTA -n "Character1_RightToeBase_rotateY";
	rename -uid "22524C11-481D-93EF-AB7A-26B9208A5461";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.6318545121029047e-009;
createNode animCurveTA -n "Character1_RightToeBase_rotateZ";
	rename -uid "8291342B-4AC0-F4FC-9740-9EB33252FAE5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.3121586563473784e-010;
createNode animCurveTU -n "Character1_RightToeBase_visibility";
	rename -uid "4C7B900E-417C-7E75-A4BE-289E9ACEF25E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateX";
	rename -uid "AC5EEF0E-4503-4289-039D-A296461F5386";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.0018006421159952879;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateY";
	rename -uid "EB247E2C-4AC9-1C2D-099D-7B8ED8B76EDC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.0064689838327467442;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateZ";
	rename -uid "F35ED238-4CE2-E7B0-CE61-9ABFFD94A5C5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.6805553436279297;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleX";
	rename -uid "1D2B99ED-4CF7-CC42-2F1E-5CA20246E48E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleY";
	rename -uid "790717A1-4C7E-9767-07D7-B6B80CD85993";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleZ";
	rename -uid "9003B93C-4154-7760-E6A2-C9B3AF4DF2F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateX";
	rename -uid "A49DCF26-4512-17A0-A3FE-668055E1D7F1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2831563367399212e-008;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateY";
	rename -uid "F20E34A4-4EEF-9DF7-2439-38848678179E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4574002005218745e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateZ";
	rename -uid "DDBF5926-4BFD-ECFB-6827-8AA6AE567F56";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.6248326017986869e-009;
createNode animCurveTU -n "Character1_RightFootMiddle1_visibility";
	rename -uid "C676B259-4E06-C503-F55F-C4B09CCD1928";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateX";
	rename -uid "25F5D21D-467B-61B2-4AC2-42AFAC956DFE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.586079745015013e-006;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateY";
	rename -uid "F7A4E5F3-4FF3-9DE2-439D-18865A72066C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.3302297803602414e-007;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateZ";
	rename -uid "4A821EB8-4668-6B2D-2444-CC8010CECB1C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.0234856605529785;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleX";
	rename -uid "617B0311-4536-4819-C427-51A740782946";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleY";
	rename -uid "6E264F34-4960-1EC5-30F9-8DA51B70B838";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleZ";
	rename -uid "3522BA17-41D8-A9F6-34CE-438D0CE345CA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateX";
	rename -uid "1CEA1FAC-46E9-76B2-03B3-56B508D7CE2F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.6217176934674171e-008;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateY";
	rename -uid "98FC44DF-4C2F-53FC-831D-449053E27B46";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.1577369224701215e-008;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateZ";
	rename -uid "82217EB1-432C-CA9A-38B6-F1BED4A675A3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.0248865634519007e-009;
createNode animCurveTU -n "Character1_RightFootMiddle2_visibility";
	rename -uid "4C8D970B-498C-09B2-5BE0-95A9BBAC05B4";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_Spine_translateX";
	rename -uid "F55BD209-4842-42D3-ED5D-CC8A4D466BAB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6075620651245117;
createNode animCurveTL -n "Character1_Spine_translateY";
	rename -uid "852D7BA6-4A79-B9F4-1437-83AC1C840588";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.353469848632812;
createNode animCurveTL -n "Character1_Spine_translateZ";
	rename -uid "5B3C5C36-4D24-FDF5-31E8-AEB4F2E73EF1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2061522006988525;
createNode animCurveTU -n "Character1_Spine_scaleX";
	rename -uid "624CE04B-4455-80B8-F5C9-2AA7D6A67C03";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine_scaleY";
	rename -uid "A0878BD7-42BF-D3E6-6AB6-33A7C421609F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine_scaleZ";
	rename -uid "9C581D01-40FF-C515-69F1-45BA814505DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_Spine_rotateX";
	rename -uid "7B2FBEAB-42B9-E073-012D-20A12E6EBD68";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -4.8795571327209473 1 -3.5662417411804199
		 2 -2.2123322486877441 3 -0.90557974576950084 4 0.29222115874290466 5 1.3528202772140503
		 6 2.0546309947967529 7 2.5925083160400391 8 3.0758330821990967 9 3.3281898498535156
		 10 3.2002716064453125 11 2.9231581687927246 12 2.5494108200073242 13 2.132807731628418
		 14 1.7315665483474731 15 1.4064480066299438 16 1.2208340167999268 17 1.2396363019943237
		 18 1.5088953971862793 19 1.9931679964065552 20 2.6345386505126953 21 3.3731739521026611
		 22 4.1460986137390137 23 4.8863296508789062 24 5.5225582122802734 25 5.9797325134277344
		 26 6.2040619850158691 27 6.2320795059204102 28 6.1255011558532715 29 5.9480085372924805
		 30 5.7636547088623047 31 5.6367702484130859 32 5.5887622833251953 33 5.5499639511108398
		 34 5.4492559432983398 35 5.217829704284668 36 4.0594053268432617 37 4.1779108047485352
		 38 3.8556909561157222 39 2.252979040145874 40 0.43708613514900208 41 -0.80583840608596802
		 42 -1.3252207040786743 43 -1.9381937980651853 44 -2.5533730983734131 45 -3.0778603553771973
		 46 -3.5666263103485107 47 -4.2105755805969238 48 -4.8795571327209473;
createNode animCurveTA -n "Character1_Spine_rotateY";
	rename -uid "D0EB2232-4E47-4D34-AE1B-74AA345C619D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -10.930930137634277 1 -9.3078985214233398
		 2 -7.509291172027587 3 -5.6052341461181641 4 -3.6779584884643559 5 -1.8231471776962282
		 6 -0.092154674232006073 7 1.3659857511520386 8 2.4152200222015381 9 2.9865293502807617
		 10 3.2318556308746338 11 3.1861822605133057 12 2.9550714492797852 13 2.643310546875
		 14 2.3538620471954346 15 2.1887528896331787 16 2.2490849494934082 17 2.6354014873504639
		 18 3.4188508987426758 19 4.5312256813049316 20 5.8642435073852539 21 7.3105068206787109
		 22 8.7639827728271484 23 10.120265960693359 24 11.276643753051758 25 12.131819725036621
		 26 12.595576286315918 27 12.723196983337402 28 12.630997657775879 29 12.43442440032959
		 30 12.248652458190918 31 12.188578605651855 32 12.18767261505127 33 12.174500465393066
		 34 12.255745887756348 35 12.53619384765625 36 14.243706703186035 37 14.679078102111815
		 38 13.088208198547363 39 7.9443402290344238 40 1.4575927257537842 41 -3.070598840713501
		 42 -5.3148269653320313 43 -6.7693500518798828 44 -7.798027515411377 45 -8.6247434616088867
		 46 -9.2607736587524414 47 -9.9955778121948242 48 -10.930930137634277;
createNode animCurveTA -n "Character1_Spine_rotateZ";
	rename -uid "8515411A-415A-E046-EED4-9F91F20B800A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 6.0455126762390137 1 4.7557039260864258
		 2 3.4284300804138184 3 2.1383581161499023 4 0.91286027431488037 5 -0.28582221269607544
		 6 -0.69947826862335205 7 -1.1587778329849243 8 -2.0365085601806641 9 -2.9967644214630127
		 10 -3.0389983654022217 11 -2.9971787929534912 12 -2.8672068119049072 13 -2.6637454032897949
		 14 -2.41744065284729 15 -2.1592600345611572 16 -1.9193810224533081 17 -1.7261402606964111
		 18 -1.5391188859939575 19 -1.3043843507766724 20 -1.0293186902999878 21 -0.7236664891242981
		 22 -0.40096777677536011 23 -0.079594522714614868 24 0.21724671125411987 25 0.46248465776443487
		 26 0.61897671222686768 27 0.69048881530761719 28 0.71283000707626343 29 0.72269994020462036
		 30 0.75488060712814331 31 0.84138917922973633 32 0.94605231285095204 33 1.0699548721313477
		 34 1.2997938394546509 35 1.72020423412323 36 3.589502096176147 37 3.4550631046295166
		 38 2.6980421543121338 39 1.529725193977356 40 0.61107629537582397 41 0.59572029113769531
		 42 0.57146722078323364 43 0.8281402587890625 44 1.8813199996948242 45 3.1519427299499512
		 46 4.6251678466796875 47 5.4549469947814941 48 6.0455126762390137;
createNode animCurveTU -n "Character1_Spine_visibility";
	rename -uid "D73DE5CE-47D0-91B5-E70B-7C86FD53DD3D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_Spine1_translateX";
	rename -uid "7193F7D7-444C-1593-196A-8591E37852ED";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.930038452148437;
createNode animCurveTL -n "Character1_Spine1_translateY";
	rename -uid "295E3A91-4BE8-57C7-CE46-2688D2529611";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1661763191223145;
createNode animCurveTL -n "Character1_Spine1_translateZ";
	rename -uid "A7450739-4260-DCF8-32DC-FA8E1B5A9DAE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.40121376514434814;
createNode animCurveTU -n "Character1_Spine1_scaleX";
	rename -uid "1AD43D2A-4F3A-F800-160E-C580F4BE2F3E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_Spine1_scaleY";
	rename -uid "477A5292-49EB-ECAB-8AA0-DE88345D16FB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine1_scaleZ";
	rename -uid "3F5E93AB-4C11-CBDB-0856-C59AA9EBE60C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_Spine1_rotateX";
	rename -uid "BB48AF8E-4359-540E-B8FD-07A830582A83";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -24.408208847045898 1 -22.034282684326172
		 2 -19.461273193359375 3 -16.840530395507812 4 -14.314322471618652 5 -12.015434265136719
		 6 -10.058993339538574 7 -8.5451440811157227 8 -7.6107783317565909 9 -7.308560848236084
		 10 -7.4077863693237296 11 -7.8721036911010742 12 -8.6041936874389648 13 -9.506648063659668
		 14 -10.478124618530273 15 -11.410983085632324 16 -12.192312240600586 17 -12.706852912902832
		 18 -12.964031219482422 19 -13.076458930969238 20 -13.085899353027344 21 -13.033757209777832
		 22 -12.959805488586426 23 -12.902527809143066 24 -12.900880813598633 25 -12.997286796569824
		 26 -13.267611503601074 27 -13.707442283630371 28 -14.243254661560059 29 -14.79258918762207
		 30 -15.258582115173338 31 -15.530345916748047 32 -15.470746994018555 33 -15.121773719787598
		 34 -14.637862205505369 35 -14.174074172973633 36 -13.516671180725098 37 -12.168160438537598
		 38 -13.00229549407959 39 -18.629993438720703 40 -25.45463752746582 41 -29.110496520996094
		 42 -29.692312240600582 43 -29.432634353637699 44 -28.670520782470703 45 -27.612958908081055
		 46 -26.329187393188477 47 -25.244941711425781 48 -24.408208847045898;
createNode animCurveTA -n "Character1_Spine1_rotateY";
	rename -uid "14DF8F69-43EC-06F2-23C3-CDBA69B86D99";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -1.5507655143737793 1 -1.7295945882797241
		 2 -1.856215238571167 3 -1.8995794057846069 4 -1.8424985408782961 5 -1.6807210445404053
		 6 -1.5584255456924438 7 -1.3933265209197998 8 -1.1088871955871582 9 -0.87017661333084106
		 10 -0.88096112012863159 11 -0.95052850246429432 12 -1.0343263149261475 13 -1.0851469039916992
		 14 -1.0574942827224731 15 -0.91237133741378784 16 -0.61737066507339478 17 -0.14460264146327972
		 18 0.57790654897689819 19 1.5538674592971802 20 2.7085227966308594 21 3.9662761688232422
		 22 5.2493462562561035 23 6.4774847030639648 24 7.5685648918151847 25 8.4400634765625
		 26 9.0487556457519531 27 9.4418163299560547 28 9.6762828826904297 29 9.8103876113891602
		 30 9.901118278503418 31 10.003764152526855 32 9.9858598709106445 33 9.8231649398803711
		 34 9.7222013473510742 35 9.8656749725341797 36 11.252218246459961 37 11.65251350402832
		 38 11.640753746032715 39 10.58507251739502 40 7.6464204788208008 41 4.637444019317627
		 42 3.1207103729248047 43 2.070340633392334 44 1.2142757177352905 45 0.4797398149967193
		 46 -0.15731853246688843 47 -0.80369818210601807 48 -1.5507655143737793;
createNode animCurveTA -n "Character1_Spine1_rotateZ";
	rename -uid "853A0F8C-4AB2-EA78-C05E-17BCA78EBC6D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 24.73478889465332 1 23.1905517578125 2 21.451192855834961
		 3 19.610311508178711 4 17.764541625976563 5 16.007226943969727 6 14.631379127502443
		 7 13.593744277954102 8 12.80423641204834 9 12.365643501281738 10 12.593684196472168
		 11 13.114336967468262 12 13.856800079345703 13 14.741868019104006 14 15.684525489807129
		 15 16.601095199584961 16 17.410942077636719 17 18.037860870361328 18 18.472230911254883
		 19 18.79694938659668 20 19.077581405639648 21 19.376693725585938 22 19.752040863037109
		 23 20.255498886108398 24 20.932832717895508 25 21.824169158935547 26 23.094804763793945
		 27 24.755813598632812 28 26.616558074951172 29 28.492807388305664 30 30.207023620605465
		 31 31.587564468383786 32 32.207225799560547 33 32.247428894042969 34 32.449718475341797
		 35 33.556571960449219 36 40.619926452636719 37 42.356212615966797 38 40.848602294921875
		 39 35.107418060302734 40 28.010765075683594 41 23.927040100097656 42 22.729856491088867
		 43 22.343515396118164 44 22.626607894897461 45 23.26850700378418 46 24.126787185668945
		 47 24.583093643188477 48 24.73478889465332;
createNode animCurveTU -n "Character1_Spine1_visibility";
	rename -uid "BE2CAD3D-4D32-EA89-F314-58B62A3B3D99";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftShoulder_translateX";
	rename -uid "4F3B9461-4C8D-071E-65F1-2C87DF2DB4FC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.0842323303222656;
createNode animCurveTL -n "Character1_LeftShoulder_translateY";
	rename -uid "44E88098-4A14-D7D1-C1F3-25B18ED82915";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.604855537414551;
createNode animCurveTL -n "Character1_LeftShoulder_translateZ";
	rename -uid "02A44172-420C-E1DE-FAAF-2392B6A19BDE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6404602527618408;
createNode animCurveTU -n "Character1_LeftShoulder_scaleX";
	rename -uid "CFFAFE7C-4563-6B50-768A-C4AA6E9239E1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftShoulder_scaleY";
	rename -uid "34A75C73-4EBA-A5C2-32DD-6AA981F32C0A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftShoulder_scaleZ";
	rename -uid "960E1A31-4353-9B77-D040-A3BBC6BC471C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftShoulder_rotateX";
	rename -uid "63A0787B-4528-8846-5824-C5AC8B39F95E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 38 ".ktv[0:37]"  0 4.2116961479187012 1 0.51629352569580078
		 2 -3.35235595703125 3 -7.2801909446716309 4 -11.143265724182129 5 -14.813523292541504
		 6 -18.164993286132812 7 -21.0787353515625 8 -23.445751190185547 9 -25.355335235595703
		 10 -26.958398818969727 11 -28.260801315307617 12 -29.2821044921875 13 -30.050107955932614
		 14 -30.596443176269531 15 -30.953109741210941 16 -31.149656295776367 17 -31.210788726806641
		 18 -31.210788726806641 30 -31.210788726806641 31 -31.210788726806641 32 -30.916399002075192
		 33 -29.800107955932617 34 -27.068332672119141 35 -21.259965896606445 36 1.784837007522583
		 37 -12.38299560546875 38 -22.530801773071289 39 -19.746496200561523 40 -11.27751350402832
		 41 -3.7473204135894771 42 -0.81270790100097656 43 1.1126431226730347 44 2.2787020206451416
		 45 2.9470961093902588 46 3.35835862159729 47 3.7204887866973873 48 4.2116961479187012;
createNode animCurveTA -n "Character1_LeftShoulder_rotateY";
	rename -uid "06B6154E-42E6-815E-43AC-D1AD96A0DA48";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 38 ".ktv[0:37]"  0 1.283650279045105 1 3.5824844837188725
		 2 5.8170557022094727 3 7.8817625045776376 4 9.7035923004150391 5 11.238919258117676
		 6 12.466146469116211 7 13.375536918640137 8 13.958026885986328 9 14.230083465576172
		 10 14.274499893188477 11 14.164498329162598 12 13.959425926208496 13 13.70728588104248
		 14 13.448073387145996 15 13.217479705810547 16 13.050616264343262 17 12.985587120056152
		 18 12.985587120056152 30 12.985587120056152 31 12.985587120056152 32 15.305202484130859
		 33 20.777565002441406 34 27.387470245361328 35 32.879688262939453 36 24.106958389282227
		 37 22.58094596862793 38 21.855424880981445 39 22.470191955566406 40 23.252511978149414
		 41 21.917707443237305 42 19.391046524047852 43 16.565933227539063 44 13.580300331115723
		 45 10.514890670776367 46 7.4202899932861337 47 4.3335838317871094 48 1.283650279045105;
createNode animCurveTA -n "Character1_LeftShoulder_rotateZ";
	rename -uid "B3DB0AA1-4C29-90E5-9320-32A9327B6AFB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 38 ".ktv[0:37]"  0 27.496171951293945 1 21.766929626464844
		 2 15.565505981445314 3 9.0723152160644531 4 2.4925403594970703 5 -3.948835134506226
		 6 -10.01414966583252 7 -15.46066474914551 8 -20.047317504882812 9 -23.92381477355957
		 10 -27.380502700805664 11 -30.399217605590817 12 -32.968902587890625 13 -35.082759857177734
		 14 -36.735973358154297 15 -37.924098968505859 16 -38.641944885253906 17 -38.882949829101563
		 18 -38.882949829101563 30 -38.882949829101563 31 -38.882949829101563 32 -37.65777587890625
		 33 -34.194618225097656 34 -28.404251098632812 35 -19.225805282592773 36 15.025790214538572
		 37 -5.4613585472106934 38 -22.951738357543945 39 -14.177430152893066 40 3.1882693767547607
		 41 15.489828109741211 42 19.707832336425781 43 22.356754302978516 44 23.912403106689453
		 45 24.81578254699707 46 25.47026252746582 47 26.247880935668945 48 27.496171951293945;
createNode animCurveTU -n "Character1_LeftShoulder_visibility";
	rename -uid "A5E0AC71-47B6-BBA1-A65D-C39869D12A81";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftArm_translateX";
	rename -uid "AF98534B-4E14-EC5D-0592-2599FFF45007";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 9.2092180252075195;
createNode animCurveTL -n "Character1_LeftArm_translateY";
	rename -uid "EB630635-4818-ED4A-06D6-5DA9058A7C17";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.4172050952911377;
createNode animCurveTL -n "Character1_LeftArm_translateZ";
	rename -uid "5B79BCC4-4C11-0411-DE49-2D9E6010455D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.69030004739761353;
createNode animCurveTU -n "Character1_LeftArm_scaleX";
	rename -uid "6508E1EE-42BE-5B19-4DAE-B4A933685FEB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleY";
	rename -uid "7247E809-41B8-C587-8F7F-B0A0001D3723";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleZ";
	rename -uid "5C1C6D3A-44E2-AF85-EBB9-87BF89176209";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_LeftArm_rotateX";
	rename -uid "4FD2270F-45BC-332B-9011-B7A0A814084B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -31.409425735473629 1 -26.202449798583984
		 2 -21.061559677124023 3 -16.106878280639648 4 -11.470194816589355 5 -7.2761020660400391
		 6 -3.6247601509094238 7 -0.57987815141677856 8 1.8380886316299441 9 3.7055592536926274
		 10 5.0841693878173828 11 6.1847405433654785 12 7.2914452552795419 13 8.2137508392333984
		 14 8.5703935623168945 15 7.91473388671875 16 6.0381979942321777 17 3.7553410530090332
		 18 2.668494701385498 19 1.6332190036773682 20 0.78005015850067139 21 0.22648091614246368
		 22 0.033942773938179016 23 0.19052416086196899 24 0.62244796752929688 25 1.2235522270202637
		 26 2.0565145015716553 27 3.1415705680847168 28 11.582948684692383 29 16.12725830078125
		 30 19.031681060791016 31 20.186393737792969 32 19.374433517456055 33 16.349878311157227
		 34 10.504505157470703 35 1.7339882850646973 36 -16.836322784423828 37 18.960390090942383
		 38 37.202751159667969 39 42.923858642578125 40 23.061288833618164 41 6.3731942176818848
		 42 -0.91979080438613892 43 -7.0251150131225586 44 -12.168505668640137 45 -16.709041595458984
		 46 -21.086601257324219 47 -25.8011474609375 48 -31.409425735473629;
createNode animCurveTA -n "Character1_LeftArm_rotateY";
	rename -uid "BB4DD774-441D-B29C-2717-429CA185894E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -29.958406448364258 1 -28.855268478393555
		 2 -27.555778503417969 3 -25.936788558959961 4 -23.919649124145508 5 -21.470611572265625
		 6 -18.593965530395508 7 -15.319624900817869 8 -11.688082695007324 9 -7.3428711891174308
		 10 -2.1342554092407227 11 3.5872206687927242 12 9.4700164794921875 13 14.884952545166016
		 14 19.278890609741211 15 22.251060485839844 16 23.654266357421875 17 23.820003509521484
		 18 23.340347290039063 19 21.811185836791992 20 19.417123794555664 21 16.397714614868164
		 22 13.078271865844727 23 9.8625326156616211 24 7.1969618797302255 25 5.5319023132324219
		 26 5.1526875495910645 27 5.8507518768310547 28 10.10163402557373 29 13.280575752258301
		 30 15.727975845336912 31 16.881641387939453 32 17.334911346435547 33 17.353715896606445
		 34 16.040153503417969 35 12.161828994750977 36 -9.1237907409667969 37 -0.6560702919960022
		 38 17.963607788085937 39 15.295298576354982 40 2.0649144649505615 41 -5.5998406410217285
		 42 -9.6373348236083984 43 -13.315728187561035 44 -16.791152954101563 45 -20.150213241577148
		 46 -23.445192337036133 47 -26.708906173706055 48 -29.958406448364258;
createNode animCurveTA -n "Character1_LeftArm_rotateZ";
	rename -uid "7F5D6F9D-4AB6-29F1-E985-ADA69CB780A2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -24.564958572387695 1 -28.718904495239258
		 2 -32.999500274658203 3 -37.281623840332031 4 -41.416824340820313 5 -45.253944396972656
		 6 -48.655036926269531 7 -51.502525329589844 8 -53.696697235107422 9 -55.128730773925781
		 10 -55.758682250976562 11 -55.705127716064453 12 -55.128814697265625 13 -54.227359771728516
		 14 -53.186111450195313 15 -52.207904815673828 16 -51.523757934570313 17 -51.564662933349609
		 18 -52.616340637207031 19 -53.988376617431641 20 -55.560287475585938 21 -57.202686309814453
		 22 -58.806015014648438 23 -60.299316406250007 24 -61.656902313232422 25 -62.891540527343757
		 26 -64.280815124511719 27 -65.968002319335938 28 -69.2186279296875 29 -71.142684936523437
		 30 -72.0638427734375 31 -71.717926025390625 32 -69.67401123046875 33 -65.755760192871094
		 34 -60.305389404296868 35 -53.9979248046875 36 -39.564624786376953 37 -53.282810211181641
		 38 -66.016448974609375 39 -60.07933044433593 40 -47.556972503662109 41 -39.463069915771484
		 42 -36.607654571533203 43 -34.420261383056641 44 -32.602119445800781 45 -30.916509628295898
		 46 -29.154640197753906 47 -27.110099792480469 48 -24.564958572387695;
createNode animCurveTU -n "Character1_LeftArm_visibility";
	rename -uid "10C91AED-425B-83BB-210B-46A02D742EE0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftForeArm_translateX";
	rename -uid "6CE8A5BA-4CC1-DAB4-5823-7D970369005F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.0427684783935547;
createNode animCurveTL -n "Character1_LeftForeArm_translateY";
	rename -uid "DC410BEF-4664-CF0D-FB79-90A47E53D810";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.323208808898926;
createNode animCurveTL -n "Character1_LeftForeArm_translateZ";
	rename -uid "77EEF0E6-4891-A7AD-A83E-D59CF551C499";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 16.678167343139648;
createNode animCurveTU -n "Character1_LeftForeArm_scaleX";
	rename -uid "FE984E2B-43BB-7AC8-3E6F-A395C52DA057";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftForeArm_scaleY";
	rename -uid "7F3439E0-4CF0-2BE0-CA51-AFB6F10AD859";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftForeArm_scaleZ";
	rename -uid "C3337537-4D6E-D3D6-D140-C090C9736841";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftForeArm_rotateX";
	rename -uid "EA203134-4B41-F9E0-1F88-808E18345DBF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -6.1113152503967285 1 -3.9326348304748535
		 2 -1.6752959489822388 3 0.47123956680297846 4 2.3831984996795654 5 3.9882626533508301
		 6 5.245335578918457 7 6.1262617111206055 8 6.6014776229858398 9 6.6942825317382812
		 10 6.6109766960144043 11 6.6303315162658691 12 7.0379533767700195 13 8.0841903686523437
		 14 9.8673086166381836 15 12.224586486816406 16 14.69312572479248 17 16.271549224853516
		 18 15.77892017364502 19 14.470723152160646 20 12.777167320251465 21 11.059541702270508
		 22 9.5566158294677734 23 8.3865289688110352 24 7.5815010070800781 25 7.1350512504577637
		 26 6.9995050430297852 27 7.0608830451965332 28 -0.70193570852279663 29 -5.0369539260864258
		 30 -7.2460651397705087 31 -6.6723089218139648 32 -7.4660577774047852 33 -10.927921295166016
		 34 -12.477103233337402 35 -8.9752416610717773 36 5.0194082260131836 37 -6.7816081047058105
		 38 -18.339666366577148 39 -28.995820999145511 40 -22.549062728881836 41 -17.946027755737305
		 42 -16.662872314453125 43 -15.133285522460939 44 -13.427754402160645 45 -11.617681503295898
		 46 -9.7660961151123047 47 -7.9209556579589853 48 -6.1113152503967285;
createNode animCurveTA -n "Character1_LeftForeArm_rotateY";
	rename -uid "4990CF0E-42C1-E95D-636C-73AF4E78CEB2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -43.348838806152344 1 -41.623542785644531
		 2 -39.402507781982422 3 -36.895774841308594 4 -34.349391937255859 5 -32.036243438720703
		 6 -30.24519157409668 7 -29.271026611328129 8 -29.405706405639652 9 -31.019132614135742
		 10 -33.9979248046875 11 -37.736824035644531 12 -41.508014678955078 13 -44.672721862792969
		 14 -46.708152770996094 15 -47.279121398925781 16 -46.329315185546875 17 -44.845512390136719
		 18 -44.124813079833984 19 -42.049594879150391 20 -38.944065093994141 21 -35.154247283935547
		 22 -31.087789535522464 23 -27.206016540527344 24 -23.995229721069336 25 -21.945381164550781
		 26 -21.2745361328125 27 -21.581411361694336 28 -31.361377716064453 29 -35.432823181152344
		 30 -36.806526184082031 31 -35.122894287109375 32 -34.60577392578125 33 -36.427402496337891
		 34 -36.692398071289063 35 -33.558231353759766 36 -8.4162826538085937 37 -12.246835708618164
		 38 -23.532426834106445 39 -25.387523651123047 40 -32.714271545410156 41 -37.791187286376953
		 42 -39.607883453369141 43 -40.870079040527344 44 -41.698379516601563 45 -42.220500946044922
		 46 -42.572475433349609 47 -42.898334503173828 48 -43.348838806152344;
createNode animCurveTA -n "Character1_LeftForeArm_rotateZ";
	rename -uid "61867F41-49CA-B168-EF49-458A19D5E4D5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 7.8022584915161124 1 3.7827427387237544
		 2 -0.51817381381988525 3 -4.7564325332641602 4 -8.6722259521484375 5 -12.075202941894531
		 6 -14.819319725036619 7 -16.775518417358398 8 -17.804512023925781 9 -17.866415023803711
		 10 -17.337867736816406 11 -16.786331176757813 12 -16.834175109863281 13 -18.076780319213867
		 14 -20.893465042114258 15 -25.271614074707031 16 -30.656141281127926 17 -34.739856719970703
		 18 -34.054450988769531 19 -32.191242218017578 20 -29.664220809936527 21 -26.918176651000977
		 22 -24.289522171020508 23 -22.015510559082031 24 -20.270467758178711 25 -19.209091186523438
		 26 -18.869565963745117 27 -19.024425506591797 28 -2.8314623832702637 29 5.3713340759277344
		 30 9.3983163833618164 31 8.3089027404785156 32 9.7128934860229492 33 15.882988929748535
		 34 18.595239639282227 35 12.369668960571289 36 -12.366657257080078 37 9.3329286575317383
		 38 28.421245574951172 39 46.953441619873047 40 35.943809509277344 41 28.095666885375977
		 42 25.928432464599609 43 23.330493927001953 44 20.421648025512695 45 17.320093154907227
		 46 14.130917549133301 47 10.938376426696777 48 7.8022584915161124;
createNode animCurveTU -n "Character1_LeftForeArm_visibility";
	rename -uid "35CCF96B-448E-D81F-E1B1-17A6BE2DED21";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHand_translateX";
	rename -uid "28C32816-4790-F296-9F06-3DA73C5CF428";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.29200950264930725;
createNode animCurveTL -n "Character1_LeftHand_translateY";
	rename -uid "46228041-4B4C-F460-72B0-808C45B14E62";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -14.295157432556152;
createNode animCurveTL -n "Character1_LeftHand_translateZ";
	rename -uid "15219A21-4513-D4A7-C310-A6B05FD76B6D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6319241523742676;
createNode animCurveTU -n "Character1_LeftHand_scaleX";
	rename -uid "B688B862-4CA3-1A14-DE23-DC9DF94F6EE2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHand_scaleY";
	rename -uid "B451DAC6-4BDD-23B3-B567-FFB76E81084B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHand_scaleZ";
	rename -uid "C77B78EC-41FD-CF4A-58E5-16A8D8E40CE1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHand_rotateX";
	rename -uid "D63BFF4F-4F6D-A49C-CFFD-7D983093B55A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -7.935187339782714 1 -6.4722428321838379
		 2 -5.0391092300415039 3 -3.792518138885498 4 -2.8210577964782715 5 -2.1461021900177002
		 6 -1.7362455129623413 7 -1.532172679901123 8 -1.4777138233184814 9 -1.4479526281356812
		 10 -1.4193941354751587 11 -1.4919980764389038 12 -1.6900570392608643 13 -1.9642970561981199
		 14 -2.307776927947998 15 -2.7549324035644531 16 -3.3459081649780273 17 -4.1225056648254395
		 18 -5.1868419647216797 19 -6.4347658157348633 20 -7.8040213584899893 21 -9.220555305480957
		 22 -10.609549522399902 23 -11.905253410339355 24 -13.043516159057617 25 -13.943765640258789
		 26 -14.566391944885254 27 -14.97034168243408 28 -16.020505905151367 29 -16.323017120361328
		 30 -16.340421676635742 31 -16.144145965576172 32 -16.089023590087891 33 -16.400876998901367
		 34 -16.664752960205078 35 -16.297527313232422 36 -14.057761192321777 37 -16.3900146484375
		 38 -14.615525245666504 39 -11.369979858398437 40 -2.7695205211639404 41 2.832758903503418
		 42 3.3982844352722168 43 2.6647000312805176 44 0.98635268211364757 45 -1.2498717308044434
		 46 -3.677083015441895 47 -5.9854450225830078 48 -7.935187339782714;
createNode animCurveTA -n "Character1_LeftHand_rotateY";
	rename -uid "71E41010-4B57-CF21-50DB-EC9FA03AD6C8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -2.9392001628875732 1 -4.4363489151000977
		 2 -6.1162991523742676 3 -7.8292169570922852 4 -9.4303455352783203 5 -10.798428535461426
		 6 -11.847856521606445 7 -12.527013778686523 8 -12.799174308776855 9 -12.557476997375488
		 10 -11.848752975463867 11 -10.754921913146973 12 -9.4319868087768555 13 -8.1896734237670898
		 14 -7.453557014465332 15 -7.4956002235412607 16 -8.2975139617919922 17 -9.3256921768188477
		 18 -9.8879842758178711 19 -11.129863739013672 20 -13.152642250061035 21 -15.930983543395998
		 22 -19.253761291503906 23 -22.741479873657227 24 -25.933788299560547 25 -28.384391784667969
		 26 -29.924722671508789 27 -30.761247634887699 28 -30.191596984863278 29 -29.451805114746094
		 30 -28.841930389404297 31 -28.791255950927731 32 -28.395261764526367 33 -27.169633865356445
		 34 -25.874948501586914 35 -25.346778869628906 36 -28.850547790527344 37 -32.854831695556641
		 38 -28.939239501953125 39 -20.738210678100586 40 -9.2097511291503906 41 -3.1143631935119629
		 42 -1.4271067380905151 43 -0.63430905342102051 44 -0.54735946655273438 45 -0.99145734310150146
		 46 -1.7448711395263672 47 -2.5140483379364014 48 -2.9392001628875732;
createNode animCurveTA -n "Character1_LeftHand_rotateZ";
	rename -uid "10466EED-4732-74A0-581F-449417A3680D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -15.827025413513182 1 -12.126811027526855
		 2 -7.9079632759094238 3 -3.4493813514709473 4 0.96035754680633556 5 5.0166635513305664
		 6 8.3978290557861328 7 10.770496368408203 8 11.799638748168945 9 11.311886787414551
		 10 9.5816450119018555 11 6.9324445724487305 12 3.9517683982849121 13 1.4173332452774048
		 14 -0.11911266297101974 15 -0.54971212148666382 16 -0.14007647335529327 17 0.20093566179275513
		 18 -0.76035982370376587 19 -1.5083990097045898 20 -1.9706655740737917 21 -2.0245749950408936
		 22 -1.5907906293869019 23 -0.7326127290725708 24 0.29803839325904846 25 1.0966885089874268
		 26 1.6101868152618408 27 2.0600643157958984 28 -6.4247102737426758 29 -10.093320846557617
		 30 -11.515082359313965 31 -10.436973571777344 32 -11.489154815673828 33 -16.075647354125977
		 34 -19.649600982666016 35 -19.245452880859375 36 1.3879462480545044 37 1.504887580871582
		 38 -0.84663426876068115 39 -0.70851075649261475 40 5.7966899871826172 41 8.2268543243408203
		 42 6.3913860321044922 43 3.5640599727630615 44 0.0060007856227457523 45 -3.9950368404388428
		 46 -8.1489629745483398 47 -12.179216384887695 48 -15.827025413513182;
createNode animCurveTU -n "Character1_LeftHand_visibility";
	rename -uid "B7564092-47D1-E123-D299-9BA46AF3CE17";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateX";
	rename -uid "3237B948-4C31-D4AB-1FF5-A291339A9176";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1364259719848633;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateY";
	rename -uid "7A02AE32-4137-6842-8E95-28A6855A9A49";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4241994619369507;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateZ";
	rename -uid "9A27B5C3-4B39-6090-443E-75AA61CCCB4B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.7211661338806152;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleX";
	rename -uid "52E4C0BD-45FC-28C0-6995-25A8FE8E78AC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleY";
	rename -uid "6ED64D04-45EE-5954-612F-168F3B319A98";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleZ";
	rename -uid "A2944A5B-4936-AD00-9399-A18821ABF2F6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateX";
	rename -uid "4B3CBBE8-4544-61F8-20F6-0194E5DA2A2F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -63.744144439697259;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateY";
	rename -uid "2BB9D2CF-4BFD-193B-3974-4694255EDFAA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 37.257007598876953;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateZ";
	rename -uid "136E2CBC-4916-6687-174F-B394569BDED6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -21.798364639282227;
createNode animCurveTU -n "Character1_LeftHandThumb1_visibility";
	rename -uid "508B4585-4FC9-4BAC-F104-16B8B95251F3";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateX";
	rename -uid "EC866849-4D19-96BF-90C7-CB8AD0280A5F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.8329257965087891;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateY";
	rename -uid "9D370B63-425D-816B-669D-FEB4BAEA96BC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.094426639378070831;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateZ";
	rename -uid "D4A5365C-4A62-36BE-C18A-B39F9CBD8042";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5438374280929565;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleX";
	rename -uid "F3D0DA0F-4E98-3E58-F6E4-F392A4845025";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleY";
	rename -uid "34E98B5D-4763-ACBC-B49E-269486CDFF56";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleZ";
	rename -uid "0DB2298C-4A03-A893-9330-45928B7D32AB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateX";
	rename -uid "B0E432EC-4BB4-7BEC-3309-A696B8F7217C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.355228424072266;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateY";
	rename -uid "976F3255-478E-0352-8A22-DEAF187F8E5C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 20.610536575317383;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateZ";
	rename -uid "A83751D2-4E6A-AFF7-CCAA-899A93ABA032";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.5256719589233398;
createNode animCurveTU -n "Character1_LeftHandThumb2_visibility";
	rename -uid "97EC04A5-43B6-A852-78EA-569728704BFD";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateX";
	rename -uid "7F2DC9B7-4C5C-45B8-56C5-D38E1C7B05CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7747817039489746;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateY";
	rename -uid "99D98FF2-4F1E-6109-6CB3-079EBEB0CE15";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3182404041290283;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateZ";
	rename -uid "4F5E0595-46B7-8A9A-1FF6-04AC09C0F961";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.26324462890625;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleX";
	rename -uid "45AB0B61-4B21-7B80-C0C6-459486492CC3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleY";
	rename -uid "DFDBEEE0-4C38-5117-6087-B8A8F137CCB9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleZ";
	rename -uid "6FE4CF2E-4780-75D7-13C4-F2B206A4CC72";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateX";
	rename -uid "1C153F5B-4C05-D4C3-9F89-E38501E1CCAB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.3174436465706094e-007;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateY";
	rename -uid "528731E6-423A-7847-473D-A9B7C2F3EE06";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.5699219463604095e-007;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateZ";
	rename -uid "D6F5CD9C-46C9-DD79-B364-F390F75A6FAC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.5132016528696113e-007;
createNode animCurveTU -n "Character1_LeftHandThumb3_visibility";
	rename -uid "24E1064F-4FB2-795E-D484-6B89B1A49D73";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateX";
	rename -uid "29710001-472A-506E-2083-59B36E3773CD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.854037284851074;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateY";
	rename -uid "CF653E8F-4894-BED2-14D7-1D90C51502C5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2616198062896729;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateZ";
	rename -uid "1CA91209-4E92-D5E1-516F-86858CCCD92D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.944699764251709;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleX";
	rename -uid "F4AA93A2-4570-E9D8-E7A3-7C9F1B485291";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleY";
	rename -uid "93DF41FD-47F2-CD5D-6787-1EAD1D84A25B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleZ";
	rename -uid "9BF60348-4B91-8F59-9761-B0BEEBA33145";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateX";
	rename -uid "A6AA7EEC-4EA0-534C-9B19-F9945BC66777";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  4 22.74671745300293 5 22.74671745300293
		 6 22.370712280273438 7 21.445632934570312 8 20.287017822265625 9 19.144834518432617
		 10 18.167781829833984 11 17.425115585327148 12 16.950143814086914 13 16.779275894165039
		 14 16.779275894165039 34 16.779275894165039 35 16.779275894165039 36 17.89222526550293
		 37 20.681686401367188 38 22.74671745300293 39 22.74671745300293;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateY";
	rename -uid "A282A752-458B-F464-627E-90AA0BFEA5A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  4 16.568584442138672 5 16.568584442138672
		 6 15.435307502746582 7 12.381068229675293 8 7.9108905792236328 9 2.5908262729644775
		 10 -2.9127459526062012 11 -7.8453526496887198 12 -11.400459289550781 13 -12.762218475341797
		 14 -12.762218475341797 34 -12.762218475341797 35 -12.762218475341797 36 -4.6595458984375
		 37 9.5253019332885742 38 16.568584442138672 39 16.568584442138672;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateZ";
	rename -uid "BCB18E8A-47B3-3E04-6905-CE990748B78E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  4 34.237068176269531 5 34.237068176269531
		 6 33.1383056640625 7 30.337860107421875 8 26.600831985473633 9 22.605587005615234
		 10 18.883125305175781 11 15.832900047302244 12 13.775307655334473 13 13.015031814575195
		 14 13.015031814575195 34 13.015031814575195 35 13.015031814575195 36 17.77482795715332
		 37 27.905843734741211 38 34.237068176269531 39 34.237068176269531;
createNode animCurveTU -n "Character1_LeftHandIndex1_visibility";
	rename -uid "E673DCF1-4EB7-E8E6-7530-F992645A4FBD";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateX";
	rename -uid "C72BE890-4D70-6447-CE7E-2F9855060247";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.1721835136413574;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateY";
	rename -uid "218EF2AB-41F0-02F6-B854-4CB7DB1AF26C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.9652466773986816;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateZ";
	rename -uid "56759FD5-452B-2A71-4A09-B0BF8835B297";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.8871781826019287;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleX";
	rename -uid "4BEF745D-4FB2-E5A7-1CB4-61A4A2228631";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleY";
	rename -uid "3E74FE9B-4894-E283-6361-FF9A3859BA4A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleZ";
	rename -uid "FDFE5B69-484E-99D9-ED72-8594702674F1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateX";
	rename -uid "FB845E4A-42ED-1191-2695-9582E4482964";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  4 9.6709728240966797 5 9.6709728240966797
		 6 9.3391494750976563 7 8.3027477264404297 8 6.4556441307067871 9 3.8257801532745366
		 10 0.69986253976821899 11 -2.3821048736572266 12 -3.7743461132049561 13 -3.777083158493042
		 34 -3.7770833969116211 35 -3.7770836353301998 36 -0.36463740468025208 37 7.1644163131713876
		 38 9.6709728240966797 39 9.6709728240966797;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateY";
	rename -uid "79EFCA27-433E-F610-C3EC-7D880340E516";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  4 -13.632480621337891 5 -13.632480621337891
		 6 -12.676430702209473 7 -10.21839714050293 8 -6.9332528114318848 9 -3.5130054950714111
		 10 -0.54687094688415527 11 1.6049946546554565 12 2.4169106483459473 13 2.4529671669006348
		 14 2.4529671669006348 34 2.4529671669006348 35 2.4529671669006348 36 0.27051430940628052
		 37 -8.0767049789428711 38 -13.632480621337891 39 -13.632480621337891;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateZ";
	rename -uid "07CC6248-4D41-CD93-06A2-92B5B80605CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  4 35.310218811035156 5 35.310218811035156
		 6 33.229053497314453 7 27.748115539550781 8 19.982458114624023 9 11.002978324890137
		 10 1.8947306871414182 11 -6.1749711036682129 12 -9.608393669128418 13 -9.6000280380249023
		 34 -9.6000280380249023 35 -9.6000280380249023 36 -0.9710773229598999 37 22.758255004882812
		 38 35.310218811035156 39 35.310218811035156;
createNode animCurveTU -n "Character1_LeftHandIndex2_visibility";
	rename -uid "C0E84522-48F6-E40F-444A-AFAA72DB5F77";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateX";
	rename -uid "76A0B16F-4555-DB91-EF05-ADAB7D240D1D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.5761528015136719;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateY";
	rename -uid "CF2C3C5D-48AC-EA68-3E87-1F8C1A940FEC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8397388458251953;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateZ";
	rename -uid "30078A41-4C2D-9DEA-BEEC-48BED6115CD5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3069263696670532;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleX";
	rename -uid "172C7045-447C-B018-B5DB-91B328037409";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleY";
	rename -uid "464ADBBD-4357-9220-BE7F-C783AA0229E5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleZ";
	rename -uid "EFC6A115-42C7-D42C-93BF-83A6B121BF17";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateX";
	rename -uid "2B8BFC9A-4A68-F924-CC44-1D93A88263EB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.4839381985184446e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateY";
	rename -uid "4C20CA38-4DFF-C65E-50B4-79B5B3486F9B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.5610335658020631e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateZ";
	rename -uid "B9E99047-4959-3AA2-A21C-22B6BB5ECF82";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.5122693488083314e-007;
createNode animCurveTU -n "Character1_LeftHandIndex3_visibility";
	rename -uid "54493060-4B42-427F-EBD8-0C9F4BB02EE1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandRing1_translateX";
	rename -uid "5F1D2970-4419-1458-EF7E-E097D6BF7508";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4602031707763672;
createNode animCurveTL -n "Character1_LeftHandRing1_translateY";
	rename -uid "FD9D2305-4A66-1A6C-84D3-3E8F11659444";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.5015654563903809;
createNode animCurveTL -n "Character1_LeftHandRing1_translateZ";
	rename -uid "0F1F8525-4730-42BC-B0F6-1087C2DC2045";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.6178884506225586;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleX";
	rename -uid "F55A788A-494D-3420-E4BF-4889794E6603";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleY";
	rename -uid "67AC26D5-402C-A77C-4327-73801B80465F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleZ";
	rename -uid "D0DA4DF8-456F-DAC3-6B2E-B6B0B8BADB60";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateX";
	rename -uid "697A6F2E-449D-E1FE-095D-B99318D4588E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.5265388488769531;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateY";
	rename -uid "C19D0FAB-4824-4563-F4E6-959B3865CAE1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 20.450643539428711;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateZ";
	rename -uid "BB98DA34-4539-928A-A827-D9B90237BAE7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.765336990356445;
createNode animCurveTU -n "Character1_LeftHandRing1_visibility";
	rename -uid "C5A22EEA-4D1E-4EEF-FF9E-C3A79E43A62A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandRing2_translateX";
	rename -uid "0827E437-48D5-D062-17DB-709DA40CF60A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.9237353801727295;
createNode animCurveTL -n "Character1_LeftHandRing2_translateY";
	rename -uid "7AA3D929-4F55-9997-FD53-C9B0B8333332";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.0094923973083496;
createNode animCurveTL -n "Character1_LeftHandRing2_translateZ";
	rename -uid "CA42FB4B-4BEB-6DF8-DA5E-D49FC7B7652D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8221585750579834;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleX";
	rename -uid "1DC1E900-4277-CE7C-CF6C-FEB85A83AF14";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleY";
	rename -uid "64A1F13A-4E36-48AB-3636-229C47B8B5C6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleZ";
	rename -uid "2FD89F18-4977-2193-5B50-3AAF78D19E79";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateX";
	rename -uid "8A4C51DF-4EF6-746F-46ED-DF8D4AF1D7E7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 28.991849899291996;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateY";
	rename -uid "A76289D7-4C7B-12D6-6D06-679BE6DEEFFF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.019344329833984;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateZ";
	rename -uid "54FCB0AB-473E-5988-EE99-F89ACF039774";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 38.54791259765625;
createNode animCurveTU -n "Character1_LeftHandRing2_visibility";
	rename -uid "F1BFF2B8-478D-4BE1-A453-D09102F7F62E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandRing3_translateX";
	rename -uid "D8881DDD-48C5-A722-4E68-86BEBD6A39B2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6098947525024414;
createNode animCurveTL -n "Character1_LeftHandRing3_translateY";
	rename -uid "98BCB23F-4B6F-E8AC-BD6F-A6A7CD929E8C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2952473163604736;
createNode animCurveTL -n "Character1_LeftHandRing3_translateZ";
	rename -uid "18A1A49B-49B7-A48B-34DA-64BF94D42FCD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3554067611694336;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleX";
	rename -uid "11FD01CE-4DAE-2E5F-5F6C-70BD9E4813EB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleY";
	rename -uid "27751AE8-474A-09A8-FEDB-E794599CE8FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleZ";
	rename -uid "48C6475E-4ADA-E7BB-6267-BCB7EE87F275";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateX";
	rename -uid "775EE57D-4FC6-FE37-B3B8-74BF220D416E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.978530631025933e-007;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateY";
	rename -uid "2346DE5C-466E-6F70-894A-40BEA9CADC2A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.7217617899805191e-007;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateZ";
	rename -uid "472ADB5E-412C-CC5A-D7D3-81BCE2A0C9D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3620073957554268e-007;
createNode animCurveTU -n "Character1_LeftHandRing3_visibility";
	rename -uid "2C59D733-4F56-B85D-BE3F-55A692A0FA4B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightShoulder_translateX";
	rename -uid "996D0A8B-4DCB-7732-6D83-55A8F8A3B243";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 14.332768440246582;
createNode animCurveTL -n "Character1_RightShoulder_translateY";
	rename -uid "18E41AC8-4C1E-806C-E929-348F43A04F9D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5798094272613525;
createNode animCurveTL -n "Character1_RightShoulder_translateZ";
	rename -uid "47100099-4F16-F1D7-DFC2-05BB448F93DD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1086856126785278;
createNode animCurveTU -n "Character1_RightShoulder_scaleX";
	rename -uid "3329E70F-4FFD-0CC3-7E1D-0BB92375AA92";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightShoulder_scaleY";
	rename -uid "C13F5F16-4E75-D778-7C6B-839C26E365AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightShoulder_scaleZ";
	rename -uid "B088BC69-4F55-8479-313D-AAADDDF4649E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightShoulder_rotateX";
	rename -uid "7071EEB5-4127-8A53-3EEB-B6977EFBFA72";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 48 ".ktv[0:47]"  0 -1.9563722610473633 1 -1.1418486833572388
		 2 -0.32354295253753662 3 0.51570373773574829 4 1.3717811107635498 5 2.293079137802124
		 6 3.3399684429168701 7 4.568690299987793 8 6.0141029357910156 9 7.9246802330017081
		 10 9.9923248291015625 11 12.324154853820801 12 13.540658950805664 13 13.540658950805664
		 14 13.540658950805664 15 14.768322944641113 16 16.471706390380859 17 16.76069450378418
		 18 16.569021224975586 19 16.579462051391602 20 16.587095260620117 21 16.589048385620117
		 22 16.60345458984375 23 16.678726196289063 24 16.849065780639648 25 17.046258926391602
		 26 16.967586517333984 27 15.951371192932131 28 13.781479835510254 29 10.899951934814453
		 30 8.2449464797973633 31 7.0800681114196777 32 7.0800681114196777 34 7.0800681114196777
		 35 7.0800681114196777 36 4.418738842010498 37 5.0415816307067871 38 5.6859264373779297
		 39 5.1619620323181152 40 4.0218448638916016 41 2.7443695068359375 42 1.9420080184936523
		 43 1.2910189628601074 44 0.7152019739151001 45 0.13503171503543854 46 -0.5006556510925293
		 47 -1.2045899629592896 48 -1.9563722610473633;
createNode animCurveTA -n "Character1_RightShoulder_rotateY";
	rename -uid "E5F5C981-4931-4C60-7796-6FAD1F0CA145";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 48 ".ktv[0:47]"  0 -18.153192520141602 1 -13.99653148651123
		 2 -9.9746026992797852 3 -5.9630355834960938 4 -1.7583825588226318 5 2.3779377937316895
		 6 6.1767063140869141 7 9.3653717041015625 8 11.673331260681152 9 10.130706787109375
		 10 5.3769640922546387 11 -1.9008215665817261 12 -6.8305807113647461 13 -6.8305807113647461
		 14 -6.8305807113647461 15 -12.697445869445801 16 -24.691165924072266 17 -34.583614349365234
		 18 -38.419929504394531 19 -39.391910552978516 20 -40.204517364501953 21 -41.274467468261719
		 22 -42.1368408203125 23 -42.332538604736328 24 -41.399982452392578 25 -38.859981536865234
		 26 -33.299129486083984 27 -24.736621856689453 28 -14.93655490875244 29 -5.7553977966308594
		 30 0.99181711673736572 31 3.6077232360839848 32 3.6077232360839848 34 3.6077232360839848
		 35 3.6077232360839848 36 0.95475929975509655 37 -1.7335504293441772 38 -5.995537281036377
		 39 -14.44963550567627 40 -23.613224029541016 41 -28.504858016967773 42 -28.99941444396973
		 43 -28.319730758666992 44 -26.769025802612305 45 -24.652091979980469 46 -22.286190032958984
		 47 -20.00432014465332 48 -18.153192520141602;
createNode animCurveTA -n "Character1_RightShoulder_rotateZ";
	rename -uid "4CBEE4B2-408F-ACCB-678B-38B0C2A21D3B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 48 ".ktv[0:47]"  0 9.5608348846435547 1 10.641449928283691
		 2 11.920672416687012 3 13.365023612976074 4 14.959891319274902 5 16.756189346313477
		 6 18.766605377197266 7 20.96375846862793 8 23.278013229370117 9 25.368991851806641
		 10 27.108442306518555 11 29.099046707153324 12 30.307746887207031 13 30.307746887207031
		 14 30.307746887207031 15 31.843490600585934 16 35.677349090576172 17 40.034530639648438
		 18 42.202304840087891 19 42.78851318359375 20 43.287006378173828 21 43.998134613037109
		 22 44.585964202880859 23 44.686782836914063 24 43.949451446533203 25 42.113353729248047
		 26 38.531524658203125 27 33.745746612548828 28 28.841217041015621 29 24.414815902709961
		 30 21.078212738037109 31 19.733858108520508 32 19.733858108520508 34 19.733858108520508
		 35 19.733858108520508 36 14.967135429382322 37 15.865829467773437 38 16.758609771728516
		 39 15.695274353027346 40 15.14121723175049 41 14.995229721069336 42 14.457370758056639
		 43 13.713790893554687 44 12.846841812133789 45 11.950307846069336 46 11.091283798217773
		 47 10.29708194732666 48 9.5608348846435547;
createNode animCurveTU -n "Character1_RightShoulder_visibility";
	rename -uid "617B1FDE-40B6-A0AA-8EB8-469D0D40040B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightArm_translateX";
	rename -uid "A1C88AAC-47F3-2DB4-542D-CEB59F3961FD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.8354010581970215;
createNode animCurveTL -n "Character1_RightArm_translateY";
	rename -uid "C77EB80E-41C2-DBDC-C9A2-76B068BF41CA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1704782247543335;
createNode animCurveTL -n "Character1_RightArm_translateZ";
	rename -uid "46C226B1-43B3-FFC9-57CF-79822396C369";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.3259291648864746;
createNode animCurveTU -n "Character1_RightArm_scaleX";
	rename -uid "40EE32B2-4D45-EF84-5D85-E3BDD2E0ECB5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightArm_scaleY";
	rename -uid "0087F244-4E5E-26CE-FB29-0A8F9EAB389C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightArm_scaleZ";
	rename -uid "13C6C409-4A22-E7DE-6804-09831DC0D5F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightArm_rotateX";
	rename -uid "787AEA39-426D-621B-BA5A-39818A80D00C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 9.2251605987548828 1 -32.320846557617187
		 2 -73.367767333984375 3 -96.183845520019531 4 -109.2139892578125 5 -117.54381561279298
		 6 -123.30942535400391 7 -127.27519989013672 8 -129.52847290039063 9 -126.20408630371095
		 10 -123.7586669921875 11 -114.07289123535156 12 -100.36222839355469 13 -128.98751831054687
		 14 -152.87968444824219 15 -138.57484436035156 16 -145.53948974609375 17 -147.431640625
		 18 -149.14447021484375 19 -154.87004089355469 20 -158.17849731445312 21 -161.63980102539062
		 22 -165.05020141601562 23 -167.97203063964844 24 -169.90728759765625 25 -171.13371276855469
		 26 -172.53366088867187 27 -176.49139404296875 28 -182.28082275390625 29 -184.89335632324219
		 30 -181.98158264160156 31 -177.62925720214844 32 -173.57424926757813 33 -169.88972473144531
		 34 -168.81138610839844 35 -171.78341674804687 36 -182.19734191894531 37 -174.91357421875
		 38 -175.56752014160156 39 -187.00949096679687 40 -215.547119140625 41 -236.05482482910156
		 42 -237.70648193359375 43 -234.21943664550781 44 -225.74984741210937 45 -212.9395751953125
		 46 -197.71031188964844 47 -182.94486999511719 48 -170.77484130859375;
createNode animCurveTA -n "Character1_RightArm_rotateY";
	rename -uid "CEA5F7D6-4831-9F4D-A8EF-0184E5D1FF62";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 59.891941070556641 1 66.298011779785156
		 2 60.38916015625 3 47.093284606933594 4 30.801265716552738 5 13.807790756225586 6 -2.5822572708129883
		 7 -17.415752410888672 8 -29.877092361450195 9 -31.01319694519043 10 -20.607601165771484
		 11 -5.7253866195678711 12 10.128940582275391 13 46.666465759277344 14 64.367378234863281
		 15 79.928863525390625 16 110.95972442626953 17 132.51387023925781 18 138.20196533203125
		 19 137.81718444824219 20 136.14816284179687 21 132.74491882324219 22 128.87673950195312
		 23 125.74288940429689 24 124.4898147583008 25 126.31591796875 26 132.24737548828125
		 27 141.31314086914062 28 151.64700317382812 29 161.91226196289062 30 170.58087158203125
		 31 176.61749267578125 32 179.38359069824219 33 179.26177978515625 34 177.59585571289062
		 35 175.86160278320312 36 166.74253845214844 37 161.59196472167969 38 154.3311767578125
		 39 139.29098510742187 40 130.46534729003906 41 130.64775085449219 42 127.40534973144531
		 43 124.00032043457033 44 120.83841705322266 45 118.62779235839844 46 117.93841552734376
		 47 118.69559478759766 48 120.10806274414062;
createNode animCurveTA -n "Character1_RightArm_rotateZ";
	rename -uid "7F3309AE-438B-2841-9E7C-FE8E98129972";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 34.769691467285156 1 -4.8259634971618652
		 2 -45.658458709716797 3 -69.695243835449219 4 -85.345626831054688 5 -97.375335693359375
		 6 -107.54155731201172 7 -116.32717132568358 8 -123.78670501708984 9 -123.94416046142578
		 10 -118.67855072021484 11 -115.05149078369141 12 -109.18387603759766 13 -129.70831298828125
		 14 -167.99430847167969 15 -175.00617980957031 16 -181.83262634277344 17 -185.28204345703125
		 18 -184.25314331054687 19 -180.91658020019531 20 -177.65937805175781 21 -175.01994323730469
		 22 -172.76545715332031 23 -170.349609375 24 -167.12348937988281 25 -163.26654052734375
		 26 -160.21818542480469 27 -160.56295776367187 28 -164.0084228515625 29 -168.69842529296875
		 30 -173.66975402832031 31 -177.75593566894531 32 -181.65737915039062 33 -185.98934936523437
		 34 -189.04415893554687 35 -189.31376647949219 36 -194.42657470703125 37 -190.62266540527344
		 38 -175.00997924804687 39 -166.60580444335937 40 -181.56236267089844 41 -193.02003479003906
		 42 -193.87347412109375 43 -191.62860107421875 44 -185.85728454589844 45 -176.64720153808594
		 46 -165.38221740722656 47 -154.39590454101562 48 -145.23031616210937;
createNode animCurveTU -n "Character1_RightArm_visibility";
	rename -uid "ACA84855-4A5A-F269-2813-BBA7DEFE1930";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightForeArm_translateX";
	rename -uid "27B2910D-465F-CB8B-5B64-C79958D0D595";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.753132820129395;
createNode animCurveTL -n "Character1_RightForeArm_translateY";
	rename -uid "B1103D9D-47D7-347F-BAB8-CDAB35C6B936";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.862128257751465;
createNode animCurveTL -n "Character1_RightForeArm_translateZ";
	rename -uid "3D1DE9BE-422B-C5E8-E283-CB90F78A2055";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.671875953674316;
createNode animCurveTU -n "Character1_RightForeArm_scaleX";
	rename -uid "C117C0E6-4DE1-DDDB-98F8-C1A528A176A1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightForeArm_scaleY";
	rename -uid "B57E85E7-4601-0C94-E7A4-D59C135BA7CE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightForeArm_scaleZ";
	rename -uid "E3B97FB8-4A2F-7AE0-F63A-F9ABD116430A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightForeArm_rotateX";
	rename -uid "17705360-4CD7-78C5-0829-4CA84DC339CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 44.704814910888672 1 40.589397430419922
		 2 29.341327667236328 3 9.504572868347168 4 -5.7628922462463379 5 -15.780532836914063
		 6 -22.375177383422852 7 -26.983745574951172 8 -30.315780639648434 9 -33.715553283691406
		 10 -33.25927734375 11 -18.968473434448242 12 -5.3318681716918945 13 -10.40822696685791
		 14 -1.3760240077972412 15 -2.1367924213409424 16 -3.5185937881469727 17 -4.1694574356079102
		 18 -0.50759696960449219 19 6.1509213447570801 20 9.5218877792358398 21 10.164995193481445
		 22 7.6213312149047843 23 2.3949909210205078 24 -5.0065927505493164 25 -14.230258941650391
		 26 -24.613300323486328 27 -32.232486724853516 28 -34.358890533447266 29 -30.641151428222656
		 30 -22.870630264282227 31 -15.117390632629395 32 -10.893712997436523 33 -9.3924903869628906
		 34 -9.6361989974975586 35 -10.555344581604004 36 18.354406356811523 37 17.497522354125977
		 38 23.070833206176758 39 24.130975723266602 40 17.448753356933594 41 18.44366455078125
		 42 18.443756103515625 43 19.266881942749023 44 21.036571502685547 45 23.973363876342773
		 46 28.450960159301758 47 35.070625305175781 48 44.704814910888672;
createNode animCurveTA -n "Character1_RightForeArm_rotateY";
	rename -uid "133D22AA-4DD5-0FFA-C581-59802F41DD48";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 59.860408782958977 1 61.20173645019532
		 2 61.178489685058594 3 55.807868957519531 4 45.777259826660156 5 32.889240264892578
		 6 19.163627624511719 7 6.373314380645752 8 -3.8977806568145752 9 5.9761643409729004
		 10 16.873153686523438 11 3.5589540004730225 12 -20.387823104858398 13 -1.8100014925003052
		 14 -10.407628059387207 15 -20.73411750793457 16 0.63632220029830933 17 15.236027717590332
		 18 29.246562957763668 19 45.293266296386719 20 51.894554138183594 21 54.528480529785156
		 22 54.753173828125 23 53.591342926025391 24 51.569900512695313 25 48.580207824707031
		 26 42.432926177978516 27 32.03533935546875 28 21.225942611694336 29 13.333703994750977
		 30 7.0941133499145508 31 -0.11856254935264589 32 -10.451119422912598 33 -16.186511993408203
		 34 -16.854433059692383 35 -14.171200752258301 36 -50.856739044189453 37 -26.986274719238281
		 38 2.3152930736541748 39 11.023617744445801 40 7.700937271118165 41 18.022274017333984
		 42 24.112266540527344 43 30.370988845825192 44 36.693881988525391 45 42.97540283203125
		 46 49.083965301513672 47 54.8218994140625 48 59.860408782958977;
createNode animCurveTA -n "Character1_RightForeArm_rotateZ";
	rename -uid "6CA89F97-4181-44E9-1B96-68AEBE899890";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 25.916189193725586 1 16.020252227783203
		 2 -1.0339177846908569 3 -26.957550048828125 4 -47.989276885986328 5 -62.890144348144538
		 6 -73.089790344238281 7 -79.835708618164062 8 -83.949432373046875 9 -89.525665283203125
		 10 -92.047348022460938 11 -70.716278076171875 12 -54.837139129638672 13 -67.532661437988281
		 14 -37.133510589599609 15 -8.9649658203125 16 -24.310764312744141 17 -25.407060623168945
		 18 -20.047109603881836 19 -18.312894821166992 20 -18.959999084472656 21 -21.148918151855469
		 22 -26.48820686340332 23 -35.430191040039063 24 -48.050811767578125 25 -63.938674926757813
		 26 -81.464668273925781 27 -93.657066345214844 28 -95.525115966796875 29 -87.111747741699219
		 30 -71.931938171386719 31 -56.048328399658203 32 -47.244937896728516 33 -44.739788055419922
		 34 -45.753009796142578 35 -48.380729675292969 36 -59.750717163085938 37 -41.701759338378906
		 38 -40.052413940429688 39 -33.834815979003906 40 -21.7628173828125 41 -17.059307098388672
		 42 -12.290613174438477 43 -7.8193774223327637 44 -3.2980742454528809 45 1.689731240272522
		 46 7.6852111816406259 47 15.43073844909668 48 25.916189193725586;
createNode animCurveTU -n "Character1_RightForeArm_visibility";
	rename -uid "4EB9B561-4291-7110-A739-8595F1CB975F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHand_translateX";
	rename -uid "529AD943-45F6-11E5-043C-F69DBC5ADF0B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7167695760726929;
createNode animCurveTL -n "Character1_RightHand_translateY";
	rename -uid "023B1080-4CB0-135B-C5F5-18B90C9DE745";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.314067840576172;
createNode animCurveTL -n "Character1_RightHand_translateZ";
	rename -uid "D5F586A4-4880-5236-F9EC-1BBF9663BB2A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4444055557250977;
createNode animCurveTU -n "Character1_RightHand_scaleX";
	rename -uid "7021F2C1-40AF-F2EF-C880-50B246EFC457";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightHand_scaleY";
	rename -uid "3F7C788D-42AF-7407-0AF1-56874E09A52E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightHand_scaleZ";
	rename -uid "C94774EC-4BC0-097B-0B70-7B9084C5541D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightHand_rotateX";
	rename -uid "5CD057D8-4641-C424-8B3F-A3AE2D7C177A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -26.301816940307617 1 -36.881370544433594
		 2 -40.278587341308594 3 -38.280017852783203 4 -33.528762817382812 5 -24.710102081298828
		 6 -11.971611022949219 7 2.7828369140625 8 16.563417434692383 9 24.814802169799805
		 10 31.220516204833984 11 21.409816741943359 12 17.340829849243164 13 10.493882179260254
		 14 -14.597798347473143 15 -24.813713073730469 16 -18.175056457519531 17 -13.499396324157715
		 18 -6.4129753112792969 19 -0.056709613651037216 20 -0.060873184353113167 21 -2.6710364818572998
		 22 -7.1205778121948242 23 -12.461733818054199 24 -17.362977981567383 25 -20.490554809570312
		 26 -21.815793991088867 27 -23.275960922241211 28 -27.993095397949219 29 -35.540542602539062
		 30 -42.378154754638672 31 -47.66943359375 32 -46.690620422363281 33 -38.895797729492188
		 34 -36.755203247070313 35 -58.394268035888665 36 65.373313903808594 37 38.233245849609375
		 38 44.142913818359375 39 41.931591033935547 40 37.363903045654297 41 32.313468933105469
		 42 22.69331169128418 43 12.514862060546875 44 2.4371602535247803 45 -6.8507471084594727
		 46 -14.831957817077637 47 -21.289751052856445 48 -26.301816940307617;
createNode animCurveTA -n "Character1_RightHand_rotateY";
	rename -uid "BA4D3147-4213-F5CF-1728-CEB3DBE9D907";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -18.719133377075195 1 -27.85228157043457
		 2 -32.266750335693359 3 -25.33949089050293 4 -16.292278289794922 5 -7.3219666481018066
		 6 -0.56919914484024048 7 3.0586252212524414 8 4.5371341705322266 9 5.2815957069396973
		 10 10.741105079650879 11 10.26336669921875 12 -2.5142595767974854 13 -7.8057398796081534
		 14 -6.3512945175170898 15 -11.511711120605469 16 -7.0621991157531738 17 -5.319976806640625
		 18 -5.549781322479248 19 -4.2883281707763672 20 -4.7159900665283203 21 -6.3723897933959961
		 22 -8.117457389831543 23 -8.7606468200683594 24 -7.5842351913452157 25 -4.9902305603027344
		 26 -2.0008087158203125 27 -0.77864766120910645 28 -4.2079529762268066 29 -13.258747100830078
		 30 -25.623804092407227 31 -37.476131439208984 32 -39.730129241943359 33 -36.708660125732422
		 34 -34.169273376464844 35 -39.649257659912109 36 13.819316864013672 37 10.13763427734375
		 38 12.81641674041748 39 11.390457153320313 40 9.973114013671875 41 10.142300605773926
		 42 10.287602424621582 43 9.3611288070678711 44 6.8000144958496094 45 2.39634108543396
		 46 -3.6654229164123535 47 -10.908879280090332 48 -18.719133377075195;
createNode animCurveTA -n "Character1_RightHand_rotateZ";
	rename -uid "866FAF4E-4470-0D93-7873-4F9236F2603A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -20.889553070068359 1 -27.185016632080078
		 2 -30.779224395751953 3 -28.058557510375977 4 -23.516101837158203 5 -17.758193969726563
		 6 -11.723895072937012 7 -7.1736783981323242 8 -5.9241042137145996 9 -11.19233512878418
		 10 -25.061935424804688 11 -59.97954177856446 12 -78.423568725585938 13 -25.669956207275391
		 14 3.0039196014404297 15 -6.8224949836730957 16 -3.4098362922668457 17 -3.6612477302551274
		 18 -12.255093574523926 19 -18.876058578491211 20 -19.146274566650391 21 -18.081823348999023
		 22 -14.961268424987793 23 -9.5737361907958984 24 -2.8231935501098633 25 3.2748501300811768
		 26 7.3641133308410653 27 7.2793855667114258 28 0.86172330379486084 29 -9.393336296081543
		 30 -19.620784759521484 31 -27.307367324829102 32 -25.251178741455078 33 -18.963539123535156
		 34 -15.94261646270752 35 -20.530912399291992 36 25.306236267089844 37 5.9786725044250488
		 38 -0.93328016996383656 39 0.95964056253433228 40 6.5708017349243164 41 10.106892585754395
		 42 7.2102875709533691 43 2.7876901626586914 44 -2.5516672134399414 45 -8.0809497833251953
		 46 -13.18715763092041 47 -17.504844665527344 48 -20.889553070068359;
createNode animCurveTU -n "Character1_RightHand_visibility";
	rename -uid "F26A281E-4807-86BA-7849-829E4CCB7E9D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandThumb1_translateX";
	rename -uid "691CF58E-4CA5-E65A-B6C0-F88FB62211C7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5741705894470215;
createNode animCurveTL -n "Character1_RightHandThumb1_translateY";
	rename -uid "055C0E0F-40D0-B102-DF91-E89E6DA63160";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4327812194824219;
createNode animCurveTL -n "Character1_RightHandThumb1_translateZ";
	rename -uid "A0A76602-4CE0-2405-BB43-38A1AAB1FF91";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8196568489074707;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleX";
	rename -uid "9ACB8158-4A14-F232-81A2-329DBFE24EAE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleY";
	rename -uid "AD3CC38D-4B4E-CA3D-FD72-439309964E21";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleZ";
	rename -uid "7A71CC19-41EC-D32B-80ED-399B5991CA69";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateX";
	rename -uid "3B521C50-40BD-1664-697F-5280AE53293C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 30 ".ktv[0:29]"  0 38.973472595214844 1 47.72698974609375
		 2 49.613059997558594 3 37.202236175537109 4 11.890069961547852 5 -3.915340900421143
		 6 6.0526247024536133 7 27.5206298828125 8 38.973472595214844 9 38.973472595214844
		 14 38.973472595214844 15 38.973472595214844 16 28.132061004638672 17 16.310888290405273
		 18 16.310888290405273 34 16.310888290405273 35 16.310888290405273 36 -3.915340900421143
		 37 -3.915340900421143 38 -3.915340900421143 39 -3.0960872173309326 40 -0.8088533878326416
		 41 2.6917028427124023 42 7.149564266204834 43 12.303740501403809 44 17.885555267333984
		 45 23.618717193603516 46 29.223897933959964 47 34.427772521972656 48 38.973472595214844;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateY";
	rename -uid "849C9B83-4DAA-61A7-4FFD-5D8C479D79C6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 30 ".ktv[0:29]"  0 27.536184310913086 1 23.489368438720703
		 2 20.283533096313477 3 28.208908081054688 4 32.326026916503906 5 28.567089080810547
		 6 27.916057586669922 7 28.410083770751953 8 27.536184310913086 9 27.536184310913086
		 14 27.536184310913086 15 27.536184310913086 16 4.5205378532409668 17 -15.881375312805176
		 18 -15.881375312805176 34 -15.881375312805176 35 -15.881375312805176 36 28.567089080810547
		 37 28.567089080810547 38 28.567089080810547 39 28.592536926269531 40 28.661643981933594
		 41 28.758392333984375 42 28.856327056884766 43 28.917364120483398 44 28.896738052368161
		 45 28.754045486450199 46 28.467414855957031 47 28.046028137207031 48 27.536184310913086;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateZ";
	rename -uid "3F2F5FCD-41D8-EC9C-B8D8-3FAB0549DC65";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 30 ".ktv[0:29]"  0 -17.683155059814453 1 5.3640580177307129
		 2 17.336156845092773 3 5.3535332679748535 4 -17.775945663452148 5 -31.846538543701172
		 6 -31.654714584350586 7 -23.585903167724609 8 -17.683155059814453 9 -17.683155059814453
		 14 -17.683155059814453 15 -17.683155059814453 16 -23.541444778442383 17 -27.851932525634766
		 18 -27.851932525634766 34 -27.851932525634766 35 -27.851932525634766 36 -31.846538543701172
		 37 -31.846538543701172 38 -31.846538543701172 39 -31.675045013427738 40 -31.175142288208008
		 41 -30.349584579467773 42 -29.192232131958008 43 -27.7071533203125 44 -25.924737930297852
		 45 -23.911998748779297 46 -21.774194717407227 47 -19.647127151489258 48 -17.683155059814453;
createNode animCurveTU -n "Character1_RightHandThumb1_visibility";
	rename -uid "CDFE7C86-4F68-2CB3-85E1-E39763941BFE";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandThumb2_translateX";
	rename -uid "C8C79EA1-4172-70E7-417F-1E90BD7C0C20";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7422792911529541;
createNode animCurveTL -n "Character1_RightHandThumb2_translateY";
	rename -uid "795E5837-4FF6-0C84-DFB4-519126C9EA16";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5767025947570801;
createNode animCurveTL -n "Character1_RightHandThumb2_translateZ";
	rename -uid "95B73714-4B38-F163-517D-F3AC0DF44BDA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0428388118743896;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleX";
	rename -uid "1206F834-4489-CDC0-381C-A199AB25A8D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleY";
	rename -uid "DD6F0FA7-46FB-D20A-ACCA-EEB7259A31D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleZ";
	rename -uid "43B02CD3-4B18-5CF7-7E39-878B8284D5D1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateX";
	rename -uid "42516692-498D-8C34-F26B-C2BDF67E585D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 3.8401813507080074 1 19.886165618896484
		 2 29.229799270629883 3 19.94183349609375 4 4.2135844230651855 5 -7.3096966743469238
		 6 -6.2271628379821777 7 0.21373364329338074 8 3.8401813507080074 9 3.8401813507080074
		 34 3.8401815891265874 35 3.8401815891265874 36 -7.3096966743469238 37 -7.3096966743469238
		 38 -7.3096966743469238 39 -7.073636531829834 40 -6.4208831787109375 41 -5.4392085075378418
		 42 -4.2177181243896484 43 -2.8420889377593994 44 -1.3915401697158813 45 0.062517300248146057
		 46 1.4563976526260376 47 2.733025074005127 48 3.8401813507080074;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateY";
	rename -uid "C2457968-4541-8E5A-E997-B394A5E64E89";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 4.2117342948913574 1 -12.114825248718262
		 2 -20.597293853759766 3 -10.050971984863281 4 7.0437908172607422 5 17.647163391113281
		 6 16.013660430908203 7 8.7254142761230469 8 4.2117342948913574 9 4.2117342948913574
		 34 4.2117342948913574 35 4.2117342948913574 36 17.647163391113281 37 17.647163391113281
		 38 17.647163391113281 39 17.406684875488281 40 16.732334136962891 41 15.691919326782227
		 42 14.352498054504395 43 12.783303260803223 44 11.057534217834473 45 9.2530899047851562
		 46 7.4524936676025391 47 5.742225170135498 48 4.2117342948913574;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateZ";
	rename -uid "0E3FD6BB-4B3E-341E-974E-DF8B140F447A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  0 2.4896588325500488 1 4.5838971138000488
		 2 3.6678612232208248 3 4.0016741752624512 4 1.116187572479248 5 -3.3688983917236328
		 6 -2.4114744663238525 7 0.92320466041564953 8 2.4896585941314697 9 2.4896588325500488
		 34 2.4896585941314697 35 2.4896585941314697 36 -3.3688983917236328 37 -3.3688983917236328
		 38 -3.3688983917236328 39 -3.2308998107910156 40 -2.8519573211669922 41 -2.2896182537078857
		 42 -1.6031574010848999 43 -0.84869682788848877 44 -0.075939469039440155 45 0.67354470491409302
		 46 1.3666983842849731 47 1.9783226251602173 48 2.4896588325500488;
createNode animCurveTU -n "Character1_RightHandThumb2_visibility";
	rename -uid "58886200-4541-1AAC-114A-09ACB2C25EFC";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandThumb3_translateX";
	rename -uid "C8E82718-4F67-7159-F91C-1392BEBC1609";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.588839054107666;
createNode animCurveTL -n "Character1_RightHandThumb3_translateY";
	rename -uid "3CDECBE4-4AE1-08B7-EF1E-21910FDE464F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.13479287922382355;
createNode animCurveTL -n "Character1_RightHandThumb3_translateZ";
	rename -uid "1BA58DE8-4AE3-5FC3-AFCC-2F9B2544E84E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.4058942794799805;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleX";
	rename -uid "29D9EDA7-4D4C-E431-2C8E-039B01918EDD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleY";
	rename -uid "AFDD1501-466D-EF52-4997-0CAF693F4C85";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleZ";
	rename -uid "97D8F356-4491-EEF8-DD5D-71BE4469B099";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateX";
	rename -uid "FC870C02-4360-6A5E-306A-84AA781B8141";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.2610313256736845e-008;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateY";
	rename -uid "9F22E1F2-4605-1D20-EF37-CCADFEFD62B4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.2768423226298182e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateZ";
	rename -uid "8B271274-4494-959A-2E60-77862B245ECD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.8074884167162963e-007;
createNode animCurveTU -n "Character1_RightHandThumb3_visibility";
	rename -uid "14C84417-4AFA-C88E-95B7-D5A6BB1C02D0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandIndex1_translateX";
	rename -uid "8BF6BC3D-47DA-5E95-6860-C0A36BC74AC9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.25625163316726685;
createNode animCurveTL -n "Character1_RightHandIndex1_translateY";
	rename -uid "834012BE-4E1E-D24D-18BD-8387764530A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.892666816711426;
createNode animCurveTL -n "Character1_RightHandIndex1_translateZ";
	rename -uid "308F4D36-4820-5F5D-6416-2FB12CCC38F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.533165454864502;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleX";
	rename -uid "08B0C12B-4A01-9783-4104-5B8C81B673F7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleY";
	rename -uid "A0DE3F9F-4A2D-7911-5BC8-0D9C91F4A4F7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleZ";
	rename -uid "7D907B62-4152-0D34-02EC-BF9C3BAF2E50";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateX";
	rename -uid "D0F10A9F-4D92-13B9-F110-EE89C88BA9D6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 30 ".ktv[0:29]"  0 -1.158916711807251 1 -2.244840145111084
		 2 -2.2581830024719238 3 -3.4806699752807617 4 -2.8339240550994873 5 -1.2266238927841187
		 6 -1.2188035249710083 7 -1.5399186611175537 8 -1.158916711807251 9 -1.1589168310165405
		 14 -1.1589168310165405 15 -1.1589168310165405 16 4.6901216506958008 17 10.573902130126953
		 18 10.573902130126953 34 10.573902130126953 35 10.573902130126953 36 -1.2266238927841187
		 37 -1.2266238927841187 38 -1.2266238927841187 39 -1.2560845613479614 40 -1.336890697479248
		 41 -1.4540959596633911 42 -1.5856829881668091 43 -1.7019258737564087 44 -1.7688416242599487
		 45 -1.7555813789367676 46 -1.6437808275222778 47 -1.4358081817626953 48 -1.158916711807251;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateY";
	rename -uid "E15D4B7D-4AE8-7308-CF39-E6B9911B8408";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 30 ".ktv[0:29]"  0 1.6353100538253784 1 5.1540794372558594
		 2 6.664154052734375 3 4.3525857925415039 4 3.3844189643859863 5 4.3339700698852539
		 6 2.7642257213592529 7 1.5106734037399292 8 1.6353100538253784 9 1.6353100538253784
		 14 1.6353100538253784 15 1.6353100538253784 16 13.725068092346191 17 25.812498092651367
		 18 25.812498092651367 34 25.812498092651367 35 25.812498092651367 36 4.3339700698852539
		 37 4.3339700698852539 38 4.3339700698852539 39 4.2068195343017578 40 3.869204044342041
		 41 3.4015955924987793 42 2.8905134201049805 43 2.4137144088745117 44 2.0287253856658936
		 45 1.766146183013916 46 1.6288083791732788 47 1.5966159105300903 48 1.6353100538253784;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateZ";
	rename -uid "F5336F01-4284-914C-BFBE-C8B019BC2F39";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 30 ".ktv[0:29]"  0 12.797025680541992 1 36.396530151367188
		 2 46.040653228759766 3 18.416175842285156 4 -21.768100738525391 5 -45.163234710693359
		 6 -32.722293853759766 7 -3.5711696147918701 8 12.797025680541992 9 12.797025680541992
		 14 12.797025680541992 15 12.797025680541992 16 2.9073851108551025 17 -7.1957440376281729
		 18 -7.1957440376281729 34 -7.1957440376281729 35 -7.1957440376281729 36 -45.163234710693359
		 37 -45.163234710693359 38 -45.163234710693359 39 -44.076488494873047 40 -41.041187286376953
		 41 -36.391681671142578 42 -30.461759567260742 43 -23.58820915222168 44 -16.112995147705078
		 45 -8.3836641311645508 46 -0.75225335359573364 47 6.4265346527099609 48 12.797025680541992;
createNode animCurveTU -n "Character1_RightHandIndex1_visibility";
	rename -uid "8F6A92E6-4C21-0F46-30FA-A9AD6B8B10CD";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandIndex2_translateX";
	rename -uid "19604CDD-4E6B-CA52-0208-FEB5AC5C2331";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.3959341049194336;
createNode animCurveTL -n "Character1_RightHandIndex2_translateY";
	rename -uid "F32A2B9D-436F-05A5-9580-0C903D43D313";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8172028064727783;
createNode animCurveTL -n "Character1_RightHandIndex2_translateZ";
	rename -uid "A7ADA2A3-4E7A-E631-4957-368D9843FC96";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.52152138948440552;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleX";
	rename -uid "A7424E43-4BD9-9741-77FF-9BB78154B32B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleY";
	rename -uid "0839EDBB-4F88-B85A-0AAB-0F952689EAB8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleZ";
	rename -uid "481559C7-4389-EE72-5266-3CB3C3DBFD58";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateX";
	rename -uid "00C1F95E-4A1C-FAF8-7E91-7D852B427143";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 32 ".ktv[0:31]"  0 11.788410186767578 1 19.305076599121094
		 2 21.766260147094727 3 16.140762329101563 4 2.5847475528717041 5 -5.9310169219970703
		 6 -5.1779575347900391 7 4.8739237785339355 8 11.788410186767578 9 14.02278995513916
		 10 14.44255256652832 11 14.44255256652832 14 14.44255256652832 15 14.44255256652832
		 16 19.591236114501953 17 23.064971923828125 18 23.064971923828125 34 23.064971923828125
		 35 23.064971923828125 36 -5.9310169219970703 37 -5.9310169219970703 38 -5.9310169219970703
		 39 -5.9133553504943848 40 -5.8664612770080566 41 -4.9995536804199219 42 -2.5518488883972168
		 43 0.13331411778926849 44 2.87213134765625 45 5.5111031532287598 46 7.9305233955383301
		 47 10.043008804321289 48 11.788410186767578;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateY";
	rename -uid "3B5EA478-4A6E-DAF6-3FE2-4798638B7107";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 32 ".ktv[0:31]"  0 -15.640760421752931 1 -31.758974075317383
		 2 -39.535037994384766 3 -23.827583312988281 4 -2.989647388458252 5 4.5945215225219727
		 6 4.3236913681030273 7 -5.548037052154541 8 -15.640760421752931 9 -19.705772399902344
		 10 -20.52461051940918 11 -20.52461051940918 14 -20.52461051940918 15 -20.52461051940918
		 16 -32.557155609130859 17 -45.151954650878906 18 -45.151954650878906 34 -45.151954650878906
		 35 -45.151954650878906 36 4.5945215225219727 37 4.5945215225219727 38 4.5945215225219727
		 39 4.620610237121582 40 4.6934099197387695 41 4.1257266998291016 42 2.1102874279022217
		 43 -0.37381851673126221 44 -3.239497184753418 45 -6.3647160530090332 46 -9.5954408645629883
		 47 -12.75263500213623 48 -15.640760421752931;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateZ";
	rename -uid "7ABC0C39-4E93-0C66-1599-52B48045DF8C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 32 ".ktv[0:31]"  0 15.306336402893066 1 27.419002532958984
		 2 32.875839233398437 3 21.391544342041016 4 2.4465630054473877 5 -7.724015235900878
		 6 -6.8155112266540527 7 5.7906055450439453 8 15.306336402893066 9 18.562168121337891
		 10 19.196533203125 11 19.196533203125 14 19.196533203125 15 19.196533203125 16 27.981464385986328
		 17 36.888134002685547 18 36.888134002685547 34 36.888134002685547 35 36.888134002685547
		 36 -7.724015235900878 37 -7.724015235900878 38 -7.724015235900878 39 -7.7187676429748535
		 40 -7.7020888328552246 41 -6.6929445266723633 42 -3.7303688526153569 43 -0.4179365336894989
		 44 3.0449681282043457 45 6.4852242469787598 46 9.7563743591308594 47 12.733755111694336
		 48 15.306336402893066;
createNode animCurveTU -n "Character1_RightHandIndex2_visibility";
	rename -uid "8F9D019B-4F6A-A594-81D7-BF84B16D41D8";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandIndex3_translateX";
	rename -uid "B5D7C95B-4618-FFEA-73D9-69AC1100D76C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6282989978790283;
createNode animCurveTL -n "Character1_RightHandIndex3_translateY";
	rename -uid "1C0D83D9-420B-F3C0-85E8-71A876D310FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.2120883464813232;
createNode animCurveTL -n "Character1_RightHandIndex3_translateZ";
	rename -uid "0C179F79-43F3-FEED-71BC-548DCFFDBE60";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7757487297058105;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleX";
	rename -uid "00D9FF7F-49B7-3144-E65B-0F8BD643EC2B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleY";
	rename -uid "DA27CF4F-4877-656B-A34B-9D8C08C5F68E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleZ";
	rename -uid "FCA16B4D-4A4E-F063-A07A-36A1548EFA80";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateX";
	rename -uid "8F5380B3-4309-085E-3626-23814716DB23";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.0945410496060504e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateY";
	rename -uid "3DEDF660-45FE-9EA7-25F4-3C811BBCA4E3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.7783982871105755e-009;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateZ";
	rename -uid "0F4C2E35-40ED-41DD-F573-9BBAD4BBB06A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.9543576001979091e-007;
createNode animCurveTU -n "Character1_RightHandIndex3_visibility";
	rename -uid "23A05573-4268-4564-0F3D-6B907159AD53";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandRing1_translateX";
	rename -uid "25D4BB51-42C2-F101-7FCA-77A289DC695E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8543868064880371;
createNode animCurveTL -n "Character1_RightHandRing1_translateY";
	rename -uid "38F78E06-4309-9C0D-623A-66998BF9BA33";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2156095504760742;
createNode animCurveTL -n "Character1_RightHandRing1_translateZ";
	rename -uid "86750053-4AFF-AC93-599D-D5B01D37CFFE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2236912250518799;
createNode animCurveTU -n "Character1_RightHandRing1_scaleX";
	rename -uid "54BBD2F5-465C-BCCC-1227-67BC84563B60";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandRing1_scaleY";
	rename -uid "CEE81F19-42E4-B8E2-3BC4-2FAE75DF466D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing1_scaleZ";
	rename -uid "75641AE5-4AE4-DC10-A5D2-528F11ED87E2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandRing1_rotateX";
	rename -uid "FFD0C271-47C6-EED6-ABBB-B8A6A8261F53";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 32 ".ktv[0:31]"  0 -9.2360963821411133 1 -9.7770891189575195
		 2 -10.030672073364258 3 -12.191829681396484 4 -10.459015846252441 5 -7.8650550842285165
		 6 -7.8826231956481934 7 -8.8791027069091797 8 -9.2360963821411133 9 -11.629571914672852
		 10 -13.840812683105469 11 -13.840812683105469 14 -13.840812683105469 15 -13.840812683105469
		 16 -18.606836318969727 17 -24.381340026855469 18 -24.381340026855469 34 -24.381340026855469
		 35 -24.381340026855469 36 -7.8650550842285165 37 -7.8650550842285165 38 -7.8650550842285165
		 39 -7.9315991401672363 40 -8.1180620193481445 41 -8.4006767272949219 42 -8.7433147430419922
		 43 -9.0941810607910156 44 -9.3914976119995117 45 -9.5767726898193359 46 -9.6108112335205078
		 47 -9.4867725372314453 48 -9.2360963821411133;
createNode animCurveTA -n "Character1_RightHandRing1_rotateY";
	rename -uid "60C549AF-4EF1-0FAB-68A3-A496FBE81D8D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 32 ".ktv[0:31]"  0 4.393496036529541 1 10.845949172973633
		 2 13.384522438049316 3 4.9955205917358398 4 -4.3612198829650879 5 -7.2120728492736808
		 6 -6.1566591262817383 7 -1.5446969270706177 8 4.393496036529541 9 7.7695627212524423
		 10 8.9757452011108398 11 8.9757452011108398 14 8.9757452011108398 15 8.9757452011108398
		 16 20.083803176879883 17 30.876806259155277 18 30.876806259155277 34 30.876806259155277
		 35 30.876806259155277 36 -7.2120728492736808 37 -7.2120728492736808 38 -7.2120728492736808
		 39 -7.1158404350280762 40 -6.8194808959960937 41 -6.2872176170349121 42 -5.4730539321899414
		 43 -4.3454318046569824 44 -2.9065723419189453 45 -1.2035411596298218 46 0.67095065116882324
		 47 2.5872578620910645 48 4.393496036529541;
createNode animCurveTA -n "Character1_RightHandRing1_rotateZ";
	rename -uid "12973810-4F95-E679-8451-FAAA150B81D6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 32 ".ktv[0:31]"  0 29.838140487670898 1 53.208637237548828
		 2 62.225337982177734 3 33.086139678955078 4 -9.3079147338867187 5 -34.570720672607422
		 6 -23.797918319702148 7 4.952110767364502 8 29.838140487670898 9 42.525135040283203
		 10 47.194232940673828 11 47.194232940673828 14 47.194232940673828 15 47.194232940673828
		 16 51.633525848388672 17 54.838859558105469 18 54.838859558105469 34 54.838859558105469
		 35 54.838859558105469 36 -34.570720672607422 37 -34.570720672607422 38 -34.570720672607422
		 39 -33.321086883544922 40 -29.838924407958981 41 -24.528997421264648 42 -17.801164627075195
		 43 -10.067181587219238 44 -1.7349234819412231 45 6.7982339859008789 46 15.149554252624513
		 47 22.949983596801758 48 29.838140487670898;
createNode animCurveTU -n "Character1_RightHandRing1_visibility";
	rename -uid "649FB24D-409D-F12B-9EBA-6B9616446860";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandRing2_translateX";
	rename -uid "E5C44F23-4914-6B70-2017-C895B73300E1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.062777042388916;
createNode animCurveTL -n "Character1_RightHandRing2_translateY";
	rename -uid "B82D87EE-40AA-E708-CB63-74AED3D5AE7D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.4930424690246582;
createNode animCurveTL -n "Character1_RightHandRing2_translateZ";
	rename -uid "824FEF5F-47DB-A834-2511-DDA1C4AADD13";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.86127841472625732;
createNode animCurveTU -n "Character1_RightHandRing2_scaleX";
	rename -uid "75B28059-4E67-4EA7-1E15-8A80060175FC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandRing2_scaleY";
	rename -uid "DF14A2DB-4AF0-0B99-2D50-2CB2B5B8981C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing2_scaleZ";
	rename -uid "3CFABB90-4E2F-FA5A-14AB-CBA2E9D3C10A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandRing2_rotateX";
	rename -uid "7D8943E5-417D-6E6C-0950-CBA7D78EB1DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 32 ".ktv[0:31]"  0 20.286947250366211 1 23.964622497558594
		 2 23.607002258300781 3 26.490240097045898 4 15.687537193298338 5 6.1718859672546387
		 6 6.6319079399108887 7 15.443471908569336 8 20.286947250366211 9 23.939481735229492
		 10 23.007652282714844 11 23.007652282714844 14 23.007652282714844 15 23.007652282714844
		 16 23.566007614135742 17 20.286947250366211 18 20.286947250366211 34 20.286947250366211
		 35 20.286947250366211 36 6.1718859672546387 37 6.1718859672546387 38 6.1718859672546387
		 39 6.3068523406982422 40 6.6266536712646484 41 7.8738923072814933 42 10.779221534729004
		 43 13.719089508056641 44 16.358030319213867 45 18.436838150024414 46 19.796375274658203
		 47 20.388696670532227 48 20.286947250366211;
createNode animCurveTA -n "Character1_RightHandRing2_rotateY";
	rename -uid "17ED2973-481D-0825-B12D-18863FDCDE76";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 32 ".ktv[0:31]"  0 -34.944370269775391 1 -53.168838500976563
		 2 -60.365306854248047 3 -38.073204040527344 4 -11.041223526000977 5 -5.3786287307739258
		 6 -3.5332093238830566 7 -16.8717041015625 8 -34.944370269775391 9 -52.699100494384766
		 10 -62.995777130126953 11 -62.995777130126953 14 -62.995777130126953 15 -62.995777130126953
		 16 -48.867397308349609 17 -34.944370269775391 18 -34.944370269775391 34 -34.944370269775391
		 35 -34.944370269775391 36 -5.3786287307739258 37 -5.3786287307739258 38 -5.3786287307739258
		 39 -5.0458674430847168 40 -4.1428766250610352 41 -4.1263022422790527 42 -6.6781363487243652
		 43 -10.145249366760254 44 -14.460103034973145 45 -19.428182601928711 46 -24.749113082885742
		 47 -30.053735733032227 48 -34.944370269775391;
createNode animCurveTA -n "Character1_RightHandRing2_rotateZ";
	rename -uid "1394355E-46C0-E2EA-1D38-848D69D47285";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 32 ".ktv[0:31]"  0 29.386693954467773 1 42.649311065673828
		 2 49.086631774902344 3 26.81671142578125 4 -1.4552251100540161 5 -14.685363769531248
		 6 -10.307660102844238 7 11.790786743164063 8 29.386693954467773 9 42.269405364990234
		 10 51.866527557373047 11 51.866527557373047 14 51.866527557373047 15 51.866527557373047
		 16 39.29022216796875 17 29.386693954467773 18 29.386693954467773 34 29.386693954467773
		 35 29.386693954467773 36 -14.685363769531248 37 -14.685363769531248 38 -14.685363769531248
		 39 -14.482313156127931 40 -13.894014358520508 41 -11.581778526306152 42 -6.3264255523681641
		 43 -0.40855017304420471 44 5.833439826965332 45 12.126607894897461 46 18.264833450317383
		 47 24.079694747924805 48 29.386693954467773;
createNode animCurveTU -n "Character1_RightHandRing2_visibility";
	rename -uid "7364B722-4EA1-45D8-989F-22859FE967AD";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandRing3_translateX";
	rename -uid "15AF3227-4DFB-2799-F253-769F8CC988B9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.5035178661346436;
createNode animCurveTL -n "Character1_RightHandRing3_translateY";
	rename -uid "08EDD0AE-472F-516F-D160-02A3862A0156";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.9153993129730225;
createNode animCurveTL -n "Character1_RightHandRing3_translateZ";
	rename -uid "BB87A5CC-4D2B-6EAE-8C45-6A8B582C4311";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.6898331642150879;
createNode animCurveTU -n "Character1_RightHandRing3_scaleX";
	rename -uid "A1A91118-4858-B834-30B9-839F2A12BBB4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing3_scaleY";
	rename -uid "F0403996-4F48-F87B-9832-FBAB03A61CF3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing3_scaleZ";
	rename -uid "C1435AA8-45AC-8ECD-00A9-19AF0D098FE9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandRing3_rotateX";
	rename -uid "C358EA90-4DC2-46C3-590F-09A494B68B97";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.2531329452467617e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateY";
	rename -uid "742279A3-4ABD-7DEB-B524-9EA3C21B5063";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.3012434442223366e-009;
createNode animCurveTA -n "Character1_RightHandRing3_rotateZ";
	rename -uid "A13B7BE1-48F1-5168-6A2D-D2BA94265158";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.120462371773101e-007;
createNode animCurveTU -n "Character1_RightHandRing3_visibility";
	rename -uid "0386CAED-4721-161F-5FD5-E59C953724B1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_Neck_translateX";
	rename -uid "FCA9058B-477E-0134-FA61-9FBBEC296F53";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 12.791294097900391;
createNode animCurveTL -n "Character1_Neck_translateY";
	rename -uid "A242ADF5-42DF-EA7B-B4B3-179C20F7E6E2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.3580474853515625;
createNode animCurveTL -n "Character1_Neck_translateZ";
	rename -uid "34E1A096-4889-7AD6-1588-349290E14A69";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.6322288513183594;
createNode animCurveTU -n "Character1_Neck_scaleX";
	rename -uid "4A10FB71-4D4B-4B28-6A2D-98B9F47DB4D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_Neck_scaleY";
	rename -uid "AD63D4AC-46AA-6D17-B7EF-2991E39F1ABC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Neck_scaleZ";
	rename -uid "906ED6DA-4DEC-7A0B-614B-BA8555FAD16E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Neck_rotateX";
	rename -uid "2C50752C-4412-4017-4039-D98446B63C7D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -0.20721951127052307 1 1.853749632835388
		 2 4.2095675468444824 3 6.5292601585388184 4 8.3429584503173828 5 9.2822885513305664
		 6 9.5386266708374023 7 9.4491767883300781 8 9.3520774841308594 9 9.3336343765258789
		 10 9.2360286712646484 11 9.0056905746459961 12 8.5565109252929687 13 7.7284202575683585
		 14 6.5953764915466309 15 5.4108905792236328 16 4.4218502044677734 17 3.8684146404266362
		 18 3.7125773429870605 19 3.7314069271087651 20 3.8893854618072514 21 4.1503210067749023
		 22 4.4768362045288086 23 4.829984188079834 24 5.1690373420715332 25 5.4514784812927246
		 26 5.6913986206054687 27 5.9411659240722656 28 6.2127189636230469 29 6.5157313346862793
		 30 6.8575758934020996 31 7.2433948516845694 32 7.7222237586975098 33 8.2864999771118164
		 34 8.8621501922607422 35 9.3863954544067383 36 9.9562673568725586 37 11.317208290100098
		 38 11.072759628295898 39 7.5581145286560059 40 2.9900867938995361 41 0.076019681990146637
		 42 -0.85385656356811523 43 -1.2734930515289307 44 -1.3202649354934692 45 -1.1220884323120117
		 46 -0.79769301414489746 47 -0.45787221193313593 48 -0.20721951127052307;
createNode animCurveTA -n "Character1_Neck_rotateY";
	rename -uid "3EBC0025-4A47-14C3-CC5B-E18DC8909ABF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -6.565436840057373 1 -10.147980690002441
		 2 -13.765473365783691 3 -16.793859481811523 4 -18.729961395263672 5 -19.233860015869141
		 6 -18.777599334716797 7 -18.104461669921875 8 -17.943027496337891 9 -18.645500183105469
		 10 -19.760429382324219 11 -20.821908950805664 12 -21.380277633666992 13 -21.261785507202148
		 14 -20.756792068481445 15 -20.115896224975586 16 -19.5968017578125 17 -19.463602066040039
		 18 -19.680948257446289 19 -20.019659042358398 20 -20.451381683349609 21 -20.948415756225586
		 22 -21.484296798706055 23 -22.03424072265625 24 -22.575435638427734 25 -23.087099075317383
		 26 -23.630794525146484 27 -24.244802474975586 28 -24.875577926635742 29 -25.470434188842773
		 30 -25.977542877197266 31 -26.346149444580078 32 -26.252967834472656 33 -25.794572830200195
		 34 -25.539108276367188 35 -26.036504745483398 36 -31.527952194213864 37 -31.324155807495117
		 38 -29.898290634155273 39 -28.414371490478516 40 -26.213987350463867 41 -23.671134948730469
		 42 -21.254793167114258 43 -18.823453903198242 44 -16.397375106811523 45 -13.977431297302246
		 46 -11.55024528503418 47 -9.0911293029785156 48 -6.565436840057373;
createNode animCurveTA -n "Character1_Neck_rotateZ";
	rename -uid "FC422A72-4D46-D00F-A956-F685118C4B58";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 1.1892544031143188 1 -0.67740744352340698
		 2 -2.8836917877197266 3 -5.2462606430053711 4 -7.4041805267333984 5 -8.9816856384277344
		 6 -10.073337554931641 7 -10.991114616394043 8 -12.053560256958008 9 -13.359941482543945
		 10 -14.750717163085938 11 -16.132606506347656 12 -17.385204315185547 13 -18.480688095092773
		 14 -19.493547439575195 15 -20.45012092590332 16 -21.365987777709961 17 -22.245279312133789
		 18 -23.105258941650391 19 -23.95994758605957 20 -24.808916091918945 21 -25.650863647460938
		 22 -26.482961654663086 23 -27.300382614135742 24 -28.096025466918945 25 -28.860473632812504
		 26 -29.558444976806641 27 -30.185825347900391 28 -30.776922225952148 29 -31.364036560058594
		 30 -31.977228164672852 31 -32.644275665283203 32 -33.371589660644531 33 -34.130176544189453
		 34 -34.904689788818359 35 -35.698040008544922 36 -36.981128692626953 37 -37.655261993408203
		 38 -35.306644439697266 39 -27.749073028564453 40 -18.685253143310547 41 -12.386279106140137
		 42 -9.1711664199829102 43 -6.7196164131164551 44 -4.8051090240478516 45 -3.2257943153381348
		 46 -1.8025559186935425 47 -0.37706902623176575 48 1.1892544031143188;
createNode animCurveTU -n "Character1_Neck_visibility";
	rename -uid "8513D75D-41FA-6F42-5537-95920C6D362C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_Head_translateX";
	rename -uid "90AE85A7-4B36-DD1E-D0B6-6BA58CE0375C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.627285957336426;
createNode animCurveTL -n "Character1_Head_translateY";
	rename -uid "4636A0D0-4615-63B6-EF23-41AE26C08259";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.64882755279541;
createNode animCurveTL -n "Character1_Head_translateZ";
	rename -uid "CF550C70-4AFD-A5DD-7F62-22BBC6F9D4DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.1324095726013184;
createNode animCurveTU -n "Character1_Head_scaleX";
	rename -uid "57AD3A71-42C7-271A-8186-04A3D0F8CE52";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Head_scaleY";
	rename -uid "FD3B409C-4871-5334-FCC5-39AA546EDF62";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Head_scaleZ";
	rename -uid "7E7DAEC4-47A6-B2D8-1FDC-4181035A8A65";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_Head_rotateX";
	rename -uid "951A88B7-48AA-93DA-7474-C49A53D83788";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 5.3204011917114258 1 8.7132101058959961
		 2 12.274172782897949 3 15.505320549011232 4 17.906906127929687 5 19.037496566772461
		 6 19.264348983764648 7 19.300224304199219 8 19.858119964599609 9 21.284507751464844
		 10 23.147115707397461 11 24.990653991699219 12 26.359445571899414 13 27.125614166259766
		 14 27.574771881103516 15 27.871706008911133 16 28.181337356567383 17 28.668586730957035
		 18 29.328422546386722 19 30.037052154541012 20 30.781307220458988 21 31.547994613647457
		 22 32.323875427246094 23 33.095668792724609 24 33.850051879882812 25 34.573650360107422
		 26 35.289913177490234 27 36.015102386474609 28 36.7249755859375 29 37.395267486572266
		 30 38.001659393310547 31 38.519790649414063 32 38.684246063232422 33 38.551395416259766
		 34 38.579788208007813 35 39.228309631347656 36 44.084430694580078 37 43.621608734130859
		 38 40.972122192382813 39 36.545875549316406 40 31.287544250488281 41 26.805482864379883
		 42 23.373834609985352 43 20.1636962890625 44 17.114137649536133 45 14.164821624755859
		 46 11.255828857421875 47 8.3275165557861328 48 5.3204011917114258;
createNode animCurveTA -n "Character1_Head_rotateY";
	rename -uid "A1F7D291-46BE-C2D3-AD04-E293A658B453";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 2.4991981983184814 1 5.3598361015319824
		 2 8.3839406967163086 3 11.076801300048828 4 12.944914817810059 5 13.692366600036621
		 6 13.638448715209961 7 13.212793350219727 8 12.844805717468262 9 12.634293556213379
		 10 12.356472969055176 11 11.95412540435791 12 11.370924949645996 13 10.469239234924316
		 14 9.3065595626831055 15 8.1197137832641602 16 7.1454238891601563 17 6.6203374862670898
		 18 6.4974336624145508 19 6.548027515411377 20 6.7361979484558105 21 7.0260329246520996
		 22 7.3816351890563965 23 7.7671222686767578 24 8.1466283798217773 25 8.4843025207519531
		 26 8.8181867599487305 27 9.1997852325439453 28 9.6134672164916992 29 10.043636322021484
		 30 10.474720001220703 31 10.891171455383301 32 11.229957580566406 33 11.526998519897461
		 34 11.915138244628906 35 12.527131080627441 36 14.725869178771973 37 15.78010368347168
		 38 15.341344833374023 39 12.475586891174316 40 8.7003154754638672 41 6.1179027557373047
		 42 5.0388445854187012 43 4.3040943145751953 44 3.8197488784790039 45 3.4915904998779297
		 46 3.2251815795898437 47 2.9259395599365234 48 2.4991981983184814;
createNode animCurveTA -n "Character1_Head_rotateZ";
	rename -uid "A50DF906-46C6-3353-614D-18838D11CD2D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 2.3467814922332764 1 2.5131800174713135
		 2 2.7564668655395508 3 2.8552267551422119 4 2.5867843627929687 5 1.7266600131988525
		 6 0.44644647836685181 7 -0.88234502077102661 8 -1.8876081705093384 9 -2.3754899501800537
		 10 -2.5744378566741943 11 -2.7462737560272217 12 -3.1550102233886719 13 -3.9093749523162837
		 14 -4.851536750793457 15 -5.8461837768554687 16 -6.7571020126342773 17 -7.4474215507507333
		 18 -7.9460196495056143 19 -8.3789348602294922 20 -8.7548160552978516 21 -9.0823640823364258
		 22 -9.3703041076660156 23 -9.6273775100708008 24 -9.8623275756835938 25 -10.083897590637207
		 26 -10.232138633728027 27 -10.27717399597168 28 -10.272879600524902 29 -10.273164749145508
		 30 -10.331967353820801 31 -10.503244400024414 32 -11.001611709594727 33 -11.756729125976563
		 34 -12.395092964172363 35 -12.543134689331055 36 -9.3871479034423828 37 -9.6670646667480469
		 38 -9.3139715194702148 39 -6.069819450378418 40 -1.9346324205398557 41 0.73803454637527466
		 42 1.6454033851623535 43 2.1341009140014648 44 2.3194634914398193 45 2.3171505928039551
		 46 2.2432093620300293 47 2.2141156196594238 48 2.3467814922332764;
createNode animCurveTU -n "Character1_Head_visibility";
	rename -uid "16FD0730-487D-CD97-6917-82A103F7AA6E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "eyes_translateX";
	rename -uid "669A3CDA-4DF4-5F99-3D28-6A94E49F463A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 30.947149276733398;
createNode animCurveTL -n "eyes_translateY";
	rename -uid "66F9C780-466E-4298-FDE2-7CA14B39BFEB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.0992542810490704e-007;
createNode animCurveTL -n "eyes_translateZ";
	rename -uid "6B8CB8FF-4127-6D50-2F50-729DCABD35A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.121469657048256e-013;
createNode animCurveTU -n "eyes_scaleX";
	rename -uid "8D9F1616-4D10-E817-EBAE-ED8232E233DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleY";
	rename -uid "56B8E4E1-42E9-503E-30F1-B6AECB3161E0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleZ";
	rename -uid "3ED158FE-4490-99A9-C048-B78BD30ADD31";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "eyes_rotateX";
	rename -uid "33049AC7-4166-6E5A-DBD4-078A0098E3DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 0 1 -4.0359869003295898 2 -8.0719738006591797
		 3 -11.101680755615234 4 -11.625663757324219 5 -10.955703735351562 6 -10.771738052368164
		 7 -10.605412483215332 8 -10.444850921630859 9 -10.278179168701172 10 -10.09351921081543
		 11 -9.8789968490600586 12 -9.6227378845214844 13 -9.3128643035888672 14 -8.9375038146972656
		 15 -8.0415306091308594 16 -6.7699203491210938 17 -6.0109424591064453 18 -6.340583324432373
		 19 -7.2199368476867685 20 -7.8649330139160156 21 -7.4915027618408212 22 -3.1879537105560303
		 23 0.85305845737457275 24 1.3792074918746948 25 1.7334198951721191 26 1.9361222982406618
		 27 2.0077407360076904 28 1.9687016010284424 29 1.8394314050674441 30 1.6403564214706421
		 31 1.3919028043746948 32 1.114497184753418 33 0.82856559753417969 34 0.55453473329544067
		 35 -0.45326781272888184 36 -2.0333967208862305 37 -2.8047492504119873 38 -2.1729707717895508
		 39 -0.87898445129394531 40 0.45539563894271851 41 1.2083547115325928 42 1.3498871326446533
		 43 1.2994363307952881 44 1.1093611717224121 45 0.83202093839645386 46 0.51977461576461792
		 47 0.22498126327991486 48 0;
createNode animCurveTA -n "eyes_rotateY";
	rename -uid "31E91F84-4F20-F89F-5A1A-BA9C6854A6F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 0 1 2.4413321018218994 2 4.8826637268066406
		 3 6.7698984146118164 4 7.2805786132812491 5 7.1274337768554687 6 7.2452268600463867
		 7 7.3919553756713858 8 7.5511531829833993 9 7.7063550949096671 10 7.8410935401916504
		 11 7.9389033317565918 12 7.9833183288574219 13 7.957871437072753 14 7.8460984230041504
		 15 7.1339287757873535 16 5.9412035942077637 17 5.2023205757141113 18 5.4068937301635742
		 19 6.0517044067382812 20 6.5818982124328613 21 6.442622184753418 22 3.7803442478179932
		 23 1.2485454082489014 24 0.90315252542495728 25 0.65647721290588379 26 0.49876105785369873
		 27 0.42024573683738708 28 0.411173015832901 29 0.46178457140922546 30 0.56232219934463501
		 31 0.70302754640579224 32 0.8741423487663269 33 1.0659081935882568 34 1.2685670852661133
		 35 2.0629181861877441 36 3.3535706996917725 37 4.1164803504943848 38 4.0028352737426758
		 39 3.5103974342346191 40 2.8849799633026123 41 2.3723981380462646 42 2.0075409412384033
		 43 1.6585381031036377 44 1.3210653066635132 45 0.99079900979995728 46 0.66341519355773926
		 47 0.33459019660949707 48 0;
createNode animCurveTA -n "eyes_rotateZ";
	rename -uid "6709DA43-4D54-DF30-5A15-158987530628";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 0 1 0.66181421279907227 2 1.3236284255981445
		 3 1.9401485919952393 4 2.4950289726257324 5 2.8938744068145752 6 3.1391949653625488
		 7 3.3498449325561523 8 3.5379705429077148 9 3.7157177925109868 10 3.8952333927154541
		 11 4.0886631011962891 12 4.3081531524658203 13 4.565849781036377 14 4.8738994598388672
		 15 5.5406613349914551 16 6.4419670104980469 17 6.9411730766296387 18 6.8983888626098633
		 19 6.6190004348754883 20 6.13427734375 21 5.4754891395568848 22 3.9047136306762695
		 23 2.5287904739379883 24 2.186164379119873 25 1.8558998107910156 26 1.5473411083221436
		 27 1.2698311805725098 28 1.0327135324478149 29 0.84533196687698364 30 0.71702969074249268
		 31 0.65715032815933228 32 0.6750374436378479 33 0.78003442287445068 34 0.98148488998413075
		 35 2.4435927867889404 36 4.8674817085266113 37 6.067873477935791 38 5.1450405120849609
		 39 3.2413618564605713 40 1.2347229719161987 41 0.0030080245342105627 42 -0.40238142013549805
		 43 -0.5602954626083374 44 -0.53822743892669678 45 -0.40367057919502258 46 -0.22411820292472839
		 47 -0.067063555121421814 48 0;
createNode animCurveTU -n "eyes_visibility";
	rename -uid "5982B32B-4FB0-E314-6A23-64BD44D1D4F2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "jaw_translateX";
	rename -uid "FA40C09C-43A3-77C5-60F9-D389AA0CC17A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.07279486209154129;
createNode animCurveTL -n "jaw_translateY";
	rename -uid "13A2D29F-47E9-5C56-1913-98AE51A4B483";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.25670051574707;
createNode animCurveTL -n "jaw_translateZ";
	rename -uid "F7DF2237-4417-5973-C19C-2FA928B237A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.2238426557523781e-007;
createNode animCurveTU -n "jaw_scaleX";
	rename -uid "D987BD9B-45B6-E012-5DB9-1EA28EBFF075";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleY";
	rename -uid "52F15B50-4D0A-3A33-0DDD-E9917A7C867B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleZ";
	rename -uid "CA260547-4D6D-2059-1AF9-639DFE3EF8E4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "jaw_rotateX";
	rename -uid "0FDECACA-4CA9-334E-20EC-2CBDC7F4AEA3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -52.637283325195313 1 -50.353408813476563
		 2 -47.654922485351562 3 -44.977264404296875 4 -42.85833740234375 5 -41.915939331054688
		 6 -42.864654541015625 7 -44.31903076171875 8 -45.451507568359375 9 -46.608062744140625
		 10 -48.435592651367188 11 -50.048446655273438 12 -51.346603393554687 13 -52.218650817871094
		 14 -52.542205810546875 15 -52.547210693359375 16 -52.5528564453125 17 -52.560546875
		 18 -52.570945739746094 19 -52.583732604980469 20 -52.598609924316406 21 -52.614898681640625
		 22 -52.631568908691406 23 -52.647377014160156 24 -52.661094665527344 25 -52.671577453613281
		 26 -52.676986694335937 27 -52.677001953125 28 -52.67315673828125 29 -52.667388916015625
		 30 -52.662269592285156 31 -52.661300659179688 32 -52.667259216308594 33 -52.679611206054688
		 34 -52.697257995605469 35 -52.719406127929687 36 -52.760025024414063 37 -51.809585571289063
		 38 -49.933761596679688 39 -48.74005126953125 40 -48.88311767578125 41 -49.608932495117188
		 42 -50.267364501953125 43 -50.45941162109375 44 -50.939132690429688 45 -51.7139892578125
		 46 -52.613021850585938 47 -52.926895141601563 48 -52.637283325195313;
createNode animCurveTA -n "jaw_rotateY";
	rename -uid "856979EB-4D95-0E1D-9215-48B184491D65";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 48 ".ktv[0:47]"  0 -120.80648803710939 1 -119.12889099121094
		 2 -117.27073669433594 3 -115.57095336914064 4 -114.33473205566406 5 -113.83878326416016
		 6 -114.27059936523437 7 -115.07674407958983 8 -115.86595153808595 9 -116.69144439697267
		 10 -117.90147399902344 11 -119.03482818603516 12 -119.986328125 13 -120.64190673828125
		 14 -120.8826141357422 15 -120.87710571289064 17 -120.87617492675781 18 -120.88232421874999
		 19 -120.89054870605469 20 -120.90045166015625 21 -120.91165161132812 22 -120.92362976074217
		 23 -120.93568420410155 24 -120.9469451904297 25 -120.95647430419922 26 -120.96503448486328
		 27 -120.97373962402344 28 -120.98245239257814 29 -120.99066162109375 30 -120.99682617187501
		 31 -120.998046875 32 -120.99183654785155 33 -120.97874450683594 34 -120.96031188964844
		 35 -120.938232421875 36 -120.90883636474611 37 -120.89733886718749 38 -120.95509338378905
		 39 -121.02078247070312 40 -121.0830078125 41 -121.12049865722658 42 -120.85532379150391
		 43 -120.5772705078125 44 -120.62539672851564 45 -120.80397033691406 46 -121.09536743164064
		 47 -120.96827697753906 48 -120.80648803710939;
createNode animCurveTA -n "jaw_rotateZ";
	rename -uid "FF7264B4-4D7C-1738-5D2E-E4A19DDEA5E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 49 ".ktv[0:48]"  0 -44.75030517578125 1 -46.2457275390625
		 2 -48.07183837890625 3 -49.945709228515625 4 -51.475494384765625 5 -52.207252502441406
		 6 -51.224441528320312 7 -49.891342163085937 8 -49.272857666015625 9 -49.00335693359375
		 10 -47.690750122070313 11 -46.602340698242188 12 -45.759307861328125 13 -45.202529907226563
		 14 -44.993896484375 15 -44.983932495117188 16 -44.974014282226563 17 -44.962936401367188
		 18 -44.951034545898438 19 -44.939224243164063 20 -44.927719116210938 21 -44.91680908203125
		 22 -44.90667724609375 23 -44.897476196289063 24 -44.88916015625 25 -44.88140869140625
		 26 -44.872833251953125 27 -44.862884521484375 28 -44.8524169921875 29 -44.8426513671875
		 30 -44.835662841796875 31 -44.834320068359375 32 -44.840530395507812 33 -44.851852416992188
		 34 -44.865432739257813 35 -44.881011962890625 36 -44.93243408203125 37 -45.356002807617188
		 38 -45.996124267578125 39 -46.127532958984375 40 -45.2867431640625 41 -43.969223022460938
		 42 -43.4073486328125 43 -43.281646728515625 44 -43.264495849609375 45 -43.422744750976563
		 46 -43.394302368164063 47 -44.331130981445313 48 -44.75030517578125;
createNode animCurveTU -n "jaw_visibility";
	rename -uid "C5C24C7D-4BE5-1FE8-DFF4-C6B31C59FF27";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 80 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "B4D294A3-4B9E-A588-FECF-43859FD75082";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "7B74ECB9-4D5E-EAAB-942F-029891ED075D";
createNode displayLayer -n "defaultLayer";
	rename -uid "8679F72C-4D0A-BE47-0152-63A7EDF18DAD";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "43F89D09-43E5-07DA-B07B-D2BEA096B744";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "4423F85A-4D17-793F-FEE2-64BBAB804DDE";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "D45ED9BD-4C00-BBFD-0F71-788E3DA31C7D";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1636\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1636\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1320\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1320\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 13 100 -ps 2 87 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1320\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1320\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "6AB167FF-4735-B087-4DD9-37B7D4203AE6";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 48 -ast 0 -aet 250 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 0;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
connectAttr "ref_frame.s" "Character1_Hips.is";
connectAttr "Character1_Hips_scaleX.o" "Character1_Hips.sx";
connectAttr "Character1_Hips_scaleY.o" "Character1_Hips.sy";
connectAttr "Character1_Hips_scaleZ.o" "Character1_Hips.sz";
connectAttr "Character1_Hips_translateX.o" "Character1_Hips.tx";
connectAttr "Character1_Hips_translateY.o" "Character1_Hips.ty";
connectAttr "Character1_Hips_translateZ.o" "Character1_Hips.tz";
connectAttr "Character1_Hips_rotateX.o" "Character1_Hips.rx";
connectAttr "Character1_Hips_rotateY.o" "Character1_Hips.ry";
connectAttr "Character1_Hips_rotateZ.o" "Character1_Hips.rz";
connectAttr "Character1_Hips_visibility.o" "Character1_Hips.v";
connectAttr "Character1_Hips.s" "Character1_LeftUpLeg.is";
connectAttr "Character1_LeftUpLeg_scaleX.o" "Character1_LeftUpLeg.sx";
connectAttr "Character1_LeftUpLeg_scaleY.o" "Character1_LeftUpLeg.sy";
connectAttr "Character1_LeftUpLeg_scaleZ.o" "Character1_LeftUpLeg.sz";
connectAttr "Character1_LeftUpLeg_translateX.o" "Character1_LeftUpLeg.tx";
connectAttr "Character1_LeftUpLeg_translateY.o" "Character1_LeftUpLeg.ty";
connectAttr "Character1_LeftUpLeg_translateZ.o" "Character1_LeftUpLeg.tz";
connectAttr "Character1_LeftUpLeg_rotateX.o" "Character1_LeftUpLeg.rx";
connectAttr "Character1_LeftUpLeg_rotateY.o" "Character1_LeftUpLeg.ry";
connectAttr "Character1_LeftUpLeg_rotateZ.o" "Character1_LeftUpLeg.rz";
connectAttr "Character1_LeftUpLeg_visibility.o" "Character1_LeftUpLeg.v";
connectAttr "Character1_LeftUpLeg.s" "Character1_LeftLeg.is";
connectAttr "Character1_LeftLeg_scaleX.o" "Character1_LeftLeg.sx";
connectAttr "Character1_LeftLeg_scaleY.o" "Character1_LeftLeg.sy";
connectAttr "Character1_LeftLeg_scaleZ.o" "Character1_LeftLeg.sz";
connectAttr "Character1_LeftLeg_translateX.o" "Character1_LeftLeg.tx";
connectAttr "Character1_LeftLeg_translateY.o" "Character1_LeftLeg.ty";
connectAttr "Character1_LeftLeg_translateZ.o" "Character1_LeftLeg.tz";
connectAttr "Character1_LeftLeg_rotateX.o" "Character1_LeftLeg.rx";
connectAttr "Character1_LeftLeg_rotateY.o" "Character1_LeftLeg.ry";
connectAttr "Character1_LeftLeg_rotateZ.o" "Character1_LeftLeg.rz";
connectAttr "Character1_LeftLeg_visibility.o" "Character1_LeftLeg.v";
connectAttr "Character1_LeftLeg.s" "Character1_LeftFoot.is";
connectAttr "Character1_LeftFoot_scaleX.o" "Character1_LeftFoot.sx";
connectAttr "Character1_LeftFoot_scaleY.o" "Character1_LeftFoot.sy";
connectAttr "Character1_LeftFoot_scaleZ.o" "Character1_LeftFoot.sz";
connectAttr "Character1_LeftFoot_translateX.o" "Character1_LeftFoot.tx";
connectAttr "Character1_LeftFoot_translateY.o" "Character1_LeftFoot.ty";
connectAttr "Character1_LeftFoot_translateZ.o" "Character1_LeftFoot.tz";
connectAttr "Character1_LeftFoot_rotateX.o" "Character1_LeftFoot.rx";
connectAttr "Character1_LeftFoot_rotateY.o" "Character1_LeftFoot.ry";
connectAttr "Character1_LeftFoot_rotateZ.o" "Character1_LeftFoot.rz";
connectAttr "Character1_LeftFoot_visibility.o" "Character1_LeftFoot.v";
connectAttr "Character1_LeftFoot.s" "Character1_LeftToeBase.is";
connectAttr "Character1_LeftToeBase_scaleX.o" "Character1_LeftToeBase.sx";
connectAttr "Character1_LeftToeBase_scaleY.o" "Character1_LeftToeBase.sy";
connectAttr "Character1_LeftToeBase_scaleZ.o" "Character1_LeftToeBase.sz";
connectAttr "Character1_LeftToeBase_translateX.o" "Character1_LeftToeBase.tx";
connectAttr "Character1_LeftToeBase_translateY.o" "Character1_LeftToeBase.ty";
connectAttr "Character1_LeftToeBase_translateZ.o" "Character1_LeftToeBase.tz";
connectAttr "Character1_LeftToeBase_rotateX.o" "Character1_LeftToeBase.rx";
connectAttr "Character1_LeftToeBase_rotateY.o" "Character1_LeftToeBase.ry";
connectAttr "Character1_LeftToeBase_rotateZ.o" "Character1_LeftToeBase.rz";
connectAttr "Character1_LeftToeBase_visibility.o" "Character1_LeftToeBase.v";
connectAttr "Character1_LeftToeBase.s" "Character1_LeftFootMiddle1.is";
connectAttr "Character1_LeftFootMiddle1_scaleX.o" "Character1_LeftFootMiddle1.sx"
		;
connectAttr "Character1_LeftFootMiddle1_scaleY.o" "Character1_LeftFootMiddle1.sy"
		;
connectAttr "Character1_LeftFootMiddle1_scaleZ.o" "Character1_LeftFootMiddle1.sz"
		;
connectAttr "Character1_LeftFootMiddle1_translateX.o" "Character1_LeftFootMiddle1.tx"
		;
connectAttr "Character1_LeftFootMiddle1_translateY.o" "Character1_LeftFootMiddle1.ty"
		;
connectAttr "Character1_LeftFootMiddle1_translateZ.o" "Character1_LeftFootMiddle1.tz"
		;
connectAttr "Character1_LeftFootMiddle1_rotateX.o" "Character1_LeftFootMiddle1.rx"
		;
connectAttr "Character1_LeftFootMiddle1_rotateY.o" "Character1_LeftFootMiddle1.ry"
		;
connectAttr "Character1_LeftFootMiddle1_rotateZ.o" "Character1_LeftFootMiddle1.rz"
		;
connectAttr "Character1_LeftFootMiddle1_visibility.o" "Character1_LeftFootMiddle1.v"
		;
connectAttr "Character1_LeftFootMiddle1.s" "Character1_LeftFootMiddle2.is";
connectAttr "Character1_LeftFootMiddle2_translateX.o" "Character1_LeftFootMiddle2.tx"
		;
connectAttr "Character1_LeftFootMiddle2_translateY.o" "Character1_LeftFootMiddle2.ty"
		;
connectAttr "Character1_LeftFootMiddle2_translateZ.o" "Character1_LeftFootMiddle2.tz"
		;
connectAttr "Character1_LeftFootMiddle2_scaleX.o" "Character1_LeftFootMiddle2.sx"
		;
connectAttr "Character1_LeftFootMiddle2_scaleY.o" "Character1_LeftFootMiddle2.sy"
		;
connectAttr "Character1_LeftFootMiddle2_scaleZ.o" "Character1_LeftFootMiddle2.sz"
		;
connectAttr "Character1_LeftFootMiddle2_rotateX.o" "Character1_LeftFootMiddle2.rx"
		;
connectAttr "Character1_LeftFootMiddle2_rotateY.o" "Character1_LeftFootMiddle2.ry"
		;
connectAttr "Character1_LeftFootMiddle2_rotateZ.o" "Character1_LeftFootMiddle2.rz"
		;
connectAttr "Character1_LeftFootMiddle2_visibility.o" "Character1_LeftFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_RightUpLeg.is";
connectAttr "Character1_RightUpLeg_scaleX.o" "Character1_RightUpLeg.sx";
connectAttr "Character1_RightUpLeg_scaleY.o" "Character1_RightUpLeg.sy";
connectAttr "Character1_RightUpLeg_scaleZ.o" "Character1_RightUpLeg.sz";
connectAttr "Character1_RightUpLeg_translateX.o" "Character1_RightUpLeg.tx";
connectAttr "Character1_RightUpLeg_translateY.o" "Character1_RightUpLeg.ty";
connectAttr "Character1_RightUpLeg_translateZ.o" "Character1_RightUpLeg.tz";
connectAttr "Character1_RightUpLeg_rotateX.o" "Character1_RightUpLeg.rx";
connectAttr "Character1_RightUpLeg_rotateY.o" "Character1_RightUpLeg.ry";
connectAttr "Character1_RightUpLeg_rotateZ.o" "Character1_RightUpLeg.rz";
connectAttr "Character1_RightUpLeg_visibility.o" "Character1_RightUpLeg.v";
connectAttr "Character1_RightUpLeg.s" "Character1_RightLeg.is";
connectAttr "Character1_RightLeg_scaleX.o" "Character1_RightLeg.sx";
connectAttr "Character1_RightLeg_scaleY.o" "Character1_RightLeg.sy";
connectAttr "Character1_RightLeg_scaleZ.o" "Character1_RightLeg.sz";
connectAttr "Character1_RightLeg_translateX.o" "Character1_RightLeg.tx";
connectAttr "Character1_RightLeg_translateY.o" "Character1_RightLeg.ty";
connectAttr "Character1_RightLeg_translateZ.o" "Character1_RightLeg.tz";
connectAttr "Character1_RightLeg_rotateX.o" "Character1_RightLeg.rx";
connectAttr "Character1_RightLeg_rotateY.o" "Character1_RightLeg.ry";
connectAttr "Character1_RightLeg_rotateZ.o" "Character1_RightLeg.rz";
connectAttr "Character1_RightLeg_visibility.o" "Character1_RightLeg.v";
connectAttr "Character1_RightLeg.s" "Character1_RightFoot.is";
connectAttr "Character1_RightFoot_scaleX.o" "Character1_RightFoot.sx";
connectAttr "Character1_RightFoot_scaleY.o" "Character1_RightFoot.sy";
connectAttr "Character1_RightFoot_scaleZ.o" "Character1_RightFoot.sz";
connectAttr "Character1_RightFoot_translateX.o" "Character1_RightFoot.tx";
connectAttr "Character1_RightFoot_translateY.o" "Character1_RightFoot.ty";
connectAttr "Character1_RightFoot_translateZ.o" "Character1_RightFoot.tz";
connectAttr "Character1_RightFoot_rotateX.o" "Character1_RightFoot.rx";
connectAttr "Character1_RightFoot_rotateY.o" "Character1_RightFoot.ry";
connectAttr "Character1_RightFoot_rotateZ.o" "Character1_RightFoot.rz";
connectAttr "Character1_RightFoot_visibility.o" "Character1_RightFoot.v";
connectAttr "Character1_RightFoot.s" "Character1_RightToeBase.is";
connectAttr "Character1_RightToeBase_scaleX.o" "Character1_RightToeBase.sx";
connectAttr "Character1_RightToeBase_scaleY.o" "Character1_RightToeBase.sy";
connectAttr "Character1_RightToeBase_scaleZ.o" "Character1_RightToeBase.sz";
connectAttr "Character1_RightToeBase_translateX.o" "Character1_RightToeBase.tx";
connectAttr "Character1_RightToeBase_translateY.o" "Character1_RightToeBase.ty";
connectAttr "Character1_RightToeBase_translateZ.o" "Character1_RightToeBase.tz";
connectAttr "Character1_RightToeBase_rotateX.o" "Character1_RightToeBase.rx";
connectAttr "Character1_RightToeBase_rotateY.o" "Character1_RightToeBase.ry";
connectAttr "Character1_RightToeBase_rotateZ.o" "Character1_RightToeBase.rz";
connectAttr "Character1_RightToeBase_visibility.o" "Character1_RightToeBase.v";
connectAttr "Character1_RightToeBase.s" "Character1_RightFootMiddle1.is";
connectAttr "Character1_RightFootMiddle1_scaleX.o" "Character1_RightFootMiddle1.sx"
		;
connectAttr "Character1_RightFootMiddle1_scaleY.o" "Character1_RightFootMiddle1.sy"
		;
connectAttr "Character1_RightFootMiddle1_scaleZ.o" "Character1_RightFootMiddle1.sz"
		;
connectAttr "Character1_RightFootMiddle1_translateX.o" "Character1_RightFootMiddle1.tx"
		;
connectAttr "Character1_RightFootMiddle1_translateY.o" "Character1_RightFootMiddle1.ty"
		;
connectAttr "Character1_RightFootMiddle1_translateZ.o" "Character1_RightFootMiddle1.tz"
		;
connectAttr "Character1_RightFootMiddle1_rotateX.o" "Character1_RightFootMiddle1.rx"
		;
connectAttr "Character1_RightFootMiddle1_rotateY.o" "Character1_RightFootMiddle1.ry"
		;
connectAttr "Character1_RightFootMiddle1_rotateZ.o" "Character1_RightFootMiddle1.rz"
		;
connectAttr "Character1_RightFootMiddle1_visibility.o" "Character1_RightFootMiddle1.v"
		;
connectAttr "Character1_RightFootMiddle1.s" "Character1_RightFootMiddle2.is";
connectAttr "Character1_RightFootMiddle2_translateX.o" "Character1_RightFootMiddle2.tx"
		;
connectAttr "Character1_RightFootMiddle2_translateY.o" "Character1_RightFootMiddle2.ty"
		;
connectAttr "Character1_RightFootMiddle2_translateZ.o" "Character1_RightFootMiddle2.tz"
		;
connectAttr "Character1_RightFootMiddle2_scaleX.o" "Character1_RightFootMiddle2.sx"
		;
connectAttr "Character1_RightFootMiddle2_scaleY.o" "Character1_RightFootMiddle2.sy"
		;
connectAttr "Character1_RightFootMiddle2_scaleZ.o" "Character1_RightFootMiddle2.sz"
		;
connectAttr "Character1_RightFootMiddle2_rotateX.o" "Character1_RightFootMiddle2.rx"
		;
connectAttr "Character1_RightFootMiddle2_rotateY.o" "Character1_RightFootMiddle2.ry"
		;
connectAttr "Character1_RightFootMiddle2_rotateZ.o" "Character1_RightFootMiddle2.rz"
		;
connectAttr "Character1_RightFootMiddle2_visibility.o" "Character1_RightFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_Spine.is";
connectAttr "Character1_Spine_scaleX.o" "Character1_Spine.sx";
connectAttr "Character1_Spine_scaleY.o" "Character1_Spine.sy";
connectAttr "Character1_Spine_scaleZ.o" "Character1_Spine.sz";
connectAttr "Character1_Spine_translateX.o" "Character1_Spine.tx";
connectAttr "Character1_Spine_translateY.o" "Character1_Spine.ty";
connectAttr "Character1_Spine_translateZ.o" "Character1_Spine.tz";
connectAttr "Character1_Spine_rotateX.o" "Character1_Spine.rx";
connectAttr "Character1_Spine_rotateY.o" "Character1_Spine.ry";
connectAttr "Character1_Spine_rotateZ.o" "Character1_Spine.rz";
connectAttr "Character1_Spine_visibility.o" "Character1_Spine.v";
connectAttr "Character1_Spine.s" "Character1_Spine1.is";
connectAttr "Character1_Spine1_scaleX.o" "Character1_Spine1.sx";
connectAttr "Character1_Spine1_scaleY.o" "Character1_Spine1.sy";
connectAttr "Character1_Spine1_scaleZ.o" "Character1_Spine1.sz";
connectAttr "Character1_Spine1_translateX.o" "Character1_Spine1.tx";
connectAttr "Character1_Spine1_translateY.o" "Character1_Spine1.ty";
connectAttr "Character1_Spine1_translateZ.o" "Character1_Spine1.tz";
connectAttr "Character1_Spine1_rotateX.o" "Character1_Spine1.rx";
connectAttr "Character1_Spine1_rotateY.o" "Character1_Spine1.ry";
connectAttr "Character1_Spine1_rotateZ.o" "Character1_Spine1.rz";
connectAttr "Character1_Spine1_visibility.o" "Character1_Spine1.v";
connectAttr "Character1_Spine1.s" "Character1_LeftShoulder.is";
connectAttr "Character1_LeftShoulder_scaleX.o" "Character1_LeftShoulder.sx";
connectAttr "Character1_LeftShoulder_scaleY.o" "Character1_LeftShoulder.sy";
connectAttr "Character1_LeftShoulder_scaleZ.o" "Character1_LeftShoulder.sz";
connectAttr "Character1_LeftShoulder_translateX.o" "Character1_LeftShoulder.tx";
connectAttr "Character1_LeftShoulder_translateY.o" "Character1_LeftShoulder.ty";
connectAttr "Character1_LeftShoulder_translateZ.o" "Character1_LeftShoulder.tz";
connectAttr "Character1_LeftShoulder_rotateX.o" "Character1_LeftShoulder.rx";
connectAttr "Character1_LeftShoulder_rotateY.o" "Character1_LeftShoulder.ry";
connectAttr "Character1_LeftShoulder_rotateZ.o" "Character1_LeftShoulder.rz";
connectAttr "Character1_LeftShoulder_visibility.o" "Character1_LeftShoulder.v";
connectAttr "Character1_LeftShoulder.s" "Character1_LeftArm.is";
connectAttr "Character1_LeftArm_scaleX.o" "Character1_LeftArm.sx";
connectAttr "Character1_LeftArm_scaleY.o" "Character1_LeftArm.sy";
connectAttr "Character1_LeftArm_scaleZ.o" "Character1_LeftArm.sz";
connectAttr "Character1_LeftArm_translateX.o" "Character1_LeftArm.tx";
connectAttr "Character1_LeftArm_translateY.o" "Character1_LeftArm.ty";
connectAttr "Character1_LeftArm_translateZ.o" "Character1_LeftArm.tz";
connectAttr "Character1_LeftArm_rotateX.o" "Character1_LeftArm.rx";
connectAttr "Character1_LeftArm_rotateY.o" "Character1_LeftArm.ry";
connectAttr "Character1_LeftArm_rotateZ.o" "Character1_LeftArm.rz";
connectAttr "Character1_LeftArm_visibility.o" "Character1_LeftArm.v";
connectAttr "Character1_LeftArm.s" "Character1_LeftForeArm.is";
connectAttr "Character1_LeftForeArm_scaleX.o" "Character1_LeftForeArm.sx";
connectAttr "Character1_LeftForeArm_scaleY.o" "Character1_LeftForeArm.sy";
connectAttr "Character1_LeftForeArm_scaleZ.o" "Character1_LeftForeArm.sz";
connectAttr "Character1_LeftForeArm_translateX.o" "Character1_LeftForeArm.tx";
connectAttr "Character1_LeftForeArm_translateY.o" "Character1_LeftForeArm.ty";
connectAttr "Character1_LeftForeArm_translateZ.o" "Character1_LeftForeArm.tz";
connectAttr "Character1_LeftForeArm_rotateX.o" "Character1_LeftForeArm.rx";
connectAttr "Character1_LeftForeArm_rotateY.o" "Character1_LeftForeArm.ry";
connectAttr "Character1_LeftForeArm_rotateZ.o" "Character1_LeftForeArm.rz";
connectAttr "Character1_LeftForeArm_visibility.o" "Character1_LeftForeArm.v";
connectAttr "Character1_LeftForeArm.s" "Character1_LeftHand.is";
connectAttr "Character1_LeftHand_lockInfluenceWeights.o" "Character1_LeftHand.liw"
		;
connectAttr "Character1_LeftHand_scaleX.o" "Character1_LeftHand.sx";
connectAttr "Character1_LeftHand_scaleY.o" "Character1_LeftHand.sy";
connectAttr "Character1_LeftHand_scaleZ.o" "Character1_LeftHand.sz";
connectAttr "Character1_LeftHand_translateX.o" "Character1_LeftHand.tx";
connectAttr "Character1_LeftHand_translateY.o" "Character1_LeftHand.ty";
connectAttr "Character1_LeftHand_translateZ.o" "Character1_LeftHand.tz";
connectAttr "Character1_LeftHand_rotateX.o" "Character1_LeftHand.rx";
connectAttr "Character1_LeftHand_rotateY.o" "Character1_LeftHand.ry";
connectAttr "Character1_LeftHand_rotateZ.o" "Character1_LeftHand.rz";
connectAttr "Character1_LeftHand_visibility.o" "Character1_LeftHand.v";
connectAttr "Character1_LeftHand.s" "Character1_LeftHandThumb1.is";
connectAttr "Character1_LeftHandThumb1_scaleX.o" "Character1_LeftHandThumb1.sx";
connectAttr "Character1_LeftHandThumb1_scaleY.o" "Character1_LeftHandThumb1.sy";
connectAttr "Character1_LeftHandThumb1_scaleZ.o" "Character1_LeftHandThumb1.sz";
connectAttr "Character1_LeftHandThumb1_translateX.o" "Character1_LeftHandThumb1.tx"
		;
connectAttr "Character1_LeftHandThumb1_translateY.o" "Character1_LeftHandThumb1.ty"
		;
connectAttr "Character1_LeftHandThumb1_translateZ.o" "Character1_LeftHandThumb1.tz"
		;
connectAttr "Character1_LeftHandThumb1_rotateX.o" "Character1_LeftHandThumb1.rx"
		;
connectAttr "Character1_LeftHandThumb1_rotateY.o" "Character1_LeftHandThumb1.ry"
		;
connectAttr "Character1_LeftHandThumb1_rotateZ.o" "Character1_LeftHandThumb1.rz"
		;
connectAttr "Character1_LeftHandThumb1_visibility.o" "Character1_LeftHandThumb1.v"
		;
connectAttr "Character1_LeftHandThumb1.s" "Character1_LeftHandThumb2.is";
connectAttr "Character1_LeftHandThumb2_scaleX.o" "Character1_LeftHandThumb2.sx";
connectAttr "Character1_LeftHandThumb2_scaleY.o" "Character1_LeftHandThumb2.sy";
connectAttr "Character1_LeftHandThumb2_scaleZ.o" "Character1_LeftHandThumb2.sz";
connectAttr "Character1_LeftHandThumb2_translateX.o" "Character1_LeftHandThumb2.tx"
		;
connectAttr "Character1_LeftHandThumb2_translateY.o" "Character1_LeftHandThumb2.ty"
		;
connectAttr "Character1_LeftHandThumb2_translateZ.o" "Character1_LeftHandThumb2.tz"
		;
connectAttr "Character1_LeftHandThumb2_rotateX.o" "Character1_LeftHandThumb2.rx"
		;
connectAttr "Character1_LeftHandThumb2_rotateY.o" "Character1_LeftHandThumb2.ry"
		;
connectAttr "Character1_LeftHandThumb2_rotateZ.o" "Character1_LeftHandThumb2.rz"
		;
connectAttr "Character1_LeftHandThumb2_visibility.o" "Character1_LeftHandThumb2.v"
		;
connectAttr "Character1_LeftHandThumb2.s" "Character1_LeftHandThumb3.is";
connectAttr "Character1_LeftHandThumb3_translateX.o" "Character1_LeftHandThumb3.tx"
		;
connectAttr "Character1_LeftHandThumb3_translateY.o" "Character1_LeftHandThumb3.ty"
		;
connectAttr "Character1_LeftHandThumb3_translateZ.o" "Character1_LeftHandThumb3.tz"
		;
connectAttr "Character1_LeftHandThumb3_scaleX.o" "Character1_LeftHandThumb3.sx";
connectAttr "Character1_LeftHandThumb3_scaleY.o" "Character1_LeftHandThumb3.sy";
connectAttr "Character1_LeftHandThumb3_scaleZ.o" "Character1_LeftHandThumb3.sz";
connectAttr "Character1_LeftHandThumb3_rotateX.o" "Character1_LeftHandThumb3.rx"
		;
connectAttr "Character1_LeftHandThumb3_rotateY.o" "Character1_LeftHandThumb3.ry"
		;
connectAttr "Character1_LeftHandThumb3_rotateZ.o" "Character1_LeftHandThumb3.rz"
		;
connectAttr "Character1_LeftHandThumb3_visibility.o" "Character1_LeftHandThumb3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandIndex1.is";
connectAttr "Character1_LeftHandIndex1_scaleX.o" "Character1_LeftHandIndex1.sx";
connectAttr "Character1_LeftHandIndex1_scaleY.o" "Character1_LeftHandIndex1.sy";
connectAttr "Character1_LeftHandIndex1_scaleZ.o" "Character1_LeftHandIndex1.sz";
connectAttr "Character1_LeftHandIndex1_translateX.o" "Character1_LeftHandIndex1.tx"
		;
connectAttr "Character1_LeftHandIndex1_translateY.o" "Character1_LeftHandIndex1.ty"
		;
connectAttr "Character1_LeftHandIndex1_translateZ.o" "Character1_LeftHandIndex1.tz"
		;
connectAttr "Character1_LeftHandIndex1_rotateX.o" "Character1_LeftHandIndex1.rx"
		;
connectAttr "Character1_LeftHandIndex1_rotateY.o" "Character1_LeftHandIndex1.ry"
		;
connectAttr "Character1_LeftHandIndex1_rotateZ.o" "Character1_LeftHandIndex1.rz"
		;
connectAttr "Character1_LeftHandIndex1_visibility.o" "Character1_LeftHandIndex1.v"
		;
connectAttr "Character1_LeftHandIndex1.s" "Character1_LeftHandIndex2.is";
connectAttr "Character1_LeftHandIndex2_scaleX.o" "Character1_LeftHandIndex2.sx";
connectAttr "Character1_LeftHandIndex2_scaleY.o" "Character1_LeftHandIndex2.sy";
connectAttr "Character1_LeftHandIndex2_scaleZ.o" "Character1_LeftHandIndex2.sz";
connectAttr "Character1_LeftHandIndex2_translateX.o" "Character1_LeftHandIndex2.tx"
		;
connectAttr "Character1_LeftHandIndex2_translateY.o" "Character1_LeftHandIndex2.ty"
		;
connectAttr "Character1_LeftHandIndex2_translateZ.o" "Character1_LeftHandIndex2.tz"
		;
connectAttr "Character1_LeftHandIndex2_rotateX.o" "Character1_LeftHandIndex2.rx"
		;
connectAttr "Character1_LeftHandIndex2_rotateY.o" "Character1_LeftHandIndex2.ry"
		;
connectAttr "Character1_LeftHandIndex2_rotateZ.o" "Character1_LeftHandIndex2.rz"
		;
connectAttr "Character1_LeftHandIndex2_visibility.o" "Character1_LeftHandIndex2.v"
		;
connectAttr "Character1_LeftHandIndex2.s" "Character1_LeftHandIndex3.is";
connectAttr "Character1_LeftHandIndex3_translateX.o" "Character1_LeftHandIndex3.tx"
		;
connectAttr "Character1_LeftHandIndex3_translateY.o" "Character1_LeftHandIndex3.ty"
		;
connectAttr "Character1_LeftHandIndex3_translateZ.o" "Character1_LeftHandIndex3.tz"
		;
connectAttr "Character1_LeftHandIndex3_scaleX.o" "Character1_LeftHandIndex3.sx";
connectAttr "Character1_LeftHandIndex3_scaleY.o" "Character1_LeftHandIndex3.sy";
connectAttr "Character1_LeftHandIndex3_scaleZ.o" "Character1_LeftHandIndex3.sz";
connectAttr "Character1_LeftHandIndex3_rotateX.o" "Character1_LeftHandIndex3.rx"
		;
connectAttr "Character1_LeftHandIndex3_rotateY.o" "Character1_LeftHandIndex3.ry"
		;
connectAttr "Character1_LeftHandIndex3_rotateZ.o" "Character1_LeftHandIndex3.rz"
		;
connectAttr "Character1_LeftHandIndex3_visibility.o" "Character1_LeftHandIndex3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandRing1.is";
connectAttr "Character1_LeftHandRing1_scaleX.o" "Character1_LeftHandRing1.sx";
connectAttr "Character1_LeftHandRing1_scaleY.o" "Character1_LeftHandRing1.sy";
connectAttr "Character1_LeftHandRing1_scaleZ.o" "Character1_LeftHandRing1.sz";
connectAttr "Character1_LeftHandRing1_translateX.o" "Character1_LeftHandRing1.tx"
		;
connectAttr "Character1_LeftHandRing1_translateY.o" "Character1_LeftHandRing1.ty"
		;
connectAttr "Character1_LeftHandRing1_translateZ.o" "Character1_LeftHandRing1.tz"
		;
connectAttr "Character1_LeftHandRing1_rotateX.o" "Character1_LeftHandRing1.rx";
connectAttr "Character1_LeftHandRing1_rotateY.o" "Character1_LeftHandRing1.ry";
connectAttr "Character1_LeftHandRing1_rotateZ.o" "Character1_LeftHandRing1.rz";
connectAttr "Character1_LeftHandRing1_visibility.o" "Character1_LeftHandRing1.v"
		;
connectAttr "Character1_LeftHandRing1.s" "Character1_LeftHandRing2.is";
connectAttr "Character1_LeftHandRing2_scaleX.o" "Character1_LeftHandRing2.sx";
connectAttr "Character1_LeftHandRing2_scaleY.o" "Character1_LeftHandRing2.sy";
connectAttr "Character1_LeftHandRing2_scaleZ.o" "Character1_LeftHandRing2.sz";
connectAttr "Character1_LeftHandRing2_translateX.o" "Character1_LeftHandRing2.tx"
		;
connectAttr "Character1_LeftHandRing2_translateY.o" "Character1_LeftHandRing2.ty"
		;
connectAttr "Character1_LeftHandRing2_translateZ.o" "Character1_LeftHandRing2.tz"
		;
connectAttr "Character1_LeftHandRing2_rotateX.o" "Character1_LeftHandRing2.rx";
connectAttr "Character1_LeftHandRing2_rotateY.o" "Character1_LeftHandRing2.ry";
connectAttr "Character1_LeftHandRing2_rotateZ.o" "Character1_LeftHandRing2.rz";
connectAttr "Character1_LeftHandRing2_visibility.o" "Character1_LeftHandRing2.v"
		;
connectAttr "Character1_LeftHandRing2.s" "Character1_LeftHandRing3.is";
connectAttr "Character1_LeftHandRing3_translateX.o" "Character1_LeftHandRing3.tx"
		;
connectAttr "Character1_LeftHandRing3_translateY.o" "Character1_LeftHandRing3.ty"
		;
connectAttr "Character1_LeftHandRing3_translateZ.o" "Character1_LeftHandRing3.tz"
		;
connectAttr "Character1_LeftHandRing3_scaleX.o" "Character1_LeftHandRing3.sx";
connectAttr "Character1_LeftHandRing3_scaleY.o" "Character1_LeftHandRing3.sy";
connectAttr "Character1_LeftHandRing3_scaleZ.o" "Character1_LeftHandRing3.sz";
connectAttr "Character1_LeftHandRing3_rotateX.o" "Character1_LeftHandRing3.rx";
connectAttr "Character1_LeftHandRing3_rotateY.o" "Character1_LeftHandRing3.ry";
connectAttr "Character1_LeftHandRing3_rotateZ.o" "Character1_LeftHandRing3.rz";
connectAttr "Character1_LeftHandRing3_visibility.o" "Character1_LeftHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_RightShoulder.is";
connectAttr "Character1_RightShoulder_scaleX.o" "Character1_RightShoulder.sx";
connectAttr "Character1_RightShoulder_scaleY.o" "Character1_RightShoulder.sy";
connectAttr "Character1_RightShoulder_scaleZ.o" "Character1_RightShoulder.sz";
connectAttr "Character1_RightShoulder_translateX.o" "Character1_RightShoulder.tx"
		;
connectAttr "Character1_RightShoulder_translateY.o" "Character1_RightShoulder.ty"
		;
connectAttr "Character1_RightShoulder_translateZ.o" "Character1_RightShoulder.tz"
		;
connectAttr "Character1_RightShoulder_rotateX.o" "Character1_RightShoulder.rx";
connectAttr "Character1_RightShoulder_rotateY.o" "Character1_RightShoulder.ry";
connectAttr "Character1_RightShoulder_rotateZ.o" "Character1_RightShoulder.rz";
connectAttr "Character1_RightShoulder_visibility.o" "Character1_RightShoulder.v"
		;
connectAttr "Character1_RightShoulder.s" "Character1_RightArm.is";
connectAttr "Character1_RightArm_scaleX.o" "Character1_RightArm.sx";
connectAttr "Character1_RightArm_scaleY.o" "Character1_RightArm.sy";
connectAttr "Character1_RightArm_scaleZ.o" "Character1_RightArm.sz";
connectAttr "Character1_RightArm_translateX.o" "Character1_RightArm.tx";
connectAttr "Character1_RightArm_translateY.o" "Character1_RightArm.ty";
connectAttr "Character1_RightArm_translateZ.o" "Character1_RightArm.tz";
connectAttr "Character1_RightArm_rotateX.o" "Character1_RightArm.rx";
connectAttr "Character1_RightArm_rotateY.o" "Character1_RightArm.ry";
connectAttr "Character1_RightArm_rotateZ.o" "Character1_RightArm.rz";
connectAttr "Character1_RightArm_visibility.o" "Character1_RightArm.v";
connectAttr "Character1_RightArm.s" "Character1_RightForeArm.is";
connectAttr "Character1_RightForeArm_scaleX.o" "Character1_RightForeArm.sx";
connectAttr "Character1_RightForeArm_scaleY.o" "Character1_RightForeArm.sy";
connectAttr "Character1_RightForeArm_scaleZ.o" "Character1_RightForeArm.sz";
connectAttr "Character1_RightForeArm_translateX.o" "Character1_RightForeArm.tx";
connectAttr "Character1_RightForeArm_translateY.o" "Character1_RightForeArm.ty";
connectAttr "Character1_RightForeArm_translateZ.o" "Character1_RightForeArm.tz";
connectAttr "Character1_RightForeArm_rotateX.o" "Character1_RightForeArm.rx";
connectAttr "Character1_RightForeArm_rotateY.o" "Character1_RightForeArm.ry";
connectAttr "Character1_RightForeArm_rotateZ.o" "Character1_RightForeArm.rz";
connectAttr "Character1_RightForeArm_visibility.o" "Character1_RightForeArm.v";
connectAttr "Character1_RightForeArm.s" "Character1_RightHand.is";
connectAttr "Character1_RightHand_scaleX.o" "Character1_RightHand.sx";
connectAttr "Character1_RightHand_scaleY.o" "Character1_RightHand.sy";
connectAttr "Character1_RightHand_scaleZ.o" "Character1_RightHand.sz";
connectAttr "Character1_RightHand_translateX.o" "Character1_RightHand.tx";
connectAttr "Character1_RightHand_translateY.o" "Character1_RightHand.ty";
connectAttr "Character1_RightHand_translateZ.o" "Character1_RightHand.tz";
connectAttr "Character1_RightHand_rotateX.o" "Character1_RightHand.rx";
connectAttr "Character1_RightHand_rotateY.o" "Character1_RightHand.ry";
connectAttr "Character1_RightHand_rotateZ.o" "Character1_RightHand.rz";
connectAttr "Character1_RightHand_visibility.o" "Character1_RightHand.v";
connectAttr "Character1_RightHand.s" "Character1_RightHandThumb1.is";
connectAttr "Character1_RightHandThumb1_scaleX.o" "Character1_RightHandThumb1.sx"
		;
connectAttr "Character1_RightHandThumb1_scaleY.o" "Character1_RightHandThumb1.sy"
		;
connectAttr "Character1_RightHandThumb1_scaleZ.o" "Character1_RightHandThumb1.sz"
		;
connectAttr "Character1_RightHandThumb1_translateX.o" "Character1_RightHandThumb1.tx"
		;
connectAttr "Character1_RightHandThumb1_translateY.o" "Character1_RightHandThumb1.ty"
		;
connectAttr "Character1_RightHandThumb1_translateZ.o" "Character1_RightHandThumb1.tz"
		;
connectAttr "Character1_RightHandThumb1_rotateX.o" "Character1_RightHandThumb1.rx"
		;
connectAttr "Character1_RightHandThumb1_rotateY.o" "Character1_RightHandThumb1.ry"
		;
connectAttr "Character1_RightHandThumb1_rotateZ.o" "Character1_RightHandThumb1.rz"
		;
connectAttr "Character1_RightHandThumb1_visibility.o" "Character1_RightHandThumb1.v"
		;
connectAttr "Character1_RightHandThumb1.s" "Character1_RightHandThumb2.is";
connectAttr "Character1_RightHandThumb2_scaleX.o" "Character1_RightHandThumb2.sx"
		;
connectAttr "Character1_RightHandThumb2_scaleY.o" "Character1_RightHandThumb2.sy"
		;
connectAttr "Character1_RightHandThumb2_scaleZ.o" "Character1_RightHandThumb2.sz"
		;
connectAttr "Character1_RightHandThumb2_translateX.o" "Character1_RightHandThumb2.tx"
		;
connectAttr "Character1_RightHandThumb2_translateY.o" "Character1_RightHandThumb2.ty"
		;
connectAttr "Character1_RightHandThumb2_translateZ.o" "Character1_RightHandThumb2.tz"
		;
connectAttr "Character1_RightHandThumb2_rotateX.o" "Character1_RightHandThumb2.rx"
		;
connectAttr "Character1_RightHandThumb2_rotateY.o" "Character1_RightHandThumb2.ry"
		;
connectAttr "Character1_RightHandThumb2_rotateZ.o" "Character1_RightHandThumb2.rz"
		;
connectAttr "Character1_RightHandThumb2_visibility.o" "Character1_RightHandThumb2.v"
		;
connectAttr "Character1_RightHandThumb2.s" "Character1_RightHandThumb3.is";
connectAttr "Character1_RightHandThumb3_translateX.o" "Character1_RightHandThumb3.tx"
		;
connectAttr "Character1_RightHandThumb3_translateY.o" "Character1_RightHandThumb3.ty"
		;
connectAttr "Character1_RightHandThumb3_translateZ.o" "Character1_RightHandThumb3.tz"
		;
connectAttr "Character1_RightHandThumb3_scaleX.o" "Character1_RightHandThumb3.sx"
		;
connectAttr "Character1_RightHandThumb3_scaleY.o" "Character1_RightHandThumb3.sy"
		;
connectAttr "Character1_RightHandThumb3_scaleZ.o" "Character1_RightHandThumb3.sz"
		;
connectAttr "Character1_RightHandThumb3_rotateX.o" "Character1_RightHandThumb3.rx"
		;
connectAttr "Character1_RightHandThumb3_rotateY.o" "Character1_RightHandThumb3.ry"
		;
connectAttr "Character1_RightHandThumb3_rotateZ.o" "Character1_RightHandThumb3.rz"
		;
connectAttr "Character1_RightHandThumb3_visibility.o" "Character1_RightHandThumb3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandIndex1.is";
connectAttr "Character1_RightHandIndex1_scaleX.o" "Character1_RightHandIndex1.sx"
		;
connectAttr "Character1_RightHandIndex1_scaleY.o" "Character1_RightHandIndex1.sy"
		;
connectAttr "Character1_RightHandIndex1_scaleZ.o" "Character1_RightHandIndex1.sz"
		;
connectAttr "Character1_RightHandIndex1_translateX.o" "Character1_RightHandIndex1.tx"
		;
connectAttr "Character1_RightHandIndex1_translateY.o" "Character1_RightHandIndex1.ty"
		;
connectAttr "Character1_RightHandIndex1_translateZ.o" "Character1_RightHandIndex1.tz"
		;
connectAttr "Character1_RightHandIndex1_rotateX.o" "Character1_RightHandIndex1.rx"
		;
connectAttr "Character1_RightHandIndex1_rotateY.o" "Character1_RightHandIndex1.ry"
		;
connectAttr "Character1_RightHandIndex1_rotateZ.o" "Character1_RightHandIndex1.rz"
		;
connectAttr "Character1_RightHandIndex1_visibility.o" "Character1_RightHandIndex1.v"
		;
connectAttr "Character1_RightHandIndex1.s" "Character1_RightHandIndex2.is";
connectAttr "Character1_RightHandIndex2_scaleX.o" "Character1_RightHandIndex2.sx"
		;
connectAttr "Character1_RightHandIndex2_scaleY.o" "Character1_RightHandIndex2.sy"
		;
connectAttr "Character1_RightHandIndex2_scaleZ.o" "Character1_RightHandIndex2.sz"
		;
connectAttr "Character1_RightHandIndex2_translateX.o" "Character1_RightHandIndex2.tx"
		;
connectAttr "Character1_RightHandIndex2_translateY.o" "Character1_RightHandIndex2.ty"
		;
connectAttr "Character1_RightHandIndex2_translateZ.o" "Character1_RightHandIndex2.tz"
		;
connectAttr "Character1_RightHandIndex2_rotateX.o" "Character1_RightHandIndex2.rx"
		;
connectAttr "Character1_RightHandIndex2_rotateY.o" "Character1_RightHandIndex2.ry"
		;
connectAttr "Character1_RightHandIndex2_rotateZ.o" "Character1_RightHandIndex2.rz"
		;
connectAttr "Character1_RightHandIndex2_visibility.o" "Character1_RightHandIndex2.v"
		;
connectAttr "Character1_RightHandIndex2.s" "Character1_RightHandIndex3.is";
connectAttr "Character1_RightHandIndex3_translateX.o" "Character1_RightHandIndex3.tx"
		;
connectAttr "Character1_RightHandIndex3_translateY.o" "Character1_RightHandIndex3.ty"
		;
connectAttr "Character1_RightHandIndex3_translateZ.o" "Character1_RightHandIndex3.tz"
		;
connectAttr "Character1_RightHandIndex3_scaleX.o" "Character1_RightHandIndex3.sx"
		;
connectAttr "Character1_RightHandIndex3_scaleY.o" "Character1_RightHandIndex3.sy"
		;
connectAttr "Character1_RightHandIndex3_scaleZ.o" "Character1_RightHandIndex3.sz"
		;
connectAttr "Character1_RightHandIndex3_rotateX.o" "Character1_RightHandIndex3.rx"
		;
connectAttr "Character1_RightHandIndex3_rotateY.o" "Character1_RightHandIndex3.ry"
		;
connectAttr "Character1_RightHandIndex3_rotateZ.o" "Character1_RightHandIndex3.rz"
		;
connectAttr "Character1_RightHandIndex3_visibility.o" "Character1_RightHandIndex3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandRing1.is";
connectAttr "Character1_RightHandRing1_scaleX.o" "Character1_RightHandRing1.sx";
connectAttr "Character1_RightHandRing1_scaleY.o" "Character1_RightHandRing1.sy";
connectAttr "Character1_RightHandRing1_scaleZ.o" "Character1_RightHandRing1.sz";
connectAttr "Character1_RightHandRing1_translateX.o" "Character1_RightHandRing1.tx"
		;
connectAttr "Character1_RightHandRing1_translateY.o" "Character1_RightHandRing1.ty"
		;
connectAttr "Character1_RightHandRing1_translateZ.o" "Character1_RightHandRing1.tz"
		;
connectAttr "Character1_RightHandRing1_rotateX.o" "Character1_RightHandRing1.rx"
		;
connectAttr "Character1_RightHandRing1_rotateY.o" "Character1_RightHandRing1.ry"
		;
connectAttr "Character1_RightHandRing1_rotateZ.o" "Character1_RightHandRing1.rz"
		;
connectAttr "Character1_RightHandRing1_visibility.o" "Character1_RightHandRing1.v"
		;
connectAttr "Character1_RightHandRing1.s" "Character1_RightHandRing2.is";
connectAttr "Character1_RightHandRing2_scaleX.o" "Character1_RightHandRing2.sx";
connectAttr "Character1_RightHandRing2_scaleY.o" "Character1_RightHandRing2.sy";
connectAttr "Character1_RightHandRing2_scaleZ.o" "Character1_RightHandRing2.sz";
connectAttr "Character1_RightHandRing2_translateX.o" "Character1_RightHandRing2.tx"
		;
connectAttr "Character1_RightHandRing2_translateY.o" "Character1_RightHandRing2.ty"
		;
connectAttr "Character1_RightHandRing2_translateZ.o" "Character1_RightHandRing2.tz"
		;
connectAttr "Character1_RightHandRing2_rotateX.o" "Character1_RightHandRing2.rx"
		;
connectAttr "Character1_RightHandRing2_rotateY.o" "Character1_RightHandRing2.ry"
		;
connectAttr "Character1_RightHandRing2_rotateZ.o" "Character1_RightHandRing2.rz"
		;
connectAttr "Character1_RightHandRing2_visibility.o" "Character1_RightHandRing2.v"
		;
connectAttr "Character1_RightHandRing2.s" "Character1_RightHandRing3.is";
connectAttr "Character1_RightHandRing3_translateX.o" "Character1_RightHandRing3.tx"
		;
connectAttr "Character1_RightHandRing3_translateY.o" "Character1_RightHandRing3.ty"
		;
connectAttr "Character1_RightHandRing3_translateZ.o" "Character1_RightHandRing3.tz"
		;
connectAttr "Character1_RightHandRing3_scaleX.o" "Character1_RightHandRing3.sx";
connectAttr "Character1_RightHandRing3_scaleY.o" "Character1_RightHandRing3.sy";
connectAttr "Character1_RightHandRing3_scaleZ.o" "Character1_RightHandRing3.sz";
connectAttr "Character1_RightHandRing3_rotateX.o" "Character1_RightHandRing3.rx"
		;
connectAttr "Character1_RightHandRing3_rotateY.o" "Character1_RightHandRing3.ry"
		;
connectAttr "Character1_RightHandRing3_rotateZ.o" "Character1_RightHandRing3.rz"
		;
connectAttr "Character1_RightHandRing3_visibility.o" "Character1_RightHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_Neck.is";
connectAttr "Character1_Neck_scaleX.o" "Character1_Neck.sx";
connectAttr "Character1_Neck_scaleY.o" "Character1_Neck.sy";
connectAttr "Character1_Neck_scaleZ.o" "Character1_Neck.sz";
connectAttr "Character1_Neck_translateX.o" "Character1_Neck.tx";
connectAttr "Character1_Neck_translateY.o" "Character1_Neck.ty";
connectAttr "Character1_Neck_translateZ.o" "Character1_Neck.tz";
connectAttr "Character1_Neck_rotateX.o" "Character1_Neck.rx";
connectAttr "Character1_Neck_rotateY.o" "Character1_Neck.ry";
connectAttr "Character1_Neck_rotateZ.o" "Character1_Neck.rz";
connectAttr "Character1_Neck_visibility.o" "Character1_Neck.v";
connectAttr "Character1_Neck.s" "Character1_Head.is";
connectAttr "Character1_Head_scaleX.o" "Character1_Head.sx";
connectAttr "Character1_Head_scaleY.o" "Character1_Head.sy";
connectAttr "Character1_Head_scaleZ.o" "Character1_Head.sz";
connectAttr "Character1_Head_translateX.o" "Character1_Head.tx";
connectAttr "Character1_Head_translateY.o" "Character1_Head.ty";
connectAttr "Character1_Head_translateZ.o" "Character1_Head.tz";
connectAttr "Character1_Head_rotateX.o" "Character1_Head.rx";
connectAttr "Character1_Head_rotateY.o" "Character1_Head.ry";
connectAttr "Character1_Head_rotateZ.o" "Character1_Head.rz";
connectAttr "Character1_Head_visibility.o" "Character1_Head.v";
connectAttr "Character1_Head.s" "eyes.is";
connectAttr "eyes_translateX.o" "eyes.tx";
connectAttr "eyes_translateY.o" "eyes.ty";
connectAttr "eyes_translateZ.o" "eyes.tz";
connectAttr "eyes_scaleX.o" "eyes.sx";
connectAttr "eyes_scaleY.o" "eyes.sy";
connectAttr "eyes_scaleZ.o" "eyes.sz";
connectAttr "eyes_rotateX.o" "eyes.rx";
connectAttr "eyes_rotateY.o" "eyes.ry";
connectAttr "eyes_rotateZ.o" "eyes.rz";
connectAttr "eyes_visibility.o" "eyes.v";
connectAttr "Character1_Head.s" "jaw.is";
connectAttr "jaw_lockInfluenceWeights.o" "jaw.liw";
connectAttr "jaw_translateX.o" "jaw.tx";
connectAttr "jaw_translateY.o" "jaw.ty";
connectAttr "jaw_translateZ.o" "jaw.tz";
connectAttr "jaw_scaleX.o" "jaw.sx";
connectAttr "jaw_scaleY.o" "jaw.sy";
connectAttr "jaw_scaleZ.o" "jaw.sz";
connectAttr "jaw_rotateX.o" "jaw.rx";
connectAttr "jaw_rotateY.o" "jaw.ry";
connectAttr "jaw_rotateZ.o" "jaw.rz";
connectAttr "jaw_visibility.o" "jaw.v";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Emeny@Archer_Fire_Bow.ma
