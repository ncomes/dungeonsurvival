//Maya ASCII 2016 scene
//Name: Emeny@Archer_Idle.ma
//Last modified: Tue, Nov 24, 2015 12:16:50 PM
//Codeset: 1252
requires maya "2016";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "97E309FA-43D7-27DC-ABA3-AE86E0686496";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 217.83242700225955 161.57362781025284 344.40927757865205 ;
	setAttr ".r" -type "double3" -14.138352729602367 32.999999999999957 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "F8C55DD4-407E-42A2-F694-70B3CABB04CB";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 449.85506562158508;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "FC7FD7BB-49DE-BD0D-7AA8-38AD4C2724CD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "49DB6577-4AA9-088B-4D08-6FA6096032CC";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "924095BC-4989-690F-DEB2-65BB85B6A690";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "524172D3-4DAD-DFC3-8311-53A2A04369DD";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "B2A1E637-4902-874F-F5BB-E382B3D51AD0";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "6837A2A6-440B-6B27-594E-299734A1E216";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "ref_frame";
	rename -uid "8CA0137E-4204-39E1-4F11-19877A6D2315";
	addAttr -ci true -sn "refFrame" -ln "refFrame" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "Character1_Hips" -p "ref_frame";
	rename -uid "A4EB9175-4C4F-9A9D-3C45-8AA5D9641923";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -19.036789801921167 ;
	setAttr ".ssc" no;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftUpLeg" -p "Character1_Hips";
	rename -uid "EE1809A9-4631-CAB5-BBE5-7280F2CD4889";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 90.000000000000014 -12.849604412773132 43.257528613448208 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftLeg" -p "Character1_LeftUpLeg";
	rename -uid "E8F6AD38-4BAC-3880-3C48-10B4F601FAF7";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -19.152297252867996 -13.416343813706657 -33.623675304480329 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFoot" -p "Character1_LeftLeg";
	rename -uid "19D42A5D-4DF2-9908-5AF2-0C8E0D20CB11";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 25.817647449536953 51.040150860538553 -112.05775014674906 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftToeBase" -p "Character1_LeftFoot";
	rename -uid "F6F354CA-425A-1DDA-C7BF-45B193DA6A90";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 43.727116820142825 -29.659481123592514 -4.6723850284209263 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle1" -p "Character1_LeftToeBase";
	rename -uid "9717BD3C-4C3E-8216-E4F1-7686C28D50B7";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 165.43734692409092 -20.35908719617909 10.197796850486212 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle2" -p "Character1_LeftFootMiddle1";
	rename -uid "FF91260F-4871-E656-10D7-7CB969CC7A11";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 127.90699782306312 -44.626258206212555 -101.65660999147298 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightUpLeg" -p "Character1_Hips";
	rename -uid "3B96F4F5-4DBF-1E70-48D9-E5BF91A00F5F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 133.25752861344816 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightLeg" -p "Character1_RightUpLeg";
	rename -uid "94F16698-4484-9202-FEBE-23B3BCE32B0D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -39.772585840344547 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFoot" -p "Character1_RightLeg";
	rename -uid "AB983CC6-475B-BCDD-9207-6086673905B9";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -53.712356932759 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightToeBase" -p "Character1_RightFoot";
	rename -uid "38E0AB09-4464-A3BD-6F41-C4ACA99692F0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 2.9245623946004821e-006 53.712356932758929 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle1" -p "Character1_RightToeBase";
	rename -uid "E221A89A-4421-0CDE-8CA0-0493357FD3DC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -4.5380835579220506e-006 -2.7209092876563258e-005 
		39.772585840347155 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle2" -p "Character1_RightFootMiddle1";
	rename -uid "331DDF88-430C-4D8C-B017-31821142981B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -2.8539668357113417e-005 0.00011485155389716205 -133.25752861351003 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine" -p "Character1_Hips";
	rename -uid "C71F0833-40B9-B1FB-D574-C9BFBBB475D0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 133.25752861344816 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine1" -p "Character1_Spine";
	rename -uid "AE760A19-46BE-B35E-4198-168EBC7967B6";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -34.34645244194413 -26.596597123422644 16.99576897690589 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftShoulder" -p "Character1_Spine1";
	rename -uid "02E3E92B-4950-86D6-881D-9C8453DDD2C6";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -166.39102833412767 21.832119962557094 -120.00622813437471 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftArm" -p "Character1_LeftShoulder";
	rename -uid "845195D6-4B3A-692A-BBC9-C8AACDF73013";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -172.11387269090633 -44.88241940154802 -37.940590556196071 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftForeArm" -p "Character1_LeftArm";
	rename -uid "7F8D145E-44E4-5675-E7F5-D2AB9018A385";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -84.022423618581243 -14.673598567163568 71.813326221989513 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHand" -p "Character1_LeftForeArm";
	rename -uid "6F9092E0-4A93-8827-4617-E997E7C31B01";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -24.404340271578391 -34.932697344706668 122.26498801320882 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr -k on ".liw" no;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb1" -p "Character1_LeftHand";
	rename -uid "328A14C1-43D5-1CBC-D6F2-8BA4BE75D347";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.1043965330995 61.333484361561069 1.8740740862626108 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb2" -p "Character1_LeftHandThumb1";
	rename -uid "35C623A8-4B86-2CB5-9D52-1B9DBC2D7E86";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -103.44292663098258 20.93424016453444 169.56041330531272 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb3" -p "Character1_LeftHandThumb2";
	rename -uid "1DFDCECF-4BA1-7BA5-B375-44A9DD1BD12A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 14.174484134960574 -59.672502756712539 -19.020394805426264 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex1" -p "Character1_LeftHand";
	rename -uid "B4CB674B-4CC4-1C95-4C3B-53A30311BA68";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -150.59621970507803 75.681392634287164 -124.28723884791442 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex2" -p "Character1_LeftHandIndex1";
	rename -uid "A886DB63-4B96-175A-A161-829DB60E1CF9";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -41.093804604175162 -57.602285575728224 -69.242456746392662 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex3" -p "Character1_LeftHandIndex2";
	rename -uid "699DA831-479D-DA9D-770B-E79081FA3F87";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.108297963098213 8.7979835742630801 27.088028901385748 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing1" -p "Character1_LeftHand";
	rename -uid "DCF5A7CA-4908-9F4A-1455-1BB8E94A6AF1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -141.42437885137375 63.919742192600388 -113.78475086453638 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing2" -p "Character1_LeftHandRing1";
	rename -uid "BB7DEEED-4184-1ECC-19E8-8FA0AD06C4FB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -35.368646977348 -76.384823071466059 -48.755963146359043 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing3" -p "Character1_LeftHandRing2";
	rename -uid "C1B37A2C-4663-3604-1408-70B3DBCD9DD9";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 9.6312888734270121 39.523687300631337 15.3258182753707 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightShoulder" -p "Character1_Spine1";
	rename -uid "4F112995-4C90-69E3-A265-BE9FB37B6B24";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -164.54352138244408 34.957040538466451 -116.14754006477595 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightArm" -p "Character1_RightShoulder";
	rename -uid "5761EDED-4135-3791-DD49-C9A5F1C9F094";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -173.75456654457099 -11.164646449014084 -45.537881339438307 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightForeArm" -p "Character1_RightArm";
	rename -uid "50E02CB9-4C36-D43F-69C6-1C91BC08B3FB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 22.753833922449758 50.186429787105524 -101.53518723839815 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHand" -p "Character1_RightForeArm";
	rename -uid "F8EE48BE-484B-BACA-5CDD-409E4ECC51ED";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 48.334026046900341 -3.7701083025157853 -29.882905994214536 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb1" -p "Character1_RightHand";
	rename -uid "05F98996-48B1-8ABD-323F-2C9B1AD12B5B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb2" -p "Character1_RightHandThumb1";
	rename -uid "B948A538-43A1-B33C-CFA8-8590F08BD1BD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb3" -p "Character1_RightHandThumb2";
	rename -uid "A7D1D199-4455-88BA-418B-08BE2E2538BD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex1" -p "Character1_RightHand";
	rename -uid "07697209-47E0-22D9-FB58-77891FEF0EC2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex2" -p "Character1_RightHandIndex1";
	rename -uid "99500C55-4FCC-CF2A-169D-20B8EEC49145";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex3" -p "Character1_RightHandIndex2";
	rename -uid "93FCB1A8-4C66-3144-C114-07A4E142A38B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing1" -p "Character1_RightHand";
	rename -uid "86D4E23C-4413-D2E8-F41E-D89313FCBCE5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing2" -p "Character1_RightHandRing1";
	rename -uid "AD77CADA-4393-436F-7F62-7A9AE8CD6942";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing3" -p "Character1_RightHandRing2";
	rename -uid "A37AF2F6-4003-957F-7300-E9B334F8B70F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Neck" -p "Character1_Spine1";
	rename -uid "72B8818F-405E-4A2A-076C-9C8C4DDDACE1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 48.229963067601958 30.670012388143238 129.35864775594831 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Head" -p "Character1_Neck";
	rename -uid "CBDE3247-4A8A-96C9-F0F8-B0B5DCF2A5F2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -14.36476107543206 22.377462763663633 -109.58762475494268 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "eyes" -p "Character1_Head";
	rename -uid "C907F3BC-4842-EF89-44FC-52984625601F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -157.21319586594478 -21.144487689504771 -3.4677530572789341 ;
	setAttr ".radi" 4.5;
	setAttr ".fbxID" 5;
createNode joint -n "jaw" -p "Character1_Head";
	rename -uid "E683A6CE-4026-9927-35D4-AEB00B228A6D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -157.21319586594478 -21.144487689504771 -3.4677530572789341 ;
	setAttr ".radi" 0.5;
	setAttr -k on ".liw" no;
	setAttr ".fbxID" 5;
createNode animCurveTU -n "Character1_LeftHand_lockInfluenceWeights";
	rename -uid "8607325B-45A6-3887-7779-7894E0501650";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "jaw_lockInfluenceWeights";
	rename -uid "05E7FB72-41FA-FC10-E67A-CE87190C1C69";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "Character1_Hips_translateX";
	rename -uid "786FC961-42CD-ED01-ED83-AB9CF30700A7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -1.7785836458206177 1 -1.8602595329284668
		 2 -1.9321936368942261 3 -1.9948784112930298 4 -2.0488150119781494 5 -2.0944852828979492
		 6 -2.1323871612548828 7 -2.1630213260650635 8 -2.186920166015625 9 -2.2045624256134033
		 10 -2.2164528369903564 11 -2.2230987548828125 12 -2.225024938583374 13 -2.2227177619934082
		 14 -2.2166767120361328 15 -2.2053048610687256 16 -2.1866507530212402 17 -2.1609807014465332
		 18 -2.1285598278045654 19 -2.0896530151367187 20 -2.0445244312286377 21 -1.9934391975402832
		 22 -1.9366607666015625 23 -1.8744759559631348 24 -1.8071300983428955 25 -1.7348852157592773
		 26 -1.6580057144165039 27 -1.5767556428909302 28 -1.491398811340332 29 -1.4005765914916992
		 30 -1.3027914762496948 31 -1.1981855630874634 32 -1.0869084596633911 33 -0.96911871433258057
		 34 -0.84501868486404419 35 -0.7147713303565979 36 -0.57857495546340942 37 -0.43666580319404602
		 38 -0.28930565714836121 39 -0.13673289120197296 40 0.02076542004942894 41 0.15780690312385559
		 42 0.25196972489356995 43 0.30740731954574585 44 0.32829573750495911 45 0.31882253289222717
		 46 0.28317725658416748 47 0.22551135718822479 48 0.15001066029071808 49 0.060849383473396301
		 50 -0.037883736193180084 51 -0.14201211929321289 52 -0.24743698537349701 53 -0.35009151697158813
		 54 -0.44585782289505005 55 -0.53066259622573853 56 -0.60043221712112427 57 -0.6650351881980896
		 58 -0.73736542463302612 59 -0.81625288724899292 60 -0.90051645040512085 61 -0.98896950483322144
		 62 -1.0804347991943359 63 -1.1737281084060669 64 -1.2676504850387573 65 -1.3610219955444336
		 66 -1.4526674747467041 67 -1.5414259433746338 68 -1.6261191368103027 69 -1.7055655717849731
		 70 -1.7785836458206177;
createNode animCurveTL -n "Character1_Hips_translateY";
	rename -uid "9CB6189C-49EB-C5C7-504A-8BBD9FF124BC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 42.430355072021484 1 42.431655883789063
		 2 42.400917053222656 3 42.341995239257813 4 42.258773803710938 5 42.155178070068359
		 6 42.035114288330078 7 41.902462005615234 8 41.761032104492188 9 41.614765167236328
		 10 41.467521667480469 11 41.323135375976563 12 41.185398101806641 13 41.058185577392578
		 14 40.945331573486328 15 40.846279144287109 16 40.757480621337891 17 40.678245544433594
		 18 40.607906341552734 19 40.545783996582031 20 40.4912109375 21 40.443515777587891
		 22 40.402034759521484 23 40.366035461425781 24 40.334918975830078 25 40.308021545410156
		 26 40.284687042236328 27 40.264251708984375 28 40.246063232421875 29 40.260158538818359
		 30 40.330150604248047 31 40.445693969726563 32 40.596443176269531 33 40.772048950195313
		 34 40.962081909179687 35 41.156211853027344 36 41.344036102294922 37 41.515083312988281
		 38 41.658824920654297 39 41.764797210693359 40 41.822471618652344 41 41.835174560546875
		 42 41.816215515136719 43 41.769973754882813 44 41.700889587402344 45 41.613376617431641
		 46 41.511863708496094 47 41.400745391845703 48 41.284481048583984 49 41.167560577392578
		 50 41.054344177246094 51 40.949420928955078 52 40.857273101806641 53 40.782325744628906
		 54 40.729198455810547 55 40.702419281005859 56 40.706512451171875 57 40.748748779296875
		 58 40.832923889160156 59 40.951694488525391 60 41.097713470458984 61 41.263618469238281
		 62 41.441989898681641 63 41.625434875488281 64 41.806598663330078 65 41.978073120117188
		 66 42.132438659667969 67 42.262252807617188 68 42.360111236572266 69 42.418617248535156
		 70 42.430355072021484;
createNode animCurveTL -n "Character1_Hips_translateZ";
	rename -uid "1EBDE72F-424A-A0EE-9471-65AF509FBC49";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -9.8668909072875977 1 -9.8749980926513672
		 2 -9.8798093795776367 3 -9.8816261291503906 4 -9.8807497024536133 5 -9.877507209777832
		 6 -9.8722124099731445 7 -9.8651771545410156 8 -9.8566818237304687 9 -9.8470668792724609
		 10 -9.8366422653198242 11 -9.8257160186767578 12 -9.8145732879638672 13 -9.8035392761230469
		 14 -9.7929248809814453 15 -9.7872552871704102 16 -9.7900714874267578 17 -9.800166130065918
		 18 -9.8163270950317383 19 -9.8373498916625977 20 -9.8620281219482422 21 -9.8891611099243164
		 22 -9.9175443649291992 23 -9.9459505081176758 24 -9.9732046127319336 25 -9.9981050491333008
		 26 -10.019454002380371 27 -10.036051750183105 28 -10.046696662902832 29 -10.033437728881836
		 30 -9.9839363098144531 31 -9.9051523208618164 32 -9.8040323257446289 33 -9.6875219345092773
		 34 -9.5625419616699219 35 -9.4360809326171875 36 -9.3151206970214844 37 -9.2066326141357422
		 38 -9.1175956726074219 39 -9.0550365447998047 40 -9.0259466171264648 41 -9.0137758255004883
		 42 -8.9981632232666016 43 -8.9803934097290039 44 -8.9617586135864258 45 -8.9435539245605469
		 46 -8.9270725250244141 47 -8.9135761260986328 48 -8.9043684005737305 49 -8.9007558822631836
		 50 -8.9039726257324219 51 -8.9153614044189453 52 -8.9362077713012695 53 -8.9677801132202148
		 54 -9.011418342590332 55 -9.068425178527832 56 -9.1401100158691406 57 -9.2190761566162109
		 58 -9.2965450286865234 59 -9.3718576431274414 60 -9.4443483352661133 61 -9.5133447647094727
		 62 -9.5781660079956055 63 -9.6381378173828125 64 -9.6926088333129883 65 -9.7409191131591797
		 66 -9.7824039459228516 67 -9.8163938522338867 68 -9.8422384262084961 69 -9.8592872619628906
		 70 -9.8668909072875977;
createNode animCurveTU -n "Character1_Hips_scaleX";
	rename -uid "ECEE54D5-49CA-4B8B-8812-668C88E0BB6E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_Hips_scaleY";
	rename -uid "422CB4BB-453D-C729-7F3D-089D2967F29A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_Hips_scaleZ";
	rename -uid "CA5ED4F5-463E-480E-4C6A-82B557D95F16";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Hips_rotateX";
	rename -uid "C4737630-4B87-4E11-D8F2-A88ABF197F42";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -10.624299049377441 1 -10.528545379638672
		 2 -10.39854621887207 3 -10.239575386047363 4 -10.05692195892334 5 -9.8558664321899414
		 6 -9.6416711807250977 7 -9.419560432434082 8 -9.1947174072265625 9 -8.9722929000854492
		 10 -8.7573919296264648 11 -8.555084228515625 12 -8.3704137802124023 13 -8.2084236145019531
		 14 -8.0741615295410156 15 -7.9669618606567392 16 -7.8819804191589355 17 -7.8172764778137207
		 18 -7.7709341049194336 19 -7.7410469055175781 20 -7.725698471069335 21 -7.7229523658752441
		 22 -7.7308406829833993 23 -7.7473440170288095 24 -7.7704148292541495 25 -7.797947883605957
		 26 -7.8277888298034677 27 -7.8577351570129403 28 -7.885540485382081 29 -7.9104757308959961
		 30 -7.9358539581298819 31 -7.9615988731384277 32 -7.9876308441162109 33 -8.0138664245605469
		 34 -8.0402231216430664 35 -8.0666074752807617 36 -8.0929269790649414 37 -8.1190967559814453
		 38 -8.1450309753417969 39 -8.1706361770629883 40 -8.195836067199707 41 -8.2515172958374023
		 42 -8.3640174865722656 43 -8.5265693664550781 44 -8.7324810028076172 45 -8.9750881195068359
		 46 -9.2477121353149414 47 -9.5436420440673828 48 -9.8560791015625 49 -10.178125381469727
		 50 -10.502791404724121 51 -10.822957992553711 52 -11.131404876708984 53 -11.420807838439941
		 54 -11.683772087097168 55 -11.912846565246582 56 -12.100565910339355 57 -12.224485397338867
		 58 -12.26734447479248 59 -12.240432739257813 60 -12.155076026916504 61 -12.022650718688965
		 62 -11.854598045349121 63 -11.662428855895996 64 -11.457740783691406 65 -11.252202987670898
		 66 -11.057561874389648 67 -10.885621070861816 68 -10.748215675354004 69 -10.657179832458496
		 70 -10.624299049377441;
createNode animCurveTA -n "Character1_Hips_rotateY";
	rename -uid "B8CDB2FB-463B-74A6-846C-1399FFBDD92A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 12.893464088439941 1 12.904102325439453
		 2 12.870418548583984 3 12.799824714660645 4 12.699740409851074 5 12.577557563781738
		 6 12.440699577331543 7 12.296610832214355 8 12.152801513671875 9 12.01671028137207
		 10 11.895835876464844 11 11.797693252563477 12 11.72984790802002 13 11.699795722961426
		 14 11.715060234069824 15 11.769748687744141 16 11.852015495300293 17 11.958353042602539
		 18 12.085219383239746 19 12.229052543640137 20 12.386274337768555 21 12.553300857543945
		 22 12.726551055908203 23 12.902493476867676 24 13.077529907226563 25 13.248110771179199
		 26 13.410710334777832 27 13.56181812286377 28 13.697945594787598 29 13.820733070373535
		 30 13.932233810424805 31 14.031843185424805 32 14.118954658508301 33 14.192967414855957
		 34 14.253324508666992 35 14.299399375915527 36 14.330596923828127 37 14.346351623535156
		 38 14.346110343933105 39 14.329273223876953 40 14.29527473449707 41 14.208928108215332
		 42 14.043000221252441 43 13.808712005615234 44 13.517332077026367 45 13.180159568786621
		 46 12.808499336242676 47 12.413674354553223 48 12.006922721862793 49 11.599423408508301
		 50 11.202352523803711 51 10.826672554016113 52 10.48331356048584 53 10.183122634887695
		 54 9.936762809753418 55 9.7548646926879883 56 9.6479969024658203 57 9.6443710327148437
		 58 9.7418422698974609 59 9.9240732192993164 60 10.174797058105469 61 10.477795600891113
		 62 10.816904067993164 63 11.175971984863281 64 11.538838386535645 65 11.889376640319824
		 66 12.211474418640137 67 12.489045143127441 68 12.70598030090332 69 12.846161842346191
		 70 12.893464088439941;
createNode animCurveTA -n "Character1_Hips_rotateZ";
	rename -uid "7DFB6980-4FF1-E126-6637-989BB56F0EB0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -5.2601604461669922 1 -5.2601604461669922
		 2 -5.2507119178771973 3 -5.2316074371337891 4 -5.2049322128295898 5 -5.172856330871582
		 6 -5.1374335289001465 7 -5.1006393432617188 8 -5.0642123222351074 9 -5.0300726890563965
		 10 -4.999913215637207 11 -4.9753375053405762 12 -4.9577751159667969 13 -4.948822021484375
		 14 -4.9499893188476563 15 -4.9718317985534668 16 -5.0258750915527344 17 -5.1085386276245117
		 18 -5.2162809371948242 19 -5.3455886840820313 20 -5.4929599761962891 21 -5.6548962593078613
		 22 -5.8278913497924805 23 -6.0083112716674805 24 -6.1927165985107422 25 -6.3775539398193359
		 26 -6.5592360496520996 27 -6.7341499328613281 28 -6.8986577987670898 29 -7.0526204109191895
		 30 -7.19913673400879 31 -7.3376173973083496 32 -7.4674468040466309 33 -7.587994098663331
		 34 -7.6984882354736328 35 -7.7983622550964364 36 -7.8869619369506845 37 -7.9635491371154785
		 38 -8.0273504257202148 39 -8.077733039855957 40 -8.1139507293701172 41 -8.1208562850952148
		 42 -8.0868644714355469 43 -8.0162534713745117 44 -7.9132595062255859 45 -7.7821450233459473
		 46 -7.6272497177124023 47 -7.4529209136962882 48 -7.2638158798217773 49 -7.0647826194763184
		 50 -6.860600471496582 51 -6.6566495895385742 52 -6.4582548141479492 53 -6.2708277702331543
		 54 -6.1001801490783691 55 -5.9520392417907715 56 -5.832160472869873 57 -5.7347936630249023
		 58 -5.6470279693603516 59 -5.568659782409668 60 -5.4992356300354004 61 -5.4382228851318359
		 62 -5.3850898742675781 63 -5.3395023345947266 64 -5.3014430999755859 65 -5.2710976600646973
		 66 -5.2489233016967773 67 -5.2355756759643555 68 -5.2319679260253906 69 -5.2390933036804199
		 70 -5.2578549385070801;
createNode animCurveTU -n "Character1_Hips_visibility";
	rename -uid "B35CCF48-491B-ADF9-90D1-66828666F8DA";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftUpLeg_translateX";
	rename -uid "CDC54677-417B-1D19-E9D3-2B944536A9F1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.784505844116211;
createNode animCurveTL -n "Character1_LeftUpLeg_translateY";
	rename -uid "F598F894-4CAE-0BF4-E1E3-BD9070520B2E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5774786561451037e-006;
createNode animCurveTL -n "Character1_LeftUpLeg_translateZ";
	rename -uid "5918EC62-4F17-B6A8-23C5-16A836118828";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0264549255371094;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleX";
	rename -uid "F2730700-44C0-DABB-6C8E-2989140EDAD9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleY";
	rename -uid "69CCB9B6-4C46-7C91-92B0-BDA09E2F5C10";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleZ";
	rename -uid "80F28335-45CE-22FD-75B1-DBBF2B86143E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateX";
	rename -uid "84BD7F9A-4F48-6D6E-ECA8-548603237D76";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -2.6662230491638184 1 -2.7537171840667725
		 2 -2.8849501609802246 3 -3.0557091236114502 4 -3.2613179683685303 5 -3.4969213008880615
		 6 -3.7577149868011475 7 -4.0391497611999512 8 -4.3371243476867676 9 -4.6478404998779297
		 10 -4.9680652618408203 11 -5.2951173782348633 12 -5.6267895698547363 13 -5.9612274169921875
		 14 -6.2969260215759277 15 -6.6480231285095215 16 -7.0283193588256836 17 -7.4317197799682617
		 18 -7.8520298004150382 19 -8.2830572128295898 20 -8.7186088562011719 21 -9.1525306701660156
		 22 -9.5786991119384766 23 -9.9911041259765625 24 -10.38365364074707 25 -10.75029468536377
		 26 -11.084966659545898 27 -11.38154125213623 28 -11.633753776550293 29 -11.739617347717285
		 30 -11.623092651367188 31 -11.32077693939209 32 -10.868825912475586 33 -10.303430557250977
		 34 -9.6612720489501953 35 -8.979736328125 36 -8.2972240447998047 37 -7.6532392501831055
		 38 -7.088343620300293 39 -6.6439471244812012 40 -6.3618931770324707 41 -6.1167879104614258
		 42 -5.7673449516296387 43 -5.3302764892578125 44 -4.8213019371032715 45 -4.2556595802307129
		 46 -3.6483752727508545 47 -3.0145342350006104 48 -2.3695013523101807 49 -1.7289533615112305
		 50 -1.1090197563171387 51 -0.52608227729797363 52 0.0031471997499465942 53 0.46179750561714172
		 54 0.83316284418106079 55 1.100741982460022 56 1.2484984397888184 57 1.2573273181915283
		 58 1.1475284099578857 59 0.93956601619720459 60 0.65379559993743896 61 0.31002071499824524
		 62 -0.072888366878032684 63 -0.47715225815773005 64 -0.88622897863388062 65 -1.2849434614181519
		 66 -1.6593911647796631 67 -1.9967824220657351 68 -2.2850461006164551 69 -2.5123128890991211
		 70 -2.6662230491638184;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateY";
	rename -uid "45393983-421C-B5AE-AD2F-19B83A32EB44";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 18.98084831237793 1 19.125082015991211
		 2 19.263881683349609 3 19.397724151611328 4 19.526826858520508 5 19.651342391967773
		 6 19.771343231201172 7 19.886922836303711 8 19.998184204101563 9 20.105440139770508
		 10 20.209049224853516 11 20.309497833251953 12 20.407281875610352 13 20.503084182739258
		 14 20.597570419311523 15 20.696466445922852 16 20.809047698974609 17 20.93353271484375
		 18 21.068099975585937 19 21.210874557495117 20 21.359886169433594 21 21.513067245483398
		 22 21.668256759643555 23 21.823169708251953 24 21.975584030151367 25 22.123231887817383
		 26 22.263916015625 27 22.395563125610352 28 22.516313552856445 29 22.615056991577148
		 30 22.684711456298828 31 22.728120803833008 32 22.747640609741211 33 22.744993209838867
		 34 22.721118927001953 35 22.676292419433594 36 22.610052108764648 37 22.521236419677734
		 38 22.408058166503906 39 22.268138885498047 40 22.098339080810547 41 21.932355880737305
		 42 21.803804397583008 43 21.707805633544922 44 21.63874626159668 45 21.590606689453125
		 46 21.557210922241211 47 21.532426834106445 48 21.510467529296875 49 21.485996246337891
		 50 21.45421028137207 51 21.411102294921875 52 21.353317260742187 53 21.278196334838867
		 54 21.183784484863281 55 21.068599700927734 56 20.931522369384766 57 20.773954391479492
		 58 20.591398239135742 59 20.391654968261719 60 20.182518005371094 61 19.971670150756836
		 62 19.766517639160156 63 19.574029922485352 64 19.400550842285156 65 19.251535415649414
		 66 19.13136100769043 67 19.043149948120117 68 18.988681793212891 69 18.968296051025391
		 70 18.98084831237793;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateZ";
	rename -uid "D1E28EF7-478A-C528-7B25-8A888826DCB3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 2.1728212833404541 1 2.1658222675323486
		 2 2.136364221572876 3 2.0868258476257324 4 2.0196380615234375 5 1.9372248649597166
		 6 1.8419145345687868 7 1.7358834743499756 8 1.6210840940475464 9 1.4993568658828735
		 10 1.3722933530807495 11 1.2412575483322144 12 1.1074107885360718 13 0.97179430723190319
		 14 0.83529508113861084 15 0.71607959270477295 16 0.62542814016342163 17 0.55442523956298828
		 18 0.4942876398563385 19 0.43637958168983459 20 0.37222632765769958 21 0.29353171586990356
		 22 0.19217604398727417 23 0.060166411101818092 24 -0.11027248203754425 25 -0.32680949568748474
		 26 -0.59704160690307617 27 -0.92851412296295166 28 -1.3287827968597412 29 -1.8092234134674074
		 30 -2.3628599643707275 31 -2.9721510410308838 32 -3.6195218563079838 33 -4.2873783111572266
		 34 -4.9581632614135742 35 -5.6143779754638672 36 -6.2387051582336426 37 -6.8140835762023926
		 38 -7.3237433433532715 39 -7.751192569732666 40 -8.0801448822021484 41 -8.3023452758789063
		 42 -8.4264497756958008 43 -8.4608898162841797 44 -8.4135637283325195 45 -8.2922277450561523
		 46 -8.1047649383544922 47 -7.8594088554382315 48 -7.5648612976074219 49 -7.2303419113159171
		 50 -6.8656177520751953 51 -6.4808125495910645 52 -6.0863742828369141 53 -5.6928749084472656
		 54 -5.3106956481933594 55 -4.9498920440673828 56 -4.6198959350585937 57 -4.3081274032592773
		 58 -3.9749240875244136 59 -3.6166553497314458 60 -3.2297909259796143 61 -2.8114893436431885
		 62 -2.3600707054138184 63 -1.8752635717391968 64 -1.3583531379699707 65 -0.81223207712173462
		 66 -0.24123911559581757 67 0.3490932285785675 68 0.95248913764953624 69 1.5624384880065918
		 70 2.1728212833404541;
createNode animCurveTU -n "Character1_LeftUpLeg_visibility";
	rename -uid "917B3D3C-4472-683A-B1A9-9E9A24C024F9";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftLeg_translateX";
	rename -uid "2C2B9A7C-4EAE-AA29-542A-2F912E28C419";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.968876838684082;
createNode animCurveTL -n "Character1_LeftLeg_translateY";
	rename -uid "E8035B60-4F37-997C-6634-239A6D122110";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.2585945129394531;
createNode animCurveTL -n "Character1_LeftLeg_translateZ";
	rename -uid "A942720C-4B09-C791-C6C6-C3BFD68F1F85";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 18.197568893432617;
createNode animCurveTU -n "Character1_LeftLeg_scaleX";
	rename -uid "AB7BB0C0-4879-5EFE-7809-04A362D72954";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_LeftLeg_scaleY";
	rename -uid "63D63ABC-4380-AD5B-5E2B-B593CDD00909";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftLeg_scaleZ";
	rename -uid "C83F1817-495D-0785-4332-349F5F4A0083";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftLeg_rotateX";
	rename -uid "911D5146-4A8B-72E4-53D3-A79E62570843";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 46.733242034912109 1 46.672042846679688
		 2 46.77703857421875 3 47.029201507568359 4 47.408733367919922 5 47.895416259765625
		 6 48.468994140625 7 49.109428405761719 8 49.797237396240234 9 50.512969970703125
		 10 51.237873077392578 11 51.953849792480469 12 52.643299102783203 13 53.288959503173828
		 14 53.873989105224609 15 54.41900634765625 16 54.958232879638672 17 55.489429473876953
		 18 56.010082244873047 19 56.517601013183594 20 57.009273529052734 21 57.482349395751953
		 22 57.934028625488288 23 58.361728668212898 24 58.762615203857415 25 59.134029388427734
		 26 59.473453521728516 27 59.7784423828125 28 60.046627044677734 29 60.121147155761719
		 30 59.887214660644531 31 59.403305053710938 32 58.725868225097656 33 57.910137176513665
		 34 57.011089324951172 35 56.083763122558594 36 55.184261322021484 37 54.370147705078125
		 38 53.700519561767578 39 53.235580444335938 40 53.036094665527344 41 53.017711639404297
		 42 53.051235198974609 43 53.128746032714844 44 53.241222381591797 45 53.379261016845703
		 46 53.533344268798828 47 53.694255828857422 48 53.853122711181641 49 54.001548767089844
		 50 54.132110595703125 51 54.237434387207031 52 54.310951232910156 53 54.346569061279297
		 54 54.337844848632813 55 54.278385162353516 56 54.161296844482422 57 53.944084167480469
		 58 53.581172943115234 59 53.093727111816406 60 52.502754211425781 61 51.829975128173828
		 62 51.098678588867188 63 50.334133148193359 64 49.564029693603516 65 48.819049835205078
		 66 48.132854461669922 67 47.541965484619141 68 47.085063934326172 69 46.802150726318359
		 70 46.733242034912109;
createNode animCurveTA -n "Character1_LeftLeg_rotateY";
	rename -uid "BFDC6281-4F28-E4CD-E4BF-0F9D19A2CC6A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 23.163423538208008 1 23.183258056640625
		 2 23.241331100463867 3 23.330821990966797 4 23.444377899169922 5 23.574453353881836
		 6 23.713420867919922 7 23.853780746459961 8 23.98820686340332 9 24.109966278076172
		 10 24.212678909301758 11 24.290430068969727 12 24.337726593017578 13 24.349655151367188
		 14 24.321645736694336 15 24.283514022827148 16 24.261648178100586 17 24.248208999633789
		 18 24.235349655151367 19 24.215272903442383 20 24.180280685424805 21 24.122829437255859
		 22 24.03553581237793 23 23.91107177734375 24 23.742467880249023 25 23.522806167602539
		 26 23.24522590637207 27 22.902900695800781 28 22.488945007324219 29 21.977846145629883
		 30 21.363910675048828 31 20.668996810913086 32 19.914327621459961 33 19.121736526489258
		 34 18.314323425292969 35 17.517154693603516 36 16.757001876831055 37 16.062154769897461
		 38 15.462079048156738 39 14.98713493347168 40 14.667860031127928 41 14.448323249816895
		 42 14.253500938415527 43 14.085115432739258 44 13.944710731506348 45 13.833643913269043
		 46 13.753134727478027 47 13.704171180725098 48 13.687784194946289 49 13.704939842224121
		 50 13.756282806396484 51 13.842792510986328 52 13.965188026428223 53 14.124079704284668
		 54 14.320330619812012 55 14.554643630981444 56 14.827713012695314 57 15.152137756347656
		 58 15.564194679260254 59 16.051593780517578 60 16.602025985717773 61 17.203275680541992
		 62 17.843318939208984 63 18.510631561279297 64 19.194435119628906 65 19.884809494018555
		 66 20.572925567626953 67 21.251091003417969 68 21.912757873535156 69 22.55211067199707
		 70 23.163423538208008;
createNode animCurveTA -n "Character1_LeftLeg_rotateZ";
	rename -uid "70A5D1F2-47A8-7E5A-08B3-F5B330A7A078";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 13.390972137451172 1 13.344562530517578
		 2 13.402797698974609 3 13.554675102233887 4 13.788895606994629 5 14.093887329101563
		 6 14.457921981811523 7 14.869163513183594 8 15.315838813781738 9 15.785767555236815
		 10 16.266878128051758 11 16.74717903137207 12 17.21473503112793 13 17.657571792602539
		 14 18.06389045715332 15 18.443347930908203 16 18.816249847412109 17 19.182346343994141
		 18 19.540962219238281 19 19.89112663269043 20 20.231565475463867 21 20.560756683349609
		 22 20.876970291137695 23 21.178470611572266 24 21.463211059570312 25 21.729190826416016
		 26 21.974462509155273 27 22.197124481201172 28 22.395366668701172 29 22.466304779052734
		 30 22.33888053894043 31 22.058345794677734 32 21.668291091918945 33 21.209720611572266
		 34 20.72052001953125 35 20.23480224609375 36 19.783103942871094 37 19.392570495605469
		 38 19.087541580200195 39 18.890407562255859 40 18.823141098022461 41 18.842227935791016
		 42 18.886064529418945 43 18.949760437011719 44 19.028068542480469 45 19.115755081176758
		 46 19.20768928527832 47 19.299043655395508 48 19.385232925415039 49 19.461938858032227
		 50 19.525371551513672 51 19.571645736694336 52 19.597223281860352 53 19.598678588867188
		 54 19.572181701660156 55 19.513717651367188 56 19.418729782104492 57 19.26127815246582
		 58 19.009313583374023 59 18.672079086303711 60 18.258752822875977 61 17.779363632202148
		 62 17.245697021484375 63 16.671857833862305 64 16.07481575012207 65 15.475013732910156
		 66 14.896512985229494 67 14.366994857788084 68 13.917243957519531 69 13.580431938171387
		 70 13.390972137451172;
createNode animCurveTU -n "Character1_LeftLeg_visibility";
	rename -uid "61FFFFD0-456A-8E30-09AA-61B354FBC431";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFoot_translateX";
	rename -uid "62CFCDAF-487D-F679-64BC-26BBFDB310DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5243232250213623;
createNode animCurveTL -n "Character1_LeftFoot_translateY";
	rename -uid "6E7A0319-4F8D-AAC4-904F-F192FF39093D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.000351905822754;
createNode animCurveTL -n "Character1_LeftFoot_translateZ";
	rename -uid "2945A6A0-4DD4-3990-D72D-AD93EB9338D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.967419624328613;
createNode animCurveTU -n "Character1_LeftFoot_scaleX";
	rename -uid "A0969767-459E-80AD-5438-6A9B9A256F0B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFoot_scaleY";
	rename -uid "E1858A82-4433-B5BA-DFCD-98947CFC2713";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleZ";
	rename -uid "CB7CEF88-46E7-7EDC-FC11-FAB1C2A4D683";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999964237213135;
createNode animCurveTA -n "Character1_LeftFoot_rotateX";
	rename -uid "AD44F3D4-47BE-4BDB-664B-90998F1A578F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 38.513771057128906 1 38.545200347900391
		 2 38.680496215820313 3 38.906276702880859 4 39.208873748779297 5 39.574424743652344
		 6 39.988990783691406 7 40.438636779785156 8 40.909561157226563 9 41.387763977050781
		 10 41.859413146972656 11 42.310783386230469 12 42.728221893310547 13 43.098037719726563
		 14 43.406589508056641 15 43.667812347412109 16 43.905406951904297 17 44.118427276611328
		 18 44.305839538574219 19 44.466644287109375 20 44.599857330322266 21 44.704544067382813
		 22 44.779830932617187 23 44.824974060058594 24 44.839092254638672 25 44.821338653564453
		 26 44.770828247070313 27 44.686511993408203 28 44.567115783691406 29 44.343574523925781
		 30 43.967418670654297 31 43.467311859130859 32 42.871726989746094 33 42.209297180175781
		 34 41.509124755859375 35 40.800601959228516 36 40.113597869873047 37 39.478271484375
		 38 38.924900054931641 39 38.48358154296875 40 38.184333801269531 41 38.019981384277344
		 42 37.955547332763672 43 37.978885650634766 44 38.077396392822266 45 38.238265991210938
		 46 38.448535919189453 47 38.695259094238281 48 38.965465545654297 49 39.246234893798828
		 50 39.525001525878906 51 39.789031982421875 52 40.025928497314453 53 40.2235107421875
		 54 40.369422912597656 55 40.451457977294922 56 40.457317352294922 57 40.381759643554688
		 58 40.242668151855469 59 40.052276611328125 60 39.822681427001953 61 39.566177368164063
		 62 39.295635223388672 63 39.024658203125 64 38.767833709716797 65 38.541084289550781
		 66 38.361763000488281 67 38.248733520507812 68 38.222152709960938 69 38.303226470947266
		 70 38.513771057128906;
createNode animCurveTA -n "Character1_LeftFoot_rotateY";
	rename -uid "7BABDC24-4031-B9B0-6701-839C6CB271AD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -3.3733468055725098 1 -3.2082383632659912
		 2 -3.084763765335083 3 -2.9993679523468018 4 -2.9483573436737061 5 -2.928037166595459
		 6 -2.9348888397216797 7 -2.965665340423584 8 -3.017509937286377 9 -3.0877499580383301
		 10 -3.1741232872009277 11 -3.2747046947479248 12 -3.3878521919250488 13 -3.5120325088500977
		 14 -3.6458332538604732 15 -3.7768654823303223 16 -3.897685050964355 17 -4.0129551887512207
		 18 -4.1274919509887695 19 -4.2462625503540039 20 -4.3743548393249512 21 -4.5169382095336914
		 22 -4.6792492866516113 23 -4.866633415222168 24 -5.0843048095703125 25 -5.3375082015991211
		 26 -5.6314544677734375 27 -5.9712691307067871 28 -6.3619723320007324 29 -6.8076066970825195
		 30 -7.2995834350585929 31 -7.8221240043640137 32 -8.359217643737793 33 -8.8961267471313477
		 34 -9.4206523895263672 35 -9.9237775802612305 36 -10.400324821472168 37 -10.848956108093262
		 38 -11.271684646606445 39 -11.672757148742676 40 -12.05717658996582 41 -12.407590866088867
		 42 -12.705131530761719 43 -12.951345443725586 44 -13.147310256958008 45 -13.293899536132813
		 46 -13.391901969909668 47 -13.442228317260742 48 -13.445902824401855 49 -13.404170989990234
		 50 -13.318757057189941 51 -13.191420555114746 52 -13.024370193481445 53 -12.820137023925781
		 54 -12.581217765808105 55 -12.310329437255859 56 -12.010193824768066 57 -11.648979187011719
		 58 -11.181632041931152 59 -10.625160217285156 60 -9.9963560104370117 61 -9.3119134902954102
		 62 -8.5885524749755859 63 -7.8429417610168457 64 -7.0916762351989746 65 -6.3513164520263672
		 66 -5.6382555961608887 67 -4.9686131477355957 68 -4.3580174446105957 69 -3.8214936256408696
		 70 -3.3733468055725098;
createNode animCurveTA -n "Character1_LeftFoot_rotateZ";
	rename -uid "174E6067-450A-F823-1D5A-2384D7F15576";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 13.039809226989746 1 12.998964309692383
		 2 12.996016502380371 3 13.027006149291992 4 13.08759593963623 5 13.173201560974121
		 6 13.279143333435059 7 13.400732040405273 8 13.533388137817383 9 13.67253303527832
		 10 13.813753128051758 11 13.952796936035156 12 14.08554744720459 13 14.207969665527344
		 14 14.316144943237305 15 14.410207748413086 16 14.494222640991213 17 14.569394111633303
		 18 14.636843681335449 19 14.697641372680664 20 14.752817153930664 21 14.803380966186523
		 22 14.850329399108888 23 14.894711494445801 24 14.937530517578123 25 14.979865074157715
		 26 15.022857666015625 27 15.067722320556641 28 15.115771293640137 29 15.15297317504883
		 30 15.168198585510256 31 15.166690826416014 32 15.153238296508787 33 15.132247924804686
		 34 15.107890129089355 35 15.084164619445803 36 15.06515598297119 37 15.055166244506836
		 38 15.058752059936522 39 15.080600738525392 40 15.125311851501463 41 15.185227394104004
		 42 15.248173713684082 43 15.311902999877928 44 15.374428749084471 45 15.434023857116701
		 46 15.48916435241699 47 15.538500785827635 48 15.580772399902346 49 15.614755630493164
		 50 15.63931941986084 51 15.653171539306639 52 15.654998779296875 53 15.64338970184326
		 54 15.616680145263672 55 15.573062896728517 56 15.510534286499023 57 15.420138359069824
		 58 15.293596267700197 59 15.135539054870605 60 14.950632095336914 61 14.743818283081056
		 62 14.520548820495605 63 14.286910057067871 64 14.049727439880371 65 13.816720962524414
		 66 13.596487045288086 67 13.398456573486328 68 13.232697486877441 69 13.109661102294922
		 70 13.039809226989746;
createNode animCurveTU -n "Character1_LeftFoot_visibility";
	rename -uid "7840F290-4288-7855-A39D-0C9909C33D26";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftToeBase_translateX";
	rename -uid "F4ADF301-498A-E107-F6A9-C9987E9B1C8E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.585639476776123;
createNode animCurveTL -n "Character1_LeftToeBase_translateY";
	rename -uid "A3863DBB-417B-F4BB-CBF4-698B87DF21EA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3360438346862793;
createNode animCurveTL -n "Character1_LeftToeBase_translateZ";
	rename -uid "7622A29E-4CC4-E66C-8C2A-00BB528028AF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.4897398948669434;
createNode animCurveTU -n "Character1_LeftToeBase_scaleX";
	rename -uid "B29D207E-4CBC-AFF4-DBDB-B5A375C051AA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftToeBase_scaleY";
	rename -uid "0C76D641-46BE-62FE-FCB0-1991AE9FF0CA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftToeBase_scaleZ";
	rename -uid "6FDD2CAF-4F77-21D8-4A77-7683F552F6A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_LeftToeBase_rotateX";
	rename -uid "3849A21C-4CFE-D29D-7335-AB88DD9ECA6F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.008536226227875e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateY";
	rename -uid "1F100E84-4E09-C34E-E3EB-4494D03A22BC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0955893404183144e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateZ";
	rename -uid "F4A009EA-4D67-7A1A-FC08-7E9A8784CF27";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3057159232185995e-009;
createNode animCurveTU -n "Character1_LeftToeBase_visibility";
	rename -uid "7B3F8DB5-490F-A544-70F8-F08513279649";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateX";
	rename -uid "35D3E33C-4825-B747-3956-BCA5D89B23F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7153444290161133;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateY";
	rename -uid "D411CEF1-4BF7-D6A2-B739-FAA1367396E9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.0020139217376709;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateZ";
	rename -uid "4C4FBB9D-4394-3C5D-F82C-B5B07563B0C4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.1786108016967773;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleX";
	rename -uid "CDE04814-4F49-A36C-AE07-58850CC1EC4E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleY";
	rename -uid "E0C4D260-4A4C-9B3F-BCF4-8883F1423F93";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999940395355225;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleZ";
	rename -uid "467293A4-4BAB-769B-F593-D784DE6A969F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateX";
	rename -uid "EEBCC5B1-45AA-38EA-4DB4-E88976C8308A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.0509727249873322e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateY";
	rename -uid "75CEEA29-422E-07EC-6BE8-65B704F200ED";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.7242437194985314e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateZ";
	rename -uid "0191BDD3-4595-C53D-B7B6-208A054F3892";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.285488160363002e-008;
createNode animCurveTU -n "Character1_LeftFootMiddle1_visibility";
	rename -uid "CD528FAB-4810-AF57-660B-9DAB56E1FFB5";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateX";
	rename -uid "924825C1-4026-CC8E-96A2-6EA2C9FCE8D2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4684751033782959;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateY";
	rename -uid "B74FAC03-4B3E-CAFD-A34B-F0BDEEB442AC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.1393222808837891;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateZ";
	rename -uid "D0C42C32-4D92-848F-0E1E-F78DAA9BB099";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.3014278411865234;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleX";
	rename -uid "83902DE4-4825-289E-7759-9A858F1F404F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleY";
	rename -uid "592CCA0F-41BC-C92A-9059-A9A63DEC4CE2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleZ";
	rename -uid "1F46F6C3-418C-C2B9-82A7-17A40E7AD123";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateX";
	rename -uid "D314E391-4EC8-1E73-E5B7-5191FD1E1127";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.3345332778035299e-008;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateY";
	rename -uid "9FC342C1-48AC-0FF2-5316-F5A9E23A5D5F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7934594254143121e-008;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateZ";
	rename -uid "4503C51C-4355-486F-656F-689F2D97B369";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.5274469184211057e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle2_visibility";
	rename -uid "30B23284-4780-69AE-425C-1BB6CB8FFCEB";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightUpLeg_translateX";
	rename -uid "253CBA55-472E-D9D5-798A-2BB92B06E803";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.489771842956543;
createNode animCurveTL -n "Character1_RightUpLeg_translateY";
	rename -uid "95BD9AD8-4AF0-97DE-21E0-2F8FB73A3056";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.6505136489868164;
createNode animCurveTL -n "Character1_RightUpLeg_translateZ";
	rename -uid "597ADA14-411C-1097-7C7F-658156062EE5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0264549255371094;
createNode animCurveTU -n "Character1_RightUpLeg_scaleX";
	rename -uid "56FD1339-46FA-F53C-E9BB-A7A79D9587ED";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightUpLeg_scaleY";
	rename -uid "4E9ACE00-4B2C-81EC-5F82-259507352401";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightUpLeg_scaleZ";
	rename -uid "37ED15BB-4379-12FB-BB18-D4B3B99E9215";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightUpLeg_rotateX";
	rename -uid "2FD1E4F6-4E13-661A-4F75-659705677518";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -24.959627151489258 1 -24.864809036254883
		 2 -24.741306304931641 3 -24.594038009643555 4 -24.428146362304688 5 -24.24882698059082
		 6 -24.061191558837891 7 -23.870166778564453 8 -23.680456161499023 9 -23.496526718139648
		 10 -23.322599411010742 11 -23.162639617919922 12 -23.020416259765625 13 -22.899473190307617
		 14 -22.803157806396484 15 -22.713180541992188 16 -22.611667633056641 17 -22.503854751586914
		 18 -22.39521598815918 19 -22.291372299194336 20 -22.198043823242188 21 -22.120992660522461
		 22 -22.065990447998047 23 -22.038806915283203 24 -22.045166015625 25 -22.090784072875977
		 26 -22.181365966796875 27 -22.322635650634766 28 -22.520383834838867 29 -22.821945190429688
		 30 -23.253934860229492 31 -23.790775299072266 32 -24.407144546508789 33 -25.077789306640625
		 34 -25.777399063110352 35 -26.480474472045898 36 -27.161352157592773 37 -27.794254302978516
		 38 -28.353422164916992 39 -28.813316345214844 40 -29.149021148681641 41 -29.395225524902344
		 42 -29.604381561279297 43 -29.779300689697262 44 -29.922935485839847 45 -30.03815841674805
		 46 -30.127559661865238 47 -30.193328857421871 48 -30.237142562866207 49 -30.260110855102536
		 50 -30.2628059387207 51 -30.245281219482418 52 -30.207160949707035 53 -30.147769927978516
		 54 -30.066347122192383 55 -29.962249755859375 56 -29.83524322509766 57 -29.670217514038086
		 58 -29.446903228759766 59 -29.17336273193359 60 -28.857778549194339 61 -28.508256912231445
		 62 -28.132671356201172 63 -27.738519668579102 64 -27.332784652709961 65 -26.921794891357422
		 66 -26.511148452758789 67 -26.105693817138672 68 -25.7095947265625 69 -25.326473236083984
		 70 -24.959627151489258;
createNode animCurveTA -n "Character1_RightUpLeg_rotateY";
	rename -uid "C27CA8BF-464D-1D55-06EC-A699E4E09175";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 31.013761520385739 1 31.199409484863281
		 2 31.503871917724609 3 31.910089492797855 4 32.400749206542969 5 32.958438873291016
		 6 33.566074371337891 7 34.206985473632812 8 34.865215301513672 9 35.524791717529297
		 10 36.170413970947266 11 36.7872314453125 12 37.360805511474609 13 37.876628875732422
		 14 38.320419311523438 15 38.691734313964844 16 39.001918792724609 17 39.256546020507813
		 18 39.460884094238281 19 39.620021820068359 20 39.738906860351562 21 39.822399139404297
		 22 39.87530517578125 23 39.902603149414063 24 39.908920288085938 25 39.899036407470703
		 26 39.877765655517578 27 39.849933624267578 28 39.820343017578125 29 39.704723358154297
		 30 39.430732727050781 31 39.025920867919922 32 38.517551422119141 33 37.932807922363281
		 34 37.299179077148438 35 36.6441650390625 36 35.995922088623047 37 35.383419036865234
		 38 34.836463928222656 39 34.385398864746094 40 34.061214447021484 41 33.835342407226563
		 42 33.656021118164063 43 33.518413543701172 44 33.416347503662109 45 33.343036651611328
		 46 33.291484832763672 47 33.255020141601563 48 33.227226257324219 49 33.202190399169922
		 50 33.175014495849609 51 33.140701293945313 52 33.095054626464844 53 33.0343017578125
		 54 32.954257965087891 55 32.850833892822266 56 32.719486236572266 57 32.572216033935547
		 58 32.421890258789063 59 32.268863677978516 60 32.113636016845703 61 31.957098007202148
		 62 31.800846099853512 63 31.647165298461911 64 31.499074935913089 65 31.360687255859375
		 66 31.237060546874996 67 31.134159088134766 68 31.058376312255859 69 31.016212463378906
		 70 31.013761520385739;
createNode animCurveTA -n "Character1_RightUpLeg_rotateZ";
	rename -uid "C902E7CC-4AE4-E4D8-4620-C3A95E6A263F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 70 ".ktv[0:69]"  0 -17.289947509765625 1 -17.144556045532227
		 2 -17.051551818847656 3 -17.00468635559082 4 -16.997751235961914 5 -17.02459716796875
		 6 -17.079652786254883 7 -17.157951354980469 8 -17.2554931640625 9 -17.368318557739258
		 10 -17.493373870849609 11 -17.628177642822266 12 -17.770854949951172 13 -17.919393539428711
		 14 -18.072027206420898 15 -18.207399368286133 16 -18.304601669311523 17 -18.369966506958008
		 18 -18.409763336181641 19 -18.430206298828125 20 -18.437404632568359 22 -18.435920715332031
		 23 -18.439121246337891 24 -18.452396392822266 25 -18.48130989074707 26 -18.531421661376953
		 27 -18.608299255371094 28 -18.717582702636719 29 -18.833198547363281 30 -18.924276351928711
		 31 -18.992071151733398 32 -19.038236618041992 33 -19.065397262573242 34 -19.077957153320313
		 35 -19.081764221191406 36 -19.085124969482422 37 -19.099006652832031 38 -19.136919021606445
		 39 -19.214214324951172 40 -19.347990036010742 41 -19.525886535644531 42 -19.723188400268555
		 43 -19.937751770019531 44 -20.165672302246094 45 -20.401899337768555 46 -20.640584945678711
		 47 -20.875732421875 48 -21.100860595703125 49 -21.309473037719727 50 -21.495868682861328
		 51 -21.653583526611328 52 -21.77699089050293 53 -21.860942840576172 54 -21.899919509887695
		 55 -21.88911247253418 56 -21.824052810668945 57 -21.696672439575195 58 -21.492986679077148
		 59 -21.225282669067383 60 -20.905851364135742 61 -20.54680061340332 62 -20.160045623779297
		 63 -19.756921768188477 64 -19.348001480102539 65 -18.943386077880859 66 -18.552501678466797
		 67 -18.184169769287109 68 -17.846311569213867 69 -17.546085357666016 70 -17.289947509765625;
createNode animCurveTU -n "Character1_RightUpLeg_visibility";
	rename -uid "3B3781C9-4745-4540-4B72-DFBCB05B96A9";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightLeg_translateX";
	rename -uid "CACCCC1F-4329-4201-8652-95BE2CAC1F80";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -18.197565078735352;
createNode animCurveTL -n "Character1_RightLeg_translateY";
	rename -uid "6CAECBE4-407E-42A2-CFFF-5E9DDC7DB564";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.1862249374389648;
createNode animCurveTL -n "Character1_RightLeg_translateZ";
	rename -uid "A0CF7513-4970-3EC0-79B1-ABB6FBDA2693";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.5520458221435547;
createNode animCurveTU -n "Character1_RightLeg_scaleX";
	rename -uid "F90CBB57-4D33-CEB0-0C09-DBB92A67EA28";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightLeg_scaleY";
	rename -uid "C3B5EB15-4EFB-8742-7AB3-5FB917BADDFE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightLeg_scaleZ";
	rename -uid "855DC74C-45ED-CEE2-5096-C2A48F24E0CA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightLeg_rotateX";
	rename -uid "34045723-4EF9-41E9-B62D-C1B67104F73A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 25.440799713134766 1 25.407585144042969
		 2 25.474998474121094 3 25.629411697387695 4 25.856988906860352 5 26.143804550170898
		 6 26.4761962890625 7 26.840808868408203 8 27.224845886230469 9 27.615402221679688
		 10 28.000064849853516 11 28.366666793823242 12 28.703275680541996 13 28.997722625732422
		 14 29.237833023071286 15 29.410854339599613 16 29.515474319458011 17 29.56093597412109
		 18 29.556253433227539 19 29.510358810424805 20 29.432140350341797 21 29.330499649047852
		 22 29.214397430419918 23 29.093088150024418 24 28.975568771362305 25 28.871110916137692
		 26 28.78913497924805 27 28.739158630371094 28 28.730789184570312 29 28.735427856445316
		 30 28.722364425659176 31 28.699481964111328 32 28.674573898315433 33 28.654449462890629
		 34 28.644565582275391 35 28.648220062255863 36 28.667098999023438 37 28.701541900634762
		 38 28.750978469848636 39 28.814292907714844 40 28.890914916992188 41 29.024513244628906
		 42 29.249561309814453 43 29.552021026611328 44 29.917043685913089 45 30.329328536987308
		 46 30.773296356201168 47 31.233430862426758 48 31.694070816040043 49 32.139598846435547
		 50 32.554927825927734 51 32.924446105957031 52 33.23297119140625 53 33.465488433837891
		 54 33.606578826904297 55 33.641098022460938 56 33.553932189941406 57 33.322135925292969
		 58 32.942844390869141 59 32.438751220703125 60 31.832611083984375 61 31.14739990234375
		 62 30.406581878662106 63 29.634033203125 64 28.854036331176754 65 28.091535568237305
		 66 27.371974945068359 67 26.721250534057617 68 26.165319442749023 69 25.730030059814453
		 70 25.440799713134766;
createNode animCurveTA -n "Character1_RightLeg_rotateY";
	rename -uid "20614DCE-418B-DAF6-D77A-DDB33D0F11E9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -28.300405502319336 1 -28.401039123535156
		 2 -28.681396484375 3 -29.116605758666996 4 -29.680875778198246 5 -30.348003387451168
		 6 -31.092693328857422 7 -31.890886306762699 8 -32.720615386962891 9 -33.560169219970703
		 10 -34.38995361328125 11 -35.191898345947266 12 -35.949424743652344 13 -36.646175384521484
		 14 -37.266731262207031 15 -37.798736572265625 16 -38.242641448974609 17 -38.60650634765625
		 18 -38.897712707519531 19 -39.123203277587891 20 -39.28948974609375 21 -39.402786254882813
		 22 -39.469020843505859 23 -39.494453430175781 24 -39.484298706054688 25 -39.444046020507813
		 26 -39.379226684570313 27 -39.295326232910156 28 -39.197853088378906 29 -38.961261749267578
		 30 -38.479572296142578 31 -37.789321899414063 32 -36.926422119140625 33 -35.927780151367188
		 34 -34.833328247070313 35 -33.685638427734375 36 -32.532142639160156 37 -31.425495147705078
		 38 -30.423286437988281 39 -29.586271286010742 40 -28.977638244628906 41 -28.635347366333008
		 42 -28.537914276123047 43 -28.656032562255856 44 -28.95682525634766 45 -29.405641555786136
		 46 -29.967195510864258 47 -30.607101440429688 48 -31.291397094726566 49 -31.987443923950199
		 50 -32.665485382080078 51 -33.295272827148438 52 -33.848972320556641 53 -34.300071716308594
		 54 -34.621162414550781 55 -34.785564422607422 56 -34.765914916992187 57 -34.571605682373047
		 58 -34.225883483886719 59 -33.754428863525391 60 -33.182708740234375 61 -32.536594390869141
		 62 -31.843141555786129 63 -31.130485534667965 64 -30.427974700927734 65 -29.767124176025391
		 66 -29.181339263916016 67 -28.705881118774411 68 -28.376710891723633 69 -28.229883193969727
		 70 -28.300405502319336;
createNode animCurveTA -n "Character1_RightLeg_rotateZ";
	rename -uid "D85FB0FC-4752-8DC1-4B5D-3389FC9F0C79";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 0.65979444980621338 1 0.63121324777603149
		 2 0.5865665078163147 3 0.52696120738983154 4 0.45341131091117859 5 0.36698195338249207
		 6 0.26877215504646301 7 0.15998983383178711 8 0.041893690824508667 9 -0.083851739764213562
		 10 -0.21545229852199554 11 -0.35090184211730957 12 -0.48800024390220642 13 -0.62415230274200439
		 14 -0.75655090808868408 15 -0.88312983512878418 16 -1.0027892589569092 17 -1.1142834424972534
		 18 -1.2164561748504639 19 -1.3082702159881592 20 -1.3887939453125 21 -1.4572023153305054
		 22 -1.5127668380737305 23 -1.5549739599227905 24 -1.5831649303436279 25 -1.5968302488327026
		 26 -1.5955190658569336 27 -1.5787914991378784 28 -1.5461912155151367 29 -1.4610695838928223
		 30 -1.2969754934310913 31 -1.070270299911499 32 -0.79751098155975342 33 -0.49485635757446284
		 34 -0.17774161696434021 35 0.13971984386444092 36 0.44460800290107727 37 0.72527009248733521
		 38 0.97128379344940197 39 1.173386812210083 40 1.323011040687561 41 1.4275951385498047
		 42 1.5031541585922241 43 1.5536519289016724 44 1.5823626518249512 45 1.5920673608779907
		 46 1.5852720737457275 47 1.5642962455749512 48 1.5315537452697754 49 1.4895834922790527
		 50 1.4409078359603882 51 1.3885544538497925 52 1.3356189727783203 53 1.2853552103042603
		 54 1.2414340972900391 55 1.2075332403182983 56 1.187355637550354 57 1.1720041036605835
		 58 1.154537558555603 59 1.1347963809967041 60 1.1126720905303955 61 1.0880300998687744
		 62 1.0606101751327515 63 1.0300657749176025 64 0.99595373868942272 65 0.957588791847229
		 66 0.9140613079071046 67 0.86417418718338013 68 0.80647158622741699 69 0.73912125825881958
		 70 0.65979444980621338;
createNode animCurveTU -n "Character1_RightLeg_visibility";
	rename -uid "82F0AEA0-4DB7-0133-9A61-F292C3B9AAC7";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFoot_translateX";
	rename -uid "C5092CD8-4943-1FA5-87CA-65BCF77AB9BA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -18.25177001953125;
createNode animCurveTL -n "Character1_RightFoot_translateY";
	rename -uid "AF1D1AB4-4DFC-E8AF-2CB6-A993933B494E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.0794563293457031;
createNode animCurveTL -n "Character1_RightFoot_translateZ";
	rename -uid "B4C8056F-4D2D-E08F-106D-5B91CF445110";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5524176359176636;
createNode animCurveTU -n "Character1_RightFoot_scaleX";
	rename -uid "569F1186-4C49-ED41-6B3E-89BD4273C6A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFoot_scaleY";
	rename -uid "84B5A5F5-436D-6790-F343-0C83E4DECCE9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFoot_scaleZ";
	rename -uid "145A0AAA-4F0C-A94F-317F-778F964FFC44";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightFoot_rotateX";
	rename -uid "9F308C33-4F39-0330-B616-5BA16CED80D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -3.7208926677703857 1 -3.7306282520294194
		 2 -3.8014581203460698 3 -3.9254698753356938 4 -4.0941219329833984 5 -4.2985639572143555
		 6 -4.5303826332092285 7 -4.7818164825439453 8 -5.0462255477905273 9 -5.317232608795166
		 10 -5.589655876159668 11 -5.8592076301574707 12 -6.1224889755249023 13 -6.376307487487793
		 14 -6.6179804801940918 15 -6.8352580070495605 16 -7.0192961692810059 17 -7.1731910705566406
		 18 -7.2996673583984375 19 -7.4012284278869629 20 -7.4802217483520508 21 -7.5389094352722159
		 22 -7.5795192718505859 23 -7.6045298576354989 24 -7.6159901618957528 25 -7.6161708831787109
		 26 -7.6073927879333496 27 -7.5919642448425284 28 -7.5721607208251953 29 -7.5264492034912118
		 30 -7.433483600616456 31 -7.2964639663696289 32 -7.1183037757873535 33 -6.9025678634643555
		 34 -6.6545190811157227 35 -6.3808979988098145 36 -6.0909671783447266 37 -5.796565055847168
		 38 -5.5118203163146973 39 -5.2521624565124512 40 -5.0339670181274414 41 -4.8900241851806641
		 42 -4.8349976539611816 43 -4.856560230255127 44 -4.9411273002624512 45 -5.0746173858642578
		 46 -5.2429542541503906 47 -5.4327521324157715 48 -5.6310162544250488 49 -5.8255758285522461
		 50 -6.005805492401123 51 -6.1609578132629395 52 -6.2815346717834473 53 -6.3587045669555664
		 54 -6.383181095123291 55 -6.3459014892578125 56 -6.23724365234375 57 -6.0802202224731445
		 58 -5.8948488235473633 59 -5.6854262351989746 60 -5.4559688568115234 61 -5.2109236717224121
		 62 -4.9558682441711426 63 -4.697685718536377 64 -4.4447388648986816 65 -4.2073564529418945
		 66 -3.9976513385772705 67 -3.8293786048889156 68 -3.7171976566314697 69 -3.6761598587036133
		 70 -3.7208926677703857;
createNode animCurveTA -n "Character1_RightFoot_rotateY";
	rename -uid "CACC2BD0-4E22-0756-4DF2-0EA279477E7A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 1.2075848579406738 1 1.2271369695663452
		 2 1.2726333141326904 3 1.3397896289825439 4 1.4241373538970947 5 1.5212169885635376
		 6 1.6269228458404541 7 1.7376117706298828 8 1.8502955436706543 9 1.9622968435287478
		 10 2.0715999603271484 11 2.1767058372497559 12 2.2765953540802002 13 2.3704378604888916
		 14 2.4576690196990967 15 2.5339105129241943 16 2.5962421894073486 17 2.6465303897857666
		 18 2.6863269805908203 19 2.7169511318206787 20 2.7395391464233398 21 2.7550921440124512
		 22 2.7645187377929687 23 2.7687637805938721 24 2.7685744762420654 25 2.7647640705108643
		 26 2.7581686973571777 27 2.7496428489685059 28 2.7400681972503662 29 2.7215099334716797
		 30 2.6853210926055908 31 2.6316597461700439 32 2.5607161521911621 33 2.4731416702270508
		 34 2.3705043792724609 35 2.2552282810211182 36 2.1310298442840576 37 2.00295090675354
		 38 1.8772348165512085 39 1.7608745098114014 40 1.6613565683364868 41 1.5966869592666626
		 42 1.5766668319702148 43 1.5951472520828247 44 1.6448365449905396 45 1.7179591655731201
		 46 1.8067538738250732 47 1.903990626335144 48 2.003002405166626 49 2.0979578495025635
		 50 2.1841707229614258 51 2.2574000358581543 52 2.3143107891082764 53 2.3521075248718262
		 54 2.367933988571167 55 2.3589260578155518 56 2.3216609954833984 57 2.2642920017242432
		 58 2.1943635940551758 59 2.1130669116973877 60 2.0214478969573975 61 1.9207860231399538
		 62 1.8129594326019285 63 1.7005971670150757 64 1.5872098207473755 65 1.4774231910705566
		 66 1.3769172430038452 67 1.2923412322998047 68 1.2309436798095703 69 1.2002513408660889
		 70 1.2075848579406738;
createNode animCurveTA -n "Character1_RightFoot_rotateZ";
	rename -uid "630F0770-488D-E4E2-0F74-728EE7CFF4DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 1.6317077875137329 1 1.4996351003646851
		 2 1.3282715082168579 3 1.12456214427948 4 0.89545780420303334 5 0.64801353216171265
		 6 0.38928830623626709 7 0.12638600170612335 8 -0.13365788757801056 9 -0.38350787758827209
		 10 -0.61590224504470825 11 -0.8235630989074707 12 -0.99926787614822388 13 -1.1356421709060669
		 14 -1.2253832817077637 15 -1.2596151828765869 16 -1.2366882562637329 17 -1.1633303165435791
		 18 -1.0464060306549072 19 -0.89288914203643799 20 -0.70981121063232422 21 -0.50423240661621094
		 22 -0.28320983052253723 23 -0.053871769458055496 24 0.17686073482036591 25 0.40204450488090515
		 26 0.61478215456008911 27 0.80823463201522827 28 0.97560906410217274 29 1.1166481971740723
		 30 1.2392678260803223 31 1.3498374223709106 32 1.4553594589233398 33 1.5628608465194702
		 34 1.6786868572235107 35 1.8082742691040039 36 1.9554120302200317 37 2.1219139099121094
		 38 2.307551383972168 39 2.5104556083679199 40 2.7274327278137207 41 2.8818981647491455
		 42 2.9089858531951904 43 2.8227927684783936 44 2.6378862857818604 45 2.3693215847015381
		 46 2.0327019691467285 47 1.6440482139587402 48 1.2200723886489868 49 0.77801483869552612
		 50 0.33521649241447449 51 -0.090121045708656311 52 -0.4798189103603363 53 -0.81565743684768677
		 54 -1.0790932178497314 55 -1.251973032951355 56 -1.3165585994720459 57 -1.2737584114074707
		 58 -1.1475878953933716 59 -0.95288932323455811 60 -0.70473045110702515 61 -0.4181789755821228
		 62 -0.10816574096679688 63 0.21084152162075043 64 0.52505391836166382 65 0.82134467363357544
		 66 1.0874189138412476 67 1.3117976188659668 68 1.4838826656341553 69 1.5937201976776123
		 70 1.6317077875137329;
createNode animCurveTU -n "Character1_RightFoot_visibility";
	rename -uid "B0E69BAD-4A0E-480F-9EC7-6290C0B3389F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightToeBase_translateX";
	rename -uid "6CAC7A9B-4EFD-98F7-5FF1-55A350D88F73";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2767767906188965;
createNode animCurveTL -n "Character1_RightToeBase_translateY";
	rename -uid "3225CEA3-4E70-B87D-24A1-F9AEFCB866C7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.0139193534851074;
createNode animCurveTL -n "Character1_RightToeBase_translateZ";
	rename -uid "CE64DF5F-452A-0396-D31F-46A434559887";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8259720802307129;
createNode animCurveTU -n "Character1_RightToeBase_scaleX";
	rename -uid "8E90D558-439B-054A-C243-0AB1DC70F945";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightToeBase_scaleY";
	rename -uid "9E12DBF5-4EFB-1617-4490-2DBDF984E540";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightToeBase_scaleZ";
	rename -uid "EAB3CC7D-40D9-36A8-A24C-59AD7693A772";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightToeBase_rotateX";
	rename -uid "D9AD0125-42AE-6A5D-FDCF-BB9C6D338711";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.0098894749671672e-009;
createNode animCurveTA -n "Character1_RightToeBase_rotateY";
	rename -uid "4FBB0200-4250-E036-F780-959237624D19";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.2744724709488082e-009;
createNode animCurveTA -n "Character1_RightToeBase_rotateZ";
	rename -uid "3A68912F-413C-E747-5117-2D9215A8F01A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3266189258231975e-009;
createNode animCurveTU -n "Character1_RightToeBase_visibility";
	rename -uid "C7DC637D-4271-2604-AF1C-538E2A270B38";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateX";
	rename -uid "2E8DE210-4E04-D72D-0B26-FBBF9A0B1668";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.0018006421159952879;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateY";
	rename -uid "0AA5F994-4922-79B8-873F-6DA75EB85B7C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.0064689838327467442;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateZ";
	rename -uid "F53EFB31-4E93-B42D-A98B-459430EBDF65";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.6805553436279297;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleX";
	rename -uid "F79EA55E-4CF2-1BDE-4989-53A284995AAE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleY";
	rename -uid "B9BB787B-40F2-8DC3-181A-33B81BF49DC4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleZ";
	rename -uid "FFB3E4F8-45BB-E8D8-1D60-E5A504CDCC7F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateX";
	rename -uid "9B7ED3F3-4AF5-C3E6-AD93-599AD8C0C168";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7257896445244114e-008;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateY";
	rename -uid "463A0361-4D8F-DB7E-0511-61ABACF238DF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.8734833118780898e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateZ";
	rename -uid "83B98F3F-4695-124C-D3C2-FAA1652E8786";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.4639691115501137e-009;
createNode animCurveTU -n "Character1_RightFootMiddle1_visibility";
	rename -uid "04F55E84-49F4-03A3-93F3-D48C09504247";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateX";
	rename -uid "7FBF3760-4F42-1284-853F-BA9A76806433";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.5860963432933204e-006;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateY";
	rename -uid "4B04422D-4D37-9936-BFA6-7D96FA852135";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.3300979036284843e-007;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateZ";
	rename -uid "D65A1E8A-41DE-34AB-19F8-29A248D05159";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.0234856605529785;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleX";
	rename -uid "50821096-4510-A262-4340-449FF0E38324";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleY";
	rename -uid "EF894CDE-4367-6B65-C716-919A085709C2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleZ";
	rename -uid "1A546E95-4125-E046-B309-97B70209A7EC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateX";
	rename -uid "E5E60287-4735-01DD-1C4D-BD85E4308503";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.7193238832646784e-008;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateY";
	rename -uid "BABE5E3C-42AC-36E9-2545-128CCE0839B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.1075757078724564e-008;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateZ";
	rename -uid "B9C4DCD2-4044-5B78-1275-9B842B98DFA8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.8635901406821639e-009;
createNode animCurveTU -n "Character1_RightFootMiddle2_visibility";
	rename -uid "0751F290-4BDB-0423-06A6-078B6A0C9EF8";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Spine_translateX";
	rename -uid "CB16DE74-4BD8-3F6B-F899-DDA9FFCC2C97";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6075620651245117;
createNode animCurveTL -n "Character1_Spine_translateY";
	rename -uid "6F5D9287-48C9-4770-5345-5CB3D1B4ED69";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.353469848632812;
createNode animCurveTL -n "Character1_Spine_translateZ";
	rename -uid "C56A6FB9-476A-7966-F82F-E5AC9DC045F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2061522006988525;
createNode animCurveTU -n "Character1_Spine_scaleX";
	rename -uid "D1DBA22A-475C-E3F6-E291-DE9EBAFD62B4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine_scaleY";
	rename -uid "5BB1E8D9-486A-3A7B-D092-63912968A3E1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine_scaleZ";
	rename -uid "9533E685-438E-4CA1-A9D8-019DB04CC040";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_Spine_rotateX";
	rename -uid "73DDAA96-40A3-20B3-8BC3-AE9963F4BFC8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 70 ".ktv[0:69]"  0 -5.865870475769043 1 -5.8569636344909668
		 2 -5.8327760696411133 3 -5.7955608367919922 4 -5.7475295066833496 5 -5.6909365653991699
		 6 -5.6280560493469238 7 -5.5612192153930664 8 -5.4927811622619629 9 -5.4252791404724121
		 10 -5.3612813949584961 11 -5.3034334182739258 12 -5.2543926239013672 13 -5.2169284820556641
		 14 -5.1937828063964844 15 -5.1865396499633789 16 -5.1936068534851074 17 -5.212897777557373
		 18 -5.2423319816589355 19 -5.2798304557800293 20 -5.3233165740966797 21 -5.3707103729248047
		 22 -5.4199299812316895 23 -5.4688506126403809 24 -5.5154304504394531 25 -5.5575957298278809
		 26 -5.5932798385620117 27 -5.6204299926757812 28 -5.6370120048522949 29 -5.6524410247802734
		 30 -5.6761116981506348 31 -5.7057371139526367 32 -5.7390217781066895 33 -5.773859977722168
		 34 -5.8084206581115723 35 -5.8413453102111816 36 -5.8716588020324707 37 -5.8987321853637695
		 38 -5.9221949577331543 39 -5.941838264465332 40 -5.9573168754577637 41 -5.9579691886901855
		 42 -5.935910701751709 43 -5.8946285247802734 44 -5.8375558853149414 45 -5.7681417465209961
		 46 -5.6899218559265137 47 -5.6065258979797363 48 -5.5218076705932617 49 -5.4398221969604492
		 50 -5.3647208213806152 51 -5.3010120391845703 52 -5.2532782554626465 53 -5.226193904876709
		 54 -5.2246217727661133 55 -5.2533516883850098 56 -5.3170738220214844 57 -5.3979105949401855
		 58 -5.4727988243103027 59 -5.5414810180664062 60 -5.6036539077758789 61 -5.6590418815612793
		 62 -5.7074356079101563 63 -5.7487678527832031 64 -5.7831559181213379 65 -5.810847282409668
		 66 -5.8322343826293945 67 -5.8478145599365234 68 -5.8581976890563965 69 -5.8640222549438477;
createNode animCurveTA -n "Character1_Spine_rotateY";
	rename -uid "52F38707-4243-ABC5-7004-E5B6CDCAD7A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -11.602585792541504 1 -11.622427940368652
		 2 -11.678238868713379 3 -11.764376640319824 4 -11.87519645690918 5 -12.005088806152344
		 6 -12.14841365814209 7 -12.299510955810547 8 -12.452643394470215 9 -12.602156639099121
		 10 -12.742321014404297 11 -12.867389678955078 12 -12.971550941467285 13 -13.049081802368164
		 14 -13.094233512878418 15 -13.12515926361084 16 -13.162126541137695 17 -13.202560424804688
		 18 -13.243880271911621 19 -13.28349781036377 20 -13.318829536437988 21 -13.3472900390625
		 22 -13.366293907165527 23 -13.373202323913574 24 -13.365484237670898 25 -13.340561866760254
		 26 -13.295852661132813 27 -13.22877025604248 28 -13.136716842651367 29 -12.986698150634766
		 30 -12.759339332580566 31 -12.472407341003418 32 -12.143643379211426 33 -11.79069709777832
		 34 -11.431024551391602 35 -11.081989288330078 36 -10.760739326477051 37 -10.484230041503906
		 38 -10.269290924072266 39 -10.13274097442627 40 -10.091422080993652 41 -10.130622863769531
		 42 -10.219626426696777 43 -10.351412773132324 44 -10.518980979919434 45 -10.715322494506836
		 46 -10.933390617370605 47 -11.166069030761719 48 -11.406219482421875 49 -11.646649360656738
		 50 -11.880024909973145 51 -12.09908390045166 52 -12.296443939208984 53 -12.464655876159668
		 54 -12.596336364746094 55 -12.684039115905762 56 -12.720320701599121 57 -12.712522506713867
		 58 -12.672749519348145 59 -12.606210708618164 60 -12.518150329589844 61 -12.413825988769531
		 62 -12.298480987548828 63 -12.177353858947754 64 -12.055686950683594 65 -11.938668251037598
		 66 -11.831453323364258 67 -11.739143371582031 68 -11.666839599609375 69 -11.619627952575684
		 70 -11.602585792541504;
createNode animCurveTA -n "Character1_Spine_rotateZ";
	rename -uid "4DB4C959-44D3-D1A1-AFD6-B796272E5736";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 70 ".ktv[0:69]"  0 6.9834203720092773 1 6.9616384506225586
		 2 6.9018778800964355 3 6.8100314140319824 4 6.6919403076171875 5 6.5536537170410156
		 6 6.4010968208312988 7 6.2401785850524902 8 6.0765213966369629 9 5.9162712097167969
		 10 5.7653083801269531 11 5.6294970512390137 12 5.514493465423584 13 5.4263420104980469
		 14 5.3709373474121094 15 5.3391480445861816 16 5.3171157836914062 17 5.3033971786499023
		 18 5.2965569496154785 20 5.2977895736694336 21 5.3030071258544922 22 5.3093938827514648
		 23 5.315312385559082 24 5.3195400238037109 25 5.3206944465637207 26 5.3173904418945313
		 27 5.308265209197998 28 5.2919802665710449 29 5.3051543235778809 30 5.3780221939086914
		 31 5.499232292175293 32 5.6573491096496582 33 5.8408565521240234 34 6.0378808975219727
		 35 6.2368626594543457 36 6.4259724617004395 37 6.5931239128112793 38 6.7260885238647461
		 39 6.8128948211669922 40 6.8413658142089844 41 6.8043355941772461 42 6.7086954116821289
		 43 6.5634274482727051 44 6.3775577545166016 45 6.1601347923278809 46 5.9202256202697754
		 47 5.6666760444641113 48 5.4086565971374512 49 5.155367374420166 50 4.9154996871948242
		 51 4.6985664367675781 52 4.5136146545410156 53 4.3695425987243652 54 4.2757768630981445
		 55 4.2414002418518066 56 4.2754559516906738 57 4.376762866973877 58 4.5311594009399414
		 59 4.7288680076599121 60 4.9601187705993652 61 5.2151584625244141 62 5.4841527938842773
		 63 5.7573757171630859 64 6.025299072265625 65 6.2782950401306152 66 6.5067572593688965
		 67 6.7010073661804199 68 6.8515400886535645 69 6.9488630294799805 70 6.9834203720092773;
createNode animCurveTU -n "Character1_Spine_visibility";
	rename -uid "4540DE57-4B2D-8362-D572-7887B40B33D6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Spine1_translateX";
	rename -uid "8F9C7EA7-41BE-0F57-D6D0-CF8A34FA2371";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.930038452148437;
createNode animCurveTL -n "Character1_Spine1_translateY";
	rename -uid "D0FB74E5-4CEB-FED9-CBCC-1D96CA3A157E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1661763191223145;
createNode animCurveTL -n "Character1_Spine1_translateZ";
	rename -uid "88C9F37B-464C-1C67-BCA4-FABBC3E01BD1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.40121376514434814;
createNode animCurveTU -n "Character1_Spine1_scaleX";
	rename -uid "0D63BED1-4B3C-0C77-9179-D2B5DD8A90EA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_Spine1_scaleY";
	rename -uid "4ED5FCBB-4400-8030-804F-0BBF3D67C3ED";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine1_scaleZ";
	rename -uid "0D465B32-4DB4-72C8-9F00-ED99C8B515B1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_Spine1_rotateX";
	rename -uid "1689DA23-4AB8-21B1-27D5-93A03CFFCB6B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -29.319906234741214 1 -29.272754669189453
		 2 -29.278512954711914 3 -29.334354400634766 4 -29.437456130981442 5 -29.585046768188477
		 6 -29.774354934692386 7 -30.002635955810543 8 -30.267099380493164 9 -30.565101623535156
		 10 -30.893972396850586 11 -31.251068115234371 12 -31.633733749389648 13 -32.039413452148437
		 14 -32.465564727783203 15 -32.902565002441406 16 -33.33935546875 17 -33.771148681640625
		 18 -34.193099975585937 19 -34.600341796875 20 -34.987987518310547 21 -35.351158142089844
		 22 -35.685005187988281 23 -35.984642028808594 24 -36.245307922363281 25 -36.46221923828125
		 26 -36.630607604980469 27 -36.745708465576172 28 -36.802742004394531 29 -36.823711395263672
		 30 -36.833713531494141 31 -36.832462310791016 32 -36.819671630859375 33 -36.795120239257813
		 34 -36.758663177490234 35 -36.710411071777344 36 -36.650581359863281 37 -36.579498291015625
		 38 -36.497528076171875 39 -36.405078887939453 40 -36.302341461181641 41 -36.202770233154297
		 42 -36.117565155029297 43 -36.043296813964844 44 -35.976566314697266 45 -35.914031982421875
		 46 -35.852447509765625 47 -35.788608551025391 48 -35.719501495361328 49 -35.6422119140625
		 50 -35.553836822509766 51 -35.451740264892578 52 -35.333309173583984 53 -35.195987701416016
		 54 -35.0374755859375 55 -34.855545043945313 56 -34.648178100585938 57 -34.385662078857422
		 58 -34.043426513671875 59 -33.637149810791016 60 -33.182445526123047 61 -32.694801330566406
		 62 -32.189590454101563 63 -31.682054519653324 64 -31.187341690063477 65 -30.720428466796871
		 66 -30.296171188354496 67 -29.929302215576172 68 -29.634521484375004 69 -29.426488876342773
		 70 -29.319906234741214;
createNode animCurveTA -n "Character1_Spine1_rotateY";
	rename -uid "C4E1979F-425C-449D-B6AB-67A35214D2DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -4.0840544700622559 1 -4.1215949058532715
		 2 -4.1798954010009766 3 -4.2578368186950684 4 -4.3542561531066895 5 -4.4680256843566895
		 6 -4.5979495048522949 7 -4.7427802085876465 8 -4.901120662689209 9 -5.0717144012451172
		 10 -5.2531599998474121 11 -5.4439959526062012 12 -5.6426377296447754 13 -5.8475823402404785
		 14 -6.0572214126586914 15 -6.2373347282409668 16 -6.3592133522033691 17 -6.4295406341552734
		 18 -6.4551677703857422 19 -6.4430818557739258 20 -6.4004054069519043 21 -6.3343691825866699
		 22 -6.2523002624511719 23 -6.1615276336669922 24 -6.0695924758911133 25 -5.9840106964111328
		 26 -5.9123291969299316 27 -5.8620977401733398 28 -5.8408589363098145 29 -5.8619523048400879
		 30 -5.9279403686523437 31 -6.0301332473754883 32 -6.1597957611083984 33 -6.3079838752746582
		 34 -6.4653425216674805 35 -6.6223053932189941 36 -6.768918514251709 37 -6.8949246406555176
		 38 -6.989962100982666 39 -7.043910026550293 40 -7.0469865798950195 41 -7.001650333404541
		 42 -6.9200987815856934 43 -6.8077311515808105 44 -6.6699328422546387 45 -6.5119490623474121
		 46 -6.3387784957885742 47 -6.1550107002258301 48 -5.9649591445922852 49 -5.7725071907043457
		 50 -5.5808792114257812 51 -5.393120288848877 52 -5.2116470336914062 53 -5.0383992195129395
		 54 -4.8751339912414551 55 -4.7232160568237305 56 -4.5838227272033691 57 -4.4592771530151367
		 58 -4.3556714057922363 59 -4.2706022262573242 60 -4.2017660140991211 61 -4.1470541954040527
		 62 -4.1045851707458496 63 -4.0728044509887695 64 -4.0505409240722656 65 -4.036902904510498
		 66 -4.0312943458557129 67 -4.0333356857299805 68 -4.042877197265625 69 -4.0598344802856445
		 70 -4.0840544700622559;
createNode animCurveTA -n "Character1_Spine1_rotateZ";
	rename -uid "9D67F584-4F1A-B7E0-9D3F-89BFA2807CF7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 25.586374282836914 1 25.456209182739258
		 2 25.320117950439453 3 25.179227828979492 4 25.034658432006836 5 24.887624740600586
		 6 24.739299774169922 7 24.590852737426758 8 24.44334602355957 9 24.297998428344727
		 10 24.155920028686523 11 24.018178939819336 12 23.885749816894531 13 23.75969123840332
		 14 23.64097785949707 15 23.541059494018555 16 23.468105316162109 17 23.418254852294922
		 18 23.387947082519531 19 23.373908996582031 20 23.373117446899414 21 23.382789611816406
		 22 23.400344848632813 23 23.423290252685547 24 23.449407577514648 25 23.476503372192383
		 26 23.502422332763672 27 23.525009155273438 28 23.542043685913086 29 23.497674942016602
		 30 23.352615356445313 31 23.12925910949707 32 22.849967956542969 33 22.537071228027344
		 34 22.212762832641602 35 21.899362564086914 36 21.619091033935547 37 21.39410400390625
		 38 21.246517181396484 39 21.198593139648438 40 21.272533416748047 41 21.474491119384766
		 42 21.784368515014648 43 22.185039520263672 44 22.659379959106445 45 23.190271377563477
		 46 23.760616302490234 47 24.353240966796875 48 24.951120376586914 49 25.537260055541992
		 50 26.09449577331543 51 26.605976104736328 52 27.054689407348633 53 27.423574447631836
		 54 27.695732116699219 55 27.854127883911133 56 27.88165283203125 57 27.816774368286133
		 58 27.710956573486328 59 27.570432662963867 60 27.401388168334961 61 27.210050582885742
		 62 27.002729415893555 63 26.785909652709961 64 26.566320419311523 65 26.350828170776367
		 66 26.146490097045898 67 25.960483551025391 68 25.800167083740234 69 25.672983169555664
		 70 25.586374282836914;
createNode animCurveTU -n "Character1_Spine1_visibility";
	rename -uid "7248C653-4A84-5FC2-F065-8C81D0882BD8";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftShoulder_translateX";
	rename -uid "248693CC-4F69-9CB2-5C28-CBA76D50136A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.0842323303222656;
createNode animCurveTL -n "Character1_LeftShoulder_translateY";
	rename -uid "88553939-47DD-0FD8-7C4F-C5A9E61CAAF4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.604855537414551;
createNode animCurveTL -n "Character1_LeftShoulder_translateZ";
	rename -uid "FE06D15E-432E-9329-5C31-A5A6DF6C1DE2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6404602527618408;
createNode animCurveTU -n "Character1_LeftShoulder_scaleX";
	rename -uid "C53E4ABD-4B40-F747-6AC9-82ABB8AED365";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftShoulder_scaleY";
	rename -uid "818B8907-421C-6CE9-85C6-4598296328C3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftShoulder_scaleZ";
	rename -uid "1F91C52D-4856-9D8F-EFEC-40850B190E4E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftShoulder_rotateX";
	rename -uid "941FFA5B-4CB5-8544-B5EF-B4AC5B264701";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 0.91715043783187866 1 0.78068786859512329
		 2 0.65425461530685425 3 0.53788048028945923 4 0.43321967124938965 5 0.3426588773727417
		 6 0.26857805252075195 7 0.21276067197322845 8 0.17596019804477692 9 0.15762446820735931
		 10 0.15578606724739075 11 0.16713042557239532 12 0.18725405633449554 13 0.21111400425434113
		 14 0.23368385434150693 15 0.26065400242805481 16 0.30029910802841187 17 0.35186058282852173
		 18 0.41445717215538025 19 0.48710113763809204 20 0.5687251091003418 21 0.65820765495300293
		 22 0.75441199541091919 23 0.85622149705886841 24 0.9625861644744873 25 1.072566032409668
		 26 1.1853837966918945 27 1.3004717826843262 28 1.417532205581665 29 1.555817723274231
		 30 1.7314366102218628 31 1.9389755725860598 32 2.1719264984130859 33 2.4225597381591797
		 34 2.6820039749145508 35 2.9405307769775391 36 3.1880342960357666 37 3.4147055149078369
		 38 3.6119024753570552 39 3.7732057571411137 40 3.8956494331359863 41 3.9716653823852544
		 42 3.9997849464416504 43 3.9889883995056152 44 3.9474751949310303 45 3.8827099800109863
		 46 3.801477432250977 47 3.709941148757935 48 3.6137092113494873 49 3.517902135848999
		 50 3.42722487449646 51 3.3460381031036377 52 3.2784326076507568 53 3.2283027172088623
		 54 3.1994204521179199 55 3.1955094337463379 56 3.2203207015991211 57 3.2415931224822998
		 58 3.2237730026245117 59 3.165010929107666 60 3.0635898113250732 61 2.9190645217895508
		 62 2.7331249713897705 63 2.5101957321166992 64 2.2577886581420898 65 1.986623167991638
		 66 1.7105262279510498 67 1.4461040496826172 68 1.2121833562850952 69 1.0290024280548096
		 70 0.91715043783187866;
createNode animCurveTA -n "Character1_LeftShoulder_rotateY";
	rename -uid "F053A1BF-45C3-EF07-ED92-8BBC09413268";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 1.2682857513427734 1 1.765350341796875
		 2 2.4106082916259766 3 3.1742396354675293 4 4.0261616706848145 5 4.936150074005127
		 6 5.8739109039306641 7 6.8091621398925781 8 7.7116961479187012 9 8.5514402389526367
		 10 9.2984991073608398 11 9.9231758117675781 12 10.395952224731445 13 10.687423706054687
		 14 10.768193244934082 15 10.707489013671875 16 10.592300415039063 17 10.419768333435059
		 18 10.18703556060791 19 9.8912343978881836 20 9.5295171737670898 21 9.0990352630615234
		 22 8.5969562530517578 23 8.0204582214355469 24 7.3667230606079102 25 6.6329355239868164
		 26 5.8162789344787598 27 4.9139184951782227 28 3.9229996204376221 29 2.8137657642364502
		 30 1.579735279083252 31 0.2530343234539032 32 -1.1340508460998535 33 -2.5490734577178955
		 34 -3.9594502449035645 35 -5.332514762878418 36 -6.6355938911437988 37 -7.836094856262207
		 38 -8.9016313552856445 39 -9.8001937866210937 40 -10.500382423400879 41 -11.038178443908691
		 42 -11.474835395812988 43 -11.817523956298828 44 -12.073375701904297 45 -12.249452590942383
		 46 -12.352745056152344 47 -12.390162467956543 48 -12.368535041809082 49 -12.294612884521484
		 50 -12.175078392028809 51 -12.01655387878418 52 -11.825621604919434 53 -11.608834266662598
		 54 -11.372742652893066 55 -11.123907089233398 56 -10.868929862976074 57 -10.49073600769043
		 58 -9.8915719985961914 59 -9.1076192855834961 60 -8.1756525039672852 61 -7.1328072547912598
		 62 -6.0163025856018066 63 -4.8631768226623535 64 -3.70998215675354 65 -2.5925419330596924
		 66 -1.5457620620727539 67 -0.60356366634368896 68 0.20104484260082245 69 0.83571016788482666
		 70 1.2682857513427734;
createNode animCurveTA -n "Character1_LeftShoulder_rotateZ";
	rename -uid "3521E001-418B-8DAB-2A35-C0984CC8D66F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 10.821895599365234 1 10.524406433105469
		 2 10.421186447143555 3 10.485754013061523 4 10.691583633422852 5 11.012078285217285
		 6 11.420494079589844 7 11.889850616455078 8 12.392815589904785 9 12.901604652404785
		 10 13.387922286987305 11 13.822957038879395 12 14.177489280700684 13 14.422088623046873
		 14 14.527440071105957 15 14.565190315246582 16 14.622205734252928 17 14.692306518554689
		 18 14.76931095123291 19 14.847046852111816 20 14.919386863708496 21 14.980256080627441
		 22 15.023664474487305 23 15.043723106384279 24 15.034670829772951 25 14.990883827209474
		 26 14.906898498535156 27 14.777409553527834 28 14.59728527069092 29 14.345957756042482
		 30 14.027856826782227 31 13.675051689147949 32 13.319004058837891 33 12.990592002868652
		 34 12.720166206359863 35 12.537637710571289 36 12.472536087036133 37 12.554012298583984
		 38 12.810758590698242 39 13.270852088928223 40 13.961505889892578 41 14.767787933349611
		 42 15.554034233093263 43 16.315711975097656 44 17.048484802246094 45 17.748199462890625
		 46 18.410896301269531 47 19.032781600952148 48 19.610214233398438 49 20.139667510986328
		 50 20.617698669433594 51 21.040914535522461 52 21.405942916870117 53 21.709392547607422
		 54 21.947830200195313 55 22.117755889892578 56 22.215585708618164 57 22.114351272583008
		 58 21.720983505249023 59 21.082239151000977 60 20.244176864624023 61 19.252159118652344
		 62 18.15095329284668 63 16.985023498535156 64 15.798844337463377 65 14.637268066406248
		 66 13.545827865600586 67 12.570932388305664 68 11.759894371032715 69 11.16075611114502
		 70 10.821895599365234;
createNode animCurveTU -n "Character1_LeftShoulder_visibility";
	rename -uid "179FFE26-4818-D691-6A5F-8BA46EF98001";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftArm_translateX";
	rename -uid "B9721C53-4C45-F712-190B-ECB33383B729";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 9.2092180252075195;
createNode animCurveTL -n "Character1_LeftArm_translateY";
	rename -uid "98B29084-48BB-FEB4-5127-F59D4F12231C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.4172050952911377;
createNode animCurveTL -n "Character1_LeftArm_translateZ";
	rename -uid "83E14BAF-4CDE-7DF6-4D1A-02887F79EBA0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.69030004739761353;
createNode animCurveTU -n "Character1_LeftArm_scaleX";
	rename -uid "5363AFD8-4CE5-B341-BD1B-BDB967A0A745";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleY";
	rename -uid "6A3D271A-4018-138B-D3C7-A3944E7AAA1A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleZ";
	rename -uid "0C094EC9-4787-71FE-E024-279E685B75C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_LeftArm_rotateX";
	rename -uid "65B178BF-4474-7237-CF4C-1D91B80C8A42";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 41.490631103515625 1 41.215293884277344
		 2 40.788013458251953 3 40.307903289794922 4 39.86004638671875 5 39.509044647216797
		 6 39.296897888183594 7 39.244087219238281 8 39.352630615234375 9 39.609943389892578
		 10 39.992938995361328 11 40.471935272216797 12 41.014350891113281 13 41.588359832763672
		 14 42.166675567626953 15 42.656150817871094 16 42.979625701904297 17 43.140586853027344
		 18 43.143524169921875 19 42.993618011474609 20 42.696468353271484 21 42.257743835449219
		 22 41.682880401611328 23 40.976654052734375 24 40.142803192138672 25 39.183528900146484
		 26 38.098941802978516 27 36.886409759521484 28 35.539787292480469 29 34.036899566650391
		 30 32.4892578125 31 31.081172943115234 32 29.971649169921879 33 29.277570724487305
		 34 29.060842514038086 35 29.320169448852539 36 29.989126205444339 37 30.942743301391605
		 38 32.013339996337891 39 33.012874603271484 40 33.757259368896484 41 34.388969421386719
		 42 35.133251190185547 43 35.947544097900391 44 36.793949127197266 45 37.641887664794922
		 46 38.469173431396484 47 39.261566162109375 48 40.011337280273438 49 40.715469360351562
		 50 41.373859405517578 51 41.987674713134766 52 42.558036804199219 53 43.084945678710938
		 54 43.566318511962891 55 43.996929168701172 56 44.367317199707031 57 44.648921966552734
		 58 44.824592590332031 59 44.898380279541016 60 44.875209808349609 61 44.761138916015625
		 62 44.563655853271484 63 44.291919708251953 64 43.956966400146484 65 43.5718994140625
		 66 43.151866912841797 67 42.714027404785156 68 42.277362823486328 69 41.862369537353516
		 70 41.490631103515625;
createNode animCurveTA -n "Character1_LeftArm_rotateY";
	rename -uid "19C3C1F7-4BDC-E28D-F12D-4AA8D38D3129";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -57.329841613769531 1 -56.820377349853516
		 2 -56.073352813720703 3 -55.125045776367188 4 -54.014533996582031 5 -52.78387451171875
		 6 -51.47735595703125 7 -50.140090942382813 8 -48.816543579101563 9 -47.549163818359375
		 10 -46.377506256103516 11 -45.337753295898438 12 -44.462833404541016 13 -43.782989501953125
		 14 -43.326786041259766 15 -43.078990936279297 16 -42.999919891357422 17 -43.081600189208984
		 18 -43.316768646240234 19 -43.698703765869141 20 -44.220985412597656 21 -44.87738037109375
		 22 -45.661602020263672 23 -46.567241668701172 24 -47.587589263916016 25 -48.715560913085938
		 26 -49.943553924560547 27 -51.263359069824219 28 -52.666053771972656 29 -54.024005889892578
		 30 -55.247707366943359 31 -56.380222320556641 32 -57.452919006347656 33 -58.480979919433594
		 34 -59.463214874267578 35 -60.386562347412102 36 -61.234092712402344 37 -61.993843078613281
		 38 -62.665401458740234 39 -63.261791229248047 40 -63.805908203124993 41 -64.169425964355469
		 42 -64.233688354492187 43 -64.039360046386719 44 -63.627475738525398 45 -63.039470672607422
		 46 -62.317066192626953 47 -61.502204895019531 48 -60.636940002441399 49 -59.763351440429695
		 50 -58.923503875732422 51 -58.159381866455078 52 -57.512893676757813 53 -57.025833129882812
		 54 -56.739849090576172 55 -56.696422576904297 56 -56.936809539794922 57 -57.322910308837891
		 58 -57.684097290039063 59 -58.012626647949219 60 -58.300804138183601 61 -58.540962219238274
		 62 -58.725475311279304 63 -58.846775054931641 64 -58.89739990234375 65 -58.870063781738281
		 66 -58.757713317871094 67 -58.553604125976562 68 -58.251407623291016 69 -57.845260620117188
		 70 -57.329841613769531;
createNode animCurveTA -n "Character1_LeftArm_rotateZ";
	rename -uid "A0F5EE83-4524-5D14-0D76-B7B4B5CE0409";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -52.253425598144531 1 -51.319637298583984
		 2 -50.020496368408203 3 -48.478885650634766 4 -46.806442260742187 5 -45.098377227783203
		 6 -43.432388305664062 7 -41.870334625244141 8 -40.461616516113281 9 -39.246971130371094
		 10 -38.262046813964844 11 -37.540496826171875 12 -37.116313934326172 13 -37.025913238525391
		 14 -37.309864044189453 15 -37.789463043212891 16 -38.252590179443359 17 -38.693622589111328
		 18 -39.106559753417969 19 -39.485084533691406 20 -39.822559356689453 21 -40.111980438232422
		 22 -40.345890045166016 23 -40.516170501708984 24 -40.613903045654297 25 -40.629001617431641
		 26 -40.549903869628906 27 -40.363052368164063 28 -40.052349090576172 29 -39.818706512451172
		 30 -39.918338775634766 31 -40.407894134521484 32 -41.319877624511719 33 -42.651119232177734
		 34 -44.356403350830078 35 -46.346099853515625 36 -48.487800598144531 37 -50.613018035888672
		 38 -52.529392242431641 39 -54.035751342773438 40 -54.936180114746094 41 -55.534259796142578
		 42 -56.231086730957031 43 -56.986080169677734 44 -57.761146545410149 45 -58.523895263671868
		 46 -59.249107360839844 47 -59.918704986572266 48 -60.520664215087891 49 -61.047466278076172
		 50 -61.494560241699226 51 -61.858875274658196 52 -62.137634277343757 53 -62.327373504638672
		 54 -62.423007965087884 55 -62.416770935058601 56 -62.297115325927734 57 -62.070568084716797
		 58 -61.752620697021491 59 -61.342483520507805 60 -60.840938568115234 61 -60.250431060791016
		 62 -59.575218200683594 63 -58.821449279785163 64 -57.997219085693352 65 -57.112628936767578
		 66 -56.179618835449219 67 -55.2117919921875 68 -54.224132537841797 69 -53.232563018798828
		 70 -52.253425598144531;
createNode animCurveTU -n "Character1_LeftArm_visibility";
	rename -uid "D10F8735-4F09-96F8-FF7A-718C0EBBD205";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftForeArm_translateX";
	rename -uid "A0D835C6-406C-433A-5051-0990ACFA75AA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.0427684783935547;
createNode animCurveTL -n "Character1_LeftForeArm_translateY";
	rename -uid "D2A08919-4DAD-C051-4F75-BC8283BAD4F4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.323208808898926;
createNode animCurveTL -n "Character1_LeftForeArm_translateZ";
	rename -uid "1C96EB4D-4817-3C77-08D9-149F36464C05";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 16.678167343139648;
createNode animCurveTU -n "Character1_LeftForeArm_scaleX";
	rename -uid "4835B8A3-4788-7A5E-8CD2-4EAC9149ACFE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftForeArm_scaleY";
	rename -uid "15EF55EA-465D-5FA7-AF48-CEA3B390AC1C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftForeArm_scaleZ";
	rename -uid "EFE906F0-4816-8DD4-F516-EBA28F40C087";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftForeArm_rotateX";
	rename -uid "DE5F2BAE-40D2-E337-9035-AF84C074D406";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -11.516050338745117 1 -11.606062889099121
		 2 -11.858004570007324 3 -12.244843482971191 4 -12.739406585693359 5 -13.314399719238281
		 6 -13.942470550537109 7 -14.596317291259766 8 -15.248832702636719 9 -15.873234748840332
		 10 -16.443201065063477 11 -16.932895660400391 12 -17.316921234130859 13 -17.57017707824707
		 14 -17.667573928833008 15 -17.594257354736328 16 -17.368804931640625 17 -17.015705108642578
		 18 -16.559148788452148 19 -16.023244857788086 20 -15.432242393493652 21 -14.810772895812988
		 22 -14.184002876281738 23 -13.577725410461426 24 -13.018374443054199 25 -12.532901763916016
		 26 -12.148612976074219 27 -11.892901420593262 28 -11.79295539855957 29 -11.7825927734375
		 30 -11.778426170349121 31 -11.781482696533203 32 -11.79278564453125 33 -11.813361167907715
		 34 -11.844233512878418 35 -11.886431694030762 36 -11.940978050231934 37 -12.008896827697754
		 38 -12.091212272644043 39 -12.188950538635254 40 -12.303133010864258 41 -12.464930534362793
		 42 -12.697089195251465 43 -12.98796558380127 44 -13.325876235961914 45 -13.699123382568359
		 46 -14.096014022827148 47 -14.504886627197266 48 -14.914139747619627 49 -15.312271118164063
		 50 -15.687892913818359 51 -16.029750823974609 52 -16.32670783996582 53 -16.567722320556641
		 54 -16.741806030273438 55 -16.837940216064453 56 -16.844989776611328 57 -16.739713668823242
		 58 -16.505342483520508 59 -16.163585662841797 60 -15.736046791076658 61 -15.244183540344238
		 62 -14.709307670593263 63 -14.152703285217285 64 -13.595797538757324 65 -13.060416221618652
		 66 -12.569005966186523 67 -12.144761085510254 68 -11.811603546142578 69 -11.593921661376953
		 70 -11.516050338745117;
createNode animCurveTA -n "Character1_LeftForeArm_rotateY";
	rename -uid "5CDEB1C6-455E-F6AB-9F2C-8FA2B2254115";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  0 4.487816333770752 1 4.4993729591369629
		 2 4.5055637359619141 6 4.5049190521240234 7 4.5007781982421875 8 4.4894981384277344
		 9 4.4639105796813965 10 4.4143581390380859 11 4.3290886878967285 12 4.1949396133422852
		 13 3.9982507228851318 14 3.7260537147521973 15 3.3020908832550049 16 2.683419942855835
		 17 1.909839987754822 18 1.0220738649368286 19 0.060685392469167716 20 -0.93482643365859985
		 21 -1.926758408546448 22 -2.8797309398651123 23 -3.760979175567627 24 -4.5403704643249512
		 25 -5.1901097297668457 26 -5.684105396270752 27 -5.9969849586486816 28 -6.1027603149414062
		 29 -6.0957560539245605 30 -6.0891127586364746 31 -6.0836405754089355 33 -6.0793976783752441
		 34 -6.082186222076416 35 -6.0892581939697266 36 -6.1013431549072266 37 -6.1191449165344238
		 38 -6.143336296081543 39 -6.174555778503418 40 -6.2134008407592773 41 -6.254054069519043
		 42 -6.290062427520752 43 -6.3209190368652344 44 -6.346186637878418 45 -6.3657503128051758
		 46 -6.380012035369873 47 -6.3900299072265625 48 -6.3976044654846191 49 -6.4053044319152832
		 50 -6.4164438247680664 51 -6.4349989891052246 52 -6.4654803276062012 53 -6.5127434730529785
		 54 -6.5817561149597168 55 -6.6773114204406738 56 -6.8036880493164062 57 -6.7687439918518066
		 58 -6.4142203330993652 59 -5.7880697250366211 60 -4.9379076957702637 61 -3.9119746685028076
		 62 -2.7598388195037842 63 -1.5328242778778076 64 -0.28418451547622681 65 0.93096023797988892
		 66 2.0558722019195557 67 3.0326104164123535 68 3.8025455474853511 69 4.3069744110107422
		 70 4.487816333770752;
createNode animCurveTA -n "Character1_LeftForeArm_rotateZ";
	rename -uid "8C3EDE8F-4D8C-D432-603D-CE95C27E35EC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 30.392219543457028 1 30.548357009887695
		 2 30.982509613037109 3 31.64836502075195 4 32.499752044677734 5 33.490623474121094
		 6 34.575016021728516 7 35.707042694091797 8 36.840908050537109 9 37.930873870849609
		 10 38.931228637695312 11 39.796184539794922 12 40.479656219482422 13 40.935050964355469
		 14 41.114894866943359 15 40.991584777832031 16 40.595970153808594 17 39.966415405273438
		 18 39.141166687011719 19 38.159595489501953 20 37.0631103515625 21 35.895801544189453
		 22 34.704776763916016 23 33.540168762207031 24 32.454898834228516 25 31.50410270690918
		 26 30.744323730468746 27 30.232414245605465 28 30.024202346801758 29 29.993436813354492
		 30 29.978683471679688 31 29.98130989074707 32 30.002679824829102 33 30.044164657592773
		 34 30.107131958007809 35 30.192962646484371 36 30.303043365478512 37 30.438764572143558
		 38 30.601537704467773 39 30.792787551879883 40 31.013956069946286 41 31.324565887451175
		 42 31.768621444702148 43 32.324287414550781 44 32.969669342041016 45 33.682853698730469
		 46 34.441898345947266 47 35.224868774414062 48 36.009830474853516 49 36.774887084960937
		 50 37.498176574707031 51 38.157863616943359 52 38.732131958007813 53 39.199138641357422
		 54 39.536979675292969 55 39.723602294921875 56 39.736740112304688 57 39.528163909912109
		 58 39.065357208251953 59 38.397464752197266 60 37.575027465820312 61 36.648113250732422
		 62 35.664833068847656 63 34.670253753662109 64 33.705654144287109 65 32.808212280273437
		 66 32.011089324951172 67 31.343963623046879 68 30.834049224853512 69 30.507650375366214
		 70 30.392219543457028;
createNode animCurveTU -n "Character1_LeftForeArm_visibility";
	rename -uid "E5E8A604-4698-63B1-686F-0DAACFEF5DFE";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHand_translateX";
	rename -uid "3B0F7E33-4FBB-8FD3-BB03-D99E5AFF4881";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.29200950264930725;
createNode animCurveTL -n "Character1_LeftHand_translateY";
	rename -uid "FCDDDD22-46E5-790F-07DF-7E84D4C55FC0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -14.295157432556152;
createNode animCurveTL -n "Character1_LeftHand_translateZ";
	rename -uid "B8542906-433D-7112-17D5-078983AD9BBE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6319241523742676;
createNode animCurveTU -n "Character1_LeftHand_scaleX";
	rename -uid "8A44430F-46F2-BD07-3173-D1B72B4BCFF0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHand_scaleY";
	rename -uid "77060159-4ECF-1DD1-F88D-C09FBE79F65E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHand_scaleZ";
	rename -uid "45686310-41FA-01CD-68D5-029CB6FA66B8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHand_rotateX";
	rename -uid "BB632BD6-4F58-BF1D-12BB-1EBDDD938431";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 30 ".ktv[0:29]"  14 -0.47899496555328369 15 -0.48367598652839661
		 16 -0.49677523970603943 17 -0.51687252521514893 18 -0.54254275560379028 19 -0.57236391305923462
		 20 -0.60491985082626343 21 -0.63880270719528198 22 -0.67261552810668945 23 -0.70497441291809082
		 24 -0.73450362682342529 25 -0.75983768701553345 26 -0.77961719036102295 27 -0.7924836277961731
		 28 -0.7970765233039856 56 -0.7970765233039856 57 -0.79397648572921753 58 -0.78513270616531372
		 59 -0.77122503519058228 60 -0.75293231010437012 61 -0.73093092441558838 62 -0.70589792728424072
		 63 -0.67851489782333374 64 -0.64946353435516357 65 -0.61943316459655762 66 -0.58911508321762085
		 67 -0.5592074990272522 68 -0.53041297197341919 69 -0.50343817472457886 70 -0.47899496555328369;
createNode animCurveTA -n "Character1_LeftHand_rotateY";
	rename -uid "A968B6BC-4FE2-467A-82BF-96AB287187D2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 30 ".ktv[0:29]"  14 11.254157066345215 15 11.246070861816406
		 16 11.22342586517334 17 11.18863582611084 18 11.144113540649414 19 11.09227466583252
		 20 11.03553581237793 21 10.976320266723633 22 10.917059898376465 23 10.860194206237793
		 24 10.808165550231934 25 10.763426780700684 26 10.728429794311523 27 10.705635070800781
		 28 10.697491645812988 56 10.697491645812988 57 10.702987670898438 58 10.718661308288574
		 59 10.743285179138184 60 10.775629997253418 61 10.814467430114746 62 10.85856819152832
		 63 10.906705856323242 64 10.957653999328613 65 11.010190963745117 66 11.063098907470703
		 67 11.115160942077637 68 11.165163040161133 69 11.211897850036621 70 11.254157066345215;
createNode animCurveTA -n "Character1_LeftHand_rotateZ";
	rename -uid "AB211AFC-4C60-309F-A4F6-E0A9B03C4010";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 30 ".ktv[0:29]"  14 -9.6625833511352539 15 -9.6664190292358398
		 16 -9.677154541015625 17 -9.6936264038085937 18 -9.7146720886230469 19 -9.7391300201416016
		 20 -9.7658395767211914 21 -9.7936477661132812 22 -9.8214101791381836 23 -9.8479890823364258
		 24 -9.87225341796875 25 -9.8930759429931641 26 -9.9093379974365234 27 -9.9199190139770508
		 28 -9.9236955642700195 56 -9.9236955642700195 57 -9.9211463928222656 58 -9.9138736724853516
		 59 -9.9024381637573242 60 -9.8873996734619141 61 -9.8693161010742187 62 -9.8487472534179687
		 63 -9.8262548446655273 64 -9.8023996353149414 65 -9.7777490615844727 66 -9.7528715133666992
		 67 -9.7283391952514648 68 -9.7047271728515625 69 -9.6826143264770508 70 -9.6625833511352539;
createNode animCurveTU -n "Character1_LeftHand_visibility";
	rename -uid "CE4AAE5C-4B08-D9CE-1B23-29A21A34B71E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateX";
	rename -uid "31B861DC-44C8-EFC8-E8A5-5F9595184133";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1364259719848633;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateY";
	rename -uid "4C292A9C-4975-4D1C-7D46-BBB249F3DAC1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4241994619369507;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateZ";
	rename -uid "7283B562-44F0-FD7B-ADDA-D2AEAEFF24EC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.7211661338806152;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleX";
	rename -uid "0E7D7D84-45F1-9AF6-1692-34A8EE6F8B38";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleY";
	rename -uid "7F9C5526-4931-1F73-2162-B59E58D74545";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleZ";
	rename -uid "6D793CD0-4F73-15D1-AB20-859D25DDE174";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateX";
	rename -uid "49A170A9-480D-33DC-E8ED-7A816F29DB81";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -64.458450317382813;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateY";
	rename -uid "51CC7861-4E47-29C3-4474-408139977432";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.9103827476501465;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateZ";
	rename -uid "CE5BA656-4F05-5BC2-8763-A290127E27E1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -41.617835998535156;
createNode animCurveTU -n "Character1_LeftHandThumb1_visibility";
	rename -uid "A8453271-44EB-5CF6-9062-44802C7A48C0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateX";
	rename -uid "91F40D07-41E0-BE8F-A507-6A98C133AA1D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.8329257965087891;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateY";
	rename -uid "18A3082E-4D25-B0F7-C468-43BB22F2686A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.094426639378070831;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateZ";
	rename -uid "0F264491-4DE2-C0D9-B97C-D893F6E43EF3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5438374280929565;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleX";
	rename -uid "71764F5F-49E4-5F46-C40D-27889F29F894";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleY";
	rename -uid "367C2C3B-48B9-67BD-15DB-C18947F45532";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleZ";
	rename -uid "434F0BB2-405E-89D7-3C2C-5AB7A2A487AF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateX";
	rename -uid "79FB3CF3-4CA9-CEEF-DD3B-859ABAE63F0E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.355228424072266;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateY";
	rename -uid "0D24D68D-4951-EB9F-2361-099A98B6E026";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 20.610536575317383;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateZ";
	rename -uid "1271C0CE-463D-8112-2B3A-03ABA4FAEC45";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.5256719589233398;
createNode animCurveTU -n "Character1_LeftHandThumb2_visibility";
	rename -uid "6881E04C-408F-9E74-CF65-17A690220591";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateX";
	rename -uid "75A89D16-4F5A-9C85-8470-32A3E7EA06E2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7747817039489746;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateY";
	rename -uid "EF702AA6-4B7F-82CD-CA8D-58AC9597104C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3182404041290283;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateZ";
	rename -uid "93C89F91-4C52-0DE0-9C81-42918B6F1875";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.26324462890625;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleX";
	rename -uid "AEE63289-4525-A7A0-9651-FCB580E84EA5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleY";
	rename -uid "62FCCFCB-48D4-D287-0802-D0B11769AAD3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleZ";
	rename -uid "E3552673-4F59-04EA-D500-E58CD1A48590";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateX";
	rename -uid "7068A54F-4B65-7FB3-EFE5-2285BBF2FA67";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.7978660328881233e-007;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateY";
	rename -uid "F231D4A2-4068-0926-D3E0-11AB73D64BF0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.0209041186662944e-008;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateZ";
	rename -uid "E2E696A1-4975-8350-9B43-B68709A01626";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.7573543093240005e-007;
createNode animCurveTU -n "Character1_LeftHandThumb3_visibility";
	rename -uid "B492BEAE-44A4-502D-1B93-7EAEC3E03C04";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateX";
	rename -uid "C8FA915D-456A-2712-058B-0B9A64FB1DD7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.854037284851074;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateY";
	rename -uid "2DBEB61E-4D97-CD82-31AE-5EB91EF73087";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2616198062896729;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateZ";
	rename -uid "5BC9F00E-4C34-0C5E-E52A-128161FAF7B1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.944699764251709;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleX";
	rename -uid "1950ADA4-4D86-70B9-CAFA-B4A8DF6DD31C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleY";
	rename -uid "4A0014FB-4764-C555-4809-F0BB74135874";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleZ";
	rename -uid "C5325D8A-43C8-954B-28DC-C5A350E251E5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateX";
	rename -uid "F6705F15-457B-6518-01E6-4D811F8E7D55";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 16.684127807617187;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateY";
	rename -uid "ADFBD765-46E9-1E9A-66B4-55A66E5FD03C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 12.993002891540527;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateZ";
	rename -uid "3EB15BF2-473B-0A5A-57D6-DD8131549789";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 9.3133392333984375;
createNode animCurveTU -n "Character1_LeftHandIndex1_visibility";
	rename -uid "29B4F49D-440F-FF79-FBCC-CF9BB0224FFE";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateX";
	rename -uid "5D331A56-42C3-4147-AF6A-599A568471A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.1721835136413574;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateY";
	rename -uid "7AE38146-47E8-4E34-F091-F0BAEF9E7EFE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.9652466773986816;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateZ";
	rename -uid "78C970E0-48A8-7A81-DD80-BD8FCDFA4D2E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.8871781826019287;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleX";
	rename -uid "546870B1-4B8B-C913-F3A2-B38E9E1066BA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleY";
	rename -uid "1A4C25B2-4536-981B-4B8C-CB93FFD7FC74";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleZ";
	rename -uid "9A58E5DF-4725-39C7-2B6B-9AA7995EA0A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateX";
	rename -uid "E80889C6-4CBF-FAC8-B6DB-F8A6830C0785";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.4679555892944327;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateY";
	rename -uid "2E0F8461-4E02-4393-B8BE-5E9259271D43";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -34.115741729736328;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateZ";
	rename -uid "DE146EC5-4086-5B36-5990-BBB04F0ED180";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 81.871932983398438;
createNode animCurveTU -n "Character1_LeftHandIndex2_visibility";
	rename -uid "0AAD9C56-46A9-5526-48BC-62925BFF39FD";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateX";
	rename -uid "E7EC34B2-4EFE-0CEE-72C5-E9824FA469E7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.5761528015136719;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateY";
	rename -uid "41F27C8E-4296-59F7-80C3-ADA4EEF0EE65";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8397388458251953;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateZ";
	rename -uid "61C1756A-495A-51F0-CD25-35A048A145DF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3069263696670532;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleX";
	rename -uid "8B5AA846-4FEC-3F1C-3AFD-3EB743DB5417";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleY";
	rename -uid "8DE4A2E5-4154-68D4-07DC-6EAF467AF905";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleZ";
	rename -uid "C5BD3805-4367-243C-4B21-CBBA8DBDC54B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateX";
	rename -uid "20FE2BF4-408F-D96A-742A-A484D42E6168";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.5363208894523268e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateY";
	rename -uid "25B320D1-409F-98C9-1BBB-6591D9CDBA33";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.539001968671073e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateZ";
	rename -uid "2BEE0226-49E8-6CB3-6E19-D68D7B5637A9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.9878401025816856e-007;
createNode animCurveTU -n "Character1_LeftHandIndex3_visibility";
	rename -uid "AC741EB9-459C-1E70-FA37-11A988FF4B65";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing1_translateX";
	rename -uid "6AC938FD-4DFE-C433-CF34-ED89730590EF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4602031707763672;
createNode animCurveTL -n "Character1_LeftHandRing1_translateY";
	rename -uid "C51AA9C8-4DCE-90F9-383E-A4B9CF7A09F1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.5015654563903809;
createNode animCurveTL -n "Character1_LeftHandRing1_translateZ";
	rename -uid "CD4A82EE-4B8A-5ACF-E705-5F853C49E7DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.6178884506225586;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleX";
	rename -uid "DC1899C5-4C7F-9B63-5FA2-488BE82A21FF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleY";
	rename -uid "F0D01389-48ED-1F2D-EC04-388997A8408C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleZ";
	rename -uid "085F6F37-4258-C6B2-A7E7-828760CA5C33";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateX";
	rename -uid "F8ECEA99-42A6-BA00-EAA3-A988276B6E71";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2500913143157959;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateY";
	rename -uid "CD47A943-4904-D270-B902-69A7A07BF820";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.943532943725585;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateZ";
	rename -uid "6758D86F-462C-8841-E1FA-0F8E20E6204F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.490734100341797;
createNode animCurveTU -n "Character1_LeftHandRing1_visibility";
	rename -uid "B44CACD5-4E63-C772-52C0-0A9414EFF717";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing2_translateX";
	rename -uid "1B5F6C85-4A83-FD8F-8E41-978C853622FD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.9237353801727295;
createNode animCurveTL -n "Character1_LeftHandRing2_translateY";
	rename -uid "54DC2D40-4095-1161-4C28-C690AECEA0A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.0094923973083496;
createNode animCurveTL -n "Character1_LeftHandRing2_translateZ";
	rename -uid "369AE7F5-49E9-E3AB-A9B8-CC981604442B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8221585750579834;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleX";
	rename -uid "AA7AFF9F-4C1F-43B1-A170-2ABD2F1FC76F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleY";
	rename -uid "20604603-42BC-5AAE-F5FE-A0A844C44BFE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleZ";
	rename -uid "B15454B2-4F41-E0A3-CD44-EFB2285B4093";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateX";
	rename -uid "D014E40F-4FBC-6C2F-449D-2085E95E152C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 43.8316650390625;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateY";
	rename -uid "C3A8A674-487C-482D-D810-B39EEB1CB7A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -33.527976989746094;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateZ";
	rename -uid "CC3065FD-4963-E3DC-B7BB-2D807D8566A7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 64.567039489746094;
createNode animCurveTU -n "Character1_LeftHandRing2_visibility";
	rename -uid "C1059AAA-4688-23E8-6BE6-6A8C98AA81BC";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing3_translateX";
	rename -uid "193BCD55-4261-077D-4C58-7FA59FE3F5BF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6098947525024414;
createNode animCurveTL -n "Character1_LeftHandRing3_translateY";
	rename -uid "29236801-4D36-E8EC-1B06-EF9B645C9B05";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2952473163604736;
createNode animCurveTL -n "Character1_LeftHandRing3_translateZ";
	rename -uid "49EB8249-4AB7-04FE-D714-AEB42AF27326";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3554067611694336;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleX";
	rename -uid "CE838405-4F5B-5D1B-2A97-55B56A549D39";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleY";
	rename -uid "0FA48656-4D1F-5848-294C-85B597FA99AD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleZ";
	rename -uid "9B7EA30F-4C95-14D6-CCAC-5280C1F8604E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateX";
	rename -uid "66D9C4C6-4860-8F9D-A794-3EA6F7F7E6D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.0067194712919445e-008;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateY";
	rename -uid "962EEA56-4A4B-0A18-9598-5282B05F22EA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.2531626099371351e-007;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateZ";
	rename -uid "0885B94A-47B5-E493-406B-3788747D6B0A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5933010761036712e-007;
createNode animCurveTU -n "Character1_LeftHandRing3_visibility";
	rename -uid "2A2B8D16-45FF-A605-2BBA-B7B98E88066F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightShoulder_translateX";
	rename -uid "09194D72-4706-0349-6D1D-97933F0D7BF5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 14.332768440246582;
createNode animCurveTL -n "Character1_RightShoulder_translateY";
	rename -uid "E5CC7790-4928-26A5-0FE3-D0A6AEE88544";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5798094272613525;
createNode animCurveTL -n "Character1_RightShoulder_translateZ";
	rename -uid "F8F6EC5A-4FE3-5203-A59E-BA824849A782";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1086856126785278;
createNode animCurveTU -n "Character1_RightShoulder_scaleX";
	rename -uid "FEE13B48-481F-59EC-FC35-11A7CB5F21B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightShoulder_scaleY";
	rename -uid "43800E9B-477F-502F-EE80-6F8EFCD5FB82";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightShoulder_scaleZ";
	rename -uid "14425240-4164-3B44-1B3A-1BB0FC1F9E21";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightShoulder_rotateX";
	rename -uid "A18D5585-474C-277E-E969-A2986A8C8167";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 1.6447980403900146 1 1.5487673282623291
		 2 1.2842806577682495 3 0.89049547910690308 4 0.40740928053855896 5 -0.12820622324943542
		 6 -0.68518984317779541 7 -1.2387348413467407 8 -1.7697609663009644 9 -2.2633688449859619
		 10 -2.7068562507629395 11 -3.0877728462219238 12 -3.3924293518066406 13 -3.6051983833312988
		 14 -3.7086498737335205 15 -3.7454848289489746 16 -3.7695271968841548 17 -3.7789971828460689
		 18 -3.7720386981964111 19 -3.7467734813690186 20 -3.7013385295867924 21 -3.6339244842529292
		 22 -3.5428030490875244 23 -3.42635178565979 24 -3.2830753326416016 25 -3.1116194725036621
		 26 -2.9107837677001953 27 -2.679534912109375 28 -2.417011022567749 29 -2.1494274139404297
		 30 -1.9051219224929807 31 -1.6867626905441284 32 -1.4972689151763916 33 -1.3397669792175293
		 34 -1.2175520658493042 35 -1.1340334415435791 36 -1.0926997661590576 37 -1.0970659255981445
		 38 -1.1506310701370239 39 -1.2568292617797852 40 -1.4189867973327637 41 -1.5710287094116211
		 42 -1.6516876220703125 43 -1.6716575622558594 44 -1.641826868057251 45 -1.5731090307235718
		 46 -1.4763062000274658 47 -1.3620141744613647 48 -1.2405723333358765 49 -1.1220418214797974
		 50 -1.0162252187728882 51 -0.93269699811935425 52 -0.88086903095245361 53 -0.87006169557571411
		 54 -0.90959304571151722 55 -1.0088993310928345 56 -1.1776658296585083 57 -1.3201302289962769
		 58 -1.3448445796966553 59 -1.2672468423843384 60 -1.1020693778991699 61 -0.86400496959686279
		 62 -0.5682523250579834 63 -0.23095911741256714 64 0.13043318688869476 65 0.4969613254070282
		 66 0.84806734323501587 67 1.1617217063903809 68 1.4147721529006958 69 1.5835626125335693
		 70 1.6447980403900146;
createNode animCurveTA -n "Character1_RightShoulder_rotateY";
	rename -uid "1ED1935E-4F17-FE60-6625-97B147BE5635";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 1.1307182312011719 1 0.81058430671691895
		 2 -0.089275486767292023 3 -1.4805248975753784 4 -3.2754261493682861 5 -5.3842549324035645
		 6 -7.7138171195983878 7 -10.166974067687988 8 -12.642945289611816 9 -15.038078308105471
		 10 -17.246801376342773 11 -19.162590026855469 12 -20.678770065307617 13 -21.689149856567383
		 14 -22.088485717773438 15 -22.098743438720703 16 -22.014633178710938 17 -21.84105110168457
		 18 -21.582963943481445 19 -21.245386123657227 20 -20.833353042602539 21 -20.35191535949707
		 22 -19.806112289428711 23 -19.200963973999023 24 -18.541444778442383 25 -17.832483291625977
		 26 -17.078941345214844 27 -16.285598754882812 28 -15.457144737243651 29 -14.647190093994141
		 30 -13.907570838928223 31 -13.240147590637207 32 -12.646670341491699 33 -12.128796577453613
		 34 -11.688104629516602 35 -11.326131820678711 36 -11.044389724731445 37 -10.844395637512207
		 38 -10.727707862854004 39 -10.695949554443359 40 -10.750846862792969 41 -10.773907661437988
		 42 -10.659772872924805 43 -10.429904937744141 44 -10.105710029602051 45 -9.7085857391357422
		 46 -9.25994873046875 47 -8.7812585830688477 48 -8.2940311431884766 49 -7.8198390007019034
		 50 -7.3803067207336426 51 -6.9971060752868652 52 -6.6919422149658203 53 -6.4865431785583496
		 54 -6.4026479721069336 55 -6.4619832038879395 56 -6.6862525939941406 57 -6.8394231796264648
		 58 -6.7010488510131836 59 -6.3170061111450195 60 -5.7338018417358398 61 -4.9980697631835937
		 62 -4.1561470031738281 63 -3.2537329196929932 64 -2.3356044292449951 65 -1.4454385042190552
		 66 -0.62572997808456421 67 0.082131527364253998 68 0.63761579990386963 69 1.000601053237915
		 70 1.1307182312011719;
createNode animCurveTA -n "Character1_RightShoulder_rotateZ";
	rename -uid "5B83F52B-49D9-9BF7-E9DA-38B9077E5341";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 18.37775993347168 1 18.187248229980469
		 2 17.66105842590332 3 16.873630523681641 4 15.90212821960449 5 14.820598602294922
		 6 13.695999145507813 7 12.585972785949707 8 11.538063049316406 9 10.589974403381348
		 10 9.7705612182617187 11 9.1014585494995117 12 8.5993642807006836 13 8.2791872024536133
		 14 8.1583023071289063 15 8.1677732467651367 16 8.2208766937255859 17 8.3134212493896484
		 18 8.4414033889770508 19 8.6009902954101562 20 8.7884902954101562 21 9.0003433227539062
		 22 9.2330894470214844 23 9.4833555221557617 24 9.7478199005126953 25 10.023196220397949
		 26 10.306206703186035 27 10.593550682067871 28 10.881882667541504 29 11.16943359375
		 30 11.45328426361084 31 11.728413581848145 32 11.989930152893066 33 12.233080863952637
		 34 12.453253746032715 35 12.64599609375 36 12.807027816772461 37 12.932249069213867
		 38 13.017767906188965 39 13.05989933013916 40 13.055194854736328 41 13.03148365020752
		 42 13.017535209655762 43 13.011932373046875 44 13.013584136962891 45 13.021457672119141
		 46 13.034355163574219 47 13.050756454467773 48 13.0687255859375 49 13.085865020751953
		 50 13.099329948425293 51 13.10589599609375 52 13.102068901062012 53 13.084255218505859
		 54 13.048972129821777 55 12.993109703063965 56 12.914251327514648 57 12.910264015197754
		 58 13.062834739685059 59 13.347945213317871 60 13.74315357208252 61 14.226533889770508
		 62 14.775796890258789 63 15.367593765258791 64 15.976994514465332 65 16.577167510986328
		 66 17.139287948608398 67 17.6326904296875 68 18.025314331054687 69 18.284442901611328
		 70 18.37775993347168;
createNode animCurveTU -n "Character1_RightShoulder_visibility";
	rename -uid "C17C9374-438F-A1BA-0DB2-FABBD61720E1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightArm_translateX";
	rename -uid "4C763FA1-47C4-CACB-47C9-62B81A31D05B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.8354010581970215;
createNode animCurveTL -n "Character1_RightArm_translateY";
	rename -uid "C8DB78EB-4B57-8BEC-CB98-F2AE4A1183D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1704782247543335;
createNode animCurveTL -n "Character1_RightArm_translateZ";
	rename -uid "689F7894-4301-1127-855F-19953C048FE4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.3259291648864746;
createNode animCurveTU -n "Character1_RightArm_scaleX";
	rename -uid "B22A55B9-430A-CDF6-1254-EEAAC1FD3874";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightArm_scaleY";
	rename -uid "6223CE32-41C5-6B9B-073D-91B73C3E3AC1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightArm_scaleZ";
	rename -uid "547D92D4-456B-F57A-CE5F-E5A0656D886C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightArm_rotateX";
	rename -uid "48C49F3C-484C-61CD-522D-53B290EFA678";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 54.190940856933594 1 53.499149322509766
		 2 52.078140258789063 3 50.081924438476562 4 47.667522430419922 5 44.990550994873047
		 6 42.200950622558594 7 39.439304351806641 8 36.833881378173828 9 34.499179840087891
		 10 32.536418914794922 11 31.036127090454105 12 30.082967758178711 13 29.732254028320309
		 14 30.03990364074707 15 30.753299713134766 16 31.546569824218746 17 32.405323028564453
		 18 33.315334320068359 19 34.261684417724609 20 35.228046417236328 21 36.19622802734375
		 22 37.145919799804688 23 38.054790496826172 24 38.898796081542969 25 39.652828216552734
		 26 40.291587829589844 27 40.790813446044922 28 41.128715515136719 29 41.505142211914063
		 30 41.866291046142578 31 42.234729766845703 32 42.633659362792969 33 43.083408355712891
		 34 43.597827911376953 35 44.180538177490234 36 44.815399169921875 37 45.474674224853516
		 38 46.119667053222656 39 46.695003509521484 40 47.137500762939453 41 47.487808227539063
		 42 47.939205169677734 43 48.450450897216797 44 48.983776092529297 45 49.506233215332031
		 46 49.990230560302734 47 50.413463592529297 48 50.758403778076172 49 51.011638641357422
		 50 51.163192749023438 51 51.205936431884766 52 51.119308471679688 53 50.877918243408203
		 54 50.471050262451172 55 49.890823364257813 56 49.132919311523438 57 48.452602386474609
		 58 48.086711883544922 59 48.004222869873047 60 48.171298980712891 61 48.551376342773437
		 62 49.104957580566406 63 49.789543151855469 64 50.559684753417969 65 51.367275238037109
		 66 52.162090301513672 67 52.892616271972656 68 53.507240295410156 69 53.955692291259766
		 70 54.190940856933594;
createNode animCurveTA -n "Character1_RightArm_rotateY";
	rename -uid "2AA8927B-47B3-9284-1E37-0CB34F48E4F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 35.272922515869141 1 35.162727355957031
		 2 35.035087585449219 3 34.889331817626953 4 34.722465515136719 5 34.533657073974609
		 6 34.327419281005859 7 34.115306854248047 8 33.916133880615234 9 33.7547607421875
		 10 33.659854888916016 11 33.660964965820313 12 33.785404205322266 13 34.050716400146484
		 14 34.463771820068359 15 34.899513244628906 16 35.244186401367188 17 35.516326904296875
		 18 35.734958648681641 19 35.91851806640625 20 36.084156036376953 21 36.247352600097656
		 22 36.421852111816406 23 36.619853973388672 24 36.852397918701172 25 37.129959106445313
		 26 37.463222503662109 27 37.864036560058594 28 38.346614837646484 29 39.061954498291016
		 30 40.051280975341797 31 41.253490447998047 32 42.606723785400391 33 44.046543121337891
		 34 45.504978179931641 35 46.910354614257813 36 48.178962707519531 37 49.228675842285156
		 38 49.988235473632812 39 50.388175964355469 40 50.361526489257813 41 49.985950469970703
		 42 49.380569458007813 43 48.575355529785156 44 47.600681304931641 45 46.487216949462891
		 46 45.265758514404297 47 43.966960906982422 48 42.621219635009766 49 41.258594512939453
		 50 39.908893585205078 51 38.601966857910156 52 37.357173919677734 53 36.191047668457031
		 54 35.131683349609375 55 34.211166381835937 56 33.46710205078125 57 32.910194396972656
		 58 32.530071258544922 59 32.326957702636719 60 32.289424896240234 61 32.397590637207031
		 62 32.625869750976562 63 32.9453125 64 33.325458526611328 65 33.735813140869141 66 34.146873474121094
		 67 34.530834197998047 68 34.861949920654297 69 35.116565704345703 70 35.272922515869141;
createNode animCurveTA -n "Character1_RightArm_rotateZ";
	rename -uid "C7BF1BF9-4C9F-8F5D-BD55-868F7FDF51F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 64.343818664550781 1 64.057708740234375
		 2 63.002777099609375 3 61.306098937988281 4 59.091793060302734 5 56.484580993652344
		 6 53.611793518066406 7 50.604427337646484 8 47.597019195556641 9 44.727310180664063
		 10 42.135951995849609 11 39.966522216796875 12 38.366065979003906 13 37.503532409667969
		 14 37.55999755859375 15 38.249771118164063 16 39.138942718505859 17 40.178466796875
		 18 41.321151733398437 19 42.521575927734375 20 43.735946655273437 21 44.922016143798828
		 22 46.038932800292969 23 47.047115325927734 24 47.907993316650391 25 48.583751678466797
		 26 49.036849975585937 27 49.229618072509766 28 49.123748779296875 29 48.937667846679688
		 30 48.61187744140625 31 48.210113525390625 32 47.795024871826172 33 47.424373626708984
		 34 47.146621704101563 35 46.996170043945313 36 47.004295349121094 37 47.173847198486328
		 38 47.465534210205078 39 47.821163177490234 40 48.175079345703125 41 48.390064239501953
		 42 48.581993103027344 43 48.733196258544922 44 48.829647064208984 45 48.862632751464844
		 46 48.829605102539062 47 48.734352111816406 48 48.586692810058594 49 48.401920318603516
		 50 48.200119018554688 51 48.005374908447266 52 47.859027862548828 53 47.809406280517578
		 54 47.894996643066406 55 48.155460357666016 56 48.630622863769531 57 49.409900665283203
		 58 50.498092651367188 59 51.813468933105469 60 53.282630920410156 61 54.840229034423828
		 62 56.427829742431641 63 57.992519378662109 64 59.485290527343757 65 60.859539031982429
		 66 62.069671630859375 67 63.070068359375007 68 63.814353942871094 69 64.255073547363281
		 70 64.343818664550781;
createNode animCurveTU -n "Character1_RightArm_visibility";
	rename -uid "57560FBA-4C93-A66F-E879-7DB3C6C55B9A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightForeArm_translateX";
	rename -uid "0E3FABCC-4702-334E-1107-ADB771E7D3AC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.753132820129395;
createNode animCurveTL -n "Character1_RightForeArm_translateY";
	rename -uid "D1777BAD-4904-6F1F-C858-969465BEF9E9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.862128257751465;
createNode animCurveTL -n "Character1_RightForeArm_translateZ";
	rename -uid "FA1AEB8C-4E72-C762-69AE-ACAB7A9B8BDA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.671875953674316;
createNode animCurveTU -n "Character1_RightForeArm_scaleX";
	rename -uid "0E32D275-421F-3550-6A25-7EACF689C04F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightForeArm_scaleY";
	rename -uid "7E71F679-4415-B1BF-CF90-40AEE8F1165F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightForeArm_scaleZ";
	rename -uid "7F9CD556-4B99-EC51-0EE8-7081B96DFDE4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightForeArm_rotateX";
	rename -uid "B8D622B6-420B-39F7-16A1-C0A62D67E278";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 20.720895767211914 1 21.429584503173828
		 2 21.91722297668457 3 22.163619995117188 4 22.144067764282227 5 21.838720321655273
		 6 21.241123199462891 7 20.365528106689453 8 19.252408981323242 9 17.970876693725586
		 10 16.617210388183594 11 15.308959960937502 12 14.175594329833984 13 13.335342407226563
		 14 12.891559600830078 15 12.795296669006348 16 12.913411140441895 17 13.214164733886719
		 18 13.660400390625 19 14.212879180908203 20 14.833196640014648 21 15.486220359802246
		 22 16.141994476318359 23 16.777002334594727 24 17.374906539916992 25 17.926599502563477
		 26 18.429784774780273 27 18.888076782226562 28 19.309768676757813 29 18.785898208618164
		 30 18.33671760559082 31 17.948410034179688 32 17.610252380371094 33 17.314271926879883
		 34 17.054821014404297 35 16.828039169311523 36 16.63568115234375 37 16.478414535522461
		 38 16.350038528442383 39 16.241909027099609 40 16.142822265625 41 15.968686103820803
		 42 15.714713096618651 43 15.399782180786135 44 15.042239189147951 45 14.659566879272461
		 46 14.267992973327637 47 13.882179260253906 48 13.514963150024414 49 13.177244186401367
		 50 12.878000259399414 51 12.624524116516113 52 12.430036544799805 53 12.310356140136719
		 54 12.276351928710937 55 12.340371131896973 56 12.516837120056152 57 12.771485328674316
		 58 13.064469337463379 59 13.403264999389648 60 13.794343948364258 61 14.242783546447754
		 62 14.751979827880858 63 15.323361396789551 64 15.956207275390625 65 16.647544860839844
		 66 17.392095565795898 67 18.182321548461914 68 19.008546829223633 69 19.859161376953125
		 70 20.720895767211914;
createNode animCurveTA -n "Character1_RightForeArm_rotateY";
	rename -uid "C1C0600E-4D9D-9080-2456-43AF8695A578";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 19.47700309753418 1 20.577213287353516
		 2 21.901618957519531 3 23.399990081787109 4 25.015090942382813 5 26.683547973632813
		 6 28.337991714477539 7 29.910762786865234 8 31.338888168334961 9 32.569526672363281
		 10 33.564552307128906 11 34.302734375 12 34.778175354003906 13 35.011390686035156
		 14 35.029834747314453 15 34.841964721679688 16 34.463981628417969 17 33.911430358886719
		 18 33.196632385253906 19 32.331111907958984 20 31.327590942382809 21 30.201410293579102
		 22 28.971303939819332 23 27.659648895263672 24 26.292289733886719 25 24.897924423217773
		 26 23.507558822631836 27 22.153703689575195 28 20.869806289672852 29 19.923933029174805
		 30 18.860456466674805 31 17.717588424682617 32 16.534887313842773 33 15.352912902832033
		 34 14.212851524353027 35 13.156269073486328 36 12.268509864807129 37 11.617844581604004
		 38 11.220369338989258 39 11.092126846313477 40 11.24913215637207 41 11.499421119689941
		 42 11.727656364440918 43 11.924398422241211 44 12.081537246704102 45 12.192931175231934
		 46 12.254632949829102 47 12.26500415802002 48 12.224550247192383 49 12.135577201843262
		 50 12.001660346984863 51 11.827033996582031 52 11.65748405456543 53 11.552464485168457
		 54 11.53582763671875 55 11.629915237426758 56 11.855109214782715 57 12.196247100830078
		 58 12.617324829101563 59 13.105712890625 60 13.64871883392334 61 14.23358154296875
		 62 14.847635269165039 63 15.478429794311523 64 16.113960266113281 65 16.742948532104492
		 66 17.355081558227539 67 17.941303253173828 68 18.4940185546875 69 19.007295608520508
		 70 19.47700309753418;
createNode animCurveTA -n "Character1_RightForeArm_rotateZ";
	rename -uid "188DA469-40E6-CE82-BD19-998B05AA7B87";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -32.175422668457031 1 -32.592525482177734
		 2 -33.904922485351563 3 -35.989303588867188 4 -38.725318908691406 5 -41.992107391357422
		 6 -45.663684844970703 7 -49.603782653808594 8 -53.660816192626953 9 -57.664764404296882
		 10 -61.427047729492188 11 -64.744209289550781 12 -67.40478515625 13 -69.256172180175781
		 14 -70.17376708984375 15 -70.246284484863281 16 -69.722625732421875 17 -68.683403015136719
		 18 -67.212844848632813 19 -65.396163940429687 20 -63.317317962646484 21 -61.057163238525391
		 22 -58.692207336425781 23 -56.293907165527344 24 -53.928497314453125 25 -51.657394409179687
		 26 -49.537956237792969 27 -47.62451171875 28 -45.969501495361328 29 -44.437252044677734
		 30 -42.881370544433594 31 -41.331790924072266 32 -39.817115783691406 33 -38.364810943603516
		 34 -37.001415252685547 35 -35.752941131591797 36 -34.674358367919922 37 -33.807880401611328
		 38 -33.160480499267578 39 -32.73968505859375 40 -32.553665161132813 41 -32.671226501464844
		 42 -33.007431030273438 43 -33.531780242919922 44 -34.212455749511719 45 -35.016403198242187
		 46 -35.909732818603516 47 -36.858024597167969 48 -37.826778411865234 49 -38.781787872314453
		 50 -39.689540863037109 51 -40.517478942871094 52 -41.273597717285156 53 -41.983844757080078
		 54 -42.643844604492188 55 -43.249309539794922 56 -43.795642852783203 57 -44.151466369628906
		 58 -44.207565307617188 59 -43.992252349853516 60 -43.534099578857422 61 -42.861988067626953
		 62 -42.005107879638672 63 -40.993049621582031 64 -39.855934143066406 65 -38.624458312988281
		 66 -37.329990386962891 67 -36.004642486572266 68 -34.681259155273438 69 -33.393440246582031
		 70 -32.175422668457031;
createNode animCurveTU -n "Character1_RightForeArm_visibility";
	rename -uid "8B4EDE4E-407B-57A8-065A-C293C19C16B9";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHand_translateX";
	rename -uid "859B70D7-4C65-5658-83FA-D3931FDE048C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7167695760726929;
createNode animCurveTL -n "Character1_RightHand_translateY";
	rename -uid "4BF2935B-4D33-A08C-5017-E4AD90EA93BD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.314067840576172;
createNode animCurveTL -n "Character1_RightHand_translateZ";
	rename -uid "08945F3B-4FB7-C154-11AD-08BD6E61C825";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4444055557250977;
createNode animCurveTU -n "Character1_RightHand_scaleX";
	rename -uid "3F8DB6F5-4BF4-FA9E-B35F-B7A84DF97467";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightHand_scaleY";
	rename -uid "5C9FAB16-4003-8583-FFDD-888FD82C39FF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightHand_scaleZ";
	rename -uid "CA0A156E-43D9-E43A-6229-E6B77986CCE7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightHand_rotateX";
	rename -uid "DCE69DC6-4628-0FE6-BE51-A99EF1179C5D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 5.1888155937194824 1 5.2718610763549805
		 2 4.9738626480102539 3 4.3576822280883789 4 3.4915871620178223 5 2.4509372711181641
		 6 1.3155626058578491 7 0.16345177590847015 8 -0.93729829788207997 9 -1.9357725381851199
		 10 -2.8023927211761475 11 -3.5284273624420166 12 -4.1200466156005859 13 -4.5600666999816895
		 14 -4.8095135688781738 15 -4.8853869438171387 16 -4.8319172859191895 17 -4.6559791564941406
		 18 -4.3619570732116699 19 -3.9554052352905278 20 -3.4457478523254395 21 -2.8480024337768555
		 22 -2.1835463047027588 23 -1.4800764322280884 24 -0.77092474699020386 25 -0.093971110880374908
		 26 0.50963199138641357 27 0.99667608737945568 28 1.3228132724761963 29 1.4675123691558838
		 30 1.4534380435943604 31 1.3013474941253662 32 1.0328658819198608 33 0.67040407657623291
		 34 0.23717346787452698 35 -0.24278113245964048 36 -0.73801630735397339 37 -1.2159069776535034
		 38 -1.6496996879577637 39 -2.012559175491333 40 -2.2778928279876709 41 -2.4939496517181396
		 42 -2.7269351482391357 43 -2.9775011539459229 44 -3.2462217807769775 45 -3.5335071086883545
		 46 -3.8395564556121831 47 -4.1643681526184082 48 -4.5077896118164062 49 -4.8696093559265137
		 50 -5.2496838569641113 51 -5.6480894088745117 52 -6.0664916038513184 53 -6.5011110305786133
		 54 -6.9440183639526367 55 -7.3870897293090811 56 -7.8219051361083984 57 -7.9358711242675781
		 58 -7.4921526908874503 59 -6.5976896286010742 60 -5.3593473434448242 61 -3.8810441493988037
		 62 -2.2619614601135254 63 -0.59602397680282593 64 1.0275424718856812 65 2.5230517387390137
		 66 3.8070309162139897 67 4.7971763610839844 68 5.4117264747619629 69 5.5692663192749023
		 70 5.1888155937194824;
createNode animCurveTA -n "Character1_RightHand_rotateY";
	rename -uid "A4AECD91-4ADA-45B0-7BE5-20AA618DABE8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 4.0617012977600098 1 4.0367932319641113
		 2 4.1248974800109863 3 4.3740239143371582 4 4.8394961357116699 5 5.5670590400695801
		 6 6.5793828964233398 7 7.8669910430908203 8 9.3844146728515625 9 11.051533699035645
		 10 12.759167671203613 11 14.377350807189943 12 15.764780044555662 13 16.806447982788086
		 14 17.407676696777344 15 17.58473014831543 16 17.452789306640625 17 17.060436248779297
		 18 16.462348937988281 19 15.715864181518555 20 14.877936363220217 21 14.002412796020508
		 22 13.1376953125 23 12.32490062713623 24 11.596709251403809 25 10.977030754089355
		 26 10.48165225982666 27 10.119909286499023 28 9.8973836898803711 29 9.7837085723876953
		 30 9.7342004776000977 31 9.7362098693847656 32 9.7807579040527344 33 9.860844612121582
		 34 9.9701156616210937 35 10.101890563964844 36 10.255171775817871 37 10.425609588623047
		 38 10.598958969116211 39 10.75869083404541 40 10.886835098266602 41 10.97425651550293
		 42 11.02769660949707 43 11.053215980529785 44 11.057294845581055 45 11.046570777893066
		 46 11.027603149414063 47 11.006644248962402 48 10.98944091796875 49 10.981053352355957
		 50 10.985699653625488 51 11.006622314453125 52 11.06162166595459 53 11.173463821411133
		 54 11.351608276367188 55 11.604841232299805 56 11.941215515136719 57 12.13902473449707
		 58 12.003667831420898 59 11.584379196166992 60 10.933334350585938 61 10.106629371643066
		 62 9.1638412475585937 63 8.1661386489868164 64 7.1735305786132812 65 6.2420234680175781
		 66 5.4215264320373535 67 4.7553038597106934 68 4.2814388275146484 69 4.0365676879882812
		 70 4.0617012977600098;
createNode animCurveTA -n "Character1_RightHand_rotateZ";
	rename -uid "AD7F6B1B-4508-FDED-4D09-729BAFE02063";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 29.221658706665039 1 30.363422393798828
		 2 32.283988952636719 3 34.833114624023438 4 37.856269836425781 5 41.194854736328125
		 6 44.688289642333984 7 48.177623748779297 8 51.509593963623047 9 54.539417266845703
		 10 57.131546020507813 11 59.158267974853509 12 60.497444152832038 13 61.154762268066413
		 14 61.181785583496094 15 60.628875732421868 16 59.624469757080085 17 58.233192443847656
		 18 56.516979217529297 19 54.535625457763672 20 52.347564697265625 21 50.010768890380859
		 22 47.583663940429688 23 45.125873565673828 24 42.698642730712891 25 40.364833831787109
		 26 38.188529968261719 27 36.234386444091797 28 34.566631317138672 29 33.216690063476562
		 30 32.154502868652344 31 31.350599288940426 32 30.775119781494141 33 30.39814567565918
		 34 30.190008163452152 35 30.121574401855469 36 30.192592620849609 37 30.379556655883789
		 38 30.619632720947262 39 30.850801467895511 40 31.01158332824707 41 31.092866897583011
		 42 31.126626968383793 43 31.112945556640625 44 31.051731109619144 45 30.9427604675293
		 46 30.785785675048825 47 30.580593109130859 48 30.32706260681152 49 30.025245666503906
		 50 29.675409317016605 51 29.278091430664062 52 28.894151687622074 53 28.599138259887695
		 54 28.416093826293945 55 28.368413925170898 56 28.479936599731445 57 28.698581695556637
		 58 28.946416854858402 59 29.196008682250977 60 29.412240982055664 61 29.562305450439453
		 62 29.623344421386722 63 29.587448120117188 64 29.464061737060547 65 29.280057907104496
		 66 29.077617645263672 67 28.910564422607425 68 28.839321136474613 69 28.924861907958988
		 70 29.221658706665039;
createNode animCurveTU -n "Character1_RightHand_visibility";
	rename -uid "00D24048-4734-B462-D940-9D90C0FA67A9";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb1_translateX";
	rename -uid "AB26E3AC-4526-02E4-45E9-EE8DF83504EA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5741705894470215;
createNode animCurveTL -n "Character1_RightHandThumb1_translateY";
	rename -uid "42A81D3B-4B35-272A-EAA4-2F81DCC9E163";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4327812194824219;
createNode animCurveTL -n "Character1_RightHandThumb1_translateZ";
	rename -uid "666734F5-42DE-9F9D-B3E3-71AC4B45A950";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8196568489074707;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleX";
	rename -uid "A39D1F92-4678-32D8-1FB6-D4AB7F0B9D2E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleY";
	rename -uid "ECDE7B89-4856-F4D0-FBFE-8DB1CC00F4FC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleZ";
	rename -uid "CC14452D-4699-736D-9EDE-C5B83B64B346";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateX";
	rename -uid "5AF3E578-4064-4EB2-A5C4-7B927A7EF214";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 71.393348693847656;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateY";
	rename -uid "0481AD1B-4E82-4D17-00E6-23B0C23A0B24";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 34.350833892822266;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateZ";
	rename -uid "91D50F7B-4E2D-0CAB-8BB3-8BB29A7DA27A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.7015657424926758;
createNode animCurveTU -n "Character1_RightHandThumb1_visibility";
	rename -uid "5B4DDB98-4593-90D2-C4B3-A8B521431C61";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb2_translateX";
	rename -uid "64DFD54B-4811-444A-EBA4-C194B0CDD213";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7422792911529541;
createNode animCurveTL -n "Character1_RightHandThumb2_translateY";
	rename -uid "60BEE593-4733-54AF-087C-919D1A986A76";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5767025947570801;
createNode animCurveTL -n "Character1_RightHandThumb2_translateZ";
	rename -uid "C0189404-401D-21D1-4AEA-22AF00E76EA7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0428388118743896;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleX";
	rename -uid "5E958FA4-4EDE-696D-58EB-B3A2AA19B53E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleY";
	rename -uid "F3D4CD6F-45B8-2138-88A4-DCA3094EA969";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleZ";
	rename -uid "1CBA7E48-4464-8437-9946-2F91D802672E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateX";
	rename -uid "1D40AA43-4898-A4CB-6956-7EB1F3F8E001";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.8401813507080074;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateY";
	rename -uid "8E189531-472F-76F0-84AD-77BABA840517";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.2117342948913574;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateZ";
	rename -uid "1A94EC19-4456-4161-5EB3-CB819CEF84FA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.4896585941314697;
createNode animCurveTU -n "Character1_RightHandThumb2_visibility";
	rename -uid "5CD24BD0-4F54-D374-D81D-EBA57415D7CB";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb3_translateX";
	rename -uid "C82FE21B-4E85-0155-ED23-6FB195B9480D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.588839054107666;
createNode animCurveTL -n "Character1_RightHandThumb3_translateY";
	rename -uid "99999B55-4BCB-E9DB-37E0-5DA959B410DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.13479287922382355;
createNode animCurveTL -n "Character1_RightHandThumb3_translateZ";
	rename -uid "44336D84-42B7-F277-FC70-A6986A617903";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.4058942794799805;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleX";
	rename -uid "93863745-4CF6-F12A-F015-AE920CC560D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleY";
	rename -uid "8362FD9B-4CB8-C440-C191-BBB048E010F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleZ";
	rename -uid "BF74F55F-4DC5-C0FD-02D6-B1BD2F7DEF01";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateX";
	rename -uid "B4317A9A-4579-5752-3F12-E5958A586AAA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.774157709633073e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateY";
	rename -uid "B6B3C684-405E-9095-F9D5-658C6AAA9902";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.4710329777662992e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateZ";
	rename -uid "D3E7E9E4-46B8-747F-3366-7FAC9C5515D4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8298723009356763e-007;
createNode animCurveTU -n "Character1_RightHandThumb3_visibility";
	rename -uid "6F40B41C-4942-C8AF-8E9B-6C8E28D698B1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex1_translateX";
	rename -uid "D7FCE811-4E0B-5C0D-7E72-71BD70624C49";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.25625163316726685;
createNode animCurveTL -n "Character1_RightHandIndex1_translateY";
	rename -uid "22057F22-4205-DB87-2E78-D7B673F63A00";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.892666816711426;
createNode animCurveTL -n "Character1_RightHandIndex1_translateZ";
	rename -uid "8C96126C-45D1-742F-D449-ACB877292B75";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.533165454864502;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleX";
	rename -uid "7EED31F5-441E-B19E-71E1-6D948BA54EA5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleY";
	rename -uid "D1D030F4-430F-F531-C65A-6ABA64257A6D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleZ";
	rename -uid "DBC75912-4C73-6D98-E8AA-7EB65B067127";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateX";
	rename -uid "19F173B0-48D2-8933-EEB6-1480C5D2C35D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.3082780838012695;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateY";
	rename -uid "D877A084-4B37-809A-5A43-34AE5643FE25";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.4730144739151001;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateZ";
	rename -uid "BF64A16C-482A-6D51-F2EA-F0AE7933AFA4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 28.820331573486328;
createNode animCurveTU -n "Character1_RightHandIndex1_visibility";
	rename -uid "684EBC13-4590-0D11-3DAF-E0A701B40DC2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex2_translateX";
	rename -uid "D1066240-4267-BCDE-827B-CB86747BBB73";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.3959341049194336;
createNode animCurveTL -n "Character1_RightHandIndex2_translateY";
	rename -uid "ACF70BE8-4B45-6572-6EE8-A0B3452F33A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8172028064727783;
createNode animCurveTL -n "Character1_RightHandIndex2_translateZ";
	rename -uid "37737305-4F8E-7DC0-38B8-2E92F2A6FFB2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.52152138948440552;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleX";
	rename -uid "4FADF9AF-470C-04E0-C96A-8D987098A1A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleY";
	rename -uid "70872348-49D9-CB87-8C77-6D8D0776C881";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleZ";
	rename -uid "C97E1E7D-412D-F41E-46A3-0183DC70D134";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateX";
	rename -uid "EBB28F2D-4857-4557-C720-7EBAEE0A3420";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 24.082914352416992;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateY";
	rename -uid "57F8E0B4-4202-35FF-2A88-0FAD3386382A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -56.004337310791016;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateZ";
	rename -uid "E1EC7BD2-41FB-600D-88E3-9289C87E93B9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 45.452117919921875;
createNode animCurveTU -n "Character1_RightHandIndex2_visibility";
	rename -uid "810545A4-4F62-4F19-98BB-548C95113C39";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex3_translateX";
	rename -uid "227C985B-473A-BB51-5472-DA9F2B68560D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6282989978790283;
createNode animCurveTL -n "Character1_RightHandIndex3_translateY";
	rename -uid "EC8B810D-424C-E460-1FC3-EEA77371144B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.2120883464813232;
createNode animCurveTL -n "Character1_RightHandIndex3_translateZ";
	rename -uid "0FC5DBEB-4E5E-BFCE-19D2-19826BCFD2DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7757487297058105;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleX";
	rename -uid "F134DFAF-4376-E9B2-3736-7C8032790333";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleY";
	rename -uid "5BB270C3-4A23-D8BD-5D85-B08130DDCF7A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleZ";
	rename -uid "8752B1E2-4223-E21A-F312-29A552A04539";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateX";
	rename -uid "D7DD5707-42EC-616F-C99F-26BB3803CAD9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.8973489558411529e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateY";
	rename -uid "CEBBB3BA-4C4C-906D-31B9-FD8338264E4E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.5108261663954181e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateZ";
	rename -uid "728F76BF-42AA-1E5C-BC35-F085865B718F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.638416276568023e-007;
createNode animCurveTU -n "Character1_RightHandIndex3_visibility";
	rename -uid "6EFF950B-4F52-EEAE-5002-65A6F06F71D0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing1_translateX";
	rename -uid "DD5DFFCD-4749-9A46-36F7-F3B91036D55C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8543868064880371;
createNode animCurveTL -n "Character1_RightHandRing1_translateY";
	rename -uid "16224F95-4AFB-6B9C-D735-FA92354BF731";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2156095504760742;
createNode animCurveTL -n "Character1_RightHandRing1_translateZ";
	rename -uid "A33FCE2B-4012-D0B0-EA04-68ACD95039DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2236912250518799;
createNode animCurveTU -n "Character1_RightHandRing1_scaleX";
	rename -uid "854DCAFA-4A5C-F5B4-5FCF-00AF32A9BB30";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandRing1_scaleY";
	rename -uid "B5FF783A-4966-C060-7EF3-A8974792B78D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing1_scaleZ";
	rename -uid "6B38241D-4065-E7C0-2B7D-D4A0C2C2D815";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandRing1_rotateX";
	rename -uid "E34CA9C2-4F57-E608-06AB-98B5709BCADD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5961043834686279;
createNode animCurveTA -n "Character1_RightHandRing1_rotateY";
	rename -uid "EA26EF0B-427B-0A4C-DD46-F8ACBE451B68";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 14.56866931915283;
createNode animCurveTA -n "Character1_RightHandRing1_rotateZ";
	rename -uid "1DF48432-48E3-3743-6783-DDB3E4D1D231";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 9.0529556274414062;
createNode animCurveTU -n "Character1_RightHandRing1_visibility";
	rename -uid "52DD7C48-432B-EA18-1C47-66AE9E0D6515";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing2_translateX";
	rename -uid "0F442BA6-4C15-6ABE-115D-E9BC2197F30F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.062777042388916;
createNode animCurveTL -n "Character1_RightHandRing2_translateY";
	rename -uid "A509A43D-4735-6AEA-476C-46A0E1F17913";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.4930424690246582;
createNode animCurveTL -n "Character1_RightHandRing2_translateZ";
	rename -uid "14B49492-45EE-79F2-B32A-18BF4E927DE0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.86127841472625732;
createNode animCurveTU -n "Character1_RightHandRing2_scaleX";
	rename -uid "3572BA34-4D67-AF43-C07A-4AB52376345B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandRing2_scaleY";
	rename -uid "A5A20EF3-4ED8-437E-E990-199A984C72C1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing2_scaleZ";
	rename -uid "D4589A4A-4F1A-1013-425E-8C8A9D4B44BD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandRing2_rotateX";
	rename -uid "302C6257-4F4D-63B4-0D65-E28F58EB7783";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.422389030456543;
createNode animCurveTA -n "Character1_RightHandRing2_rotateY";
	rename -uid "BEA48B56-4901-5318-2B90-D7B75EEC0271";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -80.43023681640625;
createNode animCurveTA -n "Character1_RightHandRing2_rotateZ";
	rename -uid "6CFCDF65-4BB7-4998-5042-B699AAF91DBD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 102.10539245605469;
createNode animCurveTU -n "Character1_RightHandRing2_visibility";
	rename -uid "840FA2C5-42D4-4866-B0E1-6F9F7CCAD245";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing3_translateX";
	rename -uid "7151E79C-4B25-C36A-5547-06AAA99F8505";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.5035178661346436;
createNode animCurveTL -n "Character1_RightHandRing3_translateY";
	rename -uid "09C972B9-4700-8D94-906E-3494F9369DD8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.9153993129730225;
createNode animCurveTL -n "Character1_RightHandRing3_translateZ";
	rename -uid "E8B90E66-44B0-14C7-EF74-21B309C57977";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.6898331642150879;
createNode animCurveTU -n "Character1_RightHandRing3_scaleX";
	rename -uid "CC5928D6-4BB1-6617-17E5-53A7EDE9F414";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing3_scaleY";
	rename -uid "FCD62E0D-4254-D069-6B0C-81BA9C33C88C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing3_scaleZ";
	rename -uid "00C445ED-4F65-C0A2-C6E4-889C40A7B789";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandRing3_rotateX";
	rename -uid "487DDA10-423D-2F9D-22C2-02A7C1998FEC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.1869089411884488e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateY";
	rename -uid "E1191B1D-415D-E570-7CBF-EBB68619C637";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1438712022027177e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateZ";
	rename -uid "6A5FE5AC-40F4-C836-8EF6-94B6222EAEC2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8735079265752574e-007;
createNode animCurveTU -n "Character1_RightHandRing3_visibility";
	rename -uid "AA231174-446F-79B8-D888-4EAABCA94DCE";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Neck_translateX";
	rename -uid "72B58525-4155-E7C0-B31F-6FADB8FD61C5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 12.791294097900391;
createNode animCurveTL -n "Character1_Neck_translateY";
	rename -uid "270D94AE-4D0C-1A8E-6BF8-C98FDDE0F0AB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.3580474853515625;
createNode animCurveTL -n "Character1_Neck_translateZ";
	rename -uid "2C025741-4B53-EAC0-D8D4-44B59062D51D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.6322288513183594;
createNode animCurveTU -n "Character1_Neck_scaleX";
	rename -uid "B07D6D48-4200-A5F9-5CDD-F7B95360CF57";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_Neck_scaleY";
	rename -uid "1A2D3582-4C8F-5F50-E34A-A4ACF34BC2D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Neck_scaleZ";
	rename -uid "3CCF01B6-4948-E801-C9CF-96B176F42ACD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Neck_rotateX";
	rename -uid "0F3753EF-42BD-3195-DA8D-9AABB45C4766";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 0.82382524013519287 1 0.69124352931976318
		 2 0.51854813098907471 3 0.31031963229179382 4 0.071163870394229889 5 -0.19431748986244202
		 6 -0.48154574632644653 7 -0.7859809398651123 8 -1.1031349897384644 9 -1.4285751581192017
		 10 -1.7579265832901001 11 -2.0868690013885498 12 -2.4111299514770508 13 -2.7264735698699951
		 14 -3.028688907623291 15 -3.3187010288238525 16 -3.5998284816741948 17 -3.8715541362762451
		 18 -4.1334710121154785 19 -4.3852787017822266 20 -4.62677001953125 21 -4.857816219329834
		 22 -5.078364372253418 23 -5.2884182929992676 24 -5.4880390167236328 25 -5.677330493927002
		 26 -5.8564352989196777 27 -6.0255289077758789 28 -6.1848130226135254 29 -6.3132529258728027
		 30 -6.3943867683410645 31 -6.4353713989257812 32 -6.4434432983398438 33 -6.425905704498291
		 34 -6.390113353729248 35 -6.34344482421875 36 -6.2932987213134766 37 -6.2470693588256836
		 38 -6.2121343612670898 39 -6.195836067199707 40 -6.2054653167724609 41 -6.1751956939697266
		 42 -6.0433263778686523 43 -5.8231654167175293 44 -5.5278983116149902 45 -5.1706323623657227
		 46 -4.7644290924072266 47 -4.3223443031311035 48 -3.8574566841125484 49 -3.3828849792480469
		 50 -2.9118187427520752 51 -2.4575252532958984 52 -2.0333631038665771 53 -1.6527875661849976
		 54 -1.329349160194397 55 -1.0766903162002563 56 -0.90853506326675404 57 -0.77301520109176636
		 58 -0.61357468366622925 59 -0.43672475218772888 60 -0.24876686930656433 61 -0.05591161921620369
		 62 0.13561689853668213 63 0.31949788331985474 64 0.48925462365150457 65 0.63821417093276978
		 66 0.75949138402938843 67 0.84599822759628296 68 0.89047724008560181 69 0.88555574417114258
		 70 0.82382524013519287;
createNode animCurveTA -n "Character1_Neck_rotateY";
	rename -uid "1E9FF8E0-44F7-7608-404F-FBBD7CA2CBC3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -5.6652488708496094 1 -5.631950855255127
		 2 -5.6758785247802734 3 -5.7891440391540527 4 -5.9638619422912598 5 -6.1921401023864746
		 6 -6.466062068939209 7 -6.7776789665222168 8 -7.1189994812011719 9 -7.4819860458374023
		 10 -7.8585467338562012 11 -8.2405357360839844 12 -8.6197500228881836 13 -8.9879302978515625
		 14 -9.3367652893066406 15 -9.6439676284790039 16 -9.8957080841064453 17 -10.096366882324219
		 18 -10.250373840332031 19 -10.362202644348145 20 -10.436371803283691 21 -10.477429389953613
		 22 -10.489955902099609 23 -10.478554725646973 24 -10.44785213470459 25 -10.402495384216309
		 26 -10.347146987915039 27 -10.286482810974121 28 -10.225191116333008 29 -10.128985404968262
		 30 -9.969085693359375 31 -9.7585010528564453 32 -9.5102729797363281 33 -9.2374649047851562
		 34 -8.9531469345092773 35 -8.6703739166259766 36 -8.402191162109375 37 -8.16162109375
		 38 -7.961662769317627 39 -7.8152999877929696 40 -7.7355027198791504 41 -7.719085693359375
		 42 -7.7503981590271005 43 -7.823582649230957 44 -7.93267822265625 45 -8.0717077255249023
		 46 -8.2347393035888672 47 -8.4159517288208008 48 -8.60968017578125 49 -8.8104391098022461
		 50 -9.0129537582397461 51 -9.2121601104736328 52 -9.4032001495361328 53 -9.5813999176025391
		 54 -9.7422304153442383 55 -9.8812532424926758 56 -9.9940519332885742 57 -10.019107818603516
		 58 -9.9115943908691406 59 -9.6920108795166016 60 -9.3807544708251953 61 -8.9981231689453125
		 62 -8.5643339157104492 63 -8.0995397567749023 64 -7.6238465309143075 65 -7.157325267791748
		 66 -6.7200345993041992 67 -6.3320450782775879 68 -6.0134587287902832 69 -5.7844409942626953
		 70 -5.6652488708496094;
createNode animCurveTA -n "Character1_Neck_rotateZ";
	rename -uid "856CC439-40E7-A516-7A65-CAB79528CEE5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -1.0567288398742676 1 -0.99977231025695801
		 2 -0.91520297527313232 3 -0.80191737413406372 4 -0.65882551670074463 5 -0.4848781526088714
		 6 -0.27908977866172791 7 -0.040558464825153351 8 0.23151907324790955 9 0.53783524036407471
		 10 0.87896323204040527 11 1.255351185798645 12 1.6673206090927124 13 2.1150660514831543
		 14 2.5986592769622803 15 3.0712811946868896 16 3.4888308048248291 17 3.8553736209869385
		 18 4.1749997138977051 19 4.4518275260925293 20 4.690009593963623 21 4.8937392234802246
		 22 5.0672488212585449 23 5.2148127555847168 24 5.3407449722290039 25 5.4493999481201172
		 26 5.5451679229736328 27 5.6324758529663086 28 5.715785026550293 29 5.7741093635559082
		 30 5.7866353988647461 31 5.7582559585571289 32 5.6940150260925293 33 5.5990490913391113
		 34 5.4785337448120117 35 5.337648868560791 36 5.1815519332885742 37 5.0153627395629883
		 38 4.8441629409790039 39 4.6730046272277832 40 4.5069293975830078 41 4.306556224822998
		 42 4.0364394187927246 43 3.7073988914489751 44 3.3301687240600586 45 2.9154891967773437
		 46 2.4741852283477783 47 2.0172293186187744 48 1.555781364440918 49 1.1012243032455444
		 50 0.66516858339309692 51 0.25945693254470825 52 -0.10385680943727493 53 -0.41253370046615601
		 54 -0.65419518947601318 55 -0.81637781858444214 56 -0.88660037517547607 57 -0.91020923852920543
		 58 -0.93918466567993153 59 -0.97128105163574219 60 -1.004533052444458 61 -1.0371909141540527
		 62 -1.0676654577255249 63 -1.094474196434021 64 -1.1161928176879883 65 -1.1314133405685425
		 66 -1.1387020349502563 67 -1.1365623474121094 68 -1.1233900785446167 69 -1.0974307060241699
		 70 -1.0567288398742676;
createNode animCurveTU -n "Character1_Neck_visibility";
	rename -uid "26A5A566-4CD2-37A5-0ABD-B9AAF4BC5C3B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Head_translateX";
	rename -uid "C94D52C0-43AE-C5E7-1383-0AB1E9ADAFE5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.627285957336426;
createNode animCurveTL -n "Character1_Head_translateY";
	rename -uid "D2AF7A6D-4D6D-F264-1DA3-B29E47E2E873";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.64882755279541;
createNode animCurveTL -n "Character1_Head_translateZ";
	rename -uid "D4342646-4609-8F99-C69A-90AE095D92E5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.1324095726013184;
createNode animCurveTU -n "Character1_Head_scaleX";
	rename -uid "E36F7603-448E-37B4-E409-FA90E0FFD78C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Head_scaleY";
	rename -uid "623ED3D9-49ED-1B50-787C-E6AD628F6331";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Head_scaleZ";
	rename -uid "340759EE-4524-9E29-AF4B-81A64DA73FD6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_Head_rotateX";
	rename -uid "1123AC68-4DA4-DD13-B01A-9A8E4CD24539";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 6.2913374900817871 1 6.4453763961791992
		 2 6.6068668365478516 3 6.7737751007080078 4 6.9440627098083496 5 7.1156949996948242
		 6 7.2866368293762198 7 7.4548530578613281 8 7.618309497833252 9 7.7749705314636239
		 10 7.9228034019470215 11 8.0597734451293945 12 8.1838464736938477 13 8.2929906845092773
		 14 8.3851737976074219 15 8.4644575119018555 16 8.5359516143798828 17 8.5991926193237305
		 18 8.6537141799926758 19 8.6990509033203125 20 8.734736442565918 21 8.7603054046630859
		 22 8.7752952575683594 23 8.77923583984375 24 8.771662712097168 25 8.7521114349365234
		 26 8.7201147079467773 27 8.675206184387207 28 8.6169195175170898 29 8.5208864212036133
		 30 8.3713979721069336 31 8.1809797286987305 32 7.962155818939209 33 7.727447509765625
		 34 7.4893779754638681 35 7.2604684829711905 36 7.0532407760620117 37 6.8802165985107422
		 38 6.7539200782775879 39 6.6868758201599121 40 6.6916112899780273 41 6.7677922248840332
		 42 6.900935173034668 43 7.0823431015014648 44 7.3033208847045898 45 7.5551700592041016
		 46 7.8291916847229004 47 8.1166849136352539 48 8.4089469909667969 49 8.6972723007202148
		 50 8.9729537963867187 51 9.2272806167602539 52 9.4515399932861328 53 9.6370172500610352
		 54 9.7749958038330078 55 9.8567609786987305 56 9.8735923767089844 57 9.8269081115722656
		 58 9.7183341979980469 59 9.5552949905395508 60 9.3452167510986328 61 9.0955305099487305
		 62 8.8136606216430664 63 8.5070343017578125 64 8.1830816268920898 65 7.8492293357849121
		 66 7.5129013061523446 67 7.1815242767333984 68 6.8625226020812988 69 6.5633187294006348
		 70 6.2913374900817871;
createNode animCurveTA -n "Character1_Head_rotateY";
	rename -uid "BADD5424-4B67-3932-A39F-F9BE99AABB00";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 3.5191104412078857 1 3.241765022277832
		 2 2.9595847129821777 3 2.6738874912261963 4 2.3859903812408447 5 2.0972120761871338
		 6 1.808870315551758 7 1.522283673286438 8 1.2387702465057373 9 0.95964866876602184
		 10 0.68623769283294678 11 0.41985583305358887 12 0.1618221253156662 13 -0.086544878780841827
		 14 -0.32392570376396179 15 -0.55439376831054688 16 -0.78225648403167725 17 -1.0065460205078125
		 18 -1.2262952327728271 19 -1.4405363798141479 20 -1.6483023166656494 21 -1.8486253023147581
		 22 -2.0405378341674805 23 -2.2230732440948486 24 -2.3952634334564209 25 -2.5561413764953613
		 26 -2.7047398090362549 27 -2.8400914669036865 28 -2.9612295627593994 29 -3.0771980285644531
		 30 -3.1942265033721924 31 -3.3071238994598389 32 -3.4107000827789307 33 -3.499763011932373
		 34 -3.5691204071044922 35 -3.6135787963867183 36 -3.6279418468475346 37 -3.6070163249969482
		 38 -3.5456044673919678 39 -3.4385106563568115 40 -3.2805395126342773 41 -3.0691215991973877
		 42 -2.8095107078552246 43 -2.5082492828369141 44 -2.1718792915344238 45 -1.8069424629211426
		 46 -1.4199810028076172 47 -1.0175354480743408 48 -0.60614657402038574 49 -0.19235318899154663
		 50 0.21730609238147736 51 0.61629432439804077 52 0.99807524681091309 53 1.3561135530471802
		 54 1.6838744878768921 55 1.9748225212097168 56 2.2224223613739014 57 2.4306240081787109
		 58 2.6046435832977295 59 2.7485804557800293 60 2.8665342330932617 61 2.9626030921936035
		 62 3.0408871173858643 63 3.1054859161376953 64 3.1605005264282227 65 3.2100338935852051
		 66 3.2581877708435059 67 3.3090674877166748 68 3.3667769432067871 69 3.435422420501709
		 70 3.5191104412078857;
createNode animCurveTA -n "Character1_Head_rotateZ";
	rename -uid "5FDD5611-483B-F9D2-1A16-9DB741DAD891";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 1.9091355800628662 1 2.2480216026306152
		 2 2.5940952301025391 3 2.9453849792480469 4 3.2999186515808105 5 3.6557247638702393
		 6 4.010831356048584 7 4.3632683753967285 8 4.711064338684082 9 5.0522489547729492
		 10 5.3848509788513184 11 5.7069005966186523 12 6.0164275169372559 13 6.3114638328552246
		 14 6.5900392532348633 15 6.8618812561035156 16 7.1359448432922363 17 7.4091072082519522
		 18 7.6782469749450675 19 7.9402408599853507 20 8.1919689178466797 21 8.4303121566772461
		 22 8.6521492004394531 23 8.8543615341186523 24 9.0338296890258789 25 9.1874380111694336
		 26 9.312067985534668 27 9.4046010971069336 28 9.4619216918945313 29 9.4727325439453125
		 30 9.432072639465332 31 9.3463401794433594 32 9.221928596496582 33 9.0652275085449219
		 34 8.8826189041137695 35 8.6804819107055664 36 8.4651784896850586 37 8.2430696487426758
		 38 8.0205059051513672 39 7.8038339614868173 40 7.5994000434875488 41 7.3971786499023429
		 42 7.1839947700500488 43 6.9614734649658203 44 6.7312407493591309 45 6.494926929473877
		 46 6.2541613578796387 47 6.0105729103088379 48 5.7657933235168457 49 5.5214505195617676
		 50 5.2791728973388672 51 5.0405921936035156 52 4.8073382377624512 53 4.5810441970825195
		 54 4.3633451461791992 55 4.1558837890625 56 3.9603066444396973 57 3.7762248516082764
		 58 3.6031582355499272 59 3.4398059844970703 60 3.2848680019378662 61 3.1370418071746826
		 62 2.9950211048126221 63 2.8574955463409424 64 2.7231483459472656 65 2.5906550884246826
		 66 2.4586846828460693 67 2.3258981704711914 68 2.1909482479095459 69 2.0524811744689941
		 70 1.9091355800628662;
createNode animCurveTU -n "Character1_Head_visibility";
	rename -uid "41F685C9-4FB6-58D3-5047-08BCCED2E758";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "eyes_translateX";
	rename -uid "75ADE54B-4E5C-495A-868F-47852AD01600";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 30.947149276733398;
createNode animCurveTL -n "eyes_translateY";
	rename -uid "4F0BA7A6-4FCB-7D18-8192-4A807B6C3370";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.0992542810490704e-007;
createNode animCurveTL -n "eyes_translateZ";
	rename -uid "BE31E232-4365-D31C-6E0A-AD888429045D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.121469657048256e-013;
createNode animCurveTU -n "eyes_scaleX";
	rename -uid "12F6B4C9-4A1D-F90F-E67E-6B8D1D502A30";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleY";
	rename -uid "5D0B30F8-4EC5-43B4-1E55-3B941C58E5BE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleZ";
	rename -uid "B4A02F2E-4883-C2B9-82E9-6AB8E9DC89E9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "eyes_rotateX";
	rename -uid "241679E0-4C68-A7C5-9139-1D8D911D48B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 0 1 0.060521338135004044 2 0.12550966441631317
		 3 0.19374671578407288 4 0.26401418447494507 5 0.33509385585784912 6 0.40576744079589844
		 7 0.47481665015220637 8 0.54102325439453125 9 0.60316896438598633 10 0.66003549098968506
		 11 0.71040457487106323 12 0.75305801630020142 13 0.78677737712860107 14 0.8103446364402771
		 15 0.82504934072494507 16 0.83281576633453369 17 0.83337724208831787 18 0.82646709680557251
		 19 0.81181865930557251 20 0.78916537761688232 21 0.75824040174484253 22 0.71877729892730713
		 23 0.67050933837890625 24 0.61316978931427002 25 0.54649209976196289 26 0.47020956873893738
		 27 0.36531150341033936 28 0.22091381251811981 29 0.048939738422632217 30 -0.13868743181228638
		 31 -0.33004447817802429 32 -0.51320803165435791 33 -0.6762549877166748 34 -0.80726200342178345
		 35 -0.8943057656288147 36 -0.94320231676101685 37 -0.9687829613685609 38 -0.97264724969863903
		 39 -0.95639485120773315 40 -0.92162549495697033 41 -0.86993867158889771 42 -0.80293411016464233
		 43 -0.72221148014068604 44 -0.62937027215957642 45 -0.51828217506408691 46 -0.38481134176254272
		 47 -0.23354668915271759 48 -0.069076977670192719 49 0.10400898009538651 50 0.2811223566532135
		 51 0.45767435431480408 52 0.62907612323760986 53 0.79073882102966309 54 0.93807387351989735
		 55 1.0664920806884766 56 1.1714049577713013 57 1.1807507276535034 58 1.1625765562057495
		 59 1.1201846599578857 60 1.0568777322769165 61 0.9759579300880431 62 0.88072776794433594
		 63 0.77448958158493042 64 0.66054576635360718 65 0.54219883680343628 66 0.42275106906890869
		 67 0.30550494790077209 68 0.19376273453235626 69 0.090826988220214844 70 0;
createNode animCurveTA -n "eyes_rotateY";
	rename -uid "F758170D-447A-8F3B-6F46-CBB988B5EE50";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 0 1 -0.11657311022281647 2 -0.24009247124195102
		 3 -0.36866366863250732 4 -0.50039219856262207 5 -0.63338375091552734 6 -0.76574373245239258
		 7 -0.89557790756225586 8 -1.0209918022155762 9 -1.1400907039642334 10 -1.2509806156158447
		 11 -1.3517667055130005 12 -1.4405547380447388 13 -1.5154504776000977 14 -1.5745590925216675
		 15 -1.6130601167678833 16 -1.6296015977859497 17 -1.6274924278259277 18 -1.6100410223007202
		 19 -1.5805567502975464 20 -1.5423480272293091 21 -1.4987237453460693 22 -1.4529927968978882
		 23 -1.408463716506958 24 -1.3684456348419189 25 -1.3362470865249634 26 -1.3151769638061523
		 27 -1.3007018566131592 28 -1.2866529226303101 29 -1.2738854885101318 30 -1.2632550001144409
		 31 -1.2556169033050537 32 -1.2518266439437866 33 -1.252739429473877 34 -1.2592109441757202
		 35 -1.2720965147018433 36 -1.27994704246521 37 -1.2761827707290649 38 -1.2689625024795532
		 39 -1.2664456367492676 40 -1.276790976524353 41 -1.3081575632095337 42 -1.3687045574188232
		 43 -1.4665910005569458 44 -1.6099759340286255 45 -1.8062374591827393 46 -2.0503323078155518
		 47 -2.331787109375 48 -2.6401293277740479 49 -2.9648852348327637 50 -3.2955822944641113
		 51 -3.6217467784881592 52 -3.9329061508178711 53 -4.2185869216918945 54 -4.4683165550231934
		 55 -4.6716213226318359 56 -4.8180279731750488 57 -4.7172279357910156 58 -4.5444331169128418
		 59 -4.308281421661377 60 -4.0174131393432617 61 -3.6804680824279781 62 -3.3060851097106934
		 63 -2.9029037952423096 64 -2.4795637130737305 65 -2.0447044372558594 66 -1.6069653034210205
		 67 -1.1749857664108276 68 -0.75740557909011841 69 -0.36286377906799316 70 0;
createNode animCurveTA -n "eyes_rotateZ";
	rename -uid "43C4E9D8-464A-6E96-AB6D-30BB4A3100F6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 0 1 -0.069832131266593933 2 -0.1421860009431839
		 3 -0.21637386083602905 4 -0.29170799255371094 5 -0.36750060319900513 6 -0.44306397438049316
		 7 -0.51771032810211182 8 -0.59075194597244263 9 -0.66150110960006714 10 -0.72926992177963257
		 11 -0.79337090253829956 12 -0.85311603546142578 13 -0.90781772136688244 14 -0.95678818225860585
		 15 -1.0046507120132446 16 -1.054970383644104 17 -1.1054719686508179 18 -1.1538801193237305
		 19 -1.1979199647903442 20 -1.2353161573410034 21 -1.2637934684753418 22 -1.2810766696929932
		 23 -1.2848905324935913 24 -1.2729600667953491 25 -1.2430100440979004 26 -1.1927649974822998
		 27 -1.1018526554107666 28 -0.96031183004379272 29 -0.78148388862609863 30 -0.57871067523956299
		 31 -0.36533373594284058 32 -0.15469469130039215 33 0.03986484557390213 34 0.20500318706035614
		 35 0.3273787796497345 36 0.3997933566570282 37 0.43198949098587036 38 0.43603658676147461
		 39 0.42400413751602173 40 0.40796166658401489 41 0.3999786376953125 42 0.41212451457977295
		 43 0.45646882057189941 44 0.54508095979690552 45 0.67792433500289917 46 0.84240227937698364
		 47 1.0317440032958984 48 1.2391787767410278 49 1.45793616771698 50 1.6812454462051392
		 51 1.9023357629776001 52 2.1144368648529053 53 2.3107776641845703 54 2.4845879077911377
		 55 2.6290967464447021 56 2.7375333309173584 57 2.6925201416015625 58 2.6029732227325439
		 59 2.4742362499237061 60 2.3116533756256104 61 2.1205685138702393 62 1.9063261747360227
		 63 1.6742702722549438 64 1.4297446012496948 65 1.1780936717987061 66 0.92466127872467041
		 67 0.67479169368743896 68 0.43382889032363892 69 0.20711687207221985 70 0;
createNode animCurveTU -n "eyes_visibility";
	rename -uid "CC7F64FD-4FE5-E076-1A23-79A5FB9E1B72";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "jaw_translateX";
	rename -uid "70327BD4-4EE3-5722-B875-3AACB4110D32";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.07279486209154129;
createNode animCurveTL -n "jaw_translateY";
	rename -uid "62D95185-4661-FF82-6765-F2BA229130E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.25670051574707;
createNode animCurveTL -n "jaw_translateZ";
	rename -uid "14C8E9B5-4BEA-6E31-7FDA-16B1F1A95E3C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.2238426557523781e-007;
createNode animCurveTU -n "jaw_scaleX";
	rename -uid "D41839D4-4803-2BD7-1FD3-2CAC639F4A0C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleY";
	rename -uid "97263072-4EA9-134B-2AFD-609084138E29";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleZ";
	rename -uid "A083E9BE-435C-1676-47DF-128E723F512E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "jaw_rotateX";
	rename -uid "DBE28F29-45F2-BAD7-0621-D0A96A29C740";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  2 -53.754219055175781 3 -53.749855041503906
		 4 -53.744010925292969 5 -53.737091064453125 6 -53.729331970214844 7 -53.720954895019531
		 8 -53.711776733398438 9 -53.702400207519531 10 -53.693077087402344 11 -53.684013366699219
		 12 -53.675117492675781 13 -53.397254943847656 14 -52.750907897949219 15 -51.991363525390625
		 16 -51.372726440429687 17 -51.147216796875 18 -51.396713256835938 19 -51.942672729492188
		 20 -52.646697998046875 21 -53.36920166015625 22 -53.969047546386719 23 -54.303512573242188
		 24 -54.430030822753906 25 -54.505035400390625 26 -54.534881591796875 27 -54.525909423828125
		 28 -54.484489440917969 29 -54.417694091796875 30 -54.333305358886719 31 -54.238014221191406
		 32 -54.138442993164063 33 -54.041229248046875 34 -53.952522277832031 35 -53.879234313964844
		 36 -53.810005187988281 37 -53.730094909667969 38 -53.64105224609375 39 -53.544921875
		 40 -53.4432373046875 41 -53.336410522460938 42 -53.225341796875 43 -53.112106323242188
		 44 -52.998794555664063 45 -52.887474060058594 46 -52.78021240234375 47 -52.678718566894531
		 48 -52.566162109375 49 -52.44586181640625 50 -52.330268859863281 51 -52.232994079589844
		 52 -52.166488647460937 53 -52.142356872558594 54 -52.153816223144531 55 -52.183753967285156
		 56 -52.229156494140625 57 -52.286613464355469 58 -52.353118896484375 59 -52.427314758300781
		 60 -52.50860595703125 61 -52.596923828125 62 -52.692062377929687 63 -52.800872802734375
		 64 -52.92718505859375 65 -53.065277099609375 66 -53.20947265625 67 -53.354507446289063
		 68 -53.496238708496094 69 -53.63153076171875 70 -53.758110046386719;
createNode animCurveTA -n "jaw_rotateY";
	rename -uid "7C7AEDBA-4D2B-CFB1-F130-CC99EF35E72C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 62 ".ktv[0:61]"  5 -124.89163208007812 6 -124.8899688720703
		 11 -124.88168334960937 12 -124.88029479980469 13 -124.9345703125 14 -125.06466674804687
		 15 -125.2178497314453 16 -125.34130859374999 17 -125.38163757324219 18 -125.32093811035156
		 19 -125.19720458984375 20 -125.04093933105469 21 -124.88230895996092 22 -124.75112915039062
		 23 -124.67681121826172 24 -124.64453125 25 -124.61952209472656 26 -124.60144805908202
		 27 -124.58997344970703 28 -124.58477020263672 29 -124.58546447753905 30 -124.59190368652344
		 31 -124.60395812988283 32 -124.62146759033203 33 -124.64427947998047 34 -124.67213439941405
		 35 -124.70492553710936 36 -124.75238800048828 37 -124.82095336914062 38 -124.90548706054687
		 39 -125.00096893310548 40 -125.10224914550783 41 -125.20346069335939 42 -125.29881286621094
		 43 -125.38330078125001 44 -125.45187377929688 45 -125.49952697753906 46 -125.52122497558595
		 47 -125.5118865966797 48 -125.50826263427733 49 -125.46730041503908 50 -125.39595031738283
		 51 -125.301513671875 52 -125.19100952148437 53 -125.07138824462891 54 -124.92979431152344
		 55 -124.75723266601562 56 -124.56687927246094 57 -124.37145996093749 58 -124.18316650390625
		 59 -124.01417541503906 60 -123.87641906738281 61 -123.78170013427733 62 -123.74173736572266
		 63 -123.76856994628906 64 -123.85702514648439 65 -123.99322509765626 66 -124.16323852539062
		 67 -124.35298156738283 68 -124.54818725585939 69 -124.73434448242187 70 -124.89665222167969;
createNode animCurveTA -n "jaw_rotateZ";
	rename -uid "65D66AF5-444F-197E-5D53-2483F3BFED7C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 60 ".ktv[0:59]"  11 -44.002166748046875 12 -44.0029296875
		 13 -44.036849975585938 14 -44.110366821289063 15 -44.182525634765625 16 -44.21331787109375
		 17 -44.164260864257813 18 -44.006500244140625 19 -43.765823364257813 20 -43.485763549804688
		 21 -43.210952758789063 22 -42.987548828125 23 -42.863037109375 24 -42.814971923828125
		 25 -42.7879638671875 26 -42.779983520507812 27 -42.788970947265625 28 -42.812789916992188
		 29 -42.849838256835938 30 -42.897689819335938 31 -42.9534912109375 32 -43.014450073242188
		 33 -43.0777587890625 34 -43.140945434570313 35 -43.201065063476563 36 -43.268569946289063
		 37 -43.352645874023438 38 -43.44879150390625 39 -43.552276611328125 40 -43.658660888671875
		 41 -43.765670776367188 42 -43.870819091796875 43 -43.96954345703125 44 -44.057327270507812
		 45 -44.129623413085937 46 -44.181915283203125 47 -44.209945678710938 48 -44.217086791992188
		 49 -44.159469604492188 50 -44.075363159179688 51 -44.002395629882812 52 -43.9791259765625
		 53 -44.044769287109375 54 -44.21063232421875 55 -44.446731567382813 56 -44.7298583984375
		 57 -45.036941528320312 58 -45.344528198242188 59 -45.627685546875 60 -45.860763549804688
		 61 -46.017578125 62 -46.072021484375 63 -46.00506591796875 64 -45.831939697265625
		 65 -45.578704833984375 66 -45.271408081054687 67 -44.93560791015625 68 -44.5958251953125
		 69 -44.275588989257813 70 -43.997589111328125;
createNode animCurveTU -n "jaw_visibility";
	rename -uid "E23B40BA-42CB-9C27-E5BD-CAB4CB7D51C4";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "FD81D68F-483B-A1FB-4E67-43B729B7C19D";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "89BF7031-4BAC-76B0-CEC4-34B95D6E60BC";
createNode displayLayer -n "defaultLayer";
	rename -uid "91F8C8E8-4DFE-E5E9-2114-53B483707C73";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "3F135D2D-48F2-ED3F-CB5E-098C24348189";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "775B89BC-4CAA-B6A5-30F7-038D2387B949";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "67DA9B10-489C-9179-167B-8AA6E5E309DD";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1636\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1636\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1287\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1287\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 15 100 -ps 2 85 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1287\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1287\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "6B2CAAB8-496C-91A6-F0BA-E8A4123ED423";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 70 -ast 0 -aet 250 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 70;
	setAttr ".unw" 70;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
connectAttr "ref_frame.s" "Character1_Hips.is";
connectAttr "Character1_Hips_scaleX.o" "Character1_Hips.sx";
connectAttr "Character1_Hips_scaleY.o" "Character1_Hips.sy";
connectAttr "Character1_Hips_scaleZ.o" "Character1_Hips.sz";
connectAttr "Character1_Hips_translateX.o" "Character1_Hips.tx";
connectAttr "Character1_Hips_translateY.o" "Character1_Hips.ty";
connectAttr "Character1_Hips_translateZ.o" "Character1_Hips.tz";
connectAttr "Character1_Hips_rotateX.o" "Character1_Hips.rx";
connectAttr "Character1_Hips_rotateY.o" "Character1_Hips.ry";
connectAttr "Character1_Hips_rotateZ.o" "Character1_Hips.rz";
connectAttr "Character1_Hips_visibility.o" "Character1_Hips.v";
connectAttr "Character1_Hips.s" "Character1_LeftUpLeg.is";
connectAttr "Character1_LeftUpLeg_scaleX.o" "Character1_LeftUpLeg.sx";
connectAttr "Character1_LeftUpLeg_scaleY.o" "Character1_LeftUpLeg.sy";
connectAttr "Character1_LeftUpLeg_scaleZ.o" "Character1_LeftUpLeg.sz";
connectAttr "Character1_LeftUpLeg_translateX.o" "Character1_LeftUpLeg.tx";
connectAttr "Character1_LeftUpLeg_translateY.o" "Character1_LeftUpLeg.ty";
connectAttr "Character1_LeftUpLeg_translateZ.o" "Character1_LeftUpLeg.tz";
connectAttr "Character1_LeftUpLeg_rotateX.o" "Character1_LeftUpLeg.rx";
connectAttr "Character1_LeftUpLeg_rotateY.o" "Character1_LeftUpLeg.ry";
connectAttr "Character1_LeftUpLeg_rotateZ.o" "Character1_LeftUpLeg.rz";
connectAttr "Character1_LeftUpLeg_visibility.o" "Character1_LeftUpLeg.v";
connectAttr "Character1_LeftUpLeg.s" "Character1_LeftLeg.is";
connectAttr "Character1_LeftLeg_scaleX.o" "Character1_LeftLeg.sx";
connectAttr "Character1_LeftLeg_scaleY.o" "Character1_LeftLeg.sy";
connectAttr "Character1_LeftLeg_scaleZ.o" "Character1_LeftLeg.sz";
connectAttr "Character1_LeftLeg_translateX.o" "Character1_LeftLeg.tx";
connectAttr "Character1_LeftLeg_translateY.o" "Character1_LeftLeg.ty";
connectAttr "Character1_LeftLeg_translateZ.o" "Character1_LeftLeg.tz";
connectAttr "Character1_LeftLeg_rotateX.o" "Character1_LeftLeg.rx";
connectAttr "Character1_LeftLeg_rotateY.o" "Character1_LeftLeg.ry";
connectAttr "Character1_LeftLeg_rotateZ.o" "Character1_LeftLeg.rz";
connectAttr "Character1_LeftLeg_visibility.o" "Character1_LeftLeg.v";
connectAttr "Character1_LeftLeg.s" "Character1_LeftFoot.is";
connectAttr "Character1_LeftFoot_scaleX.o" "Character1_LeftFoot.sx";
connectAttr "Character1_LeftFoot_scaleY.o" "Character1_LeftFoot.sy";
connectAttr "Character1_LeftFoot_scaleZ.o" "Character1_LeftFoot.sz";
connectAttr "Character1_LeftFoot_translateX.o" "Character1_LeftFoot.tx";
connectAttr "Character1_LeftFoot_translateY.o" "Character1_LeftFoot.ty";
connectAttr "Character1_LeftFoot_translateZ.o" "Character1_LeftFoot.tz";
connectAttr "Character1_LeftFoot_rotateX.o" "Character1_LeftFoot.rx";
connectAttr "Character1_LeftFoot_rotateY.o" "Character1_LeftFoot.ry";
connectAttr "Character1_LeftFoot_rotateZ.o" "Character1_LeftFoot.rz";
connectAttr "Character1_LeftFoot_visibility.o" "Character1_LeftFoot.v";
connectAttr "Character1_LeftFoot.s" "Character1_LeftToeBase.is";
connectAttr "Character1_LeftToeBase_scaleX.o" "Character1_LeftToeBase.sx";
connectAttr "Character1_LeftToeBase_scaleY.o" "Character1_LeftToeBase.sy";
connectAttr "Character1_LeftToeBase_scaleZ.o" "Character1_LeftToeBase.sz";
connectAttr "Character1_LeftToeBase_translateX.o" "Character1_LeftToeBase.tx";
connectAttr "Character1_LeftToeBase_translateY.o" "Character1_LeftToeBase.ty";
connectAttr "Character1_LeftToeBase_translateZ.o" "Character1_LeftToeBase.tz";
connectAttr "Character1_LeftToeBase_rotateX.o" "Character1_LeftToeBase.rx";
connectAttr "Character1_LeftToeBase_rotateY.o" "Character1_LeftToeBase.ry";
connectAttr "Character1_LeftToeBase_rotateZ.o" "Character1_LeftToeBase.rz";
connectAttr "Character1_LeftToeBase_visibility.o" "Character1_LeftToeBase.v";
connectAttr "Character1_LeftToeBase.s" "Character1_LeftFootMiddle1.is";
connectAttr "Character1_LeftFootMiddle1_scaleX.o" "Character1_LeftFootMiddle1.sx"
		;
connectAttr "Character1_LeftFootMiddle1_scaleY.o" "Character1_LeftFootMiddle1.sy"
		;
connectAttr "Character1_LeftFootMiddle1_scaleZ.o" "Character1_LeftFootMiddle1.sz"
		;
connectAttr "Character1_LeftFootMiddle1_translateX.o" "Character1_LeftFootMiddle1.tx"
		;
connectAttr "Character1_LeftFootMiddle1_translateY.o" "Character1_LeftFootMiddle1.ty"
		;
connectAttr "Character1_LeftFootMiddle1_translateZ.o" "Character1_LeftFootMiddle1.tz"
		;
connectAttr "Character1_LeftFootMiddle1_rotateX.o" "Character1_LeftFootMiddle1.rx"
		;
connectAttr "Character1_LeftFootMiddle1_rotateY.o" "Character1_LeftFootMiddle1.ry"
		;
connectAttr "Character1_LeftFootMiddle1_rotateZ.o" "Character1_LeftFootMiddle1.rz"
		;
connectAttr "Character1_LeftFootMiddle1_visibility.o" "Character1_LeftFootMiddle1.v"
		;
connectAttr "Character1_LeftFootMiddle1.s" "Character1_LeftFootMiddle2.is";
connectAttr "Character1_LeftFootMiddle2_translateX.o" "Character1_LeftFootMiddle2.tx"
		;
connectAttr "Character1_LeftFootMiddle2_translateY.o" "Character1_LeftFootMiddle2.ty"
		;
connectAttr "Character1_LeftFootMiddle2_translateZ.o" "Character1_LeftFootMiddle2.tz"
		;
connectAttr "Character1_LeftFootMiddle2_scaleX.o" "Character1_LeftFootMiddle2.sx"
		;
connectAttr "Character1_LeftFootMiddle2_scaleY.o" "Character1_LeftFootMiddle2.sy"
		;
connectAttr "Character1_LeftFootMiddle2_scaleZ.o" "Character1_LeftFootMiddle2.sz"
		;
connectAttr "Character1_LeftFootMiddle2_rotateX.o" "Character1_LeftFootMiddle2.rx"
		;
connectAttr "Character1_LeftFootMiddle2_rotateY.o" "Character1_LeftFootMiddle2.ry"
		;
connectAttr "Character1_LeftFootMiddle2_rotateZ.o" "Character1_LeftFootMiddle2.rz"
		;
connectAttr "Character1_LeftFootMiddle2_visibility.o" "Character1_LeftFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_RightUpLeg.is";
connectAttr "Character1_RightUpLeg_scaleX.o" "Character1_RightUpLeg.sx";
connectAttr "Character1_RightUpLeg_scaleY.o" "Character1_RightUpLeg.sy";
connectAttr "Character1_RightUpLeg_scaleZ.o" "Character1_RightUpLeg.sz";
connectAttr "Character1_RightUpLeg_translateX.o" "Character1_RightUpLeg.tx";
connectAttr "Character1_RightUpLeg_translateY.o" "Character1_RightUpLeg.ty";
connectAttr "Character1_RightUpLeg_translateZ.o" "Character1_RightUpLeg.tz";
connectAttr "Character1_RightUpLeg_rotateX.o" "Character1_RightUpLeg.rx";
connectAttr "Character1_RightUpLeg_rotateY.o" "Character1_RightUpLeg.ry";
connectAttr "Character1_RightUpLeg_rotateZ.o" "Character1_RightUpLeg.rz";
connectAttr "Character1_RightUpLeg_visibility.o" "Character1_RightUpLeg.v";
connectAttr "Character1_RightUpLeg.s" "Character1_RightLeg.is";
connectAttr "Character1_RightLeg_scaleX.o" "Character1_RightLeg.sx";
connectAttr "Character1_RightLeg_scaleY.o" "Character1_RightLeg.sy";
connectAttr "Character1_RightLeg_scaleZ.o" "Character1_RightLeg.sz";
connectAttr "Character1_RightLeg_translateX.o" "Character1_RightLeg.tx";
connectAttr "Character1_RightLeg_translateY.o" "Character1_RightLeg.ty";
connectAttr "Character1_RightLeg_translateZ.o" "Character1_RightLeg.tz";
connectAttr "Character1_RightLeg_rotateX.o" "Character1_RightLeg.rx";
connectAttr "Character1_RightLeg_rotateY.o" "Character1_RightLeg.ry";
connectAttr "Character1_RightLeg_rotateZ.o" "Character1_RightLeg.rz";
connectAttr "Character1_RightLeg_visibility.o" "Character1_RightLeg.v";
connectAttr "Character1_RightLeg.s" "Character1_RightFoot.is";
connectAttr "Character1_RightFoot_scaleX.o" "Character1_RightFoot.sx";
connectAttr "Character1_RightFoot_scaleY.o" "Character1_RightFoot.sy";
connectAttr "Character1_RightFoot_scaleZ.o" "Character1_RightFoot.sz";
connectAttr "Character1_RightFoot_translateX.o" "Character1_RightFoot.tx";
connectAttr "Character1_RightFoot_translateY.o" "Character1_RightFoot.ty";
connectAttr "Character1_RightFoot_translateZ.o" "Character1_RightFoot.tz";
connectAttr "Character1_RightFoot_rotateX.o" "Character1_RightFoot.rx";
connectAttr "Character1_RightFoot_rotateY.o" "Character1_RightFoot.ry";
connectAttr "Character1_RightFoot_rotateZ.o" "Character1_RightFoot.rz";
connectAttr "Character1_RightFoot_visibility.o" "Character1_RightFoot.v";
connectAttr "Character1_RightFoot.s" "Character1_RightToeBase.is";
connectAttr "Character1_RightToeBase_scaleX.o" "Character1_RightToeBase.sx";
connectAttr "Character1_RightToeBase_scaleY.o" "Character1_RightToeBase.sy";
connectAttr "Character1_RightToeBase_scaleZ.o" "Character1_RightToeBase.sz";
connectAttr "Character1_RightToeBase_translateX.o" "Character1_RightToeBase.tx";
connectAttr "Character1_RightToeBase_translateY.o" "Character1_RightToeBase.ty";
connectAttr "Character1_RightToeBase_translateZ.o" "Character1_RightToeBase.tz";
connectAttr "Character1_RightToeBase_rotateX.o" "Character1_RightToeBase.rx";
connectAttr "Character1_RightToeBase_rotateY.o" "Character1_RightToeBase.ry";
connectAttr "Character1_RightToeBase_rotateZ.o" "Character1_RightToeBase.rz";
connectAttr "Character1_RightToeBase_visibility.o" "Character1_RightToeBase.v";
connectAttr "Character1_RightToeBase.s" "Character1_RightFootMiddle1.is";
connectAttr "Character1_RightFootMiddle1_scaleX.o" "Character1_RightFootMiddle1.sx"
		;
connectAttr "Character1_RightFootMiddle1_scaleY.o" "Character1_RightFootMiddle1.sy"
		;
connectAttr "Character1_RightFootMiddle1_scaleZ.o" "Character1_RightFootMiddle1.sz"
		;
connectAttr "Character1_RightFootMiddle1_translateX.o" "Character1_RightFootMiddle1.tx"
		;
connectAttr "Character1_RightFootMiddle1_translateY.o" "Character1_RightFootMiddle1.ty"
		;
connectAttr "Character1_RightFootMiddle1_translateZ.o" "Character1_RightFootMiddle1.tz"
		;
connectAttr "Character1_RightFootMiddle1_rotateX.o" "Character1_RightFootMiddle1.rx"
		;
connectAttr "Character1_RightFootMiddle1_rotateY.o" "Character1_RightFootMiddle1.ry"
		;
connectAttr "Character1_RightFootMiddle1_rotateZ.o" "Character1_RightFootMiddle1.rz"
		;
connectAttr "Character1_RightFootMiddle1_visibility.o" "Character1_RightFootMiddle1.v"
		;
connectAttr "Character1_RightFootMiddle1.s" "Character1_RightFootMiddle2.is";
connectAttr "Character1_RightFootMiddle2_translateX.o" "Character1_RightFootMiddle2.tx"
		;
connectAttr "Character1_RightFootMiddle2_translateY.o" "Character1_RightFootMiddle2.ty"
		;
connectAttr "Character1_RightFootMiddle2_translateZ.o" "Character1_RightFootMiddle2.tz"
		;
connectAttr "Character1_RightFootMiddle2_scaleX.o" "Character1_RightFootMiddle2.sx"
		;
connectAttr "Character1_RightFootMiddle2_scaleY.o" "Character1_RightFootMiddle2.sy"
		;
connectAttr "Character1_RightFootMiddle2_scaleZ.o" "Character1_RightFootMiddle2.sz"
		;
connectAttr "Character1_RightFootMiddle2_rotateX.o" "Character1_RightFootMiddle2.rx"
		;
connectAttr "Character1_RightFootMiddle2_rotateY.o" "Character1_RightFootMiddle2.ry"
		;
connectAttr "Character1_RightFootMiddle2_rotateZ.o" "Character1_RightFootMiddle2.rz"
		;
connectAttr "Character1_RightFootMiddle2_visibility.o" "Character1_RightFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_Spine.is";
connectAttr "Character1_Spine_scaleX.o" "Character1_Spine.sx";
connectAttr "Character1_Spine_scaleY.o" "Character1_Spine.sy";
connectAttr "Character1_Spine_scaleZ.o" "Character1_Spine.sz";
connectAttr "Character1_Spine_translateX.o" "Character1_Spine.tx";
connectAttr "Character1_Spine_translateY.o" "Character1_Spine.ty";
connectAttr "Character1_Spine_translateZ.o" "Character1_Spine.tz";
connectAttr "Character1_Spine_rotateX.o" "Character1_Spine.rx";
connectAttr "Character1_Spine_rotateY.o" "Character1_Spine.ry";
connectAttr "Character1_Spine_rotateZ.o" "Character1_Spine.rz";
connectAttr "Character1_Spine_visibility.o" "Character1_Spine.v";
connectAttr "Character1_Spine.s" "Character1_Spine1.is";
connectAttr "Character1_Spine1_scaleX.o" "Character1_Spine1.sx";
connectAttr "Character1_Spine1_scaleY.o" "Character1_Spine1.sy";
connectAttr "Character1_Spine1_scaleZ.o" "Character1_Spine1.sz";
connectAttr "Character1_Spine1_translateX.o" "Character1_Spine1.tx";
connectAttr "Character1_Spine1_translateY.o" "Character1_Spine1.ty";
connectAttr "Character1_Spine1_translateZ.o" "Character1_Spine1.tz";
connectAttr "Character1_Spine1_rotateX.o" "Character1_Spine1.rx";
connectAttr "Character1_Spine1_rotateY.o" "Character1_Spine1.ry";
connectAttr "Character1_Spine1_rotateZ.o" "Character1_Spine1.rz";
connectAttr "Character1_Spine1_visibility.o" "Character1_Spine1.v";
connectAttr "Character1_Spine1.s" "Character1_LeftShoulder.is";
connectAttr "Character1_LeftShoulder_scaleX.o" "Character1_LeftShoulder.sx";
connectAttr "Character1_LeftShoulder_scaleY.o" "Character1_LeftShoulder.sy";
connectAttr "Character1_LeftShoulder_scaleZ.o" "Character1_LeftShoulder.sz";
connectAttr "Character1_LeftShoulder_translateX.o" "Character1_LeftShoulder.tx";
connectAttr "Character1_LeftShoulder_translateY.o" "Character1_LeftShoulder.ty";
connectAttr "Character1_LeftShoulder_translateZ.o" "Character1_LeftShoulder.tz";
connectAttr "Character1_LeftShoulder_rotateX.o" "Character1_LeftShoulder.rx";
connectAttr "Character1_LeftShoulder_rotateY.o" "Character1_LeftShoulder.ry";
connectAttr "Character1_LeftShoulder_rotateZ.o" "Character1_LeftShoulder.rz";
connectAttr "Character1_LeftShoulder_visibility.o" "Character1_LeftShoulder.v";
connectAttr "Character1_LeftShoulder.s" "Character1_LeftArm.is";
connectAttr "Character1_LeftArm_scaleX.o" "Character1_LeftArm.sx";
connectAttr "Character1_LeftArm_scaleY.o" "Character1_LeftArm.sy";
connectAttr "Character1_LeftArm_scaleZ.o" "Character1_LeftArm.sz";
connectAttr "Character1_LeftArm_translateX.o" "Character1_LeftArm.tx";
connectAttr "Character1_LeftArm_translateY.o" "Character1_LeftArm.ty";
connectAttr "Character1_LeftArm_translateZ.o" "Character1_LeftArm.tz";
connectAttr "Character1_LeftArm_rotateX.o" "Character1_LeftArm.rx";
connectAttr "Character1_LeftArm_rotateY.o" "Character1_LeftArm.ry";
connectAttr "Character1_LeftArm_rotateZ.o" "Character1_LeftArm.rz";
connectAttr "Character1_LeftArm_visibility.o" "Character1_LeftArm.v";
connectAttr "Character1_LeftArm.s" "Character1_LeftForeArm.is";
connectAttr "Character1_LeftForeArm_scaleX.o" "Character1_LeftForeArm.sx";
connectAttr "Character1_LeftForeArm_scaleY.o" "Character1_LeftForeArm.sy";
connectAttr "Character1_LeftForeArm_scaleZ.o" "Character1_LeftForeArm.sz";
connectAttr "Character1_LeftForeArm_translateX.o" "Character1_LeftForeArm.tx";
connectAttr "Character1_LeftForeArm_translateY.o" "Character1_LeftForeArm.ty";
connectAttr "Character1_LeftForeArm_translateZ.o" "Character1_LeftForeArm.tz";
connectAttr "Character1_LeftForeArm_rotateX.o" "Character1_LeftForeArm.rx";
connectAttr "Character1_LeftForeArm_rotateY.o" "Character1_LeftForeArm.ry";
connectAttr "Character1_LeftForeArm_rotateZ.o" "Character1_LeftForeArm.rz";
connectAttr "Character1_LeftForeArm_visibility.o" "Character1_LeftForeArm.v";
connectAttr "Character1_LeftForeArm.s" "Character1_LeftHand.is";
connectAttr "Character1_LeftHand_lockInfluenceWeights.o" "Character1_LeftHand.liw"
		;
connectAttr "Character1_LeftHand_scaleX.o" "Character1_LeftHand.sx";
connectAttr "Character1_LeftHand_scaleY.o" "Character1_LeftHand.sy";
connectAttr "Character1_LeftHand_scaleZ.o" "Character1_LeftHand.sz";
connectAttr "Character1_LeftHand_translateX.o" "Character1_LeftHand.tx";
connectAttr "Character1_LeftHand_translateY.o" "Character1_LeftHand.ty";
connectAttr "Character1_LeftHand_translateZ.o" "Character1_LeftHand.tz";
connectAttr "Character1_LeftHand_rotateX.o" "Character1_LeftHand.rx";
connectAttr "Character1_LeftHand_rotateY.o" "Character1_LeftHand.ry";
connectAttr "Character1_LeftHand_rotateZ.o" "Character1_LeftHand.rz";
connectAttr "Character1_LeftHand_visibility.o" "Character1_LeftHand.v";
connectAttr "Character1_LeftHand.s" "Character1_LeftHandThumb1.is";
connectAttr "Character1_LeftHandThumb1_scaleX.o" "Character1_LeftHandThumb1.sx";
connectAttr "Character1_LeftHandThumb1_scaleY.o" "Character1_LeftHandThumb1.sy";
connectAttr "Character1_LeftHandThumb1_scaleZ.o" "Character1_LeftHandThumb1.sz";
connectAttr "Character1_LeftHandThumb1_translateX.o" "Character1_LeftHandThumb1.tx"
		;
connectAttr "Character1_LeftHandThumb1_translateY.o" "Character1_LeftHandThumb1.ty"
		;
connectAttr "Character1_LeftHandThumb1_translateZ.o" "Character1_LeftHandThumb1.tz"
		;
connectAttr "Character1_LeftHandThumb1_rotateX.o" "Character1_LeftHandThumb1.rx"
		;
connectAttr "Character1_LeftHandThumb1_rotateY.o" "Character1_LeftHandThumb1.ry"
		;
connectAttr "Character1_LeftHandThumb1_rotateZ.o" "Character1_LeftHandThumb1.rz"
		;
connectAttr "Character1_LeftHandThumb1_visibility.o" "Character1_LeftHandThumb1.v"
		;
connectAttr "Character1_LeftHandThumb1.s" "Character1_LeftHandThumb2.is";
connectAttr "Character1_LeftHandThumb2_scaleX.o" "Character1_LeftHandThumb2.sx";
connectAttr "Character1_LeftHandThumb2_scaleY.o" "Character1_LeftHandThumb2.sy";
connectAttr "Character1_LeftHandThumb2_scaleZ.o" "Character1_LeftHandThumb2.sz";
connectAttr "Character1_LeftHandThumb2_translateX.o" "Character1_LeftHandThumb2.tx"
		;
connectAttr "Character1_LeftHandThumb2_translateY.o" "Character1_LeftHandThumb2.ty"
		;
connectAttr "Character1_LeftHandThumb2_translateZ.o" "Character1_LeftHandThumb2.tz"
		;
connectAttr "Character1_LeftHandThumb2_rotateX.o" "Character1_LeftHandThumb2.rx"
		;
connectAttr "Character1_LeftHandThumb2_rotateY.o" "Character1_LeftHandThumb2.ry"
		;
connectAttr "Character1_LeftHandThumb2_rotateZ.o" "Character1_LeftHandThumb2.rz"
		;
connectAttr "Character1_LeftHandThumb2_visibility.o" "Character1_LeftHandThumb2.v"
		;
connectAttr "Character1_LeftHandThumb2.s" "Character1_LeftHandThumb3.is";
connectAttr "Character1_LeftHandThumb3_translateX.o" "Character1_LeftHandThumb3.tx"
		;
connectAttr "Character1_LeftHandThumb3_translateY.o" "Character1_LeftHandThumb3.ty"
		;
connectAttr "Character1_LeftHandThumb3_translateZ.o" "Character1_LeftHandThumb3.tz"
		;
connectAttr "Character1_LeftHandThumb3_scaleX.o" "Character1_LeftHandThumb3.sx";
connectAttr "Character1_LeftHandThumb3_scaleY.o" "Character1_LeftHandThumb3.sy";
connectAttr "Character1_LeftHandThumb3_scaleZ.o" "Character1_LeftHandThumb3.sz";
connectAttr "Character1_LeftHandThumb3_rotateX.o" "Character1_LeftHandThumb3.rx"
		;
connectAttr "Character1_LeftHandThumb3_rotateY.o" "Character1_LeftHandThumb3.ry"
		;
connectAttr "Character1_LeftHandThumb3_rotateZ.o" "Character1_LeftHandThumb3.rz"
		;
connectAttr "Character1_LeftHandThumb3_visibility.o" "Character1_LeftHandThumb3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandIndex1.is";
connectAttr "Character1_LeftHandIndex1_scaleX.o" "Character1_LeftHandIndex1.sx";
connectAttr "Character1_LeftHandIndex1_scaleY.o" "Character1_LeftHandIndex1.sy";
connectAttr "Character1_LeftHandIndex1_scaleZ.o" "Character1_LeftHandIndex1.sz";
connectAttr "Character1_LeftHandIndex1_translateX.o" "Character1_LeftHandIndex1.tx"
		;
connectAttr "Character1_LeftHandIndex1_translateY.o" "Character1_LeftHandIndex1.ty"
		;
connectAttr "Character1_LeftHandIndex1_translateZ.o" "Character1_LeftHandIndex1.tz"
		;
connectAttr "Character1_LeftHandIndex1_rotateX.o" "Character1_LeftHandIndex1.rx"
		;
connectAttr "Character1_LeftHandIndex1_rotateY.o" "Character1_LeftHandIndex1.ry"
		;
connectAttr "Character1_LeftHandIndex1_rotateZ.o" "Character1_LeftHandIndex1.rz"
		;
connectAttr "Character1_LeftHandIndex1_visibility.o" "Character1_LeftHandIndex1.v"
		;
connectAttr "Character1_LeftHandIndex1.s" "Character1_LeftHandIndex2.is";
connectAttr "Character1_LeftHandIndex2_scaleX.o" "Character1_LeftHandIndex2.sx";
connectAttr "Character1_LeftHandIndex2_scaleY.o" "Character1_LeftHandIndex2.sy";
connectAttr "Character1_LeftHandIndex2_scaleZ.o" "Character1_LeftHandIndex2.sz";
connectAttr "Character1_LeftHandIndex2_translateX.o" "Character1_LeftHandIndex2.tx"
		;
connectAttr "Character1_LeftHandIndex2_translateY.o" "Character1_LeftHandIndex2.ty"
		;
connectAttr "Character1_LeftHandIndex2_translateZ.o" "Character1_LeftHandIndex2.tz"
		;
connectAttr "Character1_LeftHandIndex2_rotateX.o" "Character1_LeftHandIndex2.rx"
		;
connectAttr "Character1_LeftHandIndex2_rotateY.o" "Character1_LeftHandIndex2.ry"
		;
connectAttr "Character1_LeftHandIndex2_rotateZ.o" "Character1_LeftHandIndex2.rz"
		;
connectAttr "Character1_LeftHandIndex2_visibility.o" "Character1_LeftHandIndex2.v"
		;
connectAttr "Character1_LeftHandIndex2.s" "Character1_LeftHandIndex3.is";
connectAttr "Character1_LeftHandIndex3_translateX.o" "Character1_LeftHandIndex3.tx"
		;
connectAttr "Character1_LeftHandIndex3_translateY.o" "Character1_LeftHandIndex3.ty"
		;
connectAttr "Character1_LeftHandIndex3_translateZ.o" "Character1_LeftHandIndex3.tz"
		;
connectAttr "Character1_LeftHandIndex3_scaleX.o" "Character1_LeftHandIndex3.sx";
connectAttr "Character1_LeftHandIndex3_scaleY.o" "Character1_LeftHandIndex3.sy";
connectAttr "Character1_LeftHandIndex3_scaleZ.o" "Character1_LeftHandIndex3.sz";
connectAttr "Character1_LeftHandIndex3_rotateX.o" "Character1_LeftHandIndex3.rx"
		;
connectAttr "Character1_LeftHandIndex3_rotateY.o" "Character1_LeftHandIndex3.ry"
		;
connectAttr "Character1_LeftHandIndex3_rotateZ.o" "Character1_LeftHandIndex3.rz"
		;
connectAttr "Character1_LeftHandIndex3_visibility.o" "Character1_LeftHandIndex3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandRing1.is";
connectAttr "Character1_LeftHandRing1_scaleX.o" "Character1_LeftHandRing1.sx";
connectAttr "Character1_LeftHandRing1_scaleY.o" "Character1_LeftHandRing1.sy";
connectAttr "Character1_LeftHandRing1_scaleZ.o" "Character1_LeftHandRing1.sz";
connectAttr "Character1_LeftHandRing1_translateX.o" "Character1_LeftHandRing1.tx"
		;
connectAttr "Character1_LeftHandRing1_translateY.o" "Character1_LeftHandRing1.ty"
		;
connectAttr "Character1_LeftHandRing1_translateZ.o" "Character1_LeftHandRing1.tz"
		;
connectAttr "Character1_LeftHandRing1_rotateX.o" "Character1_LeftHandRing1.rx";
connectAttr "Character1_LeftHandRing1_rotateY.o" "Character1_LeftHandRing1.ry";
connectAttr "Character1_LeftHandRing1_rotateZ.o" "Character1_LeftHandRing1.rz";
connectAttr "Character1_LeftHandRing1_visibility.o" "Character1_LeftHandRing1.v"
		;
connectAttr "Character1_LeftHandRing1.s" "Character1_LeftHandRing2.is";
connectAttr "Character1_LeftHandRing2_scaleX.o" "Character1_LeftHandRing2.sx";
connectAttr "Character1_LeftHandRing2_scaleY.o" "Character1_LeftHandRing2.sy";
connectAttr "Character1_LeftHandRing2_scaleZ.o" "Character1_LeftHandRing2.sz";
connectAttr "Character1_LeftHandRing2_translateX.o" "Character1_LeftHandRing2.tx"
		;
connectAttr "Character1_LeftHandRing2_translateY.o" "Character1_LeftHandRing2.ty"
		;
connectAttr "Character1_LeftHandRing2_translateZ.o" "Character1_LeftHandRing2.tz"
		;
connectAttr "Character1_LeftHandRing2_rotateX.o" "Character1_LeftHandRing2.rx";
connectAttr "Character1_LeftHandRing2_rotateY.o" "Character1_LeftHandRing2.ry";
connectAttr "Character1_LeftHandRing2_rotateZ.o" "Character1_LeftHandRing2.rz";
connectAttr "Character1_LeftHandRing2_visibility.o" "Character1_LeftHandRing2.v"
		;
connectAttr "Character1_LeftHandRing2.s" "Character1_LeftHandRing3.is";
connectAttr "Character1_LeftHandRing3_translateX.o" "Character1_LeftHandRing3.tx"
		;
connectAttr "Character1_LeftHandRing3_translateY.o" "Character1_LeftHandRing3.ty"
		;
connectAttr "Character1_LeftHandRing3_translateZ.o" "Character1_LeftHandRing3.tz"
		;
connectAttr "Character1_LeftHandRing3_scaleX.o" "Character1_LeftHandRing3.sx";
connectAttr "Character1_LeftHandRing3_scaleY.o" "Character1_LeftHandRing3.sy";
connectAttr "Character1_LeftHandRing3_scaleZ.o" "Character1_LeftHandRing3.sz";
connectAttr "Character1_LeftHandRing3_rotateX.o" "Character1_LeftHandRing3.rx";
connectAttr "Character1_LeftHandRing3_rotateY.o" "Character1_LeftHandRing3.ry";
connectAttr "Character1_LeftHandRing3_rotateZ.o" "Character1_LeftHandRing3.rz";
connectAttr "Character1_LeftHandRing3_visibility.o" "Character1_LeftHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_RightShoulder.is";
connectAttr "Character1_RightShoulder_scaleX.o" "Character1_RightShoulder.sx";
connectAttr "Character1_RightShoulder_scaleY.o" "Character1_RightShoulder.sy";
connectAttr "Character1_RightShoulder_scaleZ.o" "Character1_RightShoulder.sz";
connectAttr "Character1_RightShoulder_translateX.o" "Character1_RightShoulder.tx"
		;
connectAttr "Character1_RightShoulder_translateY.o" "Character1_RightShoulder.ty"
		;
connectAttr "Character1_RightShoulder_translateZ.o" "Character1_RightShoulder.tz"
		;
connectAttr "Character1_RightShoulder_rotateX.o" "Character1_RightShoulder.rx";
connectAttr "Character1_RightShoulder_rotateY.o" "Character1_RightShoulder.ry";
connectAttr "Character1_RightShoulder_rotateZ.o" "Character1_RightShoulder.rz";
connectAttr "Character1_RightShoulder_visibility.o" "Character1_RightShoulder.v"
		;
connectAttr "Character1_RightShoulder.s" "Character1_RightArm.is";
connectAttr "Character1_RightArm_scaleX.o" "Character1_RightArm.sx";
connectAttr "Character1_RightArm_scaleY.o" "Character1_RightArm.sy";
connectAttr "Character1_RightArm_scaleZ.o" "Character1_RightArm.sz";
connectAttr "Character1_RightArm_translateX.o" "Character1_RightArm.tx";
connectAttr "Character1_RightArm_translateY.o" "Character1_RightArm.ty";
connectAttr "Character1_RightArm_translateZ.o" "Character1_RightArm.tz";
connectAttr "Character1_RightArm_rotateX.o" "Character1_RightArm.rx";
connectAttr "Character1_RightArm_rotateY.o" "Character1_RightArm.ry";
connectAttr "Character1_RightArm_rotateZ.o" "Character1_RightArm.rz";
connectAttr "Character1_RightArm_visibility.o" "Character1_RightArm.v";
connectAttr "Character1_RightArm.s" "Character1_RightForeArm.is";
connectAttr "Character1_RightForeArm_scaleX.o" "Character1_RightForeArm.sx";
connectAttr "Character1_RightForeArm_scaleY.o" "Character1_RightForeArm.sy";
connectAttr "Character1_RightForeArm_scaleZ.o" "Character1_RightForeArm.sz";
connectAttr "Character1_RightForeArm_translateX.o" "Character1_RightForeArm.tx";
connectAttr "Character1_RightForeArm_translateY.o" "Character1_RightForeArm.ty";
connectAttr "Character1_RightForeArm_translateZ.o" "Character1_RightForeArm.tz";
connectAttr "Character1_RightForeArm_rotateX.o" "Character1_RightForeArm.rx";
connectAttr "Character1_RightForeArm_rotateY.o" "Character1_RightForeArm.ry";
connectAttr "Character1_RightForeArm_rotateZ.o" "Character1_RightForeArm.rz";
connectAttr "Character1_RightForeArm_visibility.o" "Character1_RightForeArm.v";
connectAttr "Character1_RightForeArm.s" "Character1_RightHand.is";
connectAttr "Character1_RightHand_scaleX.o" "Character1_RightHand.sx";
connectAttr "Character1_RightHand_scaleY.o" "Character1_RightHand.sy";
connectAttr "Character1_RightHand_scaleZ.o" "Character1_RightHand.sz";
connectAttr "Character1_RightHand_translateX.o" "Character1_RightHand.tx";
connectAttr "Character1_RightHand_translateY.o" "Character1_RightHand.ty";
connectAttr "Character1_RightHand_translateZ.o" "Character1_RightHand.tz";
connectAttr "Character1_RightHand_rotateX.o" "Character1_RightHand.rx";
connectAttr "Character1_RightHand_rotateY.o" "Character1_RightHand.ry";
connectAttr "Character1_RightHand_rotateZ.o" "Character1_RightHand.rz";
connectAttr "Character1_RightHand_visibility.o" "Character1_RightHand.v";
connectAttr "Character1_RightHand.s" "Character1_RightHandThumb1.is";
connectAttr "Character1_RightHandThumb1_scaleX.o" "Character1_RightHandThumb1.sx"
		;
connectAttr "Character1_RightHandThumb1_scaleY.o" "Character1_RightHandThumb1.sy"
		;
connectAttr "Character1_RightHandThumb1_scaleZ.o" "Character1_RightHandThumb1.sz"
		;
connectAttr "Character1_RightHandThumb1_translateX.o" "Character1_RightHandThumb1.tx"
		;
connectAttr "Character1_RightHandThumb1_translateY.o" "Character1_RightHandThumb1.ty"
		;
connectAttr "Character1_RightHandThumb1_translateZ.o" "Character1_RightHandThumb1.tz"
		;
connectAttr "Character1_RightHandThumb1_rotateX.o" "Character1_RightHandThumb1.rx"
		;
connectAttr "Character1_RightHandThumb1_rotateY.o" "Character1_RightHandThumb1.ry"
		;
connectAttr "Character1_RightHandThumb1_rotateZ.o" "Character1_RightHandThumb1.rz"
		;
connectAttr "Character1_RightHandThumb1_visibility.o" "Character1_RightHandThumb1.v"
		;
connectAttr "Character1_RightHandThumb1.s" "Character1_RightHandThumb2.is";
connectAttr "Character1_RightHandThumb2_scaleX.o" "Character1_RightHandThumb2.sx"
		;
connectAttr "Character1_RightHandThumb2_scaleY.o" "Character1_RightHandThumb2.sy"
		;
connectAttr "Character1_RightHandThumb2_scaleZ.o" "Character1_RightHandThumb2.sz"
		;
connectAttr "Character1_RightHandThumb2_translateX.o" "Character1_RightHandThumb2.tx"
		;
connectAttr "Character1_RightHandThumb2_translateY.o" "Character1_RightHandThumb2.ty"
		;
connectAttr "Character1_RightHandThumb2_translateZ.o" "Character1_RightHandThumb2.tz"
		;
connectAttr "Character1_RightHandThumb2_rotateX.o" "Character1_RightHandThumb2.rx"
		;
connectAttr "Character1_RightHandThumb2_rotateY.o" "Character1_RightHandThumb2.ry"
		;
connectAttr "Character1_RightHandThumb2_rotateZ.o" "Character1_RightHandThumb2.rz"
		;
connectAttr "Character1_RightHandThumb2_visibility.o" "Character1_RightHandThumb2.v"
		;
connectAttr "Character1_RightHandThumb2.s" "Character1_RightHandThumb3.is";
connectAttr "Character1_RightHandThumb3_translateX.o" "Character1_RightHandThumb3.tx"
		;
connectAttr "Character1_RightHandThumb3_translateY.o" "Character1_RightHandThumb3.ty"
		;
connectAttr "Character1_RightHandThumb3_translateZ.o" "Character1_RightHandThumb3.tz"
		;
connectAttr "Character1_RightHandThumb3_scaleX.o" "Character1_RightHandThumb3.sx"
		;
connectAttr "Character1_RightHandThumb3_scaleY.o" "Character1_RightHandThumb3.sy"
		;
connectAttr "Character1_RightHandThumb3_scaleZ.o" "Character1_RightHandThumb3.sz"
		;
connectAttr "Character1_RightHandThumb3_rotateX.o" "Character1_RightHandThumb3.rx"
		;
connectAttr "Character1_RightHandThumb3_rotateY.o" "Character1_RightHandThumb3.ry"
		;
connectAttr "Character1_RightHandThumb3_rotateZ.o" "Character1_RightHandThumb3.rz"
		;
connectAttr "Character1_RightHandThumb3_visibility.o" "Character1_RightHandThumb3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandIndex1.is";
connectAttr "Character1_RightHandIndex1_scaleX.o" "Character1_RightHandIndex1.sx"
		;
connectAttr "Character1_RightHandIndex1_scaleY.o" "Character1_RightHandIndex1.sy"
		;
connectAttr "Character1_RightHandIndex1_scaleZ.o" "Character1_RightHandIndex1.sz"
		;
connectAttr "Character1_RightHandIndex1_translateX.o" "Character1_RightHandIndex1.tx"
		;
connectAttr "Character1_RightHandIndex1_translateY.o" "Character1_RightHandIndex1.ty"
		;
connectAttr "Character1_RightHandIndex1_translateZ.o" "Character1_RightHandIndex1.tz"
		;
connectAttr "Character1_RightHandIndex1_rotateX.o" "Character1_RightHandIndex1.rx"
		;
connectAttr "Character1_RightHandIndex1_rotateY.o" "Character1_RightHandIndex1.ry"
		;
connectAttr "Character1_RightHandIndex1_rotateZ.o" "Character1_RightHandIndex1.rz"
		;
connectAttr "Character1_RightHandIndex1_visibility.o" "Character1_RightHandIndex1.v"
		;
connectAttr "Character1_RightHandIndex1.s" "Character1_RightHandIndex2.is";
connectAttr "Character1_RightHandIndex2_scaleX.o" "Character1_RightHandIndex2.sx"
		;
connectAttr "Character1_RightHandIndex2_scaleY.o" "Character1_RightHandIndex2.sy"
		;
connectAttr "Character1_RightHandIndex2_scaleZ.o" "Character1_RightHandIndex2.sz"
		;
connectAttr "Character1_RightHandIndex2_translateX.o" "Character1_RightHandIndex2.tx"
		;
connectAttr "Character1_RightHandIndex2_translateY.o" "Character1_RightHandIndex2.ty"
		;
connectAttr "Character1_RightHandIndex2_translateZ.o" "Character1_RightHandIndex2.tz"
		;
connectAttr "Character1_RightHandIndex2_rotateX.o" "Character1_RightHandIndex2.rx"
		;
connectAttr "Character1_RightHandIndex2_rotateY.o" "Character1_RightHandIndex2.ry"
		;
connectAttr "Character1_RightHandIndex2_rotateZ.o" "Character1_RightHandIndex2.rz"
		;
connectAttr "Character1_RightHandIndex2_visibility.o" "Character1_RightHandIndex2.v"
		;
connectAttr "Character1_RightHandIndex2.s" "Character1_RightHandIndex3.is";
connectAttr "Character1_RightHandIndex3_translateX.o" "Character1_RightHandIndex3.tx"
		;
connectAttr "Character1_RightHandIndex3_translateY.o" "Character1_RightHandIndex3.ty"
		;
connectAttr "Character1_RightHandIndex3_translateZ.o" "Character1_RightHandIndex3.tz"
		;
connectAttr "Character1_RightHandIndex3_scaleX.o" "Character1_RightHandIndex3.sx"
		;
connectAttr "Character1_RightHandIndex3_scaleY.o" "Character1_RightHandIndex3.sy"
		;
connectAttr "Character1_RightHandIndex3_scaleZ.o" "Character1_RightHandIndex3.sz"
		;
connectAttr "Character1_RightHandIndex3_rotateX.o" "Character1_RightHandIndex3.rx"
		;
connectAttr "Character1_RightHandIndex3_rotateY.o" "Character1_RightHandIndex3.ry"
		;
connectAttr "Character1_RightHandIndex3_rotateZ.o" "Character1_RightHandIndex3.rz"
		;
connectAttr "Character1_RightHandIndex3_visibility.o" "Character1_RightHandIndex3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandRing1.is";
connectAttr "Character1_RightHandRing1_scaleX.o" "Character1_RightHandRing1.sx";
connectAttr "Character1_RightHandRing1_scaleY.o" "Character1_RightHandRing1.sy";
connectAttr "Character1_RightHandRing1_scaleZ.o" "Character1_RightHandRing1.sz";
connectAttr "Character1_RightHandRing1_translateX.o" "Character1_RightHandRing1.tx"
		;
connectAttr "Character1_RightHandRing1_translateY.o" "Character1_RightHandRing1.ty"
		;
connectAttr "Character1_RightHandRing1_translateZ.o" "Character1_RightHandRing1.tz"
		;
connectAttr "Character1_RightHandRing1_rotateX.o" "Character1_RightHandRing1.rx"
		;
connectAttr "Character1_RightHandRing1_rotateY.o" "Character1_RightHandRing1.ry"
		;
connectAttr "Character1_RightHandRing1_rotateZ.o" "Character1_RightHandRing1.rz"
		;
connectAttr "Character1_RightHandRing1_visibility.o" "Character1_RightHandRing1.v"
		;
connectAttr "Character1_RightHandRing1.s" "Character1_RightHandRing2.is";
connectAttr "Character1_RightHandRing2_scaleX.o" "Character1_RightHandRing2.sx";
connectAttr "Character1_RightHandRing2_scaleY.o" "Character1_RightHandRing2.sy";
connectAttr "Character1_RightHandRing2_scaleZ.o" "Character1_RightHandRing2.sz";
connectAttr "Character1_RightHandRing2_translateX.o" "Character1_RightHandRing2.tx"
		;
connectAttr "Character1_RightHandRing2_translateY.o" "Character1_RightHandRing2.ty"
		;
connectAttr "Character1_RightHandRing2_translateZ.o" "Character1_RightHandRing2.tz"
		;
connectAttr "Character1_RightHandRing2_rotateX.o" "Character1_RightHandRing2.rx"
		;
connectAttr "Character1_RightHandRing2_rotateY.o" "Character1_RightHandRing2.ry"
		;
connectAttr "Character1_RightHandRing2_rotateZ.o" "Character1_RightHandRing2.rz"
		;
connectAttr "Character1_RightHandRing2_visibility.o" "Character1_RightHandRing2.v"
		;
connectAttr "Character1_RightHandRing2.s" "Character1_RightHandRing3.is";
connectAttr "Character1_RightHandRing3_translateX.o" "Character1_RightHandRing3.tx"
		;
connectAttr "Character1_RightHandRing3_translateY.o" "Character1_RightHandRing3.ty"
		;
connectAttr "Character1_RightHandRing3_translateZ.o" "Character1_RightHandRing3.tz"
		;
connectAttr "Character1_RightHandRing3_scaleX.o" "Character1_RightHandRing3.sx";
connectAttr "Character1_RightHandRing3_scaleY.o" "Character1_RightHandRing3.sy";
connectAttr "Character1_RightHandRing3_scaleZ.o" "Character1_RightHandRing3.sz";
connectAttr "Character1_RightHandRing3_rotateX.o" "Character1_RightHandRing3.rx"
		;
connectAttr "Character1_RightHandRing3_rotateY.o" "Character1_RightHandRing3.ry"
		;
connectAttr "Character1_RightHandRing3_rotateZ.o" "Character1_RightHandRing3.rz"
		;
connectAttr "Character1_RightHandRing3_visibility.o" "Character1_RightHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_Neck.is";
connectAttr "Character1_Neck_scaleX.o" "Character1_Neck.sx";
connectAttr "Character1_Neck_scaleY.o" "Character1_Neck.sy";
connectAttr "Character1_Neck_scaleZ.o" "Character1_Neck.sz";
connectAttr "Character1_Neck_translateX.o" "Character1_Neck.tx";
connectAttr "Character1_Neck_translateY.o" "Character1_Neck.ty";
connectAttr "Character1_Neck_translateZ.o" "Character1_Neck.tz";
connectAttr "Character1_Neck_rotateX.o" "Character1_Neck.rx";
connectAttr "Character1_Neck_rotateY.o" "Character1_Neck.ry";
connectAttr "Character1_Neck_rotateZ.o" "Character1_Neck.rz";
connectAttr "Character1_Neck_visibility.o" "Character1_Neck.v";
connectAttr "Character1_Neck.s" "Character1_Head.is";
connectAttr "Character1_Head_scaleX.o" "Character1_Head.sx";
connectAttr "Character1_Head_scaleY.o" "Character1_Head.sy";
connectAttr "Character1_Head_scaleZ.o" "Character1_Head.sz";
connectAttr "Character1_Head_translateX.o" "Character1_Head.tx";
connectAttr "Character1_Head_translateY.o" "Character1_Head.ty";
connectAttr "Character1_Head_translateZ.o" "Character1_Head.tz";
connectAttr "Character1_Head_rotateX.o" "Character1_Head.rx";
connectAttr "Character1_Head_rotateY.o" "Character1_Head.ry";
connectAttr "Character1_Head_rotateZ.o" "Character1_Head.rz";
connectAttr "Character1_Head_visibility.o" "Character1_Head.v";
connectAttr "Character1_Head.s" "eyes.is";
connectAttr "eyes_translateX.o" "eyes.tx";
connectAttr "eyes_translateY.o" "eyes.ty";
connectAttr "eyes_translateZ.o" "eyes.tz";
connectAttr "eyes_scaleX.o" "eyes.sx";
connectAttr "eyes_scaleY.o" "eyes.sy";
connectAttr "eyes_scaleZ.o" "eyes.sz";
connectAttr "eyes_rotateX.o" "eyes.rx";
connectAttr "eyes_rotateY.o" "eyes.ry";
connectAttr "eyes_rotateZ.o" "eyes.rz";
connectAttr "eyes_visibility.o" "eyes.v";
connectAttr "Character1_Head.s" "jaw.is";
connectAttr "jaw_lockInfluenceWeights.o" "jaw.liw";
connectAttr "jaw_translateX.o" "jaw.tx";
connectAttr "jaw_translateY.o" "jaw.ty";
connectAttr "jaw_translateZ.o" "jaw.tz";
connectAttr "jaw_scaleX.o" "jaw.sx";
connectAttr "jaw_scaleY.o" "jaw.sy";
connectAttr "jaw_scaleZ.o" "jaw.sz";
connectAttr "jaw_rotateX.o" "jaw.rx";
connectAttr "jaw_rotateY.o" "jaw.ry";
connectAttr "jaw_rotateZ.o" "jaw.rz";
connectAttr "jaw_visibility.o" "jaw.v";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Emeny@Archer_Idle.ma
