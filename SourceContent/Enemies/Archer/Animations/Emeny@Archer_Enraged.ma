//Maya ASCII 2016 scene
//Name: Emeny@Archer_Enraged.ma
//Last modified: Tue, Nov 24, 2015 12:09:49 PM
//Codeset: 1252
requires maya "2016";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "A6562B70-4DF8-9B69-0F2F-BDA53D34EA20";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 267.55744588251287 253.13114140055779 246.3617168947319 ;
	setAttr ".r" -type "double3" -27.938352729602379 44.999999999999972 -5.172681101354183e-014 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "083EB216-466B-4D30-C347-C5B863EB6C66";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 439.64008364130609;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "7BECA998-4BEA-005B-C158-35A35D53E194";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "A92749E1-4279-2EF0-B0DB-CBB4CB9FD26C";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "6C256F80-4AD2-A115-537B-9BAA250C3AA9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "928A460E-482A-6800-BCCD-73B526BF3C01";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "5BEB0F27-4EEE-EB91-21DD-31B2FB20ED12";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "0F7211D5-4AB7-1979-861A-C6817759B1BE";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "ref_frame";
	rename -uid "8BE59EA0-4A82-598F-CA17-B0BBE8B3A2CB";
	addAttr -ci true -sn "refFrame" -ln "refFrame" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "Character1_Hips" -p "ref_frame";
	rename -uid "7A5461FA-431C-839B-635B-E5862DCADAFC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -19.036789801921167 ;
	setAttr ".ssc" no;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftUpLeg" -p "Character1_Hips";
	rename -uid "18DD7982-403A-BC11-7D4C-F2A9C60C4822";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 90.000000000000014 -12.849604412773132 43.257528613448208 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftLeg" -p "Character1_LeftUpLeg";
	rename -uid "DA47B61C-42C8-700B-0FFF-DFA84C61BD6B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -19.152297252867996 -13.416343813706657 -33.623675304480329 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFoot" -p "Character1_LeftLeg";
	rename -uid "3CA95301-4F7C-7E55-2882-8CA59B5049E1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 25.817647449536953 51.040150860538553 -112.05775014674906 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftToeBase" -p "Character1_LeftFoot";
	rename -uid "75E24842-48A4-31C4-B563-699501418CC7";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 43.727116820142825 -29.659481123592514 -4.6723850284209263 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle1" -p "Character1_LeftToeBase";
	rename -uid "A5E83855-4CAB-D64D-927A-6A9A22A3ED6B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 165.43734692409092 -20.35908719617909 10.197796850486212 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle2" -p "Character1_LeftFootMiddle1";
	rename -uid "9B4389B3-4CB9-B589-7C75-648C53450F77";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 127.90699782306312 -44.626258206212555 -101.65660999147298 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightUpLeg" -p "Character1_Hips";
	rename -uid "8FC775B9-4F22-C6C7-2472-1C9A095256A2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 133.25752861344816 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightLeg" -p "Character1_RightUpLeg";
	rename -uid "0E39DCC5-4FA0-5637-44B8-2A9DEACBBE18";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -39.772585840344547 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFoot" -p "Character1_RightLeg";
	rename -uid "FC4ABD34-4380-8852-6CBF-0B9B1B50B862";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -53.712356932759 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightToeBase" -p "Character1_RightFoot";
	rename -uid "878DE9BB-4F79-0791-7C75-A2AA892C70D3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 2.9245623946004821e-006 53.712356932758929 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle1" -p "Character1_RightToeBase";
	rename -uid "FC17B603-4EA8-AF14-1BAB-6793925CBD3C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -4.5380835579220506e-006 -2.7209092876563258e-005 
		39.772585840347155 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle2" -p "Character1_RightFootMiddle1";
	rename -uid "48E3F20F-4861-4BEA-8E24-7E9D85DCD971";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -2.8539668357113417e-005 0.00011485155389716205 -133.25752861351003 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine" -p "Character1_Hips";
	rename -uid "B8C84D92-4E04-EC26-2B02-608DB24A1E7C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 133.25752861344816 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine1" -p "Character1_Spine";
	rename -uid "507DB0F7-4300-49CD-D8C0-329699799062";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -34.34645244194413 -26.596597123422644 16.99576897690589 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftShoulder" -p "Character1_Spine1";
	rename -uid "EC90761B-46AB-F75D-35EE-AC81BAF91088";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -166.39102833412767 21.832119962557094 -120.00622813437471 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftArm" -p "Character1_LeftShoulder";
	rename -uid "CB537DCE-493A-51DC-ACC9-EE9F5F5A4434";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -172.11387269090633 -44.88241940154802 -37.940590556196071 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftForeArm" -p "Character1_LeftArm";
	rename -uid "67BACAB8-476F-1E34-F3C3-7E8E32467AB5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -84.022423618581243 -14.673598567163568 71.813326221989513 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHand" -p "Character1_LeftForeArm";
	rename -uid "0B0D55D0-47B9-2B49-F676-D9B9E09911CB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -24.404340271578391 -34.932697344706668 122.26498801320882 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr -k on ".liw" no;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb1" -p "Character1_LeftHand";
	rename -uid "3266D417-47DB-A756-FF17-34AB57130E1B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.1043965330995 61.333484361561069 1.8740740862626108 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb2" -p "Character1_LeftHandThumb1";
	rename -uid "81A69928-4123-7708-5BDC-55953C4A687D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -103.44292663098258 20.93424016453444 169.56041330531272 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb3" -p "Character1_LeftHandThumb2";
	rename -uid "4C769CE6-4AC9-AA20-184A-3C95BA05F91E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 14.174484134960574 -59.672502756712539 -19.020394805426264 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex1" -p "Character1_LeftHand";
	rename -uid "97600E7B-4F0E-16E8-8A08-218BC4480A8D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -150.59621970507803 75.681392634287164 -124.28723884791442 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex2" -p "Character1_LeftHandIndex1";
	rename -uid "1EAFC953-4EC3-24CE-52BF-07813F8615B8";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -41.093804604175162 -57.602285575728224 -69.242456746392662 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex3" -p "Character1_LeftHandIndex2";
	rename -uid "CBF64AB7-481E-EA37-EC6D-398258AAF0F8";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.108297963098213 8.7979835742630801 27.088028901385748 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing1" -p "Character1_LeftHand";
	rename -uid "FE1814DA-4559-42B5-A540-10A774FC6C1D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -141.42437885137375 63.919742192600388 -113.78475086453638 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing2" -p "Character1_LeftHandRing1";
	rename -uid "051C35FB-43C8-6AD9-3538-60AB7AEF8376";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -35.368646977348 -76.384823071466059 -48.755963146359043 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing3" -p "Character1_LeftHandRing2";
	rename -uid "5436AAF1-4A68-1851-F7A8-5690A884B5C7";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 9.6312888734270121 39.523687300631337 15.3258182753707 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightShoulder" -p "Character1_Spine1";
	rename -uid "BC24649E-4112-2E75-4C76-28A6F75BE9B8";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -164.54352138244408 34.957040538466451 -116.14754006477595 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightArm" -p "Character1_RightShoulder";
	rename -uid "9769827F-4C21-53D6-3C4F-92B09749E80A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -173.75456654457099 -11.164646449014084 -45.537881339438307 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightForeArm" -p "Character1_RightArm";
	rename -uid "93BBA3A8-4C0E-B8AF-7A93-F4A7F291628C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 22.753833922449758 50.186429787105524 -101.53518723839815 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHand" -p "Character1_RightForeArm";
	rename -uid "E8531530-4EEA-08DE-BF80-68B9CCC91B44";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 48.334026046900341 -3.7701083025157853 -29.882905994214536 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb1" -p "Character1_RightHand";
	rename -uid "0DAC11A0-44A7-E270-5C16-AD89254C12AA";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb2" -p "Character1_RightHandThumb1";
	rename -uid "DC92FCB5-4A76-6BDC-75C6-158B6ACCE619";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb3" -p "Character1_RightHandThumb2";
	rename -uid "1D40EE1E-49C8-D0D3-A7EC-F0B46425A14B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex1" -p "Character1_RightHand";
	rename -uid "A9A394E5-4DEC-E331-5D2C-B8B92CEFEEC8";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex2" -p "Character1_RightHandIndex1";
	rename -uid "228F419A-4210-760D-06F3-05AC25FA1E5F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex3" -p "Character1_RightHandIndex2";
	rename -uid "932403C2-417C-B1A0-9B47-EC886EAABBF1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing1" -p "Character1_RightHand";
	rename -uid "1022829B-4A96-BF8D-25CF-3A838D98F9DE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing2" -p "Character1_RightHandRing1";
	rename -uid "46B97FC8-4C01-AF6D-C193-F5876AAED673";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing3" -p "Character1_RightHandRing2";
	rename -uid "7AA5DA4E-43BA-3483-893B-BE8B19C04835";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Neck" -p "Character1_Spine1";
	rename -uid "3C26E4FB-4011-A5CB-72C3-B689EB8A570E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 48.229963067601958 30.670012388143238 129.35864775594831 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Head" -p "Character1_Neck";
	rename -uid "6082FBE4-46DD-A59D-73E7-C09EF36AC47C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -14.36476107543206 22.377462763663633 -109.58762475494268 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "eyes" -p "Character1_Head";
	rename -uid "96D53CE3-4E7D-AE04-FAAF-2CACC2C662FF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -157.21319586594478 -21.144487689504771 -3.4677530572789341 ;
	setAttr ".radi" 4.5;
	setAttr ".fbxID" 5;
createNode joint -n "jaw" -p "Character1_Head";
	rename -uid "15AE3158-485C-3DC6-E209-66A7730FA342";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -157.21319586594478 -21.144487689504771 -3.4677530572789341 ;
	setAttr ".radi" 0.5;
	setAttr -k on ".liw" no;
	setAttr ".fbxID" 5;
createNode animCurveTU -n "Character1_LeftHand_lockInfluenceWeights";
	rename -uid "6769A524-435B-DC19-BD9F-EEB6EFC968EA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "jaw_lockInfluenceWeights";
	rename -uid "771099EE-451A-909B-5210-C98DA0CCE977";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "Character1_Hips_translateX";
	rename -uid "A9FCD9C8-40B2-1572-A42C-889B0183C598";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -0.50418257713317871 1 1.2349998950958252
		 2 3.2003352642059326 3 5.0899772644042969 4 6.6353111267089844 5 7.6559891700744629
		 6 8.0705575942993164 7 8.100010871887207 8 7.9995832443237305 9 7.8603453636169434
		 10 7.8056888580322266 11 9.3616466522216797 12 9.6453533172607422 13 9.6580524444580078
		 14 8.3277683258056641 15 8.0853090286254883 16 7.8193826675415039 17 7.6685552597045898
		 18 6.9278640747070312 19 5.2151336669921875 20 3.1136214733123779 21 1.2906941175460815
		 22 -0.40653803944587708 23 -2.2771658897399902 24 -3.8265202045440674 25 -4.5607905387878418
		 26 -4.6775403022766113 27 -4.7201657295227051 28 -4.7138156890869141 29 -4.6833257675170898
		 30 -4.6465554237365723 31 -4.5677080154418945 32 -4.4158267974853516 33 -4.2332687377929687
		 34 -4.0691757202148437 35 -3.9683670997619629 36 -3.9499015808105469 37 -3.9807782173156738
		 38 -4.0267901420593262 39 -4.0617074966430664 40 -4.0546197891235352 41 -3.9785482883453369
		 42 -3.8471841812133789 43 -3.6906144618988037 44 -3.5372509956359863 45 -3.4154810905456543
		 46 -3.3323874473571777 47 -3.2696025371551514 48 -3.213078498840332 49 -3.1542813777923584
		 50 -3.0792105197906494 51 -2.9947097301483154 52 -2.9118137359619141 53 -2.8309218883514404
		 54 -2.7549777030944824 55 -2.6862201690673828 56 -2.4132034778594971 57 -1.8633068799972534
		 58 -1.2399584054946899 59 -0.74128615856170654 60 -0.56372010707855225 61 -0.61661458015441895
		 62 -0.65760612487792969 63 -0.66059541702270508 64 -0.59907782077789307 65 -0.44689300656318665
		 66 -0.21523399651050568 67 0.10421819239854813 68 0.40837898850440979 69 0.71239417791366577
		 70 1.0118883848190308 71 1.1052999496459961 72 1.0578281879425049 73 1.0177915096282959
		 74 0.81856304407119751 75 0.50069636106491089 76 0.23625379800796509 77 0.033929020166397095
		 78 -0.14709824323654175 79 -0.32163482904434204 80 -0.50418257713317871;
createNode animCurveTL -n "Character1_Hips_translateY";
	rename -uid "E24CAB10-4680-C24B-E126-5C871E59F4AF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 42.001785278320312 1 44.530868530273438
		 2 47.274227142333984 3 50.006435394287109 4 52.53759765625 5 54.707324981689453 6 56.337360382080078
		 7 57.297801971435547 8 57.748867034912109 9 57.862335205078125 10 57.843673706054688
		 11 57.259799957275391 12 51.635917663574219 13 42.306121826171875 14 30.160247802734375
		 15 26.928598403930664 16 25.796693801879883 17 25.712257385253906 18 28.055095672607422
		 19 33.233348846435547 20 38.757461547851563 21 42.018753051757812 22 43.111495971679688
		 23 43.942775726318359 24 44.482658386230469 25 44.691982269287109 26 44.684345245361328
		 27 44.627483367919922 28 44.552539825439453 29 44.490264892578125 30 44.457302093505859
		 31 44.4764404296875 32 44.522682189941406 33 44.575069427490234 34 44.626583099365234
		 35 44.662796020507813 36 44.685802459716797 37 44.706707000732422 38 44.718875885009766
		 39 44.717750549316406 40 44.713497161865234 41 44.659904479980469 42 44.533100128173828
		 43 44.378467559814453 44 44.228290557861328 45 44.110092163085938 46 44.023769378662109
		 47 43.945392608642578 48 43.876758575439453 49 43.827106475830078 50 43.794303894042969
		 51 43.779621124267578 52 43.775463104248047 53 43.773674011230469 54 43.768909454345703
		 55 43.755046844482422 56 43.702732086181641 57 43.599216461181641 58 43.454017639160156
		 59 43.273731231689453 60 43.068912506103516 61 42.557872772216797 62 41.547733306884766
		 63 40.186790466308594 64 38.621768951416016 65 36.997383117675781 66 38.267463684082031
		 67 42.953865051269531 68 47.437686920166016 69 49.304176330566406 70 49.625846862792969
		 71 49.385181427001953 72 48.493335723876953 73 46.331092834472656 74 43.272232055664063
		 75 40.315109252929687 76 38.549221038818359 77 38.419746398925781 78 39.390560150146484
		 79 40.804897308349609 80 42.001785278320312;
createNode animCurveTL -n "Character1_Hips_translateZ";
	rename -uid "39D8C717-44D5-F62B-BB7A-F4A9FE26B418";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -9.3845930099487305 1 -10.847734451293945
		 2 -12.597826957702637 3 -14.369062423706055 4 -15.950101852416992 5 -17.13554573059082
		 6 -17.6279296875 7 -17.599540710449219 8 -17.599748611450195 9 -17.732927322387695
		 10 -18.055809020996094 11 -20.320871353149414 12 -23.23253059387207 13 -26.1678466796875
		 14 -29.146167755126953 15 -30.56654167175293 16 -31.669427871704102 17 -32.432926177978516
		 18 -32.913822174072266 19 -33.178501129150391 20 -33.237335205078125 21 -33.052791595458984
		 22 -32.413928985595703 23 -31.403787612915039 24 -30.453855514526367 25 -30.029291152954102
		 26 -30.069229125976563 27 -30.162214279174805 28 -30.273273468017578 29 -30.368129730224609
		 30 -30.409473419189453 31 -30.381235122680664 32 -30.307554244995117 33 -30.223100662231445
		 34 -30.167507171630859 35 -30.177900314331055 36 -30.297046661376953 37 -30.493478775024414
		 38 -30.694000244140625 39 -30.842552185058594 40 -30.864664077758789 41 -30.701095581054688
		 42 -30.396087646484375 43 -30.018777847290039 44 -29.644237518310547 45 -29.351583480834961
		 46 -29.138481140136719 47 -28.956575393676758 48 -28.799127578735352 49 -28.667312622070312
		 50 -28.56144905090332 51 -28.463777542114258 52 -28.379268646240234 53 -28.350545883178711
		 54 -28.422468185424805 55 -28.638969421386719 56 -29.005702972412109 57 -29.481536865234375
		 58 -30.038129806518555 59 -30.64851188659668 60 -31.287384033203125 61 -31.99359130859375
		 62 -32.782684326171875 63 -33.603382110595703 64 -34.405948638916016 65 -35.143230438232422
		 66 -34.064678192138672 67 -30.887365341186523 68 -27.898136138916016 69 -26.252004623413086
		 70 -24.745090484619141 71 -22.769372940063477 72 -20.050930023193359 73 -17.374856948852539
		 74 -14.581727981567383 75 -12.059171676635742 76 -10.487075805664062 77 -9.75439453125
		 78 -9.4026041030883789 79 -9.3166475296020508 80 -9.3845930099487305;
createNode animCurveTU -n "Character1_Hips_scaleX";
	rename -uid "CDC62AF0-406F-69FA-04AF-96A81981EA73";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_Hips_scaleY";
	rename -uid "9616635F-4518-C259-2C4F-009A0D8B2CFB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_Hips_scaleZ";
	rename -uid "2D1A8AA8-4143-10C8-6273-DDBE309457EE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Hips_rotateX";
	rename -uid "A24BEC24-42FA-92B4-0F5D-68A710F32E5A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -6.9407939910888672 1 -0.10966869443655014
		 2 7.9130859375000009 3 16.820806503295898 4 25.819450378417969 5 33.644943237304688
		 6 38.904281616210938 7 41.669425964355469 8 43.704631805419922 9 45.503738403320313
		 10 47.526844024658203 11 49.010089874267578 12 50.583045959472656 13 51.528118133544922
		 14 50.47369384765625 15 47.636150360107422 16 41.409461975097656 17 31.452001571655273
		 18 23.320259094238281 19 19.220109939575195 20 18.467672348022461 21 19.359071731567383
		 22 20.796270370483398 23 22.857275009155273 24 24.52699089050293 25 25.046449661254883
		 26 23.970363616943359 27 21.8822021484375 28 19.432987213134766 29 17.344028472900391
		 30 16.483896255493164 31 17.098243713378906 32 18.535429000854492 33 20.280969619750977
		 34 21.739810943603516 35 22.297285079956055 36 21.949766159057617 37 21.217369079589844
		 38 20.26513671875 39 19.2406005859375 40 18.315427780151367 41 17.843252182006836
		 42 17.867132186889648 43 18.157207489013672 44 18.476394653320312 45 18.571018218994141
		 46 18.390302658081055 47 18.030948638916016 48 17.501035690307617 49 16.863792419433594
		 50 16.32762336730957 51 16.082939147949219 52 16.114492416381836 53 16.422704696655273
		 54 17.005683898925781 55 17.858213424682617 56 19.761493682861328 57 22.981550216674805
		 58 26.803478240966797 59 30.615619659423825 60 33.913070678710938 61 36.580284118652344
		 62 38.618385314941406 63 39.942878723144531 64 40.473167419433594 65 40.110813140869141
		 66 38.918491363525391 67 36.776180267333984 68 34.177028656005859 69 31.111431121826175
		 70 27.857637405395508 71 24.520769119262695 72 21.31397819519043 73 18.180206298828125
		 74 14.757917404174806 75 11.186473846435547 76 7.6638832092285147 77 4.1060676574707031
		 78 0.46785727143287664 79 -3.2236397266387939 80 -6.9407939910888672;
createNode animCurveTA -n "Character1_Hips_rotateY";
	rename -uid "9AA5332C-4994-51A5-C0F1-EC92B2DCA023";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 11.395454406738281 1 5.0785531997680664
		 2 -1.7408543825149536 3 -7.8470821380615234 4 -12.157749176025391 5 -14.061071395874023
		 6 -13.466073036193848 7 -12.28610897064209 8 -12.301910400390625 9 -13.058259010314941
		 10 -14.019794464111328 11 -20.055789947509766 12 -29.556718826293945 13 -39.597923278808594
		 14 -49.761943817138672 15 -58.814018249511719 16 -66.848464965820313 17 -71.917793273925781
		 18 -73.786598205566406 19 -73.522209167480469 20 -72.061515808105469 21 -70.187538146972656
		 22 -67.501533508300781 23 -63.553829193115234 24 -59.592758178710937 25 -56.878299713134766
		 26 -55.845989227294922 27 -55.770156860351563 28 -56.190185546875 29 -56.6881103515625
		 30 -56.871498107910156 31 -56.995548248291016 32 -57.370708465576165 33 -57.720554351806634
		 34 -57.807010650634766 35 -57.401405334472649 36 -56.139003753662109 37 -54.138820648193359
		 38 -51.921466827392578 39 -50.041389465332031 40 -49.011520385742187 41 -48.760456085205078
		 42 -48.909046173095703 43 -49.432788848876953 44 -50.323833465576172 45 -51.577770233154297
		 46 -53.722465515136719 47 -56.776798248291016 48 -59.926952362060547 49 -62.384113311767578
		 50 -63.364292144775391 51 -63.267787933349609 52 -63.031578063964837 53 -62.672012329101555
		 54 -62.204833984374993 55 -61.64459228515625 56 -60.728279113769538 57 -59.313930511474609
		 58 -57.606163024902344 59 -55.813941955566406 60 -54.145404815673828 61 -52.535430908203125
		 62 -50.513076782226563 63 -48.050872802734375 64 -45.095058441162109 65 -41.574851989746094
		 66 -37.293987274169922 67 -32.189807891845703 68 -27.101356506347656 69 -22.221994400024414
		 70 -17.250236511230469 71 -12.844887733459473 72 -9.2361335754394531 73 -6.3020105361938477
		 74 -3.5914025306701665 75 -0.91900277137756359 76 1.7141355276107788 77 4.2777261734008789
		 78 6.6991190910339355 79 9.0523414611816406 80 11.395454406738281;
createNode animCurveTA -n "Character1_Hips_rotateZ";
	rename -uid "99D5792F-4FC2-B727-90B5-ECA5178E7EAD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -7.7957358360290527 1 -11.805801391601563
		 2 -17.189529418945312 3 -23.737642288208008 4 -30.73668098449707 5 -36.993885040283203
		 6 -41.168262481689453 7 -43.633281707763672 8 -45.566070556640625 9 -47.482158660888672
		 10 -49.877384185791016 11 -48.713878631591797 12 -45.768363952636719 13 -42.210987091064453
		 14 -37.049938201904297 15 -30.315820693969727 16 -20.725914001464844 17 -10.326396942138672
		 18 -4.5968637466430664 19 -3.9329946041107178 20 -6.9884905815124512 21 -11.331256866455078
		 22 -14.157621383666992 23 -15.293008804321289 24 -15.207808494567871 25 -14.665324211120605
		 26 -13.644279479980469 27 -11.944186210632324 28 -9.9460716247558594 29 -8.1021728515625
		 30 -7.0202140808105469 31 -6.9796528816223145 32 -7.6539411544799805 33 -8.7114830017089844
		 34 -9.730921745300293 35 -10.274477958679199 36 -10.62450122833252 37 -11.221677780151367
		 38 -11.851132392883301 39 -12.244688987731934 40 -12.157757759094238 41 -11.871389389038086
		 42 -11.776254653930664 43 -11.769594192504883 44 -11.727455139160156 45 -11.510544776916504
		 46 -11.00578784942627 47 -10.23237133026123 48 -9.301548957824707 49 -8.3579807281494141
		 50 -7.6915616989135751 51 -7.3298764228820801 52 -7.0484199523925781 53 -6.8488335609436035
		 54 -6.7268857955932617 55 -6.6752223968505859 56 -6.8782525062561035 57 -7.3390169143676758
		 58 -7.8487930297851563 59 -8.3180303573608398 60 -8.7737970352172852 61 -8.7815513610839844
		 62 -8.0431594848632812 63 -6.9084219932556152 64 -5.7398815155029297 65 -4.9053440093994141
		 66 -4.6017971038818359 67 -4.8191900253295898 68 -5.193504810333252 69 -5.2467412948608398
		 70 -5.6274538040161133 71 -5.9240970611572266 72 -6.3659358024597168 73 -6.9534587860107422
		 74 -6.8816952705383301 75 -6.5514812469482422 76 -6.5247697830200195 77 -6.6871829032897949
		 78 -6.9119353294372559 79 -7.2592635154724121 80 -7.7957358360290527;
createNode animCurveTU -n "Character1_Hips_visibility";
	rename -uid "81AC9485-4AEE-DF83-8E03-FCA998695849";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftUpLeg_translateX";
	rename -uid "E4F8FA82-42F1-E863-3866-60AD32974012";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.784505844116211;
createNode animCurveTL -n "Character1_LeftUpLeg_translateY";
	rename -uid "4B6303CA-46B7-401F-3742-6FB160E55252";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5774786561451037e-006;
createNode animCurveTL -n "Character1_LeftUpLeg_translateZ";
	rename -uid "171F1F40-4F11-30CC-118B-4AA7CDC221AA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0264549255371094;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleX";
	rename -uid "1A27F753-4437-6027-48DF-61B18E60A397";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleY";
	rename -uid "0C4AEAE3-4E5C-E75B-5B3C-6DBC8175C406";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleZ";
	rename -uid "0BA88238-4537-A08F-F217-42A2EC1CA335";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateX";
	rename -uid "3CAF39BF-44EF-8DB3-457A-04B306FD0948";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -4.9229340553283691 1 -5.7332096099853516
		 2 -6.9536123275756836 3 -8.215240478515625 4 -9.043487548828125 5 -9.1954832077026367
		 6 -9.1374454498291016 7 -6.8271517753601074 8 -0.073465310037136078 9 6.6522970199584961
		 10 8.3300409317016602 11 -1.4522483348846436 12 -23.820240020751953 13 -38.331809997558594
		 14 -52.375705718994141 15 -45.876605987548828 16 -37.977764129638672 17 -30.089532852172852
		 18 -20.72547721862793 19 -6.2559614181518555 20 15.188200950622559 21 25.136001586914062
		 22 23.280200958251953 23 25.908666610717773 24 29.431688308715824 25 29.508981704711914
		 26 28.545087814331055 27 29.149265289306637 28 30.625608444213864 29 32.255352020263672
		 30 31.871803283691406 31 31.285232543945316 32 30.666793823242191 33 29.304431915283207
		 34 28.058212280273438 35 26.951606750488281 36 25.69523811340332 37 24.209701538085938
		 38 22.747390747070313 39 21.566570281982422 40 20.918701171875 41 20.26746940612793
		 42 19.347478866577148 43 18.617830276489258 44 18.400880813598633 45 18.953857421875
		 46 20.882165908813477 47 24.0252685546875 48 27.499687194824219 49 30.388362884521484
		 50 31.40091514587402 51 30.825618743896484 52 29.891485214233395 53 28.587722778320313
		 54 26.920469284057617 55 24.891181945800781 56 20.645547866821289 57 13.921464920043945
		 58 6.4729814529418945 59 -0.54694521427154541 60 -6.0806436538696289 61 -11.593357086181641
		 62 -18.987003326416016 63 -27.513324737548828 64 -36.493560791015625 65 -45.32647705078125
		 66 -45.8079833984375 67 -35.393348693847656 68 -21.537509918212891 69 -13.442750930786133
		 70 -12.421347618103027 71 -12.968220710754395 72 -12.151407241821289 73 -11.978846549987793
		 74 -12.631392478942871 75 -13.378085136413574 76 -12.96446418762207 77 -11.70106315612793
		 78 -9.4478111267089844 79 -6.8947591781616211 80 -4.9229340553283691;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateY";
	rename -uid "2190EC6F-4238-6784-B779-FF98B0DC587A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 21.506786346435547 1 23.397119522094727
		 2 26.250446319580078 3 30.346797943115231 4 35.272403717041016 5 40.045230865478516
		 6 43.538089752197266 7 46.1702880859375 8 48.154514312744141 9 49.392063140869141
		 10 50.914154052734375 11 45.987174987792969 12 42.033321380615234 13 41.002460479736328
		 14 49.239940643310547 15 48.056865692138672 16 44.27508544921875 17 42.038787841796875
		 18 41.079753875732422 19 42.364070892333984 20 34.152702331542969 21 14.098756790161133
		 22 3.8202278614044185 23 3.7993366718292236 24 7.2190046310424814 25 9.9712724685668945
		 26 10.667984008789063 27 9.8652973175048828 28 8.2493782043457031 29 6.5003013610839844
		 30 5.9966797828674316 31 5.9122018814086914 32 5.9331955909729004 33 6.4456229209899902
		 34 7.109954833984375 35 7.9662280082702637 36 9.5866622924804687 37 12.034342765808105
		 38 14.6160945892334 39 16.641721725463867 40 17.462282180786133 41 17.435066223144531
		 42 17.219043731689453 43 16.700687408447266 44 15.801649093627928 45 14.469459533691406
		 46 12.162038803100586 47 8.8151578903198242 48 5.161527156829834 49 2.0507419109344482
		 50 0.64823973178863525 51 0.62155401706695557 52 0.80149656534194946 53 1.229212760925293
		 54 1.9394592046737671 55 2.9682581424713135 56 5.0048270225524902 57 7.9423270225524902
		 58 11.028106689453125 59 13.951742172241211 60 16.624349594116211 61 19.121969223022461
		 62 21.280303955078125 63 22.645572662353516 64 22.94099235534668 65 22.100616455078125
		 66 19.41889762878418 67 16.149753570556641 68 13.09693717956543 69 10.901763916015625
		 70 10.299566268920898 71 10.202550888061523 72 10.763568878173828 73 13.86103343963623
		 74 18.427433013916016 75 21.854707717895508 76 23.95892333984375 77 24.365571975708008
		 78 23.661832809448242 79 22.502256393432617 80 21.506786346435547;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateZ";
	rename -uid "79829D7E-434F-D5E0-0925-A6BD8033F79A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -3.5466642379760742 1 -9.6383275985717773
		 2 -15.828084945678711 3 -21.854610443115234 4 -27.566932678222656 5 -32.726455688476562
		 6 -36.963333129882812 7 -36.564815521240234 8 -31.180912017822262 9 -26.980293273925781
		 10 -29.523012161254883 11 -41.624526977539063 12 -61.049766540527337 13 -63.513156890869134
		 14 -63.213947296142571 15 -59.768836975097663 16 -62.342193603515632 17 -63.207424163818352
		 18 -69.100807189941406 19 -79.332038879394531 20 -58.314765930175781 21 -16.662038803100586
		 22 10.437880516052246 23 14.984879493713379 24 10.325881004333496 25 7.2919445037841806
		 26 7.1304001808166504 27 7.1131553649902344 28 7.203974723815918 29 7.3476266860961914
		 30 7.054682731628418 31 6.8203015327453613 32 6.5949130058288574 33 6.1687793731689453
		 34 5.8291106224060059 35 5.60919189453125 36 5.5687494277954102 37 5.7331399917602539
		 38 6.0076088905334473 39 6.2401914596557617 40 6.2631087303161621 41 5.9669661521911621
		 42 5.484433650970459 43 4.958747386932373 44 4.4973201751708984 45 4.1993932723999023
		 46 4.1334357261657715 47 4.2049942016601562 48 4.334404468536377 49 4.469398021697998
		 50 4.4126310348510742 51 4.1235213279724121 52 3.762028694152832 53 3.3410947322845459
		 54 2.8776669502258301 55 2.3856494426727295 56 1.0336130857467651 57 -1.5794912576675415
		 58 -4.9977192878723145 59 -8.9305801391601562 60 -13.063252449035645 61 -18.144525527954102
		 62 -24.979581832885742 63 -33.040546417236328 64 -41.661212921142578 65 -50.077045440673828
		 66 -49.972648620605469 67 -41.406639099121094 68 -33.692119598388672 69 -29.967294692993164
		 70 -28.953811645507813 71 -28.861898422241214 72 -32.827800750732422 73 -40.793125152587891
		 74 -44.497898101806641 75 -38.341213226318359 76 -31.603490829467773 77 -24.792734146118164
		 78 -17.742765426635742 79 -10.57172679901123 80 -3.5466642379760742;
createNode animCurveTU -n "Character1_LeftUpLeg_visibility";
	rename -uid "95BF355F-475F-AFD4-1983-8F9F1080DD59";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftLeg_translateX";
	rename -uid "4805BED7-4C99-34B3-1BD5-53AC1CF19AD9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.968876838684082;
createNode animCurveTL -n "Character1_LeftLeg_translateY";
	rename -uid "29D2599D-4BB8-04D1-817F-16A85831E071";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.2585945129394531;
createNode animCurveTL -n "Character1_LeftLeg_translateZ";
	rename -uid "272211F1-485E-B786-B13B-909E4D6D728D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 18.197568893432617;
createNode animCurveTU -n "Character1_LeftLeg_scaleX";
	rename -uid "890DA9F8-424D-7CF6-0A25-30A5258F733A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_LeftLeg_scaleY";
	rename -uid "3F3E1246-4560-4844-630E-36898FC516DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftLeg_scaleZ";
	rename -uid "377A9B0D-44B6-272D-5C2F-189C5119DC2B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftLeg_rotateX";
	rename -uid "56A04CE1-4197-2AD5-ECF6-D3830DC8AD35";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 52.102336883544922 1 48.165145874023438
		 2 42.913700103759766 3 35.521820068359375 4 25.879814147949219 5 15.863162040710451
		 6 8.8111391067504883 7 4.7792987823486328 8 -0.087546989321708679 9 -5.1234326362609863
		 10 -8.2123327255249023 11 -0.13710719347000122 12 35.900215148925781 13 71.29034423828125
		 14 104.75906372070312 15 109.21348571777344 16 107.44345855712891 17 103.25298309326172
		 18 91.952598571777344 19 67.124298095703125 20 37.142414093017578 21 21.992404937744141
		 22 10.193768501281738 23 3.2980806827545166 24 0.18597342073917389 25 -0.098416909575462341
		 26 0.63881075382232666 27 -0.56010901927947998 28 -2.9525318145751953 29 -5.7617349624633789
		 30 -5.6436271667480469 31 -5.5879073143005371 32 -5.3190302848815918 33 -3.599582433700562
		 34 -1.9688469171524046 35 -0.45640233159065247 36 1.6222782135009766 37 4.4023275375366211
		 38 7.1920499801635733 39 9.3711357116699219 40 10.466771125793457 41 11.613510131835938
		 42 13.566171646118164 43 15.50089645385742 44 16.806915283203125 45 16.976795196533203
		 46 15.259190559387207 47 11.731842994689941 48 7.3145961761474618 49 3.2995584011077881
		 50 1.8330994844436643 51 2.5469121932983398 52 3.5133709907531738 53 4.730198860168457
		 54 6.1498494148254395 55 7.7252049446105948 56 11.595514297485352 57 17.622638702392578
		 58 23.5799560546875 59 28.597322463989258 60 32.195194244384766 61 36.357597351074219
		 62 42.389301300048828 63 48.784538269042969 64 54.676918029785156 65 59.803070068359382
		 66 57.245723724365234 67 42.439128875732422 68 20.024341583251953 69 5.1699652671813965
		 70 4.778101921081543 71 10.900076866149902 72 21.629961013793945 73 36.426280975341797
		 74 50.489028930664063 75 60.925704956054687 76 66.246833801269531 77 66.336700439453125
		 78 62.681209564208977 79 57.131923675537109 80 52.102336883544922;
createNode animCurveTA -n "Character1_LeftLeg_rotateY";
	rename -uid "86187CFE-4E56-9A48-4A47-E296FDABC5C7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 21.536457061767578 1 25.537786483764648
		 2 31.537445068359371 3 37.211948394775391 4 40.10516357421875 5 38.366603851318359
		 6 32.019287109375 7 23.429449081420898 8 13.657336235046387 9 4.8287644386291504
		 10 -1.1906956434249878 11 -4.6631360054016113 12 -14.331982612609865 13 -19.130317687988281
		 14 -28.57196044921875 15 -30.977516174316406 16 -33.512271881103516 17 -35.034660339355469
		 18 -36.897312164306641 19 -43.621376037597656 20 -27.283004760742187 21 9.4196701049804687
		 22 26.661630630493164 23 23.264875411987305 24 15.563520431518556 25 12.571475982666016
		 26 12.428896903991699 27 11.175467491149902 28 9.3173322677612305 29 7.299279212951661
		 30 7.3040351867675781 31 7.5329060554504386 32 8.1219749450683594 33 9.7234764099121094
		 34 11.125648498535156 35 12.065383911132813 36 12.739121437072754 37 13.234709739685059
		 38 13.344698905944824 39 13.111493110656738 40 12.747026443481445 41 12.819681167602539
		 42 13.432052612304687 43 14.239562034606934 44 15.015593528747559 45 15.566818237304689
		 46 15.880744934082029 47 15.861895561218262 48 15.236408233642578 49 14.129059791564941
		 50 13.607733726501465 51 13.921167373657227 52 14.375415802001953 53 14.96914005279541
		 54 15.68028736114502 55 16.494892120361328 56 18.351144790649414 57 20.798702239990234
		 58 22.649738311767578 59 23.466461181640625 60 22.970983505249023 61 21.205520629882813
		 62 17.766496658325195 63 12.393299102783203 64 5.4099268913269043 65 -2.5208861827850342
		 66 -5.1121821403503418 67 -3.1894590854644775 68 -4.311187744140625 69 -6.4908771514892578
		 70 -6.4734983444213867 71 -4.4186553955078125 72 1.6144537925720215 73 9.8807392120361328
		 74 10.823502540588379 75 9.040553092956543 76 8.4965591430664062 77 10.389352798461914
		 78 13.964298248291016 79 18.011602401733398 80 21.536457061767578;
createNode animCurveTA -n "Character1_LeftLeg_rotateZ";
	rename -uid "DC3C62BA-40E0-DFC6-F489-40B570551827";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 17.270214080810547 1 13.858617782592773
		 2 8.3777933120727539 3 0.16657468676567078 4 -10.455376625061035 5 -20.823827743530273
		 6 -26.127254486083984 7 -25.288022994995117 8 -21.864692687988281 9 -15.030618667602537
		 10 -5.3878841400146484 11 7.9790430068969727 12 21.496463775634766 13 21.933797836303711
		 14 12.755385398864746 15 8.4869318008422852 16 6.0642189979553223 17 6.2220444679260254
		 18 9.3349790573120117 19 16.785783767700195 20 26.109167098999023 21 3.9350714683532715
		 22 -20.646896362304688 23 -27.377412796020508 24 -23.960317611694336 25 -20.381052017211914
		 26 -19.062341690063477 27 -19.059005737304688 28 -19.933469772338867 29 -21.274143218994141
		 30 -21.053701400756836 31 -21.404447555541992 32 -22.040388107299805 33 -21.757761001586914
		 34 -21.237691879272461 35 -20.210813522338867 36 -18.060319900512695 37 -14.999648094177246
		 38 -11.846842765808105 39 -9.284031867980957 40 -7.8223652839660645 41 -6.814171314239502
		 42 -5.6352982521057129 43 -4.675410270690918 44 -4.2203884124755859 45 -4.5280776023864746
		 46 -6.3011775016784668 47 -9.6485271453857422 48 -13.785432815551758 49 -17.52960205078125
		 50 -18.909500122070313 51 -18.302709579467773 52 -17.543787002563477 53 -16.647861480712891
		 54 -15.661555290222166 55 -14.631313323974608 56 -12.082306861877441 57 -8.0064725875854492
		 58 -3.8259265422821045 59 -0.058385156095027917 60 3.0300183296203613 61 6.8480257987976074
		 62 12.021159172058105 63 16.957969665527344 64 20.633380889892578 65 22.650093078613281
		 66 22.03654670715332 67 18.41411018371582 68 13.787636756896973 69 12.482013702392578
		 70 12.354963302612305 71 11.435261726379395 72 9.625885009765625 73 11.523669242858887
		 74 18.080780029296875 75 23.161722183227539 76 25.649755477905273 77 25.860187530517578
		 78 24.10931396484375 79 20.809825897216797 80 17.270214080810547;
createNode animCurveTU -n "Character1_LeftLeg_visibility";
	rename -uid "808C788C-49FF-CD2B-B024-7E863CADED0E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftFoot_translateX";
	rename -uid "5B4F90BD-4E03-710C-3859-FF8C232DBE04";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5243232250213623;
createNode animCurveTL -n "Character1_LeftFoot_translateY";
	rename -uid "FDE6D0D1-4125-61FD-4B62-19B472188341";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.000351905822754;
createNode animCurveTL -n "Character1_LeftFoot_translateZ";
	rename -uid "F9F34CDC-42AD-4D52-0AFC-91B9DDA7EF8B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.967419624328613;
createNode animCurveTU -n "Character1_LeftFoot_scaleX";
	rename -uid "EEF6F95B-47E2-052A-54EF-44A5E1576A24";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFoot_scaleY";
	rename -uid "8C0F0F5F-41F4-9F94-98B3-51AC6C5658B7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleZ";
	rename -uid "44A839EF-47DE-C946-C997-6DB2868DE8FF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999964237213135;
createNode animCurveTA -n "Character1_LeftFoot_rotateX";
	rename -uid "7E3AEB6A-4C49-3DCA-9E9A-73B662CB0107";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 41.134159088134766 1 37.245841979980469
		 2 32.330825805664063 3 25.396753311157227 4 16.16319465637207 5 5.9254641532897949
		 6 -2.2385029792785645 7 -5.9678406715393066 8 -7.7035393714904785 9 -8.3050937652587891
		 10 -7.8009605407714844 11 -6.8837056159973145 12 -0.28969502449035645 13 9.0390939712524414
		 14 25.072025299072266 15 28.388919830322266 16 24.71794319152832 17 20.718404769897461
		 18 12.167208671569824 19 1.7758744955062866 20 13.360816955566406 21 28.507251739501953
		 22 30.888694763183594 23 28.932790756225586 24 27.044614791870117 25 26.770675659179688
		 26 26.915016174316406 27 25.941694259643555 28 24.308040618896484 29 22.451684951782227
		 30 22.383918762207031 31 22.370782852172852 32 22.547294616699219 33 23.605655670166016
		 34 24.545415878295898 35 25.254327774047852 36 26.031488418579102 37 26.952348709106445
		 38 27.719797134399414 39 28.168203353881836 40 28.232366561889648 41 28.526849746704102
		 42 29.353624343872074 43 30.300493240356445 44 31.076650619506836 45 31.44073486328125
		 46 31.225111007690433 47 30.426382064819339 48 29.109136581420898 49 27.612045288085938
		 50 26.952451705932617 51 27.184486389160156 52 27.528739929199219 53 27.980184555053711
		 54 28.514118194580078 55 29.109975814819336 56 30.508518218994137 57 32.393714904785156
		 58 33.913150787353516 59 34.9482421875 60 35.585189819335938 61 36.513492584228516
		 62 37.659664154052734 63 38.162876129150391 64 37.406879425048828 65 35.042854309082031
		 66 30.5948486328125 67 23.224065780639648 68 14.093910217285156 69 8.6240825653076172
		 70 7.3912935256958017 71 7.5629630088806152 72 12.019261360168457 73 22.415908813476562
		 74 31.816707611083984 75 39.852508544921875 76 44.948387145996094 77 46.473125457763672
		 78 45.583160400390625 79 43.359169006347656 80 41.134159088134766;
createNode animCurveTA -n "Character1_LeftFoot_rotateY";
	rename -uid "A87D207B-4EE8-CF5F-0221-87BB2D391212";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -7.5173330307006836 1 -1.362676739692688
		 2 6.9769597053527832 3 15.609874725341795 4 22.470466613769531 5 26.165111541748047
		 6 26.854438781738281 7 25.860280990600586 8 24.069938659667969 9 22.162792205810547
		 10 20.753421783447266 11 16.054059982299805 12 1.3229173421859741 13 -9.3296718597412109
		 14 -16.329954147338867 15 -16.281770706176758 16 -14.632424354553221 17 -12.251894950866699
		 18 -11.694118499755859 19 -11.5382080078125 20 -7.3292717933654785 21 5.8888940811157227
		 22 14.739860534667967 23 15.086319923400879 24 13.810458183288574 25 13.110250473022461
		 26 12.887563705444336 27 13.163249015808105 28 13.783920288085938 29 14.551206588745117
		 30 14.495457649230959 31 14.461181640625 32 14.345492362976074 33 13.831663131713867
		 34 13.381468772888184 35 13.001823425292969 36 12.547621726989746 37 11.954967498779297
		 38 11.314433097839355 39 10.7427978515625 40 10.302016258239746 41 9.6253137588500977
		 42 8.5620403289794922 43 7.4396028518676758 44 6.5312528610229492 45 6.0805120468139648
		 46 6.3351306915283203 47 7.218564510345459 48 8.3943195343017578 49 9.4312705993652344
		 50 9.6975069046020508 51 9.3198642730712891 52 8.901494026184082 53 8.5070981979370117
		 54 8.2201976776123047 55 8.1268529891967773 56 7.6747169494628897 57 6.9093923568725586
		 58 6.3944821357727051 59 6.1871280670166016 60 6.1509256362915039 61 5.665891170501709
		 62 4.2299942970275879 63 2.0889401435852051 64 -0.42059695720672607 65 -2.9017305374145508
		 66 -5.070744514465332 67 -5.4494390487670898 68 -1.6515407562255859 69 2.4706254005432129
		 70 1.8660308122634888 71 -2.1551783084869385 72 -7.031649112701416 73 -8.9068927764892578
		 74 -11.760156631469727 75 -16.553905487060547 76 -18.81683349609375 77 -17.896036148071289
		 78 -14.893417358398437 79 -11.045647621154785 80 -7.5173330307006836;
createNode animCurveTA -n "Character1_LeftFoot_rotateZ";
	rename -uid "7AF60D8C-4EA6-D4BB-9512-9C9E167817E3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 14.605794906616211 1 11.436677932739258
		 2 7.3519625663757315 3 2.0005137920379639 4 -4.6351509094238281 5 -11.484586715698242
		 6 -16.327966690063477 7 -17.954652786254883 8 -18.135744094848633 9 -17.521137237548828
		 10 -16.315263748168945 11 -12.748232841491699 12 -0.68602228164672852 13 8.2072381973266602
		 14 14.439309120178224 15 14.732097625732424 16 13.491580009460449 17 12.051403999328613
		 18 10.508513450622559 19 8.4062900543212891 20 8.6809072494506836 21 7.4753603935241699
		 22 5.3308720588684082 23 4.3754568099975586 24 4.2326374053955078 25 4.534421443939209
		 26 4.6632213592529297 27 4.1749567985534668 28 3.2880344390869141 29 2.2311668395996094
		 30 2.2221088409423828 31 2.2293760776519775 32 2.3475484848022461 33 2.97902512550354
		 34 3.5275096893310547 35 3.9478011131286621 36 4.4134707450866699 37 4.9735221862792969
		 38 5.4784750938415527 39 5.8305792808532715 40 5.9913687705993652 41 6.3133726119995117
		 42 6.9481143951416016 43 7.628718376159668 44 8.1699295043945312 45 8.4273967742919922
		 46 8.2809076309204102 47 7.7448296546936035 48 6.9198398590087891 49 6.043342113494873
		 50 5.7095813751220703 51 5.918708324432373 52 6.1812615394592285 53 6.473454475402832
		 54 6.7591719627380371 55 7.0051202774047852 56 7.6898803710937509 57 8.6870594024658203
		 58 9.4996824264526367 59 10.059392929077148 60 10.414724349975586 61 10.927578926086426
		 62 11.654973983764648 63 12.228598594665527 64 12.400424957275391 65 12.027273178100586
		 66 10.839646339416504 67 8.9366264343261719 68 5.6383585929870605 69 2.2264394760131836
		 70 1.7390714883804321 71 3.8184018135070805 72 7.1655550003051749 73 10.279153823852539
		 74 13.891999244689941 75 16.481828689575195 76 17.591314315795898 77 17.575414657592773
		 78 16.890645980834961 79 15.774643898010254 80 14.605794906616211;
createNode animCurveTU -n "Character1_LeftFoot_visibility";
	rename -uid "4F34BFB7-41C5-5ED9-2723-138026F4CF00";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftToeBase_translateX";
	rename -uid "A045D762-4180-D568-5452-CFB6EE23A28F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.585639476776123;
createNode animCurveTL -n "Character1_LeftToeBase_translateY";
	rename -uid "193440F9-46CF-5EAB-198E-F7A9F28693F9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3360438346862793;
createNode animCurveTL -n "Character1_LeftToeBase_translateZ";
	rename -uid "C2AE4CE9-4C69-131C-D0E4-4AB692A7F376";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.4897398948669434;
createNode animCurveTU -n "Character1_LeftToeBase_scaleX";
	rename -uid "B0C01E3B-4B43-5D69-EA04-2FA46DE1F281";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftToeBase_scaleY";
	rename -uid "EC077F35-4B5D-83DC-567D-6AAC28D9B855";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftToeBase_scaleZ";
	rename -uid "95AA1A18-484B-1CBB-631A-16A0F6FDCC05";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_LeftToeBase_rotateX";
	rename -uid "37444AD2-4ECA-3BD2-997D-50934FC14072";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 -1.4266123837813893e-009 1 4.0475287437438965
		 2 8.6183147430419922 3 13.114997863769531 4 16.979198455810547 5 19.68157958984375
		 6 20.700359344482422 7 20.700359344482422 16 20.700359344482422 17 20.700359344482422
		 18 17.563694000244141 19 10.540765762329102 20 3.335848331451416 21 -1.9855725952311332e-009
		 22 -1.5202129555191846e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateY";
	rename -uid "2C795859-4E30-2C43-73E1-5F9DF9E42F76";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 1.7694730125583646e-009 1 -1.3594192266464233
		 2 -3.1181068420410156 3 -5.076836109161377 4 -6.9370265007019043 5 -8.3321104049682617
		 6 -8.8775806427001953 7 -8.8775806427001953 16 -8.8775806427001953 17 -8.8775806427001953
		 18 -7.232302188873291 19 -3.9280276298522954 20 -1.1068810224533081 21 1.7452117528904409e-009
		 22 1.5728097713108014e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateZ";
	rename -uid "5CC59C90-4D28-709F-03FE-A29857830642";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 1.912575653406634e-009 1 2.3774752616882324
		 2 4.9796805381774902 3 7.4390397071838379 4 9.4599676132202148 5 10.816066741943359
		 6 11.314101219177246 7 11.314101219177246 16 11.314101219177246 17 11.314101219177246
		 18 9.7574558258056641 19 6.0442948341369629 20 1.9640835523605344 21 1.770392166200452e-009
		 22 1.678054251108563e-009;
createNode animCurveTU -n "Character1_LeftToeBase_visibility";
	rename -uid "58AC25E3-40A6-5B93-3320-8598D75609A2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateX";
	rename -uid "8817F3CF-41F6-4ECE-B6B9-50BE65404C57";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7153444290161133;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateY";
	rename -uid "08901640-4CAC-E037-C915-B1B577775210";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.0020139217376709;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateZ";
	rename -uid "9821AB78-49B3-28DD-83C0-66830917E388";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.1786108016967773;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleX";
	rename -uid "9AEF99C9-49A0-2764-7B6A-EEBB2C0819BF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleY";
	rename -uid "BEFB7D9C-4CB9-CA31-995A-4EB29734F2A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999940395355225;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleZ";
	rename -uid "9A9DAEEC-49EC-63B3-18B4-6B9927773E4A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateX";
	rename -uid "46F5B275-4B58-F96C-FDCA-928734B2DFD7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3689390166458111e-010;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateY";
	rename -uid "E59DAB33-45F9-170F-ADAF-9BA83FFB1FDC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8191628203823031e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateZ";
	rename -uid "3E321B9C-4EC5-F79B-B1FD-D8B2712E733A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.2117916083413994e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle1_visibility";
	rename -uid "8D442C43-42D0-52F8-6342-F498E3B8F084";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateX";
	rename -uid "D1C012C7-4B09-5564-CA09-32B24099D1B4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4684751033782959;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateY";
	rename -uid "0F831672-4BBA-68B2-BA7F-9C9B7EB1561A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.1393222808837891;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateZ";
	rename -uid "4393987D-482A-3303-9227-A3A34E2B73CA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.3014278411865234;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleX";
	rename -uid "553BB8D2-40CB-3FA4-3E80-D4B87C701E0A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleY";
	rename -uid "08F34CF0-4F73-8F22-2EFD-5E981EBAC85E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleZ";
	rename -uid "EAA92648-4393-3623-4D23-83A12198D7E3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateX";
	rename -uid "AFECA9F3-4B50-27D1-0329-C4974F2AD0DF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1989890857933005e-008;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateY";
	rename -uid "F19B65FA-43C4-FE94-DAF0-2A9CD9F191B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.5606962347005719e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateZ";
	rename -uid "825E0E4B-44B1-D7A4-9A85-3AA5177783AC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3814162581080609e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle2_visibility";
	rename -uid "0EF3AC88-41D3-7159-8C7B-F197C9A4D4B8";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightUpLeg_translateX";
	rename -uid "DDA3E4B3-4ED7-FC8B-D949-28857FB7C035";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.489771842956543;
createNode animCurveTL -n "Character1_RightUpLeg_translateY";
	rename -uid "FAB7597E-4D36-27E9-7C86-F39C438037F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.6505136489868164;
createNode animCurveTL -n "Character1_RightUpLeg_translateZ";
	rename -uid "02F30CCA-4FA6-EBE3-4E26-4486181346C6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0264549255371094;
createNode animCurveTU -n "Character1_RightUpLeg_scaleX";
	rename -uid "4261E4C4-447B-404B-B7F2-608261F045FF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightUpLeg_scaleY";
	rename -uid "532C459D-4BAD-D72B-E061-78B0ADF8DDDC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightUpLeg_scaleZ";
	rename -uid "009BB665-40DA-4102-97F1-4E8123AD2E83";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightUpLeg_rotateX";
	rename -uid "4C1571F2-4F43-0BC6-55BC-09934844873F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -31.774538040161133 1 -26.986299514770508
		 2 -23.724000930786133 3 -22.493413925170898 4 -24.524547576904297 5 -28.942365646362305
		 6 -31.428464889526367 7 -34.428981781005859 8 -40.938053131103516 9 -48.605850219726563
		 10 -54.361961364746094 11 -49.489280700683594 12 -6.8776826858520508 13 -16.938055038452148
		 14 -4.4087080955505371 15 -10.329838752746582 16 -42.856468200683594 17 -61.649253845214844
		 18 -40.736774444580078 19 -17.891569137573242 20 -7.7326536178588876 21 -2.8528435230255127
		 22 0.10648943483829498 23 0.1155252605676651 24 -1.901148796081543 25 -3.8972842693328857
		 26 -4.4935426712036133 27 -4.3977046012878418 28 -3.8872258663177486 29 -3.2140688896179199
		 30 -2.600785493850708 31 -1.9947646856307981 32 -1.3065893650054932 33 -0.74801069498062134
		 34 -0.50755941867828369 35 -0.75227296352386475 36 -1.9387276172637937 37 -4.1038312911987305
		 38 -6.772702693939209 39 -9.3654460906982422 40 -11.191933631896973 41 -11.60968017578125
		 42 -10.818911552429199 43 -9.6912546157836914 44 -8.3838510513305664 45 -6.9901962280273437
		 46 -5.089226245880127 47 -2.6146814823150635 48 -0.16498802602291107 49 1.7376371622085571
		 50 2.596827507019043 51 2.7529337406158447 52 2.9195346832275391 53 3.137643575668335
		 54 3.4471254348754883 55 3.8861219882965083 56 3.2572884559631348 57 0.48723453283309931
		 58 -4.2792234420776367 59 -10.960359573364258 60 -19.376808166503906 61 -28.531854629516602
		 62 -37.849857330322266 63 -46.734367370605469 64 -53.65887451171875 65 -56.718700408935547
		 66 -54.763893127441406 67 -53.213176727294922 68 -47.329860687255859 69 -28.918052673339847
		 70 -6.4559540748596191 71 16.574064254760742 72 20.843027114868164 73 5.197695255279541
		 74 5.6176061630249023 75 12.250205993652344 76 6.8831281661987305 77 -3.4806680679321289
		 78 -14.46882438659668 79 -24.022392272949219 80 -31.774538040161133;
createNode animCurveTA -n "Character1_RightUpLeg_rotateY";
	rename -uid "C35B7412-45A8-E905-55CE-4BBAD7B9E19A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 36.258934020996094 1 48.795345306396484
		 2 57.627971649169922 3 63.450275421142578 4 66.008926391601563 5 63.985675811767578
		 6 55.397159576416016 7 46.724609375 8 44.250003814697266 9 44.675098419189453 10 45.593971252441406
		 11 62.796997070312507 12 41.89501953125 13 12.863669395446777 14 38.941116333007813
		 15 54.609371185302734 16 63.994709014892571 17 64.235397338867188 18 61.270503997802734
		 19 46.155464172363281 20 24.980243682861328 21 6.7608494758605957 22 -4.9420199394226074
		 23 -13.648091316223145 24 -19.766138076782227 25 -23.003557205200195 26 -23.811866760253906
		 27 -24.10838508605957 28 -24.107433319091797 29 -23.887567520141602 30 -23.325666427612305
		 31 -22.322763442993164 32 -21.036331176757813 33 -19.821758270263672 34 -19.061164855957031
		 35 -19.103559494018555 36 -20.446802139282227 37 -23.002527236938477 38 -26.240854263305664
		 39 -29.558115005493164 40 -32.242683410644531 41 -33.142063140869141 42 -32.216060638427734
		 43 -30.990152359008789 44 -29.67643928527832 45 -28.389516830444336 46 -26.737314224243164
		 47 -24.666524887084961 48 -22.693954467773438 49 -21.223213195800781 50 -20.653511047363281
		 51 -20.62017822265625 52 -20.380563735961914 53 -19.813112258911133 54 -18.800043106079102
		 55 -17.230592727661133 56 -13.198940277099609 57 -5.8930525779724121 58 3.1789340972900391
		 59 12.516205787658691 60 20.605749130249023 61 27.966323852539063 62 34.838100433349609
		 63 40.066825866699219 64 43.288463592529297 65 44.767745971679688 66 40.7520751953125
		 67 28.210397720336914 68 11.507326126098633 69 17.112337112426758 70 27.2227783203125
		 71 38.182537078857422 72 48.237998962402344 73 38.291286468505859 74 36.661674499511719
		 75 49.348712921142578 76 54.190162658691406 77 53.171257019042969 78 48.567665100097656
		 79 42.322677612304688 80 36.258934020996094;
createNode animCurveTA -n "Character1_RightUpLeg_rotateZ";
	rename -uid "0F4B1238-47A1-FFC4-D659-509E8B004CC0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -18.08891487121582 1 -18.235092163085937
		 2 -16.614126205444336 3 -17.251379013061523 4 -23.75346565246582 5 -36.390331268310547
		 6 -49.555412292480469 7 -59.149070739746087 8 -66.841827392578125 9 -72.845695495605469
		 10 -76.085990905761719 11 -61.575088500976563 12 -2.5489547252655029 13 -0.86204451322555542
		 14 -24.137153625488281 15 -43.598728179931641 16 -81.103111267089844 17 -102.97270202636719
		 18 -82.377799987792969 19 -53.937694549560547 20 -36.633922576904297 21 -26.938234329223633
		 22 -21.585107803344727 23 -17.540920257568359 24 -14.436479568481445 25 -12.373120307922363
		 26 -11.808382987976074 27 -12.355045318603516 28 -13.488419532775879 29 -14.68923854827881
		 30 -15.434160232543945 31 -15.810623168945313 32 -16.140527725219727 33 -16.279455184936523
		 34 -16.144041061401367 35 -15.663593292236328 36 -14.288898468017578 37 -11.866493225097656
		 38 -8.9581060409545898 39 -6.2961492538452148 40 -4.8143916130065918 41 -4.8519201278686523
		 42 -5.8313918113708496 43 -7.2550191879272452 44 -8.9726905822753906 45 -10.873363494873047
		 46 -13.466866493225098 47 -16.765565872192383 48 -19.979040145874023 49 -22.470760345458984
		 50 -23.67430305480957 51 -23.989845275878906 52 -24.181835174560547 53 -24.201189041137695
		 54 -23.994632720947266 55 -23.510372161865234 56 -22.931272506713867 57 -22.587532043457031
		 58 -22.698024749755859 59 -23.477869033813477 60 -25.050926208496094 61 -28.081331253051758
		 62 -33.012397766113281 63 -39.011940002441406 64 -44.433380126953125 65 -47.275127410888672
		 66 -42.076972961425781 67 -27.338069915771484 68 -9.0989341735839844 69 -12.041708946228027
		 70 -5.9649114608764648 71 7.7902469635009766 72 17.512208938598633 73 16.225351333618164
		 74 10.56950569152832 75 6.2245573997497559 76 0.60003072023391724 77 -6.9402194023132324
		 78 -13.068242073059082 79 -16.510248184204102 80 -18.08891487121582;
createNode animCurveTU -n "Character1_RightUpLeg_visibility";
	rename -uid "8600C28B-40D2-75CB-9F67-B3B7AF132CC2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightLeg_translateX";
	rename -uid "300BD9E7-4F8A-31BE-5B49-0BBA23AC9880";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -18.197565078735352;
createNode animCurveTL -n "Character1_RightLeg_translateY";
	rename -uid "247CEFB7-43E1-54CD-B4BB-DFAAEF9AA8D1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.1862249374389648;
createNode animCurveTL -n "Character1_RightLeg_translateZ";
	rename -uid "80FAB5E0-4B3F-1BB1-6010-FF92BEDE0FF4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.5520458221435547;
createNode animCurveTU -n "Character1_RightLeg_scaleX";
	rename -uid "24131733-4CFA-1DE5-2A27-D7A0B8CF62C2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightLeg_scaleY";
	rename -uid "02D69132-45EE-5996-B4BC-F8A071A3E420";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightLeg_scaleZ";
	rename -uid "4310F42B-4393-EAF7-5083-76BD7201CCF0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightLeg_rotateX";
	rename -uid "DC8F5F10-446A-71F5-8609-058BCBB64918";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 32.307552337646484 1 38.737998962402344
		 2 63.511367797851563 3 120.33718872070312 4 153.66050720214844 5 170.61436462402344
		 6 186.3284912109375 7 197.37353515625 8 200.20408630371094 9 196.26449584960937 10 187.56828308105469
		 11 146.36904907226562 12 215.6888427734375 13 170.82138061523437 14 223.70045471191406
		 15 275.68475341796875 16 314.19049072265625 17 330.86236572265625 18 310.21646118164062
		 19 251.60180664062503 20 212.76718139648437 21 190.07710266113281 22 175.93045043945313
		 23 163.85325622558594 24 155.121826171875 25 150.36651611328125 26 148.94305419921875
		 27 148.73204040527344 28 149.13934326171875 29 149.67042541503906 30 149.9798583984375
		 31 150.32058715820312 32 150.9918212890625 33 151.64152526855469 34 151.91604614257812
		 35 151.49734497070313 36 149.93498229980469 37 147.28205871582031 38 144.00874328613281
		 39 140.73365783691406 40 138.24395751953125 41 137.50117492675781 42 138.47346496582031
		 43 139.9422607421875 44 141.72569274902344 45 143.72552490234375 46 146.61740112304687
		 47 150.38499450683594 48 154.02020263671875 49 156.69920349121094 50 157.69705200195312
		 51 157.51492309570312 52 157.23158264160156 53 156.94927978515625 54 156.76358032226562
		 55 156.76821899414062 56 158.91163635253906 57 164.34181213378906 58 171.92959594726562
		 59 180.60462951660156 60 189.47785949707031 61 198.62507629394531 62 207.455322265625
		 63 215.11515808105469 64 221.45916748046875 65 226.95181274414062 66 218.08744812011719
		 67 198.01089477539062 68 170.77680969238281 69 168.37298583984375 70 180.33389282226562
		 71 210.32150268554687 72 178.24333190917969 73 175.84324645996094 74 187.19354248046875
		 75 203.03291320800781 76 209.43531799316406 77 212.33306884765625 78 212.86331176757812
		 79 212.53306579589844 80 212.30755615234375;
createNode animCurveTA -n "Character1_RightLeg_rotateY";
	rename -uid "A7427BF4-4670-F079-BDBD-E2A62F4353DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -26.452003479003906 1 -54.824325561523438
		 2 -72.050399780273437 3 -75.364723205566406 4 -68.848663330078125 5 -64.118492126464844
		 6 -64.645904541015625 7 -67.862258911132813 8 -68.297531127929688 9 -66.240608215332031
		 10 -63.683242797851562 11 -55.939231872558594 12 -108.50194549560547 13 -164.938232421875
		 14 -112.98677825927734 15 -103.88626098632812 16 -101.98538970947266 17 -101.64112854003906
		 18 -101.16291046142578 19 -106.12633514404297 20 -120.77734375 21 -133.44967651367188
		 22 -139.86854553222656 23 -147.93373107910156 24 -157.05882263183594 25 -163.16554260253906
		 26 -163.70196533203125 27 -162.12071228027344 28 -159.67098999023437 29 -157.38652038574219
		 30 -155.91984558105469 31 -155.33833312988281 32 -155.156494140625 33 -155.32730102539062
		 34 -155.8282470703125 35 -156.56716918945312 36 -158.19802856445312 37 -161.27682495117187
		 38 -165.49588012695312 39 -170.1820068359375 40 -174.21414184570312 41 -175.28849792480469
		 42 -172.99539184570312 43 -170.39732360839844 44 -167.75469970703125 45 -165.09805297851563
		 46 -161.71817016601562 47 -157.844970703125 48 -154.51524353027344 49 -152.24839782714844
		 50 -151.33590698242187 51 -151.29342651367187 52 -151.24740600585937 53 -150.99064636230469
		 54 -150.3345947265625 55 -149.11529541015625 56 -147.345458984375 57 -145.58514404296875
		 58 -144.29676818847656 59 -143.5931396484375 60 -143.36993408203125 61 -141.3909912109375
		 62 -136.59329223632812 63 -130.30612182617187 64 -123.59869384765626 65 -117.36289978027344
		 66 -124.14228820800783 67 -154.28863525390625 68 -186.57948303222656 69 -150.436279296875
		 70 -120.31932067871094 71 -107.8253173828125 72 -122.15190124511717 73 -179.94422912597656
		 74 -188.18820190429687 75 -154.350341796875 76 -139.19046020507812 77 -136.1280517578125
		 78 -139.44657897949219 79 -146.32926940917969 80 -153.54800415039062;
createNode animCurveTA -n "Character1_RightLeg_rotateZ";
	rename -uid "353BB8B7-4D16-F3DA-030A-B7A8F528010E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 2.6833248138427734 1 -7.0175909996032715
		 2 -34.040012359619141 3 -91.511917114257813 4 -122.78505706787109 5 -133.56088256835937
		 6 -138.63233947753906 7 -141.11405944824219 8 -142.08538818359375 9 -141.55326843261719
		 10 -139.64041137695312 11 -134.65071105957031 12 -227.46553039550781 13 -181.48974609375
		 14 -206.407470703125 15 -246.01837158203125 16 -271.51473999023437 17 -282.55551147460937
		 18 -265.377197265625 19 -216.93125915527344 20 -194.54507446289062 21 -188.30325317382812
		 22 -186.26512145996094 23 -183.91871643066406 24 -182.06169128417969 25 -181.20135498046875
		 26 -181.06280517578125 27 -181.15106201171875 28 -181.37359619140625 29 -181.62907409667969
		 30 -181.81004333496094 31 -181.90483093261719 32 -181.98291015625 33 -182.01663208007813
		 34 -181.98069763183594 35 -181.86595153808594 36 -181.57492065429687 37 -181.09318542480469
		 38 -180.54808044433594 39 -180.07221984863281 40 -179.77656555175781 41 -179.70112609863281
		 42 -179.80268859863281 43 -179.98684692382812 44 -180.23797607421875 45 -180.54499816894531
		 46 -181.01118469238281 47 -181.65150451660156 48 -182.29995727539062 49 -182.79306030273437
		 50 -182.99612426757813 51 -182.9920654296875 52 -182.98211669921875 53 -183.00576782226562
		 54 -183.10136413574219 55 -183.31083679199219 56 -183.78739929199219 57 -184.45358276367187
		 58 -184.9400634765625 59 -184.9278564453125 60 -184.27291870117187 61 -183.60490417480469
		 62 -183.59211730957031 63 -184.57728576660156 64 -187.21040344238281 65 -192.45016479492187
		 66 -188.33378601074219 67 -180.29824829101562 68 -180.69390869140625 69 -183.51399230957031
		 70 -198.20808410644531 71 -229.30813598632812 72 -196.28512573242187 73 -180.35362243652344
		 74 -179.0699462890625 75 -179.32366943359375 76 -181.95323181152344 77 -182.37966918945312
		 78 -180.85252380371094 79 -178.81678771972656 80 -177.31668090820312;
createNode animCurveTU -n "Character1_RightLeg_visibility";
	rename -uid "53844A0D-4F75-EB5C-BAB7-579B53388451";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightFoot_translateX";
	rename -uid "49D61ED2-45B0-F197-67E7-649C09B33E93";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -18.25177001953125;
createNode animCurveTL -n "Character1_RightFoot_translateY";
	rename -uid "E1A940B0-43B4-5C57-C66C-C7B4FF832302";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.0794563293457031;
createNode animCurveTL -n "Character1_RightFoot_translateZ";
	rename -uid "51B6BA90-455A-C9FD-9BC5-7F8CBDFE41CC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5524176359176636;
createNode animCurveTU -n "Character1_RightFoot_scaleX";
	rename -uid "957E2971-4CF2-D09D-D6E9-54874AE03C5E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFoot_scaleY";
	rename -uid "C5D38369-44EB-2CF9-9876-CBB555A18EBB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFoot_scaleZ";
	rename -uid "4FEAA48A-4E54-5145-B008-71B4924A9BEB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightFoot_rotateX";
	rename -uid "C7CA1B2F-4536-53DC-74D7-0C9856339853";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -1.9345545768737795 1 -10.816319465637207
		 2 -14.117167472839355 3 -13.452543258666992 4 -10.259594917297363 5 -6.5956430435180664
		 6 -5.1613245010375977 7 -0.70016860961914063 8 6.4613180160522461 9 8.0103054046630859
		 10 -5.6488189697265625 11 -65.446525573730469 12 -65.167549133300781 13 -24.975147247314453
		 14 -25.167280197143555 15 -29.205514907836911 16 -26.143444061279297 17 -22.654024124145508
		 18 -26.755298614501953 19 -31.442760467529293 20 -29.34780120849609 21 -24.247711181640625
		 22 -21.445367813110352 23 -18.69740104675293 24 -16.13673210144043 25 -14.398591041564941
		 26 -14.252702713012695 27 -14.861698150634766 28 -15.738686561584474 29 -16.50373649597168
		 30 -16.928226470947266 31 -16.989500045776367 32 -16.865217208862305 33 -16.595876693725586
		 34 -16.223152160644531 35 -15.819038391113281 36 -15.188333511352539 37 -14.106791496276855
		 38 -12.639965057373047 39 -11.004863739013672 40 -9.5583105087280273 41 -9.0848474502563477
		 42 -9.7519340515136719 43 -10.506610870361328 44 -11.296509742736816 45 -12.1484375
		 46 -13.358256340026855 47 -14.841336250305178 48 -16.163372039794922 49 -17.058294296264648
		 50 -17.339128494262695 51 -17.203683853149414 52 -17.041482925415039 53 -16.935609817504883
		 54 -16.962673187255859 55 -17.188177108764648 56 -17.784320831298828 57 -18.826627731323242
		 58 -20.208408355712891 59 -21.803192138671875 60 -23.510213851928711 61 -25.924432754516602
		 62 -29.177421569824219 63 -32.820571899414063 64 -36.789249420166016 65 -41.147285461425781
		 66 -37.835227966308594 67 -21.562088012695313 68 9.5068092346191406 69 15.258024215698244
		 70 18.121883392333984 71 20.194089889526367 72 8.7829551696777344 73 9.8167409896850586
		 74 18.213525772094727 75 7.9184308052062988 76 -1.4232164621353149 77 -4.9384684562683105
		 78 -5.3539237976074219 79 -3.8516614437103271 80 -1.9345545768737795;
createNode animCurveTA -n "Character1_RightFoot_rotateY";
	rename -uid "A3A47A49-459F-9815-5D04-B09EEC0BB552";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 0.77543109655380249 1 3.771453857421875
		 2 4.903782844543457 3 4.812098503112793 4 3.5411267280578613 5 1.6213212013244629
		 6 0.55678844451904297 7 -2.7634711265563965 8 -7.7721147537231445 9 -8.2980823516845703
		 10 0.88464957475662231 11 35.442951202392578 12 31.770057678222656 13 10.679963111877441
		 14 14.079327583312988 15 15.718976020812988 16 11.372025489807129 17 8.4870939254760742
		 18 10.278014183044434 19 13.330997467041016 20 14.484288215637207 21 13.140762329101563
		 22 12.018203735351563 23 10.180206298828125 24 8.0908136367797852 25 6.6910171508789063
		 26 6.5972151756286621 27 7.0617241859436035 28 7.7427105903625488 29 8.3553352355957031
		 30 8.7099637985229492 31 8.7676210403442383 32 8.6665029525756836 33 8.4477682113647461
		 34 8.1516199111938477 35 7.8404130935668936 36 7.3587808609008789 37 6.5410346984863281
		 38 5.4657917022705078 39 4.3199419975280762 40 3.3531546592712402 41 3.0398492813110352
		 42 3.47149658203125 43 3.9849977493286137 44 4.5497841835021973 45 5.1836962699890137
		 46 6.1074142456054687 47 7.2652096748352051 48 8.3116416931152344 49 9.0269098281860352
		 50 9.2683181762695313 51 9.1887235641479492 52 9.0868368148803711 53 9.026641845703125
		 54 9.0673732757568359 55 9.2597646713256836 56 9.7506847381591797 57 10.527100563049316
		 58 11.344963073730469 59 11.970114707946777 60 12.277137756347656 61 12.622396469116211
		 62 13.030745506286621 63 13.360411643981934 64 13.876194000244141 65 15.062616348266602
		 66 15.643900871276854 67 10.264676094055176 68 -6.1430678367614746 69 -6.7273626327514648
		 70 -12.638022422790527 71 -17.979415893554687 72 -4.7086172103881836 73 -1.9171620607376096
		 74 -6.9449748992919922 75 -3.7658193111419682 76 0.28392207622528076 77 1.8073649406433103
		 78 2.0883963108062744 79 1.5757308006286621 80 0.77543109655380249;
createNode animCurveTA -n "Character1_RightFoot_rotateZ";
	rename -uid "1F56F36F-404A-7A22-5D04-D7B8CA7313B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -0.61463683843612671 1 -1.5994683504104614
		 2 1.5085766315460205 3 6.7137899398803711 4 12.783425331115723 5 18.781234741210937
		 6 24.226041793823242 7 27.867767333984375 8 27.906106948852539 9 25.494882583618164
		 10 23.873910903930664 11 17.063604354858398 12 16.669580459594727 13 14.350238800048828
		 14 28.397344589233398 15 19.972164154052734 16 6.3475022315979004 17 -0.43676739931106567
		 18 0.24112626910209656 19 4.9331345558166504 20 13.914772987365723 21 21.549823760986328
		 22 26.647764205932617 23 29.347427368164062 24 29.703361511230469 25 29.177120208740234
		 26 29.32475471496582 27 29.990066528320316 28 30.858112335205082 29 31.642805099487305
		 30 32.162239074707031 31 32.308479309082031 32 32.205440521240234 33 31.961971282958988
		 34 31.670587539672848 35 31.443599700927731 36 31.068361282348629 37 30.286420822143555
		 38 29.145429611206051 39 27.799161911010742 40 26.65692138671875 41 26.6383056640625
		 42 27.896646499633789 43 29.282472610473633 44 30.600570678710938 45 31.735174179077148
		 46 32.800090789794922 47 33.713405609130859 48 34.308052062988281 49 34.654796600341797
		 50 34.924224853515625 51 35.195255279541016 52 35.460880279541016 53 35.7083740234375
		 54 35.918346405029297 55 36.068267822265625 56 35.678234100341797 57 34.093360900878906
		 58 31.077142715454102 59 26.656621932983398 60 21.173711776733398 61 15.194298744201662
		 62 9.1835117340087891 63 3.7611377239227295 64 -0.17956146597862244 65 -1.6836071014404297
		 66 4.9691634178161621 67 18.621543884277344 68 17.932773590087891 69 13.577581405639648
		 70 20.427885055541992 71 26.46916389465332 72 14.157727241516113 73 -0.72316592931747437
		 74 2.1534910202026367 75 3.9249937534332275 76 1.8363481760025024 77 0.18341174721717834
		 78 -0.90289175510406483 79 -1.1136933565139771 80 -0.61463683843612671;
createNode animCurveTU -n "Character1_RightFoot_visibility";
	rename -uid "3327853D-4851-38A1-81BA-FEAAF3ADA051";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightToeBase_translateX";
	rename -uid "BF880D01-45F9-AC8E-1817-9BB89132EBFA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2767767906188965;
createNode animCurveTL -n "Character1_RightToeBase_translateY";
	rename -uid "A0B616AB-4513-7B66-96E2-22A087EE54BD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.0139193534851074;
createNode animCurveTL -n "Character1_RightToeBase_translateZ";
	rename -uid "8DE7F096-44E9-6F8B-FBF9-5EAE7662A49C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8259720802307129;
createNode animCurveTU -n "Character1_RightToeBase_scaleX";
	rename -uid "84DAF009-4CE2-1DFA-38F9-07B2E3922C82";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightToeBase_scaleY";
	rename -uid "0B73BE2D-4732-81CB-C0D4-64926043B67B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightToeBase_scaleZ";
	rename -uid "1335CB6F-41EC-DB80-1267-BC9F68D7105C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightToeBase_rotateX";
	rename -uid "55DD774C-420D-42A6-928F-719B6C3FA4F5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  10 5.6701243700274517e-009 11 5.4491633427744546e-009
		 12 -3.5964641571044922 13 -7.5767512321472177 14 5.0108242000135306e-009 15 5.1389470456797426e-009
		 66 1.9256418681834475e-008 67 1.9230691705729441e-008 68 -2.1513400077819824 69 -2.9827930927276611
		 70 -3.4702386856079102 71 -2.2073874473571777 72 -3.1543271541595459 73 -5.820948600769043
		 74 -4.5615038871765137 75 -2.1550107002258301 76 -1.4817236661911011 77 -1.0209474563598633
		 78 -0.68011587858200073 79 -0.36943644285202026 80 -3.5576384060931332e-009;
createNode animCurveTA -n "Character1_RightToeBase_rotateY";
	rename -uid "C4180B4D-4DBC-D235-19DC-39B237D03A70";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  10 -1.2652571435012305e-008 11 -1.2055264342336613e-008
		 12 12.702248573303223 13 25.350915908813477 14 -1.0079724610534413e-008 15 -1.0632337676952375e-008
		 66 -7.1050806127459509e-008 67 -7.1118471112185944e-008 68 14.774081230163574 69 27.815753936767578
		 70 5.5711250305175781 71 -15.696847915649414 72 0.35510769486427307 73 20.023153305053711
		 74 15.94827079772949 75 7.6950716972351074 76 5.3083748817443848 77 3.6633291244506836
		 78 2.442291259765625 79 1.3272322416305542 80 -7.3233380470938414e-010;
createNode animCurveTA -n "Character1_RightToeBase_rotateZ";
	rename -uid "3D459F81-4854-CF47-EA83-81ACDE00B8D2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  10 -4.6285023036496398e-010 11 -8.8998919345328886e-010
		 12 -0.40043190121650696 13 -1.706446647644043 14 -1.6356702658981703e-009 15 -1.8099455267872597e-009
		 66 -3.4660572190148287e-009 67 -3.2536311422859399e-009 68 3.0662510395050049 69 6.204432487487793
		 70 5.5178666114807129 71 2.1457357406616211 72 0.85826259851455688 73 -1.0284605026245117
		 74 -0.63930916786193848 75 -0.14494867622852325 76 -0.068692862987518311 77 -0.032650277018547058
		 78 -0.014497718773782253 79 -0.0042791436426341534 80 -2.1209846379210975e-010;
createNode animCurveTU -n "Character1_RightToeBase_visibility";
	rename -uid "4A106A43-4003-681B-75B3-1493CEEF891F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateX";
	rename -uid "48545379-4D34-5FA5-D206-41848307F6F7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.0018006421159952879;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateY";
	rename -uid "F6820042-406B-991A-99F6-C39D89188C09";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.0064689838327467442;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateZ";
	rename -uid "256D32E2-40D4-33FF-FB87-5B950741C6D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.6805553436279297;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleX";
	rename -uid "EA20E841-4720-1F76-48A6-E398764C3504";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleY";
	rename -uid "8D350072-4EC4-68B6-B853-8DB89056B4E2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleZ";
	rename -uid "0DAFBA30-476B-D16E-1C14-A68A97176534";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateX";
	rename -uid "B1CDC42B-4B54-A8B6-8A07-50887861B19B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.3304652065407936e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateY";
	rename -uid "99C78DBE-44A7-BCD1-DFED-489A0AC81F88";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.3611505518015292e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateZ";
	rename -uid "8F0231EB-4A71-C247-A764-E687322ACFC8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5465596858721398e-010;
createNode animCurveTU -n "Character1_RightFootMiddle1_visibility";
	rename -uid "0DA28699-46A2-1BB7-0C57-6AA27124EDA0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateX";
	rename -uid "57907D24-41D4-4DF6-9258-09AD5982D3BB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.5860931600618642e-006;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateY";
	rename -uid "17E90781-4476-C0D7-6339-549893115A9D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.3307760456154938e-007;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateZ";
	rename -uid "CA0469D6-4BCC-15F2-00E0-1389620BF827";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.0234856605529785;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleX";
	rename -uid "61FC2009-4FC6-A879-A3AC-F2ACEEBB1381";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleY";
	rename -uid "4BECE7F1-4FBE-E4C8-6E6E-38B21A68A85F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleZ";
	rename -uid "1FF2CB23-4D46-A327-7C33-A383304D51F4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateX";
	rename -uid "0C230F97-4A80-E705-9FF7-B3AB9E7AFB4A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.982552148646846e-009;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateY";
	rename -uid "FEA515F7-4C45-404D-4CBF-C3B4B59EF2C7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7966389265211546e-008;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateZ";
	rename -uid "43215DE1-42DB-B9F1-63A0-0581389BB896";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.5682441456554841e-010;
createNode animCurveTU -n "Character1_RightFootMiddle2_visibility";
	rename -uid "34263201-4700-A950-9A0D-AFBAE9F37A85";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_Spine_translateX";
	rename -uid "A440C3AF-4D9D-0960-7E01-AEB4C69C2A95";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6075620651245117;
createNode animCurveTL -n "Character1_Spine_translateY";
	rename -uid "1D76A76D-4536-F494-D9BB-62BD1D8FF28A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.353469848632812;
createNode animCurveTL -n "Character1_Spine_translateZ";
	rename -uid "10122EFE-4EA7-D6A1-FFFD-6A99A1638632";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2061522006988525;
createNode animCurveTU -n "Character1_Spine_scaleX";
	rename -uid "F7558A1A-4AAD-F8BB-8F52-468FC23BDEDA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine_scaleY";
	rename -uid "3DD87197-4D0B-71AB-14F7-3190B2813D2A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine_scaleZ";
	rename -uid "470831A7-4BFD-BB50-23A3-01811C398022";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_Spine_rotateX";
	rename -uid "F5041182-4056-A78F-E9E5-D88A288E51A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -4.2090029716491699 1 -5.3977837562561035
		 2 -6.5645303726196289 3 -7.5890049934387216 4 -8.4005918502807617 5 -8.9702720642089844
		 6 -9.3140087127685547 7 -10.147727012634277 8 -11.309015274047852 9 -11.46876335144043
		 10 -9.3661012649536133 11 -4.3638238906860352 12 2.2503311634063721 13 8.4920005798339844
		 14 13.142166137695313 15 16.346113204956055 16 18.347682952880859 17 19.187013626098633
		 18 19.603775024414063 19 19.911285400390625 20 20.065534591674805 21 20.120296478271484
		 22 20.132692337036133 23 20.051599502563477 24 19.793758392333984 25 19.312942504882813
		 26 18.475698471069336 27 17.423923492431641 28 16.524459838867187 29 16.070110321044922
		 30 16.045093536376953 31 16.230493545532227 32 16.543523788452148 33 16.897436141967773
		 34 17.218700408935547 35 17.427593231201172 36 17.585948944091797 37 17.795312881469727
		 38 18.026288986206055 39 18.262475967407227 40 18.480546951293945 41 18.650444030761719
		 42 18.743917465209961 43 18.733058929443359 44 18.591108322143555 45 18.294332504272461
		 46 17.815242767333984 47 17.169382095336914 48 16.376296997070313 49 15.467581748962402
		 50 14.470027923583983 51 13.402735710144043 52 12.305171012878418 53 11.20904541015625
		 54 10.144988059997559 55 9.1502189636230469 56 8.1072196960449219 57 6.9916191101074219
		 58 5.9710903167724609 59 5.220947265625 60 4.9588079452514648 61 5.636563777923584
		 62 7.1718039512634268 63 8.9515695571899414 64 10.335383415222168 65 10.654360771179199
		 66 9.8010530471801758 67 7.9992952346801758 68 5.5234928131103516 69 2.8136677742004395
		 70 0.32764986157417297 71 -1.5055272579193115 72 -2.6465516090393066 73 -3.3407196998596191
		 74 -3.9008705615997314 75 -4.2803750038146973 76 -4.4277348518371582 77 -4.4139742851257324
		 78 -4.316530704498291 79 -4.2184290885925293 80 -4.2090029716491699;
createNode animCurveTA -n "Character1_Spine_rotateY";
	rename -uid "D8B80641-429E-7AC6-286A-48922870B634";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -12.116277694702148 1 -10.435529708862305
		 2 -8.6336708068847656 3 -6.8591518402099609 4 -5.2473845481872559 5 -3.9420301914215088
		 6 -3.0868344306945801 7 -3.2208523750305176 8 -4.0407686233520508 9 -4.4960064888000488
		 10 -3.428196907043457 11 0.52886730432510376 12 6.7263145446777344 13 14.468691825866699
		 14 21.836938858032227 15 27.983154296875 16 33.275825500488281 17 37.052719116210938
		 18 39.179904937744141 19 40.248439788818359 20 40.654186248779297 21 40.742202758789063
		 22 40.502960205078125 23 39.782543182373047 24 38.771389007568359 25 37.633842468261719
		 26 36.126125335693359 27 34.246578216552734 28 32.515979766845703 29 31.475883483886722
		 30 31.133981704711914 31 31.094997406005859 32 31.289165496826172 33 31.645843505859375
		 34 32.098716735839844 35 32.578639984130859 36 33.092647552490234 37 33.687854766845703
		 38 34.347175598144531 39 35.046638488769531 40 35.7730712890625 41 36.491355895996094
		 42 37.181491851806641 43 37.839454650878906 44 38.449573516845703 45 39.001457214355469
		 46 39.632625579833984 47 40.432697296142578 48 41.319061279296875 49 42.215862274169922
		 50 43.04180908203125 51 43.700057983398438 52 44.107692718505859 53 44.182548522949219
		 54 43.838283538818359 55 42.990837097167969 56 40.615715026855469 57 36.414863586425781
		 58 31.381519317626953 59 26.460187911987305 60 22.529956817626953 61 19.556465148925781
		 62 16.885190963745117 63 14.474822998046875 64 12.327777862548828 65 10.472674369812012
		 66 8.9403791427612305 67 7.8154587745666504 68 6.9778661727905273 69 6.2416529655456543
		 70 5.4690618515014648 71 4.507237434387207 72 3.1834549903869629 73 1.3885552883148193
		 74 -0.33712098002433777 75 -2.0052733421325684 76 -3.893982887268066 77 -5.9100532531738281
		 78 -7.9924426078796387 79 -10.080016136169434 80 -12.116277694702148;
createNode animCurveTA -n "Character1_Spine_rotateZ";
	rename -uid "EEE33B24-4990-2987-608C-F1AA250A7DCD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 5.6667900085449219 1 9.0041589736938477
		 2 12.714405059814453 3 16.455490112304687 4 19.953472137451172 5 23.019199371337891
		 6 25.523439407348633 7 27.215414047241211 8 27.830488204956055 9 27.323520660400391
		 10 25.68812370300293 11 22.60151481628418 12 19.050182342529297 13 16.33477783203125
		 14 12.348316192626953 15 8.5132036209106445 16 4.5076427459716797 17 1.7484269142150879
		 18 0.87320327758789063 19 0.99912232160568237 20 1.5383895635604858 21 2.1255683898925781
		 22 2.9032697677612305 23 3.9543402194976807 24 4.9127702713012695 25 5.4085078239440918
		 26 5.1706347465515137 27 4.4931235313415527 28 3.7616524696350102 29 3.28525710105896
		 30 3.1036784648895264 31 2.9874277114868164 32 2.9250211715698242 33 2.9278154373168945
		 34 2.9699075222015381 35 3.0436608791351318 36 3.1592733860015869 37 3.3227319717407227
		 38 3.5364305973052979 39 3.7418799400329585 40 3.9369356632232666 41 4.1144089698791504
		 42 4.2796316146850586 43 4.4566388130187988 44 4.6385593414306641 45 4.8159399032592773
		 46 4.9865260124206543 47 5.147606372833252 48 5.3451061248779297 49 5.5788125991821289
		 50 5.8568973541259766 51 6.1392407417297363 52 6.3759927749633789 53 6.5525326728820801
		 54 6.6552729606628418 55 6.6818628311157227 56 6.7071242332458496 57 6.8309106826782227
		 58 7.058924674987793 59 7.2676000595092773 60 7.1887540817260742 61 6.2860431671142578
		 62 4.6708312034606934 63 2.9570231437683105 64 1.7984161376953125 65 1.8789443969726562
		 66 3.4084835052490234 67 6.1405143737792969 68 9.2581787109375 69 11.319082260131836
		 70 12.865213394165039 71 13.486638069152832 72 13.942652702331543 73 14.312838554382324
		 74 13.561654090881348 75 12.111756324768066 76 10.670685768127441 77 9.2413129806518555
		 78 7.8784880638122567 79 6.6602053642272949 80 5.6667900085449219;
createNode animCurveTU -n "Character1_Spine_visibility";
	rename -uid "F0ACE466-4304-C066-D5FC-74858DBF3222";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_Spine1_translateX";
	rename -uid "B2225D10-4EAA-5997-8B8B-5C998B0F3B5E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.930038452148437;
createNode animCurveTL -n "Character1_Spine1_translateY";
	rename -uid "44600954-48B2-3681-4F82-B3BF728DAF9D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1661763191223145;
createNode animCurveTL -n "Character1_Spine1_translateZ";
	rename -uid "C2E98E87-42E0-2188-A1D5-B584EC34EAB9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.40121376514434814;
createNode animCurveTU -n "Character1_Spine1_scaleX";
	rename -uid "66A9237F-43BF-1876-1A8D-0E8C51E75A15";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_Spine1_scaleY";
	rename -uid "2A702BF7-45EB-8563-EE30-93B7AB264182";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine1_scaleZ";
	rename -uid "0AFE9DFE-46E3-D676-A2DC-90A05904126C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_Spine1_rotateX";
	rename -uid "9DE99794-429C-3B77-BC4D-E3B0FF9B3CA8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -26.060359954833984 1 -27.631807327270508
		 2 -29.198562622070312 3 -30.78668212890625 4 -32.414878845214844 5 -34.11090087890625
		 6 -35.923248291015625 7 -38.759605407714844 8 -42.296634674072266 9 -44.641426086425781
		 10 -43.817436218261719 11 -38.37298583984375 12 -30.329244613647457 13 -22.041191101074219
		 14 -15.681429862976074 15 -11.92780590057373 16 -9.3381156921386719 17 -6.3791031837463379
		 18 -2.004291296005249 19 3.1751408576965332 20 7.9898624420166016 21 11.095715522766113
		 22 11.548640251159668 23 10.20794677734375 24 8.3775749206542969 25 7.4286918640136719
		 26 7.9547095298767081 27 9.1711797714233398 28 10.4237060546875 29 11.087890625 30 11.170161247253418
		 31 11.136309623718262 32 11.042703628540039 33 10.893807411193848 34 10.64389705657959
		 35 10.21910572052002 36 9.6163253784179687 37 8.8988027572631836 38 8.0937528610229492
		 39 7.2207016944885254 40 6.3117036819458008 41 5.3828654289245605 42 4.4568428993225098
		 43 3.5643978118896484 44 2.7216217517852783 45 1.9453278779983521 46 1.2480087280273437
		 47 0.62871348857879639 48 0.080793648958206177 49 -0.41024231910705566 50 -0.86959415674209595
		 51 -1.3543686866760254 52 -1.9138485193252563 53 -2.6002249717712402 54 -3.4701008796691895
		 55 -4.569427490234375 56 -6.4521961212158203 57 -9.1837615966796875 58 -12.135135650634766
		 59 -14.674263000488281 60 -15.949023246765137 61 -15.058873176574705 62 -12.49505615234375
		 63 -9.4767894744873047 64 -7.1061081886291504 65 -6.208892822265625 66 -6.5732636451721191
		 67 -7.7331647872924796 68 -9.4991111755371094 69 -11.682384490966797 70 -13.827712059020996
		 71 -15.586434364318846 72 -17.021516799926758 73 -18.498941421508789 74 -19.849143981933594
		 75 -21.020181655883789 76 -22.141532897949219 77 -23.199819564819336 78 -24.199800491333008
		 79 -25.148828506469727 80 -26.060359954833984;
createNode animCurveTA -n "Character1_Spine1_rotateY";
	rename -uid "44913C25-43D0-5ECB-63C2-228A3D5C3E38";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -4.7502837181091309 1 -6.1343755722045898
		 2 -7.5651311874389657 3 -8.9984302520751953 4 -10.397481918334961 5 -11.756903648376465
		 6 -13.119107246398926 7 -14.840559959411621 8 -16.670364379882813 9 -17.907459259033203
		 10 -17.926498413085938 11 -15.693921089172365 12 -11.519603729248047 13 -6.4206643104553223
		 14 -1.8528512716293337 15 0.27090847492218018 16 1.0091354846954346 17 2.3766517639160156
		 18 5.5244193077087402 19 9.3969917297363281 20 12.87448787689209 21 14.861558914184572
		 22 14.708109855651854 23 13.121973037719727 24 11.13798713684082 25 9.6875934600830078
		 26 9.1002101898193359 27 8.8707199096679687 28 8.6289129257202148 29 7.9578685760498038
		 30 6.5772562026977539 31 4.723264217376709 32 2.6738080978393555 33 0.7231401801109314
		 34 -0.7974277138710022 35 -1.5523768663406372 36 -1.6458244323730469 37 -1.432618260383606
		 38 -0.9654880166053772 39 -0.28839901089668274 40 0.55088376998901367 41 1.4776670932769775
		 42 2.4305410385131836 43 3.3602097034454346 44 4.2096638679504395 45 4.9282097816467285
		 46 5.7557611465454102 47 6.8837246894836426 48 8.1667766571044922 49 9.4761571884155273
		 50 10.677979469299316 51 11.636303901672363 52 12.242390632629395 53 12.377742767333984
		 54 11.919843673706055 55 10.744474411010742 56 7.1204304695129395 57 0.58655732870101929
		 58 -7.0824618339538574 59 -14.179186820983887 60 -19.140571594238281 61 -21.813182830810547
		 62 -23.244770050048828 63 -23.712890625 64 -23.642431259155273 65 -23.602666854858398
		 66 -23.844701766967773 67 -24.021265029907227 68 -24.012933731079102 69 -23.621809005737305
		 70 -22.823770523071289 71 -21.590295791625977 72 -20.151880264282227 73 -18.6707763671875
		 74 -16.852396011352539 75 -14.745114326477051 76 -12.644563674926758 77 -10.562322616577148
		 78 -8.5302114486694336 79 -6.5814132690429687 80 -4.7502837181091309;
createNode animCurveTA -n "Character1_Spine1_rotateZ";
	rename -uid "50B32F99-448B-9ADB-42E8-7BAFD74FC8D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 19.853261947631836 1 19.689083099365234
		 2 19.658931732177734 3 19.693096160888672 4 19.735145568847656 5 19.764331817626953
		 6 19.821941375732422 7 20.221899032592773 8 20.857734680175781 9 21.137838363647461
		 10 20.416139602661133 11 18.377925872802734 12 15.875429153442385 13 13.729371070861816
		 14 11.196792602539063 15 7.8732700347900391 16 3.9499590396881104 17 1.375754714012146
		 18 0.88310003280639648 19 1.5147272348403931 20 2.6670188903808594 21 3.6214094161987309
		 22 4.2299132347106934 23 4.7835359573364258 24 5.1766281127929687 25 5.220278263092041
		 26 4.7703971862792969 27 3.9306199550628667 28 2.814000129699707 29 1.554352879524231
		 30 0.049215115606784821 31 -1.742084264755249 32 -3.5535516738891602 33 -5.138606071472168
		 34 -6.2864198684692383 35 -6.79132080078125 36 -6.7715640068054199 37 -6.5129632949829102
		 38 -6.0542817115783691 39 -5.4571495056152344 40 -4.7605652809143066 41 -4.0034327507019043
		 42 -3.2207188606262207 43 -2.4398288726806641 44 -1.6997075080871582 45 -1.0391788482666016
		 46 -0.36464807391166687 47 0.41716590523719788 48 1.2902791500091553 49 2.2201354503631592
		 50 3.1703963279724121 51 4.0683407783508301 52 4.8391404151916504 53 5.4211673736572266
		 54 5.7552757263183594 55 5.8025503158569336 56 5.3805890083312988 57 4.7705087661743164
		 58 4.4419851303100586 59 4.2983160018920898 60 3.595494270324707 61 0.89281624555587769
		 62 -3.6952366828918453 63 -8.7542953491210937 64 -12.791220664978027 65 -14.380190849304201
		 66 -13.741446495056152 67 -11.600348472595215 68 -8.5082054138183594 69 -5.2847628593444824
		 70 -2.079702615737915 71 0.6395987868309021 72 3.1466636657714844 73 5.6398282051086426
		 74 7.826000690460206 75 9.8094644546508789 76 11.80595588684082 77 13.80727481842041
		 78 15.813100814819338 79 17.827848434448242 80 19.853261947631836;
createNode animCurveTU -n "Character1_Spine1_visibility";
	rename -uid "4CD50E6B-4309-6901-0591-07BD1B882FE9";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftShoulder_translateX";
	rename -uid "BE34E294-4A2D-6E10-A585-44959B75D269";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.0842323303222656;
createNode animCurveTL -n "Character1_LeftShoulder_translateY";
	rename -uid "0534A62F-46A5-FE6B-0A0E-63AD57D3AA06";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.604855537414551;
createNode animCurveTL -n "Character1_LeftShoulder_translateZ";
	rename -uid "B8D11CA0-4BC3-2503-A9A0-AD963F66FE75";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6404602527618408;
createNode animCurveTU -n "Character1_LeftShoulder_scaleX";
	rename -uid "743B7AE6-49E0-DD74-05AB-4F81FF5FEFDA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftShoulder_scaleY";
	rename -uid "59CDB426-449B-E17C-01D4-0C9DE17B394C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftShoulder_scaleZ";
	rename -uid "96A039A2-46B3-5861-DF4D-93936B237579";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftShoulder_rotateX";
	rename -uid "34FD31D3-42E6-A628-DCE4-7DB91C49422E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 74 ".ktv[0:73]"  0 5.2854747772216797 1 5.9054393768310547
		 2 6.0685749053955078 3 5.9241194725036621 4 5.6472258567810059 5 5.4499902725219727
		 6 5.6108245849609375 7 7.2629594802856445 8 10.89869213104248 9 15.748729705810547
		 10 19.928573608398438 11 22.023853302001953 12 22.352323532104492 13 21.234918594360352
		 14 19.729663848876953 15 17.888559341430664 16 15.385874748229982 17 13.237212181091309
		 18 11.743128776550293 19 10.467288017272949 20 9.5845212936401367 21 9.0946378707885742
		 22 8.7665729522705078 23 7.886934757232666 24 6.4276795387268066 25 5.1858968734741211
		 26 4.4880399703979492 27 3.9134421348571773 28 3.4714322090148926 29 3.1613860130310059
		 30 2.9734272956848145 31 2.8892202377319336 32 2.883033275604248 33 2.9232258796691895
		 34 2.9742624759674072 35 2.9992821216583252 36 2.9992821216583252 44 2.9992821216583252
		 45 2.9992821216583252 46 3.0768685340881348 47 3.2591228485107422 48 3.4683799743652344
		 49 3.6174163818359375 50 3.6080553531646729 51 3.4576094150543213 52 3.2187974452972412
		 53 2.8480451107025146 54 2.3171870708465576 55 1.6201720237731934 56 0.60492414236068726
		 57 -0.59705621004104614 58 -1.6511251926422119 59 -2.3952593803405762 60 -2.7895920276641846
		 61 -2.8828706741333008 62 -2.6756041049957275 63 -2.09969162940979 64 -1.2952100038528442
		 65 -0.6335335373878479 66 -0.28658828139305115 67 -0.079645581543445587 68 0.13985536992549896
		 69 0.48502522706985474 70 0.99787116050720215 71 1.6508496999740601 72 2.5200862884521484
		 73 3.5636663436889648 74 4.5603108406066895 75 5.2806944847106934 76 5.6490082740783691
		 77 5.7842679023742676 78 5.7323570251464844 79 5.5461950302124023 80 5.2854747772216797;
createNode animCurveTA -n "Character1_LeftShoulder_rotateY";
	rename -uid "2940E9AB-47BF-07F6-6307-21B8E20DC9AF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 74 ".ktv[0:73]"  0 -21.089040756225586 1 -24.9029541015625
		 2 -28.668119430541989 3 -32.403964996337891 4 -36.135143280029297 5 -39.897579193115234
		 6 -43.74365234375 7 -48.5164794921875 8 -53.999313354492188 9 -58.639118194580078
		 10 -60.908702850341797 11 -59.690589904785156 12 -56.022491455078125 13 -51.758975982666016
		 14 -48.788967132568359 15 -48.637348175048828 16 -49.647499084472656 17 -49.046932220458984
		 18 -46.217144012451172 19 -42.438396453857422 20 -37.927730560302734 21 -32.890613555908203
		 22 -26.467170715332031 23 -18.817689895629883 24 -11.751511573791504 25 -7.0481233596801758
		 26 -4.2132763862609863 27 -1.6316760778427124 28 0.6893424391746521 29 2.741234302520752
		 30 4.5138635635375977 31 5.9954876899719238 32 7.1729984283447266 33 8.0323038101196289
		 34 8.5587501525878906 35 8.7375106811523437 36 8.7375106811523437 44 8.7375106811523437
		 45 8.7375106811523437 46 8.8016033172607422 47 8.9779434204101562 48 9.2429676055908203
		 49 9.5741586685180664 50 9.949493408203125 51 10.505002975463867 52 11.252163887023926
		 53 11.981138229370117 54 12.484086990356445 55 12.557683944702148 56 11.842748641967773
		 57 10.491703033447266 58 9.0694799423217773 59 8.1088857650756836 60 8.0999526977539062
		 61 9.7851591110229492 62 12.860677719116211 63 16.211629867553711 64 18.644824981689453
		 65 18.912347793579102 66 17.095142364501953 67 13.855314254760742 68 9.7599544525146484
		 69 5.3684720993041992 70 1.219757080078125 71 -2.1726615428924561 72 -4.5902085304260254
		 73 -6.4191741943359375 74 -8.0601949691772461 75 -9.9169301986694336 76 -12.065505981445313
		 77 -14.288196563720703 78 -16.553823471069336 79 -18.830892562866211 80 -21.089040756225586;
createNode animCurveTA -n "Character1_LeftShoulder_rotateZ";
	rename -uid "6243671D-4EC4-F5E6-5860-0D89B40BCBC7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 74 ".ktv[0:73]"  0 -3.372546911239624 1 -2.0318021774291992
		 2 -0.25243377685546875 3 1.6907494068145752 4 3.5069549083709717 5 4.878028392791748
		 6 5.4357595443725586 7 3.8415641784667969 8 -0.23433610796928406 9 -5.6582603454589844
		 10 -10.397597312927246 11 -13.329965591430664 12 -14.891206741333008 13 -15.042259216308592
		 14 -14.285384178161621 15 -11.954896926879883 16 -8.1351594924926758 17 -4.8403868675231934
		 18 -2.534351110458374 19 -0.46215745806694036 20 0.89446783065795898 21 1.1650198698043823
		 22 -0.19472759962081909 23 -2.4761288166046143 24 -4.5717458724975586 25 -5.5608925819396973
		 26 -5.5209660530090332 27 -5.2464232444763184 28 -4.7949700355529785 29 -4.2216544151306152
		 30 -3.580723762512207 31 -2.926922082901001 32 -2.3162789344787598 33 -1.8064111471176145
		 34 -1.4563966989517212 35 -1.3262996673583984 36 -1.3262996673583984 44 -1.3262996673583984
		 45 -1.3262996673583984 46 -1.0166544914245605 47 -0.29254588484764099 48 0.53890484571456909
		 49 1.1702723503112793 50 1.292965292930603 51 0.91810953617095936 52 0.24697676301002502
		 53 -0.7440827488899231 54 -2.074108362197876 55 -3.7547316551208496 56 -6.2873892784118652
		 57 -9.6440286636352539 58 -13.055517196655273 59 -15.773430824279787 60 -17.056230545043945
		 61 -16.211488723754883 62 -13.760688781738281 63 -10.745345115661621 64 -8.1798057556152344
		 65 -7.1187796592712402 66 -7.608191967010498 67 -9.0682945251464844 68 -11.099519729614258
		 69 -13.305655479431152 70 -15.272237777709961 71 -16.540611267089844 72 -16.794677734375
		 73 -16.219570159912109 74 -15.087898254394531 75 -13.690060615539551 76 -12.04470157623291
		 77 -10.058526039123535 78 -7.8624258041381836 79 -5.5886898040771484 80 -3.372546911239624;
createNode animCurveTU -n "Character1_LeftShoulder_visibility";
	rename -uid "A3BF9533-4FB9-0442-39CF-D090102BB17B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftArm_translateX";
	rename -uid "751C5E15-4386-056F-B320-73906F8451DE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 9.2092180252075195;
createNode animCurveTL -n "Character1_LeftArm_translateY";
	rename -uid "1F2BC4F9-4F15-62D2-7B2E-62A67B17B972";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.4172050952911377;
createNode animCurveTL -n "Character1_LeftArm_translateZ";
	rename -uid "55A41BFC-4BAC-AD8A-9666-3398C6297526";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.69030004739761353;
createNode animCurveTU -n "Character1_LeftArm_scaleX";
	rename -uid "0CF526FD-4BCB-C84C-DD64-12A4326F17E3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleY";
	rename -uid "3E0FE7FE-44AD-0FE6-8EF3-DEAA7780A592";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleZ";
	rename -uid "3C646675-46E3-6156-C6EB-30B4B07C05B4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_LeftArm_rotateX";
	rename -uid "A1AB8BFD-4381-82DE-FD91-6381022E6E66";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 59.225181579589844 1 52.255847930908203
		 2 40.817115783691406 3 20.038684844970703 4 -19.173191070556641 5 -64.702171325683594
		 6 -93.115211486816406 7 -114.51926422119141 8 -130.00479125976562 9 -141.88681030273437
		 10 -154.71078491210937 11 -204.67379760742187 12 -137.648681640625 13 -149.60630798339844
		 14 -150.47659301757812 15 -140.13282775878906 16 -121.26846313476561 17 -105.15401458740234
		 18 -105.39601898193359 19 -115.63707733154298 20 -129.25772094726562 21 -139.0859375
		 22 -142.64468383789063 23 -144.00680541992187 24 -146.35249328613281 25 -148.98130798339844
		 26 -150.80218505859375 27 -152.556884765625 28 -154.62286376953125 29 -157.36380004882812
		 30 -161.00425720214844 31 -165.52421569824219 32 -170.61813354492187 33 -175.74758911132812
		 34 -180.27153015136719 35 -183.59022521972656 36 -185.44622802734375 37 -186.06198120117187
		 38 -185.69294738769531 39 -184.61888122558594 40 -183.12091064453125 41 -181.46598815917969
		 42 -179.89498901367187 43 -178.61509704589844 44 -177.79937744140625 45 -177.59585571289062
		 46 -178.50874328613281 47 -180.65858459472656 48 -183.54550170898437 49 -186.36866760253906
		 50 -187.84158325195312 51 -189.83660888671875 52 -193.23011779785156 53 -194.42707824707031
		 54 -189.64930725097656 55 -176.53105163574219 56 -156.85542297363281 57 -140.2147216796875
		 58 -129.92057800292969 59 -123.81959533691405 60 -120.44554138183594 61 -120.02194213867186
		 62 -123.52504730224608 63 -130.27674865722656 64 -136.70036315917969 65 -138.01324462890625
		 66 -132.24311828613281 67 -120.67581176757812 68 -107.96507263183594 69 -99.287429809570313
		 70 -95.582962036132813 71 -94.5316162109375 72 -97.51202392578125 73 -104.81989288330078
		 74 -111.54454040527344 75 -114.10501098632812 76 -114.12675476074219 77 -114.48426055908203
		 78 -116.29286193847656 79 -119.17051696777345 80 -120.77481842041016;
createNode animCurveTA -n "Character1_LeftArm_rotateY";
	rename -uid "FF53EBDD-40BE-9270-E916-11A37F6D9C5A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -69.777976989746094 1 -74.027214050292969
		 2 -78.310646057128906 3 -82.110153198242188 4 -84.304634094238281 5 -83.617424011230469
		 6 -81.283432006835938 7 -77.086700439453125 8 -71.814598083496094 9 -69.237007141113281
		 10 -73.042182922363281 11 -84.486656188964844 12 -107.98779296875 13 -130.58155822753906
		 14 -152.83021545410156 15 -179.21531677246094 16 -204.72280883789062 17 -215.50263977050781
		 18 -219.63433837890625 19 -223.22476196289062 20 -225.90927124023437 21 -227.95639038085937
		 22 -230.79292297363281 23 -233.83447265625 24 -236.45361328125 25 -238.14837646484375
		 26 -239.29930114746094 27 -240.33557128906247 28 -241.2015380859375 29 -241.84756469726562
		 30 -242.2392578125 31 -242.35733032226562 32 -242.20591735839844 33 -241.8355712890625
		 34 -241.36444091796875 35 -240.97257995605472 36 -240.69529724121091 37 -240.4729309082031
		 38 -240.33161926269531 39 -240.26828002929687 40 -240.27061462402347 41 -240.33154296875
		 42 -240.45704650878909 43 -240.66841125488281 44 -240.99990844726565 45 -241.49401855468753
		 46 -242.6197509765625 47 -244.48626708984372 48 -246.6046447753906 49 -248.52639770507812
		 50 -249.86326599121094 51 -250.81344604492185 52 -251.60302734374997 53 -252.03767395019531
		 54 -251.93597412109375 55 -250.43414306640622 56 -245.79391479492187 57 -236.9880676269531
		 58 -225.41818237304688 59 -212.96212768554687 60 -201.05557250976562 61 -188.75790405273437
		 62 -175.30155944824219 63 -162.30978393554687 64 -151.2022705078125 65 -143.17486572265625
		 66 -137.44290161132812 67 -133.39213562011719 68 -131.38185119628906 69 -130.6551513671875
		 70 -129.82118225097656 71 -128.67236328125 72 -127.75228881835937 73 -127.27352905273436
		 74 -126.85453033447264 75 -125.43505859375 76 -122.46291351318359 77 -118.68550109863281
		 78 -114.73880767822266 79 -111.54055023193359 80 -110.22203063964844;
createNode animCurveTA -n "Character1_LeftArm_rotateZ";
	rename -uid "82164BCD-41B8-8831-79BA-96BF503013CC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -77.858016967773438 1 -70.658859252929688
		 2 -59.745826721191406 3 -40.215030670166016 4 -2.9061439037322998 5 40.155422210693359
		 6 65.643043518066406 7 83.743743896484375 8 95.672592163085938 9 104.06826019287109
		 10 113.50138854980469 11 157.52763366699219 12 81.948387145996094 13 85.863189697265625
		 14 82.909637451171875 15 84.989913940429688 16 104.22264862060547 17 130.0433349609375
		 18 141.9471435546875 19 143.12921142578125 20 136.35296630859375 21 128.7239990234375
		 22 122.09027099609376 23 113.91248321533203 24 105.56590270996094 25 101.03132629394531
		 26 100.84629058837891 27 101.759521484375 28 103.22215270996094 29 104.66018676757812
		 30 105.58474731445312 31 105.70435333251953 32 104.98740386962891 33 103.63430786132812
		 34 101.9652099609375 35 100.2822265625 36 98.872299194335937 37 97.8426513671875
		 38 97.101844787597656 39 96.530120849609375 40 96.004043579101563 41 95.41339111328125
		 42 94.673561096191406 43 93.732475280761719 44 92.569786071777344 45 91.186676025390625
		 46 89.099441528320312 47 86.008255004882813 48 82.252792358398437 49 78.461929321289063
		 50 75.741996765136719 51 72.362159729003906 52 67.572456359863281 53 64.748085021972656
		 54 67.43109130859375 55 77.704200744628906 56 92.665374755859375 57 102.41057586669922
		 58 104.95393371582031 59 104.33343505859375 60 104.18454742431641 61 107.24224090576172
		 62 112.71534729003906 63 118.38230895996095 64 122.91011047363283 65 125.12106323242186
		 66 123.31281280517578 67 115.30043792724609 68 102.56993103027344 69 90.484184265136719
		 70 82.846221923828125 71 80.122673034667969 72 83.331512451171875 73 89.792282104492188
		 74 93.947563171386719 75 94.160003662109375 76 93.776718139648437 77 94.945297241210938
		 78 97.56097412109375 79 100.62389373779297 80 102.14198303222656;
createNode animCurveTU -n "Character1_LeftArm_visibility";
	rename -uid "628FB159-49BE-D53C-38E6-92B8D62BF70C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftForeArm_translateX";
	rename -uid "B87D1AD5-4A21-53F8-3B16-929253DD21C9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.0427684783935547;
createNode animCurveTL -n "Character1_LeftForeArm_translateY";
	rename -uid "9E90F4D5-414C-4AFD-81F7-BBAF00649333";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.323208808898926;
createNode animCurveTL -n "Character1_LeftForeArm_translateZ";
	rename -uid "06D2C57C-4203-EF86-EA71-F3AF8B1A9FCD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 16.678167343139648;
createNode animCurveTU -n "Character1_LeftForeArm_scaleX";
	rename -uid "97791470-4F70-6E31-7A0B-0B98B1011E07";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftForeArm_scaleY";
	rename -uid "123B9602-46D9-BD8E-6BE5-98BF6771B444";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftForeArm_scaleZ";
	rename -uid "8D21D61C-4888-115B-CE4C-1AA8F8858355";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftForeArm_rotateX";
	rename -uid "8253D041-42CF-21C9-6960-3BA066A62FDE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 80 ".ktv[0:79]"  0 -16.628414154052734 1 -21.353837966918945
		 2 -26.713146209716797 3 -31.908351898193363 4 -36.232276916503906 5 -39.143386840820312
		 6 -40.211894989013672 7 -40.211894989013672 9 -40.211894989013672 10 -40.211894989013672
		 11 -41.035663604736328 12 -43.996318817138672 13 -49.665969848632813 14 -54.368194580078125
		 15 -48.610660552978516 16 -40.313632965087891 17 -35.831291198730469 18 -31.076757431030273
		 19 -23.809978485107422 20 -17.137912750244141 21 -14.225571632385254 22 -17.636974334716797
		 23 -25.137285232543945 24 -32.548789978027344 25 -35.831291198730469 26 -35.857051849365234
		 27 -36.0081787109375 28 -36.191532135009766 29 -36.339778900146484 30 -36.4283447265625
		 31 -36.464286804199219 32 -36.45111083984375 33 -36.343517303466797 34 -36.014476776123047
		 35 -35.263416290283203 36 -33.895252227783203 37 -31.947952270507812 38 -29.59010124206543
		 39 -27.008415222167969 40 -24.373123168945313 41 -21.817523956298828 42 -19.434171676635742
		 43 -17.282732009887695 44 -15.402322769165037 45 -13.822878837585449 46 -12.650815010070801
		 47 -11.882907867431641 48 -11.406527519226074 49 -11.130467414855957 50 -10.976191520690918
		 51 -10.677921295166016 52 -10.244948387145996 53 -10.133904457092285 54 -10.939797401428223
		 55 -12.964054107666016 56 -16.381284713745117 57 -20.023305892944336 58 -23.651206970214844
		 59 -27.461540222167969 60 -29.487968444824219 61 -26.504783630371094 62 -21.145811080932617
		 63 -15.974328994750977 64 -10.979250907897949 65 -7.0141363143920898 66 -5.0851845741271973
		 67 -4.7201948165893555 68 -5.337073802947998 69 -6.3360538482666016 70 -7.3975367546081543
		 71 -8.439824104309082 72 -9.9268865585327148 73 -11.985039710998535 74 -14.141230583190918
		 75 -15.789693832397461 76 -16.618169784545898 77 -16.916555404663086 78 -16.884498596191406
		 79 -16.724424362182617 80 -16.628414154052734;
createNode animCurveTA -n "Character1_LeftForeArm_rotateY";
	rename -uid "027A123A-41A6-5EA3-FBDC-EDBD475ADFEF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 80 ".ktv[0:79]"  0 -4.6888589859008789 1 -4.6423740386962891
		 2 -3.881680965423584 3 -2.3871152400970459 4 -0.51593530178070068 5 1.1061122417449951
		 6 1.782514214515686 7 1.782514214515686 9 1.782514214515686 10 1.782514214515686
		 11 8.4958009719848633 12 23.451932907104492 13 38.844547271728516 14 47.338569641113281
		 15 42.438720703125 16 29.090793609619137 17 20.750579833984375 18 19.591875076293945
		 19 18.577211380004883 20 18.437461853027344 21 18.613040924072266 22 18.421815872192383
		 23 18.695100784301758 24 19.908306121826172 25 20.750579833984375 26 21.018857955932617
		 27 21.829595565795898 28 23.167774200439453 29 25.028375625610352 30 27.373802185058594
		 31 30.097530364990234 32 33.004299163818359 33 35.813369750976563 34 38.181106567382813
		 35 39.730739593505859 36 40.348972320556641 37 40.230503082275391 38 39.464939117431641
		 39 38.166172027587891 40 36.476306915283203 41 34.563209533691406 42 32.613384246826172
		 43 30.823595046997074 44 29.393842697143551 45 28.523069381713867 46 28.711553573608398
		 47 29.998222351074222 48 31.880130767822266 49 33.8458251953125 50 35.376491546630859
		 51 37.821285247802734 52 41.525913238525391 53 44.481685638427734 54 44.695789337158203
		 55 40.144950866699219 56 27.020149230957031 57 6.7394051551818848 58 -14.831265449523928
		 59 -32.134971618652344 60 -40.215297698974609 61 -35.500862121582031 62 -20.421537399291992
		 63 -0.13015218079090118 64 18.620244979858398 65 28.338531494140625 66 29.409223556518558
		 67 25.909877777099609 68 19.354928970336914 69 11.25083065032959 70 3.1517469882965088
		 71 -3.344104528427124 72 -9.4393177032470703 73 -16.161436080932617 74 -21.6632080078125
		 75 -24.212459564208984 76 -22.512842178344727 77 -17.708221435546875 78 -11.805931091308594
		 79 -6.8009867668151855 80 -4.6888589859008789;
createNode animCurveTA -n "Character1_LeftForeArm_rotateZ";
	rename -uid "A73B1142-4D9A-ED32-6F30-248D9FFCDD6D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 80 ".ktv[0:79]"  0 25.404184341430664 1 32.439453125 2 40.463348388671875
		 3 48.372932434082031 4 55.133216857910156 5 59.821037292480476 6 61.577499389648438
		 7 61.577499389648438 9 61.577499389648438 10 61.577499389648438 11 55.458988189697266
		 12 40.764186859130859 13 22.980833053588867 14 11.444427490234375 15 21.482028961181641
		 16 38.874637603759766 17 45.337577819824219 18 40.4678955078125 19 33.032417297363281
		 20 26.208698272705078 21 23.229990005493164 22 26.719097137451172 23 34.390106201171875
		 24 41.975055694580078 25 45.337577819824219 26 44.059909820556641 27 40.685615539550781
		 28 35.692489624023438 29 29.563684463500973 30 22.791585922241211 31 15.875709533691408
		 32 9.3242063522338867 33 3.6657824516296391 34 -0.53419440984725952 35 -2.6785478591918945
		 36 -2.7564291954040527 37 -1.3716673851013184 38 1.1153097152709961 39 4.336510181427002
		 40 7.9538555145263663 41 11.68156623840332 42 15.291238784790039 43 18.604427337646484
		 44 21.479297637939453 45 23.79585075378418 46 25.75255012512207 47 27.66358757019043
		 48 29.534776687622067 49 31.33530426025391 50 33.027107238769531 51 34.785037994384766
		 52 36.664421081542969 53 38.320449829101562 54 39.278232574462891 55 39.449306488037109
		 56 40.183341979980469 57 43.798561096191406 58 50.085071563720703 59 56.790016174316406
		 60 59.066677093505859 61 51.394474029541016 62 38.567481994628906 63 27.344745635986328
		 64 20.682443618774414 65 18.320058822631836 66 17.075593948364258 67 15.490085601806641
		 68 13.915672302246094 69 12.853841781616211 70 12.578164100646973 71 13.123190879821777
		 72 14.822385787963867 73 17.798965454101563 74 21.308155059814453 75 24.110736846923828
		 76 25.493057250976562 77 25.929000854492188 78 25.818836212158203 79 25.547914505004883
		 80 25.404184341430664;
createNode animCurveTU -n "Character1_LeftForeArm_visibility";
	rename -uid "D1614DE2-48C7-778B-C975-43A0666C1EA7";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftHand_translateX";
	rename -uid "4C6299C6-479B-D55C-1510-EE88DF6F3074";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.29200950264930725;
createNode animCurveTL -n "Character1_LeftHand_translateY";
	rename -uid "F54A8FE0-40AA-030C-F96F-73ADEE26395E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -14.295157432556152;
createNode animCurveTL -n "Character1_LeftHand_translateZ";
	rename -uid "1940C6DB-48A8-7045-EAB2-629CFF2668B4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6319241523742676;
createNode animCurveTU -n "Character1_LeftHand_scaleX";
	rename -uid "4CB167EA-4B5D-077A-51C4-C2910B2E54EB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHand_scaleY";
	rename -uid "D37146F0-40BA-2098-2A75-DAA6B28A4CCF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHand_scaleZ";
	rename -uid "42E8E8C5-4305-5A5D-84D7-8A8BAB551535";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHand_rotateX";
	rename -uid "B6280A49-4BDB-6F24-9E05-DDA9D7E96888";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  9 -7.0685840910300612e-005 10 -7.0687950938008726e-005
		 11 2.1459774971008301 12 8.0074834823608398 13 15.688575744628906 14 18.264318466186523
		 15 6.214686393737793 16 -5.7472634315490723 17 -10.316292762756348 18 -10.316292762756348
		 24 -10.316292762756348 25 -10.316292762756348 26 -10.027902603149414 27 -9.2721271514892578
		 28 -8.2313899993896484 29 -7.0735955238342285 30 -5.9246492385864258 31 -4.8639664649963379
		 32 -3.9339118003845215 33 -3.1541504859924316 34 -2.5354976654052734 35 -2.0912809371948242
		 36 -1.7620269060134888 37 -1.4714165925979614 38 -1.2175941467285156 39 -1.0014183521270752
		 40 -0.82560539245605469 41 -0.69398337602615356 42 -0.61086750030517578 43 -0.58055251836776733
		 44 -0.60691988468170166 45 -0.69316345453262329 46 -0.99132972955703724 47 -1.6660040616989136
		 48 -2.6832818984985352 49 -3.865764856338501 50 -4.8736019134521484 51 -5.7218484878540039
		 52 -6.5579476356506348 53 -7.2394680976867676 54 -7.6163969039916992 55 -7.4468083381652841
		 56 -6.3189539909362793 57 -4.1757197380065918 58 -1.3222194910049438 59 1.6326601505279541
		 60 3.750401496887207 61 4.6179747581481934 62 4.6327719688415527 63 4.0303220748901367
		 64 3.1327908039093018 65 2.3041346073150635 66 1.46474289894104 67 0.39945676922798157
		 68 -0.69654792547225952 69 -1.6530362367630005 70 -2.3107752799987793 71 -2.502021312713623
		 72 -1.5899739265441895 73 0.44035691022872925 74 2.7081942558288574 75 4.0031595230102539
		 76 3.9158656597137451 77 3.1644546985626221 78 2.0680332183837891 79 0.9270949959754945
		 80 -7.0682872319594026e-005;
createNode animCurveTA -n "Character1_LeftHand_rotateY";
	rename -uid "7F62C668-43B4-9346-50CA-FB9751CAF355";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  9 -1.4030828943134567e-008 10 -1.8542541724286821e-008
		 11 6.3788046836853027 12 19.302911758422852 13 30.092603683471676 14 33.106582641601563
		 15 19.611946105957031 16 -7.6131372451782227 17 -23.210477828979492 18 -23.210477828979492
		 24 -23.210477828979492 25 -23.210477828979492 26 -22.478567123413086 27 -20.454689025878906
		 28 -17.3897705078125 29 -13.540680885314941 30 -9.1804924011230469 31 -4.5998640060424805
		 32 -0.10328025370836258 33 3.9963431358337402 34 7.3827104568481454 35 9.7402019500732422
		 36 11.290236473083496 37 12.471900939941406 38 13.29948902130127 39 13.788274765014648
		 40 13.954267501831055 41 13.814007759094238 42 13.384400367736816 43 12.682562828063965
		 44 11.725686073303223 45 10.530898094177246 46 7.8891425132751456 47 3.4019193649291992
		 48 -1.7199233770370483 49 -6.3333921432495117 50 -9.3774394989013672 51 -10.882991790771484
		 52 -11.605570793151855 53 -11.562918663024902 54 -10.763516426086426 55 -9.2436494827270508
		 56 -6.2701177597045898 57 -1.9481022357940672 58 2.4277269840240479 59 5.7428617477416992
		 60 7.1517419815063477 61 6.2639803886413574 62 3.8134281635284428 63 0.73149728775024414
		 64 -2.1327691078186035 65 -3.9775032997131343 66 -5.0738492012023926 67 -6.0107612609863281
		 68 -6.7230496406555176 69 -7.1171092987060547 70 -7.0826377868652344 71 -6.5095338821411133
		 72 -4.3592386245727539 73 -0.79884576797485352 74 2.5599815845489502 75 4.3857293128967285
		 76 4.5202136039733887 77 3.8083362579345699 78 2.5785131454467773 79 1.1796857118606567
		 80 -4.1517370097388095e-010;
createNode animCurveTA -n "Character1_LeftHand_rotateZ";
	rename -uid "79C5A2C6-476F-25B9-AAE4-47942681031E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  9 -0.00011946736049139872 10 -0.00011946912127314135
		 11 0.16626295447349548 12 2.0442252159118652 13 6.1715645790100098 14 8.0953874588012695
		 15 2.467299222946167 16 1.1624617576599121 17 3.6846003532409672 18 3.6846003532409672
		 24 3.6846003532409672 25 3.6846003532409672 26 3.1932442188262939 27 1.8874639272689817
		 28 0.041864071041345596 29 -2.0826606750488281 30 -4.2703666687011719 31 -6.3575267791748047
		 32 -8.2241859436035156 33 -9.7803735733032227 34 -10.952574729919434 35 -11.672454833984375
		 36 -12.047638893127441 37 -12.246167182922363 38 -12.286909103393555 39 -12.187210083007812
		 40 -11.963115692138672 41 -11.629448890686035 42 -11.199792861938477 43 -10.686469078063965
		 44 -10.100509643554687 45 -9.4516782760620117 46 -8.1669845581054687 47 -6.0155911445617676
		 48 -3.5322134494781494 49 -1.3531590700149536 50 -0.28250491619110107 51 -1.0307276248931885
		 52 -3.1323544979095459 53 -5.7123723030090332 54 -7.8889117240905762 55 -8.7899360656738281
		 56 -8.1227188110351562 57 -6.3724799156188965 58 -3.8775217533111568 59 -1.1231104135513306
		 60 1.2180502414703369 61 3.2245194911956787 62 5.289100170135498 63 7.1616816520690918
		 64 8.5331478118896484 65 8.9869604110717773 66 8.4544553756713867 67 7.097681999206543
		 68 5.282163143157959 69 3.3714749813079834 70 1.7294425964355469 71 0.72324502468109131
		 72 0.84534174203872681 73 1.9325813055038452 74 3.2753350734710693 75 3.9439895153045654
		 76 3.6365985870361328 77 2.8845682144165039 78 1.8987762928009033 79 0.87935400009155273
		 80 -0.00011945857113460079;
createNode animCurveTU -n "Character1_LeftHand_visibility";
	rename -uid "17135447-43A0-8FE0-DEB1-9697E17C4252";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateX";
	rename -uid "76F78916-40C9-CF34-689A-88B14FFD76DF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1364259719848633;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateY";
	rename -uid "BEA34644-487E-CA1C-6D8B-3B89A0857251";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4241994619369507;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateZ";
	rename -uid "6A999C3A-47D8-2C18-75E5-B8A7265F4D15";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.7211661338806152;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleX";
	rename -uid "1250539C-4F9B-3927-DE25-E7909D544EE3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleY";
	rename -uid "AE86059D-4ED7-5A85-AD74-9392B54855D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleZ";
	rename -uid "8306D94D-4CDD-06EC-7B14-E58E9E2B8F01";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateX";
	rename -uid "C86A6160-418D-9A15-9F7A-CBB4305A0BD3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 -28.371952056884766 1 -30.148414611816406
		 2 -32.054462432861328 3 -33.761341094970703 4 -35.002529144287109 5 -35.668228149414063
		 6 -35.857170104980469 7 -35.857170104980469 12 -35.857170104980469 13 -35.857170104980469
		 14 -34.522247314453125 15 -29.656011581420902 16 -23.684028625488281 17 -20.756132125854492
		 18 -20.756132125854492 54 -20.756132125854492 55 -20.756132125854492 56 -22.718460083007813
		 57 -27.174104690551758 58 -31.951162338256836 59 -35.066440582275391 60 -35.857170104980469
		 61 -35.857170104980469 64 -35.857170104980469 65 -35.857170104980469 66 -35.271595001220703
		 67 -33.592845916748047 68 -31.188199996948239 69 -28.858814239501953 70 -27.585117340087891
		 71 -28.371952056884766 72 -29.664300918579102 73 -32.684871673583984 74 -35.786563873291016
		 75 -37.218132019042969 76 -36.564464569091797 77 -34.906085968017578 78 -32.697975158691406
		 79 -30.380746841430661 80 -28.371952056884766;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateY";
	rename -uid "FE40738A-4CC4-BF6F-FE47-A3BF0E3977F5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 26.454395294189453 1 32.588821411132812
		 2 39.612323760986328 3 46.623931884765625 4 52.727191925048828 5 57.033748626708984
		 6 58.664314270019524 7 58.664314270019524 12 58.664314270019524 13 58.664314270019524
		 14 49.5943603515625 15 29.541488647460938 16 9.4200439453125 17 0.26097205281257629
		 18 0.26097205281257629 54 0.26097205281257629 55 0.26097205281257629 56 6.3579258918762207
		 57 20.884222030639648 58 38.186668395996094 59 52.631977081298828 60 58.664314270019524
		 61 58.664314270019524 64 58.664314270019524 65 58.664314270019524 66 56.226104736328125
		 67 50.134910583496094 68 42.236572265625 69 34.399971008300781 70 28.509021759033203
		 71 26.454395294189453 72 25.058916091918945 73 25.943435668945313 74 27.591733932495117
		 75 28.479249954223633 76 28.331144332885742 77 27.953727722167969 78 27.448354721069336
		 79 26.916036605834961 80 26.454395294189453;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateZ";
	rename -uid "04F928D1-451B-F351-C7B1-A5A097FB01CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 -6.3497772216796875 1 -7.0321445465087891
		 2 -7.6943340301513672 3 -8.1623220443725586 4 -8.3225793838500977 5 -8.2217721939086914
		 6 -8.1192960739135742 7 -8.1192960739135742 12 -8.1192960739135742 13 -8.1192960739135742
		 14 -8.4753389358520508 15 -7.3555083274841309 16 -5.3075313568115234 17 -4.2941398620605469
		 18 -4.2941398620605469 54 -4.2941398620605469 55 -4.2941398620605469 56 -4.9697399139404297
		 57 -6.5283527374267578 58 -8.0272979736328125 59 -8.45440673828125 60 -8.1192960739135742
		 61 -8.1192960739135742 64 -8.1192960739135742 65 -8.1192960739135742 66 -7.8383936882019043
		 67 -6.9397735595703125 68 -5.5992836952209473 69 -4.4686079025268555 70 -4.422637939453125
		 71 -6.3497772216796875 72 -8.8505725860595703 73 -13.193870544433594 74 -17.307657241821289
		 75 -19.133916854858398 76 -18.210285186767578 77 -15.852351188659666 78 -12.679531097412109
		 79 -9.3080358505249023 80 -6.3497772216796875;
createNode animCurveTU -n "Character1_LeftHandThumb1_visibility";
	rename -uid "487A5285-4C71-FF65-535B-00B628CEAAC1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateX";
	rename -uid "7CA67839-4DE7-F09F-E4A9-9DB48B3B0B76";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.8329257965087891;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateY";
	rename -uid "82F7AAF8-42DA-13C1-9123-DB8B6A82FA73";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.094426639378070831;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateZ";
	rename -uid "D86BA7ED-4DF3-6A0F-73B0-EEB8DA144237";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5438374280929565;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleX";
	rename -uid "3AAC4817-4724-BE17-357F-8B8D127F3B15";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleY";
	rename -uid "6A399FCF-47FD-ACBB-DC10-9D9881A1B2A9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleZ";
	rename -uid "F453643A-4D8B-5B3D-3BB4-A7BF3645C205";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateX";
	rename -uid "E05023A5-4550-C1F6-6B19-2BB7D11959C6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 32 ".ktv[0:31]"  0 10.355228424072266 1 8.8879461288452148
		 2 6.4095683097839355 3 2.8715987205505371 4 -1.3807446956634521 5 -5.2899060249328613
		 6 -7.0229120254516602 7 -7.0229120254516602 12 -7.0229120254516602 13 -7.0229120254516602
		 14 -10.058894157409668 15 -12.739910125732422 16 -11.843351364135742 17 -10.60842227935791
		 18 -10.60842227935791 54 -10.60842227935791 55 -10.60842227935791 56 -11.480956077575684
		 57 -12.699625968933105 58 -12.134579658508301 59 -9.1999654769897461 60 -7.0229120254516602
		 61 -7.0229120254516602 64 -7.0229120254516602 65 -7.0229120254516602 66 -4.5512733459472656
		 67 0.43335571885108948 68 5.0511507987976074 69 8.196446418762207 70 9.8530750274658203
		 71 10.355228424072266 72 10.355228424072266;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateY";
	rename -uid "DD0E2D7C-46E4-FDAD-82CD-26BF97585699";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 32 ".ktv[0:31]"  0 20.610536575317383 1 27.267719268798828
		 2 34.901676177978516 3 42.473716735839844 4 48.955757141113281 5 53.417888641357422
		 6 55.072174072265625 7 55.072174072265625 12 55.072174072265625 13 55.072174072265625
		 14 50.517791748046875 15 39.728824615478516 16 28.516324996948242 17 23.416719436645508
		 18 23.416719436645508 54 23.416719436645508 55 23.416719436645508 56 26.807674407958984
		 57 34.92022705078125 58 44.463253021240234 59 52.074901580810547 60 55.072174072265625
		 61 55.072174072265625 64 55.072174072265625 65 55.072174072265625 66 52.658210754394531
		 67 46.445480346679688 68 38.136005401611328 69 29.706392288208008 70 23.205530166625977
		 71 20.610536575317383 72 20.610536575317383;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateZ";
	rename -uid "B0E6DED4-4531-61DC-DAD4-9490D4E2D2E0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 32 ".ktv[0:31]"  0 -8.5256719589233398 1 -11.161065101623535
		 2 -14.462574005126953 3 -18.336248397827148 4 -22.523147583007813 5 -26.199180603027344
		 6 -27.805004119873047 7 -27.805004119873047 12 -27.805004119873047 13 -27.805004119873047
		 14 -33.775596618652344 15 -43.729549407958984 16 -51.424919128417969 17 -54.614753723144531
		 18 -54.614753723144531 54 -54.614753723144531 55 -54.614753723144531 56 -52.502826690673828
		 57 -47.210304260253906 58 -39.84649658203125 59 -31.911224365234379 60 -27.805004119873047
		 61 -27.805004119873047 64 -27.805004119873047 65 -27.805004119873047 66 -25.511392593383789
		 67 -20.772380828857422 68 -16.020151138305664 69 -12.171934127807617 70 -9.5372085571289062
		 71 -8.5256719589233398 72 -8.5256719589233398;
createNode animCurveTU -n "Character1_LeftHandThumb2_visibility";
	rename -uid "7B44D5E1-4CE2-D5DA-FFF1-18B35E6D586D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateX";
	rename -uid "34139FE6-4014-3094-92B3-A8A1B5DA7BB5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7747817039489746;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateY";
	rename -uid "AB45BE04-46EB-58A6-3C6F-4C913EACCFCF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3182404041290283;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateZ";
	rename -uid "A5BA22B0-4258-413D-8D05-1381015D0E0F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.26324462890625;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleX";
	rename -uid "A684F072-4CFC-B194-2275-BF8C4112A6CD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleY";
	rename -uid "136E0A1A-452A-04CF-6C0B-41B48AB8F543";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleZ";
	rename -uid "4E2E3BF8-425D-EA6D-362C-EF82C862EDF6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateX";
	rename -uid "48472BF8-4BE8-4CC0-5914-768254E26C68";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.0178138028277317e-007;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateY";
	rename -uid "0DB06195-44C4-17AB-15C0-51B6A7539B83";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.6060516935049236e-008;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateZ";
	rename -uid "704F6C21-45A0-48BD-DCE8-33BB2C543084";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.0761441987815488e-007;
createNode animCurveTU -n "Character1_LeftHandThumb3_visibility";
	rename -uid "FCD8B140-4C6E-FA75-1B4E-D6A28B376D68";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateX";
	rename -uid "A98E6764-48C6-46C9-0E41-E68D5890642C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.854037284851074;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateY";
	rename -uid "E0FF915D-4FE8-8324-6BE8-52B52EC87AD8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2616198062896729;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateZ";
	rename -uid "A801CE3D-45AD-32C3-0C08-95A959633AE7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.944699764251709;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleX";
	rename -uid "44192B41-4534-98A4-A268-74A2FE1A33FA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleY";
	rename -uid "8CA39192-464F-C4EC-C77C-7C857EF3933D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleZ";
	rename -uid "3901912B-4B87-ADE5-B226-BBB509C6B4D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateX";
	rename -uid "DFE95C31-41A6-59B6-0F65-A59B16130309";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 2.1236224174499512 1 8.1200141906738281
		 2 18.213516235351563 3 31.877834320068359 4 45.965457916259766 5 56.226444244384766
		 6 60.010890960693366 7 60.010890960693366 12 60.010890960693366 13 60.010890960693366
		 14 45.865234375 15 20.354248046875 16 11.549266815185547 17 12.612855911254883 18 12.612855911254883
		 54 12.612855911254883 55 12.612855911254883 56 11.561015129089355 57 14.580009460449219
		 58 29.446220397949215 59 50.666168212890625 60 60.010890960693366 61 60.010890960693366
		 64 60.010890960693366 65 60.010890960693366 66 53.809486389160156 67 38.212848663330078
		 68 20.752689361572266 69 8.6261215209960937 70 2.9153358936309814 71 2.1236224174499512
		 72 2.0568721294403076 73 4.633336067199707 74 8.5131816864013672 75 10.716029167175293
		 76 9.941650390625 77 8.0760955810546875 78 5.8140902519226074 79 3.7146272659301762
		 80 2.1236224174499512;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateY";
	rename -uid "F635C192-4410-A587-DF95-629C2EC11DDF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 6.7255492210388184 1 16.521760940551758
		 2 25.98272705078125 3 32.435420989990234 4 34.666542053222656 5 34.062515258789063
		 6 33.363674163818359 7 33.363674163818359 12 33.363674163818359 13 33.363674163818359
		 14 34.974029541015625 15 23.080635070800781 16 -0.8616408109664917 17 -12.857967376708984
		 18 -12.857967376708984 54 -12.857967376708984 55 -12.857967376708984 56 -4.8680644035339355
		 57 13.551795959472656 58 30.42780876159668 59 34.966838836669922 60 33.363674163818359
		 61 33.363674163818359 64 33.363674163818359 65 33.363674163818359 66 34.378692626953125
		 67 33.922721862792969 68 27.63629150390625 69 17.17973518371582 70 8.3605899810791016
		 71 6.7255492210388184 72 6.5768280029296875 73 11.418044090270996 74 16.911252975463867
		 75 19.409049987792969 76 18.575571060180664 77 16.379886627197266 78 13.286289215087891
		 79 9.8486232757568359 80 6.7255492210388184;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateZ";
	rename -uid "752F6F8F-4C0D-5650-5AD3-D6AE33E04BB2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 1.4951497316360474 1 14.140872955322266
		 2 30.667280197143555 3 49.969760894775391 4 68.615455627441406 5 82.014915466308594
		 6 86.987724304199219 7 86.987724304199219 12 86.987724304199219 13 86.987724304199219
		 14 65.376548767089844 15 21.933769226074219 16 -10.388341903686523 17 -23.894474029541016
		 18 -23.894474029541016 54 -23.894474029541016 55 -23.894474029541016 56 -14.874967575073242
		 57 7.0593366622924805 58 39.142082214355469 59 72.693473815917969 60 86.987724304199219
		 61 86.987724304199219 64 86.987724304199219 65 86.987724304199219 66 78.849990844726562
		 67 58.422180175781257 68 34.3958740234375 69 15.020204544067381 70 3.3819496631622314
		 71 1.4951497316360474 72 1.3889428377151489 73 7.4108285903930655 74 15.031996726989748
		 75 18.921989440917969 76 17.576469421386719 77 14.211420059204102 78 9.8290987014770508
		 79 5.3310580253601074 80 1.4951497316360474;
createNode animCurveTU -n "Character1_LeftHandIndex1_visibility";
	rename -uid "28ABFBA6-4F8C-B8FD-2842-70AFD7DBE214";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateX";
	rename -uid "8B8A3B87-4718-FEE3-B974-EAB4DC2D165F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.1721835136413574;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateY";
	rename -uid "2896A1F6-48D3-73A5-54B0-A2B16251E2DF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.9652466773986816;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateZ";
	rename -uid "AADEAB89-4635-2615-AB02-9E9212F0C3CA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.8871781826019287;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleX";
	rename -uid "1A9C779F-4D29-E9EA-662F-EB995C965B5B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleY";
	rename -uid "78570EBD-424D-7DE3-8662-ABB6FCC73536";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleZ";
	rename -uid "355F0991-430E-A7BB-227F-92A1AE942947";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateX";
	rename -uid "86D582F6-42E3-042E-A69E-70B2A9E3D4E3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 7.2087359428405762 1 9.3566122055053711
		 2 10.765958786010742 3 10.924666404724121 4 9.9180679321289062 5 8.4935035705566406
		 6 7.7885360717773429 7 7.7885360717773429 12 7.7885360717773429 13 7.7885360717773429
		 14 8.4304742813110352 15 9.1024408340454102 16 8.7920999526977539 17 8.3434991836547852
		 18 8.3434991836547852 54 8.3434991836547852 55 8.3434991836547852 56 8.6628379821777344
		 57 9.0858163833618164 58 8.9366388320922852 59 8.239405632019043 60 7.7885360717773429
		 61 7.7885360717773429 64 7.7885360717773429 65 7.7885360717773429 66 8.9263143539428711
		 67 10.650444030761719 68 10.859106063842773 69 9.2893953323364258 70 7.3962392807006845
		 71 7.2087368965148926 72 7.4353909492492685 73 8.8711528778076172 74 10.076962471008301
		 75 10.48493766784668 76 10.34471321105957 77 9.9160528182983398 78 9.1829204559326172
		 79 8.2140178680419922 80 7.2087359428405762;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateY";
	rename -uid "A6E11F0F-407E-C640-F588-7BB91971F0A9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 -8.152409553527832 1 -12.724105834960938
		 2 -18.332328796386719 3 -24.100551605224609 4 -29.055370330810547 5 -32.403133392333984
		 6 -33.620094299316406 7 -33.620094299316406 12 -33.620094299316406 13 -33.620094299316406
		 14 -32.33270263671875 15 -29.271030426025391 16 -26.031314849853516 17 -24.541973114013672
		 18 -24.541973114013672 54 -24.541973114013672 55 -24.541973114013672 56 -25.533153533935547
		 57 -27.888648986816406 58 -30.62061882019043 59 -32.772014617919922 60 -33.620094299316406
		 61 -33.620094299316406 64 -33.620094299316406 65 -33.620094299316406 66 -31.544834136962894
		 67 -26.140850067138672 68 -19.067058563232422 69 -12.542017936706543 70 -8.4787769317626953
		 71 -8.152409553527832 72 -8.5482091903686523 73 -11.485992431640625 74 -14.988840103149414
		 75 -16.703779220581055 76 -16.057121276855469 77 -14.42086124420166 78 -12.261163711547852
		 79 -10.034485816955566 80 -8.152409553527832;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateZ";
	rename -uid "E333661A-4E31-2F1C-E9CB-41A6A5B3F19B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 22.938976287841797 1 33.333400726318359
		 2 45.320182800292969 3 57.627227783203125 4 68.861953735351563 5 77.215408325195313
		 6 80.491020202636719 7 80.491020202636719 12 80.491020202636719 13 80.491020202636719
		 14 75.394180297851562 15 64.606391906738281 16 54.331668853759766 17 49.803348541259766
		 18 49.803348541259766 54 49.803348541259766 55 49.803348541259766 56 52.808460235595703
		 57 60.125572204589837 58 69.18255615234375 59 77.084510803222656 60 80.491020202636719
		 61 80.491020202636719 64 80.491020202636719 65 80.491020202636719 66 74.991218566894531
		 67 62.137271881103516 68 46.871711730957031 69 32.934520721435547 70 23.714094161987305
		 71 22.938976287841797 72 23.878177642822266 73 30.601312637329105 74 38.228199005126953
		 75 41.87713623046875 76 40.505203247070313 77 37.010555267333984 78 32.317409515380859
		 79 27.328641891479492 80 22.938976287841797;
createNode animCurveTU -n "Character1_LeftHandIndex2_visibility";
	rename -uid "9062015B-48FB-44BF-B551-16A8197B1F24";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateX";
	rename -uid "ED72DF52-45DE-A9AB-E1D4-FC883CD85036";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.5761528015136719;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateY";
	rename -uid "224ECB8F-4B3C-F8E6-18FF-5A83F74D311D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8397388458251953;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateZ";
	rename -uid "172E70B2-4E98-8822-2F83-BF9AB0E3C19E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3069263696670532;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleX";
	rename -uid "A7E4E703-4DE2-B3B0-63B0-F38FD706D374";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleY";
	rename -uid "F9964F62-4507-3E82-F54B-B4878F9D8D79";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleZ";
	rename -uid "5BE1B35B-410F-BA5E-E978-01912C53EC45";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateX";
	rename -uid "E6EADD96-4A5F-9900-636E-DA8461BF780E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.1965678342894535e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateY";
	rename -uid "4F1CDE97-47A0-AB25-8CCC-B392DA6137BD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.3835301021790656e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateZ";
	rename -uid "903C19AF-4055-595B-1130-25A97D0D4090";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.2135232547479973e-007;
createNode animCurveTU -n "Character1_LeftHandIndex3_visibility";
	rename -uid "10C34706-4B1D-0E97-C108-60B742B914D7";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftHandRing1_translateX";
	rename -uid "40D2FB46-4000-BD06-31E4-A18ACD01D27B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4602031707763672;
createNode animCurveTL -n "Character1_LeftHandRing1_translateY";
	rename -uid "D0709B48-4B59-52C9-DAF9-CAB2BA5C8BA7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.5015654563903809;
createNode animCurveTL -n "Character1_LeftHandRing1_translateZ";
	rename -uid "29B8E178-4D94-578C-DA22-F1B662F12D22";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.6178884506225586;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleX";
	rename -uid "B12DA635-4B6E-6387-6C6E-5E8F9B76B88F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleY";
	rename -uid "2A90DC96-4EC9-3BA7-6AD5-7F8835B7B4D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleZ";
	rename -uid "7AB18579-4C25-5D16-483C-39890C58CD77";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateX";
	rename -uid "DDB97493-4CC2-27D9-970A-EBB7A595551D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 3.1731135845184326 1 5.7489070892333984
		 2 10.410351753234863 3 17.048545837402344 4 24.346931457519531 5 30.101564407348629
		 6 32.34600830078125 7 32.34600830078125 12 32.34600830078125 13 32.34600830078125
		 14 24.397884368896484 15 13.520329475402832 16 15.619690895080566 17 20.568748474121094
		 18 20.568748474121094 54 20.568748474121094 55 20.568748474121094 56 16.998846054077148
		 57 12.896666526794434 58 16.638191223144531 59 26.980098724365234 60 32.34600830078125
		 61 32.34600830078125 64 32.34600830078125 65 32.34600830078125 66 28.616138458251953
		 67 20.002294540405273 68 11.304475784301758 69 5.7663464546203613 70 3.4180140495300293
		 71 3.1731135845184326 72 3.2320654392242432 73 4.6046066284179687 74 6.8279170989990234
		 75 8.1366815567016602 76 7.6414809226989746 77 6.4749054908752441 78 5.1253566741943359
		 79 3.9643850326538086 80 3.1731135845184326;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateY";
	rename -uid "841ABF89-403B-96E7-5852-CEAFB279E12B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 7.5687365531921396 1 15.618729591369629
		 2 23.766960144042969 3 30.270307540893558 4 34.150215148925781 5 35.687999725341797
		 6 35.989974975585938 7 35.989974975585938 12 35.989974975585938 13 35.989974975585938
		 14 34.999759674072266 15 22.179704666137695 16 1.3788906335830688 17 -8.7160263061523437
		 18 -8.7160263061523437 54 -8.7160263061523437 55 -8.7160263061523437 56 -2.0088849067687988
		 57 13.73607063293457 58 29.174482345581055 59 35.720443725585938 60 35.989974975585938
		 61 35.989974975585938 64 35.989974975585938 65 35.989974975585938 66 35.445232391357422
		 67 32.288433074951172 68 25.099849700927734 69 15.818581581115723 70 8.5849781036376953
		 71 7.5687365531921396 72 7.8416543006896964 73 12.345125198364258 74 17.238473892211914
		 75 19.425556182861328 76 18.662569046020508 77 16.639636993408203 78 13.761380195617676
		 79 10.53013801574707 80 7.5687365531921396;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateZ";
	rename -uid "77951C3F-4362-A333-5107-7F92AAE89F79";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 22.875423431396484 1 33.182186126708984
		 2 46.290672302246094 3 61.168010711669915 4 75.640007019042969 5 86.527931213378906
		 6 90.74188232421875 7 90.74188232421875 12 90.74188232421875 13 90.74188232421875
		 14 70.82037353515625 15 32.110923767089844 16 1.2010465860366821 17 -12.227761268615723
		 18 -12.227761268615723 54 -12.227761268615723 55 -12.227761268615723 56 -3.2536299228668213
		 57 18.204631805419922 58 47.590641021728516 59 77.42864990234375 60 90.74188232421875
		 61 90.74188232421875 64 90.74188232421875 65 90.74188232421875 66 83.709762573242188
		 67 67.081069946289063 68 48.282810211181641 69 32.921142578125 70 23.705692291259766
		 71 22.875423431396484 72 23.661476135253906 73 30.045738220214844 74 37.705997467041016
		 75 41.532474517822266 76 40.098007202148437 77 36.503643035888672 78 31.811492919921879
		 79 26.988466262817383 80 22.875423431396484;
createNode animCurveTU -n "Character1_LeftHandRing1_visibility";
	rename -uid "1CC5200C-4AA7-F7E1-E421-938D107AA763";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftHandRing2_translateX";
	rename -uid "E6AFCDAC-4486-AA44-6F4D-A0B3137F2215";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.9237353801727295;
createNode animCurveTL -n "Character1_LeftHandRing2_translateY";
	rename -uid "516BA15C-4018-33A6-E4FD-81BE0F439B1C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.0094923973083496;
createNode animCurveTL -n "Character1_LeftHandRing2_translateZ";
	rename -uid "A4BC8FF6-48B9-DE2A-4367-D9BD146F46BD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8221585750579834;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleX";
	rename -uid "0487AA65-4AC9-2B9F-6FE0-D2BFEC3AA1D9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleY";
	rename -uid "0EDB1856-4CD9-26DC-8A33-2EA57DCE9845";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleZ";
	rename -uid "B4D1A40C-425A-B22C-3C48-8195FF7105E0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateX";
	rename -uid "DAA4A943-47E0-4764-73A6-D7B013A8EE92";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 9.7664651870727539 1 17.656158447265625
		 2 25.902639389038086 3 33.120281219482422 4 38.48797607421875 5 41.720527648925781
		 6 42.815162658691406 7 42.815162658691406 12 42.815162658691406 13 42.815162658691406
		 14 42.387920379638672 15 40.353725433349609 16 36.961437225341797 17 35.022426605224609
		 18 35.022426605224609 54 35.022426605224609 55 35.022426605224609 56 36.338996887207031
		 57 39.048435211181641 58 41.40704345703125 59 42.566761016845703 60 42.815162658691406
		 61 42.815162658691406 64 42.815162658691406 65 42.815162658691406 66 40.959491729736328
		 67 35.564792633056641 68 27.190891265869141 69 17.777553558349609 70 10.716190338134766
		 71 9.7664651870727539 72 10.077574729919434 73 14.587286949157713 74 19.602224349975586
		 75 21.907474517822266 76 21.086723327636719 77 18.94798469543457 78 15.976548194885254
		 79 12.711557388305664 80 9.7664651870727539;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateY";
	rename -uid "7D7BFA05-42CF-4A0D-2BFA-55857F2CBE0F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 -1.8647100925445559 1 -5.0831823348999023
		 2 -10.401308059692383 3 -17.178989410400391 4 -24.046638488769531 5 -29.331037521362305
		 6 -31.412193298339847 7 -31.412193298339847 12 -31.412193298339847 13 -31.412193298339847
		 14 -31.330862045288086 15 -31.547073364257816 16 -32.537433624267578 17 -33.302364349365234
		 18 -33.302364349365234 54 -33.302364349365234 55 -33.302364349365234 56 -32.769863128662109
		 57 -31.864448547363278 58 -31.37432861328125 59 -31.348442077636715 60 -31.412193298339847
		 61 -31.412193298339847 64 -31.412193298339847 65 -31.412193298339847 66 -27.982805252075195
		 67 -20.068578720092773 68 -11.446384429931641 69 -5.1459660530090332 70 -2.1665923595428467
		 71 -1.8647100925445559 72 -1.9611411094665527 73 -3.634276390075684 74 -6.1420102119445801
		 75 -7.5464630126953116 76 -7.0271196365356445 77 -5.7734584808349609 78 -4.2577071189880371
		 79 -2.8746771812438965 80 -1.8647100925445559;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateZ";
	rename -uid "98D2DF6E-4365-E7CE-C898-62B40287210B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 12.489145278930664 1 22.819978713989258
		 2 34.104896545410156 3 44.778232574462891 4 53.693767547607422 5 59.878990173339837
		 6 62.220687866210945 7 62.220687866210945 12 62.220687866210945 13 62.220687866210945
		 14 56.761421203613281 15 45.177410125732422 16 34.245819091796875 17 29.518867492675781
		 18 29.518867492675781 54 29.518867492675781 55 29.518867492675781 56 32.647285461425781
		 57 40.385669708251953 58 50.089797973632813 59 58.574199676513672 60 62.220687866210945
		 61 62.220687866210945 64 62.220687866210945 65 62.220687866210945 66 58.338188171386719
		 67 48.690597534179688 68 35.939140319824219 69 22.981712341308594 70 13.717276573181152
		 71 12.489145278930664 72 12.891085624694824 73 18.76295280456543 74 25.425863265991211
		 75 28.553325653076172 76 27.434303283691406 77 24.546577453613281 78 20.592367172241211
		 79 16.309404373168945 80 12.489145278930664;
createNode animCurveTU -n "Character1_LeftHandRing2_visibility";
	rename -uid "BFB97800-45C2-72D0-0CF3-4398B3925B02";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_LeftHandRing3_translateX";
	rename -uid "6F5678A6-4807-5BA7-FC3A-E28C2AEECFA4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6098947525024414;
createNode animCurveTL -n "Character1_LeftHandRing3_translateY";
	rename -uid "8737C3C3-4D2E-6CBF-DA10-709CA55A3927";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2952473163604736;
createNode animCurveTL -n "Character1_LeftHandRing3_translateZ";
	rename -uid "339CF146-4C0D-D7C3-AA0D-359169B30A3F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3554067611694336;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleX";
	rename -uid "423A5168-49FD-0309-9732-7998ADEF97D4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleY";
	rename -uid "93CDB151-453B-0985-D9E7-CBB11FF436F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleZ";
	rename -uid "D4D1D09D-48D2-6ECC-521C-BAA9FDDC9BE6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateX";
	rename -uid "DD848F68-4357-C25F-0360-D68EF9E714AB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.3585583970061634e-008;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateY";
	rename -uid "F6DED960-4EFA-D31D-7CDA-9E867B3BE33F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.2926560556443292e-007;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateZ";
	rename -uid "0AE2F9FA-4F22-9B5D-CEE0-D181986800C1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8869189350189117e-007;
createNode animCurveTU -n "Character1_LeftHandRing3_visibility";
	rename -uid "67B0C764-4AF1-34CE-489E-B68849B88037";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightShoulder_translateX";
	rename -uid "E2ED8282-4C65-D801-D721-08B03C53284B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 14.332768440246582;
createNode animCurveTL -n "Character1_RightShoulder_translateY";
	rename -uid "1EB9F506-4A5F-1781-C47D-81A9A24CEC48";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5798094272613525;
createNode animCurveTL -n "Character1_RightShoulder_translateZ";
	rename -uid "B06AD841-4D29-96AF-87C4-F184402E0161";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1086856126785278;
createNode animCurveTU -n "Character1_RightShoulder_scaleX";
	rename -uid "97363CC2-4A5B-A4DE-A9A0-96A79F7D7052";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightShoulder_scaleY";
	rename -uid "2B75F803-498B-5BD4-2D3C-15B3FA2C5C2F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightShoulder_scaleZ";
	rename -uid "C9DEDE94-4561-F0D3-3EB5-9EBA4369A501";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightShoulder_rotateX";
	rename -uid "5F5465A2-4709-B09C-3328-66B0204030D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  5 -1.920072078704834 6 -1.920072078704834
		 7 -0.80023634433746338 8 0.64369750022888184 9 1.9040062427520752 10 3.0022182464599609
		 11 3.7349355220794678 12 4.189213752746582 13 2.6347064971923828 14 -1.5336645841598511
		 15 -6.4516067504882813 16 -10.408930778503418 17 -12.682425498962402 18 -13.721232414245605
		 19 -14.27377986907959 20 -14.539854049682619 21 -14.611302375793457 22 -14.325162887573242
		 23 -13.679139137268066 24 -13.009084701538086 25 -12.682425498962402 26 -12.70411491394043
		 27 -12.816269874572754 28 -12.997688293457031 29 -13.226189613342285 30 -13.479243278503418
		 31 -13.734418869018555 32 -13.969654083251953 33 -14.163412094116211 34 -14.294697761535645
		 35 -14.342972755432131 36 -14.342972755432131 44 -14.342972755432131 45 -14.342972755432131
		 46 -14.250452041625977 47 -14.042290687561035 48 -13.82280158996582 49 -13.683066368103027
		 50 -13.692390441894531 51 -13.917755126953125 52 -14.292608261108398 53 -14.680324554443359
		 54 -14.97017765045166 55 -15.071715354919434 56 -14.53718090057373 57 -13.390235900878906
		 58 -11.968978881835938 59 -10.271331787109375 60 -9.0098752975463867 61 -8.602910041809082
		 62 -8.8164882659912109 63 -9.4707822799682617 64 -10.165678977966309 65 -10.347227096557617
		 66 -9.9657526016235352 67 -9.2464027404785156 68 -8.3251056671142578 69 -7.3383703231811515
		 70 -6.3947391510009766 71 -5.5619969367980957 72 -4.8204102516174316 73 -4.1369476318359375
		 74 -3.5264911651611328 75 -3.0016365051269531 76 -2.572540283203125 77 -2.2467448711395264
		 78 -2.0289497375488281 79 -1.9207127094268799 80 -1.920072078704834;
createNode animCurveTA -n "Character1_RightShoulder_rotateY";
	rename -uid "85A76E1A-4778-54FB-DEC8-FF9B58314357";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  5 -22.21434211730957 6 -22.21434211730957
		 7 -15.819046020507811 8 -4.3372664451599121 9 7.8049750328063965 10 16.512472152709961
		 11 16.048938751220703 12 13.86026668548584 13 11.342320442199707 14 8.3621015548706055
		 15 4.1964030265808105 16 -0.30038875341415405 17 -3.6282434463500977 18 -4.729914665222168
		 19 -4.3093314170837402 20 -3.3282723426818848 21 -2.7792983055114746 22 -2.7954201698303223
		 23 -2.8978738784790039 24 -3.1592001914978027 25 -3.6282434463500977 26 -4.3056669235229492
		 27 -5.1331930160522461 28 -6.0585207939147949 29 -7.029604434967041 30 -7.9944806098937997
		 31 -8.9011383056640625 32 -9.69744873046875 33 -10.331144332885742 34 -10.74983024597168
		 35 -10.90105152130127 36 -10.90105152130127 44 -10.90105152130127 45 -10.90105152130127
		 46 -11.193821907043457 47 -11.93455982208252 48 -12.915056228637695 49 -13.92109203338623
		 50 -14.733946800231934 51 -15.460965156555176 52 -16.250724792480469 53 -16.943876266479492
		 54 -17.3714599609375 55 -17.357048034667969 56 -15.671913146972654 57 -12.726385116577148
		 58 -9.6237087249755859 59 -6.3545322418212891 60 -3.2706685066223145 61 0.055783636868000031
		 62 3.7633411884307857 63 7.1626038551330566 64 9.6168737411499023 65 10.574455261230469
		 66 10.301795959472656 67 9.1024303436279297 68 7.2292990684509286 69 4.9357657432556152
		 70 2.4826605319976807 71 0.13901178538799286 72 -2.1236414909362793 73 -4.5003843307495117
		 74 -6.9670023918151855 75 -9.4983940124511719 76 -12.068763732910156 77 -14.651855468750002
		 78 -17.221242904663086 79 -19.750659942626953 80 -22.21434211730957;
createNode animCurveTA -n "Character1_RightShoulder_rotateZ";
	rename -uid "B5A65E5B-4423-9576-0EF3-998542927E31";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  5 10.851101875305176 6 10.851101875305176
		 7 11.104818344116211 8 12.669055938720703 9 15.752861976623533 10 19.094232559204102
		 11 20.199329376220703 12 20.628133773803711 13 18.704195022583008 14 13.45930290222168
		 15 6.4070148468017578 16 -0.51470047235488892 17 -5.5163307189941406 18 -8.1009988784790039
		 19 -9.3099784851074219 20 -9.6518688201904297 21 -9.6477804183959961 22 -9.0377168655395508
		 23 -7.6796560287475586 24 -6.2766666412353516 25 -5.5163307189941406 26 -5.3736557960510254
		 27 -5.3533563613891602 28 -5.4278411865234375 29 -5.5704550743103027 30 -5.7556476593017578
		 31 -5.9589548110961914 32 -6.1568379402160645 33 -6.3263959884643555 34 -6.444974422454834
		 35 -6.4896950721740723 36 -6.4896950721740723 44 -6.4896950721740723 45 -6.4896950721740723
		 46 -6.0692276954650879 47 -5.0579628944396973 48 -3.8295331001281734 49 -2.7556917667388916
		 50 -2.2095537185668945 51 -2.4259912967681885 52 -3.1579723358154297 53 -4.0601849555969238
		 54 -4.7820792198181152 55 -4.9666695594787598 56 -3.3292558193206787 57 -0.77940934896469116
		 58 1.8540809154510496 59 4.5143966674804687 60 5.4962482452392578 61 4.2590270042419434
		 62 1.8954119682312014 63 -0.89377564191818237 64 -3.3511662483215332 65 -4.6304507255554199
		 66 -4.8274726867675781 67 -4.4293241500854492 68 -3.6145482063293461 69 -2.5579371452331543
		 70 -1.4237265586853027 71 -0.36776623129844666 72 0.6362491250038147 73 1.712244987487793
		 74 2.8542280197143555 75 4.0575084686279297 76 5.3182144165039062 77 6.6327056884765625
		 78 7.9968910217285156 79 9.4054698944091797 80 10.851101875305176;
createNode animCurveTU -n "Character1_RightShoulder_visibility";
	rename -uid "8DB17C1F-45C4-2A8F-591E-9DA8A144B3E0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightArm_translateX";
	rename -uid "643DF85B-43F4-6DBA-AEE0-638249F4046A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.8354010581970215;
createNode animCurveTL -n "Character1_RightArm_translateY";
	rename -uid "413E3C9A-4B72-389C-F79D-0087E450A8E6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1704782247543335;
createNode animCurveTL -n "Character1_RightArm_translateZ";
	rename -uid "2AA9384F-4EEB-198B-6FD7-EFA5A5E163F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.3259291648864746;
createNode animCurveTU -n "Character1_RightArm_scaleX";
	rename -uid "F21E98D9-4426-CCDE-BC66-30AB2071529E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightArm_scaleY";
	rename -uid "8557F788-425B-E050-3B3E-DCB45145E4B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightArm_scaleZ";
	rename -uid "D724C355-41D0-D782-A98C-9D887B8EB56C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightArm_rotateX";
	rename -uid "445BA78B-4418-B3C6-8C19-489556E8AE9A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  5 -18.622560501098633 6 -18.622560501098633
		 7 13.463220596313477 8 43.485157012939453 9 62.787864685058594 10 73.528579711914062
		 11 77.17498779296875 12 79.551933288574219 13 75.733421325683594 14 62.351833343505859
		 15 40.837745666503906 16 15.322935104370119 17 -5.4890389442443848 18 -17.987262725830078
		 19 -25.255706787109375 20 -29.449493408203125 21 -32.543613433837891 22 -34.357852935791016
		 23 -34.453857421875 24 -34.098228454589844 25 -34.540557861328125 26 -35.91290283203125
		 27 -37.472190856933594 28 -39.155109405517578 29 -40.901069641113281 30 -42.652183532714844
		 31 -44.352970123291016 32 -45.949817657470703 33 -47.390396118164063 34 -48.622966766357422
		 35 -49.595699310302734 36 -50.272037506103516 37 -50.688667297363281 38 -50.901512145996094
		 39 -50.965843200683594 40 -50.937599182128906 41 -50.87408447265625 42 -50.834251403808594
		 43 -50.878288269042969 44 -51.066581726074219 45 -51.458057403564453 46 -52.371082305908203
		 47 -53.785472869873047 48 -55.262741088867188 49 -56.400527954101563 50 -56.833660125732422
		 51 -56.344913482666016 52 -55.192234039306641 53 -53.74468994140625 54 -52.37689208984375
		 55 -51.465053558349609 56 -52.374496459960938 57 -52.625675201416016 58 -51.753730773925781
		 59 -51.910709381103516 60 -44.686847686767578 61 -27.898870468139648 62 -12.319019317626953
		 63 2.034548282623291 64 14.961986541748047 65 21.726390838623047 66 21.153392791748047
		 67 17.648193359375 68 14.293727874755859 69 12.684548377990723 70 12.621701240539551
		 71 12.270736694335938 72 10.357456207275391 73 7.5313344001770028 74 3.8428564071655273
		 75 -0.53194659948348999 76 -5.3227100372314453 77 -10.274700164794922 78 -15.30151844024658
		 79 -20.63508415222168 80 -27.01600456237793;
createNode animCurveTA -n "Character1_RightArm_rotateY";
	rename -uid "200B66B5-470A-F059-CC86-ABB811C294C7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  5 67.734016418457031 6 67.734016418457031
		 7 61.285968780517571 8 43.40203857421875 9 25.617177963256836 10 18.479904174804687
		 11 22.612571716308594 12 27.532732009887695 13 32.70233154296875 14 39.504665374755859
		 15 46.379978179931641 16 49.779560089111328 17 49.459712982177734 18 47.356765747070313
		 19 44.4439697265625 20 41.690876007080078 21 39.655483245849609 22 38.654075622558594
		 23 38.382476806640625 24 38.35906982421875 25 38.147739410400391 26 37.683040618896484
		 27 37.187389373779297 28 36.686008453369141 29 36.20257568359375 30 35.758792877197266
		 31 35.373863220214844 32 35.064144134521484 33 34.842975616455078 34 34.720844268798828
		 35 34.705898284912109 36 34.837944030761719 37 35.120121002197266 38 35.505973815917969
		 39 35.951087951660156 40 36.412792205810547 41 36.849758148193359 42 37.221702575683594
		 43 37.489040374755859 44 37.612533569335938 45 37.553020477294922 46 37.051681518554687
		 47 36.026321411132813 48 34.700061798095703 49 33.295631408691406 50 32.043609619140625
		 51 30.584878921508789 52 28.827253341674805 53 27.393646240234375 54 26.827367782592773
		 55 27.573699951171875 56 33.350856781005859 57 41.982646942138672 58 50.858524322509766
		 59 62.85772705078125 60 69.94830322265625 61 68.568992614746094 62 64.639068603515625
		 63 61.335811614990234 64 58.7779541015625 65 56.634311676025391 66 55.258548736572266
		 67 54.646739959716797 68 55.070911407470703 69 56.815933227539062 70 59.458992004394531
		 71 61.565620422363281 72 62.65653991699218 73 63.609207153320313 74 64.347221374511719
		 75 64.819709777832031 76 65.04534912109375 77 65.144538879394531 78 65.34429931640625
		 79 65.952194213867188 80 67.298484802246094;
createNode animCurveTA -n "Character1_RightArm_rotateZ";
	rename -uid "01F36AC3-41FC-CF1D-2E2E-748963F3DA47";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  5 -12.72693920135498 6 -12.72693920135498
		 7 9.3235082626342773 8 19.049129486083984 9 12.607379913330078 10 5.6911439895629883
		 11 11.621394157409668 12 20.845619201660156 13 27.828580856323242 14 29.021987915039063
		 15 24.187276840209961 16 14.599277496337891 17 6.1652331352233887 18 1.4724268913269043
		 19 -0.46819975972175598 20 -0.78525692224502563 21 -0.51213723421096802 22 0.033398307859897614
		 23 0.81424248218536377 24 1.5982401371002197 25 2.2137248516082764 26 2.6967954635620117
		 27 3.1912989616394043 28 3.7070672512054443 29 4.2480511665344238 30 4.8121490478515625
		 31 5.391603946685791 32 5.9739127159118652 33 6.5430598258972168 34 7.0810427665710449
		 35 7.5695986747741699 36 8.0079135894775391 37 8.4026269912719727 38 8.7523374557495117
		 39 9.0590553283691406 40 9.3254928588867188 41 9.5533037185668945 42 9.7423372268676758
		 43 9.8908863067626953 44 9.9970178604125977 45 10.060946464538574 46 10.054488182067871
		 47 9.9682121276855469 48 9.8071365356445313 49 9.5104827880859375 50 8.9524526596069336
		 51 7.8695650100708008 52 6.224696159362793 53 4.2936162948608398 54 2.5043783187866211
		 55 1.3822673559188843 56 4.6298213005065918 57 4.5416836738586426 58 -6.7439470291137695
		 59 -17.390758514404297 60 -12.090282440185547 61 9.3761177062988281 62 32.812019348144531
		 63 55.213165283203125 64 73.151466369628906 65 80.805168151855469 66 77.748374938964844
		 67 69.317413330078125 68 58.938068389892578 69 49.258800506591797 70 41.575351715087891
		 71 35.243900299072266 72 28.492702484130859 73 20.853290557861328 74 12.332948684692383
		 75 3.0835955142974854 76 -6.6447362899780273 77 -16.619272232055664 78 -26.773286819458008
		 79 -37.342475891113281 80 -49.031879425048828;
createNode animCurveTU -n "Character1_RightArm_visibility";
	rename -uid "1A30597B-45C6-A5BB-6F46-FFAFFE2FE62A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightForeArm_translateX";
	rename -uid "97D2EF02-4C46-9B22-A7DF-8F91B12BF8A9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.753132820129395;
createNode animCurveTL -n "Character1_RightForeArm_translateY";
	rename -uid "DEFF23BC-45C6-8540-C872-8A896E0545E1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.862128257751465;
createNode animCurveTL -n "Character1_RightForeArm_translateZ";
	rename -uid "44E3D24E-4336-1C05-D263-1E8EE54D5037";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.671875953674316;
createNode animCurveTU -n "Character1_RightForeArm_scaleX";
	rename -uid "B77DB0CC-4604-C595-B9BF-DC92710D559B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightForeArm_scaleY";
	rename -uid "EB94B8CE-4CAE-2E29-B482-78ABA6B807DF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightForeArm_scaleZ";
	rename -uid "091BCD64-4F11-D8D5-62A7-AC91C2CB00EB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightForeArm_rotateX";
	rename -uid "6FFB1E2A-4FA8-C73E-6F1B-C799C82EE3F7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 25.008867263793945 1 20.623910903930664
		 2 16.652858734130859 3 12.678709983825684 4 8.532252311706543 5 4.2871494293212891
		 6 0.21691358089447021 7 -3.1258599758148193 8 -5.2001228332519531 9 -3.682796716690063
		 10 4.7516260147094727 11 2.9835588932037354 12 0.73142540454864502 13 -0.10277289897203445
		 14 -0.60054171085357666 15 -1.2115383148193359 16 -1.7818926572799683 17 -1.8066904544830324
		 18 -1.0320985317230225 19 -0.063295610249042511 20 0.68303418159484863 21 1.1009124517440796
		 22 1.3651860952377319 23 1.6058181524276733 24 1.7245506048202515 25 1.563569188117981
		 26 0.82512933015823364 27 -0.56734603643417358 28 -2.3904740810394287 29 -4.4435663223266602
		 30 -6.5525875091552734 31 -8.566645622253418 32 -10.348688125610352 33 -11.763736724853516
		 34 -12.669069290161133 35 -12.911042213439941 36 -12.365164756774902 37 -11.130300521850586
		 38 -9.3887157440185547 39 -7.3258461952209464 40 -5.1238932609558105 41 -2.9574742317199707
		 42 -0.99245345592498768 43 0.61239278316497803 44 1.702051043510437 45 2.1212573051452637
		 46 2.091392993927002 47 1.9059839248657227 48 1.5530024766921997 49 1.1369477510452271
		 50 0.87181514501571655 51 0.99521052837371826 52 1.3978434801101685 53 1.7795928716659546
		 54 1.8608307838439941 55 1.5196408033370972 56 0.8519824743270874 57 2.8048982620239258
		 58 10.08137035369873 59 25.862884521484375 60 38.677558898925781 61 36.740833282470703
		 62 27.406696319580078 63 16.143228530883789 64 7.4685583114624015 65 3.1572830677032471
		 66 2.1329798698425293 67 2.4954359531402588 68 3.7262585163116455 69 5.3559222221374512
		 70 6.9175047874450684 71 7.9583315849304199 72 8.5501327514648437 73 9.0284280776977539
		 74 9.3805608749389648 75 9.5994234085083008 76 9.6834802627563477 77 9.6365833282470703
		 78 9.4676017761230469 79 9.1899442672729492 80 8.8209991455078125;
createNode animCurveTA -n "Character1_RightForeArm_rotateY";
	rename -uid "B833CD71-4F22-8479-83AC-3EA75A4B80DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 48.595924377441406 1 47.611492156982422
		 2 46.785446166992188 3 45.729602813720703 4 43.98193359375 5 41.0457763671875 6 36.425117492675781
		 7 25.602231979370117 8 7.5718350410461435 9 -23.059289932250977 10 -41.806350708007813
		 11 -35.202518463134766 12 -24.014623641967773 13 -14.234565734863281 14 -7.3154339790344247
		 15 -0.98220902681350708 16 4.1508874893188477 17 7.5361208915710449 18 8.2835712432861328
		 19 6.8765130043029785 20 4.9142870903015137 21 4.0683417320251465 22 5.4629783630371094
		 23 8.0053339004516602 24 10.062870025634766 25 10.00855541229248 26 8.0346403121948242
		 27 5.4723958969116211 28 2.4065468311309814 29 -1.0560263395309448 30 -4.7788891792297363
		 31 -8.5972318649291992 32 -12.324294090270996 33 -15.759662628173828 34 -18.696775436401367
		 35 -20.92802619934082 36 -22.432855606079102 37 -23.382780075073242 38 -23.865949630737305
		 39 -23.979122161865234 40 -23.826253890991211 41 -23.515487670898437 42 -23.155366897583008
		 43 -22.852031707763672 44 -22.709135055541992 45 -22.831384658813477 46 -23.135469436645508
		 47 -23.448413848876953 48 -23.740196228027344 49 -23.940364837646484 50 -23.921024322509766
		 51 -24.184934616088867 52 -24.803556442260742 53 -24.989519119262695 54 -23.95219612121582
		 55 -20.866909027099609 56 -9.65576171875 57 7.1058330535888672 58 25.345212936401367
		 59 40.777217864990234 60 46.206356048583984 61 46.362079620361328 62 44.144939422607422
		 63 38.941757202148438 64 32.138320922851562 65 27.220891952514648 66 25.436307907104492
		 67 25.070047378540039 68 25.589540481567383 69 26.434791564941406 70 27.103395462036133
		 71 27.16786003112793 72 26.752950668334961 73 26.260696411132813 74 25.715686798095703
		 75 25.143238067626953 76 24.568643569946289 77 24.01641845703125 78 23.509571075439453
		 79 23.068954467773437 80 22.712736129760742;
createNode animCurveTA -n "Character1_RightForeArm_rotateZ";
	rename -uid "25FB3A8C-46C0-7E02-A179-FEAD86FC1720";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -14.562373161315918 1 -22.132043838500977
		 2 -29.365999221801754 3 -36.475490570068359 4 -43.448642730712891 5 -50.061565399169922
		 6 -55.940521240234375 7 -58.880985260009766 8 -61.246971130371094 9 -70.942398071289063
		 10 -80.526763916015625 11 -70.26483154296875 12 -57.282070159912109 13 -48.939273834228516
		 14 -47.194221496582031 15 -48.531272888183594 16 -50.578479766845703 17 -50.876701354980469
		 18 -48.181968688964844 19 -43.967193603515625 20 -39.742122650146484 21 -36.922416687011719
		 22 -35.959259033203125 23 -36.059642791748047 24 -36.749256134033203 25 -37.554981231689453
		 26 -38.328155517578125 27 -39.162326812744141 28 -39.931636810302734 29 -40.535377502441406
		 30 -40.912387847900391 31 -41.048229217529297 32 -40.976425170898438 33 -40.774681091308594
		 34 -40.555389404296875 35 -40.447685241699219 36 -40.469200134277344 37 -40.563522338867188
		 38 -40.716445922851563 39 -40.897781372070313 40 -41.083324432373047 41 -41.268989562988281
		 42 -41.476234436035156 43 -41.749446868896484 44 -42.146392822265625 45 -42.72283935546875
		 46 -44.568946838378906 47 -47.974536895751953 48 -51.795700073242187 49 -54.94451904296875
		 50 -56.395179748535156 51 -56.193435668945313 52 -55.044685363769531 53 -52.911834716796875
		 54 -49.752120971679688 55 -45.604129791259766 56 -37.135353088378906 57 -25.626194000244141
		 58 -9.2579669952392578 59 14.213191986083984 60 28.604227066040039 61 21.03550910949707
		 62 2.7606105804443359 63 -18.036293029785156 64 -34.517772674560547 65 -43.063232421875
		 66 -44.903987884521484 67 -43.581108093261719 68 -40.099281311035156 69 -35.407901763916016
		 70 -30.507829666137695 71 -26.462451934814453 72 -23.147781372070313 73 -19.743579864501953
		 74 -16.281375885009766 75 -12.790104866027832 76 -9.2963438034057617 77 -5.8246760368347168
		 78 -2.3980941772460937 79 0.96163451671600331 80 4.2336583137512207;
createNode animCurveTU -n "Character1_RightForeArm_visibility";
	rename -uid "DB1D04E8-46C7-2575-4A07-EA846629A027";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightHand_translateX";
	rename -uid "FA5676AA-47A9-C789-FB95-12B5651036A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7167695760726929;
createNode animCurveTL -n "Character1_RightHand_translateY";
	rename -uid "0513DFEF-4A2B-C106-3C41-E7A0FA9B1BC7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.314067840576172;
createNode animCurveTL -n "Character1_RightHand_translateZ";
	rename -uid "63FE5134-4986-AA5A-F36D-399121A5B74C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4444055557250977;
createNode animCurveTU -n "Character1_RightHand_scaleX";
	rename -uid "C0E62318-440E-6527-9E90-28912F9962CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightHand_scaleY";
	rename -uid "373914FF-45F8-809D-13D3-CE9D4D325656";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightHand_scaleZ";
	rename -uid "5BE506BC-45ED-4D7D-8E2F-49AA1D83832D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightHand_rotateX";
	rename -uid "29210D90-4AA9-A8DD-F207-90B9A061319D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 3.8450412750244136 1 -3.1720337867736816
		 2 -10.811606407165527 3 -17.895130157470703 4 -23.559028625488281 5 -27.337692260742188
		 6 -28.935316085815426 7 -18.727670669555664 8 -1.2019160985946655 9 2.9753870964050293
		 10 -2.8916728496551514 11 -12.324288368225098 12 -22.315435409545898 13 -23.335956573486328
		 14 -12.823623657226563 15 3.8971939086914067 16 20.79072380065918 17 32.285037994384766
		 18 39.393890380859375 19 44.804237365722656 20 47.732570648193359 21 47.180427551269531
		 22 40.436614990234375 23 28.248834609985352 24 15.521224021911621 25 7.8664555549621573
		 26 5.1696624755859375 27 3.4347414970397949 28 2.5010318756103516 29 2.2035901546478271
		 30 2.374481201171875 31 2.8443152904510498 32 3.444190502166748 33 4.0076498985290527
		 34 4.3718786239624023 35 4.3772478103637695 36 4.1653280258178711 37 3.9811458587646484
		 38 3.8214230537414551 39 3.683086633682251 40 3.5632297992706299 41 3.4590771198272705
		 42 3.3679490089416504 43 3.2872369289398193 44 3.2143802642822266 45 3.1468546390533447
		 46 3.3457577228546143 47 3.8338618278503422 48 4.2482428550720215 49 4.2194275856018066
		 50 3.3780553340911865 51 1.3339195251464844 52 -1.6312432289123535 53 -4.8777728080749512
		 54 -7.7995662689208975 55 -9.8412799835205078 56 -8.5393171310424805 57 -7.4445133209228507
		 58 -10.467718124389648 59 -14.875728607177733 60 -18.957986831665039 61 -22.395769119262695
		 62 -25.897750854492187 63 -29.19993782043457 64 -32.045860290527344 65 -34.198944091796875
		 66 -36.0814208984375 67 -37.951038360595703 68 -39.338779449462891 69 -39.838531494140625
		 70 -39.125675201416016 71 -36.929271697998047 72 -33.443973541259766 73 -29.156738281250004
		 74 -24.22599983215332 75 -18.806247711181641 76 -13.053701400756836 77 -7.1307535171508789
		 78 -1.2083715200424194 79 4.5337810516357422 80 9.9091672897338867;
createNode animCurveTA -n "Character1_RightHand_rotateY";
	rename -uid "DFCD22CF-4332-DFA4-5310-CE940C6EB571";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 5.704012393951416 1 4.8644499778747559
		 2 5.8004164695739746 3 8.5730915069580078 4 12.325546264648438 5 15.660696029663088
		 6 17.116743087768555 7 8.4551191329956055 8 1.1064742803573608 9 0.39079388976097107
		 10 -1.1787818670272827 11 -1.8158067464828491 12 3.0232236385345459 13 7.0833029747009277
		 14 4.1404070854187012 15 1.1258575916290283 16 3.5840370655059814 17 8.7905941009521484
		 18 12.025887489318848 19 13.421014785766602 20 13.036656379699707 21 11.519065856933594
		 22 8.5545129776000977 23 5.6477293968200684 24 4.8050494194030762 25 5.1087985038757324
		 26 5.2185215950012207 27 5.187903881072998 28 5.0257892608642578 29 4.7725667953491211
		 30 4.478726863861084 31 4.1897730827331543 32 3.9366416931152344 33 3.73139500617981
		 34 3.5682382583618164 35 3.4299118518829346 36 3.309715747833252 37 3.2125535011291504
		 38 3.1361033916473389 39 3.0776231288909912 40 3.034041166305542 41 3.0020303726196289
		 42 2.9780709743499756 43 2.9584989547729492 44 2.9395420551300049 45 2.917344331741333
		 46 2.9603056907653809 47 3.0775794982910156 48 3.1838061809539795 49 3.2106707096099854
		 50 3.0882818698883057 51 2.7050290107727051 52 2.0140388011932373 53 1.054551362991333
		 54 -0.037013977766036987 55 -1.0156683921813965 56 -1.1199492216110229 57 -0.96063655614852894
		 58 -1.5967962741851807 59 -2.3226029872894287 60 -2.7664937973022461 61 -2.79795241355896
		 62 -2.4316201210021973 63 -1.7863291501998901 64 -1.1521666049957275 65 -0.93189859390258789
		 66 -1.4246159791946411 67 -2.5229349136352539 68 -3.9759082794189458 69 -5.435063362121582
		 70 -6.490354061126709 71 -6.7431178092956543 72 -6.282191276550293 73 -5.4756631851196289
		 74 -4.4166803359985352 75 -3.1960501670837402 76 -1.892807602882385 77 -0.56748032569885254
		 78 0.74092423915863037 79 2.0136034488677979 80 3.2466509342193604;
createNode animCurveTA -n "Character1_RightHand_rotateZ";
	rename -uid "5E0C20E7-41C7-5C8F-F376-5081899DB8DE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 30.173507690429688 1 35.487022399902344
		 2 41.599102020263672 3 47.359783172607422 4 51.872531890869141 5 54.761287689208984
		 6 56.012687683105469 7 50.213733673095703 8 33.283523559570312 9 7.1105928421020508
		 10 -1.4236347675323486 11 11.545846939086914 12 26.650531768798828 13 33.976383209228516
		 14 31.471786499023434 15 22.103965759277344 16 10.701068878173828 17 3.9385058879852299
		 18 2.5955872535705566 19 3.5951268672943115 20 5.7106337547302246 21 8.0745697021484375
		 22 11.665988922119141 23 17.060901641845703 24 22.073631286621094 25 24.362737655639648
		 26 24.436080932617188 27 23.976516723632813 28 23.107606887817383 29 21.937572479248047
		 30 20.566909790039063 31 19.095224380493164 32 17.62542724609375 33 16.26515007019043
		 34 15.125986099243162 35 14.321534156799316 36 13.78343391418457 37 13.357882499694824
		 38 13.030490875244141 39 12.786966323852539 40 12.613097190856934 41 12.494724273681641
		 42 12.41773796081543 43 12.368061065673828 44 12.331637382507324 45 12.294437408447266
		 46 12.506170272827148 47 13.036354064941406 48 13.599983215332031 49 13.910372734069824
		 50 13.680624008178711 51 12.776432991027832 52 11.402848243713379 53 9.786982536315918
		 54 8.1422948837280273 55 6.6478381156921387 56 4.7292485237121582 57 3.6201643943786617
		 58 4.4230356216430664 59 6.0007815361022949 60 7.9216351509094247 61 10.327260971069336
		 62 13.182684898376465 63 15.861607551574709 64 17.811050415039063 65 18.552383422851563
		 66 17.948734283447266 67 16.456497192382812 68 14.48319149017334 69 12.380504608154297
		 70 10.463644981384277 71 9.0628118515014648 72 8.1158819198608398 73 7.3047881126403809
		 74 6.6228256225585938 75 6.0540871620178223 76 5.570711612701416 77 5.1348137855529785
		 78 4.7035751342773437 79 4.235440731048584 80 3.6951575279235835;
createNode animCurveTU -n "Character1_RightHand_visibility";
	rename -uid "F624ECDC-4B81-0253-F3B0-4284C6F2DE7D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightHandThumb1_translateX";
	rename -uid "1C245000-4D47-A02D-116F-179E6C522470";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5741705894470215;
createNode animCurveTL -n "Character1_RightHandThumb1_translateY";
	rename -uid "288F8A53-480B-BD45-60F5-449447C1734C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4327812194824219;
createNode animCurveTL -n "Character1_RightHandThumb1_translateZ";
	rename -uid "B8F4ED4A-4687-32F0-DA3A-28B289FEA3DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8196568489074707;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleX";
	rename -uid "88239520-465C-C225-BEF0-2E9AB880A51F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleY";
	rename -uid "8F946573-4409-47EE-0A8F-899ADDA6BE4D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleZ";
	rename -uid "F9DBE13C-46BC-5CED-7998-D8B4CBD4DA37";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateX";
	rename -uid "FC1C096D-45F9-49CF-F640-16AED23F7DD3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 38.973472595214844 1 43.712268829345703
		 2 48.512489318847656 3 52.504859924316406 4 55.272006988525391 5 56.817283630371094
		 6 57.314090728759766 7 57.314090728759766 12 57.314090728759766 13 57.314090728759766
		 14 50.737724304199219 15 25.89476203918457 16 -5.8893780708312988 17 -18.971302032470703
		 18 -18.971302032470703 54 -18.971302032470703 55 -18.971302032470703 56 -10.421777725219727
		 57 12.165717124938965 58 38.209911346435547 59 53.270931243896484 60 57.314090728759766
		 61 57.314090728759766 64 57.314090728759766 65 57.314090728759766 66 56.475704193115234
		 67 53.856880187988281 68 49.406192779541016 69 44.030387878417969 70 39.762828826904297
		 71 38.973472595214844 72 38.898567199707031 73 41.233482360839844 74 43.920761108398437
		 75 45.168628692626953 76 44.748828887939453 77 43.656139373779297 78 42.140182495117188
		 79 40.475666046142578 80 38.973472595214844;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateY";
	rename -uid "8289071A-4F55-B646-5140-1E99AEE07A47";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 27.536184310913086 1 26.068046569824219
		 2 23.533174514770508 3 20.220787048339844 4 16.814355850219727 5 14.171915054321289
		 6 13.127543449401855 7 13.127543449401855 12 13.127543449401855 13 13.127543449401855
		 14 21.701930999755859 15 33.242183685302734 16 30.202859878540039 17 24.630031585693359
		 18 24.630031585693359 54 24.630031585693359 55 24.630031585693359 56 28.557077407836914
		 57 33.777309417724609 58 29.842079162597656 59 18.995641708374023 60 13.127543449401855
		 61 13.127543449401855 64 13.127543449401855 65 13.127543449401855 66 14.805784225463867
		 67 18.674289703369141 68 22.872060775756836 69 25.918415069580078 70 27.348211288452148
		 71 27.536184310913086 72 27.553108215332031 73 26.964900970458984 74 26.050636291503906
		 75 25.528205871582031 76 25.709012985229492 77 26.146406173706055 78 26.678430557250977
		 79 27.169919967651367 80 27.536184310913086;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateZ";
	rename -uid "9E77B49C-4FFD-0F8D-461E-4FAF0FB3792C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 -17.683155059814453 1 -11.083030700683594
		 2 -3.8307714462280278 3 2.9958071708679199 4 8.5896387100219727 5 12.358211517333984
		 6 13.751087188720703 7 13.751087188720703 12 13.751087188720703 13 13.751087188720703
		 14 5.2324771881103516 15 -18.842226028442383 16 -43.406253814697266 17 -50.823329925537109
		 18 -50.823329925537109 54 -50.823329925537109 55 -50.823329925537109 56 -46.227527618408203
		 57 -30.27838134765625 58 -7.6493558883667001 59 8.2392396926879883 60 13.751087188720703
		 61 13.751087188720703 64 13.751087188720703 65 13.751087188720703 66 11.505882263183594
		 67 5.7213134765625 68 -2.1969637870788574 69 -10.409703254699707 70 -16.446161270141602
		 71 -17.683156967163086 72 -17.986774444580078 73 -15.113637924194336 74 -11.614334106445313
		 75 -9.9287357330322266 76 -10.475037574768066 77 -11.880752563476563 78 -13.795647621154785
		 79 -15.856430053710939 80 -17.683155059814453;
createNode animCurveTU -n "Character1_RightHandThumb1_visibility";
	rename -uid "8A96267F-4FCD-97DF-B788-98BAB90DA755";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightHandThumb2_translateX";
	rename -uid "53371194-4202-68F9-323B-119B601368F2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7422792911529541;
createNode animCurveTL -n "Character1_RightHandThumb2_translateY";
	rename -uid "55B52DCE-4D09-9FC9-4F0B-16A0BBBA97DE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5767025947570801;
createNode animCurveTL -n "Character1_RightHandThumb2_translateZ";
	rename -uid "CD8B1842-4C71-F82D-7E25-B7B578D4C4CE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0428388118743896;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleX";
	rename -uid "60CB72E2-4AC0-F184-526A-C99B66D293BC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleY";
	rename -uid "800B3E1C-44A0-3C67-C2BC-0E9BDBD2BBE8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleZ";
	rename -uid "23960F8E-45B8-1B49-14B3-D9B8E8AA61D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateX";
	rename -uid "ACBCE4C6-4DF2-3051-5DAE-BAB10EEFB177";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 32 ".ktv[0:31]"  0 3.8401813507080074 1 5.582146167755127
		 2 7.6186804771423331 3 9.685490608215332 4 11.501864433288574 5 12.787389755249023
		 6 13.273929595947266 7 13.273929595947266 12 13.273929595947266 13 13.273929595947266
		 14 16.879514694213867 15 25.941390991210937 16 36.182582855224609 17 40.978916168212891
		 18 40.978916168212891 54 40.978916168212891 55 40.978916168212891 56 37.790493011474609
		 57 30.245244979858398 58 21.863307952880859 59 15.635424613952637 60 13.273929595947266
		 61 13.273929595947266 64 13.273929595947266 65 13.273929595947266 66 12.565963745117188
		 67 10.792315483093262 68 8.4952545166015625 69 6.22802734375 70 4.5155777931213379
		 71 3.8401815891265874 72 3.8401815891265874;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateY";
	rename -uid "0930DEA5-4625-11C8-45D8-C3AF8A951507";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 32 ".ktv[0:31]"  0 4.2117342948913574 1 -1.3981891870498657
		 2 -7.8167433738708487 3 -14.21367359161377 4 -19.769496917724609 5 -23.682119369506836
		 6 -25.162002563476563 7 -25.162002563476563 12 -25.162002563476563 13 -25.162002563476563
		 14 -31.699508666992191 15 -45.328861236572266 16 -57.807762145996094 17 -63.086372375488281
		 18 -63.086372375488281 54 -63.086372375488281 55 -63.086372375488281 56 -59.600120544433594
		 57 -50.84747314453125 58 -39.595394134521484 59 -29.535396575927731 60 -25.162002563476563
		 61 -25.162002563476563 64 -25.162002563476563 65 -25.162002563476563 66 -23.008649826049805
		 67 -17.604486465454102 68 -10.541991233825684 69 -3.4484782218933105 70 2.0217182636260986
		 71 4.2117347717285156 72 4.2117347717285156;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateZ";
	rename -uid "D6A691F9-4AE4-3709-8D89-E694F7F768BB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 32 ".ktv[0:31]"  0 2.4896588325500488 1 1.6161201000213623
		 2 0.75272458791732788 3 0.037291716784238815 4 -0.4614120721817016 5 -0.73918372392654419
		 6 -0.82732129096984863 7 -0.82732129096984863 12 -0.82732129096984863 13 -0.82732129096984863
		 14 -2.8566367626190186 15 -6.8172988891601563 16 -10.03589916229248 17 -11.10146427154541
		 18 -11.10146427154541 54 -11.10146427154541 55 -11.10146427154541 56 -10.430583000183105
		 57 -8.312047004699707 58 -5.1928009986877441 59 -2.1953496932983398 60 -0.82732129096984863
		 61 -0.82732129096984863 64 -0.82732129096984863 65 -0.82732129096984863 66 -0.69592791795730591
		 67 -0.28115126490592957 68 0.42998665571212769 69 1.3246244192123413 70 2.1352977752685547
		 71 2.4896590709686279 72 2.4896590709686279;
createNode animCurveTU -n "Character1_RightHandThumb2_visibility";
	rename -uid "620EFA40-4286-5B5C-1397-F8A84FF0C66D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightHandThumb3_translateX";
	rename -uid "415CF42B-458F-0BD2-5D8A-47BB083CDF59";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.588839054107666;
createNode animCurveTL -n "Character1_RightHandThumb3_translateY";
	rename -uid "DCEF2123-4BCF-1613-EE69-15A3A1FBADAF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.13479287922382355;
createNode animCurveTL -n "Character1_RightHandThumb3_translateZ";
	rename -uid "0426869B-4E85-EAD7-6C6C-52A6F8570D49";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.4058942794799805;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleX";
	rename -uid "0316DD07-4D20-9076-3CF3-559519529EC4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleY";
	rename -uid "16C418BD-45D9-5279-01BE-BA8D0227315A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleZ";
	rename -uid "211CC719-4768-08C5-F28B-A680110D75F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateX";
	rename -uid "8DCE201A-46B6-F2FA-63A9-C0AE34949208";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3885382088574261e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateY";
	rename -uid "D9030EF0-4C1C-9450-20B8-C4B66123FA9B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.7423942457244266e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateZ";
	rename -uid "8267BDAB-4910-5E72-2370-82BDD09245CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8654235961766972e-007;
createNode animCurveTU -n "Character1_RightHandThumb3_visibility";
	rename -uid "2F3D9834-43EA-E6FE-7F79-BE9AB053A80B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightHandIndex1_translateX";
	rename -uid "4D911ACA-4886-03B4-2CCC-01943CABE67E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.25625163316726685;
createNode animCurveTL -n "Character1_RightHandIndex1_translateY";
	rename -uid "1AA28374-412B-4B1E-23AF-638769153057";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.892666816711426;
createNode animCurveTL -n "Character1_RightHandIndex1_translateZ";
	rename -uid "9B91E3AD-44A9-41C0-9655-38BB934AACAB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.533165454864502;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleX";
	rename -uid "0DA7011F-4480-F09C-03D6-B59DF0A2C396";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleY";
	rename -uid "E28DE256-43CD-C8DD-9F94-DA82A039CA5D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleZ";
	rename -uid "54A0E7FF-4824-58B3-0E3D-EAB940518397";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateX";
	rename -uid "58C358D1-4BBA-6A05-B947-0784DA5662CE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 -1.158916711807251 1 -1.8305001258850098
		 2 -2.234675407409668 3 -2.2291948795318604 4 -1.886265754699707 5 -1.4573835134506226
		 6 -1.2556071281433105 7 -1.2556071281433105 12 -1.2556071281433105 13 -1.2556071281433105
		 14 -1.7946081161499021 15 -0.83709800243377686 16 2.8783113956451416 17 5.272031307220459
		 18 5.272031307220459 54 5.272031307220459 55 5.272031307220459 56 3.6404635906219478
		 57 0.45710653066635132 58 -1.6110349893569946 59 -1.6809663772583008 60 -1.2556071281433105
		 61 -1.2556071281433105 64 -1.2556071281433105 65 -1.2556071281433105 66 -1.6049264669418335
		 67 -2.1575334072113037 68 -2.2302398681640625 69 -1.6946952342987061 70 -1.0894287824630737
		 71 -1.158916711807251 72 -1.3789664506912231 73 -1.9482262134552002 74 -2.2524471282958984
		 75 -2.2836933135986328 76 -2.279350757598877 77 -2.2128865718841553 78 -1.9988546371459959
		 79 -1.6181488037109375 80 -1.158916711807251;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateY";
	rename -uid "DCDD9DEA-4E09-18FF-01E9-D2A4AF460ABC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 1.6353100538253784 1 3.1726126670837402
		 2 5.051795482635498 3 6.9737229347229004 4 8.6163043975830078 5 9.7255287170410156
		 6 10.129959106445312 7 10.129959106445312 12 10.129959106445312 13 10.129959106445312
		 14 6.46923828125 15 -1.8329781293869019 16 -9.0934553146362305 17 -11.641811370849609
		 18 -11.641811370849609 54 -11.641811370849609 55 -11.641811370849609 56 -10.008877754211426
		 57 -5.1809678077697754 58 1.7111608982086182 59 7.7164044380187979 60 10.129959106445312
		 61 10.129959106445312 64 10.129959106445312 65 10.129959106445312 66 9.3915920257568359
		 67 7.4828124046325684 68 5.0105385780334473 69 2.7902107238769531 70 1.5119137763977051
		 71 1.6353100538253784 72 2.0595314502716064 73 3.5581080913543701 74 5.2397823333740234
		 75 6.0361762046813965 76 5.7056350708007812 77 4.8637070655822754 78 3.7459704875946045
		 79 2.5959246158599854 80 1.6353100538253784;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateZ";
	rename -uid "296D2CF4-4FCD-2143-1427-46BE2DF4C683";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 12.797025680541992 1 23.485509872436523
		 2 35.741744995117188 3 48.027542114257813 4 58.790714263916023 5 66.438873291015625
		 6 69.348670959472656 7 69.348670959472656 12 69.348670959472656 13 69.348670959472656
		 14 54.168605804443359 15 21.000724792480469 16 -12.633110046386719 17 -28.24462890625
		 18 -28.24462890625 54 -28.24462890625 55 -28.24462890625 56 -17.829309463500977 57 6.6241927146911621
		 58 35.284996032714844 59 59.228931427001946 60 69.348670959472656 61 69.348670959472656
		 64 69.348670959472656 65 69.348670959472656 66 64.090522766113281 67 51.316162109375
		 68 35.477436065673828 69 20.904020309448242 70 11.895595550537109 71 12.797025680541992
		 72 15.838609695434569 73 26.049749374389648 74 36.944694519042969 75 42.027664184570313
		 76 39.919189453125 77 34.535770416259766 78 27.287563323974609 79 19.575653076171875
		 80 12.797025680541992;
createNode animCurveTU -n "Character1_RightHandIndex1_visibility";
	rename -uid "47EF8138-48A8-3ECC-587B-4DB7D7759B0C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightHandIndex2_translateX";
	rename -uid "BC2F5EFA-4AEF-3C68-C4EC-98AA0A4B8F9B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.3959341049194336;
createNode animCurveTL -n "Character1_RightHandIndex2_translateY";
	rename -uid "D75F27CF-4E85-064F-EF80-8CBD8DDC2C67";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8172028064727783;
createNode animCurveTL -n "Character1_RightHandIndex2_translateZ";
	rename -uid "1D6E1E2B-4EE9-1153-43AA-D996F0C54CC9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.52152138948440552;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleX";
	rename -uid "00A5E9E1-413B-32D8-43A1-158821C3AFC9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleY";
	rename -uid "5429807C-44E6-2001-E62A-A092B5509136";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleZ";
	rename -uid "37BED879-4ED6-0B46-6E5A-0D9B4A8E0DBF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateX";
	rename -uid "65EEC58C-48A9-96B8-8462-B7A66A52F0B4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 11.788410186767578 1 16.967144012451172
		 2 21.432676315307617 3 23.9234619140625 4 23.176218032836914 5 18.751609802246094
		 6 15.230603218078615 7 15.230603218078615 12 15.230603218078615 13 15.230603218078615
		 14 18.565505981445313 15 23.572397232055664 16 26.583114624023438 17 27.528047561645508
		 18 27.528047561645508 54 27.528047561645508 55 27.528047561645508 56 26.923377990722656
		 57 25.053400039672852 58 21.731346130371094 59 17.542850494384766 60 15.230603218078615
		 61 15.230603218078615 64 15.230603218078615 65 15.230603218078615 66 20.490972518920898
		 67 24.091323852539063 68 21.787431716918945 69 16.619417190551758 70 12.051594734191895
		 71 11.788410186767578 72 12.500596046447754 73 16.157094955444336 74 19.557224273681641
		 75 20.901948928833008 76 20.406412124633789 77 19.019811630249023 78 16.892740249633789
		 79 14.316302299499512 80 11.788410186767578;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateY";
	rename -uid "C5013988-44A9-D836-804F-53B157884AC1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 -15.640760421752931 1 -25.895368576049805
		 2 -38.327915191650391 3 -51.199371337890625 4 -62.528541564941406 5 -70.359794616699219
		 6 -73.199981689453125 7 -73.199981689453125 12 -73.199981689453125 13 -73.199981689453125
		 14 -71.194541931152344 15 -66.597091674804687 16 -61.860485076904304 17 -59.683914184570305
		 18 -59.683914184570305 54 -59.683914184570305 55 -59.683914184570305 56 -61.133781433105469
		 57 -64.56915283203125 58 -68.600189208984375 59 -71.872886657714844 60 -73.199981689453125
		 61 -73.199981689453125 64 -73.199981689453125 65 -73.199981689453125 66 -68.282722473144531
		 67 -55.616683959960937 68 -39.613975524902344 69 -25.104856491088867 70 -16.095767974853516
		 71 -15.640760421752931 72 -16.886030197143555 73 -24.080963134765625 74 -32.461166381835938
		 75 -36.529102325439453 76 -34.958858489990234 77 -30.983608245849609 78 -25.724691390991211
		 79 -20.276338577270508 80 -15.640760421752931;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateZ";
	rename -uid "9BC5194D-4EFD-59B4-DECB-4296178E97CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 15.306336402893066 1 23.219087600708008
		 2 32.027423858642578 3 41.459175109863281 4 51.845500946044922 5 62.801605224609368
		 6 68.75714111328125 7 68.75714111328125 12 68.75714111328125 13 68.75714111328125
		 14 64.15093994140625 15 56.298168182373047 16 50.361282348632812 17 48.055324554443359
		 18 48.055324554443359 54 48.055324554443359 55 48.055324554443359 56 49.568195343017578
		 57 53.568096160888672 58 59.373416900634759 59 65.600135803222656 60 68.75714111328125
		 61 68.75714111328125 64 68.75714111328125 65 68.75714111328125 66 59.317897796630866
		 67 45.112957000732422 68 32.931423187255859 69 22.639965057373047 70 15.680549621582031
		 71 15.306336402893066 72 16.324245452880859 73 21.8839111328125 74 27.913896560668945
		 75 30.766731262207035 76 29.667160034179688 77 26.871088027954102 78 23.09437370300293
		 79 19.004871368408203 80 15.306336402893066;
createNode animCurveTU -n "Character1_RightHandIndex2_visibility";
	rename -uid "F4FBB9A1-4FCE-31E1-07B5-D79988D15945";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightHandIndex3_translateX";
	rename -uid "42D90E55-4224-C74D-B45F-998AF26C90F7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6282989978790283;
createNode animCurveTL -n "Character1_RightHandIndex3_translateY";
	rename -uid "AF964125-4ADB-018C-95B3-42B3802D726E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.2120883464813232;
createNode animCurveTL -n "Character1_RightHandIndex3_translateZ";
	rename -uid "3C160748-4EB7-8C58-8702-9CAEA3239E4C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7757487297058105;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleX";
	rename -uid "5C9BF8D3-4F88-48A0-6F8E-DBBE12FEE352";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleY";
	rename -uid "DABA1CC3-4BD8-4F8E-05B9-1999E00EBC51";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleZ";
	rename -uid "238D11FF-4502-748C-AEB1-97B392DE4DB7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateX";
	rename -uid "F5E549BC-444F-16D3-993B-138CFFD2FFB9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.6657853519936907e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateY";
	rename -uid "A7DB6045-4993-73D9-B18C-139DDE24C023";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0339611122844872e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateZ";
	rename -uid "933D04EF-4AFB-7B20-7092-D9BA88BC3939";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7499066018444864e-007;
createNode animCurveTU -n "Character1_RightHandIndex3_visibility";
	rename -uid "93876BDD-4BDD-57DA-7C1A-2BB2E84CC11F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightHandRing1_translateX";
	rename -uid "125F1E2F-40A1-6F7E-6CEF-DCB3C95019D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8543868064880371;
createNode animCurveTL -n "Character1_RightHandRing1_translateY";
	rename -uid "7F6CB949-45AE-160A-30D6-A3BD2F5242F7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2156095504760742;
createNode animCurveTL -n "Character1_RightHandRing1_translateZ";
	rename -uid "2FFFD1EF-4CF3-5737-3A83-BFB52D30F95D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2236912250518799;
createNode animCurveTU -n "Character1_RightHandRing1_scaleX";
	rename -uid "A21F4F5C-4455-1F2F-BFFE-23B18A2CB7DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandRing1_scaleY";
	rename -uid "E2ABF004-4CE4-DB59-2881-7B8E6FEA9FC3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing1_scaleZ";
	rename -uid "495097DF-4465-23C5-5095-B896733F4318";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandRing1_rotateX";
	rename -uid "86194C51-4B56-0DD5-7925-64804A52DFE3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 -9.2360963821411133 1 -9.6919374465942383
		 2 -9.9889259338378906 3 -10.031298637390137 4 -9.8513698577880859 5 -9.5987167358398437
		 6 -9.4753284454345703 7 -9.4753284454345703 12 -9.4753284454345703 13 -9.4753284454345703
		 14 -9.1393680572509766 15 -6.7871832847595215 16 -2.3712239265441895 17 0.22813262045383451
		 18 0.22813262045383451 54 0.22813262045383451 55 0.22813262045383451 56 -1.5383807420730591
		 57 -5.1243762969970703 58 -8.0651121139526367 59 -9.3041305541992187 60 -9.4753284454345703
		 61 -9.4753284454345703 64 -9.4753284454345703 65 -9.4753284454345703 66 -9.6926689147949219
		 67 -9.9861230850219727 68 -9.9070606231689453 69 -9.4469079971313477 70 -9.0460691452026367
		 71 -9.2360963821411133 72 -9.5381355285644531 73 -10.057736396789551 74 -10.246620178222656
		 75 -10.211747169494629 76 -10.233828544616699 77 -10.222715377807617 78 -10.057515144348145
		 79 -9.6990718841552734 80 -9.2360963821411133;
createNode animCurveTA -n "Character1_RightHandRing1_rotateY";
	rename -uid "996770FF-4CD3-6300-E5C0-3081CA3878A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 4.393496036529541 1 6.3734712600708008
		 2 8.7385978698730469 3 11.169281959533691 4 13.312496185302734 5 14.82593822479248
		 6 15.397130966186525 7 15.397130966186525 12 15.397130966186525 13 15.397130966186525
		 14 10.392468452453613 15 -0.42896488308906555 16 -10.105225563049316 17 -13.846923828125
		 18 -13.846923828125 54 -13.846923828125 55 -13.846923828125 56 -11.410399436950684
		 57 -4.7974796295166016 58 4.1526103019714355 59 12.069714546203613 60 15.397130966186525
		 61 15.397130966186525 64 15.397130966186525 65 15.397130966186525 66 14.282214164733887
		 67 11.565824508666992 68 8.2603626251220703 69 5.4038939476013184 70 3.8677296638488765
		 71 4.393496036529541 72 5.4044065475463867 73 8.0949850082397461 74 10.962140083312988
		 75 12.297457695007324 76 11.708212852478027 77 10.208502769470215 78 8.2137222290039062
		 79 6.1453018188476563 80 4.393496036529541;
createNode animCurveTA -n "Character1_RightHandRing1_rotateZ";
	rename -uid "06651D5F-48E0-F83B-6ADE-3F9981E98388";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 29.838140487670898 1 37.394435882568359
		 2 46.047027587890625 3 54.713031768798828 4 62.304595947265625 5 67.702690124511719
		 6 69.758003234863281 7 69.758003234863281 12 69.758003234863281 13 69.758003234863281
		 14 57.893684387207038 15 31.731424331665043 16 4.6495280265808105 17 -8.1642436981201172
		 18 -8.1642436981201172 54 -8.1642436981201172 55 -8.1642436981201172 56 0.40341034531593323
		 57 20.235198974609375 58 43.0523681640625 59 61.852977752685554 60 69.758003234863281
		 61 69.758003234863281 64 69.758003234863281 65 69.758003234863281 66 65.771652221679688
		 67 56.164989471435547 68 44.402641296386719 69 33.840847015380859 70 27.845773696899414
		 71 29.838140487670898 72 33.644477844238281 73 43.481307983398437 74 53.609058380126953
		 75 58.278194427490234 76 56.223640441894531 77 50.983509063720703 78 43.935554504394531
		 79 36.436878204345703 80 29.838140487670898;
createNode animCurveTU -n "Character1_RightHandRing1_visibility";
	rename -uid "C43F82FE-4D40-85B9-6670-E99742979363";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightHandRing2_translateX";
	rename -uid "BE74A109-480C-F25D-A2EC-629A2153D046";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.062777042388916;
createNode animCurveTL -n "Character1_RightHandRing2_translateY";
	rename -uid "3EB98FCA-4628-AA86-C113-AF9E238A7749";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.4930424690246582;
createNode animCurveTL -n "Character1_RightHandRing2_translateZ";
	rename -uid "EFCB701A-448D-32B6-D930-5C879EE15FD1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.86127841472625732;
createNode animCurveTU -n "Character1_RightHandRing2_scaleX";
	rename -uid "BE7A0BFD-4698-066E-EE18-F8BB22AC91BE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandRing2_scaleY";
	rename -uid "A4A6FD07-4091-D102-8C9B-1F8F1B3FA0F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing2_scaleZ";
	rename -uid "BD1E3915-4367-54EC-A56E-7A9032ED8B7E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandRing2_rotateX";
	rename -uid "E0804ABF-48B9-A0C3-7439-D8A4D49C8FA4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 20.286947250366211 1 22.326713562011719
		 2 23.778127670288086 3 23.776739120483398 4 21.561958312988281 5 17.465385437011719
		 6 14.933642387390137 7 14.933642387390137 12 14.933642387390137 13 14.933642387390137
		 14 20.377630233764648 15 26.489231109619141 16 29.008480072021481 17 29.549201965332031
		 18 29.549201965332031 54 29.549201965332031 55 29.549201965332031 56 29.221796035766602
		 57 27.852678298950195 58 24.508489608764648 59 18.835514068603516 60 14.933642387390137
		 61 14.933642387390137 64 14.933642387390137 65 14.933642387390137 66 19.123630523681641
		 67 23.445493698120117 68 23.714414596557617 69 21.764522552490234 70 19.962827682495117
		 71 20.286947250366211 72 21.06660270690918 73 22.963790893554687 74 23.976890563964844
		 75 23.952346801757813 76 24.010723114013672 77 23.853883743286133 78 23.101234436035156
		 79 21.787752151489258 80 20.286947250366211;
createNode animCurveTA -n "Character1_RightHandRing2_rotateY";
	rename -uid "B7AB4B70-4F4D-F563-6272-3198D423C6D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 -34.944370269775391 1 -42.211139678955078
		 2 -50.679004669189453 3 -59.205268859863288 4 -66.576683044433594 5 -71.635940551757812
		 6 -73.483367919921875 7 -73.483367919921875 12 -73.483367919921875 13 -73.483367919921875
		 14 -69.461708068847656 15 -60.200706481933587 16 -50.745693206787109 17 -46.4388427734375
		 18 -46.4388427734375 54 -46.4388427734375 55 -46.4388427734375 56 -49.304988861083984
		 57 -56.139228820800781 58 -64.229843139648438 59 -70.827293395996094 60 -73.483367919921875
		 61 -73.483367919921875 64 -73.483367919921875 65 -73.483367919921875 66 -70.0341796875
		 67 -61.228054046630859 68 -50.077499389648438 69 -39.956180572509766 70 -33.964767456054688
		 71 -34.944370269775391 72 -37.4591064453125 73 -45.19049072265625 74 -53.440284729003906
		 75 -57.279514312744141 76 -55.654205322265625 77 -51.501419067382813 78 -45.925502777099609
		 79 -40.044395446777344 80 -34.944370269775391;
createNode animCurveTA -n "Character1_RightHandRing2_rotateZ";
	rename -uid "E9702466-441C-52C2-F487-35B238C68440";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 40 ".ktv[0:39]"  0 29.386693954467773 1 34.454986572265625
		 2 40.674541473388672 3 47.95001220703125 4 56.268497467041016 5 64.582908630371094
		 6 68.698143005371094 7 68.698143005371094 12 68.698143005371094 13 68.698143005371094
		 14 61.089523315429695 15 50.074554443359375 16 42.366935729980469 17 39.340602874755859
		 18 39.340602874755859 54 39.340602874755859 55 39.340602874755859 56 41.332447052001953
		 57 46.519088745117188 58 54.194671630859375 59 63.359031677246094 60 68.698143005371094
		 61 68.698143005371094 64 68.698143005371094 65 68.698143005371094 66 61.576679229736335
		 67 49.964900970458984 68 40.210552215576172 69 32.871559143066406 70 28.705911636352543
		 71 29.386693954467773 72 31.132625579833988 73 36.581069946289063 74 42.870567321777344
		 75 46.159721374511719 76 44.727485656738281 77 41.316669464111328 78 37.113800048828125
		 79 32.933216094970703 80 29.386693954467773;
createNode animCurveTU -n "Character1_RightHandRing2_visibility";
	rename -uid "B59C77FC-483B-3B2B-A50E-CEBBFBB7764E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_RightHandRing3_translateX";
	rename -uid "C92F5C8F-4C78-8929-E901-C2959A9AC068";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.5035178661346436;
createNode animCurveTL -n "Character1_RightHandRing3_translateY";
	rename -uid "23F36722-42CE-4342-B6D4-F0BCB9946631";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.9153993129730225;
createNode animCurveTL -n "Character1_RightHandRing3_translateZ";
	rename -uid "D7D9F075-455B-2F5E-76BD-A4BB72AE17B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.6898331642150879;
createNode animCurveTU -n "Character1_RightHandRing3_scaleX";
	rename -uid "0FF159EB-487E-13BA-43FD-1CB6BB02F82A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing3_scaleY";
	rename -uid "ED8FED8A-47FE-4EDB-8891-D48069199BB4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing3_scaleZ";
	rename -uid "18D8E30D-4D8E-8F08-88DF-45A815DA0827";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandRing3_rotateX";
	rename -uid "D42EB0FD-4283-1CF3-1815-70A0911BACA1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.997153731281287e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateY";
	rename -uid "53E6755C-49F2-8B69-5234-9AADD9D8D568";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.6512589695594215e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateZ";
	rename -uid "A65873A9-4127-0C12-8292-E681C6B5F2F6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.9210230561839126e-007;
createNode animCurveTU -n "Character1_RightHandRing3_visibility";
	rename -uid "F7E5D2D1-4457-C9BF-7FB7-D88CBF34E466";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_Neck_translateX";
	rename -uid "368C1359-4B89-630D-CFBD-D8B3525F84D2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 12.791294097900391;
createNode animCurveTL -n "Character1_Neck_translateY";
	rename -uid "407C67EA-4E60-FE75-7709-47AD86790B8B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.3580474853515625;
createNode animCurveTL -n "Character1_Neck_translateZ";
	rename -uid "02A6DCCC-4378-49C3-9B48-38827218CE88";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.6322288513183594;
createNode animCurveTU -n "Character1_Neck_scaleX";
	rename -uid "8C31A09A-43B5-D02F-F3EA-578F54BD18CA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_Neck_scaleY";
	rename -uid "F8827BCE-4A80-41F5-AE23-83AB5F5573A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Neck_scaleZ";
	rename -uid "F62F5A71-4A54-00BA-E3B0-FB8F2A9BF201";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Neck_rotateX";
	rename -uid "AB377AC2-40D2-5DA7-EC01-6380731BF79E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 9.5831975936889648 1 8.7478923797607422
		 2 8.073699951171875 3 7.4432687759399414 4 6.6996860504150391 5 5.6540389060974121
		 6 4.1144547462463379 7 1.2473194599151611 8 -2.5800681114196777 9 -5.8735132217407227
		 10 -7.4657087326049805 11 -6.8337278366088867 12 -5.1499648094177246 13 -3.6484591960906982
		 14 -2.8676705360412598 15 -2.5651230812072754 16 -2.539961576461792 17 -2.8530008792877197
		 18 -3.3641557693481445 19 -3.8666746616363521 20 -4.0053410530090332 21 -3.7268164157867432
		 22 -3.2170498371124268 23 -2.5784730911254883 24 -1.8105889558792114 25 -0.89192467927932739
		 26 0.32501527667045593 27 1.8426233530044558 28 3.4470973014831543 29 4.9006857872009277
		 30 5.931180477142334 31 6.3327999114990234 32 6.2651238441467285 33 5.9783248901367188
		 34 5.7285251617431641 35 5.7796716690063477 36 6.3337273597717285 37 7.2145142555236825
		 38 8.1109762191772461 39 8.6985416412353516 40 8.661442756652832 41 7.9163341522216797
		 42 6.784489631652832 43 5.5329709053039551 44 4.3073530197143555 45 3.1199941635131836
		 46 1.5968257188796997 47 -0.36348804831504822 48 -2.394662618637085 49 -4.0937948226928711
		 50 -5.0301671028137207 51 -4.8967604637145996 52 -3.9711513519287109 53 -2.6964211463928223
		 54 -1.5218515396118164 55 -0.91439110040664673 56 -1.1887305974960327 57 -2.1666109561920166
		 58 -3.4959523677825928 59 -4.6519298553466797 60 -4.981076717376709 61 -4.1625447273254395
		 62 -2.5769984722137451 63 -0.61417025327682495 64 1.3872090578079224 65 3.1864268779754639
		 66 4.9679527282714844 67 6.8864254951477051 68 8.5056324005126953 69 9.6398792266845703
		 70 10.404939651489258 71 10.721267700195312 72 10.505465507507324 73 9.9217424392700195
		 74 9.2791318893432617 75 8.8985424041748047 76 8.8555335998535156 77 8.966893196105957
		 78 9.1690998077392578 79 9.3967351913452148 80 9.5831975936889648;
createNode animCurveTA -n "Character1_Neck_rotateY";
	rename -uid "58662DA1-4A23-B42D-7C61-F6B7E11E37F2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -8.7274017333984375 1 -10.495082855224609
		 2 -12.604039192199707 3 -14.702137947082521 4 -16.461614608764648 5 -17.574226379394531
		 6 -17.728561401367188 7 -15.701068878173826 8 -11.448080062866211 9 -6.5381889343261719
		 10 -2.8159327507019043 11 -1.1576628684997559 12 -0.68345540761947632 13 -0.58462369441986084
		 14 0.12010919302701949 15 2.8725526332855225 16 6.2037386894226074 17 6.716517448425293
		 18 2.8106982707977295 19 -3.3288466930389404 20 -9.4287118911743164 21 -13.348648071289063
		 22 -14.572901725769045 23 -14.297225952148437 24 -13.036632537841797 25 -11.296093940734863
		 26 -9.0072641372680664 27 -6.0421991348266602 28 -2.8128781318664551 29 0.28158316016197205
		 30 2.8557372093200684 31 4.9279952049255371 32 6.7777853012084961 33 8.4054384231567383
		 34 9.7989120483398437 35 10.938653945922852 36 12.079354286193848 37 13.221118927001953
		 38 13.963384628295898 39 13.916234016418457 40 12.69898796081543 41 9.713505744934082
		 42 5.2483596801757812 43 0.23857471346855164 44 -4.4270086288452148 45 -7.9184861183166495
		 46 -10.28081226348877 47 -12.116096496582031 48 -13.430506706237793 49 -14.212542533874512
		 50 -14.454751014709473 51 -13.941047668457031 52 -12.670669555664063 53 -10.969845771789551
		 54 -9.1577987670898437 55 -7.5437436103820801 56 -6.0034222602844238 57 -4.3745779991149902
		 58 -2.8865032196044922 59 -1.6958250999450684 60 -0.87238800525665283 61 -0.24613015353679654
		 62 0.24449764192104337 63 0.39353767037391663 64 0.043051686137914658 65 -0.8977414369583131
		 66 -2.5067930221557617 67 -4.8625073432922363 68 -6.7957277297973633 69 -7.9142084121704093
		 70 -8.6805219650268555 71 -9.3049755096435547 72 -9.9570293426513672 73 -10.581256866455078
		 74 -11.038080215454102 75 -11.182475090026855 76 -10.966980934143066 77 -10.510126113891602
		 78 -9.9157514572143555 79 -9.2873592376708984 80 -8.7274017333984375;
createNode animCurveTA -n "Character1_Neck_rotateZ";
	rename -uid "ADD469E8-4392-532E-0F70-86B20A000A2B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 0.27945694327354431 1 -1.751987099647522
		 2 -3.9214246273040771 3 -6.1141986846923828 4 -8.1736907958984375 5 -9.9073152542114258
		 6 -11.120160102844238 7 -12.036291122436523 8 -12.773555755615234 9 -12.814657211303711
		 10 -11.119670867919922 11 -5.7895264625549316 12 2.3078086376190186 13 10.319979667663574
		 14 15.300200462341307 15 14.515695571899414 16 10.090054512023926 17 6.3425474166870117
		 18 4.5214285850524902 19 2.8695409297943115 20 1.3378483057022095 21 0.19777083396911621
		 22 -0.056113149970769882 23 0.45987695455551142 24 1.3141368627548218 25 2.053342342376709
		 26 2.5455970764160156 27 3.0257043838500977 28 3.5837328433990474 29 4.2850103378295898
		 30 5.1693387031555176 31 6.3138728141784668 32 7.6465964317321777 33 8.9938249588012695
		 34 10.208430290222168 35 11.163213729858398 36 12.07131290435791 37 13.04549503326416
		 38 13.807133674621582 39 14.053623199462891 40 13.465486526489258 41 11.536601066589355
		 42 8.4169158935546875 43 4.8002281188964844 44 1.4645882844924927 45 -0.70125705003738403
		 46 -1.2151023149490356 47 -0.56584286689758301 48 0.68029320240020752 49 1.9034218788146973
		 50 2.4623348712921143 51 2.466724157333374 52 2.3251206874847412 53 1.8530930280685425
		 54 0.83631050586700439 55 -0.97963941097259533 56 -4.393958568572998 57 -9.2451858520507813
		 58 -14.366248130798342 59 -18.600711822509766 60 -20.795312881469727 61 -20.207693099975586
		 62 -17.575101852416992 63 -13.968494415283203 64 -10.495370864868164 65 -8.303675651550293
		 66 -7.528825283050538 67 -7.8387737274169922 68 -7.9791059494018546 69 -7.3930172920227051
		 70 -6.536980152130127 71 -5.5372171401977539 72 -4.3357095718383789 73 -2.9577963352203369
		 74 -1.6695964336395264 75 -0.74529409408569336 76 -0.25532412528991699 77 -0.014619470573961735
		 78 0.083793625235557556 79 0.14660580456256866 80 0.27945694327354431;
createNode animCurveTU -n "Character1_Neck_visibility";
	rename -uid "972F03AF-4826-E068-941C-598E7A956645";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "Character1_Head_translateX";
	rename -uid "0D759366-4AD0-0607-CB36-FAADECD3EF10";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.627285957336426;
createNode animCurveTL -n "Character1_Head_translateY";
	rename -uid "2A3D3329-44B2-4F1A-1276-B3A20604C857";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.64882755279541;
createNode animCurveTL -n "Character1_Head_translateZ";
	rename -uid "AAEF7D2C-4291-7692-D4CA-2EBB2753EDE5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.1324095726013184;
createNode animCurveTU -n "Character1_Head_scaleX";
	rename -uid "64B48B85-4E45-1FE2-A266-638748DE76A8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Head_scaleY";
	rename -uid "93F18BE6-4F97-682E-4B8B-CA93F20925C3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Head_scaleZ";
	rename -uid "8CBFDE88-4F70-E8CE-D781-2181BA33781E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_Head_rotateX";
	rename -uid "59B0F8E6-4DF2-B6F2-358C-8F81407BF746";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 5.3095211982727051 1 7.9813899993896484
		 2 10.949068069458008 3 13.918181419372559 4 16.593473434448242 5 18.678705215454102
		 6 19.877050399780273 7 19.460649490356445 8 17.444028854370117 9 14.644687652587891
		 10 11.87532901763916 11 9.020782470703125 12 5.7558960914611816 13 2.5744380950927734
		 14 -0.02841559611260891 15 -2.3511362075805664 16 -3.9375426769256592 17 -3.1594228744506836
		 18 1.0637375116348267 19 7.3179736137390128 20 13.49201488494873 21 17.477680206298828
		 22 18.600597381591797 23 18.023473739624023 24 16.39415168762207 25 14.360476493835451
		 26 11.896981239318848 27 8.7905607223510742 28 5.4084315299987793 29 2.1185567378997803
		 30 -0.71093791723251343 31 -3.0961158275604248 32 -5.2821850776672363 33 -7.2449817657470694
		 34 -8.9601602554321289 35 -10.403111457824707 36 -11.922380447387695 37 -13.534162521362305
		 38 -14.715237617492674 39 -14.942630767822264 40 -13.693534851074219 41 -10.227768898010254
		 42 -4.8939833641052246 43 1.1976412534713745 44 6.936805248260498 45 11.212881088256836
		 46 13.941784858703613 47 15.863414764404297 48 17.103984832763672 49 17.789985656738281
		 50 18.047330856323242 51 17.533058166503906 52 16.163181304931641 53 14.451654434204102
		 54 12.913164138793945 55 12.063117980957031 56 12.187034606933594 57 12.941749572753906
		 58 13.899757385253906 59 14.633355140686033 60 14.714616775512695 61 13.779689788818359
		 62 12.113704681396484 63 10.262057304382324 64 8.7699613571166992 65 8.1825904846191406
		 66 8.6232919692993164 67 10.004181861877441 68 11.03032398223877 69 11.188302993774414
		 70 10.991164207458496 71 10.683066368103027 72 10.382139205932617 73 10.027216911315918
		 74 9.5935564041137695 75 9.0564632415771484 76 8.4045820236206055 77 7.6673884391784677
		 78 6.8814282417297363 79 6.0832724571228027 80 5.3095211982727051;
createNode animCurveTA -n "Character1_Head_rotateY";
	rename -uid "57DAA4BF-44FD-4127-34E0-C5829E9DB8AF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 11.555663108825684 1 11.554347038269043
		 2 11.78528881072998 3 12.01502799987793 4 12.010575294494629 5 11.539605140686035
		 6 10.370342254638672 7 7.5619392395019522 8 3.2469441890716553 9 -1.1962512731552124
		 10 -4.3878717422485352 11 -5.828331470489502 12 -6.2977547645568848 13 -6.3340864181518555
		 14 -6.4748764038085938 15 -6.8266940116882324 16 -7.0447478294372559 17 -6.9898695945739746
		 18 -6.602412223815918 19 -6.013577938079834 20 -5.371638298034668 21 -4.8203244209289551
		 22 -4.4602947235107422 23 -4.1917505264282227 24 -3.8527376651763916 25 -3.2814316749572754
		 26 -2.3052473068237305 27 -1.0382181406021118 28 0.25274327397346497 29 1.3013937473297119
		 30 1.8417489528656004 31 1.6756912469863892 32 0.98101258277893066 33 0.055441658943891525
		 34 -0.80336666107177734 35 -1.2978196144104004 36 -1.3336731195449829 37 -1.1092658042907715
		 38 -0.76627779006958008 39 -0.44677677750587463 40 -0.29258480668067932 41 -0.22079950571060181
		 42 -0.13472023606300354 43 -0.15750704705715179 44 -0.41511473059654236 45 -1.0324181318283081
		 46 -2.3115098476409912 47 -4.1693692207336426 48 -6.1506953239440918 49 -7.8004808425903311
		 50 -8.6639804840087891 51 -8.5411653518676758 52 -7.7353301048278809 53 -6.5467429161071777
		 54 -5.2751297950744629 55 -4.2197036743164062 56 -3.3593428134918213 57 -2.4944388866424561
		 58 -1.6563372611999512 59 -0.87620073556900024 60 -0.1849949061870575 61 0.23110261559486389
		 62 0.39245432615280151 63 0.57817602157592773 64 1.0679283142089844 65 2.1417045593261719
		 66 3.9388439655303955 67 6.4263186454772949 68 8.5072250366210937 69 9.8271942138671875
		 70 10.740841865539551 71 11.21526050567627 72 11.188863754272461 73 10.817588806152344
		 74 10.394827842712402 75 10.21396541595459 76 10.334837913513184 77 10.58478832244873
		 78 10.908465385437012 79 11.250533103942871 80 11.555663108825684;
createNode animCurveTA -n "Character1_Head_rotateZ";
	rename -uid "51C3C6C5-4817-86B2-9A51-EBBFB510F1AC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 5.2613430023193359 1 4.3343253135681152
		 2 3.4958665370941162 3 2.6637704372406006 4 1.7533301115036011 5 0.67748671770095825
		 6 -0.65264952182769775 7 -3.2759678363800049 8 -6.9212789535522461 9 -9.7048177719116211
		 10 -9.7323894500732422 11 -5.1064138412475586 12 2.7095675468444824 13 10.56764030456543
		 14 15.31456470489502 15 13.931766510009766 16 8.8750944137573242 17 5.1963343620300293
		 18 4.5377717018127441 19 4.8489861488342285 20 5.6265754699707031 21 6.3327383995056152
		 22 7.0173406600952148 23 7.8883566856384268 24 8.6309947967529297 25 8.9309139251708984
		 26 8.6365327835083008 27 7.9699549674987793 28 7.1742939949035653 29 6.4906067848205566
		 30 6.157416820526123 31 6.3013515472412109 32 6.7618656158447266 33 7.3445763587951669
		 34 7.8555026054382333 35 8.1012916564941406 36 8.0362462997436523 37 7.7890453338623047
		 38 7.4265122413635263 39 7.0141382217407227 40 6.618619441986084 41 6.1987705230712891
		 42 5.723935604095459 43 5.2734560966491699 44 4.910494327545166 45 4.6801447868347168
		 46 4.6331048011779785 47 4.7275390625 48 4.8626246452331543 49 4.9413609504699707
		 50 4.8696670532226562 51 4.9018664360046387 52 5.101891040802002 53 5.0905842781066895
		 54 4.487480640411377 55 2.9110343456268311 56 -0.46214130520820618 57 -5.3780841827392578
		 58 -10.60234260559082 59 -14.901239395141602 60 -17.041841506958008 61 -16.363363265991211
		 62 -13.68631649017334 63 -10.000792503356934 64 -6.2995223999023437 65 -3.5778658390045166
		 66 -1.8089325428009033 67 -0.59552955627441406 68 0.51890301704406738 69 1.6745688915252686
		 70 2.733492374420166 71 3.7366650104522705 72 4.840632438659668 73 6.0006546974182129
		 74 6.9555587768554687 75 7.4444756507873535 76 7.3953113555908212 77 7.0068502426147461
		 78 6.4246726036071777 79 5.7943263053894043 80 5.2613430023193359;
createNode animCurveTU -n "Character1_Head_visibility";
	rename -uid "3E93B901-4EA2-39D5-D32D-2B886A4DD513";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode animCurveTL -n "eyes_translateX";
	rename -uid "12F4A1C8-4EF7-2741-A2F6-6990287FCF82";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 30.947149276733398 1 29.381168365478516
		 2 29.381168365478516 12 29.381168365478516 13 29.381168365478516 14 30.947149276733398
		 15 30.947149276733398;
createNode animCurveTL -n "eyes_translateY";
	rename -uid "81737954-4CEF-B349-D4BC-BAABF4C846C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -5.0992542810490704e-007 1 2.5438647270202637
		 2 2.5438647270202637 12 2.5438647270202637 13 2.5438647270202637 14 -5.0992542810490704e-007
		 15 -5.0992542810490704e-007;
createNode animCurveTL -n "eyes_translateZ";
	rename -uid "5DFA1263-453A-E0E5-9D19-C0830E72E65A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1.121469657048256e-013 1 0.23577801883220673
		 2 0.23577801883220673 12 0.23577801883220673 13 0.23577801883220673 14 1.121469657048256e-013
		 15 1.121469657048256e-013;
createNode animCurveTU -n "eyes_scaleX";
	rename -uid "153BF4F1-4450-F08E-8927-F38E5719EDA2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleY";
	rename -uid "B926E050-4A20-6A25-3E7C-7689C1496E0E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleZ";
	rename -uid "DD9C9C4C-49E6-6781-C318-6BA74F2E86B1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "eyes_rotateX";
	rename -uid "C87D7EB8-40E5-BA11-FE28-2B9C92AFDB8B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  12 0 13 0 14 -0.70026671886444092 15 0.86844944953918457
		 16 2.7246534824371338 17 4.6957292556762695 18 6.609062671661377 19 8.2920360565185547
		 20 9.5720367431640625 21 10.643630981445313 22 11.599987983703613 23 12.116389274597168
		 24 11.86811637878418 25 10.530452728271484 26 5.581547737121582 27 -1.7921144962310791
		 28 -6.3527512550354004 29 -7.6424717903137216 30 -8.6916351318359375 31 -9.4948358535766602
		 32 -10.046666145324707 33 -10.341720581054687 34 -10.374590873718262 35 -10.139870643615723
		 36 -9.6321544647216797 37 -8.8460350036621094 38 -7.7761044502258301 39 -2.9720413684844971
		 40 4.8452939987182617 41 9.4245367050170898 42 9.9461345672607422 43 9.8455686569213867
		 44 9.2540292739868164 45 8.3027000427246094 46 7.1227707862854004 47 5.8454279899597168
		 48 4.6018581390380859 49 3.5232491493225098 50 2.7407877445220947 51 2.3628056049346924
		 52 2.2887840270996094 53 2.3366341590881348 54 2.286651611328125 55 2.160534143447876
		 56 2.1382946968078613 57 2.399946928024292 58 3.1255042552947998 59 4.5758671760559082
		 60 6.6411752700805664 61 8.9453010559082031 62 11.112112998962402 63 12.765486717224121
		 64 13.529291152954102 65 13.182685852050781 66 11.982403755187988 67 10.268682479858398
		 68 8.3817567825317383 69 6.6618618965148926 70 5.4492359161376953 71 4.6607799530029297
		 72 3.9745478630065918 73 3.3724992275238037 74 2.8365952968597412 75 2.3487963676452637
		 76 1.8910634517669678 77 1.4453567266464233 78 0.99363690614700317 79 0.51786446571350098
		 80 0;
createNode animCurveTA -n "eyes_rotateY";
	rename -uid "8B941A71-4459-4EAE-C01B-3BB0D8DDB52C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  12 0 13 0 14 0.57024884223937988 15 0.53410524129867554
		 16 0.44806292653083796 17 0.3300502598285675 18 0.19799543917179108 19 0.069826729595661163
		 20 -0.036527629941701889 21 -0.035106666386127472 22 0.060004357248544686 23 0.1166643500328064
		 24 0.0027322233654558659 25 -0.41393312811851501 26 -1.7685476541519165 27 -3.7193822860717773
		 28 -4.933159351348877 29 -5.312955379486084 30 -5.6453032493591309 31 -5.9221076965332031
		 32 -6.1352753639221191 33 -6.2767124176025391 34 -6.338322639465332 35 -6.3120136260986328
		 36 -6.1896910667419434 37 -5.9632601737976074 38 -5.624626636505127 39 -4.0030827522277832
		 40 -1.3446952104568481 41 0.23346303403377533 42 0.41411039233207703 43 0.33719328045845032
		 44 0.071481257677078247 45 -0.31425625085830688 46 -0.75124961137771606 47 -1.1707297563552856
		 48 -1.5039266347885132 49 -1.6820712089538574 50 -1.6363937854766846 51 -0.30308672785758972
		 52 1.8067672252655027 53 2.3996016979217529 54 0.82619810104370117 55 -1.663475513458252
		 56 -4.5142045021057129 57 -7.1707739830017081 58 -9.0779705047607422 59 -10.283890724182129
		 60 -11.21464729309082 61 -11.882049560546875 62 -12.297906875610352 63 -12.474027633666992
		 64 -12.422220230102539 65 -12.026093482971191 66 -11.252920150756836 67 -10.240010261535645
		 68 -9.1246757507324219 69 -8.044224739074707 70 -7.1359677314758301 71 -6.3715095520019531
		 72 -5.6284008026123047 73 -4.9028730392456055 74 -4.1911602020263672 75 -3.4894938468933105
		 76 -2.7941067218780518 77 -2.1012313365936279 78 -1.4070999622344971 79 -0.70794564485549927
		 80 0;
createNode animCurveTA -n "eyes_rotateZ";
	rename -uid "EC23DEF4-4BB3-5B2B-480E-E9A5D04BA8AD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  12 0 13 0 14 7.0144853591918945 15 6.6369867324829102
		 16 5.5962142944335938 17 4.1683144569396973 18 2.6294326782226563 19 1.2557154893875122
		 20 0.32330861687660217 21 -0.013645089231431484 22 0.051068909466266632 23 0.27170407772064209
		 24 0.40251386165618896 25 0.1977517157793045 26 -0.89335852861404419 27 -2.4615268707275391
		 28 -3.3124003410339355 29 -3.3537170886993408 30 -3.298567533493042 31 -3.1606273651123047
		 32 -2.9535717964172363 33 -2.6910772323608398 34 -2.3868186473846436 35 -2.0544719696044922
		 36 -1.7077121734619141 37 -1.3602156639099121 38 -1.0256577730178833 39 -0.41369533538818359
		 40 0.40905806422233582 41 0.89349234104156494 42 0.96965104341506969 43 0.95229285955429077
		 44 0.86938077211380005 45 0.74887764453887939 46 0.61874645948410034 47 0.50695008039474487
		 48 0.4414513111114502 49 0.45021316409111023 50 0.56119847297668457 51 1.0568591356277466
		 52 1.9691991806030273 53 2.9505088329315186 54 4.2091069221496582 55 5.8388266563415527
		 56 7.3202295303344727 57 8.1338768005371094 58 7.760329246520997 59 5.8132572174072266
		 60 2.6644637584686279 61 -1.0682892799377441 62 -4.7672405242919922 63 -7.8146286010742187
		 64 -9.5926923751831055 65 -9.8748979568481445 66 -9.1101484298706055 67 -7.6938352584838867
		 68 -6.0213513374328613 69 -4.4880890846252441 70 -3.4894416332244873 71 -2.9375121593475342
		 72 -2.4707865715026855 73 -2.0742285251617432 74 -1.732802152633667 75 -1.4314719438552856
		 76 -1.1552015542984009 77 -0.88895505666732788 78 -0.61769658327102661 79 -0.32639029622077942
		 80 0;
createNode animCurveTU -n "eyes_visibility";
	rename -uid "B37B50A5-4AFD-8010-F898-B0B1228268FD";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "jaw_translateX";
	rename -uid "41FA5840-49E6-1ABA-D559-D69EEE147769";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.07279486209154129;
createNode animCurveTL -n "jaw_translateY";
	rename -uid "D696C2FD-4E4F-4710-8E33-14BEEC671698";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.25670051574707;
createNode animCurveTL -n "jaw_translateZ";
	rename -uid "F2426BEE-4F38-35D0-07C8-EC9B28B02CF7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.2238426557523781e-007;
createNode animCurveTU -n "jaw_scaleX";
	rename -uid "C8A473E7-4199-5F67-752B-2CAA54E022A8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleY";
	rename -uid "83214945-4F53-CB1D-D3BE-53A95BD40688";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleZ";
	rename -uid "D9670A71-4C4A-D64E-0077-538B61435CF2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "jaw_rotateX";
	rename -uid "54FDFF4D-428E-2BAB-9E23-5E8C77A50AD9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -51.490509033203125 1 -50.81585693359375
		 2 -50.4107666015625 3 -50.099700927734375 4 -49.79364013671875 5 -49.544754028320313
		 6 -49.553237915039063 7 -49.736312866210937 8 -49.692169189453125 9 -49.4150390625
		 10 -49.139297485351563 11 -48.984634399414063 12 -50.39508056640625 13 -45.453628540039063
		 14 -27.95928955078125 15 -8.7776641845703125 16 4.3248291015625 17 8.673583984375
		 18 8.652069091796875 19 7.1552734375 20 6.9946136474609375 21 8.7923583984375 22 11.165802001953125
		 23 13.971694946289063 24 17.167160034179688 25 20.188690185546875 26 22.726287841796875
		 27 24.57025146484375 28 25.460464477539063 29 20.714202880859375 30 13.358123779296875
		 31 9.8468475341796875 32 6.5131378173828125 33 3.48992919921875 34 0.99475097656250011
		 35 -0.7962493896484375 36 -1.7391815185546875 37 0.5512237548828125 38 5.440155029296875
		 39 8.8566131591796875 40 9.8315277099609375 41 10.164871215820312 42 10.133132934570312
		 43 10.007171630859375 44 10.08001708984375 45 10.664718627929688 46 12.921630859375
		 47 16.158676147460938 48 17.8516845703125 49 18.41546630859375 50 19.472442626953125
		 51 20.33221435546875 52 20.396728515625 53 19.044586181640625 54 15.677764892578127
		 55 9.73455810546875 56 -8.6591033935546875 57 -36.385055541992188 58 -54.46063232421875
		 59 -59.057174682617188 60 -59.892181396484375 61 -58.221061706542976 62 -55.31103515625
		 63 -52.466354370117188 64 -51.009552001953125 65 -50.784683227539063 66 -50.786468505859375
		 67 -50.9798583984375 68 -51.236434936523438 69 -51.1087646484375 70 -50.826263427734375
		 71 -50.500930786132813 72 -50.787109375 73 -51.610641479492188 74 -51.852218627929688
		 75 -51.510116577148438 76 -51.37713623046875 77 -51.355133056640625 78 -51.38726806640625
		 79 -51.439468383789063 80 -51.490509033203125;
createNode animCurveTA -n "jaw_rotateY";
	rename -uid "A818855E-438C-0FB1-2266-DD9EF412C1D4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -120.80289459228517 1 -120.48706817626955
		 2 -120.23799133300781 3 -119.98533630371094 4 -119.671142578125 5 -119.30873107910156
		 6 -119.03564453125001 7 -118.83644866943359 8 -118.60461425781251 9 -118.56430816650391
		 10 -118.85399627685547 11 -119.62242889404298 12 -120.48391723632811 13 -120.8643798828125
		 14 -120.30127716064453 15 -119.99269104003906 16 -119.81228637695311 17 -119.76388549804687
		 18 -119.88954162597656 19 -120.11398315429689 20 -120.27957916259766 21 -120.41215515136719
		 22 -120.59930419921875 23 -120.96121978759764 24 -121.54039001464842 25 -122.30880737304686
		 26 -123.12576293945312 27 -123.79148864746092 28 -124.08230590820314 29 -122.10682678222656
		 30 -120.4390869140625 31 -120.12388610839845 32 -119.90663146972656 33 -119.79627990722655
		 34 -119.81010437011719 35 -119.98595428466797 36 -120.37864685058595 37 -121.48389434814453
		 38 -123.17076110839845 39 -124.53430938720705 40 -125.35951232910155 41 -126.0594940185547
		 42 -126.69340515136719 43 -127.32139587402344 44 -127.97744750976562 45 -128.68087768554687
		 46 -129.69195556640625 47 -130.70614624023437 48 -130.92031860351562 49 -130.28793334960937
		 50 -129.39041137695312 51 -128.34390258789063 52 -127.244873046875 53 -126.19171905517577
		 54 -125.2754669189453 55 -124.57780456542967 56 -124.51783752441406 57 -124.92694091796874
		 58 -124.97097015380861 59 -124.3647003173828 60 -123.52198791503906 61 -122.58397674560548
		 62 -121.7008743286133 63 -121.011474609375 64 -120.64337158203125 65 -120.62243652343749
		 66 -120.85864257812501 67 -121.34856414794923 68 -121.93203735351564 69 -122.04331970214845
		 70 -122.07789611816406 71 -121.99620819091797 72 -122.24020385742186 73 -122.68994903564453
		 74 -122.6036911010742 75 -122.13421630859374 76 -121.78072357177734 77 -121.48300933837889
		 78 -121.22267150878905 79 -120.9955291748047 80 -120.80289459228517;
createNode animCurveTA -n "jaw_rotateZ";
	rename -uid "5DFEB395-4952-B7D7-468B-BA8F04C3DCF1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -44.638351440429687 1 -45.147903442382812
		 2 -45.482498168945313 3 -45.809112548828125 4 -46.225204467773438 5 -46.69720458984375
		 6 -47.007965087890625 7 -46.909210205078125 8 -46.639297485351563 9 -46.454696655273438
		 10 -46.499237060546875 11 -46.614593505859375 12 -45.662948608398438 13 -42.222320556640625
		 14 -40.167037963867188 15 -37.739547729492188 16 -36.341400146484375 17 -36.597793579101563
		 18 -37.7393798828125 19 -39.266494750976562 20 -40.87176513671875 21 -42.037200927734375
		 22 -44.020553588867188 23 -47.671966552734375 24 -51.99688720703125 25 -56.255195617675781
		 26 -59.96221923828125 27 -62.664268493652344 28 -63.895965576171875 29 -56.517982482910156
		 30 -46.202987670898438 31 -42.748641967773438 32 -40.077926635742187 33 -38.016342163085938
		 34 -36.448593139648438 35 -35.225250244140625 36 -34.164138793945313 37 -34.145675659179688
		 38 -35.353744506835938 39 -36.801223754882813 40 -38.20965576171875 41 -39.821182250976563
		 42 -41.507217407226563 43 -43.102005004882813 44 -44.491531372070312 45 -45.57421875
		 46 -46.130233764648437 47 -46.097732543945313 48 -45.515548706054687 49 -44.4561767578125
		 50 -42.992706298828125 51 -41.250701904296875 52 -39.409255981445313 53 -37.613388061523438
		 54 -36.046371459960937 55 -34.919464111328125 56 -35.370498657226562 57 -37.242507934570313
		 58 -38.865951538085937 59 -39.95989990234375 60 -41.220443725585937 61 -42.5306396484375
		 62 -43.735687255859375 63 -44.651046752929687 64 -45.078079223632813 65 -44.866714477539062
		 66 -44.12274169921875 67 -42.954666137695313 68 -41.722625732421875 69 -41.484100341796875
		 70 -41.36785888671875 71 -41.329559326171875 72 -40.761337280273437 73 -40.228622436523438
		 74 -40.498306274414062 75 -41.278167724609375 76 -42.017898559570313 77 -42.732040405273438
		 78 -43.426071166992188 79 -44.073333740234375 80 -44.638351440429687;
createNode animCurveTU -n "jaw_visibility";
	rename -uid "AAB156C1-447E-EEA6-6BD0-A39791B08E6E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.55470019578933716;
	setAttr -s 2 ".kiy[0:1]"  0 0.83205032348632813;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "9B8C1391-42E3-95D6-912A-F49119C87687";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "28DFCA23-45E5-85A5-53F2-2B998A709057";
createNode displayLayer -n "defaultLayer";
	rename -uid "DB49E07D-453A-3517-1722-2D98CEF18373";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "B95A58A3-4FC7-8E6A-B21E-DCAEB339B755";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "B2CCF4AE-4DD3-479B-7617-B5922D7C4FC7";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "F3F5A4D0-4F31-4CC0-6A74-95A3C3FB8413";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1636\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1636\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1288\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1288\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 15 100 -ps 2 85 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1288\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1288\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "9B511BBA-4661-1207-EE9E-D1B415A25BD7";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 80 -ast 0 -aet 250 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 80;
	setAttr ".unw" 80;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
connectAttr "ref_frame.s" "Character1_Hips.is";
connectAttr "Character1_Hips_scaleX.o" "Character1_Hips.sx";
connectAttr "Character1_Hips_scaleY.o" "Character1_Hips.sy";
connectAttr "Character1_Hips_scaleZ.o" "Character1_Hips.sz";
connectAttr "Character1_Hips_translateX.o" "Character1_Hips.tx";
connectAttr "Character1_Hips_translateY.o" "Character1_Hips.ty";
connectAttr "Character1_Hips_translateZ.o" "Character1_Hips.tz";
connectAttr "Character1_Hips_rotateX.o" "Character1_Hips.rx";
connectAttr "Character1_Hips_rotateY.o" "Character1_Hips.ry";
connectAttr "Character1_Hips_rotateZ.o" "Character1_Hips.rz";
connectAttr "Character1_Hips_visibility.o" "Character1_Hips.v";
connectAttr "Character1_Hips.s" "Character1_LeftUpLeg.is";
connectAttr "Character1_LeftUpLeg_scaleX.o" "Character1_LeftUpLeg.sx";
connectAttr "Character1_LeftUpLeg_scaleY.o" "Character1_LeftUpLeg.sy";
connectAttr "Character1_LeftUpLeg_scaleZ.o" "Character1_LeftUpLeg.sz";
connectAttr "Character1_LeftUpLeg_translateX.o" "Character1_LeftUpLeg.tx";
connectAttr "Character1_LeftUpLeg_translateY.o" "Character1_LeftUpLeg.ty";
connectAttr "Character1_LeftUpLeg_translateZ.o" "Character1_LeftUpLeg.tz";
connectAttr "Character1_LeftUpLeg_rotateX.o" "Character1_LeftUpLeg.rx";
connectAttr "Character1_LeftUpLeg_rotateY.o" "Character1_LeftUpLeg.ry";
connectAttr "Character1_LeftUpLeg_rotateZ.o" "Character1_LeftUpLeg.rz";
connectAttr "Character1_LeftUpLeg_visibility.o" "Character1_LeftUpLeg.v";
connectAttr "Character1_LeftUpLeg.s" "Character1_LeftLeg.is";
connectAttr "Character1_LeftLeg_scaleX.o" "Character1_LeftLeg.sx";
connectAttr "Character1_LeftLeg_scaleY.o" "Character1_LeftLeg.sy";
connectAttr "Character1_LeftLeg_scaleZ.o" "Character1_LeftLeg.sz";
connectAttr "Character1_LeftLeg_translateX.o" "Character1_LeftLeg.tx";
connectAttr "Character1_LeftLeg_translateY.o" "Character1_LeftLeg.ty";
connectAttr "Character1_LeftLeg_translateZ.o" "Character1_LeftLeg.tz";
connectAttr "Character1_LeftLeg_rotateX.o" "Character1_LeftLeg.rx";
connectAttr "Character1_LeftLeg_rotateY.o" "Character1_LeftLeg.ry";
connectAttr "Character1_LeftLeg_rotateZ.o" "Character1_LeftLeg.rz";
connectAttr "Character1_LeftLeg_visibility.o" "Character1_LeftLeg.v";
connectAttr "Character1_LeftLeg.s" "Character1_LeftFoot.is";
connectAttr "Character1_LeftFoot_scaleX.o" "Character1_LeftFoot.sx";
connectAttr "Character1_LeftFoot_scaleY.o" "Character1_LeftFoot.sy";
connectAttr "Character1_LeftFoot_scaleZ.o" "Character1_LeftFoot.sz";
connectAttr "Character1_LeftFoot_translateX.o" "Character1_LeftFoot.tx";
connectAttr "Character1_LeftFoot_translateY.o" "Character1_LeftFoot.ty";
connectAttr "Character1_LeftFoot_translateZ.o" "Character1_LeftFoot.tz";
connectAttr "Character1_LeftFoot_rotateX.o" "Character1_LeftFoot.rx";
connectAttr "Character1_LeftFoot_rotateY.o" "Character1_LeftFoot.ry";
connectAttr "Character1_LeftFoot_rotateZ.o" "Character1_LeftFoot.rz";
connectAttr "Character1_LeftFoot_visibility.o" "Character1_LeftFoot.v";
connectAttr "Character1_LeftFoot.s" "Character1_LeftToeBase.is";
connectAttr "Character1_LeftToeBase_scaleX.o" "Character1_LeftToeBase.sx";
connectAttr "Character1_LeftToeBase_scaleY.o" "Character1_LeftToeBase.sy";
connectAttr "Character1_LeftToeBase_scaleZ.o" "Character1_LeftToeBase.sz";
connectAttr "Character1_LeftToeBase_translateX.o" "Character1_LeftToeBase.tx";
connectAttr "Character1_LeftToeBase_translateY.o" "Character1_LeftToeBase.ty";
connectAttr "Character1_LeftToeBase_translateZ.o" "Character1_LeftToeBase.tz";
connectAttr "Character1_LeftToeBase_rotateX.o" "Character1_LeftToeBase.rx";
connectAttr "Character1_LeftToeBase_rotateY.o" "Character1_LeftToeBase.ry";
connectAttr "Character1_LeftToeBase_rotateZ.o" "Character1_LeftToeBase.rz";
connectAttr "Character1_LeftToeBase_visibility.o" "Character1_LeftToeBase.v";
connectAttr "Character1_LeftToeBase.s" "Character1_LeftFootMiddle1.is";
connectAttr "Character1_LeftFootMiddle1_scaleX.o" "Character1_LeftFootMiddle1.sx"
		;
connectAttr "Character1_LeftFootMiddle1_scaleY.o" "Character1_LeftFootMiddle1.sy"
		;
connectAttr "Character1_LeftFootMiddle1_scaleZ.o" "Character1_LeftFootMiddle1.sz"
		;
connectAttr "Character1_LeftFootMiddle1_translateX.o" "Character1_LeftFootMiddle1.tx"
		;
connectAttr "Character1_LeftFootMiddle1_translateY.o" "Character1_LeftFootMiddle1.ty"
		;
connectAttr "Character1_LeftFootMiddle1_translateZ.o" "Character1_LeftFootMiddle1.tz"
		;
connectAttr "Character1_LeftFootMiddle1_rotateX.o" "Character1_LeftFootMiddle1.rx"
		;
connectAttr "Character1_LeftFootMiddle1_rotateY.o" "Character1_LeftFootMiddle1.ry"
		;
connectAttr "Character1_LeftFootMiddle1_rotateZ.o" "Character1_LeftFootMiddle1.rz"
		;
connectAttr "Character1_LeftFootMiddle1_visibility.o" "Character1_LeftFootMiddle1.v"
		;
connectAttr "Character1_LeftFootMiddle1.s" "Character1_LeftFootMiddle2.is";
connectAttr "Character1_LeftFootMiddle2_translateX.o" "Character1_LeftFootMiddle2.tx"
		;
connectAttr "Character1_LeftFootMiddle2_translateY.o" "Character1_LeftFootMiddle2.ty"
		;
connectAttr "Character1_LeftFootMiddle2_translateZ.o" "Character1_LeftFootMiddle2.tz"
		;
connectAttr "Character1_LeftFootMiddle2_scaleX.o" "Character1_LeftFootMiddle2.sx"
		;
connectAttr "Character1_LeftFootMiddle2_scaleY.o" "Character1_LeftFootMiddle2.sy"
		;
connectAttr "Character1_LeftFootMiddle2_scaleZ.o" "Character1_LeftFootMiddle2.sz"
		;
connectAttr "Character1_LeftFootMiddle2_rotateX.o" "Character1_LeftFootMiddle2.rx"
		;
connectAttr "Character1_LeftFootMiddle2_rotateY.o" "Character1_LeftFootMiddle2.ry"
		;
connectAttr "Character1_LeftFootMiddle2_rotateZ.o" "Character1_LeftFootMiddle2.rz"
		;
connectAttr "Character1_LeftFootMiddle2_visibility.o" "Character1_LeftFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_RightUpLeg.is";
connectAttr "Character1_RightUpLeg_scaleX.o" "Character1_RightUpLeg.sx";
connectAttr "Character1_RightUpLeg_scaleY.o" "Character1_RightUpLeg.sy";
connectAttr "Character1_RightUpLeg_scaleZ.o" "Character1_RightUpLeg.sz";
connectAttr "Character1_RightUpLeg_translateX.o" "Character1_RightUpLeg.tx";
connectAttr "Character1_RightUpLeg_translateY.o" "Character1_RightUpLeg.ty";
connectAttr "Character1_RightUpLeg_translateZ.o" "Character1_RightUpLeg.tz";
connectAttr "Character1_RightUpLeg_rotateX.o" "Character1_RightUpLeg.rx";
connectAttr "Character1_RightUpLeg_rotateY.o" "Character1_RightUpLeg.ry";
connectAttr "Character1_RightUpLeg_rotateZ.o" "Character1_RightUpLeg.rz";
connectAttr "Character1_RightUpLeg_visibility.o" "Character1_RightUpLeg.v";
connectAttr "Character1_RightUpLeg.s" "Character1_RightLeg.is";
connectAttr "Character1_RightLeg_scaleX.o" "Character1_RightLeg.sx";
connectAttr "Character1_RightLeg_scaleY.o" "Character1_RightLeg.sy";
connectAttr "Character1_RightLeg_scaleZ.o" "Character1_RightLeg.sz";
connectAttr "Character1_RightLeg_translateX.o" "Character1_RightLeg.tx";
connectAttr "Character1_RightLeg_translateY.o" "Character1_RightLeg.ty";
connectAttr "Character1_RightLeg_translateZ.o" "Character1_RightLeg.tz";
connectAttr "Character1_RightLeg_rotateX.o" "Character1_RightLeg.rx";
connectAttr "Character1_RightLeg_rotateY.o" "Character1_RightLeg.ry";
connectAttr "Character1_RightLeg_rotateZ.o" "Character1_RightLeg.rz";
connectAttr "Character1_RightLeg_visibility.o" "Character1_RightLeg.v";
connectAttr "Character1_RightLeg.s" "Character1_RightFoot.is";
connectAttr "Character1_RightFoot_scaleX.o" "Character1_RightFoot.sx";
connectAttr "Character1_RightFoot_scaleY.o" "Character1_RightFoot.sy";
connectAttr "Character1_RightFoot_scaleZ.o" "Character1_RightFoot.sz";
connectAttr "Character1_RightFoot_translateX.o" "Character1_RightFoot.tx";
connectAttr "Character1_RightFoot_translateY.o" "Character1_RightFoot.ty";
connectAttr "Character1_RightFoot_translateZ.o" "Character1_RightFoot.tz";
connectAttr "Character1_RightFoot_rotateX.o" "Character1_RightFoot.rx";
connectAttr "Character1_RightFoot_rotateY.o" "Character1_RightFoot.ry";
connectAttr "Character1_RightFoot_rotateZ.o" "Character1_RightFoot.rz";
connectAttr "Character1_RightFoot_visibility.o" "Character1_RightFoot.v";
connectAttr "Character1_RightFoot.s" "Character1_RightToeBase.is";
connectAttr "Character1_RightToeBase_scaleX.o" "Character1_RightToeBase.sx";
connectAttr "Character1_RightToeBase_scaleY.o" "Character1_RightToeBase.sy";
connectAttr "Character1_RightToeBase_scaleZ.o" "Character1_RightToeBase.sz";
connectAttr "Character1_RightToeBase_translateX.o" "Character1_RightToeBase.tx";
connectAttr "Character1_RightToeBase_translateY.o" "Character1_RightToeBase.ty";
connectAttr "Character1_RightToeBase_translateZ.o" "Character1_RightToeBase.tz";
connectAttr "Character1_RightToeBase_rotateX.o" "Character1_RightToeBase.rx";
connectAttr "Character1_RightToeBase_rotateY.o" "Character1_RightToeBase.ry";
connectAttr "Character1_RightToeBase_rotateZ.o" "Character1_RightToeBase.rz";
connectAttr "Character1_RightToeBase_visibility.o" "Character1_RightToeBase.v";
connectAttr "Character1_RightToeBase.s" "Character1_RightFootMiddle1.is";
connectAttr "Character1_RightFootMiddle1_scaleX.o" "Character1_RightFootMiddle1.sx"
		;
connectAttr "Character1_RightFootMiddle1_scaleY.o" "Character1_RightFootMiddle1.sy"
		;
connectAttr "Character1_RightFootMiddle1_scaleZ.o" "Character1_RightFootMiddle1.sz"
		;
connectAttr "Character1_RightFootMiddle1_translateX.o" "Character1_RightFootMiddle1.tx"
		;
connectAttr "Character1_RightFootMiddle1_translateY.o" "Character1_RightFootMiddle1.ty"
		;
connectAttr "Character1_RightFootMiddle1_translateZ.o" "Character1_RightFootMiddle1.tz"
		;
connectAttr "Character1_RightFootMiddle1_rotateX.o" "Character1_RightFootMiddle1.rx"
		;
connectAttr "Character1_RightFootMiddle1_rotateY.o" "Character1_RightFootMiddle1.ry"
		;
connectAttr "Character1_RightFootMiddle1_rotateZ.o" "Character1_RightFootMiddle1.rz"
		;
connectAttr "Character1_RightFootMiddle1_visibility.o" "Character1_RightFootMiddle1.v"
		;
connectAttr "Character1_RightFootMiddle1.s" "Character1_RightFootMiddle2.is";
connectAttr "Character1_RightFootMiddle2_translateX.o" "Character1_RightFootMiddle2.tx"
		;
connectAttr "Character1_RightFootMiddle2_translateY.o" "Character1_RightFootMiddle2.ty"
		;
connectAttr "Character1_RightFootMiddle2_translateZ.o" "Character1_RightFootMiddle2.tz"
		;
connectAttr "Character1_RightFootMiddle2_scaleX.o" "Character1_RightFootMiddle2.sx"
		;
connectAttr "Character1_RightFootMiddle2_scaleY.o" "Character1_RightFootMiddle2.sy"
		;
connectAttr "Character1_RightFootMiddle2_scaleZ.o" "Character1_RightFootMiddle2.sz"
		;
connectAttr "Character1_RightFootMiddle2_rotateX.o" "Character1_RightFootMiddle2.rx"
		;
connectAttr "Character1_RightFootMiddle2_rotateY.o" "Character1_RightFootMiddle2.ry"
		;
connectAttr "Character1_RightFootMiddle2_rotateZ.o" "Character1_RightFootMiddle2.rz"
		;
connectAttr "Character1_RightFootMiddle2_visibility.o" "Character1_RightFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_Spine.is";
connectAttr "Character1_Spine_scaleX.o" "Character1_Spine.sx";
connectAttr "Character1_Spine_scaleY.o" "Character1_Spine.sy";
connectAttr "Character1_Spine_scaleZ.o" "Character1_Spine.sz";
connectAttr "Character1_Spine_translateX.o" "Character1_Spine.tx";
connectAttr "Character1_Spine_translateY.o" "Character1_Spine.ty";
connectAttr "Character1_Spine_translateZ.o" "Character1_Spine.tz";
connectAttr "Character1_Spine_rotateX.o" "Character1_Spine.rx";
connectAttr "Character1_Spine_rotateY.o" "Character1_Spine.ry";
connectAttr "Character1_Spine_rotateZ.o" "Character1_Spine.rz";
connectAttr "Character1_Spine_visibility.o" "Character1_Spine.v";
connectAttr "Character1_Spine.s" "Character1_Spine1.is";
connectAttr "Character1_Spine1_scaleX.o" "Character1_Spine1.sx";
connectAttr "Character1_Spine1_scaleY.o" "Character1_Spine1.sy";
connectAttr "Character1_Spine1_scaleZ.o" "Character1_Spine1.sz";
connectAttr "Character1_Spine1_translateX.o" "Character1_Spine1.tx";
connectAttr "Character1_Spine1_translateY.o" "Character1_Spine1.ty";
connectAttr "Character1_Spine1_translateZ.o" "Character1_Spine1.tz";
connectAttr "Character1_Spine1_rotateX.o" "Character1_Spine1.rx";
connectAttr "Character1_Spine1_rotateY.o" "Character1_Spine1.ry";
connectAttr "Character1_Spine1_rotateZ.o" "Character1_Spine1.rz";
connectAttr "Character1_Spine1_visibility.o" "Character1_Spine1.v";
connectAttr "Character1_Spine1.s" "Character1_LeftShoulder.is";
connectAttr "Character1_LeftShoulder_scaleX.o" "Character1_LeftShoulder.sx";
connectAttr "Character1_LeftShoulder_scaleY.o" "Character1_LeftShoulder.sy";
connectAttr "Character1_LeftShoulder_scaleZ.o" "Character1_LeftShoulder.sz";
connectAttr "Character1_LeftShoulder_translateX.o" "Character1_LeftShoulder.tx";
connectAttr "Character1_LeftShoulder_translateY.o" "Character1_LeftShoulder.ty";
connectAttr "Character1_LeftShoulder_translateZ.o" "Character1_LeftShoulder.tz";
connectAttr "Character1_LeftShoulder_rotateX.o" "Character1_LeftShoulder.rx";
connectAttr "Character1_LeftShoulder_rotateY.o" "Character1_LeftShoulder.ry";
connectAttr "Character1_LeftShoulder_rotateZ.o" "Character1_LeftShoulder.rz";
connectAttr "Character1_LeftShoulder_visibility.o" "Character1_LeftShoulder.v";
connectAttr "Character1_LeftShoulder.s" "Character1_LeftArm.is";
connectAttr "Character1_LeftArm_scaleX.o" "Character1_LeftArm.sx";
connectAttr "Character1_LeftArm_scaleY.o" "Character1_LeftArm.sy";
connectAttr "Character1_LeftArm_scaleZ.o" "Character1_LeftArm.sz";
connectAttr "Character1_LeftArm_translateX.o" "Character1_LeftArm.tx";
connectAttr "Character1_LeftArm_translateY.o" "Character1_LeftArm.ty";
connectAttr "Character1_LeftArm_translateZ.o" "Character1_LeftArm.tz";
connectAttr "Character1_LeftArm_rotateX.o" "Character1_LeftArm.rx";
connectAttr "Character1_LeftArm_rotateY.o" "Character1_LeftArm.ry";
connectAttr "Character1_LeftArm_rotateZ.o" "Character1_LeftArm.rz";
connectAttr "Character1_LeftArm_visibility.o" "Character1_LeftArm.v";
connectAttr "Character1_LeftArm.s" "Character1_LeftForeArm.is";
connectAttr "Character1_LeftForeArm_scaleX.o" "Character1_LeftForeArm.sx";
connectAttr "Character1_LeftForeArm_scaleY.o" "Character1_LeftForeArm.sy";
connectAttr "Character1_LeftForeArm_scaleZ.o" "Character1_LeftForeArm.sz";
connectAttr "Character1_LeftForeArm_translateX.o" "Character1_LeftForeArm.tx";
connectAttr "Character1_LeftForeArm_translateY.o" "Character1_LeftForeArm.ty";
connectAttr "Character1_LeftForeArm_translateZ.o" "Character1_LeftForeArm.tz";
connectAttr "Character1_LeftForeArm_rotateX.o" "Character1_LeftForeArm.rx";
connectAttr "Character1_LeftForeArm_rotateY.o" "Character1_LeftForeArm.ry";
connectAttr "Character1_LeftForeArm_rotateZ.o" "Character1_LeftForeArm.rz";
connectAttr "Character1_LeftForeArm_visibility.o" "Character1_LeftForeArm.v";
connectAttr "Character1_LeftForeArm.s" "Character1_LeftHand.is";
connectAttr "Character1_LeftHand_lockInfluenceWeights.o" "Character1_LeftHand.liw"
		;
connectAttr "Character1_LeftHand_scaleX.o" "Character1_LeftHand.sx";
connectAttr "Character1_LeftHand_scaleY.o" "Character1_LeftHand.sy";
connectAttr "Character1_LeftHand_scaleZ.o" "Character1_LeftHand.sz";
connectAttr "Character1_LeftHand_translateX.o" "Character1_LeftHand.tx";
connectAttr "Character1_LeftHand_translateY.o" "Character1_LeftHand.ty";
connectAttr "Character1_LeftHand_translateZ.o" "Character1_LeftHand.tz";
connectAttr "Character1_LeftHand_rotateX.o" "Character1_LeftHand.rx";
connectAttr "Character1_LeftHand_rotateY.o" "Character1_LeftHand.ry";
connectAttr "Character1_LeftHand_rotateZ.o" "Character1_LeftHand.rz";
connectAttr "Character1_LeftHand_visibility.o" "Character1_LeftHand.v";
connectAttr "Character1_LeftHand.s" "Character1_LeftHandThumb1.is";
connectAttr "Character1_LeftHandThumb1_scaleX.o" "Character1_LeftHandThumb1.sx";
connectAttr "Character1_LeftHandThumb1_scaleY.o" "Character1_LeftHandThumb1.sy";
connectAttr "Character1_LeftHandThumb1_scaleZ.o" "Character1_LeftHandThumb1.sz";
connectAttr "Character1_LeftHandThumb1_translateX.o" "Character1_LeftHandThumb1.tx"
		;
connectAttr "Character1_LeftHandThumb1_translateY.o" "Character1_LeftHandThumb1.ty"
		;
connectAttr "Character1_LeftHandThumb1_translateZ.o" "Character1_LeftHandThumb1.tz"
		;
connectAttr "Character1_LeftHandThumb1_rotateX.o" "Character1_LeftHandThumb1.rx"
		;
connectAttr "Character1_LeftHandThumb1_rotateY.o" "Character1_LeftHandThumb1.ry"
		;
connectAttr "Character1_LeftHandThumb1_rotateZ.o" "Character1_LeftHandThumb1.rz"
		;
connectAttr "Character1_LeftHandThumb1_visibility.o" "Character1_LeftHandThumb1.v"
		;
connectAttr "Character1_LeftHandThumb1.s" "Character1_LeftHandThumb2.is";
connectAttr "Character1_LeftHandThumb2_scaleX.o" "Character1_LeftHandThumb2.sx";
connectAttr "Character1_LeftHandThumb2_scaleY.o" "Character1_LeftHandThumb2.sy";
connectAttr "Character1_LeftHandThumb2_scaleZ.o" "Character1_LeftHandThumb2.sz";
connectAttr "Character1_LeftHandThumb2_translateX.o" "Character1_LeftHandThumb2.tx"
		;
connectAttr "Character1_LeftHandThumb2_translateY.o" "Character1_LeftHandThumb2.ty"
		;
connectAttr "Character1_LeftHandThumb2_translateZ.o" "Character1_LeftHandThumb2.tz"
		;
connectAttr "Character1_LeftHandThumb2_rotateX.o" "Character1_LeftHandThumb2.rx"
		;
connectAttr "Character1_LeftHandThumb2_rotateY.o" "Character1_LeftHandThumb2.ry"
		;
connectAttr "Character1_LeftHandThumb2_rotateZ.o" "Character1_LeftHandThumb2.rz"
		;
connectAttr "Character1_LeftHandThumb2_visibility.o" "Character1_LeftHandThumb2.v"
		;
connectAttr "Character1_LeftHandThumb2.s" "Character1_LeftHandThumb3.is";
connectAttr "Character1_LeftHandThumb3_translateX.o" "Character1_LeftHandThumb3.tx"
		;
connectAttr "Character1_LeftHandThumb3_translateY.o" "Character1_LeftHandThumb3.ty"
		;
connectAttr "Character1_LeftHandThumb3_translateZ.o" "Character1_LeftHandThumb3.tz"
		;
connectAttr "Character1_LeftHandThumb3_scaleX.o" "Character1_LeftHandThumb3.sx";
connectAttr "Character1_LeftHandThumb3_scaleY.o" "Character1_LeftHandThumb3.sy";
connectAttr "Character1_LeftHandThumb3_scaleZ.o" "Character1_LeftHandThumb3.sz";
connectAttr "Character1_LeftHandThumb3_rotateX.o" "Character1_LeftHandThumb3.rx"
		;
connectAttr "Character1_LeftHandThumb3_rotateY.o" "Character1_LeftHandThumb3.ry"
		;
connectAttr "Character1_LeftHandThumb3_rotateZ.o" "Character1_LeftHandThumb3.rz"
		;
connectAttr "Character1_LeftHandThumb3_visibility.o" "Character1_LeftHandThumb3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandIndex1.is";
connectAttr "Character1_LeftHandIndex1_scaleX.o" "Character1_LeftHandIndex1.sx";
connectAttr "Character1_LeftHandIndex1_scaleY.o" "Character1_LeftHandIndex1.sy";
connectAttr "Character1_LeftHandIndex1_scaleZ.o" "Character1_LeftHandIndex1.sz";
connectAttr "Character1_LeftHandIndex1_translateX.o" "Character1_LeftHandIndex1.tx"
		;
connectAttr "Character1_LeftHandIndex1_translateY.o" "Character1_LeftHandIndex1.ty"
		;
connectAttr "Character1_LeftHandIndex1_translateZ.o" "Character1_LeftHandIndex1.tz"
		;
connectAttr "Character1_LeftHandIndex1_rotateX.o" "Character1_LeftHandIndex1.rx"
		;
connectAttr "Character1_LeftHandIndex1_rotateY.o" "Character1_LeftHandIndex1.ry"
		;
connectAttr "Character1_LeftHandIndex1_rotateZ.o" "Character1_LeftHandIndex1.rz"
		;
connectAttr "Character1_LeftHandIndex1_visibility.o" "Character1_LeftHandIndex1.v"
		;
connectAttr "Character1_LeftHandIndex1.s" "Character1_LeftHandIndex2.is";
connectAttr "Character1_LeftHandIndex2_scaleX.o" "Character1_LeftHandIndex2.sx";
connectAttr "Character1_LeftHandIndex2_scaleY.o" "Character1_LeftHandIndex2.sy";
connectAttr "Character1_LeftHandIndex2_scaleZ.o" "Character1_LeftHandIndex2.sz";
connectAttr "Character1_LeftHandIndex2_translateX.o" "Character1_LeftHandIndex2.tx"
		;
connectAttr "Character1_LeftHandIndex2_translateY.o" "Character1_LeftHandIndex2.ty"
		;
connectAttr "Character1_LeftHandIndex2_translateZ.o" "Character1_LeftHandIndex2.tz"
		;
connectAttr "Character1_LeftHandIndex2_rotateX.o" "Character1_LeftHandIndex2.rx"
		;
connectAttr "Character1_LeftHandIndex2_rotateY.o" "Character1_LeftHandIndex2.ry"
		;
connectAttr "Character1_LeftHandIndex2_rotateZ.o" "Character1_LeftHandIndex2.rz"
		;
connectAttr "Character1_LeftHandIndex2_visibility.o" "Character1_LeftHandIndex2.v"
		;
connectAttr "Character1_LeftHandIndex2.s" "Character1_LeftHandIndex3.is";
connectAttr "Character1_LeftHandIndex3_translateX.o" "Character1_LeftHandIndex3.tx"
		;
connectAttr "Character1_LeftHandIndex3_translateY.o" "Character1_LeftHandIndex3.ty"
		;
connectAttr "Character1_LeftHandIndex3_translateZ.o" "Character1_LeftHandIndex3.tz"
		;
connectAttr "Character1_LeftHandIndex3_scaleX.o" "Character1_LeftHandIndex3.sx";
connectAttr "Character1_LeftHandIndex3_scaleY.o" "Character1_LeftHandIndex3.sy";
connectAttr "Character1_LeftHandIndex3_scaleZ.o" "Character1_LeftHandIndex3.sz";
connectAttr "Character1_LeftHandIndex3_rotateX.o" "Character1_LeftHandIndex3.rx"
		;
connectAttr "Character1_LeftHandIndex3_rotateY.o" "Character1_LeftHandIndex3.ry"
		;
connectAttr "Character1_LeftHandIndex3_rotateZ.o" "Character1_LeftHandIndex3.rz"
		;
connectAttr "Character1_LeftHandIndex3_visibility.o" "Character1_LeftHandIndex3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandRing1.is";
connectAttr "Character1_LeftHandRing1_scaleX.o" "Character1_LeftHandRing1.sx";
connectAttr "Character1_LeftHandRing1_scaleY.o" "Character1_LeftHandRing1.sy";
connectAttr "Character1_LeftHandRing1_scaleZ.o" "Character1_LeftHandRing1.sz";
connectAttr "Character1_LeftHandRing1_translateX.o" "Character1_LeftHandRing1.tx"
		;
connectAttr "Character1_LeftHandRing1_translateY.o" "Character1_LeftHandRing1.ty"
		;
connectAttr "Character1_LeftHandRing1_translateZ.o" "Character1_LeftHandRing1.tz"
		;
connectAttr "Character1_LeftHandRing1_rotateX.o" "Character1_LeftHandRing1.rx";
connectAttr "Character1_LeftHandRing1_rotateY.o" "Character1_LeftHandRing1.ry";
connectAttr "Character1_LeftHandRing1_rotateZ.o" "Character1_LeftHandRing1.rz";
connectAttr "Character1_LeftHandRing1_visibility.o" "Character1_LeftHandRing1.v"
		;
connectAttr "Character1_LeftHandRing1.s" "Character1_LeftHandRing2.is";
connectAttr "Character1_LeftHandRing2_scaleX.o" "Character1_LeftHandRing2.sx";
connectAttr "Character1_LeftHandRing2_scaleY.o" "Character1_LeftHandRing2.sy";
connectAttr "Character1_LeftHandRing2_scaleZ.o" "Character1_LeftHandRing2.sz";
connectAttr "Character1_LeftHandRing2_translateX.o" "Character1_LeftHandRing2.tx"
		;
connectAttr "Character1_LeftHandRing2_translateY.o" "Character1_LeftHandRing2.ty"
		;
connectAttr "Character1_LeftHandRing2_translateZ.o" "Character1_LeftHandRing2.tz"
		;
connectAttr "Character1_LeftHandRing2_rotateX.o" "Character1_LeftHandRing2.rx";
connectAttr "Character1_LeftHandRing2_rotateY.o" "Character1_LeftHandRing2.ry";
connectAttr "Character1_LeftHandRing2_rotateZ.o" "Character1_LeftHandRing2.rz";
connectAttr "Character1_LeftHandRing2_visibility.o" "Character1_LeftHandRing2.v"
		;
connectAttr "Character1_LeftHandRing2.s" "Character1_LeftHandRing3.is";
connectAttr "Character1_LeftHandRing3_translateX.o" "Character1_LeftHandRing3.tx"
		;
connectAttr "Character1_LeftHandRing3_translateY.o" "Character1_LeftHandRing3.ty"
		;
connectAttr "Character1_LeftHandRing3_translateZ.o" "Character1_LeftHandRing3.tz"
		;
connectAttr "Character1_LeftHandRing3_scaleX.o" "Character1_LeftHandRing3.sx";
connectAttr "Character1_LeftHandRing3_scaleY.o" "Character1_LeftHandRing3.sy";
connectAttr "Character1_LeftHandRing3_scaleZ.o" "Character1_LeftHandRing3.sz";
connectAttr "Character1_LeftHandRing3_rotateX.o" "Character1_LeftHandRing3.rx";
connectAttr "Character1_LeftHandRing3_rotateY.o" "Character1_LeftHandRing3.ry";
connectAttr "Character1_LeftHandRing3_rotateZ.o" "Character1_LeftHandRing3.rz";
connectAttr "Character1_LeftHandRing3_visibility.o" "Character1_LeftHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_RightShoulder.is";
connectAttr "Character1_RightShoulder_scaleX.o" "Character1_RightShoulder.sx";
connectAttr "Character1_RightShoulder_scaleY.o" "Character1_RightShoulder.sy";
connectAttr "Character1_RightShoulder_scaleZ.o" "Character1_RightShoulder.sz";
connectAttr "Character1_RightShoulder_translateX.o" "Character1_RightShoulder.tx"
		;
connectAttr "Character1_RightShoulder_translateY.o" "Character1_RightShoulder.ty"
		;
connectAttr "Character1_RightShoulder_translateZ.o" "Character1_RightShoulder.tz"
		;
connectAttr "Character1_RightShoulder_rotateX.o" "Character1_RightShoulder.rx";
connectAttr "Character1_RightShoulder_rotateY.o" "Character1_RightShoulder.ry";
connectAttr "Character1_RightShoulder_rotateZ.o" "Character1_RightShoulder.rz";
connectAttr "Character1_RightShoulder_visibility.o" "Character1_RightShoulder.v"
		;
connectAttr "Character1_RightShoulder.s" "Character1_RightArm.is";
connectAttr "Character1_RightArm_scaleX.o" "Character1_RightArm.sx";
connectAttr "Character1_RightArm_scaleY.o" "Character1_RightArm.sy";
connectAttr "Character1_RightArm_scaleZ.o" "Character1_RightArm.sz";
connectAttr "Character1_RightArm_translateX.o" "Character1_RightArm.tx";
connectAttr "Character1_RightArm_translateY.o" "Character1_RightArm.ty";
connectAttr "Character1_RightArm_translateZ.o" "Character1_RightArm.tz";
connectAttr "Character1_RightArm_rotateX.o" "Character1_RightArm.rx";
connectAttr "Character1_RightArm_rotateY.o" "Character1_RightArm.ry";
connectAttr "Character1_RightArm_rotateZ.o" "Character1_RightArm.rz";
connectAttr "Character1_RightArm_visibility.o" "Character1_RightArm.v";
connectAttr "Character1_RightArm.s" "Character1_RightForeArm.is";
connectAttr "Character1_RightForeArm_scaleX.o" "Character1_RightForeArm.sx";
connectAttr "Character1_RightForeArm_scaleY.o" "Character1_RightForeArm.sy";
connectAttr "Character1_RightForeArm_scaleZ.o" "Character1_RightForeArm.sz";
connectAttr "Character1_RightForeArm_translateX.o" "Character1_RightForeArm.tx";
connectAttr "Character1_RightForeArm_translateY.o" "Character1_RightForeArm.ty";
connectAttr "Character1_RightForeArm_translateZ.o" "Character1_RightForeArm.tz";
connectAttr "Character1_RightForeArm_rotateX.o" "Character1_RightForeArm.rx";
connectAttr "Character1_RightForeArm_rotateY.o" "Character1_RightForeArm.ry";
connectAttr "Character1_RightForeArm_rotateZ.o" "Character1_RightForeArm.rz";
connectAttr "Character1_RightForeArm_visibility.o" "Character1_RightForeArm.v";
connectAttr "Character1_RightForeArm.s" "Character1_RightHand.is";
connectAttr "Character1_RightHand_scaleX.o" "Character1_RightHand.sx";
connectAttr "Character1_RightHand_scaleY.o" "Character1_RightHand.sy";
connectAttr "Character1_RightHand_scaleZ.o" "Character1_RightHand.sz";
connectAttr "Character1_RightHand_translateX.o" "Character1_RightHand.tx";
connectAttr "Character1_RightHand_translateY.o" "Character1_RightHand.ty";
connectAttr "Character1_RightHand_translateZ.o" "Character1_RightHand.tz";
connectAttr "Character1_RightHand_rotateX.o" "Character1_RightHand.rx";
connectAttr "Character1_RightHand_rotateY.o" "Character1_RightHand.ry";
connectAttr "Character1_RightHand_rotateZ.o" "Character1_RightHand.rz";
connectAttr "Character1_RightHand_visibility.o" "Character1_RightHand.v";
connectAttr "Character1_RightHand.s" "Character1_RightHandThumb1.is";
connectAttr "Character1_RightHandThumb1_scaleX.o" "Character1_RightHandThumb1.sx"
		;
connectAttr "Character1_RightHandThumb1_scaleY.o" "Character1_RightHandThumb1.sy"
		;
connectAttr "Character1_RightHandThumb1_scaleZ.o" "Character1_RightHandThumb1.sz"
		;
connectAttr "Character1_RightHandThumb1_translateX.o" "Character1_RightHandThumb1.tx"
		;
connectAttr "Character1_RightHandThumb1_translateY.o" "Character1_RightHandThumb1.ty"
		;
connectAttr "Character1_RightHandThumb1_translateZ.o" "Character1_RightHandThumb1.tz"
		;
connectAttr "Character1_RightHandThumb1_rotateX.o" "Character1_RightHandThumb1.rx"
		;
connectAttr "Character1_RightHandThumb1_rotateY.o" "Character1_RightHandThumb1.ry"
		;
connectAttr "Character1_RightHandThumb1_rotateZ.o" "Character1_RightHandThumb1.rz"
		;
connectAttr "Character1_RightHandThumb1_visibility.o" "Character1_RightHandThumb1.v"
		;
connectAttr "Character1_RightHandThumb1.s" "Character1_RightHandThumb2.is";
connectAttr "Character1_RightHandThumb2_scaleX.o" "Character1_RightHandThumb2.sx"
		;
connectAttr "Character1_RightHandThumb2_scaleY.o" "Character1_RightHandThumb2.sy"
		;
connectAttr "Character1_RightHandThumb2_scaleZ.o" "Character1_RightHandThumb2.sz"
		;
connectAttr "Character1_RightHandThumb2_translateX.o" "Character1_RightHandThumb2.tx"
		;
connectAttr "Character1_RightHandThumb2_translateY.o" "Character1_RightHandThumb2.ty"
		;
connectAttr "Character1_RightHandThumb2_translateZ.o" "Character1_RightHandThumb2.tz"
		;
connectAttr "Character1_RightHandThumb2_rotateX.o" "Character1_RightHandThumb2.rx"
		;
connectAttr "Character1_RightHandThumb2_rotateY.o" "Character1_RightHandThumb2.ry"
		;
connectAttr "Character1_RightHandThumb2_rotateZ.o" "Character1_RightHandThumb2.rz"
		;
connectAttr "Character1_RightHandThumb2_visibility.o" "Character1_RightHandThumb2.v"
		;
connectAttr "Character1_RightHandThumb2.s" "Character1_RightHandThumb3.is";
connectAttr "Character1_RightHandThumb3_translateX.o" "Character1_RightHandThumb3.tx"
		;
connectAttr "Character1_RightHandThumb3_translateY.o" "Character1_RightHandThumb3.ty"
		;
connectAttr "Character1_RightHandThumb3_translateZ.o" "Character1_RightHandThumb3.tz"
		;
connectAttr "Character1_RightHandThumb3_scaleX.o" "Character1_RightHandThumb3.sx"
		;
connectAttr "Character1_RightHandThumb3_scaleY.o" "Character1_RightHandThumb3.sy"
		;
connectAttr "Character1_RightHandThumb3_scaleZ.o" "Character1_RightHandThumb3.sz"
		;
connectAttr "Character1_RightHandThumb3_rotateX.o" "Character1_RightHandThumb3.rx"
		;
connectAttr "Character1_RightHandThumb3_rotateY.o" "Character1_RightHandThumb3.ry"
		;
connectAttr "Character1_RightHandThumb3_rotateZ.o" "Character1_RightHandThumb3.rz"
		;
connectAttr "Character1_RightHandThumb3_visibility.o" "Character1_RightHandThumb3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandIndex1.is";
connectAttr "Character1_RightHandIndex1_scaleX.o" "Character1_RightHandIndex1.sx"
		;
connectAttr "Character1_RightHandIndex1_scaleY.o" "Character1_RightHandIndex1.sy"
		;
connectAttr "Character1_RightHandIndex1_scaleZ.o" "Character1_RightHandIndex1.sz"
		;
connectAttr "Character1_RightHandIndex1_translateX.o" "Character1_RightHandIndex1.tx"
		;
connectAttr "Character1_RightHandIndex1_translateY.o" "Character1_RightHandIndex1.ty"
		;
connectAttr "Character1_RightHandIndex1_translateZ.o" "Character1_RightHandIndex1.tz"
		;
connectAttr "Character1_RightHandIndex1_rotateX.o" "Character1_RightHandIndex1.rx"
		;
connectAttr "Character1_RightHandIndex1_rotateY.o" "Character1_RightHandIndex1.ry"
		;
connectAttr "Character1_RightHandIndex1_rotateZ.o" "Character1_RightHandIndex1.rz"
		;
connectAttr "Character1_RightHandIndex1_visibility.o" "Character1_RightHandIndex1.v"
		;
connectAttr "Character1_RightHandIndex1.s" "Character1_RightHandIndex2.is";
connectAttr "Character1_RightHandIndex2_scaleX.o" "Character1_RightHandIndex2.sx"
		;
connectAttr "Character1_RightHandIndex2_scaleY.o" "Character1_RightHandIndex2.sy"
		;
connectAttr "Character1_RightHandIndex2_scaleZ.o" "Character1_RightHandIndex2.sz"
		;
connectAttr "Character1_RightHandIndex2_translateX.o" "Character1_RightHandIndex2.tx"
		;
connectAttr "Character1_RightHandIndex2_translateY.o" "Character1_RightHandIndex2.ty"
		;
connectAttr "Character1_RightHandIndex2_translateZ.o" "Character1_RightHandIndex2.tz"
		;
connectAttr "Character1_RightHandIndex2_rotateX.o" "Character1_RightHandIndex2.rx"
		;
connectAttr "Character1_RightHandIndex2_rotateY.o" "Character1_RightHandIndex2.ry"
		;
connectAttr "Character1_RightHandIndex2_rotateZ.o" "Character1_RightHandIndex2.rz"
		;
connectAttr "Character1_RightHandIndex2_visibility.o" "Character1_RightHandIndex2.v"
		;
connectAttr "Character1_RightHandIndex2.s" "Character1_RightHandIndex3.is";
connectAttr "Character1_RightHandIndex3_translateX.o" "Character1_RightHandIndex3.tx"
		;
connectAttr "Character1_RightHandIndex3_translateY.o" "Character1_RightHandIndex3.ty"
		;
connectAttr "Character1_RightHandIndex3_translateZ.o" "Character1_RightHandIndex3.tz"
		;
connectAttr "Character1_RightHandIndex3_scaleX.o" "Character1_RightHandIndex3.sx"
		;
connectAttr "Character1_RightHandIndex3_scaleY.o" "Character1_RightHandIndex3.sy"
		;
connectAttr "Character1_RightHandIndex3_scaleZ.o" "Character1_RightHandIndex3.sz"
		;
connectAttr "Character1_RightHandIndex3_rotateX.o" "Character1_RightHandIndex3.rx"
		;
connectAttr "Character1_RightHandIndex3_rotateY.o" "Character1_RightHandIndex3.ry"
		;
connectAttr "Character1_RightHandIndex3_rotateZ.o" "Character1_RightHandIndex3.rz"
		;
connectAttr "Character1_RightHandIndex3_visibility.o" "Character1_RightHandIndex3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandRing1.is";
connectAttr "Character1_RightHandRing1_scaleX.o" "Character1_RightHandRing1.sx";
connectAttr "Character1_RightHandRing1_scaleY.o" "Character1_RightHandRing1.sy";
connectAttr "Character1_RightHandRing1_scaleZ.o" "Character1_RightHandRing1.sz";
connectAttr "Character1_RightHandRing1_translateX.o" "Character1_RightHandRing1.tx"
		;
connectAttr "Character1_RightHandRing1_translateY.o" "Character1_RightHandRing1.ty"
		;
connectAttr "Character1_RightHandRing1_translateZ.o" "Character1_RightHandRing1.tz"
		;
connectAttr "Character1_RightHandRing1_rotateX.o" "Character1_RightHandRing1.rx"
		;
connectAttr "Character1_RightHandRing1_rotateY.o" "Character1_RightHandRing1.ry"
		;
connectAttr "Character1_RightHandRing1_rotateZ.o" "Character1_RightHandRing1.rz"
		;
connectAttr "Character1_RightHandRing1_visibility.o" "Character1_RightHandRing1.v"
		;
connectAttr "Character1_RightHandRing1.s" "Character1_RightHandRing2.is";
connectAttr "Character1_RightHandRing2_scaleX.o" "Character1_RightHandRing2.sx";
connectAttr "Character1_RightHandRing2_scaleY.o" "Character1_RightHandRing2.sy";
connectAttr "Character1_RightHandRing2_scaleZ.o" "Character1_RightHandRing2.sz";
connectAttr "Character1_RightHandRing2_translateX.o" "Character1_RightHandRing2.tx"
		;
connectAttr "Character1_RightHandRing2_translateY.o" "Character1_RightHandRing2.ty"
		;
connectAttr "Character1_RightHandRing2_translateZ.o" "Character1_RightHandRing2.tz"
		;
connectAttr "Character1_RightHandRing2_rotateX.o" "Character1_RightHandRing2.rx"
		;
connectAttr "Character1_RightHandRing2_rotateY.o" "Character1_RightHandRing2.ry"
		;
connectAttr "Character1_RightHandRing2_rotateZ.o" "Character1_RightHandRing2.rz"
		;
connectAttr "Character1_RightHandRing2_visibility.o" "Character1_RightHandRing2.v"
		;
connectAttr "Character1_RightHandRing2.s" "Character1_RightHandRing3.is";
connectAttr "Character1_RightHandRing3_translateX.o" "Character1_RightHandRing3.tx"
		;
connectAttr "Character1_RightHandRing3_translateY.o" "Character1_RightHandRing3.ty"
		;
connectAttr "Character1_RightHandRing3_translateZ.o" "Character1_RightHandRing3.tz"
		;
connectAttr "Character1_RightHandRing3_scaleX.o" "Character1_RightHandRing3.sx";
connectAttr "Character1_RightHandRing3_scaleY.o" "Character1_RightHandRing3.sy";
connectAttr "Character1_RightHandRing3_scaleZ.o" "Character1_RightHandRing3.sz";
connectAttr "Character1_RightHandRing3_rotateX.o" "Character1_RightHandRing3.rx"
		;
connectAttr "Character1_RightHandRing3_rotateY.o" "Character1_RightHandRing3.ry"
		;
connectAttr "Character1_RightHandRing3_rotateZ.o" "Character1_RightHandRing3.rz"
		;
connectAttr "Character1_RightHandRing3_visibility.o" "Character1_RightHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_Neck.is";
connectAttr "Character1_Neck_scaleX.o" "Character1_Neck.sx";
connectAttr "Character1_Neck_scaleY.o" "Character1_Neck.sy";
connectAttr "Character1_Neck_scaleZ.o" "Character1_Neck.sz";
connectAttr "Character1_Neck_translateX.o" "Character1_Neck.tx";
connectAttr "Character1_Neck_translateY.o" "Character1_Neck.ty";
connectAttr "Character1_Neck_translateZ.o" "Character1_Neck.tz";
connectAttr "Character1_Neck_rotateX.o" "Character1_Neck.rx";
connectAttr "Character1_Neck_rotateY.o" "Character1_Neck.ry";
connectAttr "Character1_Neck_rotateZ.o" "Character1_Neck.rz";
connectAttr "Character1_Neck_visibility.o" "Character1_Neck.v";
connectAttr "Character1_Neck.s" "Character1_Head.is";
connectAttr "Character1_Head_scaleX.o" "Character1_Head.sx";
connectAttr "Character1_Head_scaleY.o" "Character1_Head.sy";
connectAttr "Character1_Head_scaleZ.o" "Character1_Head.sz";
connectAttr "Character1_Head_translateX.o" "Character1_Head.tx";
connectAttr "Character1_Head_translateY.o" "Character1_Head.ty";
connectAttr "Character1_Head_translateZ.o" "Character1_Head.tz";
connectAttr "Character1_Head_rotateX.o" "Character1_Head.rx";
connectAttr "Character1_Head_rotateY.o" "Character1_Head.ry";
connectAttr "Character1_Head_rotateZ.o" "Character1_Head.rz";
connectAttr "Character1_Head_visibility.o" "Character1_Head.v";
connectAttr "Character1_Head.s" "eyes.is";
connectAttr "eyes_translateX.o" "eyes.tx";
connectAttr "eyes_translateY.o" "eyes.ty";
connectAttr "eyes_translateZ.o" "eyes.tz";
connectAttr "eyes_scaleX.o" "eyes.sx";
connectAttr "eyes_scaleY.o" "eyes.sy";
connectAttr "eyes_scaleZ.o" "eyes.sz";
connectAttr "eyes_rotateX.o" "eyes.rx";
connectAttr "eyes_rotateY.o" "eyes.ry";
connectAttr "eyes_rotateZ.o" "eyes.rz";
connectAttr "eyes_visibility.o" "eyes.v";
connectAttr "Character1_Head.s" "jaw.is";
connectAttr "jaw_lockInfluenceWeights.o" "jaw.liw";
connectAttr "jaw_translateX.o" "jaw.tx";
connectAttr "jaw_translateY.o" "jaw.ty";
connectAttr "jaw_translateZ.o" "jaw.tz";
connectAttr "jaw_scaleX.o" "jaw.sx";
connectAttr "jaw_scaleY.o" "jaw.sy";
connectAttr "jaw_scaleZ.o" "jaw.sz";
connectAttr "jaw_rotateX.o" "jaw.rx";
connectAttr "jaw_rotateY.o" "jaw.ry";
connectAttr "jaw_rotateZ.o" "jaw.rz";
connectAttr "jaw_visibility.o" "jaw.v";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Emeny@Archer_Enraged.ma
