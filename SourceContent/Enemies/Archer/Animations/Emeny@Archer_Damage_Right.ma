//Maya ASCII 2016 scene
//Name: Emeny@Archer_Damage_Right.ma
//Last modified: Tue, Nov 24, 2015 12:08:23 PM
//Codeset: 1252
requires maya "2016";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "5D863E12-4CC0-051D-CB95-9EB1B37DA76C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 201.68712039765308 196.68012218311492 220.96227892378573 ;
	setAttr ".r" -type "double3" -23.738352729602362 38.999999999999943 2.0463045806097371e-015 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "72E76207-4040-B50B-AF64-8891E2FBC79F";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 358.33316008794498;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "4A32C304-4402-74C7-C63C-2A8CE8754373";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "4DFA8326-431A-DF29-D082-21B9F732C970";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "A088189E-4295-59F6-B06A-DFB9B5D0382E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "9128C28E-4A21-3A13-A306-FD92C5612245";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "0612EBC9-49A4-E60C-E0D8-CA84961562F5";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "FB3481F4-42EA-9707-DBDB-E680AFD69D9D";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "ref_frame";
	rename -uid "7FAE0B7A-48ED-8D04-867F-FD8E8541E46F";
	addAttr -ci true -sn "refFrame" -ln "refFrame" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "Character1_Hips" -p "ref_frame";
	rename -uid "64698379-453C-00CB-30AE-1E8354EC9F52";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -19.036789801921167 ;
	setAttr ".ssc" no;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftUpLeg" -p "Character1_Hips";
	rename -uid "99C35031-45A5-8BC0-4BD7-4AB9E89DB173";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 90.000000000000014 -12.849604412773132 43.257528613448208 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftLeg" -p "Character1_LeftUpLeg";
	rename -uid "D3F97EE9-48D6-647C-49AB-ABA83D694057";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -19.152297252867996 -13.416343813706657 -33.623675304480329 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFoot" -p "Character1_LeftLeg";
	rename -uid "3C81E8D2-4671-726A-A41D-2F84F7DFCC88";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 25.817647449536953 51.040150860538553 -112.05775014674906 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftToeBase" -p "Character1_LeftFoot";
	rename -uid "D5B459C9-47BE-9DD0-5219-FDAED6950F9C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 43.727116820142825 -29.659481123592514 -4.6723850284209263 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle1" -p "Character1_LeftToeBase";
	rename -uid "7516AAFD-4364-EAAD-B792-FB91697DAB78";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 165.43734692409092 -20.35908719617909 10.197796850486212 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle2" -p "Character1_LeftFootMiddle1";
	rename -uid "D18475BF-441C-A346-6CB7-C5A174B84D31";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 127.90699782306312 -44.626258206212555 -101.65660999147298 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightUpLeg" -p "Character1_Hips";
	rename -uid "E1491C96-4B8C-0C61-693D-5A882DF8F8FB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 133.25752861344816 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightLeg" -p "Character1_RightUpLeg";
	rename -uid "812AACAC-4450-D38E-FABF-EB9558025F10";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -39.772585840344547 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFoot" -p "Character1_RightLeg";
	rename -uid "1EDFF98F-4471-6047-DC0F-19A3C589AAF2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -53.712356932759 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightToeBase" -p "Character1_RightFoot";
	rename -uid "FEF4F8F0-46ED-7CDD-5CD6-20AE10DDF89F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 2.9245623946004821e-006 53.712356932758929 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle1" -p "Character1_RightToeBase";
	rename -uid "8498DA0D-4E4B-2770-1210-55B9FDB09F79";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -4.5380835579220506e-006 -2.7209092876563258e-005 
		39.772585840347155 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle2" -p "Character1_RightFootMiddle1";
	rename -uid "D2F8B011-4EFE-3001-0836-F2B95CFBC077";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -2.8539668357113417e-005 0.00011485155389716205 -133.25752861351003 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine" -p "Character1_Hips";
	rename -uid "5B8F6619-4FDE-69D7-61AF-4E96C9E55650";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 133.25752861344816 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine1" -p "Character1_Spine";
	rename -uid "A6D9F7AC-44E1-D3A5-EFE7-2CB4CCBA1B82";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -34.34645244194413 -26.596597123422644 16.99576897690589 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftShoulder" -p "Character1_Spine1";
	rename -uid "D578A9A7-4964-344E-C6F7-90BF1ACBB58B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -166.39102833412767 21.832119962557094 -120.00622813437471 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftArm" -p "Character1_LeftShoulder";
	rename -uid "351CED96-4568-5EE3-0EF0-97B544780B31";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -172.11387269090633 -44.88241940154802 -37.940590556196071 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftForeArm" -p "Character1_LeftArm";
	rename -uid "B2B7CE52-4896-F737-7177-8299EB103847";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -84.022423618581243 -14.673598567163568 71.813326221989513 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHand" -p "Character1_LeftForeArm";
	rename -uid "15D97137-4F30-4E6E-81D3-2B82F484EAFA";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -24.404340271578391 -34.932697344706668 122.26498801320882 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr -k on ".liw" no;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb1" -p "Character1_LeftHand";
	rename -uid "0277FAED-406A-6551-BC41-D8AEA21F4605";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.1043965330995 61.333484361561069 1.8740740862626108 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb2" -p "Character1_LeftHandThumb1";
	rename -uid "2DF4E79F-4DA8-89F6-F81E-F494052CC133";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -103.44292663098258 20.93424016453444 169.56041330531272 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb3" -p "Character1_LeftHandThumb2";
	rename -uid "1F9C5D67-4AC1-A43B-1B66-E6988F31D088";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 14.174484134960574 -59.672502756712539 -19.020394805426264 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex1" -p "Character1_LeftHand";
	rename -uid "814B3E0C-4703-E660-545D-6A9DE973D2C5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -150.59621970507803 75.681392634287164 -124.28723884791442 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex2" -p "Character1_LeftHandIndex1";
	rename -uid "51F18159-4314-F5B8-2BF4-CE8854E8E054";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -41.093804604175162 -57.602285575728224 -69.242456746392662 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex3" -p "Character1_LeftHandIndex2";
	rename -uid "54AB04C0-47DA-CADE-32A5-A9B866D36BFE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.108297963098213 8.7979835742630801 27.088028901385748 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing1" -p "Character1_LeftHand";
	rename -uid "EE223F47-4DB9-8DB0-9AA5-7E87C77674DB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -141.42437885137375 63.919742192600388 -113.78475086453638 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing2" -p "Character1_LeftHandRing1";
	rename -uid "E8726E6B-41E3-CE14-16E1-3498AC897BD3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -35.368646977348 -76.384823071466059 -48.755963146359043 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing3" -p "Character1_LeftHandRing2";
	rename -uid "7EE3F1E4-4FB5-0BD6-C580-32981A88BF47";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 9.6312888734270121 39.523687300631337 15.3258182753707 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightShoulder" -p "Character1_Spine1";
	rename -uid "76EC0EB4-4332-621B-6E73-B1854C3FCA51";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -164.54352138244408 34.957040538466451 -116.14754006477595 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightArm" -p "Character1_RightShoulder";
	rename -uid "079CF072-495B-2E04-3209-FBA87364A198";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -173.75456654457099 -11.164646449014084 -45.537881339438307 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightForeArm" -p "Character1_RightArm";
	rename -uid "5C0EB060-4F47-34C0-6D5E-DABD7311D005";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 22.753833922449758 50.186429787105524 -101.53518723839815 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHand" -p "Character1_RightForeArm";
	rename -uid "930C9B45-4F96-03AA-8788-1B949515A62D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 48.334026046900341 -3.7701083025157853 -29.882905994214536 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb1" -p "Character1_RightHand";
	rename -uid "5407BDB7-41AD-02DE-3DD4-4EB355B02820";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb2" -p "Character1_RightHandThumb1";
	rename -uid "F306A6A9-4264-D663-7CD0-22BCEAB24772";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb3" -p "Character1_RightHandThumb2";
	rename -uid "D95B7032-41E6-5D48-1BED-47A15AE58214";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex1" -p "Character1_RightHand";
	rename -uid "D3FCC3D9-4F6A-FE0E-F89A-2CB739FA61C1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex2" -p "Character1_RightHandIndex1";
	rename -uid "A2F7C069-4220-BB53-42C3-5F8EACBD15B5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex3" -p "Character1_RightHandIndex2";
	rename -uid "34A2B122-4284-3916-3EDD-138474B3B65C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing1" -p "Character1_RightHand";
	rename -uid "05BA4D7B-4C64-083E-EC7B-70A388A8E903";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing2" -p "Character1_RightHandRing1";
	rename -uid "28864526-47A6-FA8C-8EA1-DCAE4B935B88";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing3" -p "Character1_RightHandRing2";
	rename -uid "D6A19945-4B43-EF1E-C42D-6796CF3554BC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Neck" -p "Character1_Spine1";
	rename -uid "E94B9109-44D9-B103-B1B4-0FB2EB30C919";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 48.229963067601958 30.670012388143238 129.35864775594831 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Head" -p "Character1_Neck";
	rename -uid "693616AE-4521-3A37-7508-EF8C84D2959F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -14.36476107543206 22.377462763663633 -109.58762475494268 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "eyes" -p "Character1_Head";
	rename -uid "5266DDEA-451A-F560-8117-4784750CA34C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -157.21319586594478 -21.144487689504771 -3.4677530572789341 ;
	setAttr ".radi" 4.5;
	setAttr ".fbxID" 5;
createNode joint -n "jaw" -p "Character1_Head";
	rename -uid "6B2BE7B1-4429-91BE-D392-E68DDF6488A2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -157.21319586594478 -21.144487689504771 -3.4677530572789341 ;
	setAttr ".radi" 0.5;
	setAttr -k on ".liw" no;
	setAttr ".fbxID" 5;
createNode animCurveTU -n "Character1_LeftHand_lockInfluenceWeights";
	rename -uid "E63ADD0F-4CF0-32B6-130A-65B359C38FD4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "jaw_lockInfluenceWeights";
	rename -uid "1B84EDD6-4D1F-C02E-0866-27A15265F20B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "Character1_Hips_translateX";
	rename -uid "20606DB3-451A-FD01-1D74-B38E853C4BD5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -3.1944661140441895 1 1.7107508182525635
		 2 6.5091428756713867 3 9.3287868499755859 4 10.339713096618652 5 10.892128944396973
		 6 11.054837226867676 7 10.895874977111816 8 10.481965065002441 9 9.8782510757446289
		 10 9.1491575241088867 11 7.3780479431152344 12 4.4946966171264648 13 1.8132078647613525
		 14 -0.2917359471321106 15 -1.5215039253234863 16 -1.7078191041946411 17 -1.5519556999206543
		 18 -1.2007030248641968 19 -0.80292683839797974 20 -0.50418257713317871;
createNode animCurveTL -n "Character1_Hips_translateY";
	rename -uid "093D16EB-4EC6-92C4-1602-23A220C40211";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 38.122257232666016 1 39.804801940917969
		 2 41.316661834716797 3 42.304634094238281 4 42.976497650146484 5 43.593738555908203
		 6 44.136829376220703 7 44.587772369384766 8 44.922855377197266 9 45.116729736328125
		 10 45.143234252929687 11 44.444679260253906 12 42.914360046386719 13 41.264141082763672
		 14 39.075653076171875 15 37.654891967773437 16 38.021427154541016 17 38.823028564453125
		 18 39.879844665527344 19 41.002792358398438 20 42.001785278320312;
createNode animCurveTL -n "Character1_Hips_translateZ";
	rename -uid "E743B35E-400B-9DEE-76F0-F28085477C03";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -9.1332159042358398 1 -8.8351783752441406
		 2 -8.3693294525146484 3 -8.0819969177246094 4 -8.0464191436767578 5 -8.0613298416137695
		 6 -8.1171960830688477 7 -8.2026538848876953 8 -8.3098869323730469 9 -8.4319086074829102
		 10 -8.5613136291503906 11 -8.8506450653076172 12 -9.2731399536132812 13 -9.5125360488891602
		 14 -9.2837810516357422 15 -8.9514827728271484 16 -8.8878707885742187 17 -8.9527444839477539
		 18 -9.0952854156494141 19 -9.2587356567382812 20 -9.3845930099487305;
createNode animCurveTU -n "Character1_Hips_scaleX";
	rename -uid "44D487A5-43A8-E15E-1118-759AB7A03430";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_Hips_scaleY";
	rename -uid "B3E4B569-4CE1-FC5C-BEA1-3F880777176A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_Hips_scaleZ";
	rename -uid "305A4FE6-4432-E4CB-AC94-B09FC10AA896";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Hips_rotateX";
	rename -uid "5BD00A7F-48D5-85A2-64A1-2AABE14A981C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -20.185800552368164 1 -19.838220596313477
		 2 -19.201509475708008 3 -18.511404037475586 4 -18.047691345214844 5 -17.773082733154297
		 6 -17.620121002197266 7 -17.510786056518555 8 -17.356878280639648 9 -17.061370849609375
		 10 -16.521280288696289 11 -16.197782516479492 12 -15.525666236877441 13 -12.795307159423828
		 14 -4.6069025993347168 15 3.2167062759399414 16 4.6245408058166504 17 3.0920670032501221
		 18 -0.19753600656986237 19 -3.9816784858703613 20 -6.9407939910888672;
createNode animCurveTA -n "Character1_Hips_rotateY";
	rename -uid "87D4463A-486E-C316-3572-71846B860628";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 14.932817459106447 1 9.0425682067871094
		 2 3.1722419261932373 3 -1.3492394685745239 4 -4.3255982398986816 5 -6.6289544105529785
		 6 -8.3188238143920898 7 -9.4514932632446289 8 -10.079622268676758 9 -10.256644248962402
		 10 -10.040426254272461 11 -9.1749105453491211 12 -7.2419004440307617 13 -4.1931352615356445
		 14 0.78620940446853638 15 4.7574868202209473 16 6.7838315963745117 17 8.2776212692260742
		 18 9.4168853759765625 19 10.349584579467773 20 11.395454406738281;
createNode animCurveTA -n "Character1_Hips_rotateZ";
	rename -uid "3EAAAF41-4F2F-8C50-3ACE-8C949F6B9314";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -19.157869338989258 1 -21.698976516723633
		 2 -23.999650955200195 3 -25.427906036376953 4 -26.200855255126953 5 -26.643835067749023
		 6 -26.761129379272461 7 -26.560298919677734 8 -26.060375213623047 9 -25.282882690429688
		 10 -24.24852180480957 11 -22.980443954467773 12 -21.105838775634766 13 -17.823186874389648
		 14 -10.766464233398438 15 -3.8873589038848881 16 -1.9037964344024658 17 -2.2491269111633301
		 18 -4.0203070640563965 19 -6.2189407348632812 20 -7.7957358360290527;
createNode animCurveTU -n "Character1_Hips_visibility";
	rename -uid "661781E6-4F08-5CC6-CAC4-4BA60FAB461D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftUpLeg_translateX";
	rename -uid "C4533554-4AFD-052F-8F58-9D908CA29BAF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.784505844116211;
createNode animCurveTL -n "Character1_LeftUpLeg_translateY";
	rename -uid "8ABA29F5-4972-A8A5-F5AB-39BDE0B39F59";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5774786561451037e-006;
createNode animCurveTL -n "Character1_LeftUpLeg_translateZ";
	rename -uid "DDF767CD-4C36-84DF-75E9-AA85E27B3571";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0264549255371094;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleX";
	rename -uid "EE6AA887-4CAF-3D4F-21D2-31A918AB1304";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleY";
	rename -uid "16EB0574-4D47-3718-1205-7B964B046E76";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleZ";
	rename -uid "CC51B46C-46D4-AE11-8354-A0B326F6F197";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateX";
	rename -uid "38FAC6B3-4F1D-8BC9-CF24-2CB3C7BE1A56";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -9.8310976028442383 1 1.5247946977615356
		 2 12.09931755065918 3 19.966484069824219 4 25.554937362670898 5 29.513711929321286
		 6 31.748722076416016 7 33.773361206054688 8 35.288490295410156 9 35.760887145996094
		 10 34.944561004638672 11 30.849042892456055 12 22.195428848266602 13 11.226763725280762
		 14 -1.1189659833908081 15 -8.8931407928466797 16 -10.977672576904297 17 -11.405634880065918
		 18 -10.047641754150391 19 -7.2874636650085449 20 -4.9229340553283691;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateY";
	rename -uid "CE590198-4E49-FF7C-1D0B-1794CC11E75C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 42.495143890380859 1 35.696907043457031
		 2 28.723709106445313 3 26.79041862487793 4 31.929174423217777 5 39.676887512207031
		 6 44.69842529296875 7 45.129421234130859 8 43.331478118896484 9 40.821342468261719
		 10 39.22198486328125 11 41.61553955078125 12 46.277355194091797 13 47.065658569335938
		 14 38.491344451904297 15 27.903741836547852 16 22.810054779052734 17 20.626516342163086
		 18 20.221769332885742 19 20.939584732055664 20 21.506786346435547;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateZ";
	rename -uid "2E98448D-47C8-0A31-9C6E-D6B967E691A2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 0.68083786964416504 1 3.7704150676727295
		 2 3.993838787078857 3 0.63498950004577637 4 -7.1887421607971191 5 -17.024072647094727
		 6 -23.317642211914063 7 -23.669885635375977 8 -21.406156539916992 9 -18.7470703125
		 10 -17.768558502197266 11 -19.342897415161133 12 -23.857295989990234 13 -30.794466018676758
		 14 -36.751602172851563 15 -35.078411102294922 16 -27.570404052734375 17 -18.088033676147461
		 18 -9.4865694046020508 19 -5.3172812461853027 20 -3.5466642379760742;
createNode animCurveTU -n "Character1_LeftUpLeg_visibility";
	rename -uid "83C44644-401F-AF4E-CAD8-61B1381FC58E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftLeg_translateX";
	rename -uid "6C542915-4260-909B-2D16-20A66D5E6D7B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.968876838684082;
createNode animCurveTL -n "Character1_LeftLeg_translateY";
	rename -uid "9E0EE921-486C-C7AF-4FD5-6180F19C922B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.2585945129394531;
createNode animCurveTL -n "Character1_LeftLeg_translateZ";
	rename -uid "E85C893D-46A6-6258-3894-70B9A7C218A8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 18.197568893432617;
createNode animCurveTU -n "Character1_LeftLeg_scaleX";
	rename -uid "129674CE-4467-1DED-871A-1585C8E68DBD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_LeftLeg_scaleY";
	rename -uid "1CEDA0E7-4019-C7FB-AEC4-2981DC1C8EE4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftLeg_scaleZ";
	rename -uid "B73AF1B4-452B-7187-EFFD-5DBF88B80D04";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftLeg_rotateX";
	rename -uid "CD6F978D-474D-09EF-FD53-DEB1F94AF3F9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 69.033073425292969 1 68.149383544921875
		 2 63.524982452392578 3 57.256740570068359 4 51.729652404785156 5 46.207881927490234
		 6 41.556423187255859 7 38.692516326904297 8 37.184837341308594 9 36.623023986816406
		 10 36.727088928222656 11 40.607536315917969 12 47.444923400878906 13 53.135009765625
		 14 59.714286804199219 15 63.243862152099609 16 62.471263885498054 17 61.180873870849616
		 18 59.015693664550781 19 55.696918487548828 20 52.102336883544922;
createNode animCurveTA -n "Character1_LeftLeg_rotateY";
	rename -uid "9BF70B99-45DE-A3E9-C589-289C8385E1D4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 5.8530902862548828 1 13.174592018127441
		 2 20.478410720825195 3 20.793813705444336 4 10.879297256469727 5 -2.2020730972290039
		 6 -9.6240310668945313 7 -9.5738048553466797 8 -6.4633550643920898 9 -2.7693419456481934
		 10 -0.82404720783233643 11 -3.132288932800293 12 -8.7298507690429687 13 -12.984224319458008
		 14 -11.601963996887207 15 -4.3511028289794922 16 5.01654052734375 17 14.408246040344238
		 18 21.226293563842773 19 22.443359375 20 21.536457061767578;
createNode animCurveTA -n "Character1_LeftLeg_rotateZ";
	rename -uid "F4523F20-4659-48F3-F272-1F8924703C56";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 26.629871368408203 1 27.084991455078125
		 2 24.702226638793945 3 20.671701431274414 4 18.678779602050781 5 19.077934265136719
		 6 20.185569763183594 7 19.856374740600586 8 18.479843139648438 9 16.877172470092773
		 10 16.105451583862305 11 17.967212677001953 12 20.688808441162109 13 21.850666046142578
		 14 22.411705017089844 15 23.460609436035156 16 23.758430480957031 17 23.288333892822266
		 18 21.775270462036133 19 19.507692337036133 20 17.270214080810547;
createNode animCurveTU -n "Character1_LeftLeg_visibility";
	rename -uid "4B71501E-459E-9FC4-1D06-4A932BC8FD04";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftFoot_translateX";
	rename -uid "93644D58-4058-7413-CC3E-B69D94466B7B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5243232250213623;
createNode animCurveTL -n "Character1_LeftFoot_translateY";
	rename -uid "1BB5D516-4DF9-BD4E-1500-F7886CCFFAE9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.000351905822754;
createNode animCurveTL -n "Character1_LeftFoot_translateZ";
	rename -uid "2D94E750-47A9-4428-C2F4-ECAA847546F7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.967419624328613;
createNode animCurveTU -n "Character1_LeftFoot_scaleX";
	rename -uid "10BBF891-421F-5CC3-905B-8F8C9414DF68";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFoot_scaleY";
	rename -uid "B95546A9-4725-1FB0-7BF7-7FAC42E5942D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleZ";
	rename -uid "4AF08E44-4430-FE13-F62A-1D959D04B899";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999964237213135;
createNode animCurveTA -n "Character1_LeftFoot_rotateX";
	rename -uid "3FA40926-4FD8-50A4-BA8B-EDA3ED2E09B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 46.932594299316406 1 47.447700500488281
		 2 46.469051361083984 3 43.349998474121094 4 37.070716857910156 5 28.695941925048828
		 6 23.482767105102539 7 22.98969841003418 8 24.395530700683594 9 26.24608039855957
		 10 27.381790161132813 11 28.032184600830078 12 28.374992370605469 13 28.83173751831055
		 14 32.900703430175781 15 39.204013824462891 16 43.165390014648438 17 45.821662902832031
		 18 46.2996826171875 19 44.0809326171875 20 41.134159088134766;
createNode animCurveTA -n "Character1_LeftFoot_rotateY";
	rename -uid "5D569CCE-4397-50E4-F323-1486A865837A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -12.968051910400391 1 -18.346992492675781
		 2 -22.032846450805664 3 -25.112062454223633 4 -29.700407028198242 5 -33.292282104492188
		 6 -33.522289276123047 7 -31.718339920043945 8 -29.341743469238278 9 -27.003591537475586
		 10 -25.390569686889648 11 -25.369327545166016 12 -25.935014724731445 13 -25.702011108398437
		 14 -25.27001953125 15 -22.393016815185547 16 -17.600870132446289 17 -12.498620986938477
		 18 -8.5720882415771484 19 -7.5853986740112305 20 -7.5173330307006836;
createNode animCurveTA -n "Character1_LeftFoot_rotateZ";
	rename -uid "86596CC8-464D-B0DB-E98F-56A87C026C68";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 16.786615371704102 1 18.006828308105469
		 2 18.649436950683594 3 18.875572204589844 4 19.41920280456543 5 20.527400970458984
		 6 20.95941162109375 7 20.427581787109375 8 19.630996704101563 9 18.830463409423828
		 10 18.278293609619141 11 18.30335807800293 12 18.479223251342773 13 18.315927505493164
		 14 18.238870620727539 15 17.959808349609375 16 17.503137588500977 17 16.937047958374023
		 18 16.141262054443359 19 15.311318397521974 20 14.605794906616211;
createNode animCurveTU -n "Character1_LeftFoot_visibility";
	rename -uid "5BA1C6ED-41BE-9965-97F9-1E9846E9A986";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftToeBase_translateX";
	rename -uid "B9755628-43FF-3998-9A2E-AF8E1D9B70E2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.585639476776123;
createNode animCurveTL -n "Character1_LeftToeBase_translateY";
	rename -uid "58A86B26-4E1B-32FC-1B9C-DD955BD8502E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3360438346862793;
createNode animCurveTL -n "Character1_LeftToeBase_translateZ";
	rename -uid "47B764E3-4DE6-DED3-126F-2AAF9A7E168A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.4897398948669434;
createNode animCurveTU -n "Character1_LeftToeBase_scaleX";
	rename -uid "04FA47FD-4B2E-1290-A413-899F03D7E929";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftToeBase_scaleY";
	rename -uid "56713233-4910-2F94-3943-33B744831C20";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftToeBase_scaleZ";
	rename -uid "FFB0DFFC-48D6-7278-60FA-CA8EAF769AE9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_LeftToeBase_rotateX";
	rename -uid "3E8B50E9-4F7E-3F95-D7F3-A4955156A900";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4390260094643281e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateY";
	rename -uid "917250AF-4668-A5EC-6085-D78C39E03DD8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.9402941475732405e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateZ";
	rename -uid "436073DA-42E8-12CD-0F2C-4DAA54158673";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.823593912130832e-010;
createNode animCurveTU -n "Character1_LeftToeBase_visibility";
	rename -uid "A1FD3FD0-4B25-11C4-624B-D88710F3E785";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateX";
	rename -uid "3DC95342-4129-4089-E9C6-E7A742595B70";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7153444290161133;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateY";
	rename -uid "6DD7FE46-433B-D647-B987-E28AF28D8213";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.0020139217376709;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateZ";
	rename -uid "D67C49E7-4A0B-62E0-94F8-74BE7E08CD8C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.1786108016967773;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleX";
	rename -uid "5FAE6AA5-460F-909D-E106-1DA9F7512D87";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleY";
	rename -uid "54FEA644-4A2F-80ED-88DC-DBBFBF07712E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999940395355225;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleZ";
	rename -uid "3842E53B-44E9-864A-4944-C1B829CDA770";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateX";
	rename -uid "9A272CA7-478B-013E-8A62-27A139B5854B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1055465432008305e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateY";
	rename -uid "0912A345-4788-30B1-3D0A-4CB4BB43C683";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.2440958808166442e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateZ";
	rename -uid "583AD575-4C43-EA9F-6EE6-24A3AA7B3005";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.5839070317299502e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle1_visibility";
	rename -uid "0A5FD438-40C9-32E8-2F9B-82BD5A33AA49";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateX";
	rename -uid "08D0A75B-4A34-CD64-B027-409A85036F93";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4684751033782959;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateY";
	rename -uid "895470A4-4E1D-FACD-49DB-BF9CA2CBBD2C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.1393222808837891;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateZ";
	rename -uid "0589DCE2-466B-D5A7-37B6-6F867088F1AC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.3014278411865234;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleX";
	rename -uid "ABB7F7B8-4CAF-8D34-E635-0C993F0409A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleY";
	rename -uid "2F2B5E35-4751-068C-F020-8F97F763FAB7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleZ";
	rename -uid "A5C001FD-498C-DC8B-D520-548A1DD81084";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateX";
	rename -uid "DA2DA319-443C-AAF4-1B99-DDAA0A7327D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7528370976904739e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateY";
	rename -uid "C1CD01E2-46E3-6131-F47E-E6B3BFDB9FDA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.3426270336417474e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateZ";
	rename -uid "FBC0B95D-4AF3-8CC4-B046-4F8B5B4727DD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.2151169483447575e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle2_visibility";
	rename -uid "21282F97-42E3-E4B2-3880-EBBD6E29E2EE";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightUpLeg_translateX";
	rename -uid "8AFE58F6-4342-D46B-FAAD-9C939E0FE89D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.489771842956543;
createNode animCurveTL -n "Character1_RightUpLeg_translateY";
	rename -uid "C16FA5B6-45C6-2937-8D5D-6E86C207988C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.6505136489868164;
createNode animCurveTL -n "Character1_RightUpLeg_translateZ";
	rename -uid "974E3D19-46B1-12C5-A14F-E69B72577DC1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0264549255371094;
createNode animCurveTU -n "Character1_RightUpLeg_scaleX";
	rename -uid "E0F096B8-41BE-2DC1-B687-CAA33497737F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightUpLeg_scaleY";
	rename -uid "849F96B6-4444-A399-E579-C3B62DEDEAC8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightUpLeg_scaleZ";
	rename -uid "780F287C-4280-E9C2-73F4-0389422A21BA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightUpLeg_rotateX";
	rename -uid "3F697C85-49E9-E54E-AF59-0397F7467115";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -31.971200942993161 1 -34.861560821533203
		 2 -34.091941833496094 3 -32.777549743652344 4 -33.328907012939453 5 -34.459163665771484
		 6 -35.743862152099609 7 -36.740264892578125 8 -36.964271545410156 9 -35.879100799560547
		 10 -32.895542144775391 11 -21.713720321655273 12 -6.455939769744873 13 2.9033157825469971
		 14 2.6707196235656738 15 -21.134140014648437 16 -39.28814697265625 17 -36.220638275146484
		 18 -31.016862869262695 19 -31.20966720581055 20 -31.774538040161133;
createNode animCurveTA -n "Character1_RightUpLeg_rotateY";
	rename -uid "61207C52-47FE-394A-9DE8-56915439C165";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 31.488998413085941 1 20.150178909301758
		 2 18.26788330078125 3 18.725324630737305 4 18.773372650146484 5 19.606529235839844
		 6 20.923946380615234 7 22.474922180175781 8 24.063596725463867 9 25.488735198974609
		 10 26.456855773925781 11 22.325634002685547 12 10.516461372375488 13 4.6195693016052246
		 14 31.360027313232425 15 58.90911865234375 16 62.260051727294929 17 58.174167633056648
		 18 50.702518463134766 19 42.850112915039063 20 36.258934020996094;
createNode animCurveTA -n "Character1_RightUpLeg_rotateZ";
	rename -uid "69C014A5-4721-943A-DC74-0A979660934E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -10.728629112243652 1 -6.6581287384033203
		 2 -9.6378240585327148 3 -14.050563812255859 4 -14.205281257629395 5 -14.604058265686037
		 6 -14.922117233276367 7 -14.885056495666504 8 -14.263875961303711 9 -12.869914054870605
		 10 -10.553767204284668 11 -2.0972049236297607 12 6.164525032043457 13 5.5909008979797363
		 14 -0.48156926035881037 15 -19.00592041015625 16 -32.825332641601562 17 -30.424398422241211
		 18 -24.671302795410156 19 -20.986116409301758 20 -18.08891487121582;
createNode animCurveTU -n "Character1_RightUpLeg_visibility";
	rename -uid "B8D7B324-4B04-8FAC-118D-A7AE8901EA82";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightLeg_translateX";
	rename -uid "D9F796D9-45FD-B538-6F2C-159C7F28777D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -18.197565078735352;
createNode animCurveTL -n "Character1_RightLeg_translateY";
	rename -uid "E0478159-485B-93D0-0ACF-7DB2083EE410";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.1862249374389648;
createNode animCurveTL -n "Character1_RightLeg_translateZ";
	rename -uid "47BDD813-493D-8ADA-16EA-B39A4EEF8633";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.5520458221435547;
createNode animCurveTU -n "Character1_RightLeg_scaleX";
	rename -uid "A297D47F-4CB9-1D43-99CD-DDAE6077D8A1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightLeg_scaleY";
	rename -uid "EDC27152-4DA0-F137-A10F-848D46D36179";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightLeg_scaleZ";
	rename -uid "FCF4BDD9-4B59-0B48-3F70-5EAFACC48F77";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightLeg_rotateX";
	rename -uid "33AE2213-4B45-0168-0561-51BDA1587410";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 29.204553604125977 1 27.947832107543945
		 2 30.203739166259766 3 34.909114837646484 4 34.602382659912109 5 35.022045135498047
		 6 35.491115570068359 7 35.530139923095703 8 34.822769165039063 9 33.154579162597656
		 10 30.311153411865238 11 18.117771148681641 12 -6.13568115234375 13 -20.89317512512207
		 14 3.5964500904083252 15 33.602519989013672 16 42.111660003662109 17 42.645137786865234
		 18 39.239101409912109 19 35.622982025146484 20 32.307552337646484;
createNode animCurveTA -n "Character1_RightLeg_rotateY";
	rename -uid "86C54F4D-4861-0C6E-75C9-57975B5EFB48";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -41.620372772216797 1 -19.459417343139648
		 2 -32.686321258544922 3 -52.040641784667969 4 -53.507888793945313 5 -55.081302642822266
		 6 -56.555320739746094 7 -57.66380310058593 8 -58.157115936279297 9 -57.834148406982422
		 10 -56.571250915527344 11 -48.972381591796875 12 -35.181613922119141 13 -25.017917633056641
		 14 -43.286869049072266 15 -52.782955169677734 16 -50.283866882324219 17 -47.168174743652344
		 18 -41.718624114990234 19 -33.934368133544922 20 -26.452003479003906;
createNode animCurveTA -n "Character1_RightLeg_rotateZ";
	rename -uid "7D4E51E4-46AB-E410-88C4-C894462A0EA6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -2.3412070274353027 1 2.7321121692657471
		 2 0.801067054271698 3 -6.2987980842590332 4 -7.6046605110168457 5 -8.8717575073242187
		 6 -10.196456909179687 7 -11.451179504394531 8 -12.348752021789551 9 -12.593086242675781
		 10 -12.107241630554199 11 -8.6489334106445313 12 -4.7919297218322754 13 -2.5549764633178711
		 14 -7.231861114501954 15 -7.3712544441223153 16 -1.9643185138702395 17 -0.015457130037248133
		 18 0.88127791881561279 19 2.0202991962432861 20 2.6833248138427734;
createNode animCurveTU -n "Character1_RightLeg_visibility";
	rename -uid "CFBEAACE-42FF-CB3F-6FF3-358C8857D419";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightFoot_translateX";
	rename -uid "A7EE26E3-40E4-4040-8864-68BCAC13DD63";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -18.25177001953125;
createNode animCurveTL -n "Character1_RightFoot_translateY";
	rename -uid "BE828C67-4E16-90E0-4412-DB90D7A614C7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.0794563293457031;
createNode animCurveTL -n "Character1_RightFoot_translateZ";
	rename -uid "D710BEEC-40A3-6EAF-6061-E0958B24EA88";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5524176359176636;
createNode animCurveTU -n "Character1_RightFoot_scaleX";
	rename -uid "7C4EC5E1-4A08-DF53-3D92-3DB8E09D53A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFoot_scaleY";
	rename -uid "66D4E673-4DAF-34DA-5620-24A2D933EA5F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFoot_scaleZ";
	rename -uid "1E74F0E1-4249-8ABE-78B7-CA8372EAACB1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightFoot_rotateX";
	rename -uid "83BB2B58-4091-8B34-48E5-0F8A4C3E97DE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -12.336989402770996 1 -2.1585068702697754
		 2 0.81103056669235229 3 5.1472659111022949 4 14.111639976501465 5 22.669750213623047
		 6 30.221027374267578 7 36.073326110839844 8 39.519157409667969 9 39.834968566894531
		 10 36.200782775878906 11 22.785303115844727 12 8.9187479019165039 13 5.6276865005493164
		 14 -6.4647679328918457 15 -8.6057004928588867 16 -4.9393038749694824 17 -5.0689167976379395
		 18 -5.4500670433044434 19 -3.7915906906127925 20 -1.9345545768737795;
createNode animCurveTA -n "Character1_RightFoot_rotateY";
	rename -uid "BE66B8DF-4F1D-DBE8-98A0-8A99377C9F6D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 4.6104865074157715 1 0.26236343383789063
		 2 -1.2500149011611938 3 -2.9521679878234863 4 -6.7534399032592773 5 -9.655731201171875
		 6 -11.458806991577148 7 -12.309679985046387 8 -12.570220947265625 9 -12.580435752868652
		 10 -12.34077262878418 11 -11.574318885803223 12 -7.5367479324340811 13 -4.499852180480957
		 14 2.9253473281860352 15 3.3664696216583252 16 1.6643025875091553 17 2.0307722091674805
		 18 2.4015464782714844 19 1.7339251041412354 20 0.77543109655380249;
createNode animCurveTA -n "Character1_RightFoot_rotateZ";
	rename -uid "F485C3D3-4255-AF33-50C5-969019801C7C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -1.2427666187286377 1 6.3683700561523438
		 2 7.7215991020202637 3 6.2360787391662598 4 4.4401102066040039 5 1.371757984161377
		 6 -2.0497331619262695 7 -4.9653329849243164 8 -6.6498985290527344 9 -6.5462284088134766
		 10 -4.2754149436950684 11 5.2741756439208984 12 13.951021194458008 13 13.938156127929688
		 14 10.661031723022461 15 -6.8726186752319336 16 -13.062749862670898 17 -11.131319046020508
		 18 -7.0951814651489258 19 -3.6847476959228516 20 -0.61463683843612671;
createNode animCurveTU -n "Character1_RightFoot_visibility";
	rename -uid "6B75ACDE-4B4E-441D-0B0A-53A6D8E71B81";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightToeBase_translateX";
	rename -uid "7B7957A5-4C88-46F5-483D-FD8B3B54AA56";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2767767906188965;
createNode animCurveTL -n "Character1_RightToeBase_translateY";
	rename -uid "20F2F5EE-4633-B538-BC83-E382F4569D14";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.0139193534851074;
createNode animCurveTL -n "Character1_RightToeBase_translateZ";
	rename -uid "C2E59C69-4D31-ADE3-6214-7EBD0158A991";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8259720802307129;
createNode animCurveTU -n "Character1_RightToeBase_scaleX";
	rename -uid "BE5475AB-4237-ABD9-18E2-13B7C92865A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightToeBase_scaleY";
	rename -uid "986F0B53-4A1E-273A-FCCB-32999488FADD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightToeBase_scaleZ";
	rename -uid "478C480B-4938-C3B6-EC44-FDADB6DA387A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightToeBase_rotateX";
	rename -uid "A7304853-49B4-CBE6-1C1C-35AF856592A2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.6224084298244179e-009;
createNode animCurveTA -n "Character1_RightToeBase_rotateY";
	rename -uid "3A5C3F30-40F3-BD4F-1D41-27A85860CAA8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1252394155336631e-010;
createNode animCurveTA -n "Character1_RightToeBase_rotateZ";
	rename -uid "C17E5972-4160-0CCB-0382-F989CA462A38";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.1973689775315393e-010;
createNode animCurveTU -n "Character1_RightToeBase_visibility";
	rename -uid "E8E4882A-43C5-BF6D-BAB1-D29B720DC967";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateX";
	rename -uid "54D03D9F-4FA8-8919-C742-5D957620DF8A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.0018006421159952879;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateY";
	rename -uid "6126F24B-43A4-8B80-4AEE-1997C2A31C24";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.0064689838327467442;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateZ";
	rename -uid "570CBE09-4CE9-526D-CBD8-89BF95BA020D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.6805553436279297;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleX";
	rename -uid "58405D3F-4F24-38BA-C4EC-3C977C2C2AF5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleY";
	rename -uid "9B7F9F15-4662-3402-E89F-0DB577541B43";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleZ";
	rename -uid "3ECEA7B9-4313-B0D2-1272-96B7075BDA1D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateX";
	rename -uid "EF378667-49AB-D8EB-5A79-529697401674";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.1508409215064148e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateY";
	rename -uid "26F0EB98-41D7-51CA-797B-95BE830BAFF3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0139022388908643e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateZ";
	rename -uid "2B88F20A-4A79-3564-404D-EDA96B2EE95E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8469210605331909e-009;
createNode animCurveTU -n "Character1_RightFootMiddle1_visibility";
	rename -uid "29265A7D-4308-D8DE-5B0A-82A87C9FE333";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateX";
	rename -uid "1F82A842-4A47-8887-B6C0-5DA0909E25A7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.5860608729999512e-006;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateY";
	rename -uid "0F894CE8-4631-1A48-5545-179C8A87542F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.3304366904048948e-007;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateZ";
	rename -uid "27BB33FD-4507-8233-A28B-90A7B11C42DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.0234856605529785;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleX";
	rename -uid "02846D69-41B7-1C77-FF10-55B4E61A18E4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleY";
	rename -uid "93DB9FE1-4075-E175-C6A6-BF96A3872645";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleZ";
	rename -uid "1EAFD37C-426C-1C67-F219-009CEFBC4798";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateX";
	rename -uid "01747987-4C65-14A1-54F4-6A84ABFE8F5E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3944759236217408e-010;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateY";
	rename -uid "CFE6EB96-40D5-7296-5B08-7CB13C21E900";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.127252731389035e-009;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateZ";
	rename -uid "C2598456-40EF-F9C9-4D68-B492E0384947";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.1772370573103217e-009;
createNode animCurveTU -n "Character1_RightFootMiddle2_visibility";
	rename -uid "51DF1BB9-4DF6-060C-0655-24B8231ABBAC";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_Spine_translateX";
	rename -uid "A2C76ECB-4C2E-FA7F-6D32-9888B5C6820F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6075620651245117;
createNode animCurveTL -n "Character1_Spine_translateY";
	rename -uid "9C8ADFA8-443C-8984-5F62-2DB58C640886";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.353469848632812;
createNode animCurveTL -n "Character1_Spine_translateZ";
	rename -uid "71B9EBC0-417E-2325-E733-CD9C8378C2AD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2061522006988525;
createNode animCurveTU -n "Character1_Spine_scaleX";
	rename -uid "37E80281-4B5C-35EA-5507-0F953CA3A7C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine_scaleY";
	rename -uid "1C00346B-415D-0725-5B79-788DD98A2612";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine_scaleZ";
	rename -uid "A3153D5A-4761-3C97-7A0C-B0AE8D1DF130";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_Spine_rotateX";
	rename -uid "3DE487C3-4838-9220-9DCC-5898FF7EEC56";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -3.0937561988830566 1 -13.882601737976074
		 2 -24.793502807617188 3 -30.994138717651364 4 -32.527488708496094 5 -32.88031005859375
		 6 -32.248195648193359 7 -30.835639953613278 8 -28.853937149047855 9 -26.510374069213867
		 10 -24.004110336303711 11 -20.330778121948242 12 -15.130402565002441 13 -9.3854751586914062
		 14 -4.1405296325683594 15 -0.7729305624961853 16 0.272297203540802 17 -0.19254021346569061
		 18 -1.5248223543167114 19 -3.0796661376953125 20 -4.2090029716491699;
createNode animCurveTA -n "Character1_Spine_rotateY";
	rename -uid "444CB36B-4CB8-B1DE-4407-069B9D52251C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -12.550650596618652 1 -6.6687746047973633
		 2 -1.0017030239105225 3 2.0341308116912842 4 2.7797667980194092 5 2.9772837162017822
		 6 2.7285716533660889 7 2.1243231296539307 8 1.2497854232788086 9 0.19086487591266632
		 10 -0.96201199293136597 11 -2.7171261310577393 12 -5.3296289443969727 13 -8.2733736038208008
		 14 -10.951260566711426 15 -12.724157333374023 16 -13.430523872375488 17 -13.422149658203125
		 18 -12.997954368591309 19 -12.460916519165039 20 -12.116277694702148;
createNode animCurveTA -n "Character1_Spine_rotateZ";
	rename -uid "46810E93-40B1-4A42-447C-A780D27B49E6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 8.1610126495361328 1 2.2649776935577393
		 2 -4.915341854095459 3 -9.6208286285400391 4 -10.937870025634766 5 -11.372776985168457
		 6 -11.101574897766113 7 -10.322883605957031 8 -9.2327909469604492 9 -8.0248556137084961
		 10 -6.8852438926696777 11 -5.3431172370910645 12 -3.2688291072845459 13 -1.4515594244003296
		 14 -0.31586870551109314 15 0.53917765617370605 16 1.4697809219360352 17 2.4717750549316406
		 18 3.5335617065429687 19 4.6117568016052246 20 5.6667900085449219;
createNode animCurveTU -n "Character1_Spine_visibility";
	rename -uid "4A250C78-4DC8-4C70-F9AE-8DB82B35B844";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_Spine1_translateX";
	rename -uid "786999FF-4C51-FE4B-39CE-B3AA1EB7E9C8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.930038452148437;
createNode animCurveTL -n "Character1_Spine1_translateY";
	rename -uid "A2A26635-48CA-430D-2F39-FA902CD6E72E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1661763191223145;
createNode animCurveTL -n "Character1_Spine1_translateZ";
	rename -uid "D3933B01-44F5-FEDB-946E-35901FB7F509";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.40121376514434814;
createNode animCurveTU -n "Character1_Spine1_scaleX";
	rename -uid "F95E5FC7-449D-45CD-D730-ADB150D955F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_Spine1_scaleY";
	rename -uid "C2C75522-4668-85E3-5D84-6CB65D29BA81";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine1_scaleZ";
	rename -uid "E0529C28-4A42-6168-7886-D090F371321D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_Spine1_rotateX";
	rename -uid "59662C33-4805-69DF-510E-028054B04635";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -19.482179641723633 1 -24.312076568603516
		 2 -28.579914093017578 3 -30.799421310424801 4 -31.422870635986328 5 -31.627637863159183
		 6 -31.518991470336914 7 -31.196319580078125 8 -30.755489349365231 9 -30.291667938232422
		 10 -29.909221649169922 11 -29.530666351318359 12 -29.064687728881832 13 -28.466466903686523
		 14 -27.711589813232422 15 -26.915847778320312 16 -26.210947036743164 17 -25.689054489135742
		 18 -25.50053596496582 19 -25.676265716552734 20 -26.060359954833984;
createNode animCurveTA -n "Character1_Spine1_rotateY";
	rename -uid "2A5D130B-4BF6-D292-1D49-5AA8F646B4BF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -15.921777725219725 1 -7.3402848243713379
		 2 1.8937879800796507 3 7.9185385704040527 4 10.352977752685547 5 11.830867767333984
		 6 12.546054840087891 7 12.71351146697998 8 12.554365158081055 9 12.287318229675293
		 10 12.117221832275391 11 11.748374938964844 12 10.659076690673828 13 8.9261455535888672
		 14 6.9034414291381836 15 4.9664716720581055 16 3.1640422344207764 17 1.308417797088623
		 18 -0.6593785285949707 19 -2.7181589603424072 20 -4.7502837181091309;
createNode animCurveTA -n "Character1_Spine1_rotateZ";
	rename -uid "A7603CD4-409B-48A0-85F0-E297CD5F4297";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 23.666675567626953 1 23.643590927124023
		 2 23.426443099975586 3 23.242143630981445 4 23.301435470581055 5 23.677787780761719
		 6 24.116462707519531 7 24.368858337402344 8 24.193887710571289 9 23.355606079101563
		 10 21.618307113647461 11 17.53057861328125 12 11.039191246032715 13 3.9955918788909908
		 14 -1.7939528226852419 15 -4.3408646583557129 16 -2.7397899627685547 17 1.6996724605560303
		 18 7.7513995170593253 19 14.200176239013672 20 19.853261947631836;
createNode animCurveTU -n "Character1_Spine1_visibility";
	rename -uid "76CBF33E-47FA-ECD6-50DE-E3975B4F10E4";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftShoulder_translateX";
	rename -uid "724A7957-4FD2-F0AD-41C4-029B57067ED4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.0842323303222656;
createNode animCurveTL -n "Character1_LeftShoulder_translateY";
	rename -uid "7A91AF7D-43AA-8DDF-1C59-6D8D366C21D1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.604855537414551;
createNode animCurveTL -n "Character1_LeftShoulder_translateZ";
	rename -uid "9FDDADB8-40D7-7A5E-CE5E-A68D15035484";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6404602527618408;
createNode animCurveTU -n "Character1_LeftShoulder_scaleX";
	rename -uid "9D8684A2-44B2-FC5A-E461-CA98E588052D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftShoulder_scaleY";
	rename -uid "AF3D4701-4CF0-4189-6F82-798950EA4C15";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftShoulder_scaleZ";
	rename -uid "6F97793A-41F6-A3C5-553A-07A985360839";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftShoulder_rotateX";
	rename -uid "6F875777-4BBE-51A3-ACB3-43B0C58BAD8E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  0 21.492620468139648 1 14.571985244750975
		 2 9.3962621688842773 3 7.0595545768737793 4 6.2944345474243164 5 5.8860459327697754
		 6 5.6905794143676758 7 5.6176614761352539 8 5.6011791229248047 9 5.5823736190795898
		 10 5.5044584274291992 11 5.4026598930358887 12 5.3392653465270996 13 5.3041472434997559
		 14 5.2888422012329102 15 5.2854747772216797 16 5.2854747772216797;
createNode animCurveTA -n "Character1_LeftShoulder_rotateY";
	rename -uid "2A9AC9F6-466F-A928-5F60-AB8C3AA4C87B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  0 -34.12640380859375 1 -31.230632781982422
		 2 -27.313291549682617 3 -24.487579345703125 4 -23.254888534545898 5 -22.490707397460938
		 6 -22.087568283081055 7 -21.926261901855469 8 -21.883373260498047 9 -21.834390640258789
		 10 -21.654533386230469 11 -21.411867141723633 12 -21.248550415039063 13 -21.150104522705078
		 14 -21.101850509643555 15 -21.089040756225586 16 -21.089040756225586;
createNode animCurveTA -n "Character1_LeftShoulder_rotateZ";
	rename -uid "E0853A1A-413C-991D-7017-DB87DFE1B652";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  0 -38.991798400878906 1 -27.064411163330078
		 2 -16.016550064086914 3 -9.5139532089233398 4 -6.9228410720825195 5 -5.3810763359069824
		 6 -4.6025857925415039 7 -4.3239006996154785 8 -4.2912659645080566 9 -4.253812313079834
		 10 -3.961467027664185 11 -3.5789222717285156 12 -3.3930132389068604 13 -3.3392097949981689
		 14 -3.3536090850830078 15 -3.372546911239624 16 -3.372546911239624;
createNode animCurveTU -n "Character1_LeftShoulder_visibility";
	rename -uid "678B9994-4D9B-9863-1271-FD82482AC051";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftArm_translateX";
	rename -uid "D6872BCE-482D-F088-623F-298D4353C20E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 9.2092180252075195;
createNode animCurveTL -n "Character1_LeftArm_translateY";
	rename -uid "277D7536-4674-24EF-AD01-358367177C33";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.4172050952911377;
createNode animCurveTL -n "Character1_LeftArm_translateZ";
	rename -uid "945DBAF8-4A8D-9EC5-70DA-31BCDA832A57";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.69030004739761353;
createNode animCurveTU -n "Character1_LeftArm_scaleX";
	rename -uid "35A96F3D-4974-E61E-9964-E5A9AC2D9E61";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleY";
	rename -uid "0EA72EF7-4D2D-32C9-321C-7290A8E5213C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleZ";
	rename -uid "99F92FF1-4F99-30A3-3563-6B9F32EE462F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_LeftArm_rotateX";
	rename -uid "4193F97B-46E4-64E7-6BBD-50ACA26DAEDD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -62.76152420043946 1 -64.197257995605469
		 2 -52.416110992431641 3 -34.903728485107422 4 -17.351833343505859 5 2.38002610206604
		 6 22.682771682739258 7 41.009941101074219 8 56.013519287109375 9 67.385650634765625
		 10 73.866706848144531 11 68.021759033203125 12 48.506000518798828 13 31.264602661132812
		 14 26.439262390136719 15 29.457351684570316 16 34.56475830078125 17 39.769256591796875
		 18 44.598350524902344 19 49.772083282470703 20 59.225181579589844;
createNode animCurveTA -n "Character1_LeftArm_rotateY";
	rename -uid "8561C08D-41F8-76A9-3814-AB9F9189C1DD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -51.951694488525391 1 -28.11097526550293
		 2 -7.6112589836120614 3 -2.7230033874511719 4 -6.6310877799987793 5 -9.0562458038330078
		 6 -6.5103664398193359 7 1.4952652454376221 8 12.666914939880371 9 24.080619812011719
		 10 33.585174560546875 11 41.109806060791016 12 42.725543975830078 13 33.251602172851563
		 14 17.93925666809082 15 3.9899966716766357 16 -8.6821203231811523 17 -23.014884948730469
		 18 -38.529876708984375 19 -54.456462860107422 20 -69.777976989746094;
createNode animCurveTA -n "Character1_LeftArm_rotateZ";
	rename -uid "7C64A8F8-4984-4D74-4C97-CCAE2F82209D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -3.331554651260376 1 13.226938247680664
		 2 28.699724197387695 3 37.135456085205078 4 33.694377899169922 5 25.104972839355469
		 6 14.303436279296875 7 5.300168514251709 8 0.21923339366912842 9 -1.548357367515564
		 10 -2.8628370761871338 11 -11.386781692504883 12 -31.828178405761719 13 -52.868057250976563
		 14 -65.478981018066406 15 -71.585678100585937 16 -73.469703674316406 17 -73.254692077636719
		 18 -72.390098571777344 19 -72.443603515625 20 -77.858016967773438;
createNode animCurveTU -n "Character1_LeftArm_visibility";
	rename -uid "5FBDC751-4D72-6AD9-770D-0FB9B887748A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftForeArm_translateX";
	rename -uid "0A1CB2BB-4A38-518F-7233-7A9C706B29E0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.0427684783935547;
createNode animCurveTL -n "Character1_LeftForeArm_translateY";
	rename -uid "43A0064D-4DF1-9D31-0F63-7AAA7EF0F6A8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.323208808898926;
createNode animCurveTL -n "Character1_LeftForeArm_translateZ";
	rename -uid "3CA77F28-4297-A4E6-C157-BD97D0B5CB02";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 16.678167343139648;
createNode animCurveTU -n "Character1_LeftForeArm_scaleX";
	rename -uid "D9E630C8-48F7-ABE4-3BCB-D9BD69084124";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftForeArm_scaleY";
	rename -uid "6A8A9418-4082-4B92-7857-5EADFD8E022F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftForeArm_scaleZ";
	rename -uid "FF465F99-441D-8830-48C1-DBBF429C7310";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftForeArm_rotateX";
	rename -uid "8DF4F59F-480D-C38D-1798-65AB766E0D09";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -28.013322830200195 1 -29.148710250854489
		 2 -30.274147033691403 3 -31.778060913085937 4 -33.878414154052734 5 -36.338859558105469
		 6 -38.863754272460938 7 -41.189891815185547 8 -43.095844268798828 9 -44.397232055664063
		 10 -44.925491333007813 11 -44.060211181640625 12 -42.875110626220703 13 -43.199363708496094
		 14 -44.997734069824219 15 -44.440746307373047 16 -37.774124145507813 17 -29.834676742553714
		 18 -23.977359771728516 19 -19.915925979614258 20 -16.628414154052734;
createNode animCurveTA -n "Character1_LeftForeArm_rotateY";
	rename -uid "B08206EC-453E-C4E4-46EC-B4925AC4DF7A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -3.5800232887268066 1 -3.2777848243713379
		 2 -2.9416823387145996 3 -2.4342787265777588 4 -1.6097383499145508 5 -0.46195581555366516
		 6 0.93654513359069835 7 2.4426124095916748 8 3.8492031097412109 9 4.9080815315246582
		 10 5.3624324798583984 11 -0.579445481300354 12 -15.163414001464846 13 -33.04852294921875
		 14 -48.606307983398437 15 -56.406013488769531 16 -54.578208923339844 17 -45.895530700683594
		 18 -32.726119995117188 19 -17.999406814575195 20 -4.6888589859008789;
createNode animCurveTA -n "Character1_LeftForeArm_rotateZ";
	rename -uid "D727321E-4E9C-E49F-B246-B3B20E4B077D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 42.426883697509766 1 44.149314880371094
		 2 45.864894866943359 3 48.172168731689453 4 51.427909851074219 5 55.302631378173828
		 6 59.364814758300788 7 63.204998016357422 8 66.439079284667969 9 68.702255249023437
		 10 69.635490417480469 11 71.041664123535156 12 73.403373718261719 13 75.269317626953125
		 14 75.954872131347656 15 72.340805053710937 16 61.416568756103523 17 48.721214294433594
		 18 38.437347412109375 19 30.884729385375977 20 25.404184341430664;
createNode animCurveTU -n "Character1_LeftForeArm_visibility";
	rename -uid "12057AA3-49C3-4009-785B-74AEA9E8C4BF";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftHand_translateX";
	rename -uid "894EF653-448A-5D3B-DCE6-D69E3B584985";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.29200950264930725;
createNode animCurveTL -n "Character1_LeftHand_translateY";
	rename -uid "96B04C8E-4473-7E44-1783-0B9B293B288B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -14.295157432556152;
createNode animCurveTL -n "Character1_LeftHand_translateZ";
	rename -uid "EC6A9E5F-483D-575C-AE75-37B223E039A2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6319241523742676;
createNode animCurveTU -n "Character1_LeftHand_scaleX";
	rename -uid "238DBEEC-40BD-1C66-7694-B0A2621CFBF5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHand_scaleY";
	rename -uid "8947F28A-4BF1-63F7-9B02-23AA5B1059A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHand_scaleZ";
	rename -uid "612E4FCA-446C-A195-A02A-1BA2AFBF2CA6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHand_rotateX";
	rename -uid "6531725E-4EE0-20E0-5753-B5BD291AB296";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  9 -7.0666610554326326e-005 10 -7.0662274083588272e-005
		 11 -3.5188591480255127 12 -10.770554542541504 13 -17.296182632446289 14 -20.804590225219727
		 15 -21.636213302612305 16 -21.105716705322266 17 -18.885189056396484 18 -14.241150856018066
		 19 -7.462073802947998 20 -7.0681606302969158e-005;
createNode animCurveTA -n "Character1_LeftHand_rotateY";
	rename -uid "42BCE1D2-4EC2-E580-FA2A-37ACDF26DD0A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  9 -2.2598143800678372e-008 10 -2.4824961641911614e-008
		 11 -3.4219715595245361 12 -12.401669502258301 13 -24.301025390625 14 -34.950363159179688
		 15 -39.532562255859375 16 -36.354923248291016 17 -28.365934371948242 18 -18.055583953857422
		 19 -7.9242548942565927 20 1.8197416906673425e-009;
createNode animCurveTA -n "Character1_LeftHand_rotateZ";
	rename -uid "ACB46D08-4BE4-EC80-7C97-37B69EE71B44";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  9 -0.00011945400910917671 10 -0.00011945699952775611
		 11 -1.7654995918273926 12 -5.5679354667663574 13 -9.662928581237793 14 -13.217705726623535
		 15 -14.917356491088867 16 -13.720871925354004 17 -10.989743232727051 18 -7.5881848335266122
		 19 -3.7915122509002686 20 -0.00011945827282033861;
createNode animCurveTU -n "Character1_LeftHand_visibility";
	rename -uid "414CCEB3-43C4-B957-D0CD-7B91EDD45367";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateX";
	rename -uid "E393D3C1-4F0F-2990-4957-EB90A6FC4AB5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1364259719848633;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateY";
	rename -uid "F7C179A6-48F9-1F7F-53CE-B1B9FBFC5B2A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4241994619369507;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateZ";
	rename -uid "E3D975CF-48EE-110E-14BE-47944290048E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.7211661338806152;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleX";
	rename -uid "E24DDC5D-411A-7DA9-F3B7-239FE547716F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleY";
	rename -uid "E56BEBDC-4FA2-D7F9-CF51-339D13141A76";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleZ";
	rename -uid "E254E28B-499A-61D6-212B-1D9AD6A33F2E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateX";
	rename -uid "1F8DEE57-417C-2A61-FA82-D28A7D633025";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 -35.857170104980469 1 -34.609470367431641
		 2 -32.460830688476563 3 -28.371952056884766 4 -20.863876342773438 5 -11.517886161804199
		 6 -3.6692347526550297 7 -0.39304834604263306 8 -0.39304834604263306 14 -0.39304831624031067
		 15 -0.39304831624031067 16 -7.5108056068420401 17 -21.012104034423828 18 -28.371952056884766
		 19 -28.371952056884766;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateY";
	rename -uid "6BA9F8F5-406C-561F-4A42-95960E3C6499";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 58.664314270019524 1 47.571804046630859
		 2 36.438327789306641 3 26.454395294189453 4 16.520473480224609 5 6.3180875778198242
		 6 -1.7080868482589722 7 -4.9669790267944336 8 -4.9669790267944336 14 -4.9669790267944336
		 15 -4.9669790267944336 16 3.2933392524719238 17 18.42218017578125 18 26.454395294189453
		 19 26.454395294189453;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateZ";
	rename -uid "CFA7ED7E-4B6D-C481-1B0D-10B44B88B4D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 -8.1192960739135742 1 -8.5206899642944336
		 2 -8.0394420623779297 3 -6.3497772216796875 4 -2.7581865787506104 5 2.0255725383758545
		 6 6.2831296920776367 7 8.1312093734741211 8 8.1312093734741211 14 8.1312093734741211
		 15 8.1312093734741211 16 4.1936287879943848 17 -2.7815954685211182 18 -6.3497772216796875
		 19 -6.3497772216796875;
createNode animCurveTU -n "Character1_LeftHandThumb1_visibility";
	rename -uid "32D91519-4151-9398-2681-FFA0D1A54DF7";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateX";
	rename -uid "FDB0775D-4C08-A456-7543-C1937D483BF4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.8329257965087891;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateY";
	rename -uid "CA4AF218-4666-418E-F02A-8E86AE7B7F37";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.094426639378070831;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateZ";
	rename -uid "4D2E9583-4233-4F3B-8C0E-199B6AC46486";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5438374280929565;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleX";
	rename -uid "45C5D7B1-495D-3467-7809-FFB8B69E672E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleY";
	rename -uid "6AF1CF38-42A2-CFB8-DB10-68BCCD31048B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleZ";
	rename -uid "5D13A5DB-434C-ABA1-09A6-C9B71D0E8465";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateX";
	rename -uid "7ED15A73-41F5-9E27-4CED-55A696C26C22";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 -7.0229120254516602 1 2.3504726886749268
		 2 7.913447380065918 3 10.355228424072266 4 9.7697000503540039 5 6.1235957145690918
		 6 1.0273935794830322 7 -1.6614576578140259 8 -1.6614576578140259 14 -1.6614577770233154
		 15 -1.6614577770233154 16 3.8484427928924565 17 9.5432920455932617 18 10.355228424072266
		 19 10.355228424072266;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateY";
	rename -uid "93664D2A-4BD2-0376-F879-62AF66103582";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 55.072174072265625 1 43.678169250488281
		 2 31.845926284790043 3 20.610536575317383 4 8.5676431655883789 5 -3.7233502864837651
		 6 -12.803005218505859 7 -16.250497817993164 8 -16.250497817993164 14 -16.250497817993164
		 15 -16.250497817993164 16 -7.5042734146118155 17 10.449928283691406 18 20.610536575317383
		 19 20.610536575317383;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateZ";
	rename -uid "532BCFF3-42D6-368A-3EC4-E2A2CC382EE6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 -27.805004119873047 1 -19.140890121459961
		 2 -13.267426490783691 3 -8.5256719589233398 4 -3.2468466758728027 5 2.9618704319000244
		 6 8.750213623046875 7 11.418617248535156 8 11.418617248535156 14 11.418617248535156
		 15 11.418617248535156 16 5.335052490234375 17 -3.9354412555694576 18 -8.5256719589233398
		 19 -8.5256719589233398;
createNode animCurveTU -n "Character1_LeftHandThumb2_visibility";
	rename -uid "FF645BD7-4BDF-0F19-649F-19A982685B7C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateX";
	rename -uid "6129F0BA-4A2B-0BB3-75FE-16900DE8613F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7747817039489746;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateY";
	rename -uid "2E0D8DE3-4EFD-C9C3-F4DC-8DB3033439C4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3182404041290283;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateZ";
	rename -uid "C0D6178C-4935-A8A3-CA6B-96B54F0670DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.26324462890625;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleX";
	rename -uid "E9F5C402-43A4-3735-E579-E080DCB644F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleY";
	rename -uid "088B7743-44F3-5CD8-DC28-0FB9CFA3080D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleZ";
	rename -uid "7AFA27DE-440D-DEE1-878F-E4AC126FA5CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateX";
	rename -uid "31857561-4345-6465-ED1C-66A1E0C3037B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.6342340220253391e-007;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateY";
	rename -uid "860899D7-4D30-6E92-F97C-DB903FAF80F5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.8655012341878319e-008;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateZ";
	rename -uid "6572F160-41E6-F697-741B-BA9382AB7BD0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.20841130271765e-007;
createNode animCurveTU -n "Character1_LeftHandThumb3_visibility";
	rename -uid "8AA663B2-4F8F-4056-DE42-4387B07DA716";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateX";
	rename -uid "A85515F5-4D80-EB0C-ED49-D3BA053AB7DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.854037284851074;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateY";
	rename -uid "1117F2E6-4E30-7274-6B1E-739039D4B5FD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2616198062896729;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateZ";
	rename -uid "4EC7124A-4CE8-2F97-DE79-53A54597EBC2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.944699764251709;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleX";
	rename -uid "0F3A6580-4270-9A9B-4450-B89116260A49";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleY";
	rename -uid "2BE8A264-492D-7CD9-9E0E-6CB7C1D5DE14";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleZ";
	rename -uid "2F99891C-4BE9-AF67-712C-20A1FC3CEAD3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateX";
	rename -uid "620B4D6D-46BD-1AC0-C08D-30B4A28E14CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 60.010890960693366 1 32.652698516845703
		 2 11.506572723388672 3 2.1236224174499512 4 -1.2244020700454712 5 -2.54726243019104
		 6 -2.9679858684539795 7 -3.0544147491455078 8 -3.0544147491455078 14 -3.0544147491455078
		 15 -3.0544147491455078 16 -2.502366304397583 17 -0.0051168045029044151 18 2.1236224174499512
		 19 2.1236224174499512;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateY";
	rename -uid "8ADB2C40-4E68-7206-B5D2-4EA1A24CB2DF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 33.363674163818359 1 32.680332183837891
		 2 20.407039642333984 3 6.7255492210388184 4 -2.7030599117279053 5 -9.3923063278198242
		 6 -13.288646697998047 7 -14.544971466064451 8 -14.544971466064451 14 -14.544971466064451
		 15 -14.544971466064451 16 -8.9451045989990234 17 1.3453395366668701 18 6.7255492210388184
		 19 6.7255492210388184;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateZ";
	rename -uid "8BEA585D-433A-67FA-1321-E5A860BDF1D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 86.987724304199219 1 51.010475158691406
		 2 20.056804656982422 3 1.4951498508453369 4 -8.77166748046875 5 -15.563195228576662
		 6 -19.428586959838867 7 -20.654909133911133 8 -20.654909133911133 14 -20.654909133911133
		 15 -20.654909133911133 16 -14.914940834045408 17 -4.3437838554382324 18 1.4951497316360474
		 19 1.4951497316360474;
createNode animCurveTU -n "Character1_LeftHandIndex1_visibility";
	rename -uid "D8F8A88D-4861-1F39-830B-07AB50481638";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateX";
	rename -uid "B7AC4AD7-499F-1B56-A3B6-978D734FD546";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.1721835136413574;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateY";
	rename -uid "E381B5A8-4ADC-90EA-9588-E6BC1DF6BD06";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.9652466773986816;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateZ";
	rename -uid "31BA6960-4DEF-C7D1-8AF0-378A5106ECED";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.8871781826019287;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleX";
	rename -uid "9FFF76A2-47FD-C1FF-C141-B28F7F15A905";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleY";
	rename -uid "1FB112AD-4E62-E5B0-4772-C4945E04234C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleZ";
	rename -uid "3ACF0C6E-49ED-1EAD-798B-10B8FFA95972";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateX";
	rename -uid "3AAB01BE-4C6F-6372-72C3-EE829DA4596A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 7.7885360717773429 1 10.885133743286133
		 2 10.086164474487305 3 7.2087359428405762 4 4.136573314666748 5 1.1446115970611572
		 6 -1.0694171190261841 7 -1.9113292694091795 8 -1.9113292694091795 14 -1.9113292694091795
		 15 -1.9113292694091795 16 0.87730944156646729 17 5.2956867218017578 18 7.2087359428405762
		 19 7.2087359428405762;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateY";
	rename -uid "B799DDCD-4E7D-FA25-C243-2B8206CF4A22";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 -33.620094299316406 1 -24.502443313598633
		 2 -15.022773742675783 3 -8.152409553527832 4 -3.8641824722290039 5 -0.91430908441543579
		 6 0.7669529914855957 7 1.3168554306030273 8 1.3168554306030273 14 1.3168554306030273
		 15 1.3168554306030273 16 -0.69155675172805786 17 -5.2887845039367676 18 -8.152409553527832
		 19 -8.152409553527832;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateZ";
	rename -uid "731696B3-4656-DB6E-7076-A1A58D81FAD7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 80.491020202636719 1 58.50575256347657
		 2 38.300777435302734 3 22.938976287841797 4 11.984194755554199 5 3.1218056678771973
		 6 -2.8196754455566406 7 -4.9837503433227539 8 -4.9837503433227539 14 -4.9837503433227539
		 15 -4.9837503433227539 16 2.3820545673370361 17 15.812365531921387 18 22.938976287841797
		 19 22.938976287841797;
createNode animCurveTU -n "Character1_LeftHandIndex2_visibility";
	rename -uid "A44C933A-44E2-7FA0-8C6F-A5A6EF2482A6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateX";
	rename -uid "B806DC22-41F4-C2EA-166A-918B1CCB34E9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.5761528015136719;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateY";
	rename -uid "90D2C6D9-4690-C55D-97C6-63956BCE38A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8397388458251953;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateZ";
	rename -uid "B05354CB-4322-6891-2F59-76B4DFA73113";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3069263696670532;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleX";
	rename -uid "38E48A5F-470A-83DD-1249-75BE5A9465F1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleY";
	rename -uid "7C54D133-43C5-6D23-7992-E79104C29FFF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleZ";
	rename -uid "54C8F76E-4B53-AB77-0EB5-32858B42D838";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateX";
	rename -uid "538D2266-460F-AD76-E748-DE998131E6CD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.966858403397055e-008;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateY";
	rename -uid "21AC426F-4D8C-C7F6-16C6-D2852AE6B42D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6960386751161423e-008;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateZ";
	rename -uid "1575C999-4EE4-DE4C-2FCA-358064C2AA46";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5025929062394425e-007;
createNode animCurveTU -n "Character1_LeftHandIndex3_visibility";
	rename -uid "C5B02480-4536-7B09-B7E9-00B5C9EE20DA";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftHandRing1_translateX";
	rename -uid "CB605A12-4CFF-CF34-F5A4-A39A302FB5F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4602031707763672;
createNode animCurveTL -n "Character1_LeftHandRing1_translateY";
	rename -uid "B787BDF2-45EB-9A03-4BFF-2A9734625453";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.5015654563903809;
createNode animCurveTL -n "Character1_LeftHandRing1_translateZ";
	rename -uid "7A8FF3CE-4C29-94E5-577E-6198619BC842";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.6178884506225586;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleX";
	rename -uid "A841296C-43F0-E438-94B0-EA9C077D45F4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleY";
	rename -uid "3E537B77-427F-716B-CEB2-B291553FE075";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleZ";
	rename -uid "8929AE1E-4A57-847D-9A9A-8BB4FD01A609";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateX";
	rename -uid "0A70020A-4B7A-02CA-10DF-5F89A5B18E32";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 32.34600830078125 1 17.544410705566406
		 2 7.4091386795043954 3 3.1731135845184326 4 1.9976946115493774 5 2.1391000747680664
		 6 2.7758691310882568 7 3.1195135116577148 8 3.1195135116577148 14 3.1195135116577148
		 15 3.1195135116577148 16 2.2996773719787598 17 2.3294179439544678 18 3.1731135845184326
		 19 3.1731135845184326;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateY";
	rename -uid "57B6E521-42FC-FF08-9A84-AB93ABE29990";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 35.989974975585938 1 30.4993896484375
		 2 18.865394592285156 3 7.5687365531921396 4 -0.31101945042610168 5 -6.1465091705322266
		 6 -9.6822433471679687 7 -10.855649948120117 8 -10.855649948120117 14 -10.855649948120117
		 15 -10.855649948120117 16 -5.9289169311523438 17 3.0024204254150391 18 7.5687365531921396
		 19 7.5687365531921396;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateZ";
	rename -uid "DBB847BA-4788-200E-E1F3-96BE9A177DD6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 90.74188232421875 1 62.231811523437507
		 2 38.430835723876953 3 22.875423431396484 4 12.703634262084961 5 4.7286562919616699
		 6 -0.58629602193832397 7 -2.5283217430114746 8 -2.5283217430114746 14 -2.5283217430114746
		 15 -2.5283217430114746 16 3.9989919662475581 17 16.148639678955078 18 22.875423431396484
		 19 22.875423431396484;
createNode animCurveTU -n "Character1_LeftHandRing1_visibility";
	rename -uid "2B7B62BC-4A74-C699-7486-C495C5471C7B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftHandRing2_translateX";
	rename -uid "677061DE-4DB1-5768-F103-59836137851B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.9237353801727295;
createNode animCurveTL -n "Character1_LeftHandRing2_translateY";
	rename -uid "889A81B5-4920-4E6B-3D08-D9A042D3A437";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.0094923973083496;
createNode animCurveTL -n "Character1_LeftHandRing2_translateZ";
	rename -uid "82BE0FCE-4454-CD98-A6D2-32881ABCF021";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8221585750579834;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleX";
	rename -uid "CB5F99A4-46FE-85A2-58B9-DAB70F511526";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleY";
	rename -uid "3F942138-4F0C-6E42-4A62-1C84D417FF4C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleZ";
	rename -uid "F7E4E344-40B0-5270-3349-BCB0B1E171B7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateX";
	rename -uid "73CA0F52-49A1-C427-7864-36924A62356B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 42.815162658691406 1 33.461963653564453
		 2 20.898191452026367 3 9.7664651870727539 4 2.3711004257202148 5 -2.5267918109893799
		 6 -2.2199053764343262 7 -2.119004487991333 8 -2.119004487991333 14 -2.119004487991333
		 15 -2.119004487991333 16 -2.4644198417663574 17 5.5519270896911621 18 9.7664651870727539
		 19 9.7664651870727539;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateY";
	rename -uid "05DCC495-426D-D896-33D4-28BC7A77DB1B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 -31.412193298339847 1 -17.561967849731445
		 2 -6.9108786582946777 3 -1.8647102117538452 4 -0.25125408172607422 5 0.14838522672653198
		 6 0.3286857008934021 7 0.37931835651397705 8 0.37931835651397705 14 0.37931835651397705
		 15 0.37931835651397705 16 0.12812095880508423 17 -0.78851395845413208 18 -1.8647100925445559
		 19 -1.8647100925445559;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateZ";
	rename -uid "38E3EDDC-42B8-967F-09FB-E39B5FCFC281";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  0 62.220687866210945 1 45.313316345214844
		 2 27.178152084350586 3 12.489145278930664 4 3.0155332088470459 5 -3.2694728374481201
		 6 -3.56315016746521 7 -3.6594281196594238 8 -3.6594281196594238 14 -3.6594281196594238
		 15 -3.6594281196594238 16 -3.1291775703430176 17 7.0742063522338867 18 12.489145278930664
		 19 12.489145278930664;
createNode animCurveTU -n "Character1_LeftHandRing2_visibility";
	rename -uid "CD01135E-4A3E-93C1-F792-7491D794E2E0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_LeftHandRing3_translateX";
	rename -uid "EFA76D2D-4E12-F0A6-6EE0-DAAA5041C480";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6098947525024414;
createNode animCurveTL -n "Character1_LeftHandRing3_translateY";
	rename -uid "C8DB7408-4D7E-70A9-EC6C-F5A3F2E84601";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2952473163604736;
createNode animCurveTL -n "Character1_LeftHandRing3_translateZ";
	rename -uid "2EF214F5-41DD-FA58-8492-2DB03B2846A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3554067611694336;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleX";
	rename -uid "45ECE3DC-4A94-31AF-911E-579739D2C94E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleY";
	rename -uid "E175E4A7-42B0-3E48-D445-08A42C4C8C21";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleZ";
	rename -uid "042E7691-4FB7-5490-A60A-53908EF08F8C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateX";
	rename -uid "C389A4FB-4A84-5CB2-37A8-59A150E4D425";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3849707986300928e-007;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateY";
	rename -uid "58057743-43CE-2C32-10F6-1382B65FFF42";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4752912136373197e-007;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateZ";
	rename -uid "CD7481D9-4055-CD90-B6D2-5DADD2172055";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.6490627647035581e-007;
createNode animCurveTU -n "Character1_LeftHandRing3_visibility";
	rename -uid "DC40B1BB-4095-69F8-0FF0-D8959F3BE4DD";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightShoulder_translateX";
	rename -uid "C3165A6E-4501-99BD-6870-819B4205B558";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 14.332768440246582;
createNode animCurveTL -n "Character1_RightShoulder_translateY";
	rename -uid "A9301298-4EEA-5134-6FBF-42BD32D1593E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5798094272613525;
createNode animCurveTL -n "Character1_RightShoulder_translateZ";
	rename -uid "C374F7A1-4D21-CA99-5427-92996F995945";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1086856126785278;
createNode animCurveTU -n "Character1_RightShoulder_scaleX";
	rename -uid "92E06B37-4923-191C-CC7F-33A55B69847C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightShoulder_scaleY";
	rename -uid "597CDFF4-4063-3469-F211-B888D7452F98";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightShoulder_scaleZ";
	rename -uid "44160AD4-46B2-B2FD-DDCA-E3B2ACB3E297";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightShoulder_rotateX";
	rename -uid "1B970B7E-463A-A176-2913-BDAD7860A392";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -1.4918075799942017 1 9.5109405517578125
		 2 5.6809415817260742 3 -3.8389418125152592 4 -8.582179069519043 5 -10.522036552429199
		 6 -11.08106803894043 7 -12.016488075256348 8 -12.098737716674805 9 -10.88172435760498
		 10 -7.3806419372558594 11 -1.3416715860366821 12 2.4812297821044922 13 3.4322121143341064
		 14 1.8348327875137331 15 -2.5498340129852295 16 -3.7246241569519043 17 -1.8663595914840698
		 18 0.33364218473434448 19 0.055012345314025879 20 -1.920072078704834;
createNode animCurveTA -n "Character1_RightShoulder_rotateY";
	rename -uid "F137C95B-4CD7-344C-7B55-CF9607E75218";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -48.272300720214844 1 -20.077911376953125
		 2 5.6352758407592773 3 21.016218185424805 4 25.155704498291016 5 26.318910598754883
		 6 26.287136077880859 7 26.616155624389648 8 26.267255783081055 9 24.628961563110352
		 10 19.670848846435547 11 7.9850559234619141 12 -5.9288697242736816 13 -19.553237915039063
		 14 -32.80743408203125 15 -40.243663787841797 16 -23.764619827270508 17 -3.5807027816772461
		 18 2.5966475009918213 19 -6.7769684791564941 20 -22.21434211730957;
createNode animCurveTA -n "Character1_RightShoulder_rotateZ";
	rename -uid "B1B3CD41-4321-69D9-79D9-75A7F490CE8C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -6.5164856910705566 1 -20.919118881225586
		 2 -29.11085319519043 3 -36.397377014160156 4 -38.747482299804688 5 -38.840740203857422
		 6 -37.858627319335938 7 -37.196193695068359 8 -35.98193359375 9 -33.84881591796875
		 10 -30.738433837890625 11 -26.642169952392578 12 -23.222673416137695 13 -19.723934173583984
		 14 -15.483370780944824 15 -8.8998937606811523 16 -1.046366810798645 17 8.1484060287475586
		 18 13.65496826171875 19 12.608970642089844 20 10.851101875305176;
createNode animCurveTU -n "Character1_RightShoulder_visibility";
	rename -uid "7B41E838-428B-A065-6AC0-D196462DF76C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightArm_translateX";
	rename -uid "942470C5-4E46-CC97-1760-9BAE80988D28";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.8354010581970215;
createNode animCurveTL -n "Character1_RightArm_translateY";
	rename -uid "E50E86DA-4A03-1665-4E69-428440E99C97";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1704782247543335;
createNode animCurveTL -n "Character1_RightArm_translateZ";
	rename -uid "03171D68-4821-78B3-3B04-0ABDA5931A8B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.3259291648864746;
createNode animCurveTU -n "Character1_RightArm_scaleX";
	rename -uid "A8D88D40-465A-54C2-9F4B-5484ACC4EE19";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightArm_scaleY";
	rename -uid "C7E31607-4813-0969-EF2E-B2937ACA5F56";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightArm_scaleZ";
	rename -uid "9CA5894F-4587-E3B7-2C5E-0AA786465023";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightArm_rotateX";
	rename -uid "0773A792-458C-16D4-925A-F0BA3B3A788A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -22.79180908203125 1 12.702665328979492
		 2 -3.2111690044403076 3 -23.463462829589844 4 -30.033706665039059 5 -33.356365203857422
		 6 -35.141887664794922 7 -37.614982604980469 8 -41.208999633789063 9 -44.515155792236328
		 10 -48.34228515625 11 -50.797649383544922 12 -52.839603424072266 13 -54.647144317626953
		 14 -8.8727140426635742 15 -61.830986022949219 16 -141.72891235351562 17 -143.53121948242187
		 18 -122.82998657226561 19 -153.27571105957031 20 -202.2598876953125;
createNode animCurveTA -n "Character1_RightArm_rotateY";
	rename -uid "01953159-4722-2FE3-F8B4-40BDEC73084B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 38.565711975097656 1 55.877086639404297
		 2 43.738922119140625 3 41.443046569824219 4 45.00677490234375 5 49.484989166259766
		 6 54.262462615966797 7 59.746452331542969 8 63.624065399169922 9 65.867462158203125
		 10 67.512359619140625 11 68.533477783203125 12 70.232505798339844 13 73.055740356445313
		 14 75.310745239257813 15 82.180572509765625 16 101.25828552246094 17 111.45046997070312
		 18 113.79853057861328 19 108.41323089599609 20 113.39449310302734;
createNode animCurveTA -n "Character1_RightArm_rotateZ";
	rename -uid "2C7902AF-4512-190E-0F13-D48ED6A53C88";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -19.728612899780273 1 -22.905418395996094
		 2 -11.043552398681641 3 3.6777403354644775 4 11.594110488891602 5 17.534835815429688
		 6 22.191562652587891 7 25.202981948852539 8 24.505382537841797 9 22.051506042480469
		 10 17.483705520629883 11 10.463800430297852 12 3.7122161388397217 13 -0.61504310369491577
		 14 42.343730926513672 15 -4.1872305870056152 16 -90.614875793457031 17 -111.85850524902344
		 18 -108.75521087646484 19 -148.98265075683594 20 -212.5684814453125;
createNode animCurveTU -n "Character1_RightArm_visibility";
	rename -uid "0A117D22-4A44-0A4A-7C12-84AD85646702";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightForeArm_translateX";
	rename -uid "06B5BB16-46AA-C2B7-5CE7-CFA1E0115B55";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.753132820129395;
createNode animCurveTL -n "Character1_RightForeArm_translateY";
	rename -uid "0C3C8A19-43F8-61FE-BE3E-2E971C58A1F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.862128257751465;
createNode animCurveTL -n "Character1_RightForeArm_translateZ";
	rename -uid "3186BF30-4D4A-D60A-62E7-E78A0B3391B7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.671875953674316;
createNode animCurveTU -n "Character1_RightForeArm_scaleX";
	rename -uid "9F7B22FE-4105-70B6-443B-4A9C8AB32B6A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightForeArm_scaleY";
	rename -uid "D6C9D143-4F04-4ED9-9C37-6AA1162C2174";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightForeArm_scaleZ";
	rename -uid "30CBC8E5-463D-2665-7DCF-63A140E39E11";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightForeArm_rotateX";
	rename -uid "93DA4A6B-43D7-6D84-21BF-4A8834379C44";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 43.930099487304688 1 8.398066520690918
		 2 30.160299301147461 3 56.931858062744141 4 59.457065582275391 5 55.786552429199219
		 6 49.453407287597656 7 43.154155731201172 8 30.609567642211911 9 17.199480056762695
		 10 11.142806053161621 11 13.707165718078613 12 18.266986846923828 13 26.246633529663086
		 14 44.057792663574219 15 55.985725402832031 16 28.891065597534183 17 22.473941802978516
		 18 23.630964279174805 19 26.573677062988281 20 29.266485214233398;
createNode animCurveTA -n "Character1_RightForeArm_rotateY";
	rename -uid "134A85C3-4063-D8D0-F604-9B99A90CB65E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 51.837024688720703 1 32.490940093994141
		 2 43.614055633544922 3 55.209434509277344 4 50.562854766845703 5 41.955478668212891
		 6 33.0439453125 7 26.402801513671875 8 22.953868865966797 9 15.123952865600584 10 10.40211009979248
		 11 12.368496894836426 12 15.136984825134276 13 16.465150833129883 14 14.112112045288086
		 15 17.839382171630859 16 37.366748809814453 17 41.300956726074219 18 41.986953735351562
		 19 41.935710906982422 20 42.191154479980469;
createNode animCurveTA -n "Character1_RightForeArm_rotateZ";
	rename -uid "EEED950F-4E4E-3C73-F5A5-109B9DE10EA7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 13.605631828308105 1 37.792194366455078
		 2 26.446144104003906 3 21.160257339477539 4 24.292219161987305 5 30.029354095458984
		 6 37.858997344970703 7 44.865737915039063 8 40.2144775390625 9 31.400558471679688
		 10 29.05973052978516 11 31.680259704589844 12 35.303905487060547 13 35.610233306884766
		 14 24.140739440917969 15 7.0867371559143066 16 3.3205890655517578 17 -2.2578065395355225
		 18 -1.7898738384246826 19 9.6615409851074219 20 24.203212738037109;
createNode animCurveTU -n "Character1_RightForeArm_visibility";
	rename -uid "78187BAF-45E5-5A6B-6234-1F8E7B0A199A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightHand_translateX";
	rename -uid "95DA3093-49C0-98E6-489B-4BBA108D4B2E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7167695760726929;
createNode animCurveTL -n "Character1_RightHand_translateY";
	rename -uid "6EE2333D-44B1-601C-62B4-199051CA7569";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.314067840576172;
createNode animCurveTL -n "Character1_RightHand_translateZ";
	rename -uid "60535000-4C14-24BC-7CC9-9ABC480478E1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4444055557250977;
createNode animCurveTU -n "Character1_RightHand_scaleX";
	rename -uid "EA426013-40B6-B5C3-DD0E-0FA05EE94963";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightHand_scaleY";
	rename -uid "C837A17A-4B74-C6B5-688D-B09F81889F11";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightHand_scaleZ";
	rename -uid "48C1B703-4AE3-7EB5-5E19-438D594E8ACC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightHand_rotateX";
	rename -uid "A6B15CAC-424D-1B6F-7293-EBAB180BD074";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 6.1326141357421875 1 23.883523941040039
		 2 14.809505462646483 3 6.6437764167785645 4 8.5438995361328125 5 13.260794639587402
		 6 18.343471527099609 7 20.928281784057617 8 7.5251870155334464 9 -20.175712585449219
		 10 -39.68341064453125 11 -43.195091247558594 12 -43.447193145751953 13 -43.362964630126953
		 14 -47.046276092529297 15 -44.21026611328125 16 -5.7926387786865234 17 -2.7679150104522705
		 18 -8.9561948776245117 19 -18.42683219909668 20 -27.040277481079102;
createNode animCurveTA -n "Character1_RightHand_rotateY";
	rename -uid "4993123F-4E3C-9941-9608-25A72EAABB40";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 1.8330816030502319 1 6.9417657852172852
		 2 4.2510294914245605 3 0.093263104557991028 4 0.88370019197463989 5 3.0448739528656006
		 6 5.0983734130859375 7 6.0274791717529297 8 1.5273298025131226 9 -8.7954006195068359
		 10 -15.227413177490234 11 -15.201795578002928 12 -13.969355583190918 13 -12.739640235900879
		 14 -13.003776550292969 15 -11.061307907104492 16 0.73530149459838867 17 2.8109836578369141
		 18 4.8123273849487305 19 1.6398309469223022 20 -7.3374385833740234;
createNode animCurveTA -n "Character1_RightHand_rotateZ";
	rename -uid "BF6A54FF-4B00-BE8E-2107-87BB0CFA87E2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 0.32900974154472351 1 -3.37593674659729
		 2 -9.3388595581054687 3 -13.314023017883301 4 -13.341037750244141 5 -12.107963562011719
		 6 -10.424561500549316 7 -9.1701345443725586 8 -9.7514333724975586 9 -8.2606544494628906
		 10 -3.4354007244110107 11 0.10917064547538757 12 3.2776503562927246 13 5.6080350875854492
		 14 6.2865982055664062 15 5.52252197265625 16 5.5244808197021484 17 18.940317153930664
		 18 27.471324920654297 19 17.25120735168457 20 2.4331135749816895;
createNode animCurveTU -n "Character1_RightHand_visibility";
	rename -uid "CFA3F094-4290-92DD-5B76-CDB223DE0D48";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightHandThumb1_translateX";
	rename -uid "7743D7C8-4D33-9D51-9153-9B9308873EC8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5741705894470215;
createNode animCurveTL -n "Character1_RightHandThumb1_translateY";
	rename -uid "560DEB39-4E4E-A51F-6DE8-A893EFFD9930";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4327812194824219;
createNode animCurveTL -n "Character1_RightHandThumb1_translateZ";
	rename -uid "E2FD9CF0-4B55-6FAC-ADBB-6288F7B1F9E3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8196568489074707;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleX";
	rename -uid "1D196AFB-4CA0-A1FE-4C3F-F2910BC93C42";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleY";
	rename -uid "BDCA7DF7-4550-C0FD-F3D5-71BB204BC124";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleZ";
	rename -uid "4D7CD903-47D7-FE76-8F49-7A8FB41B2CC3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateX";
	rename -uid "2C164319-42D4-B0C6-7753-628F7294EAB7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  0 57.314090728759766 1 53.592891693115234
		 2 47.663345336914063 3 38.973472595214844 4 26.647058486938477 5 12.869129180908203
		 6 1.6843122243881226 7 -3.0263726711273193 8 -3.0263726711273193 9 -3.0263726711273193
		 10 -3.0263726711273193 11 1.2968231439590454 12 11.546096801757813 13 23.905872344970703
		 14 34.488811492919922 15 38.973472595214844 16 38.973472595214844;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateY";
	rename -uid "EA2604BD-4431-C8D4-A012-ED92E82691A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  0 13.127543449401855 1 20.28571891784668
		 2 25.974287033081055 3 27.536184310913086 4 23.626007080078125 5 16.022493362426758
		 6 8.7553806304931641 7 5.6640901565551758 8 5.6640901565551758 9 5.6640901565551758
		 10 5.6640901565551758 11 8.3761205673217773 12 14.645556449890137 13 21.366989135742188
		 14 25.966058731079102 15 27.536184310913086 16 27.536184310913086;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateZ";
	rename -uid "12DBC430-477E-FE38-719C-06BC1CE5438E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  0 13.751087188720703 1 4.1280593872070313
		 2 -6.6980190277099609 3 -17.683155059814453 4 -28.206401824951172 5 -36.059646606445313
		 6 -39.541675567626953 7 -40.240188598632813 8 -40.240188598632813 9 -40.240188598632813
		 10 -40.240188598632813 11 -39.227596282958984 12 -35.566932678222656 13 -28.860872268676758
		 14 -21.323688507080078 15 -17.683155059814453 16 -17.683155059814453;
createNode animCurveTU -n "Character1_RightHandThumb1_visibility";
	rename -uid "15EB9FA1-4CED-F4D4-22E5-67B98512D200";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightHandThumb2_translateX";
	rename -uid "176F9D44-4E76-05E1-3125-92915046726D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7422792911529541;
createNode animCurveTL -n "Character1_RightHandThumb2_translateY";
	rename -uid "25F317DC-4F3E-C208-7E3A-5DB650F5CBED";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5767025947570801;
createNode animCurveTL -n "Character1_RightHandThumb2_translateZ";
	rename -uid "667BB0BE-4DFF-FEA6-7FDD-17AA9A420734";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0428388118743896;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleX";
	rename -uid "7CF6DF91-4984-086A-D414-688ED288FBD6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleY";
	rename -uid "CF54A259-475C-556F-2F98-9E874B99E974";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleZ";
	rename -uid "3900A10F-4EDA-2A62-6960-E5A79115DAAF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateX";
	rename -uid "A6EEF999-4B0F-4DBA-1347-B2A79878AD5B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  0 13.273929595947266 1 9.6930303573608398
		 2 6.1259140968322754 3 3.8401811122894287 4 3.32318115234375 5 3.8335850238800049
		 6 4.6823234558105469 7 5.1234993934631348 8 5.1234993934631348 9 5.1234993934631348
		 10 5.1234993934631348 11 4.9151415824890137 12 4.4925341606140137 13 4.1173944473266602
		 14 3.9039137363433833 15 3.8401813507080074 16 3.8401813507080074;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateY";
	rename -uid "895937A6-4C08-F469-D392-9CB1D142329A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  0 -25.162002563476563 1 -14.507267951965332
		 2 -3.7693455219268794 3 4.2117342948913574 4 8.9427661895751953 5 11.96063232421875
		 6 13.518528938293457 7 13.956454277038574 8 13.956454277038574 9 13.956454277038574
		 10 13.956454277038574 11 12.971592903137207 12 10.592875480651855 13 7.704573154449462
		 14 5.249544620513916 15 4.2117342948913574 16 4.2117342948913574;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateZ";
	rename -uid "CB04DEEB-4187-C3E8-765A-E38410822E82";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  0 -0.82732129096984863 1 -0.10752705484628677
		 2 1.0172450542449951 3 2.4896590709686279 4 4.4627175331115723 5 6.7663254737854004
		 6 8.6724357604980469 7 9.4561424255371094 8 9.4561424255371094 9 9.4561424255371094
		 10 9.4561424255371094 11 8.6566791534423828 12 6.8249154090881348 13 4.7679495811462402
		 14 3.1448113918304443 15 2.4896585941314697 16 2.4896588325500488;
createNode animCurveTU -n "Character1_RightHandThumb2_visibility";
	rename -uid "351EA822-4157-79EA-C565-8EA90FE5878A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightHandThumb3_translateX";
	rename -uid "AC775EC6-4426-7510-2810-7F92661169A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.588839054107666;
createNode animCurveTL -n "Character1_RightHandThumb3_translateY";
	rename -uid "8FA0068D-4B70-1A07-5CC1-AC81C2486020";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.13479287922382355;
createNode animCurveTL -n "Character1_RightHandThumb3_translateZ";
	rename -uid "8A8A0FB4-48C1-8983-5669-B898F33D7C4E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.4058942794799805;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleX";
	rename -uid "4820C70D-4E38-482E-2DD6-BA8A85AC9724";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleY";
	rename -uid "BAD020DA-4B71-E524-3D2B-6B865A7A3D85";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleZ";
	rename -uid "E08FCFBB-4C0B-6350-1698-05B27C080FCB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateX";
	rename -uid "E1112A79-461D-88BF-8268-04B900390F39";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.4193615761068941e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateY";
	rename -uid "C92D7D1F-4FCC-8F26-6E62-EBA3C654FA87";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.3720630073003122e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateZ";
	rename -uid "ACB9E802-43C3-D2FF-486C-AC8EB1B2C43A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1052615889184381e-007;
createNode animCurveTU -n "Character1_RightHandThumb3_visibility";
	rename -uid "7FEEC2F0-4009-25B6-2983-0398F38DCCED";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightHandIndex1_translateX";
	rename -uid "B5CD8979-4CFA-1248-D69E-7EA5F1FBD561";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.25625163316726685;
createNode animCurveTL -n "Character1_RightHandIndex1_translateY";
	rename -uid "F7F458A4-4230-C96C-52D4-CDA838C429F9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.892666816711426;
createNode animCurveTL -n "Character1_RightHandIndex1_translateZ";
	rename -uid "EB48CF9C-4D6E-4ACB-0CF9-348A66B1D5E6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.533165454864502;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleX";
	rename -uid "4C4FBB5B-4C18-40AF-AFF3-BEB6BD9B54F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleY";
	rename -uid "DDB19AC6-43B9-054D-9B05-1AB78D43989F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleZ";
	rename -uid "09122BF3-44AE-D142-173F-5F9ACE30F815";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateX";
	rename -uid "C9D3D742-4EAB-BD57-6BD8-3CA5C54ADD49";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -1.2556071281433105 1 -2.2111489772796631
		 2 -2.0552585124969482 3 -1.1589168310165405 4 -0.082279019057750702 5 1.0526323318481445
		 6 1.9375414848327634 7 2.2854771614074707 8 2.2854771614074707 9 2.2854771614074707
		 10 2.2854771614074707 11 2.0547342300415039 12 1.4438281059265137 13 0.59761792421340942
		 14 -0.32194006443023682 15 -1.158916711807251 16 -2.2033607959747314 17 -2.1793737411499023
		 18 -2.2797033786773682 19 -2.030423641204834 20 -1.158916711807251;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateY";
	rename -uid "D121E2C8-4260-EB20-6D82-3E85BBC49071";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 10.129959106445312 1 7.1241464614868164
		 2 3.9772212505340576 3 1.6353100538253784 4 0.093574322760105133 5 -1.0034418106079102
		 6 -1.6345446109771729 7 -1.8412685394287109 8 -1.8412685394287109 9 -1.8412685394287109
		 10 -1.8412685394287109 11 -1.7065646648406982 12 -1.3025429248809814 13 -0.60926800966262817
		 14 0.3819197416305542 15 1.6353100538253784 16 4.7908267974853516 17 7.3484797477722168
		 18 6.2594308853149414 19 3.8721103668212891 20 1.6353100538253784;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateZ";
	rename -uid "F075CD3B-4CDD-1105-352D-8D90606D69D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 69.348670959472656 1 48.996139526367188
		 2 28.80207633972168 3 12.797025680541992 4 0.794819176197052 5 -9.2968864440917969
		 6 -16.254104614257813 7 -18.845792770385742 8 -18.845792770385742 9 -18.845792770385742
		 10 -18.845792770385742 11 -17.13475227355957 12 -12.4461669921875 13 -5.4495878219604492
		 14 3.1858475208282471 15 12.797025680541992 16 34.067722320556641 17 50.445323944091797
		 18 43.452560424804688 19 28.114862442016602 20 12.797025680541992;
createNode animCurveTU -n "Character1_RightHandIndex1_visibility";
	rename -uid "FC13D9CF-4132-6392-B797-B8B17B8C6CD2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightHandIndex2_translateX";
	rename -uid "AD613F5C-4E61-AFC2-1233-DDB3A568398E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.3959341049194336;
createNode animCurveTL -n "Character1_RightHandIndex2_translateY";
	rename -uid "2D6EDF94-4E2D-A533-EF27-448F6632F2D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8172028064727783;
createNode animCurveTL -n "Character1_RightHandIndex2_translateZ";
	rename -uid "A36FD16D-4B02-8781-F91A-C8A638E969A7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.52152138948440552;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleX";
	rename -uid "B0F712B3-42D3-E974-9321-5289F71AE380";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleY";
	rename -uid "DE8FA509-44DA-5577-CAC8-3C9DC5443DA4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleZ";
	rename -uid "F9F6E8A3-407D-7A03-774E-1B8C1CC005C3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateX";
	rename -uid "4DD4C07D-4D93-3385-8164-51ACD505F439";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 15.230603218078615 1 23.981679916381836
		 2 18.899560928344727 3 11.788411140441895 4 5.9021120071411133 5 0.9788019061088562
		 6 -2.2937281131744385 7 -3.4437625408172607 8 -3.4437625408172607 9 -3.4437625408172607
		 10 -3.4437625408172607 11 -2.4669618606567383 12 0.11672545969486237 13 3.7228991985321045
		 14 7.7748064994812021 15 11.788410186767578 16 18.891302108764648 17 22.586557388305664
		 18 21.220945358276367 19 17.150121688842773 20 11.788410186767578;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateY";
	rename -uid "DD3A8EB8-488C-1A8A-8896-02A278BAD6D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -73.199981689453125 1 -51.930412292480469
		 2 -30.662485122680668 3 -15.640761375427246 4 -6.8138136863708496 5 -1.0141253471374512
		 6 2.217109203338623 7 3.2495944499969482 8 3.2495944499969482 9 3.2495944499969482
		 10 3.2495942115783691 11 2.375906229019165 12 -0.11872772127389908 13 -4.0942506790161133
		 14 -9.3683538436889648 15 -15.640761375427246 16 -30.640556335449222 17 -42.855976104736328
		 18 -37.593971252441406 19 -26.318748474121094 20 -15.640760421752931;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateZ";
	rename -uid "99E50BEC-43AE-A4DB-AEFA-8495899433C4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 68.75714111328125 1 42.040767669677734
		 2 26.643659591674805 3 15.306336402893066 4 7.3867802619934082 5 1.1991125345230103
		 6 -2.7785530090332031 7 -4.1569900512695313 8 -4.1569900512695313 9 -4.1569900512695313
		 10 -4.1569900512695313 11 -2.9867775440216064 12 0.1425480991601944 13 4.6119165420532227
		 14 9.8286828994750977 15 15.306336402893066 16 26.628116607666016 17 35.230831146240234
		 18 31.512687683105472 19 23.527721405029297 20 15.306336402893066;
createNode animCurveTU -n "Character1_RightHandIndex2_visibility";
	rename -uid "120F719B-4F96-0374-4963-99B600B62F90";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightHandIndex3_translateX";
	rename -uid "68F7B223-4C2C-A01A-8CFC-23B9C07AF1E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6282989978790283;
createNode animCurveTL -n "Character1_RightHandIndex3_translateY";
	rename -uid "61D41905-40B7-ED03-5EDC-1CA4DC40EB6C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.2120883464813232;
createNode animCurveTL -n "Character1_RightHandIndex3_translateZ";
	rename -uid "6ADE7F24-4BE8-63D6-4EBF-E09538AB4F33";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7757487297058105;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleX";
	rename -uid "FD4223CE-49D3-1E3F-5E52-25A56B3C8F09";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleY";
	rename -uid "0D7F039B-467E-243C-D3F2-0A97120687CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleZ";
	rename -uid "0269978A-4BA7-613D-6631-41B893432A6A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateX";
	rename -uid "92A078F6-4CD4-6DE1-FECF-CC8E85C14F00";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.09851228558e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateY";
	rename -uid "9C113164-4E93-EAD9-808C-9485AE26C665";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.6110765816156345e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateZ";
	rename -uid "979A1C06-4166-25FE-2327-F89849FA4860";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2862921039413777e-007;
createNode animCurveTU -n "Character1_RightHandIndex3_visibility";
	rename -uid "991CD083-4EFB-68A1-1759-D6A9A6669354";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightHandRing1_translateX";
	rename -uid "9533B598-430B-E71D-5E26-BD9B88C4CAA1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8543868064880371;
createNode animCurveTL -n "Character1_RightHandRing1_translateY";
	rename -uid "7A7B9232-40D5-51BD-FDC4-689AD39C63DD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2156095504760742;
createNode animCurveTL -n "Character1_RightHandRing1_translateZ";
	rename -uid "D0181777-4B72-2355-8D5F-AFB50FD83019";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2236912250518799;
createNode animCurveTU -n "Character1_RightHandRing1_scaleX";
	rename -uid "59746B57-4BD0-1908-EDA7-66AFD7935E96";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandRing1_scaleY";
	rename -uid "A0608B7D-4635-F2A0-27BC-F18FBEEE6A60";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing1_scaleZ";
	rename -uid "72B2C809-401A-E5DA-7E0C-BFADCF40E223";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandRing1_rotateX";
	rename -uid "A79FC4CA-4269-413C-0A49-72B46A50A77E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -9.4753284454345703 1 -10.074559211730957
		 2 -9.9779720306396484 3 -9.2360963821411133 4 -7.9295125007629395 5 -6.2420845031738281
		 6 -4.7574191093444824 7 -4.1254706382751465 8 -4.1254706382751465 9 -4.1254706382751465
		 10 -4.1254706382751465 11 -4.4971370697021484 12 -5.4585647583007813 13 -6.7524867057800293
		 14 -8.1018381118774414 15 -9.2360963821411133 16 -10.148946762084961 17 -9.742985725402832
		 18 -10.002813339233398 19 -9.9943208694458008 20 -9.2360963821411133;
createNode animCurveTA -n "Character1_RightHandRing1_rotateY";
	rename -uid "54EEBAC9-4E1F-AFF7-9213-D9A97A6ACAA2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 15.397130966186525 1 11.441056251525879
		 2 7.5418148040771493 3 4.393496036529541 4 1.7979205846786501 5 -0.44553691148757935
		 6 -1.9526268243789673 7 -2.5015833377838135 8 -2.5015833377838135 9 -2.5015833377838135
		 10 -2.5015833377838135 11 -2.1927969455718994 12 -1.305327296257019 13 0.12758128345012665
		 14 2.0600275993347168 15 4.393496036529541 16 10.030913352966309 17 14.558754920959474
		 18 12.624667167663574 19 8.4034919738769531 20 4.393496036529541;
createNode animCurveTA -n "Character1_RightHandRing1_rotateZ";
	rename -uid "00994F31-42EB-9906-575F-24B89BE23476";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 69.758003234863281 1 55.653724670410156
		 2 41.663112640380859 3 29.838140487670898 4 19.401424407958984 5 9.4622821807861328
		 6 2.0026154518127441 7 -0.93790376186370839 8 -0.93790376186370839 9 -0.93790376186370839
		 10 -0.93790376186370839 11 0.73214489221572876 12 5.3051433563232422 13 12.116865158081055
		 14 20.507438659667969 15 29.838140487670898 16 50.557384490966797 17 66.576698303222656
		 18 59.729980468750007 19 44.769638061523438 20 29.838140487670898;
createNode animCurveTU -n "Character1_RightHandRing1_visibility";
	rename -uid "E43DE6C4-459A-98F3-47E3-FB8A5CC44E5A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightHandRing2_translateX";
	rename -uid "AF48D5F0-44A3-19C1-39B8-39905EC16EE9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.062777042388916;
createNode animCurveTL -n "Character1_RightHandRing2_translateY";
	rename -uid "B7BC69EC-4D1B-FA8D-803D-0094A0A9557A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.4930424690246582;
createNode animCurveTL -n "Character1_RightHandRing2_translateZ";
	rename -uid "7527FECE-475A-72CA-8B82-399FAECD0A43";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.86127841472625732;
createNode animCurveTU -n "Character1_RightHandRing2_scaleX";
	rename -uid "C90BBDA5-4061-0859-88DA-E39ADC853154";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandRing2_scaleY";
	rename -uid "BF9715AC-4433-03A4-02AE-2CA5C8A9ECA6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing2_scaleZ";
	rename -uid "8738928B-484C-5043-DAC6-5CBB5C424014";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandRing2_rotateX";
	rename -uid "F1029B7B-4539-C99A-BC25-F29239B2E5FC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 14.933642387390137 1 23.580265045166016
		 2 23.313127517700195 3 20.286947250366211 4 15.325875282287596 5 8.420684814453125
		 6 1.6812093257904053 7 -1.4053092002868652 8 -1.4053092002868652 9 -1.4053092002868652
		 10 -1.4053093194961548 11 0.54949796199798584 12 5.2741227149963379 13 10.989693641662598
		 14 16.280752182006836 15 20.286947250366211 16 23.876642227172852 17 22.7779541015625
		 18 23.867576599121094 19 23.213275909423828 20 20.286947250366211;
createNode animCurveTA -n "Character1_RightHandRing2_rotateY";
	rename -uid "22ED42D2-4440-6E7B-D0DD-92B47C426F10";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -73.483367919921875 1 -60.520496368408203
		 2 -47.158447265625 3 -34.944370269775391 4 -22.525449752807617 5 -10.398344039916992
		 6 -1.7855169773101807 7 1.3975378274917603 8 1.3975378274917603 9 1.3975378274917603
		 10 1.3975378274917603 11 -0.56958794593811035 12 -6.0608859062194824 13 -14.431308746337889
		 14 -24.585775375366211 15 -34.944370269775391 16 -51.782203674316406 17 -63.730579376220703
		 18 -58.376632690429695 19 -46.560127258300781 20 -34.944370269775391;
createNode animCurveTA -n "Character1_RightHandRing2_rotateZ";
	rename -uid "AA8490A8-4C38-1700-886F-BDB82E9CBA1B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 68.698143005371094 1 49.242431640625 2 38.0166015625
		 3 29.386693954467773 4 20.525728225708008 5 10.665127754211426 6 2.0611650943756104
		 7 -1.7039388418197632 8 -1.7039388418197632 9 -1.7039388418197632 10 -1.7039388418197632
		 11 0.6708107590675354 12 6.5677480697631836 13 14.15735912322998 14 22.049409866333008
		 15 29.386693954467773 16 41.538078308105469 17 52.703304290771484 18 47.165977478027344
		 19 37.576969146728516 20 29.386693954467773;
createNode animCurveTU -n "Character1_RightHandRing2_visibility";
	rename -uid "EFFA1B3A-494B-D420-9D60-A8836081935D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_RightHandRing3_translateX";
	rename -uid "93CF98BC-42C6-1169-5247-6CB1EE415590";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.5035178661346436;
createNode animCurveTL -n "Character1_RightHandRing3_translateY";
	rename -uid "EB5D1529-401A-E8AA-FD05-30A906B8A3AC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.9153993129730225;
createNode animCurveTL -n "Character1_RightHandRing3_translateZ";
	rename -uid "A58628E4-4A33-3EA9-1B9B-2BA522099433";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.6898331642150879;
createNode animCurveTU -n "Character1_RightHandRing3_scaleX";
	rename -uid "2B247F7C-4493-E45B-25BC-AB9CF03DBF02";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing3_scaleY";
	rename -uid "848C7156-4A71-CA30-39B5-6E9AE062F1AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing3_scaleZ";
	rename -uid "9B290E05-4C00-7A35-C46E-6DA935BF8EEB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandRing3_rotateX";
	rename -uid "0E34D9F4-4F05-E1C4-9545-BDB8F802E6AF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8060468676139863e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateY";
	rename -uid "B4A3068A-4058-EBF2-FF5F-169D2E1A4AB4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8223044523656426e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateZ";
	rename -uid "80D2A9D7-4106-DF4B-1E50-63AED86DCED0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8983762117841252e-007;
createNode animCurveTU -n "Character1_RightHandRing3_visibility";
	rename -uid "6D6C4B74-4E25-3EA0-D0F6-EA8D4171B1E9";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_Neck_translateX";
	rename -uid "2C4C1522-46F8-AE10-48DC-81B6D005B652";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 12.791294097900391;
createNode animCurveTL -n "Character1_Neck_translateY";
	rename -uid "E23A3CC5-4A7A-E133-3F2A-15B7FB6BAB2D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.3580474853515625;
createNode animCurveTL -n "Character1_Neck_translateZ";
	rename -uid "5ADE56D1-4A29-B6C2-2053-EE9670A26C37";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.6322288513183594;
createNode animCurveTU -n "Character1_Neck_scaleX";
	rename -uid "1BCFA817-4BEE-E8FF-A579-7D89FAE73614";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_Neck_scaleY";
	rename -uid "257FBF5F-4048-E4B7-5454-3181A0C75DDB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Neck_scaleZ";
	rename -uid "8C897885-488E-72D3-17BF-83ACA099BC19";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Neck_rotateX";
	rename -uid "300263EF-4E6D-7BAE-0A54-A9ACF9295789";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -1.8338052034378052 1 -3.9919862747192383
		 2 5.3670058250427246 3 11.719755172729492 4 12.580987930297852 5 12.800875663757324
		 6 12.56746768951416 7 12.039172172546387 8 11.377632141113281 9 10.761409759521484
		 10 10.380446434020996 11 11.204030990600586 12 9.7424049377441406 13 2.2342121601104736
		 14 -7.1106767654418945 15 -12.227648735046387 16 -9.2451114654541016 17 -1.7868446111679077
		 18 4.8886728286743164 19 7.773888111114502 20 9.5831975936889648;
createNode animCurveTA -n "Character1_Neck_rotateY";
	rename -uid "45D61FA7-4D3F-29DD-B281-C5A19C2D0BD1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 14.439492225646971 1 15.394542694091799
		 2 13.89285945892334 3 11.935980796813965 4 11.634413719177246 5 11.554109573364258
		 6 11.647758483886719 7 11.863092422485352 8 12.147427558898926 9 12.450980186462402
		 10 12.728231430053711 11 13.950593948364258 12 14.242578506469727 13 11.449750900268555
		 14 6.9360871315002441 15 2.4207630157470703 16 -1.0912102460861206 17 -3.9848177433013916
		 18 -6.3237180709838867 19 -7.7463045120239258 20 -8.7274017333984375;
createNode animCurveTA -n "Character1_Neck_rotateZ";
	rename -uid "34A26F90-4739-1DC1-8319-B1B4939685D5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -4.8509678840637207 1 -12.460810661315918
		 2 2.9950544834136963 3 16.775064468383789 4 18.965051651000977 5 19.501840591430664
		 6 18.901556015014648 7 17.685073852539063 8 16.373689651489258 9 15.485877990722654
		 10 15.535298347473146 11 20.301668167114258 12 22.813320159912109 13 15.863630294799805
		 14 7.1841931343078613 15 1.6407370567321777 16 -0.36452224850654602 17 -0.85388725996017456
		 18 -0.89724767208099365 19 -0.55391854047775269 20 0.27945694327354431;
createNode animCurveTU -n "Character1_Neck_visibility";
	rename -uid "97BCF25F-492A-12DA-9BA4-C38F7D57749E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "Character1_Head_translateX";
	rename -uid "7E94B271-4361-0E89-01E7-568620915289";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.627285957336426;
createNode animCurveTL -n "Character1_Head_translateY";
	rename -uid "E5493351-4BAF-07C0-19F2-318FC7E5E3A2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.64882755279541;
createNode animCurveTL -n "Character1_Head_translateZ";
	rename -uid "7D0344E5-4B9A-704F-F9F1-5A8B67EBCBCF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.1324095726013184;
createNode animCurveTU -n "Character1_Head_scaleX";
	rename -uid "6D1D74CE-4132-2E4A-704C-D4822ED789DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Head_scaleY";
	rename -uid "E6DF954A-4338-863C-EFF3-B4ACD894D1DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Head_scaleZ";
	rename -uid "7BAC0C45-48CE-A1A2-38D3-0490C23E5090";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_Head_rotateX";
	rename -uid "41AA03B1-4666-3E0A-924A-A59A7F710622";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -8.1512603759765625 1 -6.8721160888671875
		 2 -13.464981079101562 3 -18.956878662109375 4 -19.981416702270508 5 -20.367284774780273
		 6 -20.310441970825195 7 -20.006620407104492 8 -19.651437759399414 9 -19.440498352050781
		 10 -19.569486618041992 11 -22.178735733032227 12 -22.239429473876953 13 -14.339940071105955
		 14 -3.7208335399627686 15 4.1812443733215332 16 6.7503490447998047 17 6.6134839057922363
		 18 6.1824831962585449 19 5.9117279052734375 20 5.3095211982727051;
createNode animCurveTA -n "Character1_Head_rotateY";
	rename -uid "2977E606-4815-DB55-54AE-A3B6376A4187";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -5.3131990432739258 1 -0.97128665447235107
		 2 2.4371719360351562 3 4.5896391868591309 4 5.2896461486816406 5 5.8302388191223145
		 6 6.2078642845153809 7 6.4191498756408691 8 6.4607448577880859 9 6.3292446136474609
		 10 6.0212154388427734 11 4.3125123977661133 12 1.0136381387710571 13 -4.1325764656066895
		 14 -9.7380266189575195 15 -11.818535804748535 16 -7.5191421508789054 17 0.30000847578048706
		 18 7.044060230255127 19 9.9046239852905273 20 11.555663108825684;
createNode animCurveTA -n "Character1_Head_rotateZ";
	rename -uid "06137EDE-409A-F95E-EBF8-F99EF3E61A62";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -10.237140655517578 1 -18.257381439208984
		 2 -4.461268424987793 3 7.6118040084838876 4 9.1694726943969727 5 9.2353525161743164
		 6 8.2856025695800781 7 6.7968401908874512 8 5.2456779479980469 9 4.1085319519042969
		 10 3.8616681098937988 11 7.755913257598877 12 10.819770812988281 13 7.0947270393371582
		 14 1.2929229736328125 15 -2.4950435161590576 16 -2.3308515548706055 17 -0.25197499990463257
		 18 1.8615882396697996 19 3.5181405544281006 20 5.2613430023193359;
createNode animCurveTU -n "Character1_Head_visibility";
	rename -uid "DBD2E69F-4B9C-59EA-664C-EBAC73A4EEDD";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode animCurveTL -n "eyes_translateX";
	rename -uid "B52A2743-47EA-AA7B-6A13-719B5946DB2B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  12 30.644031524658203 13 30.644031524658203
		 14 31.059986114501953 15 31.059986114501953 17 31.059986114501953 18 31.059986114501953
		 19 31.017671585083008 20 30.947149276733398;
createNode animCurveTL -n "eyes_translateY";
	rename -uid "77D237F8-4F6F-D064-783F-5CA091D98B64";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  12 4.4016318321228027 13 4.4016318321228027
		 14 -1.248521089553833 15 -1.248521089553833 17 -1.248521089553833 18 -1.248521089553833
		 19 -0.78032588958740234 20 -5.0992542810490704e-007;
createNode animCurveTL -n "eyes_translateZ";
	rename -uid "D1F07D8A-4674-7019-9E9B-7EA70A9AB663";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  12 1.0660551786422729 13 1.0660551786422729
		 14 1.2201347351074219 15 1.2201347351074219 17 1.2201347351074219 18 1.2201347351074219
		 19 0.76258420944213867 20 1.121469657048256e-013;
createNode animCurveTU -n "eyes_scaleX";
	rename -uid "1BD06DDA-4496-E11F-7F60-7D90A96EBA42";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleY";
	rename -uid "D9E85E79-4718-413C-F658-B788371829DE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleZ";
	rename -uid "693FEEA5-41CB-89C6-C588-8CB744D73CCC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "eyes_rotateX";
	rename -uid "6507379E-48D2-F130-401B-94989AF0FFC2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  12 0 13 0 14 3.0423920154571533 15 2.8143744468688965
		 16 2.1147055625915527 17 1.2599389553070068 18 0.56662797927856445 19 0.22737616300582886
		 20 0;
createNode animCurveTA -n "eyes_rotateY";
	rename -uid "901522CE-40D9-EDEC-5042-6189814788AB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  12 0 13 0 14 -5.5776996612548828 15 -5.9919772148132324
		 16 -5.7351245880126953 17 -5.0390787124633789 18 -4.1357769966125488 19 -2.3524565696716309
		 20 0;
createNode animCurveTA -n "eyes_rotateZ";
	rename -uid "F530089A-4277-625A-B3A9-4DB3018DABE4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  12 0 13 0 14 8.6488733291625977 15 8.7821731567382812
		 16 7.7564163208007821 17 6.1443519592285156 18 4.5187335014343262 19 2.4638388156890869
		 20 0;
createNode animCurveTU -n "eyes_visibility";
	rename -uid "05FE8E00-49DC-3DF4-CF5D-F7A223BA8647";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "jaw_translateX";
	rename -uid "8BEB6456-4834-7179-71EA-CEBCDCCDFA14";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.07279486209154129;
createNode animCurveTL -n "jaw_translateY";
	rename -uid "DC8406E7-4DB1-9BF1-F423-4CBC8BA6BCB2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.25670051574707;
createNode animCurveTL -n "jaw_translateZ";
	rename -uid "772AEBC2-4AD7-9554-7FF4-4687C90B3220";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.2238426557523781e-007;
createNode animCurveTU -n "jaw_scaleX";
	rename -uid "C266B916-4029-8FBB-B83A-C4BD34198C40";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleY";
	rename -uid "39764234-4AAA-5928-EBC8-DD831B456EBE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleZ";
	rename -uid "27BBC79D-415B-5F1C-002F-B3B4D91D5B1C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "jaw_rotateX";
	rename -uid "1E725E6F-47C0-DC18-C8E4-69A8F191B349";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -49.420639038085938 1 -49.507904052734375
		 2 -45.284500122070313 3 -38.05487060546875 4 -31.042037963867191 5 -24.963485717773438
		 6 -20.002822875976562 7 -14.533035278320311 8 -8.2519989013671875 9 -3.468719482421875
		 10 -3.1415557861328125 11 -12.901885986328125 12 -29.643020629882816 13 -44.314743041992188
		 14 -52.564323425292969 15 -54.659049987792969 16 -51.805709838867188 17 -47.528289794921875
		 18 -42.388229370117188 19 -37.029937744140625 20 -32.155029296875;
createNode animCurveTA -n "jaw_rotateY";
	rename -uid "49943168-4DF0-15AC-2C5F-7FBBE6EC31B2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -121.1512985229492 1 -124.86289978027342
		 2 -128.7880859375 3 -132.85845947265625 4 -137.174560546875 5 -140.82276916503906
		 6 -142.81217956542969 7 -142.31451416015625 8 -140.21298217773437 9 -137.81320190429688
		 10 -136.08261108398438 11 -135.75184631347656 12 -135.69618225097656 13 -133.73587036132812
		 14 -124.06605529785156 15 -115.25096893310547 16 -114.36754608154297 17 -115.18109893798827
		 18 -117.02458953857422 19 -119.15931701660156 20 -120.80289459228517;
createNode animCurveTA -n "jaw_rotateZ";
	rename -uid "B96C138B-47D2-A419-2212-858B7E56C8D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -44.718704223632812 1 -31.68621826171875
		 2 -25.130157470703125 3 -26.950698852539062 4 -30.736953735351559 5 -34.962692260742188
		 6 -38.6575927734375 7 -42.625717163085937 8 -47.5662841796875 9 -52.302886962890625
		 10 -55.108871459960938 11 -53.388206481933594 12 -48.667922973632812 13 -44.259033203125
		 14 -42.6046142578125 15 -43.233047485351563 16 -43.602096557617187 17 -43.845809936523438
		 18 -44.096969604492188 19 -44.381744384765625 20 -44.638351440429687;
createNode animCurveTU -n "jaw_visibility";
	rename -uid "DB382D9A-4500-C333-F72D-6CB108F990B3";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 1 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 0.033314831554889679;
	setAttr -s 2 ".kiy[0:1]"  0 0.99944490194320679;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "77D4D858-4A17-206B-950D-56835F44FA50";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "5912A5A5-4E98-E80F-137D-92933AE50E92";
createNode displayLayer -n "defaultLayer";
	rename -uid "D86A39EE-4D23-C701-8C8A-6480B9439A93";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "4230A104-453E-D5A4-7D24-70B4A9499666";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "CF9B8D7D-4503-F458-00AE-6AA0727F546E";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "818DCF2E-4077-B9EE-39F9-5681B238DD34";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1636\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1636\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1313\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1313\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 14 100 -ps 2 86 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1313\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1313\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "EE1DA04C-4F43-D2DB-936C-21B43A00862B";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 20 -ast 0 -aet 250 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 20;
	setAttr ".unw" 20;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
connectAttr "ref_frame.s" "Character1_Hips.is";
connectAttr "Character1_Hips_scaleX.o" "Character1_Hips.sx";
connectAttr "Character1_Hips_scaleY.o" "Character1_Hips.sy";
connectAttr "Character1_Hips_scaleZ.o" "Character1_Hips.sz";
connectAttr "Character1_Hips_translateX.o" "Character1_Hips.tx";
connectAttr "Character1_Hips_translateY.o" "Character1_Hips.ty";
connectAttr "Character1_Hips_translateZ.o" "Character1_Hips.tz";
connectAttr "Character1_Hips_rotateX.o" "Character1_Hips.rx";
connectAttr "Character1_Hips_rotateY.o" "Character1_Hips.ry";
connectAttr "Character1_Hips_rotateZ.o" "Character1_Hips.rz";
connectAttr "Character1_Hips_visibility.o" "Character1_Hips.v";
connectAttr "Character1_Hips.s" "Character1_LeftUpLeg.is";
connectAttr "Character1_LeftUpLeg_scaleX.o" "Character1_LeftUpLeg.sx";
connectAttr "Character1_LeftUpLeg_scaleY.o" "Character1_LeftUpLeg.sy";
connectAttr "Character1_LeftUpLeg_scaleZ.o" "Character1_LeftUpLeg.sz";
connectAttr "Character1_LeftUpLeg_translateX.o" "Character1_LeftUpLeg.tx";
connectAttr "Character1_LeftUpLeg_translateY.o" "Character1_LeftUpLeg.ty";
connectAttr "Character1_LeftUpLeg_translateZ.o" "Character1_LeftUpLeg.tz";
connectAttr "Character1_LeftUpLeg_rotateX.o" "Character1_LeftUpLeg.rx";
connectAttr "Character1_LeftUpLeg_rotateY.o" "Character1_LeftUpLeg.ry";
connectAttr "Character1_LeftUpLeg_rotateZ.o" "Character1_LeftUpLeg.rz";
connectAttr "Character1_LeftUpLeg_visibility.o" "Character1_LeftUpLeg.v";
connectAttr "Character1_LeftUpLeg.s" "Character1_LeftLeg.is";
connectAttr "Character1_LeftLeg_scaleX.o" "Character1_LeftLeg.sx";
connectAttr "Character1_LeftLeg_scaleY.o" "Character1_LeftLeg.sy";
connectAttr "Character1_LeftLeg_scaleZ.o" "Character1_LeftLeg.sz";
connectAttr "Character1_LeftLeg_translateX.o" "Character1_LeftLeg.tx";
connectAttr "Character1_LeftLeg_translateY.o" "Character1_LeftLeg.ty";
connectAttr "Character1_LeftLeg_translateZ.o" "Character1_LeftLeg.tz";
connectAttr "Character1_LeftLeg_rotateX.o" "Character1_LeftLeg.rx";
connectAttr "Character1_LeftLeg_rotateY.o" "Character1_LeftLeg.ry";
connectAttr "Character1_LeftLeg_rotateZ.o" "Character1_LeftLeg.rz";
connectAttr "Character1_LeftLeg_visibility.o" "Character1_LeftLeg.v";
connectAttr "Character1_LeftLeg.s" "Character1_LeftFoot.is";
connectAttr "Character1_LeftFoot_scaleX.o" "Character1_LeftFoot.sx";
connectAttr "Character1_LeftFoot_scaleY.o" "Character1_LeftFoot.sy";
connectAttr "Character1_LeftFoot_scaleZ.o" "Character1_LeftFoot.sz";
connectAttr "Character1_LeftFoot_translateX.o" "Character1_LeftFoot.tx";
connectAttr "Character1_LeftFoot_translateY.o" "Character1_LeftFoot.ty";
connectAttr "Character1_LeftFoot_translateZ.o" "Character1_LeftFoot.tz";
connectAttr "Character1_LeftFoot_rotateX.o" "Character1_LeftFoot.rx";
connectAttr "Character1_LeftFoot_rotateY.o" "Character1_LeftFoot.ry";
connectAttr "Character1_LeftFoot_rotateZ.o" "Character1_LeftFoot.rz";
connectAttr "Character1_LeftFoot_visibility.o" "Character1_LeftFoot.v";
connectAttr "Character1_LeftFoot.s" "Character1_LeftToeBase.is";
connectAttr "Character1_LeftToeBase_scaleX.o" "Character1_LeftToeBase.sx";
connectAttr "Character1_LeftToeBase_scaleY.o" "Character1_LeftToeBase.sy";
connectAttr "Character1_LeftToeBase_scaleZ.o" "Character1_LeftToeBase.sz";
connectAttr "Character1_LeftToeBase_translateX.o" "Character1_LeftToeBase.tx";
connectAttr "Character1_LeftToeBase_translateY.o" "Character1_LeftToeBase.ty";
connectAttr "Character1_LeftToeBase_translateZ.o" "Character1_LeftToeBase.tz";
connectAttr "Character1_LeftToeBase_rotateX.o" "Character1_LeftToeBase.rx";
connectAttr "Character1_LeftToeBase_rotateY.o" "Character1_LeftToeBase.ry";
connectAttr "Character1_LeftToeBase_rotateZ.o" "Character1_LeftToeBase.rz";
connectAttr "Character1_LeftToeBase_visibility.o" "Character1_LeftToeBase.v";
connectAttr "Character1_LeftToeBase.s" "Character1_LeftFootMiddle1.is";
connectAttr "Character1_LeftFootMiddle1_scaleX.o" "Character1_LeftFootMiddle1.sx"
		;
connectAttr "Character1_LeftFootMiddle1_scaleY.o" "Character1_LeftFootMiddle1.sy"
		;
connectAttr "Character1_LeftFootMiddle1_scaleZ.o" "Character1_LeftFootMiddle1.sz"
		;
connectAttr "Character1_LeftFootMiddle1_translateX.o" "Character1_LeftFootMiddle1.tx"
		;
connectAttr "Character1_LeftFootMiddle1_translateY.o" "Character1_LeftFootMiddle1.ty"
		;
connectAttr "Character1_LeftFootMiddle1_translateZ.o" "Character1_LeftFootMiddle1.tz"
		;
connectAttr "Character1_LeftFootMiddle1_rotateX.o" "Character1_LeftFootMiddle1.rx"
		;
connectAttr "Character1_LeftFootMiddle1_rotateY.o" "Character1_LeftFootMiddle1.ry"
		;
connectAttr "Character1_LeftFootMiddle1_rotateZ.o" "Character1_LeftFootMiddle1.rz"
		;
connectAttr "Character1_LeftFootMiddle1_visibility.o" "Character1_LeftFootMiddle1.v"
		;
connectAttr "Character1_LeftFootMiddle1.s" "Character1_LeftFootMiddle2.is";
connectAttr "Character1_LeftFootMiddle2_translateX.o" "Character1_LeftFootMiddle2.tx"
		;
connectAttr "Character1_LeftFootMiddle2_translateY.o" "Character1_LeftFootMiddle2.ty"
		;
connectAttr "Character1_LeftFootMiddle2_translateZ.o" "Character1_LeftFootMiddle2.tz"
		;
connectAttr "Character1_LeftFootMiddle2_scaleX.o" "Character1_LeftFootMiddle2.sx"
		;
connectAttr "Character1_LeftFootMiddle2_scaleY.o" "Character1_LeftFootMiddle2.sy"
		;
connectAttr "Character1_LeftFootMiddle2_scaleZ.o" "Character1_LeftFootMiddle2.sz"
		;
connectAttr "Character1_LeftFootMiddle2_rotateX.o" "Character1_LeftFootMiddle2.rx"
		;
connectAttr "Character1_LeftFootMiddle2_rotateY.o" "Character1_LeftFootMiddle2.ry"
		;
connectAttr "Character1_LeftFootMiddle2_rotateZ.o" "Character1_LeftFootMiddle2.rz"
		;
connectAttr "Character1_LeftFootMiddle2_visibility.o" "Character1_LeftFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_RightUpLeg.is";
connectAttr "Character1_RightUpLeg_scaleX.o" "Character1_RightUpLeg.sx";
connectAttr "Character1_RightUpLeg_scaleY.o" "Character1_RightUpLeg.sy";
connectAttr "Character1_RightUpLeg_scaleZ.o" "Character1_RightUpLeg.sz";
connectAttr "Character1_RightUpLeg_translateX.o" "Character1_RightUpLeg.tx";
connectAttr "Character1_RightUpLeg_translateY.o" "Character1_RightUpLeg.ty";
connectAttr "Character1_RightUpLeg_translateZ.o" "Character1_RightUpLeg.tz";
connectAttr "Character1_RightUpLeg_rotateX.o" "Character1_RightUpLeg.rx";
connectAttr "Character1_RightUpLeg_rotateY.o" "Character1_RightUpLeg.ry";
connectAttr "Character1_RightUpLeg_rotateZ.o" "Character1_RightUpLeg.rz";
connectAttr "Character1_RightUpLeg_visibility.o" "Character1_RightUpLeg.v";
connectAttr "Character1_RightUpLeg.s" "Character1_RightLeg.is";
connectAttr "Character1_RightLeg_scaleX.o" "Character1_RightLeg.sx";
connectAttr "Character1_RightLeg_scaleY.o" "Character1_RightLeg.sy";
connectAttr "Character1_RightLeg_scaleZ.o" "Character1_RightLeg.sz";
connectAttr "Character1_RightLeg_translateX.o" "Character1_RightLeg.tx";
connectAttr "Character1_RightLeg_translateY.o" "Character1_RightLeg.ty";
connectAttr "Character1_RightLeg_translateZ.o" "Character1_RightLeg.tz";
connectAttr "Character1_RightLeg_rotateX.o" "Character1_RightLeg.rx";
connectAttr "Character1_RightLeg_rotateY.o" "Character1_RightLeg.ry";
connectAttr "Character1_RightLeg_rotateZ.o" "Character1_RightLeg.rz";
connectAttr "Character1_RightLeg_visibility.o" "Character1_RightLeg.v";
connectAttr "Character1_RightLeg.s" "Character1_RightFoot.is";
connectAttr "Character1_RightFoot_scaleX.o" "Character1_RightFoot.sx";
connectAttr "Character1_RightFoot_scaleY.o" "Character1_RightFoot.sy";
connectAttr "Character1_RightFoot_scaleZ.o" "Character1_RightFoot.sz";
connectAttr "Character1_RightFoot_translateX.o" "Character1_RightFoot.tx";
connectAttr "Character1_RightFoot_translateY.o" "Character1_RightFoot.ty";
connectAttr "Character1_RightFoot_translateZ.o" "Character1_RightFoot.tz";
connectAttr "Character1_RightFoot_rotateX.o" "Character1_RightFoot.rx";
connectAttr "Character1_RightFoot_rotateY.o" "Character1_RightFoot.ry";
connectAttr "Character1_RightFoot_rotateZ.o" "Character1_RightFoot.rz";
connectAttr "Character1_RightFoot_visibility.o" "Character1_RightFoot.v";
connectAttr "Character1_RightFoot.s" "Character1_RightToeBase.is";
connectAttr "Character1_RightToeBase_scaleX.o" "Character1_RightToeBase.sx";
connectAttr "Character1_RightToeBase_scaleY.o" "Character1_RightToeBase.sy";
connectAttr "Character1_RightToeBase_scaleZ.o" "Character1_RightToeBase.sz";
connectAttr "Character1_RightToeBase_translateX.o" "Character1_RightToeBase.tx";
connectAttr "Character1_RightToeBase_translateY.o" "Character1_RightToeBase.ty";
connectAttr "Character1_RightToeBase_translateZ.o" "Character1_RightToeBase.tz";
connectAttr "Character1_RightToeBase_rotateX.o" "Character1_RightToeBase.rx";
connectAttr "Character1_RightToeBase_rotateY.o" "Character1_RightToeBase.ry";
connectAttr "Character1_RightToeBase_rotateZ.o" "Character1_RightToeBase.rz";
connectAttr "Character1_RightToeBase_visibility.o" "Character1_RightToeBase.v";
connectAttr "Character1_RightToeBase.s" "Character1_RightFootMiddle1.is";
connectAttr "Character1_RightFootMiddle1_scaleX.o" "Character1_RightFootMiddle1.sx"
		;
connectAttr "Character1_RightFootMiddle1_scaleY.o" "Character1_RightFootMiddle1.sy"
		;
connectAttr "Character1_RightFootMiddle1_scaleZ.o" "Character1_RightFootMiddle1.sz"
		;
connectAttr "Character1_RightFootMiddle1_translateX.o" "Character1_RightFootMiddle1.tx"
		;
connectAttr "Character1_RightFootMiddle1_translateY.o" "Character1_RightFootMiddle1.ty"
		;
connectAttr "Character1_RightFootMiddle1_translateZ.o" "Character1_RightFootMiddle1.tz"
		;
connectAttr "Character1_RightFootMiddle1_rotateX.o" "Character1_RightFootMiddle1.rx"
		;
connectAttr "Character1_RightFootMiddle1_rotateY.o" "Character1_RightFootMiddle1.ry"
		;
connectAttr "Character1_RightFootMiddle1_rotateZ.o" "Character1_RightFootMiddle1.rz"
		;
connectAttr "Character1_RightFootMiddle1_visibility.o" "Character1_RightFootMiddle1.v"
		;
connectAttr "Character1_RightFootMiddle1.s" "Character1_RightFootMiddle2.is";
connectAttr "Character1_RightFootMiddle2_translateX.o" "Character1_RightFootMiddle2.tx"
		;
connectAttr "Character1_RightFootMiddle2_translateY.o" "Character1_RightFootMiddle2.ty"
		;
connectAttr "Character1_RightFootMiddle2_translateZ.o" "Character1_RightFootMiddle2.tz"
		;
connectAttr "Character1_RightFootMiddle2_scaleX.o" "Character1_RightFootMiddle2.sx"
		;
connectAttr "Character1_RightFootMiddle2_scaleY.o" "Character1_RightFootMiddle2.sy"
		;
connectAttr "Character1_RightFootMiddle2_scaleZ.o" "Character1_RightFootMiddle2.sz"
		;
connectAttr "Character1_RightFootMiddle2_rotateX.o" "Character1_RightFootMiddle2.rx"
		;
connectAttr "Character1_RightFootMiddle2_rotateY.o" "Character1_RightFootMiddle2.ry"
		;
connectAttr "Character1_RightFootMiddle2_rotateZ.o" "Character1_RightFootMiddle2.rz"
		;
connectAttr "Character1_RightFootMiddle2_visibility.o" "Character1_RightFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_Spine.is";
connectAttr "Character1_Spine_scaleX.o" "Character1_Spine.sx";
connectAttr "Character1_Spine_scaleY.o" "Character1_Spine.sy";
connectAttr "Character1_Spine_scaleZ.o" "Character1_Spine.sz";
connectAttr "Character1_Spine_translateX.o" "Character1_Spine.tx";
connectAttr "Character1_Spine_translateY.o" "Character1_Spine.ty";
connectAttr "Character1_Spine_translateZ.o" "Character1_Spine.tz";
connectAttr "Character1_Spine_rotateX.o" "Character1_Spine.rx";
connectAttr "Character1_Spine_rotateY.o" "Character1_Spine.ry";
connectAttr "Character1_Spine_rotateZ.o" "Character1_Spine.rz";
connectAttr "Character1_Spine_visibility.o" "Character1_Spine.v";
connectAttr "Character1_Spine.s" "Character1_Spine1.is";
connectAttr "Character1_Spine1_scaleX.o" "Character1_Spine1.sx";
connectAttr "Character1_Spine1_scaleY.o" "Character1_Spine1.sy";
connectAttr "Character1_Spine1_scaleZ.o" "Character1_Spine1.sz";
connectAttr "Character1_Spine1_translateX.o" "Character1_Spine1.tx";
connectAttr "Character1_Spine1_translateY.o" "Character1_Spine1.ty";
connectAttr "Character1_Spine1_translateZ.o" "Character1_Spine1.tz";
connectAttr "Character1_Spine1_rotateX.o" "Character1_Spine1.rx";
connectAttr "Character1_Spine1_rotateY.o" "Character1_Spine1.ry";
connectAttr "Character1_Spine1_rotateZ.o" "Character1_Spine1.rz";
connectAttr "Character1_Spine1_visibility.o" "Character1_Spine1.v";
connectAttr "Character1_Spine1.s" "Character1_LeftShoulder.is";
connectAttr "Character1_LeftShoulder_scaleX.o" "Character1_LeftShoulder.sx";
connectAttr "Character1_LeftShoulder_scaleY.o" "Character1_LeftShoulder.sy";
connectAttr "Character1_LeftShoulder_scaleZ.o" "Character1_LeftShoulder.sz";
connectAttr "Character1_LeftShoulder_translateX.o" "Character1_LeftShoulder.tx";
connectAttr "Character1_LeftShoulder_translateY.o" "Character1_LeftShoulder.ty";
connectAttr "Character1_LeftShoulder_translateZ.o" "Character1_LeftShoulder.tz";
connectAttr "Character1_LeftShoulder_rotateX.o" "Character1_LeftShoulder.rx";
connectAttr "Character1_LeftShoulder_rotateY.o" "Character1_LeftShoulder.ry";
connectAttr "Character1_LeftShoulder_rotateZ.o" "Character1_LeftShoulder.rz";
connectAttr "Character1_LeftShoulder_visibility.o" "Character1_LeftShoulder.v";
connectAttr "Character1_LeftShoulder.s" "Character1_LeftArm.is";
connectAttr "Character1_LeftArm_scaleX.o" "Character1_LeftArm.sx";
connectAttr "Character1_LeftArm_scaleY.o" "Character1_LeftArm.sy";
connectAttr "Character1_LeftArm_scaleZ.o" "Character1_LeftArm.sz";
connectAttr "Character1_LeftArm_translateX.o" "Character1_LeftArm.tx";
connectAttr "Character1_LeftArm_translateY.o" "Character1_LeftArm.ty";
connectAttr "Character1_LeftArm_translateZ.o" "Character1_LeftArm.tz";
connectAttr "Character1_LeftArm_rotateX.o" "Character1_LeftArm.rx";
connectAttr "Character1_LeftArm_rotateY.o" "Character1_LeftArm.ry";
connectAttr "Character1_LeftArm_rotateZ.o" "Character1_LeftArm.rz";
connectAttr "Character1_LeftArm_visibility.o" "Character1_LeftArm.v";
connectAttr "Character1_LeftArm.s" "Character1_LeftForeArm.is";
connectAttr "Character1_LeftForeArm_scaleX.o" "Character1_LeftForeArm.sx";
connectAttr "Character1_LeftForeArm_scaleY.o" "Character1_LeftForeArm.sy";
connectAttr "Character1_LeftForeArm_scaleZ.o" "Character1_LeftForeArm.sz";
connectAttr "Character1_LeftForeArm_translateX.o" "Character1_LeftForeArm.tx";
connectAttr "Character1_LeftForeArm_translateY.o" "Character1_LeftForeArm.ty";
connectAttr "Character1_LeftForeArm_translateZ.o" "Character1_LeftForeArm.tz";
connectAttr "Character1_LeftForeArm_rotateX.o" "Character1_LeftForeArm.rx";
connectAttr "Character1_LeftForeArm_rotateY.o" "Character1_LeftForeArm.ry";
connectAttr "Character1_LeftForeArm_rotateZ.o" "Character1_LeftForeArm.rz";
connectAttr "Character1_LeftForeArm_visibility.o" "Character1_LeftForeArm.v";
connectAttr "Character1_LeftForeArm.s" "Character1_LeftHand.is";
connectAttr "Character1_LeftHand_lockInfluenceWeights.o" "Character1_LeftHand.liw"
		;
connectAttr "Character1_LeftHand_scaleX.o" "Character1_LeftHand.sx";
connectAttr "Character1_LeftHand_scaleY.o" "Character1_LeftHand.sy";
connectAttr "Character1_LeftHand_scaleZ.o" "Character1_LeftHand.sz";
connectAttr "Character1_LeftHand_translateX.o" "Character1_LeftHand.tx";
connectAttr "Character1_LeftHand_translateY.o" "Character1_LeftHand.ty";
connectAttr "Character1_LeftHand_translateZ.o" "Character1_LeftHand.tz";
connectAttr "Character1_LeftHand_rotateX.o" "Character1_LeftHand.rx";
connectAttr "Character1_LeftHand_rotateY.o" "Character1_LeftHand.ry";
connectAttr "Character1_LeftHand_rotateZ.o" "Character1_LeftHand.rz";
connectAttr "Character1_LeftHand_visibility.o" "Character1_LeftHand.v";
connectAttr "Character1_LeftHand.s" "Character1_LeftHandThumb1.is";
connectAttr "Character1_LeftHandThumb1_scaleX.o" "Character1_LeftHandThumb1.sx";
connectAttr "Character1_LeftHandThumb1_scaleY.o" "Character1_LeftHandThumb1.sy";
connectAttr "Character1_LeftHandThumb1_scaleZ.o" "Character1_LeftHandThumb1.sz";
connectAttr "Character1_LeftHandThumb1_translateX.o" "Character1_LeftHandThumb1.tx"
		;
connectAttr "Character1_LeftHandThumb1_translateY.o" "Character1_LeftHandThumb1.ty"
		;
connectAttr "Character1_LeftHandThumb1_translateZ.o" "Character1_LeftHandThumb1.tz"
		;
connectAttr "Character1_LeftHandThumb1_rotateX.o" "Character1_LeftHandThumb1.rx"
		;
connectAttr "Character1_LeftHandThumb1_rotateY.o" "Character1_LeftHandThumb1.ry"
		;
connectAttr "Character1_LeftHandThumb1_rotateZ.o" "Character1_LeftHandThumb1.rz"
		;
connectAttr "Character1_LeftHandThumb1_visibility.o" "Character1_LeftHandThumb1.v"
		;
connectAttr "Character1_LeftHandThumb1.s" "Character1_LeftHandThumb2.is";
connectAttr "Character1_LeftHandThumb2_scaleX.o" "Character1_LeftHandThumb2.sx";
connectAttr "Character1_LeftHandThumb2_scaleY.o" "Character1_LeftHandThumb2.sy";
connectAttr "Character1_LeftHandThumb2_scaleZ.o" "Character1_LeftHandThumb2.sz";
connectAttr "Character1_LeftHandThumb2_translateX.o" "Character1_LeftHandThumb2.tx"
		;
connectAttr "Character1_LeftHandThumb2_translateY.o" "Character1_LeftHandThumb2.ty"
		;
connectAttr "Character1_LeftHandThumb2_translateZ.o" "Character1_LeftHandThumb2.tz"
		;
connectAttr "Character1_LeftHandThumb2_rotateX.o" "Character1_LeftHandThumb2.rx"
		;
connectAttr "Character1_LeftHandThumb2_rotateY.o" "Character1_LeftHandThumb2.ry"
		;
connectAttr "Character1_LeftHandThumb2_rotateZ.o" "Character1_LeftHandThumb2.rz"
		;
connectAttr "Character1_LeftHandThumb2_visibility.o" "Character1_LeftHandThumb2.v"
		;
connectAttr "Character1_LeftHandThumb2.s" "Character1_LeftHandThumb3.is";
connectAttr "Character1_LeftHandThumb3_translateX.o" "Character1_LeftHandThumb3.tx"
		;
connectAttr "Character1_LeftHandThumb3_translateY.o" "Character1_LeftHandThumb3.ty"
		;
connectAttr "Character1_LeftHandThumb3_translateZ.o" "Character1_LeftHandThumb3.tz"
		;
connectAttr "Character1_LeftHandThumb3_scaleX.o" "Character1_LeftHandThumb3.sx";
connectAttr "Character1_LeftHandThumb3_scaleY.o" "Character1_LeftHandThumb3.sy";
connectAttr "Character1_LeftHandThumb3_scaleZ.o" "Character1_LeftHandThumb3.sz";
connectAttr "Character1_LeftHandThumb3_rotateX.o" "Character1_LeftHandThumb3.rx"
		;
connectAttr "Character1_LeftHandThumb3_rotateY.o" "Character1_LeftHandThumb3.ry"
		;
connectAttr "Character1_LeftHandThumb3_rotateZ.o" "Character1_LeftHandThumb3.rz"
		;
connectAttr "Character1_LeftHandThumb3_visibility.o" "Character1_LeftHandThumb3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandIndex1.is";
connectAttr "Character1_LeftHandIndex1_scaleX.o" "Character1_LeftHandIndex1.sx";
connectAttr "Character1_LeftHandIndex1_scaleY.o" "Character1_LeftHandIndex1.sy";
connectAttr "Character1_LeftHandIndex1_scaleZ.o" "Character1_LeftHandIndex1.sz";
connectAttr "Character1_LeftHandIndex1_translateX.o" "Character1_LeftHandIndex1.tx"
		;
connectAttr "Character1_LeftHandIndex1_translateY.o" "Character1_LeftHandIndex1.ty"
		;
connectAttr "Character1_LeftHandIndex1_translateZ.o" "Character1_LeftHandIndex1.tz"
		;
connectAttr "Character1_LeftHandIndex1_rotateX.o" "Character1_LeftHandIndex1.rx"
		;
connectAttr "Character1_LeftHandIndex1_rotateY.o" "Character1_LeftHandIndex1.ry"
		;
connectAttr "Character1_LeftHandIndex1_rotateZ.o" "Character1_LeftHandIndex1.rz"
		;
connectAttr "Character1_LeftHandIndex1_visibility.o" "Character1_LeftHandIndex1.v"
		;
connectAttr "Character1_LeftHandIndex1.s" "Character1_LeftHandIndex2.is";
connectAttr "Character1_LeftHandIndex2_scaleX.o" "Character1_LeftHandIndex2.sx";
connectAttr "Character1_LeftHandIndex2_scaleY.o" "Character1_LeftHandIndex2.sy";
connectAttr "Character1_LeftHandIndex2_scaleZ.o" "Character1_LeftHandIndex2.sz";
connectAttr "Character1_LeftHandIndex2_translateX.o" "Character1_LeftHandIndex2.tx"
		;
connectAttr "Character1_LeftHandIndex2_translateY.o" "Character1_LeftHandIndex2.ty"
		;
connectAttr "Character1_LeftHandIndex2_translateZ.o" "Character1_LeftHandIndex2.tz"
		;
connectAttr "Character1_LeftHandIndex2_rotateX.o" "Character1_LeftHandIndex2.rx"
		;
connectAttr "Character1_LeftHandIndex2_rotateY.o" "Character1_LeftHandIndex2.ry"
		;
connectAttr "Character1_LeftHandIndex2_rotateZ.o" "Character1_LeftHandIndex2.rz"
		;
connectAttr "Character1_LeftHandIndex2_visibility.o" "Character1_LeftHandIndex2.v"
		;
connectAttr "Character1_LeftHandIndex2.s" "Character1_LeftHandIndex3.is";
connectAttr "Character1_LeftHandIndex3_translateX.o" "Character1_LeftHandIndex3.tx"
		;
connectAttr "Character1_LeftHandIndex3_translateY.o" "Character1_LeftHandIndex3.ty"
		;
connectAttr "Character1_LeftHandIndex3_translateZ.o" "Character1_LeftHandIndex3.tz"
		;
connectAttr "Character1_LeftHandIndex3_scaleX.o" "Character1_LeftHandIndex3.sx";
connectAttr "Character1_LeftHandIndex3_scaleY.o" "Character1_LeftHandIndex3.sy";
connectAttr "Character1_LeftHandIndex3_scaleZ.o" "Character1_LeftHandIndex3.sz";
connectAttr "Character1_LeftHandIndex3_rotateX.o" "Character1_LeftHandIndex3.rx"
		;
connectAttr "Character1_LeftHandIndex3_rotateY.o" "Character1_LeftHandIndex3.ry"
		;
connectAttr "Character1_LeftHandIndex3_rotateZ.o" "Character1_LeftHandIndex3.rz"
		;
connectAttr "Character1_LeftHandIndex3_visibility.o" "Character1_LeftHandIndex3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandRing1.is";
connectAttr "Character1_LeftHandRing1_scaleX.o" "Character1_LeftHandRing1.sx";
connectAttr "Character1_LeftHandRing1_scaleY.o" "Character1_LeftHandRing1.sy";
connectAttr "Character1_LeftHandRing1_scaleZ.o" "Character1_LeftHandRing1.sz";
connectAttr "Character1_LeftHandRing1_translateX.o" "Character1_LeftHandRing1.tx"
		;
connectAttr "Character1_LeftHandRing1_translateY.o" "Character1_LeftHandRing1.ty"
		;
connectAttr "Character1_LeftHandRing1_translateZ.o" "Character1_LeftHandRing1.tz"
		;
connectAttr "Character1_LeftHandRing1_rotateX.o" "Character1_LeftHandRing1.rx";
connectAttr "Character1_LeftHandRing1_rotateY.o" "Character1_LeftHandRing1.ry";
connectAttr "Character1_LeftHandRing1_rotateZ.o" "Character1_LeftHandRing1.rz";
connectAttr "Character1_LeftHandRing1_visibility.o" "Character1_LeftHandRing1.v"
		;
connectAttr "Character1_LeftHandRing1.s" "Character1_LeftHandRing2.is";
connectAttr "Character1_LeftHandRing2_scaleX.o" "Character1_LeftHandRing2.sx";
connectAttr "Character1_LeftHandRing2_scaleY.o" "Character1_LeftHandRing2.sy";
connectAttr "Character1_LeftHandRing2_scaleZ.o" "Character1_LeftHandRing2.sz";
connectAttr "Character1_LeftHandRing2_translateX.o" "Character1_LeftHandRing2.tx"
		;
connectAttr "Character1_LeftHandRing2_translateY.o" "Character1_LeftHandRing2.ty"
		;
connectAttr "Character1_LeftHandRing2_translateZ.o" "Character1_LeftHandRing2.tz"
		;
connectAttr "Character1_LeftHandRing2_rotateX.o" "Character1_LeftHandRing2.rx";
connectAttr "Character1_LeftHandRing2_rotateY.o" "Character1_LeftHandRing2.ry";
connectAttr "Character1_LeftHandRing2_rotateZ.o" "Character1_LeftHandRing2.rz";
connectAttr "Character1_LeftHandRing2_visibility.o" "Character1_LeftHandRing2.v"
		;
connectAttr "Character1_LeftHandRing2.s" "Character1_LeftHandRing3.is";
connectAttr "Character1_LeftHandRing3_translateX.o" "Character1_LeftHandRing3.tx"
		;
connectAttr "Character1_LeftHandRing3_translateY.o" "Character1_LeftHandRing3.ty"
		;
connectAttr "Character1_LeftHandRing3_translateZ.o" "Character1_LeftHandRing3.tz"
		;
connectAttr "Character1_LeftHandRing3_scaleX.o" "Character1_LeftHandRing3.sx";
connectAttr "Character1_LeftHandRing3_scaleY.o" "Character1_LeftHandRing3.sy";
connectAttr "Character1_LeftHandRing3_scaleZ.o" "Character1_LeftHandRing3.sz";
connectAttr "Character1_LeftHandRing3_rotateX.o" "Character1_LeftHandRing3.rx";
connectAttr "Character1_LeftHandRing3_rotateY.o" "Character1_LeftHandRing3.ry";
connectAttr "Character1_LeftHandRing3_rotateZ.o" "Character1_LeftHandRing3.rz";
connectAttr "Character1_LeftHandRing3_visibility.o" "Character1_LeftHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_RightShoulder.is";
connectAttr "Character1_RightShoulder_scaleX.o" "Character1_RightShoulder.sx";
connectAttr "Character1_RightShoulder_scaleY.o" "Character1_RightShoulder.sy";
connectAttr "Character1_RightShoulder_scaleZ.o" "Character1_RightShoulder.sz";
connectAttr "Character1_RightShoulder_translateX.o" "Character1_RightShoulder.tx"
		;
connectAttr "Character1_RightShoulder_translateY.o" "Character1_RightShoulder.ty"
		;
connectAttr "Character1_RightShoulder_translateZ.o" "Character1_RightShoulder.tz"
		;
connectAttr "Character1_RightShoulder_rotateX.o" "Character1_RightShoulder.rx";
connectAttr "Character1_RightShoulder_rotateY.o" "Character1_RightShoulder.ry";
connectAttr "Character1_RightShoulder_rotateZ.o" "Character1_RightShoulder.rz";
connectAttr "Character1_RightShoulder_visibility.o" "Character1_RightShoulder.v"
		;
connectAttr "Character1_RightShoulder.s" "Character1_RightArm.is";
connectAttr "Character1_RightArm_scaleX.o" "Character1_RightArm.sx";
connectAttr "Character1_RightArm_scaleY.o" "Character1_RightArm.sy";
connectAttr "Character1_RightArm_scaleZ.o" "Character1_RightArm.sz";
connectAttr "Character1_RightArm_translateX.o" "Character1_RightArm.tx";
connectAttr "Character1_RightArm_translateY.o" "Character1_RightArm.ty";
connectAttr "Character1_RightArm_translateZ.o" "Character1_RightArm.tz";
connectAttr "Character1_RightArm_rotateX.o" "Character1_RightArm.rx";
connectAttr "Character1_RightArm_rotateY.o" "Character1_RightArm.ry";
connectAttr "Character1_RightArm_rotateZ.o" "Character1_RightArm.rz";
connectAttr "Character1_RightArm_visibility.o" "Character1_RightArm.v";
connectAttr "Character1_RightArm.s" "Character1_RightForeArm.is";
connectAttr "Character1_RightForeArm_scaleX.o" "Character1_RightForeArm.sx";
connectAttr "Character1_RightForeArm_scaleY.o" "Character1_RightForeArm.sy";
connectAttr "Character1_RightForeArm_scaleZ.o" "Character1_RightForeArm.sz";
connectAttr "Character1_RightForeArm_translateX.o" "Character1_RightForeArm.tx";
connectAttr "Character1_RightForeArm_translateY.o" "Character1_RightForeArm.ty";
connectAttr "Character1_RightForeArm_translateZ.o" "Character1_RightForeArm.tz";
connectAttr "Character1_RightForeArm_rotateX.o" "Character1_RightForeArm.rx";
connectAttr "Character1_RightForeArm_rotateY.o" "Character1_RightForeArm.ry";
connectAttr "Character1_RightForeArm_rotateZ.o" "Character1_RightForeArm.rz";
connectAttr "Character1_RightForeArm_visibility.o" "Character1_RightForeArm.v";
connectAttr "Character1_RightForeArm.s" "Character1_RightHand.is";
connectAttr "Character1_RightHand_scaleX.o" "Character1_RightHand.sx";
connectAttr "Character1_RightHand_scaleY.o" "Character1_RightHand.sy";
connectAttr "Character1_RightHand_scaleZ.o" "Character1_RightHand.sz";
connectAttr "Character1_RightHand_translateX.o" "Character1_RightHand.tx";
connectAttr "Character1_RightHand_translateY.o" "Character1_RightHand.ty";
connectAttr "Character1_RightHand_translateZ.o" "Character1_RightHand.tz";
connectAttr "Character1_RightHand_rotateX.o" "Character1_RightHand.rx";
connectAttr "Character1_RightHand_rotateY.o" "Character1_RightHand.ry";
connectAttr "Character1_RightHand_rotateZ.o" "Character1_RightHand.rz";
connectAttr "Character1_RightHand_visibility.o" "Character1_RightHand.v";
connectAttr "Character1_RightHand.s" "Character1_RightHandThumb1.is";
connectAttr "Character1_RightHandThumb1_scaleX.o" "Character1_RightHandThumb1.sx"
		;
connectAttr "Character1_RightHandThumb1_scaleY.o" "Character1_RightHandThumb1.sy"
		;
connectAttr "Character1_RightHandThumb1_scaleZ.o" "Character1_RightHandThumb1.sz"
		;
connectAttr "Character1_RightHandThumb1_translateX.o" "Character1_RightHandThumb1.tx"
		;
connectAttr "Character1_RightHandThumb1_translateY.o" "Character1_RightHandThumb1.ty"
		;
connectAttr "Character1_RightHandThumb1_translateZ.o" "Character1_RightHandThumb1.tz"
		;
connectAttr "Character1_RightHandThumb1_rotateX.o" "Character1_RightHandThumb1.rx"
		;
connectAttr "Character1_RightHandThumb1_rotateY.o" "Character1_RightHandThumb1.ry"
		;
connectAttr "Character1_RightHandThumb1_rotateZ.o" "Character1_RightHandThumb1.rz"
		;
connectAttr "Character1_RightHandThumb1_visibility.o" "Character1_RightHandThumb1.v"
		;
connectAttr "Character1_RightHandThumb1.s" "Character1_RightHandThumb2.is";
connectAttr "Character1_RightHandThumb2_scaleX.o" "Character1_RightHandThumb2.sx"
		;
connectAttr "Character1_RightHandThumb2_scaleY.o" "Character1_RightHandThumb2.sy"
		;
connectAttr "Character1_RightHandThumb2_scaleZ.o" "Character1_RightHandThumb2.sz"
		;
connectAttr "Character1_RightHandThumb2_translateX.o" "Character1_RightHandThumb2.tx"
		;
connectAttr "Character1_RightHandThumb2_translateY.o" "Character1_RightHandThumb2.ty"
		;
connectAttr "Character1_RightHandThumb2_translateZ.o" "Character1_RightHandThumb2.tz"
		;
connectAttr "Character1_RightHandThumb2_rotateX.o" "Character1_RightHandThumb2.rx"
		;
connectAttr "Character1_RightHandThumb2_rotateY.o" "Character1_RightHandThumb2.ry"
		;
connectAttr "Character1_RightHandThumb2_rotateZ.o" "Character1_RightHandThumb2.rz"
		;
connectAttr "Character1_RightHandThumb2_visibility.o" "Character1_RightHandThumb2.v"
		;
connectAttr "Character1_RightHandThumb2.s" "Character1_RightHandThumb3.is";
connectAttr "Character1_RightHandThumb3_translateX.o" "Character1_RightHandThumb3.tx"
		;
connectAttr "Character1_RightHandThumb3_translateY.o" "Character1_RightHandThumb3.ty"
		;
connectAttr "Character1_RightHandThumb3_translateZ.o" "Character1_RightHandThumb3.tz"
		;
connectAttr "Character1_RightHandThumb3_scaleX.o" "Character1_RightHandThumb3.sx"
		;
connectAttr "Character1_RightHandThumb3_scaleY.o" "Character1_RightHandThumb3.sy"
		;
connectAttr "Character1_RightHandThumb3_scaleZ.o" "Character1_RightHandThumb3.sz"
		;
connectAttr "Character1_RightHandThumb3_rotateX.o" "Character1_RightHandThumb3.rx"
		;
connectAttr "Character1_RightHandThumb3_rotateY.o" "Character1_RightHandThumb3.ry"
		;
connectAttr "Character1_RightHandThumb3_rotateZ.o" "Character1_RightHandThumb3.rz"
		;
connectAttr "Character1_RightHandThumb3_visibility.o" "Character1_RightHandThumb3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandIndex1.is";
connectAttr "Character1_RightHandIndex1_scaleX.o" "Character1_RightHandIndex1.sx"
		;
connectAttr "Character1_RightHandIndex1_scaleY.o" "Character1_RightHandIndex1.sy"
		;
connectAttr "Character1_RightHandIndex1_scaleZ.o" "Character1_RightHandIndex1.sz"
		;
connectAttr "Character1_RightHandIndex1_translateX.o" "Character1_RightHandIndex1.tx"
		;
connectAttr "Character1_RightHandIndex1_translateY.o" "Character1_RightHandIndex1.ty"
		;
connectAttr "Character1_RightHandIndex1_translateZ.o" "Character1_RightHandIndex1.tz"
		;
connectAttr "Character1_RightHandIndex1_rotateX.o" "Character1_RightHandIndex1.rx"
		;
connectAttr "Character1_RightHandIndex1_rotateY.o" "Character1_RightHandIndex1.ry"
		;
connectAttr "Character1_RightHandIndex1_rotateZ.o" "Character1_RightHandIndex1.rz"
		;
connectAttr "Character1_RightHandIndex1_visibility.o" "Character1_RightHandIndex1.v"
		;
connectAttr "Character1_RightHandIndex1.s" "Character1_RightHandIndex2.is";
connectAttr "Character1_RightHandIndex2_scaleX.o" "Character1_RightHandIndex2.sx"
		;
connectAttr "Character1_RightHandIndex2_scaleY.o" "Character1_RightHandIndex2.sy"
		;
connectAttr "Character1_RightHandIndex2_scaleZ.o" "Character1_RightHandIndex2.sz"
		;
connectAttr "Character1_RightHandIndex2_translateX.o" "Character1_RightHandIndex2.tx"
		;
connectAttr "Character1_RightHandIndex2_translateY.o" "Character1_RightHandIndex2.ty"
		;
connectAttr "Character1_RightHandIndex2_translateZ.o" "Character1_RightHandIndex2.tz"
		;
connectAttr "Character1_RightHandIndex2_rotateX.o" "Character1_RightHandIndex2.rx"
		;
connectAttr "Character1_RightHandIndex2_rotateY.o" "Character1_RightHandIndex2.ry"
		;
connectAttr "Character1_RightHandIndex2_rotateZ.o" "Character1_RightHandIndex2.rz"
		;
connectAttr "Character1_RightHandIndex2_visibility.o" "Character1_RightHandIndex2.v"
		;
connectAttr "Character1_RightHandIndex2.s" "Character1_RightHandIndex3.is";
connectAttr "Character1_RightHandIndex3_translateX.o" "Character1_RightHandIndex3.tx"
		;
connectAttr "Character1_RightHandIndex3_translateY.o" "Character1_RightHandIndex3.ty"
		;
connectAttr "Character1_RightHandIndex3_translateZ.o" "Character1_RightHandIndex3.tz"
		;
connectAttr "Character1_RightHandIndex3_scaleX.o" "Character1_RightHandIndex3.sx"
		;
connectAttr "Character1_RightHandIndex3_scaleY.o" "Character1_RightHandIndex3.sy"
		;
connectAttr "Character1_RightHandIndex3_scaleZ.o" "Character1_RightHandIndex3.sz"
		;
connectAttr "Character1_RightHandIndex3_rotateX.o" "Character1_RightHandIndex3.rx"
		;
connectAttr "Character1_RightHandIndex3_rotateY.o" "Character1_RightHandIndex3.ry"
		;
connectAttr "Character1_RightHandIndex3_rotateZ.o" "Character1_RightHandIndex3.rz"
		;
connectAttr "Character1_RightHandIndex3_visibility.o" "Character1_RightHandIndex3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandRing1.is";
connectAttr "Character1_RightHandRing1_scaleX.o" "Character1_RightHandRing1.sx";
connectAttr "Character1_RightHandRing1_scaleY.o" "Character1_RightHandRing1.sy";
connectAttr "Character1_RightHandRing1_scaleZ.o" "Character1_RightHandRing1.sz";
connectAttr "Character1_RightHandRing1_translateX.o" "Character1_RightHandRing1.tx"
		;
connectAttr "Character1_RightHandRing1_translateY.o" "Character1_RightHandRing1.ty"
		;
connectAttr "Character1_RightHandRing1_translateZ.o" "Character1_RightHandRing1.tz"
		;
connectAttr "Character1_RightHandRing1_rotateX.o" "Character1_RightHandRing1.rx"
		;
connectAttr "Character1_RightHandRing1_rotateY.o" "Character1_RightHandRing1.ry"
		;
connectAttr "Character1_RightHandRing1_rotateZ.o" "Character1_RightHandRing1.rz"
		;
connectAttr "Character1_RightHandRing1_visibility.o" "Character1_RightHandRing1.v"
		;
connectAttr "Character1_RightHandRing1.s" "Character1_RightHandRing2.is";
connectAttr "Character1_RightHandRing2_scaleX.o" "Character1_RightHandRing2.sx";
connectAttr "Character1_RightHandRing2_scaleY.o" "Character1_RightHandRing2.sy";
connectAttr "Character1_RightHandRing2_scaleZ.o" "Character1_RightHandRing2.sz";
connectAttr "Character1_RightHandRing2_translateX.o" "Character1_RightHandRing2.tx"
		;
connectAttr "Character1_RightHandRing2_translateY.o" "Character1_RightHandRing2.ty"
		;
connectAttr "Character1_RightHandRing2_translateZ.o" "Character1_RightHandRing2.tz"
		;
connectAttr "Character1_RightHandRing2_rotateX.o" "Character1_RightHandRing2.rx"
		;
connectAttr "Character1_RightHandRing2_rotateY.o" "Character1_RightHandRing2.ry"
		;
connectAttr "Character1_RightHandRing2_rotateZ.o" "Character1_RightHandRing2.rz"
		;
connectAttr "Character1_RightHandRing2_visibility.o" "Character1_RightHandRing2.v"
		;
connectAttr "Character1_RightHandRing2.s" "Character1_RightHandRing3.is";
connectAttr "Character1_RightHandRing3_translateX.o" "Character1_RightHandRing3.tx"
		;
connectAttr "Character1_RightHandRing3_translateY.o" "Character1_RightHandRing3.ty"
		;
connectAttr "Character1_RightHandRing3_translateZ.o" "Character1_RightHandRing3.tz"
		;
connectAttr "Character1_RightHandRing3_scaleX.o" "Character1_RightHandRing3.sx";
connectAttr "Character1_RightHandRing3_scaleY.o" "Character1_RightHandRing3.sy";
connectAttr "Character1_RightHandRing3_scaleZ.o" "Character1_RightHandRing3.sz";
connectAttr "Character1_RightHandRing3_rotateX.o" "Character1_RightHandRing3.rx"
		;
connectAttr "Character1_RightHandRing3_rotateY.o" "Character1_RightHandRing3.ry"
		;
connectAttr "Character1_RightHandRing3_rotateZ.o" "Character1_RightHandRing3.rz"
		;
connectAttr "Character1_RightHandRing3_visibility.o" "Character1_RightHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_Neck.is";
connectAttr "Character1_Neck_scaleX.o" "Character1_Neck.sx";
connectAttr "Character1_Neck_scaleY.o" "Character1_Neck.sy";
connectAttr "Character1_Neck_scaleZ.o" "Character1_Neck.sz";
connectAttr "Character1_Neck_translateX.o" "Character1_Neck.tx";
connectAttr "Character1_Neck_translateY.o" "Character1_Neck.ty";
connectAttr "Character1_Neck_translateZ.o" "Character1_Neck.tz";
connectAttr "Character1_Neck_rotateX.o" "Character1_Neck.rx";
connectAttr "Character1_Neck_rotateY.o" "Character1_Neck.ry";
connectAttr "Character1_Neck_rotateZ.o" "Character1_Neck.rz";
connectAttr "Character1_Neck_visibility.o" "Character1_Neck.v";
connectAttr "Character1_Neck.s" "Character1_Head.is";
connectAttr "Character1_Head_scaleX.o" "Character1_Head.sx";
connectAttr "Character1_Head_scaleY.o" "Character1_Head.sy";
connectAttr "Character1_Head_scaleZ.o" "Character1_Head.sz";
connectAttr "Character1_Head_translateX.o" "Character1_Head.tx";
connectAttr "Character1_Head_translateY.o" "Character1_Head.ty";
connectAttr "Character1_Head_translateZ.o" "Character1_Head.tz";
connectAttr "Character1_Head_rotateX.o" "Character1_Head.rx";
connectAttr "Character1_Head_rotateY.o" "Character1_Head.ry";
connectAttr "Character1_Head_rotateZ.o" "Character1_Head.rz";
connectAttr "Character1_Head_visibility.o" "Character1_Head.v";
connectAttr "Character1_Head.s" "eyes.is";
connectAttr "eyes_translateX.o" "eyes.tx";
connectAttr "eyes_translateY.o" "eyes.ty";
connectAttr "eyes_translateZ.o" "eyes.tz";
connectAttr "eyes_scaleX.o" "eyes.sx";
connectAttr "eyes_scaleY.o" "eyes.sy";
connectAttr "eyes_scaleZ.o" "eyes.sz";
connectAttr "eyes_rotateX.o" "eyes.rx";
connectAttr "eyes_rotateY.o" "eyes.ry";
connectAttr "eyes_rotateZ.o" "eyes.rz";
connectAttr "eyes_visibility.o" "eyes.v";
connectAttr "Character1_Head.s" "jaw.is";
connectAttr "jaw_lockInfluenceWeights.o" "jaw.liw";
connectAttr "jaw_translateX.o" "jaw.tx";
connectAttr "jaw_translateY.o" "jaw.ty";
connectAttr "jaw_translateZ.o" "jaw.tz";
connectAttr "jaw_scaleX.o" "jaw.sx";
connectAttr "jaw_scaleY.o" "jaw.sy";
connectAttr "jaw_scaleZ.o" "jaw.sz";
connectAttr "jaw_rotateX.o" "jaw.rx";
connectAttr "jaw_rotateY.o" "jaw.ry";
connectAttr "jaw_rotateZ.o" "jaw.rz";
connectAttr "jaw_visibility.o" "jaw.v";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Emeny@Archer_Damage_Right.ma
