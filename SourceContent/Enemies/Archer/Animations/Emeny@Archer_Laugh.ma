//Maya ASCII 2016 scene
//Name: Emeny@Archer_Laugh.ma
//Last modified: Tue, Nov 24, 2015 12:20:00 PM
//Codeset: 1252
requires maya "2016";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "0CC2A4FD-47C8-B741-2263-108FD3820828";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 263.84799962262946 239.07605937898757 230.48114565676266 ;
	setAttr ".r" -type "double3" -27.938352729602379 44.999999999999972 -5.172681101354183e-014 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "29CDA482-4834-10CE-FEAA-DD86DE63EECC";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 420.81719163000849;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "B2F5C869-4A73-ADD2-C6E7-38B4D8014001";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "69EF65AD-420B-85EC-376A-26832D6BF83D";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "5E6AD9A6-4CBD-8467-FD3D-55B0FDED3491";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "6DDC6904-497F-5D86-DAB1-19AC56D706E1";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "EE85D8F2-40B0-5E21-0112-178583C1D1D6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "33ABF877-4F33-9187-E0E5-3CAC2D77CE16";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "ref_frame";
	rename -uid "D23895C4-4C90-7D39-02F6-2D81C8E54460";
	addAttr -ci true -sn "refFrame" -ln "refFrame" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "Character1_Hips" -p "ref_frame";
	rename -uid "41414F12-4182-469A-B0EC-638CA564B3FF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -19.036789801921167 ;
	setAttr ".ssc" no;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftUpLeg" -p "Character1_Hips";
	rename -uid "C8EC888D-449D-1D6D-0991-CDBF45A9D583";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 90.000000000000014 -12.849604412773132 43.257528613448208 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftLeg" -p "Character1_LeftUpLeg";
	rename -uid "06CF4730-4D91-6674-3033-94AE50C0C499";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -19.152297252867996 -13.416343813706657 -33.623675304480329 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFoot" -p "Character1_LeftLeg";
	rename -uid "53D78DDB-4D3D-00F8-2715-33AB9F3E9961";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 25.817647449536953 51.040150860538553 -112.05775014674906 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftToeBase" -p "Character1_LeftFoot";
	rename -uid "34AED20D-409F-B635-26EA-3B8C4B487BFF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 43.727116820142825 -29.659481123592514 -4.6723850284209263 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle1" -p "Character1_LeftToeBase";
	rename -uid "AAC9CF6F-4334-D9D8-1AA6-D1B084058220";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 165.43734692409092 -20.35908719617909 10.197796850486212 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle2" -p "Character1_LeftFootMiddle1";
	rename -uid "F5AD29D9-4553-380D-7917-3EAA610DAF72";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 127.90699782306312 -44.626258206212555 -101.65660999147298 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightUpLeg" -p "Character1_Hips";
	rename -uid "75C2394A-4A35-1DE5-BA91-3292B7BB1BEE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 133.25752861344816 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightLeg" -p "Character1_RightUpLeg";
	rename -uid "722BC478-42D3-3E77-3F69-6C81714519A0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -39.772585840344547 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFoot" -p "Character1_RightLeg";
	rename -uid "16794235-4575-354B-149B-328BE1059B41";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 -53.712356932759 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightToeBase" -p "Character1_RightFoot";
	rename -uid "90AB10C7-4268-11F2-4476-AFAC4447506B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 2.9245623946004821e-006 53.712356932758929 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle1" -p "Character1_RightToeBase";
	rename -uid "C012D222-4B19-5967-BC8B-769B662A220E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -4.5380835579220506e-006 -2.7209092876563258e-005 
		39.772585840347155 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle2" -p "Character1_RightFootMiddle1";
	rename -uid "8DD469CD-48B4-B0A2-11F2-2887D565DA10";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -2.8539668357113417e-005 0.00011485155389716205 -133.25752861351003 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine" -p "Character1_Hips";
	rename -uid "791AB050-4A02-4CA4-602E-37A4AEB94DFE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 0 133.25752861344816 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine1" -p "Character1_Spine";
	rename -uid "9678D9FB-441F-E554-176A-2EA8055136B2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -34.34645244194413 -26.596597123422644 16.99576897690589 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftShoulder" -p "Character1_Spine1";
	rename -uid "548F142A-4A29-8DFE-B9F3-0580339F6671";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -166.39102833412767 21.832119962557094 -120.00622813437471 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftArm" -p "Character1_LeftShoulder";
	rename -uid "FFD41499-4D67-7737-44B6-4D9DE43FB533";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -172.11387269090633 -44.88241940154802 -37.940590556196071 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftForeArm" -p "Character1_LeftArm";
	rename -uid "1A854C11-44BE-056F-4908-3FB0A12BBCEA";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -84.022423618581243 -14.673598567163568 71.813326221989513 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHand" -p "Character1_LeftForeArm";
	rename -uid "B805FBEC-4BF9-FB36-FC54-ED8682B31ADB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -24.404340271578391 -34.932697344706668 122.26498801320882 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr -k on ".liw" no;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb1" -p "Character1_LeftHand";
	rename -uid "E9AFBDE8-45E8-0738-0053-8FB082931890";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.1043965330995 61.333484361561069 1.8740740862626108 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb2" -p "Character1_LeftHandThumb1";
	rename -uid "BFD56FB4-4454-AEDD-A9F3-3D8E806757C6";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -103.44292663098258 20.93424016453444 169.56041330531272 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb3" -p "Character1_LeftHandThumb2";
	rename -uid "51208F43-4C2F-8D1B-785A-9BB103D186CC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 14.174484134960574 -59.672502756712539 -19.020394805426264 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex1" -p "Character1_LeftHand";
	rename -uid "B4546653-4152-26FE-CCF4-128C8E0C5EA9";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -150.59621970507803 75.681392634287164 -124.28723884791442 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex2" -p "Character1_LeftHandIndex1";
	rename -uid "03D76C99-499C-0E0D-3ACE-6B9623FAD65A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -41.093804604175162 -57.602285575728224 -69.242456746392662 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex3" -p "Character1_LeftHandIndex2";
	rename -uid "F61E2A49-4021-12D8-9E33-C1BCDECAC395";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.108297963098213 8.7979835742630801 27.088028901385748 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing1" -p "Character1_LeftHand";
	rename -uid "0C13E643-4617-6B05-E814-928E5DAEB147";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -141.42437885137375 63.919742192600388 -113.78475086453638 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing2" -p "Character1_LeftHandRing1";
	rename -uid "5B20B2D4-4430-F9F8-19EF-21B353A549AD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -35.368646977348 -76.384823071466059 -48.755963146359043 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing3" -p "Character1_LeftHandRing2";
	rename -uid "0E52B651-4B8F-5C78-226F-B480794B530C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 9.6312888734270121 39.523687300631337 15.3258182753707 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightShoulder" -p "Character1_Spine1";
	rename -uid "ADAE0FC7-4032-4AFE-BF2A-89A92A43C7C4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -164.54352138244408 34.957040538466451 -116.14754006477595 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightArm" -p "Character1_RightShoulder";
	rename -uid "9E570632-400C-164F-ED2D-D58B1219E67B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -173.75456654457099 -11.164646449014084 -45.537881339438307 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightForeArm" -p "Character1_RightArm";
	rename -uid "6F9BC26C-4FE2-F7B0-F98C-BBBDE8BF2829";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 22.753833922449758 50.186429787105524 -101.53518723839815 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHand" -p "Character1_RightForeArm";
	rename -uid "B72645E7-48FD-417D-A4BA-CF95A261E71B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 48.334026046900341 -3.7701083025157853 -29.882905994214536 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb1" -p "Character1_RightHand";
	rename -uid "3E5B3D76-41BE-3FF6-BAB0-709882DB96A7";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb2" -p "Character1_RightHandThumb1";
	rename -uid "CF642CA1-429B-3B91-BEAB-DB8BA64A0DA1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb3" -p "Character1_RightHandThumb2";
	rename -uid "54E3E58F-4069-01E5-8856-26B1D43F91B4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex1" -p "Character1_RightHand";
	rename -uid "C3F2709D-4D19-9E8A-54D9-56A245C2CE25";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex2" -p "Character1_RightHandIndex1";
	rename -uid "C9481927-4963-9A43-720D-12B92D1899FC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex3" -p "Character1_RightHandIndex2";
	rename -uid "6701F6BC-4DB7-1365-1E31-06B003219715";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing1" -p "Character1_RightHand";
	rename -uid "F8868218-45CE-48A6-1507-B4BA8DB9865E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -75.157191454887879 -3.6857097796163734 105.41464019052114 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing2" -p "Character1_RightHandRing1";
	rename -uid "240593AD-4628-E268-4E4E-E2A841E758D4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -50.447779749131506 -26.025093743997171 79.323854890804469 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing3" -p "Character1_RightHandRing2";
	rename -uid "2D29B675-4DFD-87AC-AF70-50ACB30D847C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 0 42.415535166277309 -48.412944570106106 ;
	setAttr ".radi" 1.0188447817833224;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Neck" -p "Character1_Spine1";
	rename -uid "005CF9A3-46CC-6D87-6CFC-679BB9B441C1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 48.229963067601958 30.670012388143238 129.35864775594831 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Head" -p "Character1_Neck";
	rename -uid "EFAA9856-4BC2-3043-C14A-9698B5421A36";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -14.36476107543206 22.377462763663633 -109.58762475494268 ;
	setAttr ".radi" 3.0565343453499678;
	setAttr ".fbxID" 5;
createNode joint -n "eyes" -p "Character1_Head";
	rename -uid "930960EB-4C59-E7A3-8024-40BCDD9AEAE7";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -157.21319586594478 -21.144487689504771 -3.4677530572789341 ;
	setAttr ".radi" 4.5;
	setAttr ".fbxID" 5;
createNode joint -n "jaw" -p "Character1_Head";
	rename -uid "738339C5-4189-A45B-2C7E-449AB8DA741B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -157.21319586594478 -21.144487689504771 -3.4677530572789341 ;
	setAttr ".radi" 0.5;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode animCurveTU -n "Character1_LeftHand_lockInfluenceWeights";
	rename -uid "AA2277C9-4ABF-965A-A1CA-4BB8D80B7B21";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "jaw_lockInfluenceWeights";
	rename -uid "0E998374-412B-AE55-5F24-26A2AAC32FBA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "Character1_Hips_translateX";
	rename -uid "0F3CA00D-424C-0A24-C6CB-3EAE3416112D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -0.50418257713317871 1 0.068225733935832977
		 2 0.63542693853378296 3 0.94143623113632202 4 0.83175802230834961 5 0.47322967648506165
		 6 0.095984667539596558 7 -0.28241807222366333 8 -0.64723724126815796 9 -0.94584894180297852
		 10 -1.2811411619186401 11 -1.6195805072784424 12 -1.9260861873626709 13 -2.1612503528594971
		 14 -2.2860546112060547 15 -2.2617146968841553 16 -2.0677506923675537 17 -1.7365654706954956
		 18 -1.3083579540252686 19 -0.82293516397476196 20 -0.31968545913696289 21 0.16273194551467896
		 22 0.58733445405960083 23 0.91658467054367065 24 1.1115648746490479 25 1.1854281425476074
		 26 1.1893066167831421 27 1.1360932588577271 28 1.0348601341247559 29 0.89485514163970947
		 30 0.72769218683242798 31 0.54928696155548096 32 0.37530046701431274 33 0.22138899564743042
		 34 0.10327096283435822 35 0.034113552421331406 36 -0.00067465385654941201 37 -0.023883417248725891
		 38 -0.037699524313211441 39 -0.044498048722743988 40 -0.046345073729753494 41 -0.043854597955942154
		 42 -0.037251319736242294 43 -0.026860510930418968 44 -0.013181509450078011 45 0.0037438105791807175
		 46 0.022999865934252739 47 0.041836131364107132 48 0.057506944984197617 49 0.067271679639816284
		 50 0.070997297763824463 51 0.069702528417110443 52 0.064856864511966705 53 0.059159234166145325
		 54 0.055251289159059525 55 0.055635701864957809 56 0.060334686189889908 57 0.067583583295345306
		 58 0.077171653509140015 59 0.088898070156574249 60 0.10258540511131287 61 0.12437785416841507
		 62 0.15772469341754913 63 0.19822539389133453 64 0.24316710233688354 65 0.2855333685874939
		 66 0.31716704368591309 67 0.33370548486709595 68 0.33205601572990417 69 0.29707252979278564
		 70 0.22930131852626801 71 0.14254119992256165 72 0.049274735152721405 73 -0.0381004698574543
		 74 -0.10806401073932648 75 -0.14678260684013367 76 -0.17251500487327576 77 -0.22159969806671143
		 78 -0.31791073083877563 79 -0.44024962186813354 80 -0.56205260753631592;
createNode animCurveTL -n "Character1_Hips_translateY";
	rename -uid "B1F3A7EC-4197-3BF9-173C-1D8ECFA65167";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 42.001785278320312 1 38.305561065673828
		 2 34.637138366699219 3 32.6064453125 4 33.057022094726563 5 35.122596740722656 6 37.855377197265625
		 7 42.687339782714844 8 46.555011749267578 9 46.82293701171875 10 46.503463745117188
		 11 45.773181915283203 12 44.82232666015625 13 43.869338989257813 14 43.127216339111328
		 15 42.809047698974609 16 42.902545928955078 17 43.201332092285156 18 43.629451751708984
		 19 44.113311767578125 20 44.581546783447266 21 44.974136352539063 22 45.274917602539063
		 23 45.478061676025391 24 45.578212738037109 25 45.635169982910156 26 45.742172241210938
		 27 45.875919342041016 28 45.978931427001953 29 45.996440887451172 30 45.910785675048828
		 31 45.764163970947266 32 45.593723297119141 33 45.437862396240234 34 45.336151123046875
		 35 45.319202423095703 36 45.351585388183594 37 45.382076263427734 38 45.378566741943359
		 39 45.311042785644531 40 45.160774230957031 41 44.953117370605469 42 44.725063323974609
		 43 44.515766143798828 44 44.366729736328125 45 44.293357849121094 46 44.274242401123047
		 47 44.292530059814453 48 44.331790924072266 49 44.376453399658203 50 44.418251037597656
		 51 44.439357757568359 52 44.444599151611328 53 44.458293914794922 54 44.505985260009766
		 55 44.614383697509766 56 44.80181884765625 57 45.038311004638672 58 45.278892517089844
		 59 45.480758666992188 60 45.603172302246094 61 45.724781036376953 62 45.915180206298828
		 63 46.120655059814453 64 46.282955169677734 65 46.345729827880859 66 46.312522888183594
		 67 46.014354705810547 68 45.387668609619141 69 44.066097259521484 70 42.017971038818359
		 71 39.652786254882812 72 37.358974456787109 73 35.520137786865234 74 34.521541595458984
		 75 35.776008605957031 76 38.81829833984375 77 41.196632385253906 78 41.829372406005859
		 79 41.768718719482422 80 41.707988739013672;
createNode animCurveTL -n "Character1_Hips_translateZ";
	rename -uid "09CFEA93-4E8E-1CC1-7BBC-019A80EDF3BD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -9.3845930099487305 1 -9.367732048034668
		 2 -9.3566102981567383 3 -8.8589601516723633 4 -7.4812784194946289 5 -5.6033492088317871
		 6 -3.8897066116333008 7 -2.4329404830932617 8 -1.7851889133453369 9 -2.1828010082244873
		 10 -2.8009445667266846 11 -3.5719563961029053 12 -4.4154362678527832 13 -5.2224516868591309
		 14 -5.8862295150756836 15 -6.3010759353637695 16 -6.4723868370056152 17 -6.5059542655944824
		 18 -6.4470047950744629 19 -6.3417959213256836 20 -6.2374444007873535 21 -6.1728153228759766
		 22 -6.1491146087646484 23 -6.1570043563842773 24 -6.1874852180480957 25 -6.1985769271850586
		 26 -6.1409091949462891 27 -6.0368585586547852 28 -5.9277105331420898 29 -5.8567643165588379
		 30 -5.8467140197753906 31 -5.8748164176940918 32 -5.9169635772705078 33 -5.9497084617614746
		 34 -5.9505023956298828 35 -5.9029116630554199 36 -5.8216028213500977 37 -5.7309832572937012
		 38 -5.6575832366943359 39 -5.6285929679870605 40 -5.662086009979248 41 -5.7390332221984863
		 42 -5.8311729431152344 43 -5.9110221862792969 44 -5.9520535469055176 45 -5.948822021484375
		 46 -5.9150943756103516 47 -5.8645486831665039 48 -5.811213493347168 49 -5.7695951461791992
		 50 -5.7535891532897949 51 -5.7773990631103516 52 -5.830897331237793 53 -5.8920459747314453
		 54 -5.9396371841430664 55 -5.9532179832458496 56 -5.9165153503417969 57 -5.8446621894836426
		 58 -5.7647209167480469 59 -5.7047333717346191 60 -5.6934957504272461 61 -5.7272019386291504
		 62 -5.7810831069946289 63 -5.8521842956542969 64 -5.9418601989746094 65 -6.0456790924072266
		 66 -6.1156854629516602 67 -6.233184814453125 68 -6.4125590324401855 69 -6.691490650177002
		 70 -7.0765094757080078 71 -7.5271263122558594 72 -8.0028505325317383 73 -8.4632863998413086
		 74 -8.8676376342773437 75 -9.2216510772705078 76 -9.5268049240112305 77 -9.7294578552246094
		 78 -9.7667903900146484 79 -9.6931390762329102 80 -9.6191625595092773;
createNode animCurveTU -n "Character1_Hips_scaleX";
	rename -uid "57EAD980-4236-67AC-A306-3EB0A5EED1B9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_Hips_scaleY";
	rename -uid "C5D0A7C8-44B9-5AED-B595-E68F275ABDC9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_Hips_scaleZ";
	rename -uid "A42C5690-4F95-6AD5-A0CC-04AFAF3BF9AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Hips_rotateX";
	rename -uid "6254C2F5-4E02-824C-0713-0EAC505D7884";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -6.9407939910888672 1 -5.452664852142334
		 2 -4.0583205223083496 3 -3.6377859115600586 4 -4.7209944725036621 5 -6.7044448852539063
		 6 -8.7215585708618164 7 -10.707505226135254 8 -12.304506301879883 9 -13.04421329498291
		 10 -13.420312881469727 11 -14.011345863342285 12 -15.088666915893555 13 -16.280389785766602
		 14 -17.212667465209961 15 -17.51068115234375 16 -16.784461975097656 17 -15.25029182434082
		 18 -13.441590309143066 19 -11.892444610595703 20 -11.136728286743164 21 -11.552425384521484
		 22 -12.805171012878418 23 -14.358836174011229 24 -15.675520896911621 25 -16.197059631347656
		 26 -15.446877479553223 27 -13.865714073181152 28 -12.26279354095459 29 -11.445266723632813
		 30 -11.821720123291016 31 -12.937427520751953 32 -14.302409172058105 33 -15.425969123840332
		 34 -15.817082405090332 35 -15.128054618835447 36 -13.690415382385254 37 -12.028060913085938
		 38 -10.660938262939453 39 -10.108633995056152 40 -10.71861457824707 41 -12.142173767089844
		 42 -13.853778839111328 43 -15.326985359191895 44 -16.035427093505859 45 -15.867021560668947
		 46 -15.080431938171388 47 -13.971074104309082 48 -12.834349632263184 49 -11.965137481689453
		 50 -11.659862518310547 51 -12.19527530670166 52 -13.353626251220703 53 -14.691396713256836
		 54 -15.764107704162598 55 -16.12664794921875 56 -15.432352066040039 57 -13.980613708496094
		 58 -12.293943405151367 59 -10.895061492919922 60 -10.306095123291016 61 -10.731230735778809
		 62 -11.831140518188477 63 -13.312374114990234 64 -14.882011413574217 65 -16.24382209777832
		 66 -16.225040435791016 67 -16.142217636108398 68 -15.978304862976076 69 -15.715893745422363
		 70 -15.372464179992678 71 -14.980635643005373 72 -14.568890571594237 73 -14.160258293151855
		 74 -13.771173477172852 75 -13.37208080291748 76 -12.949935913085938 77 -12.538922309875488
		 78 -12.148826599121094 79 -11.772242546081543 80 -11.412990570068359;
createNode animCurveTA -n "Character1_Hips_rotateY";
	rename -uid "5FF749D5-4AB5-0943-C6A8-77A4CB44E4C4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 11.395454406738281 1 8.9480266571044922
		 2 6.4719133377075195 3 4.5627255439758301 4 3.5650475025177002 5 3.156369686126709
		 6 2.9243338108062744 7 2.8113253116607666 8 2.8254103660583496 9 2.80810546875 10 2.7957642078399658
		 11 2.6179089546203613 12 2.2101316452026367 13 1.7243025302886963 14 1.3095029592514038
		 15 1.1117548942565918 16 1.2528203725814819 17 1.6391177177429199 18 2.0923516750335693
		 19 2.4312963485717773 20 2.473698616027832 21 2.0960378646850586 22 1.4356328248977661
		 23 0.70259732007980347 24 0.10648456960916519 25 -0.15539120137691498 26 0.055373053997755051
		 27 0.57107865810394287 28 1.1189764738082886 29 1.4202996492385864 30 1.3288774490356445
		 31 0.99400120973587036 32 0.57761597633361816 33 0.23919771611690521 34 0.13505785167217255
		 35 0.3914484977722168 36 0.913338303565979 37 1.5212416648864746 38 2.0238912105560303
		 39 2.2294337749481201 40 2.0099320411682129 41 1.4925500154495239 42 0.86912137269973755
		 43 0.33107107877731323 44 0.067474722862243652 45 0.1091609001159668 46 0.35118228197097778
		 47 0.69609743356704712 48 1.0451511144638062 49 1.2978355884552002 50 1.3501685857772827
		 51 1.1072450876235962 52 0.64593642950057983 53 0.11926072090864182 54 -0.32218879461288452
		 55 -0.53074151277542114 56 -0.37340658903121948 57 0.06058161333203315 58 0.58417832851409912
		 59 1.007962703704834 60 1.1418378353118896 61 0.86842918395996094 62 0.29194411635398865
		 63 -0.44502890110015869 64 -1.2027119398117065 65 -1.8355202674865723 66 -1.8837755918502808
		 67 -1.791834354400635 68 -1.5231324434280396 69 -1.0277941226959229 70 -0.32595491409301758
		 71 0.52777475118637085 72 1.4777462482452393 73 2.4651827812194824 74 3.4280781745910645
		 75 4.3929486274719238 76 5.4216618537902832 77 6.4882864952087402 78 7.5871710777282724
		 79 8.7185459136962891 80 9.8541259765625;
createNode animCurveTA -n "Character1_Hips_rotateZ";
	rename -uid "9308D81B-45C8-996F-1CD7-18A95A2E12FF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -7.7957358360290527 1 -6.1188755035400391
		 2 -4.5391073226928711 3 -3.3041644096374512 4 -2.4610936641693115 5 -1.8633675575256348
		 6 -1.3911898136138916 7 -1.0304927825927734 8 -0.83513790369033813 9 -0.79524904489517212
		 10 -0.85453158617019653 11 -0.93302148580551147 12 -0.9722309112548827 13 -0.96707147359848022
		 14 -0.92028653621673584 15 -0.84058159589767456 16 -0.72802519798278809 17 -0.56287211179733276
		 18 -0.32976537942886353 19 -0.032601870596408844 20 0.30508214235305786 21 0.65572386980056763
		 22 0.99662882089614879 23 1.3047914505004883 24 1.5432535409927368 25 1.686001181602478
		 26 1.6943459510803223 27 1.6008585691452026 28 1.4920514822006226 29 1.4266699552536011
		 30 1.4274739027023315 31 1.473604679107666 32 1.5443328619003296 33 1.6068804264068604
		 34 1.6162800788879395 35 1.5702006816864014 36 1.5122346878051758 37 1.4616520404815674
		 38 1.4318400621414185 39 1.4196435213088989 40 1.4211608171463013 41 1.4422630071640015
		 42 1.4871358871459961 43 1.5422155857086182 44 1.5757527351379395 45 1.5552691221237183
		 46 1.4799686670303345 47 1.3831658363342285 48 1.2938719987869263 49 1.2337117195129395
		 50 1.2176816463470459 51 1.2612040042877197 52 1.3554002046585083 53 1.4744530916213989
		 54 1.5807856321334839 55 1.6265877485275269 56 1.6030522584915161 57 1.5592805147171021
		 58 1.5278763771057129 59 1.5235590934753418 60 1.5438076257705688 61 1.627336859703064
		 62 1.7940630912780762 63 2.0109903812408447 64 2.2375354766845703 65 2.4205887317657471
		 66 2.450312614440918 67 2.3538897037506104 68 2.0971932411193848 69 1.6487542390823364
		 70 1.0336943864822388 71 0.29667264223098755 72 -0.51449108123779297 73 -1.3515734672546387
		 74 -2.1669948101043701 75 -3.0044617652893066 76 -3.9119613170623779 77 -4.8368210792541504
		 78 -5.7460532188415527 79 -6.6554780006408691 80 -7.5587649345397949;
createNode animCurveTU -n "Character1_Hips_visibility";
	rename -uid "BB7C7A1A-4C70-8723-C276-EBB5CD47C344";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftUpLeg_translateX";
	rename -uid "83AF743F-415A-310E-8605-399BF2C428E4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.784505844116211;
createNode animCurveTL -n "Character1_LeftUpLeg_translateY";
	rename -uid "5258274C-4834-A120-1083-A39E134E302C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5774786561451037e-006;
createNode animCurveTL -n "Character1_LeftUpLeg_translateZ";
	rename -uid "EB9EF548-4312-63D8-9735-9DB27C79F86A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0264549255371094;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleX";
	rename -uid "C4C3A3CC-4EB2-37F9-A50A-BC9352615ECF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleY";
	rename -uid "F37F28D8-4DBC-6003-6FF8-6AAC6674F74E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleZ";
	rename -uid "020CDF0C-4F40-DAE4-E8A0-BD95D7F95F71";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateX";
	rename -uid "FEDDFE2F-4249-6EA7-8F71-5C812950E1C8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -4.9229340553283691 1 -7.8888087272644034
		 2 -9.446558952331543 3 -7.2001996040344238 4 -0.62622612714767456 5 7.0395431518554687
		 6 13.303971290588379 7 22.030681610107422 8 33.374855041503906 9 34.338787078857422
		 10 32.874717712402344 11 30.751897811889648 12 29.173038482666019 13 28.154533386230469
		 14 27.505718231201172 15 27.026792526245117 16 26.195487976074219 17 24.896308898925781
		 18 23.599191665649414 19 22.784833908081055 20 22.930740356445313 21 24.342996597290039
		 22 26.594993591308594 23 29.045780181884762 24 31.046293258666996 25 32.112442016601563
		 26 31.950593948364261 27 31.004739761352539 28 30.03941535949707 29 29.771457672119137
		 30 30.537593841552734 31 31.931848526000977 32 33.498222351074219 33 34.767990112304688
		 34 35.2564697265625 35 34.622310638427734 36 33.152004241943359 37 31.33106803894043
		 38 29.685535430908203 39 28.752168655395508 40 28.901393890380859 41 29.819475173950199
		 42 31.012639999389648 43 31.987548828125 44 32.259258270263672 45 31.747014999389645
		 46 30.690763473510742 47 29.39271354675293 48 28.150745391845703 49 27.256490707397461
		 50 27.005678176879883 51 27.635953903198242 52 28.928417205810547 53 30.47087287902832
		 54 31.828073501586914 55 32.550075531005859 56 32.27655029296875 57 31.260501861572262
		 58 29.970832824707028 59 28.884523391723636 60 28.482297897338867 61 29.413898468017582
		 62 31.632785797119144 63 34.579536437988281 64 37.567615509033203 65 39.659610748291016
		 66 38.924808502197266 67 36.428386688232422 68 32.641605377197266 69 27.179004669189453
		 70 20.9727783203125 71 15.140420913696289 72 10.028712272644043 73 5.6714177131652832
		 74 2.2308976650238037 75 1.0599722862243652 76 2.4821896553039551 77 3.9647297859191895
		 78 2.971721887588501 79 0.85989445447921753 80 -1.2423005104064941;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateY";
	rename -uid "969AEBD1-4A79-9778-02D0-26BF45AA0641";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 21.506786346435547 1 24.182928085327148
		 2 28.716096878051758 3 31.9482421875 4 31.230424880981449 5 27.101882934570313 6 20.440071105957031
		 7 11.421570777893066 8 3.1683144569396973 9 2.0564432144165039 10 2.9788625240325928
		 11 4.8225016593933105 12 6.8110785484313965 13 8.5816488265991211 14 9.8449554443359375
		 15 10.321551322937012 16 10.187485694885254 17 9.7776756286621094 18 9.0671043395996094
		 19 8.0597019195556641 20 6.7799901962280273 21 5.2717270851135254 22 3.6856873035430908
		 23 2.2314062118530273 24 1.1595972776412964 25 0.54859358072280884 26 0.36254173517227173
		 27 0.47773247957229614 28 0.68117231130599976 29 0.7946966290473938 30 0.71500867605209351
		 31 0.51435893774032593 32 0.28529700636863708 33 0.12915180623531342 34 0.1577581912279129
		 35 0.42384940385818481 36 0.85454463958740234 37 1.3611725568771362 38 1.8251440525054929
		 39 2.1438448429107666 40 2.2510762214660645 41 2.1986160278320312 42 2.0691008567810059
		 43 1.9579358100891113 44 1.9746989011764524 45 2.1500706672668457 46 2.4457213878631592
		 47 2.7830460071563721 48 3.0861618518829346 49 3.2877907752990723 50 3.3237864971160889
		 51 3.1497354507446289 52 2.8142387866973877 53 2.4035594463348389 54 2.0222265720367432
		 55 1.7774472236633301 56 1.745319128036499 57 1.8555794954299927 58 2.0067105293273926
		 59 2.1107778549194336 60 2.091630220413208 61 1.6841813325881958 62 0.79499828815460205
		 63 -0.39568442106246948 64 -1.630570650100708 65 -2.4897756576538086 66 -2.2396008968353271
		 67 -1.0847374200820923 68 0.72701388597488403 69 3.237102746963501 70 6.0202302932739258
		 71 8.8230171203613281 72 11.617166519165039 73 14.276777267456055 74 16.44477653503418
		 75 17.109319686889648 76 16.565345764160156 77 16.430963516235352 78 17.626932144165039
		 79 19.367809295654297 80 21.043306350708008;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateZ";
	rename -uid "FA28F412-40AE-B63C-8EFB-919254CC232A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -3.5466642379760742 1 -18.525405883789063
		 2 -32.074382781982422 3 -35.542606353759766 4 -28.122434616088867 5 -16.659364700317383
		 6 -5.0146489143371582 7 5.7116317749023438 8 13.650444984436035 9 15.462080955505371
		 10 15.305973052978516 11 14.079276084899902 12 12.506936073303223 13 10.906413078308105
		 14 9.5064678192138672 15 8.5271549224853516 16 7.6004247665405265 17 6.3768525123596191
		 18 5.1492142677307129 19 4.191986083984375 20 3.7315747737884521 21 3.8725883960723877
		 22 4.4010868072509766 23 5.0449132919311523 24 5.5676651000976563 25 5.808596134185791
		 26 5.6783089637756348 27 5.3130064010620117 28 4.9559173583984375 29 4.8730020523071289
		 30 5.1919369697570801 31 5.7524070739746094 32 6.3745307922363281 33 6.884455680847168
		 34 7.1157493591308594 35 6.9368829727172852 36 6.4481749534606934 37 5.8375029563903809
		 38 5.2894101142883301 39 4.9982619285583496 40 5.106621265411377 41 5.5085086822509766
		 42 6.0252408981323242 43 6.4674978256225586 44 6.6372365951538086 45 6.498016357421875
		 46 6.1443266868591309 47 5.6801915168762207 48 5.2160730361938477 49 4.8696136474609375
		 50 4.7637009620666504 51 4.9969577789306641 52 5.4747037887573242 53 6.0239782333374023
		 54 6.4801383018493652 55 6.6872310638427734 56 6.5198249816894531 57 6.0812358856201172
		 58 5.5594987869262695 59 5.1313533782958984 60 4.9649677276611328 61 5.1803812980651855
		 62 5.6903519630432129 63 6.348846435546875 64 6.9967350959777832 65 7.4667372703552246
		 66 7.3198542594909668 67 6.9646143913269043 68 6.4842252731323242 69 5.8706264495849609
		 70 5.1254854202270508 71 4.1804718971252441 72 3.0106275081634521 73 1.7498903274536133
		 74 0.58623099327087402 75 -0.22668410837650299 76 -0.16516740620136261 77 0.18693681061267853
		 78 -0.29028680920600891 79 -1.2046071290969849 80 -2.1936197280883789;
createNode animCurveTU -n "Character1_LeftUpLeg_visibility";
	rename -uid "55AC68D2-4AFF-E525-1E78-FFB06EC8C32A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftLeg_translateX";
	rename -uid "08791F99-4A5C-789F-60E8-60BAE745ABB3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.968876838684082;
createNode animCurveTL -n "Character1_LeftLeg_translateY";
	rename -uid "820C3D13-4CAD-EAA3-E38F-7BAF0C2A1FB6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.2585945129394531;
createNode animCurveTL -n "Character1_LeftLeg_translateZ";
	rename -uid "5E6F8313-499D-7921-814B-E9808CC4F8AD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 18.197568893432617;
createNode animCurveTU -n "Character1_LeftLeg_scaleX";
	rename -uid "EF3651FA-4216-6A57-F856-66B006DC8121";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_LeftLeg_scaleY";
	rename -uid "45759918-4E77-35E4-9C5C-C6BE8363CDFE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftLeg_scaleZ";
	rename -uid "0485CFD1-4937-2576-6A8C-BF97C719E8F5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftLeg_rotateX";
	rename -uid "21C8EB14-4FD2-E782-8B45-9F8832ACDBD0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 52.102336883544922 1 65.369949340820313
		 2 75.832565307617187 3 80.983833312988281 4 77.650894165039063 5 69.698570251464844
		 6 59.898303985595703 7 37.17156982421875 8 7.3512578010559073 9 4.1157774925231934
		 10 6.5776901245117188 11 12.01185417175293 12 18.188100814819336 13 23.679450988769531
		 14 27.606901168823242 15 29.313068389892578 16 29.270477294921879 17 28.507076263427734
		 18 27.157875061035156 19 25.353134155273438 20 23.237371444702148 21 20.96422004699707
		 22 18.691946029663086 23 16.640634536743164 24 15.092187881469728 25 13.924429893493652
		 26 12.772486686706543 27 11.59923267364502 28 10.440258026123047 29 9.4593057632446289
		 30 8.7704982757568359 31 8.2864494323730469 32 7.9563136100769043 33 7.7519574165344238
		 34 7.6794881820678702 35 7.7175145149230957 36 7.9235219955444345 37 8.3647279739379883
		 38 9.0314950942993164 39 9.9148397445678711 40 10.991034507751465 41 12.174063682556152
		 42 13.372781753540039 43 14.508522033691404 44 15.507808685302733 45 16.318487167358398
		 46 16.993692398071289 47 17.500152587890625 48 17.813596725463867 49 17.931188583374023
		 50 17.847322463989258 51 17.646755218505859 52 17.337924957275391 53 16.858343124389648
		 54 16.207435607910156 55 15.408670425415037 56 14.490357398986818 57 13.523736953735352
		 58 12.610065460205078 59 11.855575561523438 60 11.374072074890137 61 10.270586967468262
		 62 7.9426412582397461 63 4.9375090599060059 64 2.0389013290405273 65 0.46938753128051758
		 66 1.704898476600647 67 6.2790098190307617 68 13.504941940307617 69 24.643491744995117
		 70 38.094318389892578 71 51.082847595214844 72 62.154823303222663 73 70.438812255859375
		 74 75.156906127929688 75 72.049636840820312 76 61.625377655029297 77 52.394420623779297
		 78 50.840667724609375 79 52.498470306396484 80 53.966159820556641;
createNode animCurveTA -n "Character1_LeftLeg_rotateY";
	rename -uid "76C3F355-4DDA-7A88-A68D-32A1C30EB37C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 21.536457061767578 1 7.0237622261047363
		 2 -9.8699455261230469 3 -17.681772232055664 4 -13.741401672363281 5 -3.8157289028167725
		 6 8.2389430999755859 7 19.212736129760742 8 16.230321884155273 9 15.392215728759766
		 10 16.880191802978516 11 18.992485046386719 12 20.346353530883789 13 20.880613327026367
		 14 20.968961715698242 15 20.988447189331055 16 21.056552886962891 17 21.090175628662109
		 18 21.025781631469727 19 20.806337356567383 20 20.393661499023438 21 19.795045852661133
		 22 19.102926254272461 23 18.424079895019531 24 17.877113342285156 25 17.477130889892578
		 26 17.154008865356445 27 16.900838851928711 28 16.669233322143555 29 16.422603607177734
		 30 16.180215835571289 31 15.941675186157228 32 15.724842071533203 33 15.566470146179199
		 34 15.520065307617188 35 15.587092399597168 36 15.771405220031737 37 16.08799934387207
		 38 16.490457534790039 39 16.921609878540039 40 17.326789855957031 41 17.689800262451172
		 42 18.018119812011719 43 18.328550338745117 44 18.640256881713867 45 18.964597702026367
		 46 19.320056915283203 47 19.663375854492188 48 19.953832626342773 49 20.149864196777344
		 50 20.197877883911133 51 20.072858810424805 52 19.807910919189453 53 19.451461791992188
		 54 19.061504364013672 55 18.713981628417969 56 18.433151245117188 57 18.192955017089844
		 58 17.988693237304687 59 17.819356918334961 60 17.688737869262695 61 17.193546295166016
		 62 16.058303833007812 63 14.476905822753906 64 12.825827598571777 65 11.868927955627441
		 66 12.622973442077637 67 15.102418899536135 68 18.294467926025391 69 21.737148284912109
		 70 23.893264770507813 71 24.127220153808594 72 22.803524017333984 73 20.410619735717773
		 74 18.042446136474609 75 18.908332824707031 76 21.822040557861328 77 22.875783920288086
		 78 22.110097885131836 79 20.879934310913086 80 19.711389541625977;
createNode animCurveTA -n "Character1_LeftLeg_rotateZ";
	rename -uid "1E3EA5B9-4A2C-DD67-B347-63B69415220F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 17.270214080810547 1 25.414056777954102
		 2 25.41424560546875 3 22.530309677124023 4 23.362430572509766 5 24.281745910644531
		 6 21.858499526977539 7 7.7875180244445792 8 -14.818386077880858 9 -17.812282562255859
		 10 -16.193088531494141 11 -12.043988227844238 12 -7.1823720932006845 13 -2.7913432121276855
		 14 0.38548263907432556 15 1.7435400485992432 16 1.648118257522583 17 1.0030397176742554
		 18 -0.067933633923530579 19 -1.444128155708313 20 -2.9878017902374268 21 -4.560699462890625
		 22 -6.0783305168151855 23 -7.430027961730957 24 -8.4525213241577148 25 -9.2647686004638672
		 26 -10.170805931091309 27 -11.153251647949219 28 -12.115372657775879 29 -12.887026786804199
		 30 -13.346242904663086 31 -13.604436874389648 32 -13.745351791381836 33 -13.827028274536133
		 34 -13.878425598144531 35 -13.923094749450684 36 -13.896594047546387 37 -13.729029655456543
		 38 -13.385544776916504 39 -12.828168869018555 40 -12.045991897583008 41 -11.130101203918457
		 42 -10.187798500061035 43 -9.3094882965087891 44 -8.5723886489868164 45 -8.0177640914916992
		 46 -7.6054329872131348 47 -7.341188907623291 48 -7.2210540771484384 49 -7.2263269424438477
		 50 -7.3344244956970215 51 -7.4591202735900879 52 -7.6113567352294922 53 -7.8703155517578134
		 54 -8.2738733291625977 55 -8.8296270370483398 56 -9.5350971221923828 57 -10.325016021728516
		 58 -11.103559494018555 59 -11.758997917175293 60 -12.165189743041992 61 -12.910196304321289
		 62 -14.426640510559084 63 -16.356466293334961 64 -18.195068359375 65 -19.172220230102539
		 66 -18.419750213623047 67 -15.436513900756836 68 -10.559255599975586 69 -2.779923677444458
		 70 6.779566764831543 71 15.978776931762694 72 23.687963485717773 73 29.235000610351563
		 74 32.157634735107422 75 30.300966262817383 76 23.674798965454102 77 17.550617218017578
		 78 16.728805541992188 79 18.095848083496094 80 19.274892807006836;
createNode animCurveTU -n "Character1_LeftLeg_visibility";
	rename -uid "743FE63E-4EBF-AC53-107C-059AF7E9A8AB";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFoot_translateX";
	rename -uid "ACDEC865-4CA0-FA6B-3E17-D787CC0A5DF6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5243232250213623;
createNode animCurveTL -n "Character1_LeftFoot_translateY";
	rename -uid "E68C0489-4EC0-72EE-7182-75BEF4F5EBC1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.000351905822754;
createNode animCurveTL -n "Character1_LeftFoot_translateZ";
	rename -uid "895835AF-4EE5-FBBD-A298-D98DF5B342CC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.967419624328613;
createNode animCurveTU -n "Character1_LeftFoot_scaleX";
	rename -uid "9A984CCC-4CEC-559E-B52F-9DB7374A6EA3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFoot_scaleY";
	rename -uid "9F90E71E-4A0D-3897-6C5E-4CA630A9CAB1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleZ";
	rename -uid "8043D9FF-4E0F-41CE-1F06-87A90716FB60";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999964237213135;
createNode animCurveTA -n "Character1_LeftFoot_rotateX";
	rename -uid "ECAF5CE0-4559-778F-F5E9-C285C994BF05";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 41.134159088134766 1 42.128139495849609
		 2 38.446659088134766 3 36.536144256591797 4 40.301582336425781 5 44.268527984619141
		 6 45.810276031494141 7 38.909782409667969 8 24.851282119750977 9 23.095491409301758
		 10 25.134634017944336 11 28.933969497680668 12 32.806175231933594 13 35.995326995849609
		 14 38.155284881591797 15 39.017551422119141 16 38.800849914550781 17 38.029388427734375
		 18 36.814121246337891 19 35.266567230224609 20 33.512668609619141 21 31.695089340209961
		 22 29.97270393371582 23 28.525920867919922 24 27.564834594726563 25 27.04203987121582
		 26 26.72270393371582 27 26.557403564453125 28 26.506429672241211 29 26.588874816894531
		 30 26.82432746887207 31 27.129095077514648 32 27.441867828369141 33 27.726787567138672
		 34 27.975482940673828 35 28.146902084350586 36 28.316192626953125 37 28.597095489501953
		 38 28.979066848754883 39 29.450887680053711 40 29.993545532226559 41 30.565441131591793
		 42 31.128723144531246 43 31.656923294067379 44 32.130027770996094 45 32.539295196533203
		 46 32.911151885986328 47 33.213283538818359 48 33.421989440917969 49 33.524753570556641
		 50 33.483688354492188 51 33.305046081542969 52 33.005329132080078 53 32.578590393066406
		 54 32.047962188720703 55 31.4622802734375 56 30.84477424621582 57 30.209878921508793
		 58 29.59880256652832 59 29.060995101928714 60 28.655254364013672 61 27.869104385375977
		 62 26.350973129272461 63 24.399450302124023 64 22.455146789550781 65 21.263622283935547
		 66 21.749336242675781 67 24.179359436035156 68 28.012578964233398 69 33.919929504394531
		 70 40.960300445556641 71 47.63214111328125 72 53.170341491699219 73 57.028717041015625
		 74 58.71272277832032 75 55.586284637451172 76 48.766654968261719 77 43.209342956542969
		 78 41.518428802490234 79 41.435096740722656 80 41.326488494873047;
createNode animCurveTA -n "Character1_LeftFoot_rotateY";
	rename -uid "839AD371-48F1-D135-9BBA-E294227FCDD1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -7.5173330307006836 1 -19.881317138671875
		 2 -33.115871429443359 3 -39.773490905761719 4 -36.433525085449219 5 -26.270925521850586
		 6 -13.499103546142578 7 2.331770658493042 8 13.841523170471191 9 14.823898315429688
		 10 13.455951690673828 11 10.590600967407227 12 6.9259982109069824 13 3.2332017421722412
		 14 0.22505262494087222 15 -1.450471043586731 16 -1.9642491340637207 17 -1.9839119911193848
		 18 -1.6305170059204102 19 -1.0188748836517334 20 -0.26160785555839539 21 0.5408787727355957
		 22 1.3116222620010376 23 1.9472925662994387 24 2.3169889450073242 25 2.4570887088775635
		 26 2.5240590572357178 27 2.5610549449920654 28 2.5728001594543457 29 2.5023269653320313
		 30 2.3239271640777588 31 2.0963714122772217 32 1.8711603879928589 33 1.6907691955566406
		 34 1.5812351703643799 35 1.5680921077728271 36 1.5731714963912964 37 1.5035775899887085
		 38 1.3564356565475464 39 1.1247018575668335 40 0.80626815557479858 41 0.43216830492019653
		 42 0.04122244194149971 43 -0.33094280958175659 44 -0.65076953172683716 45 -0.89513272047042847
		 46 -1.0747655630111694 47 -1.1847831010818481 48 -1.2264882326126099 49 -1.2128361463546753
		 50 -1.1276692152023315 51 -0.98709172010421753 52 -0.79167193174362183 53 -0.50862479209899902
		 54 -0.13039541244506836 55 0.34080493450164795 56 0.88553398847579956 57 1.4592214822769165
		 58 2.0117430686950684 59 2.4917874336242676 60 2.8461756706237793 61 3.3925671577453613
		 62 4.3516340255737305 63 5.5262575149536133 64 6.6473865509033203 65 7.3277316093444815
		 66 7.1237244606018066 67 5.8264331817626953 68 3.5544071197509766 69 -0.31402888894081116
		 70 -5.2677602767944336 71 -10.200042724609375 72 -14.506534576416016 73 -17.787403106689453
		 74 -19.622188568115234 75 -17.967708587646484 76 -13.561123847961426 77 -9.9777317047119141
		 78 -8.9377317428588867 79 -8.9439773559570312 80 -8.9984550476074219;
createNode animCurveTA -n "Character1_LeftFoot_rotateZ";
	rename -uid "C2D2A37E-4D68-87F7-C427-E3BE3A85290F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 14.605794906616211 1 18.402673721313477
		 2 21.10920524597168 3 22.78326416015625 4 22.426658630371094 5 20.862056732177734
		 6 18.515464782714844 7 12.787728309631348 8 3.4900040626525879 9 1.9072487354278564
		 10 3.0502293109893799 11 5.4967684745788574 12 8.1019906997680664 13 10.334153175354004
		 14 11.936566352844238 15 12.706279754638672 16 12.813618659973145 17 12.63609504699707
		 18 12.227386474609375 19 11.639824867248535 20 10.932726860046387 21 10.172452926635742
		 22 9.4254360198974609 23 8.7814044952392578 24 8.3583526611328125 25 8.1358766555786133
		 26 7.9815649986267099 27 7.8927855491638184 28 7.8752994537353525 29 7.9310150146484384
		 30 8.0535926818847656 31 8.2134332656860352 32 8.3915071487426758 33 8.5645895004272461
		 34 8.703801155090332 35 8.7719364166259766 36 8.8218402862548828 37 8.9228496551513672
		 38 9.0776729583740234 39 9.289881706237793 40 9.5563745498657227 41 9.8495397567749023
		 42 10.139971733093262 43 10.403926849365234 44 10.622154235839844 45 10.784712791442871
		 46 10.914361953735352 47 11.014225006103516 48 11.082920074462891 49 11.116086959838867
		 50 11.088662147521973 51 10.997532844543457 52 10.854414939880371 53 10.661408424377441
		 54 10.422565460205078 55 10.135520935058594 56 9.7977714538574219 57 9.4299602508544922
		 58 9.0643405914306641 59 8.7384872436523437 60 8.4946441650390625 61 8.1055946350097656
		 62 7.3854670524597159 63 6.4401187896728516 64 5.4668493270874023 65 4.8527035713195801
		 66 5.0925211906433105 67 6.3006973266601563 68 8.0921039581298828 69 10.468375205993652
		 70 12.92710018157959 71 15.201203346252441 72 17.214893341064453 73 18.590829849243164
		 74 19.119245529174805 75 18.645523071289063 76 17.04380989074707 77 15.321121215820313
		 78 14.745171546936037 79 14.807309150695801 80 14.946505546569822;
createNode animCurveTU -n "Character1_LeftFoot_visibility";
	rename -uid "2F7D7FC6-4659-C449-81F5-F7AEAA060AEE";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftToeBase_translateX";
	rename -uid "82DED536-4CF8-671F-64C5-5DAE15F89EC2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.585639476776123;
createNode animCurveTL -n "Character1_LeftToeBase_translateY";
	rename -uid "695AAE96-483D-21E7-02F6-2FBE8435A38B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3360438346862793;
createNode animCurveTL -n "Character1_LeftToeBase_translateZ";
	rename -uid "542E9E5C-4815-64DD-F354-07B31C6C3CDB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.4897398948669434;
createNode animCurveTU -n "Character1_LeftToeBase_scaleX";
	rename -uid "1F5920E0-4359-C8C7-2E7B-61933231178E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftToeBase_scaleY";
	rename -uid "A5C6FE18-4CFF-A633-9034-24A07B2650C9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftToeBase_scaleZ";
	rename -uid "C39E165B-4148-27D1-8508-828671A82094";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_LeftToeBase_rotateX";
	rename -uid "7008B32A-4D4B-951A-242F-A28E871FBAD9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  2 -2.3807342763859651e-009 3 -2.6993463020374975e-009
		 4 1.2535476684570313 5 4.2215032577514648 6 7.7239351272583008 7 10.624774932861328
		 8 11.832228660583496 9 11.246917724609375 10 9.7175588607788086 11 7.582228660583497
		 12 5.1839251518249512 13 2.8762133121490479 14 1.0240949392318726 15 -5.4753663825124477e-009
		 16 -0.23600688576698303 17 -0.043743487447500229 18 0.46926814317703247 19 1.1949398517608643
		 20 2.0255510807037354 21 2.8543269634246826 22 3.5756547451019287 23 4.084907054901123
		 24 4.277869701385498 25 4.1789474487304687 26 3.9075317382812496 27 3.50152587890625
		 28 2.9988207817077637 29 2.4374516010284424 30 1.8556989431381223 31 1.2921382188796997
		 32 0.78563457727432251 33 0.37528952956199646 34 0.1003381758928299 35 -4.4345136451795497e-009
		 36 -4.3368455493464353e-009 48 -4.4010417532547308e-009 49 -4.40581260363615e-009
		 50 0.035686582326889038 51 0.13580361008644104 52 0.28991624712944031 53 0.48757764697074896
		 54 0.71834540367126465 55 0.97179490327835083 56 1.2375291585922241 57 1.5051848888397217
		 58 1.7644357681274414 59 2.004990816116333 60 2.2165911197662354 61 2.468132495880127
		 62 2.8013846874237061 63 3.1655335426330566 64 3.5099890232086182 65 3.7843935489654541
		 66 3.9385502338409428 67 3.9222519397735596 68 3.6850299835205078 69 2.8539748191833496
		 70 1.3756928443908691 71 -0.31820076704025269 72 -1.7877782583236694 73 -2.5853466987609863
		 74 -2.7158677577972412 75 -2.5399842262268066 76 -2.1416127681732178 77 -1.6048612594604492
		 78 -1.0135672092437744 79 -0.45103785395622253 80 -1.3792285091795975e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateY";
	rename -uid "DE328638-4077-55B2-A54A-078571706D22";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  2 1.0853737908433914e-009 3 8.3311768594995783e-010
		 4 -0.40595409274101257 5 -1.4389553070068359 6 -2.78739333152771 7 -4.0093579292297363
		 8 -4.5457644462585449 9 -4.2836480140686035 10 -3.616804838180542 11 -2.7297191619873047
		 12 -1.7950214147567747 13 -0.95765572786331188 14 -0.32989394664764404 15 -2.4722102143215352e-009
		 16 0.073677897453308105 17 0.012070669792592525 18 -0.15286724269390106 19 -0.39092713594436646
		 20 -0.67063570022583008 21 -0.95750689506530773 22 -1.2135342359542847 23 -1.397845983505249
		 24 -1.4684514999389648 25 -1.4321233034133911 26 -1.333024263381958 27 -1.1863588094711304
		 28 -1.0073797702789307 29 -0.81094062328338623 30 -0.61118650436401367 31 -0.42138698697090149
		 32 -0.25391918420791626 33 -0.12040858715772629 34 -0.03203393891453743 35 -7.1557071379402259e-010
		 36 -5.5042681523786996e-010 48 9.3861349248491877e-011 49 2.5272051118463423e-010
		 50 -0.011478224769234657 51 -0.04375641793012619 52 -0.093665488064289093 53 -0.15807333588600159
		 54 -0.23383133113384247 55 -0.31773260235786438 56 -0.40648189187049866 57 -0.49667727947235107
		 58 -0.58480322360992432 59 -0.66723453998565674 60 -0.74025213718414307 61 -0.82730478048324585
		 62 -0.94332116842269886 63 -1.0713893175125122 64 -1.193928599357605 65 -1.2926656007766724
		 66 -1.3488701581954956 67 -1.3438409566879272 68 -1.2596460580825806 69 -0.96800172328948975
		 70 -0.4680684506893158 71 0.074588581919670105 72 0.51946067810058594 73 0.75169295072555542
		 74 0.79104220867156982 75 0.74387389421463013 76 0.63257241249084473 77 0.47889724373817444
		 78 0.30557507276535034 79 0.13713480532169342 80 1.1630111318439162e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateZ";
	rename -uid "D4AC4C6B-42AB-078D-1548-AB98B1665CF7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  2 1.2183177799940381e-009 3 1.0052619847655819e-009
		 4 0.74527430534362793 5 2.4852008819580078 6 4.4891557693481445 7 6.1035146713256836
		 8 6.7622261047363281 9 6.4439277648925781 10 5.6033391952514648 11 4.4091620445251465
		 12 3.0413765907287598 13 1.700901985168457 14 0.60921698808670044 15 -3.1963301450055326e-010
		 16 -0.14079801738262177 17 -0.025719435885548592 18 0.28032219409942627 19 0.71143293380737305
		 20 1.2023301124572754 21 1.6893621683120728 22 2.1109282970428467 23 2.407214879989624
		 24 2.5191876888275146 25 2.4617934226989746 26 2.3040995597839355 27 2.0676124095916748
		 28 1.773826003074646 29 1.4445056915283203 30 1.1018660068511963 31 0.76864880323410034
		 32 0.46810668706893915 33 0.22389999032020569 34 0.059913866221904748 35 6.2582183879555942e-010
		 36 7.1269234958037941e-010 48 9.3019458802245925e-010 49 8.4062695693276623e-010
		 50 0.021342851221561432 51 0.081193067133426666 52 0.17324663698673248 53 0.29117807745933533
		 54 0.42866933345794678 55 0.57943320274353027 56 0.73723059892654419 57 0.89588224887847889
		 58 1.049274206161499 59 1.1913566589355469 60 1.3161365985870361 61 1.4641040563583374
		 62 1.6596029996871948 63 1.8726601600646973 64 2.0736963748931885 65 2.233551025390625
		 66 2.3233504295349121 67 2.3142104148864746 68 2.1767795085906982 69 1.6928435564041138
		 70 0.82500934600830078 71 -0.17970612645149231 72 -1.059859037399292 73 -1.5410645008087158
		 74 -1.6209233999252319 75 -1.5159467458724976 76 -1.2770923376083374 77 -0.95564591884613037
		 78 -0.60247015953063965 79 -0.26757049560546875 80 1.8174071136911605e-009;
createNode animCurveTU -n "Character1_LeftToeBase_visibility";
	rename -uid "3464C400-4A11-13FD-627C-8D94F08D256A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateX";
	rename -uid "4A117D29-42C9-0983-D756-96810370EC0A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7153444290161133;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateY";
	rename -uid "082A99D8-4ED2-3F7C-9942-DF82E1CAA5AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.0020139217376709;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateZ";
	rename -uid "7E54C6D6-4CF3-D5A1-6B2D-54A6AC939D21";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.1786108016967773;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleX";
	rename -uid "58B072D2-4A5F-5177-8790-9899A3E22014";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleY";
	rename -uid "A3510B98-4011-7274-4D24-81BBDEF80119";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999940395355225;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleZ";
	rename -uid "BCFBFD4B-49E8-8B30-C727-CA8919018ED1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateX";
	rename -uid "8BE0E692-4217-AC29-AF6F-CEB94748B756";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.7149036624661846e-010;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateY";
	rename -uid "ECE349BD-428D-EE6F-E865-F8837514916F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8438121029973329e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateZ";
	rename -uid "3EEE0BB4-44C1-989B-0418-2580F7FD0381";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1840031701242424e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle1_visibility";
	rename -uid "1CEECB93-4741-01D5-B986-34B15369D523";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateX";
	rename -uid "41ECB5E4-4F96-0E31-7C50-E7B6A0D5286D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4684751033782959;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateY";
	rename -uid "B34170E2-4D78-4936-EFF8-638A46D1AB00";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.1393222808837891;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateZ";
	rename -uid "9E8E822C-4EA2-5CEA-EB11-51B1F714BA78";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.3014278411865234;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleX";
	rename -uid "98F9D319-4D89-A6BB-F7B1-F98099CBB398";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleY";
	rename -uid "AA7BF93D-4082-ADD9-6A3C-25BE7CDC46BC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleZ";
	rename -uid "0C3D5073-4043-DFA6-9713-63B55FC93DF5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateX";
	rename -uid "92BB3E76-411A-B1D7-0237-A6B391C9F15E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1745718175859565e-008;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateY";
	rename -uid "0BC9602B-4A6C-5779-80A9-20BC2CF5B91F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.658756461305984e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateZ";
	rename -uid "B636E6F9-44C8-2940-AA0F-A69CA0B08071";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.4837759343986363e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle2_visibility";
	rename -uid "DDCDAA19-430E-24E4-16A2-6BBD0F961FE7";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightUpLeg_translateX";
	rename -uid "4D1CB384-409D-E886-1B07-37B74629E534";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.489771842956543;
createNode animCurveTL -n "Character1_RightUpLeg_translateY";
	rename -uid "DE93C1A5-4462-FE48-04C4-84BA4A680698";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.6505136489868164;
createNode animCurveTL -n "Character1_RightUpLeg_translateZ";
	rename -uid "E248750A-4E37-F584-0B52-AF96B09BAC75";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0264549255371094;
createNode animCurveTU -n "Character1_RightUpLeg_scaleX";
	rename -uid "9DDBF4E3-4E4A-1804-F2C3-F9A31933C329";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightUpLeg_scaleY";
	rename -uid "4029691E-4032-09AD-CE4C-9EA665E17301";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightUpLeg_scaleZ";
	rename -uid "F90DF1C7-4EF5-47DC-8153-F59FBDFDA6B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightUpLeg_rotateX";
	rename -uid "955848F9-4BF1-1146-4E26-F8BCA2F5AEB5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -31.774538040161133 1 -42.392665863037109
		 2 -59.961521148681641 3 -68.917350769042969 4 -58.871543884277337 5 -41.798809051513672
		 6 -28.211391448974609 7 -22.533679962158203 8 -24.485454559326172 9 -25.05824089050293
		 10 -25.732906341552734 11 -27.018739700317383 12 -29.162479400634766 13 -31.750715255737301
		 14 -34.154109954833984 15 -35.678318023681641 16 -36.250240325927734 17 -36.347846984863281
		 18 -36.131641387939453 19 -35.814689636230469 20 -35.666091918945313 21 -35.913764953613281
		 22 -36.499855041503906 23 -37.238899230957031 24 -37.896129608154297 25 -38.223011016845703
		 26 -38.075836181640625 27 -37.580181121826172 28 -36.979305267333984 29 -36.586624145507812
		 30 -36.601741790771484 31 -36.870624542236328 32 -37.212070465087891 33 -37.465263366699219
		 34 -37.490669250488281 35 -37.186046600341797 36 -36.620628356933594 37 -35.936588287353516
		 38 -35.329532623291016 39 -35.020755767822266 40 -35.154266357421875 41 -35.568840026855469
		 42 -36.041244506835938 43 -36.393787384033203 44 -36.496746063232422 45 -36.356449127197266
		 46 -36.057651519775391 47 -35.677642822265625 48 -35.296642303466797 49 -35.006851196289063
		 50 -34.918567657470703 51 -35.135448455810547 52 -35.577232360839844 53 -36.08770751953125
		 54 -36.530811309814453 55 -36.783073425292969 56 -36.725849151611328 57 -36.40081787109375
		 58 -35.938690185546875 59 -35.519641876220703 60 -35.367107391357422 61 -35.668846130371094
		 62 -36.357105255126953 63 -37.278430938720703 64 -38.245552062988281 65 -39.006053924560547
		 66 -38.924110412597656 67 -38.383167266845703 68 -37.474609375 69 -36.073463439941406
		 70 -34.676792144775391 71 -33.789794921875 72 -33.514087677001953 73 -33.726142883300781
		 74 -34.076747894287109 75 -33.908714294433594 76 -33.739028930664063 77 -34.058994293212891
		 78 -34.135753631591797 79 -33.960506439208984 80 -33.752704620361328;
createNode animCurveTA -n "Character1_RightUpLeg_rotateY";
	rename -uid "9D72E5B4-4DD8-61C8-9749-158415DBCDDD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 36.258934020996094 1 47.230724334716797
		 2 54.211750030517578 3 55.846138000488281 4 53.824649810791016 5 47.112815856933594
		 6 37.012252807617187 7 20.859640121459961 8 5.0733394622802734 9 3.4000852108001709
		 10 5.7424526214599609 11 9.7541866302490234 12 13.603443145751953 13 16.7327880859375
		 14 18.939239501953125 15 20.15294075012207 16 21.104530334472656 17 22.29876708984375
		 18 23.289796829223633 19 23.627443313598633 20 22.882232666015625 21 20.812740325927734
		 22 17.933258056640625 23 14.999340057373047 24 12.818343162536621 25 11.914802551269531
		 26 12.470394134521484 27 13.989191055297852 28 15.669820785522459 29 16.725893020629883
		 30 16.717390060424805 31 16.016504287719727 32 15.067120552062988 33 14.32768726348877
		 34 14.267740249633789 35 15.228279113769531 36 16.930814743041992 37 18.895833969116211
		 38 20.58873176574707 39 21.481706619262695 40 21.21772575378418 41 20.129812240600586
		 42 18.731683731079102 43 17.548244476318359 44 17.111455917358398 45 17.514835357666016
		 46 18.502561569213867 47 19.767976760864258 48 21.004373550415039 49 21.906816482543945
		 50 22.147317886352539 51 21.449947357177734 52 20.056917190551758 53 18.41948127746582
		 54 16.992681503295898 55 16.233089447021484 56 16.514841079711914 57 17.564287185668945
		 58 18.869894027709961 59 19.929771423339844 60 20.254396438598633 61 19.333015441894531
		 62 17.30864143371582 63 14.681540489196779 64 12.014259338378906 65 9.9625663757324219
		 66 10.161473274230957 67 11.590133666992188 68 14.239385604858398 69 18.912508010864258
		 70 25.001029968261719 71 31.261220932006836 72 37.00384521484375 73 41.610099792480469
		 74 44.396739959716797 75 42.629486083984375 76 36.896125793457031 77 31.83320426940918
		 78 30.631254196166996 79 31.209857940673828 80 31.798828125;
createNode animCurveTA -n "Character1_RightUpLeg_rotateZ";
	rename -uid "E9AC5EF2-4E4C-D244-7FC2-7DB8BD4EA7A3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -18.08891487121582 1 -32.974987030029297
		 2 -53.058780670166016 3 -64.636589050292969 4 -58.098941802978516 5 -43.549694061279297
		 6 -30.768611907958988 7 -19.783233642578125 8 -12.136049270629883 9 -11.032142639160156
		 10 -11.493121147155762 11 -12.988752365112305 12 -15.083057403564453 13 -17.392553329467773
		 14 -19.449220657348633 15 -20.698606491088867 16 -21.254718780517578 17 -21.620195388793945
		 18 -21.804679870605469 19 -21.839385986328125 20 -21.770946502685547 21 -21.630012512207031
		 22 -21.413433074951172 23 -21.155296325683594 24 -20.969358444213867 25 -20.891628265380859
		 26 -20.781047821044922 27 -20.607271194458008 28 -20.441387176513672 29 -20.378545761108398
		 30 -20.459707260131836 31 -20.616542816162109 32 -20.798492431640625 33 -20.964267730712891
		 34 -21.081298828125 35 -21.170846939086914 36 -21.282793045043945 37 -21.417760848999023
		 38 -21.582328796386719 39 -21.80186653137207 40 -22.095041275024414 41 -22.429706573486328
		 42 -22.762811660766602 43 -23.066125869750977 44 -23.326580047607422 45 -23.508071899414063
		 46 -23.60028076171875 47 -23.617414474487305 48 -23.576761245727539 49 -23.501602172851563
		 50 -23.405294418334961 51 -23.323904037475586 52 -23.247341156005859 53 -23.128074645996094
		 54 -22.929950714111328 55 -22.626943588256836 56 -22.249231338500977 57 -21.856367111206055
		 58 -21.472267150878906 59 -21.135263442993164 60 -20.897676467895508 61 -20.588741302490234
		 62 -20.050907135009766 63 -19.361087799072266 64 -18.657291412353516 65 -18.183794021606445
		 66 -18.304609298706055 67 -19.217363357543945 68 -20.825359344482422 69 -23.619918823242187
		 70 -27.122928619384766 71 -30.599792480468754 72 -33.774017333984375 73 -36.293754577636719
		 74 -37.508499145507812 75 -34.498443603515625 76 -28.042848587036133 77 -22.528848648071289
		 78 -20.368953704833984 79 -19.712360382080078 80 -19.052003860473633;
createNode animCurveTU -n "Character1_RightUpLeg_visibility";
	rename -uid "3CB2AD02-47ED-E103-FD9B-00A2BEDDBB59";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightLeg_translateX";
	rename -uid "19F2E251-4BBE-0B00-CA07-1BAC6B2C7FCD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -18.197565078735352;
createNode animCurveTL -n "Character1_RightLeg_translateY";
	rename -uid "779F0143-42CB-30F8-DAF3-3ABE2A471FA4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.1862249374389648;
createNode animCurveTL -n "Character1_RightLeg_translateZ";
	rename -uid "EEEAFF32-4131-6051-1F91-30A0F791842B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.5520458221435547;
createNode animCurveTU -n "Character1_RightLeg_scaleX";
	rename -uid "40229C6B-433B-29BA-80C7-0EB012486350";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightLeg_scaleY";
	rename -uid "9E57DB96-4E96-BC6E-A715-C1B5FC6B7D58";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightLeg_scaleZ";
	rename -uid "0EF204BD-4FE1-E286-01F8-5DB6B2C18F66";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightLeg_rotateX";
	rename -uid "EAA9A4E8-4014-83FE-1A49-9BBF689FA991";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 32.307552337646484 1 44.846736907958984
		 2 55.919021606445313 3 61.869937896728509 4 61.781166076660149 5 56.466053009033203
		 6 47.457592010498047 7 33.650039672851563 8 22.580078125 9 21.355268478393555 10 23.280797958374023
		 11 26.96095085144043 12 31.160453796386719 13 35.026870727539063 14 37.980903625488281
		 15 39.561794281005859 16 40.055820465087891 17 40.124050140380859 18 39.861709594726563
		 19 39.397632598876953 20 38.913486480712891 21 38.591648101806641 22 38.454227447509766
		 23 38.464851379394531 24 38.598701477050781 25 38.697910308837891 26 38.530963897705078
		 27 38.166824340820312 28 37.789394378662109 29 37.630332946777344 30 37.806560516357422
		 31 38.170314788818359 32 38.581874847412109 33 38.923484802246094 34 39.097305297851563
		 35 39.03546142578125 36 38.804363250732422 37 38.513336181640625 38 38.290077209472656
		 39 38.28375244140625 40 38.591777801513672 41 39.100288391113281 42 39.650592803955078
		 43 40.112007141113281 44 40.383804321289063 45 40.449825286865234 46 40.375640869140625
		 47 40.213497161865234 48 40.019008636474609 49 39.855609893798828 50 39.786251068115234
		 51 39.885578155517578 52 40.108440399169922 53 40.344734191894531 54 40.498149871826172
		 55 40.483924865722656 56 40.234920501708984 57 39.804462432861328 58 39.298851013183594
		 59 38.855419158935547 60 38.639541625976563 61 38.638027191162109 62 38.702419281005859
		 63 38.798866271972656 64 38.913860321044922 65 39.070407867431641 66 39.143234252929687
		 67 39.51043701171875 68 40.115230560302734 69 41.220172882080078 70 42.835960388183594
		 71 44.912601470947266 72 47.496662139892578 73 50.213169097900391 74 51.759590148925781
		 75 47.775276184082031 76 41.261653900146484 77 36.800155639648437 78 34.951202392578125
		 79 34.139007568359375 80 33.322238922119141;
createNode animCurveTA -n "Character1_RightLeg_rotateY";
	rename -uid "4C9E5BB5-4132-3800-F0FC-F68B1822BC93";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -26.452003479003906 1 -44.344451904296875
		 2 -58.055255889892578 3 -65.56597900390625 4 -66.501914978027344 5 -62.420482635498054
		 6 -54.616725921630859 7 -34.164432525634766 8 -7.1790800094604501 9 -3.8782057762145992
		 10 -6.6252412796020508 11 -12.244027137756348 12 -18.100032806396484 13 -22.968978881835938
		 14 -26.21165657043457 15 -27.28050422668457 16 -26.614587783813477 17 -25.174840927124023
		 18 -23.061233520507813 19 -20.356752395629883 20 -17.150030136108398 21 -13.568305015563965
		 22 -9.8936452865600586 23 -6.5927739143371582 24 -4.3445320129394531 25 -3.2865281105041504
		 26 -2.8918313980102539 27 -2.9771676063537598 28 -3.4751968383789062 29 -4.314445972442627
		 30 -5.3425660133361816 31 -6.4100594520568848 32 -7.4568128585815421 33 -8.4403114318847656
		 34 -9.3307657241821289 35 -10.136045455932617 36 -10.982304573059082 37 -11.946076393127441
		 38 -12.932026863098145 39 -13.854997634887695 40 -14.655702590942381 41 -15.353534698486328
		 42 -15.98774242401123 43 -16.599639892578125 44 -17.229473114013672 45 -17.822713851928711
		 46 -18.352565765380859 47 -18.779687881469727 48 -19.066179275512695 49 -19.175527572631836
		 50 -19.017673492431641 51 -18.570978164672852 52 -17.899600982666016 53 -17.047147750854492
		 54 -16.060602188110352 55 -14.990614891052248 56 -13.942198753356934 57 -12.982068061828613
		 58 -12.106226921081543 59 -11.31265926361084 60 -10.604573249816895 61 -9.2084941864013672
		 62 -6.658897876739502 63 -3.4384148120880127 64 -0.21142220497131348 65 2.0283551216125488
		 66 1.8328806161880493 67 -1.3466836214065552 68 -7.2507028579711914 69 -17.472173690795898
		 70 -29.907314300537113 71 -41.477634429931641 72 -50.930622100830078 73 -57.504478454589844
		 74 -60.555076599121094 75 -55.364170074462891 76 -41.984355926513672 77 -29.264705657958984
		 78 -25.35064697265625 79 -25.86866569519043 80 -26.395648956298828;
createNode animCurveTA -n "Character1_RightLeg_rotateZ";
	rename -uid "43B04293-44ED-CF62-91AE-C7832D9CEADD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 2.6833248138427734 1 2.0795967578887939
		 2 1.2222001552581787 3 -0.47639942169189448 4 -2.2342612743377686 5 -2.5607337951660156
		 6 -1.9172153472900393 7 1.4152323007583618 8 2.849278450012207 9 2.8094737529754639
		 10 2.9870154857635498 11 3.2784101963043213 12 3.6069915294647217 13 3.943105936050415
		 14 4.268887996673584 15 4.564753532409668 16 4.8256096839904785 17 5.0567083358764648
		 18 5.2613635063171387 19 5.4587240219116211 20 5.6786084175109863 21 5.9400653839111328
		 22 6.2073445320129395 23 6.438084602355957 24 6.6061205863952637 25 6.6906957626342773
		 26 6.6733694076538086 27 6.5839705467224121 28 6.4699230194091797 29 6.3888387680053711
		 30 6.3752689361572266 31 6.4014992713928223 32 6.4372234344482422 33 6.4562110900878906
		 34 6.4359326362609863 35 6.3568210601806641 36 6.2260818481445313 37 6.066932201385498
		 38 5.9208002090454102 39 5.8361372947692871 40 5.8476638793945313 41 5.9257106781005859
		 42 6.0242204666137695 43 6.1018581390380859 44 6.1233859062194824 45 6.0885467529296875
		 46 6.0196638107299805 47 5.9352593421936035 48 5.8552227020263672 49 5.8015918731689453
		 50 5.8017582893371582 51 5.8770351409912109 52 6.0055828094482422 53 6.1507077217102051
		 54 6.2800741195678711 55 6.3657145500183105 56 6.3788242340087891 57 6.3300690650939941
		 58 6.2534265518188477 59 6.1890435218811035 60 6.182220458984375 61 6.2845883369445801
		 62 6.4730215072631836 63 6.6833257675170898 64 6.8668398857116699 65 6.997868537902832
		 66 7.0103216171264648 67 6.9846792221069336 68 6.8435788154602051 69 6.3426098823547363
		 70 5.1721343994140625 71 3.1964466571807861 72 0.40179231762886047 73 -2.7925293445587158
		 74 -5.0682392120361328 75 -2.5818002223968506 76 1.487650990486145 77 3.3498439788818359
		 78 3.5266072750091553 79 3.2139766216278076 80 2.898113489151001;
createNode animCurveTU -n "Character1_RightLeg_visibility";
	rename -uid "2DAF15B6-4029-2B47-87A4-1896096267D3";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFoot_translateX";
	rename -uid "6494A7A5-4B3A-596A-6BBB-9CA45BA6B048";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -18.25177001953125;
createNode animCurveTL -n "Character1_RightFoot_translateY";
	rename -uid "AF386FA8-4A73-4B5A-C5C4-D2BC7BC16915";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.0794563293457031;
createNode animCurveTL -n "Character1_RightFoot_translateZ";
	rename -uid "CBEDC745-41AD-D3B7-C0F5-9895EC0CD31A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5524176359176636;
createNode animCurveTU -n "Character1_RightFoot_scaleX";
	rename -uid "90310DCE-47B4-A204-3776-F4B83A24B459";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFoot_scaleY";
	rename -uid "942A43D0-45C5-FE8A-B81F-5C9187FFDDF5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFoot_scaleZ";
	rename -uid "06F0042B-4D9F-B1F2-7236-50A1FC77503B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightFoot_rotateX";
	rename -uid "AFB4E38A-4637-D5AB-338C-8AA8EE2687EB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -1.9345545768737795 1 -4.9043254852294922
		 2 -2.9963991641998291 3 -1.8885294198989868 4 -6.7297892570495605 5 -13.955952644348145
		 6 -18.719919204711914 7 -15.054502487182615 8 -4.6541190147399902 9 -2.745995044708252
		 10 -3.1530787944793701 11 -4.4570722579956055 12 -5.4892668724060059 13 -5.856743335723877
		 14 -5.6130471229553223 15 -4.9884982109069824 16 -4.2281780242919922 17 -3.4362056255340576
		 18 -2.5922014713287354 19 -1.6560488939285278 20 -0.58803802728652954 21 0.61953198909759521
		 22 1.8767066001892088 23 3.0230824947357178 24 3.8344326019287109 25 4.2254490852355957
		 26 4.3267354965209961 27 4.2257809638977051 28 3.971349716186523 29 3.6126389503479004
		 30 3.2174885272979736 31 2.8301870822906494 32 2.4623298645019531 33 2.1157567501068115
		 34 1.784943699836731 35 1.4483416080474854 36 1.0662124156951904 37 0.6310582160949707
		 38 0.20320683717727661 39 -0.15873363614082336 40 -0.41175296902656555 41 -0.57697463035583496
		 42 -0.69537073373794556 43 -0.81305825710296631 44 -0.9785962700843811 45 -1.1806573867797852
		 46 -1.389062762260437 47 -1.5764366388320923 48 -1.7150564193725586 49 -1.7773565053939819
		 50 -1.717769980430603 51 -1.5140610933303833 52 -1.1993005275726318 53 -0.81251758337020874
		 54 -0.39251106977462769 55 0.022974351420998573 56 0.37754243612289429 57 0.66439706087112427
		 58 0.91911923885345459 59 1.1726601123809814 60 1.4495613574981689 61 2.0208022594451904
		 62 3.0205590724945068 63 4.2601180076599121 64 5.5031833648681641 65 6.4064345359802246
		 66 6.454531192779541 67 5.5340256690979004 68 3.7382502555847164 69 0.55694383382797241
		 70 -3.2602541446685791 71 -6.6512165069580078 72 -9.2106485366821289 73 -10.795793533325195
		 74 -11.39653205871582 75 -9.8119754791259766 76 -5.6715502738952637 77 -1.4899992942810059
		 78 -0.43490099906921387 79 -1.1166576147079468 80 -1.7979464530944826;
createNode animCurveTA -n "Character1_RightFoot_rotateY";
	rename -uid "0CFF0966-4F44-B0CC-061A-CC8732B597C3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 0.77543109655380249 1 2.3830821514129639
		 2 2.3593740463256836 3 2.4381237030029297 4 3.4022190570831299 5 4.9864225387573242
		 6 6.4835243225097656 7 5.6902594566345215 8 1.5019576549530029 9 0.59130597114562988
		 10 0.81040376424789429 11 1.4762028455734253 12 2.0398776531219482 13 2.3448669910430908
		 14 2.4163191318511963 15 2.3014843463897705 16 2.0692036151885986 17 1.7815978527069092
		 18 1.4278782606124878 19 0.9943242073059082 20 0.47038277983665466 21 -0.14109548926353455
		 22 -0.79858154058456421 23 -1.4164342880249023 24 -1.857108473777771 25 -2.0673813819885254
		 26 -2.1291043758392334 27 -2.0825831890106201 28 -1.9477478265762329 29 -1.744770884513855
		 30 -1.5081007480621338 31 -1.2701137065887451 32 -1.0452079772949219 33 -0.84152281284332275
		 34 -0.6625257134437561 35 -0.50362664461135864 36 -0.34104946255683899 37 -0.16182294487953186
		 38 0.013970209285616875 39 0.16859474778175354 40 0.28929868340492249 41 0.38123226165771484
		 42 0.45529523491859436 43 0.52475762367248535 44 0.60432112216949463 45 0.68866145610809326
		 46 0.76915627717971802 47 0.83740395307540894 48 0.88509321212768555 49 0.90417259931564331
		 50 0.87877452373504639 51 0.80224472284317017 52 0.68542158603668213 53 0.53882086277008057
		 54 0.37325426936149597 55 0.19980733096599579 56 0.038249600678682327 57 -0.1054447740316391
		 58 -0.2399140149354935 59 -0.37125208973884583 60 -0.50220578908920288 61 -0.75799697637557983
		 62 -1.2190080881118774 63 -1.8159440755844116 64 -2.4426584243774414 65 -2.9111299514770508
		 66 -2.9278426170349121 67 -2.4061930179595947 68 -1.4498803615570068 69 0.074918359518051147
		 70 1.6488882303237915 71 2.8339450359344482 72 3.6097099781036377 73 4.0475916862487793
		 74 4.2104587554931641 75 3.7759346961975098 76 2.441929817199707 77 0.82206547260284424
		 78 0.32204684615135193 79 0.5380246639251709 80 0.75473040342330933;
createNode animCurveTA -n "Character1_RightFoot_rotateZ";
	rename -uid "8EE59450-4B5B-8F9F-6685-3D97628D710C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -0.61463683843612671 1 -8.6383686065673828
		 2 -16.728897094726563 3 -20.68501091003418 4 -19.75341796875 5 -15.912026405334473
		 6 -10.174581527709961 7 -2.2737164497375488 8 4.1309723854064941 9 4.4258203506469727
		 10 3.1172544956207275 11 0.76503938436508179 12 -2.0901310443878174 13 -4.8918814659118652
		 14 -7.0627603530883789 15 -8.0235662460327148 16 -7.8400321006774911 17 -7.051537036895752
		 18 -5.801297664642334 19 -4.2507448196411133 20 -2.5746264457702637 21 -0.92931431531906117
		 22 0.58171969652175903 23 1.8277498483657835 24 2.6039974689483643 25 2.9350199699401855
		 26 3.0754115581512451 27 3.0640878677368164 28 2.8878319263458252 29 2.5200238227844238
		 30 1.9786317348480225 31 1.3558868169784546 32 0.72588300704956055 33 0.1573236882686615
		 34 -0.28599336743354797 35 -0.55420804023742676 36 -0.71235674619674683 37 -0.84904605150222778
		 38 -0.99270790815353394 39 -1.1892126798629761 40 -1.4751074314117432 41 -1.8171545267105103
		 42 -2.1625714302062988 43 -2.4663560390472412 44 -2.6928162574768066 45 -2.8226895332336426
		 46 -2.8800642490386963 47 -2.880237340927124 48 -2.8420939445495605 49 -2.7894086837768555
		 50 -2.7313520908355713 51 -2.6910445690155029 52 -2.6565215587615967 53 -2.5845985412597656
		 54 -2.4421849250793457 55 -2.2065379619598389 56 -1.8762198686599734 57 -1.4918394088745117
		 58 -1.0983127355575562 59 -0.74784016609191895 60 -0.49962797760963445 61 -0.16566824913024902
		 62 0.43114668130874634 63 1.2085882425308228 64 2.0182886123657227 65 2.5904994010925293
		 66 2.5638778209686279 67 1.6863477230072021 68 0.054655399173498154 69 -2.7291760444641113
		 70 -5.943577766418457 71 -8.6296825408935547 72 -10.45325756072998 73 -11.336116790771484
		 74 -11.330033302307129 75 -9.624760627746582 76 -6.1565728187561035 77 -2.8506076335906982
		 78 -1.5990115404129028 79 -1.3360226154327393 80 -1.0547091960906982;
createNode animCurveTU -n "Character1_RightFoot_visibility";
	rename -uid "66C222EF-47F6-CE38-DE4F-FEB0F21392F8";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightToeBase_translateX";
	rename -uid "7801AE18-422C-90F0-AC30-2CBB8418ABA3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2767767906188965;
createNode animCurveTL -n "Character1_RightToeBase_translateY";
	rename -uid "29CA5492-4F4B-856A-EF32-A1A8667EDF1D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.0139193534851074;
createNode animCurveTL -n "Character1_RightToeBase_translateZ";
	rename -uid "F4684EEC-4DA8-1A95-03C1-B1A9FA0025D1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8259720802307129;
createNode animCurveTU -n "Character1_RightToeBase_scaleX";
	rename -uid "5B0A3E05-4B9C-8F80-FEF4-8583E2E50583";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightToeBase_scaleY";
	rename -uid "6399BB8E-40D1-36B0-2697-238B13978F63";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightToeBase_scaleZ";
	rename -uid "0F70D107-4908-8FBB-1C9E-8486E0510615";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightToeBase_rotateX";
	rename -uid "D664B772-443B-4143-1374-739FA3DB0EA1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.5931770892005939e-009;
createNode animCurveTA -n "Character1_RightToeBase_rotateY";
	rename -uid "FBFFAEFF-4CAC-2E4B-248C-E3B8D91BA148";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.0815870662220846e-010;
createNode animCurveTA -n "Character1_RightToeBase_rotateZ";
	rename -uid "BA2636F8-4814-CC9F-BF22-628AB71D44B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.3684318395389425e-010;
createNode animCurveTU -n "Character1_RightToeBase_visibility";
	rename -uid "FC1D1341-47FE-B775-06EF-7AA8479FE963";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateX";
	rename -uid "A8D2DE1E-4B71-FA4E-0443-1BBFB9ECF17A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.0018006421159952879;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateY";
	rename -uid "8C63E9DE-463E-5B5C-33EE-6685A718DBA4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.0064689838327467442;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateZ";
	rename -uid "CF5E0460-4FB5-953D-F5EE-088662196F6D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.6805553436279297;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleX";
	rename -uid "3929980C-4CA7-7A0D-10D9-29B3BCBFC52F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleY";
	rename -uid "9B1CEC63-46F2-4FC2-72B1-2D9681336CC7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleZ";
	rename -uid "13C0B0A9-47E0-8103-5F1D-AF8489056C73";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateX";
	rename -uid "92EFD8E4-4980-0152-B7A1-00B98F2B19BA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.3609821288632702e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateY";
	rename -uid "4A795AE2-47EB-E69B-CC01-26A6134A1850";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.2912984277165833e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateZ";
	rename -uid "8FDFA6C1-4DB5-3CA9-E3A8-F498D64BE4F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.5098927236854038e-011;
createNode animCurveTU -n "Character1_RightFootMiddle1_visibility";
	rename -uid "86C0CB87-470B-1F7B-BC29-778C3C6E5291";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateX";
	rename -uid "1B41CDE9-44CA-FBC7-35A0-D293A2FAC734";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.586081791378092e-006;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateY";
	rename -uid "C496C3F6-422D-02CA-111A-0B84405BC21D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.3307112441179925e-007;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateZ";
	rename -uid "0FB016C5-44E5-0516-7381-D58095309753";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.0234856605529785;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleX";
	rename -uid "8C001DDD-4428-AA66-953D-2AAB19795613";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleY";
	rename -uid "9199FD88-4817-CDD3-9DC2-45A7A8C13844";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleZ";
	rename -uid "C13DDEDD-4BA8-7BC8-7B0B-3B9B25A3F82E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateX";
	rename -uid "0A27E0A9-48FC-1C3D-4D16-979910A0ACA2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.1052091443182235e-009;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateY";
	rename -uid "1D8EE5FC-40A5-AFE1-61DD-F2AA9CCDB0F6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7923277084719302e-008;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateZ";
	rename -uid "5292EA69-4B16-E474-05E2-6FAB7487BFEB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.6549725185585094e-010;
createNode animCurveTU -n "Character1_RightFootMiddle2_visibility";
	rename -uid "5CC249B4-4BB9-EA38-A11C-44BE97237AB5";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Spine_translateX";
	rename -uid "EDCCE24E-47B2-9390-5968-089C7F8F520C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6075620651245117;
createNode animCurveTL -n "Character1_Spine_translateY";
	rename -uid "443BA40B-438B-A7C3-F354-318F07D4EC0C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.353469848632812;
createNode animCurveTL -n "Character1_Spine_translateZ";
	rename -uid "8963956E-4D10-249E-1B81-37915896E083";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2061522006988525;
createNode animCurveTU -n "Character1_Spine_scaleX";
	rename -uid "16FEC8B0-4BFF-CDC0-6749-5E83A516E65F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine_scaleY";
	rename -uid "DDF9A12A-475C-96D8-3386-8F86F9A0AF22";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine_scaleZ";
	rename -uid "C8D39287-4F9C-601E-42D5-E69B61872FFA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_Spine_rotateX";
	rename -uid "29B639E2-4D0C-A639-3770-B4BBF4E71856";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -4.2090029716491699 1 -3.6013133525848393
		 2 -3.05987548828125 3 -1.8662388324737549 4 0.23246969282627103 5 2.6913602352142334
		 6 5.1109714508056641 7 7.2121262550354013 8 8.5074796676635742 9 8.8133888244628906
		 10 8.5782718658447266 11 7.9778966903686523 12 7.1966915130615243 13 6.4001884460449219
		 14 5.7327466011047363 15 5.3278341293334961 16 5.077723503112793 17 4.8102169036865234
		 18 4.5611720085144043 19 4.366213321685791 20 4.2606301307678223 21 4.2672863006591797
		 22 4.3615560531616211 23 4.5079078674316406 24 4.6721377372741699 25 4.8360862731933594
		 26 5.0214862823486328 27 5.252892017364502 28 5.51715087890625 29 5.8017549514770508
		 30 6.0957975387573242 31 6.3869419097900391 32 6.6616454124450684 33 6.9053263664245605
		 34 7.1025471687316895 35 7.1985468864440909 36 7.1867194175720215 37 7.1251101493835449
		 38 7.0565357208251953 39 7.0240612030029297 40 7.0576934814453125 41 7.1357083320617676
		 42 7.2235250473022461 43 7.2869110107421875 44 7.2921414375305176 45 7.258612632751464
		 46 7.2310156822204581 47 7.2146272659301758 48 7.2145872116088867 49 7.2359390258789054
		 50 7.282660484313964 51 7.3526363372802725 52 7.4404091835021973 53 7.539910316467286
		 54 7.6450748443603507 55 7.7498884201049814 56 7.8088126182556152 57 7.8023118972778329
		 58 7.763741970062255 59 7.7268810272216797 60 7.7257471084594718 61 7.8135128021240234
		 62 7.9859509468078613 63 8.1911993026733398 64 8.3806257247924805 65 8.4920511245727539
		 66 8.3995800018310547 67 8.1681385040283203 68 7.776808261871337 69 7.1182694435119629
		 70 6.2041945457458496 71 5.1567306518554687 72 4.0708465576171875 73 3.0192384719848633
		 74 2.0634651184082031 75 1.1756092309951782 76 0.24574680626392362 77 -0.75929921865463257
		 78 -1.8392549753189087 79 -2.9963481426239014 80 -4.2330412864685059;
createNode animCurveTA -n "Character1_Spine_rotateY";
	rename -uid "015AA531-475A-12C1-B349-FABEA806C349";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -12.116277694702148 1 -10.853448867797852
		 2 -9.5456180572509766 3 -6.1940698623657227 4 0.27813178300857544 5 8.6287508010864258
		 6 17.324968338012695 7 24.749103546142578 8 29.264507293701175 9 30.787670135498047
		 10 30.694583892822269 11 29.466575622558594 12 27.58625602722168 13 25.536882400512695
		 14 23.801603317260742 15 22.862676620483398 16 22.465417861938477 17 22.077606201171875
		 18 21.764389038085938 19 21.590826034545898 20 21.621923446655273 21 21.891342163085938
		 22 22.306961059570313 23 22.744916915893555 24 23.080715179443359 25 23.24104118347168
		 26 23.305566787719727 27 23.375150680541992 28 23.447578430175781 29 23.520137786865234
		 30 23.590450286865234 31 23.656867980957031 32 23.718297958374023 33 23.774112701416016
		 34 23.824106216430664 35 23.764896392822266 36 23.559852600097656 37 23.307258605957031
		 38 23.10107421875 39 23.034996032714844 40 23.171340942382813 41 23.44703483581543
		 42 23.767553329467773 43 24.038120269775391 44 24.163698196411133 45 24.183525085449219
		 46 24.199941635131836 47 24.212060928344727 48 24.219047546386719 49 24.220075607299805
		 50 24.210332870483398 51 24.186883926391602 52 24.15240478515625 53 24.109804153442383
		 54 24.061918258666992 55 24.011550903320313 56 23.858158111572266 57 23.563995361328125
		 58 23.22607421875 59 22.941316604614258 60 22.806509017944336 61 22.993568420410156
		 62 23.497522354125977 63 24.149734497070313 64 24.783876419067383 65 25.22796630859375
		 66 25.158618927001953 67 24.686420440673828 68 23.695062637329102 69 21.889106750488281
		 70 19.242408752441406 71 16.030813217163086 72 12.532768249511719 73 9.0282669067382812
		 74 5.7942934036254883 75 2.7683858871459961 76 -0.27271431684494019 77 -3.2941582202911377
		 78 -6.2727475166320801 79 -9.2072086334228516 80 -12.096697807312012;
createNode animCurveTA -n "Character1_Spine_rotateZ";
	rename -uid "EC5CF96B-4342-ACE0-6744-4BBCCEF45BD3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 5.6667900085449219 1 4.3706741333007812
		 2 3.1304512023925781 3 1.8589820861816406 4 0.62452703714370728 5 -0.20554184913635254
		 6 -0.37949877977371216 7 -0.00058807723689824343 8 0.42007100582122803 9 0.52079319953918457
		 10 0.39271539449691772 11 0.11611944437026978 12 -0.20293784141540527 13 -0.48185634613037109
		 14 -0.67551261186599731 15 -0.76696336269378662 16 -0.79206842184066772 17 -0.79495406150817871
		 18 -0.77843725681304932 19 -0.74684470891952515 20 -0.70570200681686401 21 -0.65766149759292603
		 22 -0.59273892641067505 23 -0.49887165427207947 24 -0.36675971746444702 25 -0.17627672851085663
		 26 0.073414050042629242 27 0.36058962345123291 28 0.67238354682922363 29 0.99868679046630859
		 30 1.3293485641479492 31 1.6519663333892822 32 1.9529362916946411 33 2.2184474468231201
		 34 2.4348609447479248 35 2.5921545028686523 36 2.7026593685150146 37 2.7858681678771973
		 38 2.8426260948181152 39 2.8727717399597168 40 2.8767464160919189 41 2.8598208427429199
		 42 2.8286688327789307 43 2.7896385192871094 44 2.7486145496368408 45 2.7075066566467285
		 46 2.6665751934051514 47 2.6294374465942383 48 2.5998096466064453 49 2.5815019607543945
		 50 2.567070484161377 51 2.5478935241699219 52 2.5248081684112549 53 2.4983019828796387
		 54 2.4693999290466309 55 2.4393110275268555 56 2.4117403030395508 57 2.3890759944915771
		 58 2.3710439205169678 59 2.357391357421875 60 2.3476176261901855 61 2.3621764183044434
		 62 2.4122991561889648 63 2.4821071624755859 64 2.5582327842712402 65 2.6166963577270508
		 66 2.6307954788208008 67 2.5782039165496826 68 2.4482085704803467 69 2.178300142288208
		 70 1.8093388080596924 71 1.4708582162857056 72 1.2558199167251587 73 1.2201701402664185
		 74 1.3857409954071045 75 1.7695980072021484 76 2.3553597927093506 77 3.0852410793304443
		 78 3.8883259296417232 79 4.7557277679443359 80 5.6817870140075684;
createNode animCurveTU -n "Character1_Spine_visibility";
	rename -uid "94399E50-4857-F244-199D-B7B1E4A498F6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Spine1_translateX";
	rename -uid "A2690475-4091-52A9-E4EE-05BCF7384848";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.930038452148437;
createNode animCurveTL -n "Character1_Spine1_translateY";
	rename -uid "CE4D13FF-4660-724E-F718-999E1023F84C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1661763191223145;
createNode animCurveTL -n "Character1_Spine1_translateZ";
	rename -uid "D9FA7F8F-4389-BAC9-9F43-02ADEE1DB3DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.40121376514434814;
createNode animCurveTU -n "Character1_Spine1_scaleX";
	rename -uid "5DF52F4B-4547-A94E-04E5-5E8C78C2D7EA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_Spine1_scaleY";
	rename -uid "5DF2AEC7-48E6-88A5-0A8E-67A45CB51A96";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine1_scaleZ";
	rename -uid "3978FDB5-40C7-1E64-3B56-36BA85B942F9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_Spine1_rotateX";
	rename -uid "006FD520-430F-7A04-0841-719720DBDA53";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -26.060359954833984 1 -24.876972198486328
		 2 -23.632785797119141 3 -20.367748260498047 4 -14.520400047302248 5 -7.4965038299560547
		 6 -0.3241080641746521 7 6.0775504112243652 8 10.46894359588623 9 12.678803443908691
		 10 13.818164825439453 11 13.036717414855957 12 10.286724090576172 13 6.9732913970947266
		 14 4.278378963470459 15 3.2206325531005859 16 4.2005681991577148 17 6.4559149742126465
		 18 9.2275285720825195 19 11.611614227294922 20 12.517745018005371 21 11.193035125732422
		 22 8.4603662490844727 23 5.4687972068786621 24 3.1746273040771484 25 2.3642914295196533
		 26 4.0311374664306641 27 7.6344184875488272 28 11.509749412536621 29 13.612587928771973
		 30 12.939539909362793 31 10.767500877380371 32 8.1857633590698242 33 6.1398715972900391
		 34 5.4649720191955566 35 6.7072439193725586 36 9.3548822402954102 37 12.621920585632324
		 38 15.522663116455078 39 16.835287094116211 40 15.686564445495605 41 12.933326721191406
		 42 9.8067111968994141 43 7.3183221817016593 44 6.3068852424621582 45 7.0316872596740723
		 46 8.8889331817626953 47 11.406349182128906 48 14.063626289367676 49 16.243856430053711
		 50 17.242790222167969 51 16.234472274780273 52 13.703306198120117 53 10.827121734619141
		 54 8.591609001159668 55 7.8411359786987314 56 9.1209001541137695 57 11.876591682434082
		 58 15.264486312866211 59 18.262609481811523 60 19.623615264892578 61 19.107309341430664
		 62 17.613304138183594 63 15.545415878295897 64 13.286587715148926 65 11.158577919006348
		 66 10.965671539306641 67 10.118307113647461 68 8.4721813201904297 69 5.3268280029296875
		 70 0.80595982074737549 71 -4.2015171051025391 72 -9.1584558486938477 73 -13.757500648498535
		 74 -17.67540168762207 75 -20.92717170715332 76 -23.909656524658203 77 -26.673435211181641
		 78 -29.307371139526367 79 -31.995794296264645 80 -34.997623443603516;
createNode animCurveTA -n "Character1_Spine1_rotateY";
	rename -uid "41589253-4163-A976-600B-C299D2BDD959";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -4.7502837181091309 1 -6.4026765823364258
		 2 -8.1222038269042969 3 -7.3424615859985352 4 -2.9135668277740479 5 3.622773170471191
		 6 10.866799354553223 7 17.317743301391602 8 21.575639724731445 9 23.652774810791016
		 10 24.681411743164063 11 23.879484176635742 12 21.070863723754883 13 17.469644546508789
		 14 14.393857002258301 15 13.238097190856934 16 14.65250873565674 17 17.60508918762207
		 18 20.984903335571289 19 23.752840042114258 20 24.960046768188477 21 23.988897323608398
		 22 21.458305358886719 23 18.34105110168457 24 15.716197967529297 25 14.723845481872559
		 26 16.662040710449219 27 20.639181137084961 28 24.542957305908203 29 26.432275772094727
		 30 25.641778945922852 31 23.346115112304687 32 20.498064041137695 33 18.112842559814453
		 34 17.253559112548828 35 18.644163131713867 36 21.555549621582031 37 24.88287353515625
		 38 27.595714569091797 39 28.787370681762695 40 27.883064270019531 41 25.463003158569336
		 42 22.433099746704102 43 19.818357467651367 44 18.733642578125 45 19.586017608642578
		 46 21.613864898681641 47 24.186616897583008 48 26.694072723388672 49 28.569784164428711
		 50 29.270511627197262 51 28.147111892700195 52 25.513832092285156 53 22.304985046386719
		 54 19.537626266479492 55 18.28718376159668 56 19.287714004516602 57 21.809909820556641
		 58 24.753767013549805 59 27.124702453613281 60 28.066034317016602 61 27.575521469116211
		 62 26.349750518798828 63 24.619022369384766 64 22.632064819335938 65 20.662796020507813
		 66 20.666181564331055 67 19.945804595947266 68 18.267892837524414 69 14.816289901733398
		 70 9.3668403625488281 71 2.6694598197937012 72 -4.3251824378967285 73 -10.531711578369141
		 74 -14.849834442138672 75 -17.036848068237305 76 -17.825418472290039 77 -17.66234016418457
		 78 -16.978292465209961 79 -16.174409866333008 80 -15.570843696594238;
createNode animCurveTA -n "Character1_Spine1_rotateZ";
	rename -uid "78864765-4248-AF65-DDDA-AEB9081C8F12";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 19.853261947631836 1 16.517986297607422
		 2 13.160655975341797 3 10.558413505554199 4 9.0287904739379883 5 8.5652532577514648
		 6 9.3385086059570312 7 10.969979286193848 8 12.415073394775391 9 13.069185256958008
		 10 13.315460205078125 11 12.642788887023926 12 11.085161209106445 13 9.4737939834594727
		 14 8.3494787216186523 15 7.9865932464599618 16 8.5550813674926758 17 9.8840017318725586
		 18 11.649717330932617 19 13.339443206787109 20 14.228630065917969 21 13.805046081542969
		 22 12.593380928039551 23 11.289022445678711 24 10.335568428039551 25 10.001808166503906
		 26 10.578823089599609 27 11.974062919616699 28 13.681537628173828 29 14.629454612731932
		 30 14.138829231262207 31 12.905869483947754 32 11.563984870910645 33 10.564085960388184
		 34 10.195971488952637 35 10.740355491638184 36 12.113800048828125 37 13.983311653137207
		 38 15.778461456298828 39 16.658666610717773 40 16.001777648925781 41 14.397154808044434
		 42 12.673687934875488 43 11.404321670532227 44 10.939563751220703 45 11.273844718933105
		 46 12.095234870910645 47 13.258954048156738 48 14.54624080657959 49 15.609551429748535
		 50 15.977125167846678 51 15.131338119506838 52 13.413827896118164 53 11.559402465820313
		 54 10.075607299804687 55 9.2862796783447266 56 9.4955787658691406 57 10.594752311706543
		 58 12.215139389038086 59 13.749274253845215 60 14.323036193847656 61 13.696547508239746
		 62 12.456742286682129 63 10.909499168395996 64 9.3358144760131836 65 7.9448637962341309
		 66 7.7449951171875 67 7.2116212844848633 68 6.2905912399291992 69 4.5153260231018066
		 70 2.1489477157592773 71 0.10740664601325989 72 -1.0046312808990479 73 -0.87453174591064453
		 74 0.57576173543930054 75 3.2626597881317139 76 6.8523898124694824 77 10.955066680908203
		 78 15.239953994750977 79 19.447725296020508 80 23.346593856811523;
createNode animCurveTU -n "Character1_Spine1_visibility";
	rename -uid "656F3457-434C-EEBC-963F-5DAD488C9100";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftShoulder_translateX";
	rename -uid "A3F2D442-4A0F-875D-A670-CCAD939273E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.0842323303222656;
createNode animCurveTL -n "Character1_LeftShoulder_translateY";
	rename -uid "1E8A55BD-40E4-64CE-3BA0-828DCBF27E41";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.604855537414551;
createNode animCurveTL -n "Character1_LeftShoulder_translateZ";
	rename -uid "A6E19EB0-42A5-A850-352B-18AFADF2BABE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6404602527618408;
createNode animCurveTU -n "Character1_LeftShoulder_scaleX";
	rename -uid "053B540B-4AA4-FAAF-D406-4B97CF9B31FA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftShoulder_scaleY";
	rename -uid "0279888B-4603-26EA-4B5A-07B53D7E52B9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftShoulder_scaleZ";
	rename -uid "B5A298C7-4694-3663-4F08-F6ABF701BCD9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftShoulder_rotateX";
	rename -uid "D955F00B-4613-509A-C680-BF9AE1AFA5E1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 5.2854747772216797 1 6.3775629997253418
		 2 7.7556533813476562 3 8.7785434722900391 4 8.9333076477050781 5 8.2676191329956055
		 6 6.8659706115722656 7 5.1784381866455078 8 3.9421093463897705 9 3.40773606300354
		 10 3.2317979335784912 11 3.5134117603302002 12 4.158536434173584 13 4.8090701103210449
		 14 5.2546281814575195 15 5.458803653717041 16 5.4794759750366211 17 5.3454341888427734
		 18 5.0588231086730957 19 4.730954647064209 20 4.5709309577941895 21 4.7320151329040527
		 22 5.062492847442627 23 5.3517494201660156 24 5.4855923652648926 25 5.4510841369628906
		 26 5.1911864280700684 27 4.6902589797973633 28 4.0824661254882812 29 3.6659798622131348
		 30 3.669360876083374 31 3.9697635173797603 32 4.4280881881713867 33 4.881873607635498
		 34 5.150672435760498 35 5.1167030334472656 36 4.8169898986816406 37 4.3222651481628418
		 38 3.816900491714478 39 3.5817761421203613 40 3.8155069351196285 41 4.3176765441894531
		 42 4.8094344139099121 43 5.1095671653747559 44 5.1436805725097656 45 4.9090137481689453
		 46 4.4924507141113281 47 3.9804515838623047 48 3.4676823616027832 49 3.0666351318359375
		 50 2.9041168689727783 51 3.1326904296875 52 3.6691496372222896 53 4.3002214431762695
		 54 4.8439640998840332 55 5.1436805725097656 56 5.1167030334472656 57 4.8169898986816406
		 58 4.3222651481628418 59 3.816900491714478 60 3.5817761421203613 61 3.8313455581665044
		 62 4.4222750663757324 63 5.11517333984375 64 5.7160520553588867 65 6.1068239212036133
		 66 6.1270637512207031 67 5.9418182373046875 68 5.5066924095153809 69 4.5551505088806152
		 70 3.2321429252624512 71 2.0371754169464111 72 1.2597146034240723 73 0.94607770442962646
		 74 0.96276038885116577 75 1.1153155565261841 76 1.2601323127746582 77 1.3345100879669189
		 78 1.2738627195358276 79 0.99011528491973877 80 0.39329838752746582;
createNode animCurveTA -n "Character1_LeftShoulder_rotateY";
	rename -uid "243CAC1E-48DA-F9F3-EA11-A28A30BD028D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -21.089040756225586 1 -24.802967071533203
		 2 -28.474599838256836 3 -28.958763122558597 4 -25.342252731323242 5 -19.724578857421875
		 6 -13.524412155151367 7 -8.1023130416870117 8 -4.7154865264892578 9 -3.2478785514831543
		 10 -2.6228170394897461 11 -3.3910377025604248 12 -5.6827058792114258 13 -8.6035480499267578
		 14 -11.148487091064453 15 -12.217041969299316 16 -11.275169372558594 17 -9.1057357788085938
		 18 -6.5640778541564941 19 -4.4718203544616699 20 -3.6050627231597896 21 -4.4707374572753906
		 22 -6.5610618591308594 23 -9.1016912460327148 24 -11.271950721740723 25 -12.141952514648438
		 26 -10.674762725830078 27 -7.6059613227844229 28 -4.551602840423584 29 -2.9614450931549072
		 30 -3.3449437618255615 31 -4.8378968238830566 32 -6.7700686454772949 33 -8.4352035522460937
		 34 -9.0448474884033203 35 -8.0314016342163086 36 -5.8846383094787598 37 -3.3842036724090576
		 38 -1.3386498689651489 39 -0.49492406845092779 40 -1.3396890163421631 41 -3.3870396614074707
		 42 -5.8883423805236816 43 -8.0342721939086914 44 -8.968353271484375 45 -8.3928842544555664
		 46 -6.917536735534668 47 -5.0351777076721191 48 -3.197321891784668 49 -1.8117644786834715
		 50 -1.263704776763916 51 -2.0330026149749756 52 -3.897061824798584 53 -6.1829018592834473
		 54 -8.150517463684082 55 -8.968353271484375 56 -8.0314016342163086 57 -5.8846383094787598
		 58 -3.3842036724090576 59 -1.3386498689651489 60 -0.49492406845092779 61 -1.1482149362564087
		 62 -2.8326842784881592 63 -5.1333842277526855 64 -7.5980296134948722 65 -9.7245073318481445
		 66 -9.6148662567138672 67 -9.2454614639282227 68 -8.5398311614990234 69 -6.7721853256225586
		 70 -3.8035581111907959 71 -0.46102815866470337 72 2.4099307060241699 73 3.9873383045196538
		 74 3.5039443969726562 75 0.63463222980499268 76 -4.0386724472045898 77 -9.8986148834228516
		 78 -16.329498291015625 79 -22.710988998413086 80 -28.422317504882812;
createNode animCurveTA -n "Character1_LeftShoulder_rotateZ";
	rename -uid "58E885CB-4E77-0A7E-FA0F-D18D09BC1F38";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -3.372546911239624 1 -6.7657961845397949
		 2 -10.319635391235352 3 -13.243545532226563 4 -15.422143936157228 5 -17.299905776977539
		 6 -18.802087783813477 7 -19.877204895019531 8 -20.341127395629883 9 -20.010055541992188
		 10 -19.049875259399414 11 -15.814298629760742 12 -9.819828987121582 13 -3.0271937847137451
		 14 2.6675775051116943 15 5.407193660736084 16 4.5605096817016602 17 1.6091357469558716
		 18 -2.14434814453125 19 -5.3859724998474121 20 -6.7689657211303711 21 -5.374204158782959
		 22 -2.1092908382415771 23 1.6612156629562378 24 4.6064023971557617 25 5.1918759346008301
		 26 1.2910761833190918 27 -5.991053581237793 28 -13.363463401794434 29 -17.421689987182617
		 30 -16.791631698608398 31 -13.491181373596191 32 -9.1495723724365234 33 -5.3508815765380859
		 34 -3.6082825660705571 35 -4.6665840148925781 36 -7.6098551750183114 37 -11.368409156799316
		 38 -14.627575874328615 39 -16.021518707275391 40 -14.637163162231445 41 -11.396921157836914
		 42 -7.6521248817443857 43 -4.7037625312805176 44 -3.8228054046630859 45 -5.5928587913513184
		 46 -9.1484947204589844 47 -13.584991455078125 48 -17.96681022644043 49 -21.323108673095703
		 50 -22.662063598632813 51 -20.771724700927734 52 -16.253292083740234 53 -10.826322555541992
		 54 -6.1524381637573242 55 -3.8228054046630859 56 -4.6665840148925781 57 -7.6098551750183114
		 58 -11.368409156799316 59 -14.627575874328615 60 -16.021518707275391 61 -15.204480171203613
		 62 -13.105786323547363 63 -10.251303672790527 64 -7.1368355751037598 65 -4.2230334281921387
		 66 -3.7609131336212158 67 -2.8710470199584961 68 -1.4513170719146729 69 1.136736273765564
		 70 4.9951748847961426 71 9.454777717590332 72 13.875658988952637 73 17.592798233032227
		 74 19.899999618530273 75 20.605972290039063 76 20.290960311889648 77 19.417545318603516
		 78 18.415578842163086 79 17.662967681884766 80 17.481019973754883;
createNode animCurveTU -n "Character1_LeftShoulder_visibility";
	rename -uid "7BA34685-43D2-7AB0-8065-1891E3909E03";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftArm_translateX";
	rename -uid "9F76438C-4592-3F73-2267-BCB65183F66F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 9.2092180252075195;
createNode animCurveTL -n "Character1_LeftArm_translateY";
	rename -uid "269BF861-4E76-371A-53C7-F49DAED5C85C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.4172050952911377;
createNode animCurveTL -n "Character1_LeftArm_translateZ";
	rename -uid "32D76AB0-4D04-F917-5486-6D967D1BEFE3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.69030004739761353;
createNode animCurveTU -n "Character1_LeftArm_scaleX";
	rename -uid "5D511A38-4199-434B-7CE9-7FBA15D1A8EA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleY";
	rename -uid "DFA65D9C-40F6-1308-E0AA-8DBEB99F7674";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleZ";
	rename -uid "7D6D8EE2-4FFB-118F-AA12-59BE48D9808B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_LeftArm_rotateX";
	rename -uid "9D9AE894-4EB6-AD42-1EFB-A19B1A76ECDC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 59.225181579589844 1 49.508010864257813
		 2 47.05548095703125 3 46.951160430908203 4 47.713417053222656 5 50.178432464599609
		 6 53.862823486328125 7 57.831096649169922 8 60.744335174560547 9 61.78094482421875
		 10 61.269969940185547 11 58.511245727539055 12 53.517223358154297 13 47.753452301025391
		 14 42.830757141113281 15 40.432506561279297 16 41.210884094238281 17 43.842887878417969
		 18 47.028572082519531 19 49.534580230712891 20 50.261539459228516 21 48.519683837890625
		 22 45.026908874511719 23 40.921886444091797 24 37.475379943847656 25 36.020839691162109
		 26 37.884712219238281 27 42.045562744140625 28 46.287662506103516 29 48.595672607421875
		 30 48.279369354248047 31 46.484642028808594 32 44.142322540283203 33 42.242137908935547
		 34 41.796173095703125 35 43.541851043701172 36 46.76409912109375 37 50.291210174560547
		 38 52.988452911376953 39 53.874710083007812 40 52.324581146240234 41 48.963508605957031
		 42 44.820110321044922 43 41.100727081298828 44 39.09454345703125 45 39.233749389648438
		 46 40.700393676757812 47 42.888778686523438 48 45.189399719238281 49 47.018634796142578
		 50 47.836376190185547 51 46.964019775390625 52 44.695201873779297 53 41.981777191162109
		 54 39.865489959716797 55 39.427188873291016 56 41.475669860839844 57 45.248573303222656
		 58 49.441932678222656 59 52.814224243164062 60 54.284267425537109 61 53.7027587890625
		 62 51.875232696533203 63 49.224441528320313 64 46.210731506347656 65 43.332218170166016
		 66 43.158317565917969 67 42.431449890136719 68 41.037075042724609 69 38.830432891845703
		 70 36.073482513427734 71 33.358367919921875 72 31.215862274169925 73 29.911455154418945
		 74 29.255563735961911 75 28.701274871826172 76 27.990571975708008 77 26.895120620727539
		 78 25.20404052734375 79 22.811031341552734 80 19.948932647705078;
createNode animCurveTA -n "Character1_LeftArm_rotateY";
	rename -uid "0317DBB2-4992-3EC5-549D-8B890BA0C317";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -69.777976989746094 1 -62.978645324707024
		 2 -55.512046813964844 3 -46.809555053710938 4 -35.901538848876953 5 -23.940744400024414
		 6 -12.992422103881836 7 -4.663975715637207 8 0.14369660615921021 9 2.2449285984039307
		 10 3.1438443660736084 11 2.7460486888885498 12 1.4270544052124023 13 0.18915446102619171
		 14 -0.51622998714447021 15 -0.77956646680831909 16 -1.1729986667633057 17 -1.7626582384109497
		 18 -2.0738005638122559 19 -2.030836820602417 20 -1.9528735876083374 21 -2.0680873394012451
		 22 -1.9964209794998169 23 -1.34157395362854 24 -0.19419489800930023 25 0.87603104114532471
		 26 1.4930746555328369 27 2.2157268524169922 28 3.3656899929046631 29 4.3120098114013672
		 30 4.4524950981140137 31 4.1966428756713867 32 3.9426269531250004 33 3.8068370819091797
		 34 3.6160397529602046 35 3.1060526371002197 36 2.5414490699768066 37 2.3608593940734863
		 38 2.5247588157653809 39 2.5822556018829346 40 2.219649076461792 41 1.8515020608901978
		 42 1.9522229433059695 43 2.513338565826416 44 3.0209283828735352 45 3.184434175491333
		 46 3.2562398910522461 47 3.414161205291748 48 3.7278625965118408 49 4.0836944580078125
		 50 4.2208914756774902 51 3.8508148193359375 52 3.1894776821136475 53 2.6338610649108887
		 54 2.2609238624572754 55 1.8438177108764648 56 1.0976834297180176 57 0.34311521053314209
		 58 0.07637680321931839 59 0.28261709213256836 60 0.46528550982475275 61 0.44582927227020264
		 62 0.51152443885803223 63 0.79581844806671143 64 1.3429824113845825 65 2.0124137401580811
		 66 1.8959494829177859 67 1.5569484233856201 68 0.86240488290786743 69 -0.38882866501808167
		 70 -2.530937671661377 71 -5.8483057022094727 72 -10.374542236328125 73 -15.898002624511719
		 74 -22.087924957275391 75 -29.062713623046871 76 -36.943916320800781 77 -45.407291412353516
		 78 -54.137523651123047 79 -62.841342926025391 80 -71.275894165039063;
createNode animCurveTA -n "Character1_LeftArm_rotateZ";
	rename -uid "7445DFFA-40AA-ED40-CE65-629363307656";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -77.858016967773438 1 -66.156295776367187
		 2 -60.151088714599609 3 -55.371623992919922 4 -49.973808288574219 5 -44.171566009521484
		 6 -37.948589324951172 7 -31.804658889770504 8 -27.008068084716797 9 -23.952022552490234
		 10 -21.864124298095703 11 -19.607776641845703 12 -16.498128890991211 13 -13.172854423522949
		 14 -10.429592132568359 15 -9.0976667404174805 16 -10.020516395568848 17 -12.809260368347168
		 18 -16.346891403198242 19 -19.369876861572266 20 -20.55027961730957 21 -19.011825561523438
		 22 -15.619919776916502 23 -11.700949668884277 24 -8.5102624893188477 25 -7.139195442199707
		 26 -8.6974067687988281 27 -12.342379570007324 28 -16.095067977905273 29 -17.958389282226562
		 30 -17.172697067260742 31 -14.932230949401855 32 -12.231133460998535 33 -10.070708274841309
		 34 -9.446253776550293 35 -11.137289047241211 36 -14.563879013061522 37 -18.606155395507813
		 38 -22.003030776977539 39 -23.46003532409668 40 -22.12071418762207 41 -18.841464996337891
		 42 -14.948185920715332 43 -11.727664947509766 44 -10.309895515441895 45 -10.943602561950684
		 46 -12.802103996276855 47 -15.308597564697266 48 -17.87663459777832 49 -19.911584854125977
		 50 -20.830575942993164 51 -19.94285774230957 52 -17.611509323120117 53 -14.847127914428709
		 54 -12.664288520812988 55 -12.058320045471191 56 -13.80660343170166 57 -17.333272933959961
		 58 -21.502887725830078 59 -25.027904510498047 60 -26.591009140014648 61 -25.408246994018555
		 62 -22.195154190063477 63 -17.929956436157227 64 -13.587301254272461 65 -10.10189151763916
		 66 -10.251533508300781 67 -11.30793285369873 68 -13.541105270385742 69 -17.973117828369141
		 70 -24.694404602050781 71 -32.562046051025391 72 -40.471408843994141 73 -47.381904602050781
		 74 -52.239330291748047 75 -54.733570098876953 76 -55.570247650146484 77 -55.034042358398438
		 78 -53.338024139404297 79 -50.692634582519531 80 -47.549301147460937;
createNode animCurveTU -n "Character1_LeftArm_visibility";
	rename -uid "A363F03F-428B-ADE6-0590-15B645437BE4";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftForeArm_translateX";
	rename -uid "B51F5305-4ADD-E11D-02B2-85ACA2FB1D3D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.0427684783935547;
createNode animCurveTL -n "Character1_LeftForeArm_translateY";
	rename -uid "14C09B6F-4CDA-D264-5E1D-4E9D68184FA7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.323208808898926;
createNode animCurveTL -n "Character1_LeftForeArm_translateZ";
	rename -uid "6D3A191A-47C1-B2D3-7B2C-299712918615";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 16.678167343139648;
createNode animCurveTU -n "Character1_LeftForeArm_scaleX";
	rename -uid "D465D7ED-49E2-DC3D-770B-6EB3887E58AB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftForeArm_scaleY";
	rename -uid "601B4615-4CE2-10F7-680C-F5874B1ED99D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftForeArm_scaleZ";
	rename -uid "A42DE652-421A-6937-E3F5-D7B29DDEADB1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftForeArm_rotateX";
	rename -uid "1ED5C7CB-43D0-15E2-CD9A-0CAC52A248A1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -16.628414154052734 1 -23.381328582763672
		 2 -30.340457916259769 3 -35.580913543701172 4 -38.86993408203125 5 -41.864448547363281
		 6 -44.969776153564453 7 -48.419807434082031 8 -52.14093017578125 9 -55.608375549316406
		 10 -58.411571502685547 11 -60.512504577636726 12 -62.087882995605469 13 -63.368309020996094
		 14 -64.645156860351563 15 -66.254257202148438 16 -67.590362548828125 17 -68.057884216308594
		 18 -67.994468688964844 19 -67.782501220703125 20 -67.929672241210937 21 -68.804046630859375
		 22 -69.891624450683594 23 -70.564399719238281 24 -70.388679504394531 25 -69.065109252929687
		 26 -67.23870849609375 27 -65.727928161621094 28 -64.495429992675781 29 -63.517036437988288
		 30 -62.702445983886719 31 -62.000514984130866 32 -61.456977844238281 33 -61.123477935791016
		 34 -61.06219482421875 35 -60.432796478271491 36 -58.497524261474616 37 -55.491741180419922
		 38 -51.95001220703125 39 -48.983291625976562 40 -47.754047393798828 41 -47.728118896484375
		 42 -47.878700256347656 43 -47.43414306640625 44 -45.793689727783203 45 -43.537399291992188
		 46 -41.636062622070313 47 -40.040973663330078 48 -38.720916748046875 49 -37.657794952392578
		 50 -36.852527618408203 51 -36.305641174316406 52 -36.019382476806641 53 -35.987949371337891
		 54 -36.21417236328125 55 -36.713356018066406 56 -36.257411956787109 57 -33.967136383056641
		 58 -30.453327178955075 59 -26.855491638183594 60 -25.147151947021484 61 -26.320789337158203
		 62 -29.027078628540039 63 -32.122463226318359 64 -34.878227233886719 65 -36.94830322265625
		 66 -36.94830322265625 67 -36.94830322265625 68 -36.94830322265625 69 -33.254058837890625
		 70 -28.178361892700195 71 -25.131870269775391 72 -23.561916351318359 73 -22.731203079223633
		 74 -22.100774765014648 75 -21.295944213867188 76 -20.313833236694336 77 -19.313945770263672
		 78 -18.379474639892578 79 -17.50599479675293 80 -16.661149978637695;
createNode animCurveTA -n "Character1_LeftForeArm_rotateY";
	rename -uid "8358B6B5-49DE-CE68-9927-04843C74E0B1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -4.6888589859008789 1 4.2239675521850586
		 2 13.964154243469238 3 22.980216979980469 4 31.183797836303714 5 39.234325408935547
		 6 46.566566467285156 7 52.595161437988281 8 56.682510375976562 9 59.008388519287109
		 10 60.368804931640625 11 61.06953430175782 12 61.393795013427734 13 61.555484771728523
		 14 61.749187469482422 15 62.153026580810554 16 63.142509460449219 17 64.708816528320312
		 18 66.419601440429688 19 67.79443359375 20 68.311622619628906 21 67.637382507324219
		 22 66.15228271484375 23 64.385826110839844 24 62.805328369140618 25 61.80632019042968
		 26 61.305484771728509 27 60.981651306152344 28 60.840232849121087 29 60.883773803710938
		 30 61.069538116455078 31 61.367546081542962 32 61.797939300537102 33 62.380645751953132
		 34 63.134548187255852 35 64.444602966308594 36 66.412376403808594 37 68.575233459472656
		 38 70.382843017578125 39 71.25567626953125 40 70.802833557128906 41 69.369972229003906
		 42 67.499320983886719 43 65.731910705566406 44 64.60040283203125 45 64.041458129882813
		 46 63.634410858154297 47 63.363376617431648 48 63.208637237548828 49 63.147727966308587
		 50 63.155719757080071 51 63.245346069335945 52 63.465850830078132 53 63.846168518066406
		 54 64.4141845703125 55 65.196540832519531 56 66.575233459472656 57 68.559364318847656
		 58 70.593399047851562 59 72.142623901367188 60 72.750633239746094 61 72.226417541503906
		 62 70.864944458007813 63 68.993431091308594 64 66.972503662109375 65 65.183647155761719
		 66 65.183647155761719 67 65.183647155761719 68 65.183647155761719 69 59.490665435791016
		 70 44.700332641601563 71 24.673599243164063 72 3.5693414211273193 73 -14.442235946655273
		 74 -25.213031768798828 75 -28.089248657226562 76 -26.213018417358398 77 -21.147823333740234
		 78 -14.470040321350099 79 -7.7648053169250488 80 -2.6207170486450195;
createNode animCurveTA -n "Character1_LeftForeArm_rotateZ";
	rename -uid "CFC71D44-42D4-E595-1553-27931717CF13";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 25.404184341430664 1 34.494834899902344
		 2 41.457756042480469 3 43.937870025634766 4 42.424888610839844 5 38.799396514892578
		 6 33.972724914550781 7 28.822128295898438 8 24.386014938354492 9 21.124589920043945
		 10 18.736804962158203 11 17.189401626586914 12 16.230861663818359 13 15.561774253845215
		 14 14.818980216979982 15 13.592226982116699 16 11.488463401794434 17 8.838165283203125
		 18 6.3063840866088867 19 4.4499483108520508 20 3.6405563354492187 21 4.0799045562744141
		 22 5.6629829406738281 23 8.0856847763061523 24 10.781097412109375 25 13.025959014892578
		 26 14.556602478027344 27 15.612628936767576 28 16.247331619262695 29 16.501708984375
		 30 16.493534088134766 31 16.297090530395508 32 15.877867698669434 33 15.196026802062987
		 34 14.201727867126465 35 12.712992668151855 36 11.092920303344727 37 10.08830451965332
		 38 10.123611450195312 39 10.969524383544922 40 12.034548759460449 41 13.275199890136719
		 42 14.851737976074217 43 16.62053108215332 44 18.213283538818359 45 19.422981262207031
		 46 20.324079513549805 47 20.988519668579102 48 21.469902038574219 49 21.80784797668457
		 50 22.02899169921875 51 22.132837295532227 52 22.095588684082031 53 21.902572631835938
		 54 21.530191421508789 55 20.942129135131836 56 20.34344482421875 57 20.253202438354492
		 58 21.006305694580078 59 22.344223022460938 60 23.118200302124023 61 22.604228973388672
		 62 21.603620529174805 63 20.852266311645508 64 20.648435592651367 65 20.869100570678711
		 66 20.869100570678711 67 20.869100570678711 68 20.869100570678711 69 24.681516647338867
		 70 29.993104934692383 71 33.231346130371094 72 34.820320129394531 73 35.314231872558594
		 74 34.883430480957031 75 33.615882873535156 76 31.862346649169922 77 29.994678497314453
		 78 28.256853103637695 79 26.74799919128418 80 25.457981109619141;
createNode animCurveTU -n "Character1_LeftForeArm_visibility";
	rename -uid "F07414F1-4A24-7D4A-9BB5-0BBFA61EB8F7";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHand_translateX";
	rename -uid "59DE66B5-47F9-0511-3AF2-609C365FF20E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.29200950264930725;
createNode animCurveTL -n "Character1_LeftHand_translateY";
	rename -uid "B4D1642A-4953-DDAA-ED41-57A6B6D248CA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -14.295157432556152;
createNode animCurveTL -n "Character1_LeftHand_translateZ";
	rename -uid "2F05CE87-4CC9-63D6-F12F-9E93D1133051";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6319241523742676;
createNode animCurveTU -n "Character1_LeftHand_scaleX";
	rename -uid "88CF7B38-4656-90D2-0528-8DB8112FFE2D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHand_scaleY";
	rename -uid "55135F80-4241-370F-BCDD-E6A2C5E52991";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHand_scaleZ";
	rename -uid "F872E55C-43F9-1367-74AF-C6B258824FB9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHand_rotateX";
	rename -uid "55E41C78-4A41-31F9-AB8D-25A107D75933";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -7.0681729994248599e-005 1 1.5511144399642944
		 2 2.4634294509887695 3 2.6747303009033203 4 2.331195592880249 5 1.4673093557357788
		 6 0.11050593852996826 7 -1.5650376081466675 8 -3.360764741897583 9 -5.3846421241760254
		 10 -7.7224111557006845 11 -10.166009902954102 12 -12.407462120056152 13 -14.051937103271484
		 14 -14.663170814514162 15 -13.824575424194336 16 -11.314122200012207 17 -7.8622121810913095
		 18 -4.4454684257507324 19 -1.8736878633499146 20 -0.85832738876342773 21 -1.8480993509292605
		 22 -4.3662457466125488 23 -7.7381014823913583 24 -11.19870662689209 25 -13.98671817779541
		 26 -15.90367603302002 27 -17.368173599243164 28 -18.526491165161133 29 -19.54864501953125
		 30 -20.589282989501953 31 -21.517124176025391 32 -22.071557998657227 33 -21.981439590454102
		 34 -20.987850189208984 35 -18.355806350708008 36 -14.490103721618652 37 -10.675700187683105
		 38 -7.816075325012207 39 -6.69024658203125 40 -7.8391103744506836 41 -10.747397422790527
		 42 -14.603074073791506 43 -18.461275100708008 44 -21.168136596679688 45 -22.567319869995117
		 46 -23.424238204956055 47 -23.857799530029297 48 -23.999221801757813 49 -23.984376907348633
		 50 -23.949350357055664 51 -23.965351104736328 52 -23.885051727294922 53 -23.515449523925781
		 54 -22.667518615722656 55 -21.168136596679688 56 -18.355806350708008 57 -14.490103721618652
		 58 -10.675700187683105 59 -7.816075325012207 60 -6.69024658203125 61 -7.6425771713256836
		 62 -10.133644104003906 63 -13.631039619445801 64 -17.547229766845703 65 -21.168136596679688
		 66 -21.168136596679688 67 -21.168136596679688 68 -21.168136596679688 69 -22.194553375244141
		 70 -23.649955749511719 71 -23.061166763305664 72 -20.258502960205078 73 -17.920370101928711
		 74 -17.34556770324707 75 -17.124300003051758 76 -16.012475967407227 77 -13.329567909240723
		 78 -9.0124502182006836 79 -3.5596144199371338 80 2.0614807605743408;
createNode animCurveTA -n "Character1_LeftHand_rotateY";
	rename -uid "8DF9FB29-493F-E73E-9EE5-4E8E7BFA6013";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 1.0467841038419579e-009 1 -5.8198995590209961
		 2 -11.178683280944824 3 -15.819419860839844 4 -19.891666412353516 5 -23.369997024536133
		 6 -26.017152786254883 7 -27.881158828735352 8 -29.261465072631836 9 -30.671258926391602
		 10 -32.299583435058594 11 -34.064357757568359 12 -35.749515533447266 13 -37.05377197265625
		 14 -37.671676635742187 15 -37.2774658203125 16 -35.68719482421875 17 -33.32098388671875
		 18 -30.776378631591797 19 -28.725812911987305 20 -27.883678436279297 21 -28.705869674682621
		 22 -30.717561721801754 23 -33.234977722167969 24 -35.612945556640625 25 -37.370594024658203
		 26 -38.466480255126953 27 -39.232288360595703 28 -39.80352783203125 29 -40.302146911621094
		 30 -40.812900543212891 31 -41.253574371337891 32 -41.498489379882813 33 -41.433116912841797
		 34 -40.939353942871094 35 -39.604701995849609 36 -37.472957611083984 37 -35.146526336669922
		 38 -33.250034332275391 39 -32.466297149658203 40 -33.265281677246094 41 -35.191070556640625
		 42 -37.537185668945313 43 -39.659275054931641 44 -41.019775390625 45 -41.681774139404297
		 46 -42.074359893798828 47 -42.268932342529297 48 -42.330001831054688 49 -42.320163726806641
		 50 -42.30267333984375 51 -42.311859130859375 52 -42.278976440429688 53 -42.114395141601563
		 54 -41.727645874023438 55 -41.019775390625 56 -39.604701995849609 57 -37.472957611083984
		 58 -35.146526336669922 59 -33.250034332275391 60 -32.466297149658203 61 -33.126167297363281
		 62 -34.785594940185547 63 -36.955059051513672 64 -39.168319702148437 65 -41.019775390625
		 66 -41.019775390625 67 -41.019775390625 68 -41.019775390625 69 -42.702213287353516
		 70 -46.376052856445313 71 -49.542797088623047 72 -49.883350372314453 73 -46.805648803710938
		 74 -41.625057220458984 75 -35.218547821044922 76 -27.777494430541992 77 -19.871280670166016
		 78 -12.07387638092041 79 -4.8736433982849121 80 1.4979898929595947;
createNode animCurveTA -n "Character1_LeftHand_rotateZ";
	rename -uid "5DC425B6-4665-40B0-02C5-17B9CC81556B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -0.0001194589349324815 1 5.7141194343566895
		 2 11.674528121948242 3 17.569620132446289 4 23.83079719543457 5 30.744400024414063
		 6 37.617279052734375 7 43.670368194580078 8 48.135917663574219 9 51.169784545898437
		 10 53.535400390625 11 55.462860107421875 12 57.023250579833984 13 58.072902679443359
		 14 58.408027648925774 15 57.846542358398445 16 56.23016357421875 17 54.009689331054688
		 18 51.8782958984375 19 50.329490661621094 20 49.732013702392578 21 50.3162841796875
		 22 51.835483551025391 23 53.938796997070313 24 56.160873413085938 25 57.991622924804687
		 26 59.194194793701172 27 60.041973114013672 28 60.752895355224609 29 61.569049835205078
		 30 62.633453369140618 31 63.744323730468743 32 64.623176574707031 33 64.980934143066406
		 34 64.542472839355469 35 62.765674591064453 36 60.082862854003899 37 57.501506805419929
		 38 55.622055053710938 39 54.89630126953125 40 55.635349273681641 41 57.544563293457031
		 42 60.15394592285157 43 62.834812164306648 44 64.717750549316406 45 65.604576110839844
		 46 66.039314270019531 47 66.140869140625 48 66.041839599609375 49 65.880477905273438
		 50 65.795890808105469 51 65.891799926757813 52 66.038894653320313 53 66.026290893554688
		 54 65.648468017578125 55 64.717750549316406 56 62.765674591064453 57 60.082862854003899
		 58 57.501506805419929 59 55.622055053710938 60 54.89630126953125 61 55.496963500976563
		 62 57.101207733154297 63 59.429821014404297 64 62.134445190429688 65 64.717750549316406
		 66 64.717750549316406 67 64.717750549316406 68 64.717750549316406 69 61.394702911376953
		 70 51.646743774414063 71 35.746463775634766 72 16.40216064453125 73 -0.59493309259414673
		 74 -10.41213321685791 75 -13.40632152557373 76 -12.924246788024902 77 -10.432229042053223
		 78 -6.9513630867004395 79 -3.2831299304962158 80 -0.22492891550064084;
createNode animCurveTU -n "Character1_LeftHand_visibility";
	rename -uid "6ABC4FEC-4431-8A70-D3D4-DFAE4166CC47";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateX";
	rename -uid "12D6D554-429D-ECD9-699A-328FDAF95F2D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1364259719848633;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateY";
	rename -uid "D573AFEC-405D-3CA7-FAA2-59ADBAF72976";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4241994619369507;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateZ";
	rename -uid "2FA1C1B1-4C7A-2EF5-4D77-D2BF12BFD902";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.7211661338806152;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleX";
	rename -uid "97646419-451E-9306-E1C0-EBA789405D05";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleY";
	rename -uid "342130A3-494C-4F24-3687-3490F37ED7D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleZ";
	rename -uid "619CEDF2-4382-5515-7A16-298DC9FB42C5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateX";
	rename -uid "F0FD1EB5-421D-6C77-8B85-6386E9FA86F6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -28.371952056884766 1 -32.054462432861328
		 2 -35.002529144287109 3 -35.857170104980469 4 -35.857170104980469 5 -35.857170104980469
		 6 -35.857170104980469 7 -35.339164733886719 8 -33.254081726074219 9 -29.656011581420902
		 10 -25.54676628112793 11 -22.133428573608398 12 -20.49278450012207 13 -20.269712448120117
		 14 -20.097232818603516 15 -20.074281692504883 16 -20.300327301025391 17 -20.686216354370117
		 18 -21.065467834472656 19 -21.323396682739258 20 -21.388521194458008 21 -21.2088623046875
		 22 -20.807010650634766 23 -20.240911483764648 24 -19.631301879882812 25 -19.167839050292969
		 26 -18.979715347290039 27 -18.984209060668945 28 -19.073843002319336 29 -19.139383316040039
		 30 -19.126413345336914 31 -19.086277008056641 32 -19.071796417236328 33 -19.135927200317383
		 34 -19.32795524597168 35 -19.705734252929688 36 -20.203897476196289 37 -20.69597053527832
		 38 -21.091886520385742 39 -21.341461181640625 40 -21.462396621704102 41 -21.5147705078125
		 42 -21.526033401489258 43 -21.518760681152344 44 -21.514501571655273 45 -21.506067276000977
		 46 -21.478656768798828 47 -21.442573547363281 48 -21.408737182617188 49 -21.388862609863281
		 50 -21.394912719726563 51 -21.473321914672852 52 -21.609052658081055 53 -21.731050491333008
		 54 -21.787637710571289 55 -21.732481002807617 56 -21.466196060180664 57 -20.935832977294922
		 58 -20.208267211914062 59 -19.47926139831543 60 -19.050754547119141 61 -19.085638046264648
		 62 -19.427221298217773 63 -19.911666870117188 64 -20.390834808349609 65 -20.756132125854492
		 66 -20.756132125854492 67 -20.756132125854492 68 -20.756132125854492 69 -20.864669799804688
		 70 -21.170375823974609 71 -21.642421722412109 72 -22.249319076538086 73 -22.959632873535156
		 74 -23.74247932434082 75 -24.567844390869141 76 -25.406854629516602 77 -26.231952667236328
		 78 -27.017099380493164 79 -27.737945556640625 80 -28.371952056884766;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateY";
	rename -uid "66BD84B0-4AF0-8E11-E9F6-2DB6CF4E85AA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 26.454395294189453 1 39.612323760986328
		 2 52.727191925048828 3 58.664314270019524 4 58.664314270019524 5 58.664314270019524
		 6 58.664314270019524 7 54.369869232177734 8 43.595909118652344 9 29.541488647460938
		 10 15.454508781433104 11 4.4385719299316406 12 -0.22889250516891479 13 -0.61072170734405518
		 14 -0.88823395967483521 15 -0.92409998178482056 16 -0.55992108583450317 17 0.12631845474243164
		 18 0.90425354242324818 19 1.515200138092041 20 1.683274507522583 21 1.2339749336242676
		 22 0.36124387383460999 23 -0.65807515382766724 24 -1.5732768774032593 25 -2.1777725219726562
		 26 -2.4048089981079102 27 -2.3994996547698975 28 -2.2924320697784424 29 -2.2127463817596436
		 30 -2.2286109924316406 31 -2.2774066925048828 32 -2.2949020862579346 33 -2.2169787883758545
		 34 -1.9765647649765012 35 -1.4695217609405518 36 -0.7183232307434082 37 0.14489254355430603
		 38 0.96331048011779774 39 1.5611929893493652 40 1.882079601287842 41 2.0288355350494385
		 42 2.0610761642456055 43 2.0402252674102783 44 2.0280683040618896 45 2.0040895938873291
		 46 1.927100658416748 47 1.8278292417526243 48 1.736782431602478 49 1.6841752529144287
		 50 1.7001229524612427 51 1.9122776985168457 52 2.3068838119506836 53 2.6983892917633057
		 54 2.894902229309082 55 2.7032320499420166 56 1.8925569057464602 57 0.62444609403610229
		 58 -0.71124500036239624 59 -1.77919602394104 60 -2.3202190399169922 61 -2.2781786918640137
		 62 -1.8479026556015015 63 -1.1715898513793945 64 -0.40682670474052429 65 0.26097205281257629
		 66 0.26097205281257629 67 0.26097205281257629 68 0.26097205281257629 69 0.60994279384613037
		 70 1.5961339473724365 71 3.1284279823303223 72 5.1156678199768066 73 7.4666719436645508
		 74 10.090248107910156 75 12.895224571228027 76 15.790465354919434 77 18.684911727905273
		 78 21.487594604492187 79 24.107662200927734 80 26.454395294189453;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateZ";
	rename -uid "2462CA8D-4689-2A0A-97C8-93A1F9E63B13";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -6.3497772216796875 1 -7.6943340301513672
		 2 -8.3225793838500977 3 -8.1192960739135742 4 -8.1192960739135742 5 -8.1192960739135742
		 6 -8.1192960739135742 7 -8.4033918380737305 8 -8.3224096298217773 9 -7.3555083274841309
		 10 -5.9619846343994141 11 -5.4533662796020508 12 -6.5566287040710449 13 -8.3572044372558594
		 14 -9.6886157989501953 15 -9.8621845245361328 16 -8.1156215667724609 17 -4.9110932350158691
		 18 -1.39336097240448 19 1.2990273237228394 20 2.0304088592529297 21 0.066575199365615845
		 22 -3.8370239734649663 23 -8.5829782485961914 24 -13.069047927856445 25 -16.182579040527344
		 26 -17.389137268066406 27 -17.360666275024414 28 -16.789176940917969 29 -16.367023468017578
		 30 -16.450857162475586 31 -16.709369659423828 32 -16.802303314208984 33 -16.389377593994141
		 34 -15.131009101867676 35 -12.547698974609375 36 -8.8710565567016602 37 -4.825775146484375
		 38 -1.1306184530258179 39 1.4995404481887817 40 2.890812873840332 41 3.5228486061096191
		 42 3.6613593101501465 43 3.5717942714691162 44 3.5195510387420654 45 3.4164535999298096
		 46 3.0849792957305908 47 2.6565141677856445 48 2.2624747753143311 49 2.0343186855316162
		 50 2.1035206317901611 51 3.0210778713226318 52 4.7135343551635742 53 6.376251220703125
		 54 7.2052927017211914 55 6.396723747253418 56 2.9360213279724121 57 -2.6460826396942139
		 58 -8.8371639251708984 59 -14.114591598510742 60 -16.937017440795898 61 -16.713470458984375
		 62 -14.466786384582521 63 -11.069750785827637 64 -7.3914022445678711 65 -4.2941398620605469
		 66 -4.2941398620605469 67 -4.2941398620605469 68 -4.2941398620605469 69 -4.3227052688598633
		 70 -4.4036016464233398 71 -4.5296826362609863 72 -4.6935820579528809 73 -4.8874421119689941
		 74 -5.1028084754943848 75 -5.3307137489318848 76 -5.5619325637817383 77 -5.7873687744140625
		 78 -5.9985237121582031 79 -6.1879744529724121 80 -6.3497772216796875;
createNode animCurveTU -n "Character1_LeftHandThumb1_visibility";
	rename -uid "8D27E46E-432F-0DD9-C3AD-CFB686A54E2C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateX";
	rename -uid "7DFF61E2-4C04-5461-02F8-39B2B0F44DCD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.8329257965087891;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateY";
	rename -uid "BECB8706-4C8F-FE14-7676-5084E66BE229";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.094426639378070831;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateZ";
	rename -uid "21D0D979-4844-4740-4D6E-D09964B878F7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5438374280929565;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleX";
	rename -uid "8475FD6D-4DA9-31AA-E948-87971BF26412";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleY";
	rename -uid "E6742290-4239-76E4-CF53-E5A89A75E930";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleZ";
	rename -uid "093E5A1F-4E12-0ADC-3D2A-17937EB7B513";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateX";
	rename -uid "2D21BA58-46A7-F2A2-3F39-B9A192A93E9F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 10.355228424072266 1 6.4095683097839355
		 2 -1.3807446956634521 3 -7.0229120254516602 4 -7.0229120254516602 5 -7.0229120254516602
		 6 -7.0229120254516602 7 -8.6403465270996094 8 -11.3565673828125 9 -12.739910125732422
		 10 -12.397720336914063 11 -11.112886428833008 12 -10.117956161499023 13 -9.7390518188476563
		 14 -9.465240478515625 15 -9.4299383163452148 16 -9.7893104553222656 17 -10.473066329956055
		 18 -11.261456489562988 19 -11.892958641052246 20 -12.068882942199707 21 -11.600776672363281
		 22 -10.709500312805176 23 -9.6922435760498047 24 -8.7937784194946289 25 -8.2048149108886719
		 26 -7.98406982421875 27 -7.9892311096191406 28 -8.0933151245117187 29 -8.1708011627197266
		 30 -8.1553726196289062 31 -8.1079244613647461 32 -8.0909128189086914 33 -8.1666841506958008
		 34 -8.4006071090698242 35 -8.8951425552368164 36 -9.6327409744262695 37 -10.491713523864746
		 38 -11.321988105773926 39 -11.941000938415527 40 -12.278300285339355 41 -12.43385124206543
		 42 -12.468136787414551 43 -12.445959091186523 44 -12.433036804199219 45 -12.407564163208008
		 46 -12.325931549072266 47 -12.22100830078125 48 -12.125102996826172 49 -12.069828033447266
		 50 -12.086573600769043 51 -12.310238838195801 52 -12.73090934753418 53 -13.154726028442383
		 54 -13.370057106018066 55 -13.160011291503906 56 -12.289377212524414 57 -10.976035118103027
		 58 -9.6397275924682617 59 -8.5928773880004883 60 -8.0662965774536133 61 -8.1071710586547852
		 62 -8.5259170532226562 63 -9.1868228912353516 64 -9.9410486221313477 65 -10.60842227935791
		 66 -10.60842227935791 67 -10.60842227935791 68 -10.60842227935791 69 -10.269748687744141
		 70 -9.3107767105102539 71 -7.81870412826538 72 -5.8901004791259766 73 -3.6383218765258785
		 74 -1.1954727172851562 75 1.293689489364624 76 3.68567967414856 77 5.8560938835144043
		 78 7.7152271270751962 79 9.2164068222045898 80 10.355228424072266;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateY";
	rename -uid "C5032562-43E7-CD15-E828-E694C32E524A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 20.610536575317383 1 34.901676177978516
		 2 48.955757141113281 3 55.072174072265625 4 55.072174072265625 5 55.072174072265625
		 6 55.072174072265625 7 52.952327728271484 8 47.368976593017578 9 39.728824615478516
		 10 31.888208389282223 11 26.473066329956055 12 25.563249588012695 13 27.273099899291992
		 14 28.538141250610352 15 28.703094482421875 16 27.043621063232422 17 24.001808166503906
		 18 20.668685913085938 19 18.123239517211914 20 17.43278694152832 21 19.287734985351563
		 22 22.983343124389648 23 27.487581253051758 24 31.752035140991211 25 34.713649749755859
		 26 35.861480712890625 27 35.834392547607422 28 35.290718078613281 29 34.889114379882813
		 30 34.968864440917969 31 35.214794158935547 32 35.303203582763672 33 34.910377502441406
		 34 33.713298797607422 35 31.2562255859375 36 27.761272430419922 37 23.920883178710938
		 38 20.420042037963867 39 17.93389892578125 40 16.621147155761719 41 16.02537727355957
		 42 15.894866943359375 43 15.979257583618164 44 16.028484344482422 45 16.125640869140625
		 46 16.438079833984375 47 16.842098236083984 48 17.21380615234375 49 17.429098129272461
		 50 17.363792419433594 51 16.498325347900391 52 14.904101371765135 53 13.34087085723877
		 54 12.562636375427246 55 13.321642875671387 56 16.578519821166992 57 21.85481071472168
		 58 27.729070663452148 59 32.746463775634766 60 35.431362152099609 61 35.218696594238281
		 62 33.081466674804688 63 29.8509407043457 64 26.355810165405273 65 23.416719436645508
		 66 23.416719436645508 67 23.416719436645508 68 23.416719436645508 69 23.552349090576172
		 70 23.908443450927734 71 24.381643295288086 72 24.8497314453125 73 25.191963195800781
		 74 25.307712554931641 75 25.131710052490234 76 24.643444061279297 77 23.868703842163086
		 78 22.873287200927734 79 21.751136779785156 80 20.610536575317383;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateZ";
	rename -uid "B4A73AFA-4A03-C00C-90A0-AC855DC9EA0A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -8.5256719589233398 1 -14.462574005126953
		 2 -22.523147583007813 3 -27.805004119873047 4 -27.805004119873047 5 -27.805004119873047
		 6 -27.805004119873047 7 -30.786781311035156 8 -37.129501342773437 9 -43.729549407958984
		 10 -49.249031066894531 11 -53.302936553955078 12 -55.199928283691406 13 -55.658248901367187
		 14 -55.993282318115234 15 -56.036727905273438 16 -55.597114562988281 17 -54.775402069091797
		 18 -53.847789764404297 19 -53.117294311523437 20 -52.915512084960938 21 -53.454032897949219
		 22 -54.495185852050781 23 -55.715286254882812 24 -56.830730438232422 25 -57.587760925292962
		 26 -57.878074645996094 27 -57.871242523193359 28 -57.733909606933594 29 -57.632240295410156
		 30 -57.652446746826179 31 -57.714702606201165 32 -57.737064361572266 33 -57.637630462646484
		 34 -57.333427429199226 35 -56.702713012695312 36 -55.787929534912109 37 -54.75323486328125
		 38 -53.777324676513672 39 -53.062122344970703 40 -52.676204681396484 41 -52.499053955078125
		 42 -52.460075378417969 43 -52.485286712646484 44 -52.499980926513672 45 -52.528957366943359
		 46 -52.621906280517578 47 -52.741580963134766 48 -52.851173400878906 49 -52.9144287109375
		 50 -52.895256042480469 51 -52.639785766601562 52 -52.162090301513672 53 -51.684211730957031
		 54 -51.442611694335938 55 -51.678272247314453 56 -52.663570404052734 57 -54.181434631347656
		 58 -55.779388427734375 59 -57.086326599121094 60 -57.769466400146484 61 -57.715690612792969
		 62 -57.172096252441406 63 -56.337608337402344 64 -55.413188934326172 65 -54.614753723144531
		 66 -54.614753723144531 67 -54.614753723144531 68 -54.614753723144531 69 -54.036197662353516
		 70 -52.386562347412109 71 -49.783744812011719 72 -46.345710754394531 73 -42.20611572265625
		 74 -37.523033142089844 75 -32.478469848632813 76 -27.269180297851562 77 -22.092781066894531
		 78 -17.135066986083984 79 -12.563161849975586 80 -8.5256719589233398;
createNode animCurveTU -n "Character1_LeftHandThumb2_visibility";
	rename -uid "703D403D-42DA-867B-3A8D-509E8BF04DF0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateX";
	rename -uid "7B954A82-426D-1FF6-7BBA-5EAFB66B7715";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7747817039489746;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateY";
	rename -uid "9CDCFB1F-4F0B-5F82-7A8C-068DCF786786";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3182404041290283;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateZ";
	rename -uid "BEE87616-46CD-2B99-98AD-93B809BB7F58";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.26324462890625;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleX";
	rename -uid "EFE6013B-4291-EF41-1949-F38E85924637";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleY";
	rename -uid "51336AFE-4C24-D443-C956-6B9626BDECE6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleZ";
	rename -uid "199FE4A3-4D45-818C-4F48-10939F6DDB84";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateX";
	rename -uid "3F382245-4B7A-D9A0-137C-FC8C7386D652";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 57 ".ktv[0:56]"  9 3.8451270256700809e-007 10 4.4654316866399313e-007
		 11 -0.040827270597219467 12 -0.14433109760284424 13 -0.27659317851066589 14 -0.38448011875152588
		 15 -0.39918461441993713 16 -0.25794166326522827 17 -0.036964770406484604 18 0.14999070763587952
		 19 0.25428104400634766 20 0.27681142091751099 21 0.21070675551891327 22 0.026238810271024704
		 23 -0.29427981376647949 24 -0.69798415899276733 25 -1.0390405654907227 26 -1.1853311061859131
		 27 -1.1817854642868042 28 -1.111577033996582 29 -1.0608813762664795 30 -1.0708705186843872
		 31 -1.1019175052642822 32 -1.1131693124771118 33 -1.0635414123535156 34 -0.91806316375732411
		 35 -0.64585554599761963 36 -0.31720677018165588 37 -0.031746458262205124 38 0.16164591908454895
		 39 0.26070475578308105 40 0.30013579130172729 41 0.31507307291030884 42 0.31809744238853455
		 44 0.31499999761581421 45 0.31268906593322754 46 0.30492293834686279 47 0.29412549734115601
		 48 0.28344139456748962 49 0.27692520618438721 50 0.27892720699310303 51 0.30336683988571167
		 52 0.33813828229904175 53 0.35917210578918457 54 0.36474722623825073 55 0.35934934020042419
		 56 0.30126610398292542 57 0.090018481016159058 58 -0.31448847055435181 59 -0.8067777156829834
		 60 -1.1295648813247681 61 -1.1024131774902344 62 -0.84471702575683594 63 -0.50562030076980591
		 64 -0.20371513068675995 65 5.1500597919584834e-007 66 5.1214561835877248e-007;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateY";
	rename -uid "2B98E5CD-4086-FB10-06C8-E983A85ABD7F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -9.8205845233678701e-008 10 -1.1565393975843107e-007
		 11 0.67418491840362549 12 2.242668628692627 13 4.0246014595031738 14 5.3404192924499512
		 15 5.5118355751037598 16 3.7856786251068115 17 0.61191970109939575 18 -2.8804209232330322
		 19 -5.5580296516418457 20 -6.2859482765197754 21 -4.331916332244873 22 -0.45355519652366638
		 23 4.2478432655334473 24 8.6735773086547852 25 11.732658386230469 26 12.915013313293457
		 27 12.887134552001953 28 12.327313423156738 29 11.913517951965332 30 11.995709419250488
		 31 12.249103546142578 32 12.340176582336426 33 11.935437202453613 34 10.700733184814453
		 35 8.1602840423583984 36 4.5326251983642578 37 0.52731436491012573 38 -3.1415629386901855
		 39 -5.7575726509094238 40 -7.1425299644470215 41 -7.7719125747680664 42 -7.9098567962646484
		 43 -7.82065773010254 44 -7.7686285972595224 45 -7.6659555435180664 46 -7.3358683586120605
		 47 -6.9092473983764648 48 -6.5169577598571777 49 -6.2898397445678711 50 -6.3587255477905273
		 51 -7.2722377777099618 52 -8.9579019546508789 53 -10.614579200744629 54 -11.440763473510742
		 55 -10.634980201721191 56 -7.1875438690185547 57 -1.6358591318130493 58 4.4991245269775391
		 59 9.7020721435546875 60 12.472176551818848 61 12.25312328338623 62 10.048247337341309
		 63 6.7036266326904297 64 3.0691347122192383 65 -1.1469170857481004e-007 66 -1.3541088605961704e-007;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateZ";
	rename -uid "90CA025D-4766-1864-1F26-8A98AACB9C7B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 1.576250951984548e-007 10 1.3116608954533149e-007
		 11 -0.18503089249134064 12 -0.6177484393119812 13 -1.113877534866333 14 -1.48389732837677
		 15 -1.5323563814163208 16 -1.0470422506332397 17 -0.16791965067386627 18 0.78552472591400146
		 19 1.5111092329025269 20 1.7080334424972534 21 1.1791889667510986 22 0.12419275194406509
		 23 -1.1764205694198608 24 -2.4383130073547363 25 -3.340916633605957 26 -3.6979899406433105
		 27 -3.6895124912261958 28 -3.5198824405670166 29 -3.3952174186706543 30 -3.4199318885803223
		 31 -3.4962737560272217 32 -3.5237674713134766 33 -3.401806116104126 34 -3.0331895351409912
		 35 -2.2895424365997314 36 -1.2563400268554687 37 -0.14467655122280121 38 0.8564295768737793
		 39 1.5650970935821533 40 1.9397491216659546 41 2.1100530624389648 42 2.1473896503448486
		 43 2.1232461929321289 44 2.1091642379760742 45 2.0813772678375244 46 1.9920575618743896
		 47 1.8766396045684814 48 1.7705221176147461 49 1.7090861797332764 50 1.7277199029922485
		 51 1.9748415946960451 52 2.4312453269958496 53 2.8809890747070312 54 3.1059677600860596
		 55 2.886538028717041 56 1.9519274234771731 57 0.44697913527488708 58 -1.2469305992126465
		 59 -2.7386093139648438 60 -3.5636684894561768 61 -3.4974868297576904 62 -2.8403770923614502
		 63 -1.8710397481918337 64 -0.84719938039779663 65 9.4987512966326904e-008 66 9.1322071682498063e-008;
createNode animCurveTU -n "Character1_LeftHandThumb3_visibility";
	rename -uid "302456DF-4BF2-C8E7-245C-7C86699C7AA8";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateX";
	rename -uid "933DB82E-4CBD-F540-FD54-8F9E36C312F4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.854037284851074;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateY";
	rename -uid "1D263C0A-4363-526A-86BD-E2A7528B8B49";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2616198062896729;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateZ";
	rename -uid "FFFA49D1-441E-6E5E-1D62-7E9F2FDD7C94";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.944699764251709;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleX";
	rename -uid "A4079B77-437D-B528-A768-29923E0AE58C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleY";
	rename -uid "B7FCBA70-46C6-FAEB-EBD5-1CB046CF0F2E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleZ";
	rename -uid "707FE5B4-4C60-9488-F39E-7BBFD63667B3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateX";
	rename -uid "5642AEDC-4737-7C96-25CB-E894407C0B82";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 80 ".ktv[0:79]"  0 2.1236224174499512 1 18.213516235351563
		 2 45.965457916259766 3 60.010890960693366 4 60.010890960693366 5 60.010890960693366
		 6 60.010890960693366 7 53.407886505126953 8 36.747718811035156 9 20.354248046875
		 10 12.527192115783691 11 11.796980857849121 12 12.673917770385742 13 12.751585006713867
		 14 12.82536792755127 15 12.836003303527832 16 12.739682197570801 17 12.62543773651123
		 18 12.595127105712891 19 12.64079475402832 20 12.663700103759766 21 12.612383842468262
		 22 12.605514526367188 23 12.76312255859375 24 13.074206352233887 25 13.38007926940918
		 26 13.518043518066406 27 13.514663696289062 28 13.448095321655273 29 13.400471687316895
		 30 13.409824371337891 31 13.438991546630859 32 13.449597358703613 33 13.402961730957031
		 34 13.268651008605957 35 13.030129432678223 36 12.778423309326172 37 12.623515129089355
		 38 12.596928596496582 39 12.646624565124512 40 12.696455001831055 41 12.724538803100586
		 42 12.73115062713623 44 12.724383354187012 45 12.719572067260742 46 12.704718589782715
		 47 12.686911582946777 48 12.671915054321289 49 12.663835525512695 50 12.666239738464355
		 51 12.701963424682617 52 12.786767959594727 53 12.894232749938965 54 12.956856727600098
		 55 12.895706176757812 56 12.698349952697754 57 12.594352722167969 58 12.776589393615723
		 59 13.168802261352539 60 13.465085029602051 61 13.439457893371582 62 13.202522277832031
		 63 12.916440010070801 64 12.706746101379395 65 12.612855911254883 66 12.612855911254883
		 67 12.612855911254883 68 12.612855911254883 69 12.403706550598145 70 11.823214530944824
		 71 10.952006340026855 72 9.8772153854370117 73 8.6849832534790039 74 7.4547076225280771
		 75 6.2549600601196289 76 5.1408910751342773 77 4.1528167724609375 78 3.3158302307128906
		 79 2.6403405666351318 80 2.1236224174499512;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateY";
	rename -uid "66715084-413E-5553-64F7-7C85AF6522F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 6.7255492210388184 1 25.98272705078125
		 2 34.666542053222656 3 33.363674163818359 4 33.363674163818359 5 33.363674163818359
		 6 33.363674163818359 7 34.719635009765625 8 33.434383392333984 9 23.080635070800781
		 10 6.8903155326843262 11 -6.8165969848632813 12 -11.709485054016113 13 -10.798264503479004
		 14 -10.126552581787109 15 -10.03913402557373 16 -10.920347213745117 17 -12.544479370117188
		 18 -14.333835601806642 19 -15.70436477661133 20 -16.076408386230469 21 -15.077099800109863
		 22 -13.09035587310791 23 -10.684224128723145 24 -8.4312286376953125 25 -6.8859062194824219
		 26 -6.2920351028442383 27 -6.3060140609741211 28 -6.5869636535644531 29 -6.7949309349060059
		 30 -6.7536025047302246 31 -6.626251220703125 32 -6.5805025100708008 33 -6.7839083671569824
		 34 -7.405855655670166 35 -8.6916122436523437 36 -10.538790702819824 37 -12.587820053100586
		 38 -14.467599868774414 39 -15.806381225585938 40 -16.513803482055664 41 -16.834859848022461
		 42 -16.905187606811523 43 -16.859712600708008 44 -16.833185195922852 45 -16.780830383300781
		 46 -16.612459182739258 47 -16.394729614257813 48 -16.194414138793945 49 -16.078395843505859
		 50 -16.113588333129883 51 -16.579992294311523 52 -17.438989639282227 53 -18.280664443969727
		 54 -18.699289321899414 55 -18.291011810302734 56 -16.536775588989258 57 -13.696188926696777
		 58 -10.555896759033203 59 -7.9103784561157218 60 -6.5142173767089844 61 -6.6242318153381348
		 62 -7.735349178314209 63 -9.4319839477539062 64 -11.286667823791504 65 -12.857967376708984
		 66 -12.857967376708984 67 -12.857967376708984 68 -12.857967376708984 69 -12.598550796508789
		 70 -11.863750457763672 71 -10.717613220214844 72 -9.2245912551879883 73 -7.4513754844665518
		 74 -5.4678101539611816 75 -3.3467628955841064 76 -1.1631159782409668 77 1.0078670978546143
		 78 3.0924508571624756 79 5.0202460289001465 80 6.7255492210388184;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateZ";
	rename -uid "E5491171-4F81-6132-B489-50A827ACC067";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 1.4951497316360474 1 30.667280197143555
		 2 68.615455627441406 3 86.987724304199219 4 86.987724304199219 5 86.987724304199219
		 6 86.987724304199219 7 76.865158081054688 8 51.190032958984375 9 21.933769226074219
		 10 -1.4091818332672119 11 -16.85789680480957 12 -21.881130218505859 13 -20.285182952880859
		 14 -19.108219146728516 15 -18.954963684082031 16 -20.499015808105469 17 -23.344501495361328
		 18 -26.491083145141602 19 -28.919105529785156 20 -29.582010269165039 21 -27.805347442626953
		 22 -24.302452087402344 23 -20.085422515869141 24 -16.129722595214844 25 -13.395556449890137
		 26 -12.337559700012207 27 -12.362518310546875 28 -12.863567352294922 29 -13.233780860900879
		 30 -13.16025447845459 31 -12.933548927307129 32 -12.852057456970215 33 -13.21417236328125
		 34 -14.318257331848145 35 -16.588296890258789 36 -19.830644607543945 37 -23.420513153076172
		 38 -26.72722053527832 39 -29.100696563720703 40 -30.363813400268551 41 -30.939504623413089
		 42 -31.065830230712891 43 -30.984138488769528 44 -30.936500549316403 45 -30.842512130737305
		 46 -30.540542602539063 47 -30.150705337524414 48 -29.792663574218746 49 -29.585557937622067
		 50 -29.648359298706055 51 -30.482366561889648 52 -32.027385711669922 53 -33.554214477539063
		 54 -34.319019317626953 55 -33.573074340820312 56 -30.404953002929687 57 -25.367437362670898
		 58 -19.860612869262695 59 -15.210788726806642 60 -12.733935356140137 61 -12.929951667785645
		 62 -14.901438713073729 63 -17.889760971069336 64 -21.140579223632812 65 -23.894474029541016
		 66 -23.894474029541016 67 -23.894474029541016 68 -23.894474029541016 69 -23.550363540649414
		 70 -22.58082389831543 71 -21.082202911376953 72 -19.15077018737793 73 -16.879669189453125
		 74 -14.357301712036133 75 -11.667210578918457 76 -8.8893461227416992 77 -6.1022787094116211
		 78 -3.3858547210693359 79 -0.82367473840713501 80 1.4951497316360474;
createNode animCurveTU -n "Character1_LeftHandIndex1_visibility";
	rename -uid "9F681F46-49C4-871B-5708-36902169E016";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateX";
	rename -uid "A15489E7-47EF-D438-E5D5-B88C14024BE6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.1721835136413574;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateY";
	rename -uid "DFAB3E40-4CAC-3A88-CD56-93913F1F6A9A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.9652466773986816;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateZ";
	rename -uid "BBD0EE8A-4CB4-7422-239C-079B8E674F77";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.8871781826019287;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleX";
	rename -uid "2D358155-4B59-6C9F-7320-FC9F718DB993";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleY";
	rename -uid "8E804E40-4A99-1DE3-3EE9-9694FBE81454";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleZ";
	rename -uid "3822FA9B-4ED1-25F2-7513-56B8DDE5538D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateX";
	rename -uid "90E1CA89-45B0-BC11-4072-C1AF3DECE839";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 7.2087359428405762 1 10.765958786010742
		 2 9.9180679321289062 3 7.7885360717773429 4 7.7885360717773429 5 7.7885360717773429
		 6 7.7885360717773429 7 8.1192970275878906 8 8.7379283905029297 9 9.1024408340454102
		 10 8.9845991134643555 11 8.5312576293945312 12 8.1981954574584961 13 8.0545949935913086
		 14 7.9322981834411621 15 7.9153389930725089 16 8.075312614440918 17 8.3077287673950195
		 18 8.4734029769897461 19 8.5382795333862305 20 8.5467805862426758 21 8.5151691436767578
		 22 8.3681507110595703 23 8.034825325012207 24 7.5594668388366699 25 7.1358671188354492
		 26 6.9507827758789062 27 6.9552855491638184 28 7.0442848205566406 29 7.1083340644836426
		 30 7.095728874206543 31 7.0565028190612793 32 7.0422697067260742 33 7.104978084564209
		 34 7.2876114845275879 35 7.6228432655334482 36 8.0090255737304687 37 8.3128471374511719
		 38 8.4820775985717773 39 8.5409955978393555 40 8.5518436431884766 51 8.5521469116210937
		 52 8.5450906753540039 53 8.5184497833251953 54 8.4979496002197266 55 8.5180015563964844
		 56 8.5519618988037109 57 8.4250087738037109 58 8.0120944976806641 59 7.425858974456788
		 60 7.0215144157409668 61 7.0558772087097168 62 7.3788909912109375 63 7.7908296585083017
		 64 8.1347217559814453 65 8.3434991836547852 66 8.3434991836547852 67 8.3434991836547852
		 68 8.3434991836547852 69 8.3676986694335938 70 8.4297370910644531 71 8.507781982421875
		 72 8.5764665603637695 73 8.6115398406982422 74 8.5933036804199219 75 8.50885009765625
		 76 8.3532915115356445 77 8.1301670074462891 78 7.851212978363038 79 7.5356082916259766
		 80 7.2087359428405762;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateY";
	rename -uid "649BE1BB-4EB9-B0A5-771D-C4A5C2B5D53A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -8.152409553527832 1 -18.332328796386719
		 2 -29.055370330810547 3 -33.620094299316406 4 -33.620094299316406 5 -33.620094299316406
		 6 -33.620094299316406 7 -33.019775390625 8 -31.443723678588864 9 -29.271030426025391
		 10 -27.01136589050293 11 -25.519001007080078 12 -25.43455696105957 13 -26.140195846557617
		 14 -26.658805847167969 15 -26.726198196411133 16 -26.045793533325195 17 -24.785951614379883
		 18 -23.390119552612305 19 -22.315988540649414 20 -22.023698806762695 21 -22.808113098144531
		 22 -24.360952377319336 23 -26.228338241577148 24 -27.961261749267578 25 -29.139373779296875
		 26 -29.589509963989258 27 -29.578933715820316 28 -29.366157531738281 29 -29.208431243896484
		 30 -29.239791870117188 31 -29.336376190185543 32 -29.371055603027344 33 -29.216796875
		 34 -28.744037628173825 35 -27.761861801147461 36 -26.340690612792969 37 -24.752235412597656
		 38 -23.285467147827148 39 -22.235870361328125 40 -21.6796875 41 -21.426921844482422
		 42 -21.371524810791016 43 -21.407346725463867 44 -21.428241729736328 45 -21.469474792480469
		 46 -21.602041244506836 47 -21.773378372192383 48 -21.930927276611328 49 -22.022134780883789
		 50 -21.994472503662109 51 -21.627595901489258 52 -20.950712203979492 53 -20.285989761352539
		 54 -19.954830169677734 55 -20.277809143066406 56 -21.661611557006836 57 -23.88841438293457
		 58 -26.327476501464844 59 -28.359384536743164 60 -29.421283721923828 61 -29.33790397644043
		 62 -28.492940902709961 63 -27.193582534790039 64 -25.762273788452148 65 -24.541973114013672
		 66 -24.541973114013672 67 -24.541973114013672 68 -24.541973114013672 69 -24.315586090087891
		 70 -23.676151275634766 71 -22.683908462524414 72 -21.400205612182617 73 -19.887649536132812
		 74 -18.209882736206055 75 -16.430938720703125 76 -14.614237785339355 77 -12.821405410766602
		 78 -11.111083984375 79 -9.5379819869995117 80 -8.152409553527832;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateZ";
	rename -uid "D3280CB5-44B1-D3F0-3AA4-2DBEB1B928A8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 22.938976287841797 1 45.320182800292969
		 2 68.861953735351563 3 80.491020202636719 4 80.491020202636719 5 80.491020202636719
		 6 80.491020202636719 7 78.0589599609375 8 72.102073669433594 9 64.606391906738281
		 10 57.362754821777351 11 52.604911804199219 12 52.012870788574219 13 53.783302307128906
		 14 55.099609375 15 55.271671295166016 16 53.545124053955078 17 50.404247283935547
		 18 46.993488311767578 19 44.406875610351563 20 43.707553863525391 21 45.588466644287109
		 22 49.35888671875 23 54.006072998046875 24 58.470699310302734 25 61.614871978759759
		 26 62.844017028808587 27 62.814945220947266 28 62.232059478759773 29 61.802375793457031
		 30 61.88764572143554 31 62.150775909423821 32 62.245437622070305 33 61.825111389160156
		 34 60.548572540283203 35 57.947986602783196 36 54.29058837890625 37 50.321079254150391
		 38 46.740188598632813 39 44.215015411376953 40 42.886604309082031 41 42.284717559814453
		 42 42.152942657470703 43 42.238147735595703 44 42.287857055664063 45 42.385971069335938
		 46 42.701595306396484 47 43.109977722167969 48 43.485946655273438 49 43.703819274902344
		 50 43.637725830078125 51 42.762474060058594 52 41.153438568115234 53 39.579139709472656
		 54 38.796463012695313 55 39.559795379638672 56 42.843521118164063 57 48.203891754150391
		 58 54.257095336914063 59 59.522190093994141 60 62.382713317871087 61 62.154945373535156
		 62 59.877365112304688 63 56.471782684326172 64 52.832309722900391 65 49.803348541259766
		 66 49.803348541259766 67 49.803348541259766 68 49.803348541259766 69 49.439132690429688
		 70 48.41217041015625 71 46.822750091552734 72 44.771255493164062 73 42.355892181396484
		 74 39.671531677246094 75 36.80963134765625 76 33.859081268310547 77 30.90761756896973
		 78 28.043376922607422 79 25.356283187866211 80 22.938976287841797;
createNode animCurveTU -n "Character1_LeftHandIndex2_visibility";
	rename -uid "A1B36B04-44DA-5ACF-F567-C9A0E231755E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateX";
	rename -uid "45FD80BF-498D-73D1-1DFF-7AAF3B5A6555";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.5761528015136719;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateY";
	rename -uid "B1732231-4F30-700E-CA69-94B35523D3C6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8397388458251953;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateZ";
	rename -uid "DA3C2D02-46C4-E807-A06C-7397A519807A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3069263696670532;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleX";
	rename -uid "27241F64-4026-964B-7AE0-7F9A4FFABC5B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleY";
	rename -uid "3043CF0A-4B56-09F1-A8C7-BEBCD7DB3459";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleZ";
	rename -uid "943C012D-49B3-CF53-8077-0A9AE999B479";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateX";
	rename -uid "D72258BA-41C0-0547-DEA5-8DA22A822D18";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -6.1057583877754951e-008 10 -1.0975423236914139e-007
		 11 0.029036609455943108 12 0.082129299640655518 13 0.11785058677196503 14 0.12738320231437683
		 15 0.1275688111782074 16 0.114582821726799 17 0.026511365547776222 18 -0.1660691499710083
		 19 -0.38143089413642883 20 -0.45013248920440668 21 -0.27552396059036255 22 -0.021634859964251518
		 23 0.12047736346721649 24 0.087140381336212158 25 -0.031734313815832138 26 -0.098904840648174286
		 27 -0.097183898091316223 28 -0.06402992457151413 29 -0.04123876616358757 30 -0.045649897307157516
		 31 -0.059610638767480857 32 -0.064761780202388763 33 -0.042409546673297882 34 0.017190948128700256
		 35 0.09937702864408493 36 0.12322986871004105 37 0.02302907221019268 38 -0.18448804318904877
		 39 -0.39983195066452026 40 -0.53653925657272339 41 -0.60386151075363159 42 -0.61905080080032349
		 43 -0.60921108722686768 44 -0.60350179672241211 45 -0.59230035543441772 46 -0.55687409639358521
		 47 -0.51241111755371094 48 -0.47284305095672613 49 -0.45051142573356628 50 -0.45724001526832586
		 51 -0.55014771223068237 52 -0.73955327272415161 53 -0.94843649864196777 54 -1.0610420703887939
		 55 -0.95114946365356434 56 -0.54124629497528076 57 -0.085966587066650391 58 0.12294089794158936
		 59 0.055972028523683548 60 -0.072352997958660126 61 -0.059836570173501961 62 0.043480947613716125
		 63 0.12211232632398604 64 0.10195497423410416 65 -1.7974744537241349e-007 66 -1.8018057801327814e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateY";
	rename -uid "025AC934-41F6-D344-5543-0BAEC502051B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -1.2334606935837655e-007 10 -1.7851203892860212e-007
		 11 -0.35577011108398438 12 -1.1847864389419556 13 -2.128403902053833 14 -2.8260452747344971
		 15 -2.9169671535491943 16 -2.0017950534820557 17 -0.32289701700210571 18 1.5151529312133789
		 19 2.9147758483886719 20 3.2934925556182861 21 2.2750768661499023 22 0.23912373185157779
		 23 -2.2467241287231445 24 -4.5946135520935059 25 -6.2165627479553223 26 -6.8424205780029297
		 27 -6.8276724815368652 28 -6.5314292907714844 29 -6.3123459815979004 30 -6.3558692932128906
		 31 -6.490027904510498 32 -6.538238525390625 33 -6.3239531517028809 34 -5.6697707176208496
		 35 -4.3222503662109375 36 -2.3976881504058838 37 -0.2782343327999115 38 1.6520689725875854
		 39 3.0186741352081299 40 3.7380635738372803 41 4.0639281272888184 42 4.1352567672729492
		 43 4.0891375541687012 44 4.0622296333312988 45 4.0091171264648437 46 3.838238000869751
		 47 3.617108821868896 48 3.4135062694549561 49 3.2955150604248047 50 3.3313107490539551
		 51 3.8052761554718013 52 4.676048755645752 53 5.5265583992004395 54 5.948577880859375
		 55 5.5369968414306641 56 3.7613923549652095 57 0.86154699325561523 58 -2.3799278736114502
		 59 -5.1402320861816406 60 -6.6081047058105469 61 -6.4921565055847168 62 -5.3238253593444824
		 63 -3.5492660999298096 64 -1.6222395896911621 65 -2.3247345382060303e-007 66 -2.3326315101712678e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateZ";
	rename -uid "E7111BD2-40A6-1686-7E4D-59A690E60EE6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -2.9693092074012384e-007 10 -3.2592541288067878e-007
		 11 0.60234242677688599 12 2.0036821365356445 13 3.5964169502258301 14 4.773557186126709
		 15 4.9269967079162598 16 3.3827857971191406 17 0.54671406745910645 18 -2.5749936103820801
		 19 -4.9735093116760254 20 -5.626795768737793 21 -3.874402523040771 22 -0.40526404976844788
		 23 3.7960548400878906 24 7.7623128890991211 25 10.518594741821289 26 11.588504791259766
		 27 11.563243865966797 28 11.056336402893066 29 10.682069778442383 30 10.756381988525391
		 31 10.985570907592773 32 11.067976951599121 33 10.701886177062988 34 9.5870456695556641
		 35 7.3012323379516593 36 4.0507674217224121 37 0.4711264967918396 38 -2.8086557388305664
		 39 -5.1525321006774902 40 -6.3963932991027832 41 -6.962498664855957 42 -7.0866508483886719
		 43 -7.0063667297363281 44 -6.9595432281494141 45 -6.8671550750732422 46 -6.5702338218688965
		 47 -6.1867046356201172 48 -5.8342533111572266 49 -5.6302900314331055 50 -5.692145824432373
		 51 -6.5130143165588379 52 -8.0308713912963867 53 -9.5272407531738281 54 -10.275421142578125
		 55 -9.5456991195678711 56 -6.4368629455566406 57 -1.4619646072387695 58 4.0208010673522949
		 59 8.6873035430908203 60 11.187443733215332 61 10.989208221435547 62 8.9990034103393555
		 63 5.9944992065429687 64 2.742250919342041 65 -3.4572408935673593e-007 66 -3.5039352042076644e-007;
createNode animCurveTU -n "Character1_LeftHandIndex3_visibility";
	rename -uid "984A3CBE-4B39-1F54-145D-91956B9600BB";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing1_translateX";
	rename -uid "BA3B08DE-4388-BE32-FD65-37B7E666E422";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4602031707763672;
createNode animCurveTL -n "Character1_LeftHandRing1_translateY";
	rename -uid "DBF68B5B-4452-17BD-6BEF-E8B900C60613";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.5015654563903809;
createNode animCurveTL -n "Character1_LeftHandRing1_translateZ";
	rename -uid "2D78A892-484D-8B25-989C-25ADA8110615";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.6178884506225586;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleX";
	rename -uid "04F8324B-417D-D8C9-2C3C-BD9557BFF4B4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleY";
	rename -uid "006A138E-42A1-A5B0-4789-50B98DFE83A2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleZ";
	rename -uid "F82485AC-4F4F-3AFF-370F-BBBEA5878B73";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateX";
	rename -uid "CFFD3C14-45CE-AD3E-8317-478240E0EB0D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 3.1731135845184326 1 10.410351753234863
		 2 24.346931457519531 3 32.34600830078125 4 32.34600830078125 5 32.34600830078125
		 6 32.34600830078125 7 28.509292602539063 8 19.856658935546875 9 13.520329475402832
		 10 13.701300621032715 11 17.886241912841797 12 20.301982879638672 13 20.110673904418945
		 14 19.981206893920898 15 19.965080261230469 16 20.135259628295898 17 20.493095397949219
		 18 20.953575134277344 19 21.353494644165039 20 21.469184875488281 21 21.165348052978516
		 22 20.626205444335938 23 20.088003158569336 24 19.698341369628906 25 19.495992660522461
		 26 19.432554244995117 27 19.433956146240234 28 19.463054656982422 29 19.485754013061523
		 30 19.481164932250977 31 19.467267990112305 32 19.462366104125977 33 19.484527587890625
		 34 19.558101654052734 35 19.737674713134766 36 20.059497833251953 37 20.503427505493164
		 38 20.99079704284668 39 21.384910583496094 40 21.60911750793457 41 21.714542388916016
		 42 21.737943649291992 43 21.722799301147461 44 21.713987350463867 45 21.696640014648437
		 46 21.641269683837891 47 21.570602416992188 48 21.506519317626953 49 21.469810485839844
		 50 21.480915069580078 51 21.630664825439453 52 21.919178009033203 53 22.218023300170898
		 54 22.372682571411133 55 22.221796035766602 56 21.616584777832031 57 20.781501770019531
		 58 20.062828063964844 59 19.624174118041992 60 19.455347061157227 61 19.467050552368164
		 62 19.600606918334961 63 19.857686996459961 64 20.210971832275391 65 20.568748474121094
		 66 20.568748474121094 67 20.568748474121094 68 20.568748474121094 69 20.237392425537109
		 70 19.315214157104492 71 17.924257278442383 72 16.196611404418945 73 14.264656066894531
		 74 12.252808570861816 75 10.270908355712891 76 8.4093170166015625 77 6.73583984375
		 78 5.2945494651794434 79 4.1065983772277832 80 3.1731135845184326;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateY";
	rename -uid "5FE07384-490F-77B8-8804-C6904D5002AB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 7.5687365531921396 1 23.766960144042969
		 2 34.150215148925781 3 35.989974975585938 4 35.989974975585938 5 35.989974975585938
		 6 35.989974975585938 7 35.958522796630859 8 32.504634857177734 9 22.179704666137695
		 10 7.9860491752624503 11 -3.7017641067504887 12 -7.9920344352722168 13 -7.4093894958496085
		 14 -6.9752802848815918 15 -6.9184980392456055 16 -7.4878697395324707 17 -8.5195627212524414
		 18 -9.629145622253418 19 -10.459304809570312 20 -10.681640625 21 -10.081513404846191
		 22 -8.8611021041870117 23 -7.3359622955322275 24 -5.8624019622802734 25 -4.8266139030456543
		 26 -4.4231419563293457 27 -4.432673454284668 28 -4.6238903999328613 29 -4.7650008201599121
		 30 -4.7369880676269531 31 -4.650576114654541 32 -4.6195006370544434 33 -4.7575311660766602
		 34 -5.1773934364318848 35 -6.0349249839782715 36 -7.2421598434448251 37 -8.5467758178710937
		 38 -9.7109317779541016 39 -10.520401000976562 40 -10.941354751586914 41 -11.130826950073242
		 42 -11.172198295593262 43 -11.145453453063965 44 -11.129841804504395 45 -11.099011421203613
		 46 -10.999682426452637 47 -10.870833396911621 48 -10.751889228820801 49 -10.682826042175293
		 50 -10.703788757324219 51 -10.980497360229492 52 -11.484654426574707 53 -11.97164249420166
		 54 -12.211224555969238 55 -11.977585792541504 56 -10.954944610595703 57 -9.2370405197143555
		 58 -7.2532024383544922 59 -5.51556396484375 60 -4.5744438171386719 61 -4.649205207824707
		 62 -5.398491382598877 63 -6.5223135948181152 64 -7.7225790023803702 65 -8.7160263061523437
		 66 -8.7160263061523437 67 -8.7160263061523437 68 -8.7160263061523437 69 -8.5094146728515625
		 70 -7.9211621284484854 71 -6.9952921867370605 72 -5.7756519317626953 73 -4.3103237152099609
		 74 -2.6542656421661377 75 -0.86984896659851074 76 0.97460806369781494 77 2.8079080581665039
		 78 4.5599427223205566 79 6.1657533645629883 80 7.5687365531921396;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateZ";
	rename -uid "300B8748-4B48-7F2C-D99F-C6930F9769EB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 22.875423431396484 1 46.290672302246094
		 2 75.640007019042969 3 90.74188232421875 4 90.74188232421875 5 90.74188232421875
		 6 90.74188232421875 7 81.251838684082031 8 58.232017517089851 9 32.110923767089844
		 10 10.032619476318359 11 -5.1578478813171387 12 -9.9902029037475586 13 -8.2163286209106445
		 14 -6.9084286689758301 15 -6.738154411315918 16 -8.4539852142333984 17 -11.616632461547852
		 18 -15.1112060546875 19 -17.802768707275391 20 -18.536521911621094 21 -16.568840026855469
		 22 -12.681037902832031 23 -7.9943194389343253 24 -3.6013357639312744 25 -0.57142484188079834
		 26 0.59892857074737549 27 0.57133448123931885 28 0.017223022878170013 29 -0.39238518476486206
		 30 -0.31102257966995239 31 -0.060193207114934921 32 0.029955925419926643 33 -0.37068691849708557
		 34 -1.5931292772293091 35 -4.1101446151733398 36 -7.7111778259277344 37 -11.70110034942627
		 38 -15.373212814331055 39 -18.003820419311523 40 -19.401155471801758 41 -20.037294387817383
		 42 -20.176815032958984 43 -20.086591720581055 44 -20.033973693847656 45 -19.930149078369141
		 46 -19.596492767333984 47 -19.165552139282227 48 -18.769577026367187 49 -18.540449142456055
		 50 -18.609933853149414 51 -19.532197952270508 52 -21.238006591796875 53 -22.919782638549805
		 54 -23.760534286499023 55 -22.940526962280273 56 -19.446628570556641 57 -13.863911628723145
		 58 -7.7444825172424316 59 -2.5822341442108154 60 0.16061292588710785 61 -0.056213639676570892
		 62 -2.2393274307250977 63 -5.5549488067626953 64 -9.1670684814453125 65 -12.227761268615723
		 66 -12.227761268615723 67 -12.227761268615723 68 -12.227761268615723 69 -11.766199111938477
		 70 -10.46269416809082 71 -8.4394235610961914 72 -5.8176441192626953 73 -2.7161159515380859
		 74 0.74974632263183594 75 4.467623233795166 76 8.3268203735351562 77 12.215813636779785
		 78 16.019323348999023 79 19.615707397460937 80 22.875423431396484;
createNode animCurveTU -n "Character1_LeftHandRing1_visibility";
	rename -uid "F36FD4C8-44DF-96E0-274E-6886E7D8135E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing2_translateX";
	rename -uid "C818037C-475E-EE1F-C5A6-D193E73F25A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.9237353801727295;
createNode animCurveTL -n "Character1_LeftHandRing2_translateY";
	rename -uid "BFDC742E-4E73-D97A-5985-7AAC118A4A42";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.0094923973083496;
createNode animCurveTL -n "Character1_LeftHandRing2_translateZ";
	rename -uid "F34818E5-487A-0452-6B2A-4185B4CCAF80";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.8221585750579834;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleX";
	rename -uid "EE8304CC-4107-B842-C13F-7A8800C60AB3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleY";
	rename -uid "89BE5E2C-4C8F-ABE2-733B-EE88A367ABB9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleZ";
	rename -uid "9E953E1F-485F-9D06-34D1-5F949D64372B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateX";
	rename -uid "38A32FCD-4CC2-A9CD-CD9F-D88229944A29";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 9.7664651870727539 1 25.902639389038086
		 2 38.48797607421875 3 42.815162658691406 4 42.815162658691406 5 42.815162658691406
		 6 42.815162658691406 7 42.653007507324219 8 41.931953430175781 9 40.353725433349609
		 10 38.109031677246094 11 36.126796722412109 12 35.503582000732422 13 35.860694885253906
		 14 36.109580993652344 15 36.141056060791016 16 35.814140319824219 17 35.157142639160156
		 18 34.354690551757812 19 33.685920715332031 20 33.496364593505859 21 33.997776031494141
		 22 34.920936584472656 23 35.903816223144531 24 36.681194305419922 25 37.127250671386719
		 26 37.278289794921875 27 37.274871826171875 28 37.204750061035156 29 37.151145935058594
		 30 37.161914825439453 31 37.194732666015625 32 37.206394195556641 33 37.154026031494141
		 34 36.98553466796875 35 36.598827362060547 36 35.958301544189453 37 35.138668060302734
		 38 34.291469573974609 39 33.634281158447266 40 33.269153594970703 41 33.099376678466797
		 42 33.061847686767578 43 33.086128234863281 44 33.100269317626953 45 33.128124237060547
		 46 33.217254638671875 47 33.331478118896484 48 33.435531616210938 49 33.495346069335938
		 50 33.47723388671875 51 33.234359741210937 52 32.773006439208984 53 32.303237915039063
		 54 32.063014984130859 55 32.297355651855469 56 33.257091522216797 57 34.649913787841797
		 58 35.951923370361328 59 36.839836120605469 60 37.223159790039062 61 37.19525146484375
		 62 36.891281127929687 63 36.353752136230469 64 35.672039031982422 65 35.022426605224609
		 66 35.022426605224609 67 35.022426605224609 68 35.022426605224609 69 34.607307434082031
		 70 33.455837249755859 71 31.726055145263672 72 29.578897476196286 73 27.158603668212891
		 74 24.582361221313477 75 21.93902587890625 76 19.294754028320313 77 16.702333450317383
		 78 14.211406707763672 79 11.877226829528809 80 9.7664651870727539;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateY";
	rename -uid "3CF50E46-472B-EB28-9254-66AF31104AC6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -1.8647100925445559 1 -10.401308059692383
		 2 -24.046638488769531 3 -31.412193298339847 4 -31.412193298339847 5 -31.412193298339847
		 6 -31.412193298339847 7 -31.363121032714844 8 -31.330278396606445 9 -31.547073364257816
		 10 -32.145431518554687 11 -33.2646484375 12 -34.451484680175781 13 -35.373397827148438
		 14 -36.058944702148438 15 -36.148544311523438 16 -35.249347686767578 17 -33.614631652832031
		 18 -31.845903396606445 19 -30.512924194335938 20 -30.154214859008793 21 -31.120721817016605
		 22 -33.071544647216797 23 -35.489425659179688 24 -37.812541961669922 25 -39.44158935546875
		 26 -40.075790405273437 27 -40.060806274414063 28 -39.76025390625 29 -39.538440704345703
		 30 -39.582477569580078 31 -39.718307495117187 32 -39.767154693603516 33 -39.550182342529297
		 34 -38.890098571777344 35 -37.540981292724609 36 -35.637607574462891 37 -33.571399688720703
		 38 -31.714988708496094 39 -30.414432525634766 40 -29.734170913696289 41 -29.426992416381836
		 42 -29.359832763671875 43 -29.403253555297855 44 -29.428592681884766 45 -29.478618621826172
		 46 -29.639677047729492 47 -29.848340988159183 48 -30.040710449218746 49 -30.152299880981449
		 50 -30.118438720703121 51 -29.670764923095703 52 -28.851558685302734 53 -28.055387496948242
		 54 -27.661746978759766 55 -28.045639038085938 56 -29.712160110473636 57 -32.472434997558594
		 58 -35.620166778564453 59 -38.358261108398437 60 -39.837974548339844 61 -39.720462799072266
		 62 -38.542404174804688 63 -36.773277282714844 64 -34.878120422363281 65 -33.302364349365234
		 66 -33.302364349365234 67 -33.302364349365234 68 -33.302364349365234 69 -32.892826080322266
		 70 -31.727653503417972 71 -29.896867752075195 72 -27.492456436157227 73 -24.616497039794922
		 74 -21.38459587097168 75 -17.924781799316406 76 -14.372995376586914 77 -10.866518974304199
		 78 -7.5367374420166007 79 -4.5023965835571289 80 -1.8647100925445559;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateZ";
	rename -uid "AFA75B3F-4DCE-50FF-EF1F-D0B4941ADA57";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 12.489145278930664 1 34.104896545410156
		 2 53.693767547607422 3 62.220687866210945 4 62.220687866210945 5 62.220687866210945
		 6 62.220687866210945 7 59.618309020996087 8 53.226600646972656 9 45.177410125732422
		 10 37.447734832763672 11 32.245948791503906 12 31.237752914428707 13 32.613269805908203
		 14 33.635288238525391 15 33.768856048583984 16 32.428291320800781 17 29.986627578735352
		 18 27.327974319458008 19 25.304824829101563 20 24.75663948059082 21 26.229856491088867
		 22 29.172725677490238 23 32.786270141601563 24 36.251941680908203 25 38.694679260253906
		 26 39.651115417480469 27 39.628475189208984 28 39.174789428710938 29 38.840507507324219
		 30 38.906837463378906 31 39.111541748046875 32 39.185195922851562 33 38.858192443847656
		 34 37.865760803222656 35 35.846157073974609 36 33.007190704345703 37 29.921899795532223
		 38 27.130144119262695 39 25.154483795166016 40 24.112403869628906 41 23.63957405090332
		 42 23.535995483398437 43 23.602970123291016 44 23.642038345336914 45 23.719144821166992
		 46 23.967109680175781 47 24.287769317626953 48 24.58281135559082 49 24.753711700439453
		 50 24.701869964599609 51 24.014923095703125 52 22.749662399291992 53 21.508544921875
		 54 20.890285491943359 55 21.49327278137207 56 24.078573226928711 57 28.27253532409668
		 58 32.981189727783203 59 37.068412780761719 60 39.292030334472656 61 39.114791870117188
		 62 37.344280242919922 63 34.700366973876953 64 31.874565124511719 65 29.518867492675781
		 66 29.518867492675781 67 29.518867492675781 68 29.518867492675781 69 29.480037689208984
		 70 29.337650299072266 71 29.023328781127933 72 28.453891754150391 73 27.555995941162109
		 74 26.282146453857422 75 24.618757247924805 76 22.588729858398438 77 20.251117706298828
		 78 17.699668884277344 79 15.060628890991209 80 12.489145278930664;
createNode animCurveTU -n "Character1_LeftHandRing2_visibility";
	rename -uid "1FA52221-4C2F-D847-5CF9-5A97BAD5B3F1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing3_translateX";
	rename -uid "1702CADA-4829-241D-823E-708ED59D0E55";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6098947525024414;
createNode animCurveTL -n "Character1_LeftHandRing3_translateY";
	rename -uid "1DFCC8B0-4A63-B875-68FB-1684AFF0FEC0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2952473163604736;
createNode animCurveTL -n "Character1_LeftHandRing3_translateZ";
	rename -uid "E2F4E919-4EDC-D6B5-990F-099BE87E7AD0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3554067611694336;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleX";
	rename -uid "4561DF22-4A67-2EFA-A531-E5870850B8A3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleY";
	rename -uid "9979CF6A-4B99-0727-486E-3796ADEDD92E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleZ";
	rename -uid "D203C616-4A73-D32F-B08A-23BBA081E1BA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateX";
	rename -uid "ABAC7430-4A56-13DC-7778-9781DA341FB9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 57 ".ktv[0:56]"  9 1.642568605575434e-007 10 1.305076295921026e-007
		 11 -0.035152696073055267 12 -0.11839767545461653 13 -0.21539205312728882 14 -0.28861847519874573
		 15 -0.29825606942176819 16 -0.20224045217037201 17 -0.031890135258436203 18 0.14580132067203522
		 19 0.27473485469818115 20 0.3086494505405426 21 0.21650224924087524 22 0.023432858288288116
		 23 -0.22772099077701569 24 -0.47994500398635859 25 -0.66249316930770874 26 -0.73472815752029419
		 27 -0.73301446437835693 28 -0.69870966672897339 29 -0.67348361015319824 30 -0.67848533391952515
		 31 -0.69393324851989746 32 -0.69949567317962646 33 -0.67481708526611328 34 -0.60020071268081665
		 35 -0.449952632188797 36 -0.24350503087043765 37 -0.02746218629181385 38 0.15866062045097351
		 39 0.28408080339431763 40 0.34792309999465942 41 0.37633726000785828 42 0.38251441717147827
		 44 0.37618997693061829 45 0.37158015370368958 46 0.35669168829917908 47 0.33729580044746399
		 48 0.3193090558052063 49 0.30882945656776428 50 0.31201305985450745 51 0.35380971431732178
		 52 0.42884844541549683 53 0.4999077320098877 54 0.53433054685592651 55 0.50076591968536377
		 56 0.34996780753135681 57 0.083687983453273773 58 -0.24164497852325439 59 -0.54060178995132446
		 60 -0.70756721496582031 61 -0.69417864084243774 62 -0.56118375062942505 63 -0.36587619781494141
		 64 -0.16306863725185394 65 6.7630971045673505e-008 66 8.2943756751774345e-008;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateY";
	rename -uid "5CB31A95-4B60-8011-2AC2-84B38BAE0ACC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 57 ".ktv[0:56]"  9 -2.9918399491180026e-007 10 -3.7702034205722157e-007
		 11 -0.030785420909523967 12 -0.10075461119413376 13 -0.17738460004329681 14 -0.23197181522846222
		 15 -0.2389561980962753 16 -0.1672893762588501 17 -0.027960004284977913 18 0.13620559871196747
		 19 0.26938775181770325 20 0.30664733052253723 21 0.20763640105724335 22 0.020946823060512543
		 23 -0.18676656484603882 24 -0.36242654919624329 25 -0.47195291519165039 26 -0.51158934831619263
		 27 -0.51067227125167847 28 -0.49207818508148188 29 -0.47811448574066162 30 -0.48090291023254395
		 31 -0.48945325613021856 32 -0.49250930547714239 33 -0.4788588285446167 34 -0.4361213743686676
		 35 -0.3430810272693634 36 -0.19866348803043365 37 -0.024115012958645821 38 0.14892125129699707
		 39 0.27955761551856995 40 0.35105422139167786 41 0.38406404852867126 42 0.39134162664413452
		 44 0.38389098644256592 45 0.37848442792892456 46 0.36116030812263489 47 0.33890077471733093
		 48 0.31856393814086914 49 0.30684772133827209 50 0.31039676070213318 51 0.35783088207244873
		 52 0.44712918996810913 53 0.53706461191177368 54 0.58269667625427246 55 0.5381852388381958
		 56 0.3534044623374939 57 0.076435551047325134 58 -0.19726814329624176 59 -0.40035942196846008
		 60 -0.49692258238792414 61 -0.48958835005760187 62 -0.41287606954574585 63 -0.28669744729995728
		 64 -0.13667875528335571 65 -4.606363006587344e-007 66 -4.6103801309982373e-007;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateZ";
	rename -uid "97F36A51-4117-5AE0-F0F3-FC919B54505D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -2.4331228587470832e-007 10 -2.4442104518129781e-007
		 11 0.69869095087051392 12 2.3248188495635986 13 4.1734180450439453 14 5.5393533706665039
		 15 5.7173581123352051 16 3.9254786968231206 17 0.63415586948394775 18 -2.9835085868835449
		 19 -5.7549872398376465 20 -6.5081615447998047 21 -4.4860925674438477 22 -0.4699558317661286
		 23 4.4051070213317871 24 9.0033254623413086 25 12.1881103515625 26 13.420709609985352
		 27 13.391633987426758 28 12.80791187286377 29 12.376591682434082 30 12.462254524230957
		 31 12.726380348205566 32 12.821322441101074 33 12.399435997009277 34 11.113116264343262
		 35 8.4694967269897461 36 4.7006974220275879 37 0.54646837711334229 38 -3.253880500793457
		 39 -5.9614629745483398 40 -7.3943428993225098 41 -8.0453996658325195 42 -8.1880865097045898
		 43 -8.095820426940918 44 -8.0420026779174805 45 -7.9357981681823739 46 -7.5943455696105957
		 47 -7.1530113220214844 48 -6.7471661567687988 49 -6.5121879577636719 50 -6.583458423614502
		 51 -7.528522491455079 52 -9.2720823287963867 53 -10.985342979431152 54 -11.839664459228516
		 55 -11.006439208984375 56 -7.4409089088439941 57 -1.6947062015533447 58 4.6659235954284668
		 59 10.073431015014648 60 12.958937644958496 61 12.730570793151855 62 10.433755874633789
		 63 6.9553570747375488 64 3.1820409297943115 65 -2.3171575946889789e-007 66 -2.3457585029973416e-007;
createNode animCurveTU -n "Character1_LeftHandRing3_visibility";
	rename -uid "4594E1F1-43B0-05D6-3A26-6D98B55703C6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightShoulder_translateX";
	rename -uid "180A9B3B-4DBD-160A-7096-5FB9C7508B84";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 14.332768440246582;
createNode animCurveTL -n "Character1_RightShoulder_translateY";
	rename -uid "AB019D77-4C28-4546-4166-74A86D384EC9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5798094272613525;
createNode animCurveTL -n "Character1_RightShoulder_translateZ";
	rename -uid "AF09CA25-4B23-9CFD-3CB6-1EBA094EDF26";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1086856126785278;
createNode animCurveTU -n "Character1_RightShoulder_scaleX";
	rename -uid "405CD9C7-4448-DE35-4B11-E081C7AB82B1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightShoulder_scaleY";
	rename -uid "3C9BFA0E-4CEE-F381-D5F9-B6AC4BAA05A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightShoulder_scaleZ";
	rename -uid "A9C64575-473B-FBB5-0ED0-10A4832D83E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightShoulder_rotateX";
	rename -uid "9B98BD57-49B7-1AAC-C7A6-1D939FBD0C14";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -1.9200718402862547 1 -1.2377371788024902
		 2 -0.77617686986923218 3 -0.28148692846298218 4 0.49020332098007208 5 1.4459271430969238
		 6 2.3397188186645508 7 2.9760076999664307 8 3.2265849113464355 9 3.0600082874298096
		 10 2.594202995300293 11 1.1146547794342041 12 -1.7735136747360229 13 -5.3834505081176758
		 14 -8.6841363906860352 15 -10.309802055358887 16 -9.5809335708618164 17 -7.5547771453857422
		 18 -5.3271913528442383 19 -3.6678862571716309 20 -3.0297701358795166 21 -3.673048734664917
		 22 -5.3429179191589355 23 -7.5788168907165536 24 -9.602691650390625 25 -10.216230392456055
		 26 -7.9165277481079102 27 -3.8611490726470947 28 -0.33593735098838806 29 1.3107683658599854
		 30 0.93521833419799805 31 -0.6640397310256958 32 -2.9497964382171631 33 -5.1216726303100586
		 34 -6.0958724021911621 35 -5.2258214950561523 36 -3.3184404373168945 37 -1.3248519897460937
		 38 0.089143410325050354 39 0.56760632991790771 40 -0.07753802090883255 41 -1.6440129280090332
		 42 -3.7633104324340825 43 -5.7505917549133301 44 -6.5946674346923828 45 -5.762566089630127
		 46 -3.9066481590271001 47 -1.7116312980651855 48 0.26481586694717407 49 1.648806095123291
		 50 2.1975579261779785 51 1.5262356996536255 52 -0.31218546628952026 53 -2.8321826457977295
		 54 -5.2281866073608398 55 -6.3668365478515625 56 -5.5370917320251465 57 -3.5662646293640137
		 58 -1.4760594367980957 59 0.035942129790782928 60 0.58692044019699097 61 0.2575194239616394
		 62 -0.65483027696609497 63 -2.1454446315765381 64 -4.0899291038513184 65 -6.1588363647460938
		 66 -6.4749255180358887 67 -6.9480476379394531 68 -7.5890750885009766 69 -8.668121337890625
		 70 -10.12684440612793 71 -11.543856620788574 72 -12.626296997070312 73 -13.203431129455566
		 74 -13.187520980834961 75 -12.603570938110352 76 -11.621297836303711 77 -10.368471145629883
		 78 -8.963374137878418 79 -7.5374951362609863 80 -6.2440481185913086;
createNode animCurveTA -n "Character1_RightShoulder_rotateY";
	rename -uid "058D7B43-4B5D-50A7-AC5E-3C9206ED8AB7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -22.21434211730957 1 -14.509328842163084
		 2 -6.7363014221191406 3 -1.8969259262084961 4 -0.55371731519699097 5 -0.80784046649932861
		 6 -1.955135703086853 7 -3.2531242370605469 8 -3.9492402076721191 9 -4.0082378387451172
		 10 -3.9309141635894775 11 -2.3272552490234375 12 1.1367963552474976 13 4.8997735977172852
		 14 7.7278513908386239 15 8.7822866439819336 16 7.5773406028747567 17 4.7109613418579102
		 18 1.1522226333618164 19 -1.9397596120834351 20 -3.2631962299346924 21 -1.9399299621582029
		 22 1.1508927345275879 23 4.7074265480041504 24 7.5730190277099609 25 8.7201385498046875
		 26 6.880795955657959 27 2.4965577125549316 28 -2.4094002246856689 29 -5.0862569808959961
		 30 -4.2181177139282227 31 -1.4009109735488892 32 1.9704985618591311 33 4.6396369934082031
		 34 5.5615777969360352 35 4.094022274017334 36 0.89729601144790649 37 -2.9841477870941162
		 38 -6.2976069450378418 39 -7.6770424842834464 40 -6.2166147232055664 41 -2.8545393943786621
		 42 1.0275084972381592 43 4.1773982048034668 44 5.5235705375671387 45 4.7482309341430664
		 46 2.5728178024291992 47 -0.43832004070281982 48 -3.6114432811737061 49 -6.1557049751281738
		 50 -7.2150554656982422 51 -5.7919559478759766 52 -2.4378604888916016 53 1.3942912817001343
		 54 4.4177818298339844 55 5.5869302749633789 56 4.2657089233398437 57 1.135067343711853
		 58 -2.7284784317016602 59 -6.0674853324890137 60 -7.4986481666564933 61 -6.3772091865539551
		 62 -3.5276224613189697 63 0.22106896340847015 64 4.0430717468261719 65 7.1619896888732901
		 66 7.0994081497192383 67 6.547823429107666 68 5.3515019416809082 69 2.7836935520172119
		 70 -1.3119206428527832 71 -6.2041616439819336 72 -11.108052253723145 73 -15.198550224304199
		 74 -17.633636474609375 75 -18.153669357299805 76 -17.349710464477539 77 -15.662140846252443
		 78 -13.534140586853027 79 -11.406912803649902 80 -9.7171039581298828;
createNode animCurveTA -n "Character1_RightShoulder_rotateZ";
	rename -uid "A902A717-4F4D-9A93-8159-569250656053";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 10.851101875305176 1 11.61586856842041
		 2 13.014700889587402 3 14.913562774658203 4 17.135704040527344 5 19.540773391723633
		 6 21.718990325927734 7 23.337345123291016 8 24.097871780395508 9 23.935583114624023
		 10 23.139778137207031 11 20.317195892333984 12 14.900278091430666 13 8.4353971481323242
		 14 2.7409536838531494 15 -0.070225253701210022 16 0.85411244630813599 17 3.9107072353363042
		 18 7.6493902206420898 19 10.771961212158203 20 12.079122543334961 21 10.761343002319336
		 22 7.6168842315673837 23 3.8608107566833496 24 0.80894845724105835 25 0.09585367888212204
		 26 3.9130451679229736 27 10.794034957885742 28 17.322444915771484 29 20.665018081665039
		 30 19.908037185668945 31 16.798917770385742 32 12.660096168518066 33 8.9202919006347656
		 34 7.18276023864746 35 8.3335084915161133 36 11.275226593017578 37 14.755012512207033
		 38 17.591718673706055 39 18.669267654418945 40 17.241342544555664 41 14.07465934753418
		 42 10.301480293273926 43 7.1367826461791992 44 5.9439501762390137 45 7.4024572372436523
		 46 10.587303161621094 47 14.524089813232422 48 18.327070236206055 49 21.204244613647461
		 50 22.411426544189453 51 20.954227447509766 52 17.24017333984375 53 12.598556518554688
		 54 8.4820079803466797 55 6.540494441986084 56 7.7130594253540048 57 10.834515571594238
		 58 14.525195121765135 59 17.55552864074707 60 18.775787353515625 61 17.912542343139648
		 62 15.78767681121826 63 12.85887622833252 64 9.5597248077392578 65 6.3567814826965332
		 66 5.6609926223754883 67 4.7041854858398437 68 3.4537107944488525 69 1.4937199354171753
		 70 -1.2032889127731323 71 -4.1346855163574219 72 -6.893826961517334 73 -9.1387348175048828
		 74 -10.538226127624512 75 -10.993753433227539 76 -10.78278923034668 77 -10.119812965393066
		 78 -9.2201881408691406 79 -8.3040103912353516 80 -7.6006412506103516;
createNode animCurveTU -n "Character1_RightShoulder_visibility";
	rename -uid "C2C3657A-4C2B-F101-3270-7493D4A05C9E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightArm_translateX";
	rename -uid "1DD3B6B6-4083-470C-EB9F-51A5DBFBB63B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.8354010581970215;
createNode animCurveTL -n "Character1_RightArm_translateY";
	rename -uid "60FA0117-4F48-4E97-23A7-BE9E806C9172";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1704782247543335;
createNode animCurveTL -n "Character1_RightArm_translateZ";
	rename -uid "2CADB7F4-4197-4931-600C-5DA93FB86292";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.3259291648864746;
createNode animCurveTU -n "Character1_RightArm_scaleX";
	rename -uid "B5C8F302-43C4-3669-CEE7-F4B697210D0C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightArm_scaleY";
	rename -uid "7D6CB9B7-4F0E-D523-53D8-BCBDEEB14C41";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightArm_scaleZ";
	rename -uid "B0AB3F18-4A09-3325-D701-3B9571276629";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightArm_rotateX";
	rename -uid "B9C33E62-4DEE-AA5D-6A07-34A0B93DA738";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 7.9758634567260742 1 15.093810081481935
		 2 19.476770401000977 3 18.800447463989258 4 13.019609451293945 5 4.8829131126403809
		 6 -3.8805503845214844 7 -11.491359710693359 8 -16.070917129516602 9 -17.79762077331543
		 10 -18.358074188232422 11 -17.207752227783203 12 -14.379743576049803 13 -11.042160987854004
		 14 -8.2239608764648437 15 -6.8569779396057129 16 -7.2173852920532227 17 -8.5544366836547852
		 18 -10.220694541931152 19 -11.514982223510742 20 -11.691107749938965 21 -10.258937835693359
		 22 -7.8135805130004883 23 -5.2167696952819824 24 -3.3019833564758301 25 -2.8369615077972412
		 26 -4.6312956809997559 27 -8.1771202087402344 28 -12.063276290893555 29 -14.582930564880371
		 30 -14.887430191040039 31 -13.96202564239502 32 -12.6375732421875 33 -11.618090629577637
		 34 -11.498985290527344 35 -12.642551422119141 36 -14.601909637451174 37 -16.767427444458008
		 38 -18.45551872253418 39 -18.910667419433594 40 -17.588329315185547 41 -15.018899917602539
		 42 -11.987055778503418 43 -9.2751016616821289 44 -7.6988348960876465 45 -7.5822229385375977
		 46 -8.4575691223144531 47 -9.9646739959716797 48 -11.70228099822998 49 -13.207821846008301
		 50 -13.964088439941406 51 -13.32221508026123 52 -11.626873970031738 53 -9.7681827545166016
		 54 -8.5042438507080078 55 -8.4739131927490234 56 -10.05793285369873 57 -12.690691947937012
		 58 -15.594677925109863 59 -18.039451599121094 60 -19.275932312011719 61 -19.059610366821289
		 62 -17.86149787902832 63 -15.952670097351074 64 -13.678735733032227 65 -11.535706520080566
		 66 -11.645671844482422 67 -11.964230537414551 68 -12.627972602844238 69 -15.201765060424805
		 70 -20.588565826416016 71 -28.476318359375 72 -37.544670104980469 73 -45.906951904296875
		 74 -51.521675109863281 75 -52.606975555419922 76 -48.919712066650391 77 -42.295886993408203
		 78 -35.024707794189453 79 -27.574621200561523 80 -20.871419906616211;
createNode animCurveTA -n "Character1_RightArm_rotateY";
	rename -uid "4FAB1BC8-4DA3-92BD-7B7E-61BF4D779236";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 70.84698486328125 1 55.705562591552734
		 2 40.33197021484375 3 30.455297470092773 4 26.767240524291992 5 25.5582275390625
		 6 25.563268661499023 7 25.77684211730957 8 25.568567276000977 9 25.003856658935547
		 10 24.412319183349609 11 22.471405029296875 12 18.809280395507813 13 14.812516212463379
		 14 11.625020027160645 15 10.08745002746582 16 10.717776298522949 17 13.03233528137207
		 18 16.18324089050293 19 19.001668930053711 20 20.066339492797852 21 18.462896347045898
		 22 15.281347274780273 23 11.98649787902832 24 9.6295957565307617 25 8.8586206436157227
		 26 10.591541290283203 27 14.393736839294432 28 18.549158096313477 29 20.740453720092773
		 30 19.80035400390625 31 17.082120895385742 32 13.782475471496582 33 11.031661987304688
		 34 9.8844614028930664 35 11.106794357299805 36 14.171530723571777 37 18.060375213623047
		 38 21.404146194458008 39 22.604995727539063 40 20.627782821655273 41 16.660400390625
		 42 12.395624160766602 43 9.1833248138427734 44 7.8958191871643066 45 8.5888242721557617
		 46 10.495382308959961 47 13.07682991027832 48 15.770347595214842 49 17.933603286743164
		 50 18.881763458251953 51 17.777456283569336 52 15.066351890563965 53 11.942704200744629
		 54 9.4667196273803711 55 8.5931978225708008 56 10.043919563293457 57 13.403407096862793
		 58 17.73066520690918 59 21.628602981567383 60 23.384145736694336 61 21.934391021728516
		 62 18.163970947265625 63 13.283893585205078 64 8.4446878433227539 65 4.6100478172302246
		 66 4.2081661224365234 67 4.7363276481628418 68 6.5135006904602051 69 10.313868522644043
		 70 15.965974807739258 71 22.031570434570312 72 27.485067367553711 73 31.915496826171871
		 74 34.930824279785156 75 37.288875579833984 76 40.3486328125 77 43.974613189697266
		 78 47.758987426757813 79 51.940242767333984 80 56.432392120361328;
createNode animCurveTA -n "Character1_RightArm_rotateZ";
	rename -uid "7BB43221-4851-6110-DE01-CBA22D9543F4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 17.512258529663086 1 23.68787956237793
		 2 29.510601043701172 3 33.352619171142578 4 35.060558319091797 5 36.261363983154297
		 6 36.964454650878906 7 37.398582458496094 8 38.092376708984375 9 38.857627868652344
		 10 39.079689025878906 11 37.872318267822266 12 34.844612121582031 13 30.897680282592773
		 14 27.331058502197266 15 25.788389205932617 16 27.410148620605469 17 31.004533767700195
		 18 34.835216522216797 19 37.434623718261719 20 37.613559722900391 21 34.621616363525391
		 22 29.281621932983398 23 22.946340560913086 24 17.348896026611328 25 14.436700820922852
		 26 15.866707801818846 27 20.032938003540039 28 24.154361724853516 29 26.232498168945313
		 30 25.808942794799805 31 23.994573593139648 32 21.639898300170898 33 19.779201507568359
		 34 19.616424560546875 35 22.192543029785156 36 26.538688659667969 37 30.977386474609375
		 38 34.118034362792969 39 34.906299591064453 40 32.660202026367187 41 28.023345947265625
		 42 22.112800598144531 43 16.461694717407227 44 12.99680233001709 45 12.408839225769043
		 46 13.52424144744873 47 15.547332763671875 48 17.738754272460937 49 19.491191864013672
		 50 20.367452621459961 51 19.781585693359375 52 17.955060958862305 53 15.74638557434082
		 54 14.271117210388184 55 14.836857795715332 56 18.538576126098633 57 24.257423400878906
		 58 30.084342956542972 59 34.492362976074219 60 36.351909637451172 61 35.577312469482422
		 62 33.115512847900391 63 29.447923660278317 64 25.094221115112305 65 20.731817245483398
		 66 20.175327301025391 67 18.966073989868164 68 16.988246917724609 69 13.339382171630859
		 70 7.9045228958129874 71 1.2910898923873901 72 -5.6150779724121094 73 -11.748130798339844
		 74 -16.525920867919922 75 -20.152175903320313 76 -23.486974716186523 77 -26.472831726074219
		 78 -29.051458358764648 79 -31.250881195068363 80 -33.016716003417969;
createNode animCurveTU -n "Character1_RightArm_visibility";
	rename -uid "12194C89-45AD-0E9F-891D-1087EC4595C6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightForeArm_translateX";
	rename -uid "58E943B8-4F9D-442D-43B3-698C204F030D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.753132820129395;
createNode animCurveTL -n "Character1_RightForeArm_translateY";
	rename -uid "3705005D-428E-EFAE-3D09-66B419BB0B19";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.862128257751465;
createNode animCurveTL -n "Character1_RightForeArm_translateZ";
	rename -uid "92E93C5D-4F35-7E44-DCFD-D589611E4A94";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.671875953674316;
createNode animCurveTU -n "Character1_RightForeArm_scaleX";
	rename -uid "F2E02BB5-4485-1FD9-90AB-139665C58AE3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightForeArm_scaleY";
	rename -uid "3D949C3E-4837-C7E8-77EA-0BB5C6CFD38F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightForeArm_scaleZ";
	rename -uid "D0A60CDC-4259-8AE6-C3E6-08B15787CF70";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightForeArm_rotateX";
	rename -uid "23E6F333-4818-DFCA-4BED-23AEF1732434";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 102.36378479003906 1 85.610282897949219
		 2 54.401927947998047 3 21.617942810058594 4 3.089259147644043 5 -6.0989861488342285
		 6 -10.22525691986084 7 -11.709776878356934 8 -11.910954475402832 9 -11.63446044921875
		 10 -11.1361083984375 11 -11.58683967590332 12 -13.436885833740234 13 -15.894972801208496
		 14 -18.065135955810547 15 -18.892620086669922 16 -17.811813354492188 17 -15.604640960693358
		 18 -13.14380931854248 19 -11.22062873840332 20 -10.582544326782227 21 -11.736933708190918
		 22 -14.151598930358887 23 -16.995737075805664 24 -19.354511260986328 25 -20.330242156982422
		 26 -18.825979232788086 27 -15.53751277923584 28 -12.256677627563477 29 -10.696348190307617
		 30 -11.524954795837402 31 -13.70643138885498 32 -16.362203598022461 33 -18.533580780029297
		 34 -19.187358856201172 35 -17.617555618286133 36 -14.461885452270508 37 -10.713262557983398
		 38 -7.3484239578247061 39 -5.292386531829834 40 -5.1529402732849121 41 -6.3597903251647949
		 42 -8.0484066009521484 43 -9.3116512298583984 44 -9.1929159164428711 45 -7.3480019569396973
		 46 -4.486783504486084 47 -1.2189598083496094 48 1.8675100803375242 49 4.2156863212585449
		 50 5.2622966766357422 51 4.3628649711608887 52 1.8382360935211182 53 -1.3818424940109253
		 54 -4.2917346954345703 55 -5.8213768005371094 56 -5.2369074821472168 57 -3.1688635349273682
		 58 -0.57281327247619629 59 1.6361995935440063 60 2.5863010883331299 61 1.9188990592956545
		 62 0.14373670518398285 63 -2.2983064651489258 64 -4.945258617401123 65 -7.3066363334655771
		 66 -7.2939796447753906 67 -7.2814807891845703 68 -7.2691311836242676 69 -13.380237579345703
		 70 -23.278474807739258 71 -30.860733032226563 72 -29.8663444519043 73 3.81180739402771
		 74 58.070980072021484 75 64.889450073242187 76 70.252822875976562 77 70.827491760253906
		 78 63.209045410156257 79 55.029808044433594 80 50.958099365234375;
createNode animCurveTA -n "Character1_RightForeArm_rotateY";
	rename -uid "973FB1F9-4D1A-A08E-6C48-2791E5874CC8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 46.629539489746094 1 58.576438903808601
		 2 64.990608215332031 3 60.915737152099609 4 49.133571624755859 5 33.600368499755859
		 6 17.539649963378906 7 3.2414693832397461 8 -7.4038124084472656 9 -14.375766754150392
		 10 -18.989046096801758 11 -20.857440948486328 12 -20.344655990600586 13 -18.835573196411133
		 14 -17.467458724975586 15 -17.417901992797852 16 -19.0191650390625 17 -21.362760543823242
		 18 -23.760929107666016 19 -25.588096618652344 20 -26.312135696411133 21 -25.6346435546875
		 22 -24.007890701293945 23 -22.06666374206543 24 -20.488445281982422 25 -19.881008148193359
		 26 -20.980854034423828 27 -23.245655059814453 28 -25.375158309936523 29 -26.240310668945312
		 30 -25.483755111694336 31 -23.821178436279297 32 -21.835851669311523 33 -20.183464050292969
		 34 -19.59089469909668 35 -20.577571868896484 36 -22.709903717041016 37 -25.293081283569336
		 38 -27.661199569702148 39 -29.169538497924801 40 -29.345760345458984 41 -28.508152008056641
		 42 -27.234247207641602 43 -26.223114013671875 44 -26.28441047668457 45 -27.760833740234375
		 46 -30.108732223510742 47 -32.825550079345703 48 -35.408927917480469 49 -37.384563446044922
		 50 -38.292091369628906 51 -37.619781494140625 52 -35.632980346679688 53 -33.090633392333984
		 54 -30.823125839233398 55 -29.709760665893558 56 -30.323909759521484 57 -32.098514556884766
		 58 -34.22381591796875 59 -35.98223876953125 60 -36.751857757568359 61 -36.30426025390625
		 62 -35.021984100341797 63 -33.222389221191406 64 -31.247575759887692 65 -29.477846145629883
		 66 -29.553848266601562 67 -29.628437042236328 68 -29.701692581176761 69 -20.30317497253418
		 70 2.4353127479553223 71 28.936491012573242 72 56.152091979980469 73 76.099136352539062
		 74 74.830467224121094 75 69.443580627441406 76 63.971214294433594 77 60.067569732666023
		 78 59.58404541015625 79 59.754558563232422 80 59.443778991699219;
createNode animCurveTA -n "Character1_RightForeArm_rotateZ";
	rename -uid "6CE32916-464B-D747-E350-74ABDA22276E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 58.55743408203125 1 34.98992919921875
		 2 -2.2012150287628174 3 -38.327610015869141 4 -58.006961822509773 5 -68.014091491699219
		 6 -73.349617004394531 7 -76.808815002441406 8 -79.469375610351563 9 -81.438423156738281
		 10 -82.705520629882813 11 -84.3026123046875 12 -86.814727783203125 13 -89.592979431152344
		 14 -91.919540405273437 15 -93.138877868652344 16 -92.637283325195313 17 -90.804054260253906
		 18 -88.503433227539063 19 -86.611427307128906 20 -86.02325439453125 21 -87.326606750488281
		 22 -89.884857177734375 23 -92.770240783691406 24 -95.081024169921875 25 -95.997550964355469
		 26 -94.523139953613281 27 -91.335342407226563 28 -88.153327941894531 29 -86.67633056640625
		 30 -87.519332885742188 31 -89.566680908203125 32 -91.919548034667969 33 -93.717445373535156
		 34 -94.134101867675781 35 -92.662117004394531 36 -89.87255859375 37 -86.59356689453125
		 38 -83.647735595703125 39 -81.821304321289063 40 -81.603446960449219 41 -82.435684204101563
		 42 -83.541748046875 43 -84.221038818359375 44 -83.855056762695313 45 -82.307212829589844
		 46 -80.122406005859375 47 -77.719253540039063 48 -75.487205505371094 49 -73.812782287597656
		 50 -73.125869750976563 51 -73.88983154296875 52 -75.787322998046875 53 -78.101112365722656
		 54 -80.174461364746094 55 -81.373405456542969 56 -81.251213073730469 57 -80.150093078613281
		 58 -78.621719360351563 59 -77.262451171875 60 -76.713211059570313 61 -77.268402099609375
		 62 -78.558540344238281 63 -80.240089416503906 64 -81.98956298828125 65 -83.5057373046875
		 66 -83.550308227539063 67 -83.5941162109375 68 -83.637191772460938 69 -79.339653015136719
		 70 -74.878189086914063 71 -74.408821105957031 72 -71.619712829589844 73 -40.537979125976562
		 74 11.82133674621582 75 20.440885543823242 76 30.397148132324215 77 38.410263061523438
		 78 39.574264526367188 79 40.117218017578125 80 43.932643890380859;
createNode animCurveTU -n "Character1_RightForeArm_visibility";
	rename -uid "559C5853-4351-5EAC-B819-73AB48DA401C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHand_translateX";
	rename -uid "D519F1A6-4F44-F2AD-616B-9080529F604E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7167695760726929;
createNode animCurveTL -n "Character1_RightHand_translateY";
	rename -uid "7BEF4628-4D39-14CD-15DB-74AC9B68F760";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.314067840576172;
createNode animCurveTL -n "Character1_RightHand_translateZ";
	rename -uid "3018CEB7-4550-D7D4-9320-E6B1F0932E31";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4444055557250977;
createNode animCurveTU -n "Character1_RightHand_scaleX";
	rename -uid "BDFDDC9E-4989-2E57-34EC-5F933DA5D6CD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightHand_scaleY";
	rename -uid "0A2BFC9E-4C8E-E98B-08AE-A49F388C48F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightHand_scaleZ";
	rename -uid "CD414CC0-4049-A7C4-1BEB-D2828AB5E454";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightHand_rotateX";
	rename -uid "C10EFE53-4FC3-AEDE-47F5-F3A89496F45D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 12.774242401123047 1 15.087244033813478
		 2 15.720623016357422 3 15.430498123168947 4 16.577907562255859 5 20.595138549804688
		 6 26.647869110107422 7 33.32989501953125 8 38.769962310791016 9 42.235321044921875
		 10 44.434722900390625 11 46.875102996826172 12 50.235610961914063 13 53.752685546875
		 14 56.83489990234375 15 58.887603759765625 16 59.464813232421875 17 58.958412170410156
		 18 58.032173156738274 19 57.362384796142571 20 57.638534545898438 21 59.321861267089844
		 22 61.922863006591804 23 64.708282470703125 24 66.942291259765625 25 67.900428771972656
		 26 66.743728637695313 27 64.040206909179687 28 61.216636657714844 29 59.706481933593757
		 30 60.082508087158196 31 61.504711151123047 32 63.289138793945313 33 64.759475708007812
		 34 65.246017456054687 35 64.316963195800781 36 62.398662567138672 37 60.128543853759766
		 38 58.216361999511719 39 57.371437072753906 40 58.064228057861328 41 59.817810058593757
		 42 61.920310974121101 43 63.663848876953132 44 64.345680236816406 45 63.719795227050774
		 46 62.278907775878906 47 60.430233001708984 48 58.580543518066406 49 57.136444091796875
		 50 56.503883361816406 51 57.170516967773437 52 58.882438659667969 53 60.934318542480469
		 54 62.625782012939446 55 63.261985778808594 56 62.38273620605468 57 60.45396423339843
		 58 58.171428680419922 59 56.231552124023438 60 55.331764221191406 61 55.703479766845703
		 62 56.879268646240234 63 58.50830078125 64 60.241527557373047 65 61.732452392578118
		 66 61.542343139648437 67 61.345008850097656 68 61.140361785888665 69 57.810180664062493
		 70 48.874454498291016 71 36.732471466064453 72 22.673477172851563 73 6.6761856079101563
		 74 -6.9128475189208984 75 -13.180136680603027 76 -11.715028762817383 77 -5.6179823875427246
		 78 1.0427714586257935 79 8.4211826324462891 80 15.514721870422363;
createNode animCurveTA -n "Character1_RightHand_rotateY";
	rename -uid "37203FFD-4C41-3EEC-8829-1E865D6A36C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 6.2294893264770508 1 9.0218124389648437
		 2 9.9608602523803711 3 8.5667953491210937 4 5.4426517486572266 5 2.3699853420257568
		 6 0.60456866025924683 7 0.90863555669784535 8 3.1679103374481201 9 6.1678075790405273
		 10 8.6543998718261719 11 9.7253694534301758 12 9.7578115463256836 13 9.751495361328125
		 14 9.8134422302246094 15 10.073801040649414 16 10.60113525390625 17 11.287423133850098
		 18 12.010702133178711 19 12.674463272094727 20 13.207644462585449 21 13.560098648071289
		 22 13.745211601257324 23 13.798042297363281 24 13.779583930969238 25 13.775663375854492
		 26 13.90372371673584 27 14.073018074035645 28 14.115275382995605 29 13.950057029724121
		 30 13.600646018981934 31 13.154767036437988 32 12.67385196685791 33 12.235861778259277
		 34 11.93659496307373 35 11.845124244689941 36 11.874044418334961 37 11.90459156036377
		 38 11.87880802154541 39 11.762492179870605 40 11.532706260681152 41 11.203886032104492
		 42 10.811152458190918 43 10.421346664428711 44 10.130534172058105 45 9.9873189926147461
		 46 9.9382152557373047 47 9.9328384399414062 48 9.9252357482910156 49 9.8776788711547852
		 50 9.7565183639526367 51 9.5101795196533203 52 9.1407546997070312 53 8.7052793502807617
		 54 8.2941255569458008 55 8.0271739959716797 56 7.9966092109680176 57 8.1372842788696289
		 58 8.3397054672241211 59 8.5040102005004883 60 8.5424833297729492 61 8.4241189956665039
		 62 8.1985874176025391 63 7.9017124176025382 64 7.5814089775085449 65 7.300527572631835
		 66 7.3390407562255859 67 7.4025635719299316 68 7.4922671318054199 69 5.8086919784545898
		 70 4.1863136291503906 71 4.9618015289306641 72 8.4654989242553711 73 11.397763252258301
		 74 8.2280435562133789 75 2.3910336494445801 76 1.2645488977432251 77 2.7627098560333252
		 78 3.8899512290954585 79 5.0558686256408691 80 5.7927789688110352;
createNode animCurveTA -n "Character1_RightHand_rotateZ";
	rename -uid "09FB59CD-4888-1CC7-ADB1-C68487EF0626";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 36.059032440185547 1 46.490947723388672
		 2 58.489543914794915 3 67.867378234863281 4 70.59307861328125 5 66.281570434570313
		 6 58.281379699707024 7 48.102310180664063 8 37.299659729003906 9 27.306447982788086
		 10 19.605918884277344 11 16.035234451293945 12 15.296840667724611 13 14.702868461608887
		 14 14.044513702392578 15 13.130573272705078 16 11.808195114135742 17 10.174424171447754
		 18 8.426173210144043 19 6.7907710075378418 20 5.5225400924682617 21 4.8081374168395996
		 22 4.5345101356506348 23 4.514561653137207 24 4.5823712348937988 25 4.5805683135986328
		 26 4.2641606330871582 27 3.7107758522033687 28 3.2438399791717529 29 3.2483072280883789
		 30 3.8676531314849854 31 4.7997899055480957 32 5.7857761383056641 33 6.5906462669372559
		 34 7.0066194534301758 35 6.8925051689147949 36 6.431612491607666 37 5.8880457878112793
		 38 5.4512743949890137 39 5.3229236602783203 40 5.6303081512451172 41 6.2211437225341797
		 42 6.8848867416381836 43 7.4415163993835449 44 7.7450647354125977 45 7.7624602317810059
		 46 7.6084923744201651 47 7.3693299293518066 48 7.1347541809082031 49 7.0021443367004395
		 50 7.0724573135375977 51 7.4584870338439933 52 8.0773830413818359 53 8.7442836761474609
		 54 9.3067436218261719 55 9.6469154357910156 56 9.6984415054321289 57 9.5447416305541992
		 58 9.3053607940673828 59 9.1200942993164062 60 9.1504001617431641 61 9.4543066024780273
		 62 9.9200315475463867 63 10.458542823791504 64 10.989870071411133 65 11.449089050292969
		 66 11.594485282897949 67 11.729487419128418 68 11.853022575378418 69 17.991500854492187
		 70 28.209024429321289 71 35.856487274169922 72 39.007343292236328 73 33.183139801025391
		 74 22.368930816650391 75 13.780427932739258 76 12.773374557495117 77 13.583952903747559
		 78 11.278022766113281 79 8.5959348678588867 80 6.283902645111084;
createNode animCurveTU -n "Character1_RightHand_visibility";
	rename -uid "F5BEEE88-445D-4AE3-0096-E7B932FA3ECD";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb1_translateX";
	rename -uid "FA9A5BF4-4785-4937-5995-02829C1FEACA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5741705894470215;
createNode animCurveTL -n "Character1_RightHandThumb1_translateY";
	rename -uid "F5CA084E-4D48-D1E5-8655-BFA13B153BBE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4327812194824219;
createNode animCurveTL -n "Character1_RightHandThumb1_translateZ";
	rename -uid "2306095D-4CFD-1EBE-A37F-BB8F961ACFA2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8196568489074707;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleX";
	rename -uid "B8CEE687-44A0-D976-337A-5F89DFEAF590";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleY";
	rename -uid "50E543A5-4603-78D0-BC27-1C875532402F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleZ";
	rename -uid "4DA35DC6-4F1E-FD37-9185-97947395207E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateX";
	rename -uid "89C6AA19-4B08-FAB2-50A3-E680F0167A08";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 38.973472595214844 1 48.512489318847656
		 2 55.272006988525391 3 57.314090728759766 4 57.314090728759766 5 57.314090728759766
		 6 57.314090728759766 7 54.569000244140625 8 44.736270904541016 9 25.89476203918457
		 10 3.4557018280029297 11 -12.540787696838379 12 -17.554241180419922 13 -16.452259063720703
		 14 -15.650698661804201 15 -15.546984672546387 16 -16.598875045776367 17 -18.581083297729492
		 18 -20.847862243652344 19 -22.658212661743164 20 -23.162631988525391 21 -21.820718765258789
		 22 -19.262353897094727 23 -16.315568923950195 24 -13.660248756408691 25 -11.875063896179199
		 26 -11.193607330322266 27 -11.209628105163574 28 -11.531805038452148 29 -11.770546913146973
		 30 -11.723084449768066 31 -11.576889991760254 32 -11.524393081665039 33 -11.757889747619629
		 34 -12.473451614379883 35 -13.963393211364746 36 -16.141620635986328 37 -18.634872436523438
		 38 -21.021469116210937 39 -22.795942306518555 40 -23.76344108581543 41 -24.210062026977539
		 42 -24.308553695678711 43 -24.244840621948242 44 -24.207721710205078 45 -24.134561538696289
		 46 -23.900165557861328 47 -23.599025726318359 48 -23.323883056640625 49 -23.165346145629883
		 50 -23.213373184204102 51 -23.855119705200195 52 -25.064115524291992 53 -26.285987854003906
		 54 -26.90869140625 55 -26.301254272460937 56 -23.795234680175781 57 -20.028659820556641
		 58 -16.162057876586914 59 -13.05612850189209 60 -11.448348999023437 61 -11.57457160949707
		 62 -12.853729248046875 63 -14.830139160156252 64 -17.04066276550293 65 -18.971302032470703
		 66 -18.971302032470703 67 -18.971302032470703 68 -18.971302032470703 69 -18.237628936767578
		 70 -16.150856018066406 71 -12.868801116943359 72 -8.5407009124755859 73 -3.3214960098266602
		 74 2.6107761859893799 75 9.0387992858886719 76 15.699910163879395 77 22.298873901367188
		 78 28.539577484130859 79 34.163017272949219 80 38.973472595214844;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateY";
	rename -uid "299FA912-4476-2116-935E-08BE61FCC8B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 27.536184310913086 1 23.533174514770508
		 2 16.814355850219727 3 13.127543449401855 4 13.127543449401855 5 13.127543449401855
		 6 13.127543449401855 7 17.367149353027344 8 26.429363250732422 9 33.242183685302734
		 10 32.6439208984375 11 26.919979095458984 12 22.70030403137207 13 21.159530639648437
		 14 20.017877578735352 15 19.868917465209961 16 21.366479873657227 17 24.104598999023438
		 18 27.091323852539063 19 29.359674453735352 20 29.972721099853516 21 28.323478698730469
		 22 25.018928527832031 23 20.966062545776367 24 17.112455368041992 25 14.430891990661619
		 26 13.391025543212891 27 13.415565490722656 28 13.908124923706055 29 14.271945953369141
		 30 14.199698448181152 31 13.976907730102539 32 13.89681339263916 33 14.252681732177734
		 34 15.336948394775389 35 17.561058044433594 36 20.71912956237793 37 24.177297592163086
		 38 27.313425064086914 39 29.527887344360352 40 30.692007064819339 41 31.218999862670902
		 42 31.334327697753903 43 31.259759902954102 44 31.216253280639648 45 31.130373001098629
		 46 30.854032516479489 47 30.496347427368164 48 30.16693115234375 49 29.975994110107425
		 50 30.03392219543457 51 30.80072021484375 52 32.208412170410156 53 33.582077026367188
		 54 34.263233184814453 55 33.598930358886719 56 30.729742050170898 57 26.030378341674805
		 58 20.748186111450195 59 16.212387084960938 60 13.780707359313965 61 13.973373413085938
		 62 15.909086227416992 63 18.831840515136719 64 21.986473083496094 65 24.630031585693359
		 66 24.630031585693359 67 24.630031585693359 68 24.630031585693359 69 24.873746871948242
		 70 25.545331954956055 71 26.533388137817383 72 27.699766159057617 73 28.884601593017578
		 74 29.917604446411136 75 30.637708663940433 76 30.919200897216793 77 30.697742462158203
		 78 29.987277984619141 79 28.88095855712891 80 27.536184310913086;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateZ";
	rename -uid "7430D87B-4121-8775-CC03-9FAFBD053B2B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -17.683155059814453 1 -3.8307714462280278
		 2 8.5896387100219727 3 13.751087188720703 4 13.751087188720703 5 13.751087188720703
		 6 13.751087188720703 7 9.886073112487793 8 -1.2248704433441162 9 -18.842226028442383
		 10 -36.934867858886719 11 -47.622703552246094 12 -50.397056579589844 13 -50.098846435546875
		 14 -49.900737762451172 15 -49.876277923583984 16 -50.136810302734375 17 -50.701229095458984
		 18 -51.459022521972656 19 -52.145359039306641 20 -52.348808288574219 21 -51.819198608398438
		 22 -50.916694641113281 23 -50.063934326171875 24 -49.479255676269531 25 -49.189205169677734
		 26 -49.100944519042969 27 -49.102878570556641 28 -49.143184661865234 29 -49.174858093261719
		 30 -49.168437957763672 31 -49.1490478515625 32 -49.142223358154297 33 -49.173137664794922
		 34 -49.276996612548828 35 -49.536853790283203 36 -50.020172119140625 37 -50.717849731445312
		 38 -51.521797180175781 39 -52.200397491455078 40 -52.597858428955078 41 -52.787639617919922
		 42 -52.83001708984375 43 -52.802581787109375 44 -52.786632537841797 45 -52.755279541015625
		 46 -52.655536651611328 47 -52.528984069824219 48 -52.414939880371094 49 -52.349918365478516
		 50 -52.369560241699219 51 -52.636493682861328 52 -53.161338806152344 53 -53.719863891601563
		 54 -54.014957427978516 55 -53.727016448974609 56 -52.611240386962891 57 -51.171772003173828
		 58 -50.025276184082031 59 -49.371650695800781 60 -49.132472991943359 61 -49.14874267578125
		 62 -49.337753295898438 63 -49.714675903320312 64 -50.25439453125 65 -50.823329925537109
		 66 -50.823329925537109 67 -50.823329925537109 68 -50.823329925537109 69 -50.566719055175781
		 70 -49.803245544433594 71 -48.505714416503906 72 -46.625316619873047 73 -44.124946594238281
		 74 -41.010341644287109 75 -37.354652404785156 76 -33.307926177978516 77 -29.083986282348636
		 78 -24.925901412963867 79 -21.063697814941406 80 -17.683155059814453;
createNode animCurveTU -n "Character1_RightHandThumb1_visibility";
	rename -uid "6B387235-4776-EB05-E75F-66BD4ECD2E6D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb2_translateX";
	rename -uid "7B1FACAF-43EE-C8DE-6A8E-DDAA92A09040";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.7422792911529541;
createNode animCurveTL -n "Character1_RightHandThumb2_translateY";
	rename -uid "9AB278C7-44E0-D55F-528C-A5A4AEAC3578";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5767025947570801;
createNode animCurveTL -n "Character1_RightHandThumb2_translateZ";
	rename -uid "B416D4D6-4AEB-6BD6-1D8A-988E66BD13B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.0428388118743896;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleX";
	rename -uid "C04DBC90-4FD6-6E77-C259-BF85D307608B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleY";
	rename -uid "D27184E6-462C-6826-726F-889F27F75CD7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleZ";
	rename -uid "86388B43-43FA-24A8-D837-508997CF0DD5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateX";
	rename -uid "8F70B328-4B67-C286-C405-42998E30FB98";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 3.8401813507080074 1 7.6186804771423331
		 2 11.501864433288574 3 13.273929595947266 4 13.273929595947266 5 13.273929595947266
		 6 13.273929595947266 7 14.940045356750487 8 19.440450668334961 9 25.941390991210937
		 10 33.032806396484375 11 39.227241516113281 12 43.130508422851562 13 44.930637359619141
		 14 46.308311462402344 15 46.490673065185547 16 44.684799194335938 17 41.552997589111328
		 18 38.411533355712891 19 36.22833251953125 20 35.668903350830078 21 37.203571319580078
		 22 40.559814453125 23 45.161556243896484 24 49.963336944580078 25 53.489456176757813
		 26 54.885284423828125 27 54.852203369140625 28 54.189590454101562 29 53.701969146728516
		 30 53.798671722412109 31 54.097282409667969 32 54.204780578613281 33 53.727745056152344
		 34 52.284763336181641 35 49.386390686035156 36 45.45782470703125 37 41.473041534423828
		 38 38.18994140625 39 36.073535919189453 40 35.029182434082031 41 34.571884155273438
		 42 34.473094940185547 43 34.536914825439453 44 34.574241638183594 45 34.648117065429688
		 46 34.887557983398437 47 35.201416015625 48 35.494403839111328 49 35.665950775146484
		 50 35.61376953125 51 34.934051513671875 52 33.739303588867188 53 32.639266967773437
		 54 32.117683410644531 55 32.626174926757813 56 34.996116638183594 57 39.493267059326172
		 58 45.422878265380859 59 51.133029937744141 60 54.360744476318359 61 54.102027893066406
		 62 51.53057861328125 63 47.776088714599609 64 43.955753326416016 65 40.978916168212891
		 66 40.978916168212891 67 40.978916168212891 68 40.978916168212891 69 40.438213348388672
		 70 38.879550933837891 71 36.398319244384766 72 33.136127471923828 73 29.292898178100586
		 74 25.102668762207031 75 20.801820755004883 76 16.605148315429688 77 12.692388534545898
		 78 9.2027311325073242 79 6.2335214614868164 80 3.8401813507080074;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateY";
	rename -uid "E5F868A1-434E-3A77-9857-AEB12F9B2521";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 4.2117342948913574 1 -7.8167433738708487
		 2 -19.769496917724609 3 -25.162002563476563 4 -25.162002563476563 5 -25.162002563476563
		 6 -25.162002563476563 7 -28.285764694213867 8 -35.897403717041016 9 -45.328861236572266
		 10 -54.193416595458984 11 -60.811653137207038 12 -63.64826583862304 13 -64.0458984375
		 14 -64.310256958007813 15 -64.342811584472656 16 -63.995193481445305 17 -63.246238708496094
		 18 -62.271556854248047 19 -61.428607940673828 20 -61.186325073242187 21 -61.824272155761719
		 22 -62.964801788330071 23 -64.092529296875 24 -64.8621826171875 25 -65.211502075195312
		 26 -65.304367065429688 27 -65.302452087402344 28 -65.261192321777344 29 -65.227249145507812
		 30 -65.234222412109375 31 -65.2550048828125 32 -65.262199401855469 33 -65.229118347167969
		 34 -65.111053466796875 35 -64.788543701171875 36 -64.150970458984375 37 -63.224422454833977
		 38 -62.19282531738282 39 -61.362728118896484 40 -60.894443511962884 41 -60.675453186035163
		 42 -60.626956939697266 43 -60.658336639404304 44 -60.676605224609375 45 -60.712581634521491
		 46 -60.827575683593757 47 -60.974647521972663 48 -61.108318328857429 49 -61.185012817382813
		 50 -61.161804199218743 51 -60.849617004394524 52 -60.252761840820313 53 -59.641460418701172
		 54 -59.328006744384766 55 -59.6337890625 56 -60.87890625 57 -62.635894775390625 58 -64.144157409667969
		 59 -64.99688720703125 60 -65.272384643554688 61 -65.255325317382813 62 -65.038330078125
		 63 -64.556831359863281 64 -63.838233947753906 65 -63.086372375488281 66 -63.086372375488281
		 67 -63.086372375488281 68 -63.086372375488281 69 -62.3099365234375 70 -60.096263885498047
		 71 -56.599979400634766 72 -51.963367462158203 73 -46.330585479736328 74 -39.860198974609375
		 75 -32.735328674316406 76 -25.169895172119141 77 -17.409746170043945 78 -9.7286758422851562
		 79 -2.4203794002532959 80 4.2117342948913574;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateZ";
	rename -uid "437D3EF5-4C13-F8CE-28FE-0EA434D12100";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 80 ".ktv[0:79]"  0 2.4896585941314697 1 0.75272458791732788
		 2 -0.4614121019840241 3 -0.82732129096984863 4 -0.82732129096984863 5 -0.82732129096984863
		 6 -0.82732129096984863 7 -1.8089501857757568 8 -4.1129698753356934 9 -6.8172988891601563
		 10 -9.1707963943481445 11 -11.713350296020508 12 -15.070032119750975 13 -18.311996459960938
		 14 -20.753570556640625 15 -21.074489593505859 16 -17.872917175292969 17 -12.171611785888672
		 18 -6.1953606605529785 19 -1.8316210508346558 20 -0.67813676595687866 21 -3.80622410774231
		 22 -10.314515113830566 23 -18.723459243774414 24 -27.101860046386719 25 -33.099514007568359
		 26 -35.451042175292969 27 -35.395423889160156 28 -34.280265808105469 29 -33.458194732666016
		 30 -33.621322631835938 31 -34.124736785888672 32 -34.305850982666016 33 -33.501686096191406
		 34 -31.060977935791016 35 -26.110248565673828 36 -19.25001335144043 37 -12.023079872131348
		 38 -5.7614006996154785 39 -1.5140315294265747 40 0.66140377521514893 41 1.6334487199783325
		 42 1.8451242446899416 43 1.7083042860031128 44 1.6284040212631226 45 1.4705255031585693
		 46 0.96110028028488159 47 0.29849424958229065 48 -0.31498816609382629 49 -0.67200851440429688
		 50 -0.56358015537261963 51 0.86257368326187134 52 3.4373369216918945 53 5.8972859382629395
		 54 7.0985031127929687 55 5.927149772644043 56 0.73126697540283203 57 -8.28839111328125
		 58 -19.187978744506836 59 -29.102420806884769 60 -34.568515777587891 61 -34.132732391357422
		 62 -29.779634475708008 63 -23.32305908203125 64 -16.564178466796875 65 -11.10146427154541
		 66 -11.10146427154541 68 -11.101465225219727 69 -11.092635154724121 70 -11.01258373260498
		 71 -10.754746437072754 72 -10.235605239868164 73 -9.42083740234375 74 -8.3147964477539062
		 75 -6.9420537948608398 76 -5.3354167938232422 77 -3.532785177230835 78 -1.5808253288269043
		 79 0.45691525936126709 80 2.4896588325500488;
createNode animCurveTU -n "Character1_RightHandThumb2_visibility";
	rename -uid "1F5F747C-4AE9-88C8-C464-BB86DA17863E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb3_translateX";
	rename -uid "977B71D0-48A8-86C2-C71A-159C0770FD52";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.588839054107666;
createNode animCurveTL -n "Character1_RightHandThumb3_translateY";
	rename -uid "13DEB696-44CE-20C8-7D6F-E1BD3E53CECC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.13479287922382355;
createNode animCurveTL -n "Character1_RightHandThumb3_translateZ";
	rename -uid "01AEDC49-4DD7-7A4D-1180-7CA21C2F57F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.4058942794799805;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleX";
	rename -uid "281F3EE1-4929-7FA4-54D7-37A6B5D695CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleY";
	rename -uid "41F49BC4-428F-07A1-C8AD-C8BD8DA7D7D2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleZ";
	rename -uid "98258F5E-4527-F86B-7491-E38F47993FC0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateX";
	rename -uid "615438A7-475E-00DE-1949-42BDB2DB7423";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -2.8711519917123951e-007 10 -2.5143987159026437e-007
		 11 0.25366201996803284 12 0.8509332537651062 13 1.5424562692642212 14 2.0626628398895264
		 15 2.1310629844665527 16 1.4488928318023682 17 0.23015981912612915 18 -1.0648348331451416
		 19 -2.0302879810333252 20 -2.2891895771026611 21 -1.5908609628677368 22 -0.16966240108013153
		 23 1.6301229000091553 24 3.4207744598388672 25 4.7246026992797852 26 5.2450165748596191
		 27 5.232633113861084 28 4.9851293563842773 29 4.803584098815918 30 4.8395509719848633
		 31 4.9507260322570801 32 4.9907917976379395 33 4.81317138671875 34 4.2781319618225098
		 35 3.2076120376586914 36 1.7423026561737061 37 0.19824972748756409 38 -1.1599636077880859
		 39 -2.1014020442962646 40 -2.592085599899292 41 -2.8134884834289551 42 -2.861889123916626
		 43 -2.8305966854095459 44 -2.812335729598999 45 -2.7762813568115234 46 -2.6601991653442383
		 47 -2.5097777843475342 48 -2.3710606098175049 49 -2.2905697822570801 50 -2.3149969577789307
		 51 -2.6377918720245361 52 -3.2282142639160156 53 -3.8026099205017094 54 -4.0871443748474121
		 55 -3.8096504211425781 56 -2.607952356338501 57 -0.60834771394729614 58 1.7290859222412109
		 59 3.8525960445404053 60 5.0489645004272461 61 4.9524931907653809 62 3.999393224716187
		 63 2.6108188629150391 64 1.1698780059814453 65 -1.9196342293525959e-007 66 -1.939741025580588e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateY";
	rename -uid "E2E3D364-40C7-7DFF-4ED4-7A96D0DE2FC8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -7.8692045235584374e-007 10 -7.8091142086123e-007
		 11 -0.63436472415924072 12 -2.1079914569854736 13 -3.7782540321350102 14 -5.0088214874267578
		 15 -5.1689519882202148 16 -3.5545525550842285 17 -0.57580107450485229 18 2.7164351940155029
		 19 5.2500715255737305 20 5.9401884078979492 21 4.0889096260070801 22 0.42707875370979309
		 23 -3.9872021675109859 24 -8.1147003173828125 25 -10.949671745300293 26 -12.041083335876465
		 27 -12.015377998352051 28 -11.4989013671875 29 -11.116782188415527 30 -11.192706108093262
		 31 -11.426701545715332 32 -11.510775566101074 33 -11.137030601501465 34 -9.9951047897338867
		 35 -7.6375112533569345 36 -4.2536506652832031 37 -0.49621707201004028 38 2.9631867408752441
		 39 5.4391961097717285 40 6.7529945373535156 41 7.3506922721862802 42 7.4817466735839844
		 43 7.3970003128051758 44 7.3475723266601563 45 7.2500414848327646 46 6.9365568161010742
		 47 6.5315585136413574 48 6.1593170166015625 49 5.9438791275024414 50 6.00921630859375
		 51 6.8761391639709473 52 8.4780597686767578 53 10.055158615112305 54 10.842639923095703
		 55 10.074596405029297 56 6.7957291603088379 57 1.5415300130844116 58 -4.2223124504089355
		 59 -9.0695829391479492 60 -11.632602691650391 61 -11.430413246154785 62 -9.390594482421875
		 63 -6.2810931205749512 64 -2.883192777633667 65 -7.6250699976299074e-007 66 -7.837930411369598e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateZ";
	rename -uid "DB726C5B-4AE0-4F94-234A-87A2459D96DE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 2.1295778651619912e-007 10 2.7416413672654016e-007
		 11 -0.15496274828910828 12 -0.52665030956268311 13 -0.96842926740646373 14 -1.3084458112716675
		 15 -1.3536263704299927 16 -0.90796244144439697 17 -0.14053134620189667 18 0.63065028190612793
		 19 1.1728991270065308 20 1.3132293224334717 21 0.92973071336746216 22 0.10265684127807617
		 23 -1.025277853012085 24 -2.225024938583374 25 -3.1408505439758301 26 -3.5153443813323975
		 27 -3.5063767433166504 28 -3.3277177810668945 29 -3.1973705291748047 30 -3.2231464385986328
		 31 -3.3029704093933105 32 -3.3317928314208984 33 -3.2042391300201416 34 -2.8235347270965576
		 35 -2.0785291194915771 36 -1.0982919931411743 37 -0.12096117436885834 38 0.68536704778671265
		 39 1.2116644382476807 40 1.4745645523071289 41 1.5905145406723022 42 1.6156363487243652
		 43 1.5994036197662354 44 1.5899151563644409 45 1.5711472034454346 46 1.5104155540466309
		 47 1.4310308694839478 48 1.3571420907974243 49 1.3139714002609253 50 1.3270957469940186
		 51 1.4986392259597778 52 1.8031017780303957 53 2.0873012542724609 54 2.2235379219055176
		 55 2.0907092094421387 56 1.482930064201355 57 0.36431840062141418 58 -1.0896739959716797
		 59 -2.5246479511260986 60 -3.3736920356750488 61 -3.304241418838501 62 -2.6273550987243652
		 63 -1.6735172271728516 64 -0.72892236709594727 65 3.3662144005575101e-007 66 3.3540339927640161e-007;
createNode animCurveTU -n "Character1_RightHandThumb3_visibility";
	rename -uid "EE122676-4056-A17E-158C-899F7244C5B6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex1_translateX";
	rename -uid "609E3A5D-4509-9805-EF40-D28EBEDD3CC1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.25625163316726685;
createNode animCurveTL -n "Character1_RightHandIndex1_translateY";
	rename -uid "CB2DE3BF-4CC5-0676-8E6E-AF997B0C78B4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.892666816711426;
createNode animCurveTL -n "Character1_RightHandIndex1_translateZ";
	rename -uid "45F0DF67-4212-CC9F-1D99-6B87628665FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.533165454864502;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleX";
	rename -uid "87BF059D-496C-E790-BDF4-1FA253BB16EA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleY";
	rename -uid "E2AB90AF-4D83-A5EA-D142-1086F21A12F6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleZ";
	rename -uid "F6530E29-4844-BE1A-4E03-4584834A243F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateX";
	rename -uid "E0F44573-4CCB-5D48-42ED-258F7D8B996F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -1.158916711807251 1 -2.234675407409668
		 2 -1.886265754699707 3 -1.2556071281433105 4 -1.2556071281433105 5 -1.2556071281433105
		 6 -1.2556071281433105 7 -1.5854765176773071 8 -1.818357586860657 9 -0.83709800243377686
		 10 1.5118459463119507 11 3.8944492340087895 12 4.5560450553894043 13 3.9894351959228516
		 14 3.5727217197418213 15 3.5185549259185791 16 4.0652642250061035 17 5.0764145851135254
		 18 6.1945061683654785 19 7.0528521537780762 20 7.2860536575317383 21 6.659843921661377
		 22 5.4171233177185059 23 3.9186263084411626 24 2.5252673625946045 25 1.5768065452575684
		 26 1.214159369468689 27 1.2226831912994385 28 1.3941206932067871 29 1.5211820602416992
		 30 1.4959213733673096 31 1.418114185333252 32 1.3901752233505249 33 1.5144442319869995
		 34 1.8951863050460818 35 2.6857068538665771 36 3.8283605575561519 37 5.1034512519836426
		 38 6.2782196998596191 39 7.1167912483215332 40 7.560293674468995 41 7.7616310119628915
		 42 7.8057374954223633 43 7.7772173881530762 44 7.7605810165405273 45 7.7277469635009766
		 46 7.6221580505371094 47 7.4856300354003906 48 7.3600354194641113 49 7.2873005867004403
		 50 7.3093628883361816 51 7.6017990112304679 52 8.1405477523803711 53 8.6685113906860352
		 54 8.93109130859375 55 8.6750011444091797 56 7.5746979713439941 57 5.7956705093383789
		 58 3.838975191116333 59 2.2048563957214355 60 1.3497066497802734 61 1.4168804883956909
		 62 2.0973458290100098 63 3.142794132232666 64 4.2929549217224121 65 5.272031307220459
		 66 5.272031307220459 67 5.272031307220459 68 5.272031307220459 69 5.1680140495300293
		 70 4.8762092590332031 71 4.4293417930603027 72 3.8622303009033208 73 3.2105200290679932
		 74 2.5093288421630859 75 1.7918281555175781 76 1.0878973007202148 77 0.42302724719047546
		 78 -0.18236503005027771 79 -0.71311354637145996 80 -1.158916711807251;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateY";
	rename -uid "659021DC-4793-D169-BC75-F490EFE89444";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  0 1.6353100538253784 1 5.051795482635498
		 2 8.6163043975830078 3 10.129959106445312 4 10.129959106445312 5 10.129959106445312
		 6 10.129959106445312 7 8.4215517044067383 8 3.9717297554016118 9 -1.8329781293869019
		 10 -7.1216645240783691 11 -10.472763061523437 12 -11.570652961730957 13 -11.493466377258301
		 14 -11.424768447875977 15 -11.415088653564453 16 -11.504875183105469 17 -11.625272750854492
		 18 -11.690621376037598 19 -11.693016052246094 20 -11.68651294708252 21 -11.697057723999023
		 22 -11.652675628662109 23 -11.482510566711426 24 -11.206469535827637 25 -10.950654029846191
		 26 -10.837677001953125 27 -10.840431213378906 28 -10.894810676574707 29 -10.933878898620605
		 30 -10.92619514465332 31 -10.902268409729004 32 -10.893581390380859 33 -10.93183422088623
		 34 -11.042847633361816 35 -11.244207382202148 36 -11.468119621276855 37 -11.627688407897949
		 38 -11.69267749786377 39 -11.691537857055664 40 -11.674948692321777 41 -11.663758277893066
		 42 -11.661001205444336 45 -11.665802001953125 46 -11.671753883361816 47 -11.67851734161377
		 48 -11.683810234069824 49 -11.686470031738281 50 -11.685694694519043 51 -11.672828674316406
		 52 -11.636478424072266 53 -11.584857940673828 54 -11.553242683410645 55 -11.584124565124512
		 56 -11.674223899841309 57 -11.675411224365234 58 -11.469836235046387 59 -11.126357078552246
		 60 -10.88090705871582 61 -10.901885986328125 62 -11.098047256469727 63 -11.343143463134766
		 64 -11.537123680114746 65 -11.641811370849609 66 -11.641811370849609 67 -11.641811370849609
		 68 -11.641811370849609 69 -11.489745140075684 70 -11.055731773376465 71 -10.369047164916992
		 72 -9.4566707611083984 73 -8.3465890884399414 74 -7.0703158378601074 75 -5.6645388603210449
		 76 -4.1718378067016602 77 -2.6405501365661621 78 -1.1239489316940308 79 0.32104161381721497
		 80 1.6353100538253784;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateZ";
	rename -uid "DAA15CCC-431C-AFDB-BE5F-01A04A07B2C1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 12.797025680541992 1 35.741744995117188
		 2 58.790714263916023 3 69.348670959472656 4 69.348670959472656 5 69.348670959472656
		 6 69.348670959472656 7 62.133731842041009 8 44.223064422607422 9 21.000724792480469
		 10 -2.4598381519317627 11 -20.108894348144531 12 -25.879865646362305 13 -24.000923156738281
		 14 -22.613567352294922 15 -22.432840347290039 16 -24.252847671508789 17 -27.599409103393555
		 18 -31.281389236450192 19 -34.103275299072266 20 -34.870159149169922 21 -32.811260223388672
		 22 -28.72285079956055 23 -23.765537261962891 24 -19.099929809570312 25 -15.876604080200195
		 26 -14.631210327148439 27 -14.660573959350586 28 -15.250211715698242 29 -15.68608570098877
		 30 -15.599505424499513 31 -15.332592010498045 32 -15.236660957336424 33 -15.662994384765623
		 34 -16.963756561279297 35 -19.640926361083984 36 -23.465267181396484 37 -27.688621520996094
		 38 -31.556659698486325 39 -34.313514709472656 40 -35.772426605224609 41 -36.435249328613281
		 42 -36.58050537109375 43 -36.486579895019531 44 -36.431793212890625 45 -36.323673248291016
		 46 -35.976051330566406 47 -35.526721954345703 48 -35.113513946533203 49 -34.874263763427734
		 50 -34.946830749511719 51 -35.909038543701172 52 -37.683925628662109 53 -39.427375793457031
		 54 -40.29644775390625 55 -39.448837280273437 56 -35.819839477539063 57 -29.969381332397461
		 58 -23.500589370727539 59 -18.016042709350586 60 -15.097626686096191 61 -15.328357696533205
		 62 -17.651260375976563 63 -21.176393508911133 64 -25.008396148681641 65 -28.24462890625
		 66 -28.24462890625 67 -28.24462890625 68 -28.24462890625 69 -27.680084228515625 70 -26.087560653686523
		 71 -23.621572494506836 72 -20.438501358032227 73 -16.694404602050781 74 -12.54316234588623
		 75 -8.1350860595703125 76 -3.6161031723022461 77 0.87241524457931519 78 5.1931943893432617
		 79 9.2121210098266602 80 12.797025680541992;
createNode animCurveTU -n "Character1_RightHandIndex1_visibility";
	rename -uid "1C11C81C-490D-3333-F74A-A8B85D82F637";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex2_translateX";
	rename -uid "58F9122C-4D69-EB16-FECE-E0A51D4B3FBF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.3959341049194336;
createNode animCurveTL -n "Character1_RightHandIndex2_translateY";
	rename -uid "5B649400-43BE-8FAE-827A-51AAD417CCB6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8172028064727783;
createNode animCurveTL -n "Character1_RightHandIndex2_translateZ";
	rename -uid "59A4F603-4DBC-CC73-E422-FEB0F651639F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.52152138948440552;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleX";
	rename -uid "50300F40-43F3-492C-E607-F8BFDA489376";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleY";
	rename -uid "AA3BB135-4F74-59A6-450A-9A98CE0253A7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleZ";
	rename -uid "4FFAB5E9-410D-846F-0549-E8BDEBF397E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateX";
	rename -uid "5F34F47F-4913-69C3-1BFA-1895100893D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 11.788410186767578 1 21.432676315307617
		 2 23.176218032836914 3 15.230603218078615 4 15.230603218078615 5 15.230603218078615
		 6 15.230603218078615 7 16.917238235473633 8 20.350326538085938 9 23.572397232055664
		 10 25.832029342651367 11 27.059562683105469 12 27.414840698242188 13 27.242481231689453
		 14 27.061063766479492 15 27.033716201782227 16 27.270273208618164 17 27.507631301879883
		 18 27.531007766723633 19 27.413955688476562 20 27.36473274230957 21 27.480558395385742
		 22 27.538444519042969 23 27.215127944946289 24 26.35215950012207 25 25.297237396240234
		 26 24.755342483520508 27 24.769121170043945 28 25.03533935546875 29 25.219778060913086
		 30 25.183948516845703 31 25.07098388671875 32 25.029439926147461 33 25.210260391235352
		 34 25.704627990722656 35 26.487754821777344 36 27.178247451782227 37 27.510900497436523
		 38 27.524320602416992 39 27.401159286499023 40 27.298067092895508 41 27.243310928344727
		 42 27.230674743652344 43 27.238870620727539 44 27.243608474731445 45 27.252862930297852
		 46 27.281757354736328 47 27.317134857177734 48 27.347667694091797 49 27.364450454711914
		 50 27.359430313110352 51 27.287174224853516 52 27.127473831176758 53 26.939620971679688
		 54 26.835275650024414 55 26.937128067016602 56 27.294309616088867 57 27.547590255737305
		 58 27.182703018188477 59 26.047159194946289 60 24.968328475952148 61 25.069158554077148
		 62 25.933847427368164 63 26.817939758300781 64 27.344732284545898 65 27.528047561645508
		 66 27.528047561645508 67 27.528047561645508 68 27.528047561645508 69 27.545392990112305
		 70 27.528591156005859 71 27.334360122680664 72 26.832687377929687 73 25.949188232421875
		 74 24.667322158813477 75 23.013912200927734 76 21.044654846191406 77 18.835508346557617
		 78 16.479665756225586 79 14.087721824645996 80 11.788410186767578;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateY";
	rename -uid "C9C2FF53-44D0-03AB-3B39-69AA5EB43364";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -15.640760421752931 1 -38.327915191650391
		 2 -62.528541564941406 3 -73.199981689453125 4 -73.199981689453125 5 -73.199981689453125
		 6 -73.199981689453125 7 -72.25811767578125 8 -69.838577270507813 9 -66.597091674804687
		 10 -63.28895187377929 11 -61.26164245605468 12 -61.501377105712891 13 -62.944885253906243
		 14 -64.010009765625 15 -64.148696899414063 16 -62.751396179199219 17 -60.179828643798821
		 18 -57.350227355957031 19 -55.183116912841797 20 -54.594539642333984 21 -56.175098419189453
		 22 -59.316356658935547 23 -63.125648498535149 24 -66.702407836914063 25 -69.160675048828125
		 26 -70.105682373046875 27 -70.083442687988281 28 -69.636398315429688 29 -69.305450439453125
		 30 -69.371223449707031 31 -69.573875427246094 32 -69.646682739257813 33 -69.322990417480469
		 34 -68.333297729492188 35 -66.288520812988281 36 -63.356220245361328 37 -60.11126708984375
		 38 -57.138755798339844 39 -55.021747589111328 40 -53.902309417724609 41 -53.393955230712891
		 42 -53.282569885253906 43 -53.354591369628906 44 -53.3966064453125 45 -53.479518890380859
		 46 -53.746120452880859 47 -54.090789794921875 48 -54.407814025878906 49 -54.591400146484375
		 50 -54.535713195800781 51 -53.797519683837891 52 -52.436710357666016 53 -51.101211547851562
		 54 -50.435977935791016 55 -51.08477783203125 56 -53.865936279296875 57 -58.358352661132813
		 58 -63.329097747802741 59 -67.530662536621094 60 -69.752159118652344 61 -69.577095031738281
		 62 -67.80908203125 63 -65.112373352050781 64 -62.170993804931648 65 -59.683914184570305
		 66 -59.683914184570305 67 -59.683914184570305 68 -59.683914184570305 69 -59.075115203857422
		 70 -57.353084564208991 71 -54.675331115722656 72 -51.204540252685547 73 -47.110561370849609
		 74 -42.569202423095703 75 -37.759265899658203 76 -32.858661651611328 77 -28.040264129638672
		 78 -23.467493057250977 79 -19.290060043334961 80 -15.640760421752931;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateZ";
	rename -uid "42439476-49FA-F3A2-825E-2DA75E5D48A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 15.306336402893066 1 32.027423858642578
		 2 51.845500946044922 3 68.75714111328125 4 68.75714111328125 5 68.75714111328125
		 6 68.75714111328125 7 66.469398498535156 8 61.520801544189446 9 56.298168182373047
		 10 51.998615264892578 11 49.605224609375 12 49.610801696777344 13 50.921600341796875
		 14 51.939720153808594 15 52.075836181640625 16 50.741531372070313 17 48.470367431640625
		 18 46.182895660400391 19 44.543087005615234 20 44.111286163330078 21 45.283370971679688
		 22 47.751873016357422 23 51.091136932373047 24 54.754623413085938 25 57.727626800537116
		 26 59.007667541503906 27 58.976501464843757 28 58.360969543457024 29 57.91810226440429
		 30 58.005279541015625 31 58.276496887207024 32 58.374900817871094 33 57.941314697265625
		 34 56.674613952636719 35 54.295883178710938 36 51.309261322021484 37 48.412586212158203
		 38 46.019081115722656 39 44.424175262451172 40 43.609943389892578 41 43.245960235595703
		 42 43.166660308837891 43 43.217918395996094 44 43.247852325439453 45 43.306987762451172
		 46 43.497749328613281 47 43.745784759521484 48 43.975379943847656 49 44.108997344970703
		 50 44.068416595458984 51 43.534633636474609 52 42.569442749023438 53 41.643013000488281
		 54 41.188285827636719 55 41.631729125976563 56 43.583789825439453 57 46.976543426513672
		 58 51.283489227294922 59 55.705940246582031 60 58.518409729003899 61 58.280834197998047
		 62 56.036453247070313 63 53.046394348144531 64 50.209712982177734 65 48.055324554443359
		 66 48.055324554443359 67 48.055324554443359 68 48.055324554443359 69 47.48516845703125
		 70 45.931690216064453 71 43.660911560058594 72 40.915359497070313 73 37.874488830566406
		 74 34.65618896484375 75 31.336542129516602 76 27.971431732177734 77 24.613847732543945
		 78 21.326364517211914 79 18.189493179321289 80 15.306336402893066;
createNode animCurveTU -n "Character1_RightHandIndex2_visibility";
	rename -uid "F439BEF4-4961-AE89-A0B8-8589B317DE79";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex3_translateX";
	rename -uid "C8584266-4C79-8AA7-8DAB-72ACB7D3ECE1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.6282989978790283;
createNode animCurveTL -n "Character1_RightHandIndex3_translateY";
	rename -uid "A8CEB78D-4584-8FF9-4730-0CAD04EEE5C5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.2120883464813232;
createNode animCurveTL -n "Character1_RightHandIndex3_translateZ";
	rename -uid "00C113CD-4DA8-C709-7814-13BE449532AD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7757487297058105;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleX";
	rename -uid "E63C8E00-40A9-7C45-CBAC-DD82C09FE79C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleY";
	rename -uid "1BC6070D-4D93-8383-480D-24B64C708F92";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleZ";
	rename -uid "7339B53B-45BB-7646-C655-34B894982356";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateX";
	rename -uid "DC57A227-49F7-928B-3D1A-FFA756FE415E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -4.8049673750938382e-007 10 -6.4356811435573036e-007
		 11 0.084866195917129517 12 0.28361114859580994 13 0.51146537065505981 14 0.6810077428817749
		 15 0.7031700611114502 16 0.48079431056976318 17 0.077013708651065826 18 -0.35847622156143188
		 19 -0.68516832590103149 20 -0.77279764413833618 21 -0.53640049695968628 22 -0.056896738708019257
		 23 0.54015582799911499 24 1.1147620677947998 25 1.5172977447509766 26 1.6737804412841797
		 27 1.6700857877731323 28 1.5959447622299194 29 1.5412054061889648 30 1.5520738363265991
		 31 1.5855944156646729 32 1.5976473093032837 33 1.5441036224365234 34 1.3811048269271851
		 35 1.0476036071777344 36 0.57679986953735352 37 0.066348440945148468 38 -0.39062732458114624
		 39 -0.70924216508865356 40 -0.87523365020751953 41 -0.95001733303070079 42 -0.96635252237319957
		 43 -0.95579195022583019 44 -0.94962823390960693 45 -0.93745648860931396 46 -0.89825022220611572
		 47 -0.84741032123565674 48 -0.80049675703048706 49 -0.77326464653015137 50 -0.78152972459793091
		 51 -0.89067929983139038 52 -1.0897940397262573 53 -1.2824481725692749 54 -1.3773510456085205
		 55 -1.2848011255264282 56 -0.88059592247009277 57 -0.20443432033061981 58 0.57248657941818237
		 59 1.2496812343597412 60 1.6151207685470581 61 1.5861263275146484 62 1.2951931953430176
		 63 0.85770988464355469 64 0.38902953267097473 65 -8.2011518998115207e-007 66 -8.3506978398872889e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateY";
	rename -uid "BDB1CE59-4BD2-8FFE-556E-4AA48773BAA8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 52 ".ktv[0:51]"  9 -5.9647362604664522e-008 10 4.7069423914081199e-008
		 11 0.027287198230624199 12 0.086794279515743256 13 0.14759701490402222 14 0.18780998885631561
		 15 0.19275392591953278 16 0.13986706733703613 17 0.024809993803501129 18 -0.1280292272567749
		 19 -0.26344358921051025 20 -0.30294588208198547 21 -0.19949504733085632 22 -0.018932050094008446
		 23 0.1547008752822876 24 0.27166777849197388 25 0.32563859224319458 26 0.34054175019264221
		 32 0.33371281623840332 33 0.32843145728111267 34 0.30991047620773315 35 0.26044711470603943
		 36 0.16359639167785645 37 0.021429920569062233 38 -0.14054587483406067 39 -0.27415880560874939
		 40 -0.35088619589805603 41 -0.38710787892341614 42 -0.39515909552574158 44 -0.3869166374206543
		 45 -0.38095098733901978 46 -0.36192336678504944 47 -0.33767461776733398 48 -0.3157200813293457
		 49 -0.30316001176834106 50 -0.30695784091949463 51 -0.35828202962875366 52 -0.45764070749282831
		 53 -0.56109744310379028 54 -0.61481505632400513 55 -0.56240695714950562 56 -0.3534487783908844
		 57 -0.07044634222984314 58 0.1625596284866333 59 0.29228392243385315 60 0.33535066246986389
		 61 0.33260980248451233 62 0.29866132140159607 63 0.22524067759513855 64 0.11589864641427992
		 65 1.4521356206387281e-007 66 1.4130830550129758e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateZ";
	rename -uid "62A1920A-4F09-C0AB-0DF8-F09A30F5A5FD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -1.6625952525828325e-007 10 -1.8158441150717408e-007
		 11 0.69456499814987183 12 2.3111691474914551 13 4.1490564346313477 14 5.50714111328125
		 15 5.6841263771057129 16 3.9025478363037114 17 0.63041055202484131 18 -2.9656224250793457
		 19 -5.7200345993041992 20 -6.4684863090515137 21 -4.4590129852294922 22 -0.46716761589050299
		 23 4.3794107437133789 24 8.9513692855834961 25 12.118082046508789 26 13.343677520751953
		 27 13.314766883850098 28 12.734363555908203 29 12.305493354797363 30 12.390669822692871
		 31 12.653295516967773 32 12.747697830200195 33 12.328207969665527 34 11.049187660217285
		 35 8.4205722808837891 36 4.6732993125915527 37 0.5432395339012146 38 -3.2343504428863525
		 39 -5.9252204895019531 40 -7.3490610122680673 41 -7.9959621429443359 42 -8.1377353668212891
		 43 -8.0460605621337891 44 -7.9925870895385742 45 -7.8870625495910653 46 -7.54779052734375
		 47 -7.1092619895935059 48 -6.7059841156005859 49 -6.4724874496459961 50 -6.543309211730957
		 51 -7.4823861122131357 52 -9.2147312164306641 53 -10.916743278503418 54 -11.765365600585937
		 55 -10.937699317932129 56 -7.3953309059143066 57 -1.6846030950546265 58 4.6387252807617187
		 59 10.015399932861328 60 12.884531021118164 61 12.657462120056152 62 10.37368106842041
		 63 6.9150509834289551 64 3.1634097099304199 65 -2.0094718422569713e-007 66 -2.0437390446659265e-007;
createNode animCurveTU -n "Character1_RightHandIndex3_visibility";
	rename -uid "99F26D0D-474B-AA79-7B4F-9BB51E8F73C8";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing1_translateX";
	rename -uid "102185E0-4DE5-B2A8-413B-24B6F4BDCC16";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8543868064880371;
createNode animCurveTL -n "Character1_RightHandRing1_translateY";
	rename -uid "4844F015-497A-695C-5427-838C254E7B0F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2156095504760742;
createNode animCurveTL -n "Character1_RightHandRing1_translateZ";
	rename -uid "A1FEC0FD-4228-AC77-728F-4BB36724685E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2236912250518799;
createNode animCurveTU -n "Character1_RightHandRing1_scaleX";
	rename -uid "7E2B3986-40DA-04B5-6917-9FB38DDBC330";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTU -n "Character1_RightHandRing1_scaleY";
	rename -uid "584620D1-473C-1120-9529-35B3C11B633A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing1_scaleZ";
	rename -uid "A2665B7C-4C20-CD7A-4A5F-D5ADAF82B2CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000002384185791;
createNode animCurveTA -n "Character1_RightHandRing1_rotateX";
	rename -uid "4C81FD8E-43CF-125D-81B0-378673AAFFD8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -9.2360963821411133 1 -9.9889259338378906
		 2 -9.8513698577880859 3 -9.4753284454345703 4 -9.4753284454345703 5 -9.4753284454345703
		 6 -9.4753284454345703 7 -9.3748989105224609 8 -8.6630334854125977 9 -6.7871832847595215
		 10 -3.8952236175537109 11 -1.3112447261810303 12 -0.72945654392242432 13 -1.4825147390365601
		 14 -2.0336506366729736 15 -2.105121374130249 16 -1.3819777965545654 17 -0.034154560416936874
		 18 1.4716644287109375 19 2.6385457515716553 20 2.9571948051452637 21 2.103102445602417
		 22 0.42299559712409973 23 -1.5763272047042847 24 -3.4087574481964111 25 -4.6410646438598633
		 26 -5.1089582443237305 27 -5.0979824066162109 28 -4.8769984245300293 29 -4.71295166015625
		 30 -4.7455835342407227 31 -4.8460383415222168 32 -4.8820891380310059 33 -4.721656322479248
		 34 -4.2287812232971191 35 -3.1990885734558105 36 -1.6958212852478027 37 0.0020669074729084969
		 38 1.5850549936294556 39 2.7258436679840088 40 3.3328020572662354 41 3.6091680526733398
		 42 3.6697797775268555 43 3.630584716796875 44 3.6077251434326172 45 3.5626208782196045
		 46 3.4176654815673828 47 3.230445384979248 48 3.0584275722503662 49 2.9589002132415771
		 50 2.9890820980072021 51 3.389732837677002 52 4.1306843757629395 53 4.8603711128234863
		 54 5.2245926856994629 55 4.8693628311157227 56 3.352557897567749 57 0.93267446756362915
		 58 -1.6817746162414551 59 -3.8264360427856441 60 -4.9342870712280273 61 -4.8476295471191406
		 62 -3.9662702083587646 63 -2.5998439788818359 64 -1.0796430110931396 65 0.22813262045383451
		 66 0.22813262045383451 67 0.22813262045383451 68 0.22813262045383451 69 0.080745302140712738
		 70 -0.33269372582435608 71 -0.96599793434143066 72 -1.770743727684021 73 -2.6984398365020752
		 74 -3.7024300098419189 75 -4.7394475936889648 76 -5.7707095146179199 77 -6.7624688148498535
		 78 -7.6860389709472656 79 -8.5173435211181641 80 -9.2360963821411133;
createNode animCurveTA -n "Character1_RightHandRing1_rotateY";
	rename -uid "FEB07333-44CD-1EF8-28B5-35872D13F1BD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 80 ".ktv[0:79]"  0 4.393496036529541 1 8.7385978698730469
		 2 13.312496185302734 3 15.397130966186525 4 15.397130966186525 5 15.397130966186525
		 6 15.397130966186525 7 13.029498100280762 8 7.0936017036437988 9 -0.42896488308906555
		 10 -7.3885221481323242 11 -12.052179336547852 12 -13.611075401306152 13 -13.396482467651367
		 14 -13.222704887390137 15 -13.199113845825195 16 -13.426644325256348 17 -13.786376953125
		 18 -14.093300819396973 19 -14.264758110046387 20 -14.301719665527344 21 -14.193161964416504
		 22 -13.889952659606934 23 -13.367912292480469 24 -12.724987983703613 25 -12.19681453704834
		 26 -11.974689483642578 27 -11.98004150390625 28 -12.08634090423584 29 -12.163480758666992
		 30 -12.148255348205566 31 -12.101014137268066 32 -12.083922386169434 33 -12.159425735473633
		 34 -12.382526397705078 35 -12.806960105895996 36 -13.330924034118652 37 -13.794919013977051
		 38 -14.112472534179688 39 -14.275300979614258 40 -14.339912414550781 41 -14.364316940307617
		 42 -14.369251251220703 44 -14.364197731018066 45 -14.360424995422363 46 -14.347739219665526
		 47 -14.330079078674315 48 -14.312583923339844 49 -14.301905632019043 50 -14.305187225341797
		 51 -14.345195770263672 52 -14.401869773864748 53 -14.435851097106932 54 -14.444740295410154
		 55 -14.436135292053224 56 -14.341760635375975 57 -13.994688987731934 58 -13.335307121276855
		 59 -12.554958343505859 60 -12.059046745300293 61 -12.100260734558105 62 -12.496002197265625
		 63 -13.029059410095215 64 -13.514525413513184 65 -13.846923828125 66 -13.846923828125
		 67 -13.846923828125 68 -13.846923828125 69 -13.63102912902832 70 -13.016072273254395
		 71 -12.046679496765137 72 -10.765088081359863 73 -9.2149286270141602 74 -7.4440240859985352
		 75 -5.506080150604248 76 -3.461273193359375 77 -1.375896692276001 78 0.67868399620056152
		 79 2.627490758895874 80 4.393496036529541;
createNode animCurveTA -n "Character1_RightHandRing1_rotateZ";
	rename -uid "0CF84ED2-48B9-1D66-418C-CEA20F31398F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 29.838140487670898 1 46.047027587890625
		 2 62.304595947265625 3 69.758003234863281 4 69.758003234863281 5 69.758003234863281
		 6 69.758003234863281 7 64.123451232910156 8 50.092277526855469 9 31.731424331665043
		 10 12.911527633666992 11 -1.3454276323318481 12 -5.8138575553894043 13 -3.9499490261077876
		 14 -2.5758452415466309 15 -2.3969831466674805 16 -4.1996645927429199 17 -7.5224618911743164
		 18 -11.189440727233887 19 -14.006857872009277 20 -14.77347469329834 21 -12.716187477111816
		 22 -8.6401462554931641 23 -3.7166795730590816 24 0.89565062522888172 25 4.0690054893493652
		 26 5.2920956611633301 27 5.2632794380187988 28 4.6843914985656738 29 4.2562227249145508
		 30 4.3412885665893555 31 4.6034836769104004 32 4.6976990699768066 33 4.2789082527160645
		 34 2.9999463558197021 35 0.36196336150169373 36 -3.4191882610321045 37 -7.6111769676208496
		 38 -11.464018821716309 39 -14.216983795166016 40 -15.675903320312502 41 -16.339160919189453
		 42 -16.484548568725586 43 -16.390535354614258 44 -16.335699081420898 45 -16.22749137878418
		 46 -15.879631996154785 47 -15.430104255676268 48 -15.016818046569822 49 -14.777574539184569
		 50 -14.85013484954834 51 -15.812580108642576 52 -17.589344024658203 53 -19.336271286010742
		 54 -20.207624435424805 55 -19.3577880859375 56 -15.723336219787596 57 -9.8815202713012695
		 58 -3.4541792869567871 59 1.9639679193496702 60 4.8342299461364746 61 4.6076421737670898
		 62 2.3232314586639404 63 -1.1543965339660645 64 -4.948951244354248 65 -8.1642436981201172
		 66 -8.1642436981201172 67 -8.1642436981201172 68 -8.1642436981201172 69 -7.6257066726684579
		 70 -6.1091132164001465 71 -3.7681345939636226 72 -0.75961673259735107 73 2.7604620456695557
		 74 6.6402244567871094 75 10.734164237976074 76 14.904341697692871 77 19.020532608032227
		 78 22.959451675415039 79 26.60333251953125 80 29.838140487670898;
createNode animCurveTU -n "Character1_RightHandRing1_visibility";
	rename -uid "19AFF43C-4809-80CF-6FCD-A69455DA60E7";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing2_translateX";
	rename -uid "6EE1B0AE-4DD4-5C2A-57FF-B4866CC37F5B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.062777042388916;
createNode animCurveTL -n "Character1_RightHandRing2_translateY";
	rename -uid "E449C531-4E0E-9E71-B654-FCAA76654295";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.4930424690246582;
createNode animCurveTL -n "Character1_RightHandRing2_translateZ";
	rename -uid "C3321F0C-46D6-5236-A97B-0A92C60CAA4A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.86127841472625732;
createNode animCurveTU -n "Character1_RightHandRing2_scaleX";
	rename -uid "8B8C8D80-4C89-A04C-D563-80B25B9A89B8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandRing2_scaleY";
	rename -uid "506A1F3C-4582-1986-E35F-34B992C996D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing2_scaleZ";
	rename -uid "F7AF0DAA-4739-201F-0DAA-FD9832B6DB5C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandRing2_rotateX";
	rename -uid "6EE9A6B5-41D4-2C1A-952E-96BB98D59223";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 20.286947250366211 1 23.778127670288086
		 2 21.561958312988281 3 14.933642387390137 4 14.933642387390137 5 14.933642387390137
		 6 14.933642387390137 7 17.838289260864258 8 22.821620941162109 9 26.489231109619141
		 10 28.477388381958008 11 29.463857650756832 12 30.050815582275391 13 30.427312850952148
		 14 30.691915512084961 15 30.725507736206055 16 30.378019332885742 17 29.689020156860348
		 18 28.862833023071289 19 28.185222625732422 20 27.994726181030273 21 28.500082015991211
		 22 29.444181442260739 23 30.473028182983398 24 31.306205749511719 25 31.789594650268558
		 26 31.952695846557614 27 31.949016571044922 28 31.873403549194339 29 31.815456390380856
		 30 31.827104568481445 31 31.862583160400391 32 31.875177383422855 33 31.818569183349609
		 34 31.635993957519528 35 31.217187881469727 36 30.530864715576168 37 29.669816970825192
		 38 28.798381805419922 39 28.133264541625977 40 27.767210006713867 41 27.597759246826172
		 42 27.56036376953125 43 27.584556579589844 44 27.598648071289063 45 27.626420974731445
		 46 27.715360641479492 47 27.82952880859375 48 27.933723449707031 49 27.993701934814453
		 50 27.975536346435547 51 27.732444763183594 52 27.273271560668945 53 26.808839797973633
		 54 26.572429656982422 55 26.80303955078125 56 27.755155563354492 57 29.164987564086911
		 58 30.524089813232422 59 31.477994918823242 60 31.893280029296879 61 31.863142013549801
		 62 31.533771514892578 63 30.953256607055661 64 30.227956771850586 65 29.549201965332031
		 66 29.549201965332031 67 29.549201965332031 68 29.549201965332031 69 29.448999404907223
		 70 29.161794662475586 71 28.704006195068359 72 28.09010124206543 73 27.335668563842773
		 74 26.459556579589844 75 25.485103607177734 76 24.440608978271484 77 23.359241485595703
		 78 22.278553009033203 79 21.239774703979492 80 20.286947250366211;
createNode animCurveTA -n "Character1_RightHandRing2_rotateY";
	rename -uid "D6CC68D9-491D-19D6-FB0B-9D97A325D112";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -34.944370269775391 1 -50.679004669189453
		 2 -66.576683044433594 3 -73.483367919921875 4 -73.483367919921875 5 -73.483367919921875
		 6 -73.483367919921875 7 -71.601051330566406 8 -66.7269287109375 9 -60.200706481933587
		 10 -53.585494995117188 11 -49.03521728515625 12 -48.291145324707031 13 -49.767982482910156
		 14 -50.861320495605469 15 -51.003925323486328 16 -49.569713592529297 17 -46.943531036376953
		 18 -44.070640563964844 19 -41.880584716796875 20 -41.28717041015625 21 -42.882045745849609
		 22 -46.065120697021484 23 -49.953311920166016 24 -53.641208648681641 25 -56.204998016357422
		 26 -57.198982238769531 27 -57.175525665283203 28 -56.704704284667969 29 -56.35693359375
		 30 -56.425994873046875 31 -56.638957977294922 32 -56.715518951416016 33 -56.375347137451172
		 34 -55.338851928710938 35 -53.212173461914062 36 -50.189830780029297 37 -46.87371826171875
		 38 -43.856559753417969 39 -41.717830657958984 40 -40.589977264404297 41 -40.078472137451172
		 42 -39.966445922851562 43 -40.03887939453125 44 -40.081134796142578 45 -40.164531707763672
		 46 -40.432773590087891 47 -40.779731750488281 48 -41.099029541015625 49 -41.284000396728516
		 50 -41.227890014648438 51 -40.484504699707031 52 -39.116397857666016 53 -37.776508331298828
		 54 -37.110088348388672 55 -37.760040283203125 56 -40.553371429443359 57 -45.092361450195313
		 58 -50.162002563476563 59 -54.501880645751953 60 -56.826499938964844 61 -56.642337799072266
		 62 -54.791866302490234 63 -51.996486663818359 64 -48.975555419921875 65 -46.4388427734375
		 66 -46.4388427734375 67 -46.4388427734375 68 -46.4388427734375 69 -46.285495758056641
		 70 -45.852062225341797 71 -45.178482055664063 72 -44.304767608642578 73 -43.271076202392578
		 74 -42.117748260498047 75 -40.885231018066406 76 -39.613967895507812 77 -38.344291687011719
		 78 -37.116310119628906 79 -35.969844818115234 80 -34.944370269775391;
createNode animCurveTA -n "Character1_RightHandRing2_rotateZ";
	rename -uid "2D92758D-4DBC-63B9-1CD4-EA849CE871A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 29.386693954467773 1 40.674541473388672
		 2 56.268497467041016 3 68.698143005371094 4 68.698143005371094 5 68.698143005371094
		 6 68.698143005371094 7 64.771194458007812 8 57.199329376220703 9 50.074554443359375
		 10 44.489757537841797 11 41.066425323486328 12 40.343471527099609 13 41.14996337890625
		 14 41.752243041992188 15 41.8311767578125 16 41.041255950927734 17 39.613079071044922
		 18 38.0672607421875 19 36.893325805664062 20 36.575206756591797 21 37.430027008056641
		 22 39.139141082763672 23 41.251708984375 24 43.310295104980469 25 44.793483734130859
		 26 45.384166717529297 27 45.370113372802734 28 45.089202880859375 29 44.883140563964844
		 30 44.923965454101563 31 45.050151824951172 32 45.095626831054688 33 44.894023895263672
		 34 44.286403656005859 35 43.066879272460938 36 41.381755828857422 37 39.575359344482422
		 38 37.952434539794922 39 36.80609130859375 40 36.201213836669922 41 35.926589965820312
		 42 35.866413116455078 43 35.905323028564453 44 35.928020477294922 45 35.972812652587891
		 46 36.116836547851563 47 36.303035736083984 48 36.474311828613281 49 36.573505401611328
		 50 36.543418884277344 51 36.144603729248047 52 35.409309387207031 53 34.686695098876953
		 54 34.326087951660156 55 34.677791595458984 56 36.181568145751953 57 38.615776062011719
		 58 41.366443634033203 59 43.802433013916016 60 45.161651611328125 61 45.052158355712891
		 62 43.969482421875 63 42.383312225341797 64 40.716327667236328 65 39.340602874755859
		 66 39.340602874755859 67 39.340602874755859 68 39.340602874755859 69 39.194179534912109
		 70 38.783126831054687 71 38.152183532714844 72 37.347129821777344 73 36.412548065185547
		 74 35.390415191650391 75 34.319454193115234 76 33.235099792480469 77 32.169937133789063
		 78 31.154336929321289 79 30.217214584350589 80 29.386693954467773;
createNode animCurveTU -n "Character1_RightHandRing2_visibility";
	rename -uid "0B39FAEA-4CAE-B35E-62B1-57A1D48DA23C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing3_translateX";
	rename -uid "015FEBBF-4C76-87E7-60DD-769D706CDFE1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.5035178661346436;
createNode animCurveTL -n "Character1_RightHandRing3_translateY";
	rename -uid "A76D5502-4E6C-11C4-95CB-6A949414A0A0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.9153993129730225;
createNode animCurveTL -n "Character1_RightHandRing3_translateZ";
	rename -uid "0034ACB8-4BB0-7ACC-C575-06935B73C905";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.6898331642150879;
createNode animCurveTU -n "Character1_RightHandRing3_scaleX";
	rename -uid "059887E8-4B82-66D9-775C-C8BC8F7F6C75";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing3_scaleY";
	rename -uid "2BA2B9BE-4B3F-C9DC-6B85-C5B7438ABE28";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandRing3_scaleZ";
	rename -uid "683B6241-4745-8C79-EDC6-7689893B1F3C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightHandRing3_rotateX";
	rename -uid "2A478069-4A38-DBC6-CF02-B58774E494ED";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -1.7155116438516416e-007 10 -1.8606067442306085e-007
		 11 0.087301462888717651 12 0.29159316420555115 13 0.52554500102996826 14 0.69944733381271362
		 15 0.72216862440109253 16 0.49406915903091425 17 0.079225338995456696 18 -0.36920994520187378
		 19 -0.70634537935256958 20 -0.79688793420791626 21 -0.55274128913879395 22 -0.058550965040922165
		 23 0.55498379468917847 24 1.1436861753463745 25 1.555112361907959 26 1.7148374319076538
		 27 1.7110675573348999 28 1.6354038715362549 29 1.5795230865478516 30 1.5906193256378174
		 31 1.6248388290405273 32 1.6371415853500366 33 1.5824820995330811 34 1.4160006046295166
		 35 1.0749664306640625 36 0.59257745742797852 37 0.068255878984928131 38 -0.40236020088195801
		 39 -0.73121464252471924 40 -0.90279173851013184 41 -0.9801502823829652 42 -0.99705266952514637
		 43 -0.98612523078918457 44 -0.97974765300750744 45 -0.96715438365936279 46 -0.92659687995910656
		 47 -0.87401974201202393 48 -0.82551813125610352 49 -0.79737061262130737 50 -0.80591303110122681
		 51 -0.91876626014709473 52 -1.1248394250869751 53 -1.3244847059249878 54 -1.4229285717010498
		 55 -1.3269246816635132 56 -0.90833741426467896 57 -0.21046480536460876 58 0.58815276622772217
		 59 1.2816741466522217 60 1.6549762487411499 61 1.6253818273544312 62 1.3282010555267334
		 63 0.8805357813835144 64 0.39986699819564819 65 -2.0760441543643537e-007 66 -2.1727079513311764e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateY";
	rename -uid "8F089D2B-42D2-0B3B-D8DE-35B11481CDDB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 52 ".ktv[0:51]"  9 -3.2082027701108018e-008 10 9.7133600718279922e-008
		 11 0.02477673813700676 12 0.078329436480998993 13 0.13217891752719879 14 0.16713230311870575
		 15 0.17138338088989258 16 0.12539272010326385 17 0.022532666102051735 18 -0.11764319241046906
		 19 -0.24391335248947146 20 -0.28101736307144165 21 -0.18408970534801483 22 -0.017259761691093445
		 23 0.13839787244796753 24 0.23720976710319516 25 0.27798616886138916 26 0.28765681385993958
		 32 0.28338083624839783 33 0.27988678216934204 34 0.26676526665687561 35 0.22815275192260739
		 36 0.14616009593009949 37 0.019468970596790314 38 -0.12924599647521973 39 -0.25396731495857239
		 40 -0.32618445158004761 41 -0.36040249466896057 42 -0.36801841855049133 44 -0.36022168397903442
		 45 -0.35458093881607056 46 -0.33660310506820679 47 -0.31372290849685669 48 -0.29303851723670959
		 49 -0.28121885657310486 50 -0.2847917377948761 51 -0.33316504955291748 52 -0.4272390604019165
		 53 -0.52570825815200806 54 -0.57701635360717773 55 -0.52695769071578979 56 -0.32860279083251953
		 57 -0.064479343593120575 58 0.14525695145130157 59 0.2534465491771698 60 0.28443402051925659
		 61 0.2826630175113678 62 0.25833678245544434 63 0.19900491833686829 64 0.10423348098993301
		 65 2.3345000954577702e-007 66 2.3092414380698756e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateZ";
	rename -uid "A444EF0B-413A-EA83-0C07-948F0B0E55DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -1.8913142696419527e-007 10 -1.9090356317974511e-007
		 11 0.69435596466064453 12 2.310462474822998 13 4.1477642059326172 14 5.5054006576538086
		 15 5.6823263168334961 16 3.9013352394104004 17 0.63022047281265259 18 -2.9647579193115234
		 19 -5.7183990478515625 20 -6.4666447639465332 21 -4.4577264785766602 22 -0.46702894568443298
		 23 4.3780431747436523 24 8.9484272003173828 25 12.113946914672852 26 13.339055061340332
		 27 13.310155868530273 28 12.729985237121582 29 12.301284790039063 30 12.386427879333496
		 31 12.648948669433594 32 12.743313789367676 33 12.323990821838379 34 11.045466423034668
		 35 8.4178228378295898 36 4.6718358993530273 37 0.5430762767791748 38 -3.2334089279174805
		 39 -5.923527717590332 40 -7.3469786643981925 41 -7.9937043190002441 42 -8.1354379653930664
		 43 -8.0437889099121094 44 -7.9903302192687997 45 -7.8848342895507821 46 -7.545654296875
		 47 -7.1072449684143066 48 -6.7040777206420898 49 -6.4706449508666992 50 -6.5414471626281738
		 51 -7.4802680015563965 52 -9.2121438980102539 53 -10.913699150085449 54 -11.762094497680664
		 55 -10.934650421142578 56 -7.3932361602783203 57 -1.6841065883636475 58 4.6372733116149902
		 59 10.012068748474121 60 12.880093574523926 61 12.653114318847656 62 10.37021541595459
		 63 6.9128317832946777 64 3.1624345779418945 65 -1.9436347997725534e-007 66 -1.9030979103717982e-007;
createNode animCurveTU -n "Character1_RightHandRing3_visibility";
	rename -uid "5AF6E4B2-47E1-4654-BF98-DC9FD7C1B9ED";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Neck_translateX";
	rename -uid "616E11DA-4C84-D2CD-1965-07A32821A15B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 12.791294097900391;
createNode animCurveTL -n "Character1_Neck_translateY";
	rename -uid "3E7F2CA6-41B6-7A8D-6177-4AB6E07F5F72";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.3580474853515625;
createNode animCurveTL -n "Character1_Neck_translateZ";
	rename -uid "2E1FACB3-413A-11A9-56DF-889AE098AD3F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.6322288513183594;
createNode animCurveTU -n "Character1_Neck_scaleX";
	rename -uid "AE69776B-4EC6-18BE-2110-93A6034A9A75";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_Neck_scaleY";
	rename -uid "7CF8E4F4-45E9-DA8E-A917-FCB224008C25";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Neck_scaleZ";
	rename -uid "0E5C49A9-4601-F464-D6CD-D9AA1FF91526";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Neck_rotateX";
	rename -uid "D49BBB1C-4F32-9132-1063-4D9E303B2E8A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 9.5831975936889648 1 5.2100844383239746
		 2 0.74725377559661865 3 -1.9971016645431519 4 -2.6006138324737549 5 -2.2759413719177246
		 6 -1.5095113515853882 7 -0.73411524295806885 8 -0.31797707080841064 9 -0.19442921876907349
		 10 -0.077861376106739044 11 0.017288444563746452 12 0.078169494867324829 13 0.09275740385055542
		 14 0.044462595134973526 15 -0.086066246032714844 16 -0.31989040970802307 17 -0.65277200937271118
		 18 -1.0634514093399048 19 -1.5283582210540771 20 -2.0221626758575439 21 -2.5133283138275146
		 22 -2.9504678249359131 23 -3.2795581817626953 24 -3.4500205516815186 25 -3.456334114074707
		 26 -3.3242383003234863 27 -3.0939021110534668 28 -2.823585033416748 29 -2.5570790767669678
		 30 -2.3110687732696533 31 -2.0782546997070312 32 -1.8593258857727051 33 -1.6587470769882202
		 34 -1.4834791421890259 35 -1.3414822816848755 36 -1.2327927350997925 37 -1.1507754325866699
		 38 -1.1059688329696655 39 -1.1088665723800659 40 -1.1642482280731201 41 -1.2545543909072876
		 42 -1.3568555116653442 43 -1.4491103887557983 44 -1.5102293491363525 45 -1.5317195653915405
		 46 -1.5205179452896118 47 -1.4791162014007568 48 -1.409278392791748 49 -1.3112144470214844
		 50 -1.1754609346389771 51 -0.99889594316482533 52 -0.77995550632476807 53 -0.51552164554595947
		 54 -0.20684333145618439 55 0.13935548067092896 56 0.52090239524841309 57 0.9235566258430481
		 58 1.3157602548599243 59 1.6632474660873413 60 1.9296422004699705 61 2.0916178226470947
		 62 2.1668047904968262 63 2.1811680793762207 64 2.162628173828125 65 2.1405491828918457
		 66 2.199742317199707 67 2.2699680328369141 68 2.3627498149871826 69 2.417426586151123
		 70 2.4039726257324219 71 2.383450984954834 72 2.4273662567138672 73 2.6183633804321289
		 74 3.0442800521850586 75 3.7627208232879643 76 4.7203831672668457 77 5.8352046012878418
		 78 7.028322696685791 79 8.2264680862426758 80 9.3615398406982422;
createNode animCurveTA -n "Character1_Neck_rotateY";
	rename -uid "D0488CA3-4CE8-6DB9-5896-1894B7886670";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -8.7274017333984375 1 -3.3311190605163574
		 2 2.1147778034210205 3 5.0865626335144043 4 5.0572500228881836 5 3.6323044300079341
		 6 1.5242284536361694 7 -0.52950757741928101 8 -1.7725073099136353 9 -2.15970778465271
		 10 -2.228538990020752 11 -2.1907932758331299 12 -2.2004754543304443 13 -2.2825422286987305
		 14 -2.4602043628692627 15 -2.7558753490447998 16 -3.13582444190979 17 -3.5640952587127686
		 18 -4.0617694854736328 19 -4.6516928672790527 20 -5.3580923080444336 21 -6.1794853210449219
		 22 -7.0074725151062012 23 -7.7050671577453622 24 -8.1335792541503906 25 -8.2181529998779297
		 26 -7.9241137504577637 27 -7.3401184082031241 28 -6.6555337905883789 29 -6.064460277557373
		 30 -5.6654796600341797 31 -5.3926668167114258 32 -5.1951637268066406 33 -5.018674373626709
		 34 -4.8057365417480469 35 -4.5307626724243164 36 -4.2409005165100098 37 -3.9771182537078853
		 38 -3.8045570850372314 39 -3.7888302803039551 40 -3.9693996906280518 41 -4.2810864448547363
		 42 -4.6327767372131348 43 -4.9333124160766602 44 -5.0914649963378906 45 -5.0633149147033691
		 46 -4.8899760246276855 47 -4.6073541641235352 48 -4.2510967254638672 49 -3.8569319248199463
		 50 -3.4369566440582275 51 -3.0424094200134277 52 -2.6732592582702637 53 -2.2727301120758057
		 54 -1.7818992137908936 55 -1.138608455657959 56 -0.31562924385070801 57 0.6018100380897522
		 58 1.5106174945831299 59 2.3084518909454346 60 2.8934938907623291 61 3.2555813789367676
		 62 3.4765243530273438 63 3.5917441844940186 64 3.635945081710815 65 3.6436021327972408
		 66 3.7567973136901855 67 3.8115425109863281 68 3.808660745620728 69 3.8003518581390381
		 70 3.7768547534942631 71 3.6470730304718022 72 3.3228986263275146 73 2.7200334072113037
		 74 1.7564247846603394 75 0.33494666218757629 76 -1.4922471046447754 77 -3.5848119258880615
		 78 -5.8023777008056641 79 -8.0044326782226563 80 -10.051227569580078;
createNode animCurveTA -n "Character1_Neck_rotateZ";
	rename -uid "FC5A835C-447C-119E-098D-59927BA04CD7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 0.27945694327354431 1 -4.5183401107788086
		 2 -9.5963630676269531 3 -12.215411186218262 4 -11.523058891296387 5 -9.273310661315918
		 6 -6.3559961318969727 7 -3.6267926692962646 8 -1.8950648307800293 9 -1.1227781772613525
		 10 -0.68421220779418945 11 -0.32182770967483521 12 0.13625477254390717 13 0.65061664581298828
		 14 1.1833181381225586 15 1.6971468925476074 16 2.220120906829834 17 2.7676424980163574
		 18 3.2836818695068359 19 3.7140393257141113 20 4.0067524909973145 21 4.1259517669677734
		 22 4.0635166168212891 23 3.8073697090148926 24 3.343045711517334 25 2.6621739864349365
		 26 1.6288614273071289 27 0.29816123843193054 28 -1.0180032253265381 29 -2.0122518539428711
		 30 -2.5708780288696289 31 -2.8495132923126221 32 -2.9470810890197754 33 -2.9605872631072998
		 34 -2.9854466915130615 35 -2.9686107635498047 36 -2.8551061153411865 37 -2.7309985160827637
		 38 -2.6309592723846436 39 -2.5892496109008789 40 -2.6352934837341309 41 -2.7566325664520264
		 42 -2.9245591163635254 43 -3.1103446483612061 44 -3.2851226329803467 45 -3.5094287395477295
		 46 -3.8201503753662105 47 -4.1526799201965332 48 -4.4423165321350098 49 -4.6244993209838867
		 50 -4.6067228317260742 51 -4.2672290802001953 52 -3.6461062431335449 53 -2.8790459632873535
		 54 -2.0998790264129639 55 -1.4399788379669189 56 -0.8821374773979187 57 -0.34543865919113159
		 58 0.13477997481822968 59 0.52121472358703613 60 0.77480024099349976 61 0.90431714057922374
		 62 0.95583856105804443 63 0.94609522819519054 64 0.8927122950553894 65 0.81384783983230591
		 66 0.90395504236221313 67 0.85724937915802002 68 0.63592511415481567 69 -0.028354588896036148
		 70 -1.1855373382568359 71 -2.5461645126342773 72 -3.8207731246948238 73 -4.7207365036010742
		 74 -4.9599571228027344 75 -4.4401812553405762 76 -3.3886868953704834 77 -2.006260871887207
		 78 -0.48536109924316406 79 0.99063724279403687 80 2.2464125156402588;
createNode animCurveTU -n "Character1_Neck_visibility";
	rename -uid "AB4AAD32-49C1-5730-9E38-9BBE07A4CE71";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Head_translateX";
	rename -uid "EF94DE5E-442B-8912-3F77-7C8ED4D9739D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.627285957336426;
createNode animCurveTL -n "Character1_Head_translateY";
	rename -uid "E8B414B0-4BEB-FF07-DB4F-C8A2BAAB4BA8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.64882755279541;
createNode animCurveTL -n "Character1_Head_translateZ";
	rename -uid "D748B24A-45EA-81FD-6272-2C8CF960CE9D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.1324095726013184;
createNode animCurveTU -n "Character1_Head_scaleX";
	rename -uid "F596E918-40F9-7385-C2E5-5098FF597E56";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Head_scaleY";
	rename -uid "FE728992-4C57-BD25-36F1-DEA0CECE8287";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Head_scaleZ";
	rename -uid "68EA1F49-49E3-1978-CC3E-91A14DCDFA02";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_Head_rotateX";
	rename -uid "A00559CA-43AB-BCD2-467A-53B04D21DF7F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 5.3095211982727051 1 5.5076608657836914
		 2 5.7022552490234375 3 5.7589664459228516 4 5.6491951942443848 5 5.4622025489807129
		 6 5.2400808334350586 7 5.0249404907226562 8 4.8589115142822266 9 4.693934440612793
		 10 4.4824647903442383 11 4.2700390815734863 12 4.1006245613098145 13 4.0142078399658203
		 14 4.0505771636962891 15 4.2501430511474609 16 4.5246944427490234 17 4.7986373901367187
		 18 5.1329507827758789 19 5.5890913009643555 20 6.2287020683288574 21 7.075531005859375
		 22 8.0024032592773437 23 8.8444385528564453 24 9.4362955093383789 25 9.6798181533813477
		 26 9.6899137496948242 27 9.6052207946777344 28 9.4368476867675781 29 9.2024221420288086
		 30 8.9270744323730469 31 8.6383438110351562 32 8.3603124618530273 33 8.1150274276733398
		 34 7.9229354858398446 35 7.6806797981262207 36 7.3408346176147461 37 6.9957127571105957
		 38 6.7469396591186523 39 6.6962370872497559 40 6.9076628684997559 41 7.2943768501281738
		 42 7.7318201065063468 43 8.0954313278198242 44 8.2606220245361328 45 8.263127326965332
		 46 8.2139930725097656 47 8.1015987396240234 48 7.914515495300293 49 7.6415400505065918
		 50 7.2384939193725595 51 6.6880912780761719 52 6.0185713768005371 53 5.2579965591430664
		 54 4.4351997375488281 55 3.5796117782592773 56 2.5978860855102539 57 1.4717618227005005
		 58 0.34336292743682861 59 -0.6452210545539856 60 -1.3519042730331421 61 -1.7789130210876465
		 62 -2.0455186367034912 63 -2.1851866245269775 64 -2.2317454814910889 65 -2.2195935249328613
		 66 -2.3636031150817871 67 -2.3667352199554443 68 -2.206535816192627 69 -1.7165805101394653
		 70 -0.84836494922637939 71 0.24368621408939359 72 1.4051480293273926 73 2.4815964698791504
		 74 3.3186066150665283 75 3.9064927101135254 76 4.3668580055236816 77 4.7422089576721191
		 78 5.0750508308410645 79 5.407893180847168 80 5.7832403182983398;
createNode animCurveTA -n "Character1_Head_rotateY";
	rename -uid "D097CAE4-4FF5-A7EA-FD36-45AC026DB6F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  0 11.555663108825684 1 7.1652026176452637
		 2 2.7746682167053223 3 0.037263162434101105 4 -0.77365171909332275 5 -0.73813939094543457
		 6 -0.23314085602760315 7 0.36440518498420715 8 0.67756199836730957 9 0.7208857536315918
		 10 0.75592195987701416 11 0.76167148351669312 12 0.72672897577285767 13 0.65578937530517578
		 14 0.54959547519683838 15 0.40722325444221497 16 0.19900976121425629 17 -0.086003728210926056
		 18 -0.41814979910850525 19 -0.76657670736312866 20 -1.099906325340271 21 -1.3893522024154663
		 22 -1.6129978895187378 23 -1.7498005628585815 24 -1.7794417142868042 25 -1.7080793380737305
		 26 -1.5306463241577148 27 -1.2630630731582642 28 -0.97405338287353527 29 -0.72803223133087158
		 30 -0.54906570911407471 31 -0.41092675924301147 32 -0.2997475266456604 33 -0.20547826588153839
		 34 -0.12154356390237808 35 -0.070144623517990112 36 -0.058321513235569 39 -0.072535894811153412
		 40 -0.069062620401382446 41 -0.057607654482126243 42 -0.041738107800483704 43 -0.025140542536973953
		 44 -0.01172635518014431 45 0.011935367248952389 46 0.054787918925285339 47 0.10540154576301575
		 48 0.15219672024250031 49 0.18445274233818054 50 0.18803739547729492 51 0.14827717840671539
		 52 0.08050229400396347 53 0.011428679339587688 54 -0.039414294064044952 55 -0.06204646825790406
		 56 -0.070520929992198944 57 -0.07928786426782608 58 -0.086716696619987488 59 -0.090875566005706787
		 60 -0.08929847925901413 61 -0.11309529840946196 62 -0.18077920377254486 63 -0.26927497982978821
		 64 -0.35394185781478882 65 -0.40883859992027283 66 -0.41241404414176941 67 -0.32850247621536255
		 68 -0.12997986376285553 69 0.2035706639289856 70 0.66889244318008423 71 1.2575992345809937
		 72 1.9613050222396853 73 2.7716238498687744 74 3.6801693439483643 75 4.7327613830566406
		 76 5.9452729225158691 77 7.264397621154786 78 8.6368284225463867 79 10.009259223937988
		 80 11.328383445739746;
createNode animCurveTA -n "Character1_Head_rotateZ";
	rename -uid "DE5C9742-4B2C-4CC2-7651-039F1D0CEECC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 5.2613430023193359 1 -2.3497011661529541
		 2 -9.975123405456543 3 -13.922727584838867 4 -13.39113712310791 5 -10.7884521484375
		 6 -7.227971076965332 7 -3.8232867717742924 8 -1.6875331401824951 9 -0.81653130054473877
		 10 -0.3808707594871521 11 -0.060624897480010986 12 0.36311659216880798 13 0.86356550455093384
		 14 1.4136573076248169 15 1.9862341880798338 16 2.5893604755401611 17 3.2234494686126709
		 18 3.8501324653625493 19 4.4313464164733887 20 4.9294171333312988 21 5.3106188774108887
		 22 5.5284581184387207 23 5.526127815246582 24 5.2463054656982422 25 4.6561288833618164
		 26 3.6165769100189205 27 2.2066967487335205 28 0.7749335765838623 29 -0.32966914772987366
		 30 -0.96674144268035878 31 -1.3019553422927856 32 -1.4449021816253662 33 -1.5057573318481445
		 34 -1.5951918363571167 35 -1.6775754690170288 36 -1.6832941770553589 37 -1.670926570892334
		 38 -1.6455446481704712 39 -1.6122311353683472 40 -1.5822755098342896 41 -1.569916844367981
		 42 -1.5846726894378662 43 -1.6362190246582031 44 -1.7343934774398804 45 -1.9508037567138672
		 46 -2.30214524269104 47 -2.7160270214080811 48 -3.1200671195983887 49 -3.4417319297790527
		 50 -3.5922427177429199 51 -3.4398946762084961 52 -3.0193347930908203 53 -2.475614070892334
		 54 -1.9547982215881348 55 -1.6039324998855591 56 -1.4223523139953613 57 -1.3040522336959839
		 58 -1.2388564348220825 59 -1.2169458866119385 60 -1.2288025617599487 61 -1.2651728391647339
		 62 -1.3199180364608765 63 -1.3881996870040894 64 -1.4650911092758179 65 -1.5456411838531494
		 66 -1.5158740282058716 67 -1.5717589855194092 68 -1.7428489923477173 69 -2.2795531749725342
		 70 -3.2209484577178955 71 -4.2795867919921875 72 -5.168022632598877 73 -5.598811149597168
		 74 -5.2845096588134766 75 -4.0823421478271484 76 -2.1937112808227539 77 0.15255948901176453
		 78 2.7276475429534912 79 5.3027315139770508 80 7.6489887237548828;
createNode animCurveTU -n "Character1_Head_visibility";
	rename -uid "E57F30C4-4D29-9626-A7F2-4B9806EE6A34";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "eyes_translateX";
	rename -uid "03D1D22A-40D0-C687-7606-5B9FD550E6D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 30.947149276733398 2 30.947149276733398
		 3 29.381168365478516 4 29.381168365478516 7 29.381168365478516 8 29.381168365478516
		 9 30.947149276733398 10 30.947149276733398;
createNode animCurveTL -n "eyes_translateY";
	rename -uid "8050416E-450E-E189-34AC-88B5A41F5827";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -5.0992542810490704e-007 2 -5.0992542810490704e-007
		 3 2.5438647270202637 4 2.5438647270202637 7 2.5438647270202637 8 2.5438647270202637
		 9 -5.0992542810490704e-007 10 -5.0992542810490704e-007;
createNode animCurveTL -n "eyes_translateZ";
	rename -uid "862BBF69-4512-AB87-74C7-67BED05D1F6E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1.121469657048256e-013 2 1.121469657048256e-013
		 3 0.23577801883220673 4 0.23577801883220673 7 0.23577801883220673 8 0.23577801883220673
		 9 1.121469657048256e-013 10 1.121469657048256e-013;
createNode animCurveTU -n "eyes_scaleX";
	rename -uid "1A9FAB27-4741-CB5A-96FC-09BD87F4C8B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleY";
	rename -uid "B8743E55-41D6-1F29-5FBA-969B840520E9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleZ";
	rename -uid "FA79ECB5-43AA-D498-7EEE-09A4AF505009";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "eyes_rotateX";
	rename -uid "5395FF9B-44D3-E975-692D-6BA434DC1F94";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 74 ".ktv[0:73]"  7 0 8 0 9 2.397258996963501 10 2.4573600292205811
		 11 2.4720265865325928 12 2.4474787712097168 13 2.3899364471435547 14 2.3056197166442871
		 15 2.2007486820220947 16 2.0815432071685791 17 1.9542236328125002 18 1.8250095844268799
		 19 1.7001211643218994 20 1.5857785940170288 21 1.4882017374038696 22 1.4136106967926025
		 23 1.3682252168655396 24 1.3582658767700195 25 1.4152724742889404 26 1.5503720045089722
		 27 1.739165186882019 28 1.9572517871856689 29 2.1802325248718262 30 2.3837075233459473
		 31 2.5432770252227783 32 2.6345412731170654 33 2.6331009864807129 34 2.5586450099945068
		 35 2.4502177238464355 36 2.312453031539917 37 2.1499848365783691 38 1.9674462080001833
		 39 1.7694710493087769 40 1.5606933832168579 41 1.3457461595535278 42 1.1292635202407837
		 43 0.91587889194488514 44 0.71022593975067139 45 0.51693838834762573 46 0.34064969420433044
		 47 0.18599370121955872 48 0.057603910565376275 49 -0.039885982871055603 50 -0.087373897433280945
		 51 -0.075341641902923584 52 -0.01353142224252224 53 0.088314548134803772 54 0.22045400738716125
		 55 0.37314480543136597 56 0.53664463758468628 57 0.70121133327484131 58 0.90715533494949341
		 59 1.1826542615890503 60 1.4997669458389282 61 1.830551743507385 62 2.1470673084259033
		 63 2.4213724136352539 64 2.6255252361297607 65 2.7315852642059326 66 2.7116103172302246
		 67 1.979702830314636 68 0.61642760038375854 69 -0.40646249055862427 70 -0.90035450458526611
		 71 -1.3183424472808838 72 -1.6512341499328613 73 -1.8898372650146482 74 -2.0249598026275635
		 75 -2.0474090576171875 76 -1.8763469457626345 77 -1.4966675043106079 78 -0.99777829647064209
		 79 -0.46908664703369146 80 0;
createNode animCurveTA -n "eyes_rotateY";
	rename -uid "BB8D4E2E-4BAB-643A-7EB3-3FBFA6FBAD19";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 74 ".ktv[0:73]"  7 0 8 0 9 -2.445497989654541 10 -2.5919930934906006
		 11 -2.7020549774169922 12 -2.7797267436981201 13 -2.8290507793426514 14 -2.8540706634521484
		 15 -2.8588287830352783 16 -2.8473677635192871 17 -2.8237311840057373 18 -2.7919614315032959
		 19 -2.7561016082763672 20 -2.7201938629150391 21 -2.6882820129394531 22 -2.6644084453582764
		 23 -2.652616024017334 24 -2.6569476127624512 25 -2.6690609455108643 26 -2.678699254989624
		 27 -2.6870346069335938 28 -2.6952383518218994 29 -2.7044820785522461 30 -2.7159371376037598
		 31 -2.7307755947113037 32 -2.7501685619354248 33 -2.7752883434295654 34 -2.805983304977417
		 35 -2.8408451080322266 36 -2.8791584968566895 37 -2.9202098846435547 38 -2.9632844924926758
		 39 -3.0076677799224854 40 -3.0526449680328369 41 -3.0975019931793213 42 -3.141524076461792
		 43 -3.1839964389801025 44 -3.2242052555084229 45 -3.2614352703094482 46 -3.2949724197387695
		 47 -3.3241021633148193 48 -3.3481097221374512 49 -3.3662807941436768 50 -3.3691041469573975
		 51 -3.3513796329498291 52 -3.3188591003417969 53 -3.2772946357727051 54 -3.2324385643005371
		 55 -3.1900424957275391 56 -3.1558589935302734 57 -3.1356394290924072 58 -3.1272354125976562
		 59 -3.1240696907043457 60 -3.1252524852752686 61 -3.1298940181732178 62 -3.1371047496795654
		 63 -3.1459951400756836 64 -3.155674934387207 65 -3.1652545928955078 66 -3.173844575881958
		 67 -3.1024513244628906 68 -3.0381646156311035 69 -3.2292201519012451 70 -3.8123388290405273
		 71 -4.6389684677124023 72 -5.5294337272644043 73 -6.3040585517883301 74 -6.7831692695617676
		 75 -6.7870907783508301 76 -6.1105384826660156 77 -4.8382282257080078 78 -3.2254858016967773
		 79 -1.5276346206665039 80 0;
createNode animCurveTA -n "eyes_rotateZ";
	rename -uid "124EFA4C-412A-817A-3DA2-9EA3C5CA9ADC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 74 ".ktv[0:73]"  7 0 8 0 9 -6.2926421165466309 10 -6.6072688102722168
		 11 -6.8273835182189941 12 -6.9634408950805664 13 -7.0258984565734863 14 -7.0252137184143066
		 15 -6.9718413352966309 16 -6.8762383460998535 17 -6.7488622665405273 18 -6.6001677513122559
		 19 -6.4406132698059082 20 -6.2806539535522461 21 -6.1307463645935059 22 -6.001347541809082
		 23 -5.9029140472412109 24 -5.8459014892578125 25 -5.8057236671447754 26 -5.7542223930358887
		 27 -5.6964974403381348 28 -5.6376495361328125 29 -5.5827779769897461 30 -5.536982536315918
		 31 -5.5053625106811523 32 -5.4930195808410645 33 -5.5050520896911621 34 -5.5406179428100586
		 35 -5.5935001373291016 36 -5.6607375144958496 37 -5.7393698692321777 38 -5.8264350891113281
		 39 -5.918973445892334 40 -6.0140223503112793 41 -6.1086220741271973 42 -6.1998114585876465
		 43 -6.2846288681030273 44 -6.3601140975952148 45 -6.4233055114746094 46 -6.4712419509887695
		 47 -6.5009632110595703 48 -6.5095076560974121 49 -6.4939146041870117 50 -6.4175505638122559
		 51 -6.2601008415222168 52 -6.0430808067321777 53 -5.7880058288574219 54 -5.516392707824707
		 55 -5.2497563362121582 56 -5.0096125602722168 57 -4.8174772262573242 58 -4.757695198059082
		 59 -4.8520793914794922 60 -5.028346061706543 61 -5.2142114639282227 62 -5.3373928070068359
		 63 -5.3256063461303711 64 -5.1065683364868164 65 -4.6079959869384766 66 -3.7576053142547612
		 67 -0.25753259658813477 68 5.1078009605407715 69 7.7872514724731445 70 6.7846665382385254
		 71 4.5270380973815918 72 1.5979390144348145 73 -1.4190585613250732 74 -3.9403834342956543
		 75 -5.3824620246887207 76 -5.4480009078979492 77 -4.5142631530761719 78 -3.0095086097717285
		 79 -1.3620004653930664 80 0;
createNode animCurveTU -n "eyes_visibility";
	rename -uid "AB117A48-4BFC-E109-422C-7EACC83E5179";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "jaw_translateX";
	rename -uid "A7459921-44E5-A810-7048-069BDB1422E7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.07279486209154129;
createNode animCurveTL -n "jaw_translateY";
	rename -uid "FADAD523-4FC7-B4D8-87FE-EF8F69FFA51D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.25670051574707;
createNode animCurveTL -n "jaw_translateZ";
	rename -uid "477A820D-4ECC-B8FB-91D0-A6A8CC477DD3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.2238426557523781e-007;
createNode animCurveTU -n "jaw_scaleX";
	rename -uid "04670285-4714-9D8A-E65C-A690C627E841";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleY";
	rename -uid "7843326B-40E7-90BB-6520-48BE42D845F5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleZ";
	rename -uid "A00868F8-4F25-6992-77EE-73972F4C54E1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "jaw_rotateX";
	rename -uid "6EFB35E8-45B2-4819-E825-4E83BA537CA0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -46.740768432617188 1 -51.023727416992188
		 2 -53.491409301757812 3 -53.658416748046875 4 -53.642181396484375 5 -51.802459716796875
		 6 -46.541152954101563 7 -34.1929931640625 8 -16.607864379882812 9 -0.7409820556640625
		 10 6.7701568603515625 11 2.9539794921875 12 -7.721405029296875 13 -20.817047119140625
		 14 -31.89788818359375 15 -36.53082275390625 16 -32.290664672851562 17 -22.134033203125
		 18 -9.7019500732421875 19 1.36553955078125 20 7.429412841796875 21 6.9066925048828125
		 22 2.2248382568359375 23 -4.237457275390625 24 -10.098922729492187 25 -12.98052978515625
		 26 -10.977676391601562 27 -5.7934722900390625 28 -0.46783447265625006 29 1.9548645019531252
		 30 0.35137939453125 31 -3.4198150634765625 32 -7.9289093017578116 33 -11.745880126953125
		 34 -13.44122314453125 35 -12.0634765625 36 -8.5643157958984375 37 -4.3687744140625
		 38 -0.902313232421875 39 0.41009521484375 40 -1.5529937744140625 41 -5.840576171875
		 42 -10.768966674804688 43 -14.654464721679689 44 -15.812591552734375 45 -13.529327392578125
		 46 -8.980987548828125 47 -3.318450927734375 48 2.307037353515625 49 6.744049072265625
		 50 8.8408050537109375 51 7.2918243408203125 52 2.7954254150390625 53 -2.7956390380859375
		 54 -7.6297149658203125 55 -9.85565185546875 56 -8.4315032958984375 57 -4.5916290283203125
		 58 0.1011199951171875 59 4.084686279296875 60 5.7974395751953125 61 5.030853271484375
		 62 2.8281402587890625 63 -0.4921722412109375 64 -4.6115875244140625 65 -9.22052001953125
		 66 -14.591827392578125 67 -20.904739379882812 68 -27.703292846679687 69 -34.534149169921875
		 70 -40.939453125 71 -46.475173950195312 72 -50.591400146484375 73 -53.038711547851562
		 74 -53.826736450195312 75 -53.281028747558594 76 -51.984146118164062 77 -50.302719116210937
		 78 -48.574310302734375 79 -47.231979370117188 80 -46.71697998046875;
createNode animCurveTA -n "jaw_rotateY";
	rename -uid "0C9C27B9-4422-70B4-3074-F5BD1A0DD892";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 42 ".ktv[0:41]"  0 -120.80289459228517 1 -120.74984741210937
		 2 -120.69490051269533 3 -120.63941955566406 4 -120.59445190429687 5 -120.59445190429687
		 6 -120.62887573242187 7 -120.67057800292969 8 -120.68076324462892 9 -120.68443298339844
		 12 -120.68255615234374 16 -120.67928314208984 17 -120.68345642089842 19 -120.69245147705078
		 27 -120.69219207763672 28 -120.68791198730469 29 -120.68215942382814 30 -120.67453002929687
		 31 -120.66561126708986 32 -120.65628051757812 33 -120.64788818359375 34 -120.64234924316406
		 36 -120.64364624023439 37 -120.64808654785155 48 -120.65455627441406 52 -120.65361022949219
		 56 -120.65090179443358 57 -120.65512847900391 58 -120.6599578857422 59 -120.6641387939453
		 68 -120.66628265380859 69 -120.66037750244139 70 -120.65200805664061 71 -120.64701080322266
		 72 -120.64696502685547 73 -120.65121459960939 74 -120.65877532958986 75 -120.68828582763672
		 76 -120.74690246582031 77 -120.80400085449217 78 -120.82220458984374 79 -120.81962585449219;
createNode animCurveTA -n "jaw_rotateZ";
	rename -uid "31335815-46E7-5C3E-3C3F-50BBFC248791";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -44.638351440429687 1 -44.832138061523438
		 2 -45.00640869140625 3 -45.117889404296875 4 -45.163986206054688 5 -45.180740356445313
		 6 -45.181961059570313 7 -45.147781372070313 8 -45.13201904296875 9 -45.126724243164063
		 10 -45.13427734375 11 -45.14776611328125 12 -45.160202026367188 13 -45.168807983398438
		 14 -45.173095703125 17 -45.169509887695313 18 -45.164871215820313 19 -45.155548095703125
		 20 -45.140426635742187 21 -45.119384765625 22 -45.094879150390625 23 -45.07220458984375
		 24 -45.060317993164062 25 -45.064529418945313 26 -45.0799560546875 27 -45.103530883789063
		 28 -45.134185791015625 29 -45.1669921875 30 -45.196929931640625 31 -45.222366333007813
		 32 -45.243576049804688 33 -45.261367797851563 34 -45.276275634765625 35 -45.291244506835938
		 36 -45.307449340820312 37 -45.32257080078125 38 -45.33349609375 39 -45.337692260742187
		 40 -45.333938598632813 41 -45.324676513671875 42 -45.312957763671875 43 -45.30194091796875
		 44 -45.295394897460938 45 -45.293075561523438 50 -45.291854858398437 51 -45.287994384765625
		 52 -45.282119750976563 53 -45.27593994140625 54 -45.270248413085938 59 -45.267974853515625
		 60 -45.263885498046875 61 -45.256500244140625 62 -45.247222900390625 63 -45.236572265625
		 64 -45.214141845703125 65 -45.18988037109375 66 -45.181777954101563 67 -45.1802978515625
		 68 -45.173171997070312 69 -45.18145751953125 70 -45.191085815429688 71 -45.181411743164063
		 72 -45.153045654296875 73 -45.114364624023438 74 -45.074462890625 75 -45.016860961914063
		 76 -44.930557250976562 77 -44.8336181640625 78 -44.760055541992188 79 -44.698135375976563
		 80 -44.630569458007813;
createNode animCurveTU -n "jaw_visibility";
	rename -uid "2CF40776-483F-26EC-E5E9-1A9FCBE2195E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "17DE0F05-40E7-DF79-07F1-CCA3A17C8BF3";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "660125FE-4C8A-DAE5-D326-298CA85AD7B4";
createNode displayLayer -n "defaultLayer";
	rename -uid "F608A9B2-4D0B-D4EB-7847-07983A5444F7";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "FEAB739C-4857-1A84-4B88-848F09EE3946";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "84D03EC8-4154-109C-F314-A4B762DD3862";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "94CB31B8-4CD6-DFAF-F011-9A91F4C5DDBB";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1636\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1636\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1286\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1286\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 16 100 -ps 2 84 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1286\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1286\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "5EC35307-448E-4D37-C888-2182A3858DEB";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 80 -ast 0 -aet 250 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 80;
	setAttr ".unw" 80;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
connectAttr "ref_frame.s" "Character1_Hips.is";
connectAttr "Character1_Hips_scaleX.o" "Character1_Hips.sx";
connectAttr "Character1_Hips_scaleY.o" "Character1_Hips.sy";
connectAttr "Character1_Hips_scaleZ.o" "Character1_Hips.sz";
connectAttr "Character1_Hips_translateX.o" "Character1_Hips.tx";
connectAttr "Character1_Hips_translateY.o" "Character1_Hips.ty";
connectAttr "Character1_Hips_translateZ.o" "Character1_Hips.tz";
connectAttr "Character1_Hips_rotateX.o" "Character1_Hips.rx";
connectAttr "Character1_Hips_rotateY.o" "Character1_Hips.ry";
connectAttr "Character1_Hips_rotateZ.o" "Character1_Hips.rz";
connectAttr "Character1_Hips_visibility.o" "Character1_Hips.v";
connectAttr "Character1_Hips.s" "Character1_LeftUpLeg.is";
connectAttr "Character1_LeftUpLeg_scaleX.o" "Character1_LeftUpLeg.sx";
connectAttr "Character1_LeftUpLeg_scaleY.o" "Character1_LeftUpLeg.sy";
connectAttr "Character1_LeftUpLeg_scaleZ.o" "Character1_LeftUpLeg.sz";
connectAttr "Character1_LeftUpLeg_translateX.o" "Character1_LeftUpLeg.tx";
connectAttr "Character1_LeftUpLeg_translateY.o" "Character1_LeftUpLeg.ty";
connectAttr "Character1_LeftUpLeg_translateZ.o" "Character1_LeftUpLeg.tz";
connectAttr "Character1_LeftUpLeg_rotateX.o" "Character1_LeftUpLeg.rx";
connectAttr "Character1_LeftUpLeg_rotateY.o" "Character1_LeftUpLeg.ry";
connectAttr "Character1_LeftUpLeg_rotateZ.o" "Character1_LeftUpLeg.rz";
connectAttr "Character1_LeftUpLeg_visibility.o" "Character1_LeftUpLeg.v";
connectAttr "Character1_LeftUpLeg.s" "Character1_LeftLeg.is";
connectAttr "Character1_LeftLeg_scaleX.o" "Character1_LeftLeg.sx";
connectAttr "Character1_LeftLeg_scaleY.o" "Character1_LeftLeg.sy";
connectAttr "Character1_LeftLeg_scaleZ.o" "Character1_LeftLeg.sz";
connectAttr "Character1_LeftLeg_translateX.o" "Character1_LeftLeg.tx";
connectAttr "Character1_LeftLeg_translateY.o" "Character1_LeftLeg.ty";
connectAttr "Character1_LeftLeg_translateZ.o" "Character1_LeftLeg.tz";
connectAttr "Character1_LeftLeg_rotateX.o" "Character1_LeftLeg.rx";
connectAttr "Character1_LeftLeg_rotateY.o" "Character1_LeftLeg.ry";
connectAttr "Character1_LeftLeg_rotateZ.o" "Character1_LeftLeg.rz";
connectAttr "Character1_LeftLeg_visibility.o" "Character1_LeftLeg.v";
connectAttr "Character1_LeftLeg.s" "Character1_LeftFoot.is";
connectAttr "Character1_LeftFoot_scaleX.o" "Character1_LeftFoot.sx";
connectAttr "Character1_LeftFoot_scaleY.o" "Character1_LeftFoot.sy";
connectAttr "Character1_LeftFoot_scaleZ.o" "Character1_LeftFoot.sz";
connectAttr "Character1_LeftFoot_translateX.o" "Character1_LeftFoot.tx";
connectAttr "Character1_LeftFoot_translateY.o" "Character1_LeftFoot.ty";
connectAttr "Character1_LeftFoot_translateZ.o" "Character1_LeftFoot.tz";
connectAttr "Character1_LeftFoot_rotateX.o" "Character1_LeftFoot.rx";
connectAttr "Character1_LeftFoot_rotateY.o" "Character1_LeftFoot.ry";
connectAttr "Character1_LeftFoot_rotateZ.o" "Character1_LeftFoot.rz";
connectAttr "Character1_LeftFoot_visibility.o" "Character1_LeftFoot.v";
connectAttr "Character1_LeftFoot.s" "Character1_LeftToeBase.is";
connectAttr "Character1_LeftToeBase_scaleX.o" "Character1_LeftToeBase.sx";
connectAttr "Character1_LeftToeBase_scaleY.o" "Character1_LeftToeBase.sy";
connectAttr "Character1_LeftToeBase_scaleZ.o" "Character1_LeftToeBase.sz";
connectAttr "Character1_LeftToeBase_translateX.o" "Character1_LeftToeBase.tx";
connectAttr "Character1_LeftToeBase_translateY.o" "Character1_LeftToeBase.ty";
connectAttr "Character1_LeftToeBase_translateZ.o" "Character1_LeftToeBase.tz";
connectAttr "Character1_LeftToeBase_rotateX.o" "Character1_LeftToeBase.rx";
connectAttr "Character1_LeftToeBase_rotateY.o" "Character1_LeftToeBase.ry";
connectAttr "Character1_LeftToeBase_rotateZ.o" "Character1_LeftToeBase.rz";
connectAttr "Character1_LeftToeBase_visibility.o" "Character1_LeftToeBase.v";
connectAttr "Character1_LeftToeBase.s" "Character1_LeftFootMiddle1.is";
connectAttr "Character1_LeftFootMiddle1_scaleX.o" "Character1_LeftFootMiddle1.sx"
		;
connectAttr "Character1_LeftFootMiddle1_scaleY.o" "Character1_LeftFootMiddle1.sy"
		;
connectAttr "Character1_LeftFootMiddle1_scaleZ.o" "Character1_LeftFootMiddle1.sz"
		;
connectAttr "Character1_LeftFootMiddle1_translateX.o" "Character1_LeftFootMiddle1.tx"
		;
connectAttr "Character1_LeftFootMiddle1_translateY.o" "Character1_LeftFootMiddle1.ty"
		;
connectAttr "Character1_LeftFootMiddle1_translateZ.o" "Character1_LeftFootMiddle1.tz"
		;
connectAttr "Character1_LeftFootMiddle1_rotateX.o" "Character1_LeftFootMiddle1.rx"
		;
connectAttr "Character1_LeftFootMiddle1_rotateY.o" "Character1_LeftFootMiddle1.ry"
		;
connectAttr "Character1_LeftFootMiddle1_rotateZ.o" "Character1_LeftFootMiddle1.rz"
		;
connectAttr "Character1_LeftFootMiddle1_visibility.o" "Character1_LeftFootMiddle1.v"
		;
connectAttr "Character1_LeftFootMiddle1.s" "Character1_LeftFootMiddle2.is";
connectAttr "Character1_LeftFootMiddle2_translateX.o" "Character1_LeftFootMiddle2.tx"
		;
connectAttr "Character1_LeftFootMiddle2_translateY.o" "Character1_LeftFootMiddle2.ty"
		;
connectAttr "Character1_LeftFootMiddle2_translateZ.o" "Character1_LeftFootMiddle2.tz"
		;
connectAttr "Character1_LeftFootMiddle2_scaleX.o" "Character1_LeftFootMiddle2.sx"
		;
connectAttr "Character1_LeftFootMiddle2_scaleY.o" "Character1_LeftFootMiddle2.sy"
		;
connectAttr "Character1_LeftFootMiddle2_scaleZ.o" "Character1_LeftFootMiddle2.sz"
		;
connectAttr "Character1_LeftFootMiddle2_rotateX.o" "Character1_LeftFootMiddle2.rx"
		;
connectAttr "Character1_LeftFootMiddle2_rotateY.o" "Character1_LeftFootMiddle2.ry"
		;
connectAttr "Character1_LeftFootMiddle2_rotateZ.o" "Character1_LeftFootMiddle2.rz"
		;
connectAttr "Character1_LeftFootMiddle2_visibility.o" "Character1_LeftFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_RightUpLeg.is";
connectAttr "Character1_RightUpLeg_scaleX.o" "Character1_RightUpLeg.sx";
connectAttr "Character1_RightUpLeg_scaleY.o" "Character1_RightUpLeg.sy";
connectAttr "Character1_RightUpLeg_scaleZ.o" "Character1_RightUpLeg.sz";
connectAttr "Character1_RightUpLeg_translateX.o" "Character1_RightUpLeg.tx";
connectAttr "Character1_RightUpLeg_translateY.o" "Character1_RightUpLeg.ty";
connectAttr "Character1_RightUpLeg_translateZ.o" "Character1_RightUpLeg.tz";
connectAttr "Character1_RightUpLeg_rotateX.o" "Character1_RightUpLeg.rx";
connectAttr "Character1_RightUpLeg_rotateY.o" "Character1_RightUpLeg.ry";
connectAttr "Character1_RightUpLeg_rotateZ.o" "Character1_RightUpLeg.rz";
connectAttr "Character1_RightUpLeg_visibility.o" "Character1_RightUpLeg.v";
connectAttr "Character1_RightUpLeg.s" "Character1_RightLeg.is";
connectAttr "Character1_RightLeg_scaleX.o" "Character1_RightLeg.sx";
connectAttr "Character1_RightLeg_scaleY.o" "Character1_RightLeg.sy";
connectAttr "Character1_RightLeg_scaleZ.o" "Character1_RightLeg.sz";
connectAttr "Character1_RightLeg_translateX.o" "Character1_RightLeg.tx";
connectAttr "Character1_RightLeg_translateY.o" "Character1_RightLeg.ty";
connectAttr "Character1_RightLeg_translateZ.o" "Character1_RightLeg.tz";
connectAttr "Character1_RightLeg_rotateX.o" "Character1_RightLeg.rx";
connectAttr "Character1_RightLeg_rotateY.o" "Character1_RightLeg.ry";
connectAttr "Character1_RightLeg_rotateZ.o" "Character1_RightLeg.rz";
connectAttr "Character1_RightLeg_visibility.o" "Character1_RightLeg.v";
connectAttr "Character1_RightLeg.s" "Character1_RightFoot.is";
connectAttr "Character1_RightFoot_scaleX.o" "Character1_RightFoot.sx";
connectAttr "Character1_RightFoot_scaleY.o" "Character1_RightFoot.sy";
connectAttr "Character1_RightFoot_scaleZ.o" "Character1_RightFoot.sz";
connectAttr "Character1_RightFoot_translateX.o" "Character1_RightFoot.tx";
connectAttr "Character1_RightFoot_translateY.o" "Character1_RightFoot.ty";
connectAttr "Character1_RightFoot_translateZ.o" "Character1_RightFoot.tz";
connectAttr "Character1_RightFoot_rotateX.o" "Character1_RightFoot.rx";
connectAttr "Character1_RightFoot_rotateY.o" "Character1_RightFoot.ry";
connectAttr "Character1_RightFoot_rotateZ.o" "Character1_RightFoot.rz";
connectAttr "Character1_RightFoot_visibility.o" "Character1_RightFoot.v";
connectAttr "Character1_RightFoot.s" "Character1_RightToeBase.is";
connectAttr "Character1_RightToeBase_scaleX.o" "Character1_RightToeBase.sx";
connectAttr "Character1_RightToeBase_scaleY.o" "Character1_RightToeBase.sy";
connectAttr "Character1_RightToeBase_scaleZ.o" "Character1_RightToeBase.sz";
connectAttr "Character1_RightToeBase_translateX.o" "Character1_RightToeBase.tx";
connectAttr "Character1_RightToeBase_translateY.o" "Character1_RightToeBase.ty";
connectAttr "Character1_RightToeBase_translateZ.o" "Character1_RightToeBase.tz";
connectAttr "Character1_RightToeBase_rotateX.o" "Character1_RightToeBase.rx";
connectAttr "Character1_RightToeBase_rotateY.o" "Character1_RightToeBase.ry";
connectAttr "Character1_RightToeBase_rotateZ.o" "Character1_RightToeBase.rz";
connectAttr "Character1_RightToeBase_visibility.o" "Character1_RightToeBase.v";
connectAttr "Character1_RightToeBase.s" "Character1_RightFootMiddle1.is";
connectAttr "Character1_RightFootMiddle1_scaleX.o" "Character1_RightFootMiddle1.sx"
		;
connectAttr "Character1_RightFootMiddle1_scaleY.o" "Character1_RightFootMiddle1.sy"
		;
connectAttr "Character1_RightFootMiddle1_scaleZ.o" "Character1_RightFootMiddle1.sz"
		;
connectAttr "Character1_RightFootMiddle1_translateX.o" "Character1_RightFootMiddle1.tx"
		;
connectAttr "Character1_RightFootMiddle1_translateY.o" "Character1_RightFootMiddle1.ty"
		;
connectAttr "Character1_RightFootMiddle1_translateZ.o" "Character1_RightFootMiddle1.tz"
		;
connectAttr "Character1_RightFootMiddle1_rotateX.o" "Character1_RightFootMiddle1.rx"
		;
connectAttr "Character1_RightFootMiddle1_rotateY.o" "Character1_RightFootMiddle1.ry"
		;
connectAttr "Character1_RightFootMiddle1_rotateZ.o" "Character1_RightFootMiddle1.rz"
		;
connectAttr "Character1_RightFootMiddle1_visibility.o" "Character1_RightFootMiddle1.v"
		;
connectAttr "Character1_RightFootMiddle1.s" "Character1_RightFootMiddle2.is";
connectAttr "Character1_RightFootMiddle2_translateX.o" "Character1_RightFootMiddle2.tx"
		;
connectAttr "Character1_RightFootMiddle2_translateY.o" "Character1_RightFootMiddle2.ty"
		;
connectAttr "Character1_RightFootMiddle2_translateZ.o" "Character1_RightFootMiddle2.tz"
		;
connectAttr "Character1_RightFootMiddle2_scaleX.o" "Character1_RightFootMiddle2.sx"
		;
connectAttr "Character1_RightFootMiddle2_scaleY.o" "Character1_RightFootMiddle2.sy"
		;
connectAttr "Character1_RightFootMiddle2_scaleZ.o" "Character1_RightFootMiddle2.sz"
		;
connectAttr "Character1_RightFootMiddle2_rotateX.o" "Character1_RightFootMiddle2.rx"
		;
connectAttr "Character1_RightFootMiddle2_rotateY.o" "Character1_RightFootMiddle2.ry"
		;
connectAttr "Character1_RightFootMiddle2_rotateZ.o" "Character1_RightFootMiddle2.rz"
		;
connectAttr "Character1_RightFootMiddle2_visibility.o" "Character1_RightFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_Spine.is";
connectAttr "Character1_Spine_scaleX.o" "Character1_Spine.sx";
connectAttr "Character1_Spine_scaleY.o" "Character1_Spine.sy";
connectAttr "Character1_Spine_scaleZ.o" "Character1_Spine.sz";
connectAttr "Character1_Spine_translateX.o" "Character1_Spine.tx";
connectAttr "Character1_Spine_translateY.o" "Character1_Spine.ty";
connectAttr "Character1_Spine_translateZ.o" "Character1_Spine.tz";
connectAttr "Character1_Spine_rotateX.o" "Character1_Spine.rx";
connectAttr "Character1_Spine_rotateY.o" "Character1_Spine.ry";
connectAttr "Character1_Spine_rotateZ.o" "Character1_Spine.rz";
connectAttr "Character1_Spine_visibility.o" "Character1_Spine.v";
connectAttr "Character1_Spine.s" "Character1_Spine1.is";
connectAttr "Character1_Spine1_scaleX.o" "Character1_Spine1.sx";
connectAttr "Character1_Spine1_scaleY.o" "Character1_Spine1.sy";
connectAttr "Character1_Spine1_scaleZ.o" "Character1_Spine1.sz";
connectAttr "Character1_Spine1_translateX.o" "Character1_Spine1.tx";
connectAttr "Character1_Spine1_translateY.o" "Character1_Spine1.ty";
connectAttr "Character1_Spine1_translateZ.o" "Character1_Spine1.tz";
connectAttr "Character1_Spine1_rotateX.o" "Character1_Spine1.rx";
connectAttr "Character1_Spine1_rotateY.o" "Character1_Spine1.ry";
connectAttr "Character1_Spine1_rotateZ.o" "Character1_Spine1.rz";
connectAttr "Character1_Spine1_visibility.o" "Character1_Spine1.v";
connectAttr "Character1_Spine1.s" "Character1_LeftShoulder.is";
connectAttr "Character1_LeftShoulder_scaleX.o" "Character1_LeftShoulder.sx";
connectAttr "Character1_LeftShoulder_scaleY.o" "Character1_LeftShoulder.sy";
connectAttr "Character1_LeftShoulder_scaleZ.o" "Character1_LeftShoulder.sz";
connectAttr "Character1_LeftShoulder_translateX.o" "Character1_LeftShoulder.tx";
connectAttr "Character1_LeftShoulder_translateY.o" "Character1_LeftShoulder.ty";
connectAttr "Character1_LeftShoulder_translateZ.o" "Character1_LeftShoulder.tz";
connectAttr "Character1_LeftShoulder_rotateX.o" "Character1_LeftShoulder.rx";
connectAttr "Character1_LeftShoulder_rotateY.o" "Character1_LeftShoulder.ry";
connectAttr "Character1_LeftShoulder_rotateZ.o" "Character1_LeftShoulder.rz";
connectAttr "Character1_LeftShoulder_visibility.o" "Character1_LeftShoulder.v";
connectAttr "Character1_LeftShoulder.s" "Character1_LeftArm.is";
connectAttr "Character1_LeftArm_scaleX.o" "Character1_LeftArm.sx";
connectAttr "Character1_LeftArm_scaleY.o" "Character1_LeftArm.sy";
connectAttr "Character1_LeftArm_scaleZ.o" "Character1_LeftArm.sz";
connectAttr "Character1_LeftArm_translateX.o" "Character1_LeftArm.tx";
connectAttr "Character1_LeftArm_translateY.o" "Character1_LeftArm.ty";
connectAttr "Character1_LeftArm_translateZ.o" "Character1_LeftArm.tz";
connectAttr "Character1_LeftArm_rotateX.o" "Character1_LeftArm.rx";
connectAttr "Character1_LeftArm_rotateY.o" "Character1_LeftArm.ry";
connectAttr "Character1_LeftArm_rotateZ.o" "Character1_LeftArm.rz";
connectAttr "Character1_LeftArm_visibility.o" "Character1_LeftArm.v";
connectAttr "Character1_LeftArm.s" "Character1_LeftForeArm.is";
connectAttr "Character1_LeftForeArm_scaleX.o" "Character1_LeftForeArm.sx";
connectAttr "Character1_LeftForeArm_scaleY.o" "Character1_LeftForeArm.sy";
connectAttr "Character1_LeftForeArm_scaleZ.o" "Character1_LeftForeArm.sz";
connectAttr "Character1_LeftForeArm_translateX.o" "Character1_LeftForeArm.tx";
connectAttr "Character1_LeftForeArm_translateY.o" "Character1_LeftForeArm.ty";
connectAttr "Character1_LeftForeArm_translateZ.o" "Character1_LeftForeArm.tz";
connectAttr "Character1_LeftForeArm_rotateX.o" "Character1_LeftForeArm.rx";
connectAttr "Character1_LeftForeArm_rotateY.o" "Character1_LeftForeArm.ry";
connectAttr "Character1_LeftForeArm_rotateZ.o" "Character1_LeftForeArm.rz";
connectAttr "Character1_LeftForeArm_visibility.o" "Character1_LeftForeArm.v";
connectAttr "Character1_LeftForeArm.s" "Character1_LeftHand.is";
connectAttr "Character1_LeftHand_lockInfluenceWeights.o" "Character1_LeftHand.liw"
		;
connectAttr "Character1_LeftHand_scaleX.o" "Character1_LeftHand.sx";
connectAttr "Character1_LeftHand_scaleY.o" "Character1_LeftHand.sy";
connectAttr "Character1_LeftHand_scaleZ.o" "Character1_LeftHand.sz";
connectAttr "Character1_LeftHand_translateX.o" "Character1_LeftHand.tx";
connectAttr "Character1_LeftHand_translateY.o" "Character1_LeftHand.ty";
connectAttr "Character1_LeftHand_translateZ.o" "Character1_LeftHand.tz";
connectAttr "Character1_LeftHand_rotateX.o" "Character1_LeftHand.rx";
connectAttr "Character1_LeftHand_rotateY.o" "Character1_LeftHand.ry";
connectAttr "Character1_LeftHand_rotateZ.o" "Character1_LeftHand.rz";
connectAttr "Character1_LeftHand_visibility.o" "Character1_LeftHand.v";
connectAttr "Character1_LeftHand.s" "Character1_LeftHandThumb1.is";
connectAttr "Character1_LeftHandThumb1_scaleX.o" "Character1_LeftHandThumb1.sx";
connectAttr "Character1_LeftHandThumb1_scaleY.o" "Character1_LeftHandThumb1.sy";
connectAttr "Character1_LeftHandThumb1_scaleZ.o" "Character1_LeftHandThumb1.sz";
connectAttr "Character1_LeftHandThumb1_translateX.o" "Character1_LeftHandThumb1.tx"
		;
connectAttr "Character1_LeftHandThumb1_translateY.o" "Character1_LeftHandThumb1.ty"
		;
connectAttr "Character1_LeftHandThumb1_translateZ.o" "Character1_LeftHandThumb1.tz"
		;
connectAttr "Character1_LeftHandThumb1_rotateX.o" "Character1_LeftHandThumb1.rx"
		;
connectAttr "Character1_LeftHandThumb1_rotateY.o" "Character1_LeftHandThumb1.ry"
		;
connectAttr "Character1_LeftHandThumb1_rotateZ.o" "Character1_LeftHandThumb1.rz"
		;
connectAttr "Character1_LeftHandThumb1_visibility.o" "Character1_LeftHandThumb1.v"
		;
connectAttr "Character1_LeftHandThumb1.s" "Character1_LeftHandThumb2.is";
connectAttr "Character1_LeftHandThumb2_scaleX.o" "Character1_LeftHandThumb2.sx";
connectAttr "Character1_LeftHandThumb2_scaleY.o" "Character1_LeftHandThumb2.sy";
connectAttr "Character1_LeftHandThumb2_scaleZ.o" "Character1_LeftHandThumb2.sz";
connectAttr "Character1_LeftHandThumb2_translateX.o" "Character1_LeftHandThumb2.tx"
		;
connectAttr "Character1_LeftHandThumb2_translateY.o" "Character1_LeftHandThumb2.ty"
		;
connectAttr "Character1_LeftHandThumb2_translateZ.o" "Character1_LeftHandThumb2.tz"
		;
connectAttr "Character1_LeftHandThumb2_rotateX.o" "Character1_LeftHandThumb2.rx"
		;
connectAttr "Character1_LeftHandThumb2_rotateY.o" "Character1_LeftHandThumb2.ry"
		;
connectAttr "Character1_LeftHandThumb2_rotateZ.o" "Character1_LeftHandThumb2.rz"
		;
connectAttr "Character1_LeftHandThumb2_visibility.o" "Character1_LeftHandThumb2.v"
		;
connectAttr "Character1_LeftHandThumb2.s" "Character1_LeftHandThumb3.is";
connectAttr "Character1_LeftHandThumb3_translateX.o" "Character1_LeftHandThumb3.tx"
		;
connectAttr "Character1_LeftHandThumb3_translateY.o" "Character1_LeftHandThumb3.ty"
		;
connectAttr "Character1_LeftHandThumb3_translateZ.o" "Character1_LeftHandThumb3.tz"
		;
connectAttr "Character1_LeftHandThumb3_scaleX.o" "Character1_LeftHandThumb3.sx";
connectAttr "Character1_LeftHandThumb3_scaleY.o" "Character1_LeftHandThumb3.sy";
connectAttr "Character1_LeftHandThumb3_scaleZ.o" "Character1_LeftHandThumb3.sz";
connectAttr "Character1_LeftHandThumb3_rotateX.o" "Character1_LeftHandThumb3.rx"
		;
connectAttr "Character1_LeftHandThumb3_rotateY.o" "Character1_LeftHandThumb3.ry"
		;
connectAttr "Character1_LeftHandThumb3_rotateZ.o" "Character1_LeftHandThumb3.rz"
		;
connectAttr "Character1_LeftHandThumb3_visibility.o" "Character1_LeftHandThumb3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandIndex1.is";
connectAttr "Character1_LeftHandIndex1_scaleX.o" "Character1_LeftHandIndex1.sx";
connectAttr "Character1_LeftHandIndex1_scaleY.o" "Character1_LeftHandIndex1.sy";
connectAttr "Character1_LeftHandIndex1_scaleZ.o" "Character1_LeftHandIndex1.sz";
connectAttr "Character1_LeftHandIndex1_translateX.o" "Character1_LeftHandIndex1.tx"
		;
connectAttr "Character1_LeftHandIndex1_translateY.o" "Character1_LeftHandIndex1.ty"
		;
connectAttr "Character1_LeftHandIndex1_translateZ.o" "Character1_LeftHandIndex1.tz"
		;
connectAttr "Character1_LeftHandIndex1_rotateX.o" "Character1_LeftHandIndex1.rx"
		;
connectAttr "Character1_LeftHandIndex1_rotateY.o" "Character1_LeftHandIndex1.ry"
		;
connectAttr "Character1_LeftHandIndex1_rotateZ.o" "Character1_LeftHandIndex1.rz"
		;
connectAttr "Character1_LeftHandIndex1_visibility.o" "Character1_LeftHandIndex1.v"
		;
connectAttr "Character1_LeftHandIndex1.s" "Character1_LeftHandIndex2.is";
connectAttr "Character1_LeftHandIndex2_scaleX.o" "Character1_LeftHandIndex2.sx";
connectAttr "Character1_LeftHandIndex2_scaleY.o" "Character1_LeftHandIndex2.sy";
connectAttr "Character1_LeftHandIndex2_scaleZ.o" "Character1_LeftHandIndex2.sz";
connectAttr "Character1_LeftHandIndex2_translateX.o" "Character1_LeftHandIndex2.tx"
		;
connectAttr "Character1_LeftHandIndex2_translateY.o" "Character1_LeftHandIndex2.ty"
		;
connectAttr "Character1_LeftHandIndex2_translateZ.o" "Character1_LeftHandIndex2.tz"
		;
connectAttr "Character1_LeftHandIndex2_rotateX.o" "Character1_LeftHandIndex2.rx"
		;
connectAttr "Character1_LeftHandIndex2_rotateY.o" "Character1_LeftHandIndex2.ry"
		;
connectAttr "Character1_LeftHandIndex2_rotateZ.o" "Character1_LeftHandIndex2.rz"
		;
connectAttr "Character1_LeftHandIndex2_visibility.o" "Character1_LeftHandIndex2.v"
		;
connectAttr "Character1_LeftHandIndex2.s" "Character1_LeftHandIndex3.is";
connectAttr "Character1_LeftHandIndex3_translateX.o" "Character1_LeftHandIndex3.tx"
		;
connectAttr "Character1_LeftHandIndex3_translateY.o" "Character1_LeftHandIndex3.ty"
		;
connectAttr "Character1_LeftHandIndex3_translateZ.o" "Character1_LeftHandIndex3.tz"
		;
connectAttr "Character1_LeftHandIndex3_scaleX.o" "Character1_LeftHandIndex3.sx";
connectAttr "Character1_LeftHandIndex3_scaleY.o" "Character1_LeftHandIndex3.sy";
connectAttr "Character1_LeftHandIndex3_scaleZ.o" "Character1_LeftHandIndex3.sz";
connectAttr "Character1_LeftHandIndex3_rotateX.o" "Character1_LeftHandIndex3.rx"
		;
connectAttr "Character1_LeftHandIndex3_rotateY.o" "Character1_LeftHandIndex3.ry"
		;
connectAttr "Character1_LeftHandIndex3_rotateZ.o" "Character1_LeftHandIndex3.rz"
		;
connectAttr "Character1_LeftHandIndex3_visibility.o" "Character1_LeftHandIndex3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandRing1.is";
connectAttr "Character1_LeftHandRing1_scaleX.o" "Character1_LeftHandRing1.sx";
connectAttr "Character1_LeftHandRing1_scaleY.o" "Character1_LeftHandRing1.sy";
connectAttr "Character1_LeftHandRing1_scaleZ.o" "Character1_LeftHandRing1.sz";
connectAttr "Character1_LeftHandRing1_translateX.o" "Character1_LeftHandRing1.tx"
		;
connectAttr "Character1_LeftHandRing1_translateY.o" "Character1_LeftHandRing1.ty"
		;
connectAttr "Character1_LeftHandRing1_translateZ.o" "Character1_LeftHandRing1.tz"
		;
connectAttr "Character1_LeftHandRing1_rotateX.o" "Character1_LeftHandRing1.rx";
connectAttr "Character1_LeftHandRing1_rotateY.o" "Character1_LeftHandRing1.ry";
connectAttr "Character1_LeftHandRing1_rotateZ.o" "Character1_LeftHandRing1.rz";
connectAttr "Character1_LeftHandRing1_visibility.o" "Character1_LeftHandRing1.v"
		;
connectAttr "Character1_LeftHandRing1.s" "Character1_LeftHandRing2.is";
connectAttr "Character1_LeftHandRing2_scaleX.o" "Character1_LeftHandRing2.sx";
connectAttr "Character1_LeftHandRing2_scaleY.o" "Character1_LeftHandRing2.sy";
connectAttr "Character1_LeftHandRing2_scaleZ.o" "Character1_LeftHandRing2.sz";
connectAttr "Character1_LeftHandRing2_translateX.o" "Character1_LeftHandRing2.tx"
		;
connectAttr "Character1_LeftHandRing2_translateY.o" "Character1_LeftHandRing2.ty"
		;
connectAttr "Character1_LeftHandRing2_translateZ.o" "Character1_LeftHandRing2.tz"
		;
connectAttr "Character1_LeftHandRing2_rotateX.o" "Character1_LeftHandRing2.rx";
connectAttr "Character1_LeftHandRing2_rotateY.o" "Character1_LeftHandRing2.ry";
connectAttr "Character1_LeftHandRing2_rotateZ.o" "Character1_LeftHandRing2.rz";
connectAttr "Character1_LeftHandRing2_visibility.o" "Character1_LeftHandRing2.v"
		;
connectAttr "Character1_LeftHandRing2.s" "Character1_LeftHandRing3.is";
connectAttr "Character1_LeftHandRing3_translateX.o" "Character1_LeftHandRing3.tx"
		;
connectAttr "Character1_LeftHandRing3_translateY.o" "Character1_LeftHandRing3.ty"
		;
connectAttr "Character1_LeftHandRing3_translateZ.o" "Character1_LeftHandRing3.tz"
		;
connectAttr "Character1_LeftHandRing3_scaleX.o" "Character1_LeftHandRing3.sx";
connectAttr "Character1_LeftHandRing3_scaleY.o" "Character1_LeftHandRing3.sy";
connectAttr "Character1_LeftHandRing3_scaleZ.o" "Character1_LeftHandRing3.sz";
connectAttr "Character1_LeftHandRing3_rotateX.o" "Character1_LeftHandRing3.rx";
connectAttr "Character1_LeftHandRing3_rotateY.o" "Character1_LeftHandRing3.ry";
connectAttr "Character1_LeftHandRing3_rotateZ.o" "Character1_LeftHandRing3.rz";
connectAttr "Character1_LeftHandRing3_visibility.o" "Character1_LeftHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_RightShoulder.is";
connectAttr "Character1_RightShoulder_scaleX.o" "Character1_RightShoulder.sx";
connectAttr "Character1_RightShoulder_scaleY.o" "Character1_RightShoulder.sy";
connectAttr "Character1_RightShoulder_scaleZ.o" "Character1_RightShoulder.sz";
connectAttr "Character1_RightShoulder_translateX.o" "Character1_RightShoulder.tx"
		;
connectAttr "Character1_RightShoulder_translateY.o" "Character1_RightShoulder.ty"
		;
connectAttr "Character1_RightShoulder_translateZ.o" "Character1_RightShoulder.tz"
		;
connectAttr "Character1_RightShoulder_rotateX.o" "Character1_RightShoulder.rx";
connectAttr "Character1_RightShoulder_rotateY.o" "Character1_RightShoulder.ry";
connectAttr "Character1_RightShoulder_rotateZ.o" "Character1_RightShoulder.rz";
connectAttr "Character1_RightShoulder_visibility.o" "Character1_RightShoulder.v"
		;
connectAttr "Character1_RightShoulder.s" "Character1_RightArm.is";
connectAttr "Character1_RightArm_scaleX.o" "Character1_RightArm.sx";
connectAttr "Character1_RightArm_scaleY.o" "Character1_RightArm.sy";
connectAttr "Character1_RightArm_scaleZ.o" "Character1_RightArm.sz";
connectAttr "Character1_RightArm_translateX.o" "Character1_RightArm.tx";
connectAttr "Character1_RightArm_translateY.o" "Character1_RightArm.ty";
connectAttr "Character1_RightArm_translateZ.o" "Character1_RightArm.tz";
connectAttr "Character1_RightArm_rotateX.o" "Character1_RightArm.rx";
connectAttr "Character1_RightArm_rotateY.o" "Character1_RightArm.ry";
connectAttr "Character1_RightArm_rotateZ.o" "Character1_RightArm.rz";
connectAttr "Character1_RightArm_visibility.o" "Character1_RightArm.v";
connectAttr "Character1_RightArm.s" "Character1_RightForeArm.is";
connectAttr "Character1_RightForeArm_scaleX.o" "Character1_RightForeArm.sx";
connectAttr "Character1_RightForeArm_scaleY.o" "Character1_RightForeArm.sy";
connectAttr "Character1_RightForeArm_scaleZ.o" "Character1_RightForeArm.sz";
connectAttr "Character1_RightForeArm_translateX.o" "Character1_RightForeArm.tx";
connectAttr "Character1_RightForeArm_translateY.o" "Character1_RightForeArm.ty";
connectAttr "Character1_RightForeArm_translateZ.o" "Character1_RightForeArm.tz";
connectAttr "Character1_RightForeArm_rotateX.o" "Character1_RightForeArm.rx";
connectAttr "Character1_RightForeArm_rotateY.o" "Character1_RightForeArm.ry";
connectAttr "Character1_RightForeArm_rotateZ.o" "Character1_RightForeArm.rz";
connectAttr "Character1_RightForeArm_visibility.o" "Character1_RightForeArm.v";
connectAttr "Character1_RightForeArm.s" "Character1_RightHand.is";
connectAttr "Character1_RightHand_scaleX.o" "Character1_RightHand.sx";
connectAttr "Character1_RightHand_scaleY.o" "Character1_RightHand.sy";
connectAttr "Character1_RightHand_scaleZ.o" "Character1_RightHand.sz";
connectAttr "Character1_RightHand_translateX.o" "Character1_RightHand.tx";
connectAttr "Character1_RightHand_translateY.o" "Character1_RightHand.ty";
connectAttr "Character1_RightHand_translateZ.o" "Character1_RightHand.tz";
connectAttr "Character1_RightHand_rotateX.o" "Character1_RightHand.rx";
connectAttr "Character1_RightHand_rotateY.o" "Character1_RightHand.ry";
connectAttr "Character1_RightHand_rotateZ.o" "Character1_RightHand.rz";
connectAttr "Character1_RightHand_visibility.o" "Character1_RightHand.v";
connectAttr "Character1_RightHand.s" "Character1_RightHandThumb1.is";
connectAttr "Character1_RightHandThumb1_scaleX.o" "Character1_RightHandThumb1.sx"
		;
connectAttr "Character1_RightHandThumb1_scaleY.o" "Character1_RightHandThumb1.sy"
		;
connectAttr "Character1_RightHandThumb1_scaleZ.o" "Character1_RightHandThumb1.sz"
		;
connectAttr "Character1_RightHandThumb1_translateX.o" "Character1_RightHandThumb1.tx"
		;
connectAttr "Character1_RightHandThumb1_translateY.o" "Character1_RightHandThumb1.ty"
		;
connectAttr "Character1_RightHandThumb1_translateZ.o" "Character1_RightHandThumb1.tz"
		;
connectAttr "Character1_RightHandThumb1_rotateX.o" "Character1_RightHandThumb1.rx"
		;
connectAttr "Character1_RightHandThumb1_rotateY.o" "Character1_RightHandThumb1.ry"
		;
connectAttr "Character1_RightHandThumb1_rotateZ.o" "Character1_RightHandThumb1.rz"
		;
connectAttr "Character1_RightHandThumb1_visibility.o" "Character1_RightHandThumb1.v"
		;
connectAttr "Character1_RightHandThumb1.s" "Character1_RightHandThumb2.is";
connectAttr "Character1_RightHandThumb2_scaleX.o" "Character1_RightHandThumb2.sx"
		;
connectAttr "Character1_RightHandThumb2_scaleY.o" "Character1_RightHandThumb2.sy"
		;
connectAttr "Character1_RightHandThumb2_scaleZ.o" "Character1_RightHandThumb2.sz"
		;
connectAttr "Character1_RightHandThumb2_translateX.o" "Character1_RightHandThumb2.tx"
		;
connectAttr "Character1_RightHandThumb2_translateY.o" "Character1_RightHandThumb2.ty"
		;
connectAttr "Character1_RightHandThumb2_translateZ.o" "Character1_RightHandThumb2.tz"
		;
connectAttr "Character1_RightHandThumb2_rotateX.o" "Character1_RightHandThumb2.rx"
		;
connectAttr "Character1_RightHandThumb2_rotateY.o" "Character1_RightHandThumb2.ry"
		;
connectAttr "Character1_RightHandThumb2_rotateZ.o" "Character1_RightHandThumb2.rz"
		;
connectAttr "Character1_RightHandThumb2_visibility.o" "Character1_RightHandThumb2.v"
		;
connectAttr "Character1_RightHandThumb2.s" "Character1_RightHandThumb3.is";
connectAttr "Character1_RightHandThumb3_translateX.o" "Character1_RightHandThumb3.tx"
		;
connectAttr "Character1_RightHandThumb3_translateY.o" "Character1_RightHandThumb3.ty"
		;
connectAttr "Character1_RightHandThumb3_translateZ.o" "Character1_RightHandThumb3.tz"
		;
connectAttr "Character1_RightHandThumb3_scaleX.o" "Character1_RightHandThumb3.sx"
		;
connectAttr "Character1_RightHandThumb3_scaleY.o" "Character1_RightHandThumb3.sy"
		;
connectAttr "Character1_RightHandThumb3_scaleZ.o" "Character1_RightHandThumb3.sz"
		;
connectAttr "Character1_RightHandThumb3_rotateX.o" "Character1_RightHandThumb3.rx"
		;
connectAttr "Character1_RightHandThumb3_rotateY.o" "Character1_RightHandThumb3.ry"
		;
connectAttr "Character1_RightHandThumb3_rotateZ.o" "Character1_RightHandThumb3.rz"
		;
connectAttr "Character1_RightHandThumb3_visibility.o" "Character1_RightHandThumb3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandIndex1.is";
connectAttr "Character1_RightHandIndex1_scaleX.o" "Character1_RightHandIndex1.sx"
		;
connectAttr "Character1_RightHandIndex1_scaleY.o" "Character1_RightHandIndex1.sy"
		;
connectAttr "Character1_RightHandIndex1_scaleZ.o" "Character1_RightHandIndex1.sz"
		;
connectAttr "Character1_RightHandIndex1_translateX.o" "Character1_RightHandIndex1.tx"
		;
connectAttr "Character1_RightHandIndex1_translateY.o" "Character1_RightHandIndex1.ty"
		;
connectAttr "Character1_RightHandIndex1_translateZ.o" "Character1_RightHandIndex1.tz"
		;
connectAttr "Character1_RightHandIndex1_rotateX.o" "Character1_RightHandIndex1.rx"
		;
connectAttr "Character1_RightHandIndex1_rotateY.o" "Character1_RightHandIndex1.ry"
		;
connectAttr "Character1_RightHandIndex1_rotateZ.o" "Character1_RightHandIndex1.rz"
		;
connectAttr "Character1_RightHandIndex1_visibility.o" "Character1_RightHandIndex1.v"
		;
connectAttr "Character1_RightHandIndex1.s" "Character1_RightHandIndex2.is";
connectAttr "Character1_RightHandIndex2_scaleX.o" "Character1_RightHandIndex2.sx"
		;
connectAttr "Character1_RightHandIndex2_scaleY.o" "Character1_RightHandIndex2.sy"
		;
connectAttr "Character1_RightHandIndex2_scaleZ.o" "Character1_RightHandIndex2.sz"
		;
connectAttr "Character1_RightHandIndex2_translateX.o" "Character1_RightHandIndex2.tx"
		;
connectAttr "Character1_RightHandIndex2_translateY.o" "Character1_RightHandIndex2.ty"
		;
connectAttr "Character1_RightHandIndex2_translateZ.o" "Character1_RightHandIndex2.tz"
		;
connectAttr "Character1_RightHandIndex2_rotateX.o" "Character1_RightHandIndex2.rx"
		;
connectAttr "Character1_RightHandIndex2_rotateY.o" "Character1_RightHandIndex2.ry"
		;
connectAttr "Character1_RightHandIndex2_rotateZ.o" "Character1_RightHandIndex2.rz"
		;
connectAttr "Character1_RightHandIndex2_visibility.o" "Character1_RightHandIndex2.v"
		;
connectAttr "Character1_RightHandIndex2.s" "Character1_RightHandIndex3.is";
connectAttr "Character1_RightHandIndex3_translateX.o" "Character1_RightHandIndex3.tx"
		;
connectAttr "Character1_RightHandIndex3_translateY.o" "Character1_RightHandIndex3.ty"
		;
connectAttr "Character1_RightHandIndex3_translateZ.o" "Character1_RightHandIndex3.tz"
		;
connectAttr "Character1_RightHandIndex3_scaleX.o" "Character1_RightHandIndex3.sx"
		;
connectAttr "Character1_RightHandIndex3_scaleY.o" "Character1_RightHandIndex3.sy"
		;
connectAttr "Character1_RightHandIndex3_scaleZ.o" "Character1_RightHandIndex3.sz"
		;
connectAttr "Character1_RightHandIndex3_rotateX.o" "Character1_RightHandIndex3.rx"
		;
connectAttr "Character1_RightHandIndex3_rotateY.o" "Character1_RightHandIndex3.ry"
		;
connectAttr "Character1_RightHandIndex3_rotateZ.o" "Character1_RightHandIndex3.rz"
		;
connectAttr "Character1_RightHandIndex3_visibility.o" "Character1_RightHandIndex3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandRing1.is";
connectAttr "Character1_RightHandRing1_scaleX.o" "Character1_RightHandRing1.sx";
connectAttr "Character1_RightHandRing1_scaleY.o" "Character1_RightHandRing1.sy";
connectAttr "Character1_RightHandRing1_scaleZ.o" "Character1_RightHandRing1.sz";
connectAttr "Character1_RightHandRing1_translateX.o" "Character1_RightHandRing1.tx"
		;
connectAttr "Character1_RightHandRing1_translateY.o" "Character1_RightHandRing1.ty"
		;
connectAttr "Character1_RightHandRing1_translateZ.o" "Character1_RightHandRing1.tz"
		;
connectAttr "Character1_RightHandRing1_rotateX.o" "Character1_RightHandRing1.rx"
		;
connectAttr "Character1_RightHandRing1_rotateY.o" "Character1_RightHandRing1.ry"
		;
connectAttr "Character1_RightHandRing1_rotateZ.o" "Character1_RightHandRing1.rz"
		;
connectAttr "Character1_RightHandRing1_visibility.o" "Character1_RightHandRing1.v"
		;
connectAttr "Character1_RightHandRing1.s" "Character1_RightHandRing2.is";
connectAttr "Character1_RightHandRing2_scaleX.o" "Character1_RightHandRing2.sx";
connectAttr "Character1_RightHandRing2_scaleY.o" "Character1_RightHandRing2.sy";
connectAttr "Character1_RightHandRing2_scaleZ.o" "Character1_RightHandRing2.sz";
connectAttr "Character1_RightHandRing2_translateX.o" "Character1_RightHandRing2.tx"
		;
connectAttr "Character1_RightHandRing2_translateY.o" "Character1_RightHandRing2.ty"
		;
connectAttr "Character1_RightHandRing2_translateZ.o" "Character1_RightHandRing2.tz"
		;
connectAttr "Character1_RightHandRing2_rotateX.o" "Character1_RightHandRing2.rx"
		;
connectAttr "Character1_RightHandRing2_rotateY.o" "Character1_RightHandRing2.ry"
		;
connectAttr "Character1_RightHandRing2_rotateZ.o" "Character1_RightHandRing2.rz"
		;
connectAttr "Character1_RightHandRing2_visibility.o" "Character1_RightHandRing2.v"
		;
connectAttr "Character1_RightHandRing2.s" "Character1_RightHandRing3.is";
connectAttr "Character1_RightHandRing3_translateX.o" "Character1_RightHandRing3.tx"
		;
connectAttr "Character1_RightHandRing3_translateY.o" "Character1_RightHandRing3.ty"
		;
connectAttr "Character1_RightHandRing3_translateZ.o" "Character1_RightHandRing3.tz"
		;
connectAttr "Character1_RightHandRing3_scaleX.o" "Character1_RightHandRing3.sx";
connectAttr "Character1_RightHandRing3_scaleY.o" "Character1_RightHandRing3.sy";
connectAttr "Character1_RightHandRing3_scaleZ.o" "Character1_RightHandRing3.sz";
connectAttr "Character1_RightHandRing3_rotateX.o" "Character1_RightHandRing3.rx"
		;
connectAttr "Character1_RightHandRing3_rotateY.o" "Character1_RightHandRing3.ry"
		;
connectAttr "Character1_RightHandRing3_rotateZ.o" "Character1_RightHandRing3.rz"
		;
connectAttr "Character1_RightHandRing3_visibility.o" "Character1_RightHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_Neck.is";
connectAttr "Character1_Neck_scaleX.o" "Character1_Neck.sx";
connectAttr "Character1_Neck_scaleY.o" "Character1_Neck.sy";
connectAttr "Character1_Neck_scaleZ.o" "Character1_Neck.sz";
connectAttr "Character1_Neck_translateX.o" "Character1_Neck.tx";
connectAttr "Character1_Neck_translateY.o" "Character1_Neck.ty";
connectAttr "Character1_Neck_translateZ.o" "Character1_Neck.tz";
connectAttr "Character1_Neck_rotateX.o" "Character1_Neck.rx";
connectAttr "Character1_Neck_rotateY.o" "Character1_Neck.ry";
connectAttr "Character1_Neck_rotateZ.o" "Character1_Neck.rz";
connectAttr "Character1_Neck_visibility.o" "Character1_Neck.v";
connectAttr "Character1_Neck.s" "Character1_Head.is";
connectAttr "Character1_Head_scaleX.o" "Character1_Head.sx";
connectAttr "Character1_Head_scaleY.o" "Character1_Head.sy";
connectAttr "Character1_Head_scaleZ.o" "Character1_Head.sz";
connectAttr "Character1_Head_translateX.o" "Character1_Head.tx";
connectAttr "Character1_Head_translateY.o" "Character1_Head.ty";
connectAttr "Character1_Head_translateZ.o" "Character1_Head.tz";
connectAttr "Character1_Head_rotateX.o" "Character1_Head.rx";
connectAttr "Character1_Head_rotateY.o" "Character1_Head.ry";
connectAttr "Character1_Head_rotateZ.o" "Character1_Head.rz";
connectAttr "Character1_Head_visibility.o" "Character1_Head.v";
connectAttr "Character1_Head.s" "eyes.is";
connectAttr "eyes_translateX.o" "eyes.tx";
connectAttr "eyes_translateY.o" "eyes.ty";
connectAttr "eyes_translateZ.o" "eyes.tz";
connectAttr "eyes_scaleX.o" "eyes.sx";
connectAttr "eyes_scaleY.o" "eyes.sy";
connectAttr "eyes_scaleZ.o" "eyes.sz";
connectAttr "eyes_rotateX.o" "eyes.rx";
connectAttr "eyes_rotateY.o" "eyes.ry";
connectAttr "eyes_rotateZ.o" "eyes.rz";
connectAttr "eyes_visibility.o" "eyes.v";
connectAttr "Character1_Head.s" "jaw.is";
connectAttr "jaw_lockInfluenceWeights.o" "jaw.liw";
connectAttr "jaw_translateX.o" "jaw.tx";
connectAttr "jaw_translateY.o" "jaw.ty";
connectAttr "jaw_translateZ.o" "jaw.tz";
connectAttr "jaw_scaleX.o" "jaw.sx";
connectAttr "jaw_scaleY.o" "jaw.sy";
connectAttr "jaw_scaleZ.o" "jaw.sz";
connectAttr "jaw_rotateX.o" "jaw.rx";
connectAttr "jaw_rotateY.o" "jaw.ry";
connectAttr "jaw_rotateZ.o" "jaw.rz";
connectAttr "jaw_visibility.o" "jaw.v";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Emeny@Archer_Laugh.ma
