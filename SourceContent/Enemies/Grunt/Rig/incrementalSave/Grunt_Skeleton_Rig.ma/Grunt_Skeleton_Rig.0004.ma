//Maya ASCII 2016 scene
//Name: Grunt_Skeleton_Rig.ma
//Last modified: Sat, Nov 21, 2015 11:45:39 AM
//Codeset: 1252
requires maya "2016";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "E612F34B-4031-3A31-1377-888F3DBFFC2D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 118.44069888421187 138.17439065057351 230.98241777639203 ;
	setAttr ".r" -type "double3" -15.338352729590365 22.599999999995717 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "B171B057-4BDD-F5CA-1235-5AA4698C632F";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 264.57400766398808;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "CF635E4C-4694-5FD3-9ECC-50945A4579CC";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "72493A34-4A0C-7DE1-6A78-81B76ACF56EC";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "CF2FED04-4C22-6C38-93AB-F4B497FF7DB9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.6548747699405579 68.518733738219964 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "88E01086-454E-0BE2-5EC3-37AB794EC97A";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 66.616692776272501;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "5F3084C0-4A52-AF50-A8E4-92BA39B9CBDA";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "9CF135E0-442C-C30D-5907-A59A1FC5A491";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "DoNotDeleteRigNodes";
	rename -uid "AE18D2D2-4D02-1644-7499-27B6C0DD6E47";
	addAttr -ci true -sn "version" -ln "version" -dt "string";
	addAttr -ci true -sn "rigNode" -ln "rigNode" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".version" -type "string" "3.0";
createNode transform -n "rigInfoNode" -p "DoNotDeleteRigNodes";
	rename -uid "A93A6115-43EA-A309-52A2-66B8DF0719B0";
	addAttr -ci true -sn "scaleInfo" -ln "scaleInfo" -at "double";
	addAttr -ci true -sn "bindJoints" -ln "bindJoints" -at "double";
	addAttr -ci true -sn "rigIfNode" -ln "rigIfNode" -at "double";
	addAttr -ci true -sn "rigName" -ln "rigName" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".scaleInfo" 4;
	setAttr ".rigName" -type "string" "";
createNode transform -n "distNodes" -p "DoNotDeleteRigNodes";
	rename -uid "FD7B727A-4F61-853C-4B41-AA96CF567581";
	addAttr -ci true -sn "distNode" -ln "distNode" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "fingerNodes" -p "DoNotDeleteRigNodes";
	rename -uid "FC872A3B-41E9-EF1B-DF49-D08A97B0D1E7";
	addAttr -ci true -sn "indexFinger" -ln "indexFinger" -at "double";
	addAttr -ci true -sn "middleFinger" -ln "middleFinger" -at "double";
	addAttr -ci true -sn "ringFinger" -ln "ringFinger" -at "double";
	addAttr -ci true -sn "pinkyFinger" -ln "pinkyFinger" -at "double";
	addAttr -ci true -sn "thumbFinger" -ln "thumbFinger" -at "double";
	addAttr -ci true -sn "FkHand" -ln "FkHand" -at "double";
	addAttr -ci true -sn "IkHand" -ln "IkHand" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".indexFinger" 1;
	setAttr ".pinkyFinger" 1;
	setAttr ".thumbFinger" 1;
createNode transform -n "selections" -p "DoNotDeleteRigNodes";
	rename -uid "D881F244-4324-A90B-6D2F-C8B81ADE8305";
	addAttr -ci true -sn "jntLeftRight" -ln "jntLeftRight" -at "double";
	addAttr -ci true -sn "skeleton" -ln "skeleton" -at "double";
	addAttr -ci true -sn "propCB" -ln "propCB" -at "double";
	addAttr -ci true -sn "autoOri" -ln "autoOri" -at "double";
	addAttr -ci true -sn "mirrorJnts" -ln "mirrorJnts" -at "double";
	addAttr -ci true -sn "quickRig" -ln "quickRig" -at "double";
	addAttr -ci true -sn "quickRigBind" -ln "quickRigBind" -at "double";
	addAttr -ci true -sn "import" -ln "import" -at "double";
	addAttr -ci true -sn "startRig" -ln "startRig" -at "double";
	addAttr -ci true -sn "connectJoints" -ln "connectJoints" -at "double";
	addAttr -ci true -sn "armRollUpNum" -ln "armRollUpNum" -at "double";
	addAttr -ci true -sn "legRollUpNum" -ln "legRollUpNum" -at "double";
	addAttr -ci true -sn "armRollDownNum" -ln "armRollDownNum" -at "double";
	addAttr -ci true -sn "legRollDownNum" -ln "legRollDownNum" -at "double";
	addAttr -ci true -sn "upArmRoll" -ln "upArmRoll" -at "double";
	addAttr -ci true -sn "downArmRoll" -ln "downArmRoll" -at "double";
	addAttr -ci true -sn "upLegRoll" -ln "upLegRoll" -at "double";
	addAttr -ci true -sn "downLegRoll" -ln "downLegRoll" -at "double";
	addAttr -ci true -sn "rollArmTB" -ln "rollArmTB" -at "double";
	addAttr -ci true -sn "rollLegTB" -ln "rollLegTB" -at "double";
	addAttr -ci true -sn "IkSpineCntl" -ln "IkSpineCntl" -at "double";
	addAttr -ci true -sn "IkSpine" -ln "IkSpine" -at "double";
	addAttr -ci true -sn "FkSpineCntl" -ln "FkSpineCntl" -at "double";
	addAttr -ci true -sn "FkSpine" -ln "FkSpine" -at "double";
	addAttr -ci true -sn "FkSpineCntlNum" -ln "FkSpineCntlNum" -at "double";
	addAttr -ci true -sn "stretchScale" -ln "stretchScale" -at "double";
	addAttr -ci true -sn "rootColor" -ln "rootColor" -at "double";
	addAttr -ci true -sn "hipsColor" -ln "hipsColor" -at "double";
	addAttr -ci true -sn "FkSpineColor" -ln "FkSpineColor" -at "double";
	addAttr -ci true -sn "shoulderColor" -ln "shoulderColor" -at "double";
	addAttr -ci true -sn "LegIkFkBoth" -ln "LegIkFkBoth" -at "double";
	addAttr -ci true -sn "legStretchy" -ln "legStretchy" -at "double";
	addAttr -ci true -sn "footCtrl" -ln "footCtrl" -at "double";
	addAttr -ci true -sn "legOriCtrl" -ln "legOriCtrl" -at "double";
	addAttr -ci true -sn "legPoiCtrl" -ln "legPoiCtrl" -at "double";
	addAttr -ci true -sn "createLegCtrl" -ln "createLegCtrl" -at "double";
	addAttr -ci true -sn "bindLegCtrl" -ln "bindLegCtrl" -at "double";
	addAttr -ci true -sn "legColorLeft" -ln "legColorLeft" -at "double";
	addAttr -ci true -sn "legColorRight" -ln "legColorRight" -at "double";
	addAttr -ci true -sn "armIkFkBoth" -ln "armIkFkBoth" -at "double";
	addAttr -ci true -sn "armStretchy" -ln "armStretchy" -at "double";
	addAttr -ci true -sn "armOriCtrl" -ln "armOriCtrl" -at "double";
	addAttr -ci true -sn "armPoiCtrl" -ln "armPoiCtrl" -at "double";
	addAttr -ci true -sn "clavOriCtrl" -ln "clavOriCtrl" -at "double";
	addAttr -ci true -sn "clavPoiCtrl" -ln "clavPoiCtrl" -at "double";
	addAttr -ci true -sn "createArmCtrl" -ln "createArmCtrl" -at "double";
	addAttr -ci true -sn "bindArmCtrl" -ln "bindArmCtrl" -at "double";
	addAttr -ci true -sn "armColorLeft" -ln "armColorLeft" -at "double";
	addAttr -ci true -sn "armColorRight" -ln "armColorRight" -at "double";
	addAttr -ci true -sn "handIKFKCBG" -ln "handIKFKCBG" -at "double";
	addAttr -ci true -sn "createHandCtrl" -ln "createHandCtrl" -at "double";
	addAttr -ci true -sn "bindHandCtrl" -ln "bindHandCtrl" -at "double";
	addAttr -ci true -sn "handColorLeft" -ln "handColorLeft" -at "double";
	addAttr -ci true -sn "handColorRight" -ln "handColorRight" -at "double";
	addAttr -ci true -sn "createHeadCtrl" -ln "createHeadCtrl" -at "double";
	addAttr -ci true -sn "bindHeadCtrl" -ln "bindHeadCtrl" -at "double";
	addAttr -ci true -sn "cEyesCtrl" -ln "cEyesCtrl" -at "double";
	addAttr -ci true -sn "cJawCtrl" -ln "cJawCtrl" -at "double";
	addAttr -ci true -sn "nColor" -ln "nColor" -at "double";
	addAttr -ci true -sn "nbColor" -ln "nbColor" -at "double";
	addAttr -ci true -sn "mEyeColor" -ln "mEyeColor" -at "double";
	addAttr -ci true -sn "lEyeColor" -ln "lEyeColor" -at "double";
	addAttr -ci true -sn "rEyeColor" -ln "rEyeColor" -at "double";
	addAttr -ci true -sn "jawColor" -ln "jawColor" -at "double";
	addAttr -ci true -sn "lookAtColor" -ln "lookAtColor" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".skeleton" 1;
	setAttr ".autoOri" 1;
	setAttr ".mirrorJnts" 1;
	setAttr ".startRig" 1;
	setAttr ".IkSpineCntl" 1;
	setAttr ".IkSpine" 1;
	setAttr ".FkSpineCntl" 1;
	setAttr ".FkSpine" 1;
	setAttr ".stretchScale" 1;
	setAttr ".rootColor" 7;
	setAttr ".hipsColor" 19;
	setAttr ".FkSpineColor" 18;
	setAttr ".shoulderColor" 19;
	setAttr ".legColorLeft" 15;
	setAttr ".legColorRight" 14;
	setAttr ".armColorLeft" 15;
	setAttr ".armColorRight" 14;
	setAttr ".handColorLeft" 15;
	setAttr ".handColorRight" 14;
	setAttr ".nColor" 18;
	setAttr ".nbColor" 10;
	setAttr ".mEyeColor" 7;
	setAttr ".lEyeColor" 15;
	setAttr ".rEyeColor" 14;
	setAttr ".jawColor" 14;
	setAttr ".lookAtColor" 18;
createNode transform -n "spineExtra" -p "DoNotDeleteRigNodes";
	rename -uid "8C355120-4AA3-ABD5-629D-6AB8B39212E4";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
createNode ikHandle -n "ikSplineSpine" -p "spineExtra";
	rename -uid "71A8F510-4F81-8CAC-77AA-B59F424D559B";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -9.4895678087103662e-009 80.082627244147901 -9.4321852715680148 ;
	setAttr ".r" -type "double3" 89.999999999999972 -9.6543514239123205 89.999999999999986 ;
	setAttr ".roc" yes;
	setAttr ".dwut" 4;
	setAttr ".dwuv" -type "double3" 0 0 1 ;
	setAttr ".dwve" -type "double3" 0 0 1 ;
	setAttr ".dtce" yes;
createNode transform -n "spineCurve_ik" -p "spineExtra";
	rename -uid "05F2625B-4532-A962-26F3-C592AE7A90C2";
	setAttr ".v" no;
createNode nurbsCurve -n "spineCurve_ikShape" -p "spineCurve_ik";
	rename -uid "B4E9B286-4AF0-0BD7-A6FD-11915D8A3677";
	setAttr -k off ".v";
	setAttr -s 14 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "spineCurve_ikShapeOrig" -p "spineCurve_ik";
	rename -uid "AA5B2FAF-4B75-41B5-9844-F0932A2C053A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		3 3 0 no 3
		8 0 0 0 7.8554035766591257 15.710807153318251 23.566210729977378 23.566210729977378
		 23.566210729977378
		6
		2.7325893232234109e-015 56.649088358017032 -10.287862630020491
		-2.9196095260608423e-008 59.275004000646554 -10.329757386832791
		-7.952638228755677e-008 64.496062648911931 -10.479176482336932
		2.8470594588902456e-008 72.363191810609877 -11.277284826066461
		1.2171741546379702e-009 77.552392796654573 -10.01770418735598
		-9.4895616142039899e-009 80.082359181456326 -9.4322473036750498
		;
createNode transform -n "spineCurve_ik_CV_1_point_X" -p "spineExtra";
	rename -uid "348880BC-4BC1-E87F-92A6-37B81A2BF8F8";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr -k on ".blendSpace" 0.11199891595825476;
createNode transform -n "spineCurve_ik_CV_1_point_Y" -p "spineCurve_ik_CV_1_point_X";
	rename -uid "2C3903C1-4731-9173-4D2D-56B2B3A68624";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr -k on ".blendSpace" 0.11199891595825476;
createNode transform -n "spineCurve_ik_CV_1_point_Z" -p "spineCurve_ik_CV_1_point_Y";
	rename -uid "B0689558-40A6-7E33-9BE5-9EBE1A2D2069";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr -k on ".blendSpace" 0.11199891595825476;
createNode transform -n "spineCurve_ik_CV_1" -p "spineCurve_ik_CV_1_point_Z";
	rename -uid "26322687-4FFA-1F75-6B5B-8285ED7D4220";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
createNode clusterHandle -n "clusterHandleShape" -p "spineCurve_ik_CV_1";
	rename -uid "D6806102-43A6-485B-2432-B39874982D1B";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".io" yes;
createNode pointConstraint -n "spineCurve_ik_CV_1_point_Z_pointConstraint1" -p "spineCurve_ik_CV_1_point_Z";
	rename -uid "A4FC918B-4BC6-033E-150E-38918C53846A";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_1_L_point_blend_ZW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_1_point_blend_ZW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".o" -type "double3" 3.3087224502121107e-024 -7.1054273576010019e-015 0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "spineCurve_ik_CV_1_point_Y_pointConstraint1" -p "spineCurve_ik_CV_1_point_Y";
	rename -uid "8FD4D5BA-4E2D-2663-36DB-D6A61A8C044F";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_1_L_point_blend_YW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_1_point_blend_YW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".o" -type "double3" 3.3087224502121107e-024 0 0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "spineCurve_ik_CV_1_point_X_pointConstraint1" -p "spineCurve_ik_CV_1_point_X";
	rename -uid "A2344CE4-40E2-C986-5EAF-269AB1595076";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_1_L_point_blend_XW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_1_point_blend_XW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "spineCurve_ik_CV_2_point_X" -p "spineExtra";
	rename -uid "0D1CE594-4963-39AC-D4A2-75A02F5B50A8";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr -k on ".blendSpace" 0.3347411081692811;
createNode transform -n "spineCurve_ik_CV_2_point_Y" -p "spineCurve_ik_CV_2_point_X";
	rename -uid "F3E178E6-4CCF-D69F-0954-74A1334217C1";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr -k on ".blendSpace" 0.3347411081692811;
createNode transform -n "spineCurve_ik_CV_2_point_Z" -p "spineCurve_ik_CV_2_point_Y";
	rename -uid "50A26292-4985-828E-81A4-3E8D22704463";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr -k on ".blendSpace" 0.3347411081692811;
createNode transform -n "spineCurve_ik_CV_2" -p "spineCurve_ik_CV_2_point_Z";
	rename -uid "34374EE8-44F4-4EBC-997A-BBA0600F25F7";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
createNode clusterHandle -n "clusterHandleShape" -p "spineCurve_ik_CV_2";
	rename -uid "E81C368F-4850-A2C9-4515-BCA1E4A4C39A";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".io" yes;
createNode pointConstraint -n "spineCurve_ik_CV_2_point_Z_pointConstraint1" -p "spineCurve_ik_CV_2_point_Z";
	rename -uid "029226A0-485D-A100-4EA3-2DA523BF994F";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_2_L_point_blend_ZW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_2_point_blend_ZW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "spineCurve_ik_CV_2_point_Y_pointConstraint1" -p "spineCurve_ik_CV_2_point_Y";
	rename -uid "6ECCAE34-4580-3CAF-3282-3681A91DCB9B";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_2_L_point_blend_YW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_2_point_blend_YW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "spineCurve_ik_CV_2_point_X_pointConstraint1" -p "spineCurve_ik_CV_2_point_X";
	rename -uid "5E6ECA6A-4921-B716-B4D9-B688FAA9BBA8";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_2_L_point_blend_XW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_2_point_blend_XW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "spineCurve_ik_CV_3_point_X" -p "spineExtra";
	rename -uid "8F22AA60-45EB-5D62-5D77-0FBAAE09ADF0";
	addAttr -ci true -sn "midCvX" -ln "midCvX" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr -k on ".blendSpace" 0.67146991751525498;
createNode transform -n "spineCurve_ik_CV_3_point_Y" -p "spineCurve_ik_CV_3_point_X";
	rename -uid "7802CA22-4181-DB7F-D42A-7DB3EA40B7BD";
	addAttr -ci true -sn "midCvY" -ln "midCvY" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr -k on ".blendSpace" 0.67146991751525498;
createNode transform -n "spineCurve_ik_CV_3_point_Z" -p "spineCurve_ik_CV_3_point_Y";
	rename -uid "06B0F85E-4364-CB92-D316-9587AE16F6B9";
	addAttr -ci true -sn "midCvZ" -ln "midCvZ" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr -k on ".blendSpace" 0.67146991751525498;
createNode transform -n "spineCurve_ik_CV_3" -p "spineCurve_ik_CV_3_point_Z";
	rename -uid "A8979438-4164-C3F7-B4F8-19AFBF813CD4";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
createNode clusterHandle -n "clusterHandleShape" -p "spineCurve_ik_CV_3";
	rename -uid "DF2E5502-4EEA-5AC6-FA23-CF87CDBBDAD5";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".io" yes;
createNode pointConstraint -n "spineCurve_ik_CV_3_point_Z_pointConstraint1" -p "spineCurve_ik_CV_3_point_Z";
	rename -uid "EBEC7A93-4CC9-0582-C038-B2BF99B932B4";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_3_L_point_blend_ZW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_3_point_blend_ZW1" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w2" -ln "mid_ik_ctrlW2" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 3 ".tg";
	setAttr ".o" -type "double3" 1.4235293885683461e-008 1.9948302119741896 -0.49471110032805754 ;
	setAttr ".rst" -type "double3" 0 0 -5.3290705182007514e-015 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
	setAttr -k on ".w2";
createNode pointConstraint -n "spineCurve_ik_CV_3_point_Y_pointConstraint1" -p "spineCurve_ik_CV_3_point_Y";
	rename -uid "74AE9FBD-4428-A502-5AF8-EB950AC775C9";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_3_L_point_blend_YW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_3_point_blend_YW1" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w2" -ln "mid_ik_ctrlW2" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 3 ".tg";
	setAttr ".o" -type "double3" 1.4235293885683461e-008 1.9948302119741896 -0.49471110032805221 ;
	setAttr ".rst" -type "double3" 0 -1.4210854715202004e-014 0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
	setAttr -k on ".w2";
createNode pointConstraint -n "spineCurve_ik_CV_3_point_X_pointConstraint1" -p "spineCurve_ik_CV_3_point_X";
	rename -uid "4EDF297D-4C97-2DF4-EC8F-44BC7B368DE5";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_3_L_point_blend_XW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_3_point_blend_XW1" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w2" -ln "mid_ik_ctrlW2" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 3 ".tg";
	setAttr ".o" -type "double3" 1.4235293885683461e-008 1.9948302119742038 -0.49471110032805221 ;
	setAttr ".rst" -type "double3" -4.2176028018206618e-015 0 0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
	setAttr -k on ".w2";
createNode transform -n "spineCurve_ik_CV_4_point_X" -p "spineExtra";
	rename -uid "8E67D5B7-49D4-8654-0B11-0BBAA8EFE6E7";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr -k on ".blendSpace" 0.89151570237095445;
createNode transform -n "spineCurve_ik_CV_4_point_Y" -p "spineCurve_ik_CV_4_point_X";
	rename -uid "2DB6EC7A-48E1-CEED-855E-12817396B8BB";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr -k on ".blendSpace" 0.89151570237095445;
createNode transform -n "spineCurve_ik_CV_4_point_Z" -p "spineCurve_ik_CV_4_point_Y";
	rename -uid "C67E0E17-4923-E36C-4A50-8BBEBB8E2ABF";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr -k on ".blendSpace" 0.89151570237095445;
createNode transform -n "spineCurve_ik_CV_4" -p "spineCurve_ik_CV_4_point_Z";
	rename -uid "23622064-4F24-B5BD-A1A9-1A80E8A5B337";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
createNode clusterHandle -n "clusterHandleShape" -p "spineCurve_ik_CV_4";
	rename -uid "FE6181AF-4DF2-3E19-651C-F2B38E1C4A42";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".io" yes;
createNode pointConstraint -n "spineCurve_ik_CV_4_point_Z_pointConstraint1" -p "spineCurve_ik_CV_4_point_Z";
	rename -uid "1FC62D1C-489A-BB26-04B3-2B9F01213266";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_4_L_point_blend_ZW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_4_point_blend_ZW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".o" -type "double3" 0 -1.4210854715202004e-014 0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "spineCurve_ik_CV_4_point_Y_pointConstraint1" -p "spineCurve_ik_CV_4_point_Y";
	rename -uid "2E6609EA-43D8-2BE0-84CF-D58FFF1804C5";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_4_L_point_blend_YW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_4_point_blend_YW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "spineCurve_ik_CV_4_point_X_pointConstraint1" -p "spineCurve_ik_CV_4_point_X";
	rename -uid "768AC2A4-4922-3834-44E7-FCBFEED208D1";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_4_L_point_blend_XW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_4_point_blend_XW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "njc_top_twistCtrl" -p "spineExtra";
	rename -uid "AF5BA9DE-4C55-5EE8-29D0-3190804A54EA";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
createNode orientConstraint -n "njc_top_twistCtrl_orientConstraint1" -p "njc_top_twistCtrl";
	rename -uid "480F8A6C-48E8-681A-5106-C69E5847E210";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_5W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "njc_top_twistCtrl_pointConstraint1" -p "njc_top_twistCtrl";
	rename -uid "561D31CB-45E8-F16F-F881-89A46D8106BC";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_5W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.2 0.00026806269139001415 6.2032108798959484e-005 ;
	setAttr ".rst" -type "double3" 0.1999999905104384 80.082627244147716 -9.4321852715662509 ;
	setAttr -k on ".w0";
createNode transform -n "njc_bottom_twistCtrl" -p "spineExtra";
	rename -uid "A344421F-4E23-AB67-F61E-E5AB53C07712";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
createNode orientConstraint -n "njc_bottom_twistCtrl_orientConstraint1" -p "njc_bottom_twistCtrl";
	rename -uid "0F44D2AD-4608-730C-5D40-38BCB4024E54";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_0W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -6.3611093629270391e-015 -3.1805546814635168e-014 
		2.2263882770244621e-014 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "njc_bottom_twistCtrl_pointConstraint1" -p "njc_bottom_twistCtrl";
	rename -uid "BEAB7DEF-4888-DB2B-DDB6-209840862829";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_0W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.2 0 0 ;
	setAttr ".rst" -type "double3" 0.20000000000000273 56.649088358017032 -10.287862630020491 ;
	setAttr -k on ".w0";
createNode transform -n "worldPlacement";
	rename -uid "6CD59B42-4DB9-7244-F8F8-A6B8C299B5B0";
	addAttr -ci true -sn "worldPlacement" -ln "worldPlacement" -at "double";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "ikControl" -ln "ikControl" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "midIkCtrl" -ln "midIkCtrl" -dv 1 -min 0 -max 1 -at "bool";
	setAttr -l on -cb on ".ikControl";
	setAttr -k on ".midIkCtrl" no;
createNode nurbsCurve -n "worldPlacementShape" -p "worldPlacement";
	rename -uid "8C065D46-402C-0845-8ACE-999553E5F189";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 29;
	setAttr ".cc" -type "nurbsCurve" 
		1 17 0 no 3
		18 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
		18
		-53.552 1.46 53.552
		-53.552 -0.0054799999999999996 53.552
		-53.552 1.46 53.552
		-53.552 1.46 -53.552
		-53.552 -0.0054799999999999996 -53.552
		-53.552 1.46 -53.552
		53.552 1.46 -53.552
		53.552 -0.0054799999999999996 -53.552
		53.552 1.46 -53.552
		53.552 1.46 53.552
		53.552 -0.0054799999999999996 53.552
		53.552 1.46 53.552
		-53.552 1.46 53.552
		-53.552 -0.0054799999999999996 53.552
		-53.552 -0.0054799999999999996 -53.552
		53.552 -0.0054799999999999996 -53.552
		53.552 -0.0054799999999999996 53.552
		-53.552 -0.0054799999999999996 53.552
		;
createNode transform -n "rig_skeleton" -p "worldPlacement";
	rename -uid "C7A47E51-485D-983C-4284-C1A6C3CAD63D";
	addAttr -ci true -sn "rigSkelGrp" -ln "rigSkelGrp" -at "double";
createNode joint -n "pelvis_rig" -p "rig_skeleton";
	rename -uid "7EF06A22-44B5-1C1C-5FA1-ED96BCA05770";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "pelvisJnt" -ln "pelvisJnt" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999972 6.8217670768916099 89.999999999999972 ;
createNode joint -n "spine_01_rig" -p "pelvis_rig";
	rename -uid "27AC36FD-41C8-9280-448E-D5A07A3CF56C";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "spineJnt01" -ln "spineJnt01" -at "double";
	setAttr ".t" -type "double3" 8.2043254941233741 -1.5987211554602254e-014 -7.1054273576009924e-015 ;
	setAttr ".r" -type "double3" -6.1773880229510144e-010 -8.8888362821682857e-009 -0.085665009747192503 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 4.9955644642355752 ;
createNode joint -n "spine_02_rig" -p "spine_01_rig";
	rename -uid "079E15EC-48CB-7F50-7CD2-00AE99A98FA6";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "spineJnt02" -ln "spineJnt02" -at "double";
	setAttr ".t" -type "double3" 6.959034 8.8817841970012523e-015 -5.0722078972747858e-008 ;
	setAttr ".r" -type "double3" -1.0306248822165447e-009 7.2834948894749873e-009 0.1651291957535439 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -0.73309151890089952 ;
createNode joint -n "spine_03_rig" -p "spine_02_rig";
	rename -uid "4A3A9AF0-4D96-3321-6D19-E8814C324183";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "spineJnt03" -ln "spineJnt03" -at "double";
	setAttr ".t" -type "double3" 8.04489 1.4210854715202004e-014 5.0722070770598949e-008 ;
	setAttr ".r" -type "double3" -9.3512724618951262e-011 3.8861602952903556e-010 -0.084117675498460318 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 12.218299044961364 ;
createNode joint -n "spineEnd_rig" -p "spine_03_rig";
	rename -uid "6BF9CAB3-4F42-AB00-4462-01A5234DF729";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "spineEndJnt" -ln "spineEndJnt" -at "double";
	setAttr ".t" -type "double3" 8.562287 0 -9.4895642139149193e-009 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -6.6853931696465008 ;
createNode ikEffector -n "effector1" -p "spine_03_rig";
	rename -uid "BED6148D-44CB-EBD4-53DB-95A46C3BB4A6";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "hips_rig" -p "pelvis_rig";
	rename -uid "50EA7EE3-416C-F5E7-4905-FAB86F41413E";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "hipsJnt" -ln "hipsJnt" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "l_hip_rig" -p "hips_rig";
	rename -uid "80F54011-49BA-3A9F-BF77-9DA28A54FE74";
	addAttr -ci true -sn "l_hipJnt" -ln "l_hipJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -1.3658201354747561 2.6076276746516465 10.901285582461378 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 -1.4574876822744875e-005 178.64906316091094 ;
createNode joint -n "l_knee_rig" -p "l_hip_rig";
	rename -uid "8590FFB3-49CE-0E6C-0154-34AE1537DF4A";
	addAttr -ci true -sn "l_kneeJnt" -ln "l_kneeJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 22.362855289533638 1.7763568394002505e-015 -1.0103029524088925e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.9520412118295752e-022 13.792099994027071 -8.4503093863587517e-006 ;
createNode joint -n "l_ankle_rig" -p "l_knee_rig";
	rename -uid "129A9D71-48BB-5FF9-1AC8-7FAF85FBFFD6";
	addAttr -ci true -sn "l_ankleJnt" -ln "l_ankleJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 19.015319625082238 1.7763568394002505e-015 -1.3322676295501878e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.4410750513976186e-005 -59.41696995786635 7.6807871935862337e-005 ;
createNode joint -n "l_ball_rig" -p "l_ankle_rig";
	rename -uid "BFAFA579-4223-45B2-3C82-B3A9BC8FACE2";
	addAttr -ci true -sn "l_ballJnt" -ln "l_ballJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 10.493119080456312 -9.2270548535111629e-008 8.1712414612411521e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.029246346649313214 -36.202415693425529 0.049437522681953873 ;
createNode joint -n "l_toe_rig" -p "l_ball_rig";
	rename -uid "C0FE9DEB-4ABC-A8B2-F6D7-2DB97AE5A48C";
	addAttr -ci true -sn "l_toeJnt" -ln "l_toeJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 9.6354058851631805 -1.4641726941988509e-007 -1.0056594446083977e-011 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "r_hip_rig" -p "hips_rig";
	rename -uid "984D5CA4-4908-C100-29F2-9B81BDDCCD11";
	addAttr -ci true -sn "r_hipJnt" -ln "r_hipJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -1.3658523436602081 2.6076262856699852 -10.901299999999997 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000043 -1.4574876706754021e-005 -1.3509368390890217 ;
createNode joint -n "r_knee_rig" -p "r_hip_rig";
	rename -uid "1136D77B-4259-A22C-6C30-32A82107BBCB";
	addAttr -ci true -sn "r_kneeJnt" -ln "r_kneeJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -22.362815337331 5.6886437356240549e-006 -7.3233511266312945e-007 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 13.792099994027033 -8.4503093429707239e-006 ;
createNode joint -n "r_ankle_rig" -p "r_knee_rig";
	rename -uid "144D2B08-4946-CC10-3384-F89413BAAE98";
	addAttr -ci true -sn "r_ankleJnt" -ln "r_ankleJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -19.015331074775951 1.9740161878445406e-006 -7.5268428236796581e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.6130247516216962e-005 -59.416969957865142 7.8288174523231892e-005 ;
createNode joint -n "r_ball_rig" -p "r_ankle_rig";
	rename -uid "F7A249F7-45D0-8455-B9F0-D387FA5C95BD";
	addAttr -ci true -sn "r_ballJnt" -ln "r_ballJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -10.493109997995461 8.0792280456876142e-006 1.5153519754562694e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.029245795042113217 -36.202415693810153 0.049436443720274337 ;
createNode joint -n "r_toe_rig" -p "r_ball_rig";
	rename -uid "CB57F307-4082-957C-B01B-52B99B65E87C";
	addAttr -ci true -sn "r_toeJnt" -ln "r_toeJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -9.6354123294207881 1.206574880718847e-005 -3.8492716958327122e-008 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "spineCurve_ik_CV_0" -p "hips_rig";
	rename -uid "86283CBB-48A9-46C8-6814-638B1446C179";
	setAttr -k off -cb on ".v";
	setAttr ".t" -type "double3" -49.265717644959388 3.4861828524274419 2.4007115880596387e-014 ;
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr ".r" -type "double3" -83.178232923108396 -89.999999999999957 0 ;
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.7325893232234109e-015 56.649088358017018 -10.28786263002049 ;
	setAttr ".rpt" -type "double3" 57.470043139082755 -60.135271210444479 10.287862630020458 ;
	setAttr ".sp" -type "double3" 2.7325893232234109e-015 56.649088358017032 -10.287862630020491 ;
	setAttr ".spt" -type "double3" 0 -1.4210854715202002e-014 1.7763568394002503e-015 ;
createNode clusterHandle -n "clusterHandleShape" -p "spineCurve_ik_CV_0";
	rename -uid "ABA0C4BA-4FF6-8A84-1FD3-559833A39B38";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".io" yes;
createNode transform -n "spineCurve_ik_CV_1_point_blend_X" -p "spineCurve_ik_CV_0";
	rename -uid "AEB429A2-47E8-95BF-15C7-27AFE5C4FEEB";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
createNode transform -n "spineCurve_ik_CV_1_point_blend_Y" -p "spineCurve_ik_CV_0";
	rename -uid "866F49A2-4454-7D01-5D45-B7A9C03A04F6";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
createNode transform -n "spineCurve_ik_CV_1_point_blend_Z" -p "spineCurve_ik_CV_0";
	rename -uid "18AA9085-4668-89AC-3BF1-B5965E5A12BA";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
createNode transform -n "spineCurve_ik_CV_2_point_blend_X" -p "spineCurve_ik_CV_0";
	rename -uid "482E8C00-4542-E451-B536-6FB48149998E";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
createNode transform -n "spineCurve_ik_CV_2_point_blend_Y" -p "spineCurve_ik_CV_0";
	rename -uid "A2B016AD-4856-8E83-D3E5-549812AFF488";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
createNode transform -n "spineCurve_ik_CV_2_point_blend_Z" -p "spineCurve_ik_CV_0";
	rename -uid "A1DF67C6-4151-FB0D-2B0A-AC85C9A63960";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
createNode transform -n "spineCurve_ik_CV_3_point_blend_X" -p "spineCurve_ik_CV_0";
	rename -uid "F91A072A-4F7D-AB16-7B2A-30BEA0C51092";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
createNode transform -n "spineCurve_ik_CV_3_point_blend_Y" -p "spineCurve_ik_CV_0";
	rename -uid "333E8E64-4881-197C-37CE-6683BA54A8C8";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
createNode transform -n "spineCurve_ik_CV_3_point_blend_Z" -p "spineCurve_ik_CV_0";
	rename -uid "D57838B1-49D7-4973-899A-ABB98A10F821";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
createNode transform -n "spineCurve_ik_CV_4_point_blend_X" -p "spineCurve_ik_CV_0";
	rename -uid "878EC40E-4CE4-8E74-831A-9994A9C6BED3";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
createNode transform -n "spineCurve_ik_CV_4_point_blend_Y" -p "spineCurve_ik_CV_0";
	rename -uid "A5B07D66-450A-2764-4C6F-7E9B2A26428C";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
createNode transform -n "spineCurve_ik_CV_4_point_blend_Z" -p "spineCurve_ik_CV_0";
	rename -uid "F3AF978A-4431-3B58-C1FD-228660DD6F65";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
createNode orientConstraint -n "hips_rig_orientConstraint1" -p "hips_rig";
	rename -uid "8BF4A58B-4D3E-C2B3-B244-92A077F559EB";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "hips_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -83.178232923108396 -89.999999999999957 0 ;
	setAttr ".o" -type "double3" 89.999999999999986 6.8217670768915966 89.999999999999957 ;
	setAttr ".rsrr" -type "double3" -1.5902773407317584e-014 3.1805546814635176e-015 
		6.3611093629270327e-015 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "hips_rig_pointConstraint1" -p "hips_rig";
	rename -uid "A8EFA7B0-4DF5-9C56-79FC-6BA99690BE9D";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "hips_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -7.1054273576010019e-015 -2.6645352591003757e-015 -3.1554436208840472e-030 ;
	setAttr ".rst" -type "double3" 1.4210854715202004e-014 4.4408920985006262e-015 3.1554436208840472e-030 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "pelvis_rig_pointConstraint1" -p "pelvis_rig";
	rename -uid "5E49040A-4C46-D1C9-2287-AAAB6B7CDD85";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "pelvis_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 48.502845764160156 -9.3133430480957031 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "pelvis_rig_orientConstraint1" -p "pelvis_rig";
	rename -uid "A2F4C59C-4412-2B33-C181-76BAB521FF49";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "pelvis_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -83.178232923108396 -89.999999999999957 0 ;
	setAttr ".o" -type "double3" 89.999999999999986 6.8217670768915966 89.999999999999957 ;
	setAttr ".rsrr" -type "double3" -1.5902773407317584e-014 3.1805546814635176e-015 
		6.3611093629270327e-015 ;
	setAttr -k on ".w0";
createNode joint -n "spineParent_rig" -p "rig_skeleton";
	rename -uid "D63A4000-4463-C886-1BC1-C1975D8AD4BD";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "spineParentJnt" -ln "spineParentJnt" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999844 -2.9736117437579219 90.000000000000071 ;
createNode joint -n "neckBase_rig" -p "spineParent_rig";
	rename -uid "CE157D7D-4D46-8923-CC1A-9FBED454431E";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "neckBaseJnt" -ln "neckBaseJnt" -at "double";
	setAttr ".t" -type "double3" 16.582691502051858 -2.1316282072803006e-014 -3.7922176028118758e-008 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 20.55278972913904 ;
createNode joint -n "neck_mid_rig" -p "neckBase_rig";
	rename -uid "2046E3BB-4126-3C6B-C562-4D961D5DA5DA";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "neckMidJnt" -ln "neckMidJnt" -at "double";
	setAttr ".t" -type "double3" 19.769148705961925 -2.1316282072803006e-014 -6.3548436880882648e-008 ;
	setAttr ".r" -type "double3" 1.2358937634540444e-030 -4.4527765540489235e-014 -3.1805546814635168e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -113.52640147289696 -89.999999999999829 0 ;
createNode joint -n "neck_rig" -p "neck_mid_rig";
	rename -uid "314AD6D2-446D-DCD9-8B95-2F9571CB32B2";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "neckJnt" -ln "neckJnt" -at "double";
	setAttr ".t" -type "double3" 0 -2.8421709430404007e-014 0 ;
	setAttr ".r" -type "double3" -3.1805546814639368e-015 -2.2263882770244617e-013 2.1627771833951915e-013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 6.3611093629265792e-015 2.2504626351166665e-013 -2.2479385798591407e-013 ;
createNode joint -n "l_eye_rig" -p "neck_rig";
	rename -uid "49C7CA66-4B43-7720-E8BA-6BB6B80017AB";
	addAttr -ci true -sn "l_eyeJnt" -ln "l_eyeJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 3.2760000000000242 4.4439999999999458 5.8279999999999781 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 6.3611093629265792e-015 2.2504626351166665e-013 -2.2479385798591407e-013 ;
createNode joint -n "r_eye_rig" -p "neck_rig";
	rename -uid "05D75E52-45DA-C763-D50E-A1BB67026445";
	addAttr -ci true -sn "r_eyeJnt" -ln "r_eyeJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -3.2759998890397188 4.4444147044847995 5.8280005273533195 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 180 0 0 ;
createNode joint -n "jaw_rig" -p "neck_rig";
	rename -uid "FBFD8F61-4AC2-FBE3-765A-2A9B748A86D5";
	addAttr -ci true -sn "jawJnt" -ln "jawJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 1.0000001116525112e-006 -6.0659707934836149 12.622518224451861 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 6.3611093629265792e-015 2.2504626351166665e-013 -2.2479385798591407e-013 ;
createNode joint -n "l_clav_rig" -p "spineParent_rig";
	rename -uid "85C18C1D-4760-3726-2D95-DD9797C96506";
	addAttr -ci true -sn "l_clavJnt" -ln "l_clavJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 11.62216417704029 1.578078361225792 7.0000007956332064 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.0052494338314551039 -83.041026263282177 -92.968323351189696 ;
createNode joint -n "l_shoulder_rig" -p "l_clav_rig";
	rename -uid "6548D43D-48FE-FF4E-F790-8C8F9F01C063";
	addAttr -ci true -sn "l_shoulderJnt" -ln "l_shoulderJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 12.265966789427523 -1.4210854715202004e-014 -7.9936057773011271e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4765013140818527e-005 -9.7940192453054067 -0.0006391982343617763 ;
createNode joint -n "l_elbow_rig" -p "l_shoulder_rig";
	rename -uid "543804BF-4B13-86C6-BE77-9BA3272B3A33";
	addAttr -ci true -sn "l_elbowJnt" -ln "l_elbowJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 18.977118289932545 7.1054273576010019e-014 -1.2434497875801753e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.2708081109434524e-006 -12.235272407050054 1.1856709489641087e-005 ;
createNode joint -n "l_wrist_rig" -p "l_elbow_rig";
	rename -uid "77792121-4F93-7EFE-962C-2B985FD8E268";
	addAttr -ci true -sn "l_wristJnt" -ln "l_wristJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 17.154433690736077 0 1.4210854715202004e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "l_index_01_rig" -p "l_wrist_rig";
	rename -uid "BFA9A763-4292-87B6-6809-75B45F5BC693";
	addAttr -ci true -sn "l_indexJnt01" -ln "l_indexJnt01" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 10.392254512342582 1.9517022060271927 4.6647774069106287 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.327136464344079 6.7733761696721375 -2.5614952482332125 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_index_02_rig" -p "l_index_01_rig";
	rename -uid "A713F6C3-4302-4ED9-5BA4-AC91CDFB6238";
	addAttr -ci true -sn "l_indexJnt02" -ln "l_indexJnt02" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 6.7088787484487611 0 -7.1054273576010019e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.035869761174582 -15.470899537925916 -0.69135407776869517 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_index_03_rig" -p "l_index_02_rig";
	rename -uid "C8164EEA-43D8-48ED-2894-EF84441468ED";
	addAttr -ci true -sn "l_indexJnt03" -ln "l_indexJnt03" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 6.332299490449941 8.9338259012805565e-016 4.2632564145606011e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.3459905161050147e-014 1.0205866691719738e-014 -1.590277340731758e-015 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_pinky_01_rig" -p "l_wrist_rig";
	rename -uid "9E8A9D92-4666-1BC1-EC76-2DACE4BF5606";
	addAttr -ci true -sn "l_pinkyJnt01" -ln "l_pinkyJnt01" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 8.9227837627418651 -0.54561132659469536 -2.522599826637812 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 88.631908137195964 10.807330603990408 -5.2543246032494437 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_pinky_02_rig" -p "l_pinky_01_rig";
	rename -uid "75AFE274-4496-4B2B-8ABB-A1AA7C6BD254";
	addAttr -ci true -sn "l_pinkyJnt02" -ln "l_pinkyJnt02" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 5.5703760322813665 1.7763568394002505e-015 -1.4210854715202004e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.4452932773809029 -10.922608651197676 3.3832416023724741 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_pinky_03_rig" -p "l_pinky_02_rig";
	rename -uid "3DC43632-4879-2180-A94B-F294D2FE06C7";
	addAttr -ci true -sn "l_pinkyJnt03" -ln "l_pinkyJnt03" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 5.7079347910987934 0 -4.2632564145606011e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.0288035131007033e-014 2.420549735100638e-015 1.5902773407317588e-015 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_thumb_01_rig" -p "l_wrist_rig";
	rename -uid "9309D1FF-462C-A754-2130-228ECFD1E639";
	addAttr -ci true -sn "l_thumbJnt01" -ln "l_thumbJnt01" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 6.5995473060819876 0.12779103382383994 6.8323541418931093 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 80.839478022473841 -30.646912509941682 -26.197877778844422 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_thumb_02_rig" -p "l_thumb_01_rig";
	rename -uid "62629BF9-4A72-C75B-4B97-7680F859E385";
	addAttr -ci true -sn "l_thumbJnt02" -ln "l_thumbJnt02" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 6.0789227234193559 5.3290705182007514e-015 -4.2632564145606011e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.5005321103412927 2.8111139027753347 -15.754360215619952 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_thumb_03_rig" -p "l_thumb_02_rig";
	rename -uid "E127BDF5-4668-42F0-17AE-078296E6BC42";
	addAttr -ci true -sn "l_thumbJnt03" -ln "l_thumbJnt03" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 4.6326764669463145 7.9936057773011271e-015 -1.4210854715202004e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 2.3574106276806322e-014 -1.0663118948207636e-014 -3.1805546814635176e-015 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_clav_rig" -p "spineParent_rig";
	rename -uid "6ED25ADB-4A5A-B00F-1999-C68C61601CB3";
	addAttr -ci true -sn "r_clavJnt" -ln "r_clavJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 11.62215423126959 1.5780810334233273 -6.9999999905104167 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.0052494338286986691 -83.04102626328212 87.031676648807505 ;
createNode joint -n "r_shoulder_rig" -p "r_clav_rig";
	rename -uid "C7FC10F4-4967-9D2F-C8B9-34BB86EBF8BB";
	addAttr -ci true -sn "r_shoulderJnt" -ln "r_shoulderJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -12.265961753792824 3.7169301691619694e-005 3.2009490800177787e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4765013192861383e-005 -9.7940192453054085 -0.00063919823436477889 ;
createNode joint -n "r_elbow_rig" -p "r_shoulder_rig";
	rename -uid "21C81AF9-4DF0-E20F-5E3B-1381C5B3F528";
	addAttr -ci true -sn "r_elbowJnt" -ln "r_elbowJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -18.977126566327058 5.0134281082137022e-007 1.7840870913943263e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.2708081249146315e-006 -12.235272407050056 1.1856709482151905e-005 ;
createNode joint -n "r_wrist_rig" -p "r_elbow_rig";
	rename -uid "2A49276A-48CB-053F-DA3F-4C8728C464EB";
	addAttr -ci true -sn "r_wristJnt" -ln "r_wristJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -17.154387165172849 4.206812036500196e-007 -1.5932284643582761e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "r_index_01_rig" -p "r_wrist_rig";
	rename -uid "8AFBB06C-4D7A-93B2-0024-718DAC0F5FA5";
	addAttr -ci true -sn "r_indexJnt01" -ln "r_indexJnt01" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -10.392278887692733 -1.9517042506126501 -4.6647743389106289 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.327136464344278 6.7733761696717796 -2.5614952482327582 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_index_02_rig" -p "r_index_01_rig";
	rename -uid "22675DAF-49EE-0FE9-8D43-2B8A0B3DF7A4";
	addAttr -ci true -sn "r_indexJnt02" -ln "r_indexJnt02" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -6.7089432016256154 1.231613507446383e-005 4.0120761966022656e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.0358697611753049 -15.470899537925893 -0.69135407776869684 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_index_03_rig" -p "r_index_02_rig";
	rename -uid "08C97F56-4D47-2A87-072D-09B160A4BE57";
	addAttr -ci true -sn "r_indexJnt03" -ln "r_indexJnt03" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -6.332253924324398 -1.5478452075560339e-006 1.9382374503607025e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.2435019340449536e-014 6.1824814748656861e-015 3.1805546814635176e-015 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_pinky_01_rig" -p "r_wrist_rig";
	rename -uid "F3E99913-474B-04E2-B6E3-FA94FB33636C";
	addAttr -ci true -sn "r_pinkyJnt01" -ln "r_pinkyJnt01" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -8.9227898936340324 0.54560265527936735 2.5226020357983181 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 88.631908137196191 10.807330603990067 -5.254324603248957 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_pinky_02_rig" -p "r_pinky_01_rig";
	rename -uid "1D445E2E-4ED8-F908-4684-6DB99EDCAA99";
	addAttr -ci true -sn "r_pinkyJnt02" -ln "r_pinkyJnt02" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -5.5704052171290499 5.1312034594630518e-006 7.109870270483043e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.4452932773795741 -10.922608651197645 3.3832416023724665 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_pinky_03_rig" -p "r_pinky_02_rig";
	rename -uid "C7B6A5AA-45DA-4DCA-0A7D-94877C033948";
	addAttr -ci true -sn "r_pinkyJnt03" -ln "r_pinkyJnt03" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -5.7079534197613668 1.1124028187481372e-006 -7.3710476328869845e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -3.4563647767071607e-014 1.0912309918426207e-014 -3.180554681463516e-015 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_thumb_01_rig" -p "r_wrist_rig";
	rename -uid "555D1959-49F3-EA71-09D7-C8A3CF0B42EE";
	addAttr -ci true -sn "r_thumbJnt01" -ln "r_thumbJnt01" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -6.5996136290624179 -0.12780643716077122 -6.8323332105596304 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 80.839478022474225 -30.64691250994192 -26.19787777884417 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_thumb_02_rig" -p "r_thumb_01_rig";
	rename -uid "E821654D-433C-8FEE-98C6-9A88106DCBA7";
	addAttr -ci true -sn "r_thumbJnt02" -ln "r_thumbJnt02" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -6.0788805927821912 -3.8894801447586502e-005 9.1199209748538124e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.5005321103409244 2.8111139027752969 -15.754360215619936 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_thumb_03_rig" -p "r_thumb_02_rig";
	rename -uid "D0B553BF-47AC-DAF5-2E85-E4B53FEA1775";
	addAttr -ci true -sn "r_thumbJnt03" -ln "r_thumbJnt03" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -4.6326928260512759 2.5756352154626683e-005 -3.5911719820092003e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -5.9677117495817001e-014 -1.776117886907979e-016 6.361109362927032e-015 ;
	setAttr ".radi" 0.5;
createNode pointConstraint -n "spineParent_rig_pointConstraint1" -p "spineParent_rig";
	rename -uid "B7CEBB17-4361-4ABA-9BC2-FA92EFE8A4D8";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "topSpine_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0 0 -1.7763568394002505e-015 ;
	setAttr ".rst" -type "double3" -9.4895616142039899e-009 80.082359181456326 -9.4322473036750516 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "spineParent_rig_orientConstraint1" -p "spineParent_rig";
	rename -uid "9C91B41C-49FA-63DA-5CC7-74873962D175";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "topSpine_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -92.973611743757928 -89.999999999999829 0 ;
	setAttr ".o" -type "double3" 90 -2.973611743757917 89.999999999999829 ;
	setAttr ".rsrr" -type "double3" -2.2263882770244621e-014 -1.8538406451810674e-030 
		-9.5416640443905519e-015 ;
	setAttr -k on ".w0";
createNode transform -n "rig_controls" -p "rig_skeleton";
	rename -uid "EAB68C30-403F-4BCD-81B1-90AB5C624236";
	addAttr -ci true -sn "rigControls" -ln "rigControls" -at "double";
createNode transform -n "pelvis_ctrl" -p "rig_controls";
	rename -uid "607E5A13-40B2-D769-133B-46924E5F16AE";
	addAttr -ci true -sn "rigName" -ln "rigName" -dt "string";
	addAttr -ci true -sn "rootCtrl" -ln "rootCtrl" -at "double";
	addAttr -ci true -sn "njc_autoRigSystem" -ln "njc_autoRigSystem" -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0 48.502845764160156 -9.3133430480957031 ;
	setAttr ".sp" -type "double3" 0 48.502845764160156 -9.3133430480957031 ;
	setAttr ".rigName" -type "string" "";
createNode nurbsCurve -n "pelvis_ctrlShape" -p "pelvis_ctrl";
	rename -uid "34E31070-4050-3C02-281D-9E94D5370E8A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 17 0 no 3
		18 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
		18
		-24.607340357174937 49.17372111748918 15.293997309079236
		-24.607340357174937 48.50032768406686 15.293997309079236
		-24.607340357174937 49.17372111748918 15.293997309079236
		-24.607340357174937 49.17372111748918 -33.92068340527063
		-24.607340357174937 48.50032768406686 -33.92068340527063
		-24.607340357174937 49.17372111748918 -33.92068340527063
		24.607340357174937 49.17372111748918 -33.92068340527063
		24.607340357174937 48.50032768406686 -33.92068340527063
		24.607340357174937 49.17372111748918 -33.92068340527063
		24.607340357174937 49.17372111748918 15.293997309079236
		24.607340357174937 48.50032768406686 15.293997309079236
		24.607340357174937 49.17372111748918 15.293997309079236
		-24.607340357174937 49.17372111748918 15.293997309079236
		-24.607340357174937 48.50032768406686 15.293997309079236
		-24.607340357174937 48.50032768406686 -33.92068340527063
		24.607340357174937 48.50032768406686 -33.92068340527063
		24.607340357174937 48.50032768406686 15.293997309079236
		-24.607340357174937 48.50032768406686 15.293997309079236
		;
createNode transform -n "hips_ctrl" -p "pelvis_ctrl";
	rename -uid "9E61D8CB-4EDE-EF3F-08D2-C3814A0753F1";
	addAttr -ci true -sn "rigName" -ln "rigName" -dt "string";
	addAttr -ci true -sn "hipsCtrl" -ln "hipsCtrl" -at "double";
	addAttr -ci true -sn "njc_autoRigSystem" -ln "njc_autoRigSystem" -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 6.7546215009549136e-030 48.50284576416017 -9.3133430480956996 ;
	setAttr ".sp" -type "double3" 6.7546215009549136e-030 48.50284576416017 -9.3133430480956996 ;
	setAttr ".rigName" -type "string" "";
createNode nurbsCurve -n "hips_ctrlShape" -p "hips_ctrl";
	rename -uid "621251B2-4D40-27F4-AB57-359869768A98";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		16.030881882484923 35.107163614673965 -25.344224930580609
		-2.5865003554835181e-015 61.898527913646348 -31.984433623107002
		-16.030881882484909 35.107163614673965 -25.344224930580609
		-22.671090575011295 61.898527913646348 -9.3133430480957067
		-16.030881882484913 35.107163614673965 6.7175388343892042
		-6.8312420419376023e-015 61.898527913646348 13.357747526915599
		16.030881882484902 35.107163614673965 6.7175388343892095
		22.671090575011295 61.898527913646348 -9.3133430480956925
		16.030881882484923 35.107163614673965 -25.344224930580609
		-2.5865003554835181e-015 61.898527913646348 -31.984433623107002
		-16.030881882484909 35.107163614673965 -25.344224930580609
		;
createNode transform -n "spine_01_ctrl" -p "pelvis_ctrl";
	rename -uid "89BE69CD-4E82-7DCD-E88D-77AA6B3910F1";
	addAttr -ci true -sn "rigName" -ln "rigName" -dt "string";
	addAttr -ci true -sn "spineCtrl01" -ln "spineCtrl01" -at "double";
	addAttr -ci true -sn "njc_autoRigSystem" -ln "njc_autoRigSystem" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 2.7325893195284953e-015 56.649088358017046 -10.28786263002049 ;
	setAttr ".sp" -type "double3" 2.7325893195284953e-015 56.649088358017046 -10.28786263002049 ;
	setAttr ".rigName" -type "string" "";
createNode nurbsCurve -n "spine_01_ctrlShape" -p "spine_01_ctrl";
	rename -uid "4C007962-4FEC-992B-0A91-A0B7B21CA1BB";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		17.72544305657328 56.649088358017053 -28.013305686593732
		1.1250531399032587e-016 56.649088358017053 -35.355424599698402
		-17.725443056573258 56.649088358017053 -28.013305686593746
		-25.067561969677914 56.649088358017053 -10.287862630020499
		-17.725443056573262 56.649088358017053 7.437580426552767
		-4.5809312420678023e-015 56.649088358017053 14.779699339657434
		17.725443056573251 56.649088358017053 7.4375804265527741
		25.067561969677918 56.649088358017053 -10.28786263002047
		17.72544305657328 56.649088358017053 -28.013305686593732
		1.1250531399032587e-016 56.649088358017053 -35.355424599698402
		-17.725443056573258 56.649088358017053 -28.013305686593746
		;
createNode transform -n "spine_02_ctrl" -p "spine_01_ctrl";
	rename -uid "10A8256B-4407-D2D9-354D-86A435B64F80";
	addAttr -ci true -sn "rigName" -ln "rigName" -dt "string";
	addAttr -ci true -sn "spineCtrl02" -ln "spineCtrl02" -at "double";
	addAttr -ci true -sn "njc_autoRigSystem" -ln "njc_autoRigSystem" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" -3.4619749315630123e-015 64.464692133531344 -10.287862630020499 ;
	setAttr ".sp" -type "double3" -3.4619749315630123e-015 64.464692133531344 -10.287862630020499 ;
	setAttr ".rigName" -type "string" "";
createNode nurbsCurve -n "spine_02_ctrlShape" -p "spine_02_ctrl";
	rename -uid "D0892F89-43B1-AA5C-85D8-9C98C2BAD672";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		19.562050623125995 64.464602093303199 -29.849913253146454
		-1.5192122205160879e-016 64.464602093303199 -37.952779929074296
		-19.56205062312597 64.464602093303199 -29.849913253146468
		-27.664917299053808 64.464602093303199 -10.287862630020499
		-19.562050623125977 64.464602093303199 9.2741879931054783
		-5.3316644427431419e-015 64.464602093303199 17.377054669033324
		19.562050623125963 64.464602093303199 9.2741879931054854
		27.664917299053812 64.464602093303199 -10.28786263002047
		19.562050623125995 64.464602093303199 -29.849913253146454
		-1.5192122205160879e-016 64.464602093303199 -37.952779929074296
		-19.56205062312597 64.464602093303199 -29.849913253146468
		;
createNode transform -n "spine_03_ctrl" -p "spine_02_ctrl";
	rename -uid "37F7FFDD-4B4E-1F0F-E58A-E8BE8E2220EF";
	addAttr -ci true -sn "rigName" -ln "rigName" -dt "string";
	addAttr -ci true -sn "spineCtrl03" -ln "spineCtrl03" -at "double";
	addAttr -ci true -sn "njc_autoRigSystem" -ln "njc_autoRigSystem" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" -3.4619749315630076e-015 72.28029590904589 -10.287862630020499 ;
	setAttr ".sp" -type "double3" -3.4619749315630092e-015 72.28029590904589 -10.287862630020499 ;
	setAttr ".rigName" -type "string" "";
createNode nurbsCurve -n "spine_03_ctrlShape" -p "spine_03_ctrl";
	rename -uid "8B129066-444D-EE33-E754-B78E010FE270";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		20.206535357400448 72.280115828589373 -30.494397987420896
		-2.4471124912090579e-016 72.280115828589373 -38.864218981027648
		-20.206535357400423 72.280115828589373 -30.494397987420911
		-28.57635635100716 72.280115828589373 -10.287862630020499
		-20.206535357400426 72.280115828589373 9.9186727273799349
		-5.5951045462032453e-015 72.280115828589373 18.288493720986679
		20.206535357400416 72.280115828589373 9.918672727379942
		28.576356351007163 72.280115828589373 -10.28786263002047
		20.206535357400448 72.280115828589373 -30.494397987420896
		-2.4471124912090579e-016 72.280115828589373 -38.864218981027648
		-20.206535357400423 72.280115828589373 -30.494397987420911
		;
createNode transform -n "topSpine_ctrl" -p "spine_03_ctrl";
	rename -uid "34A0673D-4F89-9A41-D296-4281AD6D5030";
	addAttr -ci true -sn "rigName" -ln "rigName" -dt "string";
	addAttr -ci true -sn "spineTopCtrl" -ln "spineTopCtrl" -at "double";
	addAttr -ci true -k true -sn "spineScale" -ln "spineScale" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "njc_autoRigSystem" -ln "njc_autoRigSystem" -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -9.4895616142039899e-009 80.082359181456326 -9.4322473036750498 ;
	setAttr ".sp" -type "double3" -9.4895616142039899e-009 80.082359181456326 -9.4322473036750498 ;
	setAttr ".rigName" -type "string" "";
	setAttr -k on ".spineScale";
createNode nurbsCurve -n "topSpine_ctrlShape" -p "topSpine_ctrl";
	rename -uid "69D6AF3D-48D5-4484-7760-E59C9D7E6300";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		13.164675288683018 80.082359181456326 -28.508136842261759
		-9.4895631630487376e-009 80.082359181456326 -37.596448596830356
		-13.164675307662129 80.082359181456326 -28.508136842261766
		-18.617662360403276 80.082359181456326 -6.5670113453074954
		-13.164675307662133 80.082359181456326 15.374114151646786
		-9.4895666488610808e-009 80.082359181456326 24.46242590621538
		13.164675288682997 80.082359181456326 15.374114151646793
		18.617662341424154 80.082359181456326 -6.5670113453074741
		13.164675288683018 80.082359181456326 -28.508136842261759
		-9.4895631630487376e-009 80.082359181456326 -37.596448596830356
		-13.164675307662129 80.082359181456326 -28.508136842261766
		;
createNode transform -n "spineCurve_ik_CV_5" -p "topSpine_ctrl";
	rename -uid "798137FF-4F02-5A0C-A656-4083003BEEF3";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -9.4895616142039899e-009 80.082359181456326 -9.4322473036750498 ;
	setAttr ".sp" -type "double3" -9.4895616142039899e-009 80.082359181456326 -9.4322473036750498 ;
createNode clusterHandle -n "clusterHandleShape" -p "spineCurve_ik_CV_5";
	rename -uid "84FD01C9-49F8-6DB8-B37C-48B2082AB3E0";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".io" yes;
createNode transform -n "spineCurve_ik_CV_1_L_point_blend_X" -p "spineCurve_ik_CV_5";
	rename -uid "47D8CFDD-481D-4E18-9550-2CAF341D8260";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
createNode transform -n "spineCurve_ik_CV_1_L_point_blend_Y" -p "spineCurve_ik_CV_5";
	rename -uid "41071593-45F3-DEFC-B437-FD837FDE9F48";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
createNode transform -n "spineCurve_ik_CV_1_L_point_blend_Z" -p "spineCurve_ik_CV_5";
	rename -uid "6670DF8A-4652-8E41-651C-879FC26BFC82";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
createNode transform -n "spineCurve_ik_CV_2_L_point_blend_X" -p "spineCurve_ik_CV_5";
	rename -uid "731A4188-4915-4740-3242-F7965B502FDD";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
createNode transform -n "spineCurve_ik_CV_2_L_point_blend_Y" -p "spineCurve_ik_CV_5";
	rename -uid "6B307F14-463C-29F1-A33D-1F98530EF7CA";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
createNode transform -n "spineCurve_ik_CV_2_L_point_blend_Z" -p "spineCurve_ik_CV_5";
	rename -uid "46994FA2-4105-E355-C0CC-6785F651054C";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
createNode transform -n "spineCurve_ik_CV_3_L_point_blend_X" -p "spineCurve_ik_CV_5";
	rename -uid "93E6929F-4FA9-6436-9E1D-338ADE27C13B";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
createNode transform -n "spineCurve_ik_CV_3_L_point_blend_Y" -p "spineCurve_ik_CV_5";
	rename -uid "999D7655-463C-1F0D-07C8-468D77F0AA3A";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
createNode transform -n "spineCurve_ik_CV_3_L_point_blend_Z" -p "spineCurve_ik_CV_5";
	rename -uid "3D6C9433-4FA4-7439-C15F-B8922FCBC249";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
createNode transform -n "spineCurve_ik_CV_4_L_point_blend_X" -p "spineCurve_ik_CV_5";
	rename -uid "5E83EF7B-4F58-C71A-E72C-DF8D50ACFD77";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
createNode transform -n "spineCurve_ik_CV_4_L_point_blend_Y" -p "spineCurve_ik_CV_5";
	rename -uid "B4FE5FF6-4468-C146-D74F-FEABB79DF9F8";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
createNode transform -n "spineCurve_ik_CV_4_L_point_blend_Z" -p "spineCurve_ik_CV_5";
	rename -uid "F653016B-4726-34DE-540B-51A635BA716B";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
createNode transform -n "njc_mid_spine_ctrlGrp" -p "pelvis_ctrl";
	rename -uid "B5461140-4700-B6FA-0240-DD81383F1E82";
	setAttr ".rp" -type "double3" 2.7325893195284953e-015 68.373531405250986 -10.287862630020491 ;
	setAttr ".sp" -type "double3" 2.7325893195284953e-015 68.373531405250986 -10.287862630020491 ;
createNode transform -n "mid_ik_ctrl" -p "njc_mid_spine_ctrlGrp";
	rename -uid "6B985490-4EC5-C052-DC14-73AE0EF4877F";
	addAttr -ci true -sn "middleIkCtrl" -ln "middleIkCtrl" -at "double";
	setAttr -k off ".v";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" 2.7325893195284953e-015 68.373531405251001 -10.287862630020491 ;
	setAttr ".sp" -type "double3" 2.7325893195284953e-015 68.373531405251001 -10.287862630020491 ;
createNode nurbsCurve -n "mid_ik_ctrlShape" -p "mid_ik_ctrl";
	rename -uid "55A9943B-4DCF-6E1A-95E2-F1922BF89DD2";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		19.063017018075776 68.373531405251001 -29.350879648096235
		-3.4313044781086606e-016 68.373531405251001 -37.247039836732341
		-19.063017018075751 68.373531405251001 -29.350879648096249
		-26.95917720671185 68.373531405251001 -10.287862630020502
		-19.063017018075758 68.373531405251001 8.775154388055256
		-5.3907369113466679e-015 68.373531405251001 16.671314576691366
		19.063017018075747 68.373531405251001 8.7751543880552632
		26.95917720671186 68.373531405251001 -10.287862630020481
		19.063017018075776 68.373531405251001 -29.350879648096235
		-3.4313044781086606e-016 68.373531405251001 -37.247039836732341
		-19.063017018075751 68.373531405251001 -29.350879648096249
		;
createNode transform -n "bind_skeleton" -p "worldPlacement";
	rename -uid "5ACE2DC3-4274-FFE0-1ABD-36AFAE6A19EA";
	addAttr -ci true -sn "bindSkelGrp" -ln "bindSkelGrp" -at "double";
createNode joint -n "pelvis" -p "bind_skeleton";
	rename -uid "45AE3E84-44A5-70AA-F9AB-4D9ABE94D16D";
	addAttr -ci true -sn "pelvisBindJnt" -ln "pelvisBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 48.502845764160156 -9.3133430480957031 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 6.8217670768916108 89.999999999999986 ;
createNode joint -n "spine_01" -p "pelvis";
	rename -uid "D1257DEF-4744-F7F7-C009-05812C202ED7";
	addAttr -ci true -sn "spineBindJnt" -ln "spineBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 8.2043254941233954 1.7763568394002505e-015 -9.108631065094958e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 4.9955644642355823 ;
createNode joint -n "spine_02" -p "spine_01";
	rename -uid "D63DB942-4751-D692-7C93-249982FED96E";
	addAttr -ci true -sn "spineBindJnt_01" -ln "spineBindJnt_01" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 6.9590340171731313 7.1054273576010019e-015 -5.0722076695541734e-008 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -0.73309151890088731 ;
createNode joint -n "spine_03" -p "spine_02";
	rename -uid "E1509B8B-461C-1F88-472C-98A0DAE81B7B";
	addAttr -ci true -sn "spineBindJnt_02" -ln "spineBindJnt_02" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 8.0448897745064443 1.3322676295501878e-014 5.0722073366568009e-008 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 12.218299044961338 ;
createNode joint -n "spineEnd" -p "spine_03";
	rename -uid "B85BC630-4E6D-84AC-9DA7-708C29A3A93A";
	addAttr -ci true -sn "spineEndBindJnt" -ln "spineEndBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 8.5622869382978166 -3.5527136788005009e-015 -9.4895608661595547e-009 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -6.6853931696464857 ;
createNode joint -n "l_clav" -p "spineEnd";
	rename -uid "F8F8D225-4A4B-AE58-3368-779682EFA8DA";
	addAttr -ci true -sn "l_clavicleBindJnt" -ln "l_clavicleBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 11.622164177040304 1.5780783612258062 7.0000007956331762 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.0052494338302926493 -83.04102626328212 -92.968323351190847 ;
createNode joint -n "l_shoulder" -p "l_clav";
	rename -uid "215B873D-46F7-CDA8-5D4D-BF8BE01A7EF1";
	addAttr -ci true -sn "l_shoulderBindJnt" -ln "l_shoulderBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 12.265966789427518 -2.8421709430404007e-014 -2.6645352591003757e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4765013144671253e-005 -9.7940192453054067 -0.00063919823435948863 ;
createNode joint -n "l_elbow" -p "l_shoulder";
	rename -uid "2D42269E-4B4C-1053-28E3-D2BCB0CBCF45";
	addAttr -ci true -sn "l_elbowBindJnt" -ln "l_elbowBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 18.977118289932559 4.2632564145606011e-014 7.1054273576010019e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.2708081242192397e-006 -12.235272407050051 1.185670949122355e-005 ;
createNode joint -n "l_wrist" -p "l_elbow";
	rename -uid "362A9282-4056-ECBE-B0D4-CB9999D6E948";
	addAttr -ci true -sn "l_wristBindJnt" -ln "l_wristBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 17.154433690736084 2.8421709430404007e-014 1.0658141036401503e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "l_index_01" -p "l_wrist";
	rename -uid "26974DDD-4455-F33A-4937-709C4F3E727E";
	addAttr -ci true -sn "l_indexBindJnt01" -ln "l_indexBindJnt01" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 10.392254512342561 1.9517022060274627 4.6647774069108259 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.327136464344193 6.7733761696719359 -2.561495248232982 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_index_02" -p "l_index_01";
	rename -uid "754762DC-4FA1-0D06-8537-AF84C4F5711F";
	addAttr -ci true -sn "l_indexBindJnt02" -ln "l_indexBindJnt02" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 6.7088787484487611 -7.9936057773011271e-015 -2.8421709430404007e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.0358697611746304 -15.470899537925893 -0.69135407776869551 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_index_03" -p "l_index_02";
	rename -uid "BCA40912-4031-A060-797A-FA87A2FDD431";
	addAttr -ci true -sn "l_indexBindJnt03" -ln "l_indexBindJnt03" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 6.332299490449941 3.5527136788005009e-015 -1.4210854715202004e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_pinky_01" -p "l_wrist";
	rename -uid "F7262677-4CEF-53B3-1485-33B80242810E";
	addAttr -ci true -sn "l_pinkyBindJnt01" -ln "l_pinkyBindJnt01" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 8.922783762741858 -0.54561132659441114 -2.5225998266376308 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 88.631908137196064 10.807330603990216 -5.2543246032492048 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_pinky_02" -p "l_pinky_01";
	rename -uid "A039164B-41F2-5D97-00FA-BA8D23B796E7";
	addAttr -ci true -sn "l_pinkyBindJnt02" -ln "l_pinkyBindJnt02" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 5.5703760322813878 1.7763568394002505e-015 -1.4210854715202004e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.4452932773809035 -10.92260865119767 3.3832416023724741 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_pinky_03" -p "l_pinky_02";
	rename -uid "218B0BD3-43C7-A38E-589D-EBAF23B85735";
	addAttr -ci true -sn "l_pinkyBindJnt03" -ln "l_pinkyBindJnt03" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 5.7079347910987792 1.7763568394002505e-015 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_thumb_01" -p "l_wrist";
	rename -uid "F5FD9834-49DA-661D-8D64-8BB36FCDD1EC";
	addAttr -ci true -sn "l_thumbBindJnt01" -ln "l_thumbBindJnt01" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 6.599547306081945 0.12779103382409573 6.8323541418932869 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 80.839478022474054 -30.646912509941817 -26.197877778844287 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_thumb_02" -p "l_thumb_01";
	rename -uid "10F7BD13-419F-CB69-A9EC-D59A9296F9D6";
	addAttr -ci true -sn "l_thumbBindJnt02" -ln "l_thumbBindJnt02" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 6.078922723419371 -6.2172489379008766e-015 -2.8421709430404007e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.5005321103412923 2.8111139027753071 -15.754360215619949 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_thumb_03" -p "l_thumb_02";
	rename -uid "50CF6055-4756-27B9-AE0E-3293526873BD";
	addAttr -ci true -sn "l_thumbBindJnt03" -ln "l_thumbBindJnt03" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 4.632676466946311 8.8817841970012523e-015 -1.4210854715202004e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "neckBase" -p "spineEnd";
	rename -uid "132B0451-4896-5C51-1C28-8FB1C5A72E96";
	addAttr -ci true -sn "neckBaseBindJnt" -ln "neckBaseBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.0313069447810083e-014 -4.2426611175694265e-015 
		20.552789729139025 ;
createNode joint -n "neck" -p "neckBase";
	rename -uid "F399A266-4BDB-E40E-BC76-FEA7CFC51BA2";
	addAttr -ci true -sn "neckBindJnt" -ln "neckBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -113.52640147289696 -89.999999999999972 0 ;
createNode joint -n "l_eye" -p "neck";
	rename -uid "54085047-45B2-3D4F-623C-65AD56A00F6B";
	addAttr -ci true -sn "l_eyeBindJnt" -ln "l_eyeBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 3.275999999999986 4.4439999999999884 5.8279999999999781 ;
	setAttr ".r" -type "double3" -1.4312496066585824e-014 -6.3611093629270391e-015 -4.0552072188659841e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 6.3611093629270367e-015 2.5795090568549831e-014 -2.9764848546715137e-015 ;
createNode joint -n "jaw" -p "neck";
	rename -uid "FD641601-424E-92DA-4101-BBBC2B888A19";
	addAttr -ci true -sn "jawBindJnt" -ln "jawBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 1.0000000925454315e-006 -6.0659707934835865 12.622518224451849 ;
	setAttr ".r" -type "double3" -1.4312496066585824e-014 -6.3611093629270391e-015 -4.0552072188659841e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 6.3611093629270367e-015 2.5795090568549831e-014 -2.9764848546715137e-015 ;
createNode joint -n "r_eye" -p "neck";
	rename -uid "8D43D7D7-48C4-E575-B314-B2B03EF86512";
	addAttr -ci true -sn "r_eyeBindJnt" -ln "r_eyeBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -3.2759998890397544 4.4444147044848137 5.8280005273532964 ;
	setAttr ".r" -type "double3" -1.4312496066585824e-014 -6.3611093629270391e-015 -4.0552072188659841e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 1.0318036227419935e-013 -1.1905939418686055e-014 ;
createNode pointConstraint -n "neck_pointConstraint1" -p "neck";
	rename -uid "BA164F12-4489-E071-EF9C-E38E2DD33A01";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "neck_rigW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0 1.4210854715202004e-014 1.3234889800848443e-023 ;
	setAttr ".rst" -type "double3" 19.769148705961896 0 -6.3548483899259397e-008 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "neck_orientConstraint1" -p "neck";
	rename -uid "B8F32E2D-41A3-8AE3-877C-AB8B1FB2DF32";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "neck_rigW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -9.541664044390787e-015 -1.0813885916975955e-013 2.5126381983561782e-013 ;
	setAttr ".rsrr" -type "double3" -9.541664044390787e-015 -1.0813885916975955e-013 
		2.5126381983561782e-013 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "neck_scaleConstraint1" -p "neck";
	rename -uid "E372C1B1-4643-A32B-8AD6-EB817267EDBD";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "neck_rigW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1.0000000000000002 0.99999999999999978 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "neckBase_pointConstraint1" -p "neckBase";
	rename -uid "90390A28-4C41-CE99-40A6-7DBB0E57E078";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "neckBase_rigW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -1.4210854715202004e-014 3.5527136788005009e-015 -1.9852334701272664e-023 ;
	setAttr ".rst" -type "double3" 16.582691502051858 -2.3092638912203256e-014 -3.7922213667173116e-008 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "neckBase_orientConstraint1" -p "neckBase";
	rename -uid "5E038648-4C06-5228-7915-FE9DEA4FFB97";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "neckBase_rigW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0 0 1.2722218725854067e-014 ;
	setAttr ".rsrr" -type "double3" 0 0 1.2722218725854067e-014 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "neckBase_scaleConstraint1" -p "neckBase";
	rename -uid "E7CBEEDB-4162-B33A-89B5-86B068FF04E6";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "neckBase_rigW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
	setAttr -k on ".w0";
createNode joint -n "r_clav" -p "spineEnd";
	rename -uid "87788D36-472C-C1E6-F935-608A1257D5EA";
	addAttr -ci true -sn "r_clavicleBindJnt" -ln "r_clavicleBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 11.622154231269576 1.5780810334233095 -6.9999999905104486 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.0052494338299850039 -83.041026263282149 87.031676648808741 ;
createNode joint -n "r_shoulder" -p "r_clav";
	rename -uid "3F9B444D-4825-DE44-603A-CFAAC25144B8";
	addAttr -ci true -sn "r_shoulderBindJnt" -ln "r_shoulderBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -12.265961753792826 3.7169301663197984e-005 3.2009490817941355e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4765013145236861e-005 -9.7940192453054067 -0.00063919823436609023 ;
createNode joint -n "r_elbow" -p "r_shoulder";
	rename -uid "6CAC8F70-4721-1C24-9D98-939D48E4D012";
	addAttr -ci true -sn "r_elbowBindJnt" -ln "r_elbowBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -18.977126566327065 5.0134283924307965e-007 1.7840870913943263e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.2708081230543568e-006 -12.235272407050049 1.1856709480355121e-005 ;
createNode joint -n "r_wrist" -p "r_elbow";
	rename -uid "E15D1558-46B8-0312-C136-4B9EFE1C789F";
	addAttr -ci true -sn "r_wristBindJnt" -ln "r_wristBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -17.154387165172821 4.206812036500196e-007 -1.5932284647135475e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "r_index_01" -p "r_wrist";
	rename -uid "0F527015-4FA0-F558-4EEE-1B919CC7D2B4";
	addAttr -ci true -sn "r_indexBindJnt01" -ln "r_indexBindJnt01" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -10.392278887692761 -1.9517042506123801 -4.6647743389104459 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.327136464344278 6.7733761696719599 -2.5614952482330047 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_index_02" -p "r_index_01";
	rename -uid "76B315E3-4B74-5DEC-ED1E-F195EC8E37F2";
	addAttr -ci true -sn "r_indexBindJnt02" -ln "r_indexBindJnt02" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -6.7089432016256296 1.2316135071799295e-005 4.0120761994444365e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.0358697611753138 -15.470899537925897 -0.69135407776869706 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_index_03" -p "r_index_02";
	rename -uid "79C7463F-46D1-0042-611A-9082CC66FEB6";
	addAttr -ci true -sn "r_indexBindJnt03" -ln "r_indexBindJnt03" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -6.3322539243243838 -1.5478452040033203e-006 1.9382374475185316e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 8.5377364625159377e-007 4.739395860767748e-023 -8.2321481033368722e-023 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_pinky_01" -p "r_wrist";
	rename -uid "B82CC506-4F9B-1CCA-2B2E-01838D702AAF";
	addAttr -ci true -sn "r_pinkyBindJnt01" -ln "r_pinkyBindJnt01" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -8.9227898936340182 0.54560265527963736 2.5226020357984886 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 88.631908137196191 10.807330603990241 -5.2543246032492066 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_pinky_02" -p "r_pinky_01";
	rename -uid "1CD1B413-44BD-FF72-158B-3891AA2207DB";
	addAttr -ci true -sn "r_pinkyBindJnt02" -ln "r_pinkyBindJnt02" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -5.570405217129057 5.1312034639039439e-006 7.1098702747462994e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.4452932773796021 -10.922608651197642 3.3832416023724665 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_pinky_03" -p "r_pinky_02";
	rename -uid "53C79B70-41CD-73CA-0CDC-BB8D6A513015";
	addAttr -ci true -sn "r_pinkyBindJnt03" -ln "r_pinkyBindJnt03" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -5.7079534197613739 1.1124028187481372e-006 -7.3710476300448136e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_thumb_01" -p "r_wrist";
	rename -uid "FE27EA89-4320-EB09-10AE-DC8DF5C9B13B";
	addAttr -ci true -sn "r_thumbBindJnt01" -ln "r_thumbBindJnt01" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -6.5996136290624392 -0.12780643716054385 -6.8323332105594563 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 80.83947802247414 -30.64691250994176 -26.197877778844372 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_thumb_02" -p "r_thumb_01";
	rename -uid "AF3D2646-4E87-00C4-C9F6-D8BC510CC00F";
	addAttr -ci true -sn "r_thumbBindJnt02" -ln "r_thumbBindJnt02" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -6.0788805927822063 -3.8894801433819737e-005 9.1199209748538124e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.5005321103409055 2.8111139027753174 -15.754360215619936 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_thumb_03" -p "r_thumb_02";
	rename -uid "FE7592A8-4605-6A33-C4DC-2BBB0440B1CB";
	addAttr -ci true -sn "r_thumbBindJnt03" -ln "r_thumbBindJnt03" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -4.6326928260512723 2.575635213908356e-005 -3.5911719805881148e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 8.5377364625159377e-007 -9.4787916631455918e-023 8.6273358981672525e-023 ;
	setAttr ".radi" 0.5;
createNode joint -n "hips" -p "pelvis";
	rename -uid "F5C338F8-43B8-CC87-5A79-A49C0314E1DC";
	addAttr -ci true -sn "hipsBindJnt" -ln "hipsBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "l_hip" -p "hips";
	rename -uid "75637A38-4140-FCB5-86A6-48B700C05C5B";
	addAttr -ci true -sn "l_hipBindJnt" -ln "l_hipBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -1.365820135474749 2.60762767465165 10.901285582461375 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 -1.4574876783470912e-005 178.64906316091097 ;
createNode joint -n "l_knee" -p "l_hip";
	rename -uid "25FA95E6-478D-E5FA-1B63-29951D75B4BE";
	addAttr -ci true -sn "l_kneeBindJnt" -ln "l_kneeBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 22.362855289533634 8.8817841970012523e-015 -2.6645352591003757e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 13.792099994027064 -8.450309381023591e-006 ;
createNode joint -n "l_ankle" -p "l_knee";
	rename -uid "281E13B4-4006-6EEE-17FD-E7B2858997D9";
	addAttr -ci true -sn "l_ankleBindJnt" -ln "l_ankleBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 19.015319625082249 3.5527136788005009e-015 -5.3290705182007514e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.3260489205091322e-005 -59.416969957867188 7.581762031426619e-005 ;
createNode joint -n "l_ball" -p "l_ankle";
	rename -uid "51799D24-4434-89B7-D23E-708B0A9FDF4F";
	addAttr -ci true -sn "l_ballBindJnt" -ln "l_ballBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 10.49311908045631 -8.8817841970012523e-015 4.4408920985006262e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.029246164036723921 -36.202415693552801 0.049437165494055801 ;
createNode joint -n "l_toe" -p "l_ball";
	rename -uid "1CED42BF-4DA7-EDDF-DBCA-E29B101F0B52";
	addAttr -ci true -sn "l_toeBindJnt" -ln "l_toeBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 9.6354058851631912 0 -2.4702462297909733e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "r_hip" -p "hips";
	rename -uid "D8C7469A-4E8C-82B3-868C-C089C332A124";
	addAttr -ci true -sn "r_hipBindJnt" -ln "r_hipBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -1.3658523436601939 2.6076262856699879 -10.9013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000014 -1.4574876738857749e-005 -1.3509368390890364 ;
createNode joint -n "r_knee" -p "r_hip";
	rename -uid "E4103F65-4E4B-AC79-E78D-DAA1C2917273";
	addAttr -ci true -sn "r_kneeBindJnt" -ln "r_kneeBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -22.362815337330996 5.6886437409531254e-006 -7.3233510550219094e-007 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.022008702083125e-006 13.792099994027035 -8.4503093858059356e-006 ;
createNode joint -n "r_ankle" -p "r_knee";
	rename -uid "55F04025-4B7F-AC96-5FA1-A1A5830C3CDE";
	addAttr -ci true -sn "r_ankleBindJnt" -ln "r_ankleBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -19.015331074775947 1.974016171857329e-006 -7.5268428156860523e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.3260489250255383e-005 -59.416969957867138 7.5817620393420104e-005 ;
createNode joint -n "r_ball" -p "r_ankle";
	rename -uid "DF387327-43E8-06B3-C6CB-54BBF9F0B1C8";
	addAttr -ci true -sn "r_ballBindJnt" -ln "r_ballBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -10.493109997995635 7.8490247830131921e-006 1.5153520642741114e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.029246164036735103 -36.202415693552801 0.049437165494090038 ;
createNode joint -n "r_toe" -p "r_ball";
	rename -uid "362E95DA-41F6-AC07-E2C7-D59CBF5FEFE8";
	addAttr -ci true -sn "r_toeBindJnt" -ln "r_toeBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -9.6354123294209799 1.191933148980695e-005 -3.8502733723522198e-008 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 8.5377364625159355e-007 9.2564205532667063e-026 2.8436374726047257e-022 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "CCEC0CA5-42C7-B421-56FE-1189A79FC483";
	setAttr -s 6 ".lnk";
	setAttr -s 6 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "ECA6CDDB-40CD-8E8A-7DE4-23A02D275775";
	setAttr -s 2 ".dli[1]"  1;
	setAttr -s 2 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "80EF4EA2-4949-92FB-2A52-77ACDF90BF2A";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "4A4BB92C-4C04-7855-AEB0-48A8F6094B07";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "BD0B2FD8-464F-5E2F-257B-C7BAB346806C";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "52C9D8D3-441F-5C23-D67B-BF8FEAEE10B2";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1240\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1240\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\tscriptedPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 19 100 -ps 2 81 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1240\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1240\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "17B2DA4D-41FC-6E80-8198-E4A21ACF55C8";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode ikSplineSolver -n "ikSplineSolver";
	rename -uid "2BE696B7-4A6E-4076-42D5-D09941D9989E";
createNode displayLayer -n "layer1";
	rename -uid "660ECE3C-4B2D-0B81-2CBD-74980F11D3B9";
	setAttr ".do" 1;
createNode cluster -n "cluster1";
	rename -uid "3F797D61-4AF2-74BF-7557-8291E296EFD2";
	setAttr ".wcm" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 2.7325893232234109e-015
		 56.649088358017032 -10.287862630020491 0 0 0 2.7325893232234109e-015 56.649088358017032
		 -10.287862630020491 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pm" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
createNode tweak -n "tweak1";
	rename -uid "C5A1CE01-42B1-8F3F-F3CF-849408D69308";
createNode objectSet -n "cluster1Set";
	rename -uid "CDB5298A-4368-26E0-950A-A2BB6D4B9EFF";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster1GroupId";
	rename -uid "EEE95E4C-45C7-B37A-B24F-C59EE869CD5F";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster1GroupParts";
	rename -uid "B4B4592D-48DE-7CBC-A749-5BA088399625";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[0]";
createNode objectSet -n "tweakSet1";
	rename -uid "19EB1CF6-44F3-7001-ED55-7188CF926673";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId2";
	rename -uid "7B42EB98-4690-11A3-1305-69BD79EFC4D1";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "BDD3AD0F-43F3-5384-9FD3-8DA87AD4EA6E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode cluster -n "cluster2";
	rename -uid "062E2A38-44ED-938C-C268-8DA176D2A003";
	setAttr ".wcm" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 -2.9196095260608423e-008
		 59.275004000646554 -10.329757386832791 0 0 0 -2.9196095260608423e-008 59.275004000646554
		 -10.329757386832791 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pm" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
createNode objectSet -n "cluster2Set";
	rename -uid "3C16A173-4565-5B24-BC24-3B832AC840F1";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster2GroupId";
	rename -uid "4C3844BC-4EAE-CF13-DD3A-54AF05D427A3";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster2GroupParts";
	rename -uid "B52AF1E5-4DBA-2894-791B-4698BF5DE9F3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[1]";
createNode cluster -n "cluster3";
	rename -uid "8FCCEACA-4741-DD9A-B58F-BF86A7AAEA8C";
	setAttr ".wcm" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 -7.952638228755677e-008
		 64.496062648911931 -10.479176482336932 0 0 0 -7.952638228755677e-008 64.496062648911931
		 -10.479176482336932 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pm" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
createNode objectSet -n "cluster3Set";
	rename -uid "E3E0D0B6-4C7E-1C73-53B7-0191999115DD";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster3GroupId";
	rename -uid "83CF4721-4D07-5344-94D3-E6A73364FF9F";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster3GroupParts";
	rename -uid "816EC31B-4FFE-1A71-BEA5-99A606B5869B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[2]";
createNode cluster -n "cluster4";
	rename -uid "258E6612-403D-8B7F-5E50-B5B250709201";
	setAttr ".wcm" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 2.8470594588902456e-008
		 72.363191810609877 -11.277284826066461 0 0 0 2.8470594588902456e-008 72.363191810609877
		 -11.277284826066461 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pm" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
createNode objectSet -n "cluster4Set";
	rename -uid "D7F4E2F3-4B34-9213-BE8C-41A4548F5CB1";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster4GroupId";
	rename -uid "0201D773-49C7-250F-D1F9-89A262E86B0B";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster4GroupParts";
	rename -uid "09C2A5D8-4138-5D9F-5C76-31BD404BDC8A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[3]";
createNode cluster -n "cluster5";
	rename -uid "5EDC4BB9-4B67-A841-771E-1EBF29DD29FA";
	setAttr ".wcm" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 1.2171741546379702e-009
		 77.552392796654573 -10.01770418735598 0 0 0 1.2171741546379702e-009 77.552392796654573
		 -10.01770418735598 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pm" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
createNode objectSet -n "cluster5Set";
	rename -uid "8E4E24D6-4374-6252-B93B-D4922AE4E149";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster5GroupId";
	rename -uid "66C8A2FC-4F1F-7EE2-C281-B582B776ACD9";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster5GroupParts";
	rename -uid "5268358D-415C-6046-15A7-97AA8970D28C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[4]";
createNode cluster -n "cluster6";
	rename -uid "55A79E18-404D-1D39-134B-C0BFBF605B8E";
	setAttr ".wcm" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 -9.4895616142039899e-009
		 80.082359181456326 -9.4322473036750498 0 0 0 -9.4895616142039899e-009 80.082359181456326
		 -9.4322473036750498 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pm" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
createNode objectSet -n "cluster6Set";
	rename -uid "BA1C41AE-4A98-27AF-6795-0BBA5AB931EF";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster6GroupId";
	rename -uid "07BE5D7C-420A-53DF-3B29-70BC285805B7";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster6GroupParts";
	rename -uid "00D27259-4D1D-0A7E-525F-82AB9CD89097";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[5]";
createNode reverse -n "spineCurve_ik_CV_1_point_blend_X_point_reverse";
	rename -uid "F547098F-4EA8-3C30-C5F5-FE9A9FE6707A";
createNode reverse -n "spineCurve_ik_CV_1_point_blend_Y_point_reverse";
	rename -uid "0D89116F-4953-2CB5-BE6D-0FB177CA0BC1";
createNode reverse -n "spineCurve_ik_CV_1_point_blend_Z_point_reverse";
	rename -uid "771D679C-4761-70E0-5A11-C58CA98563F8";
createNode reverse -n "spineCurve_ik_CV_2_point_blend_X_point_reverse";
	rename -uid "7574F1A0-4CAC-09CD-D389-C1B6C220E7F1";
createNode reverse -n "spineCurve_ik_CV_2_point_blend_Y_point_reverse";
	rename -uid "39F630AF-4F1B-58E4-1AAF-B8ACFDAF84A5";
createNode reverse -n "spineCurve_ik_CV_2_point_blend_Z_point_reverse";
	rename -uid "33FEA918-458A-754F-DC69-549E518027AD";
createNode reverse -n "spineCurve_ik_CV_3_point_blend_X_point_reverse";
	rename -uid "4236CE04-4D38-1A62-FC64-8BAA25F2C45C";
createNode reverse -n "spineCurve_ik_CV_3_point_blend_Y_point_reverse";
	rename -uid "0E4941CF-4BB4-9F14-0AB4-7EBB4025D16C";
createNode reverse -n "spineCurve_ik_CV_3_point_blend_Z_point_reverse";
	rename -uid "47885E3B-4ED4-EDB7-33E1-C3BFEEF7043E";
createNode reverse -n "spineCurve_ik_CV_4_point_blend_X_point_reverse";
	rename -uid "3A368611-4428-D8A0-17C2-44ACBA7417DF";
createNode reverse -n "spineCurve_ik_CV_4_point_blend_Y_point_reverse";
	rename -uid "BF9BFE0A-4AD5-9BD8-947C-CAA8FA269BDF";
createNode reverse -n "spineCurve_ik_CV_4_point_blend_Z_point_reverse";
	rename -uid "D551F2D4-4AE9-967A-4CA5-4BAC3AA4A9ED";
createNode multiplyDivide -n "njcMultiSpineStre";
	rename -uid "E3EF8FF3-46BC-6343-C6D3-C7AEBF722F74";
	setAttr ".op" 2;
createNode multiplyDivide -n "njcRigScaleSpineMulti";
	rename -uid "C316A8BC-4CE6-25D7-166A-569918C5D3C5";
	setAttr ".i2" -type "float3" 23.584295 1 1 ;
createNode curveInfo -n "njcArcLengthNode";
	rename -uid "FF0AACA6-48B0-D815-4174-249D1859AA25";
createNode expression -n "expression1";
	rename -uid "F81AF0EE-4494-59A5-1BCF-D1B85A5257C8";
	setAttr -k on ".nds";
	setAttr ".ixp" -type "string" ".O[0] = .I[0] * 8.204325";
createNode expression -n "expression2";
	rename -uid "F1E84922-4ABC-D11A-A5B4-FC85CA9900AE";
	setAttr -k on ".nds";
	setAttr ".ixp" -type "string" ".O[0] = .I[0] * 6.959034";
createNode expression -n "expression3";
	rename -uid "6616C191-4CF1-C00B-B243-DFB71672E68D";
	setAttr -k on ".nds";
	setAttr ".ixp" -type "string" ".O[0] = .I[0] * 8.044890";
createNode expression -n "expression4";
	rename -uid "AF5A3DAA-46A0-0A03-A021-8191F49F321D";
	setAttr -k on ".nds";
	setAttr ".ixp" -type "string" ".O[0] = .I[0] * 8.562287";
createNode expression -n "expression5";
	rename -uid "AF9B1900-4EE9-F19E-5DDB-EAB4FFB0C4FD";
	setAttr -k on ".nds";
	setAttr -s 2 ".in";
	setAttr -s 2 ".in";
	setAttr -s 2 ".out";
	setAttr ".ixp" -type "string" "if(.I[0] >= -.5 && .I[0] <= 2 && .I[1] > 0){.O[0] = (.I[0] * (-0.150000+0.1)) + 1;}if(.I[0] >= -.5 && .I[0] <= 2 && .I[1] > 0){.O[1] = (.I[0] * -0.150000) + 1;}";
createNode expression -n "expression6";
	rename -uid "0D8EAE1F-46B5-6420-901D-7F980314F3DB";
	setAttr -k on ".nds";
	setAttr -s 2 ".in";
	setAttr -s 2 ".in";
	setAttr -s 2 ".out";
	setAttr ".ixp" -type "string" "if(.I[0] >= -.5 && .I[0] <= 2 && .I[1] > 0){.O[0] = (.I[0] * (-0.050000+0.1)) + 1;}if(.I[0] >= -.5 && .I[0] <= 2 && .I[1] > 0){.O[1] = (.I[0] * -0.050000) + 1;}";
createNode expression -n "expression7";
	rename -uid "A63AA1F6-4F5F-4305-8549-2E804F12E29F";
	setAttr -k on ".nds";
	setAttr -s 2 ".in";
	setAttr -s 2 ".in";
	setAttr -s 2 ".out";
	setAttr ".ixp" -type "string" "if(.I[0] >= -.5 && .I[0] <= 2 && .I[1] > 0){.O[0] = (.I[0] * (-0.050000+0.1)) + 1;}if(.I[0] >= -.5 && .I[0] <= 2 && .I[1] > 0){.O[1] = (.I[0] * -0.050000) + 1;}";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 14 ".u";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "spine_01_rig.msg" "ikSplineSpine.hsj";
connectAttr "effector1.hp" "ikSplineSpine.hee";
connectAttr "ikSplineSolver.msg" "ikSplineSpine.hsv";
connectAttr "spineCurve_ikShape.ws" "ikSplineSpine.ic";
connectAttr "njc_bottom_twistCtrl.xm" "ikSplineSpine.dwum";
connectAttr "njc_top_twistCtrl.xm" "ikSplineSpine.dwue";
connectAttr "cluster6.og[0]" "spineCurve_ikShape.cr";
connectAttr "tweak1.pl[0].cp[0]" "spineCurve_ikShape.twl";
connectAttr "cluster1GroupId.id" "spineCurve_ikShape.iog.og[0].gid";
connectAttr "cluster1Set.mwc" "spineCurve_ikShape.iog.og[0].gco";
connectAttr "groupId2.id" "spineCurve_ikShape.iog.og[1].gid";
connectAttr "tweakSet1.mwc" "spineCurve_ikShape.iog.og[1].gco";
connectAttr "cluster2GroupId.id" "spineCurve_ikShape.iog.og[2].gid";
connectAttr "cluster2Set.mwc" "spineCurve_ikShape.iog.og[2].gco";
connectAttr "cluster3GroupId.id" "spineCurve_ikShape.iog.og[3].gid";
connectAttr "cluster3Set.mwc" "spineCurve_ikShape.iog.og[3].gco";
connectAttr "cluster4GroupId.id" "spineCurve_ikShape.iog.og[4].gid";
connectAttr "cluster4Set.mwc" "spineCurve_ikShape.iog.og[4].gco";
connectAttr "cluster5GroupId.id" "spineCurve_ikShape.iog.og[5].gid";
connectAttr "cluster5Set.mwc" "spineCurve_ikShape.iog.og[5].gco";
connectAttr "cluster6GroupId.id" "spineCurve_ikShape.iog.og[6].gid";
connectAttr "cluster6Set.mwc" "spineCurve_ikShape.iog.og[6].gco";
connectAttr "spineCurve_ik_CV_1_point_X_pointConstraint1.ctx" "spineCurve_ik_CV_1_point_X.tx"
		;
connectAttr "spineCurve_ik_CV_1_point_Y_pointConstraint1.cty" "spineCurve_ik_CV_1_point_Y.ty"
		;
connectAttr "spineCurve_ik_CV_1_point_Z_pointConstraint1.ctz" "spineCurve_ik_CV_1_point_Z.tz"
		;
connectAttr "spineCurve_ik_CV_1_point_Z.pim" "spineCurve_ik_CV_1_point_Z_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_1_point_Z.rp" "spineCurve_ik_CV_1_point_Z_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_1_point_Z.rpt" "spineCurve_ik_CV_1_point_Z_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_Z.t" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_Z.rp" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_Z.rpt" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_Z.pm" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_1_point_Z_pointConstraint1.w0" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Z.t" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Z.rp" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Z.rpt" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Z.pm" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_1_point_Z_pointConstraint1.w1" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_1_point_Z.blendSpace" "spineCurve_ik_CV_1_point_Z_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Z_point_reverse.ox" "spineCurve_ik_CV_1_point_Z_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_1_point_Y.pim" "spineCurve_ik_CV_1_point_Y_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_1_point_Y.rp" "spineCurve_ik_CV_1_point_Y_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_1_point_Y.rpt" "spineCurve_ik_CV_1_point_Y_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_Y.t" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_Y.rp" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_Y.rpt" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_Y.pm" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_1_point_Y_pointConstraint1.w0" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Y.t" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Y.rp" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Y.rpt" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Y.pm" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_1_point_Y_pointConstraint1.w1" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_1_point_Y.blendSpace" "spineCurve_ik_CV_1_point_Y_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Y_point_reverse.ox" "spineCurve_ik_CV_1_point_Y_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_1_point_X.pim" "spineCurve_ik_CV_1_point_X_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_1_point_X.rp" "spineCurve_ik_CV_1_point_X_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_1_point_X.rpt" "spineCurve_ik_CV_1_point_X_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_X.t" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_X.rp" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_X.rpt" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_X.pm" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_1_point_X_pointConstraint1.w0" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_X.t" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_X.rp" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_X.rpt" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_X.pm" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_1_point_X_pointConstraint1.w1" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_1_point_X.blendSpace" "spineCurve_ik_CV_1_point_X_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_X_point_reverse.ox" "spineCurve_ik_CV_1_point_X_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_2_point_X_pointConstraint1.ctx" "spineCurve_ik_CV_2_point_X.tx"
		;
connectAttr "spineCurve_ik_CV_2_point_Y_pointConstraint1.cty" "spineCurve_ik_CV_2_point_Y.ty"
		;
connectAttr "spineCurve_ik_CV_2_point_Z_pointConstraint1.ctz" "spineCurve_ik_CV_2_point_Z.tz"
		;
connectAttr "spineCurve_ik_CV_2_point_Z.pim" "spineCurve_ik_CV_2_point_Z_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_2_point_Z.rp" "spineCurve_ik_CV_2_point_Z_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_2_point_Z.rpt" "spineCurve_ik_CV_2_point_Z_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_Z.t" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_Z.rp" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_Z.rpt" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_Z.pm" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_2_point_Z_pointConstraint1.w0" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Z.t" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Z.rp" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Z.rpt" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Z.pm" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_2_point_Z_pointConstraint1.w1" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_2_point_Z.blendSpace" "spineCurve_ik_CV_2_point_Z_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Z_point_reverse.ox" "spineCurve_ik_CV_2_point_Z_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_2_point_Y.pim" "spineCurve_ik_CV_2_point_Y_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_2_point_Y.rp" "spineCurve_ik_CV_2_point_Y_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_2_point_Y.rpt" "spineCurve_ik_CV_2_point_Y_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_Y.t" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_Y.rp" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_Y.rpt" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_Y.pm" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_2_point_Y_pointConstraint1.w0" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Y.t" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Y.rp" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Y.rpt" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Y.pm" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_2_point_Y_pointConstraint1.w1" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_2_point_Y.blendSpace" "spineCurve_ik_CV_2_point_Y_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Y_point_reverse.ox" "spineCurve_ik_CV_2_point_Y_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_2_point_X.pim" "spineCurve_ik_CV_2_point_X_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_2_point_X.rp" "spineCurve_ik_CV_2_point_X_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_2_point_X.rpt" "spineCurve_ik_CV_2_point_X_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_X.t" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_X.rp" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_X.rpt" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_X.pm" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_2_point_X_pointConstraint1.w0" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_X.t" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_X.rp" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_X.rpt" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_X.pm" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_2_point_X_pointConstraint1.w1" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_2_point_X.blendSpace" "spineCurve_ik_CV_2_point_X_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_X_point_reverse.ox" "spineCurve_ik_CV_2_point_X_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_3_point_X_pointConstraint1.ctx" "spineCurve_ik_CV_3_point_X.tx"
		;
connectAttr "spineCurve_ik_CV_3_point_Y_pointConstraint1.cty" "spineCurve_ik_CV_3_point_Y.ty"
		;
connectAttr "spineCurve_ik_CV_3_point_Z_pointConstraint1.ctz" "spineCurve_ik_CV_3_point_Z.tz"
		;
connectAttr "spineCurve_ik_CV_3_point_Z.pim" "spineCurve_ik_CV_3_point_Z_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_3_point_Z.rp" "spineCurve_ik_CV_3_point_Z_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_3_point_Z.rpt" "spineCurve_ik_CV_3_point_Z_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_Z.t" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_Z.rp" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_Z.rpt" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_Z.pm" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_Z_pointConstraint1.w0" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Z.t" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Z.rp" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Z.rpt" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Z.pm" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_Z_pointConstraint1.w1" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[1].tw"
		;
connectAttr "mid_ik_ctrl.t" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[2].tt"
		;
connectAttr "mid_ik_ctrl.rp" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[2].trp"
		;
connectAttr "mid_ik_ctrl.rpt" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[2].trt"
		;
connectAttr "mid_ik_ctrl.pm" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[2].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_Z_pointConstraint1.w2" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[2].tw"
		;
connectAttr "spineCurve_ik_CV_3_point_Z.blendSpace" "spineCurve_ik_CV_3_point_Z_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Z_point_reverse.ox" "spineCurve_ik_CV_3_point_Z_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_3_point_Y.pim" "spineCurve_ik_CV_3_point_Y_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_3_point_Y.rp" "spineCurve_ik_CV_3_point_Y_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_3_point_Y.rpt" "spineCurve_ik_CV_3_point_Y_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_Y.t" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_Y.rp" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_Y.rpt" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_Y.pm" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_Y_pointConstraint1.w0" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Y.t" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Y.rp" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Y.rpt" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Y.pm" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_Y_pointConstraint1.w1" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[1].tw"
		;
connectAttr "mid_ik_ctrl.t" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[2].tt"
		;
connectAttr "mid_ik_ctrl.rp" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[2].trp"
		;
connectAttr "mid_ik_ctrl.rpt" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[2].trt"
		;
connectAttr "mid_ik_ctrl.pm" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[2].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_Y_pointConstraint1.w2" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[2].tw"
		;
connectAttr "spineCurve_ik_CV_3_point_Y.blendSpace" "spineCurve_ik_CV_3_point_Y_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Y_point_reverse.ox" "spineCurve_ik_CV_3_point_Y_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_3_point_X.pim" "spineCurve_ik_CV_3_point_X_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_3_point_X.rp" "spineCurve_ik_CV_3_point_X_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_3_point_X.rpt" "spineCurve_ik_CV_3_point_X_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_X.t" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_X.rp" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_X.rpt" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_X.pm" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_X_pointConstraint1.w0" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_X.t" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_X.rp" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_X.rpt" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_X.pm" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_X_pointConstraint1.w1" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[1].tw"
		;
connectAttr "mid_ik_ctrl.t" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[2].tt"
		;
connectAttr "mid_ik_ctrl.rp" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[2].trp"
		;
connectAttr "mid_ik_ctrl.rpt" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[2].trt"
		;
connectAttr "mid_ik_ctrl.pm" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[2].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_X_pointConstraint1.w2" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[2].tw"
		;
connectAttr "spineCurve_ik_CV_3_point_X.blendSpace" "spineCurve_ik_CV_3_point_X_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_X_point_reverse.ox" "spineCurve_ik_CV_3_point_X_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_4_point_X_pointConstraint1.ctx" "spineCurve_ik_CV_4_point_X.tx"
		;
connectAttr "spineCurve_ik_CV_4_point_Y_pointConstraint1.cty" "spineCurve_ik_CV_4_point_Y.ty"
		;
connectAttr "spineCurve_ik_CV_4_point_Z_pointConstraint1.ctz" "spineCurve_ik_CV_4_point_Z.tz"
		;
connectAttr "spineCurve_ik_CV_4_point_Z.pim" "spineCurve_ik_CV_4_point_Z_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_4_point_Z.rp" "spineCurve_ik_CV_4_point_Z_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_4_point_Z.rpt" "spineCurve_ik_CV_4_point_Z_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_Z.t" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_Z.rp" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_Z.rpt" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_Z.pm" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_4_point_Z_pointConstraint1.w0" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Z.t" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Z.rp" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Z.rpt" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Z.pm" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_4_point_Z_pointConstraint1.w1" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_4_point_Z.blendSpace" "spineCurve_ik_CV_4_point_Z_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Z_point_reverse.ox" "spineCurve_ik_CV_4_point_Z_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_4_point_Y.pim" "spineCurve_ik_CV_4_point_Y_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_4_point_Y.rp" "spineCurve_ik_CV_4_point_Y_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_4_point_Y.rpt" "spineCurve_ik_CV_4_point_Y_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_Y.t" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_Y.rp" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_Y.rpt" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_Y.pm" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_4_point_Y_pointConstraint1.w0" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Y.t" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Y.rp" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Y.rpt" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Y.pm" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_4_point_Y_pointConstraint1.w1" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_4_point_Y.blendSpace" "spineCurve_ik_CV_4_point_Y_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Y_point_reverse.ox" "spineCurve_ik_CV_4_point_Y_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_4_point_X.pim" "spineCurve_ik_CV_4_point_X_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_4_point_X.rp" "spineCurve_ik_CV_4_point_X_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_4_point_X.rpt" "spineCurve_ik_CV_4_point_X_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_X.t" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_X.rp" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_X.rpt" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_X.pm" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_4_point_X_pointConstraint1.w0" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_X.t" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_X.rp" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_X.rpt" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_X.pm" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_4_point_X_pointConstraint1.w1" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_4_point_X.blendSpace" "spineCurve_ik_CV_4_point_X_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_X_point_reverse.ox" "spineCurve_ik_CV_4_point_X_pointConstraint1.w1"
		;
connectAttr "njc_top_twistCtrl_orientConstraint1.crx" "njc_top_twistCtrl.rx";
connectAttr "njc_top_twistCtrl_orientConstraint1.cry" "njc_top_twistCtrl.ry";
connectAttr "njc_top_twistCtrl_orientConstraint1.crz" "njc_top_twistCtrl.rz";
connectAttr "njc_top_twistCtrl_pointConstraint1.ctx" "njc_top_twistCtrl.tx";
connectAttr "njc_top_twistCtrl_pointConstraint1.cty" "njc_top_twistCtrl.ty";
connectAttr "njc_top_twistCtrl_pointConstraint1.ctz" "njc_top_twistCtrl.tz";
connectAttr "njc_top_twistCtrl.ro" "njc_top_twistCtrl_orientConstraint1.cro";
connectAttr "njc_top_twistCtrl.pim" "njc_top_twistCtrl_orientConstraint1.cpim";
connectAttr "spineCurve_ik_CV_5.r" "njc_top_twistCtrl_orientConstraint1.tg[0].tr"
		;
connectAttr "spineCurve_ik_CV_5.ro" "njc_top_twistCtrl_orientConstraint1.tg[0].tro"
		;
connectAttr "spineCurve_ik_CV_5.pm" "njc_top_twistCtrl_orientConstraint1.tg[0].tpm"
		;
connectAttr "njc_top_twistCtrl_orientConstraint1.w0" "njc_top_twistCtrl_orientConstraint1.tg[0].tw"
		;
connectAttr "njc_top_twistCtrl.pim" "njc_top_twistCtrl_pointConstraint1.cpim";
connectAttr "njc_top_twistCtrl.rp" "njc_top_twistCtrl_pointConstraint1.crp";
connectAttr "njc_top_twistCtrl.rpt" "njc_top_twistCtrl_pointConstraint1.crt";
connectAttr "spineCurve_ik_CV_5.t" "njc_top_twistCtrl_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_5.rp" "njc_top_twistCtrl_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_5.rpt" "njc_top_twistCtrl_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_5.pm" "njc_top_twistCtrl_pointConstraint1.tg[0].tpm"
		;
connectAttr "njc_top_twistCtrl_pointConstraint1.w0" "njc_top_twistCtrl_pointConstraint1.tg[0].tw"
		;
connectAttr "njc_bottom_twistCtrl_orientConstraint1.crx" "njc_bottom_twistCtrl.rx"
		;
connectAttr "njc_bottom_twistCtrl_orientConstraint1.cry" "njc_bottom_twistCtrl.ry"
		;
connectAttr "njc_bottom_twistCtrl_orientConstraint1.crz" "njc_bottom_twistCtrl.rz"
		;
connectAttr "njc_bottom_twistCtrl_pointConstraint1.ctx" "njc_bottom_twistCtrl.tx"
		;
connectAttr "njc_bottom_twistCtrl_pointConstraint1.cty" "njc_bottom_twistCtrl.ty"
		;
connectAttr "njc_bottom_twistCtrl_pointConstraint1.ctz" "njc_bottom_twistCtrl.tz"
		;
connectAttr "njc_bottom_twistCtrl.ro" "njc_bottom_twistCtrl_orientConstraint1.cro"
		;
connectAttr "njc_bottom_twistCtrl.pim" "njc_bottom_twistCtrl_orientConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_0.r" "njc_bottom_twistCtrl_orientConstraint1.tg[0].tr"
		;
connectAttr "spineCurve_ik_CV_0.ro" "njc_bottom_twistCtrl_orientConstraint1.tg[0].tro"
		;
connectAttr "spineCurve_ik_CV_0.pm" "njc_bottom_twistCtrl_orientConstraint1.tg[0].tpm"
		;
connectAttr "njc_bottom_twistCtrl_orientConstraint1.w0" "njc_bottom_twistCtrl_orientConstraint1.tg[0].tw"
		;
connectAttr "njc_bottom_twistCtrl.pim" "njc_bottom_twistCtrl_pointConstraint1.cpim"
		;
connectAttr "njc_bottom_twistCtrl.rp" "njc_bottom_twistCtrl_pointConstraint1.crp"
		;
connectAttr "njc_bottom_twistCtrl.rpt" "njc_bottom_twistCtrl_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_0.t" "njc_bottom_twistCtrl_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_0.rp" "njc_bottom_twistCtrl_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_0.rpt" "njc_bottom_twistCtrl_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_0.pm" "njc_bottom_twistCtrl_pointConstraint1.tg[0].tpm"
		;
connectAttr "njc_bottom_twistCtrl_pointConstraint1.w0" "njc_bottom_twistCtrl_pointConstraint1.tg[0].tw"
		;
connectAttr "pelvis_rig_pointConstraint1.ctx" "pelvis_rig.tx";
connectAttr "pelvis_rig_pointConstraint1.cty" "pelvis_rig.ty";
connectAttr "pelvis_rig_pointConstraint1.ctz" "pelvis_rig.tz";
connectAttr "pelvis_rig_orientConstraint1.crx" "pelvis_rig.rx";
connectAttr "pelvis_rig_orientConstraint1.cry" "pelvis_rig.ry";
connectAttr "pelvis_rig_orientConstraint1.crz" "pelvis_rig.rz";
connectAttr "pelvis_rig.s" "spine_01_rig.is";
connectAttr "expression5.out[0]" "spine_01_rig.sy";
connectAttr "expression5.out[1]" "spine_01_rig.sz";
connectAttr "expression1.out[0]" "spine_01_rig.tx";
connectAttr "spine_01_rig.s" "spine_02_rig.is";
connectAttr "expression6.out[0]" "spine_02_rig.sy";
connectAttr "expression6.out[1]" "spine_02_rig.sz";
connectAttr "expression2.out[0]" "spine_02_rig.tx";
connectAttr "spine_02_rig.s" "spine_03_rig.is";
connectAttr "expression7.out[0]" "spine_03_rig.sy";
connectAttr "expression7.out[1]" "spine_03_rig.sz";
connectAttr "expression3.out[0]" "spine_03_rig.tx";
connectAttr "spine_03_rig.s" "spineEnd_rig.is";
connectAttr "expression4.out[0]" "spineEnd_rig.tx";
connectAttr "spineEnd_rig.tx" "effector1.tx";
connectAttr "spineEnd_rig.ty" "effector1.ty";
connectAttr "spineEnd_rig.tz" "effector1.tz";
connectAttr "hips_rig_pointConstraint1.ctx" "hips_rig.tx";
connectAttr "hips_rig_pointConstraint1.cty" "hips_rig.ty";
connectAttr "hips_rig_pointConstraint1.ctz" "hips_rig.tz";
connectAttr "hips_rig_orientConstraint1.crx" "hips_rig.rx";
connectAttr "hips_rig_orientConstraint1.cry" "hips_rig.ry";
connectAttr "hips_rig_orientConstraint1.crz" "hips_rig.rz";
connectAttr "pelvis_rig.s" "hips_rig.is";
connectAttr "hips_rig.s" "l_hip_rig.is";
connectAttr "l_hip_rig.s" "l_knee_rig.is";
connectAttr "l_knee_rig.s" "l_ankle_rig.is";
connectAttr "l_ankle_rig.s" "l_ball_rig.is";
connectAttr "l_ball_rig.s" "l_toe_rig.is";
connectAttr "hips_rig.s" "r_hip_rig.is";
connectAttr "r_hip_rig.s" "r_knee_rig.is";
connectAttr "r_knee_rig.s" "r_ankle_rig.is";
connectAttr "r_ankle_rig.s" "r_ball_rig.is";
connectAttr "r_ball_rig.s" "r_toe_rig.is";
connectAttr "hips_rig.ro" "hips_rig_orientConstraint1.cro";
connectAttr "hips_rig.pim" "hips_rig_orientConstraint1.cpim";
connectAttr "hips_rig.jo" "hips_rig_orientConstraint1.cjo";
connectAttr "hips_rig.is" "hips_rig_orientConstraint1.is";
connectAttr "hips_ctrl.r" "hips_rig_orientConstraint1.tg[0].tr";
connectAttr "hips_ctrl.ro" "hips_rig_orientConstraint1.tg[0].tro";
connectAttr "hips_ctrl.pm" "hips_rig_orientConstraint1.tg[0].tpm";
connectAttr "hips_rig_orientConstraint1.w0" "hips_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "hips_rig.pim" "hips_rig_pointConstraint1.cpim";
connectAttr "hips_rig.rp" "hips_rig_pointConstraint1.crp";
connectAttr "hips_rig.rpt" "hips_rig_pointConstraint1.crt";
connectAttr "hips_ctrl.t" "hips_rig_pointConstraint1.tg[0].tt";
connectAttr "hips_ctrl.rp" "hips_rig_pointConstraint1.tg[0].trp";
connectAttr "hips_ctrl.rpt" "hips_rig_pointConstraint1.tg[0].trt";
connectAttr "hips_ctrl.pm" "hips_rig_pointConstraint1.tg[0].tpm";
connectAttr "hips_rig_pointConstraint1.w0" "hips_rig_pointConstraint1.tg[0].tw";
connectAttr "pelvis_rig.pim" "pelvis_rig_pointConstraint1.cpim";
connectAttr "pelvis_rig.rp" "pelvis_rig_pointConstraint1.crp";
connectAttr "pelvis_rig.rpt" "pelvis_rig_pointConstraint1.crt";
connectAttr "pelvis_ctrl.t" "pelvis_rig_pointConstraint1.tg[0].tt";
connectAttr "pelvis_ctrl.rp" "pelvis_rig_pointConstraint1.tg[0].trp";
connectAttr "pelvis_ctrl.rpt" "pelvis_rig_pointConstraint1.tg[0].trt";
connectAttr "pelvis_ctrl.pm" "pelvis_rig_pointConstraint1.tg[0].tpm";
connectAttr "pelvis_rig_pointConstraint1.w0" "pelvis_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "pelvis_rig.ro" "pelvis_rig_orientConstraint1.cro";
connectAttr "pelvis_rig.pim" "pelvis_rig_orientConstraint1.cpim";
connectAttr "pelvis_rig.jo" "pelvis_rig_orientConstraint1.cjo";
connectAttr "pelvis_rig.is" "pelvis_rig_orientConstraint1.is";
connectAttr "pelvis_ctrl.r" "pelvis_rig_orientConstraint1.tg[0].tr";
connectAttr "pelvis_ctrl.ro" "pelvis_rig_orientConstraint1.tg[0].tro";
connectAttr "pelvis_ctrl.pm" "pelvis_rig_orientConstraint1.tg[0].tpm";
connectAttr "pelvis_rig_orientConstraint1.w0" "pelvis_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "spineParent_rig_pointConstraint1.ctx" "spineParent_rig.tx";
connectAttr "spineParent_rig_pointConstraint1.cty" "spineParent_rig.ty";
connectAttr "spineParent_rig_pointConstraint1.ctz" "spineParent_rig.tz";
connectAttr "spineParent_rig_orientConstraint1.crx" "spineParent_rig.rx";
connectAttr "spineParent_rig_orientConstraint1.cry" "spineParent_rig.ry";
connectAttr "spineParent_rig_orientConstraint1.crz" "spineParent_rig.rz";
connectAttr "spineParent_rig.s" "neckBase_rig.is";
connectAttr "neckBase_rig.s" "neck_mid_rig.is";
connectAttr "neck_mid_rig.s" "neck_rig.is";
connectAttr "neck_rig.s" "l_eye_rig.is";
connectAttr "neck_rig.s" "r_eye_rig.is";
connectAttr "neck_rig.s" "jaw_rig.is";
connectAttr "spineParent_rig.s" "l_clav_rig.is";
connectAttr "l_clav_rig.s" "l_shoulder_rig.is";
connectAttr "l_shoulder_rig.s" "l_elbow_rig.is";
connectAttr "l_elbow_rig.s" "l_wrist_rig.is";
connectAttr "l_wrist_rig.s" "l_index_01_rig.is";
connectAttr "l_index_01_rig.s" "l_index_02_rig.is";
connectAttr "l_index_02_rig.s" "l_index_03_rig.is";
connectAttr "l_wrist_rig.s" "l_pinky_01_rig.is";
connectAttr "l_pinky_01_rig.s" "l_pinky_02_rig.is";
connectAttr "l_pinky_02_rig.s" "l_pinky_03_rig.is";
connectAttr "l_wrist_rig.s" "l_thumb_01_rig.is";
connectAttr "l_thumb_01_rig.s" "l_thumb_02_rig.is";
connectAttr "l_thumb_02_rig.s" "l_thumb_03_rig.is";
connectAttr "spineParent_rig.s" "r_clav_rig.is";
connectAttr "r_clav_rig.s" "r_shoulder_rig.is";
connectAttr "r_shoulder_rig.s" "r_elbow_rig.is";
connectAttr "r_elbow_rig.s" "r_wrist_rig.is";
connectAttr "r_wrist_rig.s" "r_index_01_rig.is";
connectAttr "r_index_01_rig.s" "r_index_02_rig.is";
connectAttr "r_index_02_rig.s" "r_index_03_rig.is";
connectAttr "r_wrist_rig.s" "r_pinky_01_rig.is";
connectAttr "r_pinky_01_rig.s" "r_pinky_02_rig.is";
connectAttr "r_pinky_02_rig.s" "r_pinky_03_rig.is";
connectAttr "r_wrist_rig.s" "r_thumb_01_rig.is";
connectAttr "r_thumb_01_rig.s" "r_thumb_02_rig.is";
connectAttr "r_thumb_02_rig.s" "r_thumb_03_rig.is";
connectAttr "spineParent_rig.pim" "spineParent_rig_pointConstraint1.cpim";
connectAttr "spineParent_rig.rp" "spineParent_rig_pointConstraint1.crp";
connectAttr "spineParent_rig.rpt" "spineParent_rig_pointConstraint1.crt";
connectAttr "topSpine_ctrl.t" "spineParent_rig_pointConstraint1.tg[0].tt";
connectAttr "topSpine_ctrl.rp" "spineParent_rig_pointConstraint1.tg[0].trp";
connectAttr "topSpine_ctrl.rpt" "spineParent_rig_pointConstraint1.tg[0].trt";
connectAttr "topSpine_ctrl.pm" "spineParent_rig_pointConstraint1.tg[0].tpm";
connectAttr "spineParent_rig_pointConstraint1.w0" "spineParent_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "spineParent_rig.ro" "spineParent_rig_orientConstraint1.cro";
connectAttr "spineParent_rig.pim" "spineParent_rig_orientConstraint1.cpim";
connectAttr "spineParent_rig.jo" "spineParent_rig_orientConstraint1.cjo";
connectAttr "spineParent_rig.is" "spineParent_rig_orientConstraint1.is";
connectAttr "topSpine_ctrl.r" "spineParent_rig_orientConstraint1.tg[0].tr";
connectAttr "topSpine_ctrl.ro" "spineParent_rig_orientConstraint1.tg[0].tro";
connectAttr "topSpine_ctrl.pm" "spineParent_rig_orientConstraint1.tg[0].tpm";
connectAttr "spineParent_rig_orientConstraint1.w0" "spineParent_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "worldPlacement.midIkCtrl" "mid_ik_ctrl.v" -l on;
connectAttr "pelvis.s" "spine_01.is";
connectAttr "spine_01.s" "spine_02.is";
connectAttr "spine_02.s" "spine_03.is";
connectAttr "spine_03.s" "spineEnd.is";
connectAttr "spineEnd.s" "l_clav.is";
connectAttr "l_clav.s" "l_shoulder.is";
connectAttr "l_shoulder.s" "l_elbow.is";
connectAttr "l_elbow.s" "l_wrist.is";
connectAttr "l_wrist.s" "l_index_01.is";
connectAttr "l_index_01.s" "l_index_02.is";
connectAttr "l_index_02.s" "l_index_03.is";
connectAttr "l_wrist.s" "l_pinky_01.is";
connectAttr "l_pinky_01.s" "l_pinky_02.is";
connectAttr "l_pinky_02.s" "l_pinky_03.is";
connectAttr "l_wrist.s" "l_thumb_01.is";
connectAttr "l_thumb_01.s" "l_thumb_02.is";
connectAttr "l_thumb_02.s" "l_thumb_03.is";
connectAttr "spineEnd.s" "neckBase.is";
connectAttr "neckBase_scaleConstraint1.csx" "neckBase.sx";
connectAttr "neckBase_scaleConstraint1.csy" "neckBase.sy";
connectAttr "neckBase_scaleConstraint1.csz" "neckBase.sz";
connectAttr "neckBase_pointConstraint1.ctx" "neckBase.tx";
connectAttr "neckBase_pointConstraint1.cty" "neckBase.ty";
connectAttr "neckBase_pointConstraint1.ctz" "neckBase.tz";
connectAttr "neckBase_orientConstraint1.crx" "neckBase.rx";
connectAttr "neckBase_orientConstraint1.cry" "neckBase.ry";
connectAttr "neckBase_orientConstraint1.crz" "neckBase.rz";
connectAttr "neckBase.s" "neck.is";
connectAttr "neck_scaleConstraint1.csx" "neck.sx";
connectAttr "neck_scaleConstraint1.csy" "neck.sy";
connectAttr "neck_scaleConstraint1.csz" "neck.sz";
connectAttr "neck_pointConstraint1.ctx" "neck.tx";
connectAttr "neck_pointConstraint1.cty" "neck.ty";
connectAttr "neck_pointConstraint1.ctz" "neck.tz";
connectAttr "neck_orientConstraint1.crx" "neck.rx";
connectAttr "neck_orientConstraint1.cry" "neck.ry";
connectAttr "neck_orientConstraint1.crz" "neck.rz";
connectAttr "neck.s" "l_eye.is";
connectAttr "neck.s" "jaw.is";
connectAttr "neck.s" "r_eye.is";
connectAttr "neck.pim" "neck_pointConstraint1.cpim";
connectAttr "neck.rp" "neck_pointConstraint1.crp";
connectAttr "neck.rpt" "neck_pointConstraint1.crt";
connectAttr "neck_rig.t" "neck_pointConstraint1.tg[0].tt";
connectAttr "neck_rig.rp" "neck_pointConstraint1.tg[0].trp";
connectAttr "neck_rig.rpt" "neck_pointConstraint1.tg[0].trt";
connectAttr "neck_rig.pm" "neck_pointConstraint1.tg[0].tpm";
connectAttr "neck_pointConstraint1.w0" "neck_pointConstraint1.tg[0].tw";
connectAttr "neck.ro" "neck_orientConstraint1.cro";
connectAttr "neck.pim" "neck_orientConstraint1.cpim";
connectAttr "neck.jo" "neck_orientConstraint1.cjo";
connectAttr "neck.is" "neck_orientConstraint1.is";
connectAttr "neck_rig.r" "neck_orientConstraint1.tg[0].tr";
connectAttr "neck_rig.ro" "neck_orientConstraint1.tg[0].tro";
connectAttr "neck_rig.pm" "neck_orientConstraint1.tg[0].tpm";
connectAttr "neck_rig.jo" "neck_orientConstraint1.tg[0].tjo";
connectAttr "neck_orientConstraint1.w0" "neck_orientConstraint1.tg[0].tw";
connectAttr "neck.ssc" "neck_scaleConstraint1.tsc";
connectAttr "neck.pim" "neck_scaleConstraint1.cpim";
connectAttr "neck_rig.s" "neck_scaleConstraint1.tg[0].ts";
connectAttr "neck_rig.pm" "neck_scaleConstraint1.tg[0].tpm";
connectAttr "neck_scaleConstraint1.w0" "neck_scaleConstraint1.tg[0].tw";
connectAttr "neckBase.pim" "neckBase_pointConstraint1.cpim";
connectAttr "neckBase.rp" "neckBase_pointConstraint1.crp";
connectAttr "neckBase.rpt" "neckBase_pointConstraint1.crt";
connectAttr "neckBase_rig.t" "neckBase_pointConstraint1.tg[0].tt";
connectAttr "neckBase_rig.rp" "neckBase_pointConstraint1.tg[0].trp";
connectAttr "neckBase_rig.rpt" "neckBase_pointConstraint1.tg[0].trt";
connectAttr "neckBase_rig.pm" "neckBase_pointConstraint1.tg[0].tpm";
connectAttr "neckBase_pointConstraint1.w0" "neckBase_pointConstraint1.tg[0].tw";
connectAttr "neckBase.ro" "neckBase_orientConstraint1.cro";
connectAttr "neckBase.pim" "neckBase_orientConstraint1.cpim";
connectAttr "neckBase.jo" "neckBase_orientConstraint1.cjo";
connectAttr "neckBase.is" "neckBase_orientConstraint1.is";
connectAttr "neckBase_rig.r" "neckBase_orientConstraint1.tg[0].tr";
connectAttr "neckBase_rig.ro" "neckBase_orientConstraint1.tg[0].tro";
connectAttr "neckBase_rig.pm" "neckBase_orientConstraint1.tg[0].tpm";
connectAttr "neckBase_rig.jo" "neckBase_orientConstraint1.tg[0].tjo";
connectAttr "neckBase_orientConstraint1.w0" "neckBase_orientConstraint1.tg[0].tw"
		;
connectAttr "neckBase.ssc" "neckBase_scaleConstraint1.tsc";
connectAttr "neckBase.pim" "neckBase_scaleConstraint1.cpim";
connectAttr "neckBase_rig.s" "neckBase_scaleConstraint1.tg[0].ts";
connectAttr "neckBase_rig.pm" "neckBase_scaleConstraint1.tg[0].tpm";
connectAttr "neckBase_scaleConstraint1.w0" "neckBase_scaleConstraint1.tg[0].tw";
connectAttr "spineEnd.s" "r_clav.is";
connectAttr "r_clav.s" "r_shoulder.is";
connectAttr "r_shoulder.s" "r_elbow.is";
connectAttr "r_elbow.s" "r_wrist.is";
connectAttr "r_wrist.s" "r_index_01.is";
connectAttr "r_index_01.s" "r_index_02.is";
connectAttr "r_index_02.s" "r_index_03.is";
connectAttr "r_wrist.s" "r_pinky_01.is";
connectAttr "r_pinky_01.s" "r_pinky_02.is";
connectAttr "r_pinky_02.s" "r_pinky_03.is";
connectAttr "r_wrist.s" "r_thumb_01.is";
connectAttr "r_thumb_01.s" "r_thumb_02.is";
connectAttr "r_thumb_02.s" "r_thumb_03.is";
connectAttr "pelvis.s" "hips.is";
connectAttr "hips.s" "l_hip.is";
connectAttr "l_hip.s" "l_knee.is";
connectAttr "l_knee.s" "l_ankle.is";
connectAttr "l_ankle.s" "l_ball.is";
connectAttr "l_ball.s" "l_toe.is";
connectAttr "hips.s" "r_hip.is";
connectAttr "r_hip.s" "r_knee.is";
connectAttr "r_knee.s" "r_ankle.is";
connectAttr "r_ankle.s" "r_ball.is";
connectAttr "r_ball.s" "r_toe.is";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "layerManager.dli[1]" "layer1.id";
connectAttr "cluster1GroupParts.og" "cluster1.ip[0].ig";
connectAttr "cluster1GroupId.id" "cluster1.ip[0].gi";
connectAttr "spineCurve_ik_CV_0.wm" "cluster1.ma";
connectAttr "|worldPlacement|rig_skeleton|pelvis_rig|hips_rig|spineCurve_ik_CV_0|clusterHandleShape.x" "cluster1.x"
		;
connectAttr "groupParts2.og" "tweak1.ip[0].ig";
connectAttr "groupId2.id" "tweak1.ip[0].gi";
connectAttr "cluster1GroupId.msg" "cluster1Set.gn" -na;
connectAttr "spineCurve_ikShape.iog.og[0]" "cluster1Set.dsm" -na;
connectAttr "cluster1.msg" "cluster1Set.ub[0]";
connectAttr "tweak1.og[0]" "cluster1GroupParts.ig";
connectAttr "cluster1GroupId.id" "cluster1GroupParts.gi";
connectAttr "groupId2.msg" "tweakSet1.gn" -na;
connectAttr "spineCurve_ikShape.iog.og[1]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "spineCurve_ikShapeOrig.ws" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "cluster2GroupParts.og" "cluster2.ip[0].ig";
connectAttr "cluster2GroupId.id" "cluster2.ip[0].gi";
connectAttr "spineCurve_ik_CV_1.wm" "cluster2.ma";
connectAttr "|DoNotDeleteRigNodes|spineExtra|spineCurve_ik_CV_1_point_X|spineCurve_ik_CV_1_point_Y|spineCurve_ik_CV_1_point_Z|spineCurve_ik_CV_1|clusterHandleShape.x" "cluster2.x"
		;
connectAttr "cluster2GroupId.msg" "cluster2Set.gn" -na;
connectAttr "spineCurve_ikShape.iog.og[2]" "cluster2Set.dsm" -na;
connectAttr "cluster2.msg" "cluster2Set.ub[0]";
connectAttr "cluster1.og[0]" "cluster2GroupParts.ig";
connectAttr "cluster2GroupId.id" "cluster2GroupParts.gi";
connectAttr "cluster3GroupParts.og" "cluster3.ip[0].ig";
connectAttr "cluster3GroupId.id" "cluster3.ip[0].gi";
connectAttr "spineCurve_ik_CV_2.wm" "cluster3.ma";
connectAttr "|DoNotDeleteRigNodes|spineExtra|spineCurve_ik_CV_2_point_X|spineCurve_ik_CV_2_point_Y|spineCurve_ik_CV_2_point_Z|spineCurve_ik_CV_2|clusterHandleShape.x" "cluster3.x"
		;
connectAttr "cluster3GroupId.msg" "cluster3Set.gn" -na;
connectAttr "spineCurve_ikShape.iog.og[3]" "cluster3Set.dsm" -na;
connectAttr "cluster3.msg" "cluster3Set.ub[0]";
connectAttr "cluster2.og[0]" "cluster3GroupParts.ig";
connectAttr "cluster3GroupId.id" "cluster3GroupParts.gi";
connectAttr "cluster4GroupParts.og" "cluster4.ip[0].ig";
connectAttr "cluster4GroupId.id" "cluster4.ip[0].gi";
connectAttr "spineCurve_ik_CV_3.wm" "cluster4.ma";
connectAttr "|DoNotDeleteRigNodes|spineExtra|spineCurve_ik_CV_3_point_X|spineCurve_ik_CV_3_point_Y|spineCurve_ik_CV_3_point_Z|spineCurve_ik_CV_3|clusterHandleShape.x" "cluster4.x"
		;
connectAttr "cluster4GroupId.msg" "cluster4Set.gn" -na;
connectAttr "spineCurve_ikShape.iog.og[4]" "cluster4Set.dsm" -na;
connectAttr "cluster4.msg" "cluster4Set.ub[0]";
connectAttr "cluster3.og[0]" "cluster4GroupParts.ig";
connectAttr "cluster4GroupId.id" "cluster4GroupParts.gi";
connectAttr "cluster5GroupParts.og" "cluster5.ip[0].ig";
connectAttr "cluster5GroupId.id" "cluster5.ip[0].gi";
connectAttr "spineCurve_ik_CV_4.wm" "cluster5.ma";
connectAttr "|DoNotDeleteRigNodes|spineExtra|spineCurve_ik_CV_4_point_X|spineCurve_ik_CV_4_point_Y|spineCurve_ik_CV_4_point_Z|spineCurve_ik_CV_4|clusterHandleShape.x" "cluster5.x"
		;
connectAttr "cluster5GroupId.msg" "cluster5Set.gn" -na;
connectAttr "spineCurve_ikShape.iog.og[5]" "cluster5Set.dsm" -na;
connectAttr "cluster5.msg" "cluster5Set.ub[0]";
connectAttr "cluster4.og[0]" "cluster5GroupParts.ig";
connectAttr "cluster5GroupId.id" "cluster5GroupParts.gi";
connectAttr "cluster6GroupParts.og" "cluster6.ip[0].ig";
connectAttr "cluster6GroupId.id" "cluster6.ip[0].gi";
connectAttr "spineCurve_ik_CV_5.wm" "cluster6.ma";
connectAttr "|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|spineCurve_ik_CV_5|clusterHandleShape.x" "cluster6.x"
		;
connectAttr "cluster6GroupId.msg" "cluster6Set.gn" -na;
connectAttr "spineCurve_ikShape.iog.og[6]" "cluster6Set.dsm" -na;
connectAttr "cluster6.msg" "cluster6Set.ub[0]";
connectAttr "cluster5.og[0]" "cluster6GroupParts.ig";
connectAttr "cluster6GroupId.id" "cluster6GroupParts.gi";
connectAttr "spineCurve_ik_CV_1_point_X.blendSpace" "spineCurve_ik_CV_1_point_blend_X_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_1_point_Y.blendSpace" "spineCurve_ik_CV_1_point_blend_Y_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_1_point_Z.blendSpace" "spineCurve_ik_CV_1_point_blend_Z_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_2_point_X.blendSpace" "spineCurve_ik_CV_2_point_blend_X_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_2_point_Y.blendSpace" "spineCurve_ik_CV_2_point_blend_Y_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_2_point_Z.blendSpace" "spineCurve_ik_CV_2_point_blend_Z_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_3_point_X.blendSpace" "spineCurve_ik_CV_3_point_blend_X_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_3_point_Y.blendSpace" "spineCurve_ik_CV_3_point_blend_Y_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_3_point_Z.blendSpace" "spineCurve_ik_CV_3_point_blend_Z_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_4_point_X.blendSpace" "spineCurve_ik_CV_4_point_blend_X_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_4_point_Y.blendSpace" "spineCurve_ik_CV_4_point_blend_Y_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_4_point_Z.blendSpace" "spineCurve_ik_CV_4_point_blend_Z_point_reverse.ix"
		;
connectAttr "njcArcLengthNode.al" "njcMultiSpineStre.i1x";
connectAttr "njcRigScaleSpineMulti.ox" "njcMultiSpineStre.i2x";
connectAttr "worldPlacement.sy" "njcRigScaleSpineMulti.i1x";
connectAttr "spineCurve_ikShape.ws" "njcArcLengthNode.ic";
connectAttr "njcMultiSpineStre.ox" "expression1.in[0]";
connectAttr ":time1.o" "expression1.tim";
connectAttr "spine_01_rig.msg" "expression1.obm";
connectAttr "njcMultiSpineStre.ox" "expression2.in[0]";
connectAttr ":time1.o" "expression2.tim";
connectAttr "spine_02_rig.msg" "expression2.obm";
connectAttr "njcMultiSpineStre.ox" "expression3.in[0]";
connectAttr ":time1.o" "expression3.tim";
connectAttr "spine_03_rig.msg" "expression3.obm";
connectAttr "njcMultiSpineStre.ox" "expression4.in[0]";
connectAttr ":time1.o" "expression4.tim";
connectAttr "spineEnd_rig.msg" "expression4.obm";
connectAttr "topSpine_ctrl.ty" "expression5.in[0]";
connectAttr "topSpine_ctrl.spineScale" "expression5.in[1]";
connectAttr ":time1.o" "expression5.tim";
connectAttr "spine_01_rig.msg" "expression5.obm";
connectAttr "topSpine_ctrl.ty" "expression6.in[0]";
connectAttr "topSpine_ctrl.spineScale" "expression6.in[1]";
connectAttr ":time1.o" "expression6.tim";
connectAttr "spine_02_rig.msg" "expression6.obm";
connectAttr "topSpine_ctrl.ty" "expression7.in[0]";
connectAttr "topSpine_ctrl.spineScale" "expression7.in[1]";
connectAttr ":time1.o" "expression7.tim";
connectAttr "spine_03_rig.msg" "expression7.obm";
connectAttr "spineCurve_ik_CV_1_point_blend_X_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_1_point_blend_Y_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_1_point_blend_Z_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_2_point_blend_X_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_2_point_blend_Y_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_2_point_blend_Z_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_3_point_blend_X_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_3_point_blend_Y_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_3_point_blend_Z_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_4_point_blend_X_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_4_point_blend_Y_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_4_point_blend_Z_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "njcMultiSpineStre.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "njcRigScaleSpineMulti.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "ikSplineSolver.msg" ":ikSystem.sol" -na;
// End of Grunt_Skeleton_Rig.ma
