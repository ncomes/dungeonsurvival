//Maya ASCII 2016 scene
//Name: Grunt_Skeleton_Rig.ma
//Last modified: Sat, Nov 21, 2015 11:51:02 AM
//Codeset: 1252
file -rdi 1 -dns -rpr "Grunt_Skeleton_skin" -rfn "Grunt_Skeleton_skinRN" -typ
		 "mayaAscii" "C:/Users/Nathan/Documents/dungeonsurvival/SourceContent/Enemies/Grunt/Rig/Grunt_Skeleton_skin.ma";
file -r -dns -rpr "Grunt_Skeleton_skin" -dr 1 -rfn "Grunt_Skeleton_skinRN" -typ "mayaAscii"
		 "C:/Users/Nathan/Documents/dungeonsurvival/SourceContent/Enemies/Grunt/Rig/Grunt_Skeleton_skin.ma";
requires maya "2016";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "E612F34B-4031-3A31-1377-888F3DBFFC2D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 136.44250116212771 158.11100704398348 349.65923279653651 ;
	setAttr ".r" -type "double3" -12.938352729582174 20.199999999997409 4.236252433863765e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "B171B057-4BDD-F5CA-1235-5AA4698C632F";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 411.82623366345155;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "CF635E4C-4694-5FD3-9ECC-50945A4579CC";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "72493A34-4A0C-7DE1-6A78-81B76ACF56EC";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "CF2FED04-4C22-6C38-93AB-F4B497FF7DB9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.6548747699405579 68.518733738219964 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "88E01086-454E-0BE2-5EC3-37AB794EC97A";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 66.616692776272501;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "5F3084C0-4A52-AF50-A8E4-92BA39B9CBDA";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "9CF135E0-442C-C30D-5907-A59A1FC5A491";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "DoNotDeleteRigNodes";
	rename -uid "AE18D2D2-4D02-1644-7499-27B6C0DD6E47";
	addAttr -ci true -sn "version" -ln "version" -dt "string";
	addAttr -ci true -sn "rigNode" -ln "rigNode" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".version" -type "string" "3.0";
createNode transform -n "rigInfoNode" -p "DoNotDeleteRigNodes";
	rename -uid "A93A6115-43EA-A309-52A2-66B8DF0719B0";
	addAttr -ci true -sn "scaleInfo" -ln "scaleInfo" -at "double";
	addAttr -ci true -sn "bindJoints" -ln "bindJoints" -at "double";
	addAttr -ci true -sn "rigIfNode" -ln "rigIfNode" -at "double";
	addAttr -ci true -sn "rigName" -ln "rigName" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".scaleInfo" 4;
	setAttr ".rigName" -type "string" "";
createNode transform -n "l_elbow_BlendSpace_group" -p "rigInfoNode";
	rename -uid "917FDC82-4D4E-D1C0-B3E4-C8AF3AB083F6";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "l_elbow_BlendSpace" -ln "l_elbow_BlendSpace" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr ".rp" -type "double3" 38.129497817584522 91.607147741701112 -7.8008882127965133 ;
	setAttr ".sp" -type "double3" 38.129497817584522 91.607147741701112 -7.8008882127965133 ;
	setAttr -k on ".blendSpace";
createNode pointConstraint -n "l_elbow_BlendSpace_group_pointConstraint1" -p "l_elbow_BlendSpace_group";
	rename -uid "5CE7F4C5-4765-2CEE-53BB-C9964483F1D4";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_elbow_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "l_elbow_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -7.1054273576010019e-015 2.8421709430404007e-014 
		0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "l_elbow_BlendSpace_group_orientConstraint1" -p "l_elbow_BlendSpace_group";
	rename -uid "F6B6525E-4549-F4D9-02ED-6DA1779E6253";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_elbow_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "l_elbow_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 5.4960787232943847e-005 -15.070317944995477 1.4551771796409553e-006 ;
	setAttr ".rsrr" -type "double3" 5.4960787232943847e-005 -15.070317944995477 1.4551771796409566e-006 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "r_elbow_BlendSpace_group" -p "rigInfoNode";
	rename -uid "9C053D13-4C2A-68A5-326C-829863297F72";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "r_elbow_BlendSpace" -ln "r_elbow_BlendSpace" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr ".rp" -type "double3" -38.129499999999986 91.607099999999875 -7.8008900000001073 ;
	setAttr ".sp" -type "double3" -38.129499999999986 91.607099999999875 -7.8008900000001073 ;
	setAttr -k on ".blendSpace";
createNode pointConstraint -n "r_elbow_BlendSpace_group_pointConstraint1" -p "r_elbow_BlendSpace_group";
	rename -uid "8C30B9F2-44C8-30E2-C1F9-8B9A011BA3EB";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_elbow_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "r_elbow_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 2.1316282072803006e-014 -1.4210854715202004e-014 
		1.7763568394002505e-015 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "r_elbow_BlendSpace_group_orientConstraint1" -p "r_elbow_BlendSpace_group";
	rename -uid "31A36E08-41C2-41E8-665C-B68CF7C88D03";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_elbow_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "r_elbow_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -179.99994503921221 15.070320447968568 -1.4551742251862272e-006 ;
	setAttr ".rsrr" -type "double3" -179.99994503921283 15.070317944995075 -1.4551767116718646e-006 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "distNodes" -p "DoNotDeleteRigNodes";
	rename -uid "FD7B727A-4F61-853C-4B41-AA96CF567581";
	addAttr -ci true -sn "distNode" -ln "distNode" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "l_armLocUp" -p "distNodes";
	rename -uid "C8BFF6C6-40F6-C6B1-57F8-06BD64509A3A";
	setAttr ".v" no;
createNode locator -n "l_armLocUpShape" -p "l_armLocUp";
	rename -uid "ED76E17A-4C12-7738-FE5A-F38951B8DB3A";
	setAttr -k off ".v";
createNode pointConstraint -n "l_armLocUp_pointConstraint1" -p "l_armLocUp";
	rename -uid "BF01D11D-4C34-ADC1-738D-71AD893E3669";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_shoulder_rig_ikW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 19.175606172091548 91.607147240360064 -8.7395095853443525 ;
	setAttr -k on ".w0";
createNode transform -n "r_armLocUp" -p "distNodes";
	rename -uid "710B224E-436B-DFD0-A005-DC9264012CB7";
	setAttr ".v" no;
createNode locator -n "r_armLocUpShape" -p "r_armLocUp";
	rename -uid "E7B53C85-4EA2-5510-B9DC-B4A957EFE28A";
	setAttr -k off ".v";
createNode pointConstraint -n "r_armLocUp_pointConstraint1" -p "r_armLocUp";
	rename -uid "552E4DDA-454D-C750-59EF-908091636EC3";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_shoulder_rig_ikW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -19.175599999999996 91.607099999999932 -8.7395100000000419 ;
	setAttr -k on ".w0";
createNode transform -n "l_legLocUp" -p "distNodes";
	rename -uid "2D84DA63-4F5A-DA91-8AF5-13AD9DEB3054";
	setAttr ".v" no;
createNode locator -n "l_legLocUpShape" -p "l_legLocUp";
	rename -uid "4810DCC8-412B-8247-164E-118298D818EC";
	setAttr -k off ".v";
createNode pointConstraint -n "l_legLocUp_pointConstraint1" -p "l_legLocUp";
	rename -uid "0A7B5E1A-4F44-6F88-4AD1-6EAB09AEAC8F";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_hip_rig_ikW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 10.901285582461378 47.456432145150998 -6.5619424465784242 ;
	setAttr -k on ".w0";
createNode transform -n "r_legLocUp" -p "distNodes";
	rename -uid "DDD384EE-49ED-92EB-8495-82A72DA67E38";
	setAttr ".v" no;
createNode locator -n "r_legLocUpShape" -p "r_legLocUp";
	rename -uid "A3311663-47D4-41DE-4900-D0B648719303";
	setAttr -k off ".v";
createNode pointConstraint -n "r_legLocUp_pointConstraint1" -p "r_legLocUp";
	rename -uid "13473DDA-455B-3F32-262A-B691F7628241";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_hip_rig_ikW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -10.901299999999997 47.456399999999988 -6.56194 ;
	setAttr -k on ".w0";
createNode transform -n "fingerNodes" -p "DoNotDeleteRigNodes";
	rename -uid "FC872A3B-41E9-EF1B-DF49-D08A97B0D1E7";
	addAttr -ci true -sn "indexFinger" -ln "indexFinger" -at "double";
	addAttr -ci true -sn "middleFinger" -ln "middleFinger" -at "double";
	addAttr -ci true -sn "ringFinger" -ln "ringFinger" -at "double";
	addAttr -ci true -sn "pinkyFinger" -ln "pinkyFinger" -at "double";
	addAttr -ci true -sn "thumbFinger" -ln "thumbFinger" -at "double";
	addAttr -ci true -sn "FkHand" -ln "FkHand" -at "double";
	addAttr -ci true -sn "IkHand" -ln "IkHand" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".indexFinger" 1;
	setAttr ".pinkyFinger" 1;
	setAttr ".thumbFinger" 1;
createNode transform -n "selections" -p "DoNotDeleteRigNodes";
	rename -uid "D881F244-4324-A90B-6D2F-C8B81ADE8305";
	addAttr -ci true -sn "jntLeftRight" -ln "jntLeftRight" -at "double";
	addAttr -ci true -sn "skeleton" -ln "skeleton" -at "double";
	addAttr -ci true -sn "propCB" -ln "propCB" -at "double";
	addAttr -ci true -sn "autoOri" -ln "autoOri" -at "double";
	addAttr -ci true -sn "mirrorJnts" -ln "mirrorJnts" -at "double";
	addAttr -ci true -sn "quickRig" -ln "quickRig" -at "double";
	addAttr -ci true -sn "quickRigBind" -ln "quickRigBind" -at "double";
	addAttr -ci true -sn "import" -ln "import" -at "double";
	addAttr -ci true -sn "startRig" -ln "startRig" -at "double";
	addAttr -ci true -sn "connectJoints" -ln "connectJoints" -at "double";
	addAttr -ci true -sn "armRollUpNum" -ln "armRollUpNum" -at "double";
	addAttr -ci true -sn "legRollUpNum" -ln "legRollUpNum" -at "double";
	addAttr -ci true -sn "armRollDownNum" -ln "armRollDownNum" -at "double";
	addAttr -ci true -sn "legRollDownNum" -ln "legRollDownNum" -at "double";
	addAttr -ci true -sn "upArmRoll" -ln "upArmRoll" -at "double";
	addAttr -ci true -sn "downArmRoll" -ln "downArmRoll" -at "double";
	addAttr -ci true -sn "upLegRoll" -ln "upLegRoll" -at "double";
	addAttr -ci true -sn "downLegRoll" -ln "downLegRoll" -at "double";
	addAttr -ci true -sn "rollArmTB" -ln "rollArmTB" -at "double";
	addAttr -ci true -sn "rollLegTB" -ln "rollLegTB" -at "double";
	addAttr -ci true -sn "IkSpineCntl" -ln "IkSpineCntl" -at "double";
	addAttr -ci true -sn "IkSpine" -ln "IkSpine" -at "double";
	addAttr -ci true -sn "FkSpineCntl" -ln "FkSpineCntl" -at "double";
	addAttr -ci true -sn "FkSpine" -ln "FkSpine" -at "double";
	addAttr -ci true -sn "FkSpineCntlNum" -ln "FkSpineCntlNum" -at "double";
	addAttr -ci true -sn "stretchScale" -ln "stretchScale" -at "double";
	addAttr -ci true -sn "rootColor" -ln "rootColor" -at "double";
	addAttr -ci true -sn "hipsColor" -ln "hipsColor" -at "double";
	addAttr -ci true -sn "FkSpineColor" -ln "FkSpineColor" -at "double";
	addAttr -ci true -sn "shoulderColor" -ln "shoulderColor" -at "double";
	addAttr -ci true -sn "LegIkFkBoth" -ln "LegIkFkBoth" -at "double";
	addAttr -ci true -sn "legStretchy" -ln "legStretchy" -at "double";
	addAttr -ci true -sn "footCtrl" -ln "footCtrl" -at "double";
	addAttr -ci true -sn "legOriCtrl" -ln "legOriCtrl" -at "double";
	addAttr -ci true -sn "legPoiCtrl" -ln "legPoiCtrl" -at "double";
	addAttr -ci true -sn "createLegCtrl" -ln "createLegCtrl" -at "double";
	addAttr -ci true -sn "bindLegCtrl" -ln "bindLegCtrl" -at "double";
	addAttr -ci true -sn "legColorLeft" -ln "legColorLeft" -at "double";
	addAttr -ci true -sn "legColorRight" -ln "legColorRight" -at "double";
	addAttr -ci true -sn "armIkFkBoth" -ln "armIkFkBoth" -at "double";
	addAttr -ci true -sn "armStretchy" -ln "armStretchy" -at "double";
	addAttr -ci true -sn "armOriCtrl" -ln "armOriCtrl" -at "double";
	addAttr -ci true -sn "armPoiCtrl" -ln "armPoiCtrl" -at "double";
	addAttr -ci true -sn "clavOriCtrl" -ln "clavOriCtrl" -at "double";
	addAttr -ci true -sn "clavPoiCtrl" -ln "clavPoiCtrl" -at "double";
	addAttr -ci true -sn "createArmCtrl" -ln "createArmCtrl" -at "double";
	addAttr -ci true -sn "bindArmCtrl" -ln "bindArmCtrl" -at "double";
	addAttr -ci true -sn "armColorLeft" -ln "armColorLeft" -at "double";
	addAttr -ci true -sn "armColorRight" -ln "armColorRight" -at "double";
	addAttr -ci true -sn "handIKFKCBG" -ln "handIKFKCBG" -at "double";
	addAttr -ci true -sn "createHandCtrl" -ln "createHandCtrl" -at "double";
	addAttr -ci true -sn "bindHandCtrl" -ln "bindHandCtrl" -at "double";
	addAttr -ci true -sn "handColorLeft" -ln "handColorLeft" -at "double";
	addAttr -ci true -sn "handColorRight" -ln "handColorRight" -at "double";
	addAttr -ci true -sn "createHeadCtrl" -ln "createHeadCtrl" -at "double";
	addAttr -ci true -sn "bindHeadCtrl" -ln "bindHeadCtrl" -at "double";
	addAttr -ci true -sn "cEyesCtrl" -ln "cEyesCtrl" -at "double";
	addAttr -ci true -sn "cJawCtrl" -ln "cJawCtrl" -at "double";
	addAttr -ci true -sn "nColor" -ln "nColor" -at "double";
	addAttr -ci true -sn "nbColor" -ln "nbColor" -at "double";
	addAttr -ci true -sn "mEyeColor" -ln "mEyeColor" -at "double";
	addAttr -ci true -sn "lEyeColor" -ln "lEyeColor" -at "double";
	addAttr -ci true -sn "rEyeColor" -ln "rEyeColor" -at "double";
	addAttr -ci true -sn "jawColor" -ln "jawColor" -at "double";
	addAttr -ci true -sn "lookAtColor" -ln "lookAtColor" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".skeleton" 1;
	setAttr ".autoOri" 1;
	setAttr ".mirrorJnts" 1;
	setAttr ".startRig" 1;
	setAttr ".IkSpineCntl" 1;
	setAttr ".IkSpine" 1;
	setAttr ".FkSpineCntl" 1;
	setAttr ".FkSpine" 1;
	setAttr ".stretchScale" 1;
	setAttr ".rootColor" 7;
	setAttr ".hipsColor" 19;
	setAttr ".FkSpineColor" 18;
	setAttr ".shoulderColor" 19;
	setAttr ".LegIkFkBoth" 3;
	setAttr ".legStretchy" 1;
	setAttr ".footCtrl" 1;
	setAttr ".legOriCtrl" 1;
	setAttr ".legPoiCtrl" 1;
	setAttr ".createLegCtrl" 1;
	setAttr ".bindLegCtrl" 1;
	setAttr ".legColorLeft" 15;
	setAttr ".legColorRight" 14;
	setAttr ".armIkFkBoth" 3;
	setAttr ".armStretchy" 1;
	setAttr ".armOriCtrl" 1;
	setAttr ".armPoiCtrl" 1;
	setAttr ".clavOriCtrl" 1;
	setAttr ".clavPoiCtrl" 1;
	setAttr ".createArmCtrl" 1;
	setAttr ".bindArmCtrl" 1;
	setAttr ".armColorLeft" 15;
	setAttr ".armColorRight" 14;
	setAttr ".handColorLeft" 15;
	setAttr ".handColorRight" 14;
	setAttr ".nColor" 18;
	setAttr ".nbColor" 10;
	setAttr ".mEyeColor" 7;
	setAttr ".lEyeColor" 15;
	setAttr ".rEyeColor" 14;
	setAttr ".jawColor" 14;
	setAttr ".lookAtColor" 18;
createNode transform -n "spineExtra" -p "DoNotDeleteRigNodes";
	rename -uid "8C355120-4AA3-ABD5-629D-6AB8B39212E4";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
createNode ikHandle -n "ikSplineSpine" -p "spineExtra";
	rename -uid "71A8F510-4F81-8CAC-77AA-B59F424D559B";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -9.4895678087103662e-009 80.082627244147901 -9.4321852715680148 ;
	setAttr ".r" -type "double3" 89.999999999999972 -9.6543514239123205 89.999999999999986 ;
	setAttr ".roc" yes;
	setAttr ".dwut" 4;
	setAttr ".dwuv" -type "double3" 0 0 1 ;
	setAttr ".dwve" -type "double3" 0 0 1 ;
	setAttr ".dtce" yes;
createNode transform -n "spineCurve_ik" -p "spineExtra";
	rename -uid "05F2625B-4532-A962-26F3-C592AE7A90C2";
	setAttr ".v" no;
createNode nurbsCurve -n "spineCurve_ikShape" -p "spineCurve_ik";
	rename -uid "B4E9B286-4AF0-0BD7-A6FD-11915D8A3677";
	setAttr -k off ".v";
	setAttr -s 14 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "spineCurve_ikShapeOrig" -p "spineCurve_ik";
	rename -uid "AA5B2FAF-4B75-41B5-9844-F0932A2C053A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		3 3 0 no 3
		8 0 0 0 7.8554035766591257 15.710807153318251 23.566210729977378 23.566210729977378
		 23.566210729977378
		6
		2.7325893232234109e-015 56.649088358017032 -10.287862630020491
		-2.9196095260608423e-008 59.275004000646554 -10.329757386832791
		-7.952638228755677e-008 64.496062648911931 -10.479176482336932
		2.8470594588902456e-008 72.363191810609877 -11.277284826066461
		1.2171741546379702e-009 77.552392796654573 -10.01770418735598
		-9.4895616142039899e-009 80.082359181456326 -9.4322473036750498
		;
createNode transform -n "spineCurve_ik_CV_1_point_X" -p "spineExtra";
	rename -uid "348880BC-4BC1-E87F-92A6-37B81A2BF8F8";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr -k on ".blendSpace" 0.11199891595825476;
createNode transform -n "spineCurve_ik_CV_1_point_Y" -p "spineCurve_ik_CV_1_point_X";
	rename -uid "2C3903C1-4731-9173-4D2D-56B2B3A68624";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr -k on ".blendSpace" 0.11199891595825476;
createNode transform -n "spineCurve_ik_CV_1_point_Z" -p "spineCurve_ik_CV_1_point_Y";
	rename -uid "B0689558-40A6-7E33-9BE5-9EBE1A2D2069";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr -k on ".blendSpace" 0.11199891595825476;
createNode transform -n "spineCurve_ik_CV_1" -p "spineCurve_ik_CV_1_point_Z";
	rename -uid "26322687-4FFA-1F75-6B5B-8285ED7D4220";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
createNode clusterHandle -n "clusterHandleShape" -p "spineCurve_ik_CV_1";
	rename -uid "D6806102-43A6-485B-2432-B39874982D1B";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".io" yes;
createNode pointConstraint -n "spineCurve_ik_CV_1_point_Z_pointConstraint1" -p "spineCurve_ik_CV_1_point_Z";
	rename -uid "A4FC918B-4BC6-033E-150E-38918C53846A";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_1_L_point_blend_ZW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_1_point_blend_ZW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".o" -type "double3" 3.3087224502121107e-024 -7.1054273576010019e-015 0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "spineCurve_ik_CV_1_point_Y_pointConstraint1" -p "spineCurve_ik_CV_1_point_Y";
	rename -uid "8FD4D5BA-4E2D-2663-36DB-D6A61A8C044F";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_1_L_point_blend_YW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_1_point_blend_YW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".o" -type "double3" 3.3087224502121107e-024 0 0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "spineCurve_ik_CV_1_point_X_pointConstraint1" -p "spineCurve_ik_CV_1_point_X";
	rename -uid "A2344CE4-40E2-C986-5EAF-269AB1595076";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_1_L_point_blend_XW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_1_point_blend_XW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "spineCurve_ik_CV_2_point_X" -p "spineExtra";
	rename -uid "0D1CE594-4963-39AC-D4A2-75A02F5B50A8";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr -k on ".blendSpace" 0.3347411081692811;
createNode transform -n "spineCurve_ik_CV_2_point_Y" -p "spineCurve_ik_CV_2_point_X";
	rename -uid "F3E178E6-4CCF-D69F-0954-74A1334217C1";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr -k on ".blendSpace" 0.3347411081692811;
createNode transform -n "spineCurve_ik_CV_2_point_Z" -p "spineCurve_ik_CV_2_point_Y";
	rename -uid "50A26292-4985-828E-81A4-3E8D22704463";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr -k on ".blendSpace" 0.3347411081692811;
createNode transform -n "spineCurve_ik_CV_2" -p "spineCurve_ik_CV_2_point_Z";
	rename -uid "34374EE8-44F4-4EBC-997A-BBA0600F25F7";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
createNode clusterHandle -n "clusterHandleShape" -p "spineCurve_ik_CV_2";
	rename -uid "E81C368F-4850-A2C9-4515-BCA1E4A4C39A";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".io" yes;
createNode pointConstraint -n "spineCurve_ik_CV_2_point_Z_pointConstraint1" -p "spineCurve_ik_CV_2_point_Z";
	rename -uid "029226A0-485D-A100-4EA3-2DA523BF994F";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_2_L_point_blend_ZW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_2_point_blend_ZW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "spineCurve_ik_CV_2_point_Y_pointConstraint1" -p "spineCurve_ik_CV_2_point_Y";
	rename -uid "6ECCAE34-4580-3CAF-3282-3681A91DCB9B";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_2_L_point_blend_YW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_2_point_blend_YW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "spineCurve_ik_CV_2_point_X_pointConstraint1" -p "spineCurve_ik_CV_2_point_X";
	rename -uid "5E6ECA6A-4921-B716-B4D9-B688FAA9BBA8";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_2_L_point_blend_XW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_2_point_blend_XW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "spineCurve_ik_CV_3_point_X" -p "spineExtra";
	rename -uid "8F22AA60-45EB-5D62-5D77-0FBAAE09ADF0";
	addAttr -ci true -sn "midCvX" -ln "midCvX" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr -k on ".blendSpace" 0.67146991751525498;
createNode transform -n "spineCurve_ik_CV_3_point_Y" -p "spineCurve_ik_CV_3_point_X";
	rename -uid "7802CA22-4181-DB7F-D42A-7DB3EA40B7BD";
	addAttr -ci true -sn "midCvY" -ln "midCvY" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr -k on ".blendSpace" 0.67146991751525498;
createNode transform -n "spineCurve_ik_CV_3_point_Z" -p "spineCurve_ik_CV_3_point_Y";
	rename -uid "06B0F85E-4364-CB92-D316-9587AE16F6B9";
	addAttr -ci true -sn "midCvZ" -ln "midCvZ" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr -k on ".blendSpace" 0.67146991751525498;
createNode transform -n "spineCurve_ik_CV_3" -p "spineCurve_ik_CV_3_point_Z";
	rename -uid "A8979438-4164-C3F7-B4F8-19AFBF813CD4";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
createNode clusterHandle -n "clusterHandleShape" -p "spineCurve_ik_CV_3";
	rename -uid "DF2E5502-4EEA-5AC6-FA23-CF87CDBBDAD5";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".io" yes;
createNode pointConstraint -n "spineCurve_ik_CV_3_point_Z_pointConstraint1" -p "spineCurve_ik_CV_3_point_Z";
	rename -uid "EBEC7A93-4CC9-0582-C038-B2BF99B932B4";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_3_L_point_blend_ZW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_3_point_blend_ZW1" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w2" -ln "mid_ik_ctrlW2" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 3 ".tg";
	setAttr ".o" -type "double3" 1.4235293885683461e-008 1.9948302119741896 -0.49471110032805754 ;
	setAttr ".rst" -type "double3" 0 0 -5.3290705182007514e-015 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
	setAttr -k on ".w2";
createNode pointConstraint -n "spineCurve_ik_CV_3_point_Y_pointConstraint1" -p "spineCurve_ik_CV_3_point_Y";
	rename -uid "74AE9FBD-4428-A502-5AF8-EB950AC775C9";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_3_L_point_blend_YW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_3_point_blend_YW1" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w2" -ln "mid_ik_ctrlW2" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 3 ".tg";
	setAttr ".o" -type "double3" 1.4235293885683461e-008 1.9948302119741896 -0.49471110032805221 ;
	setAttr ".rst" -type "double3" 0 -1.4210854715202004e-014 0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
	setAttr -k on ".w2";
createNode pointConstraint -n "spineCurve_ik_CV_3_point_X_pointConstraint1" -p "spineCurve_ik_CV_3_point_X";
	rename -uid "4EDF297D-4C97-2DF4-EC8F-44BC7B368DE5";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_3_L_point_blend_XW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_3_point_blend_XW1" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w2" -ln "mid_ik_ctrlW2" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 3 ".tg";
	setAttr ".o" -type "double3" 1.4235293885683461e-008 1.9948302119742038 -0.49471110032805221 ;
	setAttr ".rst" -type "double3" -4.2176028018206618e-015 0 0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
	setAttr -k on ".w2";
createNode transform -n "spineCurve_ik_CV_4_point_X" -p "spineExtra";
	rename -uid "8E67D5B7-49D4-8654-0B11-0BBAA8EFE6E7";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr -k on ".blendSpace" 0.89151570237095445;
createNode transform -n "spineCurve_ik_CV_4_point_Y" -p "spineCurve_ik_CV_4_point_X";
	rename -uid "2DB6EC7A-48E1-CEED-855E-12817396B8BB";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr -k on ".blendSpace" 0.89151570237095445;
createNode transform -n "spineCurve_ik_CV_4_point_Z" -p "spineCurve_ik_CV_4_point_Y";
	rename -uid "C67E0E17-4923-E36C-4A50-8BBEBB8E2ABF";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr -k on ".blendSpace" 0.89151570237095445;
createNode transform -n "spineCurve_ik_CV_4" -p "spineCurve_ik_CV_4_point_Z";
	rename -uid "23622064-4F24-B5BD-A1A9-1A80E8A5B337";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
createNode clusterHandle -n "clusterHandleShape" -p "spineCurve_ik_CV_4";
	rename -uid "FE6181AF-4DF2-3E19-651C-F2B38E1C4A42";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".io" yes;
createNode pointConstraint -n "spineCurve_ik_CV_4_point_Z_pointConstraint1" -p "spineCurve_ik_CV_4_point_Z";
	rename -uid "1FC62D1C-489A-BB26-04B3-2B9F01213266";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_4_L_point_blend_ZW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_4_point_blend_ZW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".o" -type "double3" 0 -1.4210854715202004e-014 0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "spineCurve_ik_CV_4_point_Y_pointConstraint1" -p "spineCurve_ik_CV_4_point_Y";
	rename -uid "2E6609EA-43D8-2BE0-84CF-D58FFF1804C5";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_4_L_point_blend_YW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_4_point_blend_YW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "spineCurve_ik_CV_4_point_X_pointConstraint1" -p "spineCurve_ik_CV_4_point_X";
	rename -uid "768AC2A4-4922-3834-44E7-FCBFEED208D1";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_4_L_point_blend_XW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "spineCurve_ik_CV_4_point_blend_XW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "njc_top_twistCtrl" -p "spineExtra";
	rename -uid "AF5BA9DE-4C55-5EE8-29D0-3190804A54EA";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
createNode orientConstraint -n "njc_top_twistCtrl_orientConstraint1" -p "njc_top_twistCtrl";
	rename -uid "480F8A6C-48E8-681A-5106-C69E5847E210";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_5W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "njc_top_twistCtrl_pointConstraint1" -p "njc_top_twistCtrl";
	rename -uid "561D31CB-45E8-F16F-F881-89A46D8106BC";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_5W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.2 0.00026806269139001415 6.2032108798959484e-005 ;
	setAttr ".rst" -type "double3" 0.1999999905104384 80.082627244147716 -9.4321852715662509 ;
	setAttr -k on ".w0";
createNode transform -n "njc_bottom_twistCtrl" -p "spineExtra";
	rename -uid "A344421F-4E23-AB67-F61E-E5AB53C07712";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
createNode orientConstraint -n "njc_bottom_twistCtrl_orientConstraint1" -p "njc_bottom_twistCtrl";
	rename -uid "0F44D2AD-4608-730C-5D40-38BCB4024E54";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_0W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -6.3611093629270391e-015 -3.1805546814635168e-014 
		2.2263882770244621e-014 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "njc_bottom_twistCtrl_pointConstraint1" -p "njc_bottom_twistCtrl";
	rename -uid "BEAB7DEF-4888-DB2B-DDB6-209840862829";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "spineCurve_ik_CV_0W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.2 0 0 ;
	setAttr ".rst" -type "double3" 0.20000000000000273 56.649088358017032 -10.287862630020491 ;
	setAttr -k on ".w0";
createNode transform -n "worldPlacement";
	rename -uid "6CD59B42-4DB9-7244-F8F8-A6B8C299B5B0";
	addAttr -ci true -sn "worldPlacement" -ln "worldPlacement" -at "double";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "ikControl" -ln "ikControl" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "midIkCtrl" -ln "midIkCtrl" -dv 1 -min 0 -max 1 -at "bool";
	setAttr -l on -cb on ".ikControl";
	setAttr -k on ".midIkCtrl" no;
createNode nurbsCurve -n "worldPlacementShape" -p "worldPlacement";
	rename -uid "8C065D46-402C-0845-8ACE-999553E5F189";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 29;
	setAttr ".cc" -type "nurbsCurve" 
		1 17 0 no 3
		18 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
		18
		-53.552 1.46 53.552
		-53.552 -0.0054799999999999996 53.552
		-53.552 1.46 53.552
		-53.552 1.46 -53.552
		-53.552 -0.0054799999999999996 -53.552
		-53.552 1.46 -53.552
		53.552 1.46 -53.552
		53.552 -0.0054799999999999996 -53.552
		53.552 1.46 -53.552
		53.552 1.46 53.552
		53.552 -0.0054799999999999996 53.552
		53.552 1.46 53.552
		-53.552 1.46 53.552
		-53.552 -0.0054799999999999996 53.552
		-53.552 -0.0054799999999999996 -53.552
		53.552 -0.0054799999999999996 -53.552
		53.552 -0.0054799999999999996 53.552
		-53.552 -0.0054799999999999996 53.552
		;
createNode transform -n "rig_skeleton" -p "worldPlacement";
	rename -uid "C7A47E51-485D-983C-4284-C1A6C3CAD63D";
	addAttr -ci true -sn "rigSkelGrp" -ln "rigSkelGrp" -at "double";
createNode joint -n "pelvis_rig" -p "rig_skeleton";
	rename -uid "7EF06A22-44B5-1C1C-5FA1-ED96BCA05770";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "pelvisJnt" -ln "pelvisJnt" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999972 6.8217670768916099 89.999999999999972 ;
createNode joint -n "spine_01_rig" -p "pelvis_rig";
	rename -uid "27AC36FD-41C8-9280-448E-D5A07A3CF56C";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "spineJnt01" -ln "spineJnt01" -at "double";
	setAttr ".t" -type "double3" 8.2043254941233741 -1.5987211554602254e-014 -7.1054273576009924e-015 ;
	setAttr ".r" -type "double3" -6.1773880229510144e-010 -8.8888362821682857e-009 -0.085665009747192503 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 4.9955644642355752 ;
createNode joint -n "spine_02_rig" -p "spine_01_rig";
	rename -uid "079E15EC-48CB-7F50-7CD2-00AE99A98FA6";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "spineJnt02" -ln "spineJnt02" -at "double";
	setAttr ".t" -type "double3" 6.959034 8.8817841970012523e-015 -5.0722078972747858e-008 ;
	setAttr ".r" -type "double3" -1.0306248822165447e-009 7.2834948894749873e-009 0.1651291957535439 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -0.73309151890089952 ;
createNode joint -n "spine_03_rig" -p "spine_02_rig";
	rename -uid "4A3A9AF0-4D96-3321-6D19-E8814C324183";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "spineJnt03" -ln "spineJnt03" -at "double";
	setAttr ".t" -type "double3" 8.04489 1.4210854715202004e-014 5.0722070770598949e-008 ;
	setAttr ".r" -type "double3" -9.3512724618951262e-011 3.8861602952903556e-010 -0.084117675498460318 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 12.218299044961364 ;
createNode joint -n "spineEnd_rig" -p "spine_03_rig";
	rename -uid "6BF9CAB3-4F42-AB00-4462-01A5234DF729";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "spineEndJnt" -ln "spineEndJnt" -at "double";
	setAttr ".t" -type "double3" 8.562287 0 -9.4895642139149193e-009 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -6.6853931696465008 ;
createNode ikEffector -n "effector1" -p "spine_03_rig";
	rename -uid "BED6148D-44CB-EBD4-53DB-95A46C3BB4A6";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "hips_rig" -p "pelvis_rig";
	rename -uid "50EA7EE3-416C-F5E7-4905-FAB86F41413E";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "hipsJnt" -ln "hipsJnt" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "l_hip_rig" -p "hips_rig";
	rename -uid "80F54011-49BA-3A9F-BF77-9DA28A54FE74";
	addAttr -ci true -sn "l_hipJnt" -ln "l_hipJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 -1.4574876822744875e-005 178.64906316091094 ;
	setAttr -k on ".blendSpace";
createNode joint -n "l_knee_rig" -p "l_hip_rig";
	rename -uid "8590FFB3-49CE-0E6C-0154-34AE1537DF4A";
	addAttr -ci true -sn "l_kneeJnt" -ln "l_kneeJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.9520412118295752e-022 13.792099994027071 -8.4503093863587517e-006 ;
	setAttr -k on ".blendSpace";
createNode joint -n "l_ankle_rig" -p "l_knee_rig";
	rename -uid "129A9D71-48BB-5FF9-1AC8-7FAF85FBFFD6";
	addAttr -ci true -sn "l_ankleJnt" -ln "l_ankleJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.4410750513976186e-005 -59.41696995786635 7.6807871935862337e-005 ;
	setAttr -k on ".blendSpace";
createNode joint -n "l_ball_rig" -p "l_ankle_rig";
	rename -uid "BFAFA579-4223-45B2-3C82-B3A9BC8FACE2";
	addAttr -ci true -sn "l_ballJnt" -ln "l_ballJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.029246346649313214 -36.202415693425529 0.049437522681953873 ;
	setAttr -k on ".blendSpace";
createNode joint -n "l_toe_rig" -p "l_ball_rig";
	rename -uid "C0FE9DEB-4ABC-A8B2-F6D7-2DB97AE5A48C";
	addAttr -ci true -sn "l_toeJnt" -ln "l_toeJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr -k on ".blendSpace";
createNode pointConstraint -n "l_toe_rig_pointConstraint1" -p "l_toe_rig";
	rename -uid "A7CA578A-49F2-57D6-7955-4DA0A5BB785E";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_toe_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "l_toe_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 9.6354058851631859 -1.4641727297259877e-007 -1.0054901355971424e-011 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "l_toe_rig_orientConstraint1" -p "l_toe_rig";
	rename -uid "07FE4BC9-4475-FBCF-E5AD-D1A99EA2B835";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_toe_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "l_toe_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "l_ball_rig_pointConstraint1" -p "l_ball_rig";
	rename -uid "D2C77A7D-4736-EFAE-6F65-398AB504FA13";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_ball_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "l_ball_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 10.493119080456314 -9.2270548535111629e-008 8.4376949871511897e-014 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "l_ball_rig_orientConstraint1" -p "l_ball_rig";
	rename -uid "54A2AD29-4153-8225-DA07-579E7E30D3F9";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_ball_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "l_ball_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "l_ankle_rig_pointConstraint1" -p "l_ankle_rig";
	rename -uid "983B6F58-4500-F2F7-E0F4-63A94B674294";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_ankle_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "l_ankle_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 19.015319625082245 5.3290705182007514e-015 -1.2434497875801753e-014 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "l_ankle_rig_orientConstraint1" -p "l_ankle_rig";
	rename -uid "8645B039-4B6A-BEA8-1C69-4F9214F09FA5";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_ankle_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "l_ankle_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -2.1396220281816754e-005 2.114348693256966e-015 2.7380073363302238e-005 ;
	setAttr ".rsrr" -type "double3" 2.1232493181461917e-020 -4.8362288526476004e-027 
		1.8246673827818837e-020 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "l_leg_ik_switch" -p "l_ankle_rig";
	rename -uid "60C313D5-4E6F-E862-7A69-288709108793";
	addAttr -ci true -sn "l_leg_ik_fk" -ln "l_leg_ik_fk" -at "double";
	addAttr -ci true -sn "l_NJC_autoRigSystem" -ln "l_NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "IkFkSwitch" -ln "IkFkSwitch" -dv 1 -min 0 -max 1 -at "double";
	addAttr -ci true -sn "__" -ln "__" -min 0 -max 0 -en "stretch" -at "enum";
	addAttr -ci true -sn "autoStretch" -ln "autoStretch" -dv 1 -min 0 -max 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.0104437369044437 -10.901300247355676 -2.0639307263572055 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 53.79757387985201 2.6090285845519305e-005 89.9999566379594 ;
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr ".s" -type "double3" 1 0.99999999999999978 1 ;
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" 32 6.3967546262919956 -5.2448738614950852 ;
	setAttr ".rpt" -type "double3" -40.010427769183543 25.603252375925351 7.3087949803163355 ;
	setAttr ".sp" -type "double3" 32 6.3967546262919974 -5.2448738614950852 ;
	setAttr ".spt" -type "double3" 0 -1.7763568394002501e-015 0 ;
	setAttr -k on ".IkFkSwitch";
	setAttr -cb on ".__";
	setAttr -k on ".autoStretch";
createNode nurbsCurve -n "l_leg_ik_switchShape" -p "l_leg_ik_switch";
	rename -uid "730FB857-44A1-EBE5-A672-4888430D290C";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 10 0 no 3
		11 0 1 2 3 4 5 6 7 8 9 10
		11
		31 4.8967546262919974 -5.2448738614950852
		32 3.8967546262919974 -5.2448738614950852
		33 4.8967546262919974 -5.2448738614950852
		32.5 4.8967546262919974 -5.2448738614950852
		32.5 7.8967546262919974 -5.2448738614950852
		33 7.8967546262919974 -5.2448738614950852
		32 8.8967546262919974 -5.2448738614950852
		31 7.8967546262919974 -5.2448738614950852
		31.5 7.8967546262919974 -5.2448738614950852
		31.5 4.8967546262919974 -5.2448738614950852
		31 4.8967546262919974 -5.2448738614950852
		;
createNode pointConstraint -n "l_knee_rig_pointConstraint1" -p "l_knee_rig";
	rename -uid "49387AFC-4019-3D0B-0AB3-DDB5C2A46967";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_knee_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "l_knee_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 22.362855289533634 -5.3290705182007514e-015 -1.099120794378905e-014 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "l_knee_rig_orientConstraint1" -p "l_knee_rig";
	rename -uid "D19508DE-4608-9615-BE67-5AB9EF6056F8";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_knee_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "l_knee_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -3.7915166395467715e-022 3.1805546814635393e-015 1.4929096768215414e-021 ;
	setAttr ".rsrr" -type "double3" -3.7915166395467715e-022 3.1805546814635393e-015 
		1.4929096768215414e-021 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "l_hip_rig_pointConstraint1" -p "l_hip_rig";
	rename -uid "9BFC1882-4861-90EB-A666-159213E37310";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_hip_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "l_hip_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -1.3658201354747561 2.60762767465165 10.901285582461382 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "l_hip_rig_orientConstraint1" -p "l_hip_rig";
	rename -uid "0F94B816-4242-087B-BDC2-80B87779215A";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_hip_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "l_hip_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 3.4510405575498424e-005 -2.1718508701042864e-016 -3.8316051437786948e-006 ;
	setAttr ".rsrr" -type "double3" -6.361109362927032e-015 -2.2069531490250784e-031 
		-3.9756933518293944e-015 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "r_hip_rig" -p "hips_rig";
	rename -uid "984D5CA4-4908-C100-29F2-9B81BDDCCD11";
	addAttr -ci true -sn "r_hipJnt" -ln "r_hipJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000043 -1.4574876706754021e-005 -1.3509368390890217 ;
	setAttr -k on ".blendSpace";
createNode joint -n "r_knee_rig" -p "r_hip_rig";
	rename -uid "1136D77B-4259-A22C-6C30-32A82107BBCB";
	addAttr -ci true -sn "r_kneeJnt" -ln "r_kneeJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 13.792099994027033 -8.4503093429707239e-006 ;
	setAttr -k on ".blendSpace";
createNode joint -n "r_ankle_rig" -p "r_knee_rig";
	rename -uid "144D2B08-4946-CC10-3384-F89413BAAE98";
	addAttr -ci true -sn "r_ankleJnt" -ln "r_ankleJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.6130247516216962e-005 -59.416969957865142 7.8288174523231892e-005 ;
	setAttr -k on ".blendSpace";
createNode joint -n "r_ball_rig" -p "r_ankle_rig";
	rename -uid "F7A249F7-45D0-8455-B9F0-D387FA5C95BD";
	addAttr -ci true -sn "r_ballJnt" -ln "r_ballJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.029245795042113217 -36.202415693810153 0.049436443720274337 ;
	setAttr -k on ".blendSpace";
createNode joint -n "r_toe_rig" -p "r_ball_rig";
	rename -uid "CB57F307-4082-957C-B01B-52B99B65E87C";
	addAttr -ci true -sn "r_toeJnt" -ln "r_toeJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr -k on ".blendSpace";
createNode pointConstraint -n "r_toe_rig_pointConstraint1" -p "r_toe_rig";
	rename -uid "E00652FE-464A-0E64-2C93-49B98372EA41";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_toe_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "r_toe_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -9.6354123294207916 1.2065748812517541e-005 -3.8492714293791863e-008 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "r_toe_rig_orientConstraint1" -p "r_toe_rig";
	rename -uid "EE784002-46E8-8BFD-E919-2687A4B249D5";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_toe_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "r_toe_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "r_ball_rig_pointConstraint1" -p "r_ball_rig";
	rename -uid "B17D2CA7-4D15-E6B8-EEEB-9AAC3EAA99D6";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_ball_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "r_ball_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -10.493109997995456 8.0792280474639711e-006 1.5153519767885371e-006 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "r_ball_rig_orientConstraint1" -p "r_ball_rig";
	rename -uid "01B4A980-459B-BCBC-4E29-CE866057AB2C";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_ball_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "r_ball_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "r_ankle_rig_pointConstraint1" -p "r_ankle_rig";
	rename -uid "64F4A551-4872-6BD7-4C67-0DAA28B8A676";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_ankle_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "r_ankle_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -19.015331074775951 1.9740161860681837e-006 -7.5268428236796581e-006 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "r_ankle_rig_orientConstraint1" -p "r_ankle_rig";
	rename -uid "D0D922BB-4B4F-DCC0-733F-378ED7DAFFED";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_ankle_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "r_ankle_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -2.4265705953691562e-020 8.5803189562868202e-006 7.2038797981857149e-021 ;
	setAttr ".rsrr" -type "double3" -2.1232493181461923e-020 9.4916641033270633e-027 
		1.2038065330561001e-020 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "r_leg_ik_switch" -p "r_ankle_rig";
	rename -uid "57A590B0-48B0-55C8-2381-8694A52B007C";
	addAttr -ci true -sn "r_leg_ik_fk" -ln "r_leg_ik_fk" -at "double";
	addAttr -ci true -sn "r_NJC_autoRigSystem" -ln "r_NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "IkFkSwitch" -ln "IkFkSwitch" -dv 1 -min 0 -max 1 -at "double";
	addAttr -ci true -sn "__" -ln "__" -min 0 -max 0 -en "stretch" -at "enum";
	addAttr -ci true -sn "autoStretch" -ln "autoStretch" -dv 1 -min 0 -max 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.0104377451519682 10.90130709148425 2.0639293586746392 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" -126.20242612017437 2.5645170628669111e-005 89.999955884801579 ;
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999956 0.99999999999999967 ;
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" -31.999999999999993 6.3967499999999733 -5.2448699999999926 ;
	setAttr ".rpt" -type "double3" 40.01042150009264 -38.39675709147587 3.1809500849487122 ;
	setAttr ".sp" -type "double3" -32 6.396749999999976 -5.2448699999999953 ;
	setAttr ".spt" -type "double3" 7.1054273576010003e-015 -2.6645352591003745e-015 
		2.6645352591003749e-015 ;
	setAttr -k on ".IkFkSwitch";
	setAttr -cb on ".__";
	setAttr -k on ".autoStretch";
createNode nurbsCurve -n "r_leg_ik_switchShape" -p "r_leg_ik_switch";
	rename -uid "FC74C6BF-4466-CBC3-2E98-C4A83BEDD6D8";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 10 0 no 3
		11 0 1 2 3 4 5 6 7 8 9 10
		11
		-33 4.896749999999976 -5.2448699999999953
		-32 3.896749999999976 -5.2448699999999953
		-31 4.896749999999976 -5.2448699999999953
		-31.5 4.896749999999976 -5.2448699999999953
		-31.5 7.896749999999976 -5.2448699999999953
		-31 7.896749999999976 -5.2448699999999953
		-32 8.896749999999976 -5.2448699999999953
		-33 7.896749999999976 -5.2448699999999953
		-32.5 7.896749999999976 -5.2448699999999953
		-32.5 4.896749999999976 -5.2448699999999953
		-33 4.896749999999976 -5.2448699999999953
		;
createNode pointConstraint -n "r_knee_rig_pointConstraint1" -p "r_knee_rig";
	rename -uid "85750C58-4A08-A645-F1C6-A29BA6DC7AB6";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_knee_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "r_knee_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -22.362815337331003 5.6886437285186275e-006 -7.3233511088677261e-007 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "r_knee_rig_orientConstraint1" -p "r_knee_rig";
	rename -uid "8646C16A-4553-5B1F-5A04-52AB5077E24A";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_knee_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "r_knee_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -1.8957583197734631e-022 -1.8919150278116526e-005 
		3.1299108632071982e-029 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "r_hip_rig_pointConstraint1" -p "r_hip_rig";
	rename -uid "451B84DB-4410-0561-C0D3-B18BC5EEA116";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_hip_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "r_hip_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -1.3658523436602081 2.607626285669987 -10.901299999999994 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "r_hip_rig_orientConstraint1" -p "r_hip_rig";
	rename -uid "94958D3A-49E8-35EB-27BF-5EA448EA1991";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_hip_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "r_hip_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 4.066078375038805e-014 8.622691153469158e-006 1.5902776466926763e-014 ;
	setAttr ".rsrr" -type "double3" 4.4720338187218459e-014 -2.6835930124848481e-015 
		1.5803381073521846e-014 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "spineCurve_ik_CV_0" -p "hips_rig";
	rename -uid "86283CBB-48A9-46C8-6814-638B1446C179";
	setAttr -k off -cb on ".v";
	setAttr ".t" -type "double3" -49.265717644959388 3.4861828524274419 2.4007115880596387e-014 ;
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr ".r" -type "double3" -83.178232923108396 -89.999999999999957 0 ;
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.7325893232234109e-015 56.649088358017018 -10.28786263002049 ;
	setAttr ".rpt" -type "double3" 57.470043139082755 -60.135271210444479 10.287862630020458 ;
	setAttr ".sp" -type "double3" 2.7325893232234109e-015 56.649088358017032 -10.287862630020491 ;
	setAttr ".spt" -type "double3" 0 -1.4210854715202002e-014 1.7763568394002503e-015 ;
createNode clusterHandle -n "clusterHandleShape" -p "spineCurve_ik_CV_0";
	rename -uid "ABA0C4BA-4FF6-8A84-1FD3-559833A39B38";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".io" yes;
createNode transform -n "spineCurve_ik_CV_1_point_blend_X" -p "spineCurve_ik_CV_0";
	rename -uid "AEB429A2-47E8-95BF-15C7-27AFE5C4FEEB";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
createNode transform -n "spineCurve_ik_CV_1_point_blend_Y" -p "spineCurve_ik_CV_0";
	rename -uid "866F49A2-4454-7D01-5D45-B7A9C03A04F6";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
createNode transform -n "spineCurve_ik_CV_1_point_blend_Z" -p "spineCurve_ik_CV_0";
	rename -uid "18AA9085-4668-89AC-3BF1-B5965E5A12BA";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
createNode transform -n "spineCurve_ik_CV_2_point_blend_X" -p "spineCurve_ik_CV_0";
	rename -uid "482E8C00-4542-E451-B536-6FB48149998E";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
createNode transform -n "spineCurve_ik_CV_2_point_blend_Y" -p "spineCurve_ik_CV_0";
	rename -uid "A2B016AD-4856-8E83-D3E5-549812AFF488";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
createNode transform -n "spineCurve_ik_CV_2_point_blend_Z" -p "spineCurve_ik_CV_0";
	rename -uid "A1DF67C6-4151-FB0D-2B0A-AC85C9A63960";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
createNode transform -n "spineCurve_ik_CV_3_point_blend_X" -p "spineCurve_ik_CV_0";
	rename -uid "F91A072A-4F7D-AB16-7B2A-30BEA0C51092";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
createNode transform -n "spineCurve_ik_CV_3_point_blend_Y" -p "spineCurve_ik_CV_0";
	rename -uid "333E8E64-4881-197C-37CE-6683BA54A8C8";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
createNode transform -n "spineCurve_ik_CV_3_point_blend_Z" -p "spineCurve_ik_CV_0";
	rename -uid "D57838B1-49D7-4973-899A-ABB98A10F821";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
createNode transform -n "spineCurve_ik_CV_4_point_blend_X" -p "spineCurve_ik_CV_0";
	rename -uid "878EC40E-4CE4-8E74-831A-9994A9C6BED3";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
createNode transform -n "spineCurve_ik_CV_4_point_blend_Y" -p "spineCurve_ik_CV_0";
	rename -uid "A5B07D66-450A-2764-4C6F-7E9B2A26428C";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
createNode transform -n "spineCurve_ik_CV_4_point_blend_Z" -p "spineCurve_ik_CV_0";
	rename -uid "F3AF978A-4431-3B58-C1FD-228660DD6F65";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
createNode orientConstraint -n "hips_rig_orientConstraint1" -p "hips_rig";
	rename -uid "8BF4A58B-4D3E-C2B3-B244-92A077F559EB";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "hips_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -83.178232923108396 -89.999999999999957 0 ;
	setAttr ".o" -type "double3" 89.999999999999986 6.8217670768915966 89.999999999999957 ;
	setAttr ".rsrr" -type "double3" -1.5902773407317584e-014 3.1805546814635176e-015 
		6.3611093629270327e-015 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "hips_rig_pointConstraint1" -p "hips_rig";
	rename -uid "A8EFA7B0-4DF5-9C56-79FC-6BA99690BE9D";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "hips_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -7.1054273576010019e-015 -2.6645352591003757e-015 -3.1554436208840472e-030 ;
	setAttr ".rst" -type "double3" 1.4210854715202004e-014 4.4408920985006262e-015 3.1554436208840472e-030 ;
	setAttr -k on ".w0";
createNode joint -n "l_hip_rig_ik" -p "hips_rig";
	rename -uid "2A2FA6F3-4CAD-4A60-BAB4-8695A0228075";
	addAttr -ci true -sn "l_hip_rig_ik_dup" -ln "l_hip_rig_ik_dup" -at "double";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.3658201354747561 2.6076276746516482 10.90128558246138 ;
	setAttr ".r" -type "double3" 3.4510405593300577e-005 -1.1481473691643696e-012 -3.8316051398514722e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999972 -1.4574876818839394e-005 178.64906316091094 ;
createNode joint -n "l_knee_rig_ik" -p "l_hip_rig_ik";
	rename -uid "BEDDA38A-40C6-F671-77A9-03AE194D7B80";
	addAttr -ci true -sn "l_knee_rig_ik_dup" -ln "l_knee_rig_ik_dup" -at "double";
	setAttr ".t" -type "double3" 22.362855 -1.7763568394002505e-015 -9.2703622556200571e-015 ;
	setAttr ".r" -type "double3" -2.5562795313032147e-022 6.8410281973719162e-007 -4.2321974150457253e-013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 13.792099994027076 -8.45030938635875e-006 ;
createNode joint -n "l_ankle_rig_ik" -p "l_knee_rig_ik";
	rename -uid "EF1B9AD7-4C9A-B721-3805-C6BBA1ADF045";
	addAttr -ci true -sn "l_ankle_rig_ik_dup" -ln "l_ankle_rig_ik_dup" -at "double";
	setAttr ".t" -type "double3" 19.01532 7.1054273576010019e-015 -1.0658141036401503e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.4410750513976192e-005 -59.41696995786635 7.6807871935862378e-005 ;
createNode joint -n "l_ball_rig_ik" -p "l_ankle_rig_ik";
	rename -uid "35AC2AA6-47B0-693B-8A79-9BAE15D90F36";
	addAttr -ci true -sn "l_ball_rig_ik_dup" -ln "l_ball_rig_ik_dup" -at "double";
	setAttr ".t" -type "double3" 10.493119080456317 -9.2270552087825308e-008 8.2600593032111647e-014 ;
	setAttr ".r" -type "double3" 1.4061105024562946e-008 -2.1366912065458967e-016 -1.4671647157627696e-020 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.029246346649313214 -36.202415693425529 0.049437522681953873 ;
createNode joint -n "l_toe_rig_ik" -p "l_ball_rig_ik";
	rename -uid "5BE54A0A-4F80-19B2-4532-59BCE9F2458E";
	addAttr -ci true -sn "l_toe_rig_ik_dup" -ln "l_toe_rig_ik_dup" -at "double";
	setAttr ".t" -type "double3" 9.6354058851631841 -1.4641727652531245e-007 -1.0053763377371183e-011 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode ikEffector -n "effector6" -p "l_ball_rig_ik";
	rename -uid "0E75627A-4D2A-E673-E7CB-40B459D36641";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode orientConstraint -n "l_ankle_rig_ik_orientConstraint1" -p "l_ankle_rig_ik";
	rename -uid "C510B3F7-46AA-915C-A0AF-EA9EC149CA43";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_foot_ik_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 53.797573879835824 4.7486506099362423e-005 89.999983984824937 ;
	setAttr ".o" -type "double3" -3.3151165826895938e-005 -53.797573879823226 -89.999926584632661 ;
	setAttr ".rsrr" -type "double3" -1.5902773407317584e-014 -6.361109362927032e-015 
		9.5416640443905519e-015 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector5" -p "l_ankle_rig_ik";
	rename -uid "2BCA86B6-4C42-21C0-5766-3088DB65802E";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector4" -p "l_knee_rig_ik";
	rename -uid "6F19B2C3-4628-DE54-252B-4BBCB33037B4";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "l_hip_rig_fk" -p "hips_rig";
	rename -uid "02F111FD-4F28-8067-EDB7-EAAEE6F72A54";
	addAttr -ci true -sn "l_hip_rig_fk_dup" -ln "l_hip_rig_fk_dup" -at "double";
	setAttr ".v" no;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999972 -1.4574876818839394e-005 178.64906316091094 ;
createNode joint -n "l_knee_rig_fk" -p "l_hip_rig_fk";
	rename -uid "4EDA8401-4259-31DF-3D3C-2AB124DAF44D";
	addAttr -ci true -sn "l_knee_rig_fk_dup" -ln "l_knee_rig_fk_dup" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 13.792099994027076 -8.45030938635875e-006 ;
createNode joint -n "l_ankle_rig_fk" -p "l_knee_rig_fk";
	rename -uid "97C521ED-4D95-5089-1EA5-2283EC629137";
	addAttr -ci true -sn "l_ankle_rig_fk_dup" -ln "l_ankle_rig_fk_dup" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.4410750513976192e-005 -59.41696995786635 7.6807871935862378e-005 ;
createNode joint -n "l_ball_rig_fk" -p "l_ankle_rig_fk";
	rename -uid "D2E5119C-4225-ED80-240E-8AA1EDA2910E";
	addAttr -ci true -sn "l_ball_rig_fk_dup" -ln "l_ball_rig_fk_dup" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.029246346649313214 -36.202415693425529 0.049437522681953873 ;
createNode joint -n "l_toe_rig_fk" -p "l_ball_rig_fk";
	rename -uid "00454972-490E-DEB0-3FE4-B9848A7ECCE9";
	addAttr -ci true -sn "l_toe_rig_fk_dup" -ln "l_toe_rig_fk_dup" -at "double";
	setAttr ".t" -type "double3" 9.6354058851631841 -1.4641727652531245e-007 -1.0053763377371183e-011 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode orientConstraint -n "l_ball_rig_fk_orientConstraint1" -p "l_ball_rig_fk";
	rename -uid "D5C91F1A-46B2-D694-D7DE-FD81D565C6D7";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_ball_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 89.99999977104838 8.5672807621742423e-008 89.960087533671668 ;
	setAttr ".o" -type "double3" -89.999671331690053 -89.960087533671029 -0.00032866821862863858 ;
	setAttr ".rsrr" -type "double3" 1.1574038485845738e-011 -9.5416640443892645e-015 
		-1.2722218725855031e-014 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "l_ball_rig_fk_pointConstraint1" -p "l_ball_rig_fk";
	rename -uid "0ECEC363-46D1-1CAF-2307-09BD4B1154A8";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_ball_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1.1733831684068718e-008 -6.0814571156697639e-009 1.1580791614562713e-007 ;
	setAttr ".rst" -type "double3" 10.493119080456317 -9.2270552087825308e-008 8.2600593032111647e-014 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "l_ankle_rig_fk_orientConstraint1" -p "l_ankle_rig_fk";
	rename -uid "36492BF7-4CF5-7422-B20D-849B50F12021";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_ankle_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 53.797573879825904 2.6090285843811761e-005 89.999956637959443 ;
	setAttr ".o" -type "double3" -3.3151165799971281e-005 -53.797573879823226 -89.999926584632718 ;
	setAttr ".rsrr" -type "double3" -6.361109362927032e-015 -5.2966875576601877e-031 
		-9.5416640443905487e-015 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "l_ankle_rig_fk_pointConstraint1" -p "l_ankle_rig_fk";
	rename -uid "8BF4425C-4D4A-62F3-6D19-968D5E9238B4";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_ankle_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -9.373230014375622e-008 1.794120407794253e-013 6.9025507087872029e-008 ;
	setAttr ".rst" -type "double3" 19.015319625082242 7.1054273576010019e-015 -1.0658141036401503e-014 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "l_knee_rig_fk_orientConstraint1" -p "l_knee_rig_fk";
	rename -uid "4DA75A2F-480C-4B12-1EBF-64935CED749E";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_knee_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 3.4428825550791841e-005 2.5857088276949331e-012 4.5061362282045937e-006 ;
	setAttr ".o" -type "double3" -3.4428825546972251e-005 1.8079360196813454e-028 -4.5061362272559727e-006 ;
	setAttr ".rsrr" -type "double3" -9.4141189075998543e-021 2.707720871895202e-012 
		1.62794758667245e-018 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "l_knee_rig_fk_pointConstraint1" -p "l_knee_rig_fk";
	rename -uid "826CFB4B-4517-5D0F-7B4E-D89D8647004A";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_knee_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 2.8953365927009145e-007 1.4954963738489369e-006 8.3821838359199319e-015 ;
	setAttr ".rst" -type "double3" 22.362855289533634 -1.7763568394002505e-015 -9.2703622556200571e-015 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "l_hip_rig_fk_orientConstraint1" -p "l_hip_rig_fk";
	rename -uid "85650DE7-4EE9-0171-EC4C-19A013398477";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_hip_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 3.4510405569137315e-005 -2.1718350372691162e-016 -3.8316051497422353e-006 ;
	setAttr ".o" -type "double3" -3.4510405569137234e-005 1.8079360196813445e-028 3.831605149741539e-006 ;
	setAttr ".rsrr" -type "double3" 7.762140692325601e-021 -2.3078531930641672e-012 
		-1.3911557573297049e-018 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "l_hip_rig_fk_pointConstraint1" -p "l_hip_rig_fk";
	rename -uid "2A80B09A-4D96-0F49-986F-338922527E61";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_hip_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 7.1054273576010019e-015 -1.7763568394002505e-015 -1.4210854715202004e-014 ;
	setAttr ".rst" -type "double3" -1.3658201354747561 2.6076276746516482 10.90128558246138 ;
	setAttr -k on ".w0";
createNode joint -n "r_hip_rig_ik" -p "hips_rig";
	rename -uid "6F0596AF-4B66-1F62-08A6-4DAAC367E4EC";
	addAttr -ci true -sn "r_hip_rig_ik_dup" -ln "r_hip_rig_ik_dup" -at "double";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.3658523436602081 2.6076262856699861 -10.901299999999996 ;
	setAttr ".r" -type "double3" 1.9158565064024546e-012 8.6226911565608046e-006 -2.5001012574909457e-012 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000071 -1.4574876722239449e-005 -1.3509368390890246 ;
createNode joint -n "r_knee_rig_ik" -p "r_hip_rig_ik";
	rename -uid "1A26AEDF-440B-9C53-A923-EA9E8B84CB5F";
	addAttr -ci true -sn "r_knee_rig_ik_dup" -ln "r_knee_rig_ik_dup" -at "double";
	setAttr ".t" -type "double3" -22.362815 5.6886437391767686e-006 -7.323351104426834e-007 ;
	setAttr ".r" -type "double3" -1.9640317343561368e-012 -1.8919150279044864e-005 5.6477937579466721e-013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 13.792099994027033 -8.4503093429707239e-006 ;
createNode joint -n "r_ankle_rig_ik" -p "r_knee_rig_ik";
	rename -uid "A5AABDF6-4B18-CBE3-521B-51B414228312";
	addAttr -ci true -sn "r_ankle_rig_ik_dup" -ln "r_ankle_rig_ik_dup" -at "double";
	setAttr ".t" -type "double3" -19.015331 1.9740161949499679e-006 -7.5268428227914796e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.6130247516217009e-005 -59.416969957865142 7.8288174523231932e-005 ;
createNode joint -n "r_ball_rig_ik" -p "r_ankle_rig_ik";
	rename -uid "F326AFD9-4D5B-B35F-549E-B782FBF1F661";
	addAttr -ci true -sn "r_ball_rig_ik_dup" -ln "r_ball_rig_ik_dup" -at "double";
	setAttr ".t" -type "double3" -10.493109997995459 8.0792280421349005e-006 1.5153519763444478e-006 ;
	setAttr ".r" -type "double3" 1.1986874483490304e-009 -1.5010319385937169e-015 4.7886621571268465e-018 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.029245795042113217 -36.202415693810153 0.049436443720274337 ;
createNode joint -n "r_toe_rig_ik" -p "r_ball_rig_ik";
	rename -uid "5570D08A-4F60-9D48-9E24-47A5190F6399";
	addAttr -ci true -sn "r_toe_rig_ik_dup" -ln "r_toe_rig_ik_dup" -at "double";
	setAttr ".t" -type "double3" -9.6354123294207898 1.2065748805412113e-005 -3.8492715181970283e-008 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode ikEffector -n "effector9" -p "r_ball_rig_ik";
	rename -uid "459883A3-4DD4-797C-5950-D189D45E84BB";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode orientConstraint -n "r_ankle_rig_ik_orientConstraint1" -p "r_ankle_rig_ik";
	rename -uid "69C46D9B-426D-0386-F017-788BB030AC79";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_foot_ik_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -126.20241582371524 2.5645170568445533e-005 89.999955884801437 ;
	setAttr ".o" -type "double3" 179.99996537475076 53.79757387982216 89.999925309476822 ;
	setAttr ".rsrr" -type "double3" 179.99999999999997 180.00000000000003 179.99999999999997 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector8" -p "r_ankle_rig_ik";
	rename -uid "750E4395-48EA-498C-DF2B-8C82B0B58777";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector7" -p "r_knee_rig_ik";
	rename -uid "DB9A86CE-443F-B828-13F5-D6AE2A2E75E9";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "r_hip_rig_fk" -p "hips_rig";
	rename -uid "351C545F-4547-F617-138F-B3984FCA588F";
	addAttr -ci true -sn "r_hip_rig_fk_dup" -ln "r_hip_rig_fk_dup" -at "double";
	setAttr ".v" no;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000071 -1.4574876722239449e-005 -1.3509368390890246 ;
createNode joint -n "r_knee_rig_fk" -p "r_hip_rig_fk";
	rename -uid "810766D2-4217-687C-D8B8-7689879DCDA3";
	addAttr -ci true -sn "r_knee_rig_fk_dup" -ln "r_knee_rig_fk_dup" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 13.792099994027033 -8.4503093429707239e-006 ;
createNode joint -n "r_ankle_rig_fk" -p "r_knee_rig_fk";
	rename -uid "33CAA2AA-4732-9D9C-97DE-028D8E0490C0";
	addAttr -ci true -sn "r_ankle_rig_fk_dup" -ln "r_ankle_rig_fk_dup" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.6130247516217009e-005 -59.416969957865142 7.8288174523231932e-005 ;
createNode joint -n "r_ball_rig_fk" -p "r_ankle_rig_fk";
	rename -uid "6D946CB9-495E-D31D-8949-D3A841817D43";
	addAttr -ci true -sn "r_ball_rig_fk_dup" -ln "r_ball_rig_fk_dup" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.029245795042113217 -36.202415693810153 0.049436443720274337 ;
createNode joint -n "r_toe_rig_fk" -p "r_ball_rig_fk";
	rename -uid "A7FAF9A0-4281-3808-3BAC-ADB540FE31C6";
	addAttr -ci true -sn "r_toe_rig_fk_dup" -ln "r_toe_rig_fk_dup" -at "double";
	setAttr ".t" -type "double3" -9.6354123294207898 1.2065748805412113e-005 -3.8492715181970283e-008 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode orientConstraint -n "r_ball_rig_fk_orientConstraint1" -p "r_ball_rig_fk";
	rename -uid "977942A9-4ACA-BA00-F144-379B2B9C1591";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_ball_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -90.000000228951706 8.5668272150766667e-008 89.960087533671739 ;
	setAttr ".o" -type "double3" 90.000328668282549 89.960087533671071 0.00032866818666834664 ;
	setAttr ".rsrr" -type "double3" 1.6138134453745884e-011 -6.3611093629256893e-015 
		-9.5416640443914464e-015 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "r_ball_rig_fk_pointConstraint1" -p "r_ball_rig_fk";
	rename -uid "0F3B4485-48F0-57A9-30B2-0C88C56F3E34";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_ball_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 2.6231145611177453e-007 4.2632564145606011e-013 4.7474979503192571e-009 ;
	setAttr ".rst" -type "double3" -10.493109997995459 8.0792280421349005e-006 1.5153519763444478e-006 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "r_ankle_rig_fk_orientConstraint1" -p "r_ankle_rig_fk";
	rename -uid "8620FFE0-4634-D84E-8C2F-46B2537AA455";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_ankle_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -126.20242612017435 2.564517062251496e-005 89.999955884801466 ;
	setAttr ".o" -type "double3" 179.99996537475076 53.79757387982216 89.999925309476822 ;
	setAttr ".rsrr" -type "double3" 179.99999999999997 180.00000000000003 179.99999999999997 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "r_ankle_rig_fk_pointConstraint1" -p "r_ankle_rig_fk";
	rename -uid "1C3B303B-4B9C-EF43-41A6-F7BCD6F1BE92";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_ankle_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 3.9994575118385001e-007 -1.5099033134902129e-013 6.8329583768900193e-008 ;
	setAttr ".rst" -type "double3" -19.015331074775954 1.9740161949499679e-006 -7.5268428227914796e-006 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "r_knee_rig_fk_orientConstraint1" -p "r_knee_rig_fk";
	rename -uid "E3468515-46F9-DB4F-ADAA-A9BC1E021DA2";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_knee_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 7.8856085015196032e-014 -1.0296459092941109e-005 -3.2123568232602822e-013 ;
	setAttr ".o" -type "double3" 0 1.0296459124746655e-005 0 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "r_knee_rig_fk_pointConstraint1" -p "r_knee_rig_fk";
	rename -uid "480A310C-4F53-3A43-1CD4-D99C495D4469";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_knee_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -3.6387670832027652e-007 -5.9713196165489535e-008 -3.4736159566506331e-006 ;
	setAttr ".rst" -type "double3" -22.362815337331 5.6886437391767686e-006 -7.323351104426834e-007 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "r_hip_rig_fk_orientConstraint1" -p "r_hip_rig_fk";
	rename -uid "90946AD0-4DEB-65A2-3290-FEAC99B1ABE2";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_hip_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 7.7937562203048484e-014 8.6226911739439823e-006 -6.0331140749429829e-014 ;
	setAttr ".o" -type "double3" 0 -8.6226911393554512e-006 0 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "r_hip_rig_fk_pointConstraint1" -p "r_hip_rig_fk";
	rename -uid "436DFF23-48A4-0BA5-A4F7-4AA6008DBB0E";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_hip_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 5.6843418860808015e-014 6.830092047493963e-013 -8.8817841970012523e-015 ;
	setAttr ".rst" -type "double3" -1.3658523436602081 2.6076262856699861 -10.901299999999996 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "pelvis_rig_pointConstraint1" -p "pelvis_rig";
	rename -uid "5E49040A-4C46-D1C9-2287-AAAB6B7CDD85";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "pelvis_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 48.502845764160156 -9.3133430480957031 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "pelvis_rig_orientConstraint1" -p "pelvis_rig";
	rename -uid "A2F4C59C-4412-2B33-C181-76BAB521FF49";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "pelvis_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -83.178232923108396 -89.999999999999957 0 ;
	setAttr ".o" -type "double3" 89.999999999999986 6.8217670768915966 89.999999999999957 ;
	setAttr ".rsrr" -type "double3" -1.5902773407317584e-014 3.1805546814635176e-015 
		6.3611093629270327e-015 ;
	setAttr -k on ".w0";
createNode joint -n "spineParent_rig" -p "rig_skeleton";
	rename -uid "D63A4000-4463-C886-1BC1-C1975D8AD4BD";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "spineParentJnt" -ln "spineParentJnt" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999844 -2.9736117437579219 90.000000000000071 ;
createNode joint -n "neckBase_rig" -p "spineParent_rig";
	rename -uid "CE157D7D-4D46-8923-CC1A-9FBED454431E";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "neckBaseJnt" -ln "neckBaseJnt" -at "double";
	setAttr ".t" -type "double3" 16.582691502051858 -2.1316282072803006e-014 -3.7922176028118758e-008 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 20.55278972913904 ;
createNode joint -n "neck_mid_rig" -p "neckBase_rig";
	rename -uid "2046E3BB-4126-3C6B-C562-4D961D5DA5DA";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "neckMidJnt" -ln "neckMidJnt" -at "double";
	setAttr ".t" -type "double3" 19.769148705961925 -2.1316282072803006e-014 -6.3548436880882648e-008 ;
	setAttr ".r" -type "double3" 1.2358937634540444e-030 -4.4527765540489235e-014 -3.1805546814635168e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -113.52640147289696 -89.999999999999829 0 ;
createNode joint -n "neck_rig" -p "neck_mid_rig";
	rename -uid "314AD6D2-446D-DCD9-8B95-2F9571CB32B2";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "neckJnt" -ln "neckJnt" -at "double";
	setAttr ".t" -type "double3" 0 -2.8421709430404007e-014 0 ;
	setAttr ".r" -type "double3" -3.1805546814639368e-015 -2.2263882770244617e-013 2.1627771833951915e-013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 6.3611093629265792e-015 2.2504626351166665e-013 -2.2479385798591407e-013 ;
createNode joint -n "l_eye_rig" -p "neck_rig";
	rename -uid "49C7CA66-4B43-7720-E8BA-6BB6B80017AB";
	addAttr -ci true -sn "l_eyeJnt" -ln "l_eyeJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 3.2760000000000242 4.4439999999999458 5.8279999999999781 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 6.3611093629265792e-015 2.2504626351166665e-013 -2.2479385798591407e-013 ;
createNode joint -n "r_eye_rig" -p "neck_rig";
	rename -uid "05D75E52-45DA-C763-D50E-A1BB67026445";
	addAttr -ci true -sn "r_eyeJnt" -ln "r_eyeJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -3.2759998890397188 4.4444147044847995 5.8280005273533195 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 180 0 0 ;
createNode joint -n "jaw_rig" -p "neck_rig";
	rename -uid "FBFD8F61-4AC2-FBE3-765A-2A9B748A86D5";
	addAttr -ci true -sn "jawJnt" -ln "jawJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 1.0000001116525112e-006 -6.0659707934836149 12.622518224451861 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 6.3611093629265792e-015 2.2504626351166665e-013 -2.2479385798591407e-013 ;
createNode joint -n "l_clav_rig" -p "spineParent_rig";
	rename -uid "85C18C1D-4760-3726-2D95-DD9797C96506";
	addAttr -ci true -sn "l_clavJnt" -ln "l_clavJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.0052494338314551039 -83.041026263282177 -92.968323351189696 ;
createNode joint -n "l_shoulder_rig" -p "l_clav_rig";
	rename -uid "6548D43D-48FE-FF4E-F790-8C8F9F01C063";
	addAttr -ci true -sn "l_shoulderJnt" -ln "l_shoulderJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4765013140818527e-005 -9.7940192453054067 -0.0006391982343617763 ;
	setAttr -k on ".blendSpace";
createNode joint -n "l_elbow_rig" -p "l_shoulder_rig";
	rename -uid "543804BF-4B13-86C6-BE77-9BA3272B3A33";
	addAttr -ci true -sn "l_elbowJnt" -ln "l_elbowJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.2708081109434524e-006 -12.235272407050054 1.1856709489641087e-005 ;
createNode joint -n "l_wrist_rig" -p "l_elbow_rig";
	rename -uid "77792121-4F93-7EFE-962C-2B985FD8E268";
	addAttr -ci true -sn "l_wristJnt" -ln "l_wristJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr -k on ".blendSpace";
createNode joint -n "l_index_01_rig" -p "l_wrist_rig";
	rename -uid "BFA9A763-4292-87B6-6809-75B45F5BC693";
	addAttr -ci true -sn "l_indexJnt01" -ln "l_indexJnt01" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 10.392254512342582 1.9517022060271927 4.6647774069106287 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.327136464344079 6.7733761696721375 -2.5614952482332125 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_index_02_rig" -p "l_index_01_rig";
	rename -uid "A713F6C3-4302-4ED9-5BA4-AC91CDFB6238";
	addAttr -ci true -sn "l_indexJnt02" -ln "l_indexJnt02" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 6.7088787484487611 0 -7.1054273576010019e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.035869761174582 -15.470899537925916 -0.69135407776869517 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_index_03_rig" -p "l_index_02_rig";
	rename -uid "C8164EEA-43D8-48ED-2894-EF84441468ED";
	addAttr -ci true -sn "l_indexJnt03" -ln "l_indexJnt03" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 6.332299490449941 8.9338259012805565e-016 4.2632564145606011e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.3459905161050147e-014 1.0205866691719738e-014 -1.590277340731758e-015 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_pinky_01_rig" -p "l_wrist_rig";
	rename -uid "9E8A9D92-4666-1BC1-EC76-2DACE4BF5606";
	addAttr -ci true -sn "l_pinkyJnt01" -ln "l_pinkyJnt01" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 8.9227837627418651 -0.54561132659469536 -2.522599826637812 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 88.631908137195964 10.807330603990408 -5.2543246032494437 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_pinky_02_rig" -p "l_pinky_01_rig";
	rename -uid "75AFE274-4496-4B2B-8ABB-A1AA7C6BD254";
	addAttr -ci true -sn "l_pinkyJnt02" -ln "l_pinkyJnt02" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 5.5703760322813665 1.7763568394002505e-015 -1.4210854715202004e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.4452932773809029 -10.922608651197676 3.3832416023724741 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_pinky_03_rig" -p "l_pinky_02_rig";
	rename -uid "3DC43632-4879-2180-A94B-F294D2FE06C7";
	addAttr -ci true -sn "l_pinkyJnt03" -ln "l_pinkyJnt03" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 5.7079347910987934 0 -4.2632564145606011e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.0288035131007033e-014 2.420549735100638e-015 1.5902773407317588e-015 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_thumb_01_rig" -p "l_wrist_rig";
	rename -uid "9309D1FF-462C-A754-2130-228ECFD1E639";
	addAttr -ci true -sn "l_thumbJnt01" -ln "l_thumbJnt01" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 6.5995473060819876 0.12779103382383994 6.8323541418931093 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 80.839478022473841 -30.646912509941682 -26.197877778844422 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_thumb_02_rig" -p "l_thumb_01_rig";
	rename -uid "62629BF9-4A72-C75B-4B97-7680F859E385";
	addAttr -ci true -sn "l_thumbJnt02" -ln "l_thumbJnt02" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 6.0789227234193559 5.3290705182007514e-015 -4.2632564145606011e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.5005321103412927 2.8111139027753347 -15.754360215619952 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_thumb_03_rig" -p "l_thumb_02_rig";
	rename -uid "E127BDF5-4668-42F0-17AE-078296E6BC42";
	addAttr -ci true -sn "l_thumbJnt03" -ln "l_thumbJnt03" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" 4.6326764669463145 7.9936057773011271e-015 -1.4210854715202004e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 2.3574106276806322e-014 -1.0663118948207636e-014 -3.1805546814635176e-015 ;
	setAttr ".radi" 0.5;
createNode pointConstraint -n "l_wrist_rig_pointConstraint1" -p "l_wrist_rig";
	rename -uid "7502C278-4B85-AFFD-9113-9587E68BA526";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_wrist_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "l_wrist_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 17.154433690736056 4.2632564145606011e-014 1.7763568394002505e-014 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "l_wrist_rig_orientConstraint1" -p "l_wrist_rig";
	rename -uid "F5E5213D-4A4D-F552-0368-138C0F1B5805";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_wrist_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "l_wrist_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "l_arm_ik_switch" -p "l_wrist_rig";
	rename -uid "4BDEF618-45D9-0728-533C-36813EA344E1";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "l_arm_ik_fk" -ln "l_arm_ik_fk" -at "double";
	addAttr -ci true -sn "IkFkSwitch" -ln "IkFkSwitch" -dv 1 -min 0 -max 1 -at "double";
	addAttr -ci true -sn "_" -ln "_" -min 0 -max 0 -en "Lock" -at "enum";
	addAttr -ci true -sn "elbowLock" -ln "elbowLock" -at "double";
	addAttr -ci true -sn "__" -ln "__" -min 0 -max 0 -en "stretch" -at "enum";
	addAttr -ci true -sn "autoStretch" -ln "autoStretch" -dv 1 -min 0 -max 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -51.944298742176876 -91.607130037829364 17.446518106325417 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" -5.731018257315739e-005 15.07031794498692 -1.630602446749386e-005 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr ".s" -type "double3" 1 0.99999999999999967 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 54.693946885477828 111.60714816239887 -3.3406615002818718 ;
	setAttr ".rpt" -type "double3" -2.7496476528180596 -1.812457886496318e-005 -14.105875923047364 ;
	setAttr ".sp" -type "double3" 54.693946885477828 111.60714816239891 -3.3406615002818718 ;
	setAttr ".spt" -type "double3" 0 -4.2632564145605999e-014 0 ;
	setAttr -k on ".IkFkSwitch";
	setAttr -cb on "._";
	setAttr -k on ".elbowLock";
	setAttr -cb on ".__";
	setAttr -k on ".autoStretch";
createNode nurbsCurve -n "l_arm_ik_switchShape" -p "l_arm_ik_switch";
	rename -uid "633B4EEB-4039-3AAB-368A-6DBEB8A9812E";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 10 0 no 3
		11 0 1 2 3 4 5 6 7 8 9 10
		11
		53.245535427313655 112.60714812561223 -3.7306670405400659
		52.279928246547385 111.60714810108855 -3.9906722778051318
		53.245535976925467 110.60714812561316 -3.7306688930515897
		53.245535839522518 111.10714812561294 -3.7306684299237087
		56.142358206239052 111.10714819918537 -2.950655496895795
		56.142358343642016 110.6071481991856 -2.9506559600236759
		57.107965524408286 111.60714822370927 -2.6906507227586101
		56.14235779403019 112.60714819918466 -2.9506541075121522
		56.142357931433153 112.10714819918491 -2.9506545706400331
		53.245535564716604 112.10714812561247 -3.7306675036679469
		53.245535427313655 112.60714812561223 -3.7306670405400659
		;
createNode parentConstraint -n "l_elbow_rig_parentConstraint1" -p "l_elbow_rig";
	rename -uid "75D7BB0C-491A-F8B6-297A-01A6AED7601D";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_elbow_lock_01W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 6.3183349888063188e-017 -4.7708320246204647e-015 3.6573925656645904e-016 ;
	setAttr ".rst" -type "double3" 18.977118289932516 1.8474111129762605e-013 -8.8817841970012523e-015 ;
	setAttr ".rsrr" -type "double3" 6.3182212433071334e-017 -4.7708320246204063e-015 
		3.6573325074920081e-016 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "l_shoulder_rig_pointConstraint1" -p "l_shoulder_rig";
	rename -uid "42B02926-4989-C103-4C13-D4B7C1D6296E";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_shoulder_rig_ikW0" -dv 1 -min 0 
		-at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "l_shoulder_rig_fkW1" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 12.265966789427523 -4.2632564145606011e-014 -7.9936057773011271e-015 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "l_shoulder_rig_orientConstraint1" -p "l_shoulder_rig";
	rename -uid "48DD0E65-4B92-72FC-5E15-BAB56FA023CE";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_shoulder_rig_ikW0" -dv 1 -min 0 
		-at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "l_shoulder_rig_fkW1" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -3.1860872625439421e-017 1.5902770893211976e-015 1.6210257091440306e-016 ;
	setAttr ".rsrr" -type "double3" -3.1860872625439421e-017 1.5902770893211976e-015 
		1.6210257091440306e-016 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "l_shoulder_rig_ik" -p "l_clav_rig";
	rename -uid "695D31EF-425E-8720-4F04-9B8B3F8B35F9";
	addAttr -ci true -sn "l_shoulder_rig_ik_dup" -ln "l_shoulder_rig_ik_dup" -at "double";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 12.265966789427523 -2.8421709430404007e-014 -7.9936057773011271e-015 ;
	setAttr ".r" -type "double3" -8.4939111191549663e-007 -8.3086385038109533e-014 -8.6395613288043798e-008 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4765013140758672e-005 -9.7940192453054049 -0.00063919823436161172 ;
createNode joint -n "l_elbow_rig_ik" -p "l_shoulder_rig_ik";
	rename -uid "5AED57E2-4A5E-DE32-D05E-20B82BE516F2";
	addAttr -ci true -sn "l_elbow_rig_ik_dup" -ln "l_elbow_rig_ik_dup" -at "double";
	setAttr ".t" -type "double3" 18.977118 1.1368683772161603e-013 -1.2434497875801753e-014 ;
	setAttr ".r" -type "double3" -1.4641510893004201e-022 -1.7674278221681948e-007 1.686628811865323e-013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.270808110943453e-006 -12.235272407050054 1.1856709489641094e-005 ;
createNode joint -n "l_wrist_rig_ik" -p "l_elbow_rig_ik";
	rename -uid "81874E3E-41E0-39DC-E4E4-12A030F6885C";
	addAttr -ci true -sn "l_wrist_rig_ik_dup" -ln "l_wrist_rig_ik_dup" -at "double";
	setAttr ".t" -type "double3" 17.154434 -1.4210854715202004e-014 1.4210854715202004e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode orientConstraint -n "l_wrist_rig_ik_orientConstraint1" -p "l_wrist_rig_ik";
	rename -uid "E3FBC636-4AFA-0636-65C1-DF8BD7C94FCE";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_wrist_ctrl_01W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -5.7310182573160785e-005 15.070317944986915 -1.6306024467660095e-005 ;
	setAttr ".o" -type "double3" 5.4960787232840238e-005 -15.070317944995461 1.4551771797146656e-006 ;
	setAttr ".rsrr" -type "double3" -1.8009704037847161e-020 3.1805546814649624e-015 
		3.0332133116374161e-021 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector2" -p "l_elbow_rig_ik";
	rename -uid "46D0717D-4129-0BE6-9F9F-54819602DDF3";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "l_shoulder_rig_fk" -p "l_clav_rig";
	rename -uid "4BA9AC6F-498D-4CF0-6244-7A9793C04941";
	addAttr -ci true -sn "l_shoulder_rig_fk_dup" -ln "l_shoulder_rig_fk_dup" -at "double";
	setAttr ".v" no;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4765013140758672e-005 -9.7940192453054049 -0.00063919823436161172 ;
createNode joint -n "l_elbow_rig_fk" -p "l_shoulder_rig_fk";
	rename -uid "721A7394-4923-979C-8AE7-6693FD026E3C";
	addAttr -ci true -sn "l_elbow_rig_fk_dup" -ln "l_elbow_rig_fk_dup" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.270808110943453e-006 -12.235272407050054 1.1856709489641094e-005 ;
createNode joint -n "l_wrist_rig_fk" -p "l_elbow_rig_fk";
	rename -uid "57EF9C03-4C91-7475-A1DF-F1B860847F8A";
	addAttr -ci true -sn "l_wrist_rig_fk_dup" -ln "l_wrist_rig_fk_dup" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode orientConstraint -n "l_wrist_rig_fk_orientConstraint1" -p "l_wrist_rig_fk";
	rename -uid "F2280D78-4609-6ECC-9364-24A24FD34CAF";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_wrist_fk_ctrl_01W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "l_wrist_rig_fk_pointConstraint1" -p "l_wrist_rig_fk";
	rename -uid "9AC40CC2-4BD9-C063-9847-0FA1843E4358";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_wrist_fk_ctrl_01W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1.4921397450962104e-013 4.2632564145606011e-014 -3.1974423109204508e-014 ;
	setAttr ".rst" -type "double3" 17.15443369073607 -1.4210854715202004e-014 1.4210854715202004e-014 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "l_elbow_rig_fk_orientConstraint1" -p "l_elbow_rig_fk";
	rename -uid "B6B1E6B7-4B0E-FFD8-A0CB-7581BB5E0258";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_elbow_fk_ctrl_01W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 5.6627248890790925e-017 -7.9513867090053519e-015 3.5024169797882898e-017 ;
	setAttr ".rsrr" -type "double3" 5.6626680163294998e-017 -4.770832027541776e-015 
		3.5022713800591793e-017 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "l_elbow_rig_fk_pointConstraint1" -p "l_elbow_rig_fk";
	rename -uid "FA2C7540-4017-A100-13BD-7089DAE44CE9";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_elbow_fk_ctrl_01W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 4.9737991503207013e-014 1.5631940186722204e-013 2.3092638912203256e-014 ;
	setAttr ".rst" -type "double3" 18.977118289932537 1.1368683772161603e-013 -1.2434497875801753e-014 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "l_shoulder_rig_fk_orientConstraint1" -p "l_shoulder_rig_fk";
	rename -uid "F5C5DCF1-4A1E-CA91-2AF0-25BA238F5A19";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_shoulder_fk_ctrl_01W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.2132853246549667e-020 -1.5902773408656293e-015 
		2.0714401380595119e-021 ;
	setAttr ".rsrr" -type "double3" -1.2132853246549667e-020 -6.6935718998783466e-026 
		1.0357200690518254e-021 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "l_shoulder_rig_fk_pointConstraint1" -p "l_shoulder_rig_fk";
	rename -uid "61B67DF7-42F4-5D9A-4DA3-60810C7E53E3";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_shoulder_fk_ctrl_01W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -1.4210854715202004e-014 5.6843418860808015e-014 1.2434497875801753e-014 ;
	setAttr ".rst" -type "double3" 12.265966789427523 -2.8421709430404007e-014 -7.9936057773011271e-015 ;
	setAttr -k on ".w0";
createNode joint -n "l_shoulderFollow_rig" -p "l_clav_rig";
	rename -uid "DC2B9050-4871-0844-A885-6B9E44E68C6B";
	setAttr ".t" -type "double3" 12.265966789427523 -5.6843418860808015e-014 -7.9936057773011271e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 2.1843442095565113e-014 -6.9589737073656526 -0.00064073427140352195 ;
createNode orientConstraint -n "l_clav_rig_orientConstraint1" -p "l_clav_rig";
	rename -uid "E2338FBC-4B56-5680-B93A-3883F359C410";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_clav_rig_ctrl_01W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.2686455326742441e-014 -6.9589737073658213 -0.00064073427116880346 ;
	setAttr ".o" -type "double3" 7.8206606344875665e-005 6.9589737069285329 0.00064548948831560774 ;
	setAttr ".rsrr" -type "double3" -1.5785359018487765e-038 1.5902773407780416e-015 
		-1.1374549918640316e-021 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "l_clav_rig_pointConstraint1" -p "l_clav_rig";
	rename -uid "D199C6D6-42CA-BFAE-966C-209A6236CE8B";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_clav_rig_ctrl_01W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 5.6843418860808015e-014 0 2.3980817331903381e-014 ;
	setAttr ".rst" -type "double3" 11.62216417704029 1.578078361225792 7.0000007956332064 ;
	setAttr -k on ".w0";
createNode joint -n "r_clav_rig" -p "spineParent_rig";
	rename -uid "6ED25ADB-4A5A-B00F-1999-C68C61601CB3";
	addAttr -ci true -sn "r_clavJnt" -ln "r_clavJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.0052494338286986691 -83.04102626328212 87.031676648807505 ;
createNode joint -n "r_shoulder_rig" -p "r_clav_rig";
	rename -uid "C7FC10F4-4967-9D2F-C8B9-34BB86EBF8BB";
	addAttr -ci true -sn "r_shoulderJnt" -ln "r_shoulderJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4765013192861383e-005 -9.7940192453054085 -0.00063919823436477889 ;
	setAttr -k on ".blendSpace";
createNode joint -n "r_elbow_rig" -p "r_shoulder_rig";
	rename -uid "21C81AF9-4DF0-E20F-5E3B-1381C5B3F528";
	addAttr -ci true -sn "r_elbowJnt" -ln "r_elbowJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.2708081249146315e-006 -12.235272407050056 1.1856709482151905e-005 ;
createNode joint -n "r_wrist_rig" -p "r_elbow_rig";
	rename -uid "2A49276A-48CB-053F-DA3F-4C8728C464EB";
	addAttr -ci true -sn "r_wristJnt" -ln "r_wristJnt" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	addAttr -ci true -sn "blendSpace" -ln "blendSpace" -dv 1 -min 0 -max 1 -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr -k on ".blendSpace";
createNode joint -n "r_index_01_rig" -p "r_wrist_rig";
	rename -uid "8AFBB06C-4D7A-93B2-0024-718DAC0F5FA5";
	addAttr -ci true -sn "r_indexJnt01" -ln "r_indexJnt01" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -10.392278887692733 -1.9517042506126501 -4.6647743389106289 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.327136464344278 6.7733761696717796 -2.5614952482327582 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_index_02_rig" -p "r_index_01_rig";
	rename -uid "22675DAF-49EE-0FE9-8D43-2B8A0B3DF7A4";
	addAttr -ci true -sn "r_indexJnt02" -ln "r_indexJnt02" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -6.7089432016256154 1.231613507446383e-005 4.0120761966022656e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.0358697611753049 -15.470899537925893 -0.69135407776869684 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_index_03_rig" -p "r_index_02_rig";
	rename -uid "08C97F56-4D47-2A87-072D-09B160A4BE57";
	addAttr -ci true -sn "r_indexJnt03" -ln "r_indexJnt03" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -6.332253924324398 -1.5478452075560339e-006 1.9382374503607025e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.2435019340449536e-014 6.1824814748656861e-015 3.1805546814635176e-015 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_pinky_01_rig" -p "r_wrist_rig";
	rename -uid "F3E99913-474B-04E2-B6E3-FA94FB33636C";
	addAttr -ci true -sn "r_pinkyJnt01" -ln "r_pinkyJnt01" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -8.9227898936340324 0.54560265527936735 2.5226020357983181 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 88.631908137196191 10.807330603990067 -5.254324603248957 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_pinky_02_rig" -p "r_pinky_01_rig";
	rename -uid "1D445E2E-4ED8-F908-4684-6DB99EDCAA99";
	addAttr -ci true -sn "r_pinkyJnt02" -ln "r_pinkyJnt02" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -5.5704052171290499 5.1312034594630518e-006 7.109870270483043e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.4452932773795741 -10.922608651197645 3.3832416023724665 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_pinky_03_rig" -p "r_pinky_02_rig";
	rename -uid "C7B6A5AA-45DA-4DCA-0A7D-94877C033948";
	addAttr -ci true -sn "r_pinkyJnt03" -ln "r_pinkyJnt03" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -5.7079534197613668 1.1124028187481372e-006 -7.3710476328869845e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -3.4563647767071607e-014 1.0912309918426207e-014 -3.180554681463516e-015 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_thumb_01_rig" -p "r_wrist_rig";
	rename -uid "555D1959-49F3-EA71-09D7-C8A3CF0B42EE";
	addAttr -ci true -sn "r_thumbJnt01" -ln "r_thumbJnt01" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -6.5996136290624179 -0.12780643716077122 -6.8323332105596304 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 80.839478022474225 -30.64691250994192 -26.19787777884417 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_thumb_02_rig" -p "r_thumb_01_rig";
	rename -uid "E821654D-433C-8FEE-98C6-9A88106DCBA7";
	addAttr -ci true -sn "r_thumbJnt02" -ln "r_thumbJnt02" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -6.0788805927821912 -3.8894801447586502e-005 9.1199209748538124e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.5005321103409244 2.8111139027752969 -15.754360215619936 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_thumb_03_rig" -p "r_thumb_02_rig";
	rename -uid "D0B553BF-47AC-DAF5-2E85-E4B53FEA1775";
	addAttr -ci true -sn "r_thumbJnt03" -ln "r_thumbJnt03" -at "double";
	addAttr -ci true -sn "rigJoint" -ln "rigJoint" -at "double";
	setAttr ".t" -type "double3" -4.6326928260512759 2.5756352154626683e-005 -3.5911719820092003e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -5.9677117495817001e-014 -1.776117886907979e-016 6.361109362927032e-015 ;
	setAttr ".radi" 0.5;
createNode pointConstraint -n "r_wrist_rig_pointConstraint1" -p "r_wrist_rig";
	rename -uid "8580FD71-44A6-5E45-0A59-4C87C55A8E00";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_wrist_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "r_wrist_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -17.154387165172857 4.206812036500196e-007 -1.5932284636477334e-005 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "r_wrist_rig_orientConstraint1" -p "r_wrist_rig";
	rename -uid "CE7007AE-4EED-6E5A-6CC0-A886A2B24DD0";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_wrist_rig_ikW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "r_wrist_rig_fkW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 0 2.5029735009150446e-006 0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "r_arm_ik_switch" -p "r_wrist_rig";
	rename -uid "302CD24E-4D26-81AD-C43D-599AA4D81BCD";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "r_arm_ik_fk" -ln "r_arm_ik_fk" -at "double";
	addAttr -ci true -sn "IkFkSwitch" -ln "IkFkSwitch" -dv 1 -min 0 -max 1 -at "double";
	addAttr -ci true -sn "_" -ln "_" -min 0 -max 0 -en "Lock" -at "enum";
	addAttr -ci true -sn "elbowLock" -ln "elbowLock" -at "double";
	addAttr -ci true -sn "__" -ln "__" -min 0 -max 0 -en "stretch" -at "enum";
	addAttr -ci true -sn "autoStretch" -ln "autoStretch" -dv 1 -min 0 -max 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 51.944253859287926 91.607081875444948 -17.446504467168701 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 179.99994268981763 15.070317944986536 -1.6306023962631415e-005 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -54.693900000000021 111.6070999999998 -3.3406600000001569 ;
	setAttr ".rpt" -type "double3" 2.7496456502294127 -223.21418187543546 20.787183784172594 ;
	setAttr ".sp" -type "double3" -54.693900000000021 111.6070999999998 -3.3406600000001561 ;
	setAttr ".spt" -type "double3" 0 0 -8.8817841970012543e-016 ;
	setAttr -k on ".IkFkSwitch";
	setAttr -cb on "._";
	setAttr -k on ".elbowLock";
	setAttr -cb on ".__";
	setAttr -k on ".autoStretch";
createNode nurbsCurve -n "r_arm_ik_switchShape" -p "r_arm_ik_switch";
	rename -uid "C02EF9B0-4DC7-3F77-88C9-B384BA53E432";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 10 0 no 3
		11 0 1 2 3 4 5 6 7 8 9 10
		11
		-56.142311458164201 110.60710003678648 -2.950654459741969
		-57.107918638930471 111.60710006131013 -2.6906492224769103
		-56.142310908552396 112.60710003678554 -2.9506526072304453
		-56.142311045955346 112.10710003678577 -2.950653070358328
		-53.245488679238804 112.10709996321336 -3.7306660033862222
		-53.245488541835854 112.60709996321313 -3.7306655402583395
		-52.27988136106957 111.60709993868946 -3.9906707775233983
		-53.245489091447659 110.60709996321407 -3.7306673927698633
		-53.24548895404471 111.10709996321383 -3.7306669296419841
		-56.142311320761252 111.10710003678624 -2.9506539966140899
		-56.142311458164201 110.60710003678648 -2.950654459741969
		;
createNode parentConstraint -n "r_elbow_rig_parentConstraint1" -p "r_elbow_rig";
	rename -uid "9A252385-4BBC-7C6D-1C6D-CF96A4941A9B";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_elbow_lock_01W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -7.300182813577406e-014 -2.5029734722900447e-006 2.4184600303568336e-012 ;
	setAttr ".rst" -type "double3" -18.977126566327065 5.0134275397795136e-007 1.7840870825125421e-006 ;
	setAttr ".rsrr" -type "double3" -1.1189571300588428e-015 6.361109486507842e-015 
		9.6261855425985452e-016 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "r_shoulder_rig_pointConstraint1" -p "r_shoulder_rig";
	rename -uid "D6378DF6-4023-58C0-F458-A68FDC55355D";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_shoulder_rig_ikW0" -dv 1 -min 0 
		-at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "r_shoulder_rig_fkW1" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -12.265961753792824 3.7169301720041403e-005 3.2009490800177787e-006 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "r_shoulder_rig_orientConstraint1" -p "r_shoulder_rig";
	rename -uid "DFFB297F-4FF1-8DCE-FE8F-A2AFC5427436";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_shoulder_rig_ikW0" -dv 1 -min 0 
		-at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "r_shoulder_rig_fkW1" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -1.2732197997649351e-014 6.3610396959294885e-015 -6.7652553365743781e-016 ;
	setAttr ".rsrr" -type "double3" -1.2732197997649351e-014 6.3610396959294885e-015 
		-6.7652553365743781e-016 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "r_shoulder_rig_ik" -p "r_clav_rig";
	rename -uid "1C21B9AF-4E83-E4CD-8AFD-6BAEB7E554E4";
	addAttr -ci true -sn "r_shoulder_rig_ik_dup" -ln "r_shoulder_rig_ik_dup" -at "double";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -12.265961753792824 3.7169301705830549e-005 3.2009490800177787e-006 ;
	setAttr ".r" -type "double3" 8.6595297350799671e-007 1.1962742299341675e-006 -7.6431368888256852e-008 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4765013180245966e-005 -9.7940192453054014 -0.0006391982343654654 ;
createNode joint -n "r_elbow_rig_ik" -p "r_shoulder_rig_ik";
	rename -uid "375CDD0D-4120-8FF5-342C-66B491B504FF";
	addAttr -ci true -sn "r_elbow_rig_ik_dup" -ln "r_elbow_rig_ik_dup" -at "double";
	setAttr ".t" -type "double3" -18.977127 5.0134281082137022e-007 1.7840870860652558e-006 ;
	setAttr ".r" -type "double3" -6.1383274757075128e-014 -2.502973485737868e-006 2.4174973902468696e-012 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.2708081249146319e-006 -12.235272407050056 1.1856709482151912e-005 ;
createNode joint -n "r_wrist_rig_ik" -p "r_elbow_rig_ik";
	rename -uid "DFD6B487-4516-62FF-FFBF-2DAF18DB723C";
	addAttr -ci true -sn "r_wrist_rig_ik_dup" -ln "r_wrist_rig_ik_dup" -at "double";
	setAttr ".t" -type "double3" -17.154387 4.2068121786087431e-007 -1.5932284629371907e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode orientConstraint -n "r_wrist_rig_ik_orientConstraint1" -p "r_wrist_rig_ik";
	rename -uid "96AA0C1A-4C12-6B8C-72F4-C79C7D110695";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_wrist_ctrl_01W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 179.99994268981689 15.070320447960011 -1.6306024153085445e-005 ;
	setAttr ".o" -type "double3" -179.99994503921283 15.070317944995063 -1.4551767080177695e-006 ;
	setAttr ".rsrr" -type "double3" -6.0905349818273936e-016 9.5416640443905503e-015 
		-4.54981996750684e-021 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector3" -p "r_elbow_rig_ik";
	rename -uid "0E52986A-4B36-1865-8088-689C9DF66876";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "r_shoulder_rig_fk" -p "r_clav_rig";
	rename -uid "4E0C07B9-48F5-1192-64B1-379A0D6DDD0F";
	addAttr -ci true -sn "r_shoulder_rig_fk_dup" -ln "r_shoulder_rig_fk_dup" -at "double";
	setAttr ".v" no;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4765013180245966e-005 -9.7940192453054014 -0.0006391982343654654 ;
createNode joint -n "r_elbow_rig_fk" -p "r_shoulder_rig_fk";
	rename -uid "9FF58DD6-4E59-7617-3EEA-3289F731E3E5";
	addAttr -ci true -sn "r_elbow_rig_fk_dup" -ln "r_elbow_rig_fk_dup" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.2708081249146319e-006 -12.235272407050056 1.1856709482151912e-005 ;
createNode joint -n "r_wrist_rig_fk" -p "r_elbow_rig_fk";
	rename -uid "10669496-46CD-9AB1-9086-BEA76CB7E794";
	addAttr -ci true -sn "r_wrist_rig_fk_dup" -ln "r_wrist_rig_fk_dup" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode orientConstraint -n "r_wrist_rig_fk_orientConstraint1" -p "r_wrist_rig_fk";
	rename -uid "8B5581E2-4331-7D74-A325-1A800A0003B7";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_wrist_fk_ctrl_01W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "r_wrist_rig_fk_pointConstraint1" -p "r_wrist_rig_fk";
	rename -uid "5E27D305-45D0-F60A-B017-74A2182452AF";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_wrist_fk_ctrl_01W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -2.0605739337042905e-013 -8.5265128291212022e-014 4.9737991503207013e-014 ;
	setAttr ".rst" -type "double3" -17.154387165172871 4.2068121786087431e-007 -1.5932284629371907e-005 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "r_elbow_rig_fk_orientConstraint1" -p "r_elbow_rig_fk";
	rename -uid "86823BAB-4F8E-E3A9-BF59-F9B573A321B9";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_elbow_fk_ctrl_01W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 3.5635911144094902e-014 -6.3611130294004491e-015 -5.8131834216065465e-015 ;
	setAttr ".rsrr" -type "double3" 3.5635901665303306e-014 -1.2722222392326459e-014 
		-5.8131717345945092e-015 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "r_elbow_rig_fk_pointConstraint1" -p "r_elbow_rig_fk";
	rename -uid "ABABC2E2-48C9-4F28-B252-F0A4CA8F4470";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_elbow_fk_ctrl_01W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 2.1316282072803006e-014 -4.9737991503207013e-013 -2.1316282072803006e-014 ;
	setAttr ".rst" -type "double3" -18.977126566327041 5.0134281082137022e-007 1.7840870860652558e-006 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "r_shoulder_rig_fk_orientConstraint1" -p "r_shoulder_rig_fk";
	rename -uid "435EC066-48CB-82D8-F528-1390C4B7FCB7";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_shoulder_fk_ctrl_01W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -6.0664266232748327e-020 3.1805546813296447e-015 -1.9205421180783865e-019 ;
	setAttr ".rsrr" -type "double3" -6.0664266232748327e-020 -3.1805546817981946e-015 
		-1.889470515983438e-019 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "r_shoulder_rig_fk_pointConstraint1" -p "r_shoulder_rig_fk";
	rename -uid "C3516104-41D5-E752-C0B8-9892D141A2B0";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_shoulder_fk_ctrl_01W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -3.5527136788005009e-015 -2.2737367544323206e-013 3.907985046680551e-014 ;
	setAttr ".rst" -type "double3" -12.265961753792824 3.7169301705830549e-005 3.2009490800177787e-006 ;
	setAttr -k on ".w0";
createNode joint -n "r_shoulderFollow_rig" -p "r_clav_rig";
	rename -uid "CF6909A6-4C75-ED8F-F188-68AABC31E298";
	setAttr ".t" -type "double3" -12.265961753792824 3.7169301734252258e-005 3.2009490800177787e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -179.99999999999994 -6.9589737073660416 -0.0006407342709240098 ;
createNode orientConstraint -n "r_clav_rig_orientConstraint1" -p "r_clav_rig";
	rename -uid "BAB4B89F-4C4C-2CBD-A55C-DBBB63572B29";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_clav_rig_ctrl_01W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 8.6651774057881613e-014 -6.9589737073658693 -0.00064073427117269271 ;
	setAttr ".o" -type "double3" 7.8206606345350939e-005 6.9589737069285809 0.00064548948831952605 ;
	setAttr ".rsrr" -type "double3" 1.8199279869824502e-020 1.5902773406854751e-015 
		-9.744197763635204e-020 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "r_clav_rig_pointConstraint1" -p "r_clav_rig";
	rename -uid "0A07E0CB-4C66-5B7A-5757-3E8BBBA4B9B7";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_clav_rig_ctrl_01W0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -2.8421709430404007e-014 3.5527136788005009e-015 2.6645352591003757e-015 ;
	setAttr ".rst" -type "double3" 11.62215423126959 1.5780810334233273 -6.9999999905104167 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "spineParent_rig_pointConstraint1" -p "spineParent_rig";
	rename -uid "B7CEBB17-4361-4ABA-9BC2-FA92EFE8A4D8";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "topSpine_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0 0 -1.7763568394002505e-015 ;
	setAttr ".rst" -type "double3" -9.4895616142039899e-009 80.082359181456326 -9.4322473036750516 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "spineParent_rig_orientConstraint1" -p "spineParent_rig";
	rename -uid "9C91B41C-49FA-63DA-5CC7-74873962D175";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "topSpine_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -92.973611743757928 -89.999999999999829 0 ;
	setAttr ".o" -type "double3" 90 -2.973611743757917 89.999999999999829 ;
	setAttr ".rsrr" -type "double3" -2.2263882770244621e-014 -1.8538406451810674e-030 
		-9.5416640443905519e-015 ;
	setAttr -k on ".w0";
createNode transform -n "rig_controls" -p "rig_skeleton";
	rename -uid "EAB68C30-403F-4BCD-81B1-90AB5C624236";
	addAttr -ci true -sn "rigControls" -ln "rigControls" -at "double";
createNode transform -n "pelvis_ctrl" -p "rig_controls";
	rename -uid "607E5A13-40B2-D769-133B-46924E5F16AE";
	addAttr -ci true -sn "rigName" -ln "rigName" -dt "string";
	addAttr -ci true -sn "rootCtrl" -ln "rootCtrl" -at "double";
	addAttr -ci true -sn "njc_autoRigSystem" -ln "njc_autoRigSystem" -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0 48.502845764160156 -9.3133430480957031 ;
	setAttr ".sp" -type "double3" 0 48.502845764160156 -9.3133430480957031 ;
	setAttr ".rigName" -type "string" "";
createNode nurbsCurve -n "pelvis_ctrlShape" -p "pelvis_ctrl";
	rename -uid "34E31070-4050-3C02-281D-9E94D5370E8A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 17 0 no 3
		18 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
		18
		-24.607340357174937 49.17372111748918 15.293997309079236
		-24.607340357174937 48.50032768406686 15.293997309079236
		-24.607340357174937 49.17372111748918 15.293997309079236
		-24.607340357174937 49.17372111748918 -33.92068340527063
		-24.607340357174937 48.50032768406686 -33.92068340527063
		-24.607340357174937 49.17372111748918 -33.92068340527063
		24.607340357174937 49.17372111748918 -33.92068340527063
		24.607340357174937 48.50032768406686 -33.92068340527063
		24.607340357174937 49.17372111748918 -33.92068340527063
		24.607340357174937 49.17372111748918 15.293997309079236
		24.607340357174937 48.50032768406686 15.293997309079236
		24.607340357174937 49.17372111748918 15.293997309079236
		-24.607340357174937 49.17372111748918 15.293997309079236
		-24.607340357174937 48.50032768406686 15.293997309079236
		-24.607340357174937 48.50032768406686 -33.92068340527063
		24.607340357174937 48.50032768406686 -33.92068340527063
		24.607340357174937 48.50032768406686 15.293997309079236
		-24.607340357174937 48.50032768406686 15.293997309079236
		;
createNode transform -n "hips_ctrl" -p "pelvis_ctrl";
	rename -uid "9E61D8CB-4EDE-EF3F-08D2-C3814A0753F1";
	addAttr -ci true -sn "rigName" -ln "rigName" -dt "string";
	addAttr -ci true -sn "hipsCtrl" -ln "hipsCtrl" -at "double";
	addAttr -ci true -sn "njc_autoRigSystem" -ln "njc_autoRigSystem" -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 6.7546215009549136e-030 48.50284576416017 -9.3133430480956996 ;
	setAttr ".sp" -type "double3" 6.7546215009549136e-030 48.50284576416017 -9.3133430480956996 ;
	setAttr ".rigName" -type "string" "";
createNode nurbsCurve -n "hips_ctrlShape" -p "hips_ctrl";
	rename -uid "621251B2-4D40-27F4-AB57-359869768A98";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		16.030881882484923 35.107163614673965 -25.344224930580609
		-2.5865003554835181e-015 61.898527913646348 -31.984433623107002
		-16.030881882484909 35.107163614673965 -25.344224930580609
		-22.671090575011295 61.898527913646348 -9.3133430480957067
		-16.030881882484913 35.107163614673965 6.7175388343892042
		-6.8312420419376023e-015 61.898527913646348 13.357747526915599
		16.030881882484902 35.107163614673965 6.7175388343892095
		22.671090575011295 61.898527913646348 -9.3133430480956925
		16.030881882484923 35.107163614673965 -25.344224930580609
		-2.5865003554835181e-015 61.898527913646348 -31.984433623107002
		-16.030881882484909 35.107163614673965 -25.344224930580609
		;
createNode transform -n "l_hip_grp" -p "hips_ctrl";
	rename -uid "D220F922-4FAC-37F6-D6AB-C497F88D6E9B";
	addAttr -ci true -sn "l_hip_grp" -ln "l_hip_grp" -at "double";
	setAttr ".t" -type "double3" 10.90128558246138 47.456432145150998 -6.5619424465784224 ;
createNode transform -n "l_hip_ctrl" -p "l_hip_grp";
	rename -uid "24C866A4-4E84-A5E0-5B67-2C928674CBA3";
	addAttr -ci true -sn "l_hipCtrl" -ln "l_hipCtrl" -at "double";
	addAttr -ci true -sn "setControl" -ln "setControl" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "rotateWith" -ln "rotateWith" -min 0 -max 2 -en "worldPlacement:pelvis_ctrl:hips_ctrl" 
		-at "enum";
	setAttr -k off ".v";
	setAttr ".ro" 4;
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" 7.1054273576010019e-015 1.0658141036401503e-014 -1.8318679906315083e-015 ;
	setAttr ".sp" -type "double3" 7.1054273576010019e-015 1.0658141036401503e-014 -1.8318679906315083e-015 ;
	setAttr -cb on ".setControl";
	setAttr -k on ".rotateWith";
createNode nurbsCurve -n "l_hip_ctrlShape" -p "l_hip_ctrl";
	rename -uid "50E45BF3-4DAF-F63B-D63F-6E957BC20CAA";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		2.4572474121348478 9.8941161691055086 -10.226721779890672
		1.8746448245020275 -8.9546707613408216e-006 -14.283373816012586
		2.4572437017307038 -9.8941294101145125 -10.226709861030576
		3.8637655229636465 -13.992403622179882 -0.4330686417830325
		5.270288881096306 -9.894118139762881 9.3605676405110074
		5.852891468729112 6.9840133676279947e-006 13.417219676632921
		5.2702925915004499 9.8941274394571259 9.3605557216509077
		3.863770770267493 13.992401651522499 -0.43308549759663073
		2.4572474121348478 9.8941161691055086 -10.226721779890672
		1.8746448245020275 -8.9546707613408216e-006 -14.283373816012586
		2.4572437017307038 -9.8941294101145125 -10.226709861030576
		;
createNode transform -n "l_knee_grp" -p "l_hip_ctrl";
	rename -uid "B8C433D9-4A6B-CD5D-1FC8-50A0CC48FBAB";
	addAttr -ci true -sn "l_knee_grp" -ln "l_knee_grp" -at "double";
	setAttr ".t" -type "double3" 22.362855000000014 0 -9.3813845580825728e-015 ;
	setAttr ".r" -type "double3" 9.229738079016701e-015 13.792099994027067 -8.450309372959119e-006 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode transform -n "l_knee_ctrl" -p "l_knee_grp";
	rename -uid "5958D43C-4824-8D41-E978-83BCD5AC10D4";
	addAttr -ci true -sn "l_kneeCtrl" -ln "l_kneeCtrl" -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" 1.0658141036401503e-014 -5.3290705182007514e-015 -6.2172489379008766e-015 ;
	setAttr ".sp" -type "double3" 1.0658141036401503e-014 -5.3290705182007514e-015 -6.2172489379008766e-015 ;
createNode nurbsCurve -n "l_knee_ctrlShape" -p "l_knee_ctrl";
	rename -uid "F07861C3-4C78-E23D-BDA9-85B92C77A572";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.61385019838738941 6.2688907927561246 -6.2387701725327709
		0.86811354175640787 -3.7397368597424929e-006 -8.8229483177611794
		0.61384791075892409 -6.2688952055011828 -6.2387629581287989
		-1.3364120903247567e-006 -8.8655520049370651 5.1703777153022656e-006
		-0.613849636016365 -6.2688878017632765 6.2387703105802581
		-0.8681129793853799 6.7307297015872791e-006 8.8229484558086675
		-0.61384734838789967 6.2688981964940247 6.2387630961762843
		1.8987831147398992e-006 8.8655549959299087 -5.0323302263066694e-006
		0.61385019838738941 6.2688907927561246 -6.2387701725327709
		0.86811354175640787 -3.7397368597424929e-006 -8.8229483177611794
		0.61384791075892409 -6.2688952055011828 -6.2387629581287989
		;
createNode transform -n "l_ankle_grp" -p "l_knee_ctrl";
	rename -uid "78E3DF2F-4AAF-939D-D457-B7A4F6BE88A4";
	addAttr -ci true -sn "l_ankle_grp" -ln "l_ankle_grp" -at "double";
	setAttr ".t" -type "double3" 19.015319906267688 2.3625545964023331e-013 6.9025492877017314e-008 ;
	setAttr ".r" -type "double3" -5.6193960780551917 3.2968731391435073e-005 89.999989545885043 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode transform -n "l_ankle_ctrl" -p "l_ankle_grp";
	rename -uid "DB9641E3-4B57-C6EB-7AF0-43A1AEA4FE34";
	addAttr -ci true -sn "l_ankleCtrl" -ln "l_ankleCtrl" -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" -1.723066134218243e-013 -8.6522884856776727e-008 -7.7872039483395383e-008 ;
	setAttr ".sp" -type "double3" -1.723066134218243e-013 -8.6522884856776727e-008 -7.7872039483395383e-008 ;
createNode nurbsCurve -n "l_ankle_ctrlShape" -p "l_ankle_ctrl";
	rename -uid "D3D7C094-4515-3A12-9A40-B29DF0A453DD";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		6.2688929991298004 -3.5527136788005009e-015 -6.2688929991297924
		-1.7763568394002505e-015 -2.6645352591003757e-015 -8.8655535004351051
		-6.2688929991297941 -3.5527136788005009e-015 -6.2688929991297959
		-8.8655535004351034 -3.5527136788005009e-015 -3.5527136788005009e-015
		-6.2688929991297959 -3.5527136788005009e-015 6.2688929991297915
		-3.5527136788005009e-015 -4.4408920985006262e-015 8.8655535004351051
		6.2688929991297897 -3.5527136788005009e-015 6.2688929991297941
		8.8655535004351016 -3.5527136788005009e-015 3.5527136788005009e-015
		6.2688929991298004 -3.5527136788005009e-015 -6.2688929991297924
		-1.7763568394002505e-015 -2.6645352591003757e-015 -8.8655535004351051
		-6.2688929991297941 -3.5527136788005009e-015 -6.2688929991297959
		;
createNode transform -n "l_ball_grp" -p "l_ankle_ctrl";
	rename -uid "1A138DAD-43E5-ECDF-DFA1-F2B796345C7A";
	addAttr -ci true -sn "l_ball_grp" -ln "l_ball_grp" -at "double";
	setAttr ".t" -type "double3" 7.8490308652590102e-006 -6.1976541416656268 8.4672682239828543 ;
	setAttr ".r" -type "double3" -2.4566750950354228e-031 7.8767089919262786e-016 -1.0965170767428517e-014 ;
	setAttr ".s" -type "double3" 0.99999999999999978 1 1 ;
createNode transform -n "l_ball_ctrl" -p "l_ball_grp";
	rename -uid "EAB78DAB-4076-1B4B-4DDA-03B1A2BBAD3D";
	addAttr -ci true -sn "l_ballCtrl" -ln "l_ballCtrl" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" 6.0815033009475883e-009 -8.6519021280651032e-008 -7.7869215076020737e-008 ;
	setAttr ".sp" -type "double3" 6.0815033009475883e-009 -8.6519021280651032e-008 -7.7869215076020737e-008 ;
createNode nurbsCurve -n "l_ball_ctrlShape" -p "l_ball_ctrl";
	rename -uid "CDFA7481-4521-1F0F-2A34-708B253C10F5";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		6.2688929991298021 6.2688929991297915 -1.3322676295501878e-015
		0 8.8655535004351051 -1.7763568394002505e-015
		-6.2688929991297924 6.268892999129795 -1.3322676295501878e-015
		-8.8655535004351016 3.5527136788005009e-015 0
		-6.2688929991297941 -6.2688929991297924 8.8817841970012523e-016
		-1.7763568394002505e-015 -8.8655535004351051 1.3322676295501878e-015
		6.2688929991297915 -6.2688929991297941 8.8817841970012523e-016
		8.8655535004351069 -3.9968028886505635e-015 0
		6.2688929991298021 6.2688929991297915 -1.3322676295501878e-015
		0 8.8655535004351051 -1.7763568394002505e-015
		-6.2688929991297924 6.268892999129795 -1.3322676295501878e-015
		;
createNode orientConstraint -n "l_hip_grp_orientConstraint1" -p "l_hip_grp";
	rename -uid "03E10720-478D-9BF5-B91E-898BD4014B23";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "worldPlacementW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "pelvis_ctrlW1" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w2" -ln "hips_ctrlW2" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 3 ".tg";
	setAttr ".o" -type "double3" 3.2967500575484931e-005 -8.1727039159805148 -89.999989146500965 ;
	setAttr ".rsrr" -type "double3" 3.2967500576288223e-005 -8.1727039159805148 -89.999989146500965 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
	setAttr -k on ".w2";
createNode transform -n "r_hip_grp" -p "hips_ctrl";
	rename -uid "45054928-4D8C-0467-316A-0A9AF5532C02";
	addAttr -ci true -sn "r_hip_grp" -ln "r_hip_grp" -at "double";
	setAttr ".t" -type "double3" -10.901299999999996 47.456399999999988 -6.56194 ;
createNode transform -n "r_hip_ctrl" -p "r_hip_grp";
	rename -uid "3AC0B7BC-4A5D-574F-E8C7-BDBA099060AB";
	addAttr -ci true -sn "r_hipCtrl" -ln "r_hipCtrl" -at "double";
	addAttr -ci true -sn "setControl" -ln "setControl" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "rotateWith" -ln "rotateWith" -min 0 -max 2 -en "worldPlacement:pelvis_ctrl:hips_ctrl" 
		-at "enum";
	setAttr -k off ".v";
	setAttr ".ro" 4;
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" -3.5527136788005009e-014 5.3290705182007514e-015 6.8389738316909643e-013 ;
	setAttr ".sp" -type "double3" -3.5527136788005009e-014 5.3290705182007514e-015 6.8389738316909643e-013 ;
	setAttr -cb on ".setControl";
	setAttr -k on ".rotateWith";
createNode nurbsCurve -n "r_hip_ctrlShape" -p "r_hip_ctrl";
	rename -uid "421E4818-4CF8-75FB-134C-0F9293133D92";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-2.457244579132805 9.8941234146851649 10.226715450661736
		-1.8746469740690515 4.7687192150647206e-007 14.283373533892444
		-2.4572496128585328 -9.8941221645381532 10.226715450661743
		-3.863771771172722 -13.992401653989763 0.43307648821574674
		-5.2702918444494316 -9.8941214489576375 -9.3605624742302549
		-5.8528894495131709 1.4888555917025315e-006 -13.417220557460963
		-5.2702868107237038 9.8941241302656664 -9.3605624742302584
		-3.8637646524095146 13.992403619717283 0.43307648821573608
		-2.457244579132805 9.8941234146851649 10.226715450661736
		-1.8746469740690515 4.7687192150647206e-007 14.283373533892444
		-2.4572496128585328 -9.8941221645381532 10.226715450661743
		;
createNode transform -n "r_knee_grp" -p "r_hip_ctrl";
	rename -uid "8E59B815-4376-89AB-64C8-B595A1689137";
	addAttr -ci true -sn "r_knee_grp" -ln "r_knee_grp" -at "double";
	setAttr ".t" -type "double3" -22.362815000000012 5.6886437214132002e-006 -7.3233510367032295e-007 ;
	setAttr ".r" -type "double3" 8.7485801082366269e-007 13.792081074876766 -8.4503093197167015e-006 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000004 ;
createNode transform -n "r_knee_ctrl" -p "r_knee_grp";
	rename -uid "7D67765D-4710-9D0F-4ECA-669053C6FFC2";
	addAttr -ci true -sn "r_kneeCtrl" -ln "r_kneeCtrl" -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" -3.5527136788005009e-015 5.9713201494560053e-008 1.1134913258104007e-007 ;
	setAttr ".sp" -type "double3" -3.5527136788005009e-015 5.9713201494560053e-008 1.1134913258104007e-007 ;
createNode nurbsCurve -n "r_knee_ctrlShape" -p "r_knee_ctrl";
	rename -uid "0E81DF55-46A2-1AEB-9962-DAB79E2A406B";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.61384652672870033 6.2688931225674569 6.2387635071569427
		-0.86811120030112576 1.4983329243989374e-007 8.8229453052808893
		-0.61384782829898654 -6.268892875692071 6.2387635071569214
		-4.4562641576817441e-007 -8.8655534407219889 -3.2375116223448686e-006
		0.61384747617421453 -6.2688930031413186 -6.2387699821801608
		0.86811214974663997 -3.0407168338797419e-008 -8.8229517803041073
		0.61384877774450075 6.268892995118196 -6.2387699821801395
		1.3950719335298345e-006 8.8655535601481201 -3.2375115965876944e-006
		-0.61384652672870033 6.2688931225674569 6.2387635071569427
		-0.86811120030112576 1.4983329243989374e-007 8.8229453052808893
		-0.61384782829898654 -6.268892875692071 6.2387635071569214
		;
createNode transform -n "r_ankle_grp" -p "r_knee_ctrl";
	rename -uid "1959AB60-4CA2-F02D-9746-C98DCB37A56C";
	addAttr -ci true -sn "r_ankle_grp" -ln "r_ankle_grp" -at "double";
	setAttr ".t" -type "double3" -19.015330600054227 1.9740160350778524e-006 -7.4585133109650315e-006 ;
	setAttr ".r" -type "double3" 174.3806142184128 -1.4600943047740379e-006 89.999994052021336 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 0.99999999999999956 ;
createNode transform -n "r_ankle_ctrl" -p "r_ankle_grp";
	rename -uid "E990DB90-4F21-6286-F9FB-8FBB68E4F4F6";
	addAttr -ci true -sn "r_ankleCtrl" -ln "r_ankleCtrl" -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" 1.0835776720341528e-013 -4.0471455875490392e-007 2.8838599952507593e-008 ;
	setAttr ".sp" -type "double3" 1.0835776720341528e-013 -4.0471455875490392e-007 2.8838599952507593e-008 ;
createNode nurbsCurve -n "r_ankle_ctrlShape" -p "r_ankle_ctrl";
	rename -uid "5F4EE875-4174-82D0-7B07-8B88F47479BF";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		6.2688929991297986 0 -6.2688929991297897
		-3.5527136788005009e-015 8.8817841970012523e-016 -8.8655535004351034
		-6.2688929991297968 0 -6.2688929991297933
		-8.8655535004351051 0 -2.6645352591003757e-015
		-6.2688929991297968 0 6.2688929991297924
		-5.3290705182007514e-015 -8.8817841970012523e-016 8.8655535004351051
		6.2688929991297888 0 6.268892999129795
		8.8655535004351016 0 4.4408920985006262e-015
		6.2688929991297986 0 -6.2688929991297897
		-3.5527136788005009e-015 8.8817841970012523e-016 -8.8655535004351034
		-6.2688929991297968 0 -6.2688929991297933
		;
createNode transform -n "r_ball_grp" -p "r_ankle_ctrl";
	rename -uid "6D5617C1-4161-0653-85BE-44A0209FC421";
	addAttr -ci true -sn "r_ball_grp" -ln "r_ball_grp" -at "double";
	setAttr ".t" -type "double3" 3.5527136788005009e-015 -6.1976499999999879 8.4672599999999978 ;
	setAttr ".r" -type "double3" -3.975693351829396e-015 -7.6746495914132268e-016 8.0407116839883065e-015 ;
createNode transform -n "r_ball_ctrl" -p "r_ball_grp";
	rename -uid "1375A5E6-4400-34D6-CEEA-639FA66A67D2";
	addAttr -ci true -sn "r_ballCtrl" -ln "r_ballCtrl" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" -6.2705396430828841e-013 -1.5110067419499273e-007 
		2.1447243714334263e-007 ;
	setAttr ".sp" -type "double3" -6.2705396430828841e-013 -1.5110067419499273e-007 
		2.1447243714334263e-007 ;
createNode nurbsCurve -n "r_ball_ctrlShape" -p "r_ball_ctrl";
	rename -uid "5AB12F6D-4E4A-1F44-7D89-5F9FBEC5FFB7";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		6.2688929991297933 6.2688929991297888 -9.3258734068513149e-015
		-8.8817841970012523e-015 8.8655535004351016 -9.7699626167013776e-015
		-6.2688929991298004 6.2688929991297924 -9.3258734068513149e-015
		-8.8655535004351087 8.8817841970012523e-016 -7.9936057773011271e-015
		-6.2688929991298039 -6.2688929991297933 -7.1054273576010019e-015
		-1.0658141036401503e-014 -8.8655535004351087 -6.6613381477509392e-015
		6.2688929991297835 -6.2688929991297968 -7.1054273576010019e-015
		8.8655535004350963 -6.6613381477509392e-015 -7.9936057773011271e-015
		6.2688929991297933 6.2688929991297888 -9.3258734068513149e-015
		-8.8817841970012523e-015 8.8655535004351016 -9.7699626167013776e-015
		-6.2688929991298004 6.2688929991297924 -9.3258734068513149e-015
		;
createNode orientConstraint -n "r_hip_grp_orientConstraint1" -p "r_hip_grp";
	rename -uid "C1966EAB-425B-2548-766C-9C859B5FA05B";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "worldPlacementW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "pelvis_ctrlW1" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w2" -ln "hips_ctrlW2" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 3 ".tg";
	setAttr ".o" -type "double3" -540.00000209318443 728.17269529328917 -630.00001472441659 ;
	setAttr ".rsrr" -type "double3" -540.00000209318443 728.17269529328917 -630.00001472441659 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
	setAttr -k on ".w2";
createNode transform -n "spine_01_ctrl" -p "pelvis_ctrl";
	rename -uid "89BE69CD-4E82-7DCD-E88D-77AA6B3910F1";
	addAttr -ci true -sn "rigName" -ln "rigName" -dt "string";
	addAttr -ci true -sn "spineCtrl01" -ln "spineCtrl01" -at "double";
	addAttr -ci true -sn "njc_autoRigSystem" -ln "njc_autoRigSystem" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 2.7325893195284953e-015 56.649088358017046 -10.28786263002049 ;
	setAttr ".sp" -type "double3" 2.7325893195284953e-015 56.649088358017046 -10.28786263002049 ;
	setAttr ".rigName" -type "string" "";
createNode nurbsCurve -n "spine_01_ctrlShape" -p "spine_01_ctrl";
	rename -uid "4C007962-4FEC-992B-0A91-A0B7B21CA1BB";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		17.72544305657328 56.649088358017053 -28.013305686593732
		1.1250531399032587e-016 56.649088358017053 -35.355424599698402
		-17.725443056573258 56.649088358017053 -28.013305686593746
		-25.067561969677914 56.649088358017053 -10.287862630020499
		-17.725443056573262 56.649088358017053 7.437580426552767
		-4.5809312420678023e-015 56.649088358017053 14.779699339657434
		17.725443056573251 56.649088358017053 7.4375804265527741
		25.067561969677918 56.649088358017053 -10.28786263002047
		17.72544305657328 56.649088358017053 -28.013305686593732
		1.1250531399032587e-016 56.649088358017053 -35.355424599698402
		-17.725443056573258 56.649088358017053 -28.013305686593746
		;
createNode transform -n "spine_02_ctrl" -p "spine_01_ctrl";
	rename -uid "10A8256B-4407-D2D9-354D-86A435B64F80";
	addAttr -ci true -sn "rigName" -ln "rigName" -dt "string";
	addAttr -ci true -sn "spineCtrl02" -ln "spineCtrl02" -at "double";
	addAttr -ci true -sn "njc_autoRigSystem" -ln "njc_autoRigSystem" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" -3.4619749315630123e-015 64.464692133531344 -10.287862630020499 ;
	setAttr ".sp" -type "double3" -3.4619749315630123e-015 64.464692133531344 -10.287862630020499 ;
	setAttr ".rigName" -type "string" "";
createNode nurbsCurve -n "spine_02_ctrlShape" -p "spine_02_ctrl";
	rename -uid "D0892F89-43B1-AA5C-85D8-9C98C2BAD672";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		19.562050623125995 64.464602093303199 -29.849913253146454
		-1.5192122205160879e-016 64.464602093303199 -37.952779929074296
		-19.56205062312597 64.464602093303199 -29.849913253146468
		-27.664917299053808 64.464602093303199 -10.287862630020499
		-19.562050623125977 64.464602093303199 9.2741879931054783
		-5.3316644427431419e-015 64.464602093303199 17.377054669033324
		19.562050623125963 64.464602093303199 9.2741879931054854
		27.664917299053812 64.464602093303199 -10.28786263002047
		19.562050623125995 64.464602093303199 -29.849913253146454
		-1.5192122205160879e-016 64.464602093303199 -37.952779929074296
		-19.56205062312597 64.464602093303199 -29.849913253146468
		;
createNode transform -n "spine_03_ctrl" -p "spine_02_ctrl";
	rename -uid "37F7FFDD-4B4E-1F0F-E58A-E8BE8E2220EF";
	addAttr -ci true -sn "rigName" -ln "rigName" -dt "string";
	addAttr -ci true -sn "spineCtrl03" -ln "spineCtrl03" -at "double";
	addAttr -ci true -sn "njc_autoRigSystem" -ln "njc_autoRigSystem" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" -3.4619749315630076e-015 72.28029590904589 -10.287862630020499 ;
	setAttr ".sp" -type "double3" -3.4619749315630092e-015 72.28029590904589 -10.287862630020499 ;
	setAttr ".rigName" -type "string" "";
createNode nurbsCurve -n "spine_03_ctrlShape" -p "spine_03_ctrl";
	rename -uid "8B129066-444D-EE33-E754-B78E010FE270";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		20.206535357400448 72.280115828589373 -30.494397987420896
		-2.4471124912090579e-016 72.280115828589373 -38.864218981027648
		-20.206535357400423 72.280115828589373 -30.494397987420911
		-28.57635635100716 72.280115828589373 -10.287862630020499
		-20.206535357400426 72.280115828589373 9.9186727273799349
		-5.5951045462032453e-015 72.280115828589373 18.288493720986679
		20.206535357400416 72.280115828589373 9.918672727379942
		28.576356351007163 72.280115828589373 -10.28786263002047
		20.206535357400448 72.280115828589373 -30.494397987420896
		-2.4471124912090579e-016 72.280115828589373 -38.864218981027648
		-20.206535357400423 72.280115828589373 -30.494397987420911
		;
createNode transform -n "topSpine_ctrl" -p "spine_03_ctrl";
	rename -uid "34A0673D-4F89-9A41-D296-4281AD6D5030";
	addAttr -ci true -sn "rigName" -ln "rigName" -dt "string";
	addAttr -ci true -sn "spineTopCtrl" -ln "spineTopCtrl" -at "double";
	addAttr -ci true -k true -sn "spineScale" -ln "spineScale" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "njc_autoRigSystem" -ln "njc_autoRigSystem" -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -9.4895616142039899e-009 80.082359181456326 -9.4322473036750498 ;
	setAttr ".sp" -type "double3" -9.4895616142039899e-009 80.082359181456326 -9.4322473036750498 ;
	setAttr ".rigName" -type "string" "";
	setAttr -k on ".spineScale";
createNode nurbsCurve -n "topSpine_ctrlShape" -p "topSpine_ctrl";
	rename -uid "69D6AF3D-48D5-4484-7760-E59C9D7E6300";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		13.164675288683018 80.082359181456326 -28.508136842261759
		-9.4895631630487376e-009 80.082359181456326 -37.596448596830356
		-13.164675307662129 80.082359181456326 -28.508136842261766
		-18.617662360403276 80.082359181456326 -6.5670113453074954
		-13.164675307662133 80.082359181456326 15.374114151646786
		-9.4895666488610808e-009 80.082359181456326 24.46242590621538
		13.164675288682997 80.082359181456326 15.374114151646793
		18.617662341424154 80.082359181456326 -6.5670113453074741
		13.164675288683018 80.082359181456326 -28.508136842261759
		-9.4895631630487376e-009 80.082359181456326 -37.596448596830356
		-13.164675307662129 80.082359181456326 -28.508136842261766
		;
createNode transform -n "spineCurve_ik_CV_5" -p "topSpine_ctrl";
	rename -uid "798137FF-4F02-5A0C-A656-4083003BEEF3";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -9.4895616142039899e-009 80.082359181456326 -9.4322473036750498 ;
	setAttr ".sp" -type "double3" -9.4895616142039899e-009 80.082359181456326 -9.4322473036750498 ;
createNode clusterHandle -n "clusterHandleShape" -p "spineCurve_ik_CV_5";
	rename -uid "84FD01C9-49F8-6DB8-B37C-48B2082AB3E0";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".io" yes;
createNode transform -n "spineCurve_ik_CV_1_L_point_blend_X" -p "spineCurve_ik_CV_5";
	rename -uid "47D8CFDD-481D-4E18-9550-2CAF341D8260";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
createNode transform -n "spineCurve_ik_CV_1_L_point_blend_Y" -p "spineCurve_ik_CV_5";
	rename -uid "41071593-45F3-DEFC-B437-FD837FDE9F48";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
createNode transform -n "spineCurve_ik_CV_1_L_point_blend_Z" -p "spineCurve_ik_CV_5";
	rename -uid "6670DF8A-4652-8E41-651C-879FC26BFC82";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
	setAttr ".sp" -type "double3" -2.9196095260608423e-008 59.275004000646554 -10.329757386832791 ;
createNode transform -n "spineCurve_ik_CV_2_L_point_blend_X" -p "spineCurve_ik_CV_5";
	rename -uid "731A4188-4915-4740-3242-F7965B502FDD";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
createNode transform -n "spineCurve_ik_CV_2_L_point_blend_Y" -p "spineCurve_ik_CV_5";
	rename -uid "6B307F14-463C-29F1-A33D-1F98530EF7CA";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
createNode transform -n "spineCurve_ik_CV_2_L_point_blend_Z" -p "spineCurve_ik_CV_5";
	rename -uid "46994FA2-4105-E355-C0CC-6785F651054C";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
	setAttr ".sp" -type "double3" -7.952638228755677e-008 64.496062648911931 -10.479176482336932 ;
createNode transform -n "spineCurve_ik_CV_3_L_point_blend_X" -p "spineCurve_ik_CV_5";
	rename -uid "93E6929F-4FA9-6436-9E1D-338ADE27C13B";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
createNode transform -n "spineCurve_ik_CV_3_L_point_blend_Y" -p "spineCurve_ik_CV_5";
	rename -uid "999D7655-463C-1F0D-07C8-468D77F0AA3A";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
createNode transform -n "spineCurve_ik_CV_3_L_point_blend_Z" -p "spineCurve_ik_CV_5";
	rename -uid "3D6C9433-4FA4-7439-C15F-B8922FCBC249";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
	setAttr ".sp" -type "double3" 2.8470594588902456e-008 72.363191810609877 -11.277284826066461 ;
createNode transform -n "spineCurve_ik_CV_4_L_point_blend_X" -p "spineCurve_ik_CV_5";
	rename -uid "5E83EF7B-4F58-C71A-E72C-DF8D50ACFD77";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
createNode transform -n "spineCurve_ik_CV_4_L_point_blend_Y" -p "spineCurve_ik_CV_5";
	rename -uid "B4FE5FF6-4468-C146-D74F-FEABB79DF9F8";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
createNode transform -n "spineCurve_ik_CV_4_L_point_blend_Z" -p "spineCurve_ik_CV_5";
	rename -uid "F653016B-4726-34DE-540B-51A635BA716B";
	setAttr -k off -cb on ".v";
	setAttr -k off -cb on ".tx";
	setAttr -k off -cb on ".ty";
	setAttr -k off -cb on ".tz";
	setAttr -k off -cb on ".rx";
	setAttr -k off -cb on ".ry";
	setAttr -k off -cb on ".rz";
	setAttr -k off -cb on ".sx";
	setAttr -k off -cb on ".sy";
	setAttr -k off -cb on ".sz";
	setAttr ".rp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
	setAttr ".sp" -type "double3" 1.2171741546379702e-009 77.552392796654573 -10.01770418735598 ;
createNode transform -n "l_clav_grp" -p "topSpine_ctrl";
	rename -uid "DEE32D89-4395-2E16-B849-EC998621FC8E";
	addAttr -ci true -sn "l_clavGrp" -ln "l_clavGrp" -at "double";
	setAttr ".t" -type "double3" 7.0000007861436018 91.607010071002037 -7.2533821526524704 ;
createNode transform -n "l_clav_rig_ctrl_01" -p "l_clav_grp";
	rename -uid "CB4A7316-441B-0637-392E-3697E64CFE0D";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "l_clavCtrl" -ln "l_clavCtrl" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" 0 -2.8421709430404007e-014 -3.5527136788005009e-015 ;
	setAttr ".sp" -type "double3" 0 2.8421709430404007e-014 -3.5527136788005009e-015 ;
createNode nurbsCurve -n "l_clav_rig_ctrl_0Shape1" -p "l_clav_rig_ctrl_01";
	rename -uid "23D04113-411D-D345-584C-58ABD91D7A72";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 31 0 no 3
		32 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31
		32
		68.202443434521172 64.211659294298158 13.104209491496063
		71.05862115893666 67.584216495758653 9.8687360350916595
		70.501954516273457 66.620041587787171 8.7554027497652473
		68.202443434521172 64.211659294298158 13.104209491496063
		70.501954516273457 66.620041587787171 8.7554027497652473
		69.13840814183115 67.407285454123951 8.7554027497652473
		68.202443434521172 64.211659294298158 13.104209491496063
		69.13840814183115 67.407285454123951 8.7554027497652473
		69.695074784494352 68.371460362095434 9.8687360350916595
		68.202443434521172 64.211659294298158 13.104209491496063
		71.05862115893666 67.584216495758653 9.8687360350916595
		69.695074784494352 68.371460362095434 9.8687360350916595
		68.202443434521172 64.211659294298158 13.104209491496063
		70.454602456808743 68.112513147624895 8.5998914469208945
		71.303622277823081 69.583058614254753 6.4706180080477758
		71.894451861458677 70.606405471726362 4.016881188784982
		72.197462953660676 71.131236078677148 1.3617216942959587
		72.197462953660676 71.131236078677148 -1.3617216942960297
		71.894451861458677 70.606405471726362 -4.0168811887850566
		71.303622277823081 69.583058614254753 -6.4706180080478468
		70.454602456808743 68.112513147624895 -8.5998914469209708
		68.202443434521157 64.211659294298158 -13.104209491496135
		70.501954516273457 66.620041587787171 -8.7554027497653237
		71.05862115893666 67.584216495758653 -9.8687360350917288
		68.202443434521157 64.211659294298158 -13.104209491496135
		71.05862115893666 67.584216495758653 -9.8687360350917288
		69.695074784494352 68.371460362095434 -9.8687360350917288
		68.202443434521157 64.211659294298158 -13.104209491496135
		69.13840814183115 67.407285454123951 -8.7554027497653237
		69.695074784494352 68.371460362095434 -9.8687360350917288
		69.13840814183115 67.407285454123951 -8.7554027497653237
		70.501954516273457 66.620041587787171 -8.7554027497653237
		;
createNode transform -n "l_shoulder_grp" -p "topSpine_ctrl";
	rename -uid "DE12482D-4605-E4D5-EF06-619971D7A17B";
	addAttr -ci true -sn "l_shoulderGrp" -ln "l_shoulderGrp" -at "double";
createNode transform -n "l_shoulder_fk_ctrl_01" -p "l_shoulder_grp";
	rename -uid "5F15B9D9-402B-5523-327A-7297EA47E12C";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "l_shoulderCtrl" -ln "l_shoulderCtrl" -at "double";
	addAttr -ci true -sn "setControl" -ln "setControl" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "rotateWith" -ln "rotateWith" -min 0 -max 2 -en "topSpine_ctrl:pelvis_ctrl:worldPlacement" 
		-at "enum";
	setAttr -k off ".v";
	setAttr ".ro" 4;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 1.0658141036401503e-014 -2.8421709430404007e-014 -1.4210854715202004e-014 ;
	setAttr ".sp" -type "double3" -3.5527136788005009e-015 -2.8421709430404007e-014 
		-1.2434497875801753e-014 ;
	setAttr -cb on ".setControl";
	setAttr -k on ".rotateWith";
createNode nurbsCurve -n "l_shoulder_fk_ctrl_01Shape" -p "l_shoulder_fk_ctrl_01";
	rename -uid "D4C07B0F-4BDE-FB6F-73DF-04BB77367E15";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.0818391628126847e-007 10.712845595431986 -10.712845620582812
		7.1868024065224745e-007 15.150251550453333 -1.7784095973638614e-008
		5.0818344732306286e-007 10.712845620582584 10.712845595432292
		-3.0553337637684308e-013 1.7784032024792396e-008 15.150251550453323
		-5.0818389496498639e-007 -10.712845595432441 10.712845620582797
		-7.1868027617938424e-007 -15.150251550453333 1.7784103079065972e-008
		-5.0818344021763551e-007 -10.712845620582982 -10.712845595432285
		3.907985046680551e-013 -1.7784202555048978e-008 -15.150251550453312
		5.0818391628126847e-007 10.712845595431986 -10.712845620582812
		7.1868024065224745e-007 15.150251550453333 -1.7784095973638614e-008
		5.0818344732306286e-007 10.712845620582584 10.712845595432292
		;
createNode transform -n "l_elbow_grp" -p "l_shoulder_fk_ctrl_01";
	rename -uid "3CA8E933-4539-0722-BBAE-F2A97A47EF0E";
	addAttr -ci true -sn "l_elbowGrp" -ln "l_elbowGrp" -at "double";
	setAttr ".t" -type "double3" 18.977118289932502 1.9895196601282805e-013 -1.4210854715202004e-014 ;
	setAttr ".r" -type "double3" -1.2708081109516478e-006 -12.235272407050065 1.1856709489480812e-005 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode transform -n "l_elbow_fk_ctrl_01" -p "l_elbow_grp";
	rename -uid "8DBE6CBD-4833-806B-4F80-52AC3B3FAEE6";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "l_elbowCtrl" -ln "l_elbowCtrl" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -1.4210854715202004e-014 -2.5579538487363607e-013 
		-1.7763568394002505e-014 ;
	setAttr ".sp" -type "double3" -1.4210854715202004e-014 -2.5579538487363607e-013 
		-1.7763568394002505e-014 ;
createNode nurbsCurve -n "l_elbow_fk_ctrl_01Shape" -p "l_elbow_fk_ctrl_01";
	rename -uid "EE56D1D7-4E88-0024-E8F9-168D9550E33F";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-5.6843418860808015e-014 9.0721334875890989 -9.072133487589582
		0 12.829934217807704 -1.4210854715202004e-014
		5.6843418860808015e-014 9.0721334875893263 9.0721334875895394
		5.6843418860808015e-014 -2.2737367544323206e-013 12.829934217808244
		5.6843418860808015e-014 -9.0721334875900084 9.0721334875895394
		0 -12.829934217808614 0
		5.6843418860808015e-014 -9.0721334875900084 -9.0721334875895678
		1.1368683772161603e-013 -2.2737367544323206e-013 -12.829934217808315
		-5.6843418860808015e-014 9.0721334875890989 -9.072133487589582
		0 12.829934217807704 -1.4210854715202004e-014
		5.6843418860808015e-014 9.0721334875893263 9.0721334875895394
		;
createNode transform -n "l_wrist_grp" -p "l_elbow_fk_ctrl_01";
	rename -uid "B3EE58A6-4193-3F5A-CAE0-D39F45E5EF6B";
	addAttr -ci true -sn "l_wristGrp" -ln "l_wristGrp" -at "double";
	setAttr ".t" -type "double3" 17.154433690736106 -8.5265128291212022e-014 1.0658141036401503e-014 ;
	setAttr ".r" -type "double3" -5.3972997667276206e-017 6.3611093629270304e-015 -2.0038013779339099e-016 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1 1.0000000000000002 ;
createNode transform -n "l_wrist_fk_ctrl_01" -p "l_wrist_grp";
	rename -uid "A959BE82-448D-F0E3-16BC-7A80B4C0D6BD";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "l_wristCtrl" -ln "l_wristCtrl" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -1.4921397450962104e-013 -5.6843418860808015e-014 
		3.1974423109204508e-014 ;
	setAttr ".sp" -type "double3" -9.2370555648813024e-014 -5.6843418860808015e-014 
		1.7763568394002505e-014 ;
createNode nurbsCurve -n "l_wrist_fk_ctrl_01Shape" -p "l_wrist_fk_ctrl_01";
	rename -uid "F62D2B7E-4942-2FA9-316C-5EBD41E02B77";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-2.2737367544323206e-013 7.375763703724715 -7.375763703724715
		-2.8421709430404007e-013 10.430905062666625 8.5265128291212022e-014
		-2.2737367544323206e-013 7.375763703724715 7.3757637037248429
		-1.7053025658242404e-013 -2.2737367544323206e-013 10.43090506266681
		-2.2737367544323206e-013 -7.3757637037249424 7.3757637037248287
		-1.7053025658242404e-013 -10.430905062666852 8.5265128291212022e-014
		-1.7053025658242404e-013 -7.3757637037248287 -7.3757637037246866
		-1.7053025658242404e-013 -1.1368683772161603e-013 -10.430905062666682
		-2.2737367544323206e-013 7.375763703724715 -7.375763703724715
		-2.8421709430404007e-013 10.430905062666625 8.5265128291212022e-014
		-2.2737367544323206e-013 7.375763703724715 7.3757637037248429
		;
createNode pointConstraint -n "l_shoulder_grp_pointConstraint1" -p "l_shoulder_grp";
	rename -uid "7CBF462F-4097-2B60-B946-4FAC6629B6BE";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_shoulderFollow_rigW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0 1.4210854715202004e-014 0 ;
	setAttr ".rst" -type "double3" 12.175605385947947 0.00013716935801255659 -1.4861274326918821 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "l_shoulder_grp_orientConstraint1" -p "l_shoulder_grp";
	rename -uid "FED58A1B-4204-5A6E-7AFB-239DA9BE2AE4";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "topSpine_ctrlW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "pelvis_ctrlW1" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w2" -ln "worldPlacementW2" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 3 ".tg";
	setAttr ".o" -type "double3" 5.4951345282390037e-005 -2.8350455379397581 1.5155051878842452e-006 ;
	setAttr ".rsrr" -type "double3" 5.4951345282390037e-005 -2.8350455379397586 1.515505187884245e-006 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
	setAttr -k on ".w2";
createNode transform -n "r_clav_grp" -p "topSpine_ctrl";
	rename -uid "C64BB855-4647-4B94-CA58-39AC35F6CC07";
	addAttr -ci true -sn "r_clavGrp" -ln "r_clavGrp" -at "double";
	setAttr ".t" -type "double3" -7 91.607000000000056 -7.25338 ;
	setAttr ".r" -type "double3" 180 0 0 ;
createNode transform -n "r_clav_rig_ctrl_01" -p "r_clav_grp";
	rename -uid "38E895FE-4F5B-F273-5CAE-9DBBB046B56D";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "r_clavCtrl" -ln "r_clavCtrl" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
createNode nurbsCurve -n "r_clav_rig_ctrl_0Shape1" -p "r_clav_rig_ctrl_01";
	rename -uid "E13B68B6-4517-D8BD-0D08-88AFF8211B45";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 31 0 no 3
		32 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31
		32
		-68.202786701172883 -64.211659294298101 -13.104209491496073
		-69.695418051146078 -68.37146036209532 -9.8687360350916702
		-69.138751408482875 -67.407285454123894 -8.7554027497652598
		-68.202786701172883 -64.211659294298101 -13.104209491496073
		-69.138751408482875 -67.407285454123894 -8.7554027497652598
		-70.502297782925169 -66.620041587787114 -8.7554027497652598
		-68.202786701172883 -64.211659294298101 -13.104209491496073
		-70.502297782925169 -66.620041587787114 -8.7554027497652598
		-71.058964425588385 -67.584216495758596 -9.8687360350916702
		-68.202786701172883 -64.211659294298101 -13.104209491496073
		-69.695418051146078 -68.37146036209532 -9.8687360350916702
		-71.058964425588385 -67.584216495758596 -9.8687360350916702
		-68.202786701172883 -64.211659294298101 -13.104209491496073
		-70.454945723460469 -68.112513147624782 -8.5998914469209087
		-71.303965544474792 -69.583058614254696 -6.4706180080477864
		-71.894795128110403 -70.606405471726248 -4.0168811887849927
		-72.197806220312387 -71.131236078677091 -1.3617216942959729
		-72.197806220312387 -71.131236078677091 1.3617216942960226
		-71.894795128110403 -70.606405471726248 4.0168811887850424
		-71.303965544474792 -69.583058614254696 6.4706180080478362
		-70.454945723460469 -68.112513147624782 8.5998914469209637
		-68.202786701172883 -64.211659294298101 13.104209491496121
		-69.138751408482875 -67.407285454123894 8.7554027497653166
		-69.695418051146078 -68.37146036209532 9.8687360350917288
		-68.202786701172883 -64.211659294298101 13.104209491496121
		-69.695418051146078 -68.37146036209532 9.8687360350917288
		-71.058964425588385 -67.584216495758596 9.8687360350917288
		-68.202786701172883 -64.211659294298101 13.104209491496121
		-70.502297782925169 -66.620041587787114 8.7554027497653166
		-71.058964425588385 -67.584216495758596 9.8687360350917288
		-70.502297782925169 -66.620041587787114 8.7554027497653166
		-69.138751408482875 -67.407285454123894 8.7554027497653166
		;
createNode transform -n "r_shoulder_grp" -p "topSpine_ctrl";
	rename -uid "345611FA-4431-A41D-45CA-6193549DE611";
	addAttr -ci true -sn "r_shoulderGrp" -ln "r_shoulderGrp" -at "double";
	setAttr ".t" -type "double3" -19.175599999999996 91.607099999999917 -8.7395100000000419 ;
	setAttr -av ".ty";
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "r_shoulder_fk_ctrl_01" -p "r_shoulder_grp";
	rename -uid "ED72F529-46E6-3875-5465-62BF2662FE78";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "r_shoulderCtrl" -ln "r_shoulderCtrl" -at "double";
	addAttr -ci true -sn "setControl" -ln "setControl" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "rotateWith" -ln "rotateWith" -min 0 -max 2 -en "topSpine_ctrl:pelvis_ctrl:worldPlacement" 
		-at "enum";
	setAttr -k off ".v";
	setAttr ".ro" 4;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -3.5527136788005009e-015 1.9895196601282805e-013 -3.907985046680551e-014 ;
	setAttr ".sp" -type "double3" -3.5527136788005009e-015 1.9895196601282805e-013 -3.907985046680551e-014 ;
	setAttr -cb on ".setControl";
	setAttr -k on ".rotateWith";
createNode nurbsCurve -n "r_shoulder_fk_ctrl_01Shape" -p "r_shoulder_fk_ctrl_01";
	rename -uid "07F481BF-4E02-0F55-BBDF-A3ABF1AB1139";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.0584319382103331 10.712845645702771 -10.660430668858709
		7.1868024775767481e-007 15.150251550453447 -1.7784124395348044e-008
		1.0584329545776541 10.712845570312936 10.660430643708139
		1.4968495205460997 -5.3308610858948668e-008 15.076125614853893
		1.0584319382102905 -10.712845645702032 10.660430668858645
		-7.1868028328481159e-007 -15.150251550453106 1.7784060446501826e-008
		-1.0584329545776967 -10.712845570312197 -10.660430643708178
		-1.4968495205461352 5.3309122449718416e-008 -15.076125614853925
		-1.0584319382103331 10.712845645702771 -10.660430668858709
		7.1868024775767481e-007 15.150251550453447 -1.7784124395348044e-008
		1.0584329545776541 10.712845570312936 10.660430643708139
		;
createNode transform -n "r_elbow_grp" -p "r_shoulder_fk_ctrl_01";
	rename -uid "5D806D71-402A-4A54-E8E6-6BA5F9AC790F";
	addAttr -ci true -sn "r_elbowGrp" -ln "r_elbowGrp" -at "double";
	setAttr ".t" -type "double3" -18.977126566327041 5.0134272555624193e-007 1.7840870896179695e-006 ;
	setAttr ".r" -type "double3" -1.2708080872381816e-006 -12.235272407050068 1.1856709477303521e-005 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
createNode transform -n "r_elbow_fk_ctrl_01" -p "r_elbow_grp";
	rename -uid "34D2CEF5-46F6-6E07-DE26-77A76AA38100";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "r_elbowCtrl" -ln "r_elbowCtrl" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -2.1316282072803006e-014 5.5422333389287814e-013 2.1316282072803006e-014 ;
	setAttr ".sp" -type "double3" -2.1316282072803006e-014 5.5422333389287814e-013 2.1316282072803006e-014 ;
createNode nurbsCurve -n "r_elbow_fk_ctrl_01Shape" -p "r_elbow_fk_ctrl_01";
	rename -uid "4F6446D0-48C1-CE08-9C32-4994E3F758D5";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0 9.0721334875902357 -9.072133487589582
		-5.6843418860808015e-014 12.829934217808955 1.4210854715202004e-014
		0 9.0721334875902357 9.0721334875895536
		5.6843418860808015e-014 4.5474735088646412e-013 12.829934217808272
		0 -9.0721334875889852 9.072133487589582
		5.6843418860808015e-014 -12.82993421780759 0
		-5.6843418860808015e-014 -9.0721334875888715 -9.0721334875895536
		0 4.5474735088646412e-013 -12.829934217808244
		0 9.0721334875902357 -9.072133487589582
		-5.6843418860808015e-014 12.829934217808955 1.4210854715202004e-014
		0 9.0721334875902357 9.0721334875895536
		;
createNode transform -n "r_wrist_grp" -p "r_elbow_fk_ctrl_01";
	rename -uid "4F3216C0-4360-2250-3AA0-719BC5C4A443";
	addAttr -ci true -sn "r_wristGrp" -ln "r_wristGrp" -at "double";
	setAttr ".t" -type "double3" -17.154387165172878 4.2068128891514789e-007 -1.5932284629371907e-005 ;
	setAttr ".r" -type "double3" -2.442774681463364e-014 1.2722218725854064e-014 3.7758470595788522e-015 ;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999989 1 ;
createNode transform -n "r_wrist_fk_ctrl_01" -p "r_wrist_grp";
	rename -uid "6BF9AFB0-4654-2F4A-3548-2E8C1640A4C1";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "r_wristCtrl" -ln "r_wristCtrl" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 2.2026824808563106e-013 8.5265128291212022e-014 -4.6185277824406512e-014 ;
	setAttr ".sp" -type "double3" 2.2026824808563106e-013 8.5265128291212022e-014 -4.6185277824406512e-014 ;
createNode nurbsCurve -n "r_wrist_fk_ctrl_01Shape" -p "r_wrist_fk_ctrl_01";
	rename -uid "D7F2D5DE-4155-ED5D-F2CF-9C9BF5505A12";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0 7.3757637037248287 -7.3757637037247719
		-5.6843418860808015e-014 10.430905062666795 1.4210854715202004e-014
		0 7.3757637037248287 7.3757637037247719
		5.6843418860808015e-014 -5.6843418860808015e-014 10.430905062666739
		0 -7.3757637037247719 7.3757637037247576
		-5.6843418860808015e-014 -10.430905062666682 -1.4210854715202004e-014
		0 -7.375763703724715 -7.3757637037247719
		-5.6843418860808015e-014 -1.1368683772161603e-013 -10.430905062666753
		0 7.3757637037248287 -7.3757637037247719
		-5.6843418860808015e-014 10.430905062666795 1.4210854715202004e-014
		0 7.3757637037248287 7.3757637037247719
		;
createNode pointConstraint -n "r_shoulder_grp_pointConstraint1" -p "r_shoulder_grp";
	rename -uid "E82CC6C7-438A-2B0A-3325-A19623A681D1";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_shoulderFollow_rigW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0 -1.4210854715202004e-014 0 ;
	setAttr ".rst" -type "double3" -12.175599999999996 -9.9999999861211109e-005 1.4861300000000428 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "r_shoulder_grp_orientConstraint1" -p "r_shoulder_grp";
	rename -uid "376A8166-482A-A959-D732-089992944D19";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "topSpine_ctrlW0" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "pelvis_ctrlW1" -dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w2" -ln "worldPlacementW2" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 3 ".tg";
	setAttr ".o" -type "double3" -179.99994504865478 2.8350455379393611 -1.5155047134869613e-006 ;
	setAttr ".rsrr" -type "double3" -179.99994504865478 2.8350455379393602 -1.5155047134869611e-006 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
	setAttr -k on ".w2";
createNode transform -n "njc_mid_spine_ctrlGrp" -p "pelvis_ctrl";
	rename -uid "B5461140-4700-B6FA-0240-DD81383F1E82";
	setAttr ".rp" -type "double3" 2.7325893195284953e-015 68.373531405250986 -10.287862630020491 ;
	setAttr ".sp" -type "double3" 2.7325893195284953e-015 68.373531405250986 -10.287862630020491 ;
createNode transform -n "mid_ik_ctrl" -p "njc_mid_spine_ctrlGrp";
	rename -uid "6B985490-4EC5-C052-DC14-73AE0EF4877F";
	addAttr -ci true -sn "middleIkCtrl" -ln "middleIkCtrl" -at "double";
	setAttr -k off ".v";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" 2.7325893195284953e-015 68.373531405251001 -10.287862630020491 ;
	setAttr ".sp" -type "double3" 2.7325893195284953e-015 68.373531405251001 -10.287862630020491 ;
createNode nurbsCurve -n "mid_ik_ctrlShape" -p "mid_ik_ctrl";
	rename -uid "55A9943B-4DCF-6E1A-95E2-F1922BF89DD2";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		19.063017018075776 68.373531405251001 -29.350879648096235
		-3.4313044781086606e-016 68.373531405251001 -37.247039836732341
		-19.063017018075751 68.373531405251001 -29.350879648096249
		-26.95917720671185 68.373531405251001 -10.287862630020502
		-19.063017018075758 68.373531405251001 8.775154388055256
		-5.3907369113466679e-015 68.373531405251001 16.671314576691366
		19.063017018075747 68.373531405251001 8.7751543880552632
		26.95917720671186 68.373531405251001 -10.287862630020481
		19.063017018075776 68.373531405251001 -29.350879648096235
		-3.4313044781086606e-016 68.373531405251001 -37.247039836732341
		-19.063017018075751 68.373531405251001 -29.350879648096249
		;
createNode transform -n "l_wrist_ik_grp" -p "rig_controls";
	rename -uid "AF326899-49F7-B362-B2EA-5192786CBF4F";
	addAttr -ci true -sn "l_wristIkGrp" -ln "l_wristIkGrp" -at "double";
	setAttr ".t" -type "double3" 54.693946885477821 91.607148162398929 -3.3406615002818731 ;
createNode transform -n "l_wrist_ctrl_01" -p "l_wrist_ik_grp";
	rename -uid "A0543B3A-4470-965B-D030-069B2CFC4A8A";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "____" -ln "____" -min 0 -max 0 -en "Extend" -at "enum";
	addAttr -ci true -sn "stretch" -ln "stretch" -at "double";
	addAttr -ci true -sn "l_ikWrist" -ln "l_ikWrist" -at "double";
	setAttr -k off ".v";
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -cb on ".____";
	setAttr -k on ".stretch";
createNode nurbsCurve -n "l_wrist_ctrl_0Shape1" -p "l_wrist_ctrl_01";
	rename -uid "F3E30ADE-4BEB-2E96-4F7E-22A9418F3A5F";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-8.6893418955664856 7.089791056577667 5.0025903852110938
		-5.0025857665673499 7.0898047519433192 -8.6893333802796349
		8.6893379989296733 7.0898050996857478 -5.0025772512784465
		5.0025818699305233 7.089791404320124 8.6893465142122839
		-8.6893418955664856 7.089791056577667 5.0025903852110938
		-8.6893379989296733 -7.0898050996858331 5.0025772512784501
		-5.0025818699305233 -7.0897914043201808 -8.6893465142122785
		-5.0025857665673499 7.0898047519433192 -8.6893333802796349
		-8.6893418955664856 7.089791056577667 5.0025903852110938
		-8.6893379989296733 -7.0898050996858331 5.0025772512784501
		5.0025857665673357 -7.089804751943376 8.6893333802796384
		5.0025818699305233 7.089791404320124 8.6893465142122839
		8.6893379989296733 7.0898050996857478 -5.0025772512784465
		8.6893418955664856 -7.0897910565777238 -5.0025903852110902
		5.0025857665673357 -7.089804751943376 8.6893333802796384
		8.6893418955664856 -7.0897910565777238 -5.0025903852110902
		-5.0025818699305233 -7.0897914043201808 -8.6893465142122785
		;
createNode ikHandle -n "l_arm_ik" -p "l_wrist_ctrl_01";
	rename -uid "6AC9D134-40A7-FC7D-5103-0EB973B7C04C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -7.1054273576010019e-015 0 -4.4408920985006262e-016 ;
	setAttr ".hs" 1;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "l_arm_ik_poleVectorConstraint1" -p "l_arm_ik";
	rename -uid "93EA9CAB-439D-CEB7-1C14-B7A315702A3B";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_arm_pv_01W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 18.953891645492973 5.0134104867538554e-007 -59.061378627452157 ;
	setAttr -k on ".w0";
createNode transform -n "l_armLocDown" -p "l_wrist_ctrl_01";
	rename -uid "5652936C-4ABD-0731-A3BC-46B8E20C9A64";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -7.1054273576010019e-015 0 -4.4408920985006262e-016 ;
createNode locator -n "l_armLocDownShape" -p "l_armLocDown";
	rename -uid "A4F644A8-431E-D2D6-E264-548ABA026E87";
	setAttr -k off ".v";
createNode transform -n "l_arm_pv_01" -p "rig_controls";
	rename -uid "26690DDA-4775-F244-4DB6-B6922334938F";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "l_poleArmV" -ln "l_poleArmV" -at "double";
	setAttr -k off ".v";
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" 38.129497817584522 91.607147741701112 -67.800888212796508 ;
	setAttr ".sp" -type "double3" 38.129497817584522 91.607147741701112 -67.800888212796508 ;
createNode nurbsCurve -n "l_arm_pv_0Shape1" -p "l_arm_pv_01";
	rename -uid "CD707DAC-4230-F0E1-7D4A-FEB75F7A5E6C";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 52 0 no 3
		53 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52
		53
		38.129497817584522 95.607147741701112 -67.800888212796508
		38.129497817584522 95.302667741701114 -66.270156212796508
		38.129497817584522 94.435575741701115 -64.972460212796506
		38.129497817584522 93.137879741701113 -64.105368212796506
		38.129497817584522 91.607147741701112 -63.800888212796508
		38.129497817584522 90.076415741701112 -64.105368212796506
		38.129497817584522 88.77871974170111 -64.972460212796506
		38.129497817584522 87.91162774170111 -66.270156212796508
		38.129497817584522 87.607147741701112 -67.800888212796508
		38.129497817584522 87.91162774170111 -69.331620212796508
		38.129497817584522 88.77871974170111 -70.62931621279651
		38.129497817584522 90.076415741701112 -71.49640821279651
		38.129497817584522 91.607147741701112 -71.800888212796508
		38.129497817584522 93.137879741701113 -71.49640821279651
		38.129497817584522 94.435575741701115 -70.62931621279651
		38.129497817584522 95.302667741701114 -69.331620212796508
		38.129497817584522 95.607147741701112 -67.800888212796508
		39.660229817584522 95.302667741701114 -67.800888212796508
		40.957925817584524 94.435575741701115 -67.800888212796508
		41.825017817584524 93.137879741701113 -67.800888212796508
		42.129497817584522 91.607147741701112 -67.800888212796508
		41.825017817584524 90.076415741701112 -67.800888212796508
		40.957925817584524 88.77871974170111 -67.800888212796508
		39.660229817584522 87.91162774170111 -67.800888212796508
		38.129497817584522 87.607147741701112 -67.800888212796508
		36.598765817584521 87.91162774170111 -67.800888212796508
		35.301069817584519 88.77871974170111 -67.800888212796508
		34.43397781758452 90.076415741701112 -67.800888212796508
		34.129497817584522 91.607147741701112 -67.800888212796508
		34.43397781758452 93.137879741701113 -67.800888212796508
		35.301069817584519 94.435575741701115 -67.800888212796508
		36.598765817584521 95.302667741701114 -67.800888212796508
		38.129497817584522 95.607147741701112 -67.800888212796508
		38.129497817584522 95.302667741701114 -69.331620212796508
		38.129497817584522 94.435575741701115 -70.62931621279651
		38.129497817584522 93.137879741701113 -71.49640821279651
		38.129497817584522 91.607147741701112 -71.800888212796508
		36.598765817584521 91.607147741701112 -71.49640821279651
		35.301069817584519 91.607147741701112 -70.62931621279651
		34.43397781758452 91.607147741701112 -69.331620212796508
		34.129497817584522 91.607147741701112 -67.800888212796508
		34.43397781758452 91.607147741701112 -66.270156212796508
		35.301069817584519 91.607147741701112 -64.972460212796506
		36.598765817584521 91.607147741701112 -64.105368212796506
		38.129497817584522 91.607147741701112 -63.800888212796508
		39.660229817584522 91.607147741701112 -64.105368212796506
		40.957925817584524 91.607147741701112 -64.972460212796506
		41.825017817584524 91.607147741701112 -66.270156212796508
		42.129497817584522 91.607147741701112 -67.800888212796508
		41.825017817584524 91.607147741701112 -69.331620212796508
		40.957925817584524 91.607147741701112 -70.62931621279651
		39.660229817584522 91.607147741701112 -71.49640821279651
		38.129497817584522 91.607147741701112 -71.800888212796508
		;
createNode transform -n "r_wrist_ik_grp" -p "rig_controls";
	rename -uid "0AAF35F1-4FC4-BDB6-2784-D3BCFE5917E8";
	addAttr -ci true -sn "r_wristIkGrp" -ln "r_wristIkGrp" -at "double";
	setAttr ".t" -type "double3" -54.693900000000042 91.607099999999747 -3.3406600000001601 ;
createNode transform -n "r_wrist_ctrl_01" -p "r_wrist_ik_grp";
	rename -uid "F2DFB608-4D41-338A-37FC-3C83139F5E4F";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "____" -ln "____" -min 0 -max 0 -en "Extend" -at "enum";
	addAttr -ci true -sn "stretch" -ln "stretch" -at "double";
	addAttr -ci true -sn "r_ikWrist" -ln "r_ikWrist" -at "double";
	setAttr -k off ".v";
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -cb on ".____";
	setAttr -k on ".stretch";
createNode nurbsCurve -n "r_wrist_ctrl_0Shape1" -p "r_wrist_ctrl_01";
	rename -uid "22D0F312-4787-92D7-BB07-D58478C7B0EE";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-8.6893418955663861 -7.0897910565777238 -5.0025903852111417
		-5.0025857665673215 -7.0898047519433192 8.6893333802796136
		8.6893379989297159 -7.0898050996856625 5.0025772512785194
		5.0025818699306654 -7.0897914043200387 -8.6893465142122359
		-8.6893418955663861 -7.0897910565777238 -5.0025903852111417
		-8.6893379989296733 7.0898050996857762 -5.0025772512785123
		-5.0025818699306228 7.0897914043201808 8.689346514212243
		-5.0025857665673215 -7.0898047519433192 8.6893333802796136
		-8.6893418955663861 -7.0897910565777238 -5.0025903852111417
		-8.6893379989296733 7.0898050996857762 -5.0025772512785123
		5.0025857665673641 7.0898047519434328 -8.6893333802796064
		5.0025818699306654 -7.0897914043200387 -8.6893465142122359
		8.6893379989297159 -7.0898050996856625 5.0025772512785194
		8.6893418955664288 7.0897910565778375 5.002590385211148
		5.0025857665673641 7.0898047519434328 -8.6893333802796064
		8.6893418955664288 7.0897910565778375 5.002590385211148
		-5.0025818699306228 7.0897914043201808 8.689346514212243
		;
createNode ikHandle -n "r_arm_ik" -p "r_wrist_ctrl_01";
	rename -uid "C17794BB-4C43-7134-3E3E-4BAAE0B50F03";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 -9.3348560881167941e-008 -2.7977620220553945e-014 ;
	setAttr ".hs" 1;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "r_arm_ik_poleVectorConstraint1" -p "r_arm_ik";
	rename -uid "50771F7A-4ACC-3F0C-7FDF-2BB0380AE9F6";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_arm_pv_01W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -18.95389999999999 -5.6843418860808015e-014 -59.061380000000071 ;
	setAttr -k on ".w0";
createNode transform -n "r_armLocDown" -p "r_wrist_ctrl_01";
	rename -uid "F29F0870-46B5-52D8-A177-6BBEC721C83D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.4210854715202004e-014 2.8421709430404007e-014 -3.9968028886505635e-015 ;
createNode locator -n "r_armLocDownShape" -p "r_armLocDown";
	rename -uid "ABB131E9-449C-85F3-7D0B-B58800950B5C";
	setAttr -k off ".v";
createNode transform -n "r_arm_pv_01" -p "rig_controls";
	rename -uid "652EE221-4170-CAA6-9993-3B8B98041F10";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "r_poleArmV" -ln "r_poleArmV" -at "double";
	setAttr -k off ".v";
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" -38.129499999999986 91.607099999999875 -67.800890000000109 ;
	setAttr ".sp" -type "double3" -38.129499999999986 91.607099999999875 -67.800890000000109 ;
createNode nurbsCurve -n "r_arm_pv_0Shape1" -p "r_arm_pv_01";
	rename -uid "E8926BBB-408B-3DE9-715D-D9BAC20C8751";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 52 0 no 3
		53 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52
		53
		-38.129499999999986 95.607099999999875 -67.800890000000109
		-38.129499999999986 95.302619999999877 -66.270158000000109
		-38.129499999999986 94.435527999999877 -64.972462000000107
		-38.129499999999986 93.137831999999875 -64.105370000000107
		-38.129499999999986 91.607099999999875 -63.800890000000109
		-38.129499999999986 90.076367999999874 -64.105370000000107
		-38.129499999999986 88.778671999999872 -64.972462000000107
		-38.129499999999986 87.911579999999873 -66.270158000000109
		-38.129499999999986 87.607099999999875 -67.800890000000109
		-38.129499999999986 87.911579999999873 -69.33162200000011
		-38.129499999999986 88.778671999999872 -70.629318000000112
		-38.129499999999986 90.076367999999874 -71.496410000000111
		-38.129499999999986 91.607099999999875 -71.800890000000109
		-38.129499999999986 93.137831999999875 -71.496410000000111
		-38.129499999999986 94.435527999999877 -70.629318000000112
		-38.129499999999986 95.302619999999877 -69.33162200000011
		-38.129499999999986 95.607099999999875 -67.800890000000109
		-36.598767999999986 95.302619999999877 -67.800890000000109
		-35.301071999999984 94.435527999999877 -67.800890000000109
		-34.433979999999984 93.137831999999875 -67.800890000000109
		-34.129499999999986 91.607099999999875 -67.800890000000109
		-34.433979999999984 90.076367999999874 -67.800890000000109
		-35.301071999999984 88.778671999999872 -67.800890000000109
		-36.598767999999986 87.911579999999873 -67.800890000000109
		-38.129499999999986 87.607099999999875 -67.800890000000109
		-39.660231999999986 87.911579999999873 -67.800890000000109
		-40.957927999999988 88.778671999999872 -67.800890000000109
		-41.825019999999988 90.076367999999874 -67.800890000000109
		-42.129499999999986 91.607099999999875 -67.800890000000109
		-41.825019999999988 93.137831999999875 -67.800890000000109
		-40.957927999999988 94.435527999999877 -67.800890000000109
		-39.660231999999986 95.302619999999877 -67.800890000000109
		-38.129499999999986 95.607099999999875 -67.800890000000109
		-38.129499999999986 95.302619999999877 -69.33162200000011
		-38.129499999999986 94.435527999999877 -70.629318000000112
		-38.129499999999986 93.137831999999875 -71.496410000000111
		-38.129499999999986 91.607099999999875 -71.800890000000109
		-39.660231999999986 91.607099999999875 -71.496410000000111
		-40.957927999999988 91.607099999999875 -70.629318000000112
		-41.825019999999988 91.607099999999875 -69.33162200000011
		-42.129499999999986 91.607099999999875 -67.800890000000109
		-41.825019999999988 91.607099999999875 -66.270158000000109
		-40.957927999999988 91.607099999999875 -64.972462000000107
		-39.660231999999986 91.607099999999875 -64.105370000000107
		-38.129499999999986 91.607099999999875 -63.800890000000109
		-36.598767999999986 91.607099999999875 -64.105370000000107
		-35.301071999999984 91.607099999999875 -64.972462000000107
		-34.433979999999984 91.607099999999875 -66.270158000000109
		-34.129499999999986 91.607099999999875 -67.800890000000109
		-34.433979999999984 91.607099999999875 -69.33162200000011
		-35.301071999999984 91.607099999999875 -70.629318000000112
		-36.598767999999986 91.607099999999875 -71.496410000000111
		-38.129499999999986 91.607099999999875 -71.800890000000109
		;
createNode transform -n "l_leg_pv_01" -p "rig_controls";
	rename -uid "EC348432-4C13-CA89-DC56-83BB3AF29239";
	addAttr -ci true -sn "l_poleLegV" -ln "l_poleLegV" -at "double";
	setAttr -k off ".v";
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" 10.901291271115294 25.32069249459482 56.617102508024331 ;
	setAttr ".sp" -type "double3" 10.901291271115294 25.32069249459482 56.617102508024331 ;
createNode nurbsCurve -n "l_leg_pv_0Shape1" -p "l_leg_pv_01";
	rename -uid "B84ED378-48D3-D9B3-C913-6BBF08B6AF12";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 52 0 no 3
		53 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52
		53
		10.901291271115294 29.32069249459482 56.617102508024331
		10.901291271115294 29.016212494594818 58.147834508024332
		10.901291271115294 28.149120494594818 59.445530508024326
		10.901291271115294 26.85142449459482 60.312622508024333
		10.901291271115294 25.32069249459482 60.617102508024331
		10.901291271115294 23.789960494594819 60.312622508024333
		10.901291271115294 22.492264494594821 59.445530508024326
		10.901291271115294 21.625172494594821 58.147834508024332
		10.901291271115294 21.32069249459482 56.617102508024331
		10.901291271115294 21.625172494594821 55.086370508024331
		10.901291271115294 22.492264494594821 53.788674508024329
		10.901291271115294 23.789960494594819 52.921582508024329
		10.901291271115294 25.32069249459482 52.617102508024331
		10.901291271115294 26.85142449459482 52.921582508024329
		10.901291271115294 28.149120494594818 53.788674508024329
		10.901291271115294 29.016212494594818 55.086370508024331
		10.901291271115294 29.32069249459482 56.617102508024331
		12.432023271115295 29.016212494594818 56.617102508024331
		13.729719271115295 28.149120494594818 56.617102508024331
		14.596811271115294 26.85142449459482 56.617102508024331
		14.901291271115294 25.32069249459482 56.617102508024331
		14.596811271115294 23.789960494594819 56.617102508024331
		13.729719271115295 22.492264494594821 56.617102508024331
		12.432023271115295 21.625172494594821 56.617102508024331
		10.901291271115294 21.32069249459482 56.617102508024331
		9.3705592711152939 21.625172494594821 56.617102508024331
		8.0728632711152937 22.492264494594821 56.617102508024331
		7.2057712711152941 23.789960494594819 56.617102508024331
		6.9012912711152943 25.32069249459482 56.617102508024331
		7.2057712711152941 26.85142449459482 56.617102508024331
		8.0728632711152937 28.149120494594818 56.617102508024331
		9.3705592711152939 29.016212494594818 56.617102508024331
		10.901291271115294 29.32069249459482 56.617102508024331
		10.901291271115294 29.016212494594818 55.086370508024331
		10.901291271115294 28.149120494594818 53.788674508024329
		10.901291271115294 26.85142449459482 52.921582508024329
		10.901291271115294 25.32069249459482 52.617102508024331
		9.3705592711152939 25.32069249459482 52.921582508024329
		8.0728632711152937 25.32069249459482 53.788674508024329
		7.2057712711152941 25.32069249459482 55.086370508024331
		6.9012912711152943 25.32069249459482 56.617102508024331
		7.2057712711152941 25.32069249459482 58.147834508024332
		8.0728632711152937 25.32069249459482 59.445530508024326
		9.3705592711152939 25.32069249459482 60.312622508024333
		10.901291271115294 25.32069249459482 60.617102508024331
		12.432023271115295 25.32069249459482 60.312622508024333
		13.729719271115295 25.32069249459482 59.445530508024326
		14.596811271115294 25.32069249459482 58.147834508024332
		14.901291271115294 25.32069249459482 56.617102508024331
		14.596811271115294 25.32069249459482 55.086370508024331
		13.729719271115295 25.32069249459482 53.788674508024329
		12.432023271115295 25.32069249459482 52.921582508024329
		10.901291271115294 25.32069249459482 52.617102508024331
		;
createNode transform -n "r_leg_pv_01" -p "rig_controls";
	rename -uid "CC7EA6C2-4AE2-09F3-C9CC-DBAC7794E4B5";
	addAttr -ci true -sn "r_poleLegV" -ln "r_poleLegV" -at "double";
	setAttr -k off ".v";
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" -10.901299999999997 25.320699999999992 56.6171 ;
	setAttr ".sp" -type "double3" -10.901299999999997 25.320699999999992 56.6171 ;
createNode nurbsCurve -n "r_leg_pv_0Shape1" -p "r_leg_pv_01";
	rename -uid "954EEF29-4730-72C0-4E7F-A09B6649949F";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 52 0 no 3
		53 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52
		53
		-10.901299999999997 29.320699999999992 56.617100000000001
		-10.901299999999997 29.01621999999999 58.147832000000001
		-10.901299999999997 28.14912799999999 59.445528000000003
		-10.901299999999997 26.851431999999992 60.312620000000003
		-10.901299999999997 25.320699999999992 60.617100000000001
		-10.901299999999997 23.789967999999991 60.312620000000003
		-10.901299999999997 22.492271999999993 59.445528000000003
		-10.901299999999997 21.625179999999993 58.147832000000001
		-10.901299999999997 21.320699999999992 56.617100000000001
		-10.901299999999997 21.625179999999993 55.086368
		-10.901299999999997 22.492271999999993 53.788671999999998
		-10.901299999999997 23.789967999999991 52.921579999999999
		-10.901299999999997 25.320699999999992 52.617100000000001
		-10.901299999999997 26.851431999999992 52.921579999999999
		-10.901299999999997 28.14912799999999 53.788671999999998
		-10.901299999999997 29.01621999999999 55.086368
		-10.901299999999997 29.320699999999992 56.617100000000001
		-9.3705679999999969 29.01621999999999 56.617100000000001
		-8.0728719999999967 28.14912799999999 56.617100000000001
		-7.2057799999999972 26.851431999999992 56.617100000000001
		-6.9012999999999973 25.320699999999992 56.617100000000001
		-7.2057799999999972 23.789967999999991 56.617100000000001
		-8.0728719999999967 22.492271999999993 56.617100000000001
		-9.3705679999999969 21.625179999999993 56.617100000000001
		-10.901299999999997 21.320699999999992 56.617100000000001
		-12.432031999999998 21.625179999999993 56.617100000000001
		-13.729727999999998 22.492271999999993 56.617100000000001
		-14.596819999999997 23.789967999999991 56.617100000000001
		-14.901299999999997 25.320699999999992 56.617100000000001
		-14.596819999999997 26.851431999999992 56.617100000000001
		-13.729727999999998 28.14912799999999 56.617100000000001
		-12.432031999999998 29.01621999999999 56.617100000000001
		-10.901299999999997 29.320699999999992 56.617100000000001
		-10.901299999999997 29.01621999999999 55.086368
		-10.901299999999997 28.14912799999999 53.788671999999998
		-10.901299999999997 26.851431999999992 52.921579999999999
		-10.901299999999997 25.320699999999992 52.617100000000001
		-12.432031999999998 25.320699999999992 52.921579999999999
		-13.729727999999998 25.320699999999992 53.788671999999998
		-14.596819999999997 25.320699999999992 55.086368
		-14.901299999999997 25.320699999999992 56.617100000000001
		-14.596819999999997 25.320699999999992 58.147832000000001
		-13.729727999999998 25.320699999999992 59.445528000000003
		-12.432031999999998 25.320699999999992 60.312620000000003
		-10.901299999999997 25.320699999999992 60.617100000000001
		-9.3705679999999969 25.320699999999992 60.312620000000003
		-8.0728719999999967 25.320699999999992 59.445528000000003
		-7.2057799999999972 25.320699999999992 58.147832000000001
		-6.9012999999999973 25.320699999999992 56.617100000000001
		-7.2057799999999972 25.320699999999992 55.086368
		-8.0728719999999967 25.320699999999992 53.788671999999998
		-9.3705679999999969 25.320699999999992 52.921579999999999
		-10.901299999999997 25.320699999999992 52.617100000000001
		;
createNode transform -n "l_ankle_ik_grp" -p "rig_controls";
	rename -uid "5109C236-4048-89C5-3CF1-BBAB095579D1";
	setAttr ".t" -type "double3" 10.901293245130104 6.3967546262919992 -5.2448738614950861 ;
createNode transform -n "l_foot_ik_ctrl" -p "l_ankle_ik_grp";
	rename -uid "AA376DD6-4A53-E279-3304-CA84E137DD51";
	addAttr -ci true -sn "l_ikFootCtrl" -ln "l_ikFootCtrl" -at "double";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "_" -ln "_" -min 0 -max 0 -en "Offset" -at "enum";
	addAttr -ci true -sn "xOffset" -ln "xOffset" -at "double";
	addAttr -ci true -sn "yOffset" -ln "yOffset" -at "double";
	addAttr -ci true -sn "zOffset" -ln "zOffset" -at "double";
	addAttr -ci true -sn "__" -ln "__" -min 0 -max 0 -en "Extend" -at "enum";
	addAttr -ci true -sn "stretch" -ln "stretch" -at "double";
	addAttr -ci true -sn "___" -ln "___" -min 0 -max 0 -en "Rolls" -at "enum";
	addAttr -ci true -sn "heelRoll" -ln "heelRoll" -at "double";
	addAttr -ci true -sn "ballRoll" -ln "ballRoll" -at "double";
	addAttr -ci true -sn "toeRoll" -ln "toeRoll" -at "double";
	addAttr -ci true -sn "toeTap" -ln "toeTap" -at "double";
	addAttr -ci true -sn "____" -ln "____" -min 0 -max 0 -en "Pivots" -at "enum";
	addAttr -ci true -sn "heelPivot" -ln "heelPivot" -at "double";
	addAttr -ci true -sn "ballPivot" -ln "ballPivot" -at "double";
	addAttr -ci true -sn "toePivot" -ln "toePivot" -at "double";
	addAttr -ci true -sn "_____" -ln "_____" -min 0 -max 0 -en "SideRoll" -at "enum";
	addAttr -ci true -sn "heelSide" -ln "heelSide" -at "double";
	addAttr -ci true -sn "ballSide" -ln "ballSide" -at "double";
	addAttr -ci true -sn "toeSide" -ln "toeSide" -at "double";
	setAttr -k off ".v";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" 0 -8.8817841970012523e-016 0 ;
	setAttr ".sp" -type "double3" 0 -8.8817841970012523e-016 0 ;
	setAttr -cb on "._";
	setAttr -k on ".xOffset";
	setAttr -k on ".yOffset";
	setAttr -k on ".zOffset";
	setAttr -cb on ".__";
	setAttr -k on ".stretch";
	setAttr -cb on ".___";
	setAttr -k on ".heelRoll";
	setAttr -k on ".ballRoll";
	setAttr -k on ".toeRoll";
	setAttr -k on ".toeTap";
	setAttr -cb on ".____";
	setAttr -k on ".heelPivot";
	setAttr -k on ".ballPivot";
	setAttr -k on ".toePivot";
	setAttr -cb on "._____";
	setAttr -k on ".heelSide";
	setAttr -k on ".ballSide";
	setAttr -k on ".toeSide";
createNode nurbsCurve -n "l_foot_ik_ctrlShape" -p "l_foot_ik_ctrl";
	rename -uid "4706EFB1-4750-970D-47E2-249257431B69";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 6 0 no 3
		11 0 0 0 1 2 3 4 5 6 6 6
		9
		-7.1471102292414415 -6.3967546262919992 5.0374920626361339
		-11.524610016291556 -6.3967546262919992 22.640092167870346
		-0.095968264562254324 -6.3967546262919992 28.40323900289825
		11.918584997126063 -6.3967546262919992 21.892549896543329
		5.2555252251065125 -6.3967546262919992 4.9709877389482573
		9.5850408798754998 -6.3967546262919992 -6.2530900804246636
		0.35019998296400745 -6.3967546262919992 -13.041582057880845
		-9.3695331980514993 -6.3967546262919992 -7.6698847230410783
		-7.1471102292414415 -6.3967546262919992 5.0374920626361339
		;
createNode transform -n "l_heelNode" -p "l_foot_ik_ctrl";
	rename -uid "6CD64B53-4043-7D0D-FFD0-168786621881";
	addAttr -ci true -sn "l_heel" -ln "l_heel" -at "double";
	setAttr ".t" -type "double3" -3.5527136788005009e-015 -6.3967546262919992 -8.2156503822261584e-013 ;
createNode transform -n "l_revToeNode" -p "l_heelNode";
	rename -uid "0C94BD92-4D37-E434-C51B-D380DE46253E";
	addAttr -ci true -sn "l_revToe" -ln "l_revToe" -at "double";
	setAttr ".rp" -type "double3" 0.0067197638704499241 0.19910044613189587 18.102671771426571 ;
	setAttr ".sp" -type "double3" 0.0067197638704499241 0.19910044613189587 18.102671771426571 ;
createNode transform -n "l_revBallNode" -p "l_revToeNode";
	rename -uid "BFFF8294-49E5-733E-C55A-83A1214D2FEA";
	addAttr -ci true -sn "l_revBall" -ln "l_revBall" -at "double";
	setAttr ".rp" -type "double3" 7.8490308794698649e-006 0.19910048463020935 8.4672682239856769 ;
	setAttr ".sp" -type "double3" 7.8490308794698649e-006 0.19910048463020935 8.4672682239856769 ;
createNode transform -n "l_revAnkleNode" -p "l_revBallNode";
	rename -uid "CF0EC0A4-4B57-0535-38E2-0EA06C9FDA25";
	addAttr -ci true -sn "l_revAnkle" -ln "l_revAnkle" -at "double";
	setAttr ".rp" -type "double3" 0 6.3967546262919832 0 ;
	setAttr ".sp" -type "double3" 0 6.3967546262919832 0 ;
createNode ikHandle -n "l__leg_ik" -p "l_revAnkleNode";
	rename -uid "77362EBD-4DE9-468C-01E4-BC8C791C4881";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.7763568394002505e-015 6.396754626292001 8.2334139506201609e-013 ;
	setAttr ".hs" 1;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "l__leg_ik_poleVectorConstraint1" -p "l__leg_ik";
	rename -uid "AF8F4125-41AA-CE49-5ABC-23B1497FDF41";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_leg_pv_01W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 5.6886539159251015e-006 -22.135739650556175 63.179044954602759 ;
	setAttr -k on ".w0";
createNode transform -n "l_legLocDown" -p "l_revAnkleNode";
	rename -uid "FA186FBD-4662-7E3E-9535-4FA97091EC08";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.7763568394002505e-015 6.3967546262919797 -8.8817841970012523e-016 ;
createNode locator -n "l_legLocDownShape" -p "l_legLocDown";
	rename -uid "5BE85478-4A3E-3362-5B84-C48035F662D0";
	setAttr -k off ".v";
createNode ikHandle -n "l_ball_rig_rev_ik" -p "l_revBallNode";
	rename -uid "0EDD30AA-4396-E8DD-E4E6-018F5A81F356";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 7.8546022468373167e-006 0.19910048463021734 8.4672682239856769 ;
	setAttr ".r" -type "double3" -3.3192727837934172e-005 -53.797573879849331 -89.999926533126683 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999989 0.99999999999999989 ;
	setAttr ".hs" 1;
	setAttr ".roc" yes;
createNode transform -n "l_toeTapNode" -p "l_revToeNode";
	rename -uid "11A6944A-4DC8-9EB4-DCEF-699858811853";
	addAttr -ci true -sn "l_toeTap" -ln "l_toeTap" -at "double";
	setAttr ".rp" -type "double3" 7.8490308794698649e-006 0.19910048463020935 8.4672682239856769 ;
	setAttr ".sp" -type "double3" 7.8490308794698649e-006 0.19910048463020935 8.4672682239856769 ;
createNode ikHandle -n "l_toe_rig_rev_ik" -p "l_toeTapNode";
	rename -uid "01282C64-4721-19E3-E7E6-46849333FA45";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.0067197820603333014 0.19910044613586417 18.102671771421178 ;
	setAttr ".r" -type "double3" -89.999671369609132 -89.960087487635391 -0.0003285109633662518 ;
	setAttr ".hs" 1;
	setAttr ".roc" yes;
createNode transform -n "r_ankle_ik_grp" -p "rig_controls";
	rename -uid "DBFE26F9-4F74-10AE-9553-FFAB89D10C8B";
	setAttr ".t" -type "double3" -10.901299999999988 6.3967499999999768 -5.2448699999999953 ;
createNode transform -n "r_foot_ik_ctrl" -p "r_ankle_ik_grp";
	rename -uid "90456830-4847-A3E3-1FB1-4A8BED685303";
	addAttr -ci true -sn "r_ikFootCtrl" -ln "r_ikFootCtrl" -at "double";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "_" -ln "_" -min 0 -max 0 -en "Offset" -at "enum";
	addAttr -ci true -sn "xOffset" -ln "xOffset" -at "double";
	addAttr -ci true -sn "yOffset" -ln "yOffset" -at "double";
	addAttr -ci true -sn "zOffset" -ln "zOffset" -at "double";
	addAttr -ci true -sn "__" -ln "__" -min 0 -max 0 -en "Extend" -at "enum";
	addAttr -ci true -sn "stretch" -ln "stretch" -at "double";
	addAttr -ci true -sn "___" -ln "___" -min 0 -max 0 -en "Rolls" -at "enum";
	addAttr -ci true -sn "heelRoll" -ln "heelRoll" -at "double";
	addAttr -ci true -sn "ballRoll" -ln "ballRoll" -at "double";
	addAttr -ci true -sn "toeRoll" -ln "toeRoll" -at "double";
	addAttr -ci true -sn "toeTap" -ln "toeTap" -at "double";
	addAttr -ci true -sn "____" -ln "____" -min 0 -max 0 -en "Pivots" -at "enum";
	addAttr -ci true -sn "heelPivot" -ln "heelPivot" -at "double";
	addAttr -ci true -sn "ballPivot" -ln "ballPivot" -at "double";
	addAttr -ci true -sn "toePivot" -ln "toePivot" -at "double";
	addAttr -ci true -sn "_____" -ln "_____" -min 0 -max 0 -en "SideRoll" -at "enum";
	addAttr -ci true -sn "heelSide" -ln "heelSide" -at "double";
	addAttr -ci true -sn "ballSide" -ln "ballSide" -at "double";
	addAttr -ci true -sn "toeSide" -ln "toeSide" -at "double";
	setAttr -k off ".v";
	setAttr ".ro" 1;
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" 0 8.8817841970012523e-016 0 ;
	setAttr ".sp" -type "double3" 0 8.8817841970012523e-016 0 ;
	setAttr -cb on "._";
	setAttr -k on ".xOffset";
	setAttr -k on ".yOffset";
	setAttr -k on ".zOffset";
	setAttr -cb on ".__";
	setAttr -k on ".stretch";
	setAttr -cb on ".___";
	setAttr -k on ".heelRoll";
	setAttr -k on ".ballRoll";
	setAttr -k on ".toeRoll";
	setAttr -k on ".toeTap";
	setAttr -cb on ".____";
	setAttr -k on ".heelPivot";
	setAttr -k on ".ballPivot";
	setAttr -k on ".toePivot";
	setAttr -cb on "._____";
	setAttr -k on ".heelSide";
	setAttr -k on ".ballSide";
	setAttr -k on ".toeSide";
createNode nurbsCurve -n "r_foot_ik_ctrlShape" -p "r_foot_ik_ctrl";
	rename -uid "ADD1E255-4C8F-59A7-33F6-5AA1A935018E";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 6 0 no 3
		11 0 0 0 1 2 3 4 5 6 6 6
		9
		-7.1471102292414379 -6.3967499999999751 5.037492062636133
		-11.524610016291552 -6.3967499999999751 22.640092167870346
		-0.095968264562250771 -6.3967499999999751 28.40323900289825
		11.918584997126066 -6.3967499999999751 21.892549896543329
		5.2555252251065161 -6.3967499999999751 4.9709877389482564
		9.5850408798755034 -6.3967499999999751 -6.2530900804246645
		0.350199982964011 -6.3967499999999751 -13.041582057880845
		-9.3695331980514958 -6.3967499999999751 -7.6698847230410792
		-7.1471102292414379 -6.3967499999999751 5.037492062636133
		;
createNode transform -n "r_heelNode" -p "r_foot_ik_ctrl";
	rename -uid "6CFDB61E-4F1B-6B86-6485-C089A86A80A7";
	addAttr -ci true -sn "r_heel" -ln "r_heel" -at "double";
	setAttr ".t" -type "double3" 1.7763568394002505e-015 -6.3967499999999768 0 ;
createNode transform -n "r_revToeNode" -p "r_heelNode";
	rename -uid "FD36CC1D-4614-B294-4E39-03BAB73CF557";
	addAttr -ci true -sn "r_revToe" -ln "r_revToe" -at "double";
	setAttr ".rp" -type "double3" -0.0067000000000128068 0.19910000000000547 18.102669999999989 ;
	setAttr ".sp" -type "double3" -0.0067000000000128068 0.19910000000000547 18.102669999999989 ;
createNode transform -n "r_revBallNode" -p "r_revToeNode";
	rename -uid "06EA7F63-41AE-52EF-ED9D-3F8FA7EF7699";
	addAttr -ci true -sn "r_revBall" -ln "r_revBall" -at "double";
	setAttr ".rp" -type "double3" 5.3290705182007514e-015 0.19909999999998806 8.467259999999996 ;
	setAttr ".sp" -type "double3" 5.3290705182007514e-015 0.19909999999998806 8.467259999999996 ;
createNode transform -n "r_revAnkleNode" -p "r_revBallNode";
	rename -uid "4AAE17FC-465C-6D11-6F06-F7BFA7D6DE7B";
	addAttr -ci true -sn "r_revAnkle" -ln "r_revAnkle" -at "double";
	setAttr ".rp" -type "double3" 0 6.396749999999976 0 ;
	setAttr ".sp" -type "double3" 0 6.396749999999976 0 ;
createNode ikHandle -n "r__leg_ik" -p "r_revAnkleNode";
	rename -uid "54515964-4FBF-080B-82FD-16B37DFF64A2";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.7763568394002505e-015 6.396749999999976 0 ;
	setAttr ".hs" 1;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "r__leg_ik_poleVectorConstraint1" -p "r__leg_ik";
	rename -uid "5902E817-4C2E-94A3-ECD8-308B397F159A";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_leg_pv_01W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 -22.135699999999996 63.17904 ;
	setAttr -k on ".w0";
createNode transform -n "r_legLocDown" -p "r_revAnkleNode";
	rename -uid "25F7A048-4887-DCE2-CA70-B3B622D779F6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.7763568394002505e-015 6.396749999999976 0 ;
createNode locator -n "r_legLocDownShape" -p "r_legLocDown";
	rename -uid "17C6BCA8-4380-E438-0E8C-AC965288AD8D";
	setAttr -k off ".v";
createNode ikHandle -n "r_ball_rig_rev_ik" -p "r_revBallNode";
	rename -uid "7B0C2A27-4783-9F23-5243-099A9DD866D6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.7763568394002505e-015 0.19909999999998274 8.4672599999999942 ;
	setAttr ".r" -type "double3" 179.99996537475073 53.797573879822167 89.999925309476794 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".hs" 1;
	setAttr ".roc" yes;
createNode transform -n "r_toeTapNode" -p "r_revToeNode";
	rename -uid "004218CA-491C-FE1F-953A-788678F2C7BE";
	addAttr -ci true -sn "r_toeTap" -ln "r_toeTap" -at "double";
	setAttr ".rp" -type "double3" 5.3290705182007514e-015 0.19909999999998806 8.467259999999996 ;
	setAttr ".sp" -type "double3" 5.3290705182007514e-015 0.19909999999998806 8.467259999999996 ;
createNode ikHandle -n "r_toe_rig_rev_ik" -p "r_toeTapNode";
	rename -uid "645085CC-4913-86AB-A2DE-1581240B4267";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.0067000000000216886 0.19909999999999656 18.102669999999982 ;
	setAttr ".r" -type "double3" 90.000328668392129 89.960087533671071 0.00032858264182277451 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".hs" 1;
	setAttr ".roc" yes;
createNode transform -n "bind_skeleton" -p "worldPlacement";
	rename -uid "5ACE2DC3-4274-FFE0-1ABD-36AFAE6A19EA";
	addAttr -ci true -sn "bindSkelGrp" -ln "bindSkelGrp" -at "double";
createNode joint -n "pelvis" -p "bind_skeleton";
	rename -uid "45AE3E84-44A5-70AA-F9AB-4D9ABE94D16D";
	addAttr -ci true -sn "pelvisBindJnt" -ln "pelvisBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 48.502845764160156 -9.3133430480957031 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 6.8217670768916108 89.999999999999986 ;
createNode joint -n "spine_01" -p "pelvis";
	rename -uid "D1257DEF-4744-F7F7-C009-05812C202ED7";
	addAttr -ci true -sn "spineBindJnt" -ln "spineBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 8.2043254941233954 1.7763568394002505e-015 -9.108631065094958e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 4.9955644642355823 ;
createNode joint -n "spine_02" -p "spine_01";
	rename -uid "D63DB942-4751-D692-7C93-249982FED96E";
	addAttr -ci true -sn "spineBindJnt_01" -ln "spineBindJnt_01" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 6.9590340171731313 7.1054273576010019e-015 -5.0722076695541734e-008 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -0.73309151890088731 ;
createNode joint -n "spine_03" -p "spine_02";
	rename -uid "E1509B8B-461C-1F88-472C-98A0DAE81B7B";
	addAttr -ci true -sn "spineBindJnt_02" -ln "spineBindJnt_02" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 8.0448897745064443 1.3322676295501878e-014 5.0722073366568009e-008 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 12.218299044961338 ;
createNode joint -n "spineEnd" -p "spine_03";
	rename -uid "B85BC630-4E6D-84AC-9DA7-708C29A3A93A";
	addAttr -ci true -sn "spineEndBindJnt" -ln "spineEndBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 8.5622869382978166 -3.5527136788005009e-015 -9.4895608661595547e-009 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -6.6853931696464857 ;
createNode joint -n "l_clav" -p "spineEnd";
	rename -uid "F8F8D225-4A4B-AE58-3368-779682EFA8DA";
	addAttr -ci true -sn "l_clavicleBindJnt" -ln "l_clavicleBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 11.622164177040304 1.5780783612258062 7.0000007956331762 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.0052494338302926493 -83.04102626328212 -92.968323351190847 ;
createNode joint -n "l_shoulder" -p "l_clav";
	rename -uid "215B873D-46F7-CDA8-5D4D-BF8BE01A7EF1";
	addAttr -ci true -sn "l_shoulderBindJnt" -ln "l_shoulderBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 12.265966789427518 -2.8421709430404007e-014 -2.6645352591003757e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4765013144671253e-005 -9.7940192453054067 -0.00063919823435948863 ;
createNode joint -n "l_elbow" -p "l_shoulder";
	rename -uid "2D42269E-4B4C-1053-28E3-D2BCB0CBCF45";
	addAttr -ci true -sn "l_elbowBindJnt" -ln "l_elbowBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 18.977118289932559 4.2632564145606011e-014 7.1054273576010019e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.2708081242192397e-006 -12.235272407050051 1.185670949122355e-005 ;
createNode joint -n "l_wrist" -p "l_elbow";
	rename -uid "362A9282-4056-ECBE-B0D4-CB9999D6E948";
	addAttr -ci true -sn "l_wristBindJnt" -ln "l_wristBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 17.154433690736084 2.8421709430404007e-014 1.0658141036401503e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "l_index_01" -p "l_wrist";
	rename -uid "26974DDD-4455-F33A-4937-709C4F3E727E";
	addAttr -ci true -sn "l_indexBindJnt01" -ln "l_indexBindJnt01" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 10.392254512342561 1.9517022060274627 4.6647774069108259 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.327136464344193 6.7733761696719359 -2.561495248232982 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_index_02" -p "l_index_01";
	rename -uid "754762DC-4FA1-0D06-8537-AF84C4F5711F";
	addAttr -ci true -sn "l_indexBindJnt02" -ln "l_indexBindJnt02" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 6.7088787484487611 -7.9936057773011271e-015 -2.8421709430404007e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.0358697611746304 -15.470899537925893 -0.69135407776869551 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_index_03" -p "l_index_02";
	rename -uid "BCA40912-4031-A060-797A-FA87A2FDD431";
	addAttr -ci true -sn "l_indexBindJnt03" -ln "l_indexBindJnt03" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 6.332299490449941 3.5527136788005009e-015 -1.4210854715202004e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_pinky_01" -p "l_wrist";
	rename -uid "F7262677-4CEF-53B3-1485-33B80242810E";
	addAttr -ci true -sn "l_pinkyBindJnt01" -ln "l_pinkyBindJnt01" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 8.922783762741858 -0.54561132659441114 -2.5225998266376308 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 88.631908137196064 10.807330603990216 -5.2543246032492048 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_pinky_02" -p "l_pinky_01";
	rename -uid "A039164B-41F2-5D97-00FA-BA8D23B796E7";
	addAttr -ci true -sn "l_pinkyBindJnt02" -ln "l_pinkyBindJnt02" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 5.5703760322813878 1.7763568394002505e-015 -1.4210854715202004e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.4452932773809035 -10.92260865119767 3.3832416023724741 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_pinky_03" -p "l_pinky_02";
	rename -uid "218B0BD3-43C7-A38E-589D-EBAF23B85735";
	addAttr -ci true -sn "l_pinkyBindJnt03" -ln "l_pinkyBindJnt03" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 5.7079347910987792 1.7763568394002505e-015 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_thumb_01" -p "l_wrist";
	rename -uid "F5FD9834-49DA-661D-8D64-8BB36FCDD1EC";
	addAttr -ci true -sn "l_thumbBindJnt01" -ln "l_thumbBindJnt01" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 6.599547306081945 0.12779103382409573 6.8323541418932869 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 80.839478022474054 -30.646912509941817 -26.197877778844287 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_thumb_02" -p "l_thumb_01";
	rename -uid "10F7BD13-419F-CB69-A9EC-D59A9296F9D6";
	addAttr -ci true -sn "l_thumbBindJnt02" -ln "l_thumbBindJnt02" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 6.078922723419371 -6.2172489379008766e-015 -2.8421709430404007e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.5005321103412923 2.8111139027753071 -15.754360215619949 ;
	setAttr ".radi" 0.5;
createNode joint -n "l_thumb_03" -p "l_thumb_02";
	rename -uid "50CF6055-4756-27B9-AE0E-3293526873BD";
	addAttr -ci true -sn "l_thumbBindJnt03" -ln "l_thumbBindJnt03" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 4.632676466946311 8.8817841970012523e-015 -1.4210854715202004e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "neckBase" -p "spineEnd";
	rename -uid "132B0451-4896-5C51-1C28-8FB1C5A72E96";
	addAttr -ci true -sn "neckBaseBindJnt" -ln "neckBaseBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.0313069447810083e-014 -4.2426611175694265e-015 
		20.552789729139025 ;
createNode joint -n "neck" -p "neckBase";
	rename -uid "F399A266-4BDB-E40E-BC76-FEA7CFC51BA2";
	addAttr -ci true -sn "neckBindJnt" -ln "neckBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -113.52640147289696 -89.999999999999972 0 ;
createNode joint -n "l_eye" -p "neck";
	rename -uid "54085047-45B2-3D4F-623C-65AD56A00F6B";
	addAttr -ci true -sn "l_eyeBindJnt" -ln "l_eyeBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 3.275999999999986 4.4439999999999884 5.8279999999999781 ;
	setAttr ".r" -type "double3" -1.4312496066585824e-014 -6.3611093629270391e-015 -4.0552072188659841e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 6.3611093629270367e-015 2.5795090568549831e-014 -2.9764848546715137e-015 ;
createNode joint -n "jaw" -p "neck";
	rename -uid "FD641601-424E-92DA-4101-BBBC2B888A19";
	addAttr -ci true -sn "jawBindJnt" -ln "jawBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 1.0000000925454315e-006 -6.0659707934835865 12.622518224451849 ;
	setAttr ".r" -type "double3" -1.4312496066585824e-014 -6.3611093629270391e-015 -4.0552072188659841e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 6.3611093629270367e-015 2.5795090568549831e-014 -2.9764848546715137e-015 ;
createNode joint -n "r_eye" -p "neck";
	rename -uid "8D43D7D7-48C4-E575-B314-B2B03EF86512";
	addAttr -ci true -sn "r_eyeBindJnt" -ln "r_eyeBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -3.2759998890397544 4.4444147044848137 5.8280005273532964 ;
	setAttr ".r" -type "double3" -1.4312496066585824e-014 -6.3611093629270391e-015 -4.0552072188659841e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 1.0318036227419935e-013 -1.1905939418686055e-014 ;
createNode pointConstraint -n "neck_pointConstraint1" -p "neck";
	rename -uid "BA164F12-4489-E071-EF9C-E38E2DD33A01";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "neck_rigW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0 1.4210854715202004e-014 1.3234889800848443e-023 ;
	setAttr ".rst" -type "double3" 19.769148705961896 0 -6.3548483899259397e-008 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "neck_orientConstraint1" -p "neck";
	rename -uid "B8F32E2D-41A3-8AE3-877C-AB8B1FB2DF32";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "neck_rigW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -9.541664044390787e-015 -1.0813885916975955e-013 2.5126381983561782e-013 ;
	setAttr ".rsrr" -type "double3" -9.541664044390787e-015 -1.0813885916975955e-013 
		2.5126381983561782e-013 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "neck_scaleConstraint1" -p "neck";
	rename -uid "E372C1B1-4643-A32B-8AD6-EB817267EDBD";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "neck_rigW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1.0000000000000002 0.99999999999999978 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "neckBase_pointConstraint1" -p "neckBase";
	rename -uid "90390A28-4C41-CE99-40A6-7DBB0E57E078";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "neckBase_rigW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -1.4210854715202004e-014 3.5527136788005009e-015 -1.9852334701272664e-023 ;
	setAttr ".rst" -type "double3" 16.582691502051858 -2.3092638912203256e-014 -3.7922213667173116e-008 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "neckBase_orientConstraint1" -p "neckBase";
	rename -uid "5E038648-4C06-5228-7915-FE9DEA4FFB97";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "neckBase_rigW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0 0 1.2722218725854067e-014 ;
	setAttr ".rsrr" -type "double3" 0 0 1.2722218725854067e-014 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "neckBase_scaleConstraint1" -p "neckBase";
	rename -uid "E7CBEEDB-4162-B33A-89B5-86B068FF04E6";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "neckBase_rigW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
	setAttr -k on ".w0";
createNode joint -n "r_clav" -p "spineEnd";
	rename -uid "87788D36-472C-C1E6-F935-608A1257D5EA";
	addAttr -ci true -sn "r_clavicleBindJnt" -ln "r_clavicleBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 11.622154231269576 1.5780810334233095 -6.9999999905104486 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.0052494338299850039 -83.041026263282149 87.031676648808741 ;
createNode joint -n "r_shoulder" -p "r_clav";
	rename -uid "3F9B444D-4825-DE44-603A-CFAAC25144B8";
	addAttr -ci true -sn "r_shoulderBindJnt" -ln "r_shoulderBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -12.265961753792826 3.7169301663197984e-005 3.2009490817941355e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4765013145236861e-005 -9.7940192453054067 -0.00063919823436609023 ;
createNode joint -n "r_elbow" -p "r_shoulder";
	rename -uid "6CAC8F70-4721-1C24-9D98-939D48E4D012";
	addAttr -ci true -sn "r_elbowBindJnt" -ln "r_elbowBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -18.977126566327065 5.0134283924307965e-007 1.7840870913943263e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.2708081230543568e-006 -12.235272407050049 1.1856709480355121e-005 ;
createNode joint -n "r_wrist" -p "r_elbow";
	rename -uid "E15D1558-46B8-0312-C136-4B9EFE1C789F";
	addAttr -ci true -sn "r_wristBindJnt" -ln "r_wristBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -17.154387165172821 4.206812036500196e-007 -1.5932284647135475e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "r_index_01" -p "r_wrist";
	rename -uid "0F527015-4FA0-F558-4EEE-1B919CC7D2B4";
	addAttr -ci true -sn "r_indexBindJnt01" -ln "r_indexBindJnt01" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -10.392278887692761 -1.9517042506123801 -4.6647743389104459 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.327136464344278 6.7733761696719599 -2.5614952482330047 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_index_02" -p "r_index_01";
	rename -uid "76B315E3-4B74-5DEC-ED1E-F195EC8E37F2";
	addAttr -ci true -sn "r_indexBindJnt02" -ln "r_indexBindJnt02" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -6.7089432016256296 1.2316135071799295e-005 4.0120761994444365e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.0358697611753138 -15.470899537925897 -0.69135407776869706 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_index_03" -p "r_index_02";
	rename -uid "79C7463F-46D1-0042-611A-9082CC66FEB6";
	addAttr -ci true -sn "r_indexBindJnt03" -ln "r_indexBindJnt03" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -6.3322539243243838 -1.5478452040033203e-006 1.9382374475185316e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 8.5377364625159377e-007 4.739395860767748e-023 -8.2321481033368722e-023 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_pinky_01" -p "r_wrist";
	rename -uid "B82CC506-4F9B-1CCA-2B2E-01838D702AAF";
	addAttr -ci true -sn "r_pinkyBindJnt01" -ln "r_pinkyBindJnt01" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -8.9227898936340182 0.54560265527963736 2.5226020357984886 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 88.631908137196191 10.807330603990241 -5.2543246032492066 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_pinky_02" -p "r_pinky_01";
	rename -uid "1CD1B413-44BD-FF72-158B-3891AA2207DB";
	addAttr -ci true -sn "r_pinkyBindJnt02" -ln "r_pinkyBindJnt02" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -5.570405217129057 5.1312034639039439e-006 7.1098702747462994e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.4452932773796021 -10.922608651197642 3.3832416023724665 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_pinky_03" -p "r_pinky_02";
	rename -uid "53C79B70-41CD-73CA-0CDC-BB8D6A513015";
	addAttr -ci true -sn "r_pinkyBindJnt03" -ln "r_pinkyBindJnt03" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -5.7079534197613739 1.1124028187481372e-006 -7.3710476300448136e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_thumb_01" -p "r_wrist";
	rename -uid "FE27EA89-4320-EB09-10AE-DC8DF5C9B13B";
	addAttr -ci true -sn "r_thumbBindJnt01" -ln "r_thumbBindJnt01" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -6.5996136290624392 -0.12780643716054385 -6.8323332105594563 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 80.83947802247414 -30.64691250994176 -26.197877778844372 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_thumb_02" -p "r_thumb_01";
	rename -uid "AF3D2646-4E87-00C4-C9F6-D8BC510CC00F";
	addAttr -ci true -sn "r_thumbBindJnt02" -ln "r_thumbBindJnt02" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -6.0788805927822063 -3.8894801433819737e-005 9.1199209748538124e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.5005321103409055 2.8111139027753174 -15.754360215619936 ;
	setAttr ".radi" 0.5;
createNode joint -n "r_thumb_03" -p "r_thumb_02";
	rename -uid "FE7592A8-4605-6A33-C4DC-2BBB0440B1CB";
	addAttr -ci true -sn "r_thumbBindJnt03" -ln "r_thumbBindJnt03" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -4.6326928260512723 2.575635213908356e-005 -3.5911719805881148e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 8.5377364625159377e-007 -9.4787916631455918e-023 8.6273358981672525e-023 ;
	setAttr ".radi" 0.5;
createNode joint -n "hips" -p "pelvis";
	rename -uid "F5C338F8-43B8-CC87-5A79-A49C0314E1DC";
	addAttr -ci true -sn "hipsBindJnt" -ln "hipsBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "l_hip" -p "hips";
	rename -uid "75637A38-4140-FCB5-86A6-48B700C05C5B";
	addAttr -ci true -sn "l_hipBindJnt" -ln "l_hipBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -1.365820135474749 2.60762767465165 10.901285582461375 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 -1.4574876783470912e-005 178.64906316091097 ;
createNode joint -n "l_knee" -p "l_hip";
	rename -uid "25FA95E6-478D-E5FA-1B63-29951D75B4BE";
	addAttr -ci true -sn "l_kneeBindJnt" -ln "l_kneeBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 22.362855289533634 8.8817841970012523e-015 -2.6645352591003757e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 13.792099994027064 -8.450309381023591e-006 ;
createNode joint -n "l_ankle" -p "l_knee";
	rename -uid "281E13B4-4006-6EEE-17FD-E7B2858997D9";
	addAttr -ci true -sn "l_ankleBindJnt" -ln "l_ankleBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 19.015319625082249 3.5527136788005009e-015 -5.3290705182007514e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.3260489205091322e-005 -59.416969957867188 7.581762031426619e-005 ;
createNode joint -n "l_ball" -p "l_ankle";
	rename -uid "51799D24-4434-89B7-D23E-708B0A9FDF4F";
	addAttr -ci true -sn "l_ballBindJnt" -ln "l_ballBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 10.49311908045631 -8.8817841970012523e-015 4.4408920985006262e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.029246164036723921 -36.202415693552801 0.049437165494055801 ;
createNode joint -n "l_toe" -p "l_ball";
	rename -uid "1CED42BF-4DA7-EDDF-DBCA-E29B101F0B52";
	addAttr -ci true -sn "l_toeBindJnt" -ln "l_toeBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" 9.6354058851631912 0 -2.4702462297909733e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "r_hip" -p "hips";
	rename -uid "D8C7469A-4E8C-82B3-868C-C089C332A124";
	addAttr -ci true -sn "r_hipBindJnt" -ln "r_hipBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -1.3658523436601939 2.6076262856699879 -10.9013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000014 -1.4574876738857749e-005 -1.3509368390890364 ;
createNode joint -n "r_knee" -p "r_hip";
	rename -uid "E4103F65-4E4B-AC79-E78D-DAA1C2917273";
	addAttr -ci true -sn "r_kneeBindJnt" -ln "r_kneeBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -22.362815337330996 5.6886437409531254e-006 -7.3233510550219094e-007 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.022008702083125e-006 13.792099994027035 -8.4503093858059356e-006 ;
createNode joint -n "r_ankle" -p "r_knee";
	rename -uid "55F04025-4B7F-AC96-5FA1-A1A5830C3CDE";
	addAttr -ci true -sn "r_ankleBindJnt" -ln "r_ankleBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -19.015331074775947 1.974016171857329e-006 -7.5268428156860523e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.3260489250255383e-005 -59.416969957867138 7.5817620393420104e-005 ;
createNode joint -n "r_ball" -p "r_ankle";
	rename -uid "DF387327-43E8-06B3-C6CB-54BBF9F0B1C8";
	addAttr -ci true -sn "r_ballBindJnt" -ln "r_ballBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -10.493109997995635 7.8490247830131921e-006 1.5153520642741114e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.029246164036735103 -36.202415693552801 0.049437165494090038 ;
createNode joint -n "r_toe" -p "r_ball";
	rename -uid "362E95DA-41F6-AC07-E2C7-D59CBF5FEFE8";
	addAttr -ci true -sn "r_toeBindJnt" -ln "r_toeBindJnt" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".t" -type "double3" -9.6354123294209799 1.191933148980695e-005 -3.8502733723522198e-008 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 8.5377364625159355e-007 9.2564205532667063e-026 2.8436374726047257e-022 ;
createNode transform -n "njc_space_switch_grp" -p "worldPlacement";
	rename -uid "DBEA2254-4EEC-0E1F-3602-989473536BB9";
createNode transform -n "njc_spaces_children" -p "njc_space_switch_grp";
	rename -uid "41A564D7-4151-6F8E-F938-91886BE044B2";
createNode transform -n "njc_l_elbow_lock_01_switch" -p "njc_spaces_children";
	rename -uid "153985AE-41CB-B902-5F18-688BFC3A0C08";
	addAttr -ci true -sn "nc_spaces_child" -ln "nc_spaces_child" -at "double";
	addAttr -ci true -sn "l_elbow_lock_01" -ln "l_elbow_lock_01" -at "double";
createNode transform -n "l_elbow_lock_01" -p "njc_l_elbow_lock_01_switch";
	rename -uid "82DF412E-469D-BC60-CBE1-2FBCA69EBFE8";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "l_elbowLock" -ln "l_elbowLock" -at "double";
	addAttr -ci true -sn "space" -ln "space" -min 0 -max 1 -en "l_elbow_BlendSpace_group:worldPlacement" 
		-at "enum";
	setAttr -k off ".v";
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" -7.1054273576010019e-015 0 -3.5527136788005009e-015 ;
	setAttr ".sp" -type "double3" -7.1054273576010019e-015 0 -3.5527136788005009e-015 ;
	setAttr -k on ".space";
createNode nurbsCurve -n "l_elbow_lock_01Shape" -p "l_elbow_lock_01";
	rename -uid "15806CBE-4F5F-5216-82DB-719E085E9FA3";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.0778010131440041 3.1344314935886644 -15.643500629647818
		-4.212247512708835 4.4327617442412759 -15.643500629647818
		-7.3466940122737796 3.1344314935886644 -15.643500629647818
		-8.6450242629264196 -1.5005976337079119e-005 -15.643500629647818
		-7.3466940122737512 -3.134461505541168 -15.643500629647811
		-4.2122475127088634 -4.4327917561938364 -15.643500629647825
		-1.0778010131439757 -3.1344615055412248 -15.643500629647818
		0.22052923750869269 -1.5005976337079119e-005 -15.643500629647832
		-1.0778010131440041 3.1344314935886644 -15.643500629647818
		-4.212247512708835 4.4327617442412759 -15.643500629647818
		-7.3466940122737796 3.1344314935886644 -15.643500629647818
		;
createNode parentConstraint -n "njc_l_elbow_lock_01_switch_parentConstraint1" -p "njc_l_elbow_lock_01_switch";
	rename -uid "689B6554-4473-289C-3353-BA821B1ABD62";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "njc_l_elbow_lock_01_worldPlacement_space_parentW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tot" -type "double3" 38.1294978175845 91.607147741701169 -7.8008882127965053 ;
	setAttr ".tg[0].tor" -type "double3" 5.4960787232943847e-005 -15.070317944995477 
		1.4551771796409595e-006 ;
	setAttr ".tg[1].tot" -type "double3" 38.1294978175845 91.607147741701169 -7.8008882127965071 ;
	setAttr ".tg[1].tor" -type "double3" 5.4960787232943847e-005 -15.070317944995477 
		1.4551771796409627e-006 ;
	setAttr ".lr" -type "double3" 5.4960787232943847e-005 -15.070317944995477 1.455177179640961e-006 ;
	setAttr ".rst" -type "double3" 38.1294978175845 91.607147741701169 -7.8008882127965071 ;
	setAttr ".rsrr" -type "double3" 5.4960787232943847e-005 -15.070317944995477 1.4551771796409627e-006 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "njc_r_elbow_lock_01_switch" -p "njc_spaces_children";
	rename -uid "E4BF375F-4CE0-6698-E67A-94A52FD4CF9A";
	addAttr -ci true -sn "nc_spaces_child" -ln "nc_spaces_child" -at "double";
	addAttr -ci true -sn "r_elbow_lock_01" -ln "r_elbow_lock_01" -at "double";
createNode transform -n "r_elbow_lock_01" -p "njc_r_elbow_lock_01_switch";
	rename -uid "9A9E096A-4C6F-1C86-28A9-49AE669561B2";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "r_elbowLock" -ln "r_elbowLock" -at "double";
	addAttr -ci true -sn "space" -ln "space" -min 0 -max 1 -en "r_elbow_BlendSpace_group:worldPlacement" 
		-at "enum";
	setAttr -k off ".v";
	setAttr -l on -k off ".sz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr ".rp" -type "double3" 1.4210854715202004e-014 -4.2632564145606011e-014 7.1054273576010019e-015 ;
	setAttr ".sp" -type "double3" 1.4210854715202004e-014 -4.2632564145606011e-014 7.1054273576010019e-015 ;
	setAttr -k on ".space";
createNode nurbsCurve -n "r_elbow_lock_01Shape" -p "r_elbow_lock_01";
	rename -uid "1481682E-4D08-2EF1-BB79-438EEF36CBAA";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		7.6487546677528258 3.1344625816211646 16.76529749460494
		4.514308168187938 4.4327928322737478 16.76529749460494
		1.3798616686230503 3.1344625816211362 16.76529749460493
		0.081531417970381881 1.6082056220056984e-005 16.765297494604944
		1.3798616686230503 -3.1344304175086961 16.765297494604944
		4.514308168187938 -4.4327606681613645 16.765297494604937
		7.6487546677528258 -3.1344304175086393 16.765297494604951
		8.9470849184054941 1.6082056163213565e-005 16.76529749460494
		7.6487546677528258 3.1344625816211646 16.76529749460494
		4.514308168187938 4.4327928322737478 16.76529749460494
		1.3798616686230503 3.1344625816211362 16.76529749460493
		;
createNode parentConstraint -n "njc_r_elbow_lock_01_switch_parentConstraint1" -p "njc_r_elbow_lock_01_switch";
	rename -uid "159CC316-449B-1C56-7B18-E7B95BA4979C";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "njc_r_elbow_lock_01_worldPlacement_space_parentW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tot" -type "double3" -38.1295 91.607099999999903 -7.8008900000000967 ;
	setAttr ".tg[0].tor" -type "double3" -179.99994503921283 15.070317944995066 -1.4551767116718652e-006 ;
	setAttr ".tg[1].tot" -type "double3" -38.129499999999993 91.607099999999889 -7.8008900000000949 ;
	setAttr ".tg[1].tor" -type "double3" -179.99994503921283 15.070317944995063 -1.4551767116718652e-006 ;
	setAttr ".lr" -type "double3" -179.99994503921292 15.070320447968555 -1.4551767287889328e-006 ;
	setAttr ".rst" -type "double3" -38.129499999999993 91.607099999999889 -7.8008900000000949 ;
	setAttr ".rsrr" -type "double3" -179.99994503921283 15.070317944995063 -1.4551767116718667e-006 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "njc_spaces_parents" -p "njc_space_switch_grp";
	rename -uid "9A174408-488E-843F-31A7-6CA68D365747";
createNode transform -n "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space" -p "njc_spaces_parents";
	rename -uid "4B6FF339-4CED-AE48-1F13-27B66906D62E";
	addAttr -ci true -sn "njc_spaces_parent" -ln "njc_spaces_parent" -at "double";
	addAttr -ci true -sn "child_l_elbow_lock_01" -ln "child_l_elbow_lock_01" -at "double";
	addAttr -ci true -sn "parent_l_elbow_BlendSpace_group" -ln "parent_l_elbow_BlendSpace_group" 
		-at "double";
createNode transform -n "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parent" 
		-p "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space";
	rename -uid "7C1ACAE3-4C1B-BFA2-FED0-8E9C947E86FA";
createNode parentConstraint -n "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1" 
		-p "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space";
	rename -uid "77F8DCEF-4FE0-ADEB-6051-27869191541C";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_elbow_BlendSpace_groupW0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -34.789865051440813 -91.607130037829407 17.446518106325435 ;
	setAttr ".tg[0].tor" -type "double3" -5.7310182573248239e-005 15.070317944986927 
		-1.6306024467611679e-005 ;
	setAttr ".lr" -type "double3" -5.3081232953654792e-021 1.4463488157450753e-027 -7.5830332790935409e-021 ;
	setAttr ".rst" -type "double3" 0 0 -1.7763568394002505e-015 ;
	setAttr ".rsrr" -type "double3" -5.3081232953654792e-021 1.4463488157450753e-027 
		-7.5830332790935409e-021 ;
	setAttr -k on ".w0";
createNode transform -n "njc_l_elbow_lock_01_worldPlacement_space" -p "njc_spaces_parents";
	rename -uid "26BA0262-4209-7595-2B9A-4396C5DEE221";
	addAttr -ci true -sn "njc_spaces_parent" -ln "njc_spaces_parent" -at "double";
	addAttr -ci true -sn "child_l_elbow_lock_01" -ln "child_l_elbow_lock_01" -at "double";
	addAttr -ci true -sn "parent_worldPlacement" -ln "parent_worldPlacement" -at "double";
createNode transform -n "njc_l_elbow_lock_01_worldPlacement_space_parent" -p "njc_l_elbow_lock_01_worldPlacement_space";
	rename -uid "B33FEF41-444D-856E-740C-CE9080605442";
createNode parentConstraint -n "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1" 
		-p "njc_l_elbow_lock_01_worldPlacement_space";
	rename -uid "509A64B8-48D2-8F10-B9EC-2EB51C26B341";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "worldPlacementW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space" -p "njc_spaces_parents";
	rename -uid "70EFDCEB-4374-82B6-0E8A-6085829B1021";
	addAttr -ci true -sn "njc_spaces_parent" -ln "njc_spaces_parent" -at "double";
	addAttr -ci true -sn "child_r_elbow_lock_01" -ln "child_r_elbow_lock_01" -at "double";
	addAttr -ci true -sn "parent_r_elbow_BlendSpace_group" -ln "parent_r_elbow_BlendSpace_group" 
		-at "double";
createNode transform -n "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parent" 
		-p "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space";
	rename -uid "F9080B3E-413A-23B1-6B1B-DCA4D6952357";
createNode parentConstraint -n "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1" 
		-p "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space";
	rename -uid "82C4C59C-43D8-1A30-4F81-789AE1483BAD";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_elbow_BlendSpace_groupW0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 34.789866694115034 91.607082296126208 -17.44652039945332 ;
	setAttr ".tg[0].tor" -type "double3" 179.99994268981763 15.070317944986529 -1.6306023964899939e-005 ;
	setAttr ".lr" -type "double3" 6.7815622781741741e-013 2.5029734913733807e-006 2.3210940593096836e-012 ;
	setAttr ".rst" -type "double3" 7.1054273576010019e-015 -1.4210854715202004e-014 
		1.7763568394002505e-015 ;
	setAttr ".rsrr" -type "double3" 3.5318906418954871e-016 -3.1805546814635168e-015 
		4.5498199674463237e-021 ;
	setAttr -k on ".w0";
createNode transform -n "njc_r_elbow_lock_01_worldPlacement_space" -p "njc_spaces_parents";
	rename -uid "204196B1-4814-92FF-EBEA-E3BF5C5C86ED";
	addAttr -ci true -sn "njc_spaces_parent" -ln "njc_spaces_parent" -at "double";
	addAttr -ci true -sn "child_r_elbow_lock_01" -ln "child_r_elbow_lock_01" -at "double";
	addAttr -ci true -sn "parent_worldPlacement" -ln "parent_worldPlacement" -at "double";
createNode transform -n "njc_r_elbow_lock_01_worldPlacement_space_parent" -p "njc_r_elbow_lock_01_worldPlacement_space";
	rename -uid "03D5AE03-4229-7012-811B-08AC30EEB9F7";
createNode parentConstraint -n "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1" 
		-p "njc_r_elbow_lock_01_worldPlacement_space";
	rename -uid "48EFE3D6-4735-F509-3EFC-4297BBF50BD9";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "worldPlacementW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "clean_ref_grp";
	rename -uid "791068C4-4BD0-5693-2B6E-67975358464F";
	setAttr ".rp" -type "double3" 0.13793946698122284 93.705004135808878 1.1512159200723033 ;
	setAttr ".sp" -type "double3" 0.13793946698122284 93.705004135808878 1.1512159200723033 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "CCEC0CA5-42C7-B421-56FE-1189A79FC483";
	setAttr -s 8 ".lnk";
	setAttr -s 8 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "ECA6CDDB-40CD-8E8A-7DE4-23A02D275775";
	setAttr -s 2 ".dli[1]"  1;
	setAttr -s 2 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "80EF4EA2-4949-92FB-2A52-77ACDF90BF2A";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "4A4BB92C-4C04-7855-AEB0-48A8F6094B07";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "BD0B2FD8-464F-5E2F-257B-C7BAB346806C";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "52C9D8D3-441F-5C23-D67B-BF8FEAEE10B2";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1240\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1240\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\tscriptedPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 19 100 -ps 2 81 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1240\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1240\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "17B2DA4D-41FC-6E80-8198-E4A21ACF55C8";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode ikSCsolver -n "ikSCsolver";
	rename -uid "D1F51B11-43D0-5443-D206-648C2DDD9BBC";
createNode ikRPsolver -n "ikRPsolver";
	rename -uid "BE4E0986-4F2C-E9A7-AC0C-27B41F7B7746";
createNode ikSplineSolver -n "ikSplineSolver";
	rename -uid "2BE696B7-4A6E-4076-42D5-D09941D9989E";
createNode displayLayer -n "layer1";
	rename -uid "660ECE3C-4B2D-0B81-2CBD-74980F11D3B9";
	setAttr ".do" 1;
createNode cluster -n "cluster1";
	rename -uid "3F797D61-4AF2-74BF-7557-8291E296EFD2";
	setAttr ".wcm" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 2.7325893232234109e-015
		 56.649088358017032 -10.287862630020491 0 0 0 2.7325893232234109e-015 56.649088358017032
		 -10.287862630020491 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pm" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
createNode tweak -n "tweak1";
	rename -uid "C5A1CE01-42B1-8F3F-F3CF-849408D69308";
createNode objectSet -n "cluster1Set";
	rename -uid "CDB5298A-4368-26E0-950A-A2BB6D4B9EFF";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster1GroupId";
	rename -uid "EEE95E4C-45C7-B37A-B24F-C59EE869CD5F";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster1GroupParts";
	rename -uid "B4B4592D-48DE-7CBC-A749-5BA088399625";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[0]";
createNode objectSet -n "tweakSet1";
	rename -uid "19EB1CF6-44F3-7001-ED55-7188CF926673";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId2";
	rename -uid "7B42EB98-4690-11A3-1305-69BD79EFC4D1";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "BDD3AD0F-43F3-5384-9FD3-8DA87AD4EA6E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode cluster -n "cluster2";
	rename -uid "062E2A38-44ED-938C-C268-8DA176D2A003";
	setAttr ".wcm" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 -2.9196095260608423e-008
		 59.275004000646554 -10.329757386832791 0 0 0 -2.9196095260608423e-008 59.275004000646554
		 -10.329757386832791 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pm" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
createNode objectSet -n "cluster2Set";
	rename -uid "3C16A173-4565-5B24-BC24-3B832AC840F1";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster2GroupId";
	rename -uid "4C3844BC-4EAE-CF13-DD3A-54AF05D427A3";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster2GroupParts";
	rename -uid "B52AF1E5-4DBA-2894-791B-4698BF5DE9F3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[1]";
createNode cluster -n "cluster3";
	rename -uid "8FCCEACA-4741-DD9A-B58F-BF86A7AAEA8C";
	setAttr ".wcm" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 -7.952638228755677e-008
		 64.496062648911931 -10.479176482336932 0 0 0 -7.952638228755677e-008 64.496062648911931
		 -10.479176482336932 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pm" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
createNode objectSet -n "cluster3Set";
	rename -uid "E3E0D0B6-4C7E-1C73-53B7-0191999115DD";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster3GroupId";
	rename -uid "83CF4721-4D07-5344-94D3-E6A73364FF9F";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster3GroupParts";
	rename -uid "816EC31B-4FFE-1A71-BEA5-99A606B5869B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[2]";
createNode cluster -n "cluster4";
	rename -uid "258E6612-403D-8B7F-5E50-B5B250709201";
	setAttr ".wcm" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 2.8470594588902456e-008
		 72.363191810609877 -11.277284826066461 0 0 0 2.8470594588902456e-008 72.363191810609877
		 -11.277284826066461 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pm" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
createNode objectSet -n "cluster4Set";
	rename -uid "D7F4E2F3-4B34-9213-BE8C-41A4548F5CB1";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster4GroupId";
	rename -uid "0201D773-49C7-250F-D1F9-89A262E86B0B";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster4GroupParts";
	rename -uid "09C2A5D8-4138-5D9F-5C76-31BD404BDC8A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[3]";
createNode cluster -n "cluster5";
	rename -uid "5EDC4BB9-4B67-A841-771E-1EBF29DD29FA";
	setAttr ".wcm" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 1.2171741546379702e-009
		 77.552392796654573 -10.01770418735598 0 0 0 1.2171741546379702e-009 77.552392796654573
		 -10.01770418735598 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pm" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
createNode objectSet -n "cluster5Set";
	rename -uid "8E4E24D6-4374-6252-B93B-D4922AE4E149";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster5GroupId";
	rename -uid "66C8A2FC-4F1F-7EE2-C281-B582B776ACD9";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster5GroupParts";
	rename -uid "5268358D-415C-6046-15A7-97AA8970D28C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[4]";
createNode cluster -n "cluster6";
	rename -uid "55A79E18-404D-1D39-134B-C0BFBF605B8E";
	setAttr ".wcm" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 -9.4895616142039899e-009
		 80.082359181456326 -9.4322473036750498 0 0 0 -9.4895616142039899e-009 80.082359181456326
		 -9.4322473036750498 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pm" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
createNode objectSet -n "cluster6Set";
	rename -uid "BA1C41AE-4A98-27AF-6795-0BBA5AB931EF";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster6GroupId";
	rename -uid "07BE5D7C-420A-53DF-3B29-70BC285805B7";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster6GroupParts";
	rename -uid "00D27259-4D1D-0A7E-525F-82AB9CD89097";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[5]";
createNode reverse -n "spineCurve_ik_CV_1_point_blend_X_point_reverse";
	rename -uid "F547098F-4EA8-3C30-C5F5-FE9A9FE6707A";
createNode reverse -n "spineCurve_ik_CV_1_point_blend_Y_point_reverse";
	rename -uid "0D89116F-4953-2CB5-BE6D-0FB177CA0BC1";
createNode reverse -n "spineCurve_ik_CV_1_point_blend_Z_point_reverse";
	rename -uid "771D679C-4761-70E0-5A11-C58CA98563F8";
createNode reverse -n "spineCurve_ik_CV_2_point_blend_X_point_reverse";
	rename -uid "7574F1A0-4CAC-09CD-D389-C1B6C220E7F1";
createNode reverse -n "spineCurve_ik_CV_2_point_blend_Y_point_reverse";
	rename -uid "39F630AF-4F1B-58E4-1AAF-B8ACFDAF84A5";
createNode reverse -n "spineCurve_ik_CV_2_point_blend_Z_point_reverse";
	rename -uid "33FEA918-458A-754F-DC69-549E518027AD";
createNode reverse -n "spineCurve_ik_CV_3_point_blend_X_point_reverse";
	rename -uid "4236CE04-4D38-1A62-FC64-8BAA25F2C45C";
createNode reverse -n "spineCurve_ik_CV_3_point_blend_Y_point_reverse";
	rename -uid "0E4941CF-4BB4-9F14-0AB4-7EBB4025D16C";
createNode reverse -n "spineCurve_ik_CV_3_point_blend_Z_point_reverse";
	rename -uid "47885E3B-4ED4-EDB7-33E1-C3BFEEF7043E";
createNode reverse -n "spineCurve_ik_CV_4_point_blend_X_point_reverse";
	rename -uid "3A368611-4428-D8A0-17C2-44ACBA7417DF";
createNode reverse -n "spineCurve_ik_CV_4_point_blend_Y_point_reverse";
	rename -uid "BF9BFE0A-4AD5-9BD8-947C-CAA8FA269BDF";
createNode reverse -n "spineCurve_ik_CV_4_point_blend_Z_point_reverse";
	rename -uid "D551F2D4-4AE9-967A-4CA5-4BAC3AA4A9ED";
createNode multiplyDivide -n "njcMultiSpineStre";
	rename -uid "E3EF8FF3-46BC-6343-C6D3-C7AEBF722F74";
	setAttr ".op" 2;
createNode multiplyDivide -n "njcRigScaleSpineMulti";
	rename -uid "C316A8BC-4CE6-25D7-166A-569918C5D3C5";
	setAttr ".i2" -type "float3" 23.584295 1 1 ;
createNode curveInfo -n "njcArcLengthNode";
	rename -uid "FF0AACA6-48B0-D815-4174-249D1859AA25";
createNode expression -n "expression1";
	rename -uid "F81AF0EE-4494-59A5-1BCF-D1B85A5257C8";
	setAttr -k on ".nds";
	setAttr ".ixp" -type "string" ".O[0] = .I[0] * 8.204325";
createNode expression -n "expression2";
	rename -uid "F1E84922-4ABC-D11A-A5B4-FC85CA9900AE";
	setAttr -k on ".nds";
	setAttr ".ixp" -type "string" ".O[0] = .I[0] * 6.959034";
createNode expression -n "expression3";
	rename -uid "6616C191-4CF1-C00B-B243-DFB71672E68D";
	setAttr -k on ".nds";
	setAttr ".ixp" -type "string" ".O[0] = .I[0] * 8.044890";
createNode expression -n "expression4";
	rename -uid "AF5A3DAA-46A0-0A03-A021-8191F49F321D";
	setAttr -k on ".nds";
	setAttr ".ixp" -type "string" ".O[0] = .I[0] * 8.562287";
createNode expression -n "expression5";
	rename -uid "AF9B1900-4EE9-F19E-5DDB-EAB4FFB0C4FD";
	setAttr -k on ".nds";
	setAttr -s 2 ".in";
	setAttr -s 2 ".in";
	setAttr -s 2 ".out";
	setAttr ".ixp" -type "string" "if(.I[0] >= -.5 && .I[0] <= 2 && .I[1] > 0){.O[0] = (.I[0] * (-0.150000+0.1)) + 1;}if(.I[0] >= -.5 && .I[0] <= 2 && .I[1] > 0){.O[1] = (.I[0] * -0.150000) + 1;}";
createNode expression -n "expression6";
	rename -uid "0D8EAE1F-46B5-6420-901D-7F980314F3DB";
	setAttr -k on ".nds";
	setAttr -s 2 ".in";
	setAttr -s 2 ".in";
	setAttr -s 2 ".out";
	setAttr ".ixp" -type "string" "if(.I[0] >= -.5 && .I[0] <= 2 && .I[1] > 0){.O[0] = (.I[0] * (-0.050000+0.1)) + 1;}if(.I[0] >= -.5 && .I[0] <= 2 && .I[1] > 0){.O[1] = (.I[0] * -0.050000) + 1;}";
createNode expression -n "expression7";
	rename -uid "A63AA1F6-4F5F-4305-8549-2E804F12E29F";
	setAttr -k on ".nds";
	setAttr -s 2 ".in";
	setAttr -s 2 ".in";
	setAttr -s 2 ".out";
	setAttr ".ixp" -type "string" "if(.I[0] >= -.5 && .I[0] <= 2 && .I[1] > 0){.O[0] = (.I[0] * (-0.050000+0.1)) + 1;}if(.I[0] >= -.5 && .I[0] <= 2 && .I[1] > 0){.O[1] = (.I[0] * -0.050000) + 1;}";
createNode reference -n "Grunt_Skeleton_skinRN";
	rename -uid "0F43A29D-4558-C3CF-4126-1EA0C375DC20";
	setAttr ".ed" -type "dataReferenceEdits" 
		"Grunt_Skeleton_skinRN"
		"Grunt_Skeleton_skinRN" 0;
lockNode -l 1 ;
createNode reverse -n "l_shoulder_rig_fk_point_reverse";
	rename -uid "828AE344-40DD-C89C-25DE-8D8FED45F1AD";
createNode reverse -n "l_shoulder_rig_fk_point_OR_reverse";
	rename -uid "DFF9C611-4CFA-B1B9-7C69-16ACBC36020E";
createNode reverse -n "l_elbow_rig_fk_point_reverse";
	rename -uid "276C7FC9-4B33-7028-EF1C-C499DC9E1D4A";
createNode reverse -n "l_elbow_rig_fk_point_OR_reverse";
	rename -uid "745EF5CA-43BC-1715-F0FE-F58CC59AF92A";
createNode reverse -n "l_wrist_rig_fk_point_reverse";
	rename -uid "7BF6CF23-43AF-4CCA-8E3D-0495A02EEA3C";
createNode reverse -n "l_wrist_rig_fk_point_OR_reverse";
	rename -uid "F4A2878B-436A-7C61-8115-51AB84A0CCEB";
createNode condition -n "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_spaceCond";
	rename -uid "74FC1C1F-4516-844F-B940-6FADADB7D60C";
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode condition -n "njc_l_elbow_lock_01_worldPlacement_spaceCond";
	rename -uid "4176FE5D-4637-50FB-9BF1-8196C661E06E";
	setAttr ".st" 1;
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode reverse -n "l_shoulder_fk_ctrl_01_rev";
	rename -uid "55E18911-4038-6146-D37C-FDB0BB47806E";
createNode condition -n "topSpine_ctrl_shoFollowCond";
	rename -uid "BE3D163F-43B8-2399-6708-ADAA7F9A64A1";
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode condition -n "pelvis_ctrl_shoFollowCond";
	rename -uid "8704A74C-4763-AE8C-D61F-EA8B6FCCC936";
	setAttr ".st" 1;
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode condition -n "worldPlacement_shoFollowCond";
	rename -uid "5E4969CA-4ECF-56F4-14AB-9881B40B8D91";
	setAttr ".st" 2;
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode distanceBetween -n "l_arm_Stretchy_distLoc_up";
	rename -uid "F1D3253E-4619-1203-EE5D-839E5671C154";
createNode multiplyDivide -n "l_arm_jntMulti";
	rename -uid "EE2D7728-4A43-BB40-1B6B-F284596EA6E3";
	setAttr ".i1" -type "float3" 36.131554 0 0 ;
createNode plusMinusAverage -n "l_arm_addSub";
	rename -uid "EF4D5D59-46BA-6513-6E7A-3EB10D2A392A";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode expression -n "expression8";
	rename -uid "9937E746-49DC-9A04-C82A-E7884C3DA25D";
	setAttr -k on ".nds";
	setAttr -s 4 ".in";
	setAttr -s 4 ".in";
	setAttr ".ixp" -type "string" "if (.I[0] == 1){if(.I[1] >= (.I[2]+.I[3])){.O[0] = (.I[1] / .I[2]) * 18.977118;}else{.O[0] = (18.977118+(.I[3] *.5));}}else if (.I[0] == 0){.O[0] = (18.977118+.I[3] *.5);}";
createNode expression -n "expression9";
	rename -uid "CEB5E0A1-4DC4-84B9-AF8A-34B511861A31";
	setAttr -k on ".nds";
	setAttr -s 4 ".in";
	setAttr -s 4 ".in";
	setAttr ".ixp" -type "string" "if (.I[0] == 1){if(.I[1] >= (.I[2]+.I[3])){.O[0] = (.I[1] / .I[2]) * 17.154434;}else{.O[0] = (17.154434+(.I[3] *.5));}}else if (.I[0] == 0){.O[0] = (17.154434+.I[3] *.5);}";
createNode reverse -n "r_shoulder_rig_fk_point_reverse";
	rename -uid "D6289F6D-48E2-4F4B-F234-288D2FE14BCD";
createNode reverse -n "r_shoulder_rig_fk_point_OR_reverse";
	rename -uid "68FED4F0-4799-205D-1D83-F28F8F5CB0E4";
createNode reverse -n "r_elbow_rig_fk_point_reverse";
	rename -uid "68B8310E-4CBB-BC94-CBCC-C196669E5298";
createNode reverse -n "r_elbow_rig_fk_point_OR_reverse";
	rename -uid "07163481-4A7B-D4D0-F63D-00B822EE60AA";
createNode reverse -n "r_wrist_rig_fk_point_reverse";
	rename -uid "22FDD185-4634-246E-5AC9-01B456647B23";
createNode reverse -n "r_wrist_rig_fk_point_OR_reverse";
	rename -uid "83F0057E-4B7B-3BD1-0937-7298A9EA316A";
createNode condition -n "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_spaceCond";
	rename -uid "E08617D6-404E-74EB-DD64-AD837B8255FE";
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode condition -n "njc_r_elbow_lock_01_worldPlacement_spaceCond";
	rename -uid "EC15965D-414B-457C-0541-AEAA2C1718BB";
	setAttr ".st" 1;
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode reverse -n "r_shoulder_fk_ctrl_01_rev";
	rename -uid "7AE180C9-443D-A9D8-5A1B-65B79B2C4B85";
createNode condition -n "topSpine_ctrl_shoFollowCond1";
	rename -uid "94A79BCD-4F19-DAC5-C955-379B18596601";
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode condition -n "pelvis_ctrl_shoFollowCond1";
	rename -uid "C82C6DDA-47C2-CED0-87A7-2F83E158E8F3";
	setAttr ".st" 1;
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode condition -n "worldPlacement_shoFollowCond1";
	rename -uid "50BFDB4E-4C24-8D06-93E0-8BBD908432FB";
	setAttr ".st" 2;
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode distanceBetween -n "r_arm_Stretchy_distLoc_up";
	rename -uid "C89AEB20-4F99-AD25-4DE7-15A258911B46";
createNode multiplyDivide -n "r_arm_jntMulti";
	rename -uid "4F032F09-47DE-CF96-532B-E5BDAD3B7FF8";
	setAttr ".i1" -type "float3" 36.131516 0 0 ;
createNode plusMinusAverage -n "r_arm_addSub";
	rename -uid "DCF641B0-4EC7-360D-344C-B09804C86480";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode expression -n "expression10";
	rename -uid "06DD3EA2-4042-8290-7F32-23B82985662F";
	setAttr -k on ".nds";
	setAttr -s 4 ".in";
	setAttr -s 4 ".in";
	setAttr ".ixp" -type "string" "if (.I[0] == 1){if(.I[1] >= (.I[2]+.I[3])){.O[0] = (.I[1] / .I[2]) * -18.977127;}else{.O[0] = (-18.977127+(.I[3] *.5));}}else if (.I[0] == 0){.O[0] = (-18.977127+.I[3] *.5);}";
createNode expression -n "expression11";
	rename -uid "A7925295-4CB2-7AC5-F3E6-9AB70CE514E4";
	setAttr -k on ".nds";
	setAttr -s 4 ".in";
	setAttr -s 4 ".in";
	setAttr ".ixp" -type "string" "if (.I[0] == 1){if(.I[1] >= (.I[2]+.I[3])){.O[0] = (.I[1] / .I[2]) * -17.154387;}else{.O[0] = (-17.154387+(.I[3] *.5));}}else if (.I[0] == 0){.O[0] = (-17.154387+.I[3] *.5);}";
createNode reverse -n "l_hip_rig_fk_point_reverse";
	rename -uid "50843A74-4C86-E700-1FF0-6E80A8B36DA5";
createNode reverse -n "l_hip_rig_fk_point_OR_reverse";
	rename -uid "20E41DB1-4484-F66A-4777-0A8697AF71B7";
createNode reverse -n "l_knee_rig_fk_point_reverse";
	rename -uid "DCAE0B32-4513-1B72-886F-BA97DC3889E7";
createNode reverse -n "l_knee_rig_fk_point_OR_reverse";
	rename -uid "078A675C-4C62-B458-E269-1B95B223E3E6";
createNode reverse -n "l_ankle_rig_fk_point_reverse";
	rename -uid "9AFA1602-48E3-A884-1A1C-2C882B5B7B41";
createNode reverse -n "l_ankle_rig_fk_point_OR_reverse";
	rename -uid "D987BBFD-46C3-0308-9B14-4E87D0C511AA";
createNode reverse -n "l_ball_rig_fk_point_reverse";
	rename -uid "9259E3AE-4C40-3F4F-C6C9-EE904CCB2134";
createNode reverse -n "l_ball_rig_fk_point_OR_reverse";
	rename -uid "85103851-4CD5-3EAD-D50B-66B1FAEB1725";
createNode reverse -n "l_toe_rig_fk_point_reverse";
	rename -uid "82B3416E-4593-E743-5C25-F2A55857CF62";
createNode reverse -n "l_toe_rig_fk_point_OR_reverse";
	rename -uid "B76E9966-48B4-70BB-CA15-D2BA5E32FD06";
createNode reverse -n "r_hip_rig_fk_point_reverse";
	rename -uid "EBCE7954-4D74-6C83-51B9-6481AF4C05F4";
createNode reverse -n "r_hip_rig_fk_point_OR_reverse";
	rename -uid "52846F8F-4463-9667-4A50-518244811663";
createNode reverse -n "r_knee_rig_fk_point_reverse";
	rename -uid "B94DB084-4FAA-F1EA-1E55-8EB54B6FCA27";
createNode reverse -n "r_knee_rig_fk_point_OR_reverse";
	rename -uid "4D90B291-47EE-3374-2A7D-E4AA6236424F";
createNode reverse -n "r_ankle_rig_fk_point_reverse";
	rename -uid "5F02B084-42AC-AC87-BD3B-75BB57D377D3";
createNode reverse -n "r_ankle_rig_fk_point_OR_reverse";
	rename -uid "477BCFD0-4BDE-DCEF-DDEF-EF8B68FAEFD0";
createNode reverse -n "r_ball_rig_fk_point_reverse";
	rename -uid "47541332-4C94-5467-8DB9-2C886A61FA0C";
createNode reverse -n "r_ball_rig_fk_point_OR_reverse";
	rename -uid "6BBEFDDB-44E1-2A23-32A5-3897FF11AC48";
createNode reverse -n "r_toe_rig_fk_point_reverse";
	rename -uid "6865BC21-437B-451E-906E-DE97E97EE130";
createNode reverse -n "r_toe_rig_fk_point_OR_reverse";
	rename -uid "C03643B2-48B3-AD77-2940-D3B3D3084630";
createNode unitConversion -n "unitConversion1";
	rename -uid "CB646F9B-4539-876C-2828-F5A978073D4D";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion2";
	rename -uid "250E10D7-44A4-5828-55B3-979F2003E466";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion3";
	rename -uid "4711B01A-42EF-1DBD-F14A-5F8FC373AC45";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion4";
	rename -uid "3CA528A6-4236-6998-2CE9-57A711E0C544";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion5";
	rename -uid "07EF928F-4A59-079E-A619-4E93F830009E";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion6";
	rename -uid "63826C0E-49BF-FEE5-9574-8AB273B4D530";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion7";
	rename -uid "DFDB8161-465D-6790-1F6F-2DA3F93D6855";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion8";
	rename -uid "B568C510-4575-4B04-745C-229B9951145B";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion9";
	rename -uid "D0AF08BF-44A4-1D8E-69DC-B5A946242AE2";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion10";
	rename -uid "199AE891-4864-EAB5-ABE3-F89C88143465";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion11";
	rename -uid "F9EDD46D-45E5-818D-EC2B-5B82858B79C4";
	setAttr ".cf" 0.017453292519943295;
createNode distanceBetween -n "l_leg_Stretchy_distLoc_up";
	rename -uid "873AC403-4C0C-52A1-B673-399C0663BAC6";
createNode multiplyDivide -n "l_leg_jntMulti";
	rename -uid "8981F5D9-4839-50B7-BCD6-EDB48F0997E9";
	setAttr ".i1" -type "float3" 41.378174 0 0 ;
createNode plusMinusAverage -n "l_leg_addSub";
	rename -uid "2FA6212E-46AC-477F-D4ED-2A90024E733F";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode expression -n "expression12";
	rename -uid "888B3655-46B6-4736-4780-0983C1F596F5";
	setAttr -k on ".nds";
	setAttr -s 4 ".in";
	setAttr -s 4 ".in";
	setAttr ".ixp" -type "string" "if (.I[0] == 1){if(.I[1] >= (.I[2]+.I[3])){.O[0] = (.I[1] / .I[2]) * 22.362855;}else{.O[0] = (22.362855+(.I[3] *.5));}}else if (.I[0] == 0){.O[0] = (22.362855+.I[3] *.5);}";
createNode expression -n "expression13";
	rename -uid "D97F110D-47F0-A349-3EEB-2DAA22C07530";
	setAttr -k on ".nds";
	setAttr -s 4 ".in";
	setAttr -s 4 ".in";
	setAttr ".ixp" -type "string" "if (.I[0] == 1){if(.I[1] >= (.I[2]+.I[3])){.O[0] = (.I[1] / .I[2]) * 19.015320;}else{.O[0] = (19.015320+(.I[3] *.5));}}else if (.I[0] == 0){.O[0] = (19.015320+.I[3] *.5);}";
createNode reverse -n "l_hip_ctrl_rev";
	rename -uid "427C1A4B-429C-290D-6E7E-60861E3B0602";
createNode condition -n "worldPlacement_hipFollowCond";
	rename -uid "C0DEA0A9-4D3B-3470-2254-08B74AC14A22";
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode condition -n "pelvis_ctrl_hipFollowCond";
	rename -uid "171180CF-46C4-60D2-4C8D-56B462DBEAAB";
	setAttr ".st" 1;
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode condition -n "hips_ctrl_hipFollowCond";
	rename -uid "25288EE7-4192-C25F-79E3-F79FFAC21C2D";
	setAttr ".st" 2;
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode unitConversion -n "unitConversion12";
	rename -uid "6C5621FA-4350-4A8B-85EB-0FA2E0339F86";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion13";
	rename -uid "DBBECA0D-4276-F8F2-24BA-3DB35E516A23";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion14";
	rename -uid "62ABCAFB-4F5B-A0FC-8E6C-F1BEE564822F";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion15";
	rename -uid "FD2B59A8-4686-20C8-AA16-5ABA5977AC52";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion16";
	rename -uid "5901E46B-462A-8A72-E266-F49B654AE82C";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion17";
	rename -uid "795B44B6-450D-EB2D-5903-0B9DA8941C66";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion18";
	rename -uid "51443F9A-4688-F8C0-1C13-E58341FCAE25";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion19";
	rename -uid "3D33B040-496C-434A-7658-2686E5AC6078";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion20";
	rename -uid "46CE0DE5-442D-742C-F155-089D9E135275";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion21";
	rename -uid "8A9C1CBA-4FE5-0A35-E006-D9A62598BEBC";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion22";
	rename -uid "3C286EFC-4FE4-4EA2-4DA4-8D9B5AC374CA";
	setAttr ".cf" 0.017453292519943295;
createNode distanceBetween -n "r_leg_Stretchy_distLoc_up";
	rename -uid "A1880382-46B2-BB42-2479-96A44B88C5AD";
createNode multiplyDivide -n "r_leg_jntMulti";
	rename -uid "D07196D1-47C8-2813-6D79-7FABF5C444E2";
	setAttr ".i1" -type "float3" 41.378147 0 0 ;
createNode plusMinusAverage -n "r_leg_addSub";
	rename -uid "F34095FC-4947-8258-3F85-EEA7C8F62FAC";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode expression -n "expression14";
	rename -uid "7631203F-4959-6BAD-7A5C-FF8B4E4BEC6E";
	setAttr -k on ".nds";
	setAttr -s 4 ".in";
	setAttr -s 4 ".in";
	setAttr ".ixp" -type "string" "if (.I[0] == 1){if(.I[1] >= (.I[2]+.I[3])){.O[0] = (.I[1] / .I[2]) * -22.362815;}else{.O[0] = (-22.362815+(.I[3] *.5));}}else if (.I[0] == 0){.O[0] = (-22.362815+.I[3] *.5);}";
createNode expression -n "expression15";
	rename -uid "895BFE5E-4A56-E1AA-6ABF-09B42CFFF6DE";
	setAttr -k on ".nds";
	setAttr -s 4 ".in";
	setAttr -s 4 ".in";
	setAttr ".ixp" -type "string" "if (.I[0] == 1){if(.I[1] >= (.I[2]+.I[3])){.O[0] = (.I[1] / .I[2]) * -19.015331;}else{.O[0] = (-19.015331+(.I[3] *.5));}}else if (.I[0] == 0){.O[0] = (-19.015331+.I[3] *.5);}";
createNode reverse -n "r_hip_ctrl_rev";
	rename -uid "738E62FF-483A-A0E5-08B2-AB8911B95CCB";
createNode condition -n "worldPlacement_hipFollowCond1";
	rename -uid "7CCC2789-433B-DD10-2B5E-40AE1192DC78";
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode condition -n "pelvis_ctrl_hipFollowCond1";
	rename -uid "79AC6651-43EF-6C0F-94C3-0088FE6A9EEC";
	setAttr ".st" 1;
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode condition -n "hips_ctrl_hipFollowCond1";
	rename -uid "24A738FC-4E11-5433-9A8C-3D9B9EE4D07C";
	setAttr ".st" 2;
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 6 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 79 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 2 ".r";
select -ne :defaultTextureList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "l_elbow_BlendSpace_group_pointConstraint1.ctx" "l_elbow_BlendSpace_group.tx"
		;
connectAttr "l_elbow_BlendSpace_group_pointConstraint1.cty" "l_elbow_BlendSpace_group.ty"
		;
connectAttr "l_elbow_BlendSpace_group_pointConstraint1.ctz" "l_elbow_BlendSpace_group.tz"
		;
connectAttr "l_elbow_BlendSpace_group_orientConstraint1.crx" "l_elbow_BlendSpace_group.rx"
		;
connectAttr "l_elbow_BlendSpace_group_orientConstraint1.cry" "l_elbow_BlendSpace_group.ry"
		;
connectAttr "l_elbow_BlendSpace_group_orientConstraint1.crz" "l_elbow_BlendSpace_group.rz"
		;
connectAttr "l_arm_ik_switch.IkFkSwitch" "l_elbow_BlendSpace_group.blendSpace";
connectAttr "l_elbow_BlendSpace_group.pim" "l_elbow_BlendSpace_group_pointConstraint1.cpim"
		;
connectAttr "l_elbow_BlendSpace_group.rp" "l_elbow_BlendSpace_group_pointConstraint1.crp"
		;
connectAttr "l_elbow_BlendSpace_group.rpt" "l_elbow_BlendSpace_group_pointConstraint1.crt"
		;
connectAttr "l_elbow_rig_ik.t" "l_elbow_BlendSpace_group_pointConstraint1.tg[0].tt"
		;
connectAttr "l_elbow_rig_ik.rp" "l_elbow_BlendSpace_group_pointConstraint1.tg[0].trp"
		;
connectAttr "l_elbow_rig_ik.rpt" "l_elbow_BlendSpace_group_pointConstraint1.tg[0].trt"
		;
connectAttr "l_elbow_rig_ik.pm" "l_elbow_BlendSpace_group_pointConstraint1.tg[0].tpm"
		;
connectAttr "l_elbow_BlendSpace_group_pointConstraint1.w0" "l_elbow_BlendSpace_group_pointConstraint1.tg[0].tw"
		;
connectAttr "l_elbow_rig_fk.t" "l_elbow_BlendSpace_group_pointConstraint1.tg[1].tt"
		;
connectAttr "l_elbow_rig_fk.rp" "l_elbow_BlendSpace_group_pointConstraint1.tg[1].trp"
		;
connectAttr "l_elbow_rig_fk.rpt" "l_elbow_BlendSpace_group_pointConstraint1.tg[1].trt"
		;
connectAttr "l_elbow_rig_fk.pm" "l_elbow_BlendSpace_group_pointConstraint1.tg[1].tpm"
		;
connectAttr "l_elbow_BlendSpace_group_pointConstraint1.w1" "l_elbow_BlendSpace_group_pointConstraint1.tg[1].tw"
		;
connectAttr "l_elbow_BlendSpace_group.blendSpace" "l_elbow_BlendSpace_group_pointConstraint1.w0"
		;
connectAttr "l_elbow_rig_fk_point_reverse.ox" "l_elbow_BlendSpace_group_pointConstraint1.w1"
		;
connectAttr "l_elbow_BlendSpace_group.ro" "l_elbow_BlendSpace_group_orientConstraint1.cro"
		;
connectAttr "l_elbow_BlendSpace_group.pim" "l_elbow_BlendSpace_group_orientConstraint1.cpim"
		;
connectAttr "l_elbow_rig_ik.r" "l_elbow_BlendSpace_group_orientConstraint1.tg[0].tr"
		;
connectAttr "l_elbow_rig_ik.ro" "l_elbow_BlendSpace_group_orientConstraint1.tg[0].tro"
		;
connectAttr "l_elbow_rig_ik.pm" "l_elbow_BlendSpace_group_orientConstraint1.tg[0].tpm"
		;
connectAttr "l_elbow_rig_ik.jo" "l_elbow_BlendSpace_group_orientConstraint1.tg[0].tjo"
		;
connectAttr "l_elbow_BlendSpace_group_orientConstraint1.w0" "l_elbow_BlendSpace_group_orientConstraint1.tg[0].tw"
		;
connectAttr "l_elbow_rig_fk.r" "l_elbow_BlendSpace_group_orientConstraint1.tg[1].tr"
		;
connectAttr "l_elbow_rig_fk.ro" "l_elbow_BlendSpace_group_orientConstraint1.tg[1].tro"
		;
connectAttr "l_elbow_rig_fk.pm" "l_elbow_BlendSpace_group_orientConstraint1.tg[1].tpm"
		;
connectAttr "l_elbow_rig_fk.jo" "l_elbow_BlendSpace_group_orientConstraint1.tg[1].tjo"
		;
connectAttr "l_elbow_BlendSpace_group_orientConstraint1.w1" "l_elbow_BlendSpace_group_orientConstraint1.tg[1].tw"
		;
connectAttr "l_elbow_BlendSpace_group.blendSpace" "l_elbow_BlendSpace_group_orientConstraint1.w0"
		;
connectAttr "l_elbow_rig_fk_point_OR_reverse.ox" "l_elbow_BlendSpace_group_orientConstraint1.w1"
		;
connectAttr "r_elbow_BlendSpace_group_pointConstraint1.ctx" "r_elbow_BlendSpace_group.tx"
		;
connectAttr "r_elbow_BlendSpace_group_pointConstraint1.cty" "r_elbow_BlendSpace_group.ty"
		;
connectAttr "r_elbow_BlendSpace_group_pointConstraint1.ctz" "r_elbow_BlendSpace_group.tz"
		;
connectAttr "r_elbow_BlendSpace_group_orientConstraint1.crx" "r_elbow_BlendSpace_group.rx"
		;
connectAttr "r_elbow_BlendSpace_group_orientConstraint1.cry" "r_elbow_BlendSpace_group.ry"
		;
connectAttr "r_elbow_BlendSpace_group_orientConstraint1.crz" "r_elbow_BlendSpace_group.rz"
		;
connectAttr "r_arm_ik_switch.IkFkSwitch" "r_elbow_BlendSpace_group.blendSpace";
connectAttr "r_elbow_BlendSpace_group.pim" "r_elbow_BlendSpace_group_pointConstraint1.cpim"
		;
connectAttr "r_elbow_BlendSpace_group.rp" "r_elbow_BlendSpace_group_pointConstraint1.crp"
		;
connectAttr "r_elbow_BlendSpace_group.rpt" "r_elbow_BlendSpace_group_pointConstraint1.crt"
		;
connectAttr "r_elbow_rig_ik.t" "r_elbow_BlendSpace_group_pointConstraint1.tg[0].tt"
		;
connectAttr "r_elbow_rig_ik.rp" "r_elbow_BlendSpace_group_pointConstraint1.tg[0].trp"
		;
connectAttr "r_elbow_rig_ik.rpt" "r_elbow_BlendSpace_group_pointConstraint1.tg[0].trt"
		;
connectAttr "r_elbow_rig_ik.pm" "r_elbow_BlendSpace_group_pointConstraint1.tg[0].tpm"
		;
connectAttr "r_elbow_BlendSpace_group_pointConstraint1.w0" "r_elbow_BlendSpace_group_pointConstraint1.tg[0].tw"
		;
connectAttr "r_elbow_rig_fk.t" "r_elbow_BlendSpace_group_pointConstraint1.tg[1].tt"
		;
connectAttr "r_elbow_rig_fk.rp" "r_elbow_BlendSpace_group_pointConstraint1.tg[1].trp"
		;
connectAttr "r_elbow_rig_fk.rpt" "r_elbow_BlendSpace_group_pointConstraint1.tg[1].trt"
		;
connectAttr "r_elbow_rig_fk.pm" "r_elbow_BlendSpace_group_pointConstraint1.tg[1].tpm"
		;
connectAttr "r_elbow_BlendSpace_group_pointConstraint1.w1" "r_elbow_BlendSpace_group_pointConstraint1.tg[1].tw"
		;
connectAttr "r_elbow_BlendSpace_group.blendSpace" "r_elbow_BlendSpace_group_pointConstraint1.w0"
		;
connectAttr "r_elbow_rig_fk_point_reverse.ox" "r_elbow_BlendSpace_group_pointConstraint1.w1"
		;
connectAttr "r_elbow_BlendSpace_group.ro" "r_elbow_BlendSpace_group_orientConstraint1.cro"
		;
connectAttr "r_elbow_BlendSpace_group.pim" "r_elbow_BlendSpace_group_orientConstraint1.cpim"
		;
connectAttr "r_elbow_rig_ik.r" "r_elbow_BlendSpace_group_orientConstraint1.tg[0].tr"
		;
connectAttr "r_elbow_rig_ik.ro" "r_elbow_BlendSpace_group_orientConstraint1.tg[0].tro"
		;
connectAttr "r_elbow_rig_ik.pm" "r_elbow_BlendSpace_group_orientConstraint1.tg[0].tpm"
		;
connectAttr "r_elbow_rig_ik.jo" "r_elbow_BlendSpace_group_orientConstraint1.tg[0].tjo"
		;
connectAttr "r_elbow_BlendSpace_group_orientConstraint1.w0" "r_elbow_BlendSpace_group_orientConstraint1.tg[0].tw"
		;
connectAttr "r_elbow_rig_fk.r" "r_elbow_BlendSpace_group_orientConstraint1.tg[1].tr"
		;
connectAttr "r_elbow_rig_fk.ro" "r_elbow_BlendSpace_group_orientConstraint1.tg[1].tro"
		;
connectAttr "r_elbow_rig_fk.pm" "r_elbow_BlendSpace_group_orientConstraint1.tg[1].tpm"
		;
connectAttr "r_elbow_rig_fk.jo" "r_elbow_BlendSpace_group_orientConstraint1.tg[1].tjo"
		;
connectAttr "r_elbow_BlendSpace_group_orientConstraint1.w1" "r_elbow_BlendSpace_group_orientConstraint1.tg[1].tw"
		;
connectAttr "r_elbow_BlendSpace_group.blendSpace" "r_elbow_BlendSpace_group_orientConstraint1.w0"
		;
connectAttr "r_elbow_rig_fk_point_OR_reverse.ox" "r_elbow_BlendSpace_group_orientConstraint1.w1"
		;
connectAttr "l_armLocUp_pointConstraint1.ctx" "l_armLocUp.tx";
connectAttr "l_armLocUp_pointConstraint1.cty" "l_armLocUp.ty";
connectAttr "l_armLocUp_pointConstraint1.ctz" "l_armLocUp.tz";
connectAttr "l_armLocUp.pim" "l_armLocUp_pointConstraint1.cpim";
connectAttr "l_armLocUp.rp" "l_armLocUp_pointConstraint1.crp";
connectAttr "l_armLocUp.rpt" "l_armLocUp_pointConstraint1.crt";
connectAttr "l_shoulder_rig_ik.t" "l_armLocUp_pointConstraint1.tg[0].tt";
connectAttr "l_shoulder_rig_ik.rp" "l_armLocUp_pointConstraint1.tg[0].trp";
connectAttr "l_shoulder_rig_ik.rpt" "l_armLocUp_pointConstraint1.tg[0].trt";
connectAttr "l_shoulder_rig_ik.pm" "l_armLocUp_pointConstraint1.tg[0].tpm";
connectAttr "l_armLocUp_pointConstraint1.w0" "l_armLocUp_pointConstraint1.tg[0].tw"
		;
connectAttr "r_armLocUp_pointConstraint1.ctx" "r_armLocUp.tx";
connectAttr "r_armLocUp_pointConstraint1.cty" "r_armLocUp.ty";
connectAttr "r_armLocUp_pointConstraint1.ctz" "r_armLocUp.tz";
connectAttr "r_armLocUp.pim" "r_armLocUp_pointConstraint1.cpim";
connectAttr "r_armLocUp.rp" "r_armLocUp_pointConstraint1.crp";
connectAttr "r_armLocUp.rpt" "r_armLocUp_pointConstraint1.crt";
connectAttr "r_shoulder_rig_ik.t" "r_armLocUp_pointConstraint1.tg[0].tt";
connectAttr "r_shoulder_rig_ik.rp" "r_armLocUp_pointConstraint1.tg[0].trp";
connectAttr "r_shoulder_rig_ik.rpt" "r_armLocUp_pointConstraint1.tg[0].trt";
connectAttr "r_shoulder_rig_ik.pm" "r_armLocUp_pointConstraint1.tg[0].tpm";
connectAttr "r_armLocUp_pointConstraint1.w0" "r_armLocUp_pointConstraint1.tg[0].tw"
		;
connectAttr "l_legLocUp_pointConstraint1.ctx" "l_legLocUp.tx";
connectAttr "l_legLocUp_pointConstraint1.cty" "l_legLocUp.ty";
connectAttr "l_legLocUp_pointConstraint1.ctz" "l_legLocUp.tz";
connectAttr "l_legLocUp.pim" "l_legLocUp_pointConstraint1.cpim";
connectAttr "l_legLocUp.rp" "l_legLocUp_pointConstraint1.crp";
connectAttr "l_legLocUp.rpt" "l_legLocUp_pointConstraint1.crt";
connectAttr "l_hip_rig_ik.t" "l_legLocUp_pointConstraint1.tg[0].tt";
connectAttr "l_hip_rig_ik.rp" "l_legLocUp_pointConstraint1.tg[0].trp";
connectAttr "l_hip_rig_ik.rpt" "l_legLocUp_pointConstraint1.tg[0].trt";
connectAttr "l_hip_rig_ik.pm" "l_legLocUp_pointConstraint1.tg[0].tpm";
connectAttr "l_legLocUp_pointConstraint1.w0" "l_legLocUp_pointConstraint1.tg[0].tw"
		;
connectAttr "r_legLocUp_pointConstraint1.ctx" "r_legLocUp.tx";
connectAttr "r_legLocUp_pointConstraint1.cty" "r_legLocUp.ty";
connectAttr "r_legLocUp_pointConstraint1.ctz" "r_legLocUp.tz";
connectAttr "r_legLocUp.pim" "r_legLocUp_pointConstraint1.cpim";
connectAttr "r_legLocUp.rp" "r_legLocUp_pointConstraint1.crp";
connectAttr "r_legLocUp.rpt" "r_legLocUp_pointConstraint1.crt";
connectAttr "r_hip_rig_ik.t" "r_legLocUp_pointConstraint1.tg[0].tt";
connectAttr "r_hip_rig_ik.rp" "r_legLocUp_pointConstraint1.tg[0].trp";
connectAttr "r_hip_rig_ik.rpt" "r_legLocUp_pointConstraint1.tg[0].trt";
connectAttr "r_hip_rig_ik.pm" "r_legLocUp_pointConstraint1.tg[0].tpm";
connectAttr "r_legLocUp_pointConstraint1.w0" "r_legLocUp_pointConstraint1.tg[0].tw"
		;
connectAttr "spine_01_rig.msg" "ikSplineSpine.hsj";
connectAttr "effector1.hp" "ikSplineSpine.hee";
connectAttr "ikSplineSolver.msg" "ikSplineSpine.hsv";
connectAttr "spineCurve_ikShape.ws" "ikSplineSpine.ic";
connectAttr "njc_bottom_twistCtrl.xm" "ikSplineSpine.dwum";
connectAttr "njc_top_twistCtrl.xm" "ikSplineSpine.dwue";
connectAttr "cluster6.og[0]" "spineCurve_ikShape.cr";
connectAttr "tweak1.pl[0].cp[0]" "spineCurve_ikShape.twl";
connectAttr "cluster1GroupId.id" "spineCurve_ikShape.iog.og[0].gid";
connectAttr "cluster1Set.mwc" "spineCurve_ikShape.iog.og[0].gco";
connectAttr "groupId2.id" "spineCurve_ikShape.iog.og[1].gid";
connectAttr "tweakSet1.mwc" "spineCurve_ikShape.iog.og[1].gco";
connectAttr "cluster2GroupId.id" "spineCurve_ikShape.iog.og[2].gid";
connectAttr "cluster2Set.mwc" "spineCurve_ikShape.iog.og[2].gco";
connectAttr "cluster3GroupId.id" "spineCurve_ikShape.iog.og[3].gid";
connectAttr "cluster3Set.mwc" "spineCurve_ikShape.iog.og[3].gco";
connectAttr "cluster4GroupId.id" "spineCurve_ikShape.iog.og[4].gid";
connectAttr "cluster4Set.mwc" "spineCurve_ikShape.iog.og[4].gco";
connectAttr "cluster5GroupId.id" "spineCurve_ikShape.iog.og[5].gid";
connectAttr "cluster5Set.mwc" "spineCurve_ikShape.iog.og[5].gco";
connectAttr "cluster6GroupId.id" "spineCurve_ikShape.iog.og[6].gid";
connectAttr "cluster6Set.mwc" "spineCurve_ikShape.iog.og[6].gco";
connectAttr "spineCurve_ik_CV_1_point_X_pointConstraint1.ctx" "spineCurve_ik_CV_1_point_X.tx"
		;
connectAttr "spineCurve_ik_CV_1_point_Y_pointConstraint1.cty" "spineCurve_ik_CV_1_point_Y.ty"
		;
connectAttr "spineCurve_ik_CV_1_point_Z_pointConstraint1.ctz" "spineCurve_ik_CV_1_point_Z.tz"
		;
connectAttr "spineCurve_ik_CV_1_point_Z.pim" "spineCurve_ik_CV_1_point_Z_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_1_point_Z.rp" "spineCurve_ik_CV_1_point_Z_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_1_point_Z.rpt" "spineCurve_ik_CV_1_point_Z_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_Z.t" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_Z.rp" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_Z.rpt" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_Z.pm" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_1_point_Z_pointConstraint1.w0" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Z.t" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Z.rp" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Z.rpt" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Z.pm" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_1_point_Z_pointConstraint1.w1" "spineCurve_ik_CV_1_point_Z_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_1_point_Z.blendSpace" "spineCurve_ik_CV_1_point_Z_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Z_point_reverse.ox" "spineCurve_ik_CV_1_point_Z_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_1_point_Y.pim" "spineCurve_ik_CV_1_point_Y_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_1_point_Y.rp" "spineCurve_ik_CV_1_point_Y_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_1_point_Y.rpt" "spineCurve_ik_CV_1_point_Y_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_Y.t" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_Y.rp" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_Y.rpt" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_Y.pm" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_1_point_Y_pointConstraint1.w0" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Y.t" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Y.rp" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Y.rpt" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Y.pm" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_1_point_Y_pointConstraint1.w1" "spineCurve_ik_CV_1_point_Y_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_1_point_Y.blendSpace" "spineCurve_ik_CV_1_point_Y_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_Y_point_reverse.ox" "spineCurve_ik_CV_1_point_Y_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_1_point_X.pim" "spineCurve_ik_CV_1_point_X_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_1_point_X.rp" "spineCurve_ik_CV_1_point_X_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_1_point_X.rpt" "spineCurve_ik_CV_1_point_X_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_X.t" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_X.rp" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_X.rpt" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_1_L_point_blend_X.pm" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_1_point_X_pointConstraint1.w0" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_X.t" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_X.rp" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_X.rpt" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_X.pm" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_1_point_X_pointConstraint1.w1" "spineCurve_ik_CV_1_point_X_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_1_point_X.blendSpace" "spineCurve_ik_CV_1_point_X_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_1_point_blend_X_point_reverse.ox" "spineCurve_ik_CV_1_point_X_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_2_point_X_pointConstraint1.ctx" "spineCurve_ik_CV_2_point_X.tx"
		;
connectAttr "spineCurve_ik_CV_2_point_Y_pointConstraint1.cty" "spineCurve_ik_CV_2_point_Y.ty"
		;
connectAttr "spineCurve_ik_CV_2_point_Z_pointConstraint1.ctz" "spineCurve_ik_CV_2_point_Z.tz"
		;
connectAttr "spineCurve_ik_CV_2_point_Z.pim" "spineCurve_ik_CV_2_point_Z_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_2_point_Z.rp" "spineCurve_ik_CV_2_point_Z_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_2_point_Z.rpt" "spineCurve_ik_CV_2_point_Z_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_Z.t" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_Z.rp" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_Z.rpt" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_Z.pm" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_2_point_Z_pointConstraint1.w0" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Z.t" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Z.rp" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Z.rpt" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Z.pm" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_2_point_Z_pointConstraint1.w1" "spineCurve_ik_CV_2_point_Z_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_2_point_Z.blendSpace" "spineCurve_ik_CV_2_point_Z_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Z_point_reverse.ox" "spineCurve_ik_CV_2_point_Z_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_2_point_Y.pim" "spineCurve_ik_CV_2_point_Y_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_2_point_Y.rp" "spineCurve_ik_CV_2_point_Y_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_2_point_Y.rpt" "spineCurve_ik_CV_2_point_Y_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_Y.t" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_Y.rp" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_Y.rpt" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_Y.pm" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_2_point_Y_pointConstraint1.w0" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Y.t" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Y.rp" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Y.rpt" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Y.pm" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_2_point_Y_pointConstraint1.w1" "spineCurve_ik_CV_2_point_Y_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_2_point_Y.blendSpace" "spineCurve_ik_CV_2_point_Y_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_Y_point_reverse.ox" "spineCurve_ik_CV_2_point_Y_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_2_point_X.pim" "spineCurve_ik_CV_2_point_X_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_2_point_X.rp" "spineCurve_ik_CV_2_point_X_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_2_point_X.rpt" "spineCurve_ik_CV_2_point_X_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_X.t" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_X.rp" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_X.rpt" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_2_L_point_blend_X.pm" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_2_point_X_pointConstraint1.w0" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_X.t" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_X.rp" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_X.rpt" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_X.pm" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_2_point_X_pointConstraint1.w1" "spineCurve_ik_CV_2_point_X_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_2_point_X.blendSpace" "spineCurve_ik_CV_2_point_X_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_2_point_blend_X_point_reverse.ox" "spineCurve_ik_CV_2_point_X_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_3_point_X_pointConstraint1.ctx" "spineCurve_ik_CV_3_point_X.tx"
		;
connectAttr "spineCurve_ik_CV_3_point_Y_pointConstraint1.cty" "spineCurve_ik_CV_3_point_Y.ty"
		;
connectAttr "spineCurve_ik_CV_3_point_Z_pointConstraint1.ctz" "spineCurve_ik_CV_3_point_Z.tz"
		;
connectAttr "spineCurve_ik_CV_3_point_Z.pim" "spineCurve_ik_CV_3_point_Z_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_3_point_Z.rp" "spineCurve_ik_CV_3_point_Z_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_3_point_Z.rpt" "spineCurve_ik_CV_3_point_Z_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_Z.t" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_Z.rp" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_Z.rpt" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_Z.pm" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_Z_pointConstraint1.w0" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Z.t" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Z.rp" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Z.rpt" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Z.pm" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_Z_pointConstraint1.w1" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[1].tw"
		;
connectAttr "mid_ik_ctrl.t" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[2].tt"
		;
connectAttr "mid_ik_ctrl.rp" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[2].trp"
		;
connectAttr "mid_ik_ctrl.rpt" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[2].trt"
		;
connectAttr "mid_ik_ctrl.pm" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[2].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_Z_pointConstraint1.w2" "spineCurve_ik_CV_3_point_Z_pointConstraint1.tg[2].tw"
		;
connectAttr "spineCurve_ik_CV_3_point_Z.blendSpace" "spineCurve_ik_CV_3_point_Z_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Z_point_reverse.ox" "spineCurve_ik_CV_3_point_Z_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_3_point_Y.pim" "spineCurve_ik_CV_3_point_Y_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_3_point_Y.rp" "spineCurve_ik_CV_3_point_Y_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_3_point_Y.rpt" "spineCurve_ik_CV_3_point_Y_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_Y.t" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_Y.rp" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_Y.rpt" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_Y.pm" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_Y_pointConstraint1.w0" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Y.t" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Y.rp" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Y.rpt" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Y.pm" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_Y_pointConstraint1.w1" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[1].tw"
		;
connectAttr "mid_ik_ctrl.t" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[2].tt"
		;
connectAttr "mid_ik_ctrl.rp" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[2].trp"
		;
connectAttr "mid_ik_ctrl.rpt" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[2].trt"
		;
connectAttr "mid_ik_ctrl.pm" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[2].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_Y_pointConstraint1.w2" "spineCurve_ik_CV_3_point_Y_pointConstraint1.tg[2].tw"
		;
connectAttr "spineCurve_ik_CV_3_point_Y.blendSpace" "spineCurve_ik_CV_3_point_Y_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_Y_point_reverse.ox" "spineCurve_ik_CV_3_point_Y_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_3_point_X.pim" "spineCurve_ik_CV_3_point_X_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_3_point_X.rp" "spineCurve_ik_CV_3_point_X_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_3_point_X.rpt" "spineCurve_ik_CV_3_point_X_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_X.t" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_X.rp" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_X.rpt" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_3_L_point_blend_X.pm" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_X_pointConstraint1.w0" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_X.t" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_X.rp" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_X.rpt" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_X.pm" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_X_pointConstraint1.w1" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[1].tw"
		;
connectAttr "mid_ik_ctrl.t" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[2].tt"
		;
connectAttr "mid_ik_ctrl.rp" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[2].trp"
		;
connectAttr "mid_ik_ctrl.rpt" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[2].trt"
		;
connectAttr "mid_ik_ctrl.pm" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[2].tpm"
		;
connectAttr "spineCurve_ik_CV_3_point_X_pointConstraint1.w2" "spineCurve_ik_CV_3_point_X_pointConstraint1.tg[2].tw"
		;
connectAttr "spineCurve_ik_CV_3_point_X.blendSpace" "spineCurve_ik_CV_3_point_X_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_3_point_blend_X_point_reverse.ox" "spineCurve_ik_CV_3_point_X_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_4_point_X_pointConstraint1.ctx" "spineCurve_ik_CV_4_point_X.tx"
		;
connectAttr "spineCurve_ik_CV_4_point_Y_pointConstraint1.cty" "spineCurve_ik_CV_4_point_Y.ty"
		;
connectAttr "spineCurve_ik_CV_4_point_Z_pointConstraint1.ctz" "spineCurve_ik_CV_4_point_Z.tz"
		;
connectAttr "spineCurve_ik_CV_4_point_Z.pim" "spineCurve_ik_CV_4_point_Z_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_4_point_Z.rp" "spineCurve_ik_CV_4_point_Z_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_4_point_Z.rpt" "spineCurve_ik_CV_4_point_Z_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_Z.t" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_Z.rp" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_Z.rpt" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_Z.pm" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_4_point_Z_pointConstraint1.w0" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Z.t" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Z.rp" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Z.rpt" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Z.pm" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_4_point_Z_pointConstraint1.w1" "spineCurve_ik_CV_4_point_Z_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_4_point_Z.blendSpace" "spineCurve_ik_CV_4_point_Z_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Z_point_reverse.ox" "spineCurve_ik_CV_4_point_Z_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_4_point_Y.pim" "spineCurve_ik_CV_4_point_Y_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_4_point_Y.rp" "spineCurve_ik_CV_4_point_Y_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_4_point_Y.rpt" "spineCurve_ik_CV_4_point_Y_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_Y.t" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_Y.rp" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_Y.rpt" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_Y.pm" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_4_point_Y_pointConstraint1.w0" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Y.t" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Y.rp" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Y.rpt" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Y.pm" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_4_point_Y_pointConstraint1.w1" "spineCurve_ik_CV_4_point_Y_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_4_point_Y.blendSpace" "spineCurve_ik_CV_4_point_Y_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_Y_point_reverse.ox" "spineCurve_ik_CV_4_point_Y_pointConstraint1.w1"
		;
connectAttr "spineCurve_ik_CV_4_point_X.pim" "spineCurve_ik_CV_4_point_X_pointConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_4_point_X.rp" "spineCurve_ik_CV_4_point_X_pointConstraint1.crp"
		;
connectAttr "spineCurve_ik_CV_4_point_X.rpt" "spineCurve_ik_CV_4_point_X_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_X.t" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_X.rp" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_X.rpt" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_4_L_point_blend_X.pm" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[0].tpm"
		;
connectAttr "spineCurve_ik_CV_4_point_X_pointConstraint1.w0" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[0].tw"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_X.t" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[1].tt"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_X.rp" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[1].trp"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_X.rpt" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[1].trt"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_X.pm" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[1].tpm"
		;
connectAttr "spineCurve_ik_CV_4_point_X_pointConstraint1.w1" "spineCurve_ik_CV_4_point_X_pointConstraint1.tg[1].tw"
		;
connectAttr "spineCurve_ik_CV_4_point_X.blendSpace" "spineCurve_ik_CV_4_point_X_pointConstraint1.w0"
		;
connectAttr "spineCurve_ik_CV_4_point_blend_X_point_reverse.ox" "spineCurve_ik_CV_4_point_X_pointConstraint1.w1"
		;
connectAttr "njc_top_twistCtrl_orientConstraint1.crx" "njc_top_twistCtrl.rx";
connectAttr "njc_top_twistCtrl_orientConstraint1.cry" "njc_top_twistCtrl.ry";
connectAttr "njc_top_twistCtrl_orientConstraint1.crz" "njc_top_twistCtrl.rz";
connectAttr "njc_top_twistCtrl_pointConstraint1.ctx" "njc_top_twistCtrl.tx";
connectAttr "njc_top_twistCtrl_pointConstraint1.cty" "njc_top_twistCtrl.ty";
connectAttr "njc_top_twistCtrl_pointConstraint1.ctz" "njc_top_twistCtrl.tz";
connectAttr "njc_top_twistCtrl.ro" "njc_top_twistCtrl_orientConstraint1.cro";
connectAttr "njc_top_twistCtrl.pim" "njc_top_twistCtrl_orientConstraint1.cpim";
connectAttr "spineCurve_ik_CV_5.r" "njc_top_twistCtrl_orientConstraint1.tg[0].tr"
		;
connectAttr "spineCurve_ik_CV_5.ro" "njc_top_twistCtrl_orientConstraint1.tg[0].tro"
		;
connectAttr "spineCurve_ik_CV_5.pm" "njc_top_twistCtrl_orientConstraint1.tg[0].tpm"
		;
connectAttr "njc_top_twistCtrl_orientConstraint1.w0" "njc_top_twistCtrl_orientConstraint1.tg[0].tw"
		;
connectAttr "njc_top_twistCtrl.pim" "njc_top_twistCtrl_pointConstraint1.cpim";
connectAttr "njc_top_twistCtrl.rp" "njc_top_twistCtrl_pointConstraint1.crp";
connectAttr "njc_top_twistCtrl.rpt" "njc_top_twistCtrl_pointConstraint1.crt";
connectAttr "spineCurve_ik_CV_5.t" "njc_top_twistCtrl_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_5.rp" "njc_top_twistCtrl_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_5.rpt" "njc_top_twistCtrl_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_5.pm" "njc_top_twistCtrl_pointConstraint1.tg[0].tpm"
		;
connectAttr "njc_top_twistCtrl_pointConstraint1.w0" "njc_top_twistCtrl_pointConstraint1.tg[0].tw"
		;
connectAttr "njc_bottom_twistCtrl_orientConstraint1.crx" "njc_bottom_twistCtrl.rx"
		;
connectAttr "njc_bottom_twistCtrl_orientConstraint1.cry" "njc_bottom_twistCtrl.ry"
		;
connectAttr "njc_bottom_twistCtrl_orientConstraint1.crz" "njc_bottom_twistCtrl.rz"
		;
connectAttr "njc_bottom_twistCtrl_pointConstraint1.ctx" "njc_bottom_twistCtrl.tx"
		;
connectAttr "njc_bottom_twistCtrl_pointConstraint1.cty" "njc_bottom_twistCtrl.ty"
		;
connectAttr "njc_bottom_twistCtrl_pointConstraint1.ctz" "njc_bottom_twistCtrl.tz"
		;
connectAttr "njc_bottom_twistCtrl.ro" "njc_bottom_twistCtrl_orientConstraint1.cro"
		;
connectAttr "njc_bottom_twistCtrl.pim" "njc_bottom_twistCtrl_orientConstraint1.cpim"
		;
connectAttr "spineCurve_ik_CV_0.r" "njc_bottom_twistCtrl_orientConstraint1.tg[0].tr"
		;
connectAttr "spineCurve_ik_CV_0.ro" "njc_bottom_twistCtrl_orientConstraint1.tg[0].tro"
		;
connectAttr "spineCurve_ik_CV_0.pm" "njc_bottom_twistCtrl_orientConstraint1.tg[0].tpm"
		;
connectAttr "njc_bottom_twistCtrl_orientConstraint1.w0" "njc_bottom_twistCtrl_orientConstraint1.tg[0].tw"
		;
connectAttr "njc_bottom_twistCtrl.pim" "njc_bottom_twistCtrl_pointConstraint1.cpim"
		;
connectAttr "njc_bottom_twistCtrl.rp" "njc_bottom_twistCtrl_pointConstraint1.crp"
		;
connectAttr "njc_bottom_twistCtrl.rpt" "njc_bottom_twistCtrl_pointConstraint1.crt"
		;
connectAttr "spineCurve_ik_CV_0.t" "njc_bottom_twistCtrl_pointConstraint1.tg[0].tt"
		;
connectAttr "spineCurve_ik_CV_0.rp" "njc_bottom_twistCtrl_pointConstraint1.tg[0].trp"
		;
connectAttr "spineCurve_ik_CV_0.rpt" "njc_bottom_twistCtrl_pointConstraint1.tg[0].trt"
		;
connectAttr "spineCurve_ik_CV_0.pm" "njc_bottom_twistCtrl_pointConstraint1.tg[0].tpm"
		;
connectAttr "njc_bottom_twistCtrl_pointConstraint1.w0" "njc_bottom_twistCtrl_pointConstraint1.tg[0].tw"
		;
connectAttr "pelvis_rig_pointConstraint1.ctx" "pelvis_rig.tx";
connectAttr "pelvis_rig_pointConstraint1.cty" "pelvis_rig.ty";
connectAttr "pelvis_rig_pointConstraint1.ctz" "pelvis_rig.tz";
connectAttr "pelvis_rig_orientConstraint1.crx" "pelvis_rig.rx";
connectAttr "pelvis_rig_orientConstraint1.cry" "pelvis_rig.ry";
connectAttr "pelvis_rig_orientConstraint1.crz" "pelvis_rig.rz";
connectAttr "pelvis_rig.s" "spine_01_rig.is";
connectAttr "expression5.out[0]" "spine_01_rig.sy";
connectAttr "expression5.out[1]" "spine_01_rig.sz";
connectAttr "expression1.out[0]" "spine_01_rig.tx";
connectAttr "spine_01_rig.s" "spine_02_rig.is";
connectAttr "expression6.out[0]" "spine_02_rig.sy";
connectAttr "expression6.out[1]" "spine_02_rig.sz";
connectAttr "expression2.out[0]" "spine_02_rig.tx";
connectAttr "spine_02_rig.s" "spine_03_rig.is";
connectAttr "expression7.out[0]" "spine_03_rig.sy";
connectAttr "expression7.out[1]" "spine_03_rig.sz";
connectAttr "expression3.out[0]" "spine_03_rig.tx";
connectAttr "spine_03_rig.s" "spineEnd_rig.is";
connectAttr "expression4.out[0]" "spineEnd_rig.tx";
connectAttr "spineEnd_rig.tx" "effector1.tx";
connectAttr "spineEnd_rig.ty" "effector1.ty";
connectAttr "spineEnd_rig.tz" "effector1.tz";
connectAttr "hips_rig_pointConstraint1.ctx" "hips_rig.tx";
connectAttr "hips_rig_pointConstraint1.cty" "hips_rig.ty";
connectAttr "hips_rig_pointConstraint1.ctz" "hips_rig.tz";
connectAttr "hips_rig_orientConstraint1.crx" "hips_rig.rx";
connectAttr "hips_rig_orientConstraint1.cry" "hips_rig.ry";
connectAttr "hips_rig_orientConstraint1.crz" "hips_rig.rz";
connectAttr "pelvis_rig.s" "hips_rig.is";
connectAttr "l_hip_rig_orientConstraint1.crx" "l_hip_rig.rx";
connectAttr "l_hip_rig_orientConstraint1.cry" "l_hip_rig.ry";
connectAttr "l_hip_rig_orientConstraint1.crz" "l_hip_rig.rz";
connectAttr "hips_rig.s" "l_hip_rig.is";
connectAttr "l_hip_rig_pointConstraint1.ctx" "l_hip_rig.tx";
connectAttr "l_hip_rig_pointConstraint1.cty" "l_hip_rig.ty";
connectAttr "l_hip_rig_pointConstraint1.ctz" "l_hip_rig.tz";
connectAttr "l_leg_ik_switch.IkFkSwitch" "l_hip_rig.blendSpace";
connectAttr "l_knee_rig_orientConstraint1.crx" "l_knee_rig.rx";
connectAttr "l_knee_rig_orientConstraint1.cry" "l_knee_rig.ry";
connectAttr "l_knee_rig_orientConstraint1.crz" "l_knee_rig.rz";
connectAttr "l_hip_rig.s" "l_knee_rig.is";
connectAttr "l_knee_rig_pointConstraint1.ctx" "l_knee_rig.tx";
connectAttr "l_knee_rig_pointConstraint1.cty" "l_knee_rig.ty";
connectAttr "l_knee_rig_pointConstraint1.ctz" "l_knee_rig.tz";
connectAttr "l_leg_ik_switch.IkFkSwitch" "l_knee_rig.blendSpace";
connectAttr "l_ankle_rig_orientConstraint1.crx" "l_ankle_rig.rx";
connectAttr "l_ankle_rig_orientConstraint1.cry" "l_ankle_rig.ry";
connectAttr "l_ankle_rig_orientConstraint1.crz" "l_ankle_rig.rz";
connectAttr "l_knee_rig.s" "l_ankle_rig.is";
connectAttr "l_ankle_rig_pointConstraint1.ctx" "l_ankle_rig.tx";
connectAttr "l_ankle_rig_pointConstraint1.cty" "l_ankle_rig.ty";
connectAttr "l_ankle_rig_pointConstraint1.ctz" "l_ankle_rig.tz";
connectAttr "l_leg_ik_switch.IkFkSwitch" "l_ankle_rig.blendSpace";
connectAttr "l_ball_rig_orientConstraint1.crx" "l_ball_rig.rx";
connectAttr "l_ball_rig_orientConstraint1.cry" "l_ball_rig.ry";
connectAttr "l_ball_rig_orientConstraint1.crz" "l_ball_rig.rz";
connectAttr "l_ankle_rig.s" "l_ball_rig.is";
connectAttr "l_ball_rig_pointConstraint1.ctx" "l_ball_rig.tx";
connectAttr "l_ball_rig_pointConstraint1.cty" "l_ball_rig.ty";
connectAttr "l_ball_rig_pointConstraint1.ctz" "l_ball_rig.tz";
connectAttr "l_leg_ik_switch.IkFkSwitch" "l_ball_rig.blendSpace";
connectAttr "l_toe_rig_orientConstraint1.crx" "l_toe_rig.rx";
connectAttr "l_toe_rig_orientConstraint1.cry" "l_toe_rig.ry";
connectAttr "l_toe_rig_orientConstraint1.crz" "l_toe_rig.rz";
connectAttr "l_ball_rig.s" "l_toe_rig.is";
connectAttr "l_toe_rig_pointConstraint1.ctx" "l_toe_rig.tx";
connectAttr "l_toe_rig_pointConstraint1.cty" "l_toe_rig.ty";
connectAttr "l_toe_rig_pointConstraint1.ctz" "l_toe_rig.tz";
connectAttr "l_leg_ik_switch.IkFkSwitch" "l_toe_rig.blendSpace";
connectAttr "l_toe_rig.pim" "l_toe_rig_pointConstraint1.cpim";
connectAttr "l_toe_rig.rp" "l_toe_rig_pointConstraint1.crp";
connectAttr "l_toe_rig.rpt" "l_toe_rig_pointConstraint1.crt";
connectAttr "l_toe_rig_ik.t" "l_toe_rig_pointConstraint1.tg[0].tt";
connectAttr "l_toe_rig_ik.rp" "l_toe_rig_pointConstraint1.tg[0].trp";
connectAttr "l_toe_rig_ik.rpt" "l_toe_rig_pointConstraint1.tg[0].trt";
connectAttr "l_toe_rig_ik.pm" "l_toe_rig_pointConstraint1.tg[0].tpm";
connectAttr "l_toe_rig_pointConstraint1.w0" "l_toe_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "l_toe_rig_fk.t" "l_toe_rig_pointConstraint1.tg[1].tt";
connectAttr "l_toe_rig_fk.rp" "l_toe_rig_pointConstraint1.tg[1].trp";
connectAttr "l_toe_rig_fk.rpt" "l_toe_rig_pointConstraint1.tg[1].trt";
connectAttr "l_toe_rig_fk.pm" "l_toe_rig_pointConstraint1.tg[1].tpm";
connectAttr "l_toe_rig_pointConstraint1.w1" "l_toe_rig_pointConstraint1.tg[1].tw"
		;
connectAttr "l_toe_rig.blendSpace" "l_toe_rig_pointConstraint1.w0";
connectAttr "l_toe_rig_fk_point_reverse.ox" "l_toe_rig_pointConstraint1.w1";
connectAttr "l_toe_rig.ro" "l_toe_rig_orientConstraint1.cro";
connectAttr "l_toe_rig.pim" "l_toe_rig_orientConstraint1.cpim";
connectAttr "l_toe_rig.jo" "l_toe_rig_orientConstraint1.cjo";
connectAttr "l_toe_rig.is" "l_toe_rig_orientConstraint1.is";
connectAttr "l_toe_rig_ik.r" "l_toe_rig_orientConstraint1.tg[0].tr";
connectAttr "l_toe_rig_ik.ro" "l_toe_rig_orientConstraint1.tg[0].tro";
connectAttr "l_toe_rig_ik.pm" "l_toe_rig_orientConstraint1.tg[0].tpm";
connectAttr "l_toe_rig_ik.jo" "l_toe_rig_orientConstraint1.tg[0].tjo";
connectAttr "l_toe_rig_orientConstraint1.w0" "l_toe_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "l_toe_rig_fk.r" "l_toe_rig_orientConstraint1.tg[1].tr";
connectAttr "l_toe_rig_fk.ro" "l_toe_rig_orientConstraint1.tg[1].tro";
connectAttr "l_toe_rig_fk.pm" "l_toe_rig_orientConstraint1.tg[1].tpm";
connectAttr "l_toe_rig_fk.jo" "l_toe_rig_orientConstraint1.tg[1].tjo";
connectAttr "l_toe_rig_orientConstraint1.w1" "l_toe_rig_orientConstraint1.tg[1].tw"
		;
connectAttr "l_toe_rig.blendSpace" "l_toe_rig_orientConstraint1.w0";
connectAttr "l_toe_rig_fk_point_OR_reverse.ox" "l_toe_rig_orientConstraint1.w1";
connectAttr "l_ball_rig.pim" "l_ball_rig_pointConstraint1.cpim";
connectAttr "l_ball_rig.rp" "l_ball_rig_pointConstraint1.crp";
connectAttr "l_ball_rig.rpt" "l_ball_rig_pointConstraint1.crt";
connectAttr "l_ball_rig_ik.t" "l_ball_rig_pointConstraint1.tg[0].tt";
connectAttr "l_ball_rig_ik.rp" "l_ball_rig_pointConstraint1.tg[0].trp";
connectAttr "l_ball_rig_ik.rpt" "l_ball_rig_pointConstraint1.tg[0].trt";
connectAttr "l_ball_rig_ik.pm" "l_ball_rig_pointConstraint1.tg[0].tpm";
connectAttr "l_ball_rig_pointConstraint1.w0" "l_ball_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "l_ball_rig_fk.t" "l_ball_rig_pointConstraint1.tg[1].tt";
connectAttr "l_ball_rig_fk.rp" "l_ball_rig_pointConstraint1.tg[1].trp";
connectAttr "l_ball_rig_fk.rpt" "l_ball_rig_pointConstraint1.tg[1].trt";
connectAttr "l_ball_rig_fk.pm" "l_ball_rig_pointConstraint1.tg[1].tpm";
connectAttr "l_ball_rig_pointConstraint1.w1" "l_ball_rig_pointConstraint1.tg[1].tw"
		;
connectAttr "l_ball_rig.blendSpace" "l_ball_rig_pointConstraint1.w0";
connectAttr "l_ball_rig_fk_point_reverse.ox" "l_ball_rig_pointConstraint1.w1";
connectAttr "l_ball_rig.ro" "l_ball_rig_orientConstraint1.cro";
connectAttr "l_ball_rig.pim" "l_ball_rig_orientConstraint1.cpim";
connectAttr "l_ball_rig.jo" "l_ball_rig_orientConstraint1.cjo";
connectAttr "l_ball_rig.is" "l_ball_rig_orientConstraint1.is";
connectAttr "l_ball_rig_ik.r" "l_ball_rig_orientConstraint1.tg[0].tr";
connectAttr "l_ball_rig_ik.ro" "l_ball_rig_orientConstraint1.tg[0].tro";
connectAttr "l_ball_rig_ik.pm" "l_ball_rig_orientConstraint1.tg[0].tpm";
connectAttr "l_ball_rig_ik.jo" "l_ball_rig_orientConstraint1.tg[0].tjo";
connectAttr "l_ball_rig_orientConstraint1.w0" "l_ball_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "l_ball_rig_fk.r" "l_ball_rig_orientConstraint1.tg[1].tr";
connectAttr "l_ball_rig_fk.ro" "l_ball_rig_orientConstraint1.tg[1].tro";
connectAttr "l_ball_rig_fk.pm" "l_ball_rig_orientConstraint1.tg[1].tpm";
connectAttr "l_ball_rig_fk.jo" "l_ball_rig_orientConstraint1.tg[1].tjo";
connectAttr "l_ball_rig_orientConstraint1.w1" "l_ball_rig_orientConstraint1.tg[1].tw"
		;
connectAttr "l_ball_rig.blendSpace" "l_ball_rig_orientConstraint1.w0";
connectAttr "l_ball_rig_fk_point_OR_reverse.ox" "l_ball_rig_orientConstraint1.w1"
		;
connectAttr "l_ankle_rig.pim" "l_ankle_rig_pointConstraint1.cpim";
connectAttr "l_ankle_rig.rp" "l_ankle_rig_pointConstraint1.crp";
connectAttr "l_ankle_rig.rpt" "l_ankle_rig_pointConstraint1.crt";
connectAttr "l_ankle_rig_ik.t" "l_ankle_rig_pointConstraint1.tg[0].tt";
connectAttr "l_ankle_rig_ik.rp" "l_ankle_rig_pointConstraint1.tg[0].trp";
connectAttr "l_ankle_rig_ik.rpt" "l_ankle_rig_pointConstraint1.tg[0].trt";
connectAttr "l_ankle_rig_ik.pm" "l_ankle_rig_pointConstraint1.tg[0].tpm";
connectAttr "l_ankle_rig_pointConstraint1.w0" "l_ankle_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "l_ankle_rig_fk.t" "l_ankle_rig_pointConstraint1.tg[1].tt";
connectAttr "l_ankle_rig_fk.rp" "l_ankle_rig_pointConstraint1.tg[1].trp";
connectAttr "l_ankle_rig_fk.rpt" "l_ankle_rig_pointConstraint1.tg[1].trt";
connectAttr "l_ankle_rig_fk.pm" "l_ankle_rig_pointConstraint1.tg[1].tpm";
connectAttr "l_ankle_rig_pointConstraint1.w1" "l_ankle_rig_pointConstraint1.tg[1].tw"
		;
connectAttr "l_ankle_rig.blendSpace" "l_ankle_rig_pointConstraint1.w0";
connectAttr "l_ankle_rig_fk_point_reverse.ox" "l_ankle_rig_pointConstraint1.w1";
connectAttr "l_ankle_rig.ro" "l_ankle_rig_orientConstraint1.cro";
connectAttr "l_ankle_rig.pim" "l_ankle_rig_orientConstraint1.cpim";
connectAttr "l_ankle_rig.jo" "l_ankle_rig_orientConstraint1.cjo";
connectAttr "l_ankle_rig.is" "l_ankle_rig_orientConstraint1.is";
connectAttr "l_ankle_rig_ik.r" "l_ankle_rig_orientConstraint1.tg[0].tr";
connectAttr "l_ankle_rig_ik.ro" "l_ankle_rig_orientConstraint1.tg[0].tro";
connectAttr "l_ankle_rig_ik.pm" "l_ankle_rig_orientConstraint1.tg[0].tpm";
connectAttr "l_ankle_rig_ik.jo" "l_ankle_rig_orientConstraint1.tg[0].tjo";
connectAttr "l_ankle_rig_orientConstraint1.w0" "l_ankle_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "l_ankle_rig_fk.r" "l_ankle_rig_orientConstraint1.tg[1].tr";
connectAttr "l_ankle_rig_fk.ro" "l_ankle_rig_orientConstraint1.tg[1].tro";
connectAttr "l_ankle_rig_fk.pm" "l_ankle_rig_orientConstraint1.tg[1].tpm";
connectAttr "l_ankle_rig_fk.jo" "l_ankle_rig_orientConstraint1.tg[1].tjo";
connectAttr "l_ankle_rig_orientConstraint1.w1" "l_ankle_rig_orientConstraint1.tg[1].tw"
		;
connectAttr "l_ankle_rig.blendSpace" "l_ankle_rig_orientConstraint1.w0";
connectAttr "l_ankle_rig_fk_point_OR_reverse.ox" "l_ankle_rig_orientConstraint1.w1"
		;
connectAttr "l_knee_rig.pim" "l_knee_rig_pointConstraint1.cpim";
connectAttr "l_knee_rig.rp" "l_knee_rig_pointConstraint1.crp";
connectAttr "l_knee_rig.rpt" "l_knee_rig_pointConstraint1.crt";
connectAttr "l_knee_rig_ik.t" "l_knee_rig_pointConstraint1.tg[0].tt";
connectAttr "l_knee_rig_ik.rp" "l_knee_rig_pointConstraint1.tg[0].trp";
connectAttr "l_knee_rig_ik.rpt" "l_knee_rig_pointConstraint1.tg[0].trt";
connectAttr "l_knee_rig_ik.pm" "l_knee_rig_pointConstraint1.tg[0].tpm";
connectAttr "l_knee_rig_pointConstraint1.w0" "l_knee_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "l_knee_rig_fk.t" "l_knee_rig_pointConstraint1.tg[1].tt";
connectAttr "l_knee_rig_fk.rp" "l_knee_rig_pointConstraint1.tg[1].trp";
connectAttr "l_knee_rig_fk.rpt" "l_knee_rig_pointConstraint1.tg[1].trt";
connectAttr "l_knee_rig_fk.pm" "l_knee_rig_pointConstraint1.tg[1].tpm";
connectAttr "l_knee_rig_pointConstraint1.w1" "l_knee_rig_pointConstraint1.tg[1].tw"
		;
connectAttr "l_knee_rig.blendSpace" "l_knee_rig_pointConstraint1.w0";
connectAttr "l_knee_rig_fk_point_reverse.ox" "l_knee_rig_pointConstraint1.w1";
connectAttr "l_foot_ik_ctrl.xOffset" "l_knee_rig_pointConstraint1.ox";
connectAttr "l_foot_ik_ctrl.xOffset" "l_knee_rig_pointConstraint1.oy";
connectAttr "l_foot_ik_ctrl.zOffset" "l_knee_rig_pointConstraint1.oz";
connectAttr "l_knee_rig.ro" "l_knee_rig_orientConstraint1.cro";
connectAttr "l_knee_rig.pim" "l_knee_rig_orientConstraint1.cpim";
connectAttr "l_knee_rig.jo" "l_knee_rig_orientConstraint1.cjo";
connectAttr "l_knee_rig.is" "l_knee_rig_orientConstraint1.is";
connectAttr "l_knee_rig_ik.r" "l_knee_rig_orientConstraint1.tg[0].tr";
connectAttr "l_knee_rig_ik.ro" "l_knee_rig_orientConstraint1.tg[0].tro";
connectAttr "l_knee_rig_ik.pm" "l_knee_rig_orientConstraint1.tg[0].tpm";
connectAttr "l_knee_rig_ik.jo" "l_knee_rig_orientConstraint1.tg[0].tjo";
connectAttr "l_knee_rig_orientConstraint1.w0" "l_knee_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "l_knee_rig_fk.r" "l_knee_rig_orientConstraint1.tg[1].tr";
connectAttr "l_knee_rig_fk.ro" "l_knee_rig_orientConstraint1.tg[1].tro";
connectAttr "l_knee_rig_fk.pm" "l_knee_rig_orientConstraint1.tg[1].tpm";
connectAttr "l_knee_rig_fk.jo" "l_knee_rig_orientConstraint1.tg[1].tjo";
connectAttr "l_knee_rig_orientConstraint1.w1" "l_knee_rig_orientConstraint1.tg[1].tw"
		;
connectAttr "l_knee_rig.blendSpace" "l_knee_rig_orientConstraint1.w0";
connectAttr "l_knee_rig_fk_point_OR_reverse.ox" "l_knee_rig_orientConstraint1.w1"
		;
connectAttr "l_hip_rig.pim" "l_hip_rig_pointConstraint1.cpim";
connectAttr "l_hip_rig.rp" "l_hip_rig_pointConstraint1.crp";
connectAttr "l_hip_rig.rpt" "l_hip_rig_pointConstraint1.crt";
connectAttr "l_hip_rig_ik.t" "l_hip_rig_pointConstraint1.tg[0].tt";
connectAttr "l_hip_rig_ik.rp" "l_hip_rig_pointConstraint1.tg[0].trp";
connectAttr "l_hip_rig_ik.rpt" "l_hip_rig_pointConstraint1.tg[0].trt";
connectAttr "l_hip_rig_ik.pm" "l_hip_rig_pointConstraint1.tg[0].tpm";
connectAttr "l_hip_rig_pointConstraint1.w0" "l_hip_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "l_hip_rig_fk.t" "l_hip_rig_pointConstraint1.tg[1].tt";
connectAttr "l_hip_rig_fk.rp" "l_hip_rig_pointConstraint1.tg[1].trp";
connectAttr "l_hip_rig_fk.rpt" "l_hip_rig_pointConstraint1.tg[1].trt";
connectAttr "l_hip_rig_fk.pm" "l_hip_rig_pointConstraint1.tg[1].tpm";
connectAttr "l_hip_rig_pointConstraint1.w1" "l_hip_rig_pointConstraint1.tg[1].tw"
		;
connectAttr "l_hip_rig.blendSpace" "l_hip_rig_pointConstraint1.w0";
connectAttr "l_hip_rig_fk_point_reverse.ox" "l_hip_rig_pointConstraint1.w1";
connectAttr "l_hip_rig.ro" "l_hip_rig_orientConstraint1.cro";
connectAttr "l_hip_rig.pim" "l_hip_rig_orientConstraint1.cpim";
connectAttr "l_hip_rig.jo" "l_hip_rig_orientConstraint1.cjo";
connectAttr "l_hip_rig.is" "l_hip_rig_orientConstraint1.is";
connectAttr "l_hip_rig_ik.r" "l_hip_rig_orientConstraint1.tg[0].tr";
connectAttr "l_hip_rig_ik.ro" "l_hip_rig_orientConstraint1.tg[0].tro";
connectAttr "l_hip_rig_ik.pm" "l_hip_rig_orientConstraint1.tg[0].tpm";
connectAttr "l_hip_rig_ik.jo" "l_hip_rig_orientConstraint1.tg[0].tjo";
connectAttr "l_hip_rig_orientConstraint1.w0" "l_hip_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "l_hip_rig_fk.r" "l_hip_rig_orientConstraint1.tg[1].tr";
connectAttr "l_hip_rig_fk.ro" "l_hip_rig_orientConstraint1.tg[1].tro";
connectAttr "l_hip_rig_fk.pm" "l_hip_rig_orientConstraint1.tg[1].tpm";
connectAttr "l_hip_rig_fk.jo" "l_hip_rig_orientConstraint1.tg[1].tjo";
connectAttr "l_hip_rig_orientConstraint1.w1" "l_hip_rig_orientConstraint1.tg[1].tw"
		;
connectAttr "l_hip_rig.blendSpace" "l_hip_rig_orientConstraint1.w0";
connectAttr "l_hip_rig_fk_point_OR_reverse.ox" "l_hip_rig_orientConstraint1.w1";
connectAttr "r_hip_rig_orientConstraint1.crx" "r_hip_rig.rx";
connectAttr "r_hip_rig_orientConstraint1.cry" "r_hip_rig.ry";
connectAttr "r_hip_rig_orientConstraint1.crz" "r_hip_rig.rz";
connectAttr "hips_rig.s" "r_hip_rig.is";
connectAttr "r_hip_rig_pointConstraint1.ctx" "r_hip_rig.tx";
connectAttr "r_hip_rig_pointConstraint1.cty" "r_hip_rig.ty";
connectAttr "r_hip_rig_pointConstraint1.ctz" "r_hip_rig.tz";
connectAttr "r_leg_ik_switch.IkFkSwitch" "r_hip_rig.blendSpace";
connectAttr "r_knee_rig_orientConstraint1.crx" "r_knee_rig.rx";
connectAttr "r_knee_rig_orientConstraint1.cry" "r_knee_rig.ry";
connectAttr "r_knee_rig_orientConstraint1.crz" "r_knee_rig.rz";
connectAttr "r_hip_rig.s" "r_knee_rig.is";
connectAttr "r_knee_rig_pointConstraint1.ctx" "r_knee_rig.tx";
connectAttr "r_knee_rig_pointConstraint1.cty" "r_knee_rig.ty";
connectAttr "r_knee_rig_pointConstraint1.ctz" "r_knee_rig.tz";
connectAttr "r_leg_ik_switch.IkFkSwitch" "r_knee_rig.blendSpace";
connectAttr "r_ankle_rig_orientConstraint1.crx" "r_ankle_rig.rx";
connectAttr "r_ankle_rig_orientConstraint1.cry" "r_ankle_rig.ry";
connectAttr "r_ankle_rig_orientConstraint1.crz" "r_ankle_rig.rz";
connectAttr "r_knee_rig.s" "r_ankle_rig.is";
connectAttr "r_ankle_rig_pointConstraint1.ctx" "r_ankle_rig.tx";
connectAttr "r_ankle_rig_pointConstraint1.cty" "r_ankle_rig.ty";
connectAttr "r_ankle_rig_pointConstraint1.ctz" "r_ankle_rig.tz";
connectAttr "r_leg_ik_switch.IkFkSwitch" "r_ankle_rig.blendSpace";
connectAttr "r_ball_rig_orientConstraint1.crx" "r_ball_rig.rx";
connectAttr "r_ball_rig_orientConstraint1.cry" "r_ball_rig.ry";
connectAttr "r_ball_rig_orientConstraint1.crz" "r_ball_rig.rz";
connectAttr "r_ankle_rig.s" "r_ball_rig.is";
connectAttr "r_ball_rig_pointConstraint1.ctx" "r_ball_rig.tx";
connectAttr "r_ball_rig_pointConstraint1.cty" "r_ball_rig.ty";
connectAttr "r_ball_rig_pointConstraint1.ctz" "r_ball_rig.tz";
connectAttr "r_leg_ik_switch.IkFkSwitch" "r_ball_rig.blendSpace";
connectAttr "r_toe_rig_orientConstraint1.crx" "r_toe_rig.rx";
connectAttr "r_toe_rig_orientConstraint1.cry" "r_toe_rig.ry";
connectAttr "r_toe_rig_orientConstraint1.crz" "r_toe_rig.rz";
connectAttr "r_ball_rig.s" "r_toe_rig.is";
connectAttr "r_toe_rig_pointConstraint1.ctx" "r_toe_rig.tx";
connectAttr "r_toe_rig_pointConstraint1.cty" "r_toe_rig.ty";
connectAttr "r_toe_rig_pointConstraint1.ctz" "r_toe_rig.tz";
connectAttr "r_leg_ik_switch.IkFkSwitch" "r_toe_rig.blendSpace";
connectAttr "r_toe_rig.pim" "r_toe_rig_pointConstraint1.cpim";
connectAttr "r_toe_rig.rp" "r_toe_rig_pointConstraint1.crp";
connectAttr "r_toe_rig.rpt" "r_toe_rig_pointConstraint1.crt";
connectAttr "r_toe_rig_ik.t" "r_toe_rig_pointConstraint1.tg[0].tt";
connectAttr "r_toe_rig_ik.rp" "r_toe_rig_pointConstraint1.tg[0].trp";
connectAttr "r_toe_rig_ik.rpt" "r_toe_rig_pointConstraint1.tg[0].trt";
connectAttr "r_toe_rig_ik.pm" "r_toe_rig_pointConstraint1.tg[0].tpm";
connectAttr "r_toe_rig_pointConstraint1.w0" "r_toe_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "r_toe_rig_fk.t" "r_toe_rig_pointConstraint1.tg[1].tt";
connectAttr "r_toe_rig_fk.rp" "r_toe_rig_pointConstraint1.tg[1].trp";
connectAttr "r_toe_rig_fk.rpt" "r_toe_rig_pointConstraint1.tg[1].trt";
connectAttr "r_toe_rig_fk.pm" "r_toe_rig_pointConstraint1.tg[1].tpm";
connectAttr "r_toe_rig_pointConstraint1.w1" "r_toe_rig_pointConstraint1.tg[1].tw"
		;
connectAttr "r_toe_rig.blendSpace" "r_toe_rig_pointConstraint1.w0";
connectAttr "r_toe_rig_fk_point_reverse.ox" "r_toe_rig_pointConstraint1.w1";
connectAttr "r_toe_rig.ro" "r_toe_rig_orientConstraint1.cro";
connectAttr "r_toe_rig.pim" "r_toe_rig_orientConstraint1.cpim";
connectAttr "r_toe_rig.jo" "r_toe_rig_orientConstraint1.cjo";
connectAttr "r_toe_rig.is" "r_toe_rig_orientConstraint1.is";
connectAttr "r_toe_rig_ik.r" "r_toe_rig_orientConstraint1.tg[0].tr";
connectAttr "r_toe_rig_ik.ro" "r_toe_rig_orientConstraint1.tg[0].tro";
connectAttr "r_toe_rig_ik.pm" "r_toe_rig_orientConstraint1.tg[0].tpm";
connectAttr "r_toe_rig_ik.jo" "r_toe_rig_orientConstraint1.tg[0].tjo";
connectAttr "r_toe_rig_orientConstraint1.w0" "r_toe_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "r_toe_rig_fk.r" "r_toe_rig_orientConstraint1.tg[1].tr";
connectAttr "r_toe_rig_fk.ro" "r_toe_rig_orientConstraint1.tg[1].tro";
connectAttr "r_toe_rig_fk.pm" "r_toe_rig_orientConstraint1.tg[1].tpm";
connectAttr "r_toe_rig_fk.jo" "r_toe_rig_orientConstraint1.tg[1].tjo";
connectAttr "r_toe_rig_orientConstraint1.w1" "r_toe_rig_orientConstraint1.tg[1].tw"
		;
connectAttr "r_toe_rig.blendSpace" "r_toe_rig_orientConstraint1.w0";
connectAttr "r_toe_rig_fk_point_OR_reverse.ox" "r_toe_rig_orientConstraint1.w1";
connectAttr "r_ball_rig.pim" "r_ball_rig_pointConstraint1.cpim";
connectAttr "r_ball_rig.rp" "r_ball_rig_pointConstraint1.crp";
connectAttr "r_ball_rig.rpt" "r_ball_rig_pointConstraint1.crt";
connectAttr "r_ball_rig_ik.t" "r_ball_rig_pointConstraint1.tg[0].tt";
connectAttr "r_ball_rig_ik.rp" "r_ball_rig_pointConstraint1.tg[0].trp";
connectAttr "r_ball_rig_ik.rpt" "r_ball_rig_pointConstraint1.tg[0].trt";
connectAttr "r_ball_rig_ik.pm" "r_ball_rig_pointConstraint1.tg[0].tpm";
connectAttr "r_ball_rig_pointConstraint1.w0" "r_ball_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "r_ball_rig_fk.t" "r_ball_rig_pointConstraint1.tg[1].tt";
connectAttr "r_ball_rig_fk.rp" "r_ball_rig_pointConstraint1.tg[1].trp";
connectAttr "r_ball_rig_fk.rpt" "r_ball_rig_pointConstraint1.tg[1].trt";
connectAttr "r_ball_rig_fk.pm" "r_ball_rig_pointConstraint1.tg[1].tpm";
connectAttr "r_ball_rig_pointConstraint1.w1" "r_ball_rig_pointConstraint1.tg[1].tw"
		;
connectAttr "r_ball_rig.blendSpace" "r_ball_rig_pointConstraint1.w0";
connectAttr "r_ball_rig_fk_point_reverse.ox" "r_ball_rig_pointConstraint1.w1";
connectAttr "r_ball_rig.ro" "r_ball_rig_orientConstraint1.cro";
connectAttr "r_ball_rig.pim" "r_ball_rig_orientConstraint1.cpim";
connectAttr "r_ball_rig.jo" "r_ball_rig_orientConstraint1.cjo";
connectAttr "r_ball_rig.is" "r_ball_rig_orientConstraint1.is";
connectAttr "r_ball_rig_ik.r" "r_ball_rig_orientConstraint1.tg[0].tr";
connectAttr "r_ball_rig_ik.ro" "r_ball_rig_orientConstraint1.tg[0].tro";
connectAttr "r_ball_rig_ik.pm" "r_ball_rig_orientConstraint1.tg[0].tpm";
connectAttr "r_ball_rig_ik.jo" "r_ball_rig_orientConstraint1.tg[0].tjo";
connectAttr "r_ball_rig_orientConstraint1.w0" "r_ball_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "r_ball_rig_fk.r" "r_ball_rig_orientConstraint1.tg[1].tr";
connectAttr "r_ball_rig_fk.ro" "r_ball_rig_orientConstraint1.tg[1].tro";
connectAttr "r_ball_rig_fk.pm" "r_ball_rig_orientConstraint1.tg[1].tpm";
connectAttr "r_ball_rig_fk.jo" "r_ball_rig_orientConstraint1.tg[1].tjo";
connectAttr "r_ball_rig_orientConstraint1.w1" "r_ball_rig_orientConstraint1.tg[1].tw"
		;
connectAttr "r_ball_rig.blendSpace" "r_ball_rig_orientConstraint1.w0";
connectAttr "r_ball_rig_fk_point_OR_reverse.ox" "r_ball_rig_orientConstraint1.w1"
		;
connectAttr "r_ankle_rig.pim" "r_ankle_rig_pointConstraint1.cpim";
connectAttr "r_ankle_rig.rp" "r_ankle_rig_pointConstraint1.crp";
connectAttr "r_ankle_rig.rpt" "r_ankle_rig_pointConstraint1.crt";
connectAttr "r_ankle_rig_ik.t" "r_ankle_rig_pointConstraint1.tg[0].tt";
connectAttr "r_ankle_rig_ik.rp" "r_ankle_rig_pointConstraint1.tg[0].trp";
connectAttr "r_ankle_rig_ik.rpt" "r_ankle_rig_pointConstraint1.tg[0].trt";
connectAttr "r_ankle_rig_ik.pm" "r_ankle_rig_pointConstraint1.tg[0].tpm";
connectAttr "r_ankle_rig_pointConstraint1.w0" "r_ankle_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "r_ankle_rig_fk.t" "r_ankle_rig_pointConstraint1.tg[1].tt";
connectAttr "r_ankle_rig_fk.rp" "r_ankle_rig_pointConstraint1.tg[1].trp";
connectAttr "r_ankle_rig_fk.rpt" "r_ankle_rig_pointConstraint1.tg[1].trt";
connectAttr "r_ankle_rig_fk.pm" "r_ankle_rig_pointConstraint1.tg[1].tpm";
connectAttr "r_ankle_rig_pointConstraint1.w1" "r_ankle_rig_pointConstraint1.tg[1].tw"
		;
connectAttr "r_ankle_rig.blendSpace" "r_ankle_rig_pointConstraint1.w0";
connectAttr "r_ankle_rig_fk_point_reverse.ox" "r_ankle_rig_pointConstraint1.w1";
connectAttr "r_ankle_rig.ro" "r_ankle_rig_orientConstraint1.cro";
connectAttr "r_ankle_rig.pim" "r_ankle_rig_orientConstraint1.cpim";
connectAttr "r_ankle_rig.jo" "r_ankle_rig_orientConstraint1.cjo";
connectAttr "r_ankle_rig.is" "r_ankle_rig_orientConstraint1.is";
connectAttr "r_ankle_rig_ik.r" "r_ankle_rig_orientConstraint1.tg[0].tr";
connectAttr "r_ankle_rig_ik.ro" "r_ankle_rig_orientConstraint1.tg[0].tro";
connectAttr "r_ankle_rig_ik.pm" "r_ankle_rig_orientConstraint1.tg[0].tpm";
connectAttr "r_ankle_rig_ik.jo" "r_ankle_rig_orientConstraint1.tg[0].tjo";
connectAttr "r_ankle_rig_orientConstraint1.w0" "r_ankle_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "r_ankle_rig_fk.r" "r_ankle_rig_orientConstraint1.tg[1].tr";
connectAttr "r_ankle_rig_fk.ro" "r_ankle_rig_orientConstraint1.tg[1].tro";
connectAttr "r_ankle_rig_fk.pm" "r_ankle_rig_orientConstraint1.tg[1].tpm";
connectAttr "r_ankle_rig_fk.jo" "r_ankle_rig_orientConstraint1.tg[1].tjo";
connectAttr "r_ankle_rig_orientConstraint1.w1" "r_ankle_rig_orientConstraint1.tg[1].tw"
		;
connectAttr "r_ankle_rig.blendSpace" "r_ankle_rig_orientConstraint1.w0";
connectAttr "r_ankle_rig_fk_point_OR_reverse.ox" "r_ankle_rig_orientConstraint1.w1"
		;
connectAttr "r_knee_rig.pim" "r_knee_rig_pointConstraint1.cpim";
connectAttr "r_knee_rig.rp" "r_knee_rig_pointConstraint1.crp";
connectAttr "r_knee_rig.rpt" "r_knee_rig_pointConstraint1.crt";
connectAttr "r_knee_rig_ik.t" "r_knee_rig_pointConstraint1.tg[0].tt";
connectAttr "r_knee_rig_ik.rp" "r_knee_rig_pointConstraint1.tg[0].trp";
connectAttr "r_knee_rig_ik.rpt" "r_knee_rig_pointConstraint1.tg[0].trt";
connectAttr "r_knee_rig_ik.pm" "r_knee_rig_pointConstraint1.tg[0].tpm";
connectAttr "r_knee_rig_pointConstraint1.w0" "r_knee_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "r_knee_rig_fk.t" "r_knee_rig_pointConstraint1.tg[1].tt";
connectAttr "r_knee_rig_fk.rp" "r_knee_rig_pointConstraint1.tg[1].trp";
connectAttr "r_knee_rig_fk.rpt" "r_knee_rig_pointConstraint1.tg[1].trt";
connectAttr "r_knee_rig_fk.pm" "r_knee_rig_pointConstraint1.tg[1].tpm";
connectAttr "r_knee_rig_pointConstraint1.w1" "r_knee_rig_pointConstraint1.tg[1].tw"
		;
connectAttr "r_knee_rig.blendSpace" "r_knee_rig_pointConstraint1.w0";
connectAttr "r_knee_rig_fk_point_reverse.ox" "r_knee_rig_pointConstraint1.w1";
connectAttr "r_foot_ik_ctrl.xOffset" "r_knee_rig_pointConstraint1.ox";
connectAttr "r_foot_ik_ctrl.xOffset" "r_knee_rig_pointConstraint1.oy";
connectAttr "r_foot_ik_ctrl.zOffset" "r_knee_rig_pointConstraint1.oz";
connectAttr "r_knee_rig.ro" "r_knee_rig_orientConstraint1.cro";
connectAttr "r_knee_rig.pim" "r_knee_rig_orientConstraint1.cpim";
connectAttr "r_knee_rig.jo" "r_knee_rig_orientConstraint1.cjo";
connectAttr "r_knee_rig.is" "r_knee_rig_orientConstraint1.is";
connectAttr "r_knee_rig_ik.r" "r_knee_rig_orientConstraint1.tg[0].tr";
connectAttr "r_knee_rig_ik.ro" "r_knee_rig_orientConstraint1.tg[0].tro";
connectAttr "r_knee_rig_ik.pm" "r_knee_rig_orientConstraint1.tg[0].tpm";
connectAttr "r_knee_rig_ik.jo" "r_knee_rig_orientConstraint1.tg[0].tjo";
connectAttr "r_knee_rig_orientConstraint1.w0" "r_knee_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "r_knee_rig_fk.r" "r_knee_rig_orientConstraint1.tg[1].tr";
connectAttr "r_knee_rig_fk.ro" "r_knee_rig_orientConstraint1.tg[1].tro";
connectAttr "r_knee_rig_fk.pm" "r_knee_rig_orientConstraint1.tg[1].tpm";
connectAttr "r_knee_rig_fk.jo" "r_knee_rig_orientConstraint1.tg[1].tjo";
connectAttr "r_knee_rig_orientConstraint1.w1" "r_knee_rig_orientConstraint1.tg[1].tw"
		;
connectAttr "r_knee_rig.blendSpace" "r_knee_rig_orientConstraint1.w0";
connectAttr "r_knee_rig_fk_point_OR_reverse.ox" "r_knee_rig_orientConstraint1.w1"
		;
connectAttr "r_hip_rig.pim" "r_hip_rig_pointConstraint1.cpim";
connectAttr "r_hip_rig.rp" "r_hip_rig_pointConstraint1.crp";
connectAttr "r_hip_rig.rpt" "r_hip_rig_pointConstraint1.crt";
connectAttr "r_hip_rig_ik.t" "r_hip_rig_pointConstraint1.tg[0].tt";
connectAttr "r_hip_rig_ik.rp" "r_hip_rig_pointConstraint1.tg[0].trp";
connectAttr "r_hip_rig_ik.rpt" "r_hip_rig_pointConstraint1.tg[0].trt";
connectAttr "r_hip_rig_ik.pm" "r_hip_rig_pointConstraint1.tg[0].tpm";
connectAttr "r_hip_rig_pointConstraint1.w0" "r_hip_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "r_hip_rig_fk.t" "r_hip_rig_pointConstraint1.tg[1].tt";
connectAttr "r_hip_rig_fk.rp" "r_hip_rig_pointConstraint1.tg[1].trp";
connectAttr "r_hip_rig_fk.rpt" "r_hip_rig_pointConstraint1.tg[1].trt";
connectAttr "r_hip_rig_fk.pm" "r_hip_rig_pointConstraint1.tg[1].tpm";
connectAttr "r_hip_rig_pointConstraint1.w1" "r_hip_rig_pointConstraint1.tg[1].tw"
		;
connectAttr "r_hip_rig.blendSpace" "r_hip_rig_pointConstraint1.w0";
connectAttr "r_hip_rig_fk_point_reverse.ox" "r_hip_rig_pointConstraint1.w1";
connectAttr "r_hip_rig.ro" "r_hip_rig_orientConstraint1.cro";
connectAttr "r_hip_rig.pim" "r_hip_rig_orientConstraint1.cpim";
connectAttr "r_hip_rig.jo" "r_hip_rig_orientConstraint1.cjo";
connectAttr "r_hip_rig.is" "r_hip_rig_orientConstraint1.is";
connectAttr "r_hip_rig_ik.r" "r_hip_rig_orientConstraint1.tg[0].tr";
connectAttr "r_hip_rig_ik.ro" "r_hip_rig_orientConstraint1.tg[0].tro";
connectAttr "r_hip_rig_ik.pm" "r_hip_rig_orientConstraint1.tg[0].tpm";
connectAttr "r_hip_rig_ik.jo" "r_hip_rig_orientConstraint1.tg[0].tjo";
connectAttr "r_hip_rig_orientConstraint1.w0" "r_hip_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "r_hip_rig_fk.r" "r_hip_rig_orientConstraint1.tg[1].tr";
connectAttr "r_hip_rig_fk.ro" "r_hip_rig_orientConstraint1.tg[1].tro";
connectAttr "r_hip_rig_fk.pm" "r_hip_rig_orientConstraint1.tg[1].tpm";
connectAttr "r_hip_rig_fk.jo" "r_hip_rig_orientConstraint1.tg[1].tjo";
connectAttr "r_hip_rig_orientConstraint1.w1" "r_hip_rig_orientConstraint1.tg[1].tw"
		;
connectAttr "r_hip_rig.blendSpace" "r_hip_rig_orientConstraint1.w0";
connectAttr "r_hip_rig_fk_point_OR_reverse.ox" "r_hip_rig_orientConstraint1.w1";
connectAttr "hips_rig.ro" "hips_rig_orientConstraint1.cro";
connectAttr "hips_rig.pim" "hips_rig_orientConstraint1.cpim";
connectAttr "hips_rig.jo" "hips_rig_orientConstraint1.cjo";
connectAttr "hips_rig.is" "hips_rig_orientConstraint1.is";
connectAttr "hips_ctrl.r" "hips_rig_orientConstraint1.tg[0].tr";
connectAttr "hips_ctrl.ro" "hips_rig_orientConstraint1.tg[0].tro";
connectAttr "hips_ctrl.pm" "hips_rig_orientConstraint1.tg[0].tpm";
connectAttr "hips_rig_orientConstraint1.w0" "hips_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "hips_rig.pim" "hips_rig_pointConstraint1.cpim";
connectAttr "hips_rig.rp" "hips_rig_pointConstraint1.crp";
connectAttr "hips_rig.rpt" "hips_rig_pointConstraint1.crt";
connectAttr "hips_ctrl.t" "hips_rig_pointConstraint1.tg[0].tt";
connectAttr "hips_ctrl.rp" "hips_rig_pointConstraint1.tg[0].trp";
connectAttr "hips_ctrl.rpt" "hips_rig_pointConstraint1.tg[0].trt";
connectAttr "hips_ctrl.pm" "hips_rig_pointConstraint1.tg[0].tpm";
connectAttr "hips_rig_pointConstraint1.w0" "hips_rig_pointConstraint1.tg[0].tw";
connectAttr "hips_rig.s" "l_hip_rig_ik.is";
connectAttr "l_hip_rig_ik.s" "l_knee_rig_ik.is";
connectAttr "expression12.out[0]" "l_knee_rig_ik.tx";
connectAttr "l_knee_rig_ik.s" "l_ankle_rig_ik.is";
connectAttr "l_ankle_rig_ik_orientConstraint1.crx" "l_ankle_rig_ik.rx";
connectAttr "l_ankle_rig_ik_orientConstraint1.cry" "l_ankle_rig_ik.ry";
connectAttr "l_ankle_rig_ik_orientConstraint1.crz" "l_ankle_rig_ik.rz";
connectAttr "expression13.out[0]" "l_ankle_rig_ik.tx";
connectAttr "l_ankle_rig_ik.s" "l_ball_rig_ik.is";
connectAttr "l_ball_rig_ik.s" "l_toe_rig_ik.is";
connectAttr "l_toe_rig_ik.tx" "effector6.tx";
connectAttr "l_toe_rig_ik.ty" "effector6.ty";
connectAttr "l_toe_rig_ik.tz" "effector6.tz";
connectAttr "l_ankle_rig_ik.ro" "l_ankle_rig_ik_orientConstraint1.cro";
connectAttr "l_ankle_rig_ik.pim" "l_ankle_rig_ik_orientConstraint1.cpim";
connectAttr "l_ankle_rig_ik.jo" "l_ankle_rig_ik_orientConstraint1.cjo";
connectAttr "l_ankle_rig_ik.is" "l_ankle_rig_ik_orientConstraint1.is";
connectAttr "l_foot_ik_ctrl.r" "l_ankle_rig_ik_orientConstraint1.tg[0].tr";
connectAttr "l_foot_ik_ctrl.ro" "l_ankle_rig_ik_orientConstraint1.tg[0].tro";
connectAttr "l_foot_ik_ctrl.pm" "l_ankle_rig_ik_orientConstraint1.tg[0].tpm";
connectAttr "l_ankle_rig_ik_orientConstraint1.w0" "l_ankle_rig_ik_orientConstraint1.tg[0].tw"
		;
connectAttr "l_ball_rig_ik.tx" "effector5.tx";
connectAttr "l_ball_rig_ik.ty" "effector5.ty";
connectAttr "l_ball_rig_ik.tz" "effector5.tz";
connectAttr "l_ankle_rig_ik.tx" "effector4.tx";
connectAttr "l_ankle_rig_ik.ty" "effector4.ty";
connectAttr "l_ankle_rig_ik.tz" "effector4.tz";
connectAttr "l_hip_rig_fk_orientConstraint1.crx" "l_hip_rig_fk.rx";
connectAttr "l_hip_rig_fk_orientConstraint1.cry" "l_hip_rig_fk.ry";
connectAttr "l_hip_rig_fk_orientConstraint1.crz" "l_hip_rig_fk.rz";
connectAttr "hips_rig.s" "l_hip_rig_fk.is";
connectAttr "l_hip_rig_fk_pointConstraint1.ctx" "l_hip_rig_fk.tx";
connectAttr "l_hip_rig_fk_pointConstraint1.cty" "l_hip_rig_fk.ty";
connectAttr "l_hip_rig_fk_pointConstraint1.ctz" "l_hip_rig_fk.tz";
connectAttr "l_hip_rig_fk.s" "l_knee_rig_fk.is";
connectAttr "l_knee_rig_fk_orientConstraint1.crx" "l_knee_rig_fk.rx";
connectAttr "l_knee_rig_fk_orientConstraint1.cry" "l_knee_rig_fk.ry";
connectAttr "l_knee_rig_fk_orientConstraint1.crz" "l_knee_rig_fk.rz";
connectAttr "l_knee_rig_fk_pointConstraint1.ctx" "l_knee_rig_fk.tx";
connectAttr "l_knee_rig_fk_pointConstraint1.cty" "l_knee_rig_fk.ty";
connectAttr "l_knee_rig_fk_pointConstraint1.ctz" "l_knee_rig_fk.tz";
connectAttr "l_knee_rig_fk.s" "l_ankle_rig_fk.is";
connectAttr "l_ankle_rig_fk_orientConstraint1.crx" "l_ankle_rig_fk.rx";
connectAttr "l_ankle_rig_fk_orientConstraint1.cry" "l_ankle_rig_fk.ry";
connectAttr "l_ankle_rig_fk_orientConstraint1.crz" "l_ankle_rig_fk.rz";
connectAttr "l_ankle_rig_fk_pointConstraint1.ctx" "l_ankle_rig_fk.tx";
connectAttr "l_ankle_rig_fk_pointConstraint1.cty" "l_ankle_rig_fk.ty";
connectAttr "l_ankle_rig_fk_pointConstraint1.ctz" "l_ankle_rig_fk.tz";
connectAttr "l_ankle_rig_fk.s" "l_ball_rig_fk.is";
connectAttr "l_ball_rig_fk_orientConstraint1.crx" "l_ball_rig_fk.rx";
connectAttr "l_ball_rig_fk_orientConstraint1.cry" "l_ball_rig_fk.ry";
connectAttr "l_ball_rig_fk_orientConstraint1.crz" "l_ball_rig_fk.rz";
connectAttr "l_ball_rig_fk_pointConstraint1.ctx" "l_ball_rig_fk.tx";
connectAttr "l_ball_rig_fk_pointConstraint1.cty" "l_ball_rig_fk.ty";
connectAttr "l_ball_rig_fk_pointConstraint1.ctz" "l_ball_rig_fk.tz";
connectAttr "l_ball_rig_fk.s" "l_toe_rig_fk.is";
connectAttr "l_ball_rig_fk.ro" "l_ball_rig_fk_orientConstraint1.cro";
connectAttr "l_ball_rig_fk.pim" "l_ball_rig_fk_orientConstraint1.cpim";
connectAttr "l_ball_rig_fk.jo" "l_ball_rig_fk_orientConstraint1.cjo";
connectAttr "l_ball_rig_fk.is" "l_ball_rig_fk_orientConstraint1.is";
connectAttr "l_ball_ctrl.r" "l_ball_rig_fk_orientConstraint1.tg[0].tr";
connectAttr "l_ball_ctrl.ro" "l_ball_rig_fk_orientConstraint1.tg[0].tro";
connectAttr "l_ball_ctrl.pm" "l_ball_rig_fk_orientConstraint1.tg[0].tpm";
connectAttr "l_ball_rig_fk_orientConstraint1.w0" "l_ball_rig_fk_orientConstraint1.tg[0].tw"
		;
connectAttr "l_ball_rig_fk.pim" "l_ball_rig_fk_pointConstraint1.cpim";
connectAttr "l_ball_rig_fk.rp" "l_ball_rig_fk_pointConstraint1.crp";
connectAttr "l_ball_rig_fk.rpt" "l_ball_rig_fk_pointConstraint1.crt";
connectAttr "l_ball_ctrl.t" "l_ball_rig_fk_pointConstraint1.tg[0].tt";
connectAttr "l_ball_ctrl.rp" "l_ball_rig_fk_pointConstraint1.tg[0].trp";
connectAttr "l_ball_ctrl.rpt" "l_ball_rig_fk_pointConstraint1.tg[0].trt";
connectAttr "l_ball_ctrl.pm" "l_ball_rig_fk_pointConstraint1.tg[0].tpm";
connectAttr "l_ball_rig_fk_pointConstraint1.w0" "l_ball_rig_fk_pointConstraint1.tg[0].tw"
		;
connectAttr "l_ankle_rig_fk.ro" "l_ankle_rig_fk_orientConstraint1.cro";
connectAttr "l_ankle_rig_fk.pim" "l_ankle_rig_fk_orientConstraint1.cpim";
connectAttr "l_ankle_rig_fk.jo" "l_ankle_rig_fk_orientConstraint1.cjo";
connectAttr "l_ankle_rig_fk.is" "l_ankle_rig_fk_orientConstraint1.is";
connectAttr "l_ankle_ctrl.r" "l_ankle_rig_fk_orientConstraint1.tg[0].tr";
connectAttr "l_ankle_ctrl.ro" "l_ankle_rig_fk_orientConstraint1.tg[0].tro";
connectAttr "l_ankle_ctrl.pm" "l_ankle_rig_fk_orientConstraint1.tg[0].tpm";
connectAttr "l_ankle_rig_fk_orientConstraint1.w0" "l_ankle_rig_fk_orientConstraint1.tg[0].tw"
		;
connectAttr "l_ankle_rig_fk.pim" "l_ankle_rig_fk_pointConstraint1.cpim";
connectAttr "l_ankle_rig_fk.rp" "l_ankle_rig_fk_pointConstraint1.crp";
connectAttr "l_ankle_rig_fk.rpt" "l_ankle_rig_fk_pointConstraint1.crt";
connectAttr "l_ankle_ctrl.t" "l_ankle_rig_fk_pointConstraint1.tg[0].tt";
connectAttr "l_ankle_ctrl.rp" "l_ankle_rig_fk_pointConstraint1.tg[0].trp";
connectAttr "l_ankle_ctrl.rpt" "l_ankle_rig_fk_pointConstraint1.tg[0].trt";
connectAttr "l_ankle_ctrl.pm" "l_ankle_rig_fk_pointConstraint1.tg[0].tpm";
connectAttr "l_ankle_rig_fk_pointConstraint1.w0" "l_ankle_rig_fk_pointConstraint1.tg[0].tw"
		;
connectAttr "l_knee_rig_fk.ro" "l_knee_rig_fk_orientConstraint1.cro";
connectAttr "l_knee_rig_fk.pim" "l_knee_rig_fk_orientConstraint1.cpim";
connectAttr "l_knee_rig_fk.jo" "l_knee_rig_fk_orientConstraint1.cjo";
connectAttr "l_knee_rig_fk.is" "l_knee_rig_fk_orientConstraint1.is";
connectAttr "l_knee_ctrl.r" "l_knee_rig_fk_orientConstraint1.tg[0].tr";
connectAttr "l_knee_ctrl.ro" "l_knee_rig_fk_orientConstraint1.tg[0].tro";
connectAttr "l_knee_ctrl.pm" "l_knee_rig_fk_orientConstraint1.tg[0].tpm";
connectAttr "l_knee_rig_fk_orientConstraint1.w0" "l_knee_rig_fk_orientConstraint1.tg[0].tw"
		;
connectAttr "l_knee_rig_fk.pim" "l_knee_rig_fk_pointConstraint1.cpim";
connectAttr "l_knee_rig_fk.rp" "l_knee_rig_fk_pointConstraint1.crp";
connectAttr "l_knee_rig_fk.rpt" "l_knee_rig_fk_pointConstraint1.crt";
connectAttr "l_knee_ctrl.t" "l_knee_rig_fk_pointConstraint1.tg[0].tt";
connectAttr "l_knee_ctrl.rp" "l_knee_rig_fk_pointConstraint1.tg[0].trp";
connectAttr "l_knee_ctrl.rpt" "l_knee_rig_fk_pointConstraint1.tg[0].trt";
connectAttr "l_knee_ctrl.pm" "l_knee_rig_fk_pointConstraint1.tg[0].tpm";
connectAttr "l_knee_rig_fk_pointConstraint1.w0" "l_knee_rig_fk_pointConstraint1.tg[0].tw"
		;
connectAttr "l_hip_rig_fk.ro" "l_hip_rig_fk_orientConstraint1.cro";
connectAttr "l_hip_rig_fk.pim" "l_hip_rig_fk_orientConstraint1.cpim";
connectAttr "l_hip_rig_fk.jo" "l_hip_rig_fk_orientConstraint1.cjo";
connectAttr "l_hip_rig_fk.is" "l_hip_rig_fk_orientConstraint1.is";
connectAttr "l_hip_ctrl.r" "l_hip_rig_fk_orientConstraint1.tg[0].tr";
connectAttr "l_hip_ctrl.ro" "l_hip_rig_fk_orientConstraint1.tg[0].tro";
connectAttr "l_hip_ctrl.pm" "l_hip_rig_fk_orientConstraint1.tg[0].tpm";
connectAttr "l_hip_rig_fk_orientConstraint1.w0" "l_hip_rig_fk_orientConstraint1.tg[0].tw"
		;
connectAttr "l_hip_rig_fk.pim" "l_hip_rig_fk_pointConstraint1.cpim";
connectAttr "l_hip_rig_fk.rp" "l_hip_rig_fk_pointConstraint1.crp";
connectAttr "l_hip_rig_fk.rpt" "l_hip_rig_fk_pointConstraint1.crt";
connectAttr "l_hip_ctrl.t" "l_hip_rig_fk_pointConstraint1.tg[0].tt";
connectAttr "l_hip_ctrl.rp" "l_hip_rig_fk_pointConstraint1.tg[0].trp";
connectAttr "l_hip_ctrl.rpt" "l_hip_rig_fk_pointConstraint1.tg[0].trt";
connectAttr "l_hip_ctrl.pm" "l_hip_rig_fk_pointConstraint1.tg[0].tpm";
connectAttr "l_hip_rig_fk_pointConstraint1.w0" "l_hip_rig_fk_pointConstraint1.tg[0].tw"
		;
connectAttr "hips_rig.s" "r_hip_rig_ik.is";
connectAttr "r_hip_rig_ik.s" "r_knee_rig_ik.is";
connectAttr "expression14.out[0]" "r_knee_rig_ik.tx";
connectAttr "r_knee_rig_ik.s" "r_ankle_rig_ik.is";
connectAttr "r_ankle_rig_ik_orientConstraint1.crx" "r_ankle_rig_ik.rx";
connectAttr "r_ankle_rig_ik_orientConstraint1.cry" "r_ankle_rig_ik.ry";
connectAttr "r_ankle_rig_ik_orientConstraint1.crz" "r_ankle_rig_ik.rz";
connectAttr "expression15.out[0]" "r_ankle_rig_ik.tx";
connectAttr "r_ankle_rig_ik.s" "r_ball_rig_ik.is";
connectAttr "r_ball_rig_ik.s" "r_toe_rig_ik.is";
connectAttr "r_toe_rig_ik.tx" "effector9.tx";
connectAttr "r_toe_rig_ik.ty" "effector9.ty";
connectAttr "r_toe_rig_ik.tz" "effector9.tz";
connectAttr "r_ankle_rig_ik.ro" "r_ankle_rig_ik_orientConstraint1.cro";
connectAttr "r_ankle_rig_ik.pim" "r_ankle_rig_ik_orientConstraint1.cpim";
connectAttr "r_ankle_rig_ik.jo" "r_ankle_rig_ik_orientConstraint1.cjo";
connectAttr "r_ankle_rig_ik.is" "r_ankle_rig_ik_orientConstraint1.is";
connectAttr "r_foot_ik_ctrl.r" "r_ankle_rig_ik_orientConstraint1.tg[0].tr";
connectAttr "r_foot_ik_ctrl.ro" "r_ankle_rig_ik_orientConstraint1.tg[0].tro";
connectAttr "r_foot_ik_ctrl.pm" "r_ankle_rig_ik_orientConstraint1.tg[0].tpm";
connectAttr "r_ankle_rig_ik_orientConstraint1.w0" "r_ankle_rig_ik_orientConstraint1.tg[0].tw"
		;
connectAttr "r_ball_rig_ik.tx" "effector8.tx";
connectAttr "r_ball_rig_ik.ty" "effector8.ty";
connectAttr "r_ball_rig_ik.tz" "effector8.tz";
connectAttr "r_ankle_rig_ik.tx" "effector7.tx";
connectAttr "r_ankle_rig_ik.ty" "effector7.ty";
connectAttr "r_ankle_rig_ik.tz" "effector7.tz";
connectAttr "r_hip_rig_fk_orientConstraint1.crx" "r_hip_rig_fk.rx";
connectAttr "r_hip_rig_fk_orientConstraint1.cry" "r_hip_rig_fk.ry";
connectAttr "r_hip_rig_fk_orientConstraint1.crz" "r_hip_rig_fk.rz";
connectAttr "hips_rig.s" "r_hip_rig_fk.is";
connectAttr "r_hip_rig_fk_pointConstraint1.ctx" "r_hip_rig_fk.tx";
connectAttr "r_hip_rig_fk_pointConstraint1.cty" "r_hip_rig_fk.ty";
connectAttr "r_hip_rig_fk_pointConstraint1.ctz" "r_hip_rig_fk.tz";
connectAttr "r_hip_rig_fk.s" "r_knee_rig_fk.is";
connectAttr "r_knee_rig_fk_orientConstraint1.crx" "r_knee_rig_fk.rx";
connectAttr "r_knee_rig_fk_orientConstraint1.cry" "r_knee_rig_fk.ry";
connectAttr "r_knee_rig_fk_orientConstraint1.crz" "r_knee_rig_fk.rz";
connectAttr "r_knee_rig_fk_pointConstraint1.ctx" "r_knee_rig_fk.tx";
connectAttr "r_knee_rig_fk_pointConstraint1.cty" "r_knee_rig_fk.ty";
connectAttr "r_knee_rig_fk_pointConstraint1.ctz" "r_knee_rig_fk.tz";
connectAttr "r_knee_rig_fk.s" "r_ankle_rig_fk.is";
connectAttr "r_ankle_rig_fk_orientConstraint1.crx" "r_ankle_rig_fk.rx";
connectAttr "r_ankle_rig_fk_orientConstraint1.cry" "r_ankle_rig_fk.ry";
connectAttr "r_ankle_rig_fk_orientConstraint1.crz" "r_ankle_rig_fk.rz";
connectAttr "r_ankle_rig_fk_pointConstraint1.ctx" "r_ankle_rig_fk.tx";
connectAttr "r_ankle_rig_fk_pointConstraint1.cty" "r_ankle_rig_fk.ty";
connectAttr "r_ankle_rig_fk_pointConstraint1.ctz" "r_ankle_rig_fk.tz";
connectAttr "r_ankle_rig_fk.s" "r_ball_rig_fk.is";
connectAttr "r_ball_rig_fk_orientConstraint1.crx" "r_ball_rig_fk.rx";
connectAttr "r_ball_rig_fk_orientConstraint1.cry" "r_ball_rig_fk.ry";
connectAttr "r_ball_rig_fk_orientConstraint1.crz" "r_ball_rig_fk.rz";
connectAttr "r_ball_rig_fk_pointConstraint1.ctx" "r_ball_rig_fk.tx";
connectAttr "r_ball_rig_fk_pointConstraint1.cty" "r_ball_rig_fk.ty";
connectAttr "r_ball_rig_fk_pointConstraint1.ctz" "r_ball_rig_fk.tz";
connectAttr "r_ball_rig_fk.s" "r_toe_rig_fk.is";
connectAttr "r_ball_rig_fk.ro" "r_ball_rig_fk_orientConstraint1.cro";
connectAttr "r_ball_rig_fk.pim" "r_ball_rig_fk_orientConstraint1.cpim";
connectAttr "r_ball_rig_fk.jo" "r_ball_rig_fk_orientConstraint1.cjo";
connectAttr "r_ball_rig_fk.is" "r_ball_rig_fk_orientConstraint1.is";
connectAttr "r_ball_ctrl.r" "r_ball_rig_fk_orientConstraint1.tg[0].tr";
connectAttr "r_ball_ctrl.ro" "r_ball_rig_fk_orientConstraint1.tg[0].tro";
connectAttr "r_ball_ctrl.pm" "r_ball_rig_fk_orientConstraint1.tg[0].tpm";
connectAttr "r_ball_rig_fk_orientConstraint1.w0" "r_ball_rig_fk_orientConstraint1.tg[0].tw"
		;
connectAttr "r_ball_rig_fk.pim" "r_ball_rig_fk_pointConstraint1.cpim";
connectAttr "r_ball_rig_fk.rp" "r_ball_rig_fk_pointConstraint1.crp";
connectAttr "r_ball_rig_fk.rpt" "r_ball_rig_fk_pointConstraint1.crt";
connectAttr "r_ball_ctrl.t" "r_ball_rig_fk_pointConstraint1.tg[0].tt";
connectAttr "r_ball_ctrl.rp" "r_ball_rig_fk_pointConstraint1.tg[0].trp";
connectAttr "r_ball_ctrl.rpt" "r_ball_rig_fk_pointConstraint1.tg[0].trt";
connectAttr "r_ball_ctrl.pm" "r_ball_rig_fk_pointConstraint1.tg[0].tpm";
connectAttr "r_ball_rig_fk_pointConstraint1.w0" "r_ball_rig_fk_pointConstraint1.tg[0].tw"
		;
connectAttr "r_ankle_rig_fk.ro" "r_ankle_rig_fk_orientConstraint1.cro";
connectAttr "r_ankle_rig_fk.pim" "r_ankle_rig_fk_orientConstraint1.cpim";
connectAttr "r_ankle_rig_fk.jo" "r_ankle_rig_fk_orientConstraint1.cjo";
connectAttr "r_ankle_rig_fk.is" "r_ankle_rig_fk_orientConstraint1.is";
connectAttr "r_ankle_ctrl.r" "r_ankle_rig_fk_orientConstraint1.tg[0].tr";
connectAttr "r_ankle_ctrl.ro" "r_ankle_rig_fk_orientConstraint1.tg[0].tro";
connectAttr "r_ankle_ctrl.pm" "r_ankle_rig_fk_orientConstraint1.tg[0].tpm";
connectAttr "r_ankle_rig_fk_orientConstraint1.w0" "r_ankle_rig_fk_orientConstraint1.tg[0].tw"
		;
connectAttr "r_ankle_rig_fk.pim" "r_ankle_rig_fk_pointConstraint1.cpim";
connectAttr "r_ankle_rig_fk.rp" "r_ankle_rig_fk_pointConstraint1.crp";
connectAttr "r_ankle_rig_fk.rpt" "r_ankle_rig_fk_pointConstraint1.crt";
connectAttr "r_ankle_ctrl.t" "r_ankle_rig_fk_pointConstraint1.tg[0].tt";
connectAttr "r_ankle_ctrl.rp" "r_ankle_rig_fk_pointConstraint1.tg[0].trp";
connectAttr "r_ankle_ctrl.rpt" "r_ankle_rig_fk_pointConstraint1.tg[0].trt";
connectAttr "r_ankle_ctrl.pm" "r_ankle_rig_fk_pointConstraint1.tg[0].tpm";
connectAttr "r_ankle_rig_fk_pointConstraint1.w0" "r_ankle_rig_fk_pointConstraint1.tg[0].tw"
		;
connectAttr "r_knee_rig_fk.ro" "r_knee_rig_fk_orientConstraint1.cro";
connectAttr "r_knee_rig_fk.pim" "r_knee_rig_fk_orientConstraint1.cpim";
connectAttr "r_knee_rig_fk.jo" "r_knee_rig_fk_orientConstraint1.cjo";
connectAttr "r_knee_rig_fk.is" "r_knee_rig_fk_orientConstraint1.is";
connectAttr "r_knee_ctrl.r" "r_knee_rig_fk_orientConstraint1.tg[0].tr";
connectAttr "r_knee_ctrl.ro" "r_knee_rig_fk_orientConstraint1.tg[0].tro";
connectAttr "r_knee_ctrl.pm" "r_knee_rig_fk_orientConstraint1.tg[0].tpm";
connectAttr "r_knee_rig_fk_orientConstraint1.w0" "r_knee_rig_fk_orientConstraint1.tg[0].tw"
		;
connectAttr "r_knee_rig_fk.pim" "r_knee_rig_fk_pointConstraint1.cpim";
connectAttr "r_knee_rig_fk.rp" "r_knee_rig_fk_pointConstraint1.crp";
connectAttr "r_knee_rig_fk.rpt" "r_knee_rig_fk_pointConstraint1.crt";
connectAttr "r_knee_ctrl.t" "r_knee_rig_fk_pointConstraint1.tg[0].tt";
connectAttr "r_knee_ctrl.rp" "r_knee_rig_fk_pointConstraint1.tg[0].trp";
connectAttr "r_knee_ctrl.rpt" "r_knee_rig_fk_pointConstraint1.tg[0].trt";
connectAttr "r_knee_ctrl.pm" "r_knee_rig_fk_pointConstraint1.tg[0].tpm";
connectAttr "r_knee_rig_fk_pointConstraint1.w0" "r_knee_rig_fk_pointConstraint1.tg[0].tw"
		;
connectAttr "r_hip_rig_fk.ro" "r_hip_rig_fk_orientConstraint1.cro";
connectAttr "r_hip_rig_fk.pim" "r_hip_rig_fk_orientConstraint1.cpim";
connectAttr "r_hip_rig_fk.jo" "r_hip_rig_fk_orientConstraint1.cjo";
connectAttr "r_hip_rig_fk.is" "r_hip_rig_fk_orientConstraint1.is";
connectAttr "r_hip_ctrl.r" "r_hip_rig_fk_orientConstraint1.tg[0].tr";
connectAttr "r_hip_ctrl.ro" "r_hip_rig_fk_orientConstraint1.tg[0].tro";
connectAttr "r_hip_ctrl.pm" "r_hip_rig_fk_orientConstraint1.tg[0].tpm";
connectAttr "r_hip_rig_fk_orientConstraint1.w0" "r_hip_rig_fk_orientConstraint1.tg[0].tw"
		;
connectAttr "r_hip_rig_fk.pim" "r_hip_rig_fk_pointConstraint1.cpim";
connectAttr "r_hip_rig_fk.rp" "r_hip_rig_fk_pointConstraint1.crp";
connectAttr "r_hip_rig_fk.rpt" "r_hip_rig_fk_pointConstraint1.crt";
connectAttr "r_hip_ctrl.t" "r_hip_rig_fk_pointConstraint1.tg[0].tt";
connectAttr "r_hip_ctrl.rp" "r_hip_rig_fk_pointConstraint1.tg[0].trp";
connectAttr "r_hip_ctrl.rpt" "r_hip_rig_fk_pointConstraint1.tg[0].trt";
connectAttr "r_hip_ctrl.pm" "r_hip_rig_fk_pointConstraint1.tg[0].tpm";
connectAttr "r_hip_rig_fk_pointConstraint1.w0" "r_hip_rig_fk_pointConstraint1.tg[0].tw"
		;
connectAttr "pelvis_rig.pim" "pelvis_rig_pointConstraint1.cpim";
connectAttr "pelvis_rig.rp" "pelvis_rig_pointConstraint1.crp";
connectAttr "pelvis_rig.rpt" "pelvis_rig_pointConstraint1.crt";
connectAttr "pelvis_ctrl.t" "pelvis_rig_pointConstraint1.tg[0].tt";
connectAttr "pelvis_ctrl.rp" "pelvis_rig_pointConstraint1.tg[0].trp";
connectAttr "pelvis_ctrl.rpt" "pelvis_rig_pointConstraint1.tg[0].trt";
connectAttr "pelvis_ctrl.pm" "pelvis_rig_pointConstraint1.tg[0].tpm";
connectAttr "pelvis_rig_pointConstraint1.w0" "pelvis_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "pelvis_rig.ro" "pelvis_rig_orientConstraint1.cro";
connectAttr "pelvis_rig.pim" "pelvis_rig_orientConstraint1.cpim";
connectAttr "pelvis_rig.jo" "pelvis_rig_orientConstraint1.cjo";
connectAttr "pelvis_rig.is" "pelvis_rig_orientConstraint1.is";
connectAttr "pelvis_ctrl.r" "pelvis_rig_orientConstraint1.tg[0].tr";
connectAttr "pelvis_ctrl.ro" "pelvis_rig_orientConstraint1.tg[0].tro";
connectAttr "pelvis_ctrl.pm" "pelvis_rig_orientConstraint1.tg[0].tpm";
connectAttr "pelvis_rig_orientConstraint1.w0" "pelvis_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "spineParent_rig_pointConstraint1.ctx" "spineParent_rig.tx";
connectAttr "spineParent_rig_pointConstraint1.cty" "spineParent_rig.ty";
connectAttr "spineParent_rig_pointConstraint1.ctz" "spineParent_rig.tz";
connectAttr "spineParent_rig_orientConstraint1.crx" "spineParent_rig.rx";
connectAttr "spineParent_rig_orientConstraint1.cry" "spineParent_rig.ry";
connectAttr "spineParent_rig_orientConstraint1.crz" "spineParent_rig.rz";
connectAttr "spineParent_rig.s" "neckBase_rig.is";
connectAttr "neckBase_rig.s" "neck_mid_rig.is";
connectAttr "neck_mid_rig.s" "neck_rig.is";
connectAttr "neck_rig.s" "l_eye_rig.is";
connectAttr "neck_rig.s" "r_eye_rig.is";
connectAttr "neck_rig.s" "jaw_rig.is";
connectAttr "l_clav_rig_orientConstraint1.crx" "l_clav_rig.rx";
connectAttr "l_clav_rig_orientConstraint1.cry" "l_clav_rig.ry";
connectAttr "l_clav_rig_orientConstraint1.crz" "l_clav_rig.rz";
connectAttr "spineParent_rig.s" "l_clav_rig.is";
connectAttr "l_clav_rig_pointConstraint1.ctx" "l_clav_rig.tx";
connectAttr "l_clav_rig_pointConstraint1.cty" "l_clav_rig.ty";
connectAttr "l_clav_rig_pointConstraint1.ctz" "l_clav_rig.tz";
connectAttr "l_shoulder_rig_orientConstraint1.crx" "l_shoulder_rig.rx";
connectAttr "l_shoulder_rig_orientConstraint1.cry" "l_shoulder_rig.ry";
connectAttr "l_shoulder_rig_orientConstraint1.crz" "l_shoulder_rig.rz";
connectAttr "l_clav_rig.s" "l_shoulder_rig.is";
connectAttr "l_shoulder_rig_pointConstraint1.ctx" "l_shoulder_rig.tx";
connectAttr "l_shoulder_rig_pointConstraint1.cty" "l_shoulder_rig.ty";
connectAttr "l_shoulder_rig_pointConstraint1.ctz" "l_shoulder_rig.tz";
connectAttr "l_arm_ik_switch.IkFkSwitch" "l_shoulder_rig.blendSpace";
connectAttr "l_elbow_rig_parentConstraint1.crx" "l_elbow_rig.rx";
connectAttr "l_elbow_rig_parentConstraint1.cry" "l_elbow_rig.ry";
connectAttr "l_elbow_rig_parentConstraint1.crz" "l_elbow_rig.rz";
connectAttr "l_shoulder_rig.s" "l_elbow_rig.is";
connectAttr "l_elbow_rig_parentConstraint1.ctx" "l_elbow_rig.tx";
connectAttr "l_elbow_rig_parentConstraint1.cty" "l_elbow_rig.ty";
connectAttr "l_elbow_rig_parentConstraint1.ctz" "l_elbow_rig.tz";
connectAttr "l_wrist_rig_orientConstraint1.crx" "l_wrist_rig.rx";
connectAttr "l_wrist_rig_orientConstraint1.cry" "l_wrist_rig.ry";
connectAttr "l_wrist_rig_orientConstraint1.crz" "l_wrist_rig.rz";
connectAttr "l_elbow_rig.s" "l_wrist_rig.is";
connectAttr "l_wrist_rig_pointConstraint1.ctx" "l_wrist_rig.tx";
connectAttr "l_wrist_rig_pointConstraint1.cty" "l_wrist_rig.ty";
connectAttr "l_wrist_rig_pointConstraint1.ctz" "l_wrist_rig.tz";
connectAttr "l_arm_ik_switch.IkFkSwitch" "l_wrist_rig.blendSpace";
connectAttr "l_wrist_rig.s" "l_index_01_rig.is";
connectAttr "l_index_01_rig.s" "l_index_02_rig.is";
connectAttr "l_index_02_rig.s" "l_index_03_rig.is";
connectAttr "l_wrist_rig.s" "l_pinky_01_rig.is";
connectAttr "l_pinky_01_rig.s" "l_pinky_02_rig.is";
connectAttr "l_pinky_02_rig.s" "l_pinky_03_rig.is";
connectAttr "l_wrist_rig.s" "l_thumb_01_rig.is";
connectAttr "l_thumb_01_rig.s" "l_thumb_02_rig.is";
connectAttr "l_thumb_02_rig.s" "l_thumb_03_rig.is";
connectAttr "l_wrist_rig.pim" "l_wrist_rig_pointConstraint1.cpim";
connectAttr "l_wrist_rig.rp" "l_wrist_rig_pointConstraint1.crp";
connectAttr "l_wrist_rig.rpt" "l_wrist_rig_pointConstraint1.crt";
connectAttr "l_wrist_rig_ik.t" "l_wrist_rig_pointConstraint1.tg[0].tt";
connectAttr "l_wrist_rig_ik.rp" "l_wrist_rig_pointConstraint1.tg[0].trp";
connectAttr "l_wrist_rig_ik.rpt" "l_wrist_rig_pointConstraint1.tg[0].trt";
connectAttr "l_wrist_rig_ik.pm" "l_wrist_rig_pointConstraint1.tg[0].tpm";
connectAttr "l_wrist_rig_pointConstraint1.w0" "l_wrist_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "l_wrist_rig_fk.t" "l_wrist_rig_pointConstraint1.tg[1].tt";
connectAttr "l_wrist_rig_fk.rp" "l_wrist_rig_pointConstraint1.tg[1].trp";
connectAttr "l_wrist_rig_fk.rpt" "l_wrist_rig_pointConstraint1.tg[1].trt";
connectAttr "l_wrist_rig_fk.pm" "l_wrist_rig_pointConstraint1.tg[1].tpm";
connectAttr "l_wrist_rig_pointConstraint1.w1" "l_wrist_rig_pointConstraint1.tg[1].tw"
		;
connectAttr "l_wrist_rig.blendSpace" "l_wrist_rig_pointConstraint1.w0";
connectAttr "l_wrist_rig_fk_point_reverse.ox" "l_wrist_rig_pointConstraint1.w1";
connectAttr "l_wrist_rig.ro" "l_wrist_rig_orientConstraint1.cro";
connectAttr "l_wrist_rig.pim" "l_wrist_rig_orientConstraint1.cpim";
connectAttr "l_wrist_rig.jo" "l_wrist_rig_orientConstraint1.cjo";
connectAttr "l_wrist_rig.is" "l_wrist_rig_orientConstraint1.is";
connectAttr "l_wrist_rig_ik.r" "l_wrist_rig_orientConstraint1.tg[0].tr";
connectAttr "l_wrist_rig_ik.ro" "l_wrist_rig_orientConstraint1.tg[0].tro";
connectAttr "l_wrist_rig_ik.pm" "l_wrist_rig_orientConstraint1.tg[0].tpm";
connectAttr "l_wrist_rig_ik.jo" "l_wrist_rig_orientConstraint1.tg[0].tjo";
connectAttr "l_wrist_rig_orientConstraint1.w0" "l_wrist_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "l_wrist_rig_fk.r" "l_wrist_rig_orientConstraint1.tg[1].tr";
connectAttr "l_wrist_rig_fk.ro" "l_wrist_rig_orientConstraint1.tg[1].tro";
connectAttr "l_wrist_rig_fk.pm" "l_wrist_rig_orientConstraint1.tg[1].tpm";
connectAttr "l_wrist_rig_fk.jo" "l_wrist_rig_orientConstraint1.tg[1].tjo";
connectAttr "l_wrist_rig_orientConstraint1.w1" "l_wrist_rig_orientConstraint1.tg[1].tw"
		;
connectAttr "l_wrist_rig.blendSpace" "l_wrist_rig_orientConstraint1.w0";
connectAttr "l_wrist_rig_fk_point_OR_reverse.ox" "l_wrist_rig_orientConstraint1.w1"
		;
connectAttr "l_elbow_rig.ro" "l_elbow_rig_parentConstraint1.cro";
connectAttr "l_elbow_rig.pim" "l_elbow_rig_parentConstraint1.cpim";
connectAttr "l_elbow_rig.rp" "l_elbow_rig_parentConstraint1.crp";
connectAttr "l_elbow_rig.rpt" "l_elbow_rig_parentConstraint1.crt";
connectAttr "l_elbow_rig.jo" "l_elbow_rig_parentConstraint1.cjo";
connectAttr "l_elbow_lock_01.t" "l_elbow_rig_parentConstraint1.tg[0].tt";
connectAttr "l_elbow_lock_01.rp" "l_elbow_rig_parentConstraint1.tg[0].trp";
connectAttr "l_elbow_lock_01.rpt" "l_elbow_rig_parentConstraint1.tg[0].trt";
connectAttr "l_elbow_lock_01.r" "l_elbow_rig_parentConstraint1.tg[0].tr";
connectAttr "l_elbow_lock_01.ro" "l_elbow_rig_parentConstraint1.tg[0].tro";
connectAttr "l_elbow_lock_01.s" "l_elbow_rig_parentConstraint1.tg[0].ts";
connectAttr "l_elbow_lock_01.pm" "l_elbow_rig_parentConstraint1.tg[0].tpm";
connectAttr "l_elbow_rig_parentConstraint1.w0" "l_elbow_rig_parentConstraint1.tg[0].tw"
		;
connectAttr "l_shoulder_rig.pim" "l_shoulder_rig_pointConstraint1.cpim";
connectAttr "l_shoulder_rig.rp" "l_shoulder_rig_pointConstraint1.crp";
connectAttr "l_shoulder_rig.rpt" "l_shoulder_rig_pointConstraint1.crt";
connectAttr "l_shoulder_rig_ik.t" "l_shoulder_rig_pointConstraint1.tg[0].tt";
connectAttr "l_shoulder_rig_ik.rp" "l_shoulder_rig_pointConstraint1.tg[0].trp";
connectAttr "l_shoulder_rig_ik.rpt" "l_shoulder_rig_pointConstraint1.tg[0].trt";
connectAttr "l_shoulder_rig_ik.pm" "l_shoulder_rig_pointConstraint1.tg[0].tpm";
connectAttr "l_shoulder_rig_pointConstraint1.w0" "l_shoulder_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "l_shoulder_rig_fk.t" "l_shoulder_rig_pointConstraint1.tg[1].tt";
connectAttr "l_shoulder_rig_fk.rp" "l_shoulder_rig_pointConstraint1.tg[1].trp";
connectAttr "l_shoulder_rig_fk.rpt" "l_shoulder_rig_pointConstraint1.tg[1].trt";
connectAttr "l_shoulder_rig_fk.pm" "l_shoulder_rig_pointConstraint1.tg[1].tpm";
connectAttr "l_shoulder_rig_pointConstraint1.w1" "l_shoulder_rig_pointConstraint1.tg[1].tw"
		;
connectAttr "l_shoulder_rig.blendSpace" "l_shoulder_rig_pointConstraint1.w0";
connectAttr "l_shoulder_rig_fk_point_reverse.ox" "l_shoulder_rig_pointConstraint1.w1"
		;
connectAttr "l_shoulder_rig.ro" "l_shoulder_rig_orientConstraint1.cro";
connectAttr "l_shoulder_rig.pim" "l_shoulder_rig_orientConstraint1.cpim";
connectAttr "l_shoulder_rig.jo" "l_shoulder_rig_orientConstraint1.cjo";
connectAttr "l_shoulder_rig.is" "l_shoulder_rig_orientConstraint1.is";
connectAttr "l_shoulder_rig_ik.r" "l_shoulder_rig_orientConstraint1.tg[0].tr";
connectAttr "l_shoulder_rig_ik.ro" "l_shoulder_rig_orientConstraint1.tg[0].tro";
connectAttr "l_shoulder_rig_ik.pm" "l_shoulder_rig_orientConstraint1.tg[0].tpm";
connectAttr "l_shoulder_rig_ik.jo" "l_shoulder_rig_orientConstraint1.tg[0].tjo";
connectAttr "l_shoulder_rig_orientConstraint1.w0" "l_shoulder_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "l_shoulder_rig_fk.r" "l_shoulder_rig_orientConstraint1.tg[1].tr";
connectAttr "l_shoulder_rig_fk.ro" "l_shoulder_rig_orientConstraint1.tg[1].tro";
connectAttr "l_shoulder_rig_fk.pm" "l_shoulder_rig_orientConstraint1.tg[1].tpm";
connectAttr "l_shoulder_rig_fk.jo" "l_shoulder_rig_orientConstraint1.tg[1].tjo";
connectAttr "l_shoulder_rig_orientConstraint1.w1" "l_shoulder_rig_orientConstraint1.tg[1].tw"
		;
connectAttr "l_shoulder_rig.blendSpace" "l_shoulder_rig_orientConstraint1.w0";
connectAttr "l_shoulder_rig_fk_point_OR_reverse.ox" "l_shoulder_rig_orientConstraint1.w1"
		;
connectAttr "l_clav_rig.s" "l_shoulder_rig_ik.is";
connectAttr "l_shoulder_rig_ik.s" "l_elbow_rig_ik.is";
connectAttr "expression8.out[0]" "l_elbow_rig_ik.tx";
connectAttr "l_elbow_rig_ik.s" "l_wrist_rig_ik.is";
connectAttr "l_wrist_rig_ik_orientConstraint1.crx" "l_wrist_rig_ik.rx";
connectAttr "l_wrist_rig_ik_orientConstraint1.cry" "l_wrist_rig_ik.ry";
connectAttr "l_wrist_rig_ik_orientConstraint1.crz" "l_wrist_rig_ik.rz";
connectAttr "expression9.out[0]" "l_wrist_rig_ik.tx";
connectAttr "l_wrist_rig_ik.ro" "l_wrist_rig_ik_orientConstraint1.cro";
connectAttr "l_wrist_rig_ik.pim" "l_wrist_rig_ik_orientConstraint1.cpim";
connectAttr "l_wrist_rig_ik.jo" "l_wrist_rig_ik_orientConstraint1.cjo";
connectAttr "l_wrist_rig_ik.is" "l_wrist_rig_ik_orientConstraint1.is";
connectAttr "l_wrist_ctrl_01.r" "l_wrist_rig_ik_orientConstraint1.tg[0].tr";
connectAttr "l_wrist_ctrl_01.ro" "l_wrist_rig_ik_orientConstraint1.tg[0].tro";
connectAttr "l_wrist_ctrl_01.pm" "l_wrist_rig_ik_orientConstraint1.tg[0].tpm";
connectAttr "l_wrist_rig_ik_orientConstraint1.w0" "l_wrist_rig_ik_orientConstraint1.tg[0].tw"
		;
connectAttr "l_wrist_rig_ik.tx" "effector2.tx";
connectAttr "l_wrist_rig_ik.ty" "effector2.ty";
connectAttr "l_wrist_rig_ik.tz" "effector2.tz";
connectAttr "l_shoulder_rig_fk_orientConstraint1.crx" "l_shoulder_rig_fk.rx";
connectAttr "l_shoulder_rig_fk_orientConstraint1.cry" "l_shoulder_rig_fk.ry";
connectAttr "l_shoulder_rig_fk_orientConstraint1.crz" "l_shoulder_rig_fk.rz";
connectAttr "l_clav_rig.s" "l_shoulder_rig_fk.is";
connectAttr "l_shoulder_rig_fk_pointConstraint1.ctx" "l_shoulder_rig_fk.tx";
connectAttr "l_shoulder_rig_fk_pointConstraint1.cty" "l_shoulder_rig_fk.ty";
connectAttr "l_shoulder_rig_fk_pointConstraint1.ctz" "l_shoulder_rig_fk.tz";
connectAttr "l_shoulder_rig_fk.s" "l_elbow_rig_fk.is";
connectAttr "l_elbow_rig_fk_orientConstraint1.crx" "l_elbow_rig_fk.rx";
connectAttr "l_elbow_rig_fk_orientConstraint1.cry" "l_elbow_rig_fk.ry";
connectAttr "l_elbow_rig_fk_orientConstraint1.crz" "l_elbow_rig_fk.rz";
connectAttr "l_elbow_rig_fk_pointConstraint1.ctx" "l_elbow_rig_fk.tx";
connectAttr "l_elbow_rig_fk_pointConstraint1.cty" "l_elbow_rig_fk.ty";
connectAttr "l_elbow_rig_fk_pointConstraint1.ctz" "l_elbow_rig_fk.tz";
connectAttr "l_elbow_rig_fk.s" "l_wrist_rig_fk.is";
connectAttr "l_wrist_rig_fk_orientConstraint1.crx" "l_wrist_rig_fk.rx";
connectAttr "l_wrist_rig_fk_orientConstraint1.cry" "l_wrist_rig_fk.ry";
connectAttr "l_wrist_rig_fk_orientConstraint1.crz" "l_wrist_rig_fk.rz";
connectAttr "l_wrist_rig_fk_pointConstraint1.ctx" "l_wrist_rig_fk.tx";
connectAttr "l_wrist_rig_fk_pointConstraint1.cty" "l_wrist_rig_fk.ty";
connectAttr "l_wrist_rig_fk_pointConstraint1.ctz" "l_wrist_rig_fk.tz";
connectAttr "l_wrist_rig_fk.ro" "l_wrist_rig_fk_orientConstraint1.cro";
connectAttr "l_wrist_rig_fk.pim" "l_wrist_rig_fk_orientConstraint1.cpim";
connectAttr "l_wrist_rig_fk.jo" "l_wrist_rig_fk_orientConstraint1.cjo";
connectAttr "l_wrist_rig_fk.is" "l_wrist_rig_fk_orientConstraint1.is";
connectAttr "l_wrist_fk_ctrl_01.r" "l_wrist_rig_fk_orientConstraint1.tg[0].tr";
connectAttr "l_wrist_fk_ctrl_01.ro" "l_wrist_rig_fk_orientConstraint1.tg[0].tro"
		;
connectAttr "l_wrist_fk_ctrl_01.pm" "l_wrist_rig_fk_orientConstraint1.tg[0].tpm"
		;
connectAttr "l_wrist_rig_fk_orientConstraint1.w0" "l_wrist_rig_fk_orientConstraint1.tg[0].tw"
		;
connectAttr "l_wrist_rig_fk.pim" "l_wrist_rig_fk_pointConstraint1.cpim";
connectAttr "l_wrist_rig_fk.rp" "l_wrist_rig_fk_pointConstraint1.crp";
connectAttr "l_wrist_rig_fk.rpt" "l_wrist_rig_fk_pointConstraint1.crt";
connectAttr "l_wrist_fk_ctrl_01.t" "l_wrist_rig_fk_pointConstraint1.tg[0].tt";
connectAttr "l_wrist_fk_ctrl_01.rp" "l_wrist_rig_fk_pointConstraint1.tg[0].trp";
connectAttr "l_wrist_fk_ctrl_01.rpt" "l_wrist_rig_fk_pointConstraint1.tg[0].trt"
		;
connectAttr "l_wrist_fk_ctrl_01.pm" "l_wrist_rig_fk_pointConstraint1.tg[0].tpm";
connectAttr "l_wrist_rig_fk_pointConstraint1.w0" "l_wrist_rig_fk_pointConstraint1.tg[0].tw"
		;
connectAttr "l_elbow_rig_fk.ro" "l_elbow_rig_fk_orientConstraint1.cro";
connectAttr "l_elbow_rig_fk.pim" "l_elbow_rig_fk_orientConstraint1.cpim";
connectAttr "l_elbow_rig_fk.jo" "l_elbow_rig_fk_orientConstraint1.cjo";
connectAttr "l_elbow_rig_fk.is" "l_elbow_rig_fk_orientConstraint1.is";
connectAttr "l_elbow_fk_ctrl_01.r" "l_elbow_rig_fk_orientConstraint1.tg[0].tr";
connectAttr "l_elbow_fk_ctrl_01.ro" "l_elbow_rig_fk_orientConstraint1.tg[0].tro"
		;
connectAttr "l_elbow_fk_ctrl_01.pm" "l_elbow_rig_fk_orientConstraint1.tg[0].tpm"
		;
connectAttr "l_elbow_rig_fk_orientConstraint1.w0" "l_elbow_rig_fk_orientConstraint1.tg[0].tw"
		;
connectAttr "l_elbow_rig_fk.pim" "l_elbow_rig_fk_pointConstraint1.cpim";
connectAttr "l_elbow_rig_fk.rp" "l_elbow_rig_fk_pointConstraint1.crp";
connectAttr "l_elbow_rig_fk.rpt" "l_elbow_rig_fk_pointConstraint1.crt";
connectAttr "l_elbow_fk_ctrl_01.t" "l_elbow_rig_fk_pointConstraint1.tg[0].tt";
connectAttr "l_elbow_fk_ctrl_01.rp" "l_elbow_rig_fk_pointConstraint1.tg[0].trp";
connectAttr "l_elbow_fk_ctrl_01.rpt" "l_elbow_rig_fk_pointConstraint1.tg[0].trt"
		;
connectAttr "l_elbow_fk_ctrl_01.pm" "l_elbow_rig_fk_pointConstraint1.tg[0].tpm";
connectAttr "l_elbow_rig_fk_pointConstraint1.w0" "l_elbow_rig_fk_pointConstraint1.tg[0].tw"
		;
connectAttr "l_shoulder_rig_fk.ro" "l_shoulder_rig_fk_orientConstraint1.cro";
connectAttr "l_shoulder_rig_fk.pim" "l_shoulder_rig_fk_orientConstraint1.cpim";
connectAttr "l_shoulder_rig_fk.jo" "l_shoulder_rig_fk_orientConstraint1.cjo";
connectAttr "l_shoulder_rig_fk.is" "l_shoulder_rig_fk_orientConstraint1.is";
connectAttr "l_shoulder_fk_ctrl_01.r" "l_shoulder_rig_fk_orientConstraint1.tg[0].tr"
		;
connectAttr "l_shoulder_fk_ctrl_01.ro" "l_shoulder_rig_fk_orientConstraint1.tg[0].tro"
		;
connectAttr "l_shoulder_fk_ctrl_01.pm" "l_shoulder_rig_fk_orientConstraint1.tg[0].tpm"
		;
connectAttr "l_shoulder_rig_fk_orientConstraint1.w0" "l_shoulder_rig_fk_orientConstraint1.tg[0].tw"
		;
connectAttr "l_shoulder_rig_fk.pim" "l_shoulder_rig_fk_pointConstraint1.cpim";
connectAttr "l_shoulder_rig_fk.rp" "l_shoulder_rig_fk_pointConstraint1.crp";
connectAttr "l_shoulder_rig_fk.rpt" "l_shoulder_rig_fk_pointConstraint1.crt";
connectAttr "l_shoulder_fk_ctrl_01.t" "l_shoulder_rig_fk_pointConstraint1.tg[0].tt"
		;
connectAttr "l_shoulder_fk_ctrl_01.rp" "l_shoulder_rig_fk_pointConstraint1.tg[0].trp"
		;
connectAttr "l_shoulder_fk_ctrl_01.rpt" "l_shoulder_rig_fk_pointConstraint1.tg[0].trt"
		;
connectAttr "l_shoulder_fk_ctrl_01.pm" "l_shoulder_rig_fk_pointConstraint1.tg[0].tpm"
		;
connectAttr "l_shoulder_rig_fk_pointConstraint1.w0" "l_shoulder_rig_fk_pointConstraint1.tg[0].tw"
		;
connectAttr "l_clav_rig.s" "l_shoulderFollow_rig.is";
connectAttr "l_clav_rig.ro" "l_clav_rig_orientConstraint1.cro";
connectAttr "l_clav_rig.pim" "l_clav_rig_orientConstraint1.cpim";
connectAttr "l_clav_rig.jo" "l_clav_rig_orientConstraint1.cjo";
connectAttr "l_clav_rig.is" "l_clav_rig_orientConstraint1.is";
connectAttr "l_clav_rig_ctrl_01.r" "l_clav_rig_orientConstraint1.tg[0].tr";
connectAttr "l_clav_rig_ctrl_01.ro" "l_clav_rig_orientConstraint1.tg[0].tro";
connectAttr "l_clav_rig_ctrl_01.pm" "l_clav_rig_orientConstraint1.tg[0].tpm";
connectAttr "l_clav_rig_orientConstraint1.w0" "l_clav_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "l_clav_rig.pim" "l_clav_rig_pointConstraint1.cpim";
connectAttr "l_clav_rig.rp" "l_clav_rig_pointConstraint1.crp";
connectAttr "l_clav_rig.rpt" "l_clav_rig_pointConstraint1.crt";
connectAttr "l_clav_rig_ctrl_01.t" "l_clav_rig_pointConstraint1.tg[0].tt";
connectAttr "l_clav_rig_ctrl_01.rp" "l_clav_rig_pointConstraint1.tg[0].trp";
connectAttr "l_clav_rig_ctrl_01.rpt" "l_clav_rig_pointConstraint1.tg[0].trt";
connectAttr "l_clav_rig_ctrl_01.pm" "l_clav_rig_pointConstraint1.tg[0].tpm";
connectAttr "l_clav_rig_pointConstraint1.w0" "l_clav_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "r_clav_rig_orientConstraint1.crx" "r_clav_rig.rx";
connectAttr "r_clav_rig_orientConstraint1.cry" "r_clav_rig.ry";
connectAttr "r_clav_rig_orientConstraint1.crz" "r_clav_rig.rz";
connectAttr "spineParent_rig.s" "r_clav_rig.is";
connectAttr "r_clav_rig_pointConstraint1.ctx" "r_clav_rig.tx";
connectAttr "r_clav_rig_pointConstraint1.cty" "r_clav_rig.ty";
connectAttr "r_clav_rig_pointConstraint1.ctz" "r_clav_rig.tz";
connectAttr "r_shoulder_rig_orientConstraint1.crx" "r_shoulder_rig.rx";
connectAttr "r_shoulder_rig_orientConstraint1.cry" "r_shoulder_rig.ry";
connectAttr "r_shoulder_rig_orientConstraint1.crz" "r_shoulder_rig.rz";
connectAttr "r_clav_rig.s" "r_shoulder_rig.is";
connectAttr "r_shoulder_rig_pointConstraint1.ctx" "r_shoulder_rig.tx";
connectAttr "r_shoulder_rig_pointConstraint1.cty" "r_shoulder_rig.ty";
connectAttr "r_shoulder_rig_pointConstraint1.ctz" "r_shoulder_rig.tz";
connectAttr "r_arm_ik_switch.IkFkSwitch" "r_shoulder_rig.blendSpace";
connectAttr "r_elbow_rig_parentConstraint1.crx" "r_elbow_rig.rx";
connectAttr "r_elbow_rig_parentConstraint1.cry" "r_elbow_rig.ry";
connectAttr "r_elbow_rig_parentConstraint1.crz" "r_elbow_rig.rz";
connectAttr "r_shoulder_rig.s" "r_elbow_rig.is";
connectAttr "r_elbow_rig_parentConstraint1.ctx" "r_elbow_rig.tx";
connectAttr "r_elbow_rig_parentConstraint1.cty" "r_elbow_rig.ty";
connectAttr "r_elbow_rig_parentConstraint1.ctz" "r_elbow_rig.tz";
connectAttr "r_wrist_rig_orientConstraint1.crx" "r_wrist_rig.rx";
connectAttr "r_wrist_rig_orientConstraint1.cry" "r_wrist_rig.ry";
connectAttr "r_wrist_rig_orientConstraint1.crz" "r_wrist_rig.rz";
connectAttr "r_elbow_rig.s" "r_wrist_rig.is";
connectAttr "r_wrist_rig_pointConstraint1.ctx" "r_wrist_rig.tx";
connectAttr "r_wrist_rig_pointConstraint1.cty" "r_wrist_rig.ty";
connectAttr "r_wrist_rig_pointConstraint1.ctz" "r_wrist_rig.tz";
connectAttr "r_arm_ik_switch.IkFkSwitch" "r_wrist_rig.blendSpace";
connectAttr "r_wrist_rig.s" "r_index_01_rig.is";
connectAttr "r_index_01_rig.s" "r_index_02_rig.is";
connectAttr "r_index_02_rig.s" "r_index_03_rig.is";
connectAttr "r_wrist_rig.s" "r_pinky_01_rig.is";
connectAttr "r_pinky_01_rig.s" "r_pinky_02_rig.is";
connectAttr "r_pinky_02_rig.s" "r_pinky_03_rig.is";
connectAttr "r_wrist_rig.s" "r_thumb_01_rig.is";
connectAttr "r_thumb_01_rig.s" "r_thumb_02_rig.is";
connectAttr "r_thumb_02_rig.s" "r_thumb_03_rig.is";
connectAttr "r_wrist_rig.pim" "r_wrist_rig_pointConstraint1.cpim";
connectAttr "r_wrist_rig.rp" "r_wrist_rig_pointConstraint1.crp";
connectAttr "r_wrist_rig.rpt" "r_wrist_rig_pointConstraint1.crt";
connectAttr "r_wrist_rig_ik.t" "r_wrist_rig_pointConstraint1.tg[0].tt";
connectAttr "r_wrist_rig_ik.rp" "r_wrist_rig_pointConstraint1.tg[0].trp";
connectAttr "r_wrist_rig_ik.rpt" "r_wrist_rig_pointConstraint1.tg[0].trt";
connectAttr "r_wrist_rig_ik.pm" "r_wrist_rig_pointConstraint1.tg[0].tpm";
connectAttr "r_wrist_rig_pointConstraint1.w0" "r_wrist_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "r_wrist_rig_fk.t" "r_wrist_rig_pointConstraint1.tg[1].tt";
connectAttr "r_wrist_rig_fk.rp" "r_wrist_rig_pointConstraint1.tg[1].trp";
connectAttr "r_wrist_rig_fk.rpt" "r_wrist_rig_pointConstraint1.tg[1].trt";
connectAttr "r_wrist_rig_fk.pm" "r_wrist_rig_pointConstraint1.tg[1].tpm";
connectAttr "r_wrist_rig_pointConstraint1.w1" "r_wrist_rig_pointConstraint1.tg[1].tw"
		;
connectAttr "r_wrist_rig.blendSpace" "r_wrist_rig_pointConstraint1.w0";
connectAttr "r_wrist_rig_fk_point_reverse.ox" "r_wrist_rig_pointConstraint1.w1";
connectAttr "r_wrist_rig.ro" "r_wrist_rig_orientConstraint1.cro";
connectAttr "r_wrist_rig.pim" "r_wrist_rig_orientConstraint1.cpim";
connectAttr "r_wrist_rig.jo" "r_wrist_rig_orientConstraint1.cjo";
connectAttr "r_wrist_rig.is" "r_wrist_rig_orientConstraint1.is";
connectAttr "r_wrist_rig_ik.r" "r_wrist_rig_orientConstraint1.tg[0].tr";
connectAttr "r_wrist_rig_ik.ro" "r_wrist_rig_orientConstraint1.tg[0].tro";
connectAttr "r_wrist_rig_ik.pm" "r_wrist_rig_orientConstraint1.tg[0].tpm";
connectAttr "r_wrist_rig_ik.jo" "r_wrist_rig_orientConstraint1.tg[0].tjo";
connectAttr "r_wrist_rig_orientConstraint1.w0" "r_wrist_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "r_wrist_rig_fk.r" "r_wrist_rig_orientConstraint1.tg[1].tr";
connectAttr "r_wrist_rig_fk.ro" "r_wrist_rig_orientConstraint1.tg[1].tro";
connectAttr "r_wrist_rig_fk.pm" "r_wrist_rig_orientConstraint1.tg[1].tpm";
connectAttr "r_wrist_rig_fk.jo" "r_wrist_rig_orientConstraint1.tg[1].tjo";
connectAttr "r_wrist_rig_orientConstraint1.w1" "r_wrist_rig_orientConstraint1.tg[1].tw"
		;
connectAttr "r_wrist_rig.blendSpace" "r_wrist_rig_orientConstraint1.w0";
connectAttr "r_wrist_rig_fk_point_OR_reverse.ox" "r_wrist_rig_orientConstraint1.w1"
		;
connectAttr "r_elbow_rig.ro" "r_elbow_rig_parentConstraint1.cro";
connectAttr "r_elbow_rig.pim" "r_elbow_rig_parentConstraint1.cpim";
connectAttr "r_elbow_rig.rp" "r_elbow_rig_parentConstraint1.crp";
connectAttr "r_elbow_rig.rpt" "r_elbow_rig_parentConstraint1.crt";
connectAttr "r_elbow_rig.jo" "r_elbow_rig_parentConstraint1.cjo";
connectAttr "r_elbow_lock_01.t" "r_elbow_rig_parentConstraint1.tg[0].tt";
connectAttr "r_elbow_lock_01.rp" "r_elbow_rig_parentConstraint1.tg[0].trp";
connectAttr "r_elbow_lock_01.rpt" "r_elbow_rig_parentConstraint1.tg[0].trt";
connectAttr "r_elbow_lock_01.r" "r_elbow_rig_parentConstraint1.tg[0].tr";
connectAttr "r_elbow_lock_01.ro" "r_elbow_rig_parentConstraint1.tg[0].tro";
connectAttr "r_elbow_lock_01.s" "r_elbow_rig_parentConstraint1.tg[0].ts";
connectAttr "r_elbow_lock_01.pm" "r_elbow_rig_parentConstraint1.tg[0].tpm";
connectAttr "r_elbow_rig_parentConstraint1.w0" "r_elbow_rig_parentConstraint1.tg[0].tw"
		;
connectAttr "r_shoulder_rig.pim" "r_shoulder_rig_pointConstraint1.cpim";
connectAttr "r_shoulder_rig.rp" "r_shoulder_rig_pointConstraint1.crp";
connectAttr "r_shoulder_rig.rpt" "r_shoulder_rig_pointConstraint1.crt";
connectAttr "r_shoulder_rig_ik.t" "r_shoulder_rig_pointConstraint1.tg[0].tt";
connectAttr "r_shoulder_rig_ik.rp" "r_shoulder_rig_pointConstraint1.tg[0].trp";
connectAttr "r_shoulder_rig_ik.rpt" "r_shoulder_rig_pointConstraint1.tg[0].trt";
connectAttr "r_shoulder_rig_ik.pm" "r_shoulder_rig_pointConstraint1.tg[0].tpm";
connectAttr "r_shoulder_rig_pointConstraint1.w0" "r_shoulder_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "r_shoulder_rig_fk.t" "r_shoulder_rig_pointConstraint1.tg[1].tt";
connectAttr "r_shoulder_rig_fk.rp" "r_shoulder_rig_pointConstraint1.tg[1].trp";
connectAttr "r_shoulder_rig_fk.rpt" "r_shoulder_rig_pointConstraint1.tg[1].trt";
connectAttr "r_shoulder_rig_fk.pm" "r_shoulder_rig_pointConstraint1.tg[1].tpm";
connectAttr "r_shoulder_rig_pointConstraint1.w1" "r_shoulder_rig_pointConstraint1.tg[1].tw"
		;
connectAttr "r_shoulder_rig.blendSpace" "r_shoulder_rig_pointConstraint1.w0";
connectAttr "r_shoulder_rig_fk_point_reverse.ox" "r_shoulder_rig_pointConstraint1.w1"
		;
connectAttr "r_shoulder_rig.ro" "r_shoulder_rig_orientConstraint1.cro";
connectAttr "r_shoulder_rig.pim" "r_shoulder_rig_orientConstraint1.cpim";
connectAttr "r_shoulder_rig.jo" "r_shoulder_rig_orientConstraint1.cjo";
connectAttr "r_shoulder_rig.is" "r_shoulder_rig_orientConstraint1.is";
connectAttr "r_shoulder_rig_ik.r" "r_shoulder_rig_orientConstraint1.tg[0].tr";
connectAttr "r_shoulder_rig_ik.ro" "r_shoulder_rig_orientConstraint1.tg[0].tro";
connectAttr "r_shoulder_rig_ik.pm" "r_shoulder_rig_orientConstraint1.tg[0].tpm";
connectAttr "r_shoulder_rig_ik.jo" "r_shoulder_rig_orientConstraint1.tg[0].tjo";
connectAttr "r_shoulder_rig_orientConstraint1.w0" "r_shoulder_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "r_shoulder_rig_fk.r" "r_shoulder_rig_orientConstraint1.tg[1].tr";
connectAttr "r_shoulder_rig_fk.ro" "r_shoulder_rig_orientConstraint1.tg[1].tro";
connectAttr "r_shoulder_rig_fk.pm" "r_shoulder_rig_orientConstraint1.tg[1].tpm";
connectAttr "r_shoulder_rig_fk.jo" "r_shoulder_rig_orientConstraint1.tg[1].tjo";
connectAttr "r_shoulder_rig_orientConstraint1.w1" "r_shoulder_rig_orientConstraint1.tg[1].tw"
		;
connectAttr "r_shoulder_rig.blendSpace" "r_shoulder_rig_orientConstraint1.w0";
connectAttr "r_shoulder_rig_fk_point_OR_reverse.ox" "r_shoulder_rig_orientConstraint1.w1"
		;
connectAttr "r_clav_rig.s" "r_shoulder_rig_ik.is";
connectAttr "r_shoulder_rig_ik.s" "r_elbow_rig_ik.is";
connectAttr "expression10.out[0]" "r_elbow_rig_ik.tx";
connectAttr "r_elbow_rig_ik.s" "r_wrist_rig_ik.is";
connectAttr "r_wrist_rig_ik_orientConstraint1.crx" "r_wrist_rig_ik.rx";
connectAttr "r_wrist_rig_ik_orientConstraint1.cry" "r_wrist_rig_ik.ry";
connectAttr "r_wrist_rig_ik_orientConstraint1.crz" "r_wrist_rig_ik.rz";
connectAttr "expression11.out[0]" "r_wrist_rig_ik.tx";
connectAttr "r_wrist_rig_ik.ro" "r_wrist_rig_ik_orientConstraint1.cro";
connectAttr "r_wrist_rig_ik.pim" "r_wrist_rig_ik_orientConstraint1.cpim";
connectAttr "r_wrist_rig_ik.jo" "r_wrist_rig_ik_orientConstraint1.cjo";
connectAttr "r_wrist_rig_ik.is" "r_wrist_rig_ik_orientConstraint1.is";
connectAttr "r_wrist_ctrl_01.r" "r_wrist_rig_ik_orientConstraint1.tg[0].tr";
connectAttr "r_wrist_ctrl_01.ro" "r_wrist_rig_ik_orientConstraint1.tg[0].tro";
connectAttr "r_wrist_ctrl_01.pm" "r_wrist_rig_ik_orientConstraint1.tg[0].tpm";
connectAttr "r_wrist_rig_ik_orientConstraint1.w0" "r_wrist_rig_ik_orientConstraint1.tg[0].tw"
		;
connectAttr "r_wrist_rig_ik.tx" "effector3.tx";
connectAttr "r_wrist_rig_ik.ty" "effector3.ty";
connectAttr "r_wrist_rig_ik.tz" "effector3.tz";
connectAttr "r_shoulder_rig_fk_orientConstraint1.crx" "r_shoulder_rig_fk.rx";
connectAttr "r_shoulder_rig_fk_orientConstraint1.cry" "r_shoulder_rig_fk.ry";
connectAttr "r_shoulder_rig_fk_orientConstraint1.crz" "r_shoulder_rig_fk.rz";
connectAttr "r_clav_rig.s" "r_shoulder_rig_fk.is";
connectAttr "r_shoulder_rig_fk_pointConstraint1.ctx" "r_shoulder_rig_fk.tx";
connectAttr "r_shoulder_rig_fk_pointConstraint1.cty" "r_shoulder_rig_fk.ty";
connectAttr "r_shoulder_rig_fk_pointConstraint1.ctz" "r_shoulder_rig_fk.tz";
connectAttr "r_shoulder_rig_fk.s" "r_elbow_rig_fk.is";
connectAttr "r_elbow_rig_fk_orientConstraint1.crx" "r_elbow_rig_fk.rx";
connectAttr "r_elbow_rig_fk_orientConstraint1.cry" "r_elbow_rig_fk.ry";
connectAttr "r_elbow_rig_fk_orientConstraint1.crz" "r_elbow_rig_fk.rz";
connectAttr "r_elbow_rig_fk_pointConstraint1.ctx" "r_elbow_rig_fk.tx";
connectAttr "r_elbow_rig_fk_pointConstraint1.cty" "r_elbow_rig_fk.ty";
connectAttr "r_elbow_rig_fk_pointConstraint1.ctz" "r_elbow_rig_fk.tz";
connectAttr "r_elbow_rig_fk.s" "r_wrist_rig_fk.is";
connectAttr "r_wrist_rig_fk_orientConstraint1.crx" "r_wrist_rig_fk.rx";
connectAttr "r_wrist_rig_fk_orientConstraint1.cry" "r_wrist_rig_fk.ry";
connectAttr "r_wrist_rig_fk_orientConstraint1.crz" "r_wrist_rig_fk.rz";
connectAttr "r_wrist_rig_fk_pointConstraint1.ctx" "r_wrist_rig_fk.tx";
connectAttr "r_wrist_rig_fk_pointConstraint1.cty" "r_wrist_rig_fk.ty";
connectAttr "r_wrist_rig_fk_pointConstraint1.ctz" "r_wrist_rig_fk.tz";
connectAttr "r_wrist_rig_fk.ro" "r_wrist_rig_fk_orientConstraint1.cro";
connectAttr "r_wrist_rig_fk.pim" "r_wrist_rig_fk_orientConstraint1.cpim";
connectAttr "r_wrist_rig_fk.jo" "r_wrist_rig_fk_orientConstraint1.cjo";
connectAttr "r_wrist_rig_fk.is" "r_wrist_rig_fk_orientConstraint1.is";
connectAttr "r_wrist_fk_ctrl_01.r" "r_wrist_rig_fk_orientConstraint1.tg[0].tr";
connectAttr "r_wrist_fk_ctrl_01.ro" "r_wrist_rig_fk_orientConstraint1.tg[0].tro"
		;
connectAttr "r_wrist_fk_ctrl_01.pm" "r_wrist_rig_fk_orientConstraint1.tg[0].tpm"
		;
connectAttr "r_wrist_rig_fk_orientConstraint1.w0" "r_wrist_rig_fk_orientConstraint1.tg[0].tw"
		;
connectAttr "r_wrist_rig_fk.pim" "r_wrist_rig_fk_pointConstraint1.cpim";
connectAttr "r_wrist_rig_fk.rp" "r_wrist_rig_fk_pointConstraint1.crp";
connectAttr "r_wrist_rig_fk.rpt" "r_wrist_rig_fk_pointConstraint1.crt";
connectAttr "r_wrist_fk_ctrl_01.t" "r_wrist_rig_fk_pointConstraint1.tg[0].tt";
connectAttr "r_wrist_fk_ctrl_01.rp" "r_wrist_rig_fk_pointConstraint1.tg[0].trp";
connectAttr "r_wrist_fk_ctrl_01.rpt" "r_wrist_rig_fk_pointConstraint1.tg[0].trt"
		;
connectAttr "r_wrist_fk_ctrl_01.pm" "r_wrist_rig_fk_pointConstraint1.tg[0].tpm";
connectAttr "r_wrist_rig_fk_pointConstraint1.w0" "r_wrist_rig_fk_pointConstraint1.tg[0].tw"
		;
connectAttr "r_elbow_rig_fk.ro" "r_elbow_rig_fk_orientConstraint1.cro";
connectAttr "r_elbow_rig_fk.pim" "r_elbow_rig_fk_orientConstraint1.cpim";
connectAttr "r_elbow_rig_fk.jo" "r_elbow_rig_fk_orientConstraint1.cjo";
connectAttr "r_elbow_rig_fk.is" "r_elbow_rig_fk_orientConstraint1.is";
connectAttr "r_elbow_fk_ctrl_01.r" "r_elbow_rig_fk_orientConstraint1.tg[0].tr";
connectAttr "r_elbow_fk_ctrl_01.ro" "r_elbow_rig_fk_orientConstraint1.tg[0].tro"
		;
connectAttr "r_elbow_fk_ctrl_01.pm" "r_elbow_rig_fk_orientConstraint1.tg[0].tpm"
		;
connectAttr "r_elbow_rig_fk_orientConstraint1.w0" "r_elbow_rig_fk_orientConstraint1.tg[0].tw"
		;
connectAttr "r_elbow_rig_fk.pim" "r_elbow_rig_fk_pointConstraint1.cpim";
connectAttr "r_elbow_rig_fk.rp" "r_elbow_rig_fk_pointConstraint1.crp";
connectAttr "r_elbow_rig_fk.rpt" "r_elbow_rig_fk_pointConstraint1.crt";
connectAttr "r_elbow_fk_ctrl_01.t" "r_elbow_rig_fk_pointConstraint1.tg[0].tt";
connectAttr "r_elbow_fk_ctrl_01.rp" "r_elbow_rig_fk_pointConstraint1.tg[0].trp";
connectAttr "r_elbow_fk_ctrl_01.rpt" "r_elbow_rig_fk_pointConstraint1.tg[0].trt"
		;
connectAttr "r_elbow_fk_ctrl_01.pm" "r_elbow_rig_fk_pointConstraint1.tg[0].tpm";
connectAttr "r_elbow_rig_fk_pointConstraint1.w0" "r_elbow_rig_fk_pointConstraint1.tg[0].tw"
		;
connectAttr "r_shoulder_rig_fk.ro" "r_shoulder_rig_fk_orientConstraint1.cro";
connectAttr "r_shoulder_rig_fk.pim" "r_shoulder_rig_fk_orientConstraint1.cpim";
connectAttr "r_shoulder_rig_fk.jo" "r_shoulder_rig_fk_orientConstraint1.cjo";
connectAttr "r_shoulder_rig_fk.is" "r_shoulder_rig_fk_orientConstraint1.is";
connectAttr "r_shoulder_fk_ctrl_01.r" "r_shoulder_rig_fk_orientConstraint1.tg[0].tr"
		;
connectAttr "r_shoulder_fk_ctrl_01.ro" "r_shoulder_rig_fk_orientConstraint1.tg[0].tro"
		;
connectAttr "r_shoulder_fk_ctrl_01.pm" "r_shoulder_rig_fk_orientConstraint1.tg[0].tpm"
		;
connectAttr "r_shoulder_rig_fk_orientConstraint1.w0" "r_shoulder_rig_fk_orientConstraint1.tg[0].tw"
		;
connectAttr "r_shoulder_rig_fk.pim" "r_shoulder_rig_fk_pointConstraint1.cpim";
connectAttr "r_shoulder_rig_fk.rp" "r_shoulder_rig_fk_pointConstraint1.crp";
connectAttr "r_shoulder_rig_fk.rpt" "r_shoulder_rig_fk_pointConstraint1.crt";
connectAttr "r_shoulder_fk_ctrl_01.t" "r_shoulder_rig_fk_pointConstraint1.tg[0].tt"
		;
connectAttr "r_shoulder_fk_ctrl_01.rp" "r_shoulder_rig_fk_pointConstraint1.tg[0].trp"
		;
connectAttr "r_shoulder_fk_ctrl_01.rpt" "r_shoulder_rig_fk_pointConstraint1.tg[0].trt"
		;
connectAttr "r_shoulder_fk_ctrl_01.pm" "r_shoulder_rig_fk_pointConstraint1.tg[0].tpm"
		;
connectAttr "r_shoulder_rig_fk_pointConstraint1.w0" "r_shoulder_rig_fk_pointConstraint1.tg[0].tw"
		;
connectAttr "r_clav_rig.s" "r_shoulderFollow_rig.is";
connectAttr "r_clav_rig.ro" "r_clav_rig_orientConstraint1.cro";
connectAttr "r_clav_rig.pim" "r_clav_rig_orientConstraint1.cpim";
connectAttr "r_clav_rig.jo" "r_clav_rig_orientConstraint1.cjo";
connectAttr "r_clav_rig.is" "r_clav_rig_orientConstraint1.is";
connectAttr "r_clav_rig_ctrl_01.r" "r_clav_rig_orientConstraint1.tg[0].tr";
connectAttr "r_clav_rig_ctrl_01.ro" "r_clav_rig_orientConstraint1.tg[0].tro";
connectAttr "r_clav_rig_ctrl_01.pm" "r_clav_rig_orientConstraint1.tg[0].tpm";
connectAttr "r_clav_rig_orientConstraint1.w0" "r_clav_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "r_clav_rig.pim" "r_clav_rig_pointConstraint1.cpim";
connectAttr "r_clav_rig.rp" "r_clav_rig_pointConstraint1.crp";
connectAttr "r_clav_rig.rpt" "r_clav_rig_pointConstraint1.crt";
connectAttr "r_clav_rig_ctrl_01.t" "r_clav_rig_pointConstraint1.tg[0].tt";
connectAttr "r_clav_rig_ctrl_01.rp" "r_clav_rig_pointConstraint1.tg[0].trp";
connectAttr "r_clav_rig_ctrl_01.rpt" "r_clav_rig_pointConstraint1.tg[0].trt";
connectAttr "r_clav_rig_ctrl_01.pm" "r_clav_rig_pointConstraint1.tg[0].tpm";
connectAttr "r_clav_rig_pointConstraint1.w0" "r_clav_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "spineParent_rig.pim" "spineParent_rig_pointConstraint1.cpim";
connectAttr "spineParent_rig.rp" "spineParent_rig_pointConstraint1.crp";
connectAttr "spineParent_rig.rpt" "spineParent_rig_pointConstraint1.crt";
connectAttr "topSpine_ctrl.t" "spineParent_rig_pointConstraint1.tg[0].tt";
connectAttr "topSpine_ctrl.rp" "spineParent_rig_pointConstraint1.tg[0].trp";
connectAttr "topSpine_ctrl.rpt" "spineParent_rig_pointConstraint1.tg[0].trt";
connectAttr "topSpine_ctrl.pm" "spineParent_rig_pointConstraint1.tg[0].tpm";
connectAttr "spineParent_rig_pointConstraint1.w0" "spineParent_rig_pointConstraint1.tg[0].tw"
		;
connectAttr "spineParent_rig.ro" "spineParent_rig_orientConstraint1.cro";
connectAttr "spineParent_rig.pim" "spineParent_rig_orientConstraint1.cpim";
connectAttr "spineParent_rig.jo" "spineParent_rig_orientConstraint1.cjo";
connectAttr "spineParent_rig.is" "spineParent_rig_orientConstraint1.is";
connectAttr "topSpine_ctrl.r" "spineParent_rig_orientConstraint1.tg[0].tr";
connectAttr "topSpine_ctrl.ro" "spineParent_rig_orientConstraint1.tg[0].tro";
connectAttr "topSpine_ctrl.pm" "spineParent_rig_orientConstraint1.tg[0].tpm";
connectAttr "spineParent_rig_orientConstraint1.w0" "spineParent_rig_orientConstraint1.tg[0].tw"
		;
connectAttr "l_hip_grp_orientConstraint1.crx" "l_hip_grp.rx";
connectAttr "l_hip_grp_orientConstraint1.cry" "l_hip_grp.ry";
connectAttr "l_hip_grp_orientConstraint1.crz" "l_hip_grp.rz";
connectAttr "l_hip_ctrl_rev.ox" "l_hip_ctrl.v" -l on;
connectAttr "l_hip_grp.ro" "l_hip_grp_orientConstraint1.cro";
connectAttr "l_hip_grp.pim" "l_hip_grp_orientConstraint1.cpim";
connectAttr "worldPlacement.r" "l_hip_grp_orientConstraint1.tg[0].tr";
connectAttr "worldPlacement.ro" "l_hip_grp_orientConstraint1.tg[0].tro";
connectAttr "worldPlacement.pm" "l_hip_grp_orientConstraint1.tg[0].tpm";
connectAttr "l_hip_grp_orientConstraint1.w0" "l_hip_grp_orientConstraint1.tg[0].tw"
		;
connectAttr "pelvis_ctrl.r" "l_hip_grp_orientConstraint1.tg[1].tr";
connectAttr "pelvis_ctrl.ro" "l_hip_grp_orientConstraint1.tg[1].tro";
connectAttr "pelvis_ctrl.pm" "l_hip_grp_orientConstraint1.tg[1].tpm";
connectAttr "l_hip_grp_orientConstraint1.w1" "l_hip_grp_orientConstraint1.tg[1].tw"
		;
connectAttr "hips_ctrl.r" "l_hip_grp_orientConstraint1.tg[2].tr";
connectAttr "hips_ctrl.ro" "l_hip_grp_orientConstraint1.tg[2].tro";
connectAttr "hips_ctrl.pm" "l_hip_grp_orientConstraint1.tg[2].tpm";
connectAttr "l_hip_grp_orientConstraint1.w2" "l_hip_grp_orientConstraint1.tg[2].tw"
		;
connectAttr "worldPlacement_hipFollowCond.ocr" "l_hip_grp_orientConstraint1.w0";
connectAttr "pelvis_ctrl_hipFollowCond.ocr" "l_hip_grp_orientConstraint1.w1";
connectAttr "hips_ctrl_hipFollowCond.ocr" "l_hip_grp_orientConstraint1.w2";
connectAttr "r_hip_grp_orientConstraint1.crx" "r_hip_grp.rx";
connectAttr "r_hip_grp_orientConstraint1.cry" "r_hip_grp.ry";
connectAttr "r_hip_grp_orientConstraint1.crz" "r_hip_grp.rz";
connectAttr "r_hip_ctrl_rev.ox" "r_hip_ctrl.v" -l on;
connectAttr "r_hip_grp.ro" "r_hip_grp_orientConstraint1.cro";
connectAttr "r_hip_grp.pim" "r_hip_grp_orientConstraint1.cpim";
connectAttr "worldPlacement.r" "r_hip_grp_orientConstraint1.tg[0].tr";
connectAttr "worldPlacement.ro" "r_hip_grp_orientConstraint1.tg[0].tro";
connectAttr "worldPlacement.pm" "r_hip_grp_orientConstraint1.tg[0].tpm";
connectAttr "r_hip_grp_orientConstraint1.w0" "r_hip_grp_orientConstraint1.tg[0].tw"
		;
connectAttr "pelvis_ctrl.r" "r_hip_grp_orientConstraint1.tg[1].tr";
connectAttr "pelvis_ctrl.ro" "r_hip_grp_orientConstraint1.tg[1].tro";
connectAttr "pelvis_ctrl.pm" "r_hip_grp_orientConstraint1.tg[1].tpm";
connectAttr "r_hip_grp_orientConstraint1.w1" "r_hip_grp_orientConstraint1.tg[1].tw"
		;
connectAttr "hips_ctrl.r" "r_hip_grp_orientConstraint1.tg[2].tr";
connectAttr "hips_ctrl.ro" "r_hip_grp_orientConstraint1.tg[2].tro";
connectAttr "hips_ctrl.pm" "r_hip_grp_orientConstraint1.tg[2].tpm";
connectAttr "r_hip_grp_orientConstraint1.w2" "r_hip_grp_orientConstraint1.tg[2].tw"
		;
connectAttr "worldPlacement_hipFollowCond1.ocr" "r_hip_grp_orientConstraint1.w0"
		;
connectAttr "pelvis_ctrl_hipFollowCond1.ocr" "r_hip_grp_orientConstraint1.w1";
connectAttr "hips_ctrl_hipFollowCond1.ocr" "r_hip_grp_orientConstraint1.w2";
connectAttr "l_shoulder_grp_pointConstraint1.ctx" "l_shoulder_grp.tx";
connectAttr "l_shoulder_grp_pointConstraint1.cty" "l_shoulder_grp.ty";
connectAttr "l_shoulder_grp_pointConstraint1.ctz" "l_shoulder_grp.tz";
connectAttr "l_shoulder_grp_orientConstraint1.crx" "l_shoulder_grp.rx";
connectAttr "l_shoulder_grp_orientConstraint1.cry" "l_shoulder_grp.ry";
connectAttr "l_shoulder_grp_orientConstraint1.crz" "l_shoulder_grp.rz";
connectAttr "l_shoulder_fk_ctrl_01_rev.ox" "l_shoulder_fk_ctrl_01.v" -l on;
connectAttr "l_shoulder_grp.pim" "l_shoulder_grp_pointConstraint1.cpim";
connectAttr "l_shoulder_grp.rp" "l_shoulder_grp_pointConstraint1.crp";
connectAttr "l_shoulder_grp.rpt" "l_shoulder_grp_pointConstraint1.crt";
connectAttr "l_shoulderFollow_rig.t" "l_shoulder_grp_pointConstraint1.tg[0].tt";
connectAttr "l_shoulderFollow_rig.rp" "l_shoulder_grp_pointConstraint1.tg[0].trp"
		;
connectAttr "l_shoulderFollow_rig.rpt" "l_shoulder_grp_pointConstraint1.tg[0].trt"
		;
connectAttr "l_shoulderFollow_rig.pm" "l_shoulder_grp_pointConstraint1.tg[0].tpm"
		;
connectAttr "l_shoulder_grp_pointConstraint1.w0" "l_shoulder_grp_pointConstraint1.tg[0].tw"
		;
connectAttr "l_shoulder_grp.ro" "l_shoulder_grp_orientConstraint1.cro";
connectAttr "l_shoulder_grp.pim" "l_shoulder_grp_orientConstraint1.cpim";
connectAttr "topSpine_ctrl.r" "l_shoulder_grp_orientConstraint1.tg[0].tr";
connectAttr "topSpine_ctrl.ro" "l_shoulder_grp_orientConstraint1.tg[0].tro";
connectAttr "topSpine_ctrl.pm" "l_shoulder_grp_orientConstraint1.tg[0].tpm";
connectAttr "l_shoulder_grp_orientConstraint1.w0" "l_shoulder_grp_orientConstraint1.tg[0].tw"
		;
connectAttr "pelvis_ctrl.r" "l_shoulder_grp_orientConstraint1.tg[1].tr";
connectAttr "pelvis_ctrl.ro" "l_shoulder_grp_orientConstraint1.tg[1].tro";
connectAttr "pelvis_ctrl.pm" "l_shoulder_grp_orientConstraint1.tg[1].tpm";
connectAttr "l_shoulder_grp_orientConstraint1.w1" "l_shoulder_grp_orientConstraint1.tg[1].tw"
		;
connectAttr "worldPlacement.r" "l_shoulder_grp_orientConstraint1.tg[2].tr";
connectAttr "worldPlacement.ro" "l_shoulder_grp_orientConstraint1.tg[2].tro";
connectAttr "worldPlacement.pm" "l_shoulder_grp_orientConstraint1.tg[2].tpm";
connectAttr "l_shoulder_grp_orientConstraint1.w2" "l_shoulder_grp_orientConstraint1.tg[2].tw"
		;
connectAttr "topSpine_ctrl_shoFollowCond.ocr" "l_shoulder_grp_orientConstraint1.w0"
		;
connectAttr "pelvis_ctrl_shoFollowCond.ocr" "l_shoulder_grp_orientConstraint1.w1"
		;
connectAttr "worldPlacement_shoFollowCond.ocr" "l_shoulder_grp_orientConstraint1.w2"
		;
connectAttr "r_shoulder_grp_pointConstraint1.ctx" "r_shoulder_grp.tx";
connectAttr "r_shoulder_grp_pointConstraint1.cty" "r_shoulder_grp.ty";
connectAttr "r_shoulder_grp_pointConstraint1.ctz" "r_shoulder_grp.tz";
connectAttr "r_shoulder_grp_orientConstraint1.crx" "r_shoulder_grp.rx";
connectAttr "r_shoulder_grp_orientConstraint1.cry" "r_shoulder_grp.ry";
connectAttr "r_shoulder_grp_orientConstraint1.crz" "r_shoulder_grp.rz";
connectAttr "r_shoulder_fk_ctrl_01_rev.ox" "r_shoulder_fk_ctrl_01.v" -l on;
connectAttr "r_shoulder_grp.pim" "r_shoulder_grp_pointConstraint1.cpim";
connectAttr "r_shoulder_grp.rp" "r_shoulder_grp_pointConstraint1.crp";
connectAttr "r_shoulder_grp.rpt" "r_shoulder_grp_pointConstraint1.crt";
connectAttr "r_shoulderFollow_rig.t" "r_shoulder_grp_pointConstraint1.tg[0].tt";
connectAttr "r_shoulderFollow_rig.rp" "r_shoulder_grp_pointConstraint1.tg[0].trp"
		;
connectAttr "r_shoulderFollow_rig.rpt" "r_shoulder_grp_pointConstraint1.tg[0].trt"
		;
connectAttr "r_shoulderFollow_rig.pm" "r_shoulder_grp_pointConstraint1.tg[0].tpm"
		;
connectAttr "r_shoulder_grp_pointConstraint1.w0" "r_shoulder_grp_pointConstraint1.tg[0].tw"
		;
connectAttr "r_shoulder_grp.ro" "r_shoulder_grp_orientConstraint1.cro";
connectAttr "r_shoulder_grp.pim" "r_shoulder_grp_orientConstraint1.cpim";
connectAttr "topSpine_ctrl.r" "r_shoulder_grp_orientConstraint1.tg[0].tr";
connectAttr "topSpine_ctrl.ro" "r_shoulder_grp_orientConstraint1.tg[0].tro";
connectAttr "topSpine_ctrl.pm" "r_shoulder_grp_orientConstraint1.tg[0].tpm";
connectAttr "r_shoulder_grp_orientConstraint1.w0" "r_shoulder_grp_orientConstraint1.tg[0].tw"
		;
connectAttr "pelvis_ctrl.r" "r_shoulder_grp_orientConstraint1.tg[1].tr";
connectAttr "pelvis_ctrl.ro" "r_shoulder_grp_orientConstraint1.tg[1].tro";
connectAttr "pelvis_ctrl.pm" "r_shoulder_grp_orientConstraint1.tg[1].tpm";
connectAttr "r_shoulder_grp_orientConstraint1.w1" "r_shoulder_grp_orientConstraint1.tg[1].tw"
		;
connectAttr "worldPlacement.r" "r_shoulder_grp_orientConstraint1.tg[2].tr";
connectAttr "worldPlacement.ro" "r_shoulder_grp_orientConstraint1.tg[2].tro";
connectAttr "worldPlacement.pm" "r_shoulder_grp_orientConstraint1.tg[2].tpm";
connectAttr "r_shoulder_grp_orientConstraint1.w2" "r_shoulder_grp_orientConstraint1.tg[2].tw"
		;
connectAttr "topSpine_ctrl_shoFollowCond1.ocr" "r_shoulder_grp_orientConstraint1.w0"
		;
connectAttr "pelvis_ctrl_shoFollowCond1.ocr" "r_shoulder_grp_orientConstraint1.w1"
		;
connectAttr "worldPlacement_shoFollowCond1.ocr" "r_shoulder_grp_orientConstraint1.w2"
		;
connectAttr "worldPlacement.midIkCtrl" "mid_ik_ctrl.v" -l on;
connectAttr "l_arm_ik_switch.IkFkSwitch" "l_wrist_ctrl_01.v" -l on;
connectAttr "l_shoulder_rig_ik.msg" "l_arm_ik.hsj";
connectAttr "effector2.hp" "l_arm_ik.hee";
connectAttr "ikRPsolver.msg" "l_arm_ik.hsv";
connectAttr "l_arm_ik_poleVectorConstraint1.ctx" "l_arm_ik.pvx";
connectAttr "l_arm_ik_poleVectorConstraint1.cty" "l_arm_ik.pvy";
connectAttr "l_arm_ik_poleVectorConstraint1.ctz" "l_arm_ik.pvz";
connectAttr "l_arm_ik.pim" "l_arm_ik_poleVectorConstraint1.cpim";
connectAttr "l_shoulder_rig_ik.pm" "l_arm_ik_poleVectorConstraint1.ps";
connectAttr "l_shoulder_rig_ik.t" "l_arm_ik_poleVectorConstraint1.crp";
connectAttr "l_arm_pv_01.t" "l_arm_ik_poleVectorConstraint1.tg[0].tt";
connectAttr "l_arm_pv_01.rp" "l_arm_ik_poleVectorConstraint1.tg[0].trp";
connectAttr "l_arm_pv_01.rpt" "l_arm_ik_poleVectorConstraint1.tg[0].trt";
connectAttr "l_arm_pv_01.pm" "l_arm_ik_poleVectorConstraint1.tg[0].tpm";
connectAttr "l_arm_ik_poleVectorConstraint1.w0" "l_arm_ik_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "l_arm_ik_switch.IkFkSwitch" "l_arm_pv_01.v" -l on;
connectAttr "r_arm_ik_switch.IkFkSwitch" "r_wrist_ctrl_01.v" -l on;
connectAttr "r_shoulder_rig_ik.msg" "r_arm_ik.hsj";
connectAttr "effector3.hp" "r_arm_ik.hee";
connectAttr "ikRPsolver.msg" "r_arm_ik.hsv";
connectAttr "r_arm_ik_poleVectorConstraint1.ctx" "r_arm_ik.pvx";
connectAttr "r_arm_ik_poleVectorConstraint1.cty" "r_arm_ik.pvy";
connectAttr "r_arm_ik_poleVectorConstraint1.ctz" "r_arm_ik.pvz";
connectAttr "r_arm_ik.pim" "r_arm_ik_poleVectorConstraint1.cpim";
connectAttr "r_shoulder_rig_ik.pm" "r_arm_ik_poleVectorConstraint1.ps";
connectAttr "r_shoulder_rig_ik.t" "r_arm_ik_poleVectorConstraint1.crp";
connectAttr "r_arm_pv_01.t" "r_arm_ik_poleVectorConstraint1.tg[0].tt";
connectAttr "r_arm_pv_01.rp" "r_arm_ik_poleVectorConstraint1.tg[0].trp";
connectAttr "r_arm_pv_01.rpt" "r_arm_ik_poleVectorConstraint1.tg[0].trt";
connectAttr "r_arm_pv_01.pm" "r_arm_ik_poleVectorConstraint1.tg[0].tpm";
connectAttr "r_arm_ik_poleVectorConstraint1.w0" "r_arm_ik_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "r_arm_ik_switch.IkFkSwitch" "r_arm_pv_01.v" -l on;
connectAttr "l_leg_ik_switch.IkFkSwitch" "l_leg_pv_01.v" -l on;
connectAttr "r_leg_ik_switch.IkFkSwitch" "r_leg_pv_01.v" -l on;
connectAttr "l_leg_ik_switch.IkFkSwitch" "l_foot_ik_ctrl.v" -l on;
connectAttr "unitConversion1.o" "l_heelNode.rx";
connectAttr "unitConversion5.o" "l_heelNode.ry";
connectAttr "unitConversion9.o" "l_heelNode.rz";
connectAttr "unitConversion3.o" "l_revToeNode.rx";
connectAttr "unitConversion8.o" "l_revToeNode.ry";
connectAttr "unitConversion2.o" "l_revBallNode.rx";
connectAttr "unitConversion6.o" "l_revBallNode.ry";
connectAttr "unitConversion10.o" "l_revBallNode.rz";
connectAttr "l_hip_rig_ik.msg" "l__leg_ik.hsj";
connectAttr "effector4.hp" "l__leg_ik.hee";
connectAttr "ikRPsolver.msg" "l__leg_ik.hsv";
connectAttr "l__leg_ik_poleVectorConstraint1.ctx" "l__leg_ik.pvx";
connectAttr "l__leg_ik_poleVectorConstraint1.cty" "l__leg_ik.pvy";
connectAttr "l__leg_ik_poleVectorConstraint1.ctz" "l__leg_ik.pvz";
connectAttr "l__leg_ik.pim" "l__leg_ik_poleVectorConstraint1.cpim";
connectAttr "l_hip_rig_ik.pm" "l__leg_ik_poleVectorConstraint1.ps";
connectAttr "l_hip_rig_ik.t" "l__leg_ik_poleVectorConstraint1.crp";
connectAttr "l_leg_pv_01.t" "l__leg_ik_poleVectorConstraint1.tg[0].tt";
connectAttr "l_leg_pv_01.rp" "l__leg_ik_poleVectorConstraint1.tg[0].trp";
connectAttr "l_leg_pv_01.rpt" "l__leg_ik_poleVectorConstraint1.tg[0].trt";
connectAttr "l_leg_pv_01.pm" "l__leg_ik_poleVectorConstraint1.tg[0].tpm";
connectAttr "l__leg_ik_poleVectorConstraint1.w0" "l__leg_ik_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "l_ankle_rig_ik.msg" "l_ball_rig_rev_ik.hsj";
connectAttr "effector5.hp" "l_ball_rig_rev_ik.hee";
connectAttr "ikSCsolver.msg" "l_ball_rig_rev_ik.hsv";
connectAttr "unitConversion4.o" "l_toeTapNode.rx";
connectAttr "unitConversion7.o" "l_toeTapNode.ry";
connectAttr "unitConversion11.o" "l_toeTapNode.rz";
connectAttr "l_ball_rig_ik.msg" "l_toe_rig_rev_ik.hsj";
connectAttr "effector6.hp" "l_toe_rig_rev_ik.hee";
connectAttr "ikSCsolver.msg" "l_toe_rig_rev_ik.hsv";
connectAttr "r_leg_ik_switch.IkFkSwitch" "r_foot_ik_ctrl.v" -l on;
connectAttr "unitConversion12.o" "r_heelNode.rx";
connectAttr "unitConversion16.o" "r_heelNode.ry";
connectAttr "unitConversion20.o" "r_heelNode.rz";
connectAttr "unitConversion14.o" "r_revToeNode.rx";
connectAttr "unitConversion19.o" "r_revToeNode.ry";
connectAttr "unitConversion13.o" "r_revBallNode.rx";
connectAttr "unitConversion17.o" "r_revBallNode.ry";
connectAttr "unitConversion21.o" "r_revBallNode.rz";
connectAttr "r_hip_rig_ik.msg" "r__leg_ik.hsj";
connectAttr "effector7.hp" "r__leg_ik.hee";
connectAttr "ikRPsolver.msg" "r__leg_ik.hsv";
connectAttr "r__leg_ik_poleVectorConstraint1.ctx" "r__leg_ik.pvx";
connectAttr "r__leg_ik_poleVectorConstraint1.cty" "r__leg_ik.pvy";
connectAttr "r__leg_ik_poleVectorConstraint1.ctz" "r__leg_ik.pvz";
connectAttr "r__leg_ik.pim" "r__leg_ik_poleVectorConstraint1.cpim";
connectAttr "r_hip_rig_ik.pm" "r__leg_ik_poleVectorConstraint1.ps";
connectAttr "r_hip_rig_ik.t" "r__leg_ik_poleVectorConstraint1.crp";
connectAttr "r_leg_pv_01.t" "r__leg_ik_poleVectorConstraint1.tg[0].tt";
connectAttr "r_leg_pv_01.rp" "r__leg_ik_poleVectorConstraint1.tg[0].trp";
connectAttr "r_leg_pv_01.rpt" "r__leg_ik_poleVectorConstraint1.tg[0].trt";
connectAttr "r_leg_pv_01.pm" "r__leg_ik_poleVectorConstraint1.tg[0].tpm";
connectAttr "r__leg_ik_poleVectorConstraint1.w0" "r__leg_ik_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "r_ankle_rig_ik.msg" "r_ball_rig_rev_ik.hsj";
connectAttr "effector8.hp" "r_ball_rig_rev_ik.hee";
connectAttr "ikSCsolver.msg" "r_ball_rig_rev_ik.hsv";
connectAttr "unitConversion15.o" "r_toeTapNode.rx";
connectAttr "unitConversion18.o" "r_toeTapNode.ry";
connectAttr "unitConversion22.o" "r_toeTapNode.rz";
connectAttr "r_ball_rig_ik.msg" "r_toe_rig_rev_ik.hsj";
connectAttr "effector9.hp" "r_toe_rig_rev_ik.hee";
connectAttr "ikSCsolver.msg" "r_toe_rig_rev_ik.hsv";
connectAttr "pelvis.s" "spine_01.is";
connectAttr "spine_01.s" "spine_02.is";
connectAttr "spine_02.s" "spine_03.is";
connectAttr "spine_03.s" "spineEnd.is";
connectAttr "spineEnd.s" "l_clav.is";
connectAttr "l_clav.s" "l_shoulder.is";
connectAttr "l_shoulder.s" "l_elbow.is";
connectAttr "l_elbow.s" "l_wrist.is";
connectAttr "l_wrist.s" "l_index_01.is";
connectAttr "l_index_01.s" "l_index_02.is";
connectAttr "l_index_02.s" "l_index_03.is";
connectAttr "l_wrist.s" "l_pinky_01.is";
connectAttr "l_pinky_01.s" "l_pinky_02.is";
connectAttr "l_pinky_02.s" "l_pinky_03.is";
connectAttr "l_wrist.s" "l_thumb_01.is";
connectAttr "l_thumb_01.s" "l_thumb_02.is";
connectAttr "l_thumb_02.s" "l_thumb_03.is";
connectAttr "spineEnd.s" "neckBase.is";
connectAttr "neckBase_scaleConstraint1.csx" "neckBase.sx";
connectAttr "neckBase_scaleConstraint1.csy" "neckBase.sy";
connectAttr "neckBase_scaleConstraint1.csz" "neckBase.sz";
connectAttr "neckBase_pointConstraint1.ctx" "neckBase.tx";
connectAttr "neckBase_pointConstraint1.cty" "neckBase.ty";
connectAttr "neckBase_pointConstraint1.ctz" "neckBase.tz";
connectAttr "neckBase_orientConstraint1.crx" "neckBase.rx";
connectAttr "neckBase_orientConstraint1.cry" "neckBase.ry";
connectAttr "neckBase_orientConstraint1.crz" "neckBase.rz";
connectAttr "neckBase.s" "neck.is";
connectAttr "neck_scaleConstraint1.csx" "neck.sx";
connectAttr "neck_scaleConstraint1.csy" "neck.sy";
connectAttr "neck_scaleConstraint1.csz" "neck.sz";
connectAttr "neck_pointConstraint1.ctx" "neck.tx";
connectAttr "neck_pointConstraint1.cty" "neck.ty";
connectAttr "neck_pointConstraint1.ctz" "neck.tz";
connectAttr "neck_orientConstraint1.crx" "neck.rx";
connectAttr "neck_orientConstraint1.cry" "neck.ry";
connectAttr "neck_orientConstraint1.crz" "neck.rz";
connectAttr "neck.s" "l_eye.is";
connectAttr "neck.s" "|worldPlacement|bind_skeleton|pelvis|spine_01|spine_02|spine_03|spineEnd|neckBase|neck|jaw.is"
		;
connectAttr "neck.s" "r_eye.is";
connectAttr "neck.pim" "neck_pointConstraint1.cpim";
connectAttr "neck.rp" "neck_pointConstraint1.crp";
connectAttr "neck.rpt" "neck_pointConstraint1.crt";
connectAttr "neck_rig.t" "neck_pointConstraint1.tg[0].tt";
connectAttr "neck_rig.rp" "neck_pointConstraint1.tg[0].trp";
connectAttr "neck_rig.rpt" "neck_pointConstraint1.tg[0].trt";
connectAttr "neck_rig.pm" "neck_pointConstraint1.tg[0].tpm";
connectAttr "neck_pointConstraint1.w0" "neck_pointConstraint1.tg[0].tw";
connectAttr "neck.ro" "neck_orientConstraint1.cro";
connectAttr "neck.pim" "neck_orientConstraint1.cpim";
connectAttr "neck.jo" "neck_orientConstraint1.cjo";
connectAttr "neck.is" "neck_orientConstraint1.is";
connectAttr "neck_rig.r" "neck_orientConstraint1.tg[0].tr";
connectAttr "neck_rig.ro" "neck_orientConstraint1.tg[0].tro";
connectAttr "neck_rig.pm" "neck_orientConstraint1.tg[0].tpm";
connectAttr "neck_rig.jo" "neck_orientConstraint1.tg[0].tjo";
connectAttr "neck_orientConstraint1.w0" "neck_orientConstraint1.tg[0].tw";
connectAttr "neck.ssc" "neck_scaleConstraint1.tsc";
connectAttr "neck.pim" "neck_scaleConstraint1.cpim";
connectAttr "neck_rig.s" "neck_scaleConstraint1.tg[0].ts";
connectAttr "neck_rig.pm" "neck_scaleConstraint1.tg[0].tpm";
connectAttr "neck_scaleConstraint1.w0" "neck_scaleConstraint1.tg[0].tw";
connectAttr "neckBase.pim" "neckBase_pointConstraint1.cpim";
connectAttr "neckBase.rp" "neckBase_pointConstraint1.crp";
connectAttr "neckBase.rpt" "neckBase_pointConstraint1.crt";
connectAttr "neckBase_rig.t" "neckBase_pointConstraint1.tg[0].tt";
connectAttr "neckBase_rig.rp" "neckBase_pointConstraint1.tg[0].trp";
connectAttr "neckBase_rig.rpt" "neckBase_pointConstraint1.tg[0].trt";
connectAttr "neckBase_rig.pm" "neckBase_pointConstraint1.tg[0].tpm";
connectAttr "neckBase_pointConstraint1.w0" "neckBase_pointConstraint1.tg[0].tw";
connectAttr "neckBase.ro" "neckBase_orientConstraint1.cro";
connectAttr "neckBase.pim" "neckBase_orientConstraint1.cpim";
connectAttr "neckBase.jo" "neckBase_orientConstraint1.cjo";
connectAttr "neckBase.is" "neckBase_orientConstraint1.is";
connectAttr "neckBase_rig.r" "neckBase_orientConstraint1.tg[0].tr";
connectAttr "neckBase_rig.ro" "neckBase_orientConstraint1.tg[0].tro";
connectAttr "neckBase_rig.pm" "neckBase_orientConstraint1.tg[0].tpm";
connectAttr "neckBase_rig.jo" "neckBase_orientConstraint1.tg[0].tjo";
connectAttr "neckBase_orientConstraint1.w0" "neckBase_orientConstraint1.tg[0].tw"
		;
connectAttr "neckBase.ssc" "neckBase_scaleConstraint1.tsc";
connectAttr "neckBase.pim" "neckBase_scaleConstraint1.cpim";
connectAttr "neckBase_rig.s" "neckBase_scaleConstraint1.tg[0].ts";
connectAttr "neckBase_rig.pm" "neckBase_scaleConstraint1.tg[0].tpm";
connectAttr "neckBase_scaleConstraint1.w0" "neckBase_scaleConstraint1.tg[0].tw";
connectAttr "spineEnd.s" "r_clav.is";
connectAttr "r_clav.s" "r_shoulder.is";
connectAttr "r_shoulder.s" "r_elbow.is";
connectAttr "r_elbow.s" "r_wrist.is";
connectAttr "r_wrist.s" "r_index_01.is";
connectAttr "r_index_01.s" "r_index_02.is";
connectAttr "r_index_02.s" "r_index_03.is";
connectAttr "r_wrist.s" "r_pinky_01.is";
connectAttr "r_pinky_01.s" "r_pinky_02.is";
connectAttr "r_pinky_02.s" "r_pinky_03.is";
connectAttr "r_wrist.s" "r_thumb_01.is";
connectAttr "r_thumb_01.s" "r_thumb_02.is";
connectAttr "r_thumb_02.s" "r_thumb_03.is";
connectAttr "pelvis.s" "hips.is";
connectAttr "hips.s" "l_hip.is";
connectAttr "l_hip.s" "l_knee.is";
connectAttr "l_knee.s" "l_ankle.is";
connectAttr "l_ankle.s" "l_ball.is";
connectAttr "l_ball.s" "l_toe.is";
connectAttr "hips.s" "r_hip.is";
connectAttr "r_hip.s" "r_knee.is";
connectAttr "r_knee.s" "r_ankle.is";
connectAttr "r_ankle.s" "r_ball.is";
connectAttr "r_ball.s" "r_toe.is";
connectAttr "njc_l_elbow_lock_01_switch_parentConstraint1.ctx" "njc_l_elbow_lock_01_switch.tx"
		;
connectAttr "njc_l_elbow_lock_01_switch_parentConstraint1.cty" "njc_l_elbow_lock_01_switch.ty"
		;
connectAttr "njc_l_elbow_lock_01_switch_parentConstraint1.ctz" "njc_l_elbow_lock_01_switch.tz"
		;
connectAttr "njc_l_elbow_lock_01_switch_parentConstraint1.crx" "njc_l_elbow_lock_01_switch.rx"
		;
connectAttr "njc_l_elbow_lock_01_switch_parentConstraint1.cry" "njc_l_elbow_lock_01_switch.ry"
		;
connectAttr "njc_l_elbow_lock_01_switch_parentConstraint1.crz" "njc_l_elbow_lock_01_switch.rz"
		;
connectAttr "l_arm_ik_switch.elbowLock" "l_elbow_lock_01.v" -l on;
connectAttr "njc_l_elbow_lock_01_switch.ro" "njc_l_elbow_lock_01_switch_parentConstraint1.cro"
		;
connectAttr "njc_l_elbow_lock_01_switch.pim" "njc_l_elbow_lock_01_switch_parentConstraint1.cpim"
		;
connectAttr "njc_l_elbow_lock_01_switch.rp" "njc_l_elbow_lock_01_switch_parentConstraint1.crp"
		;
connectAttr "njc_l_elbow_lock_01_switch.rpt" "njc_l_elbow_lock_01_switch_parentConstraint1.crt"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parent.t" "njc_l_elbow_lock_01_switch_parentConstraint1.tg[0].tt"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parent.rp" "njc_l_elbow_lock_01_switch_parentConstraint1.tg[0].trp"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parent.rpt" "njc_l_elbow_lock_01_switch_parentConstraint1.tg[0].trt"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parent.r" "njc_l_elbow_lock_01_switch_parentConstraint1.tg[0].tr"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parent.ro" "njc_l_elbow_lock_01_switch_parentConstraint1.tg[0].tro"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parent.s" "njc_l_elbow_lock_01_switch_parentConstraint1.tg[0].ts"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parent.pm" "njc_l_elbow_lock_01_switch_parentConstraint1.tg[0].tpm"
		;
connectAttr "njc_l_elbow_lock_01_switch_parentConstraint1.w0" "njc_l_elbow_lock_01_switch_parentConstraint1.tg[0].tw"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space_parent.t" "njc_l_elbow_lock_01_switch_parentConstraint1.tg[1].tt"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space_parent.rp" "njc_l_elbow_lock_01_switch_parentConstraint1.tg[1].trp"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space_parent.rpt" "njc_l_elbow_lock_01_switch_parentConstraint1.tg[1].trt"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space_parent.r" "njc_l_elbow_lock_01_switch_parentConstraint1.tg[1].tr"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space_parent.ro" "njc_l_elbow_lock_01_switch_parentConstraint1.tg[1].tro"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space_parent.s" "njc_l_elbow_lock_01_switch_parentConstraint1.tg[1].ts"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space_parent.pm" "njc_l_elbow_lock_01_switch_parentConstraint1.tg[1].tpm"
		;
connectAttr "njc_l_elbow_lock_01_switch_parentConstraint1.w1" "njc_l_elbow_lock_01_switch_parentConstraint1.tg[1].tw"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_spaceCond.ocr" "njc_l_elbow_lock_01_switch_parentConstraint1.w0"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_spaceCond.ocr" "njc_l_elbow_lock_01_switch_parentConstraint1.w1"
		;
connectAttr "njc_r_elbow_lock_01_switch_parentConstraint1.ctx" "njc_r_elbow_lock_01_switch.tx"
		;
connectAttr "njc_r_elbow_lock_01_switch_parentConstraint1.cty" "njc_r_elbow_lock_01_switch.ty"
		;
connectAttr "njc_r_elbow_lock_01_switch_parentConstraint1.ctz" "njc_r_elbow_lock_01_switch.tz"
		;
connectAttr "njc_r_elbow_lock_01_switch_parentConstraint1.crx" "njc_r_elbow_lock_01_switch.rx"
		;
connectAttr "njc_r_elbow_lock_01_switch_parentConstraint1.cry" "njc_r_elbow_lock_01_switch.ry"
		;
connectAttr "njc_r_elbow_lock_01_switch_parentConstraint1.crz" "njc_r_elbow_lock_01_switch.rz"
		;
connectAttr "r_arm_ik_switch.elbowLock" "r_elbow_lock_01.v" -l on;
connectAttr "njc_r_elbow_lock_01_switch.ro" "njc_r_elbow_lock_01_switch_parentConstraint1.cro"
		;
connectAttr "njc_r_elbow_lock_01_switch.pim" "njc_r_elbow_lock_01_switch_parentConstraint1.cpim"
		;
connectAttr "njc_r_elbow_lock_01_switch.rp" "njc_r_elbow_lock_01_switch_parentConstraint1.crp"
		;
connectAttr "njc_r_elbow_lock_01_switch.rpt" "njc_r_elbow_lock_01_switch_parentConstraint1.crt"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parent.t" "njc_r_elbow_lock_01_switch_parentConstraint1.tg[0].tt"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parent.rp" "njc_r_elbow_lock_01_switch_parentConstraint1.tg[0].trp"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parent.rpt" "njc_r_elbow_lock_01_switch_parentConstraint1.tg[0].trt"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parent.r" "njc_r_elbow_lock_01_switch_parentConstraint1.tg[0].tr"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parent.ro" "njc_r_elbow_lock_01_switch_parentConstraint1.tg[0].tro"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parent.s" "njc_r_elbow_lock_01_switch_parentConstraint1.tg[0].ts"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parent.pm" "njc_r_elbow_lock_01_switch_parentConstraint1.tg[0].tpm"
		;
connectAttr "njc_r_elbow_lock_01_switch_parentConstraint1.w0" "njc_r_elbow_lock_01_switch_parentConstraint1.tg[0].tw"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space_parent.t" "njc_r_elbow_lock_01_switch_parentConstraint1.tg[1].tt"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space_parent.rp" "njc_r_elbow_lock_01_switch_parentConstraint1.tg[1].trp"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space_parent.rpt" "njc_r_elbow_lock_01_switch_parentConstraint1.tg[1].trt"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space_parent.r" "njc_r_elbow_lock_01_switch_parentConstraint1.tg[1].tr"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space_parent.ro" "njc_r_elbow_lock_01_switch_parentConstraint1.tg[1].tro"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space_parent.s" "njc_r_elbow_lock_01_switch_parentConstraint1.tg[1].ts"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space_parent.pm" "njc_r_elbow_lock_01_switch_parentConstraint1.tg[1].tpm"
		;
connectAttr "njc_r_elbow_lock_01_switch_parentConstraint1.w1" "njc_r_elbow_lock_01_switch_parentConstraint1.tg[1].tw"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_spaceCond.ocr" "njc_r_elbow_lock_01_switch_parentConstraint1.w0"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_spaceCond.ocr" "njc_r_elbow_lock_01_switch_parentConstraint1.w1"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.ctx" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space.tx"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.cty" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space.ty"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.ctz" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space.tz"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.crx" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space.rx"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.cry" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space.ry"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.crz" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space.rz"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space.ro" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.cro"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space.pim" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.cpim"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space.rp" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.crp"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space.rpt" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.crt"
		;
connectAttr "l_elbow_BlendSpace_group.t" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.tg[0].tt"
		;
connectAttr "l_elbow_BlendSpace_group.rp" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.tg[0].trp"
		;
connectAttr "l_elbow_BlendSpace_group.rpt" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.tg[0].trt"
		;
connectAttr "l_elbow_BlendSpace_group.r" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.tg[0].tr"
		;
connectAttr "l_elbow_BlendSpace_group.ro" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.tg[0].tro"
		;
connectAttr "l_elbow_BlendSpace_group.s" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.tg[0].ts"
		;
connectAttr "l_elbow_BlendSpace_group.pm" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.tg[0].tpm"
		;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.w0" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_space_parentConstraint1.tg[0].tw"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.ctx" "njc_l_elbow_lock_01_worldPlacement_space.tx"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.cty" "njc_l_elbow_lock_01_worldPlacement_space.ty"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.ctz" "njc_l_elbow_lock_01_worldPlacement_space.tz"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.crx" "njc_l_elbow_lock_01_worldPlacement_space.rx"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.cry" "njc_l_elbow_lock_01_worldPlacement_space.ry"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.crz" "njc_l_elbow_lock_01_worldPlacement_space.rz"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space.ro" "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.cro"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space.pim" "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.cpim"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space.rp" "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.crp"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space.rpt" "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.crt"
		;
connectAttr "worldPlacement.t" "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.tg[0].tt"
		;
connectAttr "worldPlacement.rp" "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.tg[0].trp"
		;
connectAttr "worldPlacement.rpt" "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.tg[0].trt"
		;
connectAttr "worldPlacement.r" "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.tg[0].tr"
		;
connectAttr "worldPlacement.ro" "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.tg[0].tro"
		;
connectAttr "worldPlacement.s" "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.tg[0].ts"
		;
connectAttr "worldPlacement.pm" "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.tg[0].tpm"
		;
connectAttr "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.w0" "njc_l_elbow_lock_01_worldPlacement_space_parentConstraint1.tg[0].tw"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.ctx" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space.tx"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.cty" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space.ty"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.ctz" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space.tz"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.crx" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space.rx"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.cry" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space.ry"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.crz" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space.rz"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space.ro" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.cro"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space.pim" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.cpim"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space.rp" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.crp"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space.rpt" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.crt"
		;
connectAttr "r_elbow_BlendSpace_group.t" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.tg[0].tt"
		;
connectAttr "r_elbow_BlendSpace_group.rp" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.tg[0].trp"
		;
connectAttr "r_elbow_BlendSpace_group.rpt" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.tg[0].trt"
		;
connectAttr "r_elbow_BlendSpace_group.r" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.tg[0].tr"
		;
connectAttr "r_elbow_BlendSpace_group.ro" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.tg[0].tro"
		;
connectAttr "r_elbow_BlendSpace_group.s" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.tg[0].ts"
		;
connectAttr "r_elbow_BlendSpace_group.pm" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.tg[0].tpm"
		;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.w0" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_space_parentConstraint1.tg[0].tw"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.ctx" "njc_r_elbow_lock_01_worldPlacement_space.tx"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.cty" "njc_r_elbow_lock_01_worldPlacement_space.ty"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.ctz" "njc_r_elbow_lock_01_worldPlacement_space.tz"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.crx" "njc_r_elbow_lock_01_worldPlacement_space.rx"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.cry" "njc_r_elbow_lock_01_worldPlacement_space.ry"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.crz" "njc_r_elbow_lock_01_worldPlacement_space.rz"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space.ro" "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.cro"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space.pim" "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.cpim"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space.rp" "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.crp"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space.rpt" "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.crt"
		;
connectAttr "worldPlacement.t" "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.tg[0].tt"
		;
connectAttr "worldPlacement.rp" "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.tg[0].trp"
		;
connectAttr "worldPlacement.rpt" "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.tg[0].trt"
		;
connectAttr "worldPlacement.r" "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.tg[0].tr"
		;
connectAttr "worldPlacement.ro" "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.tg[0].tro"
		;
connectAttr "worldPlacement.s" "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.tg[0].ts"
		;
connectAttr "worldPlacement.pm" "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.tg[0].tpm"
		;
connectAttr "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.w0" "njc_r_elbow_lock_01_worldPlacement_space_parentConstraint1.tg[0].tw"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "layerManager.dli[1]" "layer1.id";
connectAttr "cluster1GroupParts.og" "cluster1.ip[0].ig";
connectAttr "cluster1GroupId.id" "cluster1.ip[0].gi";
connectAttr "spineCurve_ik_CV_0.wm" "cluster1.ma";
connectAttr "|worldPlacement|rig_skeleton|pelvis_rig|hips_rig|spineCurve_ik_CV_0|clusterHandleShape.x" "cluster1.x"
		;
connectAttr "groupParts2.og" "tweak1.ip[0].ig";
connectAttr "groupId2.id" "tweak1.ip[0].gi";
connectAttr "cluster1GroupId.msg" "cluster1Set.gn" -na;
connectAttr "spineCurve_ikShape.iog.og[0]" "cluster1Set.dsm" -na;
connectAttr "cluster1.msg" "cluster1Set.ub[0]";
connectAttr "tweak1.og[0]" "cluster1GroupParts.ig";
connectAttr "cluster1GroupId.id" "cluster1GroupParts.gi";
connectAttr "groupId2.msg" "tweakSet1.gn" -na;
connectAttr "spineCurve_ikShape.iog.og[1]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "spineCurve_ikShapeOrig.ws" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "cluster2GroupParts.og" "cluster2.ip[0].ig";
connectAttr "cluster2GroupId.id" "cluster2.ip[0].gi";
connectAttr "spineCurve_ik_CV_1.wm" "cluster2.ma";
connectAttr "|DoNotDeleteRigNodes|spineExtra|spineCurve_ik_CV_1_point_X|spineCurve_ik_CV_1_point_Y|spineCurve_ik_CV_1_point_Z|spineCurve_ik_CV_1|clusterHandleShape.x" "cluster2.x"
		;
connectAttr "cluster2GroupId.msg" "cluster2Set.gn" -na;
connectAttr "spineCurve_ikShape.iog.og[2]" "cluster2Set.dsm" -na;
connectAttr "cluster2.msg" "cluster2Set.ub[0]";
connectAttr "cluster1.og[0]" "cluster2GroupParts.ig";
connectAttr "cluster2GroupId.id" "cluster2GroupParts.gi";
connectAttr "cluster3GroupParts.og" "cluster3.ip[0].ig";
connectAttr "cluster3GroupId.id" "cluster3.ip[0].gi";
connectAttr "spineCurve_ik_CV_2.wm" "cluster3.ma";
connectAttr "|DoNotDeleteRigNodes|spineExtra|spineCurve_ik_CV_2_point_X|spineCurve_ik_CV_2_point_Y|spineCurve_ik_CV_2_point_Z|spineCurve_ik_CV_2|clusterHandleShape.x" "cluster3.x"
		;
connectAttr "cluster3GroupId.msg" "cluster3Set.gn" -na;
connectAttr "spineCurve_ikShape.iog.og[3]" "cluster3Set.dsm" -na;
connectAttr "cluster3.msg" "cluster3Set.ub[0]";
connectAttr "cluster2.og[0]" "cluster3GroupParts.ig";
connectAttr "cluster3GroupId.id" "cluster3GroupParts.gi";
connectAttr "cluster4GroupParts.og" "cluster4.ip[0].ig";
connectAttr "cluster4GroupId.id" "cluster4.ip[0].gi";
connectAttr "spineCurve_ik_CV_3.wm" "cluster4.ma";
connectAttr "|DoNotDeleteRigNodes|spineExtra|spineCurve_ik_CV_3_point_X|spineCurve_ik_CV_3_point_Y|spineCurve_ik_CV_3_point_Z|spineCurve_ik_CV_3|clusterHandleShape.x" "cluster4.x"
		;
connectAttr "cluster4GroupId.msg" "cluster4Set.gn" -na;
connectAttr "spineCurve_ikShape.iog.og[4]" "cluster4Set.dsm" -na;
connectAttr "cluster4.msg" "cluster4Set.ub[0]";
connectAttr "cluster3.og[0]" "cluster4GroupParts.ig";
connectAttr "cluster4GroupId.id" "cluster4GroupParts.gi";
connectAttr "cluster5GroupParts.og" "cluster5.ip[0].ig";
connectAttr "cluster5GroupId.id" "cluster5.ip[0].gi";
connectAttr "spineCurve_ik_CV_4.wm" "cluster5.ma";
connectAttr "|DoNotDeleteRigNodes|spineExtra|spineCurve_ik_CV_4_point_X|spineCurve_ik_CV_4_point_Y|spineCurve_ik_CV_4_point_Z|spineCurve_ik_CV_4|clusterHandleShape.x" "cluster5.x"
		;
connectAttr "cluster5GroupId.msg" "cluster5Set.gn" -na;
connectAttr "spineCurve_ikShape.iog.og[5]" "cluster5Set.dsm" -na;
connectAttr "cluster5.msg" "cluster5Set.ub[0]";
connectAttr "cluster4.og[0]" "cluster5GroupParts.ig";
connectAttr "cluster5GroupId.id" "cluster5GroupParts.gi";
connectAttr "cluster6GroupParts.og" "cluster6.ip[0].ig";
connectAttr "cluster6GroupId.id" "cluster6.ip[0].gi";
connectAttr "spineCurve_ik_CV_5.wm" "cluster6.ma";
connectAttr "|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|spineCurve_ik_CV_5|clusterHandleShape.x" "cluster6.x"
		;
connectAttr "cluster6GroupId.msg" "cluster6Set.gn" -na;
connectAttr "spineCurve_ikShape.iog.og[6]" "cluster6Set.dsm" -na;
connectAttr "cluster6.msg" "cluster6Set.ub[0]";
connectAttr "cluster5.og[0]" "cluster6GroupParts.ig";
connectAttr "cluster6GroupId.id" "cluster6GroupParts.gi";
connectAttr "spineCurve_ik_CV_1_point_X.blendSpace" "spineCurve_ik_CV_1_point_blend_X_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_1_point_Y.blendSpace" "spineCurve_ik_CV_1_point_blend_Y_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_1_point_Z.blendSpace" "spineCurve_ik_CV_1_point_blend_Z_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_2_point_X.blendSpace" "spineCurve_ik_CV_2_point_blend_X_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_2_point_Y.blendSpace" "spineCurve_ik_CV_2_point_blend_Y_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_2_point_Z.blendSpace" "spineCurve_ik_CV_2_point_blend_Z_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_3_point_X.blendSpace" "spineCurve_ik_CV_3_point_blend_X_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_3_point_Y.blendSpace" "spineCurve_ik_CV_3_point_blend_Y_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_3_point_Z.blendSpace" "spineCurve_ik_CV_3_point_blend_Z_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_4_point_X.blendSpace" "spineCurve_ik_CV_4_point_blend_X_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_4_point_Y.blendSpace" "spineCurve_ik_CV_4_point_blend_Y_point_reverse.ix"
		;
connectAttr "spineCurve_ik_CV_4_point_Z.blendSpace" "spineCurve_ik_CV_4_point_blend_Z_point_reverse.ix"
		;
connectAttr "njcArcLengthNode.al" "njcMultiSpineStre.i1x";
connectAttr "njcRigScaleSpineMulti.ox" "njcMultiSpineStre.i2x";
connectAttr "worldPlacement.sy" "njcRigScaleSpineMulti.i1x";
connectAttr "spineCurve_ikShape.ws" "njcArcLengthNode.ic";
connectAttr "njcMultiSpineStre.ox" "expression1.in[0]";
connectAttr ":time1.o" "expression1.tim";
connectAttr "spine_01_rig.msg" "expression1.obm";
connectAttr "njcMultiSpineStre.ox" "expression2.in[0]";
connectAttr ":time1.o" "expression2.tim";
connectAttr "spine_02_rig.msg" "expression2.obm";
connectAttr "njcMultiSpineStre.ox" "expression3.in[0]";
connectAttr ":time1.o" "expression3.tim";
connectAttr "spine_03_rig.msg" "expression3.obm";
connectAttr "njcMultiSpineStre.ox" "expression4.in[0]";
connectAttr ":time1.o" "expression4.tim";
connectAttr "spineEnd_rig.msg" "expression4.obm";
connectAttr "topSpine_ctrl.ty" "expression5.in[0]";
connectAttr "topSpine_ctrl.spineScale" "expression5.in[1]";
connectAttr ":time1.o" "expression5.tim";
connectAttr "spine_01_rig.msg" "expression5.obm";
connectAttr "topSpine_ctrl.ty" "expression6.in[0]";
connectAttr "topSpine_ctrl.spineScale" "expression6.in[1]";
connectAttr ":time1.o" "expression6.tim";
connectAttr "spine_02_rig.msg" "expression6.obm";
connectAttr "topSpine_ctrl.ty" "expression7.in[0]";
connectAttr "topSpine_ctrl.spineScale" "expression7.in[1]";
connectAttr ":time1.o" "expression7.tim";
connectAttr "spine_03_rig.msg" "expression7.obm";
connectAttr "clean_ref_grp.msg" "Grunt_Skeleton_skinRN.asn[0]";
connectAttr "l_shoulder_rig.blendSpace" "l_shoulder_rig_fk_point_reverse.ix";
connectAttr "l_shoulder_rig.blendSpace" "l_shoulder_rig_fk_point_OR_reverse.ix";
connectAttr "l_elbow_BlendSpace_group.blendSpace" "l_elbow_rig_fk_point_reverse.ix"
		;
connectAttr "l_elbow_BlendSpace_group.blendSpace" "l_elbow_rig_fk_point_OR_reverse.ix"
		;
connectAttr "l_wrist_rig.blendSpace" "l_wrist_rig_fk_point_reverse.ix";
connectAttr "l_wrist_rig.blendSpace" "l_wrist_rig_fk_point_OR_reverse.ix";
connectAttr "l_elbow_lock_01.space" "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_spaceCond.ft"
		;
connectAttr "l_elbow_lock_01.space" "njc_l_elbow_lock_01_worldPlacement_spaceCond.ft"
		;
connectAttr "l_arm_ik_switch.IkFkSwitch" "l_shoulder_fk_ctrl_01_rev.ix";
connectAttr "l_shoulder_fk_ctrl_01.rotateWith" "topSpine_ctrl_shoFollowCond.ft";
connectAttr "l_shoulder_fk_ctrl_01.rotateWith" "pelvis_ctrl_shoFollowCond.ft";
connectAttr "l_shoulder_fk_ctrl_01.rotateWith" "worldPlacement_shoFollowCond.ft"
		;
connectAttr "l_armLocUp.wm" "l_arm_Stretchy_distLoc_up.im1";
connectAttr "l_armLocDown.wm" "l_arm_Stretchy_distLoc_up.im2";
connectAttr "l_armLocUp.rpt" "l_arm_Stretchy_distLoc_up.p1";
connectAttr "l_armLocDown.rpt" "l_arm_Stretchy_distLoc_up.p2";
connectAttr "worldPlacement.sx" "l_arm_jntMulti.i2x";
connectAttr "l_wrist_ctrl_01.stretch" "l_arm_addSub.i1[0]";
connectAttr "l_arm_Stretchy_distLoc_up.d" "l_arm_addSub.i1[1]";
connectAttr "l_arm_ik_switch.autoStretch" "expression8.in[0]";
connectAttr "l_arm_addSub.o1" "expression8.in[1]";
connectAttr "l_arm_jntMulti.ox" "expression8.in[2]";
connectAttr "l_wrist_ctrl_01.stretch" "expression8.in[3]";
connectAttr ":time1.o" "expression8.tim";
connectAttr "l_elbow_rig_ik.msg" "expression8.obm";
connectAttr "l_arm_ik_switch.autoStretch" "expression9.in[0]";
connectAttr "l_arm_addSub.o1" "expression9.in[1]";
connectAttr "l_arm_jntMulti.ox" "expression9.in[2]";
connectAttr "l_wrist_ctrl_01.stretch" "expression9.in[3]";
connectAttr ":time1.o" "expression9.tim";
connectAttr "l_wrist_rig_ik.msg" "expression9.obm";
connectAttr "r_shoulder_rig.blendSpace" "r_shoulder_rig_fk_point_reverse.ix";
connectAttr "r_shoulder_rig.blendSpace" "r_shoulder_rig_fk_point_OR_reverse.ix";
connectAttr "r_elbow_BlendSpace_group.blendSpace" "r_elbow_rig_fk_point_reverse.ix"
		;
connectAttr "r_elbow_BlendSpace_group.blendSpace" "r_elbow_rig_fk_point_OR_reverse.ix"
		;
connectAttr "r_wrist_rig.blendSpace" "r_wrist_rig_fk_point_reverse.ix";
connectAttr "r_wrist_rig.blendSpace" "r_wrist_rig_fk_point_OR_reverse.ix";
connectAttr "r_elbow_lock_01.space" "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_spaceCond.ft"
		;
connectAttr "r_elbow_lock_01.space" "njc_r_elbow_lock_01_worldPlacement_spaceCond.ft"
		;
connectAttr "r_arm_ik_switch.IkFkSwitch" "r_shoulder_fk_ctrl_01_rev.ix";
connectAttr "r_shoulder_fk_ctrl_01.rotateWith" "topSpine_ctrl_shoFollowCond1.ft"
		;
connectAttr "r_shoulder_fk_ctrl_01.rotateWith" "pelvis_ctrl_shoFollowCond1.ft";
connectAttr "r_shoulder_fk_ctrl_01.rotateWith" "worldPlacement_shoFollowCond1.ft"
		;
connectAttr "r_armLocUp.wm" "r_arm_Stretchy_distLoc_up.im1";
connectAttr "r_armLocDown.wm" "r_arm_Stretchy_distLoc_up.im2";
connectAttr "r_armLocUp.rpt" "r_arm_Stretchy_distLoc_up.p1";
connectAttr "r_armLocDown.rpt" "r_arm_Stretchy_distLoc_up.p2";
connectAttr "worldPlacement.sx" "r_arm_jntMulti.i2x";
connectAttr "r_wrist_ctrl_01.stretch" "r_arm_addSub.i1[0]";
connectAttr "r_arm_Stretchy_distLoc_up.d" "r_arm_addSub.i1[1]";
connectAttr "r_arm_ik_switch.autoStretch" "expression10.in[0]";
connectAttr "r_arm_addSub.o1" "expression10.in[1]";
connectAttr "r_arm_jntMulti.ox" "expression10.in[2]";
connectAttr "r_wrist_ctrl_01.stretch" "expression10.in[3]";
connectAttr ":time1.o" "expression10.tim";
connectAttr "r_elbow_rig_ik.msg" "expression10.obm";
connectAttr "r_arm_ik_switch.autoStretch" "expression11.in[0]";
connectAttr "r_arm_addSub.o1" "expression11.in[1]";
connectAttr "r_arm_jntMulti.ox" "expression11.in[2]";
connectAttr "r_wrist_ctrl_01.stretch" "expression11.in[3]";
connectAttr ":time1.o" "expression11.tim";
connectAttr "r_wrist_rig_ik.msg" "expression11.obm";
connectAttr "l_hip_rig.blendSpace" "l_hip_rig_fk_point_reverse.ix";
connectAttr "l_hip_rig.blendSpace" "l_hip_rig_fk_point_OR_reverse.ix";
connectAttr "l_knee_rig.blendSpace" "l_knee_rig_fk_point_reverse.ix";
connectAttr "l_knee_rig.blendSpace" "l_knee_rig_fk_point_OR_reverse.ix";
connectAttr "l_ankle_rig.blendSpace" "l_ankle_rig_fk_point_reverse.ix";
connectAttr "l_ankle_rig.blendSpace" "l_ankle_rig_fk_point_OR_reverse.ix";
connectAttr "l_ball_rig.blendSpace" "l_ball_rig_fk_point_reverse.ix";
connectAttr "l_ball_rig.blendSpace" "l_ball_rig_fk_point_OR_reverse.ix";
connectAttr "l_toe_rig.blendSpace" "l_toe_rig_fk_point_reverse.ix";
connectAttr "l_toe_rig.blendSpace" "l_toe_rig_fk_point_OR_reverse.ix";
connectAttr "r_hip_rig.blendSpace" "r_hip_rig_fk_point_reverse.ix";
connectAttr "r_hip_rig.blendSpace" "r_hip_rig_fk_point_OR_reverse.ix";
connectAttr "r_knee_rig.blendSpace" "r_knee_rig_fk_point_reverse.ix";
connectAttr "r_knee_rig.blendSpace" "r_knee_rig_fk_point_OR_reverse.ix";
connectAttr "r_ankle_rig.blendSpace" "r_ankle_rig_fk_point_reverse.ix";
connectAttr "r_ankle_rig.blendSpace" "r_ankle_rig_fk_point_OR_reverse.ix";
connectAttr "r_ball_rig.blendSpace" "r_ball_rig_fk_point_reverse.ix";
connectAttr "r_ball_rig.blendSpace" "r_ball_rig_fk_point_OR_reverse.ix";
connectAttr "r_toe_rig.blendSpace" "r_toe_rig_fk_point_reverse.ix";
connectAttr "r_toe_rig.blendSpace" "r_toe_rig_fk_point_OR_reverse.ix";
connectAttr "l_foot_ik_ctrl.heelRoll" "unitConversion1.i";
connectAttr "l_foot_ik_ctrl.ballRoll" "unitConversion2.i";
connectAttr "l_foot_ik_ctrl.toeRoll" "unitConversion3.i";
connectAttr "l_foot_ik_ctrl.toeTap" "unitConversion4.i";
connectAttr "l_foot_ik_ctrl.heelPivot" "unitConversion5.i";
connectAttr "l_foot_ik_ctrl.ballPivot" "unitConversion6.i";
connectAttr "l_foot_ik_ctrl.ballPivot" "unitConversion7.i";
connectAttr "l_foot_ik_ctrl.toePivot" "unitConversion8.i";
connectAttr "l_foot_ik_ctrl.heelSide" "unitConversion9.i";
connectAttr "l_foot_ik_ctrl.ballSide" "unitConversion10.i";
connectAttr "l_foot_ik_ctrl.toeSide" "unitConversion11.i";
connectAttr "l_legLocUp.wm" "l_leg_Stretchy_distLoc_up.im1";
connectAttr "l_legLocDown.wm" "l_leg_Stretchy_distLoc_up.im2";
connectAttr "l_legLocUp.rpt" "l_leg_Stretchy_distLoc_up.p1";
connectAttr "l_legLocDown.rpt" "l_leg_Stretchy_distLoc_up.p2";
connectAttr "worldPlacement.sy" "l_leg_jntMulti.i2x";
connectAttr "l_foot_ik_ctrl.stretch" "l_leg_addSub.i1[0]";
connectAttr "l_leg_Stretchy_distLoc_up.d" "l_leg_addSub.i1[1]";
connectAttr "l_leg_ik_switch.autoStretch" "expression12.in[0]";
connectAttr "l_leg_addSub.o1" "expression12.in[1]";
connectAttr "l_leg_jntMulti.ox" "expression12.in[2]";
connectAttr "l_foot_ik_ctrl.stretch" "expression12.in[3]";
connectAttr ":time1.o" "expression12.tim";
connectAttr "l_knee_rig_ik.msg" "expression12.obm";
connectAttr "l_leg_ik_switch.autoStretch" "expression13.in[0]";
connectAttr "l_leg_addSub.o1" "expression13.in[1]";
connectAttr "l_leg_jntMulti.ox" "expression13.in[2]";
connectAttr "l_foot_ik_ctrl.stretch" "expression13.in[3]";
connectAttr ":time1.o" "expression13.tim";
connectAttr "l_ankle_rig_ik.msg" "expression13.obm";
connectAttr "l_leg_ik_switch.IkFkSwitch" "l_hip_ctrl_rev.ix";
connectAttr "l_hip_ctrl.rotateWith" "worldPlacement_hipFollowCond.ft";
connectAttr "l_hip_ctrl.rotateWith" "pelvis_ctrl_hipFollowCond.ft";
connectAttr "l_hip_ctrl.rotateWith" "hips_ctrl_hipFollowCond.ft";
connectAttr "r_foot_ik_ctrl.heelRoll" "unitConversion12.i";
connectAttr "r_foot_ik_ctrl.ballRoll" "unitConversion13.i";
connectAttr "r_foot_ik_ctrl.toeRoll" "unitConversion14.i";
connectAttr "r_foot_ik_ctrl.toeTap" "unitConversion15.i";
connectAttr "r_foot_ik_ctrl.heelPivot" "unitConversion16.i";
connectAttr "r_foot_ik_ctrl.ballPivot" "unitConversion17.i";
connectAttr "r_foot_ik_ctrl.ballPivot" "unitConversion18.i";
connectAttr "r_foot_ik_ctrl.toePivot" "unitConversion19.i";
connectAttr "r_foot_ik_ctrl.heelSide" "unitConversion20.i";
connectAttr "r_foot_ik_ctrl.ballSide" "unitConversion21.i";
connectAttr "r_foot_ik_ctrl.toeSide" "unitConversion22.i";
connectAttr "r_legLocUp.wm" "r_leg_Stretchy_distLoc_up.im1";
connectAttr "r_legLocDown.wm" "r_leg_Stretchy_distLoc_up.im2";
connectAttr "r_legLocUp.rpt" "r_leg_Stretchy_distLoc_up.p1";
connectAttr "r_legLocDown.rpt" "r_leg_Stretchy_distLoc_up.p2";
connectAttr "worldPlacement.sy" "r_leg_jntMulti.i2x";
connectAttr "r_foot_ik_ctrl.stretch" "r_leg_addSub.i1[0]";
connectAttr "r_leg_Stretchy_distLoc_up.d" "r_leg_addSub.i1[1]";
connectAttr "r_leg_ik_switch.autoStretch" "expression14.in[0]";
connectAttr "r_leg_addSub.o1" "expression14.in[1]";
connectAttr "r_leg_jntMulti.ox" "expression14.in[2]";
connectAttr "r_foot_ik_ctrl.stretch" "expression14.in[3]";
connectAttr ":time1.o" "expression14.tim";
connectAttr "r_knee_rig_ik.msg" "expression14.obm";
connectAttr "r_leg_ik_switch.autoStretch" "expression15.in[0]";
connectAttr "r_leg_addSub.o1" "expression15.in[1]";
connectAttr "r_leg_jntMulti.ox" "expression15.in[2]";
connectAttr "r_foot_ik_ctrl.stretch" "expression15.in[3]";
connectAttr ":time1.o" "expression15.tim";
connectAttr "r_ankle_rig_ik.msg" "expression15.obm";
connectAttr "r_leg_ik_switch.IkFkSwitch" "r_hip_ctrl_rev.ix";
connectAttr "r_hip_ctrl.rotateWith" "worldPlacement_hipFollowCond1.ft";
connectAttr "r_hip_ctrl.rotateWith" "pelvis_ctrl_hipFollowCond1.ft";
connectAttr "r_hip_ctrl.rotateWith" "hips_ctrl_hipFollowCond1.ft";
connectAttr "spineCurve_ik_CV_1_point_blend_X_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_1_point_blend_Y_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_1_point_blend_Z_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_2_point_blend_X_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_2_point_blend_Y_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_2_point_blend_Z_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_3_point_blend_X_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_3_point_blend_Y_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_3_point_blend_Z_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_4_point_blend_X_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_4_point_blend_Y_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "spineCurve_ik_CV_4_point_blend_Z_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "njcMultiSpineStre.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "njcRigScaleSpineMulti.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "l_shoulder_rig_fk_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "l_shoulder_rig_fk_point_OR_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "l_elbow_rig_fk_point_reverse.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "l_elbow_rig_fk_point_OR_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "l_wrist_rig_fk_point_reverse.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "l_wrist_rig_fk_point_OR_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "njc_l_elbow_lock_01_l_elbow_BlendSpace_group_spaceCond.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "njc_l_elbow_lock_01_worldPlacement_spaceCond.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "l_shoulder_fk_ctrl_01_rev.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "topSpine_ctrl_shoFollowCond.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "pelvis_ctrl_shoFollowCond.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "worldPlacement_shoFollowCond.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "l_arm_Stretchy_distLoc_up.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "l_arm_jntMulti.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "l_arm_addSub.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "r_shoulder_rig_fk_point_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "r_shoulder_rig_fk_point_OR_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "r_elbow_rig_fk_point_reverse.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "r_elbow_rig_fk_point_OR_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "r_wrist_rig_fk_point_reverse.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "r_wrist_rig_fk_point_OR_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "njc_r_elbow_lock_01_r_elbow_BlendSpace_group_spaceCond.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "njc_r_elbow_lock_01_worldPlacement_spaceCond.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "r_shoulder_fk_ctrl_01_rev.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "topSpine_ctrl_shoFollowCond1.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "pelvis_ctrl_shoFollowCond1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "worldPlacement_shoFollowCond1.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "r_arm_Stretchy_distLoc_up.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "r_arm_jntMulti.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "r_arm_addSub.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "l_hip_rig_fk_point_reverse.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "l_hip_rig_fk_point_OR_reverse.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "l_knee_rig_fk_point_reverse.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "l_knee_rig_fk_point_OR_reverse.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "l_ankle_rig_fk_point_reverse.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "l_ankle_rig_fk_point_OR_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "l_ball_rig_fk_point_reverse.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "l_ball_rig_fk_point_OR_reverse.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "l_toe_rig_fk_point_reverse.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "l_toe_rig_fk_point_OR_reverse.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "r_hip_rig_fk_point_reverse.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "r_hip_rig_fk_point_OR_reverse.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "r_knee_rig_fk_point_reverse.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "r_knee_rig_fk_point_OR_reverse.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "r_ankle_rig_fk_point_reverse.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "r_ankle_rig_fk_point_OR_reverse.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "r_ball_rig_fk_point_reverse.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "r_ball_rig_fk_point_OR_reverse.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "r_toe_rig_fk_point_reverse.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "r_toe_rig_fk_point_OR_reverse.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "l_leg_Stretchy_distLoc_up.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "l_leg_jntMulti.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "l_leg_addSub.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "l_hip_ctrl_rev.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "worldPlacement_hipFollowCond.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "pelvis_ctrl_hipFollowCond.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "hips_ctrl_hipFollowCond.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "r_leg_Stretchy_distLoc_up.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "r_leg_jntMulti.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "r_leg_addSub.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "r_hip_ctrl_rev.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "worldPlacement_hipFollowCond1.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "pelvis_ctrl_hipFollowCond1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "hips_ctrl_hipFollowCond1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "ikSCsolver.msg" ":ikSystem.sol" -na;
connectAttr "ikRPsolver.msg" ":ikSystem.sol" -na;
connectAttr "ikSplineSolver.msg" ":ikSystem.sol" -na;
// End of Grunt_Skeleton_Rig.ma
