//Maya ASCII 2016 scene
//Name: Enemy@Grunt_Laughing_01.ma
//Last modified: Sun, Nov 22, 2015 03:18:49 PM
//Codeset: 1252
requires maya "2016";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "73CB8A7C-42F4-6954-222D-0FBFA51261DF";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 115.15538217018208 157.2730267033896 255.4112634836375 ;
	setAttr ".r" -type "double3" -19.538352729602394 21.799999999999986 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "C114D39F-429F-4E7C-32A8-B181EC9FF57D";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 326.41459506778358;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "883D9450-411C-1930-0D04-07B31841C94C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "C197160B-4032-F310-3B54-A3A31C277EE0";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "F8A30D7A-4C34-42DB-96AC-FF8CD2ABC2B2";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "12818177-48BE-A056-772B-A59E3BC2D2AD";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "A405073F-40FF-B814-A782-7D9DE784A7EE";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "8D80766B-45FC-A57A-66FF-46B0E0EE58A4";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "ref_frame";
	rename -uid "64363FB7-4ABA-2805-AF30-20B86593E83C";
	addAttr -ci true -sn "refFrame" -ln "refFrame" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "Character1_Hips" -p "ref_frame";
	rename -uid "1EB96618-4A6A-AB27-01A9-0C9F236FEA01";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 1.406920211983633 -3.3458391704689405 -22.822435086355146 ;
	setAttr ".ssc" no;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftUpLeg" -p "Character1_Hips";
	rename -uid "17353728-4554-1F9C-BCC3-D78321DA3ED8";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 96.630252250634001 6.5132649289235918 97.599644355322027 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftLeg" -p "Character1_LeftUpLeg";
	rename -uid "EABAD932-49D2-3F4B-37DF-E3B2D3CD892E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -129.48089468354581 -7.9067044581664252 177.65996829599328 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFoot" -p "Character1_LeftLeg";
	rename -uid "AE754F6F-4113-892F-1A4E-9EACEAFEC330";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -82.075120143962664 29.940914059061775 128.00062508119765 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftToeBase" -p "Character1_LeftFoot";
	rename -uid "F2874AC9-4747-4761-E9D0-F48AC3C9E17A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -126.16837928120003 -7.059491002168353 35.858557690282048 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle1" -p "Character1_LeftToeBase";
	rename -uid "2E007947-4C86-E1FE-4882-89AAF3A33208";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -100.08944018296057 24.928076457701248 56.143888543356006 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle2" -p "Character1_LeftFootMiddle1";
	rename -uid "7D94284D-4F42-20EB-DBCB-A086A12653C3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -39.508027973091373 -2.0582897454817326 31.259679900637106 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightUpLeg" -p "Character1_Hips";
	rename -uid "59A1EABF-4977-C695-688A-B9BEC0A66CC5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 11.416489881294433 -11.43673382189878 -152.25229525481123 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightLeg" -p "Character1_RightUpLeg";
	rename -uid "0CF97106-40FE-4062-C33F-7DAF4F44B5B9";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -22.942928776539453 -35.956391389429932 101.2794675694641 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFoot" -p "Character1_RightLeg";
	rename -uid "D4E37172-412E-0539-030F-B3AF67FFA1CB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 66.643786034585347 17.03863148951929 124.91120862192221 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightToeBase" -p "Character1_RightFoot";
	rename -uid "D5991CCC-47B8-E243-2342-73BA9F619CBE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -155.84366405206632 -5.8738653577349114 -93.73595023516998 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle1" -p "Character1_RightToeBase";
	rename -uid "2A1383D3-480E-8B4F-B45C-EE973F1E1EC2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -166.06091029301223 54.415170601070251 -127.18425880983375 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle2" -p "Character1_RightFootMiddle1";
	rename -uid "AAE2F669-420F-8722-6C03-48BAA86DC1F2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -129.2483692316568 10.731788640430997 6.9892235317174372 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine" -p "Character1_Hips";
	rename -uid "1BB7AC0A-4CDB-0572-7831-10B8C215AA96";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 11.416489881294433 -11.43673382189878 -152.25229525481123 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine1" -p "Character1_Spine";
	rename -uid "DDF0BAE9-4996-3ECB-A91D-90B75B3D85A0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 24.933471933369759 -37.782249520228419 -20.761597110625811 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftShoulder" -p "Character1_Spine1";
	rename -uid "717D4914-4736-CA1C-5CF4-2BA92D1B82A3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 144.67442469508543 70.793071067570196 19.807411116001759 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftArm" -p "Character1_LeftShoulder";
	rename -uid "E14E9260-458D-DE99-E144-0DB28028AACF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 99.384574925726014 -60.006523708111558 170.15034313799799 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftForeArm" -p "Character1_LeftArm";
	rename -uid "106237E3-4720-DF16-F605-FAB74FB34F32";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 70.061799635480924 -4.8580147780287897 21.798459685601706 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHand" -p "Character1_LeftForeArm";
	rename -uid "B99AC52D-4FAE-4FC8-8CFF-5597BD6A8D20";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.9708494230587 -13.075378107872792 150.36023365917421 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb1" -p "Character1_LeftHand";
	rename -uid "E3442B00-4E6F-4288-F804-F9965BA2981B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -65.748924167512314 39.787402356574312 166.7746235430387 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb2" -p "Character1_LeftHandThumb1";
	rename -uid "E920BCBD-4B63-6CD9-335A-45B6C9FD1054";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -39.861597330550808 6.7221908644292059 68.670730027774553 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb3" -p "Character1_LeftHandThumb2";
	rename -uid "0A81D18C-46CF-90A6-353E-43B79B3887DB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -120.21671274765816 -26.239331955300646 57.413978285788289 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex1" -p "Character1_LeftHand";
	rename -uid "C9C69AA1-400C-70D0-8CF2-84B5D6D46D36";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -152.38220651039205 4.806304023778381 48.842756939944849 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex2" -p "Character1_LeftHandIndex1";
	rename -uid "9FBFA874-4026-C329-24C4-F8A6858B6295";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 176.30914278667802 -13.434888179376443 97.633542306389202 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex3" -p "Character1_LeftHandIndex2";
	rename -uid "307974B9-445B-DEC5-CE30-21BF02E92011";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -102.43860404365057 -60.659303181068822 -152.9472923666747 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing1" -p "Character1_LeftHand";
	rename -uid "4C6A13CD-4B0B-B494-07A2-8A9E272EBD30";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -31.516946849722316 2.5758733401430476 -137.43155166436202 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing2" -p "Character1_LeftHandRing1";
	rename -uid "4BEBE73C-4BC3-8F81-137E-FC8576A6B900";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -37.632109434888541 57.372964012610872 -137.42548313576771 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing3" -p "Character1_LeftHandRing2";
	rename -uid "5280D48D-4204-4CCB-69DE-0390CA867A94";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -16.443988862208798 8.3290143241887424 -59.438981507600595 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightShoulder" -p "Character1_Spine1";
	rename -uid "BD144F34-4593-146D-0105-D693AA295D22";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -174.37324535156188 -81.21351031321781 -130.38505903740904 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightArm" -p "Character1_RightShoulder";
	rename -uid "06056155-44E7-5001-5280-36BC52E3B2BF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -104.33239449525594 -37.101615257968433 20.137114731244228 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightForeArm" -p "Character1_RightArm";
	rename -uid "6BD5664B-44C3-D78E-C0CA-E0942182BC22";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -43.743657547705759 -9.6726323149773243 103.83730983340543 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHand" -p "Character1_RightForeArm";
	rename -uid "4DCB0AE3-452B-274D-45CB-6FB34694AC4D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 140.03960701773187 13.516449256889247 -135.99675148159383 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb1" -p "Character1_RightHand";
	rename -uid "2007DE22-42D5-5090-BAB5-0FBD815D77A1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.10974980768816 71.94515848234029 -178.78334614629264 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb2" -p "Character1_RightHandThumb1";
	rename -uid "52F28114-4C9C-CBC5-86C2-C5A3D5AF5085";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.2056241799594538 33.542113270229827 -130.48989956049317 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb3" -p "Character1_RightHandThumb2";
	rename -uid "A9429B7E-4D85-DFEF-9801-B6ADEBDBE3ED";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 134.7539820196863 -70.074251222974951 75.428204415317296 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex1" -p "Character1_RightHand";
	rename -uid "99EBF011-4372-B777-5885-DBB465E5E238";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.10974980768816 71.94515848234029 -178.78334614629264 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex2" -p "Character1_RightHandIndex1";
	rename -uid "F31BD2F8-428A-1BC5-4066-A4B894A45BCB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.2057394778445882 33.54331930430493 -130.49010822234442 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex3" -p "Character1_RightHandIndex2";
	rename -uid "7E01473D-4D1F-FA9D-B5F1-0F920B0185FB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 134.79643182941854 -70.056599207814415 75.383915829770686 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing1" -p "Character1_RightHand";
	rename -uid "10EF2D68-4BA7-6865-5492-78B7012342F7";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.10974980768816 71.94515848234029 -178.78334614629264 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing2" -p "Character1_RightHandRing1";
	rename -uid "F9A4E2FA-4B3F-F5A3-1B87-93837E9A292F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.2056241799594538 33.542113270229827 -130.48989956049317 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing3" -p "Character1_RightHandRing2";
	rename -uid "F3FD43C0-4100-2186-9D4C-AAAD13D5153A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 134.7539820196863 -70.074251222974951 75.428204415317296 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Neck" -p "Character1_Spine1";
	rename -uid "F7E2DFD6-490A-9FA6-0277-768989CAE60D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 161.13402532275541 31.934232202611735 171.90006021381112 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Head" -p "Character1_Neck";
	rename -uid "1157AE07-4485-2117-5017-F8A524C32774";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -18.098521216998183 -9.9246993917445305 116.26360516393567 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "jaw" -p "Character1_Head";
	rename -uid "51B52863-41AC-889F-299C-32880E4941CF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.667422224424381 90 0 ;
	setAttr ".radi" 6;
	setAttr -k on ".liw" no;
	setAttr ".fbxID" 5;
createNode joint -n "eyes" -p "Character1_Head";
	rename -uid "5E9D1DB2-48CE-A014-D2C3-A6B50AB0D143";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.667422224424381 90 0 ;
	setAttr ".radi" 4.5;
	setAttr ".fbxID" 5;
createNode animCurveTU -n "jaw_lockInfluenceWeights";
	rename -uid "75A94FC2-49D7-5B39-8219-C9A0A6FF789B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "Character1_Hips_translateX";
	rename -uid "A88B58B6-4C2D-FECE-E16C-F8A4B4E7F1B9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -0.8014373779296875 1 -0.1322367936372757
		 2 0.53696376085281372 3 0.92585915327072144 4 0.85722321271896362 5 0.50828206539154053
		 6 0.13040879368782043 7 -0.25303778052330017 8 -0.61688214540481567 9 -0.92230510711669922
		 10 -1.2729954719543457 11 -1.6295499801635742 12 -1.9520338773727417 13 -2.1993143558502197
		 14 -2.3302581310272217 15 -2.3037323951721191 16 -2.0984506607055664 17 -1.7482043504714966
		 18 -1.2944785356521606 19 -0.77875667810440063 20 -0.24252352118492126 21 0.27287811040878296
		 22 0.72673237323760986 23 1.0784956216812134 24 1.2876245975494385 25 1.3688170909881592
		 26 1.3738769292831421 27 1.3163009881973267 28 1.208423376083374 29 1.0625776052474976
		 30 0.89211952686309814 31 0.71199768781661987 32 0.53693497180938721 33 0.381654292345047
		 34 0.26087856292724609 35 0.18884146213531494 36 0.1519772857427597 37 0.12662917375564575
		 38 0.11102032661437988 39 0.10337387770414352 40 0.10207688808441162 41 0.10612098127603531
		 42 0.11463620513677597 43 0.12675261497497559 44 0.14160028100013733 45 0.15816411375999451
		 46 0.17483031749725342 47 0.18983094394207001 48 0.20139800012111664 49 0.20776353776454926
		 50 0.20971280336380005 51 0.20909883081912994 52 0.20665101706981659 53 0.2036413848400116
		 54 0.20134201645851135 55 0.20102493464946747 56 0.20347288250923157 57 0.20863008499145508
		 58 0.21651084721088409 59 0.22712944447994232 60 0.24050016701221466 61 0.26444599032402039
		 62 0.30259841680526733 63 0.34868478775024414 64 0.39643228054046631 65 0.43956822156906128
		 66 0.47228413820266724 67 0.48745191097259521 68 0.4786522388458252 69 0.43371495604515076
		 70 0.35292536020278931 71 0.24854731559753418 72 0.13284462690353394 73 0.018081093207001686
		 74 -0.083479449152946472 75 -0.16319821774959564 76 -0.23916028439998627 77 -0.33918771147727966
		 78 -0.4805762767791748 79 -0.6460302472114563 80 -0.81148415803909302;
createNode animCurveTL -n "Character1_Hips_translateY";
	rename -uid "133CCBFF-4757-1F2D-05B6-668DEE108540";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 41.757061004638672 1 38.069908142089844
		 2 34.382759094238281 3 32.334323883056641 4 32.794094085693359 5 34.892585754394531
		 6 37.660057067871094 7 42.492755889892578 8 46.227916717529297 9 46.478420257568359
		 10 46.184062957763672 11 45.500545501708984 12 44.600238800048828 13 43.690231323242188
		 14 42.977615356445313 15 42.669467926025391 16 42.749889373779297 17 43.020973205566406
		 18 43.416801452636719 19 43.8714599609375 20 44.319046020507813 21 44.703174591064453
		 22 45.004997253417969 23 45.214908599853516 24 45.323291778564453 25 45.38043212890625
		 26 45.467761993408203 27 45.570224761962891 28 45.644947052001953 29 45.649040222167969
		 30 45.568836212158203 31 45.439594268798828 32 45.291881561279297 33 45.156257629394531
		 34 45.063282012939453 35 45.037357330322266 36 45.050529479980469 37 45.061637878417969
		 38 45.045562744140625 39 44.977188110351563 40 44.840717315673828 41 44.658084869384766
		 42 44.460811614990234 43 44.280399322509766 44 44.148365020751953 45 44.076530456542969
		 46 44.048084259033203 47 44.051605224609375 48 44.075668334960938 49 44.108848571777344
		 50 44.146255493164062 51 44.173892974853516 52 44.194389343261719 53 44.226219177246094
		 54 44.287853240966797 55 44.397762298583984 56 44.568256378173828 57 44.776157379150391
		 58 44.986709594726563 59 45.1651611328125 60 45.276752471923828 61 45.396106719970703
		 62 45.584903717041016 63 45.788959503173828 64 45.954093933105469 65 46.026126861572266
		 66 45.9981689453125 67 45.728927612304687 68 45.1492919921875 69 43.887321472167969
		 70 41.881618499755859 71 39.528877258300781 72 37.225799560546875 73 35.369075775146484
		 74 34.355396270751953 75 35.611141204833984 76 38.652301788330078 77 41.010025024414063
		 78 41.633548736572266 79 41.573635101318359 80 41.513721466064453;
createNode animCurveTL -n "Character1_Hips_translateZ";
	rename -uid "A5B41622-4E97-FD1F-6D9E-539425A2DD19";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -9.2881870269775391 1 -9.3194065093994141
		 2 -9.3506278991699219 3 -8.8227109909057617 4 -7.3011126518249512 5 -5.2203769683837891
		 6 -3.3250024318695068 7 -1.7383601665496826 8 -1.0500673055648804 9 -1.4411965608596802
		 10 -2.0684792995452881 11 -2.8424587249755859 12 -3.6716337203979492 13 -4.4602932929992676
		 14 -5.1127223968505859 15 -5.5332088470458984 16 -5.7434415817260742 17 -5.8449721336364746
		 18 -5.8618836402893066 19 -5.8182559013366699 20 -5.7381725311279297 21 -5.6444611549377441
		 22 -5.5550537109375 23 -5.4866843223571777 24 -5.4560904502868652 25 -5.4448542594909668
		 26 -5.4214482307434082 27 -5.389369010925293 28 -5.3553791046142578 29 -5.3262457847595215
		 30 -5.3051986694335937 31 -5.2894797325134277 32 -5.2768678665161133 33 -5.2651453018188477
		 34 -5.2520909309387207 35 -5.236091136932373 36 -5.2185664176940918 37 -5.2018284797668457
		 38 -5.189277172088623 39 -5.1843123435974121 40 -5.1891236305236816 41 -5.2009615898132324
		 42 -5.215815544128418 43 -5.2296772003173828 44 -5.2385373115539551 45 -5.241051197052002
		 46 -5.2395467758178711 47 -5.2355241775512695 48 -5.2304844856262207 49 -5.2259268760681152
		 50 -5.222196102142334 51 -5.2208824157714844 52 -5.2219648361206055 53 -5.2234964370727539
		 54 -5.2235326766967773 55 -5.2201285362243652 56 -5.2119426727294922 57 -5.2021389007568359
		 58 -5.195528507232666 59 -5.1969213485717773 60 -5.2111268043518066 61 -5.224794864654541
		 62 -5.2288107872009277 63 -5.2343387603759766 64 -5.2525434494018555 65 -5.2945899963378906
		 66 -5.3653049468994141 67 -5.4875292778015137 68 -5.6744284629821777 69 -5.9737019538879395
		 70 -6.3941397666931152 71 -6.8905439376831055 72 -7.4177179336547852 73 -7.9304637908935547
		 74 -8.3835849761962891 75 -8.7806510925292969 76 -9.1200084686279297 77 -9.3486118316650391
		 78 -9.4118633270263672 79 -9.3643627166748047 80 -9.316864013671875;
createNode animCurveTU -n "Character1_Hips_scaleX";
	rename -uid "FDA160D8-42C3-B998-C562-1C85B8D6C665";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_Hips_scaleY";
	rename -uid "D6159D00-446A-0037-AA0E-E8805A1793BD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_Hips_scaleZ";
	rename -uid "39F6AE7A-425F-2B9F-DA98-ABA92459FAD7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Hips_rotateX";
	rename -uid "6C846066-4F45-D79A-F5AC-4180FA763149";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -7.9859161376953116 1 -6.2514595985412598
		 2 -4.5920891761779785 3 -3.9671001434326167 4 -4.9671592712402344 5 -6.935767650604248
		 6 -8.9415817260742187 7 -10.90366268157959 8 -12.474521636962891 9 -13.204794883728027
		 10 -13.583268165588379 11 -14.169363021850586 12 -15.22453498840332 13 -16.386669158935547
		 14 -17.291519165039063 15 -17.573173522949219 16 -16.848457336425781 17 -15.328370094299318
		 18 -13.536319732666016 19 -11.994631767272949 20 -11.224088668823242 21 -11.594375610351563
		 22 -12.783711433410645 23 -14.273670196533203 24 -15.541967391967772 25 -16.041421890258789
		 26 -15.303685188293459 27 -13.760078430175781 28 -12.20024585723877 29 -11.408584594726563
		 30 -11.780634880065918 31 -12.873441696166992 32 -14.210036277770996 33 -15.311527252197264
		 34 -15.697460174560547 35 -15.026725769042969 36 -13.624486923217773 37 -12.004665374755859
		 38 -10.673969268798828 39 -10.137082099914551 40 -10.731378555297852 41 -12.118356704711914
		 42 -13.78748607635498 43 -15.225559234619141 44 -15.917306900024416 45 -15.752620697021484
		 46 -14.984502792358397 47 -13.901721000671387 48 -12.792534828186035 49 -11.943872451782227
		 50 -11.643572807312012 51 -12.160725593566895 52 -13.284360885620117 53 -14.583617210388185
		 54 -15.62516975402832 55 -15.973587036132813 56 -15.288851737976074 57 -13.863985061645508
		 58 -12.21085262298584 59 -10.839741706848145 60 -10.258828163146973 61 -10.661688804626465
		 62 -11.716300964355469 63 -13.141848564147949 64 -14.65593910217285 65 -15.973705291748047
		 66 -15.95027446746826 67 -15.87748908996582 68 -15.741393089294435 69 -15.529048919677734
		 70 -15.252516746520996 71 -14.936287879943848 72 -14.603157997131348 73 -14.273188591003418
		 74 -13.963181495666504 75 -13.660345077514648 76 -13.354256629943848 77 -13.060425758361816
		 78 -12.781999588012695 79 -12.517365455627441 80 -12.271350860595703;
createNode animCurveTA -n "Character1_Hips_rotateY";
	rename -uid "95C5BF29-438C-7431-A007-7AB8799F0EEC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 10.749359130859375 1 8.3951511383056641
		 2 5.9927096366882324 3 4.0931167602539062 4 3.0457322597503662 5 2.5655264854431152
		 6 2.2673759460449219 7 2.1051087379455566 8 2.0841410160064697 9 2.0342161655426025
		 10 1.9984390735626218 11 1.7829247713088989 12 1.3151005506515503 13 0.7670586109161377
		 14 0.30591318011283875 15 0.09422466903924942 16 0.26982560753822327 17 0.73085713386535645
		 18 1.2777395248413086 19 1.7035387754440308 20 1.7960009574890137 21 1.4090484380722046
		 22 0.69715136289596558 23 -0.10133080929517746 24 -0.75148344039916992 25 -1.0321493148803711
		 26 -0.7849918007850647 27 -0.19473245739936829 28 0.43156448006629944 29 0.77447354793548584
		 30 0.66515195369720459 31 0.27643829584121704 32 -0.20387387275695801 33 -0.59315180778503418
		 34 -0.71486186981201172 35 -0.42780226469039917 36 0.16058826446533203 37 0.84862184524536133
		 38 1.4195557832717896 39 1.6526222229003906 40 1.4002094268798828 41 0.80959427356719971
		 42 0.10197543352842331 43 -0.50551563501358032 44 -0.80199611186981201 45 -0.75417381525039673
		 46 -0.4786136150360108 47 -0.084296643733978271 48 0.31743285059928894 49 0.61179661750793457
		 50 0.67929983139038086 51 0.4107704758644104 52 -0.10499963164329529 53 -0.69218254089355469
		 54 -1.17998206615448 55 -1.40288245677948 56 -1.2131462097167969 57 -0.71064287424087524
		 58 -0.10405728220939636 59 0.39163419604301453 60 0.55718618631362915 61 0.26504170894622803
		 62 -0.36097452044487 63 -1.1628937721252441 64 -1.9841045141220093 65 -2.6712942123413086
		 66 -2.7218255996704102 67 -2.6373515129089355 68 -2.3769934177398682 69 -1.8962360620498659
		 70 -1.2132382392883301 71 -0.38086378574371338 72 0.54725772142410278 73 1.5165406465530396
		 74 2.4714944362640381 75 3.4575214385986328 76 4.522465705871582 77 5.6131892204284668
		 78 6.7190370559692383 79 7.8508038520812997 80 8.9866189956665039;
createNode animCurveTA -n "Character1_Hips_rotateZ";
	rename -uid "ADEBC9FE-4338-9A77-6573-CBAD75F2F484";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -7.546454906463623 1 -5.9299807548522949
		 2 -4.3930063247680664 3 -3.148883581161499 4 -2.2384700775146484 5 -1.5272201299667358
		 6 -0.92300313711166371 7 -0.4188438355922699 8 -0.11985401064157487 9 -0.024798208847641945
		 10 -0.058875769376754761 11 -0.09436972439289093 12 -0.048478391021490097 13 0.054569307714700699
		 14 0.180845707654953 15 0.28918322920799255 16 0.34613990783691406 17 0.39068537950515747
		 18 0.48440340161323553 19 0.66752833127975464 20 0.95799636840820313 21 1.356192946434021
		 22 1.8115078210830691 23 2.2568261623382568 24 2.6084194183349609 25 2.7919807434082031
		 26 2.731820821762085 27 2.4993741512298584 28 2.2482233047485352 29 2.1027495861053467
		 30 2.1182358264923096 31 2.2390537261962891 32 2.4067559242248535 33 2.5496053695678711
		 34 2.5827960968017578 35 2.4748227596282959 36 2.2962367534637451 37 2.1096911430358887
		 38 1.9704642295837405 39 1.9152818918228147 40 1.9665025472640991 41 2.103104829788208
		 42 2.2885594367980957 43 2.4666533470153809 44 2.5609846115112305 45 2.5290935039520264
		 46 2.3923659324645996 47 2.2083909511566162 48 2.0302302837371826 49 1.9029099941253664
		 50 1.8646578788757324 51 1.9529542922973631 52 2.1420447826385498 53 2.3711273670196533
		 54 2.5668783187866211 55 2.6449630260467529 56 2.5673036575317383 57 2.4080126285552979
		 58 2.2438890933990479 59 2.1318762302398682 60 2.109607458114624 61 2.2296011447906494
		 62 2.4848787784576416 63 2.8202009201049805 64 3.1733956336975098 65 3.4706342220306396
		 66 3.5090978145599365 67 3.4201462268829346 68 3.1646347045898438 69 2.7044634819030762
		 70 2.0625731945037842 71 1.2916011810302734 72 0.44324144721031189 73 -0.43212920427322388
		 74 -1.2855377197265625 75 -2.1575853824615479 76 -3.0895664691925049 77 -4.0352673530578613
		 78 -4.9861397743225098 79 -5.9520449638366699 80 -6.9153032302856445;
createNode animCurveTU -n "Character1_Hips_visibility";
	rename -uid "B2810819-451F-448E-26C6-22AC3268C974";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftUpLeg_translateX";
	rename -uid "B0B1E891-4C6C-C23E-0E16-C18BF1705FF9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.596480369567871;
createNode animCurveTL -n "Character1_LeftUpLeg_translateY";
	rename -uid "62AA87EA-40F5-BB4A-EA48-80BCB4486891";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.315338134765625;
createNode animCurveTL -n "Character1_LeftUpLeg_translateZ";
	rename -uid "4D10C1BD-45AF-FBCE-7BEE-6DB72000102E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.0558223724365234;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleX";
	rename -uid "9CE64028-4F8D-3709-DA9B-DDA860812D29";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleY";
	rename -uid "F47249B8-499F-078A-D5B5-D6B57DC7BE64";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleZ";
	rename -uid "E102EA23-4F10-9503-1A6D-15A5427634FA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateX";
	rename -uid "3EEBFB49-435D-DEB2-4B45-ED98375F135C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -8.2673254013061523 1 0.058417659252882004
		 2 7.9157438278198233 3 12.611660957336426 4 13.740798950195313 5 12.54664421081543
		 6 9.6083917617797852 7 9.030583381652832 8 11.694046020507813 9 11.044116020202637
		 10 10.112591743469238 11 9.4776010513305664 12 9.4962787628173828 13 9.9500923156738281
		 14 10.524204254150391 15 10.888396263122559 16 10.898944854736328 17 10.756487846374512
		 18 10.643397331237793 19 10.735010147094727 20 11.2022705078125 21 12.14470100402832
		 22 13.352375030517578 23 14.550336837768555 24 15.489805221557615 25 15.992513656616213
		 26 15.937858581542969 27 15.537702560424805 28 15.131406784057617 29 15.014296531677246
		 30 15.321004867553709 31 15.889678955078127 32 16.538175582885742 33 17.069742202758789
		 34 17.269863128662109 35 16.989166259765625 36 16.345720291137695 37 15.539108276367188
		 38 14.800098419189453 39 14.367401123046875 40 14.404760360717775 41 14.773488044738771
		 42 15.256238937377928 43 15.640137672424316 44 15.722332954406738 45 15.469247817993164
		 46 14.982276916503906 47 14.39850425720215 48 13.850030899047852 49 13.461610794067383
		 50 13.357254028320313 51 13.636178016662598 52 14.208406448364258 53 14.901609420776367
		 54 15.52442741394043 55 15.872200012207031 56 15.78427219390869 57 15.368009567260742
		 58 14.821374893188477 59 14.354061126708986 60 14.182098388671875 61 14.657189369201662
		 62 15.777228355407715 63 17.227474212646484 64 18.658710479736328 65 19.612077713012695
		 66 19.238828659057617 67 17.958042144775391 68 15.92916202545166 69 12.724760055541992
		 70 8.8084936141967773 71 5.0262646675109863 72 1.7050679922103882 73 -1.1652151346206665
		 74 -3.4702305793762207 75 -4.0414457321166992 76 -2.9874379634857178 77 -2.1830081939697266
		 78 -2.9903085231781006 79 -4.5353846549987793 80 -6.0700507164001465;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateY";
	rename -uid "87ADA907-46BC-D3F1-7AEE-EBAA2EC46E9D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 18.255149841308594 1 23.251880645751953
		 2 31.267467498779297 3 35.7200927734375 4 33.923419952392578 5 29.084560394287113
		 6 23.076694488525391 7 17.249780654907227 8 13.998442649841309 9 13.843633651733398
		 10 14.331398963928223 11 15.14143180847168 12 16.135490417480469 13 17.104415893554687
		 14 17.786594390869141 15 17.906156539916992 16 17.419544219970703 17 16.537921905517578
		 18 15.389750480651855 19 14.113119125366211 20 12.845365524291992 21 11.686790466308594
		 22 10.658527374267578 23 9.8000431060791016 24 9.2018423080444336 25 8.8372306823730469
		 26 8.6168670654296875 27 8.5188302993774414 28 8.5044269561767578 29 8.5667581558227539
		 30 8.6864023208618164 31 8.8361082077026367 32 8.9953908920288086 33 9.1524734497070312
		 34 9.3066701889038086 35 9.4280872344970703 36 9.510645866394043 37 9.5834388732910156
		 38 9.6522464752197266 39 9.7450971603393555 40 9.8896818161010742 41 10.065279960632324
		 42 10.236055374145508 43 10.375666618347168 44 10.469179153442383 45 10.524492263793945
		 46 10.561923027038574 47 10.575822830200195 48 10.565483093261719 49 10.539627075195312
		 50 10.511391639709473 51 10.496823310852051 52 10.48606014251709 53 10.45734691619873
		 54 10.40489673614502 55 10.328182220458984 56 10.218428611755371 57 10.071276664733887
		 58 9.9033746719360352 59 9.7420654296875 60 9.6256752014160156 61 9.4300050735473633
		 62 9.0288619995117188 63 8.4582147598266602 64 7.8176999092102051 65 7.3647956848144522
		 66 7.4867582321167001 67 8.1995277404785156 68 9.2811946868896484 69 10.630674362182617
		 70 11.873254776000977 71 12.953572273254395 72 14.01600170135498 73 15.05979633331299
		 74 15.889841079711916 75 16.170772552490234 76 16.24028205871582 77 16.639772415161133
		 78 17.420825958251953 79 18.315923690795898 80 19.143594741821289;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateZ";
	rename -uid "7126CC48-4E9F-470E-8BF0-4F8192268A45";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -12.22728443145752 1 -20.132043838500977
		 2 -25.155900955200195 3 -23.4088134765625 4 -16.296045303344727 5 -7.2335643768310538
		 6 2.1282832622528076 7 15.133026123046875 8 30.118951797485352 9 32.135868072509766
		 10 30.557741165161136 11 27.649898529052734 12 25.017768859863281 13 22.943464279174805
		 14 21.450723648071289 15 20.521623611450195 16 19.498281478881836 17 18.036979675292969
		 18 16.642416000366211 19 15.815730094909668 20 16.040582656860352 21 17.617462158203125
		 22 20.083110809326172 23 22.746517181396484 24 24.90045166015625 25 26.038448333740234
		 26 25.871217727661133 27 24.864274978637695 28 23.82948112487793 29 23.541351318359375
		 30 24.363845825195313 31 25.861589431762695 32 27.543394088745117 33 28.905681610107418
		 34 29.431194305419922 35 28.757160186767582 36 27.1942138671875 37 25.264305114746094
		 38 23.524097442626953 39 22.53422737121582 40 22.682510375976563 41 23.647590637207031
		 42 24.918405532836914 43 25.97247314453125 44 26.281770706176758 45 25.751180648803711
		 46 24.636268615722656 47 23.258810043334961 48 21.93780517578125 49 20.985857009887695
		 50 20.718782424926758 51 21.3902587890625 52 22.765275955200195 53 24.402004241943359
		 54 25.835369110107422 55 26.587980270385742 56 26.279119491577148 57 25.188880920410156
		 58 23.825492858886719 59 22.691045761108398 60 22.280059814453125 61 23.246191024780273
		 62 25.540531158447266 63 28.603836059570313 64 31.760906219482422 65 33.990234375
		 66 33.189689636230469 67 30.587003707885742 68 26.81056022644043 69 21.545392990112305
		 70 15.63700008392334 71 9.9957275390625 72 4.8799905776977539 73 0.3938460648059845
		 74 -3.2001581192016602 75 -4.6711616516113281 76 -3.6802880764007568 77 -2.6044447422027588
		 78 -3.9526059627532963 79 -6.3950929641723633 80 -8.823735237121582;
createNode animCurveTU -n "Character1_LeftUpLeg_visibility";
	rename -uid "E40781CF-46B6-719F-6322-E987F82D7555";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftLeg_translateX";
	rename -uid "C2F80D7D-42D0-42C4-DA75-17B4534A98A3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -21.532840728759766;
createNode animCurveTL -n "Character1_LeftLeg_translateY";
	rename -uid "0DBAFFCA-4963-A726-AC3C-A096BF772619";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4219808578491211;
createNode animCurveTL -n "Character1_LeftLeg_translateZ";
	rename -uid "CEB7FB5B-44F0-EFFA-1BEC-5C8B3F799F8F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8661990165710449;
createNode animCurveTU -n "Character1_LeftLeg_scaleX";
	rename -uid "7598BDCC-483E-34B2-1344-CC839381F1C6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftLeg_scaleY";
	rename -uid "C02068D5-4D84-3D5E-4E23-14A3C51FB1B3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftLeg_scaleZ";
	rename -uid "3A5A5095-4B61-90FD-3FBE-9FA4C06D4FF0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftLeg_rotateX";
	rename -uid "E619F7B3-4292-AE39-60F4-0B85D78B08ED";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -8.7684335708618164 1 11.919341087341309
		 2 38.626628875732422 3 53.436676025390625 4 44.507400512695313 5 25.247547149658203
		 6 6.3921122550964355 7 -12.91773509979248 8 -22.812921524047852 9 -24.007270812988281
		 10 -24.067001342773438 11 -23.161306381225586 12 -21.500818252563477 13 -19.588245391845703
		 14 -17.981924057006836 15 -17.269643783569336 16 -17.368770599365234 17 -17.727550506591797
		 18 -18.234731674194336 19 -18.786378860473633 20 -19.283012390136719 21 -19.656229019165039
		 22 -19.932140350341797 23 -20.138645172119141 24 -20.281511306762695 25 -20.435590744018555
		 26 -20.717283248901367 27 -21.093622207641602 28 -21.466817855834961 29 -21.722322463989258
		 30 -21.798244476318359 31 -21.764627456665039 32 -21.68731689453125 33 -21.625370025634766
		 34 -21.630983352661133 35 -21.718637466430664 36 -21.852603912353516 37 -22.000625610351562
		 38 -22.107212066650391 39 -22.108970642089844 40 -21.962108612060547 41 -21.710302352905273
		 42 -21.421667098999023 43 -21.158941268920898 44 -20.977012634277344 45 -20.902032852172852
		 46 -20.925165176391602 47 -21.020345687866211 48 -21.156354904174805 49 -21.294809341430664
		 50 -21.386764526367188 51 -21.370323181152344 52 -21.274852752685547 53 -21.1751708984375
		 54 -21.133392333984375 55 -21.200294494628906 56 -21.393817901611328 57 -21.665763854980469
		 58 -21.960969924926758 59 -22.214437484741211 60 -22.353775024414063 61 -22.417766571044922
		 62 -22.463541030883789 63 -22.442607879638672 64 -22.33357048034668 65 -22.221305847167969
		 66 -22.32469367980957 67 -22.352178573608398 68 -21.88481330871582 69 -20.226289749145508
		 70 -16.884792327880859 71 -12.05626392364502 72 -6.1080217361450195 73 0.31026613712310791
		 74 5.4222917556762695 75 2.9217338562011719 76 -4.9580340385437012 77 -9.7097415924072266
		 78 -9.4956417083740234 79 -7.5774431228637695 80 -5.774132251739502;
createNode animCurveTA -n "Character1_LeftLeg_rotateY";
	rename -uid "A1D5D27B-4D4D-A6BE-7FC6-FCA4FF14957D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -32.898441314697266 1 -44.205051422119141
		 2 -53.134346008300781 3 -56.442626953125 4 -55.342140197753906 5 -50.268161773681641
		 6 -41.671375274658203 7 -23.758945465087891 8 -0.20763687789440155 9 2.7290771007537842
		 10 0.62770193815231323 11 -3.942185640335083 12 -8.953526496887207 13 -13.262042045593262
		 14 -16.253328323364258 15 -17.52241325378418 16 -17.489923477172852 17 -16.929183959960937
		 18 -15.944350242614746 19 -14.629042625427246 20 -13.08284854888916 21 -11.408939361572266
		 22 -9.7101964950561523 23 -8.1504640579223633 24 -6.9525327682495117 25 -6.0207328796386719
		 26 -5.0642776489257813 27 -4.0494370460510254 28 -3.012570858001709 29 -2.1092174053192139
		 30 -1.4574861526489258 31 -0.99023985862731945 32 -0.66470080614089966 33 -0.45311990380287165
		 34 -0.35486802458763123 35 -0.34871107339859009 36 -0.48608151078224182 37 -0.82763409614562988
		 38 -1.3719699382781982 39 -2.1162867546081543 40 -3.0402803421020508 41 -4.0583558082580566
		 42 -5.0810279846191406 43 -6.0335907936096191 44 -6.850135326385498 45 -7.4896960258483887
		 46 -8.001063346862793 47 -8.3681344985961914 48 -8.5806980133056641 49 -8.646122932434082
		 50 -8.571502685546875 51 -8.4333057403564453 52 -8.2313947677612305 53 -7.9019432067871103
		 54 -7.4314985275268555 55 -6.8210182189941406 56 -6.0852856636047363 57 -5.287959098815918
		 58 -4.5204801559448242 59 -3.8820371627807613 60 -3.4808163642883301 61 -2.5716137886047363
		 62 -0.60826671123504639 63 2.0033164024353027 64 4.6483063697814941 65 6.1101174354553223
		 66 4.8825187683105469 67 0.67309904098510742 68 -5.4181051254272461 69 -13.970522880554199
		 70 -23.300205230712891 71 -31.435146331787109 72 -37.691596984863281 73 -41.978412628173828
		 74 -44.32257080078125 75 -42.979454040527344 76 -37.646121978759766 77 -32.482284545898438
		 78 -31.797250747680664 79 -33.077587127685547 80 -34.219871520996094;
createNode animCurveTA -n "Character1_LeftLeg_rotateZ";
	rename -uid "8893BD1F-4063-9CF5-FC94-20A40D82CDDD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -42.592575073242187 1 -57.912418365478509
		 2 -76.658088684082031 3 -87.204109191894531 4 -79.843223571777344 5 -64.362052917480469
		 6 -49.838401794433594 7 -29.105770111083984 8 -8.0562820434570312 9 -5.8111457824707031
		 10 -7.9243812561035156 11 -11.976977348327637 12 -16.167530059814453 13 -19.84477424621582
		 14 -22.577341079711914 15 -23.815570831298828 16 -23.781768798828125 17 -23.225313186645508
		 18 -22.239561080932617 19 -20.916912078857422 20 -19.361568450927734 21 -17.690114974975586
		 22 -16.028678894042969 23 -14.529944419860838 24 -13.383668899536133 25 -12.502415657043457
		 26 -11.629082679748535 27 -10.773930549621582 28 -9.9536676406860352 29 -9.2311086654663086
		 30 -8.6917266845703125 31 -8.2709636688232422 32 -7.9404792785644531 33 -7.7046074867248544
		 34 -7.5987443923950195 35 -7.6100840568542489 36 -7.7752437591552725 37 -8.1462039947509766
		 38 -8.701695442199707 39 -9.4150114059448242 40 -10.250105857849121 41 -11.143960952758789
		 42 -12.038959503173828 43 -12.888857841491699 44 -13.652310371398926 45 -14.304593086242676
		 46 -14.884659767150881 47 -15.350428581237793 48 -15.67052745819092 49 -15.82567024230957
		 50 -15.791828155517578 51 -15.611429214477537 52 -15.301639556884764 53 -14.843855857849121
		 54 -14.259095191955566 55 -13.606017112731934 56 -12.910996437072754 57 -12.201788902282715
		 58 -11.541164398193359 59 -10.998262405395508 60 -10.649203300476074 61 -9.7839956283569336
		 62 -7.9094123840332022 63 -5.4153475761413574 64 -2.8737118244171143 65 -1.449074387550354
		 66 -2.6357216835021973 67 -6.6103930473327637 68 -12.290140151977539 69 -20.538558959960938
		 70 -30.634035110473633 71 -41.377010345458984 72 -52.071884155273437 73 -61.594978332519538
		 74 -67.961143493652344 75 -63.998649597167976 76 -52.002140045166016 77 -43.136238098144531
		 78 -41.80303955078125 79 -43.375938415527344 80 -44.846435546875;
createNode animCurveTU -n "Character1_LeftLeg_visibility";
	rename -uid "81ABDF0A-4D68-3316-9B3F-3AAA4E48CF7A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFoot_translateX";
	rename -uid "935E9997-4A09-32D2-FF90-99A8856E1EAF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 18.368806838989258;
createNode animCurveTL -n "Character1_LeftFoot_translateY";
	rename -uid "B3419BD8-4FAE-50ED-3043-FCA945054F90";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.7176852226257324;
createNode animCurveTL -n "Character1_LeftFoot_translateZ";
	rename -uid "9D9D5023-4031-8311-337C-8280DBABD313";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3830562829971313;
createNode animCurveTU -n "Character1_LeftFoot_scaleX";
	rename -uid "DEB90D87-471F-0B19-C75F-6F82E5172E1C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleY";
	rename -uid "E47BB450-4BAA-92C0-2D3A-44AF46837802";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleZ";
	rename -uid "3A39AA9E-4FE9-3910-865B-7CB1457CB6FA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftFoot_rotateX";
	rename -uid "441E241C-4BD2-4FC1-3655-8E838AF19094";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -22.115396499633789 1 -14.417258262634277
		 2 5.2834224700927734 3 17.177581787109375 4 8.788665771484375 5 -10.561610221862793
		 6 -24.411382675170898 7 -25.802461624145508 8 -19.89729118347168 9 -18.915193557739258
		 10 -19.464834213256836 11 -20.701322555541992 12 -21.879482269287109 13 -22.751121520996094
		 14 -23.224636077880859 15 -23.213512420654297 16 -22.772563934326172 17 -22.085687637329102
		 18 -21.21783447265625 19 -20.239459991455078 20 -19.224353790283203 21 -18.247629165649414
		 22 -17.382587432861328 23 -16.685184478759766 24 -16.20616340637207 25 -15.903646469116211
		 26 -15.682374000549316 27 -15.54714298248291 28 -15.486008644104006 29 -15.470920562744141
		 30 -15.49665355682373 31 -15.548572540283201 32 -15.622607231140137 33 -15.717662811279299
		 34 -15.830914497375487 35 -15.936001777648928 36 -16.045814514160156 37 -16.194952011108398
		 38 -16.381681442260742 39 -16.601537704467773 40 -16.844089508056641 41 -17.090457916259766
		 42 -17.325939178466797 43 -17.542594909667969 44 -17.737226486206055 45 -17.913539886474609
		 46 -18.093852996826172 47 -18.263261795043945 48 -18.404499053955078 49 -18.497283935546875
		 50 -18.515584945678711 51 -18.45301628112793 52 -18.327035903930664 53 -18.158779144287109
		 54 -17.971582412719727 55 -17.793342590332031 56 -17.622091293334961 57 -17.448118209838867
		 58 -17.280590057373047 59 -17.133953094482422 60 -17.027448654174805 61 -16.779788970947266
		 62 -16.27714729309082 63 -15.635534286499023 64 -14.998846054077148 65 -14.631125450134277
		 66 -14.860963821411133 67 -15.74611282348633 68 -17.066112518310547 69 -19.134119033813477
		 70 -22.179712295532227 71 -26.293752670288086 72 -31.243221282958988 73 -35.773708343505859
		 74 -37.898754119873047 75 -33.583824157714844 76 -26.638153076171875 77 -22.493932723999023
		 78 -21.430614471435547 79 -21.427030563354492 80 -21.410055160522461;
createNode animCurveTA -n "Character1_LeftFoot_rotateY";
	rename -uid "DE677E01-4D49-2DF2-79FB-0780E1EDF3B2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -39.130428314208984 1 -49.992176055908203
		 2 -55.736286163330078 3 -57.002166748046875 4 -59.195007324218743 5 -56.476303100585938
		 6 -47.328926086425781 7 -29.156440734863278 8 -9.9400157928466797 9 -7.8421292304992676
		 10 -10.338092803955078 11 -15.257624626159666 12 -20.854362487792969 13 -26.020702362060547
		 14 -29.9502067565918 15 -31.916664123535156 16 -32.196704864501953 17 -31.691585540771484
		 18 -30.567512512207031 19 -28.987968444824219 20 -27.128000259399414 21 -25.16912841796875
		 22 -23.289474487304687 23 -21.706142425537109 24 -20.679012298583984 25 -20.141469955444336
		 26 -19.795862197875977 27 -19.580690383911133 28 -19.464599609375 29 -19.501394271850586
		 30 -19.73326301574707 31 -20.05927848815918 32 -20.400722503662109 33 -20.702810287475586
		 34 -20.940927505493164 35 -21.060737609863281 36 -21.176839828491211 37 -21.446731567382812
		 38 -21.865564346313477 39 -22.429836273193359 40 -23.124595642089844 41 -23.888362884521484
		 42 -24.656911849975586 43 -25.378204345703125 44 -26.007974624633789 45 -26.522281646728516
		 46 -26.954431533813477 47 -27.278059005737305 48 -27.476646423339844 49 -27.550058364868164
		 50 -27.457937240600586 51 -27.219366073608398 52 -26.847600936889648 53 -26.312992095947266
		 54 -25.626941680908203 55 -24.82612419128418 56 -23.939529418945313 57 -23.012786865234375
		 58 -22.119558334350586 59 -21.341148376464844 60 -20.767448425292969 61 -19.749364852905273
		 62 -17.828836441040039 63 -15.37993335723877 64 -12.927042961120605 65 -11.435911178588867
		 66 -12.095182418823242 67 -15.180599212646484 68 -19.977689743041992 69 -27.416221618652344
		 70 -36.386856079101563 71 -44.948429107666016 72 -52.084354400634766 73 -57.171566009521484
		 74 -59.760025024414063 75 -56.805221557617188 76 -49.029945373535156 77 -42.390434265136719
		 78 -40.426193237304688 79 -40.456512451171875 80 -40.514019012451172;
createNode animCurveTA -n "Character1_LeftFoot_rotateZ";
	rename -uid "66937555-465E-D6F4-1B53-F0ABEBB47902";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 11.882567405700684 1 0.95459264516830433
		 2 -19.946020126342773 3 -32.259437561035156 4 -24.626621246337891 5 -5.058189868927002
		 6 10.575173377990723 7 16.777561187744141 8 17.362482070922852 9 17.689668655395508
		 10 17.669536590576172 11 17.376407623291016 12 16.803186416625977 13 16.061588287353516
		 14 15.299273490905762 15 14.661833763122559 16 14.108767509460449 17 13.523179054260254
		 18 12.926624298095703 19 12.344565391540527 20 11.802121162414551 21 11.32479190826416
		 22 10.943473815917969 23 10.660035133361816 24 10.455021858215332 25 10.307394981384277
		 26 10.213319778442383 27 10.163481712341309 28 10.131214141845703 29 10.100123405456543
		 30 10.077206611633301 31 10.055137634277344 32 10.030851364135742 33 10.016234397888184
		 34 10.036243438720703 35 10.098183631896973 36 10.179287910461426 37 10.266752243041992
		 38 10.353549957275391 39 10.429586410522461 40 10.48484992980957 41 10.522027969360352
		 42 10.550067901611328 43 10.580719947814941 44 10.62678050994873 45 10.699698448181152
		 46 10.800493240356445 47 10.906373023986816 48 10.999425888061523 49 11.064779281616211
		 50 11.097707748413086 51 11.097081184387207 52 11.068751335144043 53 11.025882720947266
		 54 10.987822532653809 55 10.990756034851074 56 11.041034698486328 57 11.112918853759766
		 58 11.191089630126953 59 11.26193904876709 60 11.314077377319336 61 11.297524452209473
		 62 11.21055793762207 63 11.111030578613281 64 11.041562080383301 65 11.02567195892334
		 66 11.078070640563965 67 11.227217674255371 68 11.530611038208008 69 12.304646492004395
		 70 13.795295715332031 71 16.017646789550781 72 18.909608840942383 73 21.824880599975586
		 74 23.218812942504883 75 19.556825637817383 76 14.374762535095215 77 11.777312278747559
		 78 11.173457145690918 79 11.081952095031738 80 10.897116661071777;
createNode animCurveTU -n "Character1_LeftFoot_visibility";
	rename -uid "33275CFC-4ADB-8323-7D58-79AED04CED47";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftToeBase_translateX";
	rename -uid "EF32B24B-4D67-52D0-D1A3-309098D04793";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3813290596008301;
createNode animCurveTL -n "Character1_LeftToeBase_translateY";
	rename -uid "30B0367A-4584-2B2F-DCEC-AAA7BCBD320E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.3898367881774902;
createNode animCurveTL -n "Character1_LeftToeBase_translateZ";
	rename -uid "1EF15A4F-4C77-02DF-6595-8F9026A7B78D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2284431457519531;
createNode animCurveTU -n "Character1_LeftToeBase_scaleX";
	rename -uid "1A49D723-409E-88E4-37D0-ECAE86857E29";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftToeBase_scaleY";
	rename -uid "75E5611F-4993-EFEE-41F0-948E8A54B5C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftToeBase_scaleZ";
	rename -uid "8728FC5E-4330-674D-7775-9FB57F6B6307";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftToeBase_rotateX";
	rename -uid "A7434F99-4208-01FF-E77A-0E996B4584B2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  2 1.6660667290224751e-009 3 2.0493553520850583e-009
		 4 -0.79966974258422852 5 -2.7600142955780029 6 -5.2065844535827637 7 -7.354473590850831
		 8 -8.2837333679199219 9 -7.8306217193603516 10 -6.6702666282653809 11 -5.1047563552856445
		 12 -3.4175024032592773 13 -1.8596184253692627 14 -0.65229344367980957 15 5.8589124662944414e-009
		 16 0.14916218817234039 17 0.028538530692458153 18 -0.29593804478645325 19 -0.75973588228225708
		 20 -1.2974917888641357 21 -1.841549873352051 22 -2.3213260173797607 23 -2.6636345386505127
		 24 -2.7941296100616455 25 -2.7272131443023682 26 -2.5441997051239014 27 -2.2720251083374023
		 28 -1.9376330375671387 29 -1.567566990852356 30 -1.1877176761627197 31 -0.82321840524673462
		 32 -0.49847817420959473 33 -0.23733462393283841 34 -0.063315190374851227 35 4.3150807371716837e-009
		 36 4.1452614674142296e-009 48 3.86305698540923e-009 49 3.7862690760448459e-009 50 -0.022455340251326561
		 51 -0.085522443056106567 52 -0.18280388414859772 53 -0.30793255567550659 54 -0.45453134179115295
		 55 -0.61618083715438843 56 -0.78639465570449829 57 -0.95860308408737194 58 -1.1261447668075562
		 59 -1.2822675704956055 60 -1.4201387166976929 61 -1.5848797559738159 62 -1.8044229745864868
		 63 -2.0458102226257324 64 -2.2755157947540283 65 -2.459402322769165 66 -2.5629146099090576
		 67 -2.5515103340148926 68 -2.3913369178771973 69 -1.8360563516616821 70 -0.86731737852096558
		 71 0.21446333825588226 72 1.1297940015792847 73 1.6174674034118652 74 1.6955287456512451
		 75 1.5868523120880127 76 1.3415622711181641 77 1.0093276500701904 78 0.64042437076568604
		 79 0.28635632991790771 80 8.375895532708454e-010;
createNode animCurveTA -n "Character1_LeftToeBase_rotateY";
	rename -uid "881C18BB-45AF-4A5F-B82C-24859925C494";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  2 2.6916477380289905e-010 3 3.7410480963062653e-011
		 4 1.1212501525878906 5 3.757109403610229 6 6.8271055221557617 7 9.3287210464477539
		 8 10.356989860534668 9 9.8595485687255859 10 8.5507993698120117 11 6.7037267684936523
		 12 4.6052560806274414 13 2.56561279296875 14 0.91617155075073242 15 -1.2548639904963466e-009
		 16 -0.21125979721546173 17 -0.038435157388448715 18 0.42154803872108459 19 1.070770263671875
		 20 1.8119029998779295 21 2.5492401123046875 22 3.1891522407531738 23 3.6398546695709229
		 24 3.8103923797607417 25 3.7229514122009277 26 3.4828598499298096 27 3.1232337951660156
		 28 2.6771824359893799 29 2.1781003475189209 30 1.6598465442657471 31 1.156816840171814
		 32 0.70391964912414551 33 0.33646854758262634 34 0.089996606111526489 35 -4.2826328594536278e-010
		 36 -3.7955227849550965e-010 48 -3.7149594511731721e-010 49 -3.9382822003553031e-010
		 50 0.032061517238616943 51 0.12198922038078308 52 0.26036167144775391 53 0.43773603439331055
		 54 0.64467644691467285 55 0.87177711725234985 56 1.1096796989440918 57 1.3490849733352661
		 58 1.580758810043335 59 1.7955310344696045 60 1.9842891693115234 61 2.2082738876342773
		 62 2.5044763088226318 63 2.8276505470275879 64 3.1329550743103027 65 3.3759796619415283
		 66 3.5126135349273682 67 3.4987359046936035 68 3.2897365093231201 69 2.555018424987793
		 70 1.2423624992370605 71 -0.26979970932006836 72 -1.5882152318954468 73 -2.3066515922546387
		 74 -2.4256401062011719 75 -2.2689917087554932 76 -1.9124535322189331 77 -1.4320966005325317
		 78 -0.90355670452117931 79 -0.40159547328948975 80 1.1530775223533851e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateZ";
	rename -uid "9059A5C6-447E-E1EF-8205-8289FCECC065";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  2 2.3293753592668054e-009 3 2.1801827010392572e-009
		 4 -0.64156770706176758 5 -2.2369928359985352 6 -4.2682676315307617 7 -6.0832867622375488
		 8 -6.8766708374023437 9 -6.4891557693481445 10 -5.5019063949584961 11 -4.1824488639831543
		 12 -2.7781238555908203 13 -1.4997199773788452 14 -0.52243095636367798 15 1.9075456492156917e-010
		 16 0.11816932260990144 17 0.020696397870779037 18 -0.24032026529312134 19 -0.6145140528678894
		 20 -1.0504293441772461 21 -1.4937407970428467 22 -1.8865505456924438 23 -2.167858362197876
		 24 -2.2753219604492187 25 -2.2201085090637207 26 -2.069272518157959 27 -1.8454189300537107
		 28 -1.5711725950241089 29 -1.2686929702758789 30 -0.95936411619186401 31 -0.66365736722946167
		 32 -0.40115454792976379 33 -0.19072315096855164 34 -0.05083116888999939 35 1.5610408521382624e-009
		 36 1.650533709707247e-009 48 2.2783703812478961e-009 49 2.3773103485780211e-009 50 -0.018138259649276733
		 51 -0.069102145731449127 52 -0.14777849614620209 53 -0.24909114837646484 54 -0.36795055866241455
		 55 -0.49921360611915594 56 -0.63765376806259155 57 -0.77794110774993896 58 -0.91463375091552734
		 59 -1.0421788692474365 60 -1.1549268960952759 61 -1.2893720865249634 62 -1.4683878421783447
		 63 -1.6654757261276245 64 -1.8534469604492188 65 -2.0043778419494629 66 -2.0898451805114746
		 67 -2.0814468860626221 68 -1.951608419418335 69 -1.5011667013168335 70 -0.72068959474563599
		 71 0.14179375767707825 72 0.86384636163711548 73 1.2465118169784546 74 1.3099180459976196
		 75 1.2286028861999512 76 1.0409632921218872 77 0.78481918573379517 78 0.49881431460380554
		 79 0.22320280969142914 80 2.1298338648279014e-009;
createNode animCurveTU -n "Character1_LeftToeBase_visibility";
	rename -uid "FA04E29D-408A-D66A-A697-AF87F61BC74B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateX";
	rename -uid "DF64A7A7-4E01-ADA4-1AE1-ABA89D105F14";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.1971817016601563;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateY";
	rename -uid "91242BD3-42D3-D650-A5A2-68B35277DF11";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.7569773197174072;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateZ";
	rename -uid "DB99FA45-4D15-59F3-97A5-2F9081F198BA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4053930044174194;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleX";
	rename -uid "E18B48F1-4BFF-AE73-1DCB-0095A15EDB55";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleY";
	rename -uid "C4F2ADE7-4BC7-532F-513F-86977C8FD99C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999958276748657;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleZ";
	rename -uid "838D3117-41F9-CCF2-C6BC-5C97ECCA4722";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateX";
	rename -uid "5E0031E6-49BF-0A76-27FC-5492308EC6E0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.6568018068596757e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateY";
	rename -uid "D9E83C95-49E5-9648-D171-57884604AC3F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.2495760505830739e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateZ";
	rename -uid "549A25E2-45A5-E164-6B93-9294FCBA2106";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.119714251873738e-010;
createNode animCurveTU -n "Character1_LeftFootMiddle1_visibility";
	rename -uid "22FAEF69-493B-C710-35AE-30A65113D9A0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateX";
	rename -uid "D68049B8-4099-24E6-5125-E988DBA3358C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8763875961303711;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateY";
	rename -uid "9BD10193-4356-1A08-E4FD-1791D8905842";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.1604456901550293;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateZ";
	rename -uid "4A2CCB1B-4F25-C3E1-1384-A2989AEB8F3A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.313550591468811;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleX";
	rename -uid "8277C35D-4B43-3AC7-21DB-0C9431E9A1A7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleY";
	rename -uid "753EF79C-44CA-4596-FB00-809986E73355";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleZ";
	rename -uid "407A73DF-4EBF-9FC6-0ABE-A6B4FF21D62B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateX";
	rename -uid "9D847892-4A4C-F209-5EAC-909613EF47F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.5457031168422191e-010;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateY";
	rename -uid "CD61C2CA-4CD4-F39A-DD9C-3F85AB00DED8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3283386834928024e-008;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateZ";
	rename -uid "1594A1A6-4CCF-E1E6-3F70-33B52A5847A9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.9161930344184839e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle2_visibility";
	rename -uid "81B88EFA-4BC4-CAA7-A659-74AA4CA71C9A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightUpLeg_translateX";
	rename -uid "F212F9FA-47AC-3091-8714-B1B325558E79";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.4649467468261719;
createNode animCurveTL -n "Character1_RightUpLeg_translateY";
	rename -uid "A9DC4DD0-41EA-4919-B7EB-97898E30F637";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.110023021697998;
createNode animCurveTL -n "Character1_RightUpLeg_translateZ";
	rename -uid "1A4E2B90-4370-EC4D-F393-04A60D473B98";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.4359440803527832;
createNode animCurveTU -n "Character1_RightUpLeg_scaleX";
	rename -uid "4D543CA9-4C72-9068-1907-5DB660B6A0A2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightUpLeg_scaleY";
	rename -uid "BCC92FAD-485A-4308-C823-10B017BD48F7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightUpLeg_scaleZ";
	rename -uid "703AE01B-481F-226E-54EA-789C1EAF25EE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightUpLeg_rotateX";
	rename -uid "C9AE8AE3-4762-8A97-9439-5DA171AABF1E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 26.973535537719727 1 38.799617767333984
		 2 50.489986419677734 3 55.664669036865234 4 48.671657562255859 5 37.248210906982422
		 6 26.839561462402344 7 12.181840896606445 8 -3.0589599609375 9 -4.7783441543579102
		 10 -2.8835983276367187 11 0.31644439697265625 12 3.0808262825012207 13 5.0866732597351074
		 14 6.3631715774536133 15 7.0511288642883301 16 7.9197897911071786 17 9.2949199676513672
		 18 10.577031135559082 19 11.144291877746582 20 10.39964485168457 21 7.9947500228881836
		 22 4.5519070625305176 23 0.99638670682907104 24 -1.6867579221725464 25 -2.8095386028289795
		 26 -2.0896511077880859 27 -0.13030490279197693 28 2.0577290058135986 29 3.4374663829803467
		 30 3.4221019744873047 31 2.5052802562713623 32 1.2715549468994141 33 0.3124663233757019
		 34 0.21925452351570129 35 1.4201186895370483 36 3.567101001739502 37 6.0518407821655273
		 38 8.1845703125 39 9.2805728912353516 40 8.8841829299926758 41 7.4385771751403809
		 42 5.61724853515625 43 4.0919671058654785 44 3.5198025703430176 45 4.0038943290710449
		 46 5.2194705009460449 47 6.7897539138793945 48 8.3351612091064453 49 9.4723978042602539
		 50 9.7876100540161133 51 8.9252958297729492 52 7.1892905235290527 53 5.1545333862304687
		 54 3.3929102420806885 55 2.4709486961364746 56 2.8586077690124512 57 4.2289071083068848
		 58 5.9457950592041016 59 7.3592486381530753 60 7.8187260627746582 61 6.704747200012207
		 62 4.2261910438537598 63 0.9882907271385194 64 -2.3344817161560059 65 -4.9215598106384277
		 66 -4.6881313323974609 67 -2.9905121326446533 68 0.12025423347949983 69 5.5547347068786621
		 70 12.498684883117676 71 19.502561569213867 72 25.856861114501953 73 30.951154708862305
		 74 34.081798553466797 75 32.305999755859375 76 26.217628479003906 77 20.771678924560547
		 78 19.544326782226563 79 20.311733245849609 80 21.10589599609375;
createNode animCurveTA -n "Character1_RightUpLeg_rotateY";
	rename -uid "77864D19-48E5-8CDE-7B09-9DAA2DF6050B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 36.329387664794922 1 43.131118774414062
		 2 50.892696380615234 3 53.629814147949219 4 49.087028503417969 5 40.235813140869141
		 6 30.427425384521484 7 23.45875358581543 8 21.472335815429687 9 21.592319488525391
		 10 23.012214660644531 11 25.386335372924805 12 28.326513290405273 13 31.335153579711914
		 14 33.874038696289062 15 35.417823791503906 16 36.123905181884766 17 36.487773895263672
		 18 36.521797180175781 19 36.297878265380859 20 35.940773010253906 21 35.561176300048828
		 22 35.205348968505859 23 34.904670715332031 24 34.724788665771484 25 34.685478210449219
		 26 34.793308258056641 27 34.959487915039063 28 35.060699462890625 29 35.087150573730469
		 30 35.084327697753906 31 35.057163238525391 32 34.996250152587891 33 34.925834655761719
		 34 34.904823303222656 35 34.958385467529297 36 35.013114929199219 37 35.008323669433594
		 38 34.946384429931641 39 34.884700775146484 40 34.865684509277344 41 34.842140197753906
		 42 34.759147644042969 43 34.624507904052734 44 34.512931823730469 45 34.480525970458984
		 46 34.514537811279297 47 34.569408416748047 48 34.611976623535156 49 34.633186340332031
		 50 34.644203186035156 51 34.654548645019531 52 34.650535583496094 53 34.620372772216797
		 54 34.591999053955078 55 34.627635955810547 56 34.756938934326172 57 34.903232574462891
		 58 34.992294311523438 59 35.016036987304688 60 35.027320861816406 61 35.081439971923828
		 62 35.164821624755859 63 35.23590087890625 64 35.264049530029297 65 35.231941223144531
		 66 35.211528778076172 67 35.096572875976562 68 34.930446624755859 69 34.653343200683594
		 70 34.410858154296875 71 34.355258941650391 72 34.485397338867188 73 34.739059448242188
		 74 35.035488128662109 75 35.422843933105469 76 35.983314514160156 77 36.568740844726562
		 78 36.866085052490234 79 36.982357025146484 80 37.067623138427734;
createNode animCurveTA -n "Character1_RightUpLeg_rotateZ";
	rename -uid "F94C9C08-4F71-1B27-BA67-D291208AD50F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -10.592131614685059 1 -12.934423446655273
		 2 -12.90416431427002 3 -13.718198776245117 4 -19.491376876831055 5 -23.857328414916992
		 6 -23.770971298217773 7 -20.240104675292969 8 -16.849826812744141 9 -16.262603759765625
		 10 -16.383699417114258 11 -17.10508918762207 12 -18.311595916748047 13 -19.708278656005859
		 14 -20.96588134765625 15 -21.702140808105469 16 -21.772708892822266 17 -21.468482971191406
		 18 -21.085590362548828 19 -20.936302185058594 20 -21.302453994750977 21 -22.331415176391602
		 22 -23.737163543701172 23 -25.149303436279297 24 -26.232315063476563 25 -26.695001602172852
		 26 -26.261405944824219 27 -25.188144683837891 28 -24.024557113647461 29 -23.33525276184082
		 30 -23.411741256713867 31 -23.966970443725586 32 -24.694292068481445 33 -25.284391403198242
		 34 -25.430265426635742 35 -24.955717086791992 36 -24.077999114990234 37 -23.087541580200195
		 38 -22.302200317382812 39 -22.028938293457031 40 -22.46537971496582 41 -23.400077819824219
		 42 -24.512876510620117 43 -25.47776985168457 44 -25.977264404296875 45 -25.925722122192383
		 46 -25.461978912353516 47 -24.774358749389648 48 -24.051946640014648 49 -23.484159469604492
		 50 -23.258228302001953 51 -23.558319091796875 52 -24.250911712646484 53 -25.048307418823242
		 54 -25.660503387451172 55 -25.799125671386719 56 -25.272708892822266 57 -24.289039611816406
		 58 -23.167390823364258 59 -22.236629486083984 60 -21.81999397277832 61 -22.026493072509766
		 62 -22.638511657714844 63 -23.48151969909668 64 -24.380401611328125 65 -25.170680999755859
		 66 -25.171272277832031 67 -25.203968048095703 68 -25.224184036254883 69 -25.314428329467773
		 70 -25.411661148071289 71 -25.380800247192383 72 -25.164663314819336 73 -24.717422485351563
		 74 -23.981443405151367 75 -22.253578186035156 76 -19.567789077758789 77 -17.088630676269531
		 78 -15.626164436340334 79 -14.64713191986084 80 -13.663820266723633;
createNode animCurveTU -n "Character1_RightUpLeg_visibility";
	rename -uid "E69D6CB0-4E87-BCFA-1D81-63B2C723074A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightLeg_translateX";
	rename -uid "107F68C3-4565-D977-FCBB-C894A293A50D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3108952045440674;
createNode animCurveTL -n "Character1_RightLeg_translateY";
	rename -uid "DD3811FC-4210-D624-E882-DAB5AB6813D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 22.181583404541016;
createNode animCurveTL -n "Character1_RightLeg_translateZ";
	rename -uid "B396C055-4D11-4442-191D-0DA801AC0771";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.6536058187484741;
createNode animCurveTU -n "Character1_RightLeg_scaleX";
	rename -uid "FF374D55-4736-97EC-6783-348F03C5E710";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightLeg_scaleY";
	rename -uid "3C192B6C-4BAC-75A4-4881-4F9DC9E6048F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightLeg_scaleZ";
	rename -uid "E99A7AFB-462D-0226-D51D-68AEF7D86C0E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightLeg_rotateX";
	rename -uid "B4BB1BFB-4CB5-E60B-9E8B-04AFD7636834";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 9.1717491149902344 1 26.730249404907227
		 2 40.651493072509766 3 47.678153991699219 4 49.296646118164063 5 46.329063415527344
		 6 38.617298126220703 7 16.753152847290039 8 -3.9631831645965572 9 -5.9122123718261719
		 10 -4.591346263885498 11 -1.3744293451309204 12 2.5266697406768799 13 6.2156949043273926
		 14 8.8669281005859375 15 9.7067070007324219 16 8.9965591430664062 17 7.5896987915039062
		 18 5.5987386703491211 19 3.1217114925384521 20 0.24736028909683228 21 -2.9233865737915039
		 22 -6.1478066444396973 23 -9.029728889465332 24 -11.003992080688477 25 -11.925431251525879
		 26 -12.182639122009277 27 -11.959329605102539 28 -11.377580642700195 29 -10.577749252319336
		 30 -9.7221622467041016 31 -8.885589599609375 32 -8.074772834777832 33 -7.294090747833252
		 34 -6.547724723815918 35 -5.8157267570495605 36 -5.0079069137573242 37 -4.0862011909484863
		 38 -3.1649188995361328 39 -2.3500642776489258 40 -1.7103532552719116 41 -1.1996163129806519
		 42 -0.74803376197814941 43 -0.29146164655685425 44 0.22688530385494229 45 0.75753527879714966
		 46 1.2590322494506836 47 1.6833059787750244 48 1.9836950302124026 49 2.1141781806945801
		 50 1.9794089794158936 51 1.5445470809936523 52 0.87736904621124268 53 0.040396671742200851
		 54 -0.90178269147872936 55 -1.8812820911407473 56 -2.7818100452423096 57 -3.550828218460083
		 58 -4.216524600982666 59 -4.8155074119567871 60 -5.3897013664245605 61 -6.6094522476196289
		 62 -8.8261775970458984 63 -11.594539642333984 64 -14.355183601379396 65 -16.296310424804688
		 66 -16.182727813720703 67 -13.665451049804688 68 -8.8101472854614258 69 0.10884324461221695
		 70 11.876826286315918 71 23.771085739135742 72 34.154338836669922 73 41.746421813964844
		 74 45.443737030029297 75 39.402442932128906 76 24.382673263549805 77 11.518566131591797
		 78 7.9162707328796387 79 8.4396276473999023 80 8.9661350250244141;
createNode animCurveTA -n "Character1_RightLeg_rotateY";
	rename -uid "66BAE1F7-4CFF-12FB-4658-33A75F322DEC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 22.336856842041016 1 27.34276008605957
		 2 23.965862274169922 3 20.5469970703125 4 21.593297958374023 5 25.683755874633789
		 6 30.138740539550778 7 26.814136505126953 8 9.8173303604125977 9 7.2513561248779288
		 10 9.5530223846435547 11 13.854960441589355 12 17.762300491333008 13 20.405349731445313
		 14 21.747318267822266 15 22.003576278686523 16 21.609888076782227 17 20.947219848632812
		 18 20.010486602783203 19 18.769216537475586 20 17.20930290222168 21 15.37962532043457
		 22 13.447399139404297 23 11.688353538513184 24 10.499024391174316 25 9.9515209197998047
		 26 9.7185535430908203 27 9.7126483917236328 28 9.9366950988769531 29 10.392243385314941
		 30 11.005112648010254 31 11.656741142272949 32 12.288083076477051 33 12.861092567443848
		 34 13.353277206420898 35 13.771542549133301 36 14.195782661437988 37 14.67778491973877
		 38 15.176761627197264 39 15.650862693786621 40 16.063652038574219 41 16.415243148803711
		 42 16.721698760986328 43 17.005632400512695 44 17.292900085449219 45 17.563114166259766
		 46 17.80555534362793 47 18.004011154174805 48 18.140186309814453 49 18.194004058837891
		 50 18.118844985961914 51 17.901872634887695 52 17.57861328125 53 17.175065994262695
		 54 16.712987899780273 55 16.209306716918945 56 15.705238342285156 57 15.226840972900392
		 58 14.770895004272461 59 14.343926429748535 60 13.963627815246582 61 13.238448143005371
		 62 11.913987159729004 63 10.230718612670898 64 8.5244894027709961 65 7.3410596847534189
		 66 7.4488492012023926 67 9.1976261138916016 68 12.315574645996094 69 17.30339241027832
		 70 22.459009170532227 71 26.170236587524414 72 28.356584548950195 73 29.506719589233395
		 74 30.20217132568359 75 30.473844528198242 76 28.110649108886719 77 23.322250366210937
		 78 21.458761215209961 79 21.822788238525391 80 22.204551696777344;
createNode animCurveTA -n "Character1_RightLeg_rotateZ";
	rename -uid "CE6C94B5-49D9-38C4-A956-CF8F7A295FE1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 36.241134643554688 1 57.981674194335938
		 2 75.54193115234375 3 83.123703002929688 4 82.679771423339844 5 76.542442321777344
		 6 64.741470336914063 7 40.889369964599609 8 21.216438293457031 9 19.471225738525391
		 10 21.770542144775391 11 26.591848373413086 12 32.519207000732422 13 38.277194976806641
		 14 42.739360809326172 15 44.921829223632812 16 45.195827484130859 17 44.670558929443359
		 18 43.515491485595703 19 41.940868377685547 20 40.213665008544922 21 38.597515106201172
		 22 37.218540191650391 23 36.178810119628906 24 35.628105163574219 25 35.418556213378906
		 26 35.162464141845703 27 34.866188049316406 28 34.676433563232422 29 34.786781311035156
		 30 35.269264221191406 31 35.949275970458984 32 36.678966522216797 33 37.330524444580078
		 34 37.794578552246094 35 38.000709533691406 36 38.051795959472656 37 38.081302642822266
		 38 38.186225891113281 39 38.491558074951172 40 39.080894470214844 41 39.848110198974609
		 42 40.644557952880859 43 41.348667144775391 44 41.867954254150391 45 42.162227630615234
		 46 42.287601470947266 47 42.282054901123047 48 42.188556671142578 49 42.059593200683594
		 50 41.926971435546875 51 41.860282897949219 52 41.836212158203125 53 41.756904602050781
		 54 41.541980743408203 55 41.126834869384766 56 40.485725402832031 57 39.701171875
		 58 38.881668090820313 59 38.163936614990234 60 37.710739135742188 61 37.256465911865234
		 62 36.524394989013672 63 35.665439605712891 64 34.869316101074219 65 34.413608551025391
		 66 34.523647308349609 67 35.712200164794922 68 38.053752899169922 69 42.772102355957031
		 70 49.640522003173828 71 57.051010131835937 72 63.705760955810547 73 68.504592895507813
		 74 70.516029357910156 75 65.11346435546875 76 52.795921325683594 77 42.365261077880859
		 78 38.752460479736328 79 38.012897491455078 80 37.268138885498047;
createNode animCurveTU -n "Character1_RightLeg_visibility";
	rename -uid "2F53C55B-4EB1-7762-A5CA-169E8358E10B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFoot_translateX";
	rename -uid "F52D2ADF-400F-9251-A534-34BCEFC96FCF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.668137550354004;
createNode animCurveTL -n "Character1_RightFoot_translateY";
	rename -uid "01CE2137-43FE-2B8C-7361-0FAF8159961A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3660926818847656;
createNode animCurveTL -n "Character1_RightFoot_translateZ";
	rename -uid "77A6A645-4775-795A-1668-B0B081939055";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -15.681424140930176;
createNode animCurveTU -n "Character1_RightFoot_scaleX";
	rename -uid "F3E00D2D-4FEF-0F7B-76B9-6AA945D82EA1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightFoot_scaleY";
	rename -uid "66953658-4FC0-34C0-E4CA-F59D438F99D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999958276748657;
createNode animCurveTU -n "Character1_RightFoot_scaleZ";
	rename -uid "9FC6D0A7-416D-D98F-B313-DE8C627206B2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFoot_rotateX";
	rename -uid "779718FC-471C-9E26-3F01-51980C1387F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -0.13029107451438904 1 5.4448909759521484
		 2 13.719953536987305 3 17.94841194152832 4 14.772312164306642 5 7.6166005134582528
		 6 -0.10240593552589417 7 -5.3785700798034668 8 -5.5345921516418457 9 -4.7890338897705078
		 10 -3.8962922096252441 11 -2.5656559467315674 12 -0.63917356729507446 13 1.6006335020065308
		 14 3.6075766086578369 15 4.7439923286437988 16 4.9381532669067383 17 4.6117448806762695
		 18 3.9031400680541992 19 2.9758343696594238 20 2.0025568008422852 21 1.1216773986816406
		 22 0.38038548827171326 23 -0.18203647434711456 24 -0.49073672294616694 25 -0.60215079784393311
		 26 -0.67141836881637573 27 -0.69510400295257568 28 -0.64589965343475342 29 -0.48561254143714905
		 30 -0.20131286978721619 31 0.15278826653957367 32 0.52211767435073853 33 0.8506353497505188
		 34 1.0818854570388794 35 1.163124680519104 36 1.13063645362854 37 1.0555253028869629
		 38 0.98775541782379139 39 0.99204212427139271 40 1.1192722320556641 41 1.3333927392959595
		 42 1.5722914934158325 43 1.7772998809814453 44 1.8958822488784792 45 1.9160977602005005
		 46 1.8718695640563965 47 1.788330078125 48 1.6940561532974243 49 1.6218241453170776
		 50 1.5998638868331909 51 1.6574867963790894 52 1.769179105758667 53 1.8803509473800659
		 54 1.945543646812439 55 1.929180979728699 56 1.8059898614883423 57 1.6083918809890747
		 58 1.3905876874923706 59 1.2096868753433228 60 1.1247117519378662 61 1.0947203636169434
		 62 1.0283164978027344 63 0.91095036268234242 64 0.76170152425765991 65 0.6622389554977417
		 66 0.70507299900054932 67 1.0483133792877197 68 1.6533329486846924 69 2.6455674171447754
		 70 3.7348339557647701 71 4.5589261054992676 72 5.0071578025817871 73 5.072969913482666
		 74 4.7983798980712891 75 4.0142149925231934 76 2.8900198936462402 77 1.9672508239746096
		 78 1.3867547512054443 79 0.85145819187164307 80 0.29841136932373047;
createNode animCurveTA -n "Character1_RightFoot_rotateY";
	rename -uid "CB47E58F-4FA5-FA83-A7B1-50A0932A62A1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -0.0623842105269432 1 -0.54287987947463989
		 2 -0.074131667613983154 3 0.33436107635498047 4 0.19857214391231537 5 -0.84676086902618408
		 6 -2.2782895565032959 7 -2.0493955612182617 8 0.021737560629844666 9 0.26753699779510498
		 10 0.03289518877863884 11 -0.35269704461097717 12 -0.62834572792053223 13 -0.71283966302871704
		 14 -0.65851080417633057 15 -0.56953847408294678 16 -0.49571287631988525 17 -0.41961723566055298
		 18 -0.3238392174243927 19 -0.19066129624843597 20 -0.0098941447213292122 21 0.21561922132968903
		 22 0.46689951419830328 23 0.70704793930053711 24 0.87632685899734497 25 0.95500838756561268
		 26 0.98315352201461803 27 0.97304773330688477 28 0.92704242467880249 29 0.84708482027053833
		 30 0.74385637044906616 31 0.63519787788391113 32 0.53173857927322388 33 0.4409334659576416
		 34 0.36772942543029785 35 0.31306493282318115 36 0.26526761054992676 37 0.21460598707199097
		 38 0.16350704431533813 39 0.11386708915233611 40 0.06779828667640686 41 0.026802675798535347
		 42 -0.0088166315108537674 43 -0.04029083251953125 44 -0.0699123814702034 45 -0.095764495432376862
		 46 -0.11740364879369737 47 -0.13380023837089539 48 -0.14369004964828491 49 -0.14579285681247711
		 50 -0.13620179891586304 51 -0.11450195312500001 52 -0.083617396652698517 53 -0.044683072715997696
		 54 0.0013841894688084722 55 0.054081037640571594 56 0.11017412692308426 57 0.16678878664970398
		 58 0.22350190579891205 59 0.27807149291038513 60 0.3263033926486969 61 0.40866580605506897
		 62 0.55458927154541016 63 0.74238556623458862 64 0.93926960229873646 65 1.0836710929870605
		 66 1.0826511383056641 67 0.89499557018280029 68 0.56619805097579956 69 0.068091586232185364
		 70 -0.41293036937713623 71 -0.75560426712036133 72 -0.98390400409698486 73 -1.1350657939910889
		 74 -1.2236185073852539 75 -1.1197166442871094 76 -0.68789291381835938 77 -0.14336298406124115
		 78 0.037218153476715088 79 -0.019217701628804207 80 -0.08016233891248703;
createNode animCurveTA -n "Character1_RightFoot_rotateZ";
	rename -uid "8601CB87-4F00-1B1C-5B21-418D150CB8DD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -0.53402864933013916 1 -7.9476666450500488
		 2 -10.67994213104248 3 -11.899694442749023 4 -15.805294990539551 5 -20.135299682617188
		 6 -21.16792106628418 7 -13.47492504119873 8 -0.19014172255992889 9 1.88813841342926
		 10 0.81884688138961792 11 -1.690799355506897 12 -4.1959028244018555 13 -6.036252498626709
		 14 -6.9690113067626953 15 -6.9076728820800781 16 -6.1206645965576172 17 -4.9919395446777344
		 18 -3.5688371658325195 19 -1.8885388374328611 20 0.0045894100330770016 21 2.0432932376861572
		 22 4.0871949195861816 23 5.9028363227844238 24 7.1404876708984375 25 7.7119569778442392
		 26 7.8846220970153809 27 7.7683515548706055 28 7.4066710472106942 29 6.8392939567565918
		 30 6.147796630859375 31 5.4268412590026855 32 4.7263679504394531 33 4.0830097198486328
		 34 3.5231788158416748 35 3.0491569042205811 36 2.5867650508880615 37 2.0834419727325439
		 38 1.5846183300018311 39 1.1249228715896606 40 0.72800445556640625 41 0.38917255401611328
		 42 0.09434160590171814 43 -0.1790279746055603 44 -0.46016532182693481 45 -0.72748047113418579
		 46 -0.96342611312866222 47 -1.1490702629089355 48 -1.2668825387954712 49 -1.301804780960083
		 50 -1.2133522033691406 51 -0.99121993780136108 52 -0.6620098352432251 53 -0.24213449656963348
		 54 0.24744272232055661 55 0.78168022632598877 56 1.305483341217041 57 1.7903989553451538
		 58 2.2480370998382568 59 2.6820318698883057 60 3.0863630771636963 61 3.8304243087768559
		 62 5.1449861526489258 63 6.8038406372070313 64 8.4958581924438477 65 9.7279348373413086
		 66 9.7674427032470703 67 8.355499267578125 68 5.6526279449462891 69 0.96999168395996094
		 70 -4.5163283348083496 71 -9.2935771942138672 72 -12.854037284851074 73 -15.024354934692383
		 74 -15.75587272644043 75 -13.293227195739746 76 -7.3212013244628906 77 -1.3639631271362305
		 78 0.41118934750556946 79 -0.10349518805742264 80 -0.60929501056671143;
createNode animCurveTU -n "Character1_RightFoot_visibility";
	rename -uid "EF9442F3-4C3A-0364-6753-889A580BCB10";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightToeBase_translateX";
	rename -uid "B93E0F32-4C2E-D798-A936-3996B4B000CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.103175163269043;
createNode animCurveTL -n "Character1_RightToeBase_translateY";
	rename -uid "9E321BD4-4905-3F80-AD00-C8B54F1E9E13";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.7509346008300781;
createNode animCurveTL -n "Character1_RightToeBase_translateZ";
	rename -uid "E5F3F8EA-4F24-AB80-C7A1-D988CC23FA5F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.6768350601196289;
createNode animCurveTU -n "Character1_RightToeBase_scaleX";
	rename -uid "C6D8EFC5-48DF-1473-A3EA-6F9C1BC5A540";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightToeBase_scaleY";
	rename -uid "F5F3FD1B-4867-9738-E16D-7FA3A16E953E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightToeBase_scaleZ";
	rename -uid "7E8F38DF-4FCD-2B7D-445F-D8AD14FFED36";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightToeBase_rotateX";
	rename -uid "A5F5F6BA-479B-C7B5-9FC1-799090EF748D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.7451921564013446e-009;
createNode animCurveTA -n "Character1_RightToeBase_rotateY";
	rename -uid "C6C2D03E-4412-D347-C253-F0BF085447E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6944986960284485e-012;
createNode animCurveTA -n "Character1_RightToeBase_rotateZ";
	rename -uid "B68F36F6-420B-522C-71C4-7AAC5C87B317";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.4473941730500712e-011;
createNode animCurveTU -n "Character1_RightToeBase_visibility";
	rename -uid "E490086F-456D-163C-8D1F-0BBF6BC18122";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateX";
	rename -uid "AC19CB8B-496A-8035-0628-0FAFE739513D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.31789970397949219;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateY";
	rename -uid "65228C61-4A85-EFE2-F671-5EAC8000EB40";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.6937241554260254;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateZ";
	rename -uid "8E739B8D-4C6E-4C7A-7EEE-A5B1F3A6EE55";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.4678492546081543;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleX";
	rename -uid "46B1E87E-4480-13D7-4146-809C9D8B792A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleY";
	rename -uid "FE8FEDEB-47CF-8048-0018-AE8CF5D57962";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleZ";
	rename -uid "430EC9B7-415E-A787-6C7C-BD9837BA036D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateX";
	rename -uid "03D76392-49D7-C3C8-253E-96A5152FC074";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.3147915523557003e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateY";
	rename -uid "698960DD-4444-0DB1-095A-BABA0CB5C4CC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.8928173451988641e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateZ";
	rename -uid "FD51119D-4371-5FE5-6A99-F9ADFAA8A4F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.6990106795117299e-009;
createNode animCurveTU -n "Character1_RightFootMiddle1_visibility";
	rename -uid "DE841566-44FB-DED8-E8E3-659C221BD1C2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateX";
	rename -uid "E30411D4-4AEF-160C-2941-83849768F4F6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3449039459228516;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateY";
	rename -uid "227701CF-4E7E-29E3-8B6C-8F9D688779D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3579239845275879;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateZ";
	rename -uid "AB66D96D-4A38-B2B3-A8AE-45A180495135";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.7014765739440918;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleX";
	rename -uid "D4338E92-4F03-BD97-42B2-A7B45C77AA6D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleY";
	rename -uid "CDE792DC-41B1-A203-C336-DAA994271862";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleZ";
	rename -uid "FDDBB21D-44FB-4B97-6BE3-39A9AB972B3C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateX";
	rename -uid "E2DEEF1B-4A4B-605B-77DE-969642EB72F1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.0076489864729865e-008;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateY";
	rename -uid "E9A57571-43B4-0027-8A98-FB8C3928AC44";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.9133777612599943e-009;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateZ";
	rename -uid "5EFEFEBF-43BA-9F5A-32C2-A6B080DD4064";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5658656593586784e-008;
createNode animCurveTU -n "Character1_RightFootMiddle2_visibility";
	rename -uid "75D0FE48-421C-05B6-4BAB-FFB16B87E753";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Spine_translateX";
	rename -uid "7707E747-4B98-F1CC-AD04-34950CE1F9B8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.9174466133117676;
createNode animCurveTL -n "Character1_Spine_translateY";
	rename -uid "75BCBAF7-4CBA-AF27-9ECD-A0BA81F58228";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.894325256347656;
createNode animCurveTL -n "Character1_Spine_translateZ";
	rename -uid "2847334C-4024-5C87-E74F-3AB91E0A9552";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1938900947570801;
createNode animCurveTU -n "Character1_Spine_scaleX";
	rename -uid "8EA7517C-4210-6C6A-BBD0-A7B5A5D1276B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine_scaleY";
	rename -uid "E4AD569F-4232-BE21-1FFD-D19F073E167A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine_scaleZ";
	rename -uid "9B06C668-4B39-A840-2E41-1EB8D29BD7B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_Spine_rotateX";
	rename -uid "441163F1-4411-70B7-82AF-4AA8A6C22766";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  0 -12.008066177368164 1 -10.8389892578125
		 2 -9.6512918472290039 3 -6.2935075759887695 4 0.35659441351890564 5 8.9127864837646484
		 6 17.754117965698242 7 25.240745544433594 8 29.733757019042972 9 31.203268051147461
		 10 31.037082672119137 11 29.724578857421875 12 27.756410598754883 13 25.623147964477539
		 14 23.814584732055664 15 22.819803237915039 16 22.373291015625 17 21.931068420410156
		 18 21.564216613769531 19 21.343523025512695 20 21.339412689208984 21 21.589788436889648
		 22 22.003629684448242 23 22.457939147949219 24 22.82988166809082 25 23.049430847167969
		 26 23.196006774902344 27 23.366985321044922 28 23.554994583129883 29 23.75269889831543
		 30 23.952796936035156 31 24.148029327392578 32 24.331180572509766 33 24.495077133178711
		 34 24.632568359375 35 24.630983352661133 36 24.456689834594727 37 24.219858169555664
		 38 24.01939582824707 39 23.954143524169922 40 24.09037971496582 41 24.364479064941406
		 42 24.680349349975586 43 24.941926956176758 44 25.053197860717773 45 25.056163787841797
		 47 25.059194564819336 48 25.062002182006836 50 25.071550369262695 51 25.069656372070313
		 52 25.062639236450195 53 25.051332473754883 54 25.036558151245117 55 25.019128799438477
		 56 24.893987655639648 57 24.621023178100586 58 24.298688888549805 59 24.025661468505859
		 60 23.90074348449707 61 24.098760604858398 62 24.614128112792969 63 25.275039672851563
		 64 25.909719467163086 65 26.34648323059082 66 26.259048461914062 67 25.761032104492188
		 68 24.729940414428711 69 22.858526229858398 70 20.115104675292969 71 16.79083251953125
		 72 13.176803588867187 73 9.5640220642089844 74 6.2432565689086914 75 3.1671779155731201
		 76 0.098650574684143066 77 -2.9569556713104248 78 -5.9950532913208008 79 -9.012476921081543
		 80 -12.008066177368164;
createNode animCurveTA -n "Character1_Spine_rotateY";
	rename -uid "0CE9E2E0-40DE-3CCE-8B13-79B1D68D52A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 1.3196120262145996 1 0.90700811147689819
		 2 0.54220688343048096 3 0.14002889394760132 4 -0.14945782721042633 5 -0.012531333602964878
		 6 0.62478810548782349 7 1.5082050561904907 8 2.2185783386230469 9 2.579195499420166
		 10 2.7186479568481445 11 2.6982967853546143 12 2.5910696983337402 13 2.4667694568634033
		 14 2.3837180137634277 15 2.38783860206604 16 2.4646077156066895 17 2.5662398338317871
		 18 2.6832156181335449 19 2.8051977157592773 20 2.9214165210723877 21 3.0217339992523193
		 22 3.0974597930908203 23 3.1397190093994141 24 3.1384851932525635 25 3.0895059108734131
		 26 3.0009281635284424 27 2.8809595108032227 28 2.7372791767120361 29 2.5777740478515625
		 30 2.41058349609375 31 2.2441112995147705 32 2.0870182514190674 33 1.9481918811798096
		 34 1.8366837501525877 35 1.7599396705627441 36 1.7135932445526123 37 1.6874493360519409
		 38 1.679701566696167 39 1.6878557205200195 40 1.7091165781021118 41 1.7400645017623901
		 42 1.7768756151199341 43 1.8149311542510986 44 1.8488602638244627 45 1.8757580518722532
		 46 1.894255518913269 47 1.9011191129684446 48 1.893119215965271 49 1.8670274019241333
		 50 1.8167005777359009 51 1.7417672872543335 52 1.6476544141769409 53 1.5397855043411255
		 54 1.4235842227935791 55 1.3044755458831787 56 1.1860474348068237 57 1.0734280347824097
		 58 0.97415685653686523 59 0.89499133825302124 60 0.84190207719802856 61 0.83666694164276123
		 62 0.88458782434463501 63 0.9665224552154541 64 1.0620932579040527 65 1.1493986845016479
		 66 1.2018623352050781 67 1.2053638696670532 68 1.1419180631637573 69 0.99353218078613281
		 70 0.77578842639923096 71 0.52651423215866089 72 0.28698599338531494 73 0.09709089994430542
		 74 -0.011733358725905418 75 -0.031921938061714172 76 0.040139909833669662 77 0.21359024941921234
		 78 0.48988956212997437 79 0.86297845840454102 80 1.3196120262145996;
createNode animCurveTA -n "Character1_Spine_rotateZ";
	rename -uid "A52C93BE-4C92-0278-EE03-B7A5FB6DD945";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 6.5334582328796387 1 5.3690519332885742
		 2 4.192716121673584 3 2.7103393077850342 4 0.70091867446899414 5 -1.5368672609329224
		 6 -3.5180411338806152 7 -4.913240909576416 8 -5.6352925300598145 9 -5.8851103782653809
		 10 -5.9081273078918457 11 -5.7769927978515625 12 -5.5451579093933105 13 -5.2688436508178711
		 14 -5.016568660736084 15 -4.867030143737793 16 -4.7786712646484375 17 -4.6702995300292969
		 18 -4.5623965263366699 19 -4.475430965423584 20 -4.4301052093505859 21 -4.4373540878295898
		 22 -4.4681310653686523 23 -4.4836139678955078 24 -4.4452533721923828 25 -4.3159642219543457
		 26 -4.1137566566467285 27 -3.8815534114837651 28 -3.6296377182006836 29 -3.3681683540344238
		 30 -3.107135534286499 31 -2.8563351631164551 32 -2.6253664493560791 33 -2.4236485958099365
		 34 -2.260462760925293 35 -2.1125035285949707 36 -1.9584252834320068 37 -1.8115719556808472
		 38 -1.6995218992233276 39 -1.6500152349472046 40 -1.6807700395584106 41 -1.7692222595214844
		 42 -1.8827676773071289 43 -1.988935589790344 44 -2.0553927421569824 45 -2.0923073291778564
		 46 -2.1297767162322998 47 -2.1653926372528076 48 -2.196746826171875 49 -2.2214338779449463
		 50 -2.2474708557128906 51 -2.282977819442749 52 -2.3257026672363281 53 -2.3733894824981689
		 54 -2.4237821102142334 55 -2.4746298789978027 56 -2.4911956787109375 57 -2.4586827754974365
		 58 -2.4047985076904297 59 -2.3573992252349854 60 -2.3444783687591553 61 -2.3897194862365723
		 62 -2.4799125194549561 63 -2.5875248908996582 64 -2.6859369277954102 65 -2.7500784397125244
		 66 -2.7085680961608887 67 -2.6241097450256348 68 -2.4838948249816895 69 -2.2671964168548584
		 70 -1.9510791301727295 71 -1.5178987979888916 72 -0.96112507581710815 73 -0.29110407829284668
		 74 0.46413800120353699 75 1.3199182748794556 76 2.2911486625671387 77 3.3375406265258789
		 78 4.4189705848693848 79 5.4965219497680664 80 6.5334582328796387;
createNode animCurveTU -n "Character1_Spine_visibility";
	rename -uid "D4A65C2D-4256-3821-33FA-26A5E76C99D5";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Spine1_translateX";
	rename -uid "8FC3DC17-4A48-8528-C7C7-1DA49CB94B5D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2515636682510376;
createNode animCurveTL -n "Character1_Spine1_translateY";
	rename -uid "F72EA278-48B2-8E97-E8E2-139232FF4739";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -15.818062782287598;
createNode animCurveTL -n "Character1_Spine1_translateZ";
	rename -uid "2814A42B-44DF-1963-7F41-7C9D587FFB85";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.5716743469238281;
createNode animCurveTU -n "Character1_Spine1_scaleX";
	rename -uid "84A415AF-4318-A7EC-7664-7E91B84A2202";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine1_scaleY";
	rename -uid "64B4D09F-4843-481E-A78B-FAA753DDEBC3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine1_scaleZ";
	rename -uid "532CCE18-45B3-D0C2-4954-9897638AC96F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_Spine1_rotateX";
	rename -uid "233BAD8F-495D-3E28-92B7-D7BE3A505EBC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -16.682300567626953 1 -16.742132186889648
		 2 -16.904352188110352 3 -14.997391700744629 4 -10.196393013000488 5 -3.8919661045074467
		 6 2.7239503860473633 7 8.4357585906982422 8 12.104207992553711 9 13.885433197021484
		 10 14.798628807067871 11 14.206313133239746 12 11.96910285949707 13 9.0943603515625
		 14 6.6308135986328125 15 5.6394858360290527 16 6.5751571655273437 17 8.6430892944335938
		 18 11.019392013549805 19 12.904549598693848 20 13.542411804199219 21 12.470342636108398
		 22 10.269545555114746 23 7.7632102966308603 24 5.7619547843933105 25 5.0373697280883789
		 26 6.5079760551452637 27 9.5703516006469727 28 12.674568176269531 29 14.28945255279541
		 30 13.819241523742676 31 12.168322563171387 32 10.086909294128418 33 8.3534650802612305
		 34 7.7653532028198242 35 8.850834846496582 36 11.079090118408203 37 13.656274795532227
		 38 15.782579421997069 39 16.685647964477539 40 15.864696502685547 41 13.844703674316406
		 42 11.410030364990234 43 9.3596591949462891 44 8.4975032806396484 45 9.1119556427001953
		 46 10.652955055236816 47 12.663698196411133 48 14.692701339721678 49 16.298166275024414
		 50 17.051275253295898 51 16.407569885253906 52 14.59626293182373 53 12.359370231628418
		 54 10.488723754882812 55 9.8245172500610352 56 10.957079887390137 57 13.35334300994873
		 58 16.166641235351562 59 18.533655166625977 60 19.594820022583008 61 19.246128082275391
		 62 18.120126724243164 63 16.46794319152832 64 14.554186820983887 65 12.65655517578125
		 66 12.473749160766602 67 11.699621200561523 68 10.158632278442383 69 7.0539426803588867
		 70 2.2241981029510498 71 -3.5152316093444824 72 -9.2908821105957031 73 -14.243586540222168
		 74 -17.61253547668457 75 -19.338079452514648 76 -20.107122421264648 77 -20.32130241394043
		 78 -20.343046188354492 79 -20.519210815429687 80 -21.206758499145508;
createNode animCurveTA -n "Character1_Spine1_rotateY";
	rename -uid "FE145A25-4F94-7651-0F59-4A8BEBC14183";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 27.137903213500977 1 23.603069305419922
		 2 20.090147018432617 3 16.776529312133789 4 13.616880416870117 5 10.926555633544922
		 6 9.2695159912109375 7 8.6568918228149414 8 8.5236024856567383 9 8.372288703918457
		 10 8.2019882202148437 11 7.8194003105163583 12 7.2589640617370605 13 6.8260865211486816
		 14 6.650296688079834 15 6.6605463027954102 16 6.8918085098266602 17 7.4387803077697745
		 18 8.2324943542480469 19 9.0692949295043945 20 9.6300859451293945 21 9.6955556869506836
		 22 9.4727611541748047 23 9.2248697280883789 24 9.065974235534668 25 9.0107278823852539
		 26 9.0146007537841797 27 9.148982048034668 28 9.4609403610229492 29 9.6327247619628906
		 30 9.3960647583007813 31 8.9620771408081055 32 8.5478668212890625 33 8.2704172134399414
		 34 8.1392374038696289 35 8.2500467300415039 36 8.6873998641967773 37 9.3731794357299805
		 38 10.083926200866699 39 10.461755752563477 40 10.246720314025879 41 9.6790761947631836
		 42 9.099034309387207 43 8.7176675796508789 44 8.6100311279296875 45 8.6904964447021484
		 46 8.8527021408081055 47 9.105931282043457 48 9.4084758758544922 49 9.6441545486450195
		 50 9.6265811920166016 51 9.1665105819702148 52 8.3944311141967773 53 7.5803995132446289
		 54 6.8836455345153809 55 6.3473176956176758 56 6.0896921157836914 57 6.1905364990234375
		 58 6.5678749084472656 59 6.9804830551147461 60 7.0368709564208984 61 6.6047425270080566
		 62 5.9250822067260742 63 5.143712043762207 64 4.3941173553466797 65 3.7729697227478027
		 66 3.6467771530151367 67 3.4234066009521484 68 3.0960593223571777 69 2.461024284362793
		 70 1.7661730051040649 71 1.6570725440979004 72 2.5323348045349121 73 4.4360179901123047
		 74 7.1112966537475586 75 10.398086547851562 76 14.21706485748291 77 18.378671646118164
		 78 22.722686767578125 79 27.08555793762207 80 31.294910430908203;
createNode animCurveTA -n "Character1_Spine1_rotateZ";
	rename -uid "DD04AA13-4F73-67DB-3F03-B9B1B8B52FCA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 3.2056405544281006 1 4.5236024856567383
		 2 5.9514250755310059 3 5.3898048400878906 4 1.7216581106185913 5 -3.9478676319122314
		 6 -10.490827560424805 7 -16.520664215087891 8 -20.600135803222656 9 -22.652538299560547
		 10 -23.68665885925293 11 -22.899526596069336 12 -20.14588737487793 13 -16.674144744873047
		 14 -13.756497383117676 15 -12.674434661865234 16 -14.010865211486816 17 -16.814601898193359
		 18 -20.066074371337891 19 -22.771308898925781 20 -23.965126037597656 21 -23.005653381347656
		 22 -20.53748893737793 23 -17.546716690063477 24 -15.060646057128906 25 -14.124347686767578
		 26 -15.947702407836914 27 -19.74415397644043 28 -23.549125671386719 29 -25.422657012939453
		 30 -24.626609802246094 31 -22.349233627319336 32 -19.568094253540039 33 -17.269960403442383
		 34 -16.446359634399414 35 -17.770811080932617 36 -20.576314926147461 37 -23.842208862304688
		 38 -26.564691543579102 39 -27.781536102294922 40 -26.857782363891602 41 -24.424596786499023
		 42 -21.442972183227539 43 -18.915849685668945 44 -17.879653930664062 45 -18.700349807739258
		 46 -20.662425994873047 47 -23.182737350463867 48 -25.679885864257812 49 -27.580661773681641
		 50 -28.302095413208008 51 -27.158185958862305 52 -24.507541656494141 53 -21.33690071105957
		 54 -18.645362854003906 55 -17.438295364379883 56 -18.3978271484375 57 -20.849761962890625
		 58 -23.769987106323242 59 -26.180023193359375 60 -27.15934944152832 61 -26.658500671386719
		 62 -25.406148910522461 63 -23.661819458007813 64 -21.689291000366211 65 -19.766500473022461
		 66 -19.784423828125 67 -19.105560302734375 68 -17.508144378662109 69 -14.257265090942383
		 70 -9.2137565612792969 71 -3.1382575035095215 72 3.0612781047821045 73 8.4110269546508789
		 74 12.019219398498535 75 13.789695739746094 76 14.438728332519531 77 14.426223754882814
		 78 14.149636268615723 79 13.902114868164063 80 13.853244781494141;
createNode animCurveTU -n "Character1_Spine1_visibility";
	rename -uid "F1D3B805-40C6-9C29-20EF-DAAC0102B143";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftShoulder_translateX";
	rename -uid "A8C24145-4482-2ED7-6A47-12A2A8E7B94D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.49134865403175354;
createNode animCurveTL -n "Character1_LeftShoulder_translateY";
	rename -uid "6E479EAE-4EB2-2FF4-6F8A-6CA718076DDF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.20281982421875;
createNode animCurveTL -n "Character1_LeftShoulder_translateZ";
	rename -uid "87F7167F-49FD-6851-9B7E-C994225A9F81";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.910428047180176;
createNode animCurveTU -n "Character1_LeftShoulder_scaleX";
	rename -uid "A8EE5F29-479F-E05E-5A9F-BB8FF0499299";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftShoulder_scaleY";
	rename -uid "12885B39-4340-55BE-FD9A-EBAD21D6BA61";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999934434890747;
createNode animCurveTU -n "Character1_LeftShoulder_scaleZ";
	rename -uid "9FBFCDCD-4147-13E7-D8F0-D99F4D52E6CC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftShoulder_rotateX";
	rename -uid "DC3A70C5-4831-090B-27C1-CBB1E76715FF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -14.023898124694824 1 -16.562200546264648
		 2 -19.151239395141602 3 -19.77734375 4 -17.821796417236328 5 -14.548683166503904
		 6 -10.731199264526367 7 -7.2011771202087393 8 -4.9276390075683594 9 -4.016270637512207
		 10 -3.7567944526672363 11 -4.5137710571289062 12 -6.1648797988891602 13 -7.9148492813110352
		 14 -9.2275638580322266 15 -9.7450723648071289 16 -9.3725519180297852 17 -8.449986457824707
		 18 -7.2978239059448251 19 -6.2817926406860352 20 -5.8397388458251953 21 -6.2821717262268066
		 22 -7.298759937286377 23 -8.4510822296142578 24 -9.3733205795288086 25 -9.7168149948120117
		 26 -9.0200920104980469 27 -7.4270529747009286 28 -5.5705046653747559 29 -4.4140205383300781
		 30 -4.5807161331176758 31 -5.551234245300293 32 -6.8239789009094238 33 -7.927575111389161
		 34 -8.4101104736328125 35 -7.9811000823974609 36 -6.8886027336120605 37 -5.4911913871765137
		 38 -4.2438502311706543 39 -3.6975328922271724 40 -4.2423849105834961 41 -5.4869980812072754
		 42 -6.882603645324707 43 -7.9759430885314941 44 -8.3745021820068359 45 -7.9347600936889648
		 46 -6.9374442100524902 47 -5.6214427947998047 48 -4.248931884765625 49 -3.1420323848724365
		 50 -2.6849939823150635 51 -3.3257110118865967 52 -4.7897791862487793 53 -6.4413814544677734
		 54 -7.7773261070251465 55 -8.3745021820068359 56 -7.9811000823974609 57 -6.8886027336120605
		 58 -5.4911913871765137 59 -4.2438502311706543 60 -3.6975328922271724 61 -4.1734180450439453
		 62 -5.3509006500244141 63 -6.8576040267944336 64 -8.3570680618286133 65 -9.552891731262207
		 66 -9.5241355895996094 67 -9.1957893371582031 68 -8.4807281494140625 69 -6.7763352394104004
		 70 -4.0355477333068848 71 -0.99074923992156971 72 1.6919999122619629 73 3.4448795318603516
		 74 3.7762396335601807 75 2.5202217102050781 76 0.069664239883422852 77 -3.2554759979248047
		 78 -7.1311917304992676 79 -11.180834770202637 80 -14.943625450134277;
createNode animCurveTA -n "Character1_LeftShoulder_rotateY";
	rename -uid "38AE70A1-4ACB-EBB2-7821-02964A93E557";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 12.374356269836426 1 12.80128288269043
		 2 13.056331634521484 3 11.604254722595215 4 7.897634506225585 5 2.9856665134429932
		 6 -2.1224470138549805 7 -6.429560661315918 8 -8.9826145172119141 9 -9.7938442230224609
		 10 -9.6909456253051758 11 -7.3374300003051758 12 -2.3280849456787109 13 3.6090981960296635
		 14 8.6608476638793945 15 10.966913223266602 16 9.7803764343261719 17 6.5063223838806152
		 18 2.5097455978393555 19 -0.86059528589248657 20 -2.2773773670196533 21 -0.85496199131011963
		 22 2.526573657989502 23 6.5314435958862305 24 9.802638053894043 25 10.790642738342285
		 26 7.5376982688903817 27 1.1905381679534912 28 -5.1951384544372559 29 -8.6197490692138672
		 30 -7.9673409461975098 31 -5.0202913284301758 32 -1.1843115091323853 33 2.1366677284240723
		 34 3.5282554626464844 35 2.1675345897674561 36 -1.0725129842758179 37 -5.0142126083374023
		 38 -8.3269872665405273 39 -9.7161445617675781 40 -8.3312873840332031 41 -5.0271635055541992
		 42 -1.0920171737670898 43 2.1501407623291016 44 3.3497495651245117 45 1.9599955081939697
		 46 -1.0874422788619995 47 -4.9253153800964355 48 -8.6952857971191406 49 -11.558274269104004
		 50 -12.694085121154785 51 -11.093401908874512 52 -7.2365283966064444 53 -2.5564744472503662
		 54 1.4728035926818848 55 3.3497495651245117 56 2.1675345897674561 57 -1.0725129842758179
		 58 -5.0142126083374023 59 -8.3269872665405273 60 -9.7161445617675781 61 -8.807276725769043
		 62 -6.4562463760375977 63 -3.2285192012786865 64 0.2857973575592041 65 3.4660003185272217
		 66 3.6431732177734375 67 3.9164698123931889 68 4.2966842651367188 69 4.6596512794494629
		 70 4.9002804756164551 71 5.0987720489501953 72 5.443422794342041 73 6.2314004898071289
		 74 7.8178939819335938 75 10.465714454650879 76 13.988032341003418 77 18.03801155090332
		 78 22.269832611083984 79 26.375339508056641 80 30.113260269165036;
createNode animCurveTA -n "Character1_LeftShoulder_rotateZ";
	rename -uid "7F5AF159-4413-5876-7FD4-AFA3124629B7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -12.99474048614502 1 -17.706018447875977
		 2 -22.430515289306641 3 -24.679248809814453 4 -24.051185607910156 5 -22.418863296508789
		 6 -20.597814559936523 7 -19.087543487548828 8 -18.06995964050293 9 -17.190082550048828
		 10 -16.115819931030273 11 -13.737617492675781 12 -9.8010311126708984 13 -5.5908589363098145
		 14 -2.1596195697784424 15 -0.42086935043334961 16 -0.58862537145614624 17 -1.8796176910400391
		 18 -3.6947872638702393 19 -5.355344295501709 20 -6.0872979164123535 21 -5.3448095321655273
		 22 -3.6632544994354248 23 -1.8324095010757449 24 -0.54671430587768555 25 -0.56017321348190308
		 26 -3.0537619590759277 27 -7.5214519500732413 28 -12.146481513977051 29 -14.805165290832521
		 30 -14.478722572326662 31 -12.435240745544434 32 -9.7458114624023437 33 -7.3845391273498526
		 34 -6.2024879455566406 35 -6.5329265594482422 36 -7.8948855400085449 37 -9.8238039016723633
		 38 -11.60837459564209 39 -12.401042938232422 40 -11.617245674133301 41 -9.8501005172729492
		 42 -7.9338150024414071 43 -6.5671772956848145 44 -6.3391399383544922 45 -7.5518107414245614
		 46 -9.8040122985839844 47 -12.612865447998047 48 -15.439430236816406 49 -17.653919219970703
		 50 -18.550897598266602 51 -17.284326553344727 52 -14.318402290344238 53 -10.850069046020508
		 54 -7.8998799324035645 55 -6.3391399383544922 56 -6.5329265594482422 57 -7.8948855400085449
		 58 -9.8238039016723633 59 -11.60837459564209 60 -12.401042938232422 61 -11.984109878540039
		 62 -10.94005298614502 63 -9.5783710479736328 64 -8.1423816680908203 65 -6.7795510292053223
		 66 -6.3340177536010742 67 -5.4599719047546387 68 -4.0454483032226563 69 -1.2566808462142944
		 70 3.0937488079071045 71 8.1394014358520508 72 12.995918273925781 73 16.762031555175781
		 74 18.513450622558594 75 17.868457794189453 76 15.395061492919922 77 11.564156532287598
		 78 6.871546745300293 79 1.8765074014663696 80 -2.7613897323608398;
createNode animCurveTU -n "Character1_LeftShoulder_visibility";
	rename -uid "D190E282-4448-CAE1-3943-A6A3178C21E6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftArm_translateX";
	rename -uid "AD61A44F-450B-8E69-8458-E492D79EA635";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.39077091217041;
createNode animCurveTL -n "Character1_LeftArm_translateY";
	rename -uid "45713B91-4ED0-4691-236E-88A8A96F6C76";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.8805270195007324;
createNode animCurveTL -n "Character1_LeftArm_translateZ";
	rename -uid "F7A3F9EC-4973-4830-A5B2-DB8A29E9260A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8116312026977539;
createNode animCurveTU -n "Character1_LeftArm_scaleX";
	rename -uid "13E52934-430F-33A4-51CC-4B888E246998";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999964237213135;
createNode animCurveTU -n "Character1_LeftArm_scaleY";
	rename -uid "31800941-4D6A-AAF7-A73A-C88E4AD18AC4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleZ";
	rename -uid "1CFC1B41-435F-1E8C-3C47-CE99C51D2E4C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_LeftArm_rotateX";
	rename -uid "5CAD5CA1-41EF-253D-D083-B1962B538480";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 33.673992156982422 1 39.6962890625 2 46.493934631347656
		 3 52.761882781982422 4 57.467323303222656 5 60.349868774414063 6 61.21489334106446
		 7 60.596313476562507 8 59.369342803955085 9 57.885246276855469 10 55.980194091796875
		 11 52.639938354492188 12 47.523502349853516 13 41.878795623779297 14 37.124736785888672
		 15 34.826087951660156 16 36.015792846679688 17 39.549232482910156 18 43.774799346923828
		 19 47.159759521484375 20 48.279460906982422 21 46.199371337890625 22 41.840274810791016
		 23 36.607639312744141 24 32.024501800537109 25 29.781467437744141 26 31.57907867431641
		 27 36.162166595458984 28 40.895706176757812 29 43.347557067871094 30 42.605777740478516
		 31 40.076938629150391 32 36.934978485107422 33 34.425468444824219 34 33.862152099609375
		 35 36.295722961425781 36 40.798179626464844 37 45.771488189697266 38 49.707935333251953
		 39 51.210521697998047 40 49.347408294677734 41 45.042606353759766 42 39.734912872314453
		 43 34.994415283203125 44 32.519443511962891 45 32.859027862548828 46 34.884353637695313
		 47 37.8062744140625 48 40.852157592773438 49 43.282844543457031 50 44.397716522216797
		 51 43.339958190917969 52 40.526767730712891 53 37.18426513671875 54 34.611576080322266
		 55 34.166816711425781 56 36.926483154296875 57 41.910888671875 58 47.431663513183594
		 59 51.912193298339844 60 53.884838104248047 61 52.781303405761719 62 49.584796905517578
		 63 45.165061950683594 64 40.386497497558594 65 36.138511657714844 66 36.13580322265625
		 67 36.255104064941406 68 36.559127807617188 69 37.398155212402344 70 38.948322296142578
		 71 41.018795013427734 72 43.312862396240234 73 45.2603759765625 74 45.835906982421875
		 75 44.050975799560547 76 39.83294677734375 77 33.558750152587891 78 26.185890197753906
		 79 18.923549652099609 80 12.662160873413086;
createNode animCurveTA -n "Character1_LeftArm_rotateY";
	rename -uid "9F401C29-4F67-4705-E033-1E8EA5C18D1D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 56.054130554199219 1 52.343265533447266
		 2 47.644870758056641 3 41.332893371582031 4 32.492397308349609 5 21.61292839050293
		 6 10.348307609558105 7 0.54233235120773315 8 -6.0467796325683594 9 -9.3752298355102539
		 10 -10.787862777709961 11 -10.846465110778809 12 -10.248783111572266 13 -9.5543718338012695
		 14 -9.0643196105957031 15 -8.8816614151000977 16 -8.4892988204956055 17 -7.5193052291870126
		 18 -6.3057036399841309 19 -5.238311767578125 20 -4.698488712310791 21 -4.944035530090332
		 22 -5.7911548614501953 23 -6.8928165435791016 24 -7.8573918342590332 25 -8.3329229354858398
		 26 -7.9997878074645987 27 -7.2177534103393555 28 -6.6058416366577148 29 -6.521216869354248
		 30 -6.9623012542724609 31 -7.6890511512756339 32 -8.5299472808837891 33 -9.2403707504272461
		 34 -9.5049753189086914 35 -9.0509786605834961 36 -8.0327568054199219 37 -6.8040904998779297
		 38 -5.7288732528686523 39 -5.1288981437683105 40 -5.2107348442077637 41 -5.8043665885925293
		 42 -6.5987944602966309 43 -7.2298641204833984 44 -7.3565754890441895 45 -6.9165349006652832
		 46 -6.1812496185302734 47 -5.3554849624633789 48 -4.6166243553161621 49 -4.0926094055175781
		 50 -3.8627979755401611 51 -4.0127840042114258 52 -4.5008726119995117 53 -5.1731886863708496
		 54 -5.7868037223815918 55 -6.0260915756225586 56 -5.6282753944396973 57 -4.7504434585571289
		 58 -3.7355608940124507 59 -2.9291763305664062 60 -2.5994095802307129 61 -3.1800105571746826
		 62 -4.6573748588562012 63 -6.5712728500366211 64 -8.43206787109375 65 -9.7390575408935547
		 66 -9.5174121856689453 67 -8.2383832931518555 68 -5.5910019874572754 69 -0.5809856653213501
		 70 6.8792414665222168 71 15.732406616210939 72 24.939975738525391 73 33.505683898925781
		 74 40.466587066650391 75 45.672248840332031 76 49.559223175048828 77 51.993072509765625
		 78 52.972965240478516 79 52.803108215332031 80 52.099552154541016;
createNode animCurveTA -n "Character1_LeftArm_rotateZ";
	rename -uid "8515BAE3-427E-EDE5-CF9E-238F5529379A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -44.005985260009766 1 -32.585842132568359
		 2 -21.771579742431641 3 -11.199601173400879 4 -0.4918195903301239 5 9.3259801864624023
		 6 17.256387710571289 7 23.010105133056641 8 26.352804183959961 9 27.758403778076172
		 10 28.215620040893555 11 27.189924240112305 12 24.688558578491211 13 21.793357849121094
		 14 19.411615371704102 15 18.242387771606445 16 18.288009643554688 17 19.017110824584961
		 18 20.22150993347168 19 21.428892135620117 20 21.908557891845703 21 21.15089225769043
		 22 19.738193511962891 23 18.462484359741211 24 17.830926895141602 25 18.063907623291016
		 26 19.576498031616211 27 22.263933181762695 28 25.228704452514648 29 27.029275894165039
		 30 26.921773910522461 31 25.797409057617188 32 24.40705680847168 33 23.311714172363281
		 34 22.872076034545898 35 23.213592529296875 36 24.177982330322266 37 25.584897994995117
		 38 26.931095123291016 39 27.444034576416016 40 26.586080551147461 41 24.936685562133789
		 42 23.290138244628906 43 22.1561279296875 44 21.746376037597656 45 22.091579437255859
		 46 23.012632369995117 47 24.331790924072266 48 25.790012359619141 49 27.009729385375977
		 50 27.524942398071289 51 26.785829544067383 52 25.107650756835938 53 23.261138916015625
		 54 21.820993423461914 55 21.160129547119141 56 21.425310134887695 57 22.44444465637207
		 58 23.990940093994141 59 25.534795761108398 60 26.262214660644531 61 25.874443054199219
		 62 24.903909683227539 63 23.740505218505859 64 22.685356140136719 65 21.885038375854492
		 66 21.761837005615234 67 21.492137908935547 68 21.028619766235352 69 20.545513153076172
		 70 20.133377075195312 71 19.603635787963867 72 18.674150466918945 73 16.8367919921875
		 74 13.215600967407227 75 6.7429862022399902 76 -2.7634682655334473 77 -14.749160766601561
		 78 -28.116191864013672 79 -41.556797027587891 80 -54.110607147216797;
createNode animCurveTU -n "Character1_LeftArm_visibility";
	rename -uid "15FF20E0-4E2C-6DE6-6056-96833FA15469";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftForeArm_translateX";
	rename -uid "AE943B81-4A3B-0552-5C44-30985B94D8BF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.9387187957763672;
createNode animCurveTL -n "Character1_LeftForeArm_translateY";
	rename -uid "F2B35047-4B0E-1D65-5A76-CC97BC30B861";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -14.273021697998047;
createNode animCurveTL -n "Character1_LeftForeArm_translateZ";
	rename -uid "0BA36BDE-47B4-1135-A037-04B4A6951DB0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.7470760345458984;
createNode animCurveTU -n "Character1_LeftForeArm_scaleX";
	rename -uid "F35D697C-4B8F-C2C5-054A-2FBF8A111A93";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftForeArm_scaleY";
	rename -uid "85625255-4F22-D63D-395F-CA9DAA920275";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftForeArm_scaleZ";
	rename -uid "84C1DD68-4AEB-520A-C0B7-A882689F450D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftForeArm_rotateX";
	rename -uid "7A0D8AAB-4BF0-83E2-1A3E-9F96954CB5C3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -15.058594703674316 1 -15.361235618591309
		 2 -12.539124488830566 3 -7.2926926612854004 4 -2.163557767868042 5 1.9132381677627563
		 6 4.5001492500305176 7 5.9261527061462402 8 6.8625321388244629 9 7.6999087333679208
		 10 8.3808145523071289 11 8.9667844772338867 12 9.4914417266845703 13 9.9698495864868164
		 14 10.414464950561523 15 10.836283683776855 16 10.637591361999512 17 9.5468606948852539
		 18 8.0922842025756836 19 6.8517618179321289 20 6.4502754211425781 21 7.3057756423950195
		 22 8.9661798477172852 23 10.768153190612793 24 12.084048271179199 25 12.358701705932617
		 26 11.928021430969238 27 11.465832710266113 28 10.983951568603516 29 10.49126148223877
		 30 9.9858837127685547 31 9.468231201171875 32 8.9471597671508789 33 8.4293212890625
		 34 7.9188051223754883 35 6.8074769973754883 36 4.7927360534667969 37 2.3742568492889404
		 38 0.14210401475429535 39 -1.2463023662567139 40 -1.3304997682571411 41 -0.53697323799133301
		 42 0.49167022109031683 43 1.1877832412719727 44 1.073973536491394 45 0.43917036056518555
		 46 -0.13913065195083618 47 -0.65654003620147705 48 -1.108875036239624 49 -1.4915138483047485
		 50 -1.7953826189041138 51 -2.0240235328674316 52 -2.1913526058197021 53 -2.3105506896972656
		 54 -2.3949534893035889 55 -2.4587118625640869 56 -3.0707700252532959 57 -4.5114684104919434
		 58 -6.3103265762329102 59 -7.8890109062194824 60 -8.570002555847168 61 -8.0668745040893555
		 62 -6.8238677978515625 63 -5.2415828704833984 64 -3.6674048900604248 65 -2.3714084625244141
		 66 -2.3714084625244141 67 -2.3714084625244141 68 -2.3714084625244141 69 -2.3193182945251465
		 70 -3.4395759105682373 71 -7.7239995002746591 72 -15.772686004638674 73 -25.308414459228516
		 74 -31.532171249389648 75 -32.435695648193359 76 -30.020109176635746 77 -25.731485366821289
		 78 -21.021718978881836 79 -17.013038635253906 80 -14.291915893554688;
createNode animCurveTA -n "Character1_LeftForeArm_rotateY";
	rename -uid "A94612D6-47A3-068E-40AC-44A311E5B641";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -19.900390625 1 -24.793659210205078 2 -30.153493881225589
		 3 -32.047534942626953 4 -29.41731071472168 5 -24.39647102355957 6 -18.481113433837891
		 7 -13.112333297729492 8 -9.5695390701293945 9 -7.7914681434631348 10 -6.8281240463256836
		 11 -6.4576735496520996 12 -6.4339332580566406 13 -6.5452046394348145 14 -6.5745072364807129
		 15 -6.3005123138427734 16 -4.9241132736206055 17 -2.417407751083374 18 0.31719940900802612
		 19 2.444636344909668 20 3.1944797039031982 21 2.0829231739044189 22 -0.35411697626113892
		 23 -3.332510232925415 24 -6.0141825675964355 25 -7.5176477432250977 26 -7.983020305633544
		 27 -8.1567602157592773 28 -8.0556097030639648 29 -7.6971449851989746 30 -7.1464724540710449
		 31 -6.4498581886291504 32 -5.5978484153747559 33 -4.5819730758666992 34 -3.3946506977081299
		 35 -1.2219316959381104 36 2.1496243476867676 37 5.8239078521728516 38 8.922093391418457
		 39 10.629105567932129 40 10.397103309631348 41 8.7374420166015625 42 6.4566526412963867
		 43 4.4417924880981445 44 3.6640951633453369 45 3.9041793346405029 46 4.2466859817504883
		 47 4.6543879508972168 48 5.0912466049194336 49 5.5224194526672363 50 5.9094443321228027
		 51 6.2569441795349121 52 6.6075682640075684 53 6.9916162490844727 54 7.4400005340576172
		 55 7.984025001525878 56 9.5227756500244141 57 12.27294921875 58 15.274931907653809
		 59 17.653980255126953 60 18.618770599365234 61 17.885675430297852 62 15.981309890747069
		 63 13.3477783203125 64 10.465831756591797 65 7.8712186813354492 66 7.8712186813354492
		 67 7.8712186813354492 68 7.8712186813354492 69 4.2839703559875488 70 -4.6755051612854004
		 71 -15.77633476257324 72 -25.355300903320312 73 -30.852012634277347 74 -32.476818084716797
		 75 -32.234188079833984 76 -31.128049850463864 77 -29.055400848388672 78 -25.977237701416016
		 79 -22.326444625854492 80 -18.975210189819336;
createNode animCurveTA -n "Character1_LeftForeArm_rotateZ";
	rename -uid "020C9AC3-4D12-35F9-994F-6BAEA2966180";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -14.527209281921387 1 -27.936700820922852
		 2 -42.354843139648438 3 -55.035209655761719 4 -64.696487426757813 5 -72.54217529296875
		 6 -78.536697387695313 7 -83.045455932617188 8 -86.3753662109375 9 -88.753387451171875
		 10 -90.411041259765625 11 -91.552131652832031 12 -92.376548767089844 13 -93.038520812988281
		 14 -93.684486389160156 15 -94.454391479492187 16 -94.947860717773438 17 -94.914421081542969
		 18 -94.688102722167969 19 -94.511833190917969 20 -94.546157836914063 21 -94.904922485351563
		 22 -95.485343933105469 23 -96.081092834472656 24 -96.372879028320313 25 -95.966506958007813
		 26 -95.124534606933594 27 -94.375358581542969 28 -93.727005004882813 29 -93.187881469726563
		 30 -92.7283935546875 31 -92.331939697265625 32 -92.026687622070313 33 -91.841041564941406
		 34 -91.803627014160156 35 -91.521461486816406 36 -90.817062377929688 37 -89.994461059570313
		 38 -89.241958618164063 39 -88.610382080078125 40 -88.065788269042969 41 -87.526832580566406
		 42 -86.935401916503906 43 -86.186325073242187 44 -85.122993469238281 45 -83.945060729980469
		 46 -82.953140258789063 47 -82.138908386230469 48 -81.492500305175781 49 -81.002693176269531
		 50 -80.660873413085938 51 -80.475166320800781 52 -80.473175048828125 53 -80.666893005371094
		 54 -81.06842041015625 55 -81.690055847167969 56 -82.26458740234375 57 -82.623153686523438
		 58 -82.889312744140625 59 -83.093597412109375 60 -83.178436279296875 61 -83.031944274902344
		 62 -82.691825866699219 63 -82.307823181152344 64 -81.986968994140625 65 -81.774711608886719
		 66 -81.774711608886719 67 -81.774711608886719 68 -81.774711608886719 69 -76.999954223632812
		 70 -64.808303833007812 71 -47.631294250488281 72 -27.414831161499023 73 -7.9039416313171387
		 74 4.6278133392333984 75 8.1063556671142578 76 6.1034951210021973 77 0.69859695434570313
		 78 -6.0165920257568359 79 -12.219120025634766 80 -16.486003875732422;
createNode animCurveTU -n "Character1_LeftForeArm_visibility";
	rename -uid "99E9A25B-4692-1EC3-1D2A-44AB1A29BB45";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHand_translateX";
	rename -uid "A3D9C5C5-4E4D-2BBF-4242-79BA7347EBD3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.95125424861907959;
createNode animCurveTL -n "Character1_LeftHand_translateY";
	rename -uid "8052CD09-4413-4043-EE7B-71A1FB901411";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.299518585205078;
createNode animCurveTL -n "Character1_LeftHand_translateZ";
	rename -uid "1B8C002F-4190-25EB-5C8D-4BB731F137F5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 12.872089385986328;
createNode animCurveTU -n "Character1_LeftHand_scaleX";
	rename -uid "49D82D94-464F-5ACA-98E5-5F8237C306F9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHand_scaleY";
	rename -uid "7EC21FE4-4CE7-F977-A708-EEA3F9E8A149";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHand_scaleZ";
	rename -uid "B43363EF-4471-2DDA-52A8-31BB43083F0E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHand_rotateX";
	rename -uid "F57084A2-43D3-7D98-762D-25AA1BBE5DEF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 3.5318756808067064e-009 1 7.4616613388061532
		 2 14.435797691345213 3 20.560413360595703 4 26.109197616577148 5 31.141386032104492
		 6 35.343223571777344 7 38.706352233886719 8 41.438938140869141 9 44.131698608398438
		 10 47.095882415771484 11 50.143966674804688 12 52.911880493164062 13 54.932258605957031
		 14 55.749744415283203 15 54.914836883544922 16 52.115764617919922 17 48.057285308837891
		 18 43.825511932373047 19 40.508865356445313 20 39.170162200927734 21 40.476459503173828
		 22 43.727939605712891 23 47.910831451416016 24 51.986068725585938 25 55.096397399902344
		 26 57.106224060058594 27 58.554985046386726 28 59.67559814453125 29 60.702495574951165
		 30 61.806987762451172 31 62.824153900146484 32 63.481151580810547 33 63.501121520996087
		 34 62.603179931640625 35 59.987895965576165 36 55.90521240234375 37 51.622074127197266
		 38 48.246803283691406 39 46.87994384765625 40 48.273838043212891 41 51.703056335449219
		 42 56.025943756103516 43 60.094139099121094 44 62.788211822509759 45 64.104881286621094
		 46 64.862602233886719 47 65.205703735351563 48 65.276840209960937 49 65.217063903808594
		 50 65.166191101074219 51 65.206932067871094 52 65.194305419921875 53 64.924636840820312
		 54 64.191986083984375 55 62.788211822509759 56 59.987895965576165 57 55.90521240234375
		 58 51.622074127197266 59 48.246803283691406 60 46.87994384765625 61 48.032012939453125
		 62 50.976966857910156 63 54.939266204833984 64 59.133876800537109 65 62.788211822509759
		 66 62.788211822509759 67 62.788211822509759 68 62.788211822509759 69 63.26649475097657
		 70 62.763954162597656 71 58.473850250244141 72 49.247215270996094 73 36.921051025390625
		 74 26.425136566162109 75 19.874673843383789 76 15.225837707519531 77 11.337363243103027
		 78 7.3891682624816895 79 3.0672023296356201 80 -1.5961844921112061;
createNode animCurveTA -n "Character1_LeftHand_rotateY";
	rename -uid "BF1EE7BA-454B-8FF8-5EA5-B1B4B1F104F1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 6.0393453168217093e-005 1 -0.72521251440048218
		 2 -2.3841514587402344 3 -4.7406802177429199 4 -7.8366785049438477 5 -11.775582313537598
		 6 -16.047395706176758 7 -19.968015670776367 8 -22.79395866394043 9 -24.393436431884766
		 10 -25.20750617980957 11 -25.425859451293945 12 -25.269660949707031 13 -24.947469711303711
		 14 -24.68571662902832 15 -24.694643020629883 16 -24.906267166137695 17 -25.010889053344727
		 18 -24.911958694458008 19 -24.690767288208008 20 -24.565544128417969 21 -24.688907623291016
		 22 -24.910152435302734 23 -25.015359878540039 24 -24.916322708129883 25 -24.711330413818359
		 26 -24.479513168334961 27 -24.235317230224609 28 -24.060070037841797 29 -24.023635864257813
		 30 -24.132169723510742 31 -24.336824417114258 32 -24.619524002075195 33 -24.957355499267578
		 34 -25.314098358154297 35 -25.725286483764648 36 -26.098033905029297 37 -26.2799072265625
		 38 -26.27900505065918 39 -26.242475509643555 40 -26.278583526611328 41 -26.275506973266602
		 42 -26.085607528686523 43 -25.70947265625 44 -25.327136993408203 45 -25.033403396606445
		 46 -24.764190673828125 47 -24.539402008056641 48 -24.370918273925781 49 -24.266056060791016
		 50 -24.230047225952148 51 -24.287317276000977 52 -24.447792053222656 53 -24.69268798828125
		 54 -24.997568130493164 55 -25.327136993408203 56 -25.725286483764648 57 -26.098033905029297
		 58 -26.2799072265625 59 -26.27900505065918 60 -26.242475509643555 61 -26.267450332641602
		 62 -26.265436172485352 63 -26.11522102355957 64 -25.774452209472656 65 -25.327136993408203
		 66 -25.327136993408203 67 -25.327136993408203 68 -25.327136993408203 69 -21.425321578979492
		 70 -11.500767707824707 71 1.1515532732009888 72 12.38520622253418 73 18.583742141723633
		 74 18.994834899902344 75 15.891846656799316 76 11.530947685241699 77 7.1821079254150391
		 78 3.7081880569458008 79 1.6303865909576416 80 1.0345077514648437;
createNode animCurveTA -n "Character1_LeftHand_rotateZ";
	rename -uid "FBD6C61C-4D42-B1BD-842A-E4A5C81B8225";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -0.0001607897283975035 1 3.6428306102752686
		 2 7.3680319786071786 3 10.72831916809082 4 13.938928604125977 5 17.099218368530273
		 6 19.751243591308594 7 21.562490463256836 8 22.280727386474609 9 21.913812637329102
		 10 20.848688125610352 11 19.477128982543945 12 18.17559814453125 13 17.231502532958984
		 14 16.893733978271484 15 17.401960372924805 16 18.91801643371582 17 21.088306427001953
		 18 23.356334686279297 19 25.135217666625977 20 25.851888656616211 21 25.154390335083008
		 22 23.413730621337891 23 21.173385620117188 24 18.992015838623047 25 17.326499938964844
		 26 16.181884765625 27 15.283092498779295 28 14.603075981140137 29 14.113723754882813
		 30 13.750635147094727 31 13.516371726989746 32 13.485261917114258 33 13.738847732543945
		 34 14.368096351623535 35 15.777674674987793 36 17.928745269775391 37 20.213832855224609
		 38 22.031597137451172 39 22.770227432250977 40 22.015619277954102 41 20.166572570800781
		 42 17.859794616699219 43 15.718624114990234 44 14.299067497253418 45 13.54715633392334
		 46 13.040489196777344 47 12.726330757141113 48 12.55613899230957 49 12.485025405883789
		 50 12.470552444458008 51 12.506852149963379 52 12.65054988861084 53 12.956252098083496
		 54 13.483785629272461 55 14.299067497253418 56 15.777674674987793 57 17.928745269775391
		 58 20.213832855224609 59 22.031597137451172 60 22.770227432250977 61 22.13688850402832
		 62 20.527650833129883 63 18.391231536865234 64 16.177202224731445 65 14.299067497253418
		 66 14.299067497253418 67 14.299067497253418 68 14.299067497253418 69 12.093504905700684
		 70 6.592094898223877 71 -1.4613820314407349 72 -12.04572582244873 73 -23.436399459838867
		 74 -30.921113967895508 75 -32.139228820800781 76 -28.998514175415039 77 -22.723760604858398
		 78 -14.594803810119629 79 -5.9669346809387207 80 1.6997882127761841;
createNode animCurveTU -n "Character1_LeftHand_visibility";
	rename -uid "2911AB28-423E-D2DA-0A28-9EB6734A6276";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateX";
	rename -uid "D1348921-40E2-8D58-9C16-A78EC8DAEDEB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.1041760444641113;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateY";
	rename -uid "17629B6C-4635-BA1B-8647-67A2203A6F8B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.7491040229797363;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateZ";
	rename -uid "8D31D18E-45CE-910D-9E98-779BFBB6CF46";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.7276492118835449;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleX";
	rename -uid "D25A351A-4484-3B0B-802C-CAB7D2D040B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleY";
	rename -uid "D08536C8-47E4-4EC9-A1FB-81B1505DBFB8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleZ";
	rename -uid "7A84928C-4589-53F6-E0DA-179C4F90AAAD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateX";
	rename -uid "621E98B3-4658-345C-192B-A78B993A73B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 7.3663825988769522 1 13.63456916809082
		 2 17.756494522094727 3 18.860912322998047 4 18.860912322998047 5 18.860912322998047
		 6 18.860912322998047 7 18.161003112792969 8 15.315528869628904 9 9.3845548629760742
		 10 1.3374292850494385 11 -5.4252829551696777 12 -7.6413888931274414 13 -6.8743243217468262
		 14 -6.3166937828063965 15 -6.2445883750915527 16 -6.9763708114624023 17 -8.3557071685791016
		 18 -9.9267511367797852 19 -11.171188354492188 20 -11.515724182128906 21 -10.596920967102051
		 22 -8.828953742980957 23 -6.7791976928710937 24 -4.9363036155700684 25 -3.7082307338714604
		 26 -3.2430968284606934 27 -3.2540044784545898 28 -3.4736461639404297 29 -3.6367437839508052
		 30 -3.6042969226837158 31 -3.5044233798980713 32 -3.468585729598999 33 -3.6280884742736816
		 34 -4.1184749603271484 35 -5.1459455490112305 36 -6.6581563949584961 37 -8.3930950164794922
		 38 -10.046561241149902 39 -11.265365600585937 40 -11.924654006958008 41 -12.227559089660645
		 42 -12.29422664642334 43 -12.251105308532715 44 -12.225973129272461 45 -12.176419258117676
		 46 -12.017480850219727 47 -11.812907218933105 48 -11.625633239746094 49 -11.517573356628418
		 50 -11.550319671630859 51 -11.986908912658691 52 -12.804047584533691 53 -13.622048377990723
		 54 -14.035641670227051 55 -13.63221549987793 56 -11.946249008178711 57 -9.3602762222290039
		 58 -6.6723771095275879 59 -4.5193719863891602 60 -3.4166984558105469 61 -3.502840518951416
		 62 -4.3799691200256348 63 -5.7466487884521484 64 -7.2838969230651855 65 -8.6268606185913086
		 66 -8.6268606185913086 67 -8.6268606185913086 68 -8.6268606185913086 69 -8.3975353240966797
		 70 -7.7494678497314453 71 -6.7433738708496094 72 -5.442441463470459 73 -3.9135119915008545
		 74 -2.2270717620849609 75 -0.45572885870933533 76 1.3285847902297974 77 3.0581409931182861
		 78 4.6727414131164551 79 6.1220617294311523 80 7.3663825988769522;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateY";
	rename -uid "9E9E46FA-4FFE-3465-9874-F2A6C67EA108";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 11.432723045349121 1 5.8634839057922363
		 2 -1.8554450273513794 3 -5.9013795852661133 4 -5.9013795852661133 5 -5.9013795852661133
		 6 -5.9013795852661133 7 -2.9171111583709717 8 3.8671860694885258 9 10.784833908081055
		 10 14.867915153503416 11 16.229709625244141 12 17.131143569946289 13 18.467082977294922
		 14 19.460758209228516 15 19.59065055847168 16 18.28730583190918 17 15.918596267700195
		 18 13.355381965637207 19 11.422484397888184 20 10.902070045471191 21 12.303999900817871
		 22 15.131671905517576 23 18.635244369506836 24 22.004386901855469 25 24.371219635009766
		 26 25.294076919555664 27 25.272266387939453 28 24.834808349609375 29 24.51209831237793
		 30 24.576154708862305 31 24.773771286010742 32 24.8448486328125 33 24.529176712036133
		 34 23.569423675537109 35 21.610248565673828 36 18.850015640258789 37 15.855954170227053
		 38 13.165607452392578 39 11.279604911804199 40 10.292507171630859 41 9.8466062545776367
		 42 9.7491016387939453 43 9.8121423721313477 44 9.8489284515380859 45 9.9215555191040039
		 46 10.155352592468262 47 10.458209991455078 48 10.737374305725098 49 10.899292945861816
		 50 10.850159645080566 51 10.200473785400391 52 9.0110006332397461 53 7.8541350364685059
		 54 7.2818284034729004 55 7.8399658203125 56 10.260560035705566 57 14.26347827911377
		 58 18.824735641479492 59 22.796743392944336 60 24.947912216186523 61 24.776908874511719
		 62 23.064218521118164 63 20.496522903442383 64 17.749347686767578 65 15.466133117675781
		 66 15.466133117675781 67 15.466133117675781 68 15.466133117675781 69 15.487862586975098
		 70 15.53804302215576 71 15.583052635192871 72 15.581590652465819 73 15.49263381958008
		 74 15.282206535339357 75 14.92873477935791 76 14.426750183105469 77 13.788632392883301
		 78 13.044408798217773 79 12.239770889282227 80 11.432723045349121;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateZ";
	rename -uid "FDB6398F-4323-6147-5F00-E2B8E1053217";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 36.344264984130859 1 47.958274841308594
		 2 58.204322814941413 3 62.457702636718743 4 62.457702636718743 5 62.457702636718743
		 6 62.457702636718743 7 59.405693054199219 8 51.249290466308594 9 39.309268951416016
		 10 25.906469345092773 11 14.68806838989258 12 9.5232248306274414 13 8.7024106979370117
		 14 8.1039333343505859 15 8.0264310836791992 16 8.8117666244506836 17 10.285243034362793
		 18 11.954043388366699 19 13.269736289978027 20 13.633132934570312 21 12.663214683532715
		 22 10.788928985595703 23 8.6004247665405273 24 6.6152896881103516 25 5.2811789512634277
		 26 4.7731614112854004 27 4.785092830657959 28 5.0251650810241699 29 5.2032032012939453
		 30 5.1677994728088379 31 5.0587763786315918 32 5.0196380615234375 33 5.1937599182128906
		 34 5.7279605865478516 35 6.8420734405517578 36 8.4705924987792969 37 10.325068473815918
		 38 12.080937385559082 39 13.369104385375977 40 14.063990592956543 41 14.382830619812012
		 42 14.45297050476074 43 14.407605171203612 44 14.381161689758301 45 14.329017639160156
		 46 14.161728858947754 47 13.946300506591797 48 13.748983383178711 49 13.635082244873047
		 50 13.669602394104004 51 14.129542350769043 52 14.988945960998535 53 15.847496032714842
		 54 16.280961990356445 55 15.858156204223633 56 14.08673095703125 57 11.353385925292969
		 58 8.4858493804931641 59 6.1634635925292969 60 4.9629554748535156 61 5.0570487976074219
		 62 6.0121469497680664 63 7.4904723167419434 64 9.141021728515625 65 10.573945999145508
		 66 10.573945999145508 67 10.573945999145508 68 10.573945999145508 69 10.926711082458496
		 70 11.924154281616211 71 13.47481632232666 72 15.485548973083496 73 17.860185623168945
		 74 20.499284744262695 75 23.301200866699219 76 26.164304733276367 77 28.98980712890625
		 78 31.684398651123047 79 34.162017822265625 80 36.344264984130859;
createNode animCurveTU -n "Character1_LeftHandThumb1_visibility";
	rename -uid "19922EDC-4534-533E-4706-278E6E076844";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateX";
	rename -uid "C736CC6B-4CE3-5E90-23BA-E09B77AD931B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.3594832420349121;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateY";
	rename -uid "14828685-4533-510D-671E-979F7769BAD0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.8193249702453613;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateZ";
	rename -uid "C77EFB2F-4273-F802-0092-828C6999A95E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3286550045013428;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleX";
	rename -uid "518835FB-41A0-7D22-6EB2-DA8D0C234DC8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleY";
	rename -uid "2776DD9D-4207-0A75-194E-258EDAE45248";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleZ";
	rename -uid "EB2FFF0E-45CD-3D37-EEBC-AFAD71AA1B72";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateX";
	rename -uid "271CE028-4A40-49C5-FE7D-2781820E19C4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 22.477453231811523 1 34.993122100830078
		 2 47.982452392578125 3 53.687442779541016 4 53.687442779541016 5 53.687442779541016
		 6 53.687442779541016 7 52.536033630371094 8 49.541660308837891 9 45.189159393310547
		 10 39.868358612060547 11 35.53973388671875 12 35.209060668945313 13 37.646888732910156
		 14 39.444889068603516 15 39.678947448730469 16 37.3201904296875 17 32.976360321044922
		 18 28.195283889770508 19 24.536075592041016 20 23.543087005615234 21 26.210559844970703
		 22 31.517322540283203 23 37.95208740234375 24 43.986854553222656 25 48.133014678955078
		 26 49.728305816650391 27 49.690742492675781 28 48.935901641845703 29 48.377323150634766
		 30 48.488315582275391 31 48.830368041992187 32 48.953258514404297 33 48.40692138671875
		 34 46.737224578857422 35 43.288848876953125 36 38.341335296630859 37 32.860504150390625
		 38 27.838045120239258 39 24.263778686523438 40 22.375827789306641 41 21.519124984741211
		 42 21.331472396850586 43 21.452812194824219 44 21.523593902587891 45 21.663291931152344
		 46 22.112569808959961 47 22.693578720092773 48 23.228153228759766 49 23.53778076171875
		 50 23.443859100341797 51 22.199203491210937 52 19.907228469848633 53 17.661697387695313
		 54 16.544820785522461 55 17.634092330932617 56 22.31452751159668 57 29.898534774780273
		 58 38.295547485351563 59 45.383586883544922 60 49.131320953369141 61 48.835788726806641
		 62 45.853115081787109 63 41.304981231689453 64 36.340065002441406 65 32.138408660888672
		 66 32.138408660888672 67 32.138408660888672 68 32.138408660888672 69 31.910181045532227
		 70 31.291675567626953 71 30.402326583862308 72 29.363456726074219 73 28.275949478149414
		 74 27.210628509521484 75 26.209028244018555 76 25.290073394775391 77 24.458581924438477
		 78 23.713096618652344 79 23.051961898803711 80 22.477453231811523;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateY";
	rename -uid "7AF5574C-4519-EBE5-FD46-A2ABD37FC97A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 80 ".ktv[0:79]"  0 -11.664257049560547 1 -15.687603950500488
		 2 -15.904830932617188 3 -14.673751831054688 4 -14.673751831054688 5 -14.673751831054688
		 6 -14.673751831054688 7 -17.241289138793945 8 -23.771492004394531 9 -32.417701721191406
		 10 -41.098850250244141 11 -47.651416778564453 12 -50.196441650390625 13 -50.139583587646484
		 14 -50.082267761230469 15 -50.073844909667969 16 -50.148597717285156 17 -50.227466583251953
		 18 -50.226253509521484 19 -50.163017272949219 20 -50.136531829833984 21 -50.198661804199219
		 22 -50.236873626708984 23 -50.130775451660156 24 -49.879207611083984 25 -49.62078857421875
		 26 -49.502735137939453 27 -49.505634307861328 28 -49.562671661376953 29 -49.603382110595703
		 30 -49.595394134521484 31 -49.570461273193359 32 -49.561386108398438 33 -49.60125732421875
		 34 -49.715587615966797 35 -49.915847778320312 36 -50.118995666503906 37 -50.228527069091797
		 38 -50.222461700439453 39 -50.156154632568359 40 -50.100303649902344 41 -50.070209503173828
		 42 -50.063217163085938 44 -50.070369720458984 45 -50.075477600097656 46 -50.09136962890625
		 47 -50.110710144042969 48 -50.127296447753906 49 -50.136379241943359 50 -50.133663177490234
		 51 -50.094337463378906 52 -50.005531311035156 53 -49.897895812988281 54 -49.836746215820312
		 55 -49.896446228027344 56 -50.098247528076172 57 -50.237258911132813 58 -50.120414733886719
		 59 -49.799953460693359 60 -49.548126220703125 61 -49.570060729980469 62 -49.771533966064453
		 63 -50.009208679199219 64 -50.173049926757813 65 -50.233917236328125 66 -50.233917236328125
		 67 -50.233917236328125 68 -50.233917236328125 69 -49.783500671386719 70 -48.498199462890625
		 71 -46.466209411621094 72 -43.771377563476562 73 -40.502983093261719 74 -36.761642456054687
		 75 -32.661830902099609 76 -28.331933975219727 77 -23.912771224975586 78 -19.555294036865234
		 79 -15.417993545532227 80 -11.664257049560547;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateZ";
	rename -uid "81CC6F95-40D6-7C4A-EC86-75BB8BC78F21";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -2.6664080619812012 1 -13.958452224731445
		 2 -26.589992523193359 3 -32.373161315917969 4 -32.373161315917969 5 -32.373161315917969
		 6 -32.373161315917969 7 -32.670280456542969 8 -32.952987670898437 9 -32.217716217041016
		 10 -29.909673690795902 11 -26.973747253417969 12 -25.984371185302734 13 -26.861358642578125
		 14 -27.505699157714844 15 -27.589389801025391 16 -26.74403190612793 17 -25.178867340087891
		 18 -23.451597213745117 19 -22.132701873779297 20 -21.775951385498047 21 -22.735549926757812
		 22 -24.651815414428711 23 -26.97089958190918 24 -29.119991302490231 25 -30.570808410644535
		 26 -31.121725082397461 27 -31.108804702758789 28 -30.848636627197262 29 -30.655464172363281
		 30 -30.693891525268551 31 -30.812179565429688 32 -30.85462760925293 33 -30.665712356567383
		 34 -30.085271835327148 35 -28.873397827148437 36 -27.110511779785156 37 -25.137029647827148
		 38 -23.322624206542969 39 -22.034812927246094 40 -21.357458114624023 41 -21.050983428955078
		 42 -20.983938217163086 43 -21.027288436889648 44 -21.052581787109375 45 -21.102514266967773
		 46 -21.263216018676758 47 -21.471279144287109 48 -21.662940979003906 49 -21.774045944213867
		 50 -21.740337371826172 51 -21.294221878051758 52 -20.476137161254883 53 -19.679862976074219
		 54 -19.286083221435547 55 -19.670110702514648 56 -21.335506439208984 57 -24.066860198974609
		 58 -27.094093322753906 59 -29.611526489257813 60 -30.916086196899411 61 -30.814052581787109
		 62 -29.776157379150394 63 -28.169418334960938 64 -26.391647338867188 65 -24.876209259033203
		 66 -24.876209259033203 67 -24.876209259033203 68 -24.876209259033203 69 -24.405942916870117
		 70 -23.114261627197266 71 -21.210668563842773 72 -18.913776397705078 73 -16.421524047851563
		 74 -13.895572662353516 75 -11.457618713378906 76 -9.1929540634155273 77 -7.1570334434509277
		 78 -5.382326602935791 79 -3.8843045234680176 80 -2.6664080619812012;
createNode animCurveTU -n "Character1_LeftHandThumb2_visibility";
	rename -uid "38156401-4A39-C2C6-7A26-B9864C9B3566";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateX";
	rename -uid "709A2553-487F-78BB-4752-7EB29A446D3A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1733376979827881;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateY";
	rename -uid "D737C3C3-4930-7F33-D64C-14B27FF495F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.34823575615882874;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateZ";
	rename -uid "660A9AEE-42D1-CA7B-398A-509A180A5F37";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.468076229095459;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleX";
	rename -uid "EB10A4A3-4648-47F9-019D-37AE026A6E8B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleY";
	rename -uid "C6F0526A-431C-D7F3-44F4-0882CFCB21CC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleZ";
	rename -uid "60A44E73-4BA5-0F82-AEA8-7E8EAD14563D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateX";
	rename -uid "2CE7A13C-4A12-ED0F-A519-2799587E0581";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 3.0936419648242008e-007 10 3.6917546708536975e-007
		 11 0.15685170888900757 12 0.50718998908996582 13 0.88070392608642578 14 1.1399415731430054
		 15 1.1726822853088379 16 0.83212602138519287 17 0.14252406358718872 18 -0.71318894624710083
		 19 -1.4400703907012939 20 -1.6485781669616699 21 -1.0994564294815063 22 -0.10765534639358521
		 23 0.92567455768585205 24 1.7341705560684204 25 2.2010624408721924 26 2.361358642578125
		 27 2.3577089309692383 28 2.2830915451049805 29 2.2263123989105225 30 2.2376999855041504
		 31 2.272465705871582 32 2.2848343849182129 33 2.2293546199798584 34 2.0519611835479736
		 35 1.6484746932983398 36 0.98245507478713989 37 0.12300413846969604 38 -0.78133964538574219
		 39 -1.4967589378356934 40 -1.9000179767608645 41 -2.0889952182769775 42 -2.1308963298797607
		 43 -2.1037819385528564 44 -2.0879998207092285 45 -2.0569283962249756 46 -1.9576864242553711
		 47 -1.8308856487274168 48 -1.7157386541366577 49 -1.6497056484222412 50 -1.6696847677230835
		 51 -1.9386694431304932 52 -2.4549565315246582 53 -2.9880869388580322 54 -3.2636899948120117
		 55 -2.9948139190673828 56 -1.9134143590927124 57 -0.39641433954238892 58 0.97580975294113159
		 59 1.8995233774185183 60 2.3026425838470459 61 2.2730128765106201 62 1.9532704353332517
		 63 1.3937667608261108 64 0.68365085124969482 65 4.3877329858332819e-007 66 4.3874379684893944e-007;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateY";
	rename -uid "7FCE6BA7-4039-67DA-FE30-369B379120CE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -8.8328008018834225e-008 10 -1.1726259430133724e-007
		 11 0.5573582649230957 12 1.8585468530654907 13 3.3441550731658936 14 4.445953369140625
		 15 4.5897793769836426 16 3.1445236206054687 17 0.50583314895629883 18 -2.3675041198730469
		 19 -4.546994686126709 20 -5.1357173919677734 21 -3.5516371726989746 22 -0.37428861856460571
		 23 3.5308051109313965 24 7.2541065216064453 25 9.8514680862426758 26 10.860173225402832
		 27 10.836358070373535 28 10.358463287353516 29 10.005596160888672 30 10.075660705566406
		 31 10.291744232177734 32 10.369438171386719 33 10.024279594421387 34 8.9732494354248047
		 35 6.8201231956481934 36 3.7690761089324951 37 0.43583744764328003 38 -2.5809979438781738
		 39 -4.7085456848144531 40 -5.8263397216796875 41 -6.3322601318359375 42 -6.4429693222045898
		 43 -6.3713884353637695 44 -6.3296236991882324 45 -6.2471795082092285 46 -5.9818911552429199
		 47 -5.6384897232055664 48 -5.322202205657959 49 -5.1388607025146484 50 -5.194486141204834
		 51 -5.9307103157043457 52 -7.2819995880126953 53 -8.6005020141601562 54 -9.2543478012084961
		 55 -8.6166772842407227 56 -5.8625669479370117 57 -1.3473649024963379 58 3.7410371303558354
		 59 8.1253042221069336 60 10.482070922851562 61 10.295173645019531 62 8.4190139770507812
		 63 5.591547966003418 64 2.5466275215148926 65 -1.5465715819118486e-007 66 -1.5487056259644305e-007;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateZ";
	rename -uid "2D4C62C7-4E1A-6FFE-630F-18A024832C7E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 2.8223513481862028e-007 10 2.8117597139498685e-007
		 11 -0.39305272698402405 12 -1.3022232055664063 13 -2.327162504196167 14 -3.0793399810791016
		 15 -3.1770713329315186 16 -2.1901829242706299 17 -0.35681134462356567 18 1.6967514753341675
		 19 3.3032050132751465 20 3.7454304695129395 21 2.5637814998626709 22 0.26524621248245239
		 23 -2.4550361633300781 24 -4.971071720123291 25 -6.6962895393371582 26 -7.3618979454040518
		 27 -7.3462052345275879 28 -7.0310792922973633 29 -6.7981224060058594 30 -6.8443975448608398
		 31 -6.9870519638061523 32 -7.0383210182189941 33 -6.8104634284973145 34 -6.1150188446044922
		 35 -4.6807804107666016 36 -2.6180014610290527 37 -0.30754777789115906 38 1.8521071672439575
		 39 3.4241845607757568 40 4.269078254699707 41 4.6561660766601563 42 4.741276741027832
		 43 4.6862301826477051 44 4.6541409492492676 45 4.590857982635498 46 4.3877730369567871
		 47 4.1261100769042969 48 3.8862993717193599 49 3.7478015422821049 50 3.7897820472717281
		 51 4.3486876487731934 52 5.3911981582641602 53 6.4309334754943848 54 6.9554376602172852
		 55 6.4438357353210449 56 4.296696662902832 57 0.95996302366256725 58 -2.5988399982452393
		 59 -5.5518956184387207 60 -7.1126265525817871 61 -6.9893150329589844 62 -5.7471785545349121
		 63 -3.8550963401794438 64 -1.7785691022872925 65 2.6779576955959783e-007 66 2.6803340347214544e-007;
createNode animCurveTU -n "Character1_LeftHandThumb3_visibility";
	rename -uid "700173CF-40B1-43BD-AD4E-48B41DC77284";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateX";
	rename -uid "18837C2F-42C6-FBAB-8F85-99A5178CC90C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.4336724281311035;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateY";
	rename -uid "170754E5-4C14-7FDE-51D0-9DBA5E69C4A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.6582565307617187;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateZ";
	rename -uid "4C8F03AD-4DBF-16B9-8890-F8B635203473";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.338188648223877;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleX";
	rename -uid "C4E231D7-4EEF-4759-7BFE-ABA416379CAA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleY";
	rename -uid "39B14C49-4438-FF9A-78FF-C5BA1301A7D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleZ";
	rename -uid "441D504F-41E9-B737-C4E1-D0AB883294A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateX";
	rename -uid "0FAB119E-4D5A-458B-99A3-6E8F5B532AD4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -5.4912371635437012 1 -28.606420516967773
		 2 -64.074050903320312 3 -81.639427185058594 4 -81.639427185058594 5 -81.639427185058594
		 6 -81.639427185058594 7 -71.948883056640625 8 -48.174758911132813 9 -22.204639434814453
		 10 -1.4785029888153076 11 12.874552726745605 12 18.105751037597656 13 17.186985015869141
		 14 16.511102676391602 15 16.423173904418945 16 17.309915542602539 17 18.951313018798828
		 18 20.783584594726563 19 22.215099334716797 20 22.609243392944336 21 21.556221008300781
		 22 19.506858825683594 23 17.072185516357422 24 14.804102897644043 25 13.237160682678223
		 26 12.629779815673828 27 12.644119262695313 28 12.93186092376709 29 13.144341468811035
		 30 13.102149963378906 31 12.972034454345703 32 12.925253868103027 33 13.133090019226074
		 34 13.766267776489258 35 15.066768646240234 36 16.925821304321289 37 18.995330810546875
		 38 20.922046661376953 39 22.322916030883789 40 23.076131820678711 41 23.42143440246582
		 42 23.497385025024414 43 23.448263168334961 44 23.41963005065918 45 23.363166809082031
		 46 23.181997299194336 47 22.948637008666992 48 22.734821319580078 49 22.611356735229492
		 50 22.648778915405273 51 23.147134780883789 52 24.077682495117188 53 25.007669448852539
		 54 25.477785110473633 55 25.01922607421875 56 23.100763320922852 57 20.126703262329102
		 58 16.943035125732422 59 14.277715682983398 60 12.857436180114746 61 12.969969749450684
		 62 14.100478172302246 63 15.812410354614258 64 17.679044723510742 65 19.270042419433594
		 66 19.270042419433594 67 19.270042419433594 68 19.270042419433594 69 18.875711441040039
		 70 17.771852493286133 71 16.087347030639648 72 13.957456588745117 73 11.51581859588623
		 74 8.8879804611206055 75 6.1868143081665039 76 3.5101175308227539 77 0.94035542011260986
		 78 -1.4537674188613892 79 -3.614448070526123 80 -5.4912371635437012;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateY";
	rename -uid "952DE155-420E-98EB-7E50-1D83492DF30A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -2.1310844421386719 1 -25.227054595947266
		 2 -35.135936737060547 3 -33.114273071289063 4 -33.114273071289063 5 -33.114273071289063
		 6 -33.114273071289063 7 -33.719615936279297 8 -30.371517181396484 9 -17.856208801269531
		 10 -0.34553855657577515 11 13.804364204406738 12 18.330316543579102 13 16.741424560546875
		 14 15.568520545959474 15 15.415751457214354 16 16.954435348510742 17 19.784692764282227
		 18 22.899229049682617 19 25.285934448242188 20 25.934423446655273 21 24.193244934082031
		 22 20.734977722167969 23 16.542404174804688 24 12.599556922912598 25 9.878596305847168
		 26 8.8281965255737305 27 8.8529558181762695 28 9.3502092361450195 29 9.7178716659545898
		 30 9.6448354721069336 31 9.4196929931640625 32 9.3387813568115234 33 9.6983938217163086
		 34 10.795966148376465 35 13.056517601013184 36 16.288534164428711 37 19.860151290893555
		 38 23.132068634033203 39 25.463722229003906 40 26.697286605834961 41 27.25761604309082
		 42 27.380403518676758 43 27.301006317138672 44 27.254693984985352 45 27.163297653198242
		 46 26.86943244934082 47 26.489557266235352 48 26.140186309814453 49 25.937889099121094
		 50 25.999248504638672 51 26.812778472900391 52 28.312980651855469 53 29.785894393920898
		 54 30.519786834716797 55 29.804023742675778 56 26.737369537353516 57 21.78941535949707
		 58 16.318397521972656 59 11.684262275695801 60 9.2215194702148437 61 9.4161205291748047
		 62 11.376291275024414 63 14.353832244873047 64 17.593326568603516 65 20.330459594726562
		 66 20.330459594726562 67 20.330459594726562 68 20.330459594726562 69 20.084131240844727
		 70 19.378677368164063 71 18.25578498840332 72 16.752552032470703 73 14.908716201782227
		 74 12.771916389465332 75 10.400772094726562 76 7.865969181060791 77 5.2497267723083496
		 78 2.6441464424133301 79 0.14893874526023865 80 -2.1310844421386719;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateZ";
	rename -uid "7D5BF47D-40D5-C19B-25F8-71A4378478E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 80 ".ktv[0:79]"  0 4.2169618606567383 1 23.089494705200195
		 2 54.650672912597656 3 70.20220947265625 4 70.20220947265625 5 70.20220947265625
		 6 70.20220947265625 7 62.162845611572259 8 43.017024993896484 9 24.618497848510742
		 10 14.357954978942869 11 11.177870750427246 12 10.961957931518555 13 11.058904647827148
		 14 11.147015571594238 15 11.159516334533691 16 11.044403076171875 17 10.896186828613281
		 18 10.831480026245117 19 10.855351448059082 20 10.873378753662109 21 10.836252212524414
		 22 10.865289688110352 23 11.072870254516602 24 11.431941032409668 25 11.77034854888916
		 26 11.920768737792969 27 11.917096138000488 28 11.844633102416992 29 11.792648315429687
		 30 11.802867889404297 31 11.834705352783203 32 11.846270561218262 33 11.795369148254395
		 34 11.648006439208984 35 11.382347106933594 36 11.09127140045166 37 10.893381118774414
		 38 10.830934524536133 39 10.85979175567627 40 10.90111255645752 41 10.926064491271973
		 42 10.932058334350586 44 10.925924301147461 45 10.921589851379395 46 10.90836238861084
		 47 10.892853736877441 48 10.880158424377441 49 10.873488426208496 50 10.875459671020508
		 51 10.905936241149902 52 10.983851432800293 53 11.088861465454102 54 11.152179718017578
		 55 11.090335845947266 56 10.902767181396484 57 10.842391014099121 58 11.089072227478027
		 59 11.537540435791016 60 11.86314868927002 61 11.835214614868164 62 11.574948310852051
		 63 11.252910614013672 64 11.003696441650391 65 10.877263069152832 66 10.877263069152832
		 67 10.877263069152832 68 10.877263069152832 69 10.730321884155273 70 10.323637008666992
		 71 9.7169675827026367 72 8.976038932800293 73 8.1664199829101562 74 7.3482813835144043
		 75 6.5723114013671875 76 5.8770570755004883 77 5.2877264022827148 78 4.8163223266601563
		 79 4.4627933502197266 80 4.2169618606567383;
createNode animCurveTU -n "Character1_LeftHandIndex1_visibility";
	rename -uid "E41CFD8D-4C13-B045-5280-F593FDD4382E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateX";
	rename -uid "8091FE89-498E-4695-849E-E48A38EC51D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.5846562385559082;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateY";
	rename -uid "855A8B60-41A2-D519-32AC-A18916C51F6D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.7001519203186035;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateZ";
	rename -uid "959A6540-42DF-C659-909A-129A7B04EA0B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.35993427038192749;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleX";
	rename -uid "53949973-4AF9-33F4-AF34-DDBA08F7A651";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleY";
	rename -uid "6A229396-4304-8FCE-9B18-1FAD62E80305";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleZ";
	rename -uid "0F872905-4DAA-AFC6-16B4-3B925F32176F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateX";
	rename -uid "3EE2DD6D-4193-6A3B-5AA4-84891C907BA2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -15.816792488098145 1 -30.2904052734375
		 2 -47.060653686523438 3 -57.842254638671875 4 -57.842254638671875 5 -57.842254638671875
		 6 -57.842254638671875 7 -55.886096954345703 8 -51.394016265869141 9 -46.104454040527344
		 10 -41.150424957275391 11 -37.895519256591797 12 -37.443977355957031 13 -38.635875701904297
		 14 -39.533432006835937 15 -39.651535034179688 16 -38.474544525146484 17 -36.374507904052734
		 18 -34.142169952392578 19 -32.474552154541016 20 -32.026649475097656 21 -33.234046936035156
		 22 -35.685615539550781 23 -38.787059783935547 24 -41.884086608886719 25 -44.157535552978516
		 26 -45.071525573730469 27 -45.049728393554687 28 -44.614555358886719 29 -44.295978546142578
		 30 -44.359054565429688 31 -44.554145812988281 32 -44.624500274658203 33 -44.312789916992188
		 34 -43.376651763916016 35 -41.514202117919922 36 -38.980564117431641 37 -36.319530487060547
		 38 -33.978015899658203 39 -32.351554870605469 40 -31.50221061706543 41 -31.118566513061523
		 42 -31.034664154052734 43 -31.088911056518551 44 -31.120565414428711 45 -31.183057785034176
		 46 -31.384212493896481 47 -31.644767761230469 48 -31.884943008422852 49 -32.024261474609375
		 50 -31.981985092163086 51 -31.42303466796875 52 -30.399219512939453 53 -29.401340484619141
		 54 -28.906347274780273 55 -29.389097213745114 56 -31.474727630615234 57 -34.929485321044922
		 58 -38.957759857177734 59 -42.634796142578125 60 -44.726692199707031 61 -44.557243347167969
		 62 -42.890464782714844 63 -40.480636596679688 64 -37.993579864501953 65 -35.977962493896484
		 66 -35.977962493896484 67 -35.977962493896484 68 -35.977962493896484 69 -35.652805328369141
		 70 -34.74664306640625 71 -33.374111175537109 72 -31.653141021728519 73 -29.69425201416016
		 74 -27.594419479370117 75 -25.435209274291992 76 -23.284151077270508 77 -21.197996139526367
		 78 -19.226688385009766 79 -17.417213439941406 80 -15.816792488098145;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateY";
	rename -uid "1FCE67F6-411C-89D6-CB4F-8E806AE16593";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -16.401388168334961 1 -34.660442352294922
		 2 -53.695446014404297 3 -62.163970947265632 4 -62.163970947265632 5 -62.163970947265632
		 6 -62.163970947265632 7 -60.498043060302734 8 -56.357700347900391 9 -51.059364318847656
		 10 -45.891864776611328 11 -42.548828125 12 -42.248928070068359 13 -43.634376525878906
		 14 -44.658401489257813 15 -44.791854858398437 16 -43.448516845703125 17 -40.982719421386719
		 18 -38.278095245361328 19 -36.212451934814453 20 -35.652294158935547 21 -37.157375335693359
		 22 -40.156467437744141 23 -43.808059692382813 24 -47.254638671875 25 -49.637691497802734
		 26 -50.558116912841797 27 -50.536422729492188 28 -50.100688934326172 29 -49.778526306152344
		 30 -49.842521667480469 31 -50.039802551269531 32 -50.110702514648438 33 -49.79559326171875
		 34 -48.833992004394531 35 -46.854717254638672 36 -44.029659271240234 37 -40.917079925537109
		 38 -38.076301574707031 39 -36.058834075927734 40 -34.993949890136719 41 -34.510818481445313
		 42 -34.404994964599609 43 -34.473423004150391 44 -34.513339996337891 45 -34.592117309570313
		 46 -34.845485687255859 47 -35.17315673828125 48 -35.474658966064453 49 -35.649299621582031
		 50 -35.596324920654297 51 -34.894344329833984 52 -33.601840972900391 53 -32.335384368896484
		 54 -31.7053108215332 55 -32.319816589355469 56 -34.959381103515625 57 -39.240707397460937
		 58 -44.003585815429687 59 -48.055976867675781 60 -50.213451385498047 61 -50.042926788330078
		 62 -48.32568359375 63 -45.719944000244141 64 -42.891288757324219 65 -40.508079528808594
		 66 -40.508079528808594 67 -40.508079528808594 68 -40.508079528808594 69 -40.199298858642578
		 70 -39.323440551757813 71 -37.953651428222656 72 -36.162605285644531 73 -34.025344848632812
		 74 -31.620771408081055 75 -29.031917572021484 76 -26.345281600952148 77 -23.649633407592773
		 78 -21.034605026245117 79 -18.589326858520508 80 -16.401388168334961;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateZ";
	rename -uid "D77CCD93-428E-C1E2-21F7-439FEAC213B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -10.206292152404785 1 -16.222845077514648
		 2 -16.692291259765625 3 -13.085442543029785 4 -13.085442543029785 5 -13.085442543029785
		 6 -13.085442543029785 7 -13.443425178527832 8 -13.842299461364746 9 -13.587527275085449
		 10 -12.723215103149414 11 -11.774099349975586 12 -11.417449951171875 13 -11.472084045410156
		 14 -11.487249374389648 15 -11.487580299377441 16 -11.466989517211914 17 -11.335083961486816
		 18 -11.06273078918457 19 -10.772367477416992 20 -10.681991577148438 21 -10.913688659667969
		 22 -11.265440940856934 23 -11.476202964782715 24 -11.421074867248535 25 -11.212224006652832
		 26 -11.088685035705566 27 -11.091892242431641 28 -11.153260231018066 29 -11.19495677947998
		 30 -11.186918258666992 31 -11.161376953125 32 -11.151913642883301 33 -11.192824363708496
		 34 -11.299882888793945 35 -11.441630363464355 36 -11.480550765991211 37 -11.330001831054688
		 38 -11.037409782409668 39 -10.748066902160645 40 -10.569610595703125 41 -10.482956886291504
		 42 -10.463507652282715 43 -10.476102828979492 44 -10.483417510986328 45 -10.497783660888672
		 46 -10.543355941772461 47 -10.600855827331543 48 -10.652320861816406 49 -10.681496620178223
		 50 -10.672696113586426 51 -10.552032470703125 52 -10.310468673706055 53 -10.04991626739502
		 54 -9.9116525650024414 55 -10.046567916870117 56 -10.56352710723877 57 -11.174154281616211
		 58 -11.480093002319336 59 -11.367761611938477 60 -11.137932777404785 61 -11.16096305847168
		 62 -11.3460693359375 63 -11.479006767272949 64 -11.44752311706543 65 -11.296576499938965
		 66 -11.296576499938965 67 -11.296576499938965 68 -11.296576499938965 69 -11.36409854888916
		 70 -11.539671897888184 71 -11.769330978393555 72 -11.993067741394043 73 -12.156651496887207
		 74 -12.21904468536377 75 -12.15578556060791 76 -11.959449768066406 77 -11.638444900512695
		 78 -11.215046882629395 79 -10.723217010498047 80 -10.206292152404785;
createNode animCurveTU -n "Character1_LeftHandIndex2_visibility";
	rename -uid "AE00CE75-42EB-3444-3C8F-9CB7548A3022";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateX";
	rename -uid "DA677639-43C5-76DA-668C-988705749E33";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.2315034866333008;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateY";
	rename -uid "4E6A6A2C-483A-D146-DAF3-DAB8C767C91D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.1625156402587891;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateZ";
	rename -uid "16E74A58-40AF-FD61-B623-4A878928F6D4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.733155369758606;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleX";
	rename -uid "7432FDFE-41E9-002C-80FF-E6B68AAED0FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleY";
	rename -uid "E3BE18E1-4312-B692-43C9-63A5D76489B2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleZ";
	rename -uid "C3E41AFD-4009-37E7-A8DB-409FD57BC58C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateX";
	rename -uid "E7DAAD15-40AC-AEF5-A631-1399C256990C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 56 ".ktv[0:55]"  10 3.2274479622174113e-007 11 0.0025075748562812805
		 12 0.020901715382933617 13 0.063214927911758423 14 0.10920087993144989 15 0.11612059175968172
		 16 0.056212678551673889 17 0.002140153432264924 18 0.025791056454181671 19 0.10301792621612549
		 20 0.13296051323413849 21 0.061254281550645828 22 0.00013723954907618463 23 0.070131003856658936
		 24 0.28270423412322998 25 0.51601791381835938 26 0.62589412927627563 27 0.62317383289337158
		 28 0.5698782205581665 29 0.53210079669952393 30 0.53949570655822754 31 0.56263309717178345
		 32 0.57107454538345337 33 0.53406769037246704 34 0.4291948676109314 35 0.2505854070186615
		 36 0.079478055238723755 37 0.0016851925756782293 38 0.031057529151439663 39 0.11084320396184921
		 40 0.17314112186431885 41 0.20609299838542938 42 0.21370513737201691 44 0.2059134840965271
		 45 0.2003413587808609 46 0.18295350670814514 47 0.16166655719280243 48 0.14326883852481842
		 49 0.13313095271587372 50 0.13616606593132019 51 0.1796937882900238 52 0.27614641189575195
		 53 0.39163413643836975 54 0.45702448487281799 55 0.39318588376045227 56 0.17540118098258972
		 57 0.0073869219049811363 58 0.078347943723201752 59 0.35308173298835754 60 0.5834273099899292
		 61 0.56300437450408936 62 0.37859207391738892 63 0.17019554972648621 64 0.037684056907892227
		 65 3.9289471942538512e-007 66 3.9327241552200576e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateY";
	rename -uid "A04FBBAD-4DDE-F097-2AC7-D6A54B1BFDF5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -1.8989975103522738e-007 10 -2.0593482474851044e-007
		 11 0.63658446073532104 12 2.1179893016815186 13 3.8015463352203369 14 5.0450029373168945
		 15 5.2070064544677734 16 3.5757853984832764 17 0.57778722047805786 18 -2.7183456420898437
		 19 -5.2427201271057129 20 -5.9284677505493164 21 -4.0871238708496094 22 -0.42819485068321228
		 23 4.0124969482421875 24 8.1954326629638672 25 11.086774826049805 26 12.204095840454102
		 27 12.177751541137695 28 11.64874267578125 29 11.257697105407715 30 11.335371017456055
		 31 11.574833869934082 32 11.660898208618164 33 11.278410911560059 34 10.111494064331055
		 35 7.7102556228637695 36 4.2816085815429687 37 0.49789562821388245 38 -2.9646627902984619
		 39 -5.4307260513305664 40 -6.7351102828979492 41 -7.3275718688964835 42 -7.4573965072631827
		 43 -7.3734488487243661 44 -7.3244810104370108 45 -7.2278447151184073 46 -6.9171280860900879
		 47 -6.5154633522033691 48 -6.1460437774658203 49 -5.9321331977844238 50 -5.9970154762268066
		 51 -6.8572254180908203 52 -8.4434280395507812 53 -10.000811576843262 54 -10.776853561401367
		 55 -10.019979476928711 56 -6.7774906158447266 57 -1.5441217422485352 58 4.2499508857727051
		 59 9.1675815582275391 60 11.785635948181152 61 11.578632354736328 62 9.4947853088378906
		 63 6.3334145545959473 64 2.8987648487091064 65 -2.2740307770163781e-007 66 -2.2745783212485546e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateZ";
	rename -uid "B10C6285-47A5-C90F-7F74-BEBC14A36EA3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -6.3186455179220502e-008 10 -1.9145710794532533e-008
		 11 0.2917228639125824 12 0.97108590602874756 13 1.7450346946716309 14 2.3187849521636963
		 15 2.3937053680419922 16 1.6410837173461914 17 0.26477572321891785 18 -1.2465584278106689
		 19 -2.4094150066375732 20 -2.7268614768981934 21 -1.8761240243911743 22 -0.19621387124061584
		 23 1.8422220945358276 24 3.7849843502044673 25 5.1534748077392578 26 5.6900339126586914
		 27 5.6773266792297363 28 5.4227390289306641 29 5.2352457046508789 30 5.2724423408508301
		 31 5.3872575759887695 32 5.4285769462585449 33 5.2451634407043457 34 4.6888799667358398
		 35 3.5577378273010254 36 1.9662877321243284 37 0.22816197574138641 38 -1.3597158193588257
		 39 -2.4963672161102295 40 -3.101381778717041 41 -3.3773159980773926 42 -3.4378859996795654
		 43 -3.3987157344818115 44 -3.3758745193481445 45 -3.3308145999908447 46 -3.1860737800598145
		 47 -2.9992730617523193 48 -2.8277566432952881 49 -2.7285604476928711 50 -2.7586390972137451
		 51 -3.1581935882568359 52 -3.8992538452148438 53 -4.6333646774291992 54 -5.0019893646240234
		 55 -4.6424455642700195 56 -3.1210947036743164 57 -0.70772045850753784 58 1.9516879320144651
		 59 4.2422285079956055 60 5.4885134696960449 61 5.3890810012817383 62 4.3967432975769043
		 63 2.915905237197876 64 1.3296852111816406 65 4.1586424970319058e-008 66 4.1124270211412295e-008;
createNode animCurveTU -n "Character1_LeftHandIndex3_visibility";
	rename -uid "00085EA3-43D7-7B37-4CBE-F0AC58318133";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing1_translateX";
	rename -uid "E25F1BD0-44B8-2281-5B3E-04962276896E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.3586184978485107;
createNode animCurveTL -n "Character1_LeftHandRing1_translateY";
	rename -uid "32202A86-4DFC-7EA0-627A-ADA9ECB79153";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4706459045410156;
createNode animCurveTL -n "Character1_LeftHandRing1_translateZ";
	rename -uid "E0C6CD18-415D-1BBC-C0B7-0C9D67D2DE1A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8014267683029175;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleX";
	rename -uid "B436A95B-452D-3F72-187A-2C8A35A3B1A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleY";
	rename -uid "37DDDB3B-4338-4B3F-42E7-A5A34CD4101E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleZ";
	rename -uid "20807016-4783-E83B-182C-A7B1931656EB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateX";
	rename -uid "B923B9BF-4625-C0B0-4524-029B1A795720";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 7.0871744155883789 1 32.142608642578125
		 2 68.665504455566406 3 87.599990844726563 4 87.599990844726563 5 87.599990844726563
		 6 87.599990844726563 7 76.019294738769531 8 50.303272247314453 9 25.029155731201172
		 10 4.2828350067138672 11 -11.164340019226074 12 -16.971441268920898 13 -16.004888534545898
		 14 -15.291132926940918 15 -15.19810962677002 16 -16.13446044921875 17 -17.857147216796875
		 18 -19.763383865356445 19 -21.239763259887695 20 -21.644189834594727 21 -20.561681747436523
		 22 -18.437026977539063 23 -15.883816719055176 24 -13.478439331054688 25 -11.802055358886719
		 26 -11.14909839630127 27 -11.164533615112305 28 -11.474066734313965 29 -11.702383995056152
		 30 -11.657064437866211 31 -11.517250061035156 32 -11.466962814331055 33 -11.690299987792969
		 34 -12.369431495666504 35 -13.758293151855469 36 -15.729360580444336 37 -17.903152465820313
		 38 -19.90669059753418 39 -21.350481033325195 40 -22.122074127197266 41 -22.474672317504883
		 42 -22.552127838134766 43 -22.502035140991211 44 -22.472829818725586 45 -22.41522216796875
		 46 -22.230251312255859 47 -21.991704940795898 48 -21.772851943969727 49 -21.646356582641602
		 50 -21.684707641601563 51 -22.194635391235352 52 -23.142782211303711 53 -24.084995269775391
		 54 -24.559196472167969 55 -24.096668243408203 56 -22.147249221801758 57 -19.082073211669922
		 58 -15.747531890869141 59 -12.916601181030273 60 -11.39404296875 61 -11.515030860900879
		 62 -12.727126121520996 63 -14.550903320312502 64 -16.523075103759766 65 -18.190038681030273
		 66 -18.190038681030273 67 -18.190038681030273 68 -18.190038681030273 69 -17.748741149902344
		 70 -16.519279479980469 71 -14.660357475280763 72 -12.341177940368652 73 -9.7275962829589844
		 74 -6.9707427024841309 75 -4.1992192268371582 76 -1.5155456066131592 77 1.0032540559768677
		 78 3.3017449378967285 79 5.3400931358337402 80 7.0871744155883789;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateY";
	rename -uid "4A45B1D2-4BAE-2F36-37A9-CFB1B7B2D475";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -22.507083892822266 1 -39.86505126953125
		 2 -49.190048217773437 3 -48.969036102294922 4 -48.969036102294922 5 -48.969036102294922
		 6 -48.969036102294922 7 -48.008007049560547 8 -40.831764221191406 9 -25.583869934082031
		 10 -8.3061895370483398 11 3.9895038604736328 12 7.4242486953735352 13 5.8501644134521484
		 14 4.6879940032958984 15 4.5366120338439941 16 6.0612087249755859 17 8.8647642135620117
		 18 11.948463439941406 19 14.310334205627441 20 14.951878547668459 21 13.229153633117676
		 22 9.8058147430419922 23 5.6529760360717773 24 1.7456068992614746 25 -0.95147359371185303
		 26 -1.9927002191543581 27 -1.9681575298309326 28 -1.475245475769043 29 -1.1107940673828125
		 30 -1.1831920146942139 31 -1.4063689708709717 32 -1.4865736961364746 33 -1.1301019191741943
		 34 -0.042125418782234192 35 2.1985218524932861 36 5.4014372825622559 37 8.9394950866699219
		 38 12.178929328918457 39 14.486226081848146 40 15.706459999084474 41 16.260627746582031
		 42 16.382055282592773 43 16.303537368774414 44 16.25773811340332 45 16.167350769042969
		 46 15.876719474792479 47 15.500997543334963 48 15.155419349670408 49 14.955307960510254
		 50 15.016005516052244 51 15.820686340332029 52 17.30419921875 53 18.760248184204102
		 54 19.485555648803711 55 18.778165817260742 56 15.746102333068848 57 10.84982967376709
		 58 5.4310274124145508 59 0.83837807178497314 60 -1.6028118133544922 61 -1.409909725189209
		 62 0.53311187028884888 63 3.4842691421508789 64 6.6941680908203125 65 9.4052448272705078
		 66 9.4052448272705078 67 9.4052448272705078 68 9.4052448272705078 69 9.0753507614135742
		 70 8.1247434616088867 71 6.5956535339355469 72 4.5227861404418945 73 1.9481438398361206
		 74 -1.0689462423324585 75 -4.4469432830810547 76 -8.0816736221313477 77 -11.849103927612305
		 78 -15.609857559204103 79 -19.214256286621094 80 -22.507083892822266;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateZ";
	rename -uid "B6DE2679-4E73-1EE6-8A42-36B16B966AEF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 80 ".ktv[0:79]"  0 -6.542933464050293 1 -23.755945205688477
		 2 -53.540172576904297 3 -69.716575622558594 4 -69.716575622558594 5 -69.716575622558594
		 6 -69.716575622558594 7 -59.524177551269531 8 -38.608531951904297 9 -22.765043258666992
		 10 -15.976033210754395 11 -15.030269622802734 12 -15.460098266601563 13 -15.528106689453125
		 14 -15.595350265502928 15 -15.60517406463623 16 -15.517446517944336 17 -15.421198844909668
		 18 -15.41379451751709 19 -15.479668617248535 20 -15.508576393127441 21 -15.441669464111328
		 22 -15.407907485961914 23 -15.538497924804686 24 -15.830409049987791 25 -16.128402709960937
		 26 -16.264972686767578 27 -16.261613845825195 28 -16.195585250854492 29 -16.148515701293945
		 30 -16.157747268676758 31 -16.186574935913086 32 -16.197072982788086 33 -16.150972366333008
		 34 -16.019004821777344 35 -15.788151741027832 36 -15.552356719970703 37 -15.419792175292969
		 38 -15.417452812194824 39 -15.487118721008301 40 -15.548724174499512 41 -15.582494735717772
		 42 -15.590383529663086 44 -15.582308769226072 45 -15.576554298400879 46 -15.558711051940918
		 47 -15.537129402160645 48 -15.518747329711914 49 -15.508743286132813 50 -15.511728286743164
		 51 -15.555387496948244 52 -15.656098365783691 53 -15.781134605407717 54 -15.853382110595703
		 55 -15.782837867736816 56 -15.551018714904785 57 -15.404460906982422 58 -15.55069160461426
		 59 -15.921753883361815 60 -16.212411880493164 61 -16.187038421630859 62 -15.954507827758789
		 63 -15.680256843566895 64 -15.488338470458984 65 -15.412388801574709 66 -15.412388801574709
		 67 -15.412388801574709 68 -15.412388801574709 69 -15.196043014526367 70 -14.599141120910646
		 71 -13.714460372924805 72 -12.645051956176758 73 -11.493642807006836 74 -10.35333251953125
		 75 -9.3005170822143555 76 -8.3907146453857422 77 -7.657271385192872 78 -7.1122536659240723
		 79 -6.7486076354980469 80 -6.542933464050293;
createNode animCurveTU -n "Character1_LeftHandRing1_visibility";
	rename -uid "C2CACDC2-4760-D130-5FEA-D88417B3C2ED";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing2_translateX";
	rename -uid "7AAF7121-4853-6EA7-030A-7DB487AF5A2C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7360267639160156;
createNode animCurveTL -n "Character1_LeftHandRing2_translateY";
	rename -uid "4701FD02-4ED6-9556-17BE-919C31786B28";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8172166347503662;
createNode animCurveTL -n "Character1_LeftHandRing2_translateZ";
	rename -uid "E9D1047E-447F-B26A-0FFE-F08BF939E915";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.81390875577926636;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleX";
	rename -uid "687431D3-4354-52A3-E5CC-9290EC1DA07D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleY";
	rename -uid "82AA3534-4012-F64C-D8B0-0983D2D53E2C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleZ";
	rename -uid "B3363CAF-4E9D-5459-F469-24A9432AE40B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateX";
	rename -uid "4332FF38-47F4-3670-A177-F69BF32EE8C8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 5.3052768707275391 1 22.004505157470703
		 2 50.005168914794922 3 64.615447998046875 4 64.615447998046875 5 64.615447998046875
		 6 64.615447998046875 7 61.136722564697273 8 52.475082397460937 9 41.354164123535156
		 10 30.462028503417972 11 22.82073974609375 12 20.819910049438477 13 22.294395446777344
		 14 23.41810417175293 15 23.566692352294922 16 22.093551635742188 17 19.517587661743164
		 18 16.876232147216797 19 14.98289680480957 20 14.487301826477053 21 15.836014747619627
		 22 18.690624237060547 23 22.482940673828125 24 26.398887634277344 25 29.304641723632813
		 26 30.471029281616214 27 30.443250656127933 28 29.888273239135742 29 29.481506347656246
		 30 29.562068939208984 31 29.811166763305664 32 29.900968551635742 33 29.502979278564457
		 34 28.306303024291992 35 25.927265167236328 36 22.724712371826172 37 19.45123291015625
		 38 16.68663215637207 39 14.846239089965822 40 13.914339065551758 41 13.500309944152832
		 42 13.410346031188965 43 13.468487739562988 44 13.502455711364746 45 13.569602012634277
		 46 13.786531448364258 47 14.069293022155762 48 14.331698417663572 49 14.484674453735353
		 50 14.438194274902342 51 13.828533172607422 52 12.735904693603516 53 11.701943397521973
		 54 11.200709342956543 55 11.689452171325684 56 13.884532928466797 57 17.794801712036133
		 58 22.696197509765625 59 27.357702255249023 60 30.031370162963867 61 29.815128326416016
		 62 27.684564590454102 63 24.613849639892578 64 21.497053146362305 65 19.040355682373047
		 66 19.040355682373047 67 19.040355682373047 68 19.040355682373047 69 18.715911865234375
		 70 17.809175491333008 71 16.435497283935547 72 14.732349395751955 73 12.85666561126709
		 74 10.973223686218262 75 9.2353439331054687 76 7.7643694877624512 77 6.6350789070129395
		 78 5.8709216117858887 79 5.4481086730957031 80 5.3052768707275391;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateY";
	rename -uid "ABDD1864-4F7D-F4DE-AB43-9FBCEC5277F4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 12.256824493408203 1 31.217006683349606
		 2 40.664775848388672 3 40.079448699951172 4 40.079448699951172 5 40.079448699951172
		 6 40.079448699951172 7 39.739498138427734 8 38.670761108398438 9 36.836524963378906
		 10 34.537471771240234 11 32.839733123779297 12 32.874343872070313 13 33.852222442626953
		 14 34.549476623535156 15 34.638706207275391 16 33.723320007324219 17 31.947084426879883
		 18 29.8643684387207 19 28.184919357299805 20 27.716821670532227 21 28.962409973144535
		 22 31.325294494628906 23 33.972034454345703 24 36.215114593505859 25 37.607151031494141
		 26 38.107627868652344 27 38.096076965332031 28 37.861560821533203 29 37.685100555419922
		 30 37.720359802246094 31 37.828411102294922 32 37.867008209228516 33 37.694515228271484
		 34 37.152927398681641 35 35.968391418457031 36 34.123981475830078 37 31.898162841796879
		 38 29.703607559204105 39 28.057075500488281 40 27.159946441650391 41 26.746711730957031
		 42 26.655689239501953 43 26.714565277099609 44 26.748876571655273 45 26.816516876220703
		 46 27.033369064331055 47 27.312244415283203 48 27.567276000976563 49 27.714305877685547
		 50 27.669759750366211 51 27.075063705444336 52 25.958921432495117 53 24.839269638061523
		 54 24.272846221923828 55 24.825345993041992 56 27.13050651550293 57 30.621145248413086
		 58 34.106163024902344 59 36.698322296142578 60 37.922714233398438 61 37.830120086669922
		 62 36.857555389404297 63 35.248584747314453 64 33.332588195800781 65 31.591485977172852
		 66 31.591485977172852 67 31.591485977172852 68 31.591485977172852 69 31.528385162353519
		 70 31.315969467163086 71 30.887031555175785 72 30.155185699462891 73 29.042343139648437
		 74 27.501047134399414 75 25.528953552246094 76 23.173723220825195 77 20.528936386108398
		 78 17.724020004272461 79 14.911820411682127 80 12.256824493408203;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateZ";
	rename -uid "6DA19700-4BCD-D097-81B5-61BE5FA3794D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 9.5605840682983398 1 32.551433563232422
		 2 65.941635131835937 3 82.926361083984375 4 82.926361083984375 5 82.926361083984375
		 6 82.926361083984375 7 80.618255615234375 8 75.004600524902344 9 68.088363647460938
		 10 61.651771545410156 11 57.722625732421882 12 57.466186523437507 13 59.256458282470703
		 14 60.611675262451172 15 60.790325164794915 16 59.013427734374993 17 55.872409820556641
		 18 52.598148345947266 19 50.210586547851562 20 49.579166412353516 21 51.291046142578125
		 22 54.853664398193359 23 59.484378814697259 24 64.173164367675781 25 67.605567932128906
		 26 68.97412109375 27 68.941581726074219 28 68.290962219238281 29 67.813400268554687
		 30 67.908035278320312 31 68.200485229492188 32 68.305862426757813 33 67.838630676269531
		 34 66.430183410644531 35 63.612617492675788 36 59.776321411132812 37 55.790863037109375
		 38 52.360706329345703 39 50.036758422851563 40 48.845516204833984 41 48.312820434570313
		 42 48.196781158447266 43 48.27178955078125 44 48.315589904785156 45 48.402126312255859
		 46 48.681312561035156 47 49.044326782226562 48 49.380317687988281 49 49.575809478759766
		 50 49.516441345214844 51 48.735298156738281 52 47.323375701904297 53 45.971797943115234
		 54 45.310684204101563 55 45.955371856689453 56 48.807239532470703 57 53.743705749511719
		 58 59.741909027099616 59 65.309661865234375 60 68.458824157714844 61 68.20513916015625
		 62 65.696182250976563 63 62.045871734619141 64 58.290130615234375 65 55.285167694091797
		 66 55.285167694091797 67 55.285167694091797 68 55.285167694091797 69 54.587394714355469
		 70 52.619598388671875 71 49.579269409179688 72 45.680812835693359 73 41.156192779541016
		 74 36.246143341064453 75 31.183506011962891 76 26.175495147705078 77 21.392152786254883
		 78 16.964925765991211 79 12.994029998779297 80 9.5605840682983398;
createNode animCurveTU -n "Character1_LeftHandRing2_visibility";
	rename -uid "246B1359-4F57-E6D8-D762-AF90E5C2A669";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing3_translateX";
	rename -uid "0C2C547B-406D-6C72-2185-55BDD4912593";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.4660000801086426;
createNode animCurveTL -n "Character1_LeftHandRing3_translateY";
	rename -uid "988F0367-41C6-2B2B-FD31-2080D2F06C68";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9262526035308838;
createNode animCurveTL -n "Character1_LeftHandRing3_translateZ";
	rename -uid "6E01BB18-42F2-7D97-AED6-ED855B750BCD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.018019437789917;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleX";
	rename -uid "8037EF81-4872-D258-AD47-3A8E41830D35";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleY";
	rename -uid "3AC1D32A-46AF-1F2D-9685-AE85880C2E06";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleZ";
	rename -uid "34863F7D-491A-B23F-9D99-F38E8F0ED237";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateX";
	rename -uid "C031203D-48DF-D448-6B17-7BB55D509FC7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 1.6966563975984172e-007 10 1.1404165434214519e-007
		 11 -0.43188151717185974 12 -1.4272178411483765 13 -2.5421006679534912 14 -3.3545882701873779
		 15 -3.4597654342651367 16 -2.3935930728912354 17 -0.39209693670272827 18 1.872814416885376
		 19 3.6541097164154053 20 4.1450982093811035 21 2.8335952758789063 22 0.29192283749580383
		 23 -2.680588960647583 24 -5.3723955154418945 25 -7.1744170188903809 26 -7.8584179878234863
		 27 -7.8423686027526855 28 -7.5192890167236337 29 -7.2794914245605478 30 -7.3271894454956046
		 31 -7.4740304946899414 32 -7.5267300605773935 33 -7.2922148704528817 34 -6.5717873573303223
		 35 -5.0653905868530273 36 -2.85687255859375 37 -0.33800467848777771 38 2.0448267459869385
		 39 3.7884166240692139 40 4.7265586853027344 41 5.1563320159912109 42 5.2508139610290527
		 43 5.1897072792053223 44 5.1540837287902832 45 5.0838289260864258 46 4.8583512306213379
		 47 4.5678081512451172 48 4.301518440246582 49 4.147730827331543 50 4.1943454742431641
		 51 4.8149538040161133 52 5.9720191955566406 53 7.1241545677185059 54 7.704242229461669
		 55 7.1384344100952148 56 4.7572250366210937 57 1.0581101179122925 58 -2.8361573219299316
		 59 -5.9835114479064941 60 -7.603038787841796 61 -7.4763574600219735 62 -6.1879992485046387
		 63 -4.1867842674255371 64 -1.9463990926742554 65 3.9638898385874199e-008 66 3.9648647032208828e-008;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateY";
	rename -uid "103A3E86-4189-06BB-2240-D5A96E244B7A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -3.8192109741430613e-007 10 -4.4221584971637645e-007
		 11 0.32988676428794861 12 1.1103408336639404 13 2.0186097621917725 14 2.703723669052124
		 15 2.7938647270202637 16 1.8955138921737673 17 0.29927793145179749 18 -1.3707256317138672
		 19 -2.5869925022125244 20 -2.9076974391937256 21 -2.0371193885803223 22 -0.22001965343952179
		 23 2.133991003036499 24 4.4924445152282715 25 6.1989216804504395 26 6.8745613098144531
		 27 6.8585286140441895 28 6.5376262664794922 29 6.3016986846923828 30 6.3484745025634766
		 31 6.4929513931274414 32 6.5449776649475098 33 6.3141689300537109 34 5.6165027618408203
		 35 4.2121114730834961 36 2.2816882133483887 37 0.25773307681083679 38 -1.4918394088745117
		 39 -2.675335168838501 40 -3.2795360088348389 41 -3.5488827228546143 42 -3.6074757575988774
		 43 -3.569605827331543 44 -3.5474860668182373 45 -3.5037689208984375 46 -3.3626260757446289
		 47 -3.1788675785064697 48 -3.0085716247558594 49 -2.909400463104248 50 -2.9395239353179932
		 51 -3.3353140354156494 52 -4.0474224090576172 53 -4.7237958908081055 54 -5.0522360801696777
		 55 -4.7319774627685547 56 -3.2989091873168945 57 -0.78624802827835083 58 2.2642841339111328
		 59 5.0593910217285156 60 6.6204757690429687 61 6.4952468872070313 62 5.251774787902832
		 63 3.4261715412139893 64 1.5287735462188721 65 -5.1441003279251163e-007 66 -5.1444663995425799e-007;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateZ";
	rename -uid "2804151C-4177-B779-0760-31A0AB7A1F4A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 6.6847000823599956e-008 10 7.3158091140612669e-008
		 11 0.4403400719165802 12 1.4555546045303345 13 2.5933756828308105 14 3.4230573177337646
		 15 3.5304908752441406 16 2.4417698383331299 17 0.39977216720581055 18 -1.9084042310714722
		 19 -3.7220423221588135 20 -4.2217087745666504 21 -2.8868019580841064 22 -0.29758492112159729
		 23 2.7347655296325684 24 5.4854655265808105 25 7.3298635482788086 26 8.0306463241577148
		 27 8.0141992568969727 28 7.6831464767456046 29 7.4374899864196777 30 7.4863495826721191
		 31 7.6367783546447763 32 7.690770149230957 33 7.4505233764648438 34 6.7127752304077148
		 35 5.1714897155761719 36 2.914759635925293 37 0.34461623430252075 38 -2.0836014747619629
		 39 -3.8587329387664795 40 -4.813321590423584 41 -5.2505145072937012 42 -5.3466181755065918
		 43 -5.2844629287719727 44 -5.248227596282959 45 -5.176764965057373 46 -4.9473967552185059
		 47 -4.6518125534057617 48 -4.3808736801147461 49 -4.2243876457214355 50 -4.2718210220336914
		 51 -4.9032483100891113 52 -6.0800938606262207 53 -7.2514476776123038 54 -7.8410420417785645
		 55 -7.265963077545166 56 -4.844520092010498 57 -1.0784285068511963 58 2.8936073780059814
		 59 6.1106672286987305 60 7.7689542770385742 61 7.639162540435791 62 6.3199319839477539
		 63 4.2733073234558105 64 1.9853264093399048 65 7.9636784278136474e-008 66 7.9514514084166876e-008;
createNode animCurveTU -n "Character1_LeftHandRing3_visibility";
	rename -uid "3D2E2F11-4E43-8644-276A-00864EE108ED";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightShoulder_translateX";
	rename -uid "780A32D5-4C01-9EBD-2E93-E1B612F8AF4F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.151631355285645;
createNode animCurveTL -n "Character1_RightShoulder_translateY";
	rename -uid "D5833E4B-409F-470B-E12A-848F0F398A42";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.1017274856567383;
createNode animCurveTL -n "Character1_RightShoulder_translateZ";
	rename -uid "BEEA3931-4641-C015-EDE1-4EA4E921E906";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.81731081008911133;
createNode animCurveTU -n "Character1_RightShoulder_scaleX";
	rename -uid "1155835A-47E4-A94B-D133-749E95911455";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightShoulder_scaleY";
	rename -uid "0A80A8B2-45DD-4A99-BC0C-95BEDCFD961D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightShoulder_scaleZ";
	rename -uid "6A449A03-4E0B-6DE9-79C9-FDA6E4104B58";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightShoulder_rotateX";
	rename -uid "4693A242-4097-4CE5-C3A3-8B910E792627";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 11.355966567993164 1 5.5424432754516602
		 2 -0.14319173991680145 3 -3.7200305461883545 4 -4.9353570938110352 5 -5.1349430084228516
		 6 -4.7665033340454102 7 -4.2544217109680176 8 -3.9958293437957768 9 -3.9824554920196538
		 10 -3.9503297805786133 11 -4.6924662590026855 12 -6.7009105682373047 13 -9.4393854141235352
		 14 -12.037224769592285 15 -13.246502876281738 16 -12.175228118896484 17 -9.6442403793334961
		 18 -6.8022737503051758 19 -4.5719046592712402 20 -3.6758363246917725 21 -4.571995735168457
		 22 -6.8033175468444824 23 -9.6471986770629883 24 -12.178908348083496 25 -13.174619674682617
		 26 -11.244382858276367 27 -7.5847163200378418 28 -4.3452715873718262 29 -2.86434006690979
		 30 -3.3677716255187988 31 -5.0238761901855469 32 -7.2413797378540048 33 -9.2377214431762695
		 34 -9.9932775497436523 35 -8.7893447875976563 36 -6.3383817672729492 37 -3.6432759761810303
		 38 -1.5412623882293701 39 -0.70453894138336182 40 -1.5671499967575073 41 -3.6982970237731934
		 42 -6.4166240692138672 43 -8.8647184371948242 44 -9.9779253005981445 45 -9.3078022003173828
		 46 -7.5967798233032235 47 -5.4983115196228027 48 -3.5521955490112305 49 -2.1548669338226318
		 50 -1.6193621158599854 51 -2.3723652362823486 52 -4.2782869338989258 53 -6.7680759429931641
		 54 -9.0393037796020508 55 -10.019968032836914 56 -8.9352102279663086 57 -6.5198631286621094
		 58 -3.8312501907348637 59 -1.7196623086929321 60 -0.86029899120330811 61 -1.5055714845657349
		 62 -3.2179973125457764 63 -5.6244401931762695 64 -8.3117437362670898 65 -10.764705657958984
		 66 -10.760767936706543 67 -10.514664649963379 68 -9.9405450820922852 69 -8.7097854614257812
		 70 -6.7065801620483398 71 -4.2002382278442383 72 -1.5443432331085205 73 0.83169347047805786
		 74 2.4643299579620361 75 3.2163772583007812 76 3.3880627155303955 77 3.1852831840515137
		 78 2.8145716190338135 79 2.471076488494873 80 2.331308126449585;
createNode animCurveTA -n "Character1_RightShoulder_rotateY";
	rename -uid "0C025917-4BD0-A23E-B14E-F388F1E6608D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -21.784000396728516 1 -17.628993988037109
		 2 -13.375358581542969 3 -11.068476676940918 4 -11.211759567260742 5 -12.510963439941406
		 6 -14.328488349914551 7 -16.008995056152344 8 -16.875968933105469 9 -16.882314682006836
		 10 -16.524187088012695 11 -14.190008163452148 12 -9.3909587860107422 13 -3.9348118305206294
		 14 0.50666940212249756 15 2.4708833694458008 16 1.3257395029067993 17 -1.8850557804107664
		 18 -5.972048282623291 19 -9.5524358749389648 20 -11.091608047485352 21 -9.5473337173461914
		 22 -5.9569616317749023 23 -1.8629238605499268 24 1.3449339866638184 25 2.3539066314697266
		 26 -0.55435687303543091 27 -6.5778317451477051 28 -12.928961753845215 29 -16.336864471435547
		 30 -15.378641128540039 31 -11.996309280395508 32 -7.8108115196228027 33 -4.332953929901123
		 34 -2.9260709285736084 35 -4.3702044486999512 36 -7.8119697570800781 37 -12.063739776611328
		 38 -15.707696914672852 39 -17.193153381347656 40 -15.484012603759766 41 -11.646610260009766
		 42 -7.2487587928771982 43 -3.723053932189941 44 -2.3263955116271973 45 -3.50797438621521
		 46 -6.4085111618041992 47 -10.256730079650879 48 -14.205606460571289 49 -17.323013305664063
		 50 -18.631418228149414 51 -16.950542449951172 52 -12.89157772064209 53 -8.1374893188476563
		 54 -4.2429099082946777 55 -2.5897026062011719 56 -3.9610869884490967 57 -7.4493718147277823
		 58 -11.789094924926758 59 -15.540432929992676 60 -17.128377914428711 61 -15.911164283752441
		 62 -12.878287315368652 63 -8.8941669464111328 64 -4.7874727249145508 65 -1.3007609844207764
		 66 -1.0092345476150513 67 -0.9384397864341737 68 -1.1603842973709106 69 -1.9352582693099978
		 70 -3.1894321441650391 71 -4.5173468589782715 72 -5.6093010902404785 73 -6.2878499031066895
		 74 -6.4843692779541016 75 -6.2635478973388672 76 -5.7754693031311035 77 -5.081946849822998
		 78 -4.2458305358886719 79 -3.3607277870178223 80 -2.5589070320129395;
createNode animCurveTA -n "Character1_RightShoulder_rotateZ";
	rename -uid "FA69F95F-4EFC-7EBD-C047-58B9005B07F1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -5.0574803352355957 1 0.58860176801681519
		 2 5.95111083984375 3 9.8509130477905273 4 12.528775215148926 5 14.826117515563967
		 6 16.61726188659668 7 17.800031661987305 8 18.306964874267578 9 18.055870056152344
		 10 17.164379119873047 11 14.673357009887694 12 10.276485443115234 13 5.1761178970336914
		 14 0.7025296688079834 15 -1.5706578493118286 16 -1.1014738082885742 17 0.78954702615737915
		 18 3.0461065769195557 19 4.8499884605407715 20 5.5820846557617188 21 4.8391828536987305
		 22 3.0137114524841309 23 0.74081200361251831 24 -1.1450432538986206 25 -1.4386748075485229
		 26 1.53728187084198 27 6.6783332824707031 28 11.471962928771973 29 13.944216728210449
		 30 13.48084545135498 31 11.30012321472168 32 8.3148202896118164 33 5.5504741668701172
		 34 4.1950359344482422 35 4.8514132499694824 36 6.6494898796081543 37 8.689361572265625
		 38 10.241879463195801 39 10.758428573608398 40 9.9036159515380859 41 8.0473928451538086
		 42 5.7512989044189453 43 3.7615375518798828 44 3.051666259765625 45 4.1835231781005859
		 46 6.5348567962646484 47 9.3835592269897461 48 12.094794273376465 49 14.127147674560547
		 50 14.993128776550293 51 14.00605583190918 52 11.40886402130127 53 8.0954179763793945
		 54 5.0906357765197754 55 3.6141633987426762 56 4.3192129135131836 57 6.293492317199707
		 58 8.5404453277587891 59 10.285989761352539 60 10.937586784362793 61 10.559207916259766
		 62 9.6238603591918945 63 8.1689672470092773 64 6.2958579063415527 65 4.2318844795227051
		 66 3.5622191429138184 67 2.4309036731719971 68 0.7290031909942627 69 -2.3349120616912842
		 70 -6.951690673828125 71 -12.322268486022949 72 -17.610502243041992 73 -21.937177658081055
		 74 -24.400188446044922 75 -24.703012466430664 76 -23.490285873413086 77 -21.278573989868164
		 78 -18.581333160400391 79 -15.901342391967772 80 -13.727509498596191;
createNode animCurveTU -n "Character1_RightShoulder_visibility";
	rename -uid "3F199481-4A35-5A7D-76F3-5893CFF1CC8E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightArm_translateX";
	rename -uid "45DEB94F-4547-F204-6817-93B748AA483E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.05986499786377;
createNode animCurveTL -n "Character1_RightArm_translateY";
	rename -uid "704FD68E-442E-A165-9D57-AE95C1B73B4E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.8638343811035156;
createNode animCurveTL -n "Character1_RightArm_translateZ";
	rename -uid "3DED6C78-4E42-8086-27B5-64A94F65C27C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4631747007369995;
createNode animCurveTU -n "Character1_RightArm_scaleX";
	rename -uid "07312AFA-4BAD-7B06-6700-6392DC604E89";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightArm_scaleY";
	rename -uid "AF93C398-49CC-B890-9D63-CCA1104047FC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightArm_scaleZ";
	rename -uid "561F79EA-47C6-C6AF-487D-20922447AEDF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightArm_rotateX";
	rename -uid "47AB66C2-4AC7-2194-5BE2-AEA64D09D673";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -65.858718872070313 1 -49.204521179199219
		 2 -31.032623291015625 3 -18.138072967529297 4 -13.28016185760498 5 -12.109354972839355
		 6 -12.915228843688965 7 -14.104714393615723 8 -14.310791015625 9 -13.648853302001953
		 10 -13.102053642272949 11 -11.955093383789063 12 -9.8864364624023437 13 -7.5099892616271973
		 14 -5.4302272796630859 15 -4.2398462295532227 16 -4.2184591293334961 17 -5.0352888107299805
		 18 -6.3561081886291504 19 -7.7531218528747559 20 -8.6491107940673828 21 -8.5066003799438477
		 22 -7.5426969528198251 23 -6.2858209609985352 24 -5.3234772682189941 25 -5.3076868057250977
		 26 -7.2162399291992196 27 -10.660344123840332 28 -14.156903266906738 29 -16.014471054077148
		 30 -15.518113136291502 31 -13.728240966796875 32 -11.502384185791016 33 -9.6062431335449219
		 34 -8.7207212448120117 35 -9.1664524078369141 36 -10.479290962219238 37 -12.163642883300781
		 38 -13.664045333862305 39 -14.317605018615723 40 -13.606021881103516 41 -11.922525405883789
		 42 -9.9140691757202148 43 -8.2251987457275391 44 -7.5255460739135733 45 -8.1729936599731445
		 46 -9.7828683853149414 47 -11.936825752258301 48 -14.165587425231934 49 -15.941705703735352
		 50 -16.696918487548828 51 -15.752686500549316 52 -13.487875938415527 53 -10.877270698547363
		 54 -8.7730836868286133 55 -7.8971657752990723 56 -8.5472192764282227 57 -10.112812995910645
		 58 -11.963151931762695 59 -13.514368057250977 60 -14.177852630615234 61 -13.394875526428223
		 62 -11.428433418273926 63 -8.8820457458496094 64 -6.317558765411377 65 -4.2894845008850098
		 66 -4.2819790840148926 67 -5.338249683380127 68 -7.785275936126709 69 -13.308694839477539
		 70 -22.113323211669922 71 -32.602775573730469 72 -43.026458740234375 73 -51.823169708251953
		 74 -57.67383956909179 75 -60.153327941894524 76 -59.869255065917969 77 -57.747272491455085
		 78 -55.012344360351562 79 -52.20050048828125 80 -50.0416259765625;
createNode animCurveTA -n "Character1_RightArm_rotateY";
	rename -uid "DB2C3E48-4E43-F733-4E42-69AD56255133";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 27.430723190307617 1 33.148769378662109
		 2 39.144092559814453 3 41.791042327880859 4 41.031951904296875 5 39.248268127441406
		 6 37.279457092285156 7 35.641754150390625 8 34.717693328857422 9 34.286392211914063
		 10 33.840419769287109 11 32.209804534912109 12 28.9703369140625 13 25.208150863647461
		 14 22.04998779296875 15 20.652688980102539 16 21.90534782409668 17 25.010534286499023
		 18 28.615299224853516 19 31.418239593505863 20 32.143589019775391 21 29.924304962158203
		 22 25.619611740112305 23 20.569931030273438 24 16.200920104980469 25 13.957393646240234
		 26 15.27305793762207 27 19.106672286987305 28 23.160202026367188 29 25.187700271606445
		 30 24.262693405151367 31 21.653507232666016 32 18.455358505249023 33 15.83991527557373
		 34 15.042896270751953 35 17.075799942016602 36 21.108022689819336 37 25.638402938842773
		 38 29.181596755981449 39 30.311374664306641 40 28.092191696166992 41 23.508609771728516
		 42 18.061599731445313 43 13.291707038879395 44 10.729174613952637 45 10.785242080688477
		 46 12.393124580383301 47 14.841115951538086 48 17.440397262573242 49 19.527067184448242
		 50 20.498956680297852 51 19.591808319091797 52 17.174497604370117 53 14.348771095275881
		 54 12.264105796813965 55 12.122373580932617 56 14.920140266418457 57 19.815946578979492
		 58 25.244585037231445 59 29.644670486450195 60 31.508796691894528 61 30.153448104858398
		 62 26.560163497924805 63 21.753034591674805 64 16.704708099365234 65 12.276714324951172
		 66 11.625652313232422 67 11.113635063171387 68 10.82133674621582 69 10.213007926940918
		 70 9.2332162857055664 71 8.1850919723510742 72 7.7113237380981445 73 8.0971851348876953
		 74 8.4437923431396484 75 7.9349455833435067 76 6.4612517356872559 77 4.5446977615356445
		 78 2.7957799434661865 79 1.496504545211792 80 0.77460235357284546;
createNode animCurveTA -n "Character1_RightArm_rotateZ";
	rename -uid "24058E1B-4CCB-0281-937C-C581103A95F7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -40.528045654296875 1 -31.992130279541012
		 2 -20.221437454223633 3 -8.2573642730712891 4 1.7061994075775146 5 11.04864501953125
		 6 19.400543212890625 7 26.271764755249023 8 30.987085342407227 9 33.490703582763672
		 10 34.439861297607422 11 33.047676086425781 12 29.310253143310543 13 24.764123916625977
		 14 20.927196502685547 15 19.305057525634766 16 20.654037475585937 17 23.769622802734375
		 18 27.200441360473633 19 29.516416549682617 20 29.378288269042972 21 26.06627082824707
		 22 20.775991439819336 23 15.05142402648926 24 10.389727592468262 25 8.2274112701416016
		 26 9.7440080642700195 27 13.751472473144531 28 18.091562271118164 29 20.795963287353516
		 30 21.090948104858398 31 20.035467147827148 32 18.50834846496582 33 17.405996322631836
		 34 17.639423370361328 35 20.005186080932617 36 23.824213027954102 37 27.839479446411133
		 38 30.774398803710938 39 31.420190811157227 40 29.014074325561523 41 24.440877914428711
		 42 18.980365753173828 43 13.963883399963379 44 10.832992553710937 45 10.040221214294434
		 46 10.663725852966309 47 12.131111145019531 48 13.885083198547363 49 15.408940315246584
		 50 16.25010871887207 51 15.798183441162111 52 14.310462951660156 53 12.629974365234375
		 54 11.679595947265625 55 12.435164451599121 56 15.745688438415527 57 20.831823348999023
		 58 26.271120071411133 59 30.714000701904297 60 32.831630706787109 61 32.465656280517578
		 62 30.601840972900391 63 27.619499206542969 64 24.000505447387695 65 20.386241912841797
		 66 20.164331436157227 67 19.286649703979492 68 17.639265060424805 69 15.302240371704102
		 70 12.887796401977539 71 11.194069862365723 72 10.219758987426758 73 9.4190044403076172
		 74 8.1053743362426758 75 4.757779598236084 76 -2.0797684192657471 77 -11.060637474060059
		 78 -20.38554573059082 79 -29.938835144042972 80 -38.964550018310547;
createNode animCurveTU -n "Character1_RightArm_visibility";
	rename -uid "DCCE51F5-499B-1901-E208-5AA600E1C3FD";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightForeArm_translateX";
	rename -uid "9D289C45-489F-289E-5626-5A8EC094581F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -16.894504547119141;
createNode animCurveTL -n "Character1_RightForeArm_translateY";
	rename -uid "F47940D5-400B-DC52-E60B-D6A6E999522B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.7289671897888184;
createNode animCurveTL -n "Character1_RightForeArm_translateZ";
	rename -uid "F4285E62-4737-052F-B82D-F589E569264A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.2348899841308594;
createNode animCurveTU -n "Character1_RightForeArm_scaleX";
	rename -uid "7DB4BFCC-4461-EAD8-C95D-019786F69261";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightForeArm_scaleY";
	rename -uid "E3798554-41CF-793E-4B8B-B4B576D48D38";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightForeArm_scaleZ";
	rename -uid "CB0A725B-49DD-8D35-FD62-B382F6A37660";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightForeArm_rotateX";
	rename -uid "AA00E18D-4B05-165A-0792-548233C98D82";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -52.750164031982422 1 -40.863006591796875
		 2 -35.991359710693359 3 -32.962703704833984 4 -30.218236923217777 5 -27.830087661743164
		 6 -25.838821411132812 7 -24.180791854858398 8 -22.962076187133789 9 -22.101547241210937
		 10 -21.134693145751953 11 -22.56242561340332 12 -27.843185424804688 13 -33.446395874023438
		 14 -37.168563842773437 15 -39.287776947021484 16 -39.565784454345703 17 -36.932479858398437
		 18 -31.125339508056641 19 -24.210775375366211 20 -21.395788192749023 21 -26.390724182128906
		 22 -34.988735198974609 23 -41.807220458984375 24 -45.314743041992187 25 -46.30926513671875
		 26 -44.627006530761719 27 -38.846733093261719 28 -29.44381141662598 29 -24.006008148193359
		 30 -27.576541900634766 31 -34.350383758544922 32 -39.685390472412109 33 -42.184768676757813
		 34 -42.355781555175781 35 -40.281723022460938 36 -34.960487365722656 37 -25.122097015380859
		 38 -12.726213455200195 39 -3.8834955692291255 40 -2.7196147441864014 41 -7.0221877098083496
		 42 -12.81416130065918 43 -16.311550140380859 44 -15.054282188415527 45 -8.007420539855957
		 46 3.464853048324585 47 16.273975372314453 48 26.304525375366211 49 32.1090087890625
		 50 34.340248107910156 51 33.007171630859375 52 27.394168853759766 53 17.334888458251953
		 54 6.2375693321228027 55 0.10009919106960297 56 2.4148812294006348 57 11.274821281433105
		 58 21.956039428710937 59 29.530437469482418 60 32.390655517578125 61 30.951700210571289
		 62 25.716991424560547 63 16.37537956237793 64 4.4846863746643066 65 -6.0436305999755859
		 66 -5.9168453216552734 67 -5.7901763916015625 68 -5.6635565757751465 69 -9.9085636138916016
		 70 -9.9944992065429687 71 -7.6094703674316406 72 -7.6284170150756836 73 -12.890401840209961
		 74 -20.177042007446289 75 -23.228662490844727 76 -24.258075714111328 77 -21.390453338623047
		 78 -13.352609634399414 79 -4.5793843269348145 80 2.8954775333404541;
createNode animCurveTA -n "Character1_RightForeArm_rotateY";
	rename -uid "1DD96548-4ECA-8083-E00E-4BB5ED92F893";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -64.743614196777344 1 -48.504798889160156
		 2 -31.780679702758786 3 -16.112602233886719 4 -0.19429488480091095 5 16.986310958862305
		 6 33.738487243652344 7 48.405998229980469 8 59.357696533203118 9 66.582366943359375
		 10 71.33453369140625 11 73.595588684082031 12 73.707061767578125 13 72.546463012695313
		 14 71.197959899902344 15 71.071098327636719 16 72.705650329589844 17 75.110939025878906
		 18 77.345245361328125 19 78.749565124511719 20 79.263519287109375 21 78.970603942871094
		 22 77.7254638671875 23 75.727615356445313 24 73.840309143066406 25 73.0611572265625
		 26 74.422744750976562 27 76.988677978515625 28 78.882827758789063 29 79.389877319335937
		 30 78.867233276367188 31 77.5177001953125 32 75.558975219726563 33 73.755409240722656
		 34 73.114295959472656 35 74.25628662109375 36 76.429351806640625 37 78.464271545410156
		 38 79.535179138183594 39 79.748687744140625 40 79.744110107421875 41 79.599990844726563
		 42 79.120391845703125 43 78.53155517578125 44 78.433631896972656 45 78.937576293945313
		 46 79.301300048828125 47 78.940071105957031 48 77.902069091796875 49 76.7734375 50 76.240142822265625
		 51 76.837409973144531 52 78.173835754394531 53 79.331108093261719 54 79.766250610351563
		 55 79.848052978515625 56 80.181404113769531 57 80.440788269042969 58 80.109329223632812
		 59 79.388481140136719 60 79.0208740234375 61 79.426284790039063 62 80.256263732910156
		 63 81.013641357421875 64 81.253929138183594 65 80.932624816894531 66 81.015861511230469
		 67 81.097518920898437 68 81.177680969238281 69 71.176918029785156 70 48.514270782470703
		 71 22.920600891113281 72 -3.9356441497802734 73 -26.820472717285156 74 -38.102115631103516
		 75 -40.9942626953125 76 -45.550743103027344 77 -49.980678558349609 78 -50.695560455322266
		 79 -50.982894897460938 80 -52.992496490478516;
createNode animCurveTA -n "Character1_RightForeArm_rotateZ";
	rename -uid "4413073A-4114-CF07-B982-41BAEE203972";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -34.288749694824219 1 -50.817138671875
		 2 -60.984306335449226 3 -67.24713134765625 4 -70.181991577148438 5 -71.241912841796875
		 6 -70.862297058105469 7 -69.684425354003906 8 -68.576126098632813 9 -67.738601684570313
		 10 -66.57000732421875 11 -69.131118774414063 12 -77.522270202636719 13 -86.962966918945313
		 14 -93.962333679199219 15 -97.482078552246094 16 -96.464317321777344 17 -90.739372253417969
		 18 -81.282699584960938 19 -71.412086486816406 20 -67.622222900390625 21 -74.485450744628906
		 22 -86.864227294921875 23 -97.994247436523438 24 -104.97718048095703 25 -107.37652587890625
		 26 -103.49879455566406 27 -92.856376647949219 28 -78.50128173828125 29 -70.697990417480469
		 30 -75.565681457519531 31 -85.618453979492188 32 -94.809043884277344 33 -100.34832000732422
		 34 -101.36318206787109 35 -97.017013549804687 36 -87.150108337402344 37 -71.816818237304688
		 38 -54.379600524902344 39 -42.387397766113281 40 -40.951045989990234 41 -46.974739074707031
		 42 -55.131454467773437 43 -60.307868957519531 44 -58.732933044433587 45 -48.922252655029297
		 46 -33.214527130126953 47 -15.526457786560059 48 -0.82032626867294312 49 8.584986686706543
		 50 12.403182983398438 51 9.6110877990722656 52 0.082919403910636902 53 -14.827210426330568
		 54 -30.221220016479489 55 -38.645362854003906 56 -35.635551452636719 57 -23.949762344360352
		 58 -9.6030063629150391 59 1.1586927175521851 60 5.3802528381347656 61 2.8903815746307373
		 62 -5.017702579498291 63 -17.945175170898437 64 -33.632205963134766 65 -47.480197906494141
		 66 -47.361610412597656 67 -47.242988586425781 68 -47.124271392822266 69 -55.836505889892578
		 70 -66.271781921386719 71 -75.044502258300781 72 -80.017707824707031 73 -78.652107238769531
		 74 -71.645042419433594 75 -64.812156677246094 76 -57.91737365722657 77 -53.091327667236328
		 78 -52.341300964355469 79 -52.502117156982422 80 -52.663631439208984;
createNode animCurveTU -n "Character1_RightForeArm_visibility";
	rename -uid "C27C2E7D-44B3-1E4C-CA3F-9A8171EEB7FF";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHand_translateX";
	rename -uid "CD98D0AA-4FF8-8349-162B-05A1634A63F7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.9317862987518311;
createNode animCurveTL -n "Character1_RightHand_translateY";
	rename -uid "F0B4E17F-4399-BA35-3EC1-A0BFFE3028BD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 16.156974792480469;
createNode animCurveTL -n "Character1_RightHand_translateZ";
	rename -uid "94BF0090-41C2-D5E3-BA6B-7F93DF43EB58";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.4309353828430176;
createNode animCurveTU -n "Character1_RightHand_scaleX";
	rename -uid "8D91E71E-48C7-F9CA-C2A1-2D9BC0E26EBB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightHand_scaleY";
	rename -uid "748AB757-4BAF-13E3-F42F-FC8C0B02DA01";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHand_scaleZ";
	rename -uid "A1924E99-4487-BAF7-C6DA-ED909EE2FCD5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightHand_rotateX";
	rename -uid "D09C3297-4D0E-12D8-0A0E-18A4BA2BE0FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -6.1131048202514648 1 -7.7152023315429687
		 2 -11.928182601928711 3 -17.021011352539063 4 -20.566812515258789 5 -20.706457138061523
		 6 -17.526941299438477 7 -10.477060317993164 8 -0.070547893643379211 9 10.891451835632324
		 10 19.390878677368164 11 24.24882698059082 12 27.415233612060547 13 30.911655426025391
		 14 34.423202514648438 15 37.40948486328125 16 39.274177551269531 17 40.178375244140625
		 18 40.667171478271484 19 41.295585632324219 20 42.605251312255859 21 45.005977630615234
		 22 48.188018798828125 23 51.559776306152344 24 54.337993621826172 25 55.585250854492188
		 26 54.283252716064453 27 51.24786376953125 28 48.162342071533203 29 46.347209930419922
		 30 46.222698211669922 31 47.081600189208984 32 48.392429351806641 33 49.524959564208984
		 34 49.723854064941406 35 48.497413635253906 36 46.341831207275391 37 43.945755004882813
		 38 41.996128082275391 39 41.043239593505859 40 41.431625366210937 41 42.797664642333984
		 42 44.589153289794922 43 46.132610321044922 44 46.602195739746094 45 45.656837463378906
		 46 43.880668640136719 47 41.782299041748047 48 39.79400634765625 49 38.254291534423828
		 50 37.443161010742188 51 37.720714569091797 52 38.923454284667969 53 40.533782958984375
		 54 41.9180908203125 55 42.292926788330078 56 41.124183654785156 57 39.007049560546875
		 58 36.744884490966797 59 34.944843292236328 60 34.055324554443359 61 34.160472869873047
		 62 34.911830902099609 63 36.090480804443359 64 37.457637786865234 65 38.714851379394531
		 66 38.446746826171875 67 38.203128814697266 68 37.985984802246094 69 28.719623565673832
		 70 12.272956848144531 71 1.7979140281677246 72 -2.0521430969238281 73 -2.4039444923400879
		 74 -4.7736148834228516 75 -8.0388355255126953 76 -8.0675239562988281 77 -5.0968027114868164
		 78 -0.91594409942626942 79 3.7889983654022221 80 8.0732011795043945;
createNode animCurveTA -n "Character1_RightHand_rotateY";
	rename -uid "FE34FF60-451D-81DC-5640-19ACC697F5A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 14.262735366821289 1 14.782350540161131
		 2 13.149834632873535 3 11.167256355285645 4 12.587655067443848 5 18.493240356445313
		 6 26.53544807434082 7 34.455623626708984 8 39.694869995117188 9 41.440433502197266
		 10 41.297569274902344 11 42.126323699951172 12 44.609565734863281 13 47.147960662841797
		 14 49.176910400390625 15 50.175800323486328 16 49.825485229492188 17 48.545169830322266
		 18 46.927665710449219 19 45.544036865234375 20 44.950202941894531 21 45.484085083007813
		 22 46.728439331054687 23 48.132080078125 24 49.23675537109375 25 49.667652130126953
		 26 48.927932739257813 27 47.291774749755859 28 45.607177734375 29 44.876655578613281
		 30 45.545177459716797 31 46.984081268310547 32 48.654823303222656 33 50.042133331298828
		 34 50.662281036376953 35 50.18865966796875 36 48.935035705566406 37 47.391925811767578
		 38 46.082927703857422 39 45.576221466064453 40 46.260509490966797 41 47.762157440185547
		 42 49.516407012939453 43 50.988498687744141 44 51.682537078857422 45 51.428073883056641
		 46 50.557315826416016 47 49.343395233154297 48 48.087223052978516 49 47.119472503662109
		 50 46.788784027099609 51 47.511730194091797 52 49.081371307373047 53 50.911220550537109
		 54 52.436931610107422 55 53.124397277832031 56 52.612052917480469 57 51.225872039794922
		 58 49.491798400878906 59 47.989925384521484 60 47.339973449707031 61 47.766887664794922
		 62 48.874797821044922 63 50.356132507324219 64 51.905078887939453 65 53.226226806640625
		 66 53.124500274658203 67 52.998321533203125 68 52.846755981445313 69 53.206966400146484
		 70 48.278911590576172 71 37.181529998779297 72 22.726215362548828 73 6.9664502143859863
		 74 -5.1695990562438965 75 -10.365846633911133 76 -8.8179168701171875 77 -3.3750686645507812
		 78 2.2293558120727539 79 8.3417110443115234 80 14.211385726928711;
createNode animCurveTA -n "Character1_RightHand_rotateZ";
	rename -uid "278DB544-4442-902B-B515-4F9826E58A31";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -35.589801788330078 1 -46.615253448486328
		 2 -58.742053985595703 3 -67.70062255859375 4 -70.3504638671875 5 -66.977783203125
		 6 -60.014152526855476 7 -49.306808471679688 8 -35.306545257568359 9 -20.797155380249023
		 10 -9.3402881622314453 11 -3.1398153305053711 12 0.1388886570930481 13 3.6829681396484371
		 14 7.3137898445129395 15 10.607448577880859 16 13.072616577148437 17 14.802969932556152
		 18 16.193874359130859 19 17.643795013427734 20 19.534624099731445 21 22.177005767822266
		 22 25.362812042236328 23 28.632200241088867 24 31.302120208740234 25 32.525199890136719
		 26 31.431652069091797 27 28.800445556640625 28 26.122177124023438 29 24.45344352722168
		 30 24.079509735107422 31 24.467979431152344 32 25.251657485961914 33 25.961978912353516
		 34 25.997343063354492 35 24.974197387695313 36 23.271675109863281 37 21.403379440307617
		 38 19.910861968994141 39 19.183994293212891 40 19.462566375732422 41 20.500576019287109
		 42 21.909528732299805 43 23.154642105102539 44 23.524456024169922 45 22.705818176269531
		 46 21.192661285400391 47 19.4246826171875 48 17.762392044067383 49 16.466400146484375
		 50 15.735654830932617 51 15.840696334838869 52 16.684186935424805 53 17.893259048461914
		 54 18.952487945556641 55 19.171764373779297 56 18.069429397583008 57 16.169549942016602
		 58 14.17668628692627 59 12.589091300964355 60 11.734523773193359 61 11.657458305358887
		 62 12.096711158752441 63 12.90078067779541 64 13.889220237731934 65 14.809985160827638
		 66 14.423219680786133 67 14.053337097167969 68 13.702243804931641 69 1.2733047008514404
		 70 -19.93519401550293 71 -33.38507080078125 72 -38.516616821289063 73 -34.407695770263672
		 74 -24.029041290283203 75 -13.606686592102051 76 -12.143525123596191 77 -13.635595321655273
		 78 -11.712830543518066 79 -8.6726369857788086 80 -5.1461181640625;
createNode animCurveTU -n "Character1_RightHand_visibility";
	rename -uid "90DDC83B-40A1-1E3C-E0E4-E48D4D21771C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb1_translateX";
	rename -uid "19ADA97F-462D-87C6-F68D-6EA37DB74BDA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2155418395996094;
createNode animCurveTL -n "Character1_RightHandThumb1_translateY";
	rename -uid "A00B2AE2-4192-3646-D3BF-DC9FCD595DEE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8998895883560181;
createNode animCurveTL -n "Character1_RightHandThumb1_translateZ";
	rename -uid "398D015B-4FBD-5037-4CCE-3183FD282409";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3098368644714355;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleX";
	rename -uid "A02EA419-47E4-9E3A-7F17-06A56EB24000";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleY";
	rename -uid "5E06A9A9-4935-CD18-5B35-2A9886A3AE58";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleZ";
	rename -uid "40F6516A-4B85-C50A-DBF7-9E89CA4D7403";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateX";
	rename -uid "3E6C19AC-407B-71F4-B5AE-999718CC626B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -7.8129382133483887 1 -5.073634147644043
		 2 -3.328951358795166 3 -2.7197971343994141 4 -2.7197971343994141 5 -2.7197971343994141
		 6 -2.7197971343994141 7 -4.6713194847106934 8 -10.50279712677002 9 -21.081428527832031
		 10 -35.111038208007813 11 -45.797332763671875 12 -48.595165252685547 13 -47.212966918945313
		 14 -46.187675476074219 15 -46.053787231445313 16 -47.398693084716797 17 -49.8546142578125
		 18 -52.537982940673828 19 -54.586257934570313 20 -55.142181396484375 21 -53.649063110351562
		 22 -50.675071716308594 23 -47.039310455322266 24 -43.569309234619141 25 -41.133792877197266
		 26 -40.18255615234375 27 -40.205055236816406 28 -40.656120300292969 29 -40.988670349121094
		 30 -40.922672271728516 31 -40.719032287597656 32 -40.645771026611328 33 -40.971073150634766
		 34 -41.959297180175781 35 -43.974723815917969 36 -46.817611694335938 37 -49.919834136962891
		 38 -52.738033294677734 39 -54.738681793212891 40 -55.796016693115234 41 -56.276237487792969
		 42 -56.381473541259766 43 -56.313426971435547 44 -56.273735046386719 45 -56.195404052734375
		 46 -55.943550109863281 47 -55.617984771728516 48 -55.318546295166016 49 -55.145153045654297
		 50 -55.197746276855469 51 -55.894996643066406 52 -57.18084716796875 53 -58.444183349609375
		 54 -59.074253082275391 55 -58.459743499755852 56 -55.830368041992188 57 -51.58355712890625
		 58 -46.843700408935547 59 -42.754295349121094 60 -40.539531707763672 61 -40.715797424316406
		 62 -42.479133605957031 63 -45.120735168457031 64 -47.954936981201172 65 -50.326034545898438
		 66 -50.326034545898438 67 -50.326034545898438 68 -50.326034545898438 69 -49.859828948974609
		 70 -48.502525329589844 71 -46.281219482421875 72 -43.212440490722656 73 -39.347503662109375
		 74 -34.811859130859375 75 -29.823404312133786 76 -24.67396354675293 77 -19.67479133605957
		 78 -15.092275619506836 79 -11.107389450073242 80 -7.8129382133483887;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateY";
	rename -uid "C56FFCC9-4886-6380-8CF4-94B5EBD5B242";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 24.506990432739258 1 12.43733024597168
		 2 -0.40152126550674438 3 -6.4185709953308105 4 -6.4185709953308105 5 -6.4185709953308105
		 6 -6.4185709953308105 7 -1.3125717639923096 8 10.465705871582031 9 22.467977523803711
		 10 28.584684371948242 11 28.577653884887695 12 28.036931991577148 13 29.001615524291992
		 14 29.71904182434082 15 29.812810897827148 16 28.871801376342773 17 27.161376953125
		 18 25.311313629150391 19 23.917570114135742 20 23.542600631713867 21 24.553009033203125
		 22 26.593242645263672 23 29.1230354309082 24 31.554441452026367 25 33.259727478027344
		 26 33.923667907714844 27 33.907985687255859 28 33.593334197998047 29 33.361122131347656
		 30 33.407222747802734 31 33.549419403076172 32 33.600555419921875 33 33.3734130859375
		 34 32.682399749755859 35 31.270187377929688 36 29.278106689453121 37 27.11614990234375
		 38 25.174411773681641 39 23.814609527587891 40 23.103580474853516 41 22.782562255859375
		 42 22.712381362915039 43 22.757755279541016 44 22.784233093261719 45 22.836511611938477
		 46 23.004823684692383 47 23.222900390625 48 23.423961639404297 49 23.540599822998047
		 50 23.505205154418945 51 23.037311553955078 52 22.181308746337891 53 21.34965705871582
		 54 20.938591003417969 55 21.3394775390625 56 23.080577850341797 57 25.966575622558594
		 58 29.259855270385739 59 32.125675201416016 60 33.674697875976563 61 33.551673889160156
		 62 32.318431854248047 63 30.466657638549805 64 28.483348846435547 65 26.834701538085938
		 66 26.834701538085938 67 26.834701538085938 68 26.834701538085938 69 27.151458740234375
		 70 28.00358772277832 71 29.196329116821289 72 30.491153717041019 73 31.631471633911133
		 74 32.374351501464844 75 32.528652191162109 76 31.991584777832031 77 30.770179748535156
		 78 28.978046417236328 79 26.809963226318359 80 24.506990432739258;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateZ";
	rename -uid "54499D3B-40B6-00AD-A95B-9AA43A8F6508";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 45.780338287353516 1 52.329460144042969
		 2 56.874759674072266 3 58.378379821777337 4 58.378379821777337 5 58.378379821777337
		 6 58.378379821777337 7 56.27459716796875 8 48.894401550292969 9 34.348236083984375
		 10 14.471496582031248 11 -2.6700291633605957 12 -9.7056255340576172 13 -10.058706283569336
		 14 -10.304473876953125 15 -10.335539817810059 16 -10.01270580291748 17 -9.3625736236572266
		 18 -8.5659217834472656 19 -7.8998079299926749 20 -7.7106518745422354 21 -8.2106685638427734
		 22 -9.1283721923828125 23 -10.10130786895752 24 -10.868136405944824 25 -11.307602882385254
		 26 -11.456503868103027 27 -11.453131675720215 28 -11.383988380432129 29 -11.33115291595459
		 30 -11.341763496398926 31 -11.374114036560059 32 -11.38560676574707 33 -11.333988189697266
		 34 -11.16796875 35 -10.786969184875488 36 -10.155118942260742 37 -9.3442659378051758
		 38 -8.5030384063720703 39 -7.8482956886291504 40 -7.4837150573730469 41 -7.3139977455139169
		 42 -7.2764654159545907 43 -7.3007493019103995 44 -7.3148903846740723 45 -7.3427457809448251
		 46 -7.4318475723266593 47 -7.5459837913513184 48 -7.6499123573303214 49 -7.7096328735351571
		 50 -7.6915545463562021 51 -7.448944091796875 52 -6.9874086380004883 53 -6.5165486335754395
		 54 -6.2754225730895996 55 -6.5106449127197266 56 -7.4716596603393546 57 -8.8593463897705078
		 58 -10.148818969726562 59 -11.024430274963379 60 -11.402137756347656 61 -11.374621391296387
		 62 -11.075111389160156 63 -10.545367240905762 64 -9.8722476959228516 65 -9.229029655456543
		 66 -9.229029655456543 67 -9.229029655456543 68 -9.229029655456543 69 -8.5347604751586914
		 70 -6.5433297157287598 71 -3.366356372833252 72 0.89030218124389648 73 6.0888481140136719
		 74 12.025081634521484 75 18.412576675415039 76 24.902124404907227 77 31.136346817016602
		 78 36.813976287841797 79 41.730403900146484 80 45.780338287353516;
createNode animCurveTU -n "Character1_RightHandThumb1_visibility";
	rename -uid "0849FAB3-4A13-9904-C1DC-91BB65D20A5D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb2_translateX";
	rename -uid "DBACA698-47E1-EBF9-9103-6987ED0CF959";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9627087116241455;
createNode animCurveTL -n "Character1_RightHandThumb2_translateY";
	rename -uid "1068839C-49A7-3F2E-411B-07BC495E3B7C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5399179458618164;
createNode animCurveTL -n "Character1_RightHandThumb2_translateZ";
	rename -uid "CBFC72A6-4C35-74E8-680F-F5B6FB5500B8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6609530448913574;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleX";
	rename -uid "6C428309-4A80-FDAF-4DEE-CBA3E22D93DE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleY";
	rename -uid "089E5A30-4FE2-9E8C-E190-1F8D32D611F6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleZ";
	rename -uid "C9F03C4D-4ACE-5BF7-8A4C-B09DBF8D63DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateX";
	rename -uid "160EABA5-4466-F92C-05DE-AF8BFF647A54";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 5.4960494041442871 1 2.3524806499481201
		 2 1.3683763742446899 3 1.6604856252670288 4 1.6604856252670288 5 1.6604856252670288
		 6 1.6604856252670288 7 1.848296523094177 8 3.4805870056152344 9 8.3600931167602539
		 10 16.823616027832031 11 25.405847549438477 12 28.455747604370117 13 27.024139404296875
		 14 25.954425811767578 15 25.814239501953125 16 27.217197418212891 17 29.750034332275394
		 18 32.476467132568359 19 34.530261993408203 20 35.083744049072266 21 33.593406677246094
		 22 30.588088989257809 23 26.843427658081055 24 23.191362380981445 25 20.578876495361328
		 26 19.546855926513672 27 19.571344375610352 28 20.061481475830078 29 20.421863555908203
		 30 20.350406646728516 31 20.129718780517578 32 20.050251007080078 33 20.402812957763672
		 34 21.469100952148437 35 23.622198104858398 36 26.612449645996094 37 29.816795349121094
		 38 32.678081512451172 39 34.682186126708984 40 35.732608795166016 41 36.207736968994141
		 42 36.311695098876953 43 36.244480133056641 44 36.20526123046875 45 36.127841949462891
		 46 35.878704071044922 47 35.556148529052734 48 35.258991241455078 49 35.086696624755859
		 50 35.138973236083984 51 35.83062744140625 52 37.099498748779297 53 38.337902069091797
		 54 38.952541351318359 55 38.353103637695313 56 35.766632080078125 57 31.511480331420895
		 58 26.6396484375 59 22.321807861328125 60 19.9349365234375 61 20.126213073730469
		 62 22.027181625366211 63 24.834037780761719 64 27.794107437133789 65 30.232049942016602
		 66 30.232049942016602 67 30.232049942016602 68 30.232049942016602 69 28.924154281616211
		 70 25.389659881591797 71 20.39177131652832 72 14.822804450988771 73 9.5246458053588867
		 74 5.1305971145629883 75 2.0098111629486084 76 0.30494087934494019 77 -0.0041747903451323509
		 78 0.94147795438766479 79 2.8930251598358154 80 5.4960494041442871;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateY";
	rename -uid "255E8751-49B4-E7CC-8F34-B0A5DC97BCEC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -2.6941189765930176 1 6.5796351432800293
		 2 16.739946365356445 3 21.524660110473633 4 21.524660110473633 5 21.524660110473633
		 6 21.524660110473633 7 24.084165573120117 8 30.491281509399414 9 38.468524932861328
		 10 45.426723480224609 11 49.905319213867188 12 51.874473571777344 13 52.640125274658203
		 14 53.208755493164062 15 53.283027648925781 16 52.537166595458984 17 51.178504943847656
		 18 49.704421997070313 19 48.590560913085938 20 48.290355682373047 21 49.098770141601563
		 22 50.726341247558594 23 52.736408233642578 24 54.660507202148438 25 56.005485534667969
		 26 56.528102874755859 27 56.515762329101563 28 56.268154144287109 29 56.085334777832031
		 30 56.121631622314453 31 56.233585357666016 32 56.273838043212891 33 56.095012664794922
		 34 55.550563812255859 35 54.435951232910156 36 52.859344482421875 37 51.142520904541016
		 38 49.595138549804688 39 48.508152008056641 40 47.938571929931641 41 47.681129455566406
		 42 47.624820709228516 43 47.661228179931641 44 47.682468414306641 45 47.724407196044922
		 46 47.859394073486328 47 48.034217834472656 48 48.195323944091797 49 48.288753509521484
		 50 48.260406494140625 51 47.885440826416016 52 47.198444366455078 53 46.529666900634766
		 54 46.198589324951172 55 46.521469116210938 56 47.920127868652344 57 50.227073669433594
		 58 52.844879150390625 59 55.111465454101563 60 56.332195281982422 61 56.235363006591797
		 62 55.263542175292969 63 53.800613403320312 64 52.228935241699219 65 50.918567657470703
		 66 50.918567657470703 67 50.918567657470703 68 50.918567657470703 69 50.494312286376953
		 70 49.191753387451172 71 46.886280059814453 72 43.452556610107422 73 38.870361328125
		 74 33.271903991699219 75 26.929439544677734 76 20.209905624389648 77 13.522018432617188
		 78 7.2653241157531747 79 1.7808890342712402 80 -2.6941189765930176;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateZ";
	rename -uid "6791F691-45AF-CB09-3D84-5B82154BBA6A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 0.59695249795913696 1 8.6022615432739258
		 2 15.688950538635256 3 18.76649284362793 4 18.76649284362793 5 18.76649284362793
		 6 18.76649284362793 7 21.172796249389648 8 27.336637496948242 9 36.091922760009766
		 10 46.347240447998047 11 55.645301818847656 12 59.956291198730469 13 60.265613555908196
		 14 60.477134704589837 15 60.503597259521484 16 60.225643157958984 17 59.651241302490227
		 18 58.93196105957032 19 58.323810577392578 20 58.150485992431641 21 58.608123779296875
		 22 59.441089630126953 23 60.302532196044929 24 60.944118499755859 25 61.277931213378899
		 26 61.380390167236328 27 61.378150939941413 28 61.331394195556641 29 61.294601440429695
		 30 61.302059173583991 31 61.3245849609375 32 61.332508087158196 33 61.296596527099602
		 34 61.176109313964851 35 60.878932952880852 36 60.349021911621101 37 59.634864807128906
		 38 58.874744415283203 39 58.276634216308594 40 57.942314147949226 41 57.7864990234375
		 42 57.752029418945313 43 57.774330139160163 44 57.787319183349602 45 57.812896728515625
		 46 57.894702911376953 47 57.999454498291009 48 58.094787597656243 49 58.149551391601563
		 50 58.132972717285163 51 57.910392761230469 52 57.486446380615241 53 57.053630828857422
		 54 56.831993103027344 55 57.048206329345703 56 57.931247711181634 57 59.198234558105462
		 58 60.343589782714844 59 61.066989898681634 60 61.343830108642578 61 61.324939727783203
		 62 61.105972290039063 63 60.68040466308593 64 60.102951049804695 65 59.531574249267578
		 66 59.531574249267578 67 59.531574249267578 68 59.531574249267578 69 58.375129699707024
		 70 55.196498870849609 71 50.537593841552734 72 45.011936187744141 73 39.166362762451172
		 74 33.354469299316406 75 27.707046508789063 76 22.19660758972168 77 16.736349105834961
		 78 11.269411087036133 79 5.8337092399597168 80 0.59695249795913696;
createNode animCurveTU -n "Character1_RightHandThumb2_visibility";
	rename -uid "93441F7D-4B24-BC49-B899-1B98A841A51B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb3_translateX";
	rename -uid "8C1CC577-4080-03C9-D28B-6896CE4241DD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.5867142677307129;
createNode animCurveTL -n "Character1_RightHandThumb3_translateY";
	rename -uid "0510753A-4E79-F63C-6B87-12995DB5739D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.6152248382568359;
createNode animCurveTL -n "Character1_RightHandThumb3_translateZ";
	rename -uid "9CAD44A5-4F3C-AAEB-7C90-33A73ECD9C34";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.4791715145111084;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleX";
	rename -uid "5E4FD0C5-4BC7-97DE-EB4B-BFA33BEB267F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleY";
	rename -uid "DE778F2E-4357-378A-474A-EE8B0F4C9E38";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleZ";
	rename -uid "48F2267C-43D6-BB6A-0578-8B9E10B2238D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateX";
	rename -uid "63CF27B2-41DF-1A0A-7236-7386979F3667";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -3.1300626801566978e-007 10 -2.4694853095752478e-007
		 11 -0.023964159190654755 12 -0.064434126019477844 13 -0.084434576332569122 14 -0.081394463777542114
		 15 -0.07987966388463974 16 -0.083360657095909119 17 -0.02191641554236412 18 0.14692680537700653
		 19 0.3486512303352356 20 0.41445264220237732 21 0.2484399825334549 22 0.018343886360526085
		 23 -0.084987051784992218 24 -0.0052682613022625446 25 0.15266701579093933 26 0.23688593506813049
		 27 0.23474873602390287 28 0.19338494539260864 29 0.16470152139663696 30 0.17027145624160767
		 31 0.18784074485301971 32 0.19430232048034668 33 0.16618078947067261 34 0.089806817471981049
		 35 -0.023441536352038383 36 -0.085058934986591339 37 -0.019080011174082756 38 0.16382682323455811
		 39 0.3662201464176178 40 0.49794444441795355 41 0.56349140405654907 42 0.57833528518676758
		 43 0.56871706247329712 44 0.56314009428024292 45 0.55220645666122437 46 0.51769983768463135
		 47 0.47455468773841858 48 0.43632248044013977 49 0.41481703519821167 50 0.42129102349281311
		 51 0.51116085052490234 52 0.69675326347351074 53 0.90447276830673207 54 1.017586350440979
		 55 0.90718907117843628 56 0.50251394510269165 57 0.074573315680027008 58 -0.085087321698665619
		 59 0.03829776868224144 60 0.20380569994449615 61 0.18812437355518341 62 0.055120740085840225
		 63 -0.062184281647205353 64 -0.077151790261268616 65 -1.6851716111432324e-007 66 -1.6849953965447639e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateY";
	rename -uid "E0B5F239-45A0-8932-BC4D-319B12CEF715";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -7.9032594157979474e-007 10 -7.9571856304028188e-007
		 11 -0.58099871873855591 12 -1.9336801767349241 13 -3.471869945526123 14 -4.6084690093994141
		 15 -4.7565765380859375 16 -3.2655529975891113 17 -0.52732861042022705 18 2.478795051574707
		 19 4.7769260406494141 20 5.4004769325256348 21 3.7254040241241451 22 0.3907027542591095
		 23 -3.664664506912231 24 -7.4894738197326651 25 -10.13426399230957 26 -11.156253814697266
		 27 -11.132158279418945 28 -10.648295402526855 29 -10.290608406066895 30 -10.36165714263916
		 31 -10.580692291259766 32 -10.659414291381836 33 -10.309556007385254 34 -9.2421417236328125
		 35 -7.0457038879394531 36 -3.9106316566467281 37 -0.454405426979065 38 2.7032105922698975
		 39 4.947913646697998 40 6.133514404296875 41 6.6715927124023437 42 6.789463996887207
		 43 6.7132468223571777 44 6.6687860488891602 45 6.5810394287109375 46 6.298853874206543
		 47 5.9339585304260254 48 5.5982475280761719 49 5.4038090705871582 50 5.4627885818481445
		 51 6.2444424629211426 52 7.6842422485351562 53 9.0957517623901367 54 9.7982511520385742
		 55 9.1131105422973633 56 6.1720132827758789 57 1.4085125923156738 58 -3.8816955089569092
		 59 -8.3787059783935547 60 -10.77350902557373 61 -10.58416748046875 62 -8.6780109405517578
		 63 -5.7865128517150879 64 -2.6469292640686035 65 -8.147426342475228e-007 66 -8.1423183928563958e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateZ";
	rename -uid "94BC697F-4EBC-4809-A053-61A9FC62D10A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 1.8286213787632732e-007 10 2.0828531432925956e-007
		 11 -0.39001679420471191 12 -1.2971270084381104 13 -2.3283486366271973 14 -3.091163158416748
		 15 -3.1906604766845703 16 -2.1899876594543457 17 -0.35400205850601196 18 1.669488787651062
		 19 3.2302775382995605 20 3.6567766666412354 21 2.5141582489013672 22 0.26248353719711304
		 23 -2.4576647281646729 24 -5.0332775115966797 25 -6.8353948593139648 26 -7.538893222808837
		 27 -7.5222535133361816 28 -7.1886610984802246 29 -6.942723274230957 30 -6.9915318489074707
		 31 -7.1421360969543457 32 -7.196314811706543 33 -6.9557375907897949 34 -6.2248163223266602
		 35 -4.732996940612793 36 -2.6226847171783447 37 -0.30506375432014465 38 1.8212432861328127
		 39 3.3470854759216309 40 4.1601314544677734 41 4.5310873985290527 42 4.612525463104248
		 43 4.5598592758178711 44 4.529149055480957 45 4.4685673713684082 46 4.2739801406860352
		 47 4.0228805541992187 48 3.7923619747161861 49 3.6590595245361328 50 3.6994786262512207
		 51 4.2365007400512695 52 5.2329344749450684 53 6.2202987670898437 54 6.7161011695861816
		 55 6.2325129508972168 56 4.1866302490234375 57 0.94729828834533691 58 -2.6032688617706299
		 59 -5.6366114616394043 60 -7.2748861312866211 61 -7.1445274353027344 62 -5.8402261734008789
		 63 -3.8834145069122319 64 -1.7752351760864258 65 2.2758200657335689e-007 66 2.2735513027782872e-007;
createNode animCurveTU -n "Character1_RightHandThumb3_visibility";
	rename -uid "7965511B-435A-9B31-33F4-9B8B4F0DBE5B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex1_translateX";
	rename -uid "CC3495FB-4BDC-B375-D1EE-118B3B5EAA6D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.615594863891602;
createNode animCurveTL -n "Character1_RightHandIndex1_translateY";
	rename -uid "942D737D-4FC1-B312-B7CB-12BB827280CC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4688365459442139;
createNode animCurveTL -n "Character1_RightHandIndex1_translateZ";
	rename -uid "3FFD7D60-49BF-DE66-E7FA-1D9805A11645";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.3266587257385254;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleX";
	rename -uid "F784172E-4A8C-8F7B-17DB-23921E0CF529";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleY";
	rename -uid "83786424-4D3F-6ACD-FBDE-0EBEF8FD68DF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleZ";
	rename -uid "8D29CC38-4DD4-632B-80B7-D8B3CC0B32AA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateX";
	rename -uid "FA149ACA-4315-277A-3B58-D4AF841DA499";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 3.6655156612396245 1 12.18766975402832
		 2 27.956512451171875 3 41.524635314941406 4 41.524635314941406 5 41.524635314941406
		 6 41.524635314941406 7 32.066398620605469 8 18.775577545166016 9 10.631868362426758
		 10 5.654517650604248 11 2.199756383895874 12 0.86860978603363037 13 1.2718888521194458
		 14 1.57056725025177 15 1.6095511913299561 16 1.2177495956420898 17 0.50016164779663086
		 18 -0.28986355662345886 19 -0.89980089664459229 20 -1.066725492477417 21 -0.61980718374252319
		 22 0.25944572687149048 23 1.3224982023239136 24 2.3327267169952393 25 3.0424702167510986
		 26 3.3202285766601562 27 3.3136539459228516 28 3.1818993091583252 29 3.0848207473754883
		 30 3.1040830612182617 31 3.1635310649871826 32 3.1849217414855957 33 3.0899572372436523
		 34 2.8017160892486572 35 2.2147088050842285 36 1.3870949745178223 37 0.48104998469352728
		 38 -0.34912455081939697 39 -0.94550359249114979 40 -1.2639392614364624 41 -1.4094512462615967
		 42 -1.4414180517196655 43 -1.4207439422607422 44 -1.4086904525756836 45 -1.3849157094955444
		 46 -1.3085812330245972 47 -1.2101398706436157 48 -1.1198244094848633 49 -1.0676202774047852
		 50 -1.0834476947784424 51 -1.2938838005065918 52 -1.685242772102356 53 -2.0745587348937988
		 54 -2.2707726955413818 55 -2.0793864727020264 56 -1.2743293046951294 57 -0.0079020047560334206
		 58 1.3794933557510376 59 2.5700607299804687 60 3.2159461975097656 61 3.1644749641418457
		 62 2.6502206325531006 63 1.8811577558517456 64 1.055519700050354 65 0.36192771792411804
		 66 0.36192771792411804 67 0.36192771792411804 68 0.36192771792411804 69 0.43533051013946533
		 70 0.63268363475799561 71 0.91252601146697998 72 1.233971118927002 73 1.5655159950256348
		 74 1.8885145187377932 75 2.1964256763458252 76 2.4916360378265381 77 2.7812333106994629
		 78 3.0725412368774414 79 3.3688178062438965 80 3.6655156612396245;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateY";
	rename -uid "FAFAD4CB-41FE-362E-D544-EE96CA64FBE6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -12.402667999267578 1 -34.271701812744141
		 2 -54.702136993408203 3 -62.593696594238281 4 -62.593696594238281 5 -62.593696594238281
		 6 -62.593696594238281 7 -57.071453094482422 8 -40.990436553955078 9 -18.258790969848633
		 10 5.1180458068847656 11 22.54218864440918 12 28.201686859130859 13 26.427255630493164
		 14 25.117069244384766 15 24.946390151977539 16 26.665163040161133 17 29.825735092163086
		 18 33.304439544677734 19 35.972431182861328 20 36.697883605957031 21 34.750617980957031
		 22 30.886936187744137 23 26.204963684082031 24 21.798349380493164 25 18.75245475769043
		 26 17.575099945068359 27 17.602863311767578 28 18.160324096679687 29 18.572364807128906
		 30 18.490522384643555 31 18.238203048706055 32 18.147514343261719 33 18.550539016723633
		 34 19.779960632324219 35 22.309412002563477 36 25.921398162841797 37 29.90999794006348
		 38 33.564609527587891 39 36.171291351318359 40 37.551651000976562 41 38.179031372070313
		 42 38.316543579101563 43 38.227622985839844 44 38.175758361816406 45 38.073406219482422
		 46 37.744369506835938 47 37.319126129150391 48 36.928127288818359 49 36.701763153076172
		 50 36.770420074462891 51 37.680942535400391 52 39.361396789550781 53 41.01336669921875
		 54 41.837368011474609 55 41.033714294433594 56 37.596523284912109 57 32.064609527587891
		 58 25.954755783081055 59 20.774314880371094 60 18.016073226928711 61 18.234201431274414
		 62 20.429634094238281 63 23.759748458862305 64 27.378679275512695 65 30.435190200805664
		 66 30.435190200805664 67 30.435190200805664 68 30.435190200805664 69 29.865589141845703
		 70 28.255039215087891 71 25.750453948974609 72 22.499095916748047 73 18.649370193481445
		 74 14.351036071777344 75 9.754969596862793 76 5.0126490592956543 77 0.27554675936698914
		 78 -4.3054971694946289 79 -8.5808696746826172 80 -12.402667999267578;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateZ";
	rename -uid "9286970A-4A51-44F2-4C19-EA98A2545A82";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -1.4665682315826416 1 -6.8461599349975586
		 2 -20.044975280761719 3 -32.544338226318359 4 -32.544338226318359 5 -32.544338226318359
		 6 -32.544338226318359 7 -23.153766632080078 8 -10.206391334533691 9 -3.1083276271820068
		 10 -0.45359724760055542 11 -0.48224329948425293 12 -1.1599210500717163 13 -1.36824631690979
		 14 -1.5290306806564331 15 -1.5504105091094971 16 -1.3396841287612915 17 -0.97883176803588867
		 18 -0.62238574028015137 19 -0.37913328409194946 20 -0.31770998239517212 21 -0.48719111084938049
		 22 -0.86550527811050415 23 -1.3951096534729004 24 -1.9627124071121218 25 -2.3943443298339844
		 26 -2.5699529647827148 27 -2.5657548904418945 28 -2.4820456504821777 29 -2.4208858013153076
		 30 -2.432985782623291 31 -2.4704394340515137 32 -2.4839565753936768 33 -2.4241101741790771
		 34 -2.2451040744781494 35 -1.8934551477432249 36 -1.4296249151229858 37 -0.9696880578994751
		 38 -0.59749025106430054 39 -0.36209124326705933 40 -0.24809327721595761 41 -0.19881261885166168
		 42 -0.18822659552097321 43 -0.19506305456161499 44 -0.19906561076641083 45 -0.20699669420719147
		 46 -0.23278462886810303 47 -0.26676428318023682 48 -0.29864960908889771 49 -0.31738752126693726
		 50 -0.31168285012245178 51 -0.23780643939971921 52 -0.11036814749240875 53 0.0031565038952976465
		 54 0.055221103131771088 55 0.004479261115193367 56 -0.24451588094234464 57 -0.74443286657333374
		 58 -1.4255504608154297 59 -2.1042106151580811 60 -2.5036001205444336 61 -2.4710357189178467
		 62 -2.1526579856872559 63 -1.7018202543258667 64 -1.2551926374435425 65 -0.91326099634170543
		 66 -0.91326099634170543 67 -0.91326099634170543 68 -0.91326099634170543 69 -0.85123628377914429
		 70 -0.68958264589309692 71 -0.47613295912742615 72 -0.26221218705177307 73 -0.092477709054946899
		 74 -8.3656414062716067e-005 75 -0.0057262424379587173 76 -0.11854096502065659 77 -0.33726084232330322
		 78 -0.65077263116836548 79 -1.0378719568252563 80 -1.4665682315826416;
createNode animCurveTU -n "Character1_RightHandIndex1_visibility";
	rename -uid "31942FAF-4215-AE28-DAA0-82AFA3128538";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex2_translateX";
	rename -uid "1655C6FC-45E5-9AC0-CA5C-7A8B04C4B15A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.10917675495147705;
createNode animCurveTL -n "Character1_RightHandIndex2_translateY";
	rename -uid "519A9F4B-406F-8449-478E-EBA9D8B8EF2D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.54082572460174561;
createNode animCurveTL -n "Character1_RightHandIndex2_translateZ";
	rename -uid "E6442ADE-4B27-D795-627B-1D97BEA0755B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.6861534118652344;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleX";
	rename -uid "E308C1DF-43C6-8BE9-6E2E-2BA3A67606AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleY";
	rename -uid "5575F5ED-4E6E-DA3C-21B5-FC9A641B52B1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleZ";
	rename -uid "A896330A-4C5E-B0E3-9B4A-44B5DC50CF56";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateX";
	rename -uid "F5C4432A-4618-7273-C3CB-5DB990DC3E4B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 16.027473449707031 1 44.743507385253906
		 2 81.408409118652344 3 96.6031494140625 4 96.6031494140625 5 96.6031494140625 6 96.6031494140625
		 7 95.357734680175781 8 92.174369812011719 9 87.903800964355469 10 83.51348876953125
		 11 80.827537536621094 12 81.185867309570313 13 83.139694213867188 14 84.566024780273437
		 15 84.750755310058594 16 82.879173278808594 17 79.377120971679688 18 75.446273803710938
		 19 72.390235900878906 20 71.554634094238281 21 73.793411254882813 22 78.185592651367188
		 23 83.3826904296875 24 88.1109619140625 25 91.271324157714844 26 92.467536926269531
		 27 92.439498901367188 28 91.874748229980469 29 91.455238342285156 30 91.538703918457031
		 31 91.79559326171875 32 91.887763977050781 33 91.477500915527344 34 90.215644836425781
		 35 87.571731567382812 36 83.692100524902344 37 79.28277587890625 38 75.149642944335937
		 39 72.161354064941406 40 70.569259643554687 41 69.844009399414063 42 69.684928894042969
		 43 69.787803649902344 44 69.847793579101563 45 69.966171264648438 46 70.346572875976562
		 47 70.837821960449219 48 71.289100646972656 49 71.5501708984375 50 71.471000671386719
		 51 70.419876098632813 52 68.475090026855469 53 66.559547424316406 54 65.60357666015625
		 55 66.53594970703125 56 70.517417907714844 57 76.855201721191406 58 83.655731201171875
		 59 89.183822631835938 60 92.021202087402344 61 91.799667358398438 62 89.542617797851563
		 63 86.028038024902344 64 82.095123291015625 65 78.693702697753906 66 78.693702697753906
		 67 78.693702697753906 68 78.693702697753906 69 77.78729248046875 70 75.203521728515625
		 71 71.134262084960938 72 65.791145324707031 73 59.444435119628899 74 52.439792633056641
		 75 45.176704406738281 76 38.051357269287109 77 31.393474578857425 78 25.430408477783203
		 79 20.289745330810547 80 16.027473449707031;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateY";
	rename -uid "C485682F-4A52-A201-BC61-90A41E1A49AA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 18.286495208740234 1 34.011634826660156
		 2 35.206295013427734 3 30.306468963623047 4 30.306468963623047 5 30.306468963623047
		 6 30.306468963623047 7 30.678831100463864 8 31.50299072265625 9 32.338665008544922
		 10 32.898422241210938 11 32.985504150390625 12 32.698284149169922 13 32.253086090087891
		 14 31.89179801940918 15 31.842723846435547 16 32.315738677978516 17 33.060459136962891
		 18 33.687179565429687 19 34.027179718017578 20 34.098201751708984 21 33.886802673339844
		 22 33.273365020751953 23 32.193721771240234 24 30.85623931884766 25 29.75935173034668
		 26 29.299251556396484 27 29.310327529907227 28 29.530418395996094 29 29.690254211425778
		 30 29.658699035644528 31 29.560813903808594 32 29.525409698486332 33 29.681848526000977
		 34 30.144618988037109 35 31.026742935180664 36 32.116840362548828 37 33.078056335449219
		 38 33.725753784179688 39 34.047565460205078 40 34.169971466064453 41 34.214542388916016
		 42 34.223384857177734 43 34.217704772949219 44 34.214328765869141 45 34.207523345947266
		 46 34.184398651123047 47 34.151691436767578 48 34.118816375732422 49 34.098556518554688
		 50 34.10479736328125 51 34.179721832275391 52 34.279693603515625 53 34.329368591308594
		 54 34.336109161376953 55 34.329681396484375 56 34.173389434814453 57 33.487392425537109
		 58 32.125953674316406 59 30.50275993347168 60 29.4738883972168 61 29.559251785278317
		 62 30.380266189575195 63 31.488880157470707 64 32.498161315917969 65 33.185043334960938
		 66 33.185043334960938 67 33.185043334960938 68 33.185043334960938 69 33.358230590820313
		 70 33.792892456054687 71 34.303909301757812 72 34.660091400146484 73 34.624721527099609
		 74 33.999996185302734 75 32.668788909912109 76 30.620649337768551 77 27.952444076538086
		 78 24.846389770507812 79 21.538009643554688 80 18.286495208740234;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateZ";
	rename -uid "08D9AB3A-484F-735E-5B5C-D0ABE7FE29BF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 11.694750785827637 1 36.111175537109375
		 2 68.851234436035156 3 82.211105346679688 4 82.211105346679688 5 82.211105346679688
		 6 82.211105346679688 7 81.026145935058594 8 78.012718200683594 9 74.003662109375
		 10 69.921585083007813 11 67.43389892578125 12 67.746856689453125 13 69.514930725097656
		 14 70.803474426269531 15 70.97021484375 16 69.279365539550781 17 66.10736083984375
		 18 62.53697586059571 19 59.755886077880859 20 58.994873046875 21 61.03327941894532
		 22 65.026077270507813 23 69.734588623046875 24 73.996688842773438 25 76.830268859863281
		 26 77.899002075195313 27 77.873977661132813 28 77.369667053222656 29 76.994720458984375
		 30 77.069343566894531 31 77.298942565917969 32 77.381294250488281 33 77.014633178710938
		 34 75.885284423828125 35 73.511894226074219 36 70.014198303222656 37 66.021781921386719
		 38 62.267200469970696 39 59.547458648681641 40 58.097206115722656 41 57.436355590820313
		 42 57.291385650634766 43 57.385131835937507 44 57.439804077148438 45 57.547676086425774
		 46 57.894306182861328 47 58.341888427734375 48 58.753002166748047 49 58.990802764892571
		 50 58.918693542480469 51 57.961090087890625 52 56.188716888427734 53 54.442485809326172
		 54 53.570919036865234 55 54.420967102050781 56 58.049968719482422 57 63.817722320556641
		 58 69.981338500976563 59 74.960136413574219 60 77.500495910644531 61 77.302581787109375
		 62 75.282005310058594 63 72.12213134765625 64 68.570075988769531 65 65.487289428710938
		 66 65.487289428710938 67 65.487289428710938 68 65.487289428710938 69 64.716697692871094
		 70 62.512668609619134 71 59.022018432617188 72 54.410243988037109 73 48.903816223144531
		 74 42.809135437011719 75 36.491695404052734 76 30.318700790405277 77 24.594799041748047
		 78 19.524637222290039 79 15.213700294494629 80 11.694750785827637;
createNode animCurveTU -n "Character1_RightHandIndex2_visibility";
	rename -uid "16FEFC04-49CB-70B7-C713-5CAD95063044";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex3_translateX";
	rename -uid "8A485455-4DDF-04DA-6683-249CBD8BF2EE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.8903987407684326;
createNode animCurveTL -n "Character1_RightHandIndex3_translateY";
	rename -uid "4D79EB9C-490C-D78E-0254-3E82CC32E6CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3401000499725342;
createNode animCurveTL -n "Character1_RightHandIndex3_translateZ";
	rename -uid "D9805E68-4C66-DD9F-39BE-B981044A7CE1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.8930935859680176;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleX";
	rename -uid "B1134D2B-4D10-05D8-E243-F78A8EBBE143";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleY";
	rename -uid "7B568CE2-4357-923C-4896-34A6C9EC9441";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleZ";
	rename -uid "6BFB6A45-4949-4ED0-885C-3D8DE9B5C6DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateX";
	rename -uid "D50CA835-4AD8-435F-E518-08A6EB6E2A70";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -4.9501181820232887e-007 10 -6.1307525811571395e-007
		 11 0.43758806586265564 12 1.4493577480316162 13 2.5879013538360596 14 3.4209811687469482
		 15 3.5290234088897705 16 2.4359307289123535 17 0.39724147319793701 18 -1.8872168064117429
		 19 -3.6659891605377193 20 -4.1534085273742676 21 -2.8486487865447998 22 -0.29528295993804932
		 23 2.7297041416168213 24 5.5013251304626465 25 7.3716211318969727 26 8.0842580795288086
		 27 8.0675201416015625 28 7.7307567596435547 29 7.4810032844543457 30 7.5306677818298349
		 31 7.6836056709289551 32 7.7385091781616202 33 7.494250774383544 34 6.7449579238891602
		 35 5.1838083267211914 36 2.9103233814239502 37 0.34239634871482849 38 -2.0596880912780762
		 39 -3.7994456291198726 40 -4.7290053367614746 41 -5.1532931327819824 42 -5.2464380264282227
		 43 -5.1862010955810547 44 -5.1510763168334961 45 -5.081784725189209 46 -4.859220027923584
		 47 -4.5720329284667969 48 -4.3084268569946289 49 -4.1560187339782715 50 -4.2022285461425781
		 51 -4.8163518905639648 52 -5.9558525085449219 53 -7.0833144187927246 54 -7.6482343673706064
		 55 -7.0972428321838379 56 -4.7593131065368652 57 -1.0683462619781494 58 2.889091968536377
		 59 6.1343646049499512 60 7.8180241584777832 61 7.6860299110412598 62 6.3464784622192383
		 63 4.277061939239502 64 1.978881239891052 65 -7.520035296693095e-007 66 -7.5195356430413085e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateY";
	rename -uid "1FB15772-4637-331E-C7E9-B1A01B56E3C3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 3.6536295766609328e-008 10 1.5471299263936089e-007
		 11 -0.18901552259922028 12 -0.64396023750305176 13 -1.1863301992416382 14 -1.6040171384811401
		 15 -1.6594957113265991 16 -1.1120473146438599 17 -0.17139346897602081 18 0.76269489526748657
		 19 1.4054117202758789 20 1.5689235925674438 21 1.1191153526306152 22 0.12493280321359633
		 23 -1.2561724185943604 24 -2.7262141704559326 25 -3.8352363109588623 26 -4.2837095260620117
		 27 -4.2730093002319336 28 -4.0594253540039062 29 -3.9031279087066655 30 -3.9340662956237789
		 31 -4.0297813415527344 32 -4.0643057823181152 33 -3.911373376846313 34 -3.4528012275695801
		 35 -2.5474822521209717 36 -1.3458755016326904 37 -0.1475023627281189 38 0.82821059226989746
		 39 1.450715184211731 40 1.755135178565979 41 1.8876610994338989 42 1.9162195920944214
		 43 1.8977727890014651 44 1.8869791030883789 45 1.86560595035553 46 1.7962336540222168
		 47 1.70508873462677 48 1.6198043823242187 49 1.5697848796844482 50 1.5850057601928711
		 51 1.7827454805374146 52 2.1274423599243164 53 2.4404671192169189 54 2.5869524478912354
		 55 2.4441616535186768 56 1.7647347450256348 57 0.44212859869003296 58 -1.3352878093719482
		 59 -3.0907406806945801 60 -4.1144590377807617 61 -4.031303882598877 62 -3.2153444290161133
		 63 -2.051950216293335 64 -0.89216125011444092 65 2.6861772539632511e-007 66 2.7008562142327719e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateZ";
	rename -uid "B96C68CE-45DA-2A11-B37C-0E9A469AB79F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 1.2744408195430879e-007 10 2.2659590115381434e-007
		 11 0.51224136352539063 12 1.6987283229827881 13 3.0376336574554443 14 4.0200252532958984
		 15 4.1476078033447266 16 2.858677864074707 17 0.46498933434486384 18 -2.2033567428588867
		 19 -4.2723016738891602 20 -4.8380484580993652 21 -3.3225007057189941 22 -0.3453584611415863
		 23 3.2046852111816406 24 6.4842352867126465 25 8.7147006988525391 26 9.5687646865844727
		 27 9.5486783981323242 28 9.1448040008544922 29 8.8456325531005859 30 8.9051008224487305
		 31 9.0883007049560547 32 9.1540956497192383 33 8.8614940643310547 34 7.9656405448913565
		 35 6.1070489883422852 36 3.4175622463226318 37 0.40076449513435364 38 -2.4042775630950928
		 39 -4.4272522926330566 40 -5.5055336952209473 41 -5.9971499443054199 42 -6.1050310134887695
		 43 -6.0352659225463867 44 -5.9945821762084961 45 -5.9143176078796387 46 -5.6564474105834961
		 47 -5.3235659599304199 48 -5.0178775787353516 49 -4.8410768508911133 50 -4.8946871757507324
		 51 -5.6067686080932617 52 -6.9261703491210938 53 -8.2294406890869141 54 -8.8816976547241211
		 55 -8.2455291748046875 56 -5.5406618118286133 57 -1.2484281063079834 58 3.3925330638885498
		 59 7.2374796867370605 60 9.2494096755981445 61 9.0912055969238281 62 7.4902505874633789
		 63 5.0320892333984375 64 2.3209247589111328 65 3.3626739082137647e-007 66 3.3651409125923237e-007;
createNode animCurveTU -n "Character1_RightHandIndex3_visibility";
	rename -uid "D46FF2E9-4258-0C0A-E845-55ADF14A4958";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing1_translateX";
	rename -uid "17B12D92-4058-1117-68B1-F889485B4D35";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.6450033187866211;
createNode animCurveTL -n "Character1_RightHandRing1_translateY";
	rename -uid "0DDB8792-4A12-39DD-CB4B-55A2BFAC8B72";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.0317020416259766;
createNode animCurveTL -n "Character1_RightHandRing1_translateZ";
	rename -uid "A6E98C0D-431C-9ED6-2380-58BF71F108C4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.2285447120666504;
createNode animCurveTU -n "Character1_RightHandRing1_scaleX";
	rename -uid "DBBE09FB-47D1-7746-CE4E-AF995D74A8D4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandRing1_scaleY";
	rename -uid "24D68CC2-4428-6FC7-08DD-699566B0564D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandRing1_scaleZ";
	rename -uid "E7652211-4152-FDF4-1D0B-84A8E993BFEB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightHandRing1_rotateX";
	rename -uid "C2BE041C-4319-1A90-BD8A-C19FFBAF78A0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 80 ".ktv[0:79]"  0 11.189376831054688 1 20.65165901184082
		 2 36.9410400390625 3 48.429409027099609 4 48.429409027099609 5 48.429409027099609
		 6 48.429409027099609 7 39.544761657714844 8 25.224800109863281 9 16.161251068115234
		 10 11.573809623718262 11 9.3270034790039062 12 8.742375373840332 13 9.1339311599731445
		 14 9.4329071044921875 15 9.4724836349487305 16 9.0805492401123047 17 8.3971071243286133
		 18 7.698218822479248 19 7.1985473632812509 20 7.0680079460144043 21 7.423527717590332
		 22 8.1781044006347656 23 9.1840591430664062 24 10.22929573059082 25 11.012314796447754
		 26 11.329141616821289 27 11.321577072143555 28 11.17064094543457 29 11.06025218963623
		 30 11.082099914550781 31 11.149700164794922 32 11.174088478088379 33 11.066074371337891
		 34 10.742367744445801 35 10.102900505065918 36 9.248356819152832 37 8.3795280456542969
		 38 7.6481246948242179 39 7.1625385284423828 40 6.9172630310058594 41 6.808469295501709
		 42 6.784846305847168 43 6.8001127243041992 44 6.8090329170227051 45 6.8266682624816895
		 46 6.8836660385131836 47 6.9580106735229492 48 7.0270490646362305 49 7.0673160552978516
		 50 7.0550799369812012 51 6.8947062492370605 52 6.6079707145690918 53 6.3377227783203125
		 54 6.2072196006774902 55 6.3344659805297852 56 6.9094271659851074 57 7.9410605430603027
		 58 9.2407732009887695 59 10.486801147460937 60 11.209521293640137 61 11.150775909423828
		 62 10.574767112731934 63 9.7517251968383789 64 8.9220991134643555 65 8.2707014083862305
		 66 8.2707014083862305 68 8.2707014083862305 69 8.2752676010131836 70 8.2887630462646484
		 71 8.3127927780151367 72 8.3533048629760742 73 8.4220895767211914 74 8.5367946624755859
		 75 8.7193059921264648 76 8.9925575256347656 77 9.3758583068847656 78 9.8787813186645508
		 79 10.493897438049316 80 11.189376831054688;
createNode animCurveTA -n "Character1_RightHandRing1_rotateY";
	rename -uid "9136F757-4E10-06D0-3BFE-60B4C9C9FECA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -28.328363418579102 1 -43.166980743408203
		 2 -56.089092254638672 3 -60.605064392089844 4 -60.605064392089844 5 -60.605064392089844
		 6 -60.605064392089844 7 -57.046714782714844 8 -45.396007537841797 9 -27.446964263916016
		 10 -8.2389354705810547 11 6.2732758522033691 12 10.882070541381836 13 9.1833868026733398
		 14 7.9307956695556641 15 7.7677278518676758 16 9.4109907150268555 17 12.438958168029785
		 18 15.780301094055178 19 18.348506927490234 20 19.047607421875 21 17.171821594238281
		 22 13.457343101501465 23 8.9707660675048828 24 4.7647571563720703 25 1.8683276176452639
		 26 0.75135272741317749 27 0.77767294645309448 28 1.3063747882843018 29 1.6973751783370972
		 30 1.619697093963623 31 1.3802621364593506 32 1.2942217588424683 33 1.6766599416732788
		 34 2.8443541526794434 35 5.2516465187072754 36 8.6995954513549805 37 12.519791603088379
		 38 16.030534744262695 39 18.540111541748047 40 19.87077522277832 41 20.475948333740234
		 42 20.608623504638672 43 20.522829055786133 44 20.472789764404297 45 20.374046325683594
		 46 20.056646347045898 47 19.646539688110352 48 19.269554138183594 49 19.051347732543945
		 50 19.117525100708008 51 19.995471954345703 52 21.617092132568359 53 23.212799072265625
		 54 24.009302139282227 55 23.232461929321289 56 19.91404914855957 57 14.588451385498045
		 58 8.7314910888671875 59 3.7899260520935059 60 1.1695332527160645 61 1.3764646053314209
		 62 3.4620423316955566 63 6.6347098350524902 64 10.093880653381348 65 13.023721694946289
		 66 13.023721694946289 67 13.023721694946289 68 13.023721694946289 69 12.459603309631348
		 70 10.866425514221191 71 8.3942432403564453 72 5.1945657730102539 73 1.4200351238250732
		 74 -2.7759802341461182 75 -7.2399816513061515 76 -11.819174766540527 77 -16.362598419189453
		 78 -20.722568511962891 79 -24.756427764892578 80 -28.328363418579102;
createNode animCurveTA -n "Character1_RightHandRing1_rotateZ";
	rename -uid "6E595FE2-4E65-5412-74E7-D4A68C7F6486";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -12.764520645141602 1 -20.919195175170898
		 2 -36.421989440917969 3 -47.688526153564453 4 -47.688526153564453 5 -47.688526153564453
		 6 -47.688526153564453 7 -38.098941802978516 8 -22.197700500488281 9 -11.565335273742676
		 10 -6.2132401466369629 11 -4.306307315826416 12 -4.3275747299194336 13 -4.8898930549621582
		 14 -5.3112659454345703 15 -5.3665661811828613 16 -4.8139567375183105 17 -3.820662260055542
		 18 -2.7566773891448975 19 -1.9576506614685059 20 -1.7425246238708496 21 -2.3219485282897949
		 22 -3.4931061267852783 23 -4.9610028266906738 24 -6.4047470092773437 25 -7.445580005645752
		 26 -7.8585896492004395 27 -7.848778247833252 28 -7.6525115966796866 29 -7.5083508491516113
		 30 -7.5369248390197754 31 -7.6252059936523446 32 -7.657005786895752 33 -7.515967845916748
		 34 -7.0901498794555664 35 -6.2337498664855957 36 -5.0519371032714844 37 -3.7945504188537602
		 38 -2.6781682968139648 39 -1.8985966444015503 40 -1.4903786182403564 41 -1.3057533502578735
		 42 -1.2653565406799316 43 -1.2914758920669556 44 -1.3067156076431274 45 -1.336799144744873
		 46 -1.4336086511611938 47 -1.5589457750320435 48 -1.6744202375411987 49 -1.7413762807846069
		 50 -1.7210603952407837 51 -1.4522867202758789 52 -0.95918577909469616 53 -0.47759154438972473
		 54 -0.23832778632640839 55 -0.47167688608169556 56 -1.4771565198898315 57 -3.1327433586120605
		 58 -5.0412263870239258 59 -6.7504324913024902 60 -7.7031612396240234 61 -7.62660789489746
		 62 -6.867729663848877 63 -5.7537598609924316 64 -4.5872373580932617 65 -3.6322050094604492
		 66 -3.6322050094604492 67 -3.6322050094604492 68 -3.6322050094604492 69 -3.6802229881286626
		 70 -3.8249459266662593 71 -4.0762529373168945 72 -4.4504117965698242 73 -4.9648704528808594
		 74 -5.6347270011901855 75 -6.4702057838439941 76 -7.4742140769958496 77 -8.6391620635986328
		 78 -9.942500114440918 79 -11.340971946716309 80 -12.764520645141602;
createNode animCurveTU -n "Character1_RightHandRing1_visibility";
	rename -uid "4D735212-45EB-DD3B-CB51-1E928975A937";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing2_translateX";
	rename -uid "DB2FA948-4EA2-CD41-DA3B-599EC3EE9AD6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.046228401362895966;
createNode animCurveTL -n "Character1_RightHandRing2_translateY";
	rename -uid "884A0436-4A20-8B4F-19FB-0EA35647A964";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.89510548114776611;
createNode animCurveTL -n "Character1_RightHandRing2_translateZ";
	rename -uid "510C8BCD-4860-8919-F789-4F8F3E4AD8E3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.4977946281433105;
createNode animCurveTU -n "Character1_RightHandRing2_scaleX";
	rename -uid "B435CDCA-430D-BAFF-1B50-5DAA9A5B8382";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandRing2_scaleY";
	rename -uid "A88571A7-4A1A-C3C3-2904-7A910D81910B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandRing2_scaleZ";
	rename -uid "4087E8C5-43D4-3713-824B-F2A733C05447";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightHandRing2_rotateX";
	rename -uid "13407991-4588-9E81-FF43-ACA64982CD31";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 39.536983489990234 1 62.897090911865234
		 2 86.824249267578125 3 96.525802612304688 4 96.525802612304688 5 96.525802612304688
		 6 96.525802612304688 7 94.257896423339844 8 88.342643737792969 9 80.276466369628906
		 10 72.046798706054687 11 66.530677795410156 12 65.777755737304688 13 67.72314453125
		 14 69.158439636230469 15 69.34527587890625 16 67.462371826171875 17 63.998004913330078
		 18 60.199001312255859 19 57.308544158935554 20 56.527484893798828 21 58.629005432128899
		 22 62.836578369140625 23 67.966766357421875 24 72.781173706054688 25 76.077239990234375
		 26 77.340866088867188 27 77.311141967773437 28 76.713577270507812 29 76.270942687988281
		 30 76.358924865722656 31 76.629981994628906 32 76.727325439453125 33 76.294410705566406
		 34 74.969329833984375 35 72.225006103515625 36 68.277496337890625 37 63.905727386474609
		 38 59.916057586669922 39 57.094211578369141 40 55.611408233642578 41 54.940547943115234
		 42 54.793777465820313 43 54.888671875 44 54.944038391113281 45 55.0533447265625 46 55.405113220214844
		 47 55.860549926757813 48 56.280094146728516 49 56.5233154296875 50 56.449520111083984
		 51 55.472988128662109 52 53.681983947753906 53 51.937282562255859 54 51.073513031005859
		 55 51.915901184082031 56 55.563358306884766 57 61.549983978271484 58 68.240951538085937
		 59 73.893142700195313 60 76.868354797363281 61 76.63427734375 62 74.266609191894531
		 63 70.6429443359375 64 66.68011474609375 65 63.330787658691413 66 63.330787658691413
		 67 63.330787658691413 68 63.330787658691413 69 63.020732879638672 70 62.142650604248047
		 71 60.773094177246101 72 58.988056182861328 73 56.864692687988281 74 54.482643127441406
		 75 51.924655914306641 76 49.276447296142578 77 46.625823974609375 78 44.061222076416016
		 79 41.670009613037109 80 39.536983489990234;
createNode animCurveTA -n "Character1_RightHandRing2_rotateY";
	rename -uid "837CC862-44AA-4F34-59A1-EB945DE8EF0C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 32.544033050537109 1 36.959693908691406
		 2 34.052345275878906 3 30.502138137817383 4 30.502138137817383 5 30.502138137817383
		 6 30.502138137817383 7 31.176380157470707 8 32.449863433837891 9 33.177936553955078
		 10 32.826107025146484 11 31.827159881591793 12 31.301729202270508 13 31.267976760864258
		 14 31.209022521972653 15 31.199216842651364 16 31.275581359863281 17 31.286169052124023
		 18 31.104120254516605 19 30.828479766845703 20 30.733371734619137 21 30.969240188598633
		 22 31.252071380615234 23 31.260009765625 24 30.930679321289066 25 30.513689041137692
		 26 30.311573028564457 27 30.316598892211914 28 30.414869308471683 29 30.484241485595703
		 30 30.470684051513672 31 30.428195953369137 32 30.412668228149414 33 30.480638504028317
		 34 30.671503067016598 35 30.985551834106449 36 31.248640060424801 37 31.284151077270508
		 38 31.082405090332035 39 30.803260803222656 40 30.610509872436523 41 30.512737274169918
		 42 30.490463256835941 43 30.504899978637692 44 30.513263702392575 45 30.529640197753906
		 46 30.581148147583008 47 30.645137786865238 48 30.701398849487308 49 30.732839584350586
		 50 30.723390579223636 51 30.590877532958984 52 30.311359405517575 53 29.992952346801758
		 54 29.81818962097168 55 29.988765716552738 56 30.603727340698242 57 31.192165374755856
		 58 31.250047683715824 59 30.807632446289063 60 30.389926910400394 61 30.427513122558594
		 62 30.762298583984379 63 31.117498397827148 64 31.292667388916019 65 31.268894195556637
		 66 31.268894195556637 67 31.268894195556637 68 31.268894195556637 69 31.321159362792965
		 70 31.464344024658203 71 31.673345565795898 72 31.919303894042972 73 32.17242431640625
		 74 32.404609680175781 75 32.591953277587891 76 32.716911315917969 77 32.770114898681641
		 78 32.751415252685547 79 32.670093536376953 80 32.544033050537109;
createNode animCurveTA -n "Character1_RightHandRing2_rotateZ";
	rename -uid "1533ACCD-4603-1028-C1AD-76878C33F539";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 31.752662658691406 1 52.605953216552734
		 2 74.044036865234375 3 82.574462890625 4 82.574462890625 5 82.574462890625 6 82.574462890625
		 7 80.23291015625 8 74.206779479980469 9 66.155586242675781 10 58.127548217773438
		 11 52.851890563964844 12 52.148002624511719 13 53.974834442138672 14 55.322490692138672
		 15 55.497898101806641 16 53.729961395263672 17 50.476676940917969 18 46.909904479980469
		 19 44.197792053222656 20 43.465297698974609 21 45.436538696289063 22 49.386081695556641
		 23 54.203594207763672 24 58.72265625 25 61.813407897949219 26 62.997322082519524
		 27 62.969486236572273 28 62.409687042236321 29 61.994937896728516 30 62.077381134033196
		 31 62.33135986328125 32 62.422569274902344 33 62.01692199707032 34 60.774894714355469
		 35 58.200824737548828 36 54.495361328125 37 50.390022277832031 38 46.644340515136719
		 39 43.996768951416016 40 42.606422424316406 41 41.977638244628906 42 41.840091705322266
		 43 41.929019927978516 44 41.980907440185547 45 42.083347320556641 46 42.413047790527344
		 47 42.839977264404297 48 43.233325958251953 49 43.461387634277344 50 43.392189025878906
		 51 42.476669311523438 52 40.798465728759766 53 39.164939880371094 54 38.356746673583984
		 55 39.144927978515625 56 42.561382293701172 57 48.1781005859375 58 54.461044311523438
		 59 59.765727996826179 60 62.554691314697259 61 62.33538818359375 62 60.115978240966797
		 63 56.716079711914062 64 52.995388031005859 65 49.850139617919922 66 49.850139617919922
		 67 49.850139617919922 68 49.850139617919922 69 49.631580352783203 70 49.009727478027344
		 71 48.031490325927734 72 46.741676330566406 73 45.186389923095703 74 43.415645599365234
		 75 41.484992980957031 76 39.4560546875 77 37.396022796630859 78 35.376270294189453
		 79 33.470474243164063 80 31.752662658691406;
createNode animCurveTU -n "Character1_RightHandRing2_visibility";
	rename -uid "BF8D79F9-40A5-129D-836E-01839ED7F467";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing3_translateX";
	rename -uid "20770AC5-40C3-E517-10FF-B689B2BE0D49";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.8638609647750854;
createNode animCurveTL -n "Character1_RightHandRing3_translateY";
	rename -uid "EB24C988-40B2-5E8A-1FC2-429360E40F8C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0986917018890381;
createNode animCurveTL -n "Character1_RightHandRing3_translateZ";
	rename -uid "DDC88D07-4361-7E4D-9121-3585FC0EAA23";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.2819910049438477;
createNode animCurveTU -n "Character1_RightHandRing3_scaleX";
	rename -uid "3F1DBCA3-4174-54EA-D085-72B5C1216F55";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing3_scaleY";
	rename -uid "87A66BC0-4053-69C3-93D6-7F92BC6B5769";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing3_scaleZ";
	rename -uid "F53954D9-4900-BB82-1AC3-958B4A4EFBB4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_RightHandRing3_rotateX";
	rename -uid "37D02D63-4EBA-E537-1F22-E8B5EDB09BAC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -2.4650927343827789e-007 10 -2.3201803855954492e-007
		 11 0.43865591287612915 12 1.4528474807739258 13 2.5940444469451904 14 3.4290218353271484
		 15 3.5373075008392334 16 2.4417235851287842 17 0.39821118116378784 18 -1.8919682502746582
		 19 -3.6754577159881596 20 -4.164212703704834 21 -2.8559195995330811 22 -0.29601007699966431
		 23 2.7361726760864258 24 5.5139636993408203 25 7.3882594108581543 26 8.1023960113525391
		 27 8.0856237411499023 28 7.7481517791748038 29 7.4978723526000968 30 7.5476417541503906
		 31 7.700901985168457 32 7.7559208869934082 33 7.5111479759216309 34 6.7602677345275879
		 35 5.1957573890686035 36 2.9172055721282959 37 0.34323307871818542 38 -2.0648860931396484
		 39 -3.8092772960662842 40 -4.7414112091064453 41 -5.1668977737426758 42 -5.2603068351745605
		 43 -5.1998991966247559 44 -5.1646747589111328 45 -5.0951862335205078 46 -4.8719921112060547
		 47 -4.5839991569519043 48 -4.3196597099304199 49 -4.1668300628662109 50 -4.2131671905517578
		 51 -4.8290038108825684 52 -5.9717659950256348 53 -7.1025657653808594 54 -7.669200897216796
		 55 -7.1165366172790527 56 -4.7718038558959961 57 -1.07100510597229 58 2.8959255218505859
		 59 6.148369312286377 60 7.8356027603149414 61 7.7033309936523446 62 6.3609375953674316
		 63 4.287017822265625 64 1.9836142063140869 65 -2.2113815134616743e-007 66 -2.2077898620409542e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateY";
	rename -uid "A58FA86B-4C4C-4CAD-9909-A8A447B8E114";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 4.0899806208472e-008 10 1.6422437454366445e-007
		 11 -0.19156825542449951 12 -0.65243417024612427 13 -1.2014980316162109 14 -1.6241027116775513
		 15 -1.6802202463150024 16 -1.1263197660446167 17 -0.17371045053005219 18 0.77364504337310791
		 19 1.4265892505645752 20 1.5928868055343628 21 1.1356050968170166 22 0.12665271759033203
		 23 -1.2721761465072632 24 -2.7586495876312256 25 -3.8788552284240718 26 -4.3316092491149902
		 27 -4.320807933807373 28 -4.1052002906799316 29 -3.9474031925201412 30 -3.9786398410797119
		 31 -4.0752730369567871 32 -4.1101269721984863 33 -3.95572829246521 34 -3.4926650524139404
		 35 -2.5780267715454102 36 -1.3629448413848877 37 -0.14949929714202881 38 0.84015637636184692
		 39 1.4726561307907104 40 1.7823784351348877 41 1.9173154830932619 42 1.9464023113250735
		 43 1.9276138544082639 44 1.9166208505630493 45 1.8948541879653933 46 1.8242174386978149
		 47 1.7314383983612061 48 1.6446520090103149 49 1.5937628746032715 50 1.6092475652694702
		 51 1.8104856014251707 52 2.1616411209106445 53 2.4810130596160889 54 2.630662202835083
		 55 2.484785795211792 56 1.7921503782272339 57 0.44833946228027344 58 -1.3522317409515381
		 59 -3.1269533634185791 60 -4.1607580184936523 61 -4.076810359954834 62 -3.2528245449066162
		 63 -2.077106237411499 64 -0.90374457836151134 65 2.9426377068375587e-007 66 2.9543326718339813e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateZ";
	rename -uid "B52F509D-4183-A5D0-89D7-49B27136C94A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  9 -6.0330876294756308e-008 10 -3.1079210316420358e-008
		 11 0.51036757230758667 12 1.6924059391021729 13 3.0261077880859375 14 4.0045566558837891
		 15 4.1316194534301758 16 2.8478586673736572 17 0.46328917145729059 18 -2.1956193447113037
		 19 -4.2577676773071289 20 -4.8217334747314453 21 -3.3110306262969971 22 -0.34411191940307617
		 23 3.1924960613250732 24 6.4584102630615234 25 8.6789226531982422 26 9.5290288925170898
		 27 9.5090360641479492 28 9.1070432662963867 29 8.8092536926269531 30 8.8684473037719727
		 31 9.0508012771606445 32 9.1162910461425781 33 8.8250417709350586 34 7.9332671165466309
		 35 6.0828533172607422 36 3.4045243263244629 37 0.39930081367492676 38 -2.395859956741333
		 39 -4.412226676940918 40 -5.4871606826782227 41 -5.9772906303405762 42 -6.0848479270935059
		 43 -6.0152921676635742 44 -5.9747304916381836 45 -5.8947067260742188 46 -5.6376152038574219
		 47 -5.3057494163513184 48 -5.0010037422180176 49 -4.8247523307800293 50 -4.8781957626342773
		 51 -5.5880875587463379 52 -6.9035696983337402 53 -8.2031431198120117 54 -8.8536128997802734
		 55 -8.2191867828369141 56 -5.522181510925293 57 -1.2439804077148437 58 3.3795955181121826
		 59 7.2083559036254883 60 9.2111625671386719 61 9.0536928176879883 62 7.4600052833557129
		 63 5.012448787689209 64 2.3122086524963379 65 1.2290854556340491e-008 66 1.2769858948047386e-008;
createNode animCurveTU -n "Character1_RightHandRing3_visibility";
	rename -uid "7E46CC2D-4915-ED0B-B384-319998A996E2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Neck_translateX";
	rename -uid "C3BF59E2-4367-FD39-2A96-51A06C356B95";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.1971182823181152;
createNode animCurveTL -n "Character1_Neck_translateY";
	rename -uid "8F40CDA4-4DF0-6F00-04FD-B49B1674ACE9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.621223449707031;
createNode animCurveTL -n "Character1_Neck_translateZ";
	rename -uid "A436EF4D-4AC2-7342-5703-3D9DCAEC47C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.1444897651672363;
createNode animCurveTU -n "Character1_Neck_scaleX";
	rename -uid "C1A9561F-4F03-6441-FBBB-78BF9035C0C2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_Neck_scaleY";
	rename -uid "7ADB92EA-46D8-7109-180C-02AFBBC693C9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Neck_scaleZ";
	rename -uid "C6D5A86D-423A-8FD7-2662-06A6D451B0E6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_Neck_rotateX";
	rename -uid "62CBA96F-45B8-B479-9BA1-8DB619F3201F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 3.9813573360443115 1 3.4986350536346436
		 2 2.866600513458252 3 2.1881599426269531 4 1.5361731052398682 5 0.85112082958221436
		 6 0.16448409855365753 7 -0.45295047760009766 8 -0.8785707950592041 9 -1.0432050228118896
		 10 -1.0251258611679077 11 -0.96661478281021129 12 -0.97071272134780895 13 -1.0547680854797363
		 14 -1.23963463306427 15 -1.5484588146209717 16 -1.9868450164794922 17 -2.5357036590576172
		 18 -3.1801128387451172 19 -3.9044396877288818 20 -4.692629337310791 21 -5.5131254196166992
		 22 -6.270421028137207 23 -6.8530688285827637 24 -7.1512608528137207 25 -7.123720645904541
		 26 -6.759953498840332 27 -6.139061450958252 28 -5.433387279510498 29 -4.8088369369506836
		 30 -4.3423862457275391 31 -3.9798078536987305 32 -3.6864166259765625 33 -3.4289102554321289
		 34 -3.1746895313262939 35 -2.9252233505249023 36 -2.709446907043457 37 -2.5302393436431885
		 38 -2.4229707717895508 39 -2.4232311248779297 40 -2.5500190258026123 41 -2.7591707706451416
		 42 -2.9917035102844238 43 -3.1893539428710938 44 -3.2946584224700928 45 -3.2721478939056396
		 46 -3.1424121856689453 47 -2.9324464797973633 48 -2.6686878204345703 49 -2.3763716220855713
		 50 -2.0648572444915771 51 -1.7706305980682373 52 -1.4862594604492187 53 -1.1643277406692505
		 54 -0.76124769449234009 55 -0.23811355233192441 56 0.41325315833091736 57 1.1308730840682983
		 58 1.8430427312850952 59 2.4759683609008789 60 2.9542007446289062 61 3.2509217262268066
		 62 3.4142241477966309 63 3.481534481048584 64 3.4916713237762451 65 3.4845948219299316
		 66 3.5793569087982178 67 3.672199010848999 68 3.778012752532959 69 3.8984830379486088
		 70 4.0108494758605957 71 4.095862865447998 72 4.1411352157592773 73 4.1415681838989258
		 74 4.0947456359863281 75 3.9875857830047612 76 3.821298360824585 77 3.6099429130554199
		 78 3.3697874546051025 79 3.1228890419006348 80 2.8985869884490967;
createNode animCurveTA -n "Character1_Neck_rotateY";
	rename -uid "815BA2FB-4F93-3BF7-F0C6-68A76741BF81";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -11.86030101776123 1 -5.9723091125488281
		 2 0.085873633623123169 3 3.6006097793579102 4 3.9375934600830083 5 2.8331155776977539
		 6 1.0390990972518921 7 -0.71173131465911865 8 -1.7229863405227661 9 -1.9963272809982298
		 10 -2.0383281707763672 11 -1.9889445304870608 12 -1.9504735469818115 13 -1.9456924200057983
		 14 -1.9937505722045898 15 -2.1119933128356934 16 -2.2571673393249512 17 -2.397355318069458
		 18 -2.5701904296875 19 -2.8156812191009521 20 -3.1754045486450195 21 -3.6680257320404053
		 22 -4.2214078903198242 23 -4.739346981048584 24 -5.1225991249084473 25 -5.3080487251281738
		 26 -5.2759137153625488 27 -5.0763821601867676 28 -4.7996349334716797 29 -4.5499205589294434
		 30 -4.3912496566772461 31 -4.2943553924560547 32 -4.2298970222473145 33 -4.1632094383239746
		 34 -4.0554356575012207 35 -3.8722960948944092 36 -3.646655797958374 37 -3.4307520389556885
		 38 -3.2827174663543701 39 -3.2610995769500732 40 -3.4031767845153809 41 -3.6572604179382329
		 42 -3.9486036300659175 43 -4.2020220756530762 44 -4.3418416976928711 45 -4.342383861541748
		 46 -4.2423815727233887 47 -4.0622920989990234 48 -3.8228623867034912 49 -3.5458250045776367
		 50 -3.2321798801422119 51 -2.9100544452667236 52 -2.585686206817627 53 -2.2304830551147461
		 54 -1.8124681711196897 55 -1.2949169874191284 56 -0.65086996555328369 57 0.06339138001203537
		 58 0.76535540819168091 59 1.3742048740386963 60 1.8103901147842407 61 2.0770854949951172
		 62 2.2460188865661621 63 2.3394167423248291 64 2.3780772686004639 65 2.3820123672485352
		 66 2.4695377349853516 67 2.478285551071167 68 2.3976402282714844 69 2.2580971717834473
		 70 2.0585424900054932 71 1.7394776344299316 72 1.2367430925369263 73 0.48150178790092468
		 74 -0.59684073925018311 75 -2.0938584804534912 76 -3.9681785106658936 77 -6.0830788612365723
		 78 -8.3045883178710937 79 -10.504421234130859 80 -12.561663627624512;
createNode animCurveTA -n "Character1_Neck_rotateZ";
	rename -uid "BBF0A9A4-4727-4256-5F2B-128F4D9EA7F4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 3.060516357421875 1 -3.2497310638427734
		 2 -9.440709114074707 3 -12.545565605163574 4 -11.970759391784668 5 -9.6876382827758789
		 6 -6.6041831970214844 7 -3.6421532630920406 8 -1.7510051727294922 9 -0.92934757471084584
		 10 -0.46901786327362061 11 -0.09878142923116684 12 0.36426323652267456 13 0.88192427158355713
		 14 1.4165712594985962 15 1.9307680130004883 16 2.4444589614868164 17 2.9690446853637695
		 18 3.4553289413452148 19 3.8570542335510258 20 4.1313858032226562 21 4.2501964569091797
		 22 4.203270435333252 23 3.9722530841827393 24 3.5342643260955811 25 2.8708086013793945
		 26 1.8367171287536621 27 0.49179798364639282 28 -0.84034258127212524 29 -1.8401318788528442
		 30 -2.3885970115661621 31 -2.6477165222167969 32 -2.7220325469970703 33 -2.7148234844207764
		 34 -2.7283849716186523 35 -2.714482307434082 36 -2.6131711006164551 37 -2.5035598278045654
		 38 -2.414867639541626 39 -2.3759682178497314 40 -2.4125387668609619 41 -2.5157055854797363
		 42 -2.6624755859375 43 -2.8304121494293213 44 -2.9974527359008789 45 -3.2270092964172363
		 46 -3.5519628524780273 47 -3.9042108058929439 48 -4.2155961990356445 49 -4.4182319641113281
		 50 -4.4177794456481934 51 -4.0888857841491699 52 -3.4719126224517822 53 -2.7065887451171875
		 54 -1.9313086271286009 55 -1.2825736999511719 56 -0.74544495344161987 57 -0.23370178043842318
		 58 0.2225707620382309 59 0.59008175134658813 60 0.83277904987335205 61 0.95597708225250244
		 62 1.0008689165115356 63 0.9849204421043396 64 0.92681246995925903 65 0.84602540731430054
		 66 0.93443650007247925 67 0.8952828049659729 68 0.6932641863822937 69 0.05380895733833313
		 70 -1.0756782293319702 71 -2.3979792594909668 72 -3.614861011505127 73 -4.4257755279541016
		 74 -4.5262842178344727 75 -3.7884159088134766 76 -2.4227669239044189 77 -0.62883108854293823
		 78 1.3910565376281738 79 3.4288320541381836 80 5.2685484886169434;
createNode animCurveTU -n "Character1_Neck_visibility";
	rename -uid "C9137E6A-4F99-4E43-4DCA-A4B75D25A6E8";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Head_translateX";
	rename -uid "E7DA95E4-46E0-4F76-8E9D-9AAE54C39DBA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -16.985572814941406;
createNode animCurveTL -n "Character1_Head_translateY";
	rename -uid "B0A2FEF9-4E5D-CCE2-F450-EA8B7A532290";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.0131206512451172;
createNode animCurveTL -n "Character1_Head_translateZ";
	rename -uid "FE420997-497A-DB0E-76D4-F2AB53B40C10";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.1724905967712402;
createNode animCurveTU -n "Character1_Head_scaleX";
	rename -uid "AE10610D-4DCF-1C7E-20B6-94A0699A6083";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Head_scaleY";
	rename -uid "59A0500D-45B6-3D5D-9ECB-E9803D589C16";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_Head_scaleZ";
	rename -uid "0A1A863A-41AD-B01E-92C9-70B989EE296D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_Head_rotateX";
	rename -uid "F0C3219E-490B-E298-317B-3B8A12134BDC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -12.699968338012695 1 -8.7626667022705078
		 2 -4.8152446746826172 3 -2.3194985389709473 4 -1.5281152725219727 5 -1.4864517450332642
		 6 -1.8634859323501589 7 -2.3280260562896729 8 -2.5500171184539795 9 -2.5241131782531738
		 10 -2.4720845222473145 11 -2.3927826881408691 12 -2.2932271957397461 13 -2.1936235427856445
		 14 -2.1104474067687988 15 -2.0588991641998291 16 -1.9765864610671997 17 -1.8232641220092773
		 18 -1.6503856182098389 19 -1.5107204914093018 20 -1.4577010869979858 21 -1.5275399684906006
		 22 -1.6902685165405273 23 -1.9000256061553955 24 -2.1096870899200439 25 -2.2738943099975586
		 26 -2.4431135654449463 27 -2.6579554080963135 28 -2.8587422370910645 29 -2.9926877021789551
		 30 -3.0477297306060791 31 -3.0594539642333984 32 -3.0504965782165527 33 -3.0390677452087402
		 34 -3.0394711494445801 35 -2.9896528720855713 36 -2.8641743659973145 37 -2.7244331836700439
		 38 -2.6180360317230225 39 -2.5928034782409668 40 -2.6806464195251465 41 -2.8462002277374268
		 42 -3.0364484786987305 43 -3.1979830265045166 44 -3.2768809795379639 45 -3.2997760772705078
		 46 -3.3196229934692383 47 -3.3211700916290283 48 -3.289132833480835 49 -3.2091636657714844
		 50 -3.0506050586700439 51 -2.7932760715484619 52 -2.4630072116851807 53 -2.0959115028381348
		 54 -1.7215769290924072 55 -1.3608505725860596 56 -0.96369463205337513 57 -0.50960946083068848
		 58 -0.056219551712274551 59 0.3387681245803833 60 0.61702126264572144 61 0.80795639753341675
		 62 0.97573858499526978 63 1.1123805046081543 64 1.2086169719696045 65 1.2542345523834229
		 66 1.3146004676818848 67 1.2387477159500122 68 0.99286019802093517 69 0.49235340952873236
		 70 -0.27869844436645508 71 -1.2513858079910278 72 -2.3572509288787842 73 -3.5278637409210205
		 74 -4.6941981315612793 75 -5.8938016891479492 76 -7.188960075378418 77 -8.5472679138183594
		 78 -9.9365730285644531 79 -11.324918746948242 80 -12.680449485778809;
createNode animCurveTA -n "Character1_Head_rotateY";
	rename -uid "32F9C559-420C-DBD9-1FA8-BEAC76C4BF77";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 0.22921811044216159 1 2.1837983131408691
		 2 4.1282720565795898 3 5.2727794647216797 4 5.4938173294067383 5 5.3080735206604004
		 6 4.903933048248291 7 4.4692025184631348 8 4.192378044128418 9 4.0237874984741211
		 10 3.8158326148986816 11 3.618633508682251 12 3.4770562648773193 13 3.4259264469146729
		 14 3.5014691352844238 15 3.7411501407623295 16 4.0757918357849121 17 4.440345287322998
		 18 4.8789730072021484 19 5.4358339309692383 20 6.1550827026367187 21 7.0471282005310059
		 22 7.9865384101867667 23 8.8136234283447266 24 9.3685836791992187 25 9.5637922286987305
		 26 9.5026817321777344 27 9.3187074661254883 28 9.0493068695068359 29 8.7363262176513672
		 30 8.4124832153320312 31 8.0926284790039062 32 7.7933287620544434 33 7.530798912048339
		 34 7.3211708068847656 35 7.0785055160522461 36 6.7620906829833984 37 6.446113109588623
		 38 6.2208142280578613 39 6.1764278411865234 40 6.3690118789672852 41 6.7191929817199707
		 42 7.1140842437744141 43 7.4409079551696768 44 7.5870227813720703 45 7.5798745155334464
		 46 7.5177183151245126 47 7.3944635391235352 48 7.2042531967163086 49 6.9410853385925293
		 50 6.5700535774230957 51 6.0810675621032715 52 5.4938020706176758 53 4.8232970237731934
		 54 4.0882453918457031 55 3.3117387294769287 56 2.4136743545532227 57 1.3830100297927856
		 58 0.34965300559997559 59 -0.55660402774810791 60 -1.2062168121337891 61 -1.5889278650283813
		 62 -1.8069487810134888 63 -1.9001519680023193 64 -1.9093679189682007 65 -1.8764646053314209
		 66 -2.0072720050811768 67 -2.0433900356292725 68 -1.9749191999435427 69 -1.6570253372192383
		 70 -1.0437638759613037 71 -0.27381408214569092 72 0.51371419429779053 73 1.1795542240142822
		 74 1.5848550796508789 75 1.7024416923522949 76 1.6371121406555176 77 1.4488390684127808
		 78 1.1976619958877563 79 0.94347470998764049 80 0.74579674005508423;
createNode animCurveTA -n "Character1_Head_rotateZ";
	rename -uid "621E186B-4EF7-3078-EB2E-CDBEBC04E023";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 4.6982064247131348 1 -2.8621387481689453
		 2 -10.2869873046875 3 -14.03141975402832 4 -13.426288604736328 5 -10.822148323059082
		 6 -7.2971057891845712 7 -3.9301307201385494 8 -1.8096312284469607 9 -0.93476033210754395
		 10 -0.4928178191184997 11 -0.16461743414402008 12 0.26748520135879517 13 0.77497315406799316
		 14 1.3297076225280762 15 1.9038740396499632 16 2.5111584663391113 17 3.1563613414764404
		 18 3.7985596656799312 19 4.3970441818237305 20 4.9108867645263672 21 5.3024544715881348
		 22 5.523287296295166 23 5.5150208473205566 24 5.2202968597412109 25 4.6105237007141113
		 26 3.5432310104370117 27 2.0961573123931885 28 0.62906044721603394 29 -0.49973046779632563
		 30 -1.1480778455734253 31 -1.4873934984207153 32 -1.6308268308639526 33 -1.691239595413208
		 34 -1.7812744379043579 35 -1.8578195571899414 36 -1.8488004207611082 37 -1.8207172155380247
		 38 -1.7838739156723022 39 -1.747897744178772 40 -1.7272787094116211 41 -1.7333604097366333
		 42 -1.7706478834152222 43 -1.8424444198608398 44 -1.9508831501007082 45 -2.1703147888183594
		 46 -2.5242350101470947 47 -2.9381606578826904 48 -3.3377094268798828 49 -3.6487259864807129
		 50 -3.7792744636535645 51 -3.5969588756561284 52 -3.1417582035064697 53 -2.564420223236084
		 54 -2.0147228240966797 55 -1.6413354873657227 56 -1.4410552978515625 57 -1.3091846704483032
		 58 -1.2387682199478149 59 -1.2191030979156494 60 -1.2363512516021729 61 -1.2781326770782471
		 62 -1.3385331630706787 63 -1.4117828607559204 64 -1.4921263456344604 65 -1.5741022825241089
		 66 -1.5474127531051636 67 -1.6006371974945068 68 -1.7624655961990356 69 -2.2836234569549561
		 70 -3.2134575843811035 71 -4.279271125793457 72 -5.2026453018188477 73 -5.6951818466186523
		 74 -5.4561185836791992 75 -4.3314847946166992 76 -2.5234344005584717 77 -0.25691649317741394
		 78 2.2400445938110352 79 4.735262393951416 80 6.9920434951782227;
createNode animCurveTU -n "Character1_Head_visibility";
	rename -uid "6A575BF0-4AD4-1875-22D1-E6BFEA8D8DCB";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "jaw_translateX";
	rename -uid "7829F69F-442A-B82C-75B7-5699535DB3DF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 14.004427909851074;
createNode animCurveTL -n "jaw_translateY";
	rename -uid "F1E7D894-48D1-EA66-E49D-769AAE5A661B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.9922315685958317e-015;
createNode animCurveTL -n "jaw_translateZ";
	rename -uid "D969D1EC-4C3A-9376-E3DB-79950C5F7ABF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9713712501773515e-015;
createNode animCurveTU -n "jaw_scaleX";
	rename -uid "774E7D9C-49A6-44B5-2EFA-3280AC73DBD3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleY";
	rename -uid "B9106FE9-4650-7B77-276B-908BA1FE1A9A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleZ";
	rename -uid "4C07C57A-47B7-390F-68A6-BF83858829C6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "jaw_rotateX";
	rename -uid "CB2B5DA8-44B4-ABE7-DE06-268E953BCE90";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 -1.5902774228222262e-014 1 -4.5918960571289062
		 2 -7.3470344543457031 3 -7.6185655593872079 4 -7.4588651657104483 5 -5.4064903259277344
		 6 -1.9719439297606611e-013 7 12.366542816162109 8 29.841629028320312 9 45.700534820556641
		 10 53.218528747558594 11 49.415691375732422 12 38.75225830078125 13 25.663766860961914
		 14 14.585731506347656 15 9.9536828994750977 16 14.192468643188477 17 24.345073699951172
		 18 36.774219512939453 19 47.842632293701172 20 53.913040161132812 21 53.402961730957031
		 22 48.737251281738281 23 42.289619445800781 24 36.433773040771484 25 33.543426513671875
		 26 35.526756286621094 27 40.683853149414063 28 45.976333618164063 29 48.365806579589844
		 30 46.733539581298828 31 42.938968658447266 32 38.41094970703125 33 34.578323364257813
		 34 32.869937896728516 35 34.233592987060547 36 37.716724395751953 37 41.897636413574219
		 38 45.354629516601563 39 46.666007995605469 40 44.711994171142578 41 40.440387725830078
		 42 35.530853271484375 43 31.663063049316406 44 30.51668548583984 45 32.806049346923828
		 46 37.358482360839844 47 43.023162841796875 48 48.649261474609375 49 53.085960388183594
		 50 55.18243408203125 51 53.634677886962891 52 49.140041351318359 53 43.549732208251953
		 54 38.714977264404297 55 36.486976623535156 56 37.904899597167969 57 41.734592437744141
		 58 46.417327880859375 59 50.394363403320312 60 52.106964111328125 61 51.342559814453125
		 62 49.140304565429688 63 45.819061279296875 64 41.697681427001953 65 37.095035552978516
		 66 31.744634628295902 67 25.465415954589844 68 18.70435905456543 69 11.908450126647949
		 70 5.5246691703796387 71 -4.8980540760454327e-013 72 -4.1219868659973145 73 -6.595456600189209
		 74 -7.4200305938720703 75 -6.8703985214233398 76 -5.4963192939758301 77 -3.7100152969360352
		 78 -1.9237117767333984 79 -0.54963189363479614 80 -2.2263883241884809e-014;
createNode animCurveTA -n "jaw_rotateY";
	rename -uid "46018123-4C79-9D7A-5C31-FEAC4989A2B3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 3.8274995694566327e-013 1 3.8274995694566327e-013
		 2 3.8274995694566327e-013 3 3.8274995694566327e-013 4 3.8274995694566327e-013 5 3.8274995694566327e-013
		 6 3.8274995694566327e-013 7 3.8274995694566327e-013 8 3.8274995694566327e-013 9 3.8274995694566327e-013
		 10 3.8274995694566327e-013 11 3.8274995694566327e-013 12 3.8274995694566327e-013
		 13 3.8274995694566327e-013 14 3.8274995694566327e-013 15 3.8274995694566327e-013
		 16 3.8274995694566327e-013 17 3.8274995694566327e-013 18 3.8274995694566327e-013
		 19 3.8274995694566327e-013 20 3.8274995694566327e-013 21 3.8274995694566327e-013
		 22 3.8274995694566327e-013 23 3.8274995694566327e-013 24 3.8274995694566327e-013
		 25 3.8274995694566327e-013 26 3.8274995694566327e-013 27 3.8274995694566327e-013
		 28 3.8274995694566327e-013 29 3.8274995694566327e-013 30 3.8274995694566327e-013
		 31 3.8274995694566327e-013 32 3.8274995694566327e-013 33 3.8274995694566327e-013
		 34 3.8274995694566327e-013 35 3.8274995694566327e-013 36 3.8274995694566327e-013
		 37 3.8274995694566327e-013 38 3.8274995694566327e-013 39 3.8274995694566327e-013
		 40 3.8274995694566327e-013 41 3.8274995694566327e-013 42 3.8274995694566327e-013
		 43 3.8274995694566327e-013 44 3.8274995694566327e-013 45 3.8274995694566327e-013
		 46 3.8274995694566327e-013 47 3.8274995694566327e-013 48 3.8274995694566327e-013
		 49 3.8274995694566327e-013 50 3.8274995694566327e-013 51 3.8274995694566327e-013
		 52 3.8274995694566327e-013 53 3.8274995694566327e-013 54 3.8274995694566327e-013
		 55 3.8274995694566327e-013 56 3.8274995694566327e-013 57 3.8274995694566327e-013
		 58 3.8274995694566327e-013 59 3.8274995694566327e-013 60 3.8274995694566327e-013
		 61 3.8274995694566327e-013 62 3.8274995694566327e-013 63 3.8274995694566327e-013
		 64 3.8274995694566327e-013 65 3.8274995694566327e-013 66 3.8274995694566327e-013
		 67 3.8274995694566327e-013 68 3.8274995694566327e-013 69 3.8274995694566327e-013
		 70 3.8274995694566327e-013 71 3.8274995694566327e-013 72 3.8274995694566327e-013
		 73 3.8274995694566327e-013 74 3.8274995694566327e-013 75 3.8274995694566327e-013
		 76 3.8274995694566327e-013 77 3.8274995694566327e-013 78 3.8274995694566327e-013
		 79 3.8274995694566327e-013 80 3.8274995694566327e-013;
createNode animCurveTA -n "jaw_rotateZ";
	rename -uid "A37B4CE2-44F7-8488-9CF6-E4A04DF0DCB2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  0 4.1771753699244856e-013 1 4.1771753699244856e-013
		 2 4.1771753699244856e-013 3 4.1771753699244856e-013 4 4.1771753699244856e-013 5 4.1771753699244856e-013
		 6 4.1771753699244856e-013 7 4.1771753699244856e-013 8 4.1771753699244856e-013 9 4.1771753699244856e-013
		 10 4.1771753699244856e-013 11 4.1771753699244856e-013 12 4.1771753699244856e-013
		 13 4.1771753699244856e-013 14 4.1771753699244856e-013 15 4.1771753699244856e-013
		 16 4.1771753699244856e-013 17 4.1771753699244856e-013 18 4.1771753699244856e-013
		 19 4.1771753699244856e-013 20 4.1771753699244856e-013 21 4.1771753699244856e-013
		 22 4.1771753699244856e-013 23 4.1771753699244856e-013 24 4.1771753699244856e-013
		 25 4.1771753699244856e-013 26 4.1771753699244856e-013 27 4.1771753699244856e-013
		 28 4.1771753699244856e-013 29 4.1771753699244856e-013 30 4.1771753699244856e-013
		 31 4.1771753699244856e-013 32 4.1771753699244856e-013 33 4.1771753699244856e-013
		 34 4.1771753699244856e-013 35 4.1771753699244856e-013 36 4.1771753699244856e-013
		 37 4.1771753699244856e-013 38 4.1771753699244856e-013 39 4.1771753699244856e-013
		 40 4.1771753699244856e-013 41 4.1771753699244856e-013 42 4.1771753699244856e-013
		 43 4.1771753699244856e-013 44 4.1771753699244856e-013 45 4.1771753699244856e-013
		 46 4.1771753699244856e-013 47 4.1771753699244856e-013 48 4.1771753699244856e-013
		 49 4.1771753699244856e-013 50 4.1771753699244856e-013 51 4.1771753699244856e-013
		 52 4.1771753699244856e-013 53 4.1771753699244856e-013 54 4.1771753699244856e-013
		 55 4.1771753699244856e-013 56 4.1771753699244856e-013 57 4.1771753699244856e-013
		 58 4.1771753699244856e-013 59 4.1771753699244856e-013 60 4.1771753699244856e-013
		 61 4.1771753699244856e-013 62 4.1771753699244856e-013 63 4.1771753699244856e-013
		 64 4.1771753699244856e-013 65 4.1771753699244856e-013 66 4.1771753699244856e-013
		 67 4.1771753699244856e-013 68 4.1771753699244856e-013 69 4.1771753699244856e-013
		 70 4.1771753699244856e-013 71 4.1771753699244856e-013 72 4.1771753699244856e-013
		 73 4.1771753699244856e-013 74 4.1771753699244856e-013 75 4.1771753699244856e-013
		 76 4.1771753699244856e-013 77 4.1771753699244856e-013 78 4.1771753699244856e-013
		 79 4.1771753699244856e-013 80 4.1771753699244856e-013;
createNode animCurveTU -n "jaw_visibility";
	rename -uid "7A72E054-44F3-DE69-96EF-5C965913B315";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "eyes_translateX";
	rename -uid "C4F18980-4F5F-F771-58CE-ECA5C5D5D649";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -10.20706844329834 2 -10.20706844329834
		 3 -11.907955169677734 4 -11.907955169677734 7 -11.907955169677734 8 -11.907955169677734
		 9 -10.20706844329834 10 -10.20706844329834;
createNode animCurveTL -n "eyes_translateY";
	rename -uid "94BD5186-4FF0-24A7-8A0D-8A8106EEEC74";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 22.342548370361328 2 22.342548370361328
		 3 19.886955261230469 4 19.886955261230469 7 19.886955261230469 8 19.886955261230469
		 9 22.342548370361328 10 22.342548370361328;
createNode animCurveTL -n "eyes_translateZ";
	rename -uid "9964C4DB-4F6B-8D2D-C760-CBBA5F0BAAF2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 3.7847360800924434e-008 2 3.7847360800924434e-008
		 3 0.23704111576080322 4 0.23704111576080322 7 0.23704111576080322 8 0.23704111576080322
		 9 3.7847360800924434e-008 10 3.7847360800924434e-008;
createNode animCurveTU -n "eyes_scaleX";
	rename -uid "A38AACFD-4BDB-36C2-78C1-82B5716A9CB1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleY";
	rename -uid "6B5CF613-43D4-3F36-5092-2A8BBB389E40";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleZ";
	rename -uid "6A8375B7-4111-7013-8EF4-0FA5FAD04B3C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "eyes_rotateX";
	rename -uid "18FF19EA-4C68-1967-0859-AC9DCF4DE20A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 74 ".ktv[0:73]"  7 0 8 0 9 -7.1108965873718262 10 -7.4529247283935547
		 11 -7.6848545074462882 12 -7.8192996978759757 13 -7.8688750267028809 14 -7.8461952209472665
		 15 -7.7638740539550781 16 -7.6345262527465811 17 -7.4707670211791992 18 -7.2852091789245605
		 19 -7.0904684066772461 20 -6.8991589546203613 21 -6.7238945960998535 22 -6.5772910118103027
		 23 -6.4719610214233398 24 -6.4205207824707031 25 -6.4103498458862305 26 -6.4177422523498535
		 27 -6.4386816024780273 28 -6.4691519737243652 29 -6.5051369667053223 30 -6.5426216125488281
		 31 -6.5775885581970215 32 -6.6060218811035156 33 -6.623906135559082 34 -6.6375322341918945
		 35 -6.6551351547241211 36 -6.6756119728088379 37 -6.6978631019592285 38 -6.7207860946655273
		 39 -6.7432804107666016 40 -6.7642431259155273 41 -6.7825736999511719 42 -6.7971706390380859
		 43 -6.8069334030151367 44 -6.8107590675354004 45 -6.8075461387634277 46 -6.7961945533752441
		 47 -6.775601863861084 48 -6.7446670532226563 49 -6.7022881507873535 50 -6.6208901405334473
		 51 -6.4848251342773437 52 -6.3108830451965332 53 -6.1158552169799805 54 -5.9165301322937012
		 55 -5.7296991348266602 56 -5.5721530914306641 57 -5.4606809616088867 58 -5.4792613983154297
		 59 -5.6535429954528809 60 -5.9128227233886719 61 -6.1863956451416016 62 -6.4035592079162598
		 63 -6.4936094284057617 64 -6.3858428001403809 65 -6.0095562934875488 66 -5.2940454483032227
		 67 -2.0860116481781006 68 2.9023725986480713 69 5.4436039924621582 70 4.5923957824707031
		 71 2.5848376750946045 72 -0.034508466720581055 73 -2.7210812568664551 74 -4.9303193092346191
		 75 -6.1176609992980957 76 -5.9939084053039551 77 -4.9078483581542969 78 -3.2718989849090576
		 79 -1.4984771013259888 80 0;
createNode animCurveTA -n "eyes_rotateY";
	rename -uid "14CBA1B4-4452-9AE3-E274-008BBF013674";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 74 ".ktv[0:73]"  7 0 8 0 9 -0.3219248354434967 10 -0.3966141939163208
		 11 -0.47531193494796758 12 -0.55646103620529175 13 -0.63850432634353638 14 -0.71988457441329956
		 15 -0.79904484748840332 16 -0.8744279146194458 17 -0.94447654485702515 18 -1.0076338052749634
		 19 -1.0623424053192139 20 -1.1070454120635986 21 -1.1401854753494263 22 -1.1602054834365845
		 23 -1.1655484437942505 24 -1.154657244682312 25 -1.0880701541900635 26 -0.94394510984420765
		 27 -0.74715423583984375 28 -0.52256977558135986 29 -0.29506367444992065 30 -0.089508257806301117
		 31 0.069224394857883453 32 0.15626212954521179 33 0.14673282206058502 34 0.059337291866540902
		 35 -0.066939637064933777 36 -0.22654418647289276 37 -0.41392260789871216 38 -0.62352108955383301
		 39 -0.84978580474853516 40 -1.0871630907058716 41 -1.3300988674163818 42 -1.5730400085449219
		 43 -1.8104318380355835 44 -2.0367212295532227 45 -2.2463541030883789 46 -2.4337770938873291
		 47 -2.593435525894165 48 -2.7197768688201904 49 -2.807246208190918 50 -2.8227164745330811
		 51 -2.7488205432891846 52 -2.6036460399627686 53 -2.4052798748016357 54 -2.1718094348907471
		 55 -1.9213217496871948 56 -1.6719043254852295 57 -1.4416441917419434 58 -1.2288709878921509
		 59 -1.016745924949646 60 -0.80060404539108276 61 -0.57578027248382568 62 -0.33760952949523926
		 63 -0.08142688125371933 64 0.19743281602859497 65 0.50363451242446899 66 0.84184348583221436
		 67 1.6532379388809204 68 2.6687257289886475 69 2.8275649547576904 70 1.8681148290634157
		 71 0.41933843493461609 72 -1.2436002492904663 73 -2.8455367088317871 74 -4.1113061904907227
		 75 -4.765744686126709 76 -4.5871238708496094 77 -3.7307910919189453 78 -2.4871940612792969
		 79 -1.1467809677124023 80 0;
createNode animCurveTA -n "eyes_rotateZ";
	rename -uid "F147DD59-41CB-A4BA-33A2-C196A8570F4C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 73 ".ktv[0:72]"  7 0 8 0 9 0.033448539674282074 10 0.013669262640178204
		 11 -0.0067791310139000416 12 -0.027846863493323326 13 -0.04948415607213974 14 -0.071641236543655396
		 15 -0.094268336892127991 16 -0.11731567978858946 17 -0.14073348045349121 18 -0.16447199881076813
		 19 -0.18848139047622681 20 -0.21271194517612457 21 -0.23711386322975156 22 -0.26163738965988159
		 23 -0.28623270988464355 24 -0.31085008382797241 25 -0.3371831476688385 26 -0.36625170707702637
		 27 -0.39699512720108032 28 -0.42835283279418945 29 -0.45926421880722051 30 -0.48866873979568476
		 31 -0.51550567150115967 32 -0.53871452808380127 33 -0.55723464488983154 34 -0.57139569520950317
		 35 -0.58256906270980835 36 -0.59125667810440063 37 -0.5979607105255127 38 -0.60318291187286377
		 39 -0.60742545127868652 41 -0.61497938632965088 42 -0.61929476261138916 43 -0.62463843822479248
		 44 -0.6315123438835144 45 -0.6404186487197876 46 -0.65185916423797607 47 -0.66633594036102295
		 48 -0.68435102701187134 49 -0.70640641450881958 50 -0.73820459842681885 51 -0.78292739391326904
		 52 -0.8372960090637207 53 -0.89803147315979004 54 -0.96185475587844849 55 -1.025486946105957
		 56 -1.0856492519378662 57 -1.1390625238418579 58 -1.1542634963989258 59 -1.1185933351516724
		 60 -1.0569803714752197 61 -0.99435293674469005 62 -0.95563918352127086 63 -0.96576744318008423
		 64 -1.0496659278869629 65 -1.2322629690170288 66 -1.5384868383407593 67 -2.7056617736816406
		 68 -4.536768913269043 69 -5.6552224159240723 70 -5.8344602584838867 71 -5.7936010360717773
		 72 -5.5745782852172852 73 -5.2193260192871094 74 -4.7697782516479492 75 -4.2678699493408203
		 76 -3.63155198097229 77 -2.8051352500915527 78 -1.870090126991272 79 -0.90788805484771717
		 80 0;
createNode animCurveTU -n "eyes_visibility";
	rename -uid "8FACCA52-45B4-61AA-5618-98B59CF27232";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "ECA5697A-4EE2-F3CF-8B19-1D9BF1070B1B";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "7D696F9C-4D13-246B-307C-2CB4EBBBE5F2";
createNode displayLayer -n "defaultLayer";
	rename -uid "05B03DCD-497C-80CA-4387-BCA4068975E9";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "D8B3F593-46D7-68AB-2CC4-24B9BFF2AD86";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "9E8A0878-4274-04F5-DF2F-13920F13D437";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "E2C994E3-42B3-0226-2D83-46889CA67793";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 0\n                -polymeshes 1\n                -subdivSurfaces 0\n                -planes 0\n                -lights 0\n                -cameras 0\n                -controlVertices 0\n                -hulls 0\n                -grid 1\n                -imagePlane 0\n                -joints 1\n"
		+ "                -ikHandles 0\n                -deformers 0\n                -dynamics 0\n                -particleInstancers 0\n                -fluids 0\n                -hairSystems 0\n                -follicles 0\n                -nCloths 0\n                -nParticles 0\n                -nRigids 0\n                -dynamicConstraints 0\n                -locators 0\n                -manipulators 1\n                -pluginShapes 0\n                -dimensions 0\n                -handles 0\n                -pivots 0\n                -textures 0\n                -strokes 0\n                -motionTrails 0\n                -clipGhosts 0\n                -greasePencils 0\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1239\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 0 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 0\n            -polymeshes 1\n            -subdivSurfaces 0\n"
		+ "            -planes 0\n            -lights 0\n            -cameras 0\n            -controlVertices 0\n            -hulls 0\n            -grid 1\n            -imagePlane 0\n            -joints 1\n            -ikHandles 0\n            -deformers 0\n            -dynamics 0\n            -particleInstancers 0\n            -fluids 0\n            -hairSystems 0\n            -follicles 0\n            -nCloths 0\n            -nParticles 0\n            -nRigids 0\n            -dynamicConstraints 0\n            -locators 0\n            -manipulators 1\n            -pluginShapes 0\n            -dimensions 0\n            -handles 0\n            -pivots 0\n            -textures 0\n            -strokes 0\n            -motionTrails 0\n            -clipGhosts 0\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1239\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 0 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n"
		+ "                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n"
		+ "                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n"
		+ "                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n"
		+ "                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 24 100 -ps 2 76 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 0\\n    -polymeshes 1\\n    -subdivSurfaces 0\\n    -planes 0\\n    -lights 0\\n    -cameras 0\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 1\\n    -ikHandles 0\\n    -deformers 0\\n    -dynamics 0\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 0\\n    -handles 0\\n    -pivots 0\\n    -textures 0\\n    -strokes 0\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1239\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 0 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 0\\n    -polymeshes 1\\n    -subdivSurfaces 0\\n    -planes 0\\n    -lights 0\\n    -cameras 0\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 1\\n    -ikHandles 0\\n    -deformers 0\\n    -dynamics 0\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 0\\n    -handles 0\\n    -pivots 0\\n    -textures 0\\n    -strokes 0\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1239\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 0 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "258C71B4-40A5-4DC4-6720-47A874847BE0";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 80 -ast 0 -aet 130 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 0;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
connectAttr "ref_frame.s" "Character1_Hips.is";
connectAttr "Character1_Hips_scaleX.o" "Character1_Hips.sx";
connectAttr "Character1_Hips_scaleY.o" "Character1_Hips.sy";
connectAttr "Character1_Hips_scaleZ.o" "Character1_Hips.sz";
connectAttr "Character1_Hips_translateX.o" "Character1_Hips.tx";
connectAttr "Character1_Hips_translateY.o" "Character1_Hips.ty";
connectAttr "Character1_Hips_translateZ.o" "Character1_Hips.tz";
connectAttr "Character1_Hips_rotateX.o" "Character1_Hips.rx";
connectAttr "Character1_Hips_rotateY.o" "Character1_Hips.ry";
connectAttr "Character1_Hips_rotateZ.o" "Character1_Hips.rz";
connectAttr "Character1_Hips_visibility.o" "Character1_Hips.v";
connectAttr "Character1_Hips.s" "Character1_LeftUpLeg.is";
connectAttr "Character1_LeftUpLeg_scaleX.o" "Character1_LeftUpLeg.sx";
connectAttr "Character1_LeftUpLeg_scaleY.o" "Character1_LeftUpLeg.sy";
connectAttr "Character1_LeftUpLeg_scaleZ.o" "Character1_LeftUpLeg.sz";
connectAttr "Character1_LeftUpLeg_translateX.o" "Character1_LeftUpLeg.tx";
connectAttr "Character1_LeftUpLeg_translateY.o" "Character1_LeftUpLeg.ty";
connectAttr "Character1_LeftUpLeg_translateZ.o" "Character1_LeftUpLeg.tz";
connectAttr "Character1_LeftUpLeg_rotateX.o" "Character1_LeftUpLeg.rx";
connectAttr "Character1_LeftUpLeg_rotateY.o" "Character1_LeftUpLeg.ry";
connectAttr "Character1_LeftUpLeg_rotateZ.o" "Character1_LeftUpLeg.rz";
connectAttr "Character1_LeftUpLeg_visibility.o" "Character1_LeftUpLeg.v";
connectAttr "Character1_LeftUpLeg.s" "Character1_LeftLeg.is";
connectAttr "Character1_LeftLeg_scaleX.o" "Character1_LeftLeg.sx";
connectAttr "Character1_LeftLeg_scaleY.o" "Character1_LeftLeg.sy";
connectAttr "Character1_LeftLeg_scaleZ.o" "Character1_LeftLeg.sz";
connectAttr "Character1_LeftLeg_translateX.o" "Character1_LeftLeg.tx";
connectAttr "Character1_LeftLeg_translateY.o" "Character1_LeftLeg.ty";
connectAttr "Character1_LeftLeg_translateZ.o" "Character1_LeftLeg.tz";
connectAttr "Character1_LeftLeg_rotateX.o" "Character1_LeftLeg.rx";
connectAttr "Character1_LeftLeg_rotateY.o" "Character1_LeftLeg.ry";
connectAttr "Character1_LeftLeg_rotateZ.o" "Character1_LeftLeg.rz";
connectAttr "Character1_LeftLeg_visibility.o" "Character1_LeftLeg.v";
connectAttr "Character1_LeftLeg.s" "Character1_LeftFoot.is";
connectAttr "Character1_LeftFoot_scaleX.o" "Character1_LeftFoot.sx";
connectAttr "Character1_LeftFoot_scaleY.o" "Character1_LeftFoot.sy";
connectAttr "Character1_LeftFoot_scaleZ.o" "Character1_LeftFoot.sz";
connectAttr "Character1_LeftFoot_translateX.o" "Character1_LeftFoot.tx";
connectAttr "Character1_LeftFoot_translateY.o" "Character1_LeftFoot.ty";
connectAttr "Character1_LeftFoot_translateZ.o" "Character1_LeftFoot.tz";
connectAttr "Character1_LeftFoot_rotateX.o" "Character1_LeftFoot.rx";
connectAttr "Character1_LeftFoot_rotateY.o" "Character1_LeftFoot.ry";
connectAttr "Character1_LeftFoot_rotateZ.o" "Character1_LeftFoot.rz";
connectAttr "Character1_LeftFoot_visibility.o" "Character1_LeftFoot.v";
connectAttr "Character1_LeftFoot.s" "Character1_LeftToeBase.is";
connectAttr "Character1_LeftToeBase_scaleX.o" "Character1_LeftToeBase.sx";
connectAttr "Character1_LeftToeBase_scaleY.o" "Character1_LeftToeBase.sy";
connectAttr "Character1_LeftToeBase_scaleZ.o" "Character1_LeftToeBase.sz";
connectAttr "Character1_LeftToeBase_translateX.o" "Character1_LeftToeBase.tx";
connectAttr "Character1_LeftToeBase_translateY.o" "Character1_LeftToeBase.ty";
connectAttr "Character1_LeftToeBase_translateZ.o" "Character1_LeftToeBase.tz";
connectAttr "Character1_LeftToeBase_rotateX.o" "Character1_LeftToeBase.rx";
connectAttr "Character1_LeftToeBase_rotateY.o" "Character1_LeftToeBase.ry";
connectAttr "Character1_LeftToeBase_rotateZ.o" "Character1_LeftToeBase.rz";
connectAttr "Character1_LeftToeBase_visibility.o" "Character1_LeftToeBase.v";
connectAttr "Character1_LeftToeBase.s" "Character1_LeftFootMiddle1.is";
connectAttr "Character1_LeftFootMiddle1_scaleX.o" "Character1_LeftFootMiddle1.sx"
		;
connectAttr "Character1_LeftFootMiddle1_scaleY.o" "Character1_LeftFootMiddle1.sy"
		;
connectAttr "Character1_LeftFootMiddle1_scaleZ.o" "Character1_LeftFootMiddle1.sz"
		;
connectAttr "Character1_LeftFootMiddle1_translateX.o" "Character1_LeftFootMiddle1.tx"
		;
connectAttr "Character1_LeftFootMiddle1_translateY.o" "Character1_LeftFootMiddle1.ty"
		;
connectAttr "Character1_LeftFootMiddle1_translateZ.o" "Character1_LeftFootMiddle1.tz"
		;
connectAttr "Character1_LeftFootMiddle1_rotateX.o" "Character1_LeftFootMiddle1.rx"
		;
connectAttr "Character1_LeftFootMiddle1_rotateY.o" "Character1_LeftFootMiddle1.ry"
		;
connectAttr "Character1_LeftFootMiddle1_rotateZ.o" "Character1_LeftFootMiddle1.rz"
		;
connectAttr "Character1_LeftFootMiddle1_visibility.o" "Character1_LeftFootMiddle1.v"
		;
connectAttr "Character1_LeftFootMiddle1.s" "Character1_LeftFootMiddle2.is";
connectAttr "Character1_LeftFootMiddle2_translateX.o" "Character1_LeftFootMiddle2.tx"
		;
connectAttr "Character1_LeftFootMiddle2_translateY.o" "Character1_LeftFootMiddle2.ty"
		;
connectAttr "Character1_LeftFootMiddle2_translateZ.o" "Character1_LeftFootMiddle2.tz"
		;
connectAttr "Character1_LeftFootMiddle2_scaleX.o" "Character1_LeftFootMiddle2.sx"
		;
connectAttr "Character1_LeftFootMiddle2_scaleY.o" "Character1_LeftFootMiddle2.sy"
		;
connectAttr "Character1_LeftFootMiddle2_scaleZ.o" "Character1_LeftFootMiddle2.sz"
		;
connectAttr "Character1_LeftFootMiddle2_rotateX.o" "Character1_LeftFootMiddle2.rx"
		;
connectAttr "Character1_LeftFootMiddle2_rotateY.o" "Character1_LeftFootMiddle2.ry"
		;
connectAttr "Character1_LeftFootMiddle2_rotateZ.o" "Character1_LeftFootMiddle2.rz"
		;
connectAttr "Character1_LeftFootMiddle2_visibility.o" "Character1_LeftFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_RightUpLeg.is";
connectAttr "Character1_RightUpLeg_scaleX.o" "Character1_RightUpLeg.sx";
connectAttr "Character1_RightUpLeg_scaleY.o" "Character1_RightUpLeg.sy";
connectAttr "Character1_RightUpLeg_scaleZ.o" "Character1_RightUpLeg.sz";
connectAttr "Character1_RightUpLeg_translateX.o" "Character1_RightUpLeg.tx";
connectAttr "Character1_RightUpLeg_translateY.o" "Character1_RightUpLeg.ty";
connectAttr "Character1_RightUpLeg_translateZ.o" "Character1_RightUpLeg.tz";
connectAttr "Character1_RightUpLeg_rotateX.o" "Character1_RightUpLeg.rx";
connectAttr "Character1_RightUpLeg_rotateY.o" "Character1_RightUpLeg.ry";
connectAttr "Character1_RightUpLeg_rotateZ.o" "Character1_RightUpLeg.rz";
connectAttr "Character1_RightUpLeg_visibility.o" "Character1_RightUpLeg.v";
connectAttr "Character1_RightUpLeg.s" "Character1_RightLeg.is";
connectAttr "Character1_RightLeg_scaleX.o" "Character1_RightLeg.sx";
connectAttr "Character1_RightLeg_scaleY.o" "Character1_RightLeg.sy";
connectAttr "Character1_RightLeg_scaleZ.o" "Character1_RightLeg.sz";
connectAttr "Character1_RightLeg_translateX.o" "Character1_RightLeg.tx";
connectAttr "Character1_RightLeg_translateY.o" "Character1_RightLeg.ty";
connectAttr "Character1_RightLeg_translateZ.o" "Character1_RightLeg.tz";
connectAttr "Character1_RightLeg_rotateX.o" "Character1_RightLeg.rx";
connectAttr "Character1_RightLeg_rotateY.o" "Character1_RightLeg.ry";
connectAttr "Character1_RightLeg_rotateZ.o" "Character1_RightLeg.rz";
connectAttr "Character1_RightLeg_visibility.o" "Character1_RightLeg.v";
connectAttr "Character1_RightLeg.s" "Character1_RightFoot.is";
connectAttr "Character1_RightFoot_scaleX.o" "Character1_RightFoot.sx";
connectAttr "Character1_RightFoot_scaleY.o" "Character1_RightFoot.sy";
connectAttr "Character1_RightFoot_scaleZ.o" "Character1_RightFoot.sz";
connectAttr "Character1_RightFoot_translateX.o" "Character1_RightFoot.tx";
connectAttr "Character1_RightFoot_translateY.o" "Character1_RightFoot.ty";
connectAttr "Character1_RightFoot_translateZ.o" "Character1_RightFoot.tz";
connectAttr "Character1_RightFoot_rotateX.o" "Character1_RightFoot.rx";
connectAttr "Character1_RightFoot_rotateY.o" "Character1_RightFoot.ry";
connectAttr "Character1_RightFoot_rotateZ.o" "Character1_RightFoot.rz";
connectAttr "Character1_RightFoot_visibility.o" "Character1_RightFoot.v";
connectAttr "Character1_RightFoot.s" "Character1_RightToeBase.is";
connectAttr "Character1_RightToeBase_scaleX.o" "Character1_RightToeBase.sx";
connectAttr "Character1_RightToeBase_scaleY.o" "Character1_RightToeBase.sy";
connectAttr "Character1_RightToeBase_scaleZ.o" "Character1_RightToeBase.sz";
connectAttr "Character1_RightToeBase_translateX.o" "Character1_RightToeBase.tx";
connectAttr "Character1_RightToeBase_translateY.o" "Character1_RightToeBase.ty";
connectAttr "Character1_RightToeBase_translateZ.o" "Character1_RightToeBase.tz";
connectAttr "Character1_RightToeBase_rotateX.o" "Character1_RightToeBase.rx";
connectAttr "Character1_RightToeBase_rotateY.o" "Character1_RightToeBase.ry";
connectAttr "Character1_RightToeBase_rotateZ.o" "Character1_RightToeBase.rz";
connectAttr "Character1_RightToeBase_visibility.o" "Character1_RightToeBase.v";
connectAttr "Character1_RightToeBase.s" "Character1_RightFootMiddle1.is";
connectAttr "Character1_RightFootMiddle1_scaleX.o" "Character1_RightFootMiddle1.sx"
		;
connectAttr "Character1_RightFootMiddle1_scaleY.o" "Character1_RightFootMiddle1.sy"
		;
connectAttr "Character1_RightFootMiddle1_scaleZ.o" "Character1_RightFootMiddle1.sz"
		;
connectAttr "Character1_RightFootMiddle1_translateX.o" "Character1_RightFootMiddle1.tx"
		;
connectAttr "Character1_RightFootMiddle1_translateY.o" "Character1_RightFootMiddle1.ty"
		;
connectAttr "Character1_RightFootMiddle1_translateZ.o" "Character1_RightFootMiddle1.tz"
		;
connectAttr "Character1_RightFootMiddle1_rotateX.o" "Character1_RightFootMiddle1.rx"
		;
connectAttr "Character1_RightFootMiddle1_rotateY.o" "Character1_RightFootMiddle1.ry"
		;
connectAttr "Character1_RightFootMiddle1_rotateZ.o" "Character1_RightFootMiddle1.rz"
		;
connectAttr "Character1_RightFootMiddle1_visibility.o" "Character1_RightFootMiddle1.v"
		;
connectAttr "Character1_RightFootMiddle1.s" "Character1_RightFootMiddle2.is";
connectAttr "Character1_RightFootMiddle2_translateX.o" "Character1_RightFootMiddle2.tx"
		;
connectAttr "Character1_RightFootMiddle2_translateY.o" "Character1_RightFootMiddle2.ty"
		;
connectAttr "Character1_RightFootMiddle2_translateZ.o" "Character1_RightFootMiddle2.tz"
		;
connectAttr "Character1_RightFootMiddle2_scaleX.o" "Character1_RightFootMiddle2.sx"
		;
connectAttr "Character1_RightFootMiddle2_scaleY.o" "Character1_RightFootMiddle2.sy"
		;
connectAttr "Character1_RightFootMiddle2_scaleZ.o" "Character1_RightFootMiddle2.sz"
		;
connectAttr "Character1_RightFootMiddle2_rotateX.o" "Character1_RightFootMiddle2.rx"
		;
connectAttr "Character1_RightFootMiddle2_rotateY.o" "Character1_RightFootMiddle2.ry"
		;
connectAttr "Character1_RightFootMiddle2_rotateZ.o" "Character1_RightFootMiddle2.rz"
		;
connectAttr "Character1_RightFootMiddle2_visibility.o" "Character1_RightFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_Spine.is";
connectAttr "Character1_Spine_scaleX.o" "Character1_Spine.sx";
connectAttr "Character1_Spine_scaleY.o" "Character1_Spine.sy";
connectAttr "Character1_Spine_scaleZ.o" "Character1_Spine.sz";
connectAttr "Character1_Spine_translateX.o" "Character1_Spine.tx";
connectAttr "Character1_Spine_translateY.o" "Character1_Spine.ty";
connectAttr "Character1_Spine_translateZ.o" "Character1_Spine.tz";
connectAttr "Character1_Spine_rotateX.o" "Character1_Spine.rx";
connectAttr "Character1_Spine_rotateY.o" "Character1_Spine.ry";
connectAttr "Character1_Spine_rotateZ.o" "Character1_Spine.rz";
connectAttr "Character1_Spine_visibility.o" "Character1_Spine.v";
connectAttr "Character1_Spine.s" "Character1_Spine1.is";
connectAttr "Character1_Spine1_scaleX.o" "Character1_Spine1.sx";
connectAttr "Character1_Spine1_scaleY.o" "Character1_Spine1.sy";
connectAttr "Character1_Spine1_scaleZ.o" "Character1_Spine1.sz";
connectAttr "Character1_Spine1_translateX.o" "Character1_Spine1.tx";
connectAttr "Character1_Spine1_translateY.o" "Character1_Spine1.ty";
connectAttr "Character1_Spine1_translateZ.o" "Character1_Spine1.tz";
connectAttr "Character1_Spine1_rotateX.o" "Character1_Spine1.rx";
connectAttr "Character1_Spine1_rotateY.o" "Character1_Spine1.ry";
connectAttr "Character1_Spine1_rotateZ.o" "Character1_Spine1.rz";
connectAttr "Character1_Spine1_visibility.o" "Character1_Spine1.v";
connectAttr "Character1_Spine1.s" "Character1_LeftShoulder.is";
connectAttr "Character1_LeftShoulder_scaleX.o" "Character1_LeftShoulder.sx";
connectAttr "Character1_LeftShoulder_scaleY.o" "Character1_LeftShoulder.sy";
connectAttr "Character1_LeftShoulder_scaleZ.o" "Character1_LeftShoulder.sz";
connectAttr "Character1_LeftShoulder_translateX.o" "Character1_LeftShoulder.tx";
connectAttr "Character1_LeftShoulder_translateY.o" "Character1_LeftShoulder.ty";
connectAttr "Character1_LeftShoulder_translateZ.o" "Character1_LeftShoulder.tz";
connectAttr "Character1_LeftShoulder_rotateX.o" "Character1_LeftShoulder.rx";
connectAttr "Character1_LeftShoulder_rotateY.o" "Character1_LeftShoulder.ry";
connectAttr "Character1_LeftShoulder_rotateZ.o" "Character1_LeftShoulder.rz";
connectAttr "Character1_LeftShoulder_visibility.o" "Character1_LeftShoulder.v";
connectAttr "Character1_LeftShoulder.s" "Character1_LeftArm.is";
connectAttr "Character1_LeftArm_scaleX.o" "Character1_LeftArm.sx";
connectAttr "Character1_LeftArm_scaleY.o" "Character1_LeftArm.sy";
connectAttr "Character1_LeftArm_scaleZ.o" "Character1_LeftArm.sz";
connectAttr "Character1_LeftArm_translateX.o" "Character1_LeftArm.tx";
connectAttr "Character1_LeftArm_translateY.o" "Character1_LeftArm.ty";
connectAttr "Character1_LeftArm_translateZ.o" "Character1_LeftArm.tz";
connectAttr "Character1_LeftArm_rotateX.o" "Character1_LeftArm.rx";
connectAttr "Character1_LeftArm_rotateY.o" "Character1_LeftArm.ry";
connectAttr "Character1_LeftArm_rotateZ.o" "Character1_LeftArm.rz";
connectAttr "Character1_LeftArm_visibility.o" "Character1_LeftArm.v";
connectAttr "Character1_LeftArm.s" "Character1_LeftForeArm.is";
connectAttr "Character1_LeftForeArm_scaleX.o" "Character1_LeftForeArm.sx";
connectAttr "Character1_LeftForeArm_scaleY.o" "Character1_LeftForeArm.sy";
connectAttr "Character1_LeftForeArm_scaleZ.o" "Character1_LeftForeArm.sz";
connectAttr "Character1_LeftForeArm_translateX.o" "Character1_LeftForeArm.tx";
connectAttr "Character1_LeftForeArm_translateY.o" "Character1_LeftForeArm.ty";
connectAttr "Character1_LeftForeArm_translateZ.o" "Character1_LeftForeArm.tz";
connectAttr "Character1_LeftForeArm_rotateX.o" "Character1_LeftForeArm.rx";
connectAttr "Character1_LeftForeArm_rotateY.o" "Character1_LeftForeArm.ry";
connectAttr "Character1_LeftForeArm_rotateZ.o" "Character1_LeftForeArm.rz";
connectAttr "Character1_LeftForeArm_visibility.o" "Character1_LeftForeArm.v";
connectAttr "Character1_LeftForeArm.s" "Character1_LeftHand.is";
connectAttr "Character1_LeftHand_scaleX.o" "Character1_LeftHand.sx";
connectAttr "Character1_LeftHand_scaleY.o" "Character1_LeftHand.sy";
connectAttr "Character1_LeftHand_scaleZ.o" "Character1_LeftHand.sz";
connectAttr "Character1_LeftHand_translateX.o" "Character1_LeftHand.tx";
connectAttr "Character1_LeftHand_translateY.o" "Character1_LeftHand.ty";
connectAttr "Character1_LeftHand_translateZ.o" "Character1_LeftHand.tz";
connectAttr "Character1_LeftHand_rotateX.o" "Character1_LeftHand.rx";
connectAttr "Character1_LeftHand_rotateY.o" "Character1_LeftHand.ry";
connectAttr "Character1_LeftHand_rotateZ.o" "Character1_LeftHand.rz";
connectAttr "Character1_LeftHand_visibility.o" "Character1_LeftHand.v";
connectAttr "Character1_LeftHand.s" "Character1_LeftHandThumb1.is";
connectAttr "Character1_LeftHandThumb1_scaleX.o" "Character1_LeftHandThumb1.sx";
connectAttr "Character1_LeftHandThumb1_scaleY.o" "Character1_LeftHandThumb1.sy";
connectAttr "Character1_LeftHandThumb1_scaleZ.o" "Character1_LeftHandThumb1.sz";
connectAttr "Character1_LeftHandThumb1_translateX.o" "Character1_LeftHandThumb1.tx"
		;
connectAttr "Character1_LeftHandThumb1_translateY.o" "Character1_LeftHandThumb1.ty"
		;
connectAttr "Character1_LeftHandThumb1_translateZ.o" "Character1_LeftHandThumb1.tz"
		;
connectAttr "Character1_LeftHandThumb1_rotateX.o" "Character1_LeftHandThumb1.rx"
		;
connectAttr "Character1_LeftHandThumb1_rotateY.o" "Character1_LeftHandThumb1.ry"
		;
connectAttr "Character1_LeftHandThumb1_rotateZ.o" "Character1_LeftHandThumb1.rz"
		;
connectAttr "Character1_LeftHandThumb1_visibility.o" "Character1_LeftHandThumb1.v"
		;
connectAttr "Character1_LeftHandThumb1.s" "Character1_LeftHandThumb2.is";
connectAttr "Character1_LeftHandThumb2_scaleX.o" "Character1_LeftHandThumb2.sx";
connectAttr "Character1_LeftHandThumb2_scaleY.o" "Character1_LeftHandThumb2.sy";
connectAttr "Character1_LeftHandThumb2_scaleZ.o" "Character1_LeftHandThumb2.sz";
connectAttr "Character1_LeftHandThumb2_translateX.o" "Character1_LeftHandThumb2.tx"
		;
connectAttr "Character1_LeftHandThumb2_translateY.o" "Character1_LeftHandThumb2.ty"
		;
connectAttr "Character1_LeftHandThumb2_translateZ.o" "Character1_LeftHandThumb2.tz"
		;
connectAttr "Character1_LeftHandThumb2_rotateX.o" "Character1_LeftHandThumb2.rx"
		;
connectAttr "Character1_LeftHandThumb2_rotateY.o" "Character1_LeftHandThumb2.ry"
		;
connectAttr "Character1_LeftHandThumb2_rotateZ.o" "Character1_LeftHandThumb2.rz"
		;
connectAttr "Character1_LeftHandThumb2_visibility.o" "Character1_LeftHandThumb2.v"
		;
connectAttr "Character1_LeftHandThumb2.s" "Character1_LeftHandThumb3.is";
connectAttr "Character1_LeftHandThumb3_translateX.o" "Character1_LeftHandThumb3.tx"
		;
connectAttr "Character1_LeftHandThumb3_translateY.o" "Character1_LeftHandThumb3.ty"
		;
connectAttr "Character1_LeftHandThumb3_translateZ.o" "Character1_LeftHandThumb3.tz"
		;
connectAttr "Character1_LeftHandThumb3_scaleX.o" "Character1_LeftHandThumb3.sx";
connectAttr "Character1_LeftHandThumb3_scaleY.o" "Character1_LeftHandThumb3.sy";
connectAttr "Character1_LeftHandThumb3_scaleZ.o" "Character1_LeftHandThumb3.sz";
connectAttr "Character1_LeftHandThumb3_rotateX.o" "Character1_LeftHandThumb3.rx"
		;
connectAttr "Character1_LeftHandThumb3_rotateY.o" "Character1_LeftHandThumb3.ry"
		;
connectAttr "Character1_LeftHandThumb3_rotateZ.o" "Character1_LeftHandThumb3.rz"
		;
connectAttr "Character1_LeftHandThumb3_visibility.o" "Character1_LeftHandThumb3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandIndex1.is";
connectAttr "Character1_LeftHandIndex1_scaleX.o" "Character1_LeftHandIndex1.sx";
connectAttr "Character1_LeftHandIndex1_scaleY.o" "Character1_LeftHandIndex1.sy";
connectAttr "Character1_LeftHandIndex1_scaleZ.o" "Character1_LeftHandIndex1.sz";
connectAttr "Character1_LeftHandIndex1_translateX.o" "Character1_LeftHandIndex1.tx"
		;
connectAttr "Character1_LeftHandIndex1_translateY.o" "Character1_LeftHandIndex1.ty"
		;
connectAttr "Character1_LeftHandIndex1_translateZ.o" "Character1_LeftHandIndex1.tz"
		;
connectAttr "Character1_LeftHandIndex1_rotateX.o" "Character1_LeftHandIndex1.rx"
		;
connectAttr "Character1_LeftHandIndex1_rotateY.o" "Character1_LeftHandIndex1.ry"
		;
connectAttr "Character1_LeftHandIndex1_rotateZ.o" "Character1_LeftHandIndex1.rz"
		;
connectAttr "Character1_LeftHandIndex1_visibility.o" "Character1_LeftHandIndex1.v"
		;
connectAttr "Character1_LeftHandIndex1.s" "Character1_LeftHandIndex2.is";
connectAttr "Character1_LeftHandIndex2_scaleX.o" "Character1_LeftHandIndex2.sx";
connectAttr "Character1_LeftHandIndex2_scaleY.o" "Character1_LeftHandIndex2.sy";
connectAttr "Character1_LeftHandIndex2_scaleZ.o" "Character1_LeftHandIndex2.sz";
connectAttr "Character1_LeftHandIndex2_translateX.o" "Character1_LeftHandIndex2.tx"
		;
connectAttr "Character1_LeftHandIndex2_translateY.o" "Character1_LeftHandIndex2.ty"
		;
connectAttr "Character1_LeftHandIndex2_translateZ.o" "Character1_LeftHandIndex2.tz"
		;
connectAttr "Character1_LeftHandIndex2_rotateX.o" "Character1_LeftHandIndex2.rx"
		;
connectAttr "Character1_LeftHandIndex2_rotateY.o" "Character1_LeftHandIndex2.ry"
		;
connectAttr "Character1_LeftHandIndex2_rotateZ.o" "Character1_LeftHandIndex2.rz"
		;
connectAttr "Character1_LeftHandIndex2_visibility.o" "Character1_LeftHandIndex2.v"
		;
connectAttr "Character1_LeftHandIndex2.s" "Character1_LeftHandIndex3.is";
connectAttr "Character1_LeftHandIndex3_translateX.o" "Character1_LeftHandIndex3.tx"
		;
connectAttr "Character1_LeftHandIndex3_translateY.o" "Character1_LeftHandIndex3.ty"
		;
connectAttr "Character1_LeftHandIndex3_translateZ.o" "Character1_LeftHandIndex3.tz"
		;
connectAttr "Character1_LeftHandIndex3_scaleX.o" "Character1_LeftHandIndex3.sx";
connectAttr "Character1_LeftHandIndex3_scaleY.o" "Character1_LeftHandIndex3.sy";
connectAttr "Character1_LeftHandIndex3_scaleZ.o" "Character1_LeftHandIndex3.sz";
connectAttr "Character1_LeftHandIndex3_rotateX.o" "Character1_LeftHandIndex3.rx"
		;
connectAttr "Character1_LeftHandIndex3_rotateY.o" "Character1_LeftHandIndex3.ry"
		;
connectAttr "Character1_LeftHandIndex3_rotateZ.o" "Character1_LeftHandIndex3.rz"
		;
connectAttr "Character1_LeftHandIndex3_visibility.o" "Character1_LeftHandIndex3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandRing1.is";
connectAttr "Character1_LeftHandRing1_scaleX.o" "Character1_LeftHandRing1.sx";
connectAttr "Character1_LeftHandRing1_scaleY.o" "Character1_LeftHandRing1.sy";
connectAttr "Character1_LeftHandRing1_scaleZ.o" "Character1_LeftHandRing1.sz";
connectAttr "Character1_LeftHandRing1_translateX.o" "Character1_LeftHandRing1.tx"
		;
connectAttr "Character1_LeftHandRing1_translateY.o" "Character1_LeftHandRing1.ty"
		;
connectAttr "Character1_LeftHandRing1_translateZ.o" "Character1_LeftHandRing1.tz"
		;
connectAttr "Character1_LeftHandRing1_rotateX.o" "Character1_LeftHandRing1.rx";
connectAttr "Character1_LeftHandRing1_rotateY.o" "Character1_LeftHandRing1.ry";
connectAttr "Character1_LeftHandRing1_rotateZ.o" "Character1_LeftHandRing1.rz";
connectAttr "Character1_LeftHandRing1_visibility.o" "Character1_LeftHandRing1.v"
		;
connectAttr "Character1_LeftHandRing1.s" "Character1_LeftHandRing2.is";
connectAttr "Character1_LeftHandRing2_scaleX.o" "Character1_LeftHandRing2.sx";
connectAttr "Character1_LeftHandRing2_scaleY.o" "Character1_LeftHandRing2.sy";
connectAttr "Character1_LeftHandRing2_scaleZ.o" "Character1_LeftHandRing2.sz";
connectAttr "Character1_LeftHandRing2_translateX.o" "Character1_LeftHandRing2.tx"
		;
connectAttr "Character1_LeftHandRing2_translateY.o" "Character1_LeftHandRing2.ty"
		;
connectAttr "Character1_LeftHandRing2_translateZ.o" "Character1_LeftHandRing2.tz"
		;
connectAttr "Character1_LeftHandRing2_rotateX.o" "Character1_LeftHandRing2.rx";
connectAttr "Character1_LeftHandRing2_rotateY.o" "Character1_LeftHandRing2.ry";
connectAttr "Character1_LeftHandRing2_rotateZ.o" "Character1_LeftHandRing2.rz";
connectAttr "Character1_LeftHandRing2_visibility.o" "Character1_LeftHandRing2.v"
		;
connectAttr "Character1_LeftHandRing2.s" "Character1_LeftHandRing3.is";
connectAttr "Character1_LeftHandRing3_translateX.o" "Character1_LeftHandRing3.tx"
		;
connectAttr "Character1_LeftHandRing3_translateY.o" "Character1_LeftHandRing3.ty"
		;
connectAttr "Character1_LeftHandRing3_translateZ.o" "Character1_LeftHandRing3.tz"
		;
connectAttr "Character1_LeftHandRing3_scaleX.o" "Character1_LeftHandRing3.sx";
connectAttr "Character1_LeftHandRing3_scaleY.o" "Character1_LeftHandRing3.sy";
connectAttr "Character1_LeftHandRing3_scaleZ.o" "Character1_LeftHandRing3.sz";
connectAttr "Character1_LeftHandRing3_rotateX.o" "Character1_LeftHandRing3.rx";
connectAttr "Character1_LeftHandRing3_rotateY.o" "Character1_LeftHandRing3.ry";
connectAttr "Character1_LeftHandRing3_rotateZ.o" "Character1_LeftHandRing3.rz";
connectAttr "Character1_LeftHandRing3_visibility.o" "Character1_LeftHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_RightShoulder.is";
connectAttr "Character1_RightShoulder_scaleX.o" "Character1_RightShoulder.sx";
connectAttr "Character1_RightShoulder_scaleY.o" "Character1_RightShoulder.sy";
connectAttr "Character1_RightShoulder_scaleZ.o" "Character1_RightShoulder.sz";
connectAttr "Character1_RightShoulder_translateX.o" "Character1_RightShoulder.tx"
		;
connectAttr "Character1_RightShoulder_translateY.o" "Character1_RightShoulder.ty"
		;
connectAttr "Character1_RightShoulder_translateZ.o" "Character1_RightShoulder.tz"
		;
connectAttr "Character1_RightShoulder_rotateX.o" "Character1_RightShoulder.rx";
connectAttr "Character1_RightShoulder_rotateY.o" "Character1_RightShoulder.ry";
connectAttr "Character1_RightShoulder_rotateZ.o" "Character1_RightShoulder.rz";
connectAttr "Character1_RightShoulder_visibility.o" "Character1_RightShoulder.v"
		;
connectAttr "Character1_RightShoulder.s" "Character1_RightArm.is";
connectAttr "Character1_RightArm_scaleX.o" "Character1_RightArm.sx";
connectAttr "Character1_RightArm_scaleY.o" "Character1_RightArm.sy";
connectAttr "Character1_RightArm_scaleZ.o" "Character1_RightArm.sz";
connectAttr "Character1_RightArm_translateX.o" "Character1_RightArm.tx";
connectAttr "Character1_RightArm_translateY.o" "Character1_RightArm.ty";
connectAttr "Character1_RightArm_translateZ.o" "Character1_RightArm.tz";
connectAttr "Character1_RightArm_rotateX.o" "Character1_RightArm.rx";
connectAttr "Character1_RightArm_rotateY.o" "Character1_RightArm.ry";
connectAttr "Character1_RightArm_rotateZ.o" "Character1_RightArm.rz";
connectAttr "Character1_RightArm_visibility.o" "Character1_RightArm.v";
connectAttr "Character1_RightArm.s" "Character1_RightForeArm.is";
connectAttr "Character1_RightForeArm_scaleX.o" "Character1_RightForeArm.sx";
connectAttr "Character1_RightForeArm_scaleY.o" "Character1_RightForeArm.sy";
connectAttr "Character1_RightForeArm_scaleZ.o" "Character1_RightForeArm.sz";
connectAttr "Character1_RightForeArm_translateX.o" "Character1_RightForeArm.tx";
connectAttr "Character1_RightForeArm_translateY.o" "Character1_RightForeArm.ty";
connectAttr "Character1_RightForeArm_translateZ.o" "Character1_RightForeArm.tz";
connectAttr "Character1_RightForeArm_rotateX.o" "Character1_RightForeArm.rx";
connectAttr "Character1_RightForeArm_rotateY.o" "Character1_RightForeArm.ry";
connectAttr "Character1_RightForeArm_rotateZ.o" "Character1_RightForeArm.rz";
connectAttr "Character1_RightForeArm_visibility.o" "Character1_RightForeArm.v";
connectAttr "Character1_RightForeArm.s" "Character1_RightHand.is";
connectAttr "Character1_RightHand_scaleX.o" "Character1_RightHand.sx";
connectAttr "Character1_RightHand_scaleY.o" "Character1_RightHand.sy";
connectAttr "Character1_RightHand_scaleZ.o" "Character1_RightHand.sz";
connectAttr "Character1_RightHand_translateX.o" "Character1_RightHand.tx";
connectAttr "Character1_RightHand_translateY.o" "Character1_RightHand.ty";
connectAttr "Character1_RightHand_translateZ.o" "Character1_RightHand.tz";
connectAttr "Character1_RightHand_rotateX.o" "Character1_RightHand.rx";
connectAttr "Character1_RightHand_rotateY.o" "Character1_RightHand.ry";
connectAttr "Character1_RightHand_rotateZ.o" "Character1_RightHand.rz";
connectAttr "Character1_RightHand_visibility.o" "Character1_RightHand.v";
connectAttr "Character1_RightHand.s" "Character1_RightHandThumb1.is";
connectAttr "Character1_RightHandThumb1_scaleX.o" "Character1_RightHandThumb1.sx"
		;
connectAttr "Character1_RightHandThumb1_scaleY.o" "Character1_RightHandThumb1.sy"
		;
connectAttr "Character1_RightHandThumb1_scaleZ.o" "Character1_RightHandThumb1.sz"
		;
connectAttr "Character1_RightHandThumb1_translateX.o" "Character1_RightHandThumb1.tx"
		;
connectAttr "Character1_RightHandThumb1_translateY.o" "Character1_RightHandThumb1.ty"
		;
connectAttr "Character1_RightHandThumb1_translateZ.o" "Character1_RightHandThumb1.tz"
		;
connectAttr "Character1_RightHandThumb1_rotateX.o" "Character1_RightHandThumb1.rx"
		;
connectAttr "Character1_RightHandThumb1_rotateY.o" "Character1_RightHandThumb1.ry"
		;
connectAttr "Character1_RightHandThumb1_rotateZ.o" "Character1_RightHandThumb1.rz"
		;
connectAttr "Character1_RightHandThumb1_visibility.o" "Character1_RightHandThumb1.v"
		;
connectAttr "Character1_RightHandThumb1.s" "Character1_RightHandThumb2.is";
connectAttr "Character1_RightHandThumb2_scaleX.o" "Character1_RightHandThumb2.sx"
		;
connectAttr "Character1_RightHandThumb2_scaleY.o" "Character1_RightHandThumb2.sy"
		;
connectAttr "Character1_RightHandThumb2_scaleZ.o" "Character1_RightHandThumb2.sz"
		;
connectAttr "Character1_RightHandThumb2_translateX.o" "Character1_RightHandThumb2.tx"
		;
connectAttr "Character1_RightHandThumb2_translateY.o" "Character1_RightHandThumb2.ty"
		;
connectAttr "Character1_RightHandThumb2_translateZ.o" "Character1_RightHandThumb2.tz"
		;
connectAttr "Character1_RightHandThumb2_rotateX.o" "Character1_RightHandThumb2.rx"
		;
connectAttr "Character1_RightHandThumb2_rotateY.o" "Character1_RightHandThumb2.ry"
		;
connectAttr "Character1_RightHandThumb2_rotateZ.o" "Character1_RightHandThumb2.rz"
		;
connectAttr "Character1_RightHandThumb2_visibility.o" "Character1_RightHandThumb2.v"
		;
connectAttr "Character1_RightHandThumb2.s" "Character1_RightHandThumb3.is";
connectAttr "Character1_RightHandThumb3_translateX.o" "Character1_RightHandThumb3.tx"
		;
connectAttr "Character1_RightHandThumb3_translateY.o" "Character1_RightHandThumb3.ty"
		;
connectAttr "Character1_RightHandThumb3_translateZ.o" "Character1_RightHandThumb3.tz"
		;
connectAttr "Character1_RightHandThumb3_scaleX.o" "Character1_RightHandThumb3.sx"
		;
connectAttr "Character1_RightHandThumb3_scaleY.o" "Character1_RightHandThumb3.sy"
		;
connectAttr "Character1_RightHandThumb3_scaleZ.o" "Character1_RightHandThumb3.sz"
		;
connectAttr "Character1_RightHandThumb3_rotateX.o" "Character1_RightHandThumb3.rx"
		;
connectAttr "Character1_RightHandThumb3_rotateY.o" "Character1_RightHandThumb3.ry"
		;
connectAttr "Character1_RightHandThumb3_rotateZ.o" "Character1_RightHandThumb3.rz"
		;
connectAttr "Character1_RightHandThumb3_visibility.o" "Character1_RightHandThumb3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandIndex1.is";
connectAttr "Character1_RightHandIndex1_scaleX.o" "Character1_RightHandIndex1.sx"
		;
connectAttr "Character1_RightHandIndex1_scaleY.o" "Character1_RightHandIndex1.sy"
		;
connectAttr "Character1_RightHandIndex1_scaleZ.o" "Character1_RightHandIndex1.sz"
		;
connectAttr "Character1_RightHandIndex1_translateX.o" "Character1_RightHandIndex1.tx"
		;
connectAttr "Character1_RightHandIndex1_translateY.o" "Character1_RightHandIndex1.ty"
		;
connectAttr "Character1_RightHandIndex1_translateZ.o" "Character1_RightHandIndex1.tz"
		;
connectAttr "Character1_RightHandIndex1_rotateX.o" "Character1_RightHandIndex1.rx"
		;
connectAttr "Character1_RightHandIndex1_rotateY.o" "Character1_RightHandIndex1.ry"
		;
connectAttr "Character1_RightHandIndex1_rotateZ.o" "Character1_RightHandIndex1.rz"
		;
connectAttr "Character1_RightHandIndex1_visibility.o" "Character1_RightHandIndex1.v"
		;
connectAttr "Character1_RightHandIndex1.s" "Character1_RightHandIndex2.is";
connectAttr "Character1_RightHandIndex2_scaleX.o" "Character1_RightHandIndex2.sx"
		;
connectAttr "Character1_RightHandIndex2_scaleY.o" "Character1_RightHandIndex2.sy"
		;
connectAttr "Character1_RightHandIndex2_scaleZ.o" "Character1_RightHandIndex2.sz"
		;
connectAttr "Character1_RightHandIndex2_translateX.o" "Character1_RightHandIndex2.tx"
		;
connectAttr "Character1_RightHandIndex2_translateY.o" "Character1_RightHandIndex2.ty"
		;
connectAttr "Character1_RightHandIndex2_translateZ.o" "Character1_RightHandIndex2.tz"
		;
connectAttr "Character1_RightHandIndex2_rotateX.o" "Character1_RightHandIndex2.rx"
		;
connectAttr "Character1_RightHandIndex2_rotateY.o" "Character1_RightHandIndex2.ry"
		;
connectAttr "Character1_RightHandIndex2_rotateZ.o" "Character1_RightHandIndex2.rz"
		;
connectAttr "Character1_RightHandIndex2_visibility.o" "Character1_RightHandIndex2.v"
		;
connectAttr "Character1_RightHandIndex2.s" "Character1_RightHandIndex3.is";
connectAttr "Character1_RightHandIndex3_translateX.o" "Character1_RightHandIndex3.tx"
		;
connectAttr "Character1_RightHandIndex3_translateY.o" "Character1_RightHandIndex3.ty"
		;
connectAttr "Character1_RightHandIndex3_translateZ.o" "Character1_RightHandIndex3.tz"
		;
connectAttr "Character1_RightHandIndex3_scaleX.o" "Character1_RightHandIndex3.sx"
		;
connectAttr "Character1_RightHandIndex3_scaleY.o" "Character1_RightHandIndex3.sy"
		;
connectAttr "Character1_RightHandIndex3_scaleZ.o" "Character1_RightHandIndex3.sz"
		;
connectAttr "Character1_RightHandIndex3_rotateX.o" "Character1_RightHandIndex3.rx"
		;
connectAttr "Character1_RightHandIndex3_rotateY.o" "Character1_RightHandIndex3.ry"
		;
connectAttr "Character1_RightHandIndex3_rotateZ.o" "Character1_RightHandIndex3.rz"
		;
connectAttr "Character1_RightHandIndex3_visibility.o" "Character1_RightHandIndex3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandRing1.is";
connectAttr "Character1_RightHandRing1_scaleX.o" "Character1_RightHandRing1.sx";
connectAttr "Character1_RightHandRing1_scaleY.o" "Character1_RightHandRing1.sy";
connectAttr "Character1_RightHandRing1_scaleZ.o" "Character1_RightHandRing1.sz";
connectAttr "Character1_RightHandRing1_translateX.o" "Character1_RightHandRing1.tx"
		;
connectAttr "Character1_RightHandRing1_translateY.o" "Character1_RightHandRing1.ty"
		;
connectAttr "Character1_RightHandRing1_translateZ.o" "Character1_RightHandRing1.tz"
		;
connectAttr "Character1_RightHandRing1_rotateX.o" "Character1_RightHandRing1.rx"
		;
connectAttr "Character1_RightHandRing1_rotateY.o" "Character1_RightHandRing1.ry"
		;
connectAttr "Character1_RightHandRing1_rotateZ.o" "Character1_RightHandRing1.rz"
		;
connectAttr "Character1_RightHandRing1_visibility.o" "Character1_RightHandRing1.v"
		;
connectAttr "Character1_RightHandRing1.s" "Character1_RightHandRing2.is";
connectAttr "Character1_RightHandRing2_scaleX.o" "Character1_RightHandRing2.sx";
connectAttr "Character1_RightHandRing2_scaleY.o" "Character1_RightHandRing2.sy";
connectAttr "Character1_RightHandRing2_scaleZ.o" "Character1_RightHandRing2.sz";
connectAttr "Character1_RightHandRing2_translateX.o" "Character1_RightHandRing2.tx"
		;
connectAttr "Character1_RightHandRing2_translateY.o" "Character1_RightHandRing2.ty"
		;
connectAttr "Character1_RightHandRing2_translateZ.o" "Character1_RightHandRing2.tz"
		;
connectAttr "Character1_RightHandRing2_rotateX.o" "Character1_RightHandRing2.rx"
		;
connectAttr "Character1_RightHandRing2_rotateY.o" "Character1_RightHandRing2.ry"
		;
connectAttr "Character1_RightHandRing2_rotateZ.o" "Character1_RightHandRing2.rz"
		;
connectAttr "Character1_RightHandRing2_visibility.o" "Character1_RightHandRing2.v"
		;
connectAttr "Character1_RightHandRing2.s" "Character1_RightHandRing3.is";
connectAttr "Character1_RightHandRing3_translateX.o" "Character1_RightHandRing3.tx"
		;
connectAttr "Character1_RightHandRing3_translateY.o" "Character1_RightHandRing3.ty"
		;
connectAttr "Character1_RightHandRing3_translateZ.o" "Character1_RightHandRing3.tz"
		;
connectAttr "Character1_RightHandRing3_scaleX.o" "Character1_RightHandRing3.sx";
connectAttr "Character1_RightHandRing3_scaleY.o" "Character1_RightHandRing3.sy";
connectAttr "Character1_RightHandRing3_scaleZ.o" "Character1_RightHandRing3.sz";
connectAttr "Character1_RightHandRing3_rotateX.o" "Character1_RightHandRing3.rx"
		;
connectAttr "Character1_RightHandRing3_rotateY.o" "Character1_RightHandRing3.ry"
		;
connectAttr "Character1_RightHandRing3_rotateZ.o" "Character1_RightHandRing3.rz"
		;
connectAttr "Character1_RightHandRing3_visibility.o" "Character1_RightHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_Neck.is";
connectAttr "Character1_Neck_scaleX.o" "Character1_Neck.sx";
connectAttr "Character1_Neck_scaleY.o" "Character1_Neck.sy";
connectAttr "Character1_Neck_scaleZ.o" "Character1_Neck.sz";
connectAttr "Character1_Neck_translateX.o" "Character1_Neck.tx";
connectAttr "Character1_Neck_translateY.o" "Character1_Neck.ty";
connectAttr "Character1_Neck_translateZ.o" "Character1_Neck.tz";
connectAttr "Character1_Neck_rotateX.o" "Character1_Neck.rx";
connectAttr "Character1_Neck_rotateY.o" "Character1_Neck.ry";
connectAttr "Character1_Neck_rotateZ.o" "Character1_Neck.rz";
connectAttr "Character1_Neck_visibility.o" "Character1_Neck.v";
connectAttr "Character1_Neck.s" "Character1_Head.is";
connectAttr "Character1_Head_scaleX.o" "Character1_Head.sx";
connectAttr "Character1_Head_scaleY.o" "Character1_Head.sy";
connectAttr "Character1_Head_scaleZ.o" "Character1_Head.sz";
connectAttr "Character1_Head_translateX.o" "Character1_Head.tx";
connectAttr "Character1_Head_translateY.o" "Character1_Head.ty";
connectAttr "Character1_Head_translateZ.o" "Character1_Head.tz";
connectAttr "Character1_Head_rotateX.o" "Character1_Head.rx";
connectAttr "Character1_Head_rotateY.o" "Character1_Head.ry";
connectAttr "Character1_Head_rotateZ.o" "Character1_Head.rz";
connectAttr "Character1_Head_visibility.o" "Character1_Head.v";
connectAttr "Character1_Head.s" "jaw.is";
connectAttr "jaw_lockInfluenceWeights.o" "jaw.liw";
connectAttr "jaw_translateX.o" "jaw.tx";
connectAttr "jaw_translateY.o" "jaw.ty";
connectAttr "jaw_translateZ.o" "jaw.tz";
connectAttr "jaw_scaleX.o" "jaw.sx";
connectAttr "jaw_scaleY.o" "jaw.sy";
connectAttr "jaw_scaleZ.o" "jaw.sz";
connectAttr "jaw_rotateX.o" "jaw.rx";
connectAttr "jaw_rotateY.o" "jaw.ry";
connectAttr "jaw_rotateZ.o" "jaw.rz";
connectAttr "jaw_visibility.o" "jaw.v";
connectAttr "Character1_Head.s" "eyes.is";
connectAttr "eyes_translateX.o" "eyes.tx";
connectAttr "eyes_translateY.o" "eyes.ty";
connectAttr "eyes_translateZ.o" "eyes.tz";
connectAttr "eyes_scaleX.o" "eyes.sx";
connectAttr "eyes_scaleY.o" "eyes.sy";
connectAttr "eyes_scaleZ.o" "eyes.sz";
connectAttr "eyes_rotateX.o" "eyes.rx";
connectAttr "eyes_rotateY.o" "eyes.ry";
connectAttr "eyes_rotateZ.o" "eyes.rz";
connectAttr "eyes_visibility.o" "eyes.v";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Enemy@Grunt_Laughing_01.ma
