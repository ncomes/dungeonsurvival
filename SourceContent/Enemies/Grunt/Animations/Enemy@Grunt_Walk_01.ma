//Maya ASCII 2016 scene
//Name: Enemy@Grunt_walk_01.ma
//Last modified: Sat, Nov 21, 2015 10:58:54 PM
//Codeset: 1252
file -rdi 1 -dns -rpr "Grunt_Skeleton_Rig" -rfn "Grunt_Skeleton_RigRN" -typ "mayaAscii"
		 "C:/Users/Nathan/Documents/dungeonsurvival/SourceContent/Enemies/Grunt/Rig/Grunt_Skeleton_Rig.ma";
file -rdi 2 -dns -rpr "Grunt_Skeleton_skin" -rfn "Grunt_Skeleton_skinRN" -typ
		 "mayaAscii" "C:/Users/Nathan/Documents/dungeonsurvival/SourceContent/Enemies/Grunt/Rig/Grunt_Skeleton_skin.ma";
file -r -dns -rpr "Grunt_Skeleton_Rig" -dr 1 -rfn "Grunt_Skeleton_RigRN" -typ "mayaAscii"
		 "C:/Users/Nathan/Documents/dungeonsurvival/SourceContent/Enemies/Grunt/Rig/Grunt_Skeleton_Rig.ma";
requires maya "2016";
requires -nodeType "ikSpringSolver" "ikSpringSolver" "1.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "ED55DF69-413C-4FD4-2DD3-3B944B1AD0F5";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -550.44009483703599 318.58623875435399 487.62498054751455 ;
	setAttr ".r" -type "double3" -16.538352729530963 -51.00000000000113 -2.5269757079996778e-015 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "5D1B0A1D-402F-E932-C397-2EA06A95834C";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".ncp" 1;
	setAttr ".fcp" 1000000;
	setAttr ".coi" 797.14176024407152;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "E841D76A-4B55-2D45-A981-8AB48921C3E8";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "5E1CB8A6-4318-8291-D2FA-BA978C7A9376";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "F102DC93-4BDC-9A3B-26A2-D9B51BB9FF15";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "DB2FB2A5-4DEC-7F2B-D2F0-3FA359C3758E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "D1588586-4E01-DD15-9406-1DACF2EAF7C6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "2FCB8F9B-42E5-9EC5-0622-78B2FDE79697";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "grunt_rig";
	rename -uid "E8C007BA-475C-AF3F-2D02-3393D7AFE50C";
	setAttr ".rp" -type "double3" -0.056989816302774443 94.467932384644925 -1.0564472310214938 ;
	setAttr ".sp" -type "double3" -0.056989816302774443 94.467932384644925 -1.0564472310214938 ;
createNode fosterParent -n "Grunt_Skeleton_RigRNfosterParent1";
	rename -uid "553001ED-4101-B017-5DFF-02B4142FA942";
createNode pointConstraint -n "ref_frame_pointConstraint1" -p "Grunt_Skeleton_RigRNfosterParent1";
	rename -uid "AA9D3297-4289-8CC6-63CA-C284E71E1813";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "pelvis_rigW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 48.502845764160156 -9.3133430480957031 ;
	setAttr -k on ".w0";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "C88B81A6-437A-4E2B-1849-11A7DFD0F8CB";
	setAttr -s 4 ".lnk";
	setAttr -s 4 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "D0CAC7A5-4A55-3E17-A66C-FD9C648AE3F6";
createNode displayLayer -n "defaultLayer";
	rename -uid "7F9FC0AB-4969-364F-56BC-9CB05AA437D6";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "1447E575-4421-0925-8B2D-0DA0EED47233";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "26730AAD-4126-88C8-A648-9E9E95E0581F";
	setAttr ".g" yes;
createNode reference -n "Grunt_Skeleton_RigRN";
	rename -uid "9441F9B6-40B3-F913-FEE9-478EECAE4EF1";
	setAttr -s 191 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".phl[89]" 0;
	setAttr ".phl[90]" 0;
	setAttr ".phl[91]" 0;
	setAttr ".phl[92]" 0;
	setAttr ".phl[93]" 0;
	setAttr ".phl[94]" 0;
	setAttr ".phl[95]" 0;
	setAttr ".phl[96]" 0;
	setAttr ".phl[97]" 0;
	setAttr ".phl[98]" 0;
	setAttr ".phl[99]" 0;
	setAttr ".phl[100]" 0;
	setAttr ".phl[101]" 0;
	setAttr ".phl[102]" 0;
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".phl[111]" 0;
	setAttr ".phl[112]" 0;
	setAttr ".phl[113]" 0;
	setAttr ".phl[114]" 0;
	setAttr ".phl[115]" 0;
	setAttr ".phl[116]" 0;
	setAttr ".phl[117]" 0;
	setAttr ".phl[118]" 0;
	setAttr ".phl[119]" 0;
	setAttr ".phl[120]" 0;
	setAttr ".phl[121]" 0;
	setAttr ".phl[122]" 0;
	setAttr ".phl[123]" 0;
	setAttr ".phl[124]" 0;
	setAttr ".phl[125]" 0;
	setAttr ".phl[126]" 0;
	setAttr ".phl[127]" 0;
	setAttr ".phl[128]" 0;
	setAttr ".phl[129]" 0;
	setAttr ".phl[130]" 0;
	setAttr ".phl[131]" 0;
	setAttr ".phl[132]" 0;
	setAttr ".phl[133]" 0;
	setAttr ".phl[134]" 0;
	setAttr ".phl[135]" 0;
	setAttr ".phl[136]" 0;
	setAttr ".phl[137]" 0;
	setAttr ".phl[138]" 0;
	setAttr ".phl[139]" 0;
	setAttr ".phl[140]" 0;
	setAttr ".phl[141]" 0;
	setAttr ".phl[142]" 0;
	setAttr ".phl[143]" 0;
	setAttr ".phl[144]" 0;
	setAttr ".phl[145]" 0;
	setAttr ".phl[146]" 0;
	setAttr ".phl[147]" 0;
	setAttr ".phl[148]" 0;
	setAttr ".phl[149]" 0;
	setAttr ".phl[150]" 0;
	setAttr ".phl[151]" 0;
	setAttr ".phl[152]" 0;
	setAttr ".phl[153]" 0;
	setAttr ".phl[154]" 0;
	setAttr ".phl[155]" 0;
	setAttr ".phl[156]" 0;
	setAttr ".phl[157]" 0;
	setAttr ".phl[158]" 0;
	setAttr ".phl[159]" 0;
	setAttr ".phl[160]" 0;
	setAttr ".phl[161]" 0;
	setAttr ".phl[162]" 0;
	setAttr ".phl[163]" 0;
	setAttr ".phl[164]" 0;
	setAttr ".phl[165]" 0;
	setAttr ".phl[166]" 0;
	setAttr ".phl[167]" 0;
	setAttr ".phl[168]" 0;
	setAttr ".phl[169]" 0;
	setAttr ".phl[170]" 0;
	setAttr ".phl[171]" 0;
	setAttr ".phl[172]" 0;
	setAttr ".phl[173]" 0;
	setAttr ".phl[174]" 0;
	setAttr ".phl[175]" 0;
	setAttr ".phl[176]" 0;
	setAttr ".phl[177]" 0;
	setAttr ".phl[178]" 0;
	setAttr ".phl[179]" 0;
	setAttr ".phl[180]" 0;
	setAttr ".phl[181]" 0;
	setAttr ".phl[182]" 0;
	setAttr ".phl[183]" 0;
	setAttr ".phl[184]" 0;
	setAttr ".phl[185]" 0;
	setAttr ".phl[186]" 0;
	setAttr ".phl[187]" 0;
	setAttr ".phl[188]" 0;
	setAttr ".phl[189]" 0;
	setAttr ".phl[190]" 0;
	setAttr ".phl[191]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"Grunt_Skeleton_RigRN"
		"Grunt_Skeleton_RigRN" 0
		"Grunt_Skeleton_skinRN" 0
		"Grunt_Skeleton_RigRN" 273
		2 "|grunt_rig|DoNotDeleteRigNodes|spineExtra|ikSplineSpine" "translate" " -type \"double3\" -11.007460300618154 69.750897830562124 -12.322166272642576"
		
		2 "|grunt_rig|DoNotDeleteRigNodes|spineExtra|ikSplineSpine" "rotate" " -type \"double3\" 37.095765615647053 5.2908285748120019 68.807404215403807"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl" 
		"rotate" " -type \"double3\" 25.885957896648971 37.901336706104772 -40.67292724480189"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"visibility" " 1"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"index" " -av -k 1 69.2"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"index1" " -av -k 1 -82.7"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"index2" " -av -k 1 85.3"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"pinky" " -av -k 1 -20.8"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"pinky1" " -k 1 -55.500000000000007"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"pinky2" " -k 1 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"thumb" " -k 1 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"thumb1" " -k 1 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"thumb2" " -k 1 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"thumbReach" " -av -k 1 -36.4"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"thumbTwist" " -av -k 1 -43.300000000000004"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"indexSpread" " -k 1 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"pinkySpread" " -k 1 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"indexTwist" " -k 1 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"pinkyTwist" " -k 1 24.3"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl" "translate" 
		" -type \"double3\" -10.321456821906757 -9.8656966706661109 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl" "translateX" 
		" -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl" "translateY" 
		" -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl" "translateZ" 
		" -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl" "rotate" 
		" -type \"double3\" 0 0 10.665340208588947"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl" "rotateX" 
		" -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl" "rotateY" 
		" -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl" "rotateZ" 
		" -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl" 
		"rotate" " -type \"double3\" 0 0 18.611692062174249"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl" 
		"rotate" " -type \"double3\" -10.062035509963902 -48.388974413928736 -21.117875641659385"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01" 
		"rotate" " -type \"double3\" 16.203777003200326 -33.079713443291276 -74.945925044578431"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01" 
		"rotate" " -type \"double3\" -65.516969749522502 145.17364934010533 98.499945453801061"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01" 
		"rotate" " -type \"double3\" 0 -74.269658133916877 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01" 
		"rotate" " -type \"double3\" 58.226316623094519 45.080761265948198 -63.628738999650963"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl" 
		"rotate" " -type \"double3\" -21.444517399597277 -11.172282573533053 -26.000147285453458"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"translate" " -type \"double3\" -4.9941881332379978 7.7460887010554345 0"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"rotate" " -type \"double3\" 31.210246409135681 6.8033565539370917 -26.176761016655011"
		
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"ballRoll" " -av -k 1 0"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"ballPivot" " -av -k 1 0"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"translate" " -type \"double3\" -10.836960331836949 0 11.421342243698824"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"rotate" " -type \"double3\" 0 119.49095832725905 0"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"ballRoll" " -av -k 1 0"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"heelSide" " -av -k 1 0"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"toeSide" " -av -k 1 0"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01" 
		"translate" " -type \"double3\" 77.607972466215188 0 -87.386784232732495"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01" 
		"translateZ" " -av"
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.midIkCtrl" "Grunt_Skeleton_RigRN.placeHolderList[6]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.lookAt" "Grunt_Skeleton_RigRN.placeHolderList[7]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.rotateX" "Grunt_Skeleton_RigRN.placeHolderList[8]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.rotateY" "Grunt_Skeleton_RigRN.placeHolderList[9]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.rotateZ" "Grunt_Skeleton_RigRN.placeHolderList[10]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.translateX" "Grunt_Skeleton_RigRN.placeHolderList[11]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.translateY" "Grunt_Skeleton_RigRN.placeHolderList[12]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.translateZ" "Grunt_Skeleton_RigRN.placeHolderList[13]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.scaleY" "Grunt_Skeleton_RigRN.placeHolderList[14]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.scaleX" "Grunt_Skeleton_RigRN.placeHolderList[15]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.scaleZ" "Grunt_Skeleton_RigRN.placeHolderList[16]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.visibility" "Grunt_Skeleton_RigRN.placeHolderList[17]" 
		""
		5 3 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig.translate" 
		"Grunt_Skeleton_RigRN.placeHolderList[18]" ""
		5 3 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig.rotatePivot" 
		"Grunt_Skeleton_RigRN.placeHolderList[19]" ""
		5 3 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig.rotatePivotTranslate" 
		"Grunt_Skeleton_RigRN.placeHolderList[20]" ""
		5 3 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig.parentMatrix" 
		"Grunt_Skeleton_RigRN.placeHolderList[21]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig|hips_rig|l_hip_rig|l_knee_rig|l_ankle_rig|l_leg_ik_switch.IkFkSwitch" 
		"Grunt_Skeleton_RigRN.placeHolderList[22]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig|hips_rig|l_hip_rig|l_knee_rig|l_ankle_rig|l_leg_ik_switch.autoStretch" 
		"Grunt_Skeleton_RigRN.placeHolderList[23]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig|hips_rig|r_hip_rig|r_knee_rig|r_ankle_rig|r_leg_ik_switch.IkFkSwitch" 
		"Grunt_Skeleton_RigRN.placeHolderList[24]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig|hips_rig|r_hip_rig|r_knee_rig|r_ankle_rig|r_leg_ik_switch.autoStretch" 
		"Grunt_Skeleton_RigRN.placeHolderList[25]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[26]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[27]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[28]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.visibility" 
		"Grunt_Skeleton_RigRN.placeHolderList[29]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[30]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[31]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[32]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.scaleX" 
		"Grunt_Skeleton_RigRN.placeHolderList[33]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.scaleY" 
		"Grunt_Skeleton_RigRN.placeHolderList[34]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.scaleZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[35]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_arm_ik_switch.IkFkSwitch" 
		"Grunt_Skeleton_RigRN.placeHolderList[36]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_arm_ik_switch.elbowLock" 
		"Grunt_Skeleton_RigRN.placeHolderList[37]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_arm_ik_switch.autoStretch" 
		"Grunt_Skeleton_RigRN.placeHolderList[38]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.index" 
		"Grunt_Skeleton_RigRN.placeHolderList[39]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.index1" 
		"Grunt_Skeleton_RigRN.placeHolderList[40]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.index2" 
		"Grunt_Skeleton_RigRN.placeHolderList[41]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.pinky" 
		"Grunt_Skeleton_RigRN.placeHolderList[42]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.pinky1" 
		"Grunt_Skeleton_RigRN.placeHolderList[43]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.pinky2" 
		"Grunt_Skeleton_RigRN.placeHolderList[44]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.thumb" 
		"Grunt_Skeleton_RigRN.placeHolderList[45]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.thumb1" 
		"Grunt_Skeleton_RigRN.placeHolderList[46]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.thumb2" 
		"Grunt_Skeleton_RigRN.placeHolderList[47]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.thumbReach" 
		"Grunt_Skeleton_RigRN.placeHolderList[48]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.thumbTwist" 
		"Grunt_Skeleton_RigRN.placeHolderList[49]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.indexSpread" 
		"Grunt_Skeleton_RigRN.placeHolderList[50]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.pinkySpread" 
		"Grunt_Skeleton_RigRN.placeHolderList[51]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.indexTwist" 
		"Grunt_Skeleton_RigRN.placeHolderList[52]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.pinkyTwist" 
		"Grunt_Skeleton_RigRN.placeHolderList[53]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.visibility" 
		"Grunt_Skeleton_RigRN.placeHolderList[54]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_arm_ik_switch.IkFkSwitch" 
		"Grunt_Skeleton_RigRN.placeHolderList[55]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_arm_ik_switch.elbowLock" 
		"Grunt_Skeleton_RigRN.placeHolderList[56]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_arm_ik_switch.autoStretch" 
		"Grunt_Skeleton_RigRN.placeHolderList[57]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[58]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[59]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[60]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[61]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[62]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[63]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[64]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[65]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[66]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[67]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[68]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[69]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[70]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[71]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[72]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[73]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[74]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[75]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[76]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[77]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[78]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl.spineScale" 
		"Grunt_Skeleton_RigRN.placeHolderList[79]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[80]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[81]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[82]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[83]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[84]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[85]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01.rotateWith" 
		"Grunt_Skeleton_RigRN.placeHolderList[86]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[87]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[88]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[89]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[90]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[91]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[92]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[93]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[94]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[95]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[96]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[97]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[98]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[99]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[100]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[101]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[102]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[103]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[104]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[105]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01.rotateWith" 
		"Grunt_Skeleton_RigRN.placeHolderList[106]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[107]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[108]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[109]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[110]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[111]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[112]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[113]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[114]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[115]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[116]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[117]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[118]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[119]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl.rotateWith" 
		"Grunt_Skeleton_RigRN.placeHolderList[120]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[121]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[122]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[123]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[124]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[125]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[126]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.scaleX" 
		"Grunt_Skeleton_RigRN.placeHolderList[127]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.scaleY" 
		"Grunt_Skeleton_RigRN.placeHolderList[128]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.rotateWith" 
		"Grunt_Skeleton_RigRN.placeHolderList[129]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[130]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[131]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[132]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[133]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[134]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[135]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.xOffset" 
		"Grunt_Skeleton_RigRN.placeHolderList[136]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.yOffset" 
		"Grunt_Skeleton_RigRN.placeHolderList[137]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.zOffset" 
		"Grunt_Skeleton_RigRN.placeHolderList[138]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.stretch" 
		"Grunt_Skeleton_RigRN.placeHolderList[139]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.heelRoll" 
		"Grunt_Skeleton_RigRN.placeHolderList[140]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.ballRoll" 
		"Grunt_Skeleton_RigRN.placeHolderList[141]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.toeRoll" 
		"Grunt_Skeleton_RigRN.placeHolderList[142]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.toeTap" 
		"Grunt_Skeleton_RigRN.placeHolderList[143]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.heelPivot" 
		"Grunt_Skeleton_RigRN.placeHolderList[144]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.ballPivot" 
		"Grunt_Skeleton_RigRN.placeHolderList[145]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.toePivot" 
		"Grunt_Skeleton_RigRN.placeHolderList[146]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.heelSide" 
		"Grunt_Skeleton_RigRN.placeHolderList[147]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.ballSide" 
		"Grunt_Skeleton_RigRN.placeHolderList[148]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.toeSide" 
		"Grunt_Skeleton_RigRN.placeHolderList[149]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.space" 
		"Grunt_Skeleton_RigRN.placeHolderList[150]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[151]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[152]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[153]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[154]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[155]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[156]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.xOffset" 
		"Grunt_Skeleton_RigRN.placeHolderList[157]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.yOffset" 
		"Grunt_Skeleton_RigRN.placeHolderList[158]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.zOffset" 
		"Grunt_Skeleton_RigRN.placeHolderList[159]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.stretch" 
		"Grunt_Skeleton_RigRN.placeHolderList[160]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.heelRoll" 
		"Grunt_Skeleton_RigRN.placeHolderList[161]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.ballRoll" 
		"Grunt_Skeleton_RigRN.placeHolderList[162]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.toeRoll" 
		"Grunt_Skeleton_RigRN.placeHolderList[163]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.toeTap" 
		"Grunt_Skeleton_RigRN.placeHolderList[164]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.heelPivot" 
		"Grunt_Skeleton_RigRN.placeHolderList[165]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.ballPivot" 
		"Grunt_Skeleton_RigRN.placeHolderList[166]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.toePivot" 
		"Grunt_Skeleton_RigRN.placeHolderList[167]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.heelSide" 
		"Grunt_Skeleton_RigRN.placeHolderList[168]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.ballSide" 
		"Grunt_Skeleton_RigRN.placeHolderList[169]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.toeSide" 
		"Grunt_Skeleton_RigRN.placeHolderList[170]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.space" 
		"Grunt_Skeleton_RigRN.placeHolderList[171]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[172]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[173]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[174]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[175]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[176]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[177]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01.space" 
		"Grunt_Skeleton_RigRN.placeHolderList[178]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[179]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[180]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[181]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[182]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[183]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[184]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01.space" 
		"Grunt_Skeleton_RigRN.placeHolderList[185]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[186]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[187]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[188]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[189]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[190]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[191]" ""
		"Grunt_Skeleton_skinRN" 6
		0 "|Grunt_Skeleton_RigRNfosterParent1|ref_frame_pointConstraint1" "|grunt_rig|ref_frame" 
		"-s -r "
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|ref_frame.translateX" "Grunt_Skeleton_RigRN.placeHolderList[1]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|ref_frame.translateZ" "Grunt_Skeleton_RigRN.placeHolderList[2]" 
		""
		5 3 "Grunt_Skeleton_RigRN" "|grunt_rig|ref_frame.parentInverseMatrix" 
		"Grunt_Skeleton_RigRN.placeHolderList[3]" ""
		5 3 "Grunt_Skeleton_RigRN" "|grunt_rig|ref_frame.rotatePivot" "Grunt_Skeleton_RigRN.placeHolderList[4]" 
		""
		5 3 "Grunt_Skeleton_RigRN" "|grunt_rig|ref_frame.rotatePivotTranslate" 
		"Grunt_Skeleton_RigRN.placeHolderList[5]" "";
lockNode -l 1 ;
createNode script -n "uiConfigurationScriptNode2";
	rename -uid "44AC1544-454C-980A-7280-4E8A122B805C";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 0\n                -polymeshes 1\n                -subdivSurfaces 0\n                -planes 0\n                -lights 0\n                -cameras 0\n                -controlVertices 0\n                -hulls 0\n                -grid 1\n                -imagePlane 0\n                -joints 0\n"
		+ "                -ikHandles 0\n                -deformers 0\n                -dynamics 0\n                -particleInstancers 0\n                -fluids 0\n                -hairSystems 0\n                -follicles 0\n                -nCloths 0\n                -nParticles 0\n                -nRigids 0\n                -dynamicConstraints 0\n                -locators 0\n                -manipulators 1\n                -pluginShapes 0\n                -dimensions 0\n                -handles 0\n                -pivots 0\n                -textures 0\n                -strokes 0\n                -motionTrails 0\n                -clipGhosts 0\n                -greasePencils 0\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1226\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 0 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 0\n            -polymeshes 1\n            -subdivSurfaces 0\n"
		+ "            -planes 0\n            -lights 0\n            -cameras 0\n            -controlVertices 0\n            -hulls 0\n            -grid 1\n            -imagePlane 0\n            -joints 0\n            -ikHandles 0\n            -deformers 0\n            -dynamics 0\n            -particleInstancers 0\n            -fluids 0\n            -hairSystems 0\n            -follicles 0\n            -nCloths 0\n            -nParticles 0\n            -nRigids 0\n            -dynamicConstraints 0\n            -locators 0\n            -manipulators 1\n            -pluginShapes 0\n            -dimensions 0\n            -handles 0\n            -pivots 0\n            -textures 0\n            -strokes 0\n            -motionTrails 0\n            -clipGhosts 0\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1226\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 0 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n"
		+ "                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n"
		+ "                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n"
		+ "                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n"
		+ "                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n"
		+ "                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 19 100 -ps 2 81 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 0\\n    -polymeshes 1\\n    -subdivSurfaces 0\\n    -planes 0\\n    -lights 0\\n    -cameras 0\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 0\\n    -ikHandles 0\\n    -deformers 0\\n    -dynamics 0\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 0\\n    -handles 0\\n    -pivots 0\\n    -textures 0\\n    -strokes 0\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1226\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 0 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 0\\n    -polymeshes 1\\n    -subdivSurfaces 0\\n    -planes 0\\n    -lights 0\\n    -cameras 0\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 0\\n    -ikHandles 0\\n    -deformers 0\\n    -dynamics 0\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 0\\n    -handles 0\\n    -pivots 0\\n    -textures 0\\n    -strokes 0\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1226\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 0 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode2";
	rename -uid "96DF8A48-422F-5D18-6AA9-8AA6A3D2167F";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 48 -ast 0 -aet 50 ";
	setAttr ".st" 6;
createNode animCurveTU -n "worldPlacement_visibility";
	rename -uid "E6E012D5-4FC6-BA7D-76AE-F9BDDDB9F4C2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 3 1 11 1 24 1 32 1 44 1 48 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "worldPlacement_translateX";
	rename -uid "038AD27F-45CC-B66D-28FA-AD96E22CF25C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "worldPlacement_translateY";
	rename -uid "8C0E069F-460E-F48B-EAE5-F88F6AA014A6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "worldPlacement_translateZ";
	rename -uid "8C582EB7-49D9-C340-F96D-F8BA6C477032";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "worldPlacement_rotateX";
	rename -uid "6AB2EA50-4B94-07FE-D7A8-B7894C4D2B5C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "worldPlacement_rotateY";
	rename -uid "78F4CC04-4F26-7CBD-439A-5AA034FEE3CD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "worldPlacement_rotateZ";
	rename -uid "DFDF39C7-49D7-D7EE-948E-BF93B441C44C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "worldPlacement_scaleX";
	rename -uid "70D11C83-4ED4-7C4F-D406-63A4F820FD61";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 3 1 11 1 24 1 32 1 44 1 48 1;
createNode animCurveTU -n "worldPlacement_scaleY";
	rename -uid "D18B5F8D-4548-D6FC-8F14-E3A4FF7BB533";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 3 1 11 1 24 1 32 1 44 1 48 1;
createNode animCurveTU -n "worldPlacement_scaleZ";
	rename -uid "034CD226-46E9-686A-AF51-5991CE3D6032";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 3 1 11 1 24 1 32 1 44 1 48 1;
createNode animCurveTU -n "worldPlacement_midIkCtrl";
	rename -uid "5C93E602-45CC-CAEF-3809-3EB900C00CEC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "worldPlacement_lookAt";
	rename -uid "291E3FD6-4BC9-91EE-93B8-6C87ED43A6A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "l_leg_ik_switch_IkFkSwitch";
	rename -uid "B7AFABE2-4B22-14D2-E296-57977E6F8A7A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 3 1 11 1 24 1 32 1 44 1 48 1;
createNode animCurveTU -n "l_leg_ik_switch_autoStretch";
	rename -uid "FAD7D012-48DD-DB2E-24A1-87A56C033C5A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 3 1 11 1 24 1 32 1 44 1 48 1;
createNode animCurveTU -n "r_leg_ik_switch_IkFkSwitch";
	rename -uid "7E0EE959-47CD-79CC-6D52-A9908B4972D2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 3 1 11 1 24 1 32 1 44 1 48 1;
createNode animCurveTU -n "r_leg_ik_switch_autoStretch";
	rename -uid "3B8ACA63-4C3B-9B64-C193-E78BB2E78668";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 3 1 11 1 24 1 32 1 44 1 48 1;
createNode animCurveTU -n "JawCtrl_visibility";
	rename -uid "A681AFF5-4BEC-D7C5-58D1-E8B0FF21A4D1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 1 5 1 12 1 48 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "JawCtrl_translateX";
	rename -uid "F7748B75-4986-8F1C-5929-D2B9AA4F05BA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 1.1095978640233767e-007 5 1.1095978640233767e-007
		 12 1.1095978640233767e-007 48 1.1095978640233767e-007;
createNode animCurveTL -n "JawCtrl_translateY";
	rename -uid "BB6929CD-45BC-EDB4-2964-BBBDF4026EE5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -114.76858529551514 5 -114.76858529551514
		 12 -114.76858529551514 48 -114.76858529551514;
createNode animCurveTL -n "JawCtrl_translateZ";
	rename -uid "2BF37FC6-4B5C-0307-815D-DBA62E6E9A2D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0.68072052735330368 5 0.68072052735330368
		 12 0.68072052735330368 48 0.68072052735330368;
createNode animCurveTA -n "JawCtrl_rotateX";
	rename -uid "BE81993C-4D1B-4162-420C-3B83AD80B4F9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 25.885957896648971 5 -7.4752368896462515
		 12 -2.2575485057865774 19 13.935764527196667 27 31.957491470954768 41 72.066304555863212
		 48 25.885957896648971;
createNode animCurveTA -n "JawCtrl_rotateY";
	rename -uid "FCE4C374-4A7E-86FC-3BF7-3892EE8C7AC3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 37.901336706104772 5 -1.5989749949980496
		 12 19.068575215160813 19 13.51496494281016 27 10.64713854438552 41 63.335918028671891
		 48 37.901336706104772;
createNode animCurveTA -n "JawCtrl_rotateZ";
	rename -uid "4415491E-457F-79B4-523E-2DAD2EFAC477";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -40.67292724480189 5 -14.624654440934636
		 12 -26.503858027199804 19 -25.196973696633183 27 -18.729783156494786 41 -13.808547337935078
		 48 -40.67292724480189;
createNode animCurveTU -n "JawCtrl_scaleX";
	rename -uid "4E1DBED4-4FFD-3B36-A3C7-4B9B795FF650";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 1.0000000000000002 5 1.0000000000000002
		 12 1.0000000000000002 48 1.0000000000000002;
createNode animCurveTU -n "JawCtrl_scaleY";
	rename -uid "A867A2EE-4B31-B4EC-7E77-3C8514E7B866";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0.99999999999999978 5 0.99999999999999978
		 12 0.99999999999999978 48 0.99999999999999978;
createNode animCurveTU -n "JawCtrl_scaleZ";
	rename -uid "06CF8A84-459D-B0BB-D5BC-E487DE434E61";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0.99999999999999978 5 0.99999999999999978
		 12 0.99999999999999978 48 0.99999999999999978;
createNode animCurveTU -n "l_arm_ik_switch_IkFkSwitch";
	rename -uid "E9202DB0-4554-C971-8A70-D5BC00F3956B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_arm_ik_switch_elbowLock";
	rename -uid "13C6C964-4CE8-952C-8AAF-CAB6D29CAA85";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_arm_ik_switch_autoStretch";
	rename -uid "750B2744-4C15-E42D-6B9F-D1B2CF8F1510";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 3 1 11 1 24 1 32 1 44 1 48 1;
createNode animCurveTU -n "l_hand_ctrl_visibility";
	rename -uid "F7541F89-451C-12BD-3B7C-E5A4053B4944";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 3 1 11 1 24 1 32 1 44 1 48 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "l_hand_ctrl_index";
	rename -uid "787D8DC6-464B-38AD-A1AA-7A8B46B6F5FA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -44.6 3 -44.6 11 -44.6 24 -44.6 32 -44.6
		 44 -44.6 48 -44.6;
createNode animCurveTU -n "l_hand_ctrl_index1";
	rename -uid "09100A76-4694-0033-1DA3-03B7191A5427";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -82.2 3 -82.2 11 -82.2 24 -82.2 32 -82.2
		 44 -82.2 48 -82.2;
createNode animCurveTU -n "l_hand_ctrl_index2";
	rename -uid "22315078-4D2C-F992-793A-5CAA76861E4B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -82.2 3 -82.2 11 -82.2 24 -82.2 32 -82.2
		 44 -82.2 48 -82.2;
createNode animCurveTU -n "l_hand_ctrl_pinky";
	rename -uid "A5785018-45DA-B5C6-0D6D-1A9FF4618CFE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -31.3 3 -31.3 11 -31.3 24 -31.3 32 -31.3
		 44 -31.3 48 -31.3;
createNode animCurveTU -n "l_hand_ctrl_pinky1";
	rename -uid "22E28E80-4EF6-83ED-D884-D897E5A6ADE4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -82.2 3 -82.2 11 -82.2 24 -82.2 32 -82.2
		 44 -82.2 48 -82.2;
createNode animCurveTU -n "l_hand_ctrl_pinky2";
	rename -uid "CE2060FA-48F0-4FC8-9D0E-39A7993AAB0C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -82.2 3 -82.2 11 -82.2 24 -82.2 32 -82.2
		 44 -82.2 48 -82.2;
createNode animCurveTU -n "l_hand_ctrl_thumb";
	rename -uid "BA8E4E01-4299-FF44-EE57-8583003CDB4E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_hand_ctrl_thumb1";
	rename -uid "73F8E712-49DE-A8C2-2880-D9BBD56FC4C1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_hand_ctrl_thumb2";
	rename -uid "9875252D-49CB-0B4E-DBF0-D49C6CF67B84";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_hand_ctrl_thumbReach";
	rename -uid "1F15122E-41C4-876E-343A-6698C62BF452";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_hand_ctrl_thumbTwist";
	rename -uid "89F6C7C5-4A83-1DE2-860A-4491018A089B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_hand_ctrl_indexSpread";
	rename -uid "DB4CA9EB-4041-EBCC-CCAC-E596519E5C1D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_hand_ctrl_pinkySpread";
	rename -uid "E01879E2-4B6E-B354-877B-7DA78E7B4D1F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_hand_ctrl_indexTwist";
	rename -uid "7A30DD96-45A0-4ADA-8E08-CFA3B0E15E1C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_hand_ctrl_pinkyTwist";
	rename -uid "1599067D-4DCC-5B87-5799-309F01195E09";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "r_arm_ik_switch_IkFkSwitch";
	rename -uid "E4E9A3C3-4129-487B-4ECB-61B9BBA084F2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "r_arm_ik_switch_elbowLock";
	rename -uid "5C6CA127-441B-19C9-557E-2CA1291DFBA7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "r_arm_ik_switch_autoStretch";
	rename -uid "DE76C6C3-4D8D-32FA-42C9-3C8211C96737";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 3 1 11 1 24 1 32 1 44 1 48 1;
createNode animCurveTL -n "pelvis_ctrl_translateX";
	rename -uid "56CAD098-4E1A-39C8-878B-7DAA712B1515";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -10.321456821906757 3 -0.27499443834437898
		 32 11.950489627330093 48 -10.321456821906757;
	setAttr -s 4 ".kit[1:3]"  1 18 18;
	setAttr -s 4 ".kot[1:3]"  1 18 18;
	setAttr -s 4 ".kix[1:3]"  0.025058452039957047 1 1;
	setAttr -s 4 ".kiy[1:3]"  0.99968594312667847 0 0;
	setAttr -s 4 ".kox[1:3]"  0.0250584427267313 1 1;
	setAttr -s 4 ".koy[1:3]"  0.99968594312667847 0 0;
createNode animCurveTL -n "pelvis_ctrl_translateY";
	rename -uid "3940CB3F-4977-46A2-1DE3-56A8F7099E38";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -9.8656966706661109 3 -11.023104730532364
		 11 -8.4679057367495201 24 -6.2691822793928607 32 -5.834151135806402 44 -6.9857041629470311
		 48 -9.8656966706661109;
	setAttr -s 7 ".kit[0:6]"  1 18 1 18 18 18 1;
	setAttr -s 7 ".kot[0:6]"  1 18 1 18 18 18 1;
	setAttr -s 7 ".kix[0:6]"  0.044520337134599686 1 0.13429507613182068 
		0.25686278939247131 1 0.13114742934703827 0.044589605182409286;
	setAttr -s 7 ".kiy[0:6]"  -0.99900853633880615 0 0.99094140529632568 
		0.96644788980484009 0 -0.99136286973953247 -0.99900543689727783;
	setAttr -s 7 ".kox[0:6]"  0.044520288705825806 1 0.13429507613182068 
		0.25686278939247131 1 0.13114742934703827 0.044589590281248093;
	setAttr -s 7 ".koy[0:6]"  -0.99900853633880615 0 0.99094140529632568 
		0.96644788980484009 0 -0.99136286973953247 -0.99900543689727783;
createNode animCurveTL -n "pelvis_ctrl_translateZ";
	rename -uid "AAEFCBC2-4EEB-7F95-52F8-A2BACE68E671";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "pelvis_ctrl_rotateX";
	rename -uid "A9AB721F-43C1-CFCE-B354-EA9F15C19F9E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 0 3 13.694129208429507 11 9.1585199341177077
		 24 -4.9909239046345633 48 0;
	setAttr -s 5 ".kit[0:4]"  1 18 18 18 1;
	setAttr -s 5 ".kot[0:4]"  1 18 18 18 1;
	setAttr -s 5 ".kix[0:4]"  0.96553277969360352 1 0.9064563512802124 
		1 0.96534460783004761;
	setAttr -s 5 ".kiy[0:4]"  0.26028156280517578 0 -0.42229947447776794 
		0 0.26097849011421204;
	setAttr -s 5 ".kox[0:4]"  0.96553277969360352 1 0.9064563512802124 
		1 0.96534454822540283;
	setAttr -s 5 ".koy[0:4]"  0.26028150320053101 0 -0.42229950428009033 
		0 0.26097884774208069;
createNode animCurveTA -n "pelvis_ctrl_rotateY";
	rename -uid "D9AEE037-4F48-5B5B-1B4B-E7B379A13576";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 0 11 -53.579489668178169 24 -34.102866705471662
		 44 37.178211471832974 48 0;
	setAttr -s 5 ".kit[1:4]"  18 1 18 1;
	setAttr -s 5 ".kot[1:4]"  18 1 18 1;
	setAttr -s 5 ".kix[0:4]"  0.26206600666046143 1 0.37563496828079224 
		1 0.25894221663475037;
	setAttr -s 5 ".kiy[0:4]"  -0.96504998207092285 0 0.92676770687103271 
		0 -0.96589279174804688;
	setAttr -s 5 ".kox[0:4]"  0.26206555962562561 1 0.37563520669937134 
		1 0.25894176959991455;
	setAttr -s 5 ".koy[0:4]"  -0.96505004167556763 0 0.92676764726638794 
		0 -0.96589291095733643;
createNode animCurveTA -n "pelvis_ctrl_rotateZ";
	rename -uid "9A53F038-409A-DAF2-4D4B-25BA3208E244";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 10.665340208588947 11 18.591371141562529
		 32 -7.6971962109978769 48 10.665340208588947;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.86517161130905151 1 1 0.86679589748382568;
	setAttr -s 4 ".kiy[0:3]"  0.50147587060928345 0 0 0.49866306781768799;
	setAttr -s 4 ".kox[0:3]"  0.86517149209976196 1 1 0.86679589748382568;
	setAttr -s 4 ".koy[0:3]"  0.50147604942321777 0 0 0.49866306781768799;
createNode animCurveTL -n "hips_ctrl_translateX";
	rename -uid "2768C3B1-446F-7D93-3971-DC864CF0EABC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "hips_ctrl_translateY";
	rename -uid "C770C315-4A77-9B24-AE28-29AA20B49FFF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "hips_ctrl_translateZ";
	rename -uid "BB060A14-453E-DA26-EB11-53AA1E0F2305";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "hips_ctrl_rotateX";
	rename -uid "9BE3E50C-4E3B-6637-CB9E-748606EAA911";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 -7.2475231911227471 11 -7.2475231911227471
		 24 -7.2475231911227471 32 -7.2475231911227471 44 -7.2475231911227471 48 0;
createNode animCurveTA -n "hips_ctrl_rotateY";
	rename -uid "8CA91055-449C-8E96-112F-CF8F65C0A210";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 -1.9667103926665044 11 -1.9667103926665044
		 24 -1.9667103926665044 32 -1.9667103926665044 44 -1.9667103926665044 48 0;
createNode animCurveTA -n "hips_ctrl_rotateZ";
	rename -uid "5E919945-4037-44A0-D27D-B39E293D6F4B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 18.611692062174249 3 21.222516126140285
		 11 21.222516126140285 24 21.222516126140285 32 21.222516126140285 44 21.222516126140285
		 48 18.611692062174249;
createNode animCurveTA -n "spine_02_ctrl_rotateX";
	rename -uid "04646976-4E19-F4B0-8E04-F186306B502D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 -10.062035509963902 11 -44.522973098887817
		 24 -32.157801827199457 44 -18.867378669124669 48 -10.062035509963902;
createNode animCurveTA -n "spine_02_ctrl_rotateY";
	rename -uid "C4962255-4FD9-B39A-FD8C-B6B8C9A10564";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 -48.388974413928736 11 95.83132232032969
		 24 84.748884321322535 44 -38.980931937583243 48 -48.388974413928736;
createNode animCurveTA -n "spine_02_ctrl_rotateZ";
	rename -uid "FC6824C7-4554-C20B-ED6D-328158E280DF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 -21.117875641659385 11 -1.1819705525044149
		 24 8.1812203179885739 44 -20.442793838985693 48 -21.117875641659385;
createNode animCurveTL -n "topSpine_ctrl_translateX";
	rename -uid "A895DB4A-4964-A458-BD46-C09B9FDC21D4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "topSpine_ctrl_translateY";
	rename -uid "6EAEF4BB-4301-5786-1730-CF818C68AF43";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "topSpine_ctrl_translateZ";
	rename -uid "7DEEC0D3-4919-C748-3C43-AD8AF0EB89C6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "topSpine_ctrl_rotateX";
	rename -uid "2EFDE518-48C1-D5E6-BE75-209709B7A083";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "topSpine_ctrl_rotateY";
	rename -uid "5C341603-44A1-857C-C991-4784FC7941EC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "topSpine_ctrl_rotateZ";
	rename -uid "1A1868DE-4855-F4FE-BE5E-9DBDE9E7208D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "topSpine_ctrl_spineScale";
	rename -uid "023DC3C2-42D7-AE42-9450-32B8F8645677";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "l_clav_rig_ctrl_01_translateX";
	rename -uid "6919CCA3-47A2-7BB4-8DE2-F283670BE7CD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "l_clav_rig_ctrl_01_translateY";
	rename -uid "A23F5D3B-44F7-8B7B-C6C6-059B18B89389";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "l_clav_rig_ctrl_01_translateZ";
	rename -uid "3B10EC90-4378-2431-B4C5-69B398F4510A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "l_clav_rig_ctrl_01_rotateX";
	rename -uid "91F03A98-40E8-06F4-7ED7-31B003624AEE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "l_clav_rig_ctrl_01_rotateY";
	rename -uid "68324DAB-44FD-51FC-5A41-D58EBDFBBFA2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "l_clav_rig_ctrl_01_rotateZ";
	rename -uid "AEE119AC-43C8-0078-D196-FD9A5BDA57D8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "l_shoulder_fk_ctrl_01_translateX";
	rename -uid "43EADCE0-4BEC-E715-7757-94887DF7D80A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 0 10 0 20 0 39 0 48 0;
createNode animCurveTL -n "l_shoulder_fk_ctrl_01_translateY";
	rename -uid "904AAF48-4ED6-4B5C-02EC-2398B7B39046";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 0 10 0 20 0 39 0 48 0;
createNode animCurveTL -n "l_shoulder_fk_ctrl_01_translateZ";
	rename -uid "A260F9D3-4BEE-B1F1-A2A9-9AA9CBBCEDAD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 0 10 0 20 0 39 0 48 0;
createNode animCurveTA -n "l_shoulder_fk_ctrl_01_rotateX";
	rename -uid "7CDD3584-493D-BAD1-BB9F-01A1D5A09128";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 16.203777003200326 10 -22.606109980882042
		 20 -17.700521513690926 39 -29.57583311002448 48 16.203777003200326;
createNode animCurveTA -n "l_shoulder_fk_ctrl_01_rotateY";
	rename -uid "F87A129A-4180-0169-D6CD-5E8AF0D12662";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 -33.079713443291276 10 43.751074217065366
		 20 6.8613056108953394 39 19.939180194490117 48 -33.079713443291276;
createNode animCurveTA -n "l_shoulder_fk_ctrl_01_rotateZ";
	rename -uid "852E166A-4182-8240-CC1D-A49D661867C4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 -74.945925044578431 10 -75.327208499569551
		 20 -85.312191005766223 39 -102.29839620052178 48 -74.945925044578431;
createNode animCurveTA -n "l_elbow_fk_ctrl_01_rotateY";
	rename -uid "1D004402-439D-0FF1-04AD-5885F89627AB";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 8 -19.141270768166411 14 38.072762353141414
		 27 -24.693710398269577 44 71.688027707390674 48 0;
	setAttr -s 6 ".kit[1:5]"  18 1 18 1 1;
	setAttr -s 6 ".kot[1:5]"  18 1 18 1 1;
	setAttr -s 6 ".kix[0:5]"  0.46463540196418762 1 1 1 1 0.46605873107910156;
	setAttr -s 6 ".kiy[0:5]"  -0.88550204038619995 0 0 0 0 -0.88475388288497925;
	setAttr -s 6 ".kox[0:5]"  0.46463525295257568 1 1 1 1 0.46605873107910156;
	setAttr -s 6 ".koy[0:5]"  -0.88550221920013428 0 0 0 0 -0.88475382328033447;
createNode animCurveTL -n "l_wrist_fk_ctrl_01_translateX";
	rename -uid "8AC71874-4FAC-FC46-891B-5FB8E97883F3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "l_wrist_fk_ctrl_01_translateY";
	rename -uid "7F52D04E-4C08-BEBA-C11E-198C480A430B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "l_wrist_fk_ctrl_01_translateZ";
	rename -uid "3DDCCC14-44DA-EBE7-0DAE-5BBBA0CDD4C7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "l_wrist_fk_ctrl_01_rotateX";
	rename -uid "00CC855F-4280-6CBC-03FC-C0871013AE11";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 33.398241267178939 3 33.398241267178939
		 11 33.398241267178939 24 33.398241267178939 32 33.398241267178939 44 33.398241267178939
		 48 33.398241267178939;
createNode animCurveTA -n "l_wrist_fk_ctrl_01_rotateY";
	rename -uid "79773D4D-4FA2-1ACC-5F58-9E819EE17BFE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 3.6060879842433669 3 3.6060879842433669
		 11 3.6060879842433669 24 3.6060879842433669 32 3.6060879842433669 44 3.6060879842433669
		 48 3.6060879842433669;
createNode animCurveTA -n "l_wrist_fk_ctrl_01_rotateZ";
	rename -uid "F7722EEC-48F8-9186-3C02-31BA13BC16C3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 12.944890796288306 3 12.944890796288306
		 11 12.944890796288306 24 12.944890796288306 32 12.944890796288306 44 12.944890796288306
		 48 12.944890796288306;
createNode animCurveTL -n "r_clav_rig_ctrl_01_translateX";
	rename -uid "5CC99914-4DA3-496B-88C3-ABB3F8D5C934";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "r_clav_rig_ctrl_01_translateY";
	rename -uid "8E586489-4507-F7DA-7634-E1986C016A50";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "r_clav_rig_ctrl_01_translateZ";
	rename -uid "780FC224-4F6F-7F1A-50F6-D5B2A9461107";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "r_clav_rig_ctrl_01_rotateX";
	rename -uid "381D3EBB-46FD-D7CC-DDD2-E182A0F72DB7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "r_clav_rig_ctrl_01_rotateY";
	rename -uid "EE3A7760-4620-BAFC-9F54-89A97CE5E243";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "r_clav_rig_ctrl_01_rotateZ";
	rename -uid "ED2A3C10-453F-A99B-9E9A-34AF329C8C04";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "r_wrist_fk_ctrl_01_translateX";
	rename -uid "E9E98EA3-48EB-1B6C-9D9F-37857C59402E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  0 0 8 0 12 0 16 0 20 0 24 0 28 0 32 0 36 0
		 40 0 44 0 48 0;
createNode animCurveTL -n "r_wrist_fk_ctrl_01_translateY";
	rename -uid "26FCB8A9-4585-121F-99FB-788BE094A069";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  0 0 8 0 12 0 16 0 20 0 24 0 28 0 32 0 36 0
		 40 0 44 0 48 0;
createNode animCurveTL -n "r_wrist_fk_ctrl_01_translateZ";
	rename -uid "6CB0E965-4495-A961-8BAE-6A9FB841B1E3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  0 0 8 0 12 0 16 0 20 0 24 0 28 0 32 0 36 0
		 40 0 44 0 48 0;
createNode animCurveTA -n "r_wrist_fk_ctrl_01_rotateX";
	rename -uid "E192210C-4FC5-EB3D-4707-1CB5687DCF3E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 58.226316623094519 48 58.226316623094519;
createNode animCurveTA -n "r_wrist_fk_ctrl_01_rotateY";
	rename -uid "80451991-4026-053C-ED35-439996B4DCD1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  0 45.080761265948198 2 51.241042065752104
		 6 60.680863036264476 10 40.803144103381456 14 51.052177378203254 18 42.13610244909956
		 22 64.227792534577276 26 35.344153305533858 30 57.108428605733891 34 33.603458729970228
		 42 60.778731077464727 46 38.920463942960431 48 45.080761265948198;
	setAttr -s 13 ".kit[0:12]"  1 18 18 18 18 18 18 18 
		18 18 18 1 1;
	setAttr -s 13 ".kot[0:12]"  1 18 18 18 18 18 18 18 
		18 18 18 1 1;
	setAttr -s 13 ".kix[0:12]"  0.53266370296478271 0.59200447797775269 
		1 1 1 1 1 1 1 1 1 1 0.066666841506958008;
	setAttr -s 13 ".kiy[0:12]"  0.84632700681686401 0.8059346079826355 
		0 0 0 0 0 0 0 0 0 0 0.1612762063741684;
	setAttr -s 13 ".kox[0:12]"  0.53266352415084839 0.59200453758239746 
		1 1 1 1 1 1 1 1 1 1 0.066666483879089355;
	setAttr -s 13 ".koy[0:12]"  0.84632712602615356 0.8059346079826355 
		0 0 0 0 0 0 0 0 0 0 0.16127587854862213;
createNode animCurveTA -n "r_wrist_fk_ctrl_01_rotateZ";
	rename -uid "76A23475-4F35-EB49-97A5-43ADFC278374";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -63.628738999650963 48 -63.628738999650963;
createNode animCurveTL -n "neckBase_ctrl_translateX";
	rename -uid "9ED34AD9-4EFF-B00E-1F6A-7FA11BE171E5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "neckBase_ctrl_translateY";
	rename -uid "3E007C10-4BA6-CBE0-95C3-2482F2B0532C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "neckBase_ctrl_translateZ";
	rename -uid "3C331B14-4BA7-AF1F-0DD6-7FA9A02D3A1D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "neckBase_ctrl_rotateX";
	rename -uid "2AB8D9F9-457A-42EF-1997-50A9B17130FB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "neckBase_ctrl_rotateY";
	rename -uid "7369B2DA-46F9-AE89-633F-7FA05F9C8ECA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "neckBase_ctrl_rotateZ";
	rename -uid "740C5FEF-4248-8FF1-5B1B-3FB03B837889";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "neckBase_ctrl_rotateWith";
	rename -uid "87631B86-4493-735B-D5D1-E8B12CE0BD25";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "l_foot_ik_ctrl_translateX";
	rename -uid "8C7DF378-4A2D-8C4B-6A39-BA825B746EAC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -4.9941881332379978 3 0 11 0 24 0 32 0
		 44 0 48 -4.9941881332379978;
createNode animCurveTL -n "l_foot_ik_ctrl_translateY";
	rename -uid "8B8C5727-4E8E-3BFA-2531-30B88CC36E9D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 7.7460887010554345 3 0 11 0 24 0 32 0
		 44 0 48 7.7460887010554345;
createNode animCurveTL -n "l_foot_ik_ctrl_translateZ";
	rename -uid "98359B62-4375-8BA5-C309-BE814441981E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 8.8817841970012523e-016 3 20.484042666770023
		 44 -26.127482041095021 48 0;
	setAttr -s 4 ".kit[1:3]"  18 2 1;
	setAttr -s 4 ".kot[1:3]"  2 18 1;
	setAttr -s 4 ".ktl[1:3]" no no yes;
	setAttr -s 4 ".kix[0:3]"  0.0019536721520125866 1 0.029307764023542404 
		0.0019670960027724504;
	setAttr -s 4 ".kiy[0:3]"  0.99999809265136719 0 -0.99957042932510376 
		0.99999809265136719;
	setAttr -s 4 ".kox[0:3]"  0.0019536714535206556 0.029307764023542404 
		1 0.001967092277482152;
	setAttr -s 4 ".koy[0:3]"  0.99999809265136719 -0.99957042932510376 
		0 0.99999809265136719;
createNode animCurveTA -n "l_foot_ik_ctrl_rotateX";
	rename -uid "13167BEB-411F-AD9F-7E3D-F08D449B0BB1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 31.210246409135681 3 0 11 0 24 0 32 0
		 44 0 48 31.210246409135681;
createNode animCurveTA -n "l_foot_ik_ctrl_rotateY";
	rename -uid "51C6F208-440B-1CE5-3096-9EAE72F0BBD2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 6.8033565539370917 3 6.8033565539370917
		 11 6.8033565539370917 24 6.8033565539370917 32 6.8033565539370917 44 6.8033565539370917
		 48 6.8033565539370917;
createNode animCurveTA -n "l_foot_ik_ctrl_rotateZ";
	rename -uid "BAD973EE-432F-343F-D903-B79C7694800E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -26.176761016655011 3 0 11 0 24 0 32 0
		 44 0 48 -26.176761016655011;
createNode animCurveTU -n "l_foot_ik_ctrl_xOffset";
	rename -uid "1C2CA41F-4F69-8309-7F1F-359C12B7A09D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_foot_ik_ctrl_yOffset";
	rename -uid "A1409C17-4E6D-61C7-6863-22AA50724D13";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_foot_ik_ctrl_zOffset";
	rename -uid "EAF199CE-47C6-6FC2-A17C-E8816782BFE5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_foot_ik_ctrl_stretch";
	rename -uid "A45989A6-4E85-17CB-58AB-CDB2033B14AB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_foot_ik_ctrl_heelRoll";
	rename -uid "913ED98F-4F1C-B012-C99C-8A8BD5625CA6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_foot_ik_ctrl_ballRoll";
	rename -uid "690B8E3D-43DE-ADD1-6F32-11857ED4BA9A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 22.700000000000003
		 48 0;
createNode animCurveTU -n "l_foot_ik_ctrl_toeRoll";
	rename -uid "52A14923-41BF-E7CA-280E-A49893012233";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_foot_ik_ctrl_toeTap";
	rename -uid "69F8C7BC-4FCD-DDFC-B27E-2284BD181F1C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_foot_ik_ctrl_heelPivot";
	rename -uid "22390B81-4F23-0765-3AC7-BD86A4A9B63B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_foot_ik_ctrl_ballPivot";
	rename -uid "F920A093-45B2-8343-4EE8-4BAC7C16FBA9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_foot_ik_ctrl_toePivot";
	rename -uid "5864151B-4AB3-64F4-6E5B-E28FF8645A29";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_foot_ik_ctrl_heelSide";
	rename -uid "A9C46A09-48CC-E24C-0AC8-D5B4C14DE4D3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_foot_ik_ctrl_ballSide";
	rename -uid "32C66BE5-4BA7-8DBA-D662-C68F8EF90D98";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_foot_ik_ctrl_toeSide";
	rename -uid "A9A7C6CF-41D4-806C-426B-88B40355E2D7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_foot_ik_ctrl_space";
	rename -uid "7AE09AA6-4779-B9DE-E7A3-F095A6569015";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "r_foot_ik_ctrl_translateX";
	rename -uid "CB4651F0-492B-1AA3-29A5-B5B562BDD9FD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 -10.836960331836949 5 -10.836960331836949
		 24 3.7703228877623367 32 3.7703228877623367 48 -10.836960331836949;
	setAttr -s 5 ".kit[2:4]"  1 1 18;
	setAttr -s 5 ".kot[2:4]"  1 1 18;
	setAttr -s 5 ".kix[2:4]"  0.046371925622224808 0.041011609137058258 
		1;
	setAttr -s 5 ".kiy[2:4]"  0.99892425537109375 -0.99915868043899536 
		0;
	setAttr -s 5 ".kox[2:4]"  0.046371925622224808 0.04101162776350975 
		1;
	setAttr -s 5 ".koy[2:4]"  0.99892431497573853 -0.99915868043899536 
		0;
createNode animCurveTL -n "r_foot_ik_ctrl_translateY";
	rename -uid "CEBFD086-4A05-6CD9-CA18-A4837A02EDC6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 0 5 0 11 0 41 0 48 0;
createNode animCurveTL -n "r_foot_ik_ctrl_translateZ";
	rename -uid "E1297906-41F9-F957-0B09-7D883939E7AF";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 11.421342243698824 5 1.1177487534154587
		 11 -4.2901919948752258 32 7.526179863205094 41 23.93378666492486 48 11.421342243698824;
	setAttr -s 6 ".kit[1:5]"  2 18 1 18 1;
	setAttr -s 6 ".kot[1:5]"  18 18 1 2 1;
	setAttr -s 6 ".ktl[1:5]" no yes yes no yes;
	setAttr -s 6 ".kix[0:5]"  0.011549253016710281 0.016173470765352249 
		1 0.020605197176337242 1 0.019008317962288857;
	setAttr -s 6 ".kiy[0:5]"  -0.99993330240249634 -0.99986922740936279 
		0 0.99978774785995483 0 -0.99981939792633057;
	setAttr -s 6 ".kox[0:5]"  0.011549264192581177 0.023331064730882645 
		1 0.020605187863111496 0.018644861876964569 0.019008317962288857;
	setAttr -s 6 ".koy[0:5]"  -0.99993330240249634 -0.99972778558731079 
		0 0.99978774785995483 -0.99982619285583496 -0.99981939792633057;
createNode animCurveTA -n "r_foot_ik_ctrl_rotateX";
	rename -uid "956C63C5-4665-F357-6491-F98750A27029";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 11 0 24 72.903025278685945 32 116.89879733329404
		 37 70.350014289602726 41 0 48 0;
createNode animCurveTA -n "r_foot_ik_ctrl_rotateY";
	rename -uid "01888821-45AA-8A29-EDDE-0CBFD698CD46";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 119.49095832725905 5 119.49095832725905
		 11 119.49095832725905 24 91.383794360385181 32 47.909142587793966 37 106.76208936490605
		 41 119.49095832725905 48 119.49095832725905;
createNode animCurveTA -n "r_foot_ik_ctrl_rotateZ";
	rename -uid "52AD5CEE-4911-719C-2713-6A9F716632AA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 11 0 24 -59.075646420456991 32 -76.924025588713903
		 37 -45.419941500254232 41 0 48 0;
createNode animCurveTU -n "r_foot_ik_ctrl_xOffset";
	rename -uid "6AE344CC-4024-B395-7937-DF89271B2493";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 11 0 24 0 32 0 41 0 48 0;
createNode animCurveTU -n "r_foot_ik_ctrl_yOffset";
	rename -uid "69F69E69-4CB2-8708-6A9D-3EA3F45F7DFB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 11 0 24 0 32 0 41 0 48 0;
createNode animCurveTU -n "r_foot_ik_ctrl_zOffset";
	rename -uid "0CAD9D42-4044-763E-33E8-6DAD19CA9A0C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 11 0 24 0 32 0 41 0 48 0;
createNode animCurveTU -n "r_foot_ik_ctrl_stretch";
	rename -uid "F95A8D3A-4139-76A6-32F4-B9B099A7053A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 11 0 24 0 32 0 41 0 48 0;
createNode animCurveTU -n "r_foot_ik_ctrl_heelRoll";
	rename -uid "FEEA3E53-4E5E-0DC6-80CA-AF8AD131B9FB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 11 0 24 0 32 0 41 0 48 0;
createNode animCurveTU -n "r_foot_ik_ctrl_ballRoll";
	rename -uid "7A87A96C-4625-7527-D1D0-DBB9AFE80E02";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 11 49 24 0 32 0 41 0 48 0;
createNode animCurveTU -n "r_foot_ik_ctrl_toeRoll";
	rename -uid "F9C19C8E-4406-F348-0815-9A8E71454236";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 11 0 24 0 32 0 41 0 48 0;
createNode animCurveTU -n "r_foot_ik_ctrl_toeTap";
	rename -uid "FD36D296-418A-FBD1-D40C-9B8F8FD49645";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 11 0 24 0 32 0 41 0 48 0;
createNode animCurveTU -n "r_foot_ik_ctrl_heelPivot";
	rename -uid "7DC5C336-4291-C789-1FB9-B3A413363FF1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 11 0 24 0 32 0 41 0 48 0;
createNode animCurveTU -n "r_foot_ik_ctrl_ballPivot";
	rename -uid "A71D3065-437E-A02F-4019-E993E55837A5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 11 0 24 0 32 0 41 0 48 0;
createNode animCurveTU -n "r_foot_ik_ctrl_toePivot";
	rename -uid "1312490C-4438-75DF-25C1-FBB3DB54C2DF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 11 0 24 0 32 0 41 0 48 0;
createNode animCurveTU -n "r_foot_ik_ctrl_heelSide";
	rename -uid "0B48B459-4A55-C485-4FD4-10B64CBB276D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 11 34.4 24 0 32 0 41 0 48 0;
createNode animCurveTU -n "r_foot_ik_ctrl_ballSide";
	rename -uid "26EBF847-4F8E-F538-6105-78ADA6A14BCD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 11 0 24 0 32 0 41 0 48 0;
createNode animCurveTU -n "r_foot_ik_ctrl_toeSide";
	rename -uid "9D1FBAB5-4FE1-FA24-947C-D985DB63CD91";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 11 0 24 0 32 0 41 0 48 0;
createNode animCurveTU -n "r_foot_ik_ctrl_space";
	rename -uid "EEF3D662-4836-6016-96A2-46871637576C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 11 0 24 0 32 0 41 0 48 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "r_leg_pv_01_translateX";
	rename -uid "A840E2CC-485E-6ACE-553C-0B8578531C2A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 77.607972466215188 3 77.607972466215188
		 11 77.607972466215188 24 77.607972466215188 44 77.607972466215188 48 77.607972466215188;
createNode animCurveTL -n "r_leg_pv_01_translateY";
	rename -uid "D9AE4D9A-4B6D-E4F5-E05F-C48577899CA9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 3 0 11 0 24 0 44 0 48 0;
createNode animCurveTL -n "r_leg_pv_01_translateZ";
	rename -uid "D7948CC0-4507-E4AD-BA37-1D97E1D07E65";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 -87.386784232732495 3 -87.386784232732495
		 11 -87.386784232732495 24 -125.56895116246702 44 -150.99863920402706 48 -87.386784232732495;
createNode animCurveTA -n "r_leg_pv_01_rotateX";
	rename -uid "637054D4-4A23-1443-4C87-5B9F4677C355";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 3 0 11 0 24 0 44 0 48 0;
createNode animCurveTA -n "r_leg_pv_01_rotateY";
	rename -uid "202DCE36-496D-6B25-9A0E-E594D449C168";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 3 0 11 0 24 0 44 0 48 0;
createNode animCurveTA -n "r_leg_pv_01_rotateZ";
	rename -uid "DD4C12D7-41B9-68CE-B177-07AD289CDB6E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 3 0 11 0 24 0 44 0 48 0;
createNode animCurveTU -n "r_leg_pv_01_space";
	rename -uid "9D3CBD79-4D0C-57CF-DEEE-24B61F86147D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 3 0 11 0 24 0 44 0 48 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTL -n "l_leg_pv_01_translateX";
	rename -uid "83678423-4655-5D65-A4D2-5F8D5D48F3C8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "l_leg_pv_01_translateY";
	rename -uid "4BA7B8DA-41E4-4A9D-18E8-46A95219A9D8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTL -n "l_leg_pv_01_translateZ";
	rename -uid "926E622E-4802-5B21-FA34-EB88F48E81AD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "l_leg_pv_01_rotateX";
	rename -uid "52212EA2-4719-BAAF-8C0C-E19216B17843";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "l_leg_pv_01_rotateY";
	rename -uid "31F49979-45CC-E7E5-107C-BCB80B5F4EAD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTA -n "l_leg_pv_01_rotateZ";
	rename -uid "A5BA11F5-451A-9696-6065-DFAA3C69CD3B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
createNode animCurveTU -n "l_leg_pv_01_space";
	rename -uid "A76C8FBA-4D51-DEC9-5EDF-6EB6C72B5F09";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 3 0 11 0 24 0 32 0 44 0 48 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "r_shoulder_fk_ctrl_01_rotateX";
	rename -uid "F5044A19-4938-D078-87E7-22A565068349";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  0 -65.516969749522502 4 -36.984255211039219
		 8 -55.762493363028504 12 -33.571104228297436 16 -62.116570089941334 20 -34.733387952139793
		 24 -64.040083539563398 28 -36.778705131953544 32 -66.084242678368369 36 -38.796597883229033
		 40 -68.045133211531393 44 -40.615603829199685 48 -65.516969749522502;
createNode animCurveTA -n "r_shoulder_fk_ctrl_01_rotateY";
	rename -uid "47C354C8-4A82-2AC8-2DFF-08BFDC172E9C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  0 145.17364934010533 4 158.3789161654779
		 8 151.49743484686627 12 158.79641045896366 16 143.65866682820453 20 158.63787745241274
		 24 143.39630372043848 28 158.35890058072994 32 143.11748480318525 36 158.08366433841297
		 40 142.85002353636489 44 157.83555582537156 48 145.17364934010533;
createNode animCurveTA -n "r_shoulder_fk_ctrl_01_rotateZ";
	rename -uid "E7BFC0F0-4024-8AEB-98A8-43A060333911";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  0 98.499945453801061 4 111.53092606612671
		 8 104.53179747384442 12 112.17411921739777 16 98.117644197871172 20 111.94112767018959
		 24 97.732056431050154 28 111.53112311209658 32 97.322284020481689 36 111.12661606259651
		 40 96.929203639631567 44 110.76197787981337 48 98.499945453801061;
createNode animCurveTL -n "r_shoulder_fk_ctrl_01_translateX";
	rename -uid "1ABADC49-48EA-47E1-8197-B28A959363D2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  0 0 4 0 16 0 20 0 24 0 28 0 32 0 36 0 40 0
		 44 0 48 0;
createNode animCurveTL -n "r_shoulder_fk_ctrl_01_translateY";
	rename -uid "B0B2EA0B-4FAA-F83E-E13A-CBB3491EF02D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  0 0 4 0 16 0 20 0 24 0 28 0 32 0 36 0 40 0
		 44 0 48 0;
createNode animCurveTL -n "r_shoulder_fk_ctrl_01_translateZ";
	rename -uid "C776C455-49FB-FE9E-0345-3183ADF829C9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  0 0 4 0 16 0 20 0 24 0 28 0 32 0 36 0 40 0
		 44 0 48 0;
createNode animCurveTU -n "r_shoulder_fk_ctrl_01_rotateWith";
	rename -uid "41A0E51D-417A-FDC8-4ACF-318D57BE5AF5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  0 2 4 2 16 2 20 2 24 2 28 2 32 2 36 2 40 2
		 44 2 48 2;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "r_elbow_fk_ctrl_01_rotateY";
	rename -uid "2DDFF90C-4A8F-D6C6-19F5-F5BA445A3228";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  0 -74.269658133916877 4 -80.127802220516415
		 8 -74.269658133916877 12 -81.746507065891123 16 -74.269658133916877 20 -81.746507065891123
		 24 -74.269658133916877 28 -81.746507065891123 32 -74.269658133916877 36 -81.746507065891123
		 40 -74.269658133916877 44 -81.746507065891123 48 -74.269658133916877;
createNode animCurveTL -n "neck_ctrl_translateX";
	rename -uid "22295321-41B6-9771-D92A-BC8E6FA92AF1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 48 0;
createNode animCurveTL -n "neck_ctrl_translateY";
	rename -uid "4759E97D-48E4-DE4C-4B44-30B82AE1F5C4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 48 0;
createNode animCurveTL -n "neck_ctrl_translateZ";
	rename -uid "96D82859-4B11-52AF-6608-DB8B714233C0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 48 0;
createNode animCurveTA -n "neck_ctrl_rotateX";
	rename -uid "C686AF25-467B-39BF-E4A6-239B4621A01D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -21.444517399597277 5 34.420943308518758
		 44 -66.973449107782386 48 -21.444517399597277;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.11505360156297684 1 1 0.12081322073936462;
	setAttr -s 4 ".kiy[0:3]"  0.99335932731628418 0 0 0.9926753044128418;
	setAttr -s 4 ".kox[0:3]"  0.11505363136529922 1 1 0.12081326544284821;
	setAttr -s 4 ".koy[0:3]"  0.9933592677116394 0 0 0.99267524480819702;
createNode animCurveTA -n "neck_ctrl_rotateY";
	rename -uid "A86B66FF-4609-8AC0-103C-C6AB5FC9BE38";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -11.172282573533053 5 3.3693892631287912
		 44 2.3964455117114802 48 -11.172282573533053;
createNode animCurveTA -n "neck_ctrl_rotateZ";
	rename -uid "5DED4B1D-4C8B-B933-F423-67AB007F43AC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -26.000147285453458 5 10.238999338255821
		 44 -34.082668752731017 48 -26.000147285453458;
	setAttr -s 4 ".kit[0:3]"  1 18 18 1;
	setAttr -s 4 ".kot[0:3]"  1 18 18 1;
	setAttr -s 4 ".kix[0:3]"  0.36017543077468872 1 1 0.31535843014717102;
	setAttr -s 4 ".kiy[0:3]"  0.9328845739364624 0 0 0.94897264242172241;
	setAttr -s 4 ".kox[0:3]"  0.36017423868179321 1 1 0.3153584897518158;
	setAttr -s 4 ".koy[0:3]"  0.93288511037826538 0 0 0.94897264242172241;
createNode animCurveTU -n "neck_ctrl_scaleX";
	rename -uid "BF0D2FBE-4782-5BF2-DC08-EC9816898DEC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 48 1;
createNode animCurveTU -n "neck_ctrl_scaleY";
	rename -uid "5A880FA3-4301-1078-39B5-2E8E24E53BCC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 48 1;
createNode animCurveTU -n "neck_ctrl_rotateWith";
	rename -uid "908064BA-4460-F1E4-91C3-81A3B133B46E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 2 48 2;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "l_shoulder_fk_ctrl_01_rotateWith";
	rename -uid "9C285438-4752-D8BB-B900-7DA72BED03A7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 2 10 2 20 2 39 2 48 2;
	setAttr -s 5 ".kot[0:4]"  5 5 5 5 5;
select -ne :time1;
	setAttr ".o" 0;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 6 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 114 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 3 ".r";
select -ne :defaultTextureList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 3 ".sol";
connectAttr "worldPlacement_midIkCtrl.o" "Grunt_Skeleton_RigRN.phl[6]";
connectAttr "worldPlacement_lookAt.o" "Grunt_Skeleton_RigRN.phl[7]";
connectAttr "worldPlacement_rotateX.o" "Grunt_Skeleton_RigRN.phl[8]";
connectAttr "worldPlacement_rotateY.o" "Grunt_Skeleton_RigRN.phl[9]";
connectAttr "worldPlacement_rotateZ.o" "Grunt_Skeleton_RigRN.phl[10]";
connectAttr "worldPlacement_translateX.o" "Grunt_Skeleton_RigRN.phl[11]";
connectAttr "worldPlacement_translateY.o" "Grunt_Skeleton_RigRN.phl[12]";
connectAttr "worldPlacement_translateZ.o" "Grunt_Skeleton_RigRN.phl[13]";
connectAttr "worldPlacement_scaleY.o" "Grunt_Skeleton_RigRN.phl[14]";
connectAttr "worldPlacement_scaleX.o" "Grunt_Skeleton_RigRN.phl[15]";
connectAttr "worldPlacement_scaleZ.o" "Grunt_Skeleton_RigRN.phl[16]";
connectAttr "worldPlacement_visibility.o" "Grunt_Skeleton_RigRN.phl[17]";
connectAttr "Grunt_Skeleton_RigRN.phl[18]" "ref_frame_pointConstraint1.tg[0].tt"
		;
connectAttr "Grunt_Skeleton_RigRN.phl[19]" "ref_frame_pointConstraint1.tg[0].trp"
		;
connectAttr "Grunt_Skeleton_RigRN.phl[20]" "ref_frame_pointConstraint1.tg[0].trt"
		;
connectAttr "Grunt_Skeleton_RigRN.phl[21]" "ref_frame_pointConstraint1.tg[0].tpm"
		;
connectAttr "l_leg_ik_switch_IkFkSwitch.o" "Grunt_Skeleton_RigRN.phl[22]";
connectAttr "l_leg_ik_switch_autoStretch.o" "Grunt_Skeleton_RigRN.phl[23]";
connectAttr "r_leg_ik_switch_IkFkSwitch.o" "Grunt_Skeleton_RigRN.phl[24]";
connectAttr "r_leg_ik_switch_autoStretch.o" "Grunt_Skeleton_RigRN.phl[25]";
connectAttr "JawCtrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[26]";
connectAttr "JawCtrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[27]";
connectAttr "JawCtrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[28]";
connectAttr "JawCtrl_visibility.o" "Grunt_Skeleton_RigRN.phl[29]";
connectAttr "JawCtrl_translateX.o" "Grunt_Skeleton_RigRN.phl[30]";
connectAttr "JawCtrl_translateY.o" "Grunt_Skeleton_RigRN.phl[31]";
connectAttr "JawCtrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[32]";
connectAttr "JawCtrl_scaleX.o" "Grunt_Skeleton_RigRN.phl[33]";
connectAttr "JawCtrl_scaleY.o" "Grunt_Skeleton_RigRN.phl[34]";
connectAttr "JawCtrl_scaleZ.o" "Grunt_Skeleton_RigRN.phl[35]";
connectAttr "l_arm_ik_switch_IkFkSwitch.o" "Grunt_Skeleton_RigRN.phl[36]";
connectAttr "l_arm_ik_switch_elbowLock.o" "Grunt_Skeleton_RigRN.phl[37]";
connectAttr "l_arm_ik_switch_autoStretch.o" "Grunt_Skeleton_RigRN.phl[38]";
connectAttr "l_hand_ctrl_index.o" "Grunt_Skeleton_RigRN.phl[39]";
connectAttr "l_hand_ctrl_index1.o" "Grunt_Skeleton_RigRN.phl[40]";
connectAttr "l_hand_ctrl_index2.o" "Grunt_Skeleton_RigRN.phl[41]";
connectAttr "l_hand_ctrl_pinky.o" "Grunt_Skeleton_RigRN.phl[42]";
connectAttr "l_hand_ctrl_pinky1.o" "Grunt_Skeleton_RigRN.phl[43]";
connectAttr "l_hand_ctrl_pinky2.o" "Grunt_Skeleton_RigRN.phl[44]";
connectAttr "l_hand_ctrl_thumb.o" "Grunt_Skeleton_RigRN.phl[45]";
connectAttr "l_hand_ctrl_thumb1.o" "Grunt_Skeleton_RigRN.phl[46]";
connectAttr "l_hand_ctrl_thumb2.o" "Grunt_Skeleton_RigRN.phl[47]";
connectAttr "l_hand_ctrl_thumbReach.o" "Grunt_Skeleton_RigRN.phl[48]";
connectAttr "l_hand_ctrl_thumbTwist.o" "Grunt_Skeleton_RigRN.phl[49]";
connectAttr "l_hand_ctrl_indexSpread.o" "Grunt_Skeleton_RigRN.phl[50]";
connectAttr "l_hand_ctrl_pinkySpread.o" "Grunt_Skeleton_RigRN.phl[51]";
connectAttr "l_hand_ctrl_indexTwist.o" "Grunt_Skeleton_RigRN.phl[52]";
connectAttr "l_hand_ctrl_pinkyTwist.o" "Grunt_Skeleton_RigRN.phl[53]";
connectAttr "l_hand_ctrl_visibility.o" "Grunt_Skeleton_RigRN.phl[54]";
connectAttr "r_arm_ik_switch_IkFkSwitch.o" "Grunt_Skeleton_RigRN.phl[55]";
connectAttr "r_arm_ik_switch_elbowLock.o" "Grunt_Skeleton_RigRN.phl[56]";
connectAttr "r_arm_ik_switch_autoStretch.o" "Grunt_Skeleton_RigRN.phl[57]";
connectAttr "pelvis_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[58]";
connectAttr "pelvis_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[59]";
connectAttr "pelvis_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[60]";
connectAttr "pelvis_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[61]";
connectAttr "pelvis_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[62]";
connectAttr "pelvis_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[63]";
connectAttr "hips_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[64]";
connectAttr "hips_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[65]";
connectAttr "hips_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[66]";
connectAttr "hips_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[67]";
connectAttr "hips_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[68]";
connectAttr "hips_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[69]";
connectAttr "spine_02_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[70]";
connectAttr "spine_02_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[71]";
connectAttr "spine_02_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[72]";
connectAttr "topSpine_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[73]";
connectAttr "topSpine_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[74]";
connectAttr "topSpine_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[75]";
connectAttr "topSpine_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[76]";
connectAttr "topSpine_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[77]";
connectAttr "topSpine_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[78]";
connectAttr "topSpine_ctrl_spineScale.o" "Grunt_Skeleton_RigRN.phl[79]";
connectAttr "l_clav_rig_ctrl_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[80]";
connectAttr "l_clav_rig_ctrl_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[81]";
connectAttr "l_clav_rig_ctrl_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[82]";
connectAttr "l_clav_rig_ctrl_01_translateX.o" "Grunt_Skeleton_RigRN.phl[83]";
connectAttr "l_clav_rig_ctrl_01_translateY.o" "Grunt_Skeleton_RigRN.phl[84]";
connectAttr "l_clav_rig_ctrl_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[85]";
connectAttr "l_shoulder_fk_ctrl_01_rotateWith.o" "Grunt_Skeleton_RigRN.phl[86]";
connectAttr "l_shoulder_fk_ctrl_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[87]";
connectAttr "l_shoulder_fk_ctrl_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[88]";
connectAttr "l_shoulder_fk_ctrl_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[89]";
connectAttr "l_shoulder_fk_ctrl_01_translateX.o" "Grunt_Skeleton_RigRN.phl[90]";
connectAttr "l_shoulder_fk_ctrl_01_translateY.o" "Grunt_Skeleton_RigRN.phl[91]";
connectAttr "l_shoulder_fk_ctrl_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[92]";
connectAttr "l_elbow_fk_ctrl_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[93]";
connectAttr "l_wrist_fk_ctrl_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[94]";
connectAttr "l_wrist_fk_ctrl_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[95]";
connectAttr "l_wrist_fk_ctrl_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[96]";
connectAttr "l_wrist_fk_ctrl_01_translateX.o" "Grunt_Skeleton_RigRN.phl[97]";
connectAttr "l_wrist_fk_ctrl_01_translateY.o" "Grunt_Skeleton_RigRN.phl[98]";
connectAttr "l_wrist_fk_ctrl_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[99]";
connectAttr "r_clav_rig_ctrl_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[100]";
connectAttr "r_clav_rig_ctrl_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[101]";
connectAttr "r_clav_rig_ctrl_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[102]";
connectAttr "r_clav_rig_ctrl_01_translateX.o" "Grunt_Skeleton_RigRN.phl[103]";
connectAttr "r_clav_rig_ctrl_01_translateY.o" "Grunt_Skeleton_RigRN.phl[104]";
connectAttr "r_clav_rig_ctrl_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[105]";
connectAttr "r_shoulder_fk_ctrl_01_rotateWith.o" "Grunt_Skeleton_RigRN.phl[106]"
		;
connectAttr "r_shoulder_fk_ctrl_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[107]";
connectAttr "r_shoulder_fk_ctrl_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[108]";
connectAttr "r_shoulder_fk_ctrl_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[109]";
connectAttr "r_shoulder_fk_ctrl_01_translateX.o" "Grunt_Skeleton_RigRN.phl[110]"
		;
connectAttr "r_shoulder_fk_ctrl_01_translateY.o" "Grunt_Skeleton_RigRN.phl[111]"
		;
connectAttr "r_shoulder_fk_ctrl_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[112]"
		;
connectAttr "r_elbow_fk_ctrl_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[113]";
connectAttr "r_wrist_fk_ctrl_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[114]";
connectAttr "r_wrist_fk_ctrl_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[115]";
connectAttr "r_wrist_fk_ctrl_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[116]";
connectAttr "r_wrist_fk_ctrl_01_translateX.o" "Grunt_Skeleton_RigRN.phl[117]";
connectAttr "r_wrist_fk_ctrl_01_translateY.o" "Grunt_Skeleton_RigRN.phl[118]";
connectAttr "r_wrist_fk_ctrl_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[119]";
connectAttr "neckBase_ctrl_rotateWith.o" "Grunt_Skeleton_RigRN.phl[120]";
connectAttr "neckBase_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[121]";
connectAttr "neckBase_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[122]";
connectAttr "neckBase_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[123]";
connectAttr "neckBase_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[124]";
connectAttr "neckBase_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[125]";
connectAttr "neckBase_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[126]";
connectAttr "neck_ctrl_scaleX.o" "Grunt_Skeleton_RigRN.phl[127]";
connectAttr "neck_ctrl_scaleY.o" "Grunt_Skeleton_RigRN.phl[128]";
connectAttr "neck_ctrl_rotateWith.o" "Grunt_Skeleton_RigRN.phl[129]";
connectAttr "neck_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[130]";
connectAttr "neck_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[131]";
connectAttr "neck_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[132]";
connectAttr "neck_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[133]";
connectAttr "neck_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[134]";
connectAttr "neck_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[135]";
connectAttr "l_foot_ik_ctrl_xOffset.o" "Grunt_Skeleton_RigRN.phl[136]";
connectAttr "l_foot_ik_ctrl_yOffset.o" "Grunt_Skeleton_RigRN.phl[137]";
connectAttr "l_foot_ik_ctrl_zOffset.o" "Grunt_Skeleton_RigRN.phl[138]";
connectAttr "l_foot_ik_ctrl_stretch.o" "Grunt_Skeleton_RigRN.phl[139]";
connectAttr "l_foot_ik_ctrl_heelRoll.o" "Grunt_Skeleton_RigRN.phl[140]";
connectAttr "l_foot_ik_ctrl_ballRoll.o" "Grunt_Skeleton_RigRN.phl[141]";
connectAttr "l_foot_ik_ctrl_toeRoll.o" "Grunt_Skeleton_RigRN.phl[142]";
connectAttr "l_foot_ik_ctrl_toeTap.o" "Grunt_Skeleton_RigRN.phl[143]";
connectAttr "l_foot_ik_ctrl_heelPivot.o" "Grunt_Skeleton_RigRN.phl[144]";
connectAttr "l_foot_ik_ctrl_ballPivot.o" "Grunt_Skeleton_RigRN.phl[145]";
connectAttr "l_foot_ik_ctrl_toePivot.o" "Grunt_Skeleton_RigRN.phl[146]";
connectAttr "l_foot_ik_ctrl_heelSide.o" "Grunt_Skeleton_RigRN.phl[147]";
connectAttr "l_foot_ik_ctrl_ballSide.o" "Grunt_Skeleton_RigRN.phl[148]";
connectAttr "l_foot_ik_ctrl_toeSide.o" "Grunt_Skeleton_RigRN.phl[149]";
connectAttr "l_foot_ik_ctrl_space.o" "Grunt_Skeleton_RigRN.phl[150]";
connectAttr "l_foot_ik_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[151]";
connectAttr "l_foot_ik_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[152]";
connectAttr "l_foot_ik_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[153]";
connectAttr "l_foot_ik_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[154]";
connectAttr "l_foot_ik_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[155]";
connectAttr "l_foot_ik_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[156]";
connectAttr "r_foot_ik_ctrl_xOffset.o" "Grunt_Skeleton_RigRN.phl[157]";
connectAttr "r_foot_ik_ctrl_yOffset.o" "Grunt_Skeleton_RigRN.phl[158]";
connectAttr "r_foot_ik_ctrl_zOffset.o" "Grunt_Skeleton_RigRN.phl[159]";
connectAttr "r_foot_ik_ctrl_stretch.o" "Grunt_Skeleton_RigRN.phl[160]";
connectAttr "r_foot_ik_ctrl_heelRoll.o" "Grunt_Skeleton_RigRN.phl[161]";
connectAttr "r_foot_ik_ctrl_ballRoll.o" "Grunt_Skeleton_RigRN.phl[162]";
connectAttr "r_foot_ik_ctrl_toeRoll.o" "Grunt_Skeleton_RigRN.phl[163]";
connectAttr "r_foot_ik_ctrl_toeTap.o" "Grunt_Skeleton_RigRN.phl[164]";
connectAttr "r_foot_ik_ctrl_heelPivot.o" "Grunt_Skeleton_RigRN.phl[165]";
connectAttr "r_foot_ik_ctrl_ballPivot.o" "Grunt_Skeleton_RigRN.phl[166]";
connectAttr "r_foot_ik_ctrl_toePivot.o" "Grunt_Skeleton_RigRN.phl[167]";
connectAttr "r_foot_ik_ctrl_heelSide.o" "Grunt_Skeleton_RigRN.phl[168]";
connectAttr "r_foot_ik_ctrl_ballSide.o" "Grunt_Skeleton_RigRN.phl[169]";
connectAttr "r_foot_ik_ctrl_toeSide.o" "Grunt_Skeleton_RigRN.phl[170]";
connectAttr "r_foot_ik_ctrl_space.o" "Grunt_Skeleton_RigRN.phl[171]";
connectAttr "r_foot_ik_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[172]";
connectAttr "r_foot_ik_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[173]";
connectAttr "r_foot_ik_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[174]";
connectAttr "r_foot_ik_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[175]";
connectAttr "r_foot_ik_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[176]";
connectAttr "r_foot_ik_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[177]";
connectAttr "r_leg_pv_01_space.o" "Grunt_Skeleton_RigRN.phl[178]";
connectAttr "r_leg_pv_01_translateX.o" "Grunt_Skeleton_RigRN.phl[179]";
connectAttr "r_leg_pv_01_translateY.o" "Grunt_Skeleton_RigRN.phl[180]";
connectAttr "r_leg_pv_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[181]";
connectAttr "r_leg_pv_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[182]";
connectAttr "r_leg_pv_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[183]";
connectAttr "r_leg_pv_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[184]";
connectAttr "l_leg_pv_01_space.o" "Grunt_Skeleton_RigRN.phl[185]";
connectAttr "l_leg_pv_01_translateX.o" "Grunt_Skeleton_RigRN.phl[186]";
connectAttr "l_leg_pv_01_translateY.o" "Grunt_Skeleton_RigRN.phl[187]";
connectAttr "l_leg_pv_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[188]";
connectAttr "l_leg_pv_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[189]";
connectAttr "l_leg_pv_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[190]";
connectAttr "l_leg_pv_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[191]";
connectAttr "ref_frame_pointConstraint1.ctx" "Grunt_Skeleton_RigRN.phl[1]";
connectAttr "ref_frame_pointConstraint1.ctz" "Grunt_Skeleton_RigRN.phl[2]";
connectAttr "Grunt_Skeleton_RigRN.phl[3]" "ref_frame_pointConstraint1.cpim";
connectAttr "Grunt_Skeleton_RigRN.phl[4]" "ref_frame_pointConstraint1.crp";
connectAttr "Grunt_Skeleton_RigRN.phl[5]" "ref_frame_pointConstraint1.crt";
connectAttr "ref_frame_pointConstraint1.w0" "ref_frame_pointConstraint1.tg[0].tw"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "grunt_rig.msg" "Grunt_Skeleton_RigRN.asn[0]";
connectAttr "Grunt_Skeleton_RigRNfosterParent1.msg" "Grunt_Skeleton_RigRN.fp";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Enemy@Grunt_walk_01.ma
