//Maya ASCII 2016 scene
//Name: Enemy@Grunt_Idle_Shakes.ma
//Last modified: Sun, Nov 22, 2015 03:16:10 PM
//Codeset: 1252
requires maya "2016";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "5C1C1D38-4056-C634-A5DE-9189528C11BF";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 136.45728135309668 171.61569294837327 244.23860677253899 ;
	setAttr ".r" -type "double3" -23.138352729602431 26.600000000000094 -8.8926343756588798e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "E15516AC-4EAF-7C10-FCD3-14A7F7699DEE";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 334.95480672812619;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "65FC8987-43F3-28E8-3AD0-6E81065D5997";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "A00EEA7D-4163-10A3-E376-E183868B549C";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "27F08110-4F16-538D-BE96-478AA62B4F75";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "B74D9C90-4935-624A-4ACA-30B877FEB3E7";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "21E7F67F-4817-DCB4-9E3C-E49B624AC775";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "F443567B-4BAC-2E04-01BD-2381DA0E7257";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "ref_frame";
	rename -uid "65EE1444-42D1-0777-E3A1-6FA9B7CD209B";
	addAttr -ci true -sn "refFrame" -ln "refFrame" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "Character1_Hips" -p "ref_frame";
	rename -uid "3C9F620A-47E2-7B22-A284-52876AC947A4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 1.406920211983633 -3.3458391704689405 -22.822435086355146 ;
	setAttr ".ssc" no;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftUpLeg" -p "Character1_Hips";
	rename -uid "BEA8A7BC-4CA6-F170-3474-32884FE16144";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 96.630252250634001 6.5132649289235918 97.599644355322027 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftLeg" -p "Character1_LeftUpLeg";
	rename -uid "8F806F4F-45B0-0CCD-E5A1-ED82B69D9F3B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -129.48089468354581 -7.9067044581664252 177.65996829599328 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFoot" -p "Character1_LeftLeg";
	rename -uid "B02D234B-4D7E-678F-20FF-11ABE603F527";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -82.075120143962664 29.940914059061775 128.00062508119765 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftToeBase" -p "Character1_LeftFoot";
	rename -uid "32FF0906-4EF6-4141-F001-429E9234B07D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -126.16837928120003 -7.059491002168353 35.858557690282048 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle1" -p "Character1_LeftToeBase";
	rename -uid "64102B47-4CCF-A4DD-AA2D-B48B19F45AA6";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -100.08944018296057 24.928076457701248 56.143888543356006 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle2" -p "Character1_LeftFootMiddle1";
	rename -uid "EEBFBD14-47A7-B60B-34E6-1D85666DAD8E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -39.508027973091373 -2.0582897454817326 31.259679900637106 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightUpLeg" -p "Character1_Hips";
	rename -uid "ED6A270C-4287-0710-35BF-D9BA1B89338C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 11.416489881294433 -11.43673382189878 -152.25229525481123 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightLeg" -p "Character1_RightUpLeg";
	rename -uid "BF6E3180-4F18-B203-A0F5-E58EF2FE9EA8";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -22.942928776539453 -35.956391389429932 101.2794675694641 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFoot" -p "Character1_RightLeg";
	rename -uid "AA3AF155-44E1-632C-6DF8-74B9FE43E4CD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 66.643786034585347 17.03863148951929 124.91120862192221 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightToeBase" -p "Character1_RightFoot";
	rename -uid "6F2ED580-42B1-C593-4CD2-D5A1DD07B6C3";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -155.84366405206632 -5.8738653577349114 -93.73595023516998 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle1" -p "Character1_RightToeBase";
	rename -uid "811C1717-4935-B4C8-D515-4A8EA8B89A20";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -166.06091029301223 54.415170601070251 -127.18425880983375 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle2" -p "Character1_RightFootMiddle1";
	rename -uid "5A9A5605-4A56-416E-D261-50BA77F9622D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -129.2483692316568 10.731788640430997 6.9892235317174372 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine" -p "Character1_Hips";
	rename -uid "84842E0D-4327-23E9-AEFD-BCBA224F7735";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 11.416489881294433 -11.43673382189878 -152.25229525481123 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine1" -p "Character1_Spine";
	rename -uid "A3F6F798-4752-A4C0-ADA4-ADA8927E1FBA";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 24.933471933369759 -37.782249520228419 -20.761597110625811 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftShoulder" -p "Character1_Spine1";
	rename -uid "71D961F8-41DE-D352-0B67-E8A4CC8E911E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 144.67442469508543 70.793071067570196 19.807411116001759 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftArm" -p "Character1_LeftShoulder";
	rename -uid "B7690ADC-4F39-82B3-A31B-24A1994D17A0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 99.384574925726014 -60.006523708111558 170.15034313799799 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftForeArm" -p "Character1_LeftArm";
	rename -uid "4F01148B-406C-D3B9-15A6-17A216EF45B4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 70.061799635480924 -4.8580147780287897 21.798459685601706 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHand" -p "Character1_LeftForeArm";
	rename -uid "C9F6E500-4394-009F-5477-389A63F8EA92";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.9708494230587 -13.075378107872792 150.36023365917421 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb1" -p "Character1_LeftHand";
	rename -uid "12B25AAF-446D-CDDE-C0A9-4D8A4B2EDDD4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -65.748924167512314 39.787402356574312 166.7746235430387 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb2" -p "Character1_LeftHandThumb1";
	rename -uid "7955A27D-45EC-9E8E-A377-8DBADB5EFECD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -39.861597330550808 6.7221908644292059 68.670730027774553 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb3" -p "Character1_LeftHandThumb2";
	rename -uid "F993DF90-43E8-0653-ED4D-75B9FF420B4B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -120.21671274765816 -26.239331955300646 57.413978285788289 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex1" -p "Character1_LeftHand";
	rename -uid "DA925824-4F5E-A231-134C-D3851166DC43";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -152.38220651039205 4.806304023778381 48.842756939944849 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex2" -p "Character1_LeftHandIndex1";
	rename -uid "305D1548-4C9E-2207-DFF8-D1891AA27DCD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 176.30914278667802 -13.434888179376443 97.633542306389202 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex3" -p "Character1_LeftHandIndex2";
	rename -uid "53D907B4-47D8-D69B-327B-80AF8AD1E660";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -102.43860404365057 -60.659303181068822 -152.9472923666747 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing1" -p "Character1_LeftHand";
	rename -uid "73B36875-4331-8730-5AAC-379FE02D8097";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -31.516946849722316 2.5758733401430476 -137.43155166436202 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing2" -p "Character1_LeftHandRing1";
	rename -uid "A31DB1BB-41B5-4C54-C3AF-D78BCBA6791D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -37.632109434888541 57.372964012610872 -137.42548313576771 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing3" -p "Character1_LeftHandRing2";
	rename -uid "7595C309-4579-C967-272D-50A2D5B2C282";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -16.443988862208798 8.3290143241887424 -59.438981507600595 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightShoulder" -p "Character1_Spine1";
	rename -uid "DB9F8DA7-4E2B-BA47-5699-B18542B93086";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -174.37324535156188 -81.21351031321781 -130.38505903740904 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightArm" -p "Character1_RightShoulder";
	rename -uid "79B85A92-4229-9113-2985-2B93289AA0BF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -104.33239449525594 -37.101615257968433 20.137114731244228 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightForeArm" -p "Character1_RightArm";
	rename -uid "416CC4CB-4D7A-20F7-96D3-5C9899B3083C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -43.743657547705759 -9.6726323149773243 103.83730983340543 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHand" -p "Character1_RightForeArm";
	rename -uid "AAC60409-4EA0-3C54-5C0F-9BA517207B27";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 140.03960701773187 13.516449256889247 -135.99675148159383 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb1" -p "Character1_RightHand";
	rename -uid "297077BC-47C0-7A4A-8FFB-4A827A9D3355";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.10974980768816 71.94515848234029 -178.78334614629264 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb2" -p "Character1_RightHandThumb1";
	rename -uid "092752B9-4548-E223-EF38-D68199178C7C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.2056241799594538 33.542113270229827 -130.48989956049317 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb3" -p "Character1_RightHandThumb2";
	rename -uid "F7051FCF-4286-E021-AE6E-3C8A12FDCE39";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 134.7539820196863 -70.074251222974951 75.428204415317296 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex1" -p "Character1_RightHand";
	rename -uid "907AFF62-441A-1990-84BF-C798551AEA05";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.10974980768816 71.94515848234029 -178.78334614629264 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex2" -p "Character1_RightHandIndex1";
	rename -uid "46889328-4140-09D8-48AC-36AE7FD688F1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.2057394778445882 33.54331930430493 -130.49010822234442 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex3" -p "Character1_RightHandIndex2";
	rename -uid "5AB21EB8-46EA-5751-4A3E-7DA262E3D69A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 134.79643182941854 -70.056599207814415 75.383915829770686 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing1" -p "Character1_RightHand";
	rename -uid "14C03B53-4E51-ABDB-C406-36BAF915F196";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.10974980768816 71.94515848234029 -178.78334614629264 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing2" -p "Character1_RightHandRing1";
	rename -uid "B3B6E3A7-4593-E410-8AF1-59BA61A71D04";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.2056241799594538 33.542113270229827 -130.48989956049317 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing3" -p "Character1_RightHandRing2";
	rename -uid "0145C9D3-4781-2DD0-5616-FA9BF86A534B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 134.7539820196863 -70.074251222974951 75.428204415317296 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Neck" -p "Character1_Spine1";
	rename -uid "E0706824-4C12-ABAE-29DE-0594342CD01A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 161.13402532275541 31.934232202611735 171.90006021381112 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Head" -p "Character1_Neck";
	rename -uid "E81378DF-4EFD-8341-9A14-3E871FAFDCC4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -18.098521216998183 -9.9246993917445305 116.26360516393567 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "jaw" -p "Character1_Head";
	rename -uid "F76121FE-496A-26B8-1CB0-8C8CC0B07B11";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.667422224424381 90 0 ;
	setAttr ".radi" 6;
	setAttr -k on ".liw" no;
	setAttr ".fbxID" 5;
createNode joint -n "eyes" -p "Character1_Head";
	rename -uid "771F1662-4D63-F3BA-2284-B2A990D47CB6";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.667422224424381 90 0 ;
	setAttr ".radi" 4.5;
	setAttr ".fbxID" 5;
createNode animCurveTU -n "jaw_lockInfluenceWeights";
	rename -uid "58F1F437-4B4F-7793-B83D-80B3FE02F151";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "Character1_Hips_translateX";
	rename -uid "D613B56F-40C3-976F-62EE-F6A1A589BDC2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -0.8014373779296875 1 -0.8804360032081604
		 2 -0.96323662996292114 3 -1.0488886833190918 4 -1.136441707611084 5 -1.2249453067779541
		 6 -1.3134489059448242 7 -1.4010019302368164 8 -1.4866539239883423 9 -1.569454550743103
		 10 -1.6484531164169312 11 -1.7226994037628174 12 -1.7912425994873047 13 -1.8531323671340942
		 14 -1.9074183702468872 15 -1.9531497955322266 16 -1.9497194290161133 17 -1.9485107660293579
		 18 -1.9499557018280029 19 -1.9516911506652832 20 -1.9513537883758545 21 -1.9493755102157593
		 22 -1.9477858543395996 23 -1.9466177225112915 24 -1.9459036588668823 25 -1.9456764459609985
		 26 -1.9459687471389771 27 -1.9468134641647339 28 -1.9482431411743164 29 -1.9489617347717285
		 30 -1.9484906196594238 31 -1.947356104850769 32 -1.946246862411499 33 -1.9466668367385864
		 34 -1.9483814239501953 35 -1.9503388404846191 36 -1.9528157711029053 37 -1.9560893774032593
		 38 -1.960436224937439 39 -1.9661334753036499 40 -1.9734578132629395 41 -1.9826862812042236
		 42 -1.9940956830978394 43 -2.0079631805419922 44 -2.0245652198791504 45 -2.0441792011260986
		 46 -2.0674088001251221 47 -2.0943686962127686 48 -2.1246030330657959 49 -2.1576545238494873
		 50 -2.195063591003418 51 -2.2367727756500244 52 -2.2799291610717773 53 -2.3216803073883057
		 54 -2.3619692325592041 55 -2.4023356437683105 56 -2.4423232078552246 57 -2.4814746379852295
		 58 -2.519334077835083 59 -2.5554444789886475 60 -2.5893495082855225 61 -2.6237499713897705
		 62 -2.6577847003936768 63 -2.6910951137542725 64 -2.7285289764404297 65 -2.7697288990020752
		 66 -2.8105494976043701 67 -2.8501594066619873 68 -2.8877274990081787 69 -2.9224216938018799
		 70 -2.9534108638763428 71 -2.9798634052276611 72 -3.0009477138519287 73 -3.0158321857452393
		 74 -3.0236854553222656 75 -3.0236761569976807 76 -3.0269534587860107 77 -3.0141232013702393
		 78 -2.9875345230102539 79 -2.9495351314544678 80 -2.9024734497070313 81 -2.8506944179534912
		 82 -2.7969450950622559 83 -2.7411782741546631 84 -2.6833457946777344 85 -2.6261951923370361
		 86 -2.5740714073181152 87 -2.5293228626251221 88 -2.4942975044250488 89 -2.4713437557220459
		 90 -2.4628093242645264 91 -2.3719639778137207 92 -2.2593142986297607 93 -2.1269705295562744
		 94 -1.9786396026611328 95 -1.8208246231079102 96 -1.6600279808044434 97 -1.4999574422836304
		 98 -1.3427227735519409 99 -1.1924306154251099 100 -1.0531878471374512 101 -0.93109780550003052
		 102 -0.83066648244857788 103 -0.75360459089279175 104 -0.7016226053237915 105 -0.67922681570053101
		 106 -0.71023571491241455 107 -0.73518079519271851 108 -0.75473576784133911 109 -0.76957428455352783
		 110 -0.78037029504776001 111 -0.78779751062393188 112 -0.7925296425819397 113 -0.79324382543563843
		 114 -0.79021430015563965 115 -0.7865111231803894 116 -0.78520387411117554 117 -0.78656721115112305
		 118 -0.78927809000015259 119 -0.7940102219581604 120 -0.8014373779296875;
createNode animCurveTL -n "Character1_Hips_translateY";
	rename -uid "8ECF8798-4275-B9D6-886A-0D9CC8FA5816";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 41.757061004638672 1 41.655796051025391
		 2 41.549396514892578 3 41.439117431640625 4 41.326213836669922 5 41.211925506591797
		 6 41.097587585449219 7 40.984508514404297 8 40.874019622802734 9 40.767410278320313
		 10 40.665950775146484 11 40.570930480957031 12 40.483673095703125 13 40.40545654296875
		 14 40.337562561035156 15 40.281265258789063 16 40.315769195556641 17 40.391220092773437
		 18 40.501316070556641 19 40.63970947265625 20 40.800048828125 21 40.975982666015625
		 22 41.161258697509766 23 41.349655151367188 24 41.534954071044922 25 41.710845947265625
		 26 41.870964050292969 27 42.009002685546875 28 42.118663787841797 29 42.193660736083984
		 30 42.227691650390625 31 42.214366912841797 32 42.144306182861328 33 42.026058197021484
		 34 41.868190765380859 35 41.679248809814453 36 41.467723846435547 37 41.242122650146484
		 38 41.010955810546875 39 40.782806396484375 40 40.566249847412109 41 40.369876861572266
		 42 40.202213287353516 43 40.071800231933594 44 39.987209320068359 45 39.95703125
		 46 39.939159393310547 47 39.910015106201172 48 39.871170043945312 49 39.824176788330078
		 50 39.770603179931641 51 39.712154388427734 52 39.65057373046875 53 39.587596893310547
		 54 39.524929046630859 55 39.464138031005859 56 39.40679931640625 57 39.354480743408203
		 58 39.308795928955078 59 39.271430969238281 60 39.244037628173828 61 39.185794830322266
		 62 39.111618041992187 63 39.025455474853516 64 38.931293487548828 65 38.833049774169922
		 66 38.734607696533203 67 38.639884948730469 68 38.552810668945312 69 38.477279663085937
		 70 38.417182922363281 71 38.376426696777344 72 38.358921051025391 73 38.368606567382813
		 74 38.409397125244141 75 38.485157012939453 76 38.532115936279297 77 38.658908843994141
		 78 38.853481292724609 79 39.103763580322266 80 39.397670745849609 81 39.723117828369141
		 82 40.068031311035156 83 40.420330047607422 84 40.767929077148438 85 41.098751068115234
		 86 41.400722503662109 87 41.661750793457031 88 41.869762420654297 89 42.012676239013672
		 90 42.078414916992188 91 42.029720306396484 92 41.984176635742188 93 41.941932678222656
		 94 41.903125762939453 95 41.867893218994141 96 41.836387634277344 97 41.808734893798828
		 98 41.785087585449219 99 41.765579223632813 100 41.750354766845703 101 41.739555358886719
		 102 41.733318328857422 103 41.731788635253906 104 41.735099792480469 105 41.743408203125
		 106 41.757518768310547 107 41.767990112304688 108 41.775234222412109 109 41.779647827148438
		 110 41.781635284423828 111 41.781600952148438 112 41.779953002929687 113 41.777088165283203
		 114 41.773422241210937 115 41.769344329833984 116 41.765274047851562 117 41.761604309082031
		 118 41.758743286132812 119 41.757091522216797 120 41.757061004638672;
createNode animCurveTL -n "Character1_Hips_translateZ";
	rename -uid "2D6525A4-4CF3-F6BE-5CE4-369CE3B4B3F9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -9.2881870269775391 1 -9.3044099807739258
		 2 -9.321507453918457 3 -9.3392610549926758 4 -9.357452392578125 5 -9.3758630752563477
		 6 -9.3942728042602539 7 -9.4124650955200195 8 -9.4302186965942383 9 -9.4473161697387695
		 10 -9.4635391235351562 11 -9.4786672592163086 12 -9.4924840927124023 13 -9.5047683715820312
		 14 -9.5153036117553711 15 -9.523869514465332 16 -9.5236396789550781 17 -9.527348518371582
		 18 -9.5340566635131836 19 -9.5442209243774414 20 -9.5582981109619141 21 -9.575347900390625
		 22 -9.5936355590820312 23 -9.612421989440918 24 -9.6309661865234375 25 -9.6485300064086914
		 26 -9.6643733978271484 27 -9.6777582168579102 28 -9.6879444122314453 29 -9.694854736328125
		 30 -9.6980056762695313 31 -9.7000160217285156 32 -9.69647216796875 33 -9.6875066757202148
		 34 -9.6741266250610352 35 -9.657740592956543 36 -9.6391010284423828 37 -9.6189537048339844
		 38 -9.5980491638183594 39 -9.5771360397338867 40 -9.5569620132446289 41 -9.5382785797119141
		 42 -9.5218315124511719 43 -9.5083723068237305 44 -9.4986467361450195 45 -9.4934072494506836
		 46 -9.4958467483520508 47 -9.4994134902954102 48 -9.5039300918579102 49 -9.509221076965332
		 50 -9.5141143798828125 51 -9.5182313919067383 52 -9.5225915908813477 53 -9.5282154083251953
		 54 -9.5347251892089844 55 -9.5409479141235352 56 -9.5467081069946289 57 -9.5518293380737305
		 58 -9.5561332702636719 59 -9.5594453811645508 60 -9.5615873336791992 61 -9.5600051879882812
		 62 -9.5601482391357422 63 -9.5617494583129883 64 -9.5619478225708008 65 -9.5604772567749023
		 66 -9.5589599609375 67 -9.5573673248291016 68 -9.5556697845458984 69 -9.553837776184082
		 70 -9.5518407821655273 71 -9.5496492385864258 72 -9.5472345352172852 73 -9.5445661544799805
		 74 -9.5416164398193359 75 -9.5383529663085938 76 -9.5322189331054687 77 -9.5366020202636719
		 78 -9.5499286651611328 79 -9.5706272125244141 80 -9.5971250534057617 81 -9.626856803894043
		 82 -9.6580467224121094 83 -9.6903219223022461 84 -9.7233047485351562 85 -9.7552223205566406
		 86 -9.7835073471069336 87 -9.8065891265869141 88 -9.8228931427001953 89 -9.8308486938476562
		 90 -9.828883171081543 91 -9.8234872817993164 92 -9.8059625625610352 93 -9.7791337966918945
		 94 -9.7450380325317383 95 -9.7043132781982422 96 -9.6575946807861328 97 -9.6069183349609375
		 98 -9.5551118850708008 99 -9.5040102005004883 100 -9.4554462432861328 101 -9.4102573394775391
		 102 -9.3700790405273437 103 -9.3379383087158203 104 -9.3168659210205078 105 -9.3084945678710937
		 106 -9.3097133636474609 107 -9.3101062774658203 108 -9.3097648620605469 109 -9.308781623840332
		 110 -9.3072471618652344 111 -9.3052549362182617 112 -9.3028945922851562 113 -9.3012552261352539
		 114 -9.3006277084350586 115 -9.2999086380004883 116 -9.2979936599731445 117 -9.2951745986938477
		 118 -9.2925395965576172 119 -9.2901802062988281 120 -9.2881870269775391;
createNode animCurveTU -n "Character1_Hips_scaleX";
	rename -uid "5040A357-49F0-358B-31E1-FD95D8F5FE5E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_Hips_scaleY";
	rename -uid "B95706C7-4A6D-C796-EA05-A0AB945E5D47";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_Hips_scaleZ";
	rename -uid "DA4A096F-4CEE-2236-65A7-35905F895A77";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Hips_rotateX";
	rename -uid "BF984EA5-42B7-07C3-F913-BDBE9A0256A9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -7.9859161376953116 1 -8.3239002227783203
		 2 -8.6901588439941406 3 -9.0777273178100586 4 -9.4796142578125 5 -9.8888072967529297
		 6 -10.298279762268066 7 -10.700995445251465 8 -11.089921951293945 9 -11.458027839660645
		 10 -11.798287391662598 11 -12.103679656982422 12 -12.36717700958252 13 -12.58173942565918
		 14 -12.740292549133301 15 -12.835713386535645 16 -12.794955253601074 17 -12.752266883850098
		 18 -12.706323623657227 19 -12.65562629699707 20 -12.598532676696777 21 -12.533355712890625
		 22 -12.458239555358887 23 -12.371770858764648 24 -12.272737503051758 25 -12.160138130187988
		 26 -12.033175468444824 27 -11.891221046447754 28 -11.733769416809082 29 -11.560625076293945
		 30 -11.371411323547363 31 -11.334094047546387 32 -11.258282661437988 33 -11.149176597595215
		 34 -11.011819839477539 35 -10.852023124694824 36 -10.675148010253906 37 -10.486563682556152
		 38 -10.291647911071777 39 -10.095786094665527 40 -9.9043645858764648 41 -9.7227716445922852
		 42 -9.5563936233520508 43 -9.410618782043457 44 -9.2908334732055664 45 -9.2024288177490234
		 46 -9.0923128128051758 47 -8.9479427337646484 48 -8.7766733169555664 49 -8.5857791900634766
		 50 -8.3805389404296875 51 -8.1684589385986328 52 -7.9569816589355469 53 -7.753568172454834
		 54 -7.5658888816833487 55 -7.3977293968200684 56 -7.2547593116760263 57 -7.142484188079834
		 58 -7.0663251876831055 59 -7.031710147857666 60 -7.0441861152648926 61 -7.1498394012451172
		 62 -7.333367347717286 63 -7.5806903839111337 64 -7.8774013519287109 65 -8.2099809646606445
		 66 -8.5715179443359375 67 -8.9505634307861328 68 -9.3353557586669922 69 -9.7137947082519531
		 70 -10.073437690734863 71 -10.401537895202637 72 -10.685091972351074 73 -10.910928726196289
		 74 -11.065814971923828 75 -11.136578559875488 76 -11.219690322875977 77 -11.383708000183105
		 78 -11.617837905883789 79 -11.911567687988281 80 -12.254091262817383 81 -12.633626937866211
		 82 -13.038554191589355 83 -13.45531177520752 84 -13.868228912353516 85 -14.259511947631836
		 86 -14.612025260925295 87 -14.907162666320799 88 -15.125767707824709 89 -15.248519897460936
		 90 -15.256400108337402 91 -15.0073299407959 92 -14.683856010437012 93 -14.297086715698242
		 94 -13.861329078674316 95 -13.39008617401123 96 -12.89585018157959 97 -12.390011787414551
		 98 -11.883586883544922 99 -11.388148307800293 100 -10.915088653564453 101 -10.474151611328125
		 102 -10.076597213745117 103 -9.734074592590332 104 -9.4583902359008789 105 -9.2613468170166016
		 106 -9.2323694229125977 107 -9.1890420913696289 108 -9.1327199935913086 109 -9.0647678375244141
		 110 -8.9865760803222656 111 -8.8995676040649414 112 -8.8052043914794922 113 -8.7070446014404297
		 114 -8.605616569519043 115 -8.5012578964233398 116 -8.3942747116088867 117 -8.2867212295532227
		 118 -8.1816110610961914 119 -8.0807332992553711 120 -7.9859161376953116;
createNode animCurveTA -n "Character1_Hips_rotateY";
	rename -uid "B5E38710-4163-E2AB-6857-ADB273BAB173";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 10.749359130859375 1 10.793944358825684
		 2 10.835127830505371 3 10.872811317443848 4 10.90706729888916 5 10.938173294067383
		 6 10.966628074645996 7 10.993157386779785 8 11.018709182739258 9 11.044439315795898
		 10 11.071676254272461 11 11.101884841918945 12 11.136610984802246 13 11.17741584777832
		 14 11.225803375244141 15 11.283124923706055 16 11.254392623901367 17 11.336485862731934
		 18 11.483122825622559 19 11.65363883972168 20 11.807315826416016 21 11.897684097290039
		 22 11.991606712341309 23 12.082284927368164 24 12.162915229797363 25 12.226710319519043
		 26 12.266914367675781 27 12.276826858520508 28 12.24982738494873 29 12.123908042907715
		 30 11.921711921691895 31 11.843476295471191 32 11.774837493896484 33 11.733011245727539
		 34 11.746973037719727 35 11.760799407958984 36 11.774205207824707 37 11.786982536315918
		 38 11.799028396606445 39 11.810351371765137 40 11.821079254150391 41 11.831455230712891
		 42 11.841825485229492 43 11.85261344909668 44 11.86429500579834 45 11.877355575561523
		 46 11.69244384765625 47 11.460847854614258 48 11.190505981445313 49 10.889408111572266
		 50 10.641124725341797 51 10.413451194763184 52 10.179984092712402 53 9.9143285751342773
		 54 9.5842752456665039 55 9.2743053436279297 56 8.9933691024780273 57 8.7504787445068359
		 58 8.5546655654907227 59 8.4149208068847656 60 8.3401346206665039 61 8.3269205093383789
		 62 8.2740230560302734 63 8.2393894195556641 64 8.2944669723510742 65 8.4964780807495117
		 66 8.7235145568847656 67 8.9644565582275391 68 9.208521842956543 69 9.445307731628418
		 70 9.6648168563842773 71 9.8574447631835938 72 10.013933181762695 73 10.12529182434082
		 74 10.182674407958984 75 10.177221298217773 76 10.218954086303711 77 10.373419761657715
		 78 10.621240615844727 79 10.942868232727051 80 11.318872451782227 81 11.803864479064941
		 82 12.339313507080078 83 12.873568534851074 84 13.356415748596191 85 13.733589172363281
		 86 14.06413745880127 87 14.334869384765623 88 14.53333854675293 89 14.647458076477053
		 90 14.664962768554687 91 14.699299812316895 92 14.588130950927734 93 14.280202865600586
		 94 13.835784912109375 95 13.309986114501953 96 12.75843620300293 97 12.242988586425781
		 98 11.712222099304199 99 11.18803596496582 100 10.692350387573242 101 10.321723937988281
		 102 10.058330535888672 103 9.8896770477294922 104 9.8030338287353516 105 9.7798442840576172
		 106 9.8505086898803711 107 9.9275655746459961 108 10.010127067565918 109 10.097278594970703
		 110 10.188100814819336 111 10.281685829162598 112 10.377158164978027 113 10.389636039733887
		 114 10.361700057983398 115 10.344784736633301 116 10.390507698059082 117 10.483881950378418
		 118 10.575088500976563 119 10.663702011108398 120 10.749359130859375;
createNode animCurveTA -n "Character1_Hips_rotateZ";
	rename -uid "33027C74-4E08-869D-E5D3-52B3F94E8FE7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -7.546454906463623 1 -7.8249187469482422
		 2 -8.1431264877319336 3 -8.4912395477294922 4 -8.8594017028808594 5 -9.2377376556396484
		 6 -9.6163616180419922 7 -9.9853849411010742 8 -10.334919929504395 9 -10.655089378356934
		 10 -10.93602180480957 11 -11.167856216430664 12 -11.340733528137207 13 -11.444788932800293
		 14 -11.470135688781738 15 -11.406850814819336 16 -11.345330238342285 17 -11.187216758728027
		 18 -10.932361602783203 19 -10.58283805847168 20 -10.140798568725586 21 -9.6063976287841797
		 22 -9.0226812362670898 23 -8.4048185348510742 24 -7.7680454254150391 25 -7.1276674270629883
		 26 -6.4990601539611816 27 -5.8976664543151855 28 -5.3389887809753418 29 -4.8187241554260254
		 30 -4.3634247779846191 31 -4.2767229080200195 32 -4.128596305847168 33 -3.934720516204834
		 34 -3.7146012783050533 35 -3.4581320285797119 36 -3.17474365234375 37 -2.8738753795623779
		 38 -2.5649728775024414 39 -2.25748610496521 40 -1.9608676433563235 41 -1.6845690011978149
		 42 -1.4380397796630859 43 -1.2307275533676147 44 -1.0720779895782471 45 -0.9715389609336853
		 46 -0.81420904397964478 47 -0.59126847982406616 48 -0.31479981541633606 49 0.0031798908021301031
		 50 0.32859805226325989 51 0.66238504648208618 52 1.003328800201416 53 1.3498002290725708
		 54 1.7014139890670776 55 2.0255041122436523 56 2.3111143112182617 57 2.5473847389221191
		 58 2.7234981060028076 59 2.8286175727844238 60 2.8518095016479492 61 2.7379369735717773
		 62 2.5569174289703369 63 2.3043136596679687 64 1.9708585739135742 65 1.5494033098220825
		 66 1.0837471485137939 67 0.58735191822052002 68 0.073952279984951019 69 -0.44242596626281738
		 70 -0.94747370481491089 71 -1.4266335964202881 72 -1.8651590347290039 73 -2.2481961250305176
		 74 -2.5608870983123779 75 -2.7884924411773682 76 -3.171281099319458 77 -3.7684693336486821
		 78 -4.5498642921447754 79 -5.485435962677002 80 -6.544954776763916 81 -7.7256321907043466
		 82 -8.9825010299682617 83 -10.270883560180664 84 -11.544113159179688 85 -12.751288414001465
		 86 -13.887200355529785 87 -14.917127609252928 88 -15.806191444396974 89 -16.519617080688477
		 90 -17.023014068603516 91 -16.87257194519043 92 -16.723724365234375 93 -16.546058654785156
		 94 -16.357500076293945 95 -16.172407150268555 96 -16.003452301025391 97 -15.863469123840334
		 98 -15.721632957458496 99 -15.575576782226561 100 -15.422318458557131 101 -15.28325843811035
		 102 -15.139616012573244 103 -14.975614547729494 104 -14.776323318481445 105 -14.525635719299315
		 106 -14.408230781555176 107 -14.197342872619629 108 -13.903277397155762 109 -13.536343574523926
		 110 -13.10685920715332 111 -12.625156402587891 112 -12.101585388183594 113 -11.52131462097168
		 114 -10.908140182495117 115 -10.288152694702148 116 -9.6870565414428711 117 -9.1106739044189453
		 118 -8.5548334121704102 119 -8.0299549102783203 120 -7.546454906463623;
createNode animCurveTU -n "Character1_Hips_visibility";
	rename -uid "1C8B1748-458F-7832-6DFE-A4AFC66C7047";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftUpLeg_translateX";
	rename -uid "84EBF12D-4E0E-C4F5-643F-218D235BC140";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.596480369567871;
createNode animCurveTL -n "Character1_LeftUpLeg_translateY";
	rename -uid "5D13BE26-4E20-2AAE-2EB7-EB96A240CF81";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.315338134765625;
createNode animCurveTL -n "Character1_LeftUpLeg_translateZ";
	rename -uid "775EEB4F-4EBE-5963-1920-6FA1F237DDA8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.0558223724365234;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleX";
	rename -uid "FAA5EE37-4053-B2B8-E5CB-2A8A54003565";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleY";
	rename -uid "04AD2893-44CB-B17E-6EEA-85A16D20D9FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleZ";
	rename -uid "82F23CE2-46ED-13A9-12EA-5C9A089FFF6D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateX";
	rename -uid "A9F1EC70-42CB-2009-1919-70AC7D953F36";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -8.2673254013061523 1 -6.722144603729248
		 2 -5.1347432136535645 3 -3.8693139553070068 4 -3.2696654796600342 5 -3.6217355728149414
		 6 -4.6212606430053711 7 -5.7300882339477539 8 -6.4335975646972656 9 -6.3576502799987793
		 10 -5.857633113861084 11 -5.5065436363220215 12 -5.8495426177978516 13 -7.4310503005981445
		 14 -9.7889537811279297 15 -11.958737373352051 16 -12.874594688415527 17 -12.085065841674805
		 18 -10.16839599609375 19 -7.8883042335510254 20 -6.0295314788818359 21 -4.6248130798339844
		 22 -3.4131085872650146 23 -2.6429541110992432 24 -2.5795919895172119 25 -3.9116101264953613
		 26 -6.4170770645141602 27 -8.9371051788330078 28 -10.226521492004395 29 -9.9912204742431641
		 30 -9.0382833480834961 31 -7.9194226264953613 32 -6.5583462715148926 33 -5.2264409065246582
		 34 -3.5771641731262207 35 -1.442202091217041 36 0.56051594018936157 37 1.8231678009033201
		 38 1.974981427192688 39 1.4282592535018921 40 0.75116604566574097 41 0.50687861442565918
		 42 1.1213212013244629 43 2.2030248641967773 44 3.091381311416626 45 3.1298758983612061
		 46 1.762941837310791 47 -0.59562647342681885 48 -2.8298242092132568 49 -3.7975256443023686
		 50 -2.8823869228363037 51 -0.78901255130767822 52 1.5590964555740356 53 3.2378835678100586
		 54 4.2233715057373047 55 4.8799114227294922 56 4.9607806205749512 57 4.2176976203918457
		 58 2.0092027187347412 59 -1.3715920448303223 60 -4.7577910423278809 61 -7.0122103691101074
		 62 -7.0323233604431152 63 -5.5664362907409668 64 -4.0011491775512695 65 -2.6229145526885986
		 66 -1.0495724678039551 67 0.25291332602500916 68 0.84581577777862549 69 0.42924788594245911
		 70 -0.62679862976074219 71 -1.7550948858261108 72 -2.4084541797637939 73 -2.1772558689117432
		 74 -1.4100030660629272 75 -0.69429129362106323 76 -1.0298565626144409 77 -2.9443769454956055
		 78 -5.9232645034790039 79 -8.9043140411376953 80 -10.836502075195313 81 -11.204531669616699
		 82 -10.643148422241211 83 -9.9444665908813477 84 -9.8479518890380859 85 -10.292768478393555
		 86 -10.953959465026855 87 -11.984748840332031 88 -13.532885551452637 89 -16.098526000976562
		 90 -19.389484405517578 91 -21.970541000366211 92 -23.312248229980469 93 -21.257217407226562
		 94 -17.292461395263672 95 -13.200222015380859 96 -10.704677581787109 97 -10.784378051757812
		 98 -12.342745780944824 99 -14.102564811706543 100 -14.727896690368652 101 -13.66586971282959
		 102 -11.685201644897461 103 -9.5503950119018555 104 -8.0279941558837891 105 -7.1292400360107422
		 106 -6.7221379280090332 107 -6.6709380149841309 108 -7.1930642127990723 109 -9.0620641708374023
		 110 -12.005484580993652 111 -14.626292228698729 112 -15.439260482788088 113 -13.084745407104492
		 114 -8.6291694641113281 115 -4.0738253593444824 116 -1.4529913663864136 117 -1.6146588325500488
		 118 -3.4265341758728027 119 -5.9667367935180664 120 -8.2673254013061523;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateY";
	rename -uid "9476738E-43FA-702D-D24F-B8AEB8C8481E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 18.255149841308594 1 19.735921859741211
		 2 21.375194549560547 3 22.939170837402344 4 24.171546936035156 5 24.831571578979492
		 6 25.068338394165039 7 25.208715438842773 8 25.580364227294922 9 26.433910369873047
		 10 27.529886245727539 11 28.447887420654297 12 28.751338958740238 13 28.001848220825195
		 14 26.543544769287109 15 25.084539413452148 16 24.328964233398438 17 24.550567626953125
		 18 25.358709335327148 19 26.239091873168945 20 26.645223617553711 21 26.562685012817383
		 22 26.248924255371094 23 25.584199905395508 24 24.460334777832031 25 22.550273895263672
		 26 20.131797790527344 27 17.936090469360352 28 16.553064346313477 29 16.004457473754883
		 30 15.888785362243652 31 16.317714691162109 32 16.828763961791992 33 17.319595336914063
		 34 17.993492126464844 35 18.949228286743164 36 19.881023406982422 37 20.434148788452148
		 38 20.357080459594727 39 19.865804672241211 40 19.29383659362793 41 18.985569000244141
		 42 19.215131759643555 43 19.763429641723633 44 20.223079681396484 45 20.169448852539063
		 46 19.137662887573242 47 17.441797256469727 48 15.808816909790039 49 14.905054092407227
		 50 15.096906661987303 51 15.985557556152344 52 17.044021606445312 53 17.70216178894043
		 54 17.923891067504883 55 17.979692459106445 56 17.730968475341797 57 17.042810440063477
		 58 15.557379722595217 59 13.518435478210449 60 11.660102844238281 61 10.684807777404785
		 62 11.015755653381348 63 12.279632568359375 64 13.781276702880859 65 15.374716758728027
		 66 17.21881103515625 67 18.999544143676758 68 20.374189376831055 69 21.067024230957031
		 70 21.270349502563477 71 21.339736938476563 72 21.630863189697266 73 22.411516189575195
		 74 23.415189743041992 75 24.185012817382813 76 24.480833053588867 77 23.831707000732422
		 78 22.623397827148437 79 21.61744499206543 80 21.477401733398438 81 22.498710632324219
		 82 24.206857681274414 83 26.038040161132812 84 27.436923980712891 85 28.391319274902344
		 86 29.130882263183594 87 29.5261116027832 88 29.459627151489261 89 28.604793548583984
		 90 27.192001342773438 91 25.322229385375977 92 24.214698791503906 93 24.975143432617188
		 94 26.938465118408203 95 29.166994094848633 96 30.437961578369141 97 29.934415817260742
		 98 28.391645431518555 99 26.739681243896484 100 25.84827995300293 101 26.078392028808594
		 102 26.947319030761719 103 27.9609375 104 28.570013046264648 105 28.731050491333008
		 106 28.876350402832031 107 28.617378234863281 108 27.830310821533203 109 26.048505783081055
		 110 23.575044631958008 111 21.362957000732422 112 20.214349746704102 113 20.779121398925781
		 114 22.563552856445313 115 24.496404647827148 116 25.280521392822266 117 24.329324722290039
		 118 22.411325454711914 119 20.185123443603516 120 18.255149841308594;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateZ";
	rename -uid "A49E4375-48D2-E2E5-CB70-DDB1647C52C5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -12.22728443145752 1 -13.416339874267578
		 2 -14.67759895324707 3 -15.838918685913086 4 -16.740165710449219 5 -17.233814239501953
		 6 -17.42536735534668 7 -17.543413162231445 8 -17.842302322387695 9 -18.531478881835938
		 10 -19.422918319702148 11 -20.187978744506836 12 -20.495883941650391 13 -19.984781265258789
		 14 -18.847915649414063 15 -17.641216278076172 16 -16.858673095703125 17 -17.082565307617188
		 18 -17.844154357910156 19 -18.574686050415039 20 -18.774713516235352 21 -18.42095947265625
		 22 -17.860170364379883 23 -17.026147842407227 24 -15.858092308044434 25 -14.101611137390137
		 26 -11.880255699157715 27 -9.7332687377929687 28 -8.2743473052978516 29 -7.5735979080200204
		 30 -7.305302619934082 31 -7.6817212104797363 32 -8.2638149261474609 33 -8.9416074752807617
		 34 -9.8888711929321289 35 -11.05390453338623 36 -12.161608695983887 37 -12.980907440185547
		 38 -13.388434410095215 39 -13.528690338134766 40 -13.593219757080078 41 -13.781965255737305
		 42 -14.246271133422852 43 -14.820561408996584 44 -15.250415802001953 45 -15.304142951965334
		 46 -14.574428558349609 47 -13.385551452636719 48 -12.148685455322266 49 -11.362678527832031
		 50 -11.424284934997559 51 -11.963581085205078 52 -12.544465065002441 53 -12.803347587585449
		 54 -12.721691131591797 55 -12.559135437011719 56 -12.25710391998291 57 -11.749664306640625
		 58 -10.812196731567383 59 -9.4736261367797852 60 -8.1160612106323242 61 -7.315049648284913
		 62 -7.4249496459960937 63 -8.2272462844848633 64 -9.2197942733764648 65 -10.37213134765625
		 66 -11.666388511657715 67 -12.898505210876465 68 -13.884174346923828 69 -14.480653762817383
		 70 -14.779804229736328 71 -14.961437225341797 72 -15.231627464294434 73 -15.755471229553221
		 74 -16.345956802368164 75 -16.699184417724609 76 -16.912609100341797 77 -16.598552703857422
		 78 -15.873725891113279 79 -15.18082904815674 80 -15.101978302001955 81 -16.141689300537109
		 82 -17.905031204223633 83 -19.858419418334961 84 -21.456811904907227 85 -22.589691162109375
		 86 -23.551656723022461 87 -24.183622360229492 88 -24.311553955078125 89 -23.486156463623047
		 90 -21.802440643310547 91 -19.793294906616211 92 -18.504068374633789 93 -19.45295524597168
		 94 -21.524776458740234 95 -23.416082382202148 96 -24.052036285400391 97 -23.018566131591797
		 98 -20.968130111694336 99 -18.785564422607422 100 -17.409259796142578 101 -17.386445999145508
		 102 -18.100183486938477 103 -18.980552673339844 104 -19.519098281860352 105 -19.688589096069336
		 106 -19.871061325073242 107 -19.725669860839844 108 -19.151283264160156 109 -17.727670669555664
		 110 -15.606162071228027 111 -13.614625930786133 112 -12.679157257080078 113 -13.421555519104004
		 114 -15.121931076049803 115 -16.651716232299805 116 -17.207843780517578 117 -16.595430374145508
		 118 -15.294103622436522 119 -13.691909790039063 120 -12.22728443145752;
createNode animCurveTU -n "Character1_LeftUpLeg_visibility";
	rename -uid "AEBA24AA-49D7-A235-A990-6CBA2756C61E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftLeg_translateX";
	rename -uid "8B8230D1-455E-4525-9F64-0999AD6DC247";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -21.532840728759766;
createNode animCurveTL -n "Character1_LeftLeg_translateY";
	rename -uid "CAA3D9C2-440A-E4A7-52A5-B189008C6B14";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4219808578491211;
createNode animCurveTL -n "Character1_LeftLeg_translateZ";
	rename -uid "01F1ABFE-4591-5F14-0DAC-E3B09979176F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8661990165710449;
createNode animCurveTU -n "Character1_LeftLeg_scaleX";
	rename -uid "C441C652-41C8-6775-E5C1-10950F74C516";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftLeg_scaleY";
	rename -uid "2B6C1CCC-4A2D-4B1B-8243-8D910A800E2D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftLeg_scaleZ";
	rename -uid "A588E3AB-4647-99AE-2794-929BD01FDC00";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftLeg_rotateX";
	rename -uid "33B920A0-4730-6CBE-74C2-D88FF08F754F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -8.7684335708618164 1 -5.8086419105529785
		 2 -2.6271035671234131 3 0.30305418372154236 4 2.4967639446258545 5 3.5120797157287598
		 6 3.6804769039154048 7 3.6751952171325679 8 4.173365592956543 9 5.6884098052978516
		 10 7.731914520263671 11 9.4717559814453125 12 10.0643310546875 13 8.6486854553222656
		 14 5.8632493019104004 15 3.1047983169555664 16 1.4449450969696045 17 1.9949045181274414
		 18 3.8617165088653564 19 5.9126076698303223 20 7.0134148597717285 21 7.1202220916748047
		 22 6.8359894752502441 23 5.896820068359375 24 4.0409560203552246 25 0.49863773584365845
		 26 -4.2910146713256836 27 -8.81268310546875 28 -11.597585678100586 29 -12.46776294708252
		 30 -12.34351921081543 31 -11.188983917236328 32 -9.601222038269043 33 -7.8861327171325675
		 34 -5.6053371429443359 35 -2.6668944358825684 36 0.18951055407524109 37 2.2176153659820557
		 38 2.9393627643585205 39 2.8133513927459717 40 2.4943244457244873 41 2.6371142864227295
		 42 3.7321376800537109 43 5.2923588752746582 44 6.5033655166625977 45 6.5480818748474121
		 46 4.5239691734313965 47 1.2324041128158569 48 -1.9620569944381712 49 -3.7101581096649165
		 50 -3.1740319728851318 51 -1.2298456430435181 52 1.0116673707962036 53 2.4302701950073242
		 54 2.9553008079528809 55 3.1602241992950439 56 2.7631018161773682 57 1.4824677705764771
		 58 -1.4160189628601074 59 -5.5330166816711426 60 -9.4636955261230469 61 -11.686619758605957
		 62 -11.220722198486328 63 -8.8309850692749023 64 -6.0142836570739746 65 -3.0263779163360596
		 66 0.38339677453041077 67 3.6075410842895508 68 6.0221972465515137 69 7.1362810134887695
		 70 7.3137187957763672 71 7.2089838981628418 72 7.4815664291381836 73 8.6236400604248047
		 74 10.115860939025879 75 11.096460342407227 76 11.168415069580078 77 9.2556467056274414
		 78 6.0361361503601074 79 2.9312090873718262 80 1.3259706497192383 81 2.0474872589111328
		 82 4.1745166778564453 83 6.5674352645874023 84 8.1028966903686523 85 8.7307815551757813
		 86 9.0239324569702148 87 8.7098560333251953 88 7.5192837715148926 89 4.678473949432373
		 90 0.60479015111923218 91 -3.2724792957305908 92 -5.6151342391967773 93 -3.5713400840759277
		 94 0.96212637424468983 95 5.627873420715332 96 7.9533653259277344 97 6.524437427520752
		 98 2.8498570919036865 99 -1.1167776584625244 100 -3.4133014678955078 101 -3.0688409805297852
		 102 -1.2229294776916504 103 1.0085525512695312 104 2.4770724773406982 105 3.1130959987640381
		 106 3.5609340667724609 107 3.3154020309448242 108 2.084669828414917 109 -1.1727906465530396
		 110 -5.9197902679443359 111 -10.131608963012695 112 -11.872488021850586 113 -9.6437044143676758
		 114 -4.7919750213623047 115 0.17648583650588989 116 2.6801576614379883 117 1.6289434432983398
		 118 -1.4725884199142456 119 -5.3513154983520508 120 -8.7684335708618164;
createNode animCurveTA -n "Character1_LeftLeg_rotateY";
	rename -uid "AC56619E-4DFD-9EC6-B5D1-83BF6A278193";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -32.898441314697266 1 -34.005905151367188
		 2 -35.202239990234375 3 -36.349040985107422 4 -37.301456451416016 5 -37.927986145019531
		 6 -38.311428070068359 7 -38.630294799804688 8 -39.065410614013672 9 -39.754074096679688
		 10 -40.559169769287109 11 -41.248992919921875 12 -41.590530395507813 13 -41.346225738525391
		 14 -40.685626983642578 15 -39.989189147949219 16 -39.459465026855469 17 -39.445743560791016
		 18 -39.704345703125 19 -39.926555633544922 20 -39.802330017089844 21 -39.318954467773437
		 22 -38.663619995117188 23 -37.776424407958984 24 -36.602100372314453 25 -34.953807830810547
		 26 -32.988960266113281 27 -31.159328460693363 28 -29.874029159545898 29 -29.173954010009769
		 30 -28.857223510742191 31 -29.139230728149418 32 -29.663164138793945 33 -30.334726333618164
		 34 -31.255228042602539 35 -32.416530609130859 36 -33.601497650146484 37 -34.584392547607422
		 38 -35.212039947509766 39 -35.589656829833984 40 -35.879833221435547 41 -36.247283935546875
		 42 -36.814144134521484 43 -37.436737060546875 44 -37.880435943603516 45 -37.908786773681641
		 46 -37.305641174316406 47 -36.349437713623047 48 -35.418930053710938 49 -34.880847930908203
		 50 -34.972091674804688 51 -35.450656890869141 52 -36.015029907226563 53 -36.354812622070313
		 54 -36.440853118896484 55 -36.447273254394531 56 -36.299556732177734 57 -35.923912048339844
		 58 -35.125713348388672 59 -34.026153564453125 60 -33.015750885009766 61 -32.597915649414063
		 62 -32.932167053222656 63 -33.822502136230469 64 -34.886154174804688 65 -36.053375244140625
		 66 -37.352184295654297 67 -38.59991455078125 68 -39.60784912109375 69 -40.227676391601563
		 70 -40.549396514892578 71 -40.744174957275391 72 -40.985195159912109 73 -41.399379730224609
		 74 -41.835037231445313 75 -42.046295166015625 76 -42.105770111083984 77 -41.576366424560547
		 78 -40.645687103271484 79 -39.709560394287109 80 -39.155490875244141 81 -39.22607421875
		 82 -39.667415618896484 83 -40.169040679931641 84 -40.425750732421875 85 -40.426944732666016
		 86 -40.352622985839844 87 -40.140850067138672 88 -39.731086730957031 89 -38.924285888671875
		 90 -37.849281311035156 91 -36.889545440673828 92 -36.348728179931641 93 -36.971321105957031
		 94 -38.263858795166016 95 -39.590251922607422 96 -40.265861511230469 97 -39.908226013183594
		 98 -38.917980194091797 99 -37.83514404296875 100 -37.197704315185547 101 -37.274696350097656
		 102 -37.750282287597656 103 -38.318744659423828 104 -38.659568786621094 105 -38.743694305419922
		 106 -38.798046112060547 107 -38.639373779296875 108 -38.189689636230469 109 -37.168598175048828
		 110 -35.737922668457031 111 -34.4588623046875 112 -33.827224731445313 113 -34.197299957275391
		 114 -35.255455017089844 115 -36.364208221435547 116 -36.819671630859375 117 -36.314205169677734
		 118 -35.255439758300781 119 -34.002727508544922 120 -32.898441314697266;
createNode animCurveTA -n "Character1_LeftLeg_rotateZ";
	rename -uid "D80A29F8-4809-0C09-DF3F-28ACE12B0AAD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -42.592575073242187 1 -42.864803314208984
		 2 -43.211200714111328 3 -43.676502227783203 4 -44.276363372802734 5 -45.004444122314453
		 6 -45.800514221191406 7 -46.594825744628906 8 -47.320850372314453 9 -47.93389892578125
		 10 -48.474327087402344 11 -48.983486175537109 12 -49.470218658447266 13 -49.949104309082031
		 14 -50.417514801025391 15 -50.810146331787109 16 -50.736339569091797 17 -50.200126647949219
		 18 -49.314735412597656 19 -48.227005004882813 20 -47.036041259765625 21 -45.745784759521484
		 22 -44.391147613525391 23 -43.031288146972656 24 -41.736297607421875 25 -40.646202087402344
		 26 -39.846012115478516 27 -39.244480133056641 28 -38.62628173828125 29 -37.912395477294922
		 30 -37.286304473876953 31 -37.083995819091797 32 -37.044864654541016 33 -37.193321228027344
		 34 -37.458896636962891 35 -37.792427062988281 36 -38.259586334228516 37 -38.884868621826172
		 38 -39.646377563476563 39 -40.460536956787109 40 -41.242874145507813 41 -41.90740966796875
		 42 -42.384601593017578 43 -42.692169189453125 44 -42.858142852783203 45 -42.890060424804688
		 46 -42.915706634521484 47 -43.060176849365234 48 -43.244380950927734 49 -43.299114227294922
		 50 -43.119113922119141 51 -42.799774169921875 52 -42.478633880615234 53 -42.249351501464844
		 54 -42.081069946289063 55 -41.956977844238281 56 -41.903964996337891 57 -41.956291198730469
		 58 -42.215019226074219 59 -42.712604522705078 60 -43.318519592285156 61 -44.005435943603516
		 62 -44.397960662841797 63 -44.624412536621094 64 -44.948760986328125 65 -45.423233032226563
		 66 -45.949943542480469 67 -46.570365905761719 68 -47.28265380859375 69 -48.048080444335938
		 70 -48.787071228027344 71 -49.424770355224609 72 -49.890262603759766 73 -50.127498626708984
		 74 -50.142910003662109 75 -49.941440582275391 76 -50.056354522705078 77 -50.110111236572266
		 78 -50.147472381591797 79 -50.137470245361328 80 -49.952861785888672 81 -49.524814605712891
		 82 -48.950698852539063 83 -48.360996246337891 84 -47.833473205566406 85 -47.352752685546875
		 86 -46.939113616943359 87 -46.632392883300781 88 -46.481014251708984 89 -46.589939117431641
		 90 -47.035331726074219 91 -47.695938110351563 92 -48.224788665771484 93 -48.150440216064453
		 94 -47.809757232666016 95 -47.553455352783203 96 -47.513874053955078 97 -47.687686920166016
		 98 -48.006969451904297 99 -48.395973205566406 100 -48.621261596679688 101 -48.528289794921875
		 102 -48.236293792724609 103 -47.887565612792969 104 -47.582771301269531 105 -47.287864685058594
		 106 -47.070575714111328 107 -46.854343414306641 108 -46.673240661621094 109 -46.663169860839844
		 110 -46.870151519775391 111 -47.083042144775391 112 -46.933353424072266 113 -46.086067199707031
		 114 -44.88494873046875 115 -43.805328369140625 116 -43.082630157470703 117 -42.700130462646484
		 118 -42.539783477783203 119 -42.547599792480469 120 -42.592575073242187;
createNode animCurveTU -n "Character1_LeftLeg_visibility";
	rename -uid "123792C8-43F4-8508-82D6-A3B25485D0B9";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFoot_translateX";
	rename -uid "C130959B-44BF-C8F1-4DFD-4A9BA8DD5643";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 18.368806838989258;
createNode animCurveTL -n "Character1_LeftFoot_translateY";
	rename -uid "9A9E02FD-49AA-517D-8D2C-5E9A8B5BB85E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.7176852226257324;
createNode animCurveTL -n "Character1_LeftFoot_translateZ";
	rename -uid "CED2ED72-4732-09F7-1EB9-D4A5697775F6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3830562829971313;
createNode animCurveTU -n "Character1_LeftFoot_scaleX";
	rename -uid "9E02B679-49F8-5D92-7858-52ABEBD451F7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleY";
	rename -uid "ED5A6711-4505-E30A-9345-3DBE2B7D75A8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleZ";
	rename -uid "993AD8AB-4BEB-D5CE-845F-81B4324E8E4E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftFoot_rotateX";
	rename -uid "EAB19943-49B5-1415-836F-9989A4842DBD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -22.115396499633789 1 -20.344631195068359
		 2 -18.341360092163086 3 -16.496755599975586 4 -15.241917610168457 5 -14.981377601623535
		 6 -15.460785865783693 7 -16.105234146118164 8 -16.293842315673828 9 -15.522257804870605
		 10 -14.206693649291992 11 -13.104884147644043 12 -12.989158630371094 13 -14.678882598876953
		 14 -17.635040283203125 15 -20.498329162597656 16 -21.913675308227539 17 -20.955934524536133
		 18 -18.57105827331543 19 -15.938391685485838 20 -14.162856101989746 21 -13.256431579589844
		 22 -12.689194679260254 23 -12.670270919799805 24 -13.378573417663574 25 -15.360205650329588
		 26 -18.194196701049805 27 -20.699031829833984 28 -21.939809799194336 29 -21.993972778320312
		 30 -21.579803466796875 31 -20.835914611816406 32 -19.928926467895508 33 -19.023050308227539
		 34 -17.789131164550781 35 -16.106136322021484 36 -14.458090782165527 37 -13.377498626708984
		 38 -13.231145858764648 39 -13.716396331787109 40 -14.355128288269043 41 -14.620420455932615
		 42 -14.081723213195801 43 -13.091732978820801 44 -12.289078712463379 45 -12.312130928039551
		 46 -13.954201698303223 47 -16.648845672607422 48 -19.278900146484375 49 -20.726015090942383
		 50 -20.347419738769531 51 -18.84840202331543 52 -17.117286682128906 53 -16.05787467956543
		 54 -15.717494964599609 55 -15.638461112976076 56 -16.045690536499023 57 -17.167507171630859
		 58 -19.598194122314453 59 -22.98274040222168 60 -26.126707077026367 61 -28.089574813842773
		 62 -28.172149658203125 63 -26.87701416015625 64 -25.231424331665039 65 -23.39985466003418
		 66 -21.117170333862305 67 -18.896730422973633 68 -17.320564270019531 69 -16.861717224121094
		 70 -17.208744049072266 71 -17.75318717956543 72 -17.844207763671875 73 -16.974750518798828
		 74 -15.622679710388182 75 -14.600476264953612 76 -14.626911163330078 77 -16.361225128173828
		 78 -19.211769104003906 79 -21.81218147277832 80 -22.856166839599609 81 -21.676389694213867
		 82 -19.200735092163086 83 -16.540065765380859 84 -14.735194206237791 85 -13.797791481018066
		 86 -13.225769996643066 87 -13.248299598693848 88 -14.097335815429687 89 -16.461458206176758
		 90 -20.05088996887207 91 -23.351131439208984 92 -25.338146209716797 93 -23.652265548706055
		 94 -19.709308624267578 95 -15.52927780151367 96 -13.396424293518066 97 -14.573120117187502
		 98 -17.815776824951172 99 -21.365428924560547 100 -23.365060806274414 101 -22.963949203491211
		 102 -21.159261703491211 103 -18.942537307739258 104 -17.396736145019531 105 -16.597871780395508
		 106 -16.09565544128418 107 -16.176240921020508 108 -17.104141235351563 109 -19.80775260925293
		 110 -23.69163703918457 111 -26.8485107421875 112 -27.822088241577148 113 -25.614364624023437
		 114 -21.159954071044922 115 -16.545875549316406 116 -14.104741096496582 117 -14.669356346130373
		 118 -16.91215705871582 119 -19.741046905517578 120 -22.115396499633789;
createNode animCurveTA -n "Character1_LeftFoot_rotateY";
	rename -uid "63803F3D-4689-8460-7D6A-C78AA3AA1AE2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -39.130428314208984 1 -39.708999633789063
		 2 -40.190860748291016 3 -40.541152954101563 4 -40.818958282470703 5 -41.125743865966797
		 6 -41.474105834960937 7 -41.813014984130859 8 -42.108291625976563 9 -42.324810028076172
		 10 -42.434719085693359 11 -42.489292144775391 12 -42.632389068603516 13 -42.970840454101563
		 14 -43.246223449707031 15 -43.247234344482422 16 -43.045146942138672 17 -42.830623626708984
		 18 -42.471141815185547 19 -41.823844909667969 20 -41.001701354980469 21 -40.153430938720703
		 22 -39.280963897705078 23 -38.451656341552734 24 -37.707790374755859 25 -37.052730560302734
		 26 -36.259014129638672 27 -35.256034851074219 28 -34.368995666503906 29 -33.848003387451172
		 30 -33.624492645263672 31 -33.791934967041016 32 -34.111869812011719 33 -34.516887664794922
		 34 -34.997322082519531 35 -35.474250793457031 36 -35.884712219238281 37 -36.294593811035156
		 38 -36.810562133789063 39 -37.395782470703125 40 -37.954982757568359 41 -38.413070678710937
		 42 -38.709125518798828 43 -38.831890106201172 44 -38.841545104980469 45 -38.861858367919922
		 46 -39.102306365966797 47 -39.324417114257813 48 -39.318500518798828 49 -39.218845367431641
		 50 -39.245033264160156 51 -39.3084716796875 52 -39.282482147216797 53 -39.21246337890625
		 54 -39.181705474853516 55 -39.177574157714844 56 -39.218257904052734 57 -39.294094085693359
		 58 -39.314605712890625 59 -39.02764892578125 60 -38.414199829101563 61 -38.021106719970703
		 62 -38.297657012939453 63 -39.067020416259766 64 -39.862491607666016 65 -40.581916809082031
		 66 -41.208263397216797 67 -41.653682708740234 68 -41.969791412353516 69 -42.276313781738281
		 70 -42.590904235839844 71 -42.857494354248047 72 -43.032630920410156 73 -43.073635101318359
		 74 -42.955608367919922 75 -42.736301422119141 76 -42.780803680419922 77 -42.899406433105469
		 78 -42.856109619140625 79 -42.4996337890625 80 -42.03857421875 81 -41.693515777587891
		 82 -41.296859741210938 83 -40.701663970947266 84 -40.030658721923828 85 -39.443527221679688
		 86 -38.943893432617188 87 -38.610450744628906 88 -38.497188568115234 89 -38.61627197265625
		 90 -38.676837921142578 91 -38.547695159912109 92 -38.447040557861328 93 -39.14666748046875
		 94 -39.977615356445313 95 -40.355323791503906 96 -40.604515075683594 97 -41.228336334228516
		 98 -41.849174499511719 99 -42.078174591064453 100 -42.157241821289063 101 -42.49456787109375
		 102 -42.918071746826172 103 -43.185562133789063 104 -43.241195678710938 105 -43.162078857421875
		 106 -43.014263153076172 107 -42.858489990234375 108 -42.690170288085938 109 -42.387100219726563
		 110 -41.638862609863281 111 -40.580905914306641 112 -39.962024688720703 113 -40.369148254394531
		 114 -40.977920532226563 115 -40.969387054443359 116 -40.582462310791016 117 -40.352203369140625
		 118 -40.149906158447266 119 -39.734977722167969 120 -39.130428314208984;
createNode animCurveTA -n "Character1_LeftFoot_rotateZ";
	rename -uid "90644354-4129-C766-15D3-8E832B23B10F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 11.882567405700684 1 9.9532384872436523
		 2 7.8195552825927725 3 5.8859100341796875 4 4.5743322372436523 5 4.2698450088500977
		 6 4.7047305107116699 7 5.3017177581787109 8 5.4410953521728516 9 4.6205525398254395
		 10 3.2679851055145264 11 2.1450936794281006 12 2.0102252960205078 13 3.6719310283660889
		 14 6.618903636932373 15 9.5102157592773437 16 10.97828483581543 17 10.065581321716309
		 18 7.738955020904541 19 5.2040901184082031 20 3.561420202255249 21 2.8124597072601318
		 22 2.4184794425964355 23 2.5684628486633301 24 3.4274406433105469 25 5.5453524589538574
		 26 8.5621128082275391 27 11.313546180725098 28 12.782400131225586 29 12.975109100341797
		 30 12.623025894165039 31 11.841281890869141 32 10.853670120239258 33 9.8406972885131836
		 34 8.4650430679321289 35 6.6211214065551758 36 4.8224005699157715 37 3.6084742546081543
		 38 3.3405818939208984 39 3.7123041152954102 40 4.2460966110229492 41 4.4117450714111328
		 42 3.7712020874023433 43 2.6925463676452637 44 1.8360501527786255 45 1.8528490066528318
		 46 3.5320510864257813 47 6.3156852722167969 48 9.0648632049560547 49 10.585296630859375
		 50 10.167322158813477 51 8.5669889450073242 52 6.7393450736999512 53 5.6256718635559082
		 54 5.2707376480102539 55 5.1949429512023926 56 5.6118817329406738 57 6.7424769401550293
		 58 9.1996879577636719 59 12.677950859069824 60 15.977888107299806 61 18.036169052124023
		 62 18.064907073974609 63 16.617877960205078 64 14.815097808837891 65 12.833828926086426
		 66 10.422389984130859 67 8.1152839660644531 68 6.4864091873168945 69 5.9834527969360352
		 70 6.283595085144043 71 6.7851428985595703 72 6.8473186492919922 73 5.9695310592651367
		 74 4.6336116790771484 75 3.648090124130249 76 3.6648986339569087 77 5.3987326622009277
		 78 8.2931232452392578 79 10.995724678039551 80 12.152338981628418 81 11.036196708679199
		 82 8.5939359664916992 83 5.9709506034851074 84 4.2211508750915527 85 3.3524899482727051
		 86 2.8498740196228027 87 2.9277095794677734 88 3.8081986904144287 89 6.194727897644043
		 90 9.8459663391113281 91 13.231622695922852 92 15.259244918823244 93 13.371518135070801
		 94 9.1717920303344727 95 4.8495311737060547 96 2.669764518737793 97 3.8132257461547856
		 98 7.047882080078125 99 10.626108169555664 100 12.62647819519043 101 12.136823654174805
		 102 10.216005325317383 103 7.91886281967163 104 6.3473010063171387 105 5.5678858757019043
		 106 5.1063027381896973 107 5.2206192016601562 108 6.1624226570129395 109 8.8730335235595703
		 110 12.846392631530762 111 16.186794281005859 112 17.293790817260742 113 15.019124984741211
		 114 10.440020561218262 115 5.805274486541748 116 3.4174962043762207 117 4.0295982360839844
		 118 6.3460726737976074 119 9.3175382614135742 120 11.882567405700684;
createNode animCurveTU -n "Character1_LeftFoot_visibility";
	rename -uid "C04A6C3C-4385-2286-C2E7-C7BDAA14BCD0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftToeBase_translateX";
	rename -uid "565C3C31-4E39-95A0-4DA9-1FBE51029C4F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3813290596008301;
createNode animCurveTL -n "Character1_LeftToeBase_translateY";
	rename -uid "745E152F-450D-5C4C-798C-5FA6E693455C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.3898367881774902;
createNode animCurveTL -n "Character1_LeftToeBase_translateZ";
	rename -uid "06EA86A5-4C28-937E-5D5E-7989CF835069";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2284431457519531;
createNode animCurveTU -n "Character1_LeftToeBase_scaleX";
	rename -uid "54CCC395-4B38-6B10-68CC-A8B5E454DF6B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftToeBase_scaleY";
	rename -uid "FE056FF6-477C-55C2-7259-07BB740DC427";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftToeBase_scaleZ";
	rename -uid "CFA40EB2-4C7A-8F87-7DCC-3B83D3C63E6D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftToeBase_rotateX";
	rename -uid "738DEFD3-4677-8BD9-432C-BCB720A32521";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8221960586024579e-010;
createNode animCurveTA -n "Character1_LeftToeBase_rotateY";
	rename -uid "890E167E-4A26-75E8-7055-70A3785E96E7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1483408668411244e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateZ";
	rename -uid "45EB1C15-417D-05ED-F6D8-909CAA794E54";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.619182870589043e-009;
createNode animCurveTU -n "Character1_LeftToeBase_visibility";
	rename -uid "854CAC9C-4755-215C-8182-F9A6B98D0F48";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateX";
	rename -uid "E45D4CDF-4895-98D7-EA3D-0694A2497F27";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.1971817016601563;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateY";
	rename -uid "EBFBBE07-4CF4-3D02-F1BD-A8B4E1305BFD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.7569773197174072;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateZ";
	rename -uid "8E817B09-41D0-ED77-54F7-A4BFF2C8E8A7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4053930044174194;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleX";
	rename -uid "859D2CBA-4D78-5454-A379-2785608D1918";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleY";
	rename -uid "40841169-4B4C-9609-4172-98A708F15DF5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999958276748657;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleZ";
	rename -uid "59B6171B-46E8-B991-3BC8-DFBCDF768DE0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateX";
	rename -uid "34265E2F-48C8-7610-DE3E-AA91CAB06A58";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.9512051974146516e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateY";
	rename -uid "21A90607-475E-78EC-EF3C-63A9EB15811C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.1451249361587088e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateZ";
	rename -uid "9BECC4CC-48B3-670F-14B8-88BB3BFE0E81";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.5806703995989722e-010;
createNode animCurveTU -n "Character1_LeftFootMiddle1_visibility";
	rename -uid "E126613C-41AC-450A-1484-0386C6133376";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateX";
	rename -uid "FE89B6B0-4FDF-CFC5-093D-93A3FA90B32C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8763875961303711;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateY";
	rename -uid "D709BF9A-4325-7510-82AC-C58E8891D484";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.1604456901550293;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateZ";
	rename -uid "549C2520-4AA8-06E3-E4DB-10BAFC754F15";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.313550591468811;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleX";
	rename -uid "16C521B3-4609-9DB1-7AD6-B78B26DC1C15";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleY";
	rename -uid "BF535AFF-4854-F4BB-C8C6-1F858545794C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleZ";
	rename -uid "807B936F-4D79-1B8C-CFB8-E3A0DF08AFC0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateX";
	rename -uid "51FCBF1C-44D4-5DDF-A668-488517872701";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.1918438165033933e-010;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateY";
	rename -uid "23B8D427-4186-416F-D426-5A8DBE06552B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3504959817112194e-008;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateZ";
	rename -uid "34053912-480F-80D1-6EB6-C991C64305CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.796750800537211e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle2_visibility";
	rename -uid "787D1530-488E-C084-A111-D99A2FF559BA";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightUpLeg_translateX";
	rename -uid "5F820E46-43B1-C9F5-BD09-18B8D064F16E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.4649467468261719;
createNode animCurveTL -n "Character1_RightUpLeg_translateY";
	rename -uid "B555CA6A-48C0-2532-1CE4-209C428FBE2D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.110023021697998;
createNode animCurveTL -n "Character1_RightUpLeg_translateZ";
	rename -uid "2FA56C60-41C5-556D-3A02-BC941662968F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.4359440803527832;
createNode animCurveTU -n "Character1_RightUpLeg_scaleX";
	rename -uid "99F26475-4787-1845-CCA5-19863387B2E2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightUpLeg_scaleY";
	rename -uid "8A66656E-4E85-7D41-6222-D49AA180A547";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightUpLeg_scaleZ";
	rename -uid "701CB1EE-4687-2554-0BAA-C5980A4BCCF6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightUpLeg_rotateX";
	rename -uid "1BB48D4B-44D2-4220-2EBF-FA9FD9C8E48E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 26.973535537719727 1 26.972963333129883
		 2 26.953908920288086 3 26.88050651550293 4 26.725246429443359 5 26.462688446044922
		 6 26.158147811889648 7 25.898069381713867 8 25.737516403198242 9 25.705257415771484
		 10 25.740907669067383 11 25.771219253540039 12 25.757314682006836 13 25.708271026611328
		 14 25.674325942993164 15 25.692283630371094 16 25.601078033447266 17 25.491571426391602
		 18 25.341733932495117 19 25.122678756713867 20 24.816839218139648 21 24.43736457824707
		 22 24.036514282226563 23 23.635372161865234 24 23.255243301391602 25 22.910406112670898
		 26 22.611349105834961 27 22.383455276489258 28 22.273876190185547 29 22.30839729309082
		 30 22.493780136108398 31 22.611532211303711 32 22.950691223144531 33 23.469272613525391
		 34 24.134456634521484 35 24.901472091674805 36 25.733428955078125 37 26.600284576416016
		 38 27.470939636230469 39 28.309906005859375 40 29.094890594482418 41 29.820360183715817
		 42 30.47918701171875 43 31.031200408935547 44 31.434549331665039 45 31.661455154418945
		 46 31.896028518676761 47 32.181255340576172 48 32.509624481201172 49 32.906558990478516
		 50 33.396358489990234 51 33.956165313720703 52 34.551868438720703 53 35.135051727294922
		 54 35.676918029785156 55 36.1734619140625 56 36.596355438232422 57 36.917774200439453
		 58 37.089553833007812 59 37.121902465820313 60 37.069293975830078 61 37.009494781494141
		 62 36.986446380615234 63 36.960865020751953 64 36.850746154785156 65 36.644542694091797
		 66 36.379398345947266 67 36.056346893310547 68 35.6873779296875 69 35.291156768798828
		 70 34.901309967041016 71 34.535011291503906 72 34.193893432617188 73 33.869720458984375
		 74 33.548110961914063 75 33.241230010986328 76 33.011299133300781 77 32.533794403076172
		 78 31.813838958740238 79 30.862823486328129 80 29.729984283447262 81 28.499053955078125
		 82 27.210870742797852 83 25.88941764831543 84 24.551565170288086 85 23.218282699584961
		 86 21.962285995483398 87 20.826814651489258 88 19.860624313354492 89 19.099863052368164
		 90 18.643470764160156 91 18.890922546386719 92 19.194242477416992 93 19.687015533447266
		 94 20.308317184448242 95 20.965234756469727 96 21.506595611572266 97 21.777292251586914
		 98 21.834865570068359 99 21.853300094604492 100 22.000679016113281 101 22.398557662963867
		 102 22.958194732666016 103 23.572338104248047 104 24.088863372802734 105 24.446819305419922
		 106 24.575725555419922 107 24.67066764831543 108 24.711843490600586 109 24.612068176269531
		 110 24.446596145629883 111 24.387264251708984 112 24.544771194458008 113 24.967561721801758
		 114 25.576597213745117 115 26.224399566650391 116 26.70875358581543 117 26.909788131713867
		 118 26.951663970947266 119 26.946386337280273 120 26.973535537719727;
createNode animCurveTA -n "Character1_RightUpLeg_rotateY";
	rename -uid "174854F7-4BF8-B9E2-235C-3AB83965A5D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 36.329387664794922 1 37.804096221923828
		 2 39.487689971923828 3 40.766304016113281 4 41.026145935058594 5 39.3939208984375
		 6 36.431186676025391 7 33.677993774414063 8 32.673919677734375 9 34.667106628417969
		 10 38.555015563964844 11 42.347206115722656 12 44.055717468261719 13 42.361415863037109
		 14 38.583908081054687 15 34.7015380859375 16 32.715499877929688 17 33.595573425292969
		 18 36.136035919189453 19 39.027034759521484 20 40.958545684814453 21 41.797374725341797
		 22 42.172080993652344 23 41.918285369873047 24 40.870685577392578 25 38.261814117431641
		 26 34.418827056884766 27 30.819313049316406 28 28.938739776611328 29 29.134262084960938
		 30 30.444583892822266 31 32.386749267578125 32 34.429637908935547 33 36.072727203369141
		 34 37.578159332275391 35 39.179012298583984 36 40.329479217529297 37 40.483695983886719
		 38 38.786781311035156 39 35.768421173095703 40 32.961822509765625 41 31.900955200195313
		 42 33.830818176269531 43 37.659034729003906 44 41.408088684082031 45 43.102466583251953
		 46 41.423133850097656 47 37.674060821533203 48 33.826358795166016 49 31.855051040649411
		 50 32.742412567138672 51 35.287567138671875 52 38.185070037841797 53 40.128448486328125
		 54 40.983840942382813 55 41.37628173828125 56 41.139583587646484 57 40.107093811035156
		 58 37.540298461914062 59 33.757686614990234 60 30.180883407592777 61 28.241325378417969
		 62 29.471321105957031 63 32.764736175537109 64 35.590057373046875 65 37.403629302978516
		 66 39.081188201904297 67 40.216423034667969 68 40.403644561767578 69 38.811897277832031
		 70 35.889888763427734 71 33.153770446777344 72 32.119293212890625 73 34.017070770263672
		 74 37.757240295410156 75 41.370494842529297 76 42.888137817382813 77 41.018894195556641
		 78 37.090251922607422 79 33.090728759765625 80 31.010929107666016 81 31.850049972534176
		 82 34.416801452636719 83 37.399879455566406 84 39.485179901123047 85 40.527378082275391
		 86 41.149772644042969 87 41.184574127197266 88 40.448711395263672 89 38.198982238769531
		 90 34.711986541748047 91 31.282590866088867 92 29.381694793701172 93 31.820693969726559
		 94 36.695442199707031 95 41.573177337646484 96 44.01849365234375 97 42.571628570556641
		 98 38.806205749511719 99 34.838779449462891 100 32.784904479980469 101 33.643344879150391
		 102 36.144355773925781 103 38.994422912597656 104 40.901847839355469 105 41.737506866455078
		 106 42.124912261962891 107 41.882957458496094 108 40.849246978759766 109 38.101184844970703
		 110 34.018745422363281 111 30.396652221679684 112 29.030475616455078 113 31.364645004272461
		 114 36.123584747314453 115 41.0191650390625 116 43.764492034912109 117 43.460979461669922
		 118 41.425148010253906 119 38.699642181396484 120 36.329387664794922;
createNode animCurveTA -n "Character1_RightUpLeg_rotateZ";
	rename -uid "707851C6-4780-B0E0-272A-0B9FFFBB2D96";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 120 ".ktv[0:119]"  0 -10.592131614685059 1 -10.396580696105957
		 2 -10.174321174621582 3 -9.9508628845214844 4 -9.7388572692871094 5 -9.5304384231567383
		 6 -9.2873687744140625 7 -9.0081882476806641 8 -8.7429676055908203 9 -8.5638751983642578
		 10 -8.4885292053222656 11 -8.4870309829711914 12 -8.4953947067260742 13 -8.4358968734741211
		 14 -8.3378505706787109 15 -8.2935447692871094 16 -8.2211923599243164 17 -8.4147701263427734
		 18 -8.8047208786010742 19 -9.3257541656494141 20 -9.9039535522460937 21 -10.506043434143066
		 22 -11.137866973876953 23 -11.763492584228516 24 -12.34506893157959 25 -12.804409027099609
		 26 -13.144074440002441 27 -13.452590942382813 28 -13.818911552429199 29 -14.250079154968262
		 30 -14.662921905517576 31 -14.848756790161135 32 -15.142107009887697 33 -15.489977836608887
		 34 -15.89527702331543 35 -16.352260589599609 36 -16.807659149169922 37 -17.21306037902832
		 38 -17.5123291015625 39 -17.743328094482422 40 -17.983499526977539 41 -18.282983779907227
		 42 -18.662397384643555 43 -19.044000625610352 44 -19.331127166748047 45 -19.439191818237305
		 46 -19.330226898193359 47 -19.199621200561523 48 -19.123418807983398 49 -19.136127471923828
		 50 -19.231548309326172 51 -19.338912963867188 52 -19.414358139038086 53 -19.448871612548828
		 54 -19.450677871704102 56 -19.437402725219727 57 -19.448959350585938 58 -19.503849029541016
		 59 -19.580682754516602 60 -19.618221282958984 61 -19.548992156982422 62 -19.353097915649414
		 63 -19.094165802001953 64 -18.852794647216797 65 -18.657745361328125 66 -18.471757888793945
		 67 -18.294101715087891 68 -18.107683181762695 69 -17.870515823364258 70 -17.55615234375
		 71 -17.20068359375 72 -16.878604888916016 73 -16.677501678466797 74 -16.585599899291992
		 75 -16.5123291015625 76 -16.185422897338867 77 -15.520942687988281 78 -14.60825729370117
		 79 -13.564105033874512 80 -12.502328872680664 81 -11.497411727905273 82 -10.508859634399414
		 83 -9.489954948425293 84 -8.3883905410766602 85 -7.2178826332092276 86 -6.0378985404968262
		 87 -4.8829288482666016 88 -3.8020641803741455 89 -2.8388314247131348 90 -2.0677714347839355
		 91 -2.2414205074310303 92 -2.3810558319091797 93 -2.5400693416595459 94 -2.6308083534240723
		 95 -2.5855813026428223 96 -2.4420638084411621 97 -2.3303260803222656 98 -2.288769006729126
		 99 -2.2793805599212646 100 -2.2588272094726562 101 -2.191413402557373 102 -2.0780041217803955
		 103 -1.9558354616165161 104 -1.9264878034591675 105 -2.0519306659698486 106 -2.1170525550842285
		 107 -2.36141037940979 108 -2.7785537242889404 109 -3.4002449512481689 110 -4.1411938667297363
		 111 -4.8754134178161621 112 -5.5471773147583008 113 -6.1166882514953613 114 -6.6094799041748047
		 115 -7.0820860862731934 116 -7.6530051231384268 117 -8.3888835906982422 118 -9.1794948577880859
		 119 -9.932948112487793 120 -10.592131614685059;
createNode animCurveTU -n "Character1_RightUpLeg_visibility";
	rename -uid "8703C256-4A13-FFE6-0152-F8A9CB5ABCD6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightLeg_translateX";
	rename -uid "481DCA89-41A4-486C-CF55-9CB981F6CAF0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3108952045440674;
createNode animCurveTL -n "Character1_RightLeg_translateY";
	rename -uid "1AF52C30-4EC2-A9F8-7E67-17818DD16383";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 22.181583404541016;
createNode animCurveTL -n "Character1_RightLeg_translateZ";
	rename -uid "4C979A2E-48D5-FAD0-4171-56A5514BA270";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.6536058187484741;
createNode animCurveTU -n "Character1_RightLeg_scaleX";
	rename -uid "E9B514BB-4CC7-4889-C84A-8894BBA349B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightLeg_scaleY";
	rename -uid "C92B9F81-4E60-060B-FE47-F09A2C30C829";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightLeg_scaleZ";
	rename -uid "92C3F8B1-4D09-EF9C-6D41-ADA3F59E2679";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightLeg_rotateX";
	rename -uid "FDBD9A00-4BC5-5CBA-466B-9F8461A7E3E3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 9.1717491149902344 1 9.2270965576171875
		 2 9.2299633026123047 3 9.3103504180908203 4 9.597926139831543 5 10.265397071838379
		 6 11.170858383178711 7 11.997965812683105 8 12.486284255981445 9 12.448881149291992
		 10 12.071818351745605 11 11.702877998352051 12 11.738350868225098 13 12.44207763671875
		 14 13.515968322753906 15 14.564064979553221 16 14.734036445617678 17 14.387753486633301
		 18 13.695513725280762 19 12.844381332397461 20 12.071860313415527 21 11.43402099609375
		 22 10.86985969543457 23 10.435674667358398 24 10.188389778137207 25 10.303158760070801
		 26 10.725597381591797 27 11.174740791320801 28 11.396849632263184 29 11.337855339050293
		 30 11.199101448059082 31 10.936448097229004 32 11.025030136108398 33 11.525527000427246
		 34 12.340144157409668 35 13.351161956787109 36 14.601231575012207 37 16.120561599731445
		 38 17.969219207763672 39 19.947353363037109 40 21.747804641723633 41 23.163810729980469
		 42 24.048803329467773 43 24.443048477172852 44 24.509559631347656 45 24.524745941162109
		 46 24.947629928588867 47 25.750848770141602 48 26.586170196533203 49 27.217935562133789
		 50 27.590158462524414 51 27.792827606201172 52 27.924001693725586 53 28.14665412902832
		 54 28.487104415893555 55 28.872774124145508 56 29.322067260742184 57 29.849466323852539
		 58 30.545726776123047 59 31.306135177612301 60 31.888593673706058 61 32.305618286132813
		 62 32.374492645263672 63 32.185195922851563 64 32.022975921630859 65 31.993612289428711
		 66 31.943626403808594 67 31.933074951171879 68 32.025398254394531 69 32.334220886230469
		 70 32.732345581054687 71 32.964397430419922 72 32.870338439941406 73 32.344776153564453
		 74 31.44890022277832 75 30.378978729248047 76 29.649673461914059 77 28.888273239135742
		 78 27.881864547729492 79 26.3841552734375 80 24.325246810913086 81 21.777069091796875
		 82 18.875936508178711 83 15.768451690673828 84 12.702804565429687 85 9.8227701187133789
		 86 7.1597752571105957 87 4.8709516525268555 88 3.1460511684417725 89 2.3539359569549561
		 90 2.5658135414123535 91 3.8736841678619385 92 4.5834989547729492 93 3.9499535560607915
		 94 2.5362391471862793 95 1.0350073575973511 96 0.17526619136333466 97 0.42053318023681641
		 98 1.2945183515548706 99 2.2069830894470215 100 2.5849347114562988 101 2.2125442028045654
		 102 1.4470266103744507 103 0.65713959932327271 104 0.21866877377033234 105 0.17603333294391632
		 106 0.20463086664676666 107 0.53071904182434082 108 1.1742243766784668 109 2.3514108657836914
		 110 3.9078636169433598 111 5.3387737274169922 112 6.2147355079650879 113 6.2069187164306641
		 114 5.6563482284545898 115 5.1130623817443848 116 5.1268963813781738 117 5.8726620674133301
		 118 6.9723844528198242 119 8.1526861190795898 120 9.1717491149902344;
createNode animCurveTA -n "Character1_RightLeg_rotateY";
	rename -uid "426D8784-4B97-F410-7A43-178E12F34CEA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 22.336856842041016 1 22.30799674987793
		 2 22.228096008300781 3 22.211259841918945 4 22.374229431152344 5 22.899290084838867
		 6 23.719635009765625 7 24.542503356933594 8 25.009302139282227 9 24.803745269775391
		 10 24.176046371459961 11 23.578397750854492 12 23.425928115844727 13 24.008022308349609
		 14 25.104360580444336 15 26.30445671081543 16 26.706802368164062 17 26.348167419433594
		 18 25.552885055541992 19 24.666725158691406 20 23.991083145141602 21 23.532077789306641
		 22 23.169486999511719 23 22.935390472412109 24 22.862882614135742 25 23.10981559753418
		 26 23.643098831176758 27 24.200923919677734 28 24.485988616943359 29 24.406923294067383
		 30 24.18903923034668 31 23.855731964111328 32 23.758460998535156 33 23.927377700805664
		 34 24.238979339599609 35 24.561586380004883 36 24.977739334106445 37 25.616521835327148
		 38 26.751880645751953 39 28.312149047851563 40 29.843601226806644 41 30.75828742980957
		 42 30.503643035888675 43 29.417985916137695 44 28.2364501953125 45 27.694734573364258
		 46 28.281496047973633 47 29.663457870483402 48 31.148479461669922 49 31.954355239868164
		 50 31.671241760253906 51 30.734010696411129 52 29.641910552978516 53 28.897117614746097
		 54 28.543432235717773 55 28.368741989135742 56 28.443109512329102 57 28.841516494750977
		 58 29.881620407104492 59 31.472043991088867 60 33.032127380371094 61 33.973903656005859
		 62 33.538475036621094 63 32.225242614746094 64 31.155063629150391 65 30.560079574584957
		 66 30.020053863525387 67 29.714736938476566 68 29.822984695434567 69 30.709089279174805
		 70 32.182746887207031 71 33.5615234375 72 34.124179840087891 73 33.288684844970703
		 74 31.591611862182617 75 29.949363708496094 76 29.3002815246582 77 29.981647491455078
		 78 31.326389312744141 79 32.420833587646484 80 32.442909240722656 81 31.161165237426754
		 82 29.157268524169918 83 26.942722320556641 84 24.870359420776367 85 22.911018371582031
		 86 20.960275650024414 87 19.099800109863281 88 17.447404861450195 89 16.229122161865234
		 90 15.650866508483885 91 16.463554382324219 92 17.015012741088867 93 16.917570114135742
		 94 16.493108749389648 95 16.095539093017578 96 15.909866333007812 97 15.981783866882324
		 98 16.213193893432617 99 16.490234375 100 16.617937088012695 101 16.514535903930664
		 102 16.327455520629883 103 16.190038681030273 104 16.187585830688477 105 16.307764053344727
		 106 16.37830924987793 107 16.581060409545898 108 16.923908233642578 109 17.499025344848633
		 110 18.331203460693359 111 19.24955940246582 112 19.955730438232422 113 20.121744155883789
		 114 19.922868728637695 115 19.703342437744141 116 19.750507354736328 117 20.170204162597656
		 118 20.824750900268555 119 21.603048324584961 120 22.336856842041016;
createNode animCurveTA -n "Character1_RightLeg_rotateZ";
	rename -uid "C0597ABA-42C1-8E11-2762-86A3F7AFA6F2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 36.241134643554688 1 37.424385070800781
		 2 38.740810394287109 3 39.718051910400391 4 39.887050628662109 5 38.574077606201172
		 6 36.210285186767578 7 33.998176574707031 8 33.168666839599609 9 34.726902008056641
		 10 37.789119720458984 11 40.786201477050781 12 42.194969177246094 13 41.020313262939453
		 14 38.272636413574219 15 35.474388122558594 16 33.834312438964844 17 34.348628997802734
		 18 36.103263854980469 19 38.092342376708984 20 39.324604034423828 21 39.734561920166016
		 22 39.790378570556641 23 39.383640289306641 24 38.401908874511719 25 36.255561828613281
		 26 33.185535430908203 27 30.353252410888668 28 28.964902877807614 29 29.376138687133789
		 30 30.816688537597656 31 32.508785247802734 32 34.600154876708984 33 36.638278961181641
		 34 38.774322509765625 35 41.161346435546875 36 43.301616668701172 37 44.7174072265625
		 38 44.718482971191406 39 43.692276000976563 40 42.780742645263672 41 43.126239776611328
		 42 45.610439300537109 43 49.262615203857422 44 52.529399871826172 45 53.990943908691406
		 46 53.072967529296875 47 50.767143249511719 48 48.455318450927734 49 47.589553833007812
		 50 48.858627319335938 51 51.334758758544922 52 54.029159545898438 53 56.019996643066406
		 54 57.242118835449219 55 58.094169616699219 56 58.463016510009759 57 58.233577728271491
		 58 56.901252746582031 59 54.628917694091797 60 52.320423126220703 61 51.077110290527344
		 62 52.002735137939453 63 54.2757568359375 64 56.148075103759766 65 57.271644592285156
		 66 58.267608642578118 67 58.853782653808594 68 58.750587463378906 69 57.392299652099609
		 70 55.055191040039063 71 52.751953125 72 51.572673797607422 73 52.438785552978516
		 74 54.559707641601563 75 56.535717010498047 76 57.008476257324219 77 54.580486297607422
		 78 50.131969451904297 79 45.04962158203125 80 40.940696716308594 81 38.756546020507813
		 82 37.757904052734375 83 37.045070648193359 84 35.701297760009766 85 33.709823608398437
		 86 31.597261428833008 87 29.338787078857422 88 26.905500411987305 89 23.846284866333008
		 90 20.479799270629883 91 18.400739669799805 92 17.519805908203125 93 19.984514236450195
		 94 24.307901382446289 95 28.62083625793457 96 31.078670501708988 97 30.55925178527832
		 98 28.26911735534668 99 25.800451278686523 100 24.760993957519531 101 25.883682250976562
		 102 28.213556289672852 103 30.764741897583008 104 32.554241180419922 105 33.491992950439453
		 106 33.842575073242187 107 33.825668334960938 108 33.311935424804688 109 31.58997917175293
		 110 28.920032501220707 111 26.645252227783203 112 26.134172439575195 113 28.539169311523438
		 114 32.832065582275391 115 37.205936431884766 116 39.892547607421875 117 40.219779968261719
		 118 39.199626922607422 119 37.611274719238281 120 36.241134643554688;
createNode animCurveTU -n "Character1_RightLeg_visibility";
	rename -uid "B8A70385-416C-716A-6ECA-68834AB48849";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFoot_translateX";
	rename -uid "B8AE9B4C-4107-F623-541F-E692F32601D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.668137550354004;
createNode animCurveTL -n "Character1_RightFoot_translateY";
	rename -uid "1D7B8E72-42EC-FAA0-7570-A99D29E3ECD9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3660926818847656;
createNode animCurveTL -n "Character1_RightFoot_translateZ";
	rename -uid "FFC56E71-44F6-6EA3-9953-87A4CF2CADD9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -15.681424140930176;
createNode animCurveTU -n "Character1_RightFoot_scaleX";
	rename -uid "C131C78E-4D80-81AF-EA62-FF82B1596922";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightFoot_scaleY";
	rename -uid "448F4734-4AC2-2800-8B5B-1A9E7B0F6870";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999958276748657;
createNode animCurveTU -n "Character1_RightFoot_scaleZ";
	rename -uid "A1D79A82-4E3E-6A62-6C32-2DB27E205CEB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFoot_rotateX";
	rename -uid "FDB80A63-4413-EEFE-C9B5-1190FB04A2B9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -0.13029107451438904 1 0.46731504797935486
		 2 1.1565907001495361 3 1.670649528503418 4 1.7368284463882446 5 0.96090888977050781
		 6 -0.42027309536933899 7 -1.7214809656143188 8 -2.2460691928863525 9 -1.4164055585861206
		 10 0.29676651954650879 11 1.9970834255218504 12 2.7646355628967285 13 1.9707993268966675
		 14 0.19984234869480133 15 -1.630396842956543 16 -2.523578405380249 17 -2.1036946773529053
		 18 -0.92275172472000122 19 0.41705125570297241 20 1.3225711584091187 21 1.7664881944656372
		 22 2.0012080669403076 23 1.9607594013214114 24 1.5810656547546387 25 0.53410917520523071
		 26 -1.0222048759460449 27 -2.4389522075653076 28 -3.0944695472717285 29 -2.8194139003753662
		 30 -2.0418851375579834 31 -1.1406393051147461 32 -0.19020861387252808 33 0.59955108165740967
		 34 1.3351935148239136 35 2.1665306091308594 36 2.8340523242950439 37 3.0435042381286621
		 38 2.3064911365509033 39 0.82241970300674438 40 -0.62335205078125 41 -1.1615158319473267
		 42 -0.027504382655024529 43 2.2228271961212158 44 4.4525051116943359 45 5.4898557662963867
		 46 4.7476038932800293 47 2.8553023338317871 48 0.92648607492446899 49 0.11599820107221605
		 50 0.961464524269104 51 2.7999997138977051 52 4.8784184455871582 53 6.424034595489502
		 54 7.3678503036499023 55 8.0123395919799805 56 8.2416114807128906 57 7.936640739440918
		 58 6.6219558715820313 59 4.463679313659668 60 2.333411693572998 61 1.1244485378265381
		 62 1.9003409147262573 63 3.9342269897460937 64 5.5920333862304687 65 6.492337703704834
		 66 7.2756614685058603 67 7.6916556358337402 68 7.4907937049865723 69 6.154944896697998
		 70 3.9752025604248042 71 1.932729959487915 72 1.0157061815261841 73 2.0154693126678467
		 74 4.2267179489135742 75 6.389747142791748 76 7.1480107307434082 77 5.6343717575073242
		 78 2.7789690494537354 79 -0.10155986994504929 80 -1.7995737791061401 81 -1.9029219150543213
		 82 -1.1599246263504028 83 -0.28916329145431519 84 0.10978295654058456 85 0.080731779336929321
		 86 -0.078510813415050507 87 -0.38616165518760681 88 -0.85912930965423584 89 -1.7051544189453125
		 90 -2.8096458911895752 91 -4.0184803009033203 92 -4.6910743713378906 93 -3.8705470561981201
		 94 -2.147662878036499 95 -0.34389132261276245 96 0.66993188858032227 97 0.35062149167060852
		 98 -0.74664837121963501 99 -1.9076381921768188 100 -2.4253623485565186 101 -2.0124974250793457
		 102 -1.0750391483306885 103 -0.032924667000770569 104 0.68286639451980591 105 1.0386397838592529
		 106 1.2059003114700317 107 1.1622133255004883 108 0.84180974960327148 109 -0.10048939287662506
		 110 -1.553744912147522 111 -2.8776066303253174 112 -3.398629903793335 113 -2.480811595916748
		 114 -0.5534253716468811 115 1.497160792350769 116 2.7096436023712158 117 2.6783955097198486
		 118 1.9170973300933838 119 0.83403229713439941 120 -0.13029107451438904;
createNode animCurveTA -n "Character1_RightFoot_rotateY";
	rename -uid "98C86AEA-44FD-7D75-127E-B1BD14AF77A8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -0.0623842105269432 1 -0.089456349611282349
		 2 -0.11093079298734665 3 -0.13149538636207581 4 -0.16070850193500519 5 -0.2068772166967392
		 6 -0.24920555949211121 7 -0.27001544833183289 8 -0.28913325071334839 9 -0.32783326506614685
		 10 -0.35098674893379211 11 -0.34388858079910278 12 -0.35135865211486816 13 -0.41826203465461731
		 14 -0.50035667419433594 15 -0.54599398374557495 16 -0.51967775821685791 17 -0.49981838464736933
		 18 -0.47956389188766485 19 -0.44630035758018494 20 -0.40971952676773071 21 -0.37392792105674744
		 22 -0.34052708745002747 23 -0.31948569416999817 24 -0.31690901517868042 25 -0.33612361550331116
		 26 -0.33592960238456726 27 -0.29035696387290955 28 -0.23780357837676999 29 -0.21460582315921783
		 30 -0.21077226102352142 31 -0.20595639944076538 32 -0.22570085525512695 33 -0.26925677061080933
		 34 -0.34991386532783508 35 -0.44927114248275751 36 -0.54458063840866089 37 -0.62775826454162598
		 38 -0.70825433731079102 39 -0.78638160228729248 40 -0.85182887315750122 41 -0.92021781206130981
		 42 -0.9915771484375 43 -1.0027285814285278 44 -0.94874656200408924 45 -0.91702276468276966
		 46 -0.97217708826065063 47 -1.0621490478515625 48 -1.1067755222320557 49 -1.1012383699417114
		 50 -1.0792412757873535 51 -1.0269159078598022 52 -0.94001829624176025 53 -0.85760349035263062
		 54 -0.7943044900894165 55 -0.74441987276077271 56 -0.72755342721939087 57 -0.76081055402755737
		 58 -0.88192582130432129 59 -1.042615532875061 60 -1.1539788246154785 61 -1.1980359554290771
		 62 -1.1461842060089111 63 -1.0133839845657349 64 -0.90792578458786022 65 -0.90152060985565197
		 66 -0.92358124256134022 67 -0.96861767768859874 68 -1.0341035127639771 69 -1.1401517391204834
		 70 -1.2570010423660278 71 -1.3258154392242432 72 -1.3431223630905151 73 -1.3204855918884277
		 74 -1.2283463478088379 75 -1.0759028196334839 76 -0.96671193838119518 77 -0.97833967208862316
		 78 -1.028256893157959 79 -0.99777889251708973 80 -0.87162387371063232 81 -0.70961803197860718
		 82 -0.5248674750328064 83 -0.31900882720947266 84 -0.11604570597410202 85 0.077742472290992737
		 86 0.26438114047050476 87 0.41981863975524902 88 0.51949113607406616 89 0.5272601842880249
		 90 0.44970393180847168 91 0.27232947945594788 92 0.15309539437294006 93 0.15586961805820465
		 94 0.2736981213092804 95 0.4550386369228363 96 0.57948243618011475 97 0.56590372323989868
		 98 0.4882872998714447 99 0.42626059055328369 100 0.41703957319259644 101 0.45071029663085938
		 102 0.50384217500686646 103 0.56249290704727173 104 0.59953552484512329 105 0.61035364866256714
		 106 0.61666417121887207 107 0.59290963411331177 108 0.52702319622039795 109 0.39228552579879761
		 110 0.22843599319458005 111 0.098236396908760071 112 0.017002560198307037 113 -0.013264855369925499
		 114 0.026657257229089737 115 0.1229192093014717 116 0.18518361449241638 117 0.15502817928791046
		 118 0.079060360789299011 119 -0.0029500005766749382 120 -0.0623842105269432;
createNode animCurveTA -n "Character1_RightFoot_rotateZ";
	rename -uid "2BAC6758-4156-D47B-E164-028DCFE0367B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -0.53402864933013916 1 -0.77549892663955688
		 2 -0.99848127365112305 3 -1.209791898727417 4 -1.430115818977356 5 -1.6654214859008789
		 6 -1.8526695966720583 7 -1.972339391708374 8 -2.1225943565368652 9 -2.4061353206634521
		 10 -2.7097024917602539 11 -2.9258482456207275 12 -3.1187634468078613 13 -3.3748493194580078
		 14 -3.591143131256104 15 -3.7027451992034917 16 -3.5186367034912109 17 -3.4631295204162598
		 18 -3.4423148632049561 19 -3.3461790084838867 20 -3.1516742706298828 21 -2.9003989696502686
		 22 -2.6402890682220459 23 -2.3862979412078857 24 -2.146996021270752 25 -1.9098790884017947
		 26 -1.626856803894043 27 -1.3224388360977173 28 -1.1390838623046875 29 -1.1551746129989624
		 30 -1.3109767436981201 31 -1.4669923782348633 32 -1.8193792104721067 33 -2.3203864097595215
		 34 -2.9461846351623535 35 -3.6443288326263428 36 -4.3840508460998535 37 -5.1578879356384277
		 38 -5.9551920890808105 39 -6.6901764869689941 40 -7.30906105041504 41 -7.881397247314454
		 42 -8.475916862487793 43 -8.9342222213745117 44 -9.1361188888549805 45 -9.1943159103393555
		 46 -9.3152408599853516 47 -9.4869098663330078 48 -9.5770635604858398 49 -9.6626377105712891
		 50 -9.8694047927856445 51 -10.099485397338867 52 -10.250593185424805 53 -10.342601776123047
		 54 -10.427828788757324 55 -10.5255126953125 56 -10.65516185760498 57 -10.826837539672852
		 58 -11.052080154418945 59 -11.221275329589844 60 -11.248059272766113 61 -11.291037559509277
		 62 -11.456696510314941 63 -11.648785591125488 64 -11.783169746398926 65 -11.929022789001465
		 66 -12.054804801940918 67 -12.182291030883789 68 -12.337353706359863 69 -12.533162117004395
		 70 -12.657124519348145 71 -12.635404586791992 72 -12.563522338867188 73 -12.549843788146973
		 74 -12.456565856933594 75 -12.171998977661133 76 -11.935164451599121 77 -11.604393005371094
		 78 -10.996064186096191 79 -9.9971961975097656 80 -8.7955045700073242 81 -7.6367125511169434
		 82 -6.4521393775939941 83 -5.1311407089233398 84 -3.639262437820435 85 -2.0348315238952637
		 86 -0.41975542902946472 87 1.137731671333313 88 2.5500035285949707 89 3.7377445697784424
		 90 4.5886697769165039 91 4.5890464782714844 92 4.5827713012695313 93 4.2929229736328125
		 94 3.9812662601470947 95 3.878488302230835 96 3.9620797634124756 97 4.091832160949707
		 98 4.2802581787109375 99 4.5290498733520508 100 4.726830005645752 101 4.7538132667541504
		 102 4.6933846473693848 103 4.6220731735229492 104 4.5434613227844238 105 4.4176931381225586
		 106 4.3118982315063477 107 4.1225724220275879 108 3.8624258041381831 109 3.5609426498413086
		 110 3.2929773330688477 111 3.0600404739379883 112 2.7305424213409424 113 2.2152111530303955
		 114 1.6874908208847046 115 1.3078334331512451 116 0.98796474933624279 117 0.58747094869613647
		 118 0.16035318374633789 119 -0.22308269143104553 120 -0.53402864933013916;
createNode animCurveTU -n "Character1_RightFoot_visibility";
	rename -uid "171F5D4B-4FFE-6B50-6855-2790ABDABA48";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightToeBase_translateX";
	rename -uid "AE4F17A7-4892-8E97-6309-019078C3522C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.103175163269043;
createNode animCurveTL -n "Character1_RightToeBase_translateY";
	rename -uid "C7A5F68D-4ABB-F380-FA09-4A955F86D807";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.7509346008300781;
createNode animCurveTL -n "Character1_RightToeBase_translateZ";
	rename -uid "EDD4F388-4BC7-A7F5-C50B-34993024F3CE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.6768350601196289;
createNode animCurveTU -n "Character1_RightToeBase_scaleX";
	rename -uid "3E689AE4-498B-5618-4120-D3A497EAAEE8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightToeBase_scaleY";
	rename -uid "697D102E-4C52-D39E-3F0E-FEB1682B10D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightToeBase_scaleZ";
	rename -uid "85A70ED1-4EF6-90C7-162C-6FAB83663B53";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightToeBase_rotateX";
	rename -uid "335766A4-4548-31C0-C083-B6A19694E222";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.8361593901470314e-009;
createNode animCurveTA -n "Character1_RightToeBase_rotateY";
	rename -uid "FE7F7149-4BCC-D070-5878-4C89F173DE1B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.6321476248591935e-011;
createNode animCurveTA -n "Character1_RightToeBase_rotateZ";
	rename -uid "3B5D4930-4557-5391-3F3B-74A75FB53F65";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.2923511139593131e-011;
createNode animCurveTU -n "Character1_RightToeBase_visibility";
	rename -uid "B3DA2BB0-49E9-7728-ECBA-A4B8A5F7306E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateX";
	rename -uid "D681964A-47A9-4C71-9B94-899F901328FD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.31789970397949219;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateY";
	rename -uid "1D6CB8B9-4A04-39FF-F019-E19750F71737";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.6937241554260254;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateZ";
	rename -uid "709A6D5A-4DFD-83DA-2242-25A3E1BDDEE5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.4678492546081543;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleX";
	rename -uid "75FC4C7F-4BD2-82DC-6EA8-75B7C79B8FB2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleY";
	rename -uid "1ACCB42A-4525-E516-50CD-09B66ED6E206";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleZ";
	rename -uid "12D6C426-4FCA-1BB7-527E-7092614CD829";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateX";
	rename -uid "458E567F-4B9D-F687-0CD4-EEA95FE2A5E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.267352832736492e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateY";
	rename -uid "0718523A-4BFC-AEBE-BCB8-5D9B7E2F1735";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.04429750694635e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateZ";
	rename -uid "232C0B1E-4FEB-0C5E-3897-7888ADD1FBF9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.8816796705561956e-009;
createNode animCurveTU -n "Character1_RightFootMiddle1_visibility";
	rename -uid "A6869A59-4139-D026-2BE9-1DBFF7782D61";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateX";
	rename -uid "09B96933-448E-5350-BF0E-56938883B777";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3449039459228516;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateY";
	rename -uid "24E63B0B-483F-697D-B09C-9FACB608E7B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3579239845275879;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateZ";
	rename -uid "7E0DC732-4C35-606D-F943-E1BE716DD470";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.7014765739440918;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleX";
	rename -uid "A1BC1759-4E6B-82B9-0817-11B257A0A1D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleY";
	rename -uid "479C0698-4061-3E9E-F08B-2A968E6C9CDC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleZ";
	rename -uid "79D6ACC2-4DC2-B574-9F4A-369E231D15AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateX";
	rename -uid "DD6BAA5C-42E5-8A30-2935-10B62091E945";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.0085198454135025e-008;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateY";
	rename -uid "602BE04C-45FB-EA21-6B43-FCA83A5993AA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.9889015706885402e-009;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateZ";
	rename -uid "1EB1C67D-4605-5151-D000-64AB676D8193";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.6054467977255626e-008;
createNode animCurveTU -n "Character1_RightFootMiddle2_visibility";
	rename -uid "E68141EF-436D-9E1C-93B8-68AA6B2DFB1E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Spine_translateX";
	rename -uid "9152E176-4CF6-F876-C4A4-49BB7133683F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.9174466133117676;
createNode animCurveTL -n "Character1_Spine_translateY";
	rename -uid "5BD4F2BF-499A-1BF0-7164-328906DBB2EE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.894325256347656;
createNode animCurveTL -n "Character1_Spine_translateZ";
	rename -uid "5258D669-443C-A34D-7A30-BFAFB4298A2B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1938900947570801;
createNode animCurveTU -n "Character1_Spine_scaleX";
	rename -uid "B053CC66-4AB7-069B-4CD3-C7A6C4630E36";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine_scaleY";
	rename -uid "8AB7098D-421A-25C5-A4D4-F7BE63830465";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine_scaleZ";
	rename -uid "E13DEA31-4D39-8C38-272E-36B144A0D050";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_Spine_rotateX";
	rename -uid "D3F0C53B-4A49-779A-FF49-539879693DA6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 120 ".ktv[0:119]"  0 -12.008066177368164 1 -11.992738723754883
		 2 -11.97701358795166 3 -11.960978507995605 4 -11.944725036621094 5 -11.928347587585449
		 6 -11.911937713623047 7 -11.895594596862793 8 -11.879417419433594 9 -11.863507270812988
		 10 -11.847965240478516 11 -11.83289909362793 12 -11.818413734436035 13 -11.804620742797852
		 14 -11.791629791259766 15 -11.779553413391113 16 -11.76827335357666 17 -11.791139602661133
		 18 -11.854891777038574 19 -11.919304847717285 20 -11.944055557250977 21 -11.935837745666504
		 22 -11.928220748901367 23 -11.921175003051758 24 -11.914657592773438 25 -11.908617973327637
		 26 -11.902993202209473 27 -11.89771842956543 28 -11.892726898193359 29 -11.865646362304688
		 30 -11.807863235473633 31 -11.731493949890137 32 -11.651087760925293 33 -11.590837478637695
		 34 -11.545851707458496 35 -11.497561454772949 36 -11.449653625488281 37 -11.405814170837402
		 38 -11.369728088378906 39 -11.34507942199707 40 -11.335548400878906 41 -11.344815254211426
		 42 -11.376557350158691 43 -11.434453010559082 44 -11.522175788879395 45 -11.643404960632324
		 46 -11.815766334533691 47 -12.047787666320801 48 -12.329503059387207 49 -12.650924682617187
		 50 -13.035698890686035 51 -13.480648040771484 52 -13.935298919677734 53 -14.348735809326174
		 54 -14.717179298400877 55 -15.06402587890625 56 -15.378738403320314 57 -15.650715827941893
		 58 -15.869316101074219 59 -16.023902893066406 60 -16.103897094726562 61 -16.107053756713867
		 62 -15.987277030944826 63 -15.761492729187012 64 -15.535016059875488 65 -15.324179649353029
		 66 -15.08125114440918 67 -14.814742088317869 68 -14.533099174499512 69 -14.244694709777832
		 70 -13.957819938659668 71 -13.680681228637695 72 -13.421413421630859 73 -13.188079833984375
		 74 -12.98868465423584 75 -12.831184387207031 76 -12.703268051147461 77 -12.58689022064209
		 78 -12.481341361999512 79 -12.385887145996094 80 -12.299784660339355 81 -12.255341529846191
		 82 -12.258297920227051 83 -12.26826286315918 84 -12.24509334564209 85 -12.194960594177246
		 86 -12.150303840637207 87 -12.110627174377441 88 -12.075464248657227 89 -12.044366836547852
		 90 -12.016885757446289 91 -11.997284889221191 92 -11.989231109619141 93 -11.958502769470215
		 94 -11.897003173828125 95 -11.842677116394043 96 -11.833680152893066 97 -11.862188339233398
		 98 -11.893845558166504 99 -11.927199363708496 100 -11.960774421691895 101 -12.026427268981934
		 102 -12.129555702209473 103 -12.228617668151855 104 -12.281609535217285 105 -12.29341983795166
		 106 -12.299331665039063 107 -12.302289009094238 109 -12.30063533782959 110 -12.296710968017578
		 111 -12.291184425354004 112 -12.284408569335938 113 -12.242986679077148 114 -12.160579681396484
		 115 -12.078019142150879 116 -12.035942077636719 117 -12.027826309204102 118 -12.020307540893555
		 119 -12.013639450073242 120 -12.008066177368164;
createNode animCurveTA -n "Character1_Spine_rotateY";
	rename -uid "56EEDE2E-4BE8-45AD-C0D6-949013DEDAE8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 1.3196120262145996 1 1.3744473457336426
		 2 1.4276102781295776 3 1.4795147180557251 4 1.5305750370025635 5 1.5812065601348877
		 6 1.63182532787323 7 1.6828492879867554 8 1.7346957921981812 9 1.7877848148345947
		 10 1.8425357341766355 11 1.8993691205978394 12 1.9587066173553469 13 2.0209696292877197
		 14 2.0865802764892578 15 2.1559605598449707 16 2.2350542545318604 17 2.2839188575744629
		 18 2.2909862995147705 19 2.3058798313140869 20 2.3781652450561523 21 2.4962422847747803
		 22 2.6135797500610352 23 2.7273290157318115 24 2.8346409797668457 25 2.9326660633087158
		 26 3.0185537338256836 27 3.0894544124603271 28 3.1425180435180664 29 3.2039728164672852
		 30 3.2821657657623291 31 3.3539392948150635 32 3.3983163833618164 33 3.3888652324676514
		 34 3.3372092247009277 35 3.2729041576385498 36 3.1964132785797119 37 3.108210563659668
		 38 3.0087838172912598 39 2.8986291885375977 40 2.7782473564147949 41 2.6481363773345947
		 42 2.5087833404541016 43 2.3606534004211426 44 2.2041754722595215 45 2.0397298336029053
		 46 1.8436965942382812 47 1.5984460115432739 48 1.3132867813110352 49 0.99757623672485352
		 50 0.61718529462814331 51 0.17315953969955444 52 -0.27233505249023438 53 -0.6572411060333252
		 54 -0.98031586408615112 55 -1.2747719287872314 56 -1.5302408933639526 57 -1.7362767457962036
		 58 -1.8823953866958616 59 -1.9581266641616824 60 -1.9530655145645144 61 -1.8701599836349487
		 62 -1.6515910625457764 63 -1.3135092258453369 64 -0.98492860794067372 65 -0.68240833282470703
		 66 -0.34034460783004761 67 0.035118214786052704 68 0.43787240982055664 69 0.86187052726745605
		 70 1.3011428117752075 71 1.7498177289962769 72 2.2021379470825195 73 2.6524689197540283
		 74 3.0953061580657959 75 3.5252714157104492 76 3.9921114444732662 77 4.5378732681274414
		 78 5.145756721496582 79 5.7989559173583984 80 6.4806056022644043 81 7.1300754547119141
		 82 7.7215533256530762 83 8.2902364730834961 84 8.8711662292480469 85 9.4381484985351562
		 86 9.9299783706665039 87 10.329036712646484 88 10.617681503295898 89 10.778298377990723
		 90 10.793365478515625 91 10.637862205505371 92 10.314037322998047 93 9.8891334533691406
		 94 9.3955059051513672 95 8.8043222427368164 96 8.086634635925293 97 7.274578094482421
		 98 6.4351305961608887 99 5.5913949012756348 100 4.7662968635559082 101 3.9388818740844722
		 102 3.1230084896087646 103 2.3935263156890869 104 1.8250982761383054 105 1.4311882257461548
		 106 1.1482666730880737 107 0.93628782033920277 108 0.78725892305374146 109 0.69323855638504028
		 110 0.64632219076156616 111 0.63863348960876465 112 0.66231465339660645 113 0.75317156314849854
		 114 0.91211915016174305 115 1.0789464712142944 116 1.193379282951355 117 1.2562896013259888
		 118 1.3035238981246948 119 1.3272426128387451 120 1.3196120262145996;
createNode animCurveTA -n "Character1_Spine_rotateZ";
	rename -uid "CD9DB631-4981-9AD5-9980-7E86E1175781";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 6.5334582328796387 1 6.5859651565551758
		 2 6.6415114402770996 3 6.6993408203125 4 6.7586984634399414 5 6.8188295364379883
		 6 6.8789777755737305 7 6.938389778137207 8 6.9963102340698242 9 7.0519847869873047
		 10 7.1046595573425293 11 7.1535792350769043 12 7.1979904174804687 13 7.2371377944946289
		 14 7.2702670097351074 15 7.2966213226318359 16 7.3154559135437021 17 7.3322715759277344
		 18 7.3491106033325195 19 7.3606867790222177 20 7.3612885475158691 21 7.3527026176452637
		 22 7.3412084579467773 23 7.3276624679565439 24 7.3129210472106934 25 7.2978434562683105
		 26 7.283287525177002 27 7.2701120376586914 28 7.2591767311096191 29 7.2477526664733896
		 30 7.2354154586791992 31 7.2280006408691406 32 7.2295036315917969 33 7.241264820098877
		 34 7.2601509094238272 35 7.2810888290405273 36 7.3023872375488281 37 7.3223714828491211
		 38 7.3393912315368643 39 7.3518266677856436 40 7.3580980300903329 41 7.3566718101501456
		 42 7.3460659980773935 43 7.3248610496521005 44 7.2917041778564453 45 7.2453160285949716
		 46 7.177128791809082 47 7.0834932327270508 48 6.9705624580383301 49 6.8443441390991211
		 50 6.7166690826416016 51 6.5951619148254395 52 6.4777817726135254 53 6.3600997924804687
		 54 6.2461814880371094 55 6.1462702751159668 56 6.063237190246582 57 5.9996433258056641
		 58 5.9578814506530762 59 5.9403653144836426 60 5.9497494697570801 61 5.981503963470459
		 62 6.0147213935852051 63 6.0520796775817871 64 6.1176295280456543 65 6.212611198425293
		 66 6.3235044479370117 67 6.4502105712890625 68 6.5923938751220703 69 6.7494196891784668
		 70 6.9203081130981445 71 7.1037116050720215 72 7.2979049682617179 73 7.5007987022399902
		 74 7.7099676132202148 75 7.9226937294006348 76 8.1631221771240234 77 8.4510030746459961
		 78 8.776728630065918 79 9.1306047439575195 80 9.5028982162475586 81 9.8894405364990234
		 82 10.281682014465332 83 10.663338661193848 84 11.017930030822754 85 11.337011337280273
		 86 11.616827964782715 87 11.847912788391113 88 12.020809173583984 89 12.12604808807373
		 90 12.154123306274414 91 12.093958854675293 92 11.948297500610352 93 11.722826957702637
		 94 11.427907943725586 95 11.081545829772949 96 10.701323509216309 97 10.297398567199707
		 98 9.8758916854858398 99 9.4482097625732422 100 9.0257902145385742 101 8.6254568099975586
		 102 8.2600212097167969 103 7.934680938720704 104 7.6541204452514648 105 7.4308557510375977
		 106 7.2596859931945801 107 7.1197333335876465 108 7.0075821876525879 109 6.9197969436645508
		 110 6.8529248237609863 111 6.8035016059875488 112 6.7680501937866211 113 6.7374191284179687
		 114 6.7071828842163086 115 6.6807255744934082 116 6.6609401702880859 117 6.6430540084838867
		 118 6.6181817054748535 119 6.5828194618225098 120 6.5334582328796387;
createNode animCurveTU -n "Character1_Spine_visibility";
	rename -uid "93CF210B-4166-5190-1871-A1B0079390DC";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Spine1_translateX";
	rename -uid "65A74FDD-4464-3F62-F7C8-578F8C1CBBEE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2515636682510376;
createNode animCurveTL -n "Character1_Spine1_translateY";
	rename -uid "ACCBA09B-4696-2295-3052-55A4B631CC5F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -15.818062782287598;
createNode animCurveTL -n "Character1_Spine1_translateZ";
	rename -uid "957A33BE-4088-1C5D-DAB5-75A13AA7C678";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.5716743469238281;
createNode animCurveTU -n "Character1_Spine1_scaleX";
	rename -uid "0FABD2BB-4FD9-3BF7-EA74-C2AA8D46D045";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine1_scaleY";
	rename -uid "A5278E89-4277-2C80-C40A-ECA2CC44B6CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine1_scaleZ";
	rename -uid "7810E89C-40C4-3012-53D3-988D193E1E98";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_Spine1_rotateX";
	rename -uid "68E03999-415C-85C5-5EE3-739A58FA89C2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -16.682300567626953 1 -15.64647102355957
		 2 -14.493973731994627 3 -13.490054130554199 4 -12.914712905883789 5 -12.646449089050293
		 6 -12.375710487365723 7 -12.105828285217285 8 -11.840205192565918 9 -11.582324028015137
		 10 -11.335731506347656 11 -11.104056358337402 12 -10.891007423400879 13 -10.700360298156738
		 14 -10.535980224609375 15 -10.401824951171875 16 -10.302628517150879 17 -10.356983184814453
		 18 -10.58216381072998 19 -10.830159187316895 20 -10.959096908569336 21 -10.991613388061523
		 22 -11.039450645446777 23 -11.098313331604004 24 -11.163638114929199 25 -10.883502960205078
		 26 -10.190258979797363 27 -9.5285558700561523 28 -9.3924026489257813 29 -10.02472972869873
		 30 -11.082568168640137 31 -12.22373104095459 32 -13.144631385803223 33 -13.56425666809082
		 34 -13.133868217468262 35 -12.067577362060547 36 -10.94866943359375 37 -10.395813941955566
		 38 -10.336498260498047 39 -10.276477813720703 40 -10.216330528259277 41 -10.156427383422852
		 42 -10.096912384033203 43 -10.03775691986084 44 -9.9787874221801758 45 -9.9197425842285156
		 46 -9.8532485961914062 47 -9.7737417221069336 48 -9.6836948394775391 49 -9.5856447219848633
		 50 -9.6041793823242187 51 -9.7651529312133789 52 -9.9243268966674805 53 -9.9408864974975586
		 54 -9.8430404663085938 55 -9.7538270950317383 56 -9.6763086318969727 57 -9.6135826110839844
		 58 -9.2163858413696289 59 -8.4356927871704102 60 -7.7447075843811026 61 -7.6479039192199707
		 62 -8.8994283676147461 63 -10.807465553283691 64 -11.881206512451172 65 -11.585212707519531
		 66 -10.665533065795898 67 -9.7070713043212891 68 -9.3238887786865234 69 -9.4409360885620117
		 70 -9.5649814605712891 71 -9.6950922012329102 72 -9.8303966522216797 73 -9.9700613021850586
		 74 -10.113279342651367 75 -10.259257316589355 76 -10.422386169433594 77 -10.613770484924316
		 78 -10.827731132507324 79 -11.05860424041748 80 -11.300800323486328 81 -11.672924995422363
		 82 -12.193017959594727 83 -12.706316947937012 84 -13.061031341552734 85 -13.277634620666504
		 86 -13.473471641540527 87 -13.643436431884766 88 -13.782472610473633 89 -13.680938720703125
		 90 -13.270733833312988 91 -12.754397392272949 92 -12.351067543029785 93 -12.333724021911621
		 94 -12.497901916503906 95 -12.663992881774902 96 -12.649208068847656 97 -12.475817680358887
		 98 -12.297362327575684 99 -12.12142276763916 100 -11.955677032470703 101 -11.928092956542969
		 102 -12.068751335144043 103 -12.240703582763672 104 -12.310849189758301 105 -12.312319755554199
		 106 -12.35875129699707 107 -12.437928199768066 108 -12.545794486999512 109 -12.432395935058594
		 110 -12.033684730529785 111 -11.626138687133789 112 -11.495242118835449 113 -11.700185775756836
		 114 -12.07883358001709 115 -12.591136932373047 116 -13.205944061279297 117 -13.965578079223633
		 118 -14.862648963928221 119 -15.800065040588377 120 -16.682300567626953;
createNode animCurveTA -n "Character1_Spine1_rotateY";
	rename -uid "37A328E4-42C1-A34D-90AD-5580598BE55A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 27.137903213500977 1 28.089859008789062
		 2 29.102584838867184 3 29.966262817382812 4 30.476602554321293 5 30.741895675659176
		 6 31.005973815917969 7 31.269805908203129 8 31.534423828124996 9 31.800909042358398
		 10 32.070396423339844 11 32.344066619873047 12 32.623142242431641 13 32.908882141113281
		 14 33.202552795410156 15 33.505447387695312 16 33.841232299804687 17 34.051959991455078
		 18 34.092231750488281 19 34.159782409667969 20 34.451923370361328 21 34.922931671142578
		 22 35.3885498046875 23 35.837913513183594 24 36.260166168212891 25 37.234485626220703
		 26 38.856163024902344 27 40.388343811035156 28 41.095375061035156 29 40.699440002441406
		 30 39.639286041259766 31 38.250450134277344 32 36.893539428710938 33 35.949985504150391
		 34 35.777603149414063 35 36.133815765380859 36 36.504135131835937 37 36.388240814208984
		 38 35.858901977539063 39 35.327812194824219 40 34.810726165771484 41 34.323421478271484
		 42 33.881683349609375 43 33.501319885253906 44 33.198123931884766 45 32.987903594970703
		 46 32.869178771972656 47 32.822849273681641 48 32.8382568359375 49 32.904739379882813
		 50 32.837345123291016 51 32.590427398681641 52 32.362468719482422 53 32.352062225341797
		 54 32.513957977294922 55 32.663368225097656 56 32.789783477783203 57 32.882705688476562
		 58 33.522029876708984 59 34.798885345458984 60 35.972103118896484 61 36.318244934082031
		 62 34.947402954101562 63 32.582408905029297 64 30.977895736694336 65 30.840818405151371
		 66 31.318086624145504 67 31.86042594909668 68 31.932239532470703 69 31.590875625610355
		 70 31.25044059753418 71 30.916997909545902 72 30.596654891967777 73 30.295570373535156
		 74 30.019952774047852 75 29.776092529296879 76 29.542539596557617 77 29.297195434570313
		 78 29.045476913452148 79 28.792774200439453 80 28.544401168823242 81 28.133127212524414
		 82 27.530521392822266 83 26.948709487915039 84 26.597579956054688 85 26.446020126342773
		 86 26.326713562011719 87 26.243631362915039 88 26.200637817382812 89 26.702083587646484
		 90 27.893274307250977 91 29.25455284118652 92 30.252382278442383 93 30.186197280883789
		 94 29.604694366455078 95 29.035648345947266 96 29.001317977905277 97 29.414434432983395
		 98 29.833806991577152 99 30.247112274169922 100 30.641979217529297 101 30.833845138549805
		 102 30.775680541992191 103 30.661651611328129 104 30.685714721679687 105 30.80080413818359
		 106 30.840951919555664 107 30.824750900268555 108 30.758337020874023 109 31.193820953369137
		 110 32.262561798095703 111 33.3370361328125 112 33.785373687744141 113 33.348674774169922
		 114 32.402183532714844 115 31.2633056640625 116 30.249155044555664 117 29.42578125
		 118 28.639991760253906 119 27.880653381347656 120 27.137903213500977;
createNode animCurveTA -n "Character1_Spine1_rotateZ";
	rename -uid "84773F87-4C3A-31A8-D43C-ED90A17C4AE3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 3.2056405544281006 1 3.5481889247894287
		 2 3.9478390216827388 3 4.3583378791809082 4 4.7102856636047363 5 5.0098700523376465
		 6 5.3116474151611328 7 5.6112103462219238 8 5.9041337966918945 9 6.1859846115112305
		 10 6.452324390411377 11 6.6986775398254395 12 6.9205517768859863 13 7.1134123802185059
		 14 7.272669792175293 15 7.3936715126037598 16 7.469712257385253 17 7.5637540817260751
		 18 7.6981487274169913 19 7.8057446479797363 20 7.8092522621154794 21 7.7233796119689941
		 22 7.6216812133789062 23 7.5095834732055664 24 7.3925690650939941 25 7.5224032402038565
		 26 7.968186855316163 27 8.4294528961181641 28 8.5323753356933594 29 8.1197242736816406
		 30 7.4614863395690918 31 6.7658329010009766 32 6.1885395050048828 33 5.8530130386352539
		 34 5.8458371162414551 35 6.0845723152160645 36 6.4089837074279785 37 6.6022944450378418
		 38 6.6514739990234375 39 6.7054038047790527 40 6.7630910873413086 41 6.8234109878540039
		 42 6.8851561546325684 43 6.9470343589782715 44 7.0077495574951172 45 7.0660462379455566
		 46 7.1289587020874023 47 7.2028040885925302 48 7.2854528427124023 49 7.3748435974121103
		 50 7.5278406143188477 51 7.7565946578979492 52 7.9891901016235352 53 8.1461982727050781
		 54 8.2354001998901367 55 8.3177299499511719 56 8.3904705047607422 57 8.4508228302001953
		 58 8.7580785751342773 59 9.3714323043823242 60 9.9535942077636719 61 10.094817161560059
		 62 9.2822151184082031 63 8.0354909896850586 64 7.2374749183654785 65 7.0751471519470215
		 66 7.1803874969482431 67 7.3693046569824219 68 7.40903615951538 69 7.2940630912780753
		 70 7.1926169395446777 71 7.1100711822509766 72 7.0516963005065918 73 7.0226702690124512
		 74 7.0280647277832031 75 7.0728402137756348 76 7.1656932830810547 77 7.3076949119567871
		 78 7.4921321868896493 79 7.7120451927185059 80 7.9601807594299316 81 8.2921876907348633
		 82 8.7183990478515625 83 9.15673828125 84 9.5129585266113281 85 9.7867536544799805
		 86 10.038786888122559 87 10.260008811950684 88 10.441354751586914 89 10.733070373535156
		 90 11.17331600189209 91 11.59743595123291 92 11.822384834289551 93 11.505325317382813
		 94 10.884494781494141 95 10.252889633178711 96 9.8981552124023437 97 9.7703914642333984
		 98 9.6287593841552734 99 9.4753608703613281 100 9.3117055892944336 101 9.2045631408691406
		 102 9.1701717376708984 103 9.131011962890625 104 9.0018234252929687 105 8.7939357757568359
		 106 8.5685520172119141 107 8.317936897277832 108 8.0449113845825195 109 7.9537215232849121
		 110 8.0912389755249023 111 8.2147808074951172 112 8.0515518188476562 113 7.4535059928894034
		 114 6.6021857261657715 115 5.7032828330993652 116 4.9512133598327637 117 4.4022970199584961
		 118 3.9605333805084229 119 3.5784375667572021 120 3.2056405544281006;
createNode animCurveTU -n "Character1_Spine1_visibility";
	rename -uid "A8405B02-4F37-0F8E-321A-44A063859066";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftShoulder_translateX";
	rename -uid "1A030C3D-4452-A985-E547-F5BD43642370";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.49134865403175354;
createNode animCurveTL -n "Character1_LeftShoulder_translateY";
	rename -uid "9D761E65-446E-3692-2549-DEA89FA1EDC2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.20281982421875;
createNode animCurveTL -n "Character1_LeftShoulder_translateZ";
	rename -uid "BE791E88-4575-0997-895C-36A445F56103";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.910428047180176;
createNode animCurveTU -n "Character1_LeftShoulder_scaleX";
	rename -uid "EE531810-4E1F-10C5-6428-509265E298DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftShoulder_scaleY";
	rename -uid "03FC45DE-48F3-1029-9C89-F4A462BB9DEF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999934434890747;
createNode animCurveTU -n "Character1_LeftShoulder_scaleZ";
	rename -uid "FFC280E1-4FB1-859C-372E-A0BCF191A47F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftShoulder_rotateX";
	rename -uid "AFA5DC12-4D4E-ABFD-AF1B-988675E3DF76";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -14.023898124694824 1 -13.706597328186035
		 2 -13.329599380493164 3 -13.07301139831543 4 -13.116371154785156 5 -13.776860237121582
		 6 -14.872164726257322 7 -15.828267097473146 8 -16.063676834106445 9 -15.024242401123049
		 10 -13.16245174407959 11 -11.392410278320313 12 -10.605895042419434 13 -10.605895042419434
		 14 -10.605895042419434 15 -10.605895042419434 16 -10.464827537536621 17 -9.5117626190185547
		 18 -7.7234463691711417 19 -5.8592462539672852 20 -4.6401829719543457 21 -4.6540217399597168
		 22 -5.3923783302307129 23 -5.9293861389160156 24 -5.3759164810180664 25 -3.9970848560333252
		 26 -2.643118143081665 27 -1.3420876264572144 28 -0.12027257680892944 29 0.96115404367446899
		 30 1.8722602128982542 31 2.9171237945556641 32 4.1662397384643555 33 5.3011364936828613
		 34 6.095029354095459 35 6.2022366523742676 36 4.051149845123291 37 -0.095009751617908478
		 38 -3.8325035572052002 39 -6.3781795501708984 40 -8.6310205459594727 41 -9.9924468994140625
		 42 -9.8593645095825195 43 -8.6390619277954102 44 -7.2496500015258798 45 -6.6142745018005371
		 46 -6.6076350212097168 47 -6.5015473365783691 48 -6.3092050552368164 49 -6.0439071655273437
		 50 -5.1632442474365234 51 -3.6221647262573242 52 -2.1612491607666016 53 -1.4900021553039551
		 54 -2.204920768737793 55 -3.7983891963958745 56 -5.311884880065918 57 -5.7768449783325195
		 58 -5.4185237884521484 59 -5.1152362823486328 60 -4.8793282508850098 61 -4.6986656188964844
		 62 -4.6628732681274414 63 -4.7377185821533203 64 -4.7061448097229004 65 -4.4098424911499023
		 66 -3.983039379119873 67 -3.6462781429290776 68 -3.6169967651367187 69 -4.1947455406188965
		 70 -5.1746177673339844 71 -6.0137147903442383 72 -6.1662306785583496 73 -5.1226897239685059
		 74 -3.316251277923584 75 -1.5921574831008911 76 -0.74490493535995483 77 -0.57716852426528931
		 78 -0.38217774033546448 79 -0.16907881200313568 80 0.05240151658654213 81 0.73324209451675415
		 82 1.8976732492446899 83 2.9304654598236084 84 3.2562770843505859 85 2.404876708984375
		 86 0.85784280300140381 87 -0.61782574653625488 88 -1.3799021244049072 89 -1.7336535453796387
		 90 -2.4684453010559082 91 -3.5082144737243652 92 -4.6851644515991211 93 -5.5789499282836914
		 94 -6.0436038970947266 95 -6.4943332672119141 96 -7.4051737785339347 97 -8.7089471817016602
		 98 -9.9717378616333008 99 -11.171333312988281 100 -12.286775588989258 101 -12.709061622619629
		 102 -12.297624588012695 103 -11.799787521362305 104 -12.023408889770508 105 -13.70482063293457
		 106 -16.299049377441406 107 -18.673337936401367 108 -19.629606246948242 109 -19.413080215454102
		 110 -19.081293106079102 111 -18.653423309326172 112 -18.148530960083008 113 -17.051155090332031
		 114 -15.308164596557615 115 -13.599420547485352 116 -12.55402660369873 117 -12.412530899047852
		 118 -12.807620048522949 119 -13.439257621765137 120 -14.023898124694824;
createNode animCurveTA -n "Character1_LeftShoulder_rotateY";
	rename -uid "E4F08B0E-4022-BACD-1B1B-28A675A13F5C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 12.374356269836426 1 12.238003730773926
		 2 12.060282707214355 3 11.959939002990723 4 12.058155059814453 5 12.570677757263184
		 6 13.362424850463867 7 14.041831016540527 8 14.240365982055664 9 13.602286338806152
		 10 12.37823486328125 11 11.156737327575684 12 10.597686767578125 13 10.597686767578125
		 14 10.597686767578125 15 10.597686767578125 16 10.531692504882812 17 9.8928260803222656
		 18 8.5946016311645508 19 7.2088136672973642 20 6.3801264762878418 21 6.7057356834411621
		 22 7.7762188911437979 23 8.7533693313598633 24 8.77508544921875 25 7.9951448440551758
		 26 7.1875543594360352 27 6.370206356048584 28 5.5629730224609375 29 4.7950630187988281
		 30 4.0931034088134766 31 3.205012321472168 32 2.0618479251861572 33 0.96274334192276001
		 34 0.13045838475227356 35 -0.0357227623462677 36 2.0836668014526367 37 5.9645304679870605
		 38 9.2347373962402344 39 11.402738571166992 40 13.210853576660156 41 14.248573303222656
		 42 14.15593147277832 43 13.276150703430176 44 12.273722648620605 45 11.844898223876953
		 46 11.902956962585449 47 11.888144493103027 48 11.808498382568359 49 11.671928405761719
		 50 10.992671966552734 51 9.6874723434448242 52 8.3975763320922852 53 7.811337947845459
		 54 8.5274562835693359 55 10.061641693115234 56 11.509251594543457 57 12.00620174407959
		 58 11.753886222839355 59 11.529425621032715 60 11.341861724853516 61 11.192924499511719
		 62 11.061225891113281 63 10.924567222595215 64 10.77299690246582 65 10.525495529174805
		 66 10.207597732543945 67 9.9739809036254883 68 9.9840793609619141 69 10.50468921661377
		 70 11.360561370849609 71 12.056981086730957 72 12.112236976623535 73 11.059287071228027
		 74 9.2288837432861328 75 7.3700027465820304 76 6.2368311882019043 77 5.6576824188232422
		 78 4.9681229591369629 79 4.1968679428100586 80 3.3728938102722168 81 1.9747886657714842
		 82 -0.054979365319013596 83 -1.9732121229171751 84 -3.014573335647583 85 -2.5255253314971924
		 86 -1.136894702911377 87 0.16392455995082855 88 0.58558487892150879 89 0.51327908039093018
		 90 0.96526986360549927 91 1.8326771259307861 92 2.8692257404327393 93 3.6671879291534424
		 94 4.1539440155029297 95 4.7714157104492187 96 5.9208755493164062 97 7.4674077033996591
		 98 9.0102109909057617 99 10.516166687011719 100 11.951533317565918 101 12.868776321411133
		 102 13.189889907836914 103 13.409294128417969 104 14.01942253112793 105 15.401957511901855
		 106 17.103588104248047 107 18.460821151733398 108 18.932353973388672 109 18.724433898925781
		 110 18.387300491333008 111 17.942403793334961 112 17.411289215087891 113 16.549705505371094
		 114 15.288726806640625 115 13.953018188476563 116 12.932653427124023 117 12.433162689208984
		 118 12.278576850891113 119 12.311153411865234 120 12.374356269836426;
createNode animCurveTA -n "Character1_LeftShoulder_rotateZ";
	rename -uid "44DDC6E0-4B40-D16E-51A3-1FB936BEBF71";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -12.99474048614502 1 -12.579328536987305
		 2 -12.099201202392578 3 -11.752626419067383 4 -11.735491752624512 5 -12.398101806640625
		 6 -13.556394577026367 7 -14.57899284362793 8 -14.793331146240234 9 -13.556473731994629
		 10 -11.426516532897949 11 -9.4633255004882812 12 -8.6096677780151367 13 -8.6096677780151367
		 14 -8.6096677780151367 15 -8.6096677780151367 16 -8.4053144454956055 17 -7.197561740875245
		 18 -4.9907631874084473 19 -2.6586065292358398 20 -0.97212225198745728 21 -0.5559268593788147
		 22 -0.92297434806823742 23 -1.0876295566558838 24 0.0089909303933382034 25 2.0925872325897217
		 26 4.1637063026428223 27 6.182462215423584 28 8.110285758972168 29 9.8116216659545898
		 30 11.214377403259277 31 12.887223243713379 32 15.021631240844727 33 17.123281478881836
		 34 18.766788482666016 35 19.317911148071289 36 16.321964263916016 37 10.340871810913086
		 38 5.2864370346069336 39 2.2765889167785645 40 -0.3189617395401001 41 -1.8644391298294067
		 42 -1.7208690643310547 43 -0.39886775612831116 44 1.0672674179077148 45 1.7363293170928955
		 46 1.782808780670166 47 1.9651929140090945 48 2.2648012638092041 49 2.6627192497253418
		 50 3.7292909622192387 51 5.4850730895996094 52 7.1357760429382324 53 7.978191375732421
		 54 7.4178781509399405 55 5.8986635208129883 56 4.3763093948364258 57 3.9465997219085693
		 58 4.4056901931762695 59 4.7767853736877441 60 5.0418558120727539 61 5.246100902557373
		 62 5.197319507598877 63 4.9328398704528809 64 4.8787856101989746 65 5.2584967613220215
		 66 5.8242497444152832 67 6.2983884811401367 68 6.4077892303466797 69 5.8252091407775879
		 70 4.7528715133666992 71 3.7372982501983643 72 3.3642752170562744 73 4.1887502670288086
		 74 5.7010369300842285 75 6.9640121459960938 76 7.0782995223999023 77 6.2182192802429199
		 78 5.1659369468688965 79 3.9666235446929932 80 2.6651434898376465 81 1.8301447629928589
		 82 1.5463598966598511 83 1.1725749969482422 84 0.13102352619171143 85 -2.0769908428192139
		 86 -5.1517024040222168 87 -8.2526178359985352 88 -10.240411758422852 89 -11.155192375183105
		 90 -11.750123977661133 91 -12.142597198486328 92 -12.52806568145752 93 -12.410446166992188
		 94 -11.67587947845459 95 -10.920503616333008 96 -10.749754905700684 97 -11.079061508178711
		 98 -11.405989646911621 99 -11.72516918182373 100 -12.029441833496094 101 -11.655180931091309
		 102 -10.501750946044922 103 -9.4148111343383789 104 -9.2132644653320312 105 -10.6309814453125
		 106 -13.142238616943359 107 -15.568488121032717 108 -16.554140090942383 109 -16.313310623168945
		 110 -16.010236740112305 111 -15.657466888427734 112 -15.267454147338869 113 -14.240527153015137
		 114 -12.509613990783691 115 -10.848801612854004 116 -9.9624691009521484 117 -10.12425708770752
		 118 -10.922018051147461 119 -11.999724388122559 120 -12.99474048614502;
createNode animCurveTU -n "Character1_LeftShoulder_visibility";
	rename -uid "3569EA16-48C9-3811-0F5F-36B817DA6714";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftArm_translateX";
	rename -uid "830397C1-4429-96E4-9B7B-448C43B06C04";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.39077091217041;
createNode animCurveTL -n "Character1_LeftArm_translateY";
	rename -uid "77AA6BBD-46B5-7730-35FD-2ABE13848DFB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.8805270195007324;
createNode animCurveTL -n "Character1_LeftArm_translateZ";
	rename -uid "DB45FA82-4987-FD5C-148D-3480C29F9AC5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8116312026977539;
createNode animCurveTU -n "Character1_LeftArm_scaleX";
	rename -uid "95A80E89-4C15-E1AB-40AC-1DA1115F7850";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999964237213135;
createNode animCurveTU -n "Character1_LeftArm_scaleY";
	rename -uid "46FD8ACB-4424-AE58-E856-7696157440BF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleZ";
	rename -uid "7271419D-4A58-DD4F-BDFB-ED806256ED44";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_LeftArm_rotateX";
	rename -uid "ED550C9B-48B9-0ADE-F9C4-C0A0FC9B5642";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 33.673992156982422 1 35.726589202880859
		 2 37.970867156982422 3 40.060413360595703 4 41.641468048095703 5 42.424468994140625
		 6 42.640525817871094 7 42.696029663085938 8 43.001491546630859 9 43.667430877685547
		 10 44.343551635742188 11 44.875320434570313 12 45.233509063720703 13 45.509933471679688
		 14 45.799461364746094 15 46.096092224121094 16 46.369697570800781 17 46.904476165771484
		 18 47.578662872314453 19 47.971427917480469 20 47.987445831298828 21 47.60321044921875
		 22 46.777610778808594 23 45.677566528320313 24 44.937858581542969 25 44.618919372558594
		 26 44.262210845947266 27 43.894569396972656 28 43.546943664550781 29 42.705905914306641
		 30 41.250225067138672 31 39.7664794921875 32 38.830913543701172 33 39.020263671875
		 34 40.971885681152344 35 44.637889862060547 36 51.404220581054687 37 58.808391571044922
		 38 62.783447265624993 39 64.500381469726563 40 65.979972839355469 41 67.217758178710937
		 42 67.750747680664062 43 67.4766845703125 44 66.88873291015625 45 66.398338317871094
		 46 65.912933349609375 47 65.148468017578125 48 64.114486694335937 49 62.825309753417969
		 50 61.431739807128906 51 59.990428924560554 52 58.413337707519538 53 56.697315216064453
		 54 54.707324981689453 55 52.114677429199219 56 48.985092163085938 57 46.422328948974609
		 58 44.918853759765625 59 43.846164703369141 60 43.310207366943359 61 43.382896423339844
		 62 42.252315521240234 63 40.243671417236328 64 40.145927429199219 65 42.819797515869141
		 66 46.852413177490234 67 51.282333374023438 68 55.070041656494141 69 57.865367889404297
		 70 60.183429718017578 71 62.117568969726563 72 63.631759643554687 73 64.379081726074219
		 74 64.358482360839844 75 63.993797302246094 76 63.521846771240241 77 62.837486267089844
		 78 61.765911102294922 79 60.400814056396484 80 58.825485229492188 81 57.159378051757813
		 82 55.457721710205078 83 53.726154327392578 84 52.053009033203125 85 50.449237823486328
		 86 47.689468383789063 87 43.239101409912109 88 38.781047821044922 89 35.984500885009766
		 90 35.648643493652344 91 37.311958312988281 92 39.694263458251953 93 43.123668670654297
		 94 47.494449615478516 95 52.181678771972656 96 56.880859375 97 61.721042633056641
		 98 66.760650634765625 99 71.844970703125 100 76.792152404785156 101 81.164016723632812
		 102 84.367568969726562 103 86.5665283203125 104 88.418724060058594 105 90.667366027832031
		 106 92.940742492675781 107 94.102592468261719 108 92.615242004394531 109 88.8603515625
		 110 84.160980224609375 111 78.774711608886719 112 73.017234802246094 113 67.598220825195313
		 114 62.986946105957038 115 58.706623077392585 116 54.147006988525391 117 48.928340911865234
		 118 43.422996520996094 119 38.154644012451172 120 33.673992156982422;
createNode animCurveTA -n "Character1_LeftArm_rotateY";
	rename -uid "7A077C23-4C03-178C-78E7-B7B4763F6ABF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 56.054130554199219 1 56.523853302001953
		 2 56.985401153564453 3 57.488353729248047 4 58.095386505126953 5 59.091865539550781
		 6 60.359786987304687 7 61.358791351318359 8 61.539752960205085 9 60.255481719970696
		 10 57.931991577148438 11 55.637966156005859 12 54.461727142333984 13 54.167163848876953
		 14 53.780647277832031 15 53.289409637451172 16 52.690319061279297 17 51.343025207519531
		 18 49.161449432373047 19 47.015663146972656 20 45.796794891357422 21 46.272159576416016
		 22 47.793960571289063 23 49.137870788574219 24 49.146190643310547 25 48.120075225830078
		 26 47.099678039550781 27 46.089694976806641 28 45.091983795166016 29 43.850227355957031
		 30 42.2464599609375 31 40.082206726074219 32 37.502494812011719 33 35.231990814208984
		 34 33.877670288085937 35 33.779632568359375 36 36.344074249267578 37 41.5694580078125
		 38 46.622421264648437 39 50.304103851318359 40 53.658195495605469 41 56.045646667480469
		 42 56.777809143066406 43 56.214832305908203 44 55.292259216308594 45 54.952621459960938
		 46 55.134853363037109 47 55.165725708007813 48 55.085681915283203 49 54.936759948730469
		 50 54.003074645996094 51 52.256114959716797 52 50.730560302734375 53 50.394721984863281
		 54 52.021438598632812 55 54.840938568115234 56 57.485633850097656 57 58.726615905761719
		 58 58.908760070800781 59 59.073726654052741 60 59.185092926025391 61 59.203739166259766
		 62 58.467975616455078 63 57.034904479980469 64 56.041866302490234 65 55.841793060302734
		 66 55.8485107421875 67 55.86993408203125 68 55.809707641601562 69 55.999279022216797
		 70 56.440284729003906 71 56.626384735107422 72 56.051235198974609 73 54.139560699462891
		 74 51.368038177490234 75 48.790119171142578 76 47.392261505126953 77 46.887840270996094
		 78 46.350166320800781 79 45.78363037109375 80 45.198387145996094 81 43.880939483642578
		 82 41.802562713623047 83 39.96954345703125 84 39.331169128417969 85 40.666210174560547
		 86 43.514141082763672 87 46.639476776123047 88 48.744552612304688 89 50.102447509765625
		 90 51.510635375976562 91 52.979465484619141 92 54.600223541259766 93 55.769580841064453
		 94 56.230831146240234 95 56.557521820068359 96 57.471000671386719 97 58.885414123535156
		 98 60.158283233642585 99 61.268085479736321 100 62.207878112792969 101 62.164695739746094
		 102 61.059661865234368 103 60.005027770996101 104 60.05986022949218 105 62.103481292724602
		 106 65.423919677734375 107 68.626419067382812 108 70.209709167480469 109 70.3883056640625
		 110 70.393295288085937 111 70.166938781738281 112 69.668174743652344 113 68.244705200195313
		 114 65.832283020019531 115 63.228557586669915 116 61.130977630615241 117 59.721237182617188
		 118 58.551280975341797 119 57.368991851806641 120 56.054130554199219;
createNode animCurveTA -n "Character1_LeftArm_rotateZ";
	rename -uid "8D6E9AB8-411A-949A-C024-4A988BE4C5D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -44.005985260009766 1 -42.690639495849609
		 2 -41.288364410400391 3 -39.956020355224609 4 -38.866016387939453 5 -38.169235229492188
		 6 -37.757534027099609 7 -37.419811248779297 8 -36.916706085205078 9 -36.135032653808594
		 10 -35.358932495117188 11 -34.780586242675781 12 -34.391746520996094 13 -34.030792236328125
		 14 -33.573581695556641 15 -33.013256072998047 16 -32.375511169433594 17 -30.888473510742191
		 18 -28.585155487060547 19 -26.512578964233398 20 -25.433977127075195 21 -25.961362838745117
		 22 -27.668096542358398 23 -29.49901008605957 24 -29.937173843383793 25 -29.108034133911133
		 26 -28.266487121582031 27 -27.403884887695312 28 -26.510494232177734 29 -25.906341552734375
		 30 -25.654827117919922 31 -24.959911346435547 32 -23.394756317138672 33 -21.171527862548828
		 34 -18.458145141601563 35 -15.68837833404541 36 -13.205657005310059 37 -10.991344451904297
		 38 -9.0262050628662109 39 -6.9840531349182129 40 -4.8128671646118164 41 -2.869539737701416
		 42 -1.7996690273284912 43 -1.6771416664123535 44 -1.9852113723754885 45 -2.1848909854888916
		 46 -2.3048274517059326 47 -2.6746158599853516 48 -3.2629513740539551 49 -4.0355491638183594
		 50 -4.4064035415649414 51 -4.2223653793334961 52 -4.1102890968322754 53 -4.6882839202880859
		 54 -6.6607413291931152 55 -9.9815244674682617 56 -13.808223724365234 57 -16.252603530883789
		 58 -16.996686935424805 59 -17.38630485534668 60 -17.331907272338867 61 -16.716701507568359
		 62 -16.669279098510742 63 -16.9544677734375 64 -15.886167526245115 65 -13.063875198364258
		 66 -9.3412179946899414 67 -5.2733592987060547 68 -1.5229790210723877 69 1.7295705080032349
		 70 4.7562470436096191 71 7.394789218902587 72 9.3617115020751953 73 10.213775634765625
		 74 10.03466796875 75 9.3330354690551758 76 8.4345006942749023 77 7.2086172103881836
		 78 5.4282317161560059 79 3.2358207702636719 80 0.76618248224258423 81 -1.3315463066101074
		 82 -2.836735725402832 83 -4.2965025901794434 84 -6.2661252021789551 85 -9.327204704284668
		 86 -14.024558067321777 87 -20.056364059448242 88 -25.151470184326172 89 -28.025014877319336
		 90 -28.750310897827148 91 -27.619539260864258 92 -25.769830703735352 93 -22.484216690063477
		 94 -17.82850456237793 95 -12.877243041992188 96 -8.3087215423583984 97 -3.8635811805725098
		 98 0.85472440719604492 99 5.6858887672424316 100 10.434910774230957 101 14.972159385681151
		 102 18.689105987548828 103 21.299774169921875 104 23.048810958862305 105 24.348840713500977
		 106 25.132423400878906 107 24.777639389038086 108 22.223672866821289 109 17.76158332824707
		 110 12.292312622070313 111 6.0877423286437988 112 -0.52219080924987793 113 -6.6030945777893066
		 114 -11.551219940185547 115 -16.018903732299805 116 -20.872539520263672 117 -26.626733779907227
		 118 -32.804931640625 119 -38.813491821289063 120 -44.005985260009766;
createNode animCurveTU -n "Character1_LeftArm_visibility";
	rename -uid "F6EEDDEC-4608-5EF3-F8D1-F3857E39560F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftForeArm_translateX";
	rename -uid "4FD53380-4798-7592-73C6-278C8A40EA6F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.9387187957763672;
createNode animCurveTL -n "Character1_LeftForeArm_translateY";
	rename -uid "5899D54D-4659-3C30-7BEE-D98E8334B44E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -14.273021697998047;
createNode animCurveTL -n "Character1_LeftForeArm_translateZ";
	rename -uid "8F617B91-4D28-88F8-407B-BB8A99F881D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.7470760345458984;
createNode animCurveTU -n "Character1_LeftForeArm_scaleX";
	rename -uid "E879FA33-4AEE-8D62-94BD-E1B2FE3082AF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftForeArm_scaleY";
	rename -uid "A9FA919B-43B9-B067-927E-62ACD9A3C966";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftForeArm_scaleZ";
	rename -uid "EBE51936-4626-A3CF-8972-6087B4237CDD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftForeArm_rotateX";
	rename -uid "9CA803CB-4B99-31CB-3FF7-C0B303DF7BAC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 120 ".ktv[0:119]"  0 -15.058594703674316 1 -15.77955913543701
		 2 -16.548816680908203 3 -17.128692626953125 4 -17.357074737548828 5 -16.949607849121094
		 6 -15.962765693664549 7 -14.927917480468752 8 -14.659980773925781 9 -15.981952667236328
		 10 -18.260274887084961 11 -20.210361480712891 12 -20.695884704589844 13 -19.676481246948242
		 14 -18.245969772338867 15 -17.060846328735352 16 -16.637928009033203 17 -17.542217254638672
		 18 -19.405590057373047 19 -21.299453735351563 20 -22.344734191894531 21 -22.380521774291992
		 22 -21.998254776000977 23 -21.470170974731445 24 -21.003269195556641 25 -20.599239349365234
		 26 -20.202302932739258 27 -19.824794769287109 28 -19.480012893676758 29 -18.374488830566406
		 30 -16.066692352294922 31 -13.237361907958984 32 -10.722901344299316 33 -9.3939743041992187
		 34 -9.935826301574707 35 -11.695550918579102 36 -14.155115127563477 37 -16.197357177734375
		 38 -16.299037933349609 39 -14.474894523620605 40 -12.054197311401367 41 -10.055031776428223
		 42 -9.6090049743652344 43 -10.357479095458984 44 -11.239975929260254 45 -11.216212272644043
		 46 -10.201556205749512 47 -9.1139850616455078 48 -8.3302021026611328 49 -8.0576038360595703
		 50 -8.5233612060546875 51 -9.585209846496582 52 -10.854996681213379 53 -11.923040390014648
		 54 -12.878787040710449 55 -13.92530632019043 56 -14.769156455993652 57 -15.106003761291506
		 58 -15.091261863708496 59 -15.067380905151367 60 -15.032675743103026 61 -14.983970642089844
		 62 -12.711535453796387 63 -8.2871284484863281 64 -5.4143233299255371 65 -5.5680665969848633
		 66 -6.7962827682495117 67 -8.0145082473754883 68 -8.4714374542236328 69 -8.0004949569702148
		 70 -7.1905131340026847 71 -6.6300950050354004 72 -6.9296541213989258 73 -8.6230640411376953
		 74 -11.069982528686523 75 -13.140238761901855 76 -14.087381362915039 77 -13.838562965393066
		 78 -13.204093933105469 79 -12.971872329711914 80 -13.859865188598633 81 -16.741411209106445
		 82 -21.032936096191406 83 -25.105035781860352 84 -27.333850860595703 85 -27.136457443237305
		 86 -24.585206985473633 87 -21.155616760253906 88 -18.581178665161133 89 -16.794973373413086
		 90 -15.998759269714355 91 -15.92096519470215 92 -15.982781410217283 93 -15.30243492126465
		 94 -13.730566024780273 95 -12.35719108581543 96 -12.232974052429199 97 -12.733542442321777
		 98 -12.835075378417969 99 -12.949036598205566 100 -13.532064437866211 101 -15.170790672302246
		 102 -17.767364501953125 103 -20.597648620605469 104 -22.664979934692383 105 -23.47265625
		 106 -23.516149520874023 107 -23.216026306152344 108 -23.001083374023438 109 -23.001083374023438
		 111 -23.001083374023438 112 -23.001083374023438 113 -22.649911880493164 114 -21.741949081420898
		 115 -20.523288726806641 116 -19.331605911254883 117 -18.267576217651367 118 -17.18766975402832
		 119 -16.11851692199707 120 -15.058594703674316;
createNode animCurveTA -n "Character1_LeftForeArm_rotateY";
	rename -uid "D92D4DE4-4903-9CD9-8CAC-61865FC35E35";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 119 ".ktv[0:118]"  0 -19.900390625 1 -19.671535491943359 2 -19.584400177001953
		 3 -19.586795806884766 4 -19.546792984008789 5 -19.131425857543945 6 -18.556549072265625
		 7 -18.403949737548828 8 -18.992082595825195 9 -20.839252471923828 10 -23.575910568237305
		 11 -26.045219421386719 12 -26.980897903442383 13 -26.332801818847656 14 -25.211904525756836
		 15 -24.063968658447266 16 -23.385152816772461 17 -23.456361770629883 18 -23.808603286743164
		 19 -23.925090789794922 20 -23.491458892822266 21 -22.449886322021484 22 -21.105880737304688
		 23 -19.708169937133789 24 -18.404581069946289 25 -17.219533920288086 26 -16.09553337097168
		 27 -15.060683250427246 28 -14.142632484436035 29 -13.094389915466309 30 -11.903107643127441
		 31 -10.943259239196777 32 -10.403497695922852 33 -10.262105941772461 34 -10.36997127532959
		 35 -10.861079216003418 36 -13.328821182250977 37 -17.020973205566406 38 -18.497386932373047
		 39 -17.656822204589844 40 -16.814096450805664 41 -16.359226226806641 42 -16.759395599365234
		 43 -17.762672424316406 44 -18.502958297729492 45 -18.139238357543945 46 -16.442085266113281
		 47 -14.191607475280762 48 -12.070568084716797 49 -10.814529418945313 50 -10.926515579223633
		 51 -11.838784217834473 52 -12.715071678161621 53 -12.830937385559082 54 -11.854841232299805
		 55 -10.451291084289551 56 -9.3029537200927734 57 -8.7897863388061523 58 -8.7854862213134766
		 59 -8.9934167861938477 60 -9.436065673828125 61 -10.182089805603027 62 -10.742092132568359
		 63 -11.365921020507813 64 -12.40434455871582 65 -13.412620544433594 66 -14.452454566955566
		 67 -15.743010520935059 68 -17.273679733276367 69 -18.803934097290039 70 -20.389972686767578
		 71 -22.274581909179688 72 -24.498043060302734 73 -27.504150390625 74 -31.068733215332028
		 75 -34.130638122558594 76 -35.617164611816406 77 -35.498195648193359 78 -34.837593078613281
		 79 -34.177608489990234 80 -34.0673828125 81 -34.693874359130859 82 -35.281669616699219
		 83 -35.212497711181641 84 -34.413028717041016 85 -33.104389190673828 86 -29.883750915527344
		 87 -23.960254669189453 88 -17.10057258605957 89 -11.419257164001465 90 -8.770960807800293
		 91 -8.5536375045776367 92 -8.809321403503418 93 -9.5438623428344727 94 -10.792043685913086
		 95 -12.392544746398926 96 -13.924392700195313 97 -14.69663143157959 98 -14.838143348693848
		 99 -15.068230628967285 100 -16.083654403686523 101 -18.126091003417969 102 -20.413978576660156
		 103 -22.236124038696289 104 -23.225164413452148 105 -23.410812377929688 106 -23.292726516723633
		 107 -23.212774276733398 108 -23.187042236328125 109 -23.187042236328125 112 -23.187042236328125
		 113 -23.184085845947266 114 -23.198101043701172 115 -23.220096588134766 116 -23.076210021972656
		 117 -22.555471420288086 118 -21.741928100585938 119 -20.795854568481445 120 -19.900390625;
createNode animCurveTA -n "Character1_LeftForeArm_rotateZ";
	rename -uid "4A80BF58-48CC-C9FC-4716-F7BBD6319DDF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 120 ".ktv[0:119]"  0 -14.527209281921387 1 -17.35478401184082
		 2 -20.614866256713867 3 -23.353700637817383 4 -24.558624267578125 5 -23.118894577026367
		 6 -19.863544464111328 7 -16.614896774291992 8 -15.03520679473877 9 -15.748963356018066
		 10 -17.704042434692383 11 -20.041374206542969 12 -21.938539505004883 13 -24.128192901611328
		 14 -27.015727996826172 15 -29.283269882202148 16 -29.720884323120121 17 -27.057666778564453
		 18 -22.247594833374023 19 -17.515605926513672 20 -15.153953552246092 21 -16.850446701049805
		 22 -21.052402496337891 23 -25.18128776550293 24 -26.639501571655273 25 -25.915483474731445
		 26 -25.222019195556641 27 -24.577184677124023 28 -23.99958610534668 29 -22.465702056884766
		 30 -19.63355827331543 31 -16.925510406494141 32 -15.445616722106935 33 -15.66063976287842
		 34 -18.174938201904297 35 -22.118509292602539 36 -25.649669647216797 37 -27.489721298217773
		 38 -27.348285675048828 39 -26.824359893798828 40 -27.070667266845703 41 -29.378236770629883
		 42 -34.049545288085938 43 -39.669055938720703 44 -44.859230041503906 45 -48.233184814453125
		 46 -50.790931701660156 47 -53.7244873046875 48 -55.972843170166016 49 -56.547821044921875
		 50 -54.331932067871094 51 -50.158660888671875 52 -46.000576019287109 53 -43.910896301269531
		 54 -45.489131927490234 55 -49.447772979736328 56 -53.474575042724609 57 -55.121826171875
		 58 -54.813167572021484 59 -54.655555725097656 60 -54.686092376708984 61 -54.917049407958984
		 62 -52.682708740234375 63 -48.472461700439453 64 -46.464836120605469 65 -48.093036651611328
		 66 -51.421272277832031 67 -54.841331481933594 68 -56.496444702148437 69 -54.997810363769531
		 70 -51.530059814453125 71 -47.938407897949219 72 -45.951526641845703 73 -46.332958221435547
		 74 -47.986347198486328 75 -49.857421875 76 -50.704498291015625 77 -51.251880645751953
		 78 -52.264492034912109 79 -52.488105773925781 80 -50.712432861328125 81 -45.535560607910156
		 82 -38.000644683837891 83 -30.747438430786129 84 -26.551839828491211 85 -27.366344451904297
		 86 -31.552511215209964 87 -35.692008972167969 88 -36.737812042236328 89 -35.528587341308594
		 90 -34.544136047363281 91 -33.749050140380859 92 -32.946212768554687 93 -30.720066070556641
		 94 -26.861677169799805 95 -23.105119705200195 96 -20.991052627563477 97 -21.346548080444336
		 98 -23.069543838500977 99 -24.713535308837891 100 -24.809898376464844 101 -21.990118026733398
		 102 -17.203540802001953 103 -12.559927940368652 104 -10.392290115356445 105 -12.539591789245605
		 106 -17.43342399597168 107 -22.358787536621094 108 -24.609628677368164 109 -24.609628677368164
		 111 -24.609628677368164 112 -24.609628677368164 113 -23.351924896240234 114 -20.50169563293457
		 115 -17.415231704711914 116 -15.334531784057617 117 -14.58492946624756 118 -14.456372261047362
		 119 -14.559076309204102 120 -14.527209281921387;
createNode animCurveTU -n "Character1_LeftForeArm_visibility";
	rename -uid "960C3B97-4316-69C4-F148-51A8E8D75FD3";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHand_translateX";
	rename -uid "8789B3D7-44C6-44FB-A3A6-D190E60234DD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.95125424861907959;
createNode animCurveTL -n "Character1_LeftHand_translateY";
	rename -uid "75F192DA-4DA4-3555-56AD-3BB7CA6557B7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.299518585205078;
createNode animCurveTL -n "Character1_LeftHand_translateZ";
	rename -uid "0788AE3F-4FAF-ED5F-DFB7-5B9DE232E702";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 12.872089385986328;
createNode animCurveTU -n "Character1_LeftHand_scaleX";
	rename -uid "6D215A8F-4901-DC95-2163-43B85FFEDCBD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHand_scaleY";
	rename -uid "8BDF62B7-4361-B57B-EF73-179476CE5873";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHand_scaleZ";
	rename -uid "8BCD71D7-4EE3-60B8-F107-AFAD2E2EB522";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHand_rotateX";
	rename -uid "F9FE44C1-4607-779B-C7A5-2FBA22CD28B1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 119 ".ktv[0:118]"  0 2.6665960550786849e-009 1 -1.3789867162704468
		 2 -2.8593685626983643 3 -4.1309471130371094 4 -4.8713626861572266 5 -4.9976143836975098
		 6 -4.7034845352172852 7 -4.0685720443725586 8 -3.1241331100463867 9 -1.3503313064575195
		 10 1.1586273908615112 11 3.4104516506195068 12 4.3440642356872559 13 3.3161413669586182
		 14 0.99047172069549561 15 -1.7250062227249146 16 -3.9252812862396236 17 -5.3184390068054199
		 18 -6.3900279998779297 19 -7.4142365455627441 20 -8.6680822372436523 21 -10.607072830200195
		 22 -13.008123397827148 23 -15.080780982971193 24 -15.965618133544924 25 -15.965619087219238
		 27 -15.965620040893555 28 -15.965620040893555 29 -14.319267272949219 30 -10.34013843536377
		 31 -6.2166051864624023 32 -3.7784445285797115 33 -3.8830993175506592 34 -6.9068856239318848
		 35 -11.505868911743164 36 -16.492635726928711 37 -20.875043869018555 38 -24.248317718505859
		 39 -27.487066268920898 40 -30.720993041992188 41 -33.795967102050781 42 -36.094184875488281
		 43 -37.328441619873047 44 -37.985355377197266 45 -38.719707489013672 46 -40.489265441894531
		 47 -43.196670532226563 48 -45.984851837158203 49 -48.01470947265625 50 -49.024589538574219
		 51 -49.519603729248047 52 -49.858013153076172 53 -50.423000335693359 54 -51.765865325927734
		 55 -53.607658386230469 56 -55.095855712890625 57 -55.402229309082031 58 -54.914016723632812
		 59 -54.588260650634766 60 -54.46746826171875 61 -54.626850128173828 62 -50.780738830566406
		 63 -43.224346160888672 64 -38.720321655273438 65 -39.463180541992188 66 -42.295127868652344
		 67 -45.670066833496094 68 -48.012733459472656 69 -48.908653259277344 70 -49.046047210693359
		 71 -48.547416687011719 72 -47.609298706054687 73 -45.822040557861328 74 -42.9947509765625
		 75 -39.794158935546875 76 -36.698326110839844 77 -34.161914825439453 78 -31.883054733276364
		 79 -29.264556884765625 80 -25.733749389648438 81 -21.318826675415039 82 -16.820589065551758
		 83 -12.85997486114502 84 -10.03579044342041 85 -9.0904302597045898 86 -11.195976257324219
		 87 -15.750256538391113 88 -20.338235855102539 89 -23.731590270996094 90 -25.297800064086914
		 91 -25.323585510253906 92 -25.134439468383789 93 -21.836204528808594 94 -14.738313674926758
		 95 -7.1736974716186523 96 -2.4324214458465576 97 -1.8107808828353882 98 -3.453848123550415
		 99 -5.8849945068359375 100 -7.6235899925231934 101 -8.307586669921875 102 -8.7673845291137695
		 103 -9.3146457672119141 104 -10.263269424438477 105 -12.104986190795898 106 -14.52446174621582
		 107 -16.613481521606445 108 -17.505128860473633 109 -17.505130767822266 111 -17.505130767822266
		 112 -17.505130767822266 113 -14.715815544128418 114 -8.3633766174316406 115 -1.4796391725540161
		 116 2.9198446273803711 117 3.8098573684692378 118 2.8897311687469482 119 1.2602958679199219
		 120 3.3148821465545097e-009;
createNode animCurveTA -n "Character1_LeftHand_rotateY";
	rename -uid "D32BD0EC-46A7-9526-75C7-55A5616D22F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 119 ".ktv[0:118]"  0 6.0393405874492607e-005 1 0.23395651578903195
		 2 0.48594880104064941 3 0.62903261184692383 4 0.5687897801399231 5 0.16180895268917084
		 6 -0.50534921884536743 7 -1.1901296377182007 8 -1.6084306240081787 9 -1.5519500970840454
		 10 -0.99642652273178089 11 -0.078225366771221161 12 0.68933498859405518 13 0.98567467927932739
		 14 1.0302901268005371 15 0.97828471660614014 16 0.92839050292968761 17 0.7743605375289917
		 18 0.53297591209411621 19 0.38756400346755981 20 0.49742740392684937 21 0.99739044904708862
		 22 1.6257497072219849 23 2.0660853385925293 24 2.2190773487091064 25 2.2190768718719482
		 27 2.2190742492675781 28 2.2190737724304199 29 2.1177551746368408 30 1.7461175918579102
		 31 1.2349133491516113 32 0.84402304887771606 33 0.77951723337173462 34 1.1990301609039307
		 35 1.7378851175308228 36 2.0321455001831055 37 2.172614574432373 38 2.5416371822357178
		 39 3.3775749206542969 40 4.5609087944030762 41 5.6551070213317871 42 5.9313445091247559
		 43 5.4033951759338379 44 4.6183533668518066 45 4.0876965522766113 46 4.0621957778930664
		 47 4.3782563209533691 48 4.8869247436523437 49 5.358062744140625 50 5.8471031188964844
		 51 6.4642748832702637 52 6.9748272895812988 53 7.1485567092895508 54 6.7391095161437988
		 55 5.8629746437072754 56 4.9257702827453613 57 4.4712486267089844 58 4.4524779319763184
		 59 4.4381880760192871 60 4.4225015640258789 61 4.3983192443847656 62 4.8580307960510254
		 63 5.3917837142944336 64 5.4479532241821289 65 5.4386115074157715 66 5.4079599380493164
		 67 5.3946666717529297 68 5.5979251861572266 69 6.4275307655334473 70 7.6902198791503897
		 71 8.7047309875488281 72 8.803959846496582 73 7.4896550178527832 74 5.4274563789367676
		 75 3.6156103610992432 76 2.6556053161621094 77 2.5316402912139893 78 2.7926571369171143
		 79 3.1592526435852051 80 3.382735013961792 81 3.36387038230896 82 3.0640268325805664
		 83 2.5534176826477051 84 2.1176843643188477 85 2.1493000984191895 86 2.5987577438354492
		 87 2.9434943199157715 88 2.932856559753418 89 2.8255267143249512 90 2.7709548473358154
		 91 2.7456855773925781 92 2.7269902229309082 93 2.5777544975280762 94 2.1947786808013916
		 95 1.6495153903961182 96 1.2060686349868774 97 1.1210662126541138 98 1.2533608675003052
		 99 1.4318552017211914 100 1.504663348197937 101 1.3738778829574585 102 1.1529284715652466
		 103 1.0067960023880005 104 1.0962038040161133 105 1.5589431524276733 106 2.136791467666626
		 107 2.5271358489990234 108 2.6574370861053467 109 2.6574366092681885 111 2.6574339866638184
		 112 2.6574335098266602 113 2.5277862548828125 114 2.1799812316894531 115 1.6552780866622925
		 116 1.1087733507156372 117 0.69838941097259521 118 0.39085164666175842 119 0.18474836647510529
		 120 6.0393402236513787e-005;
createNode animCurveTA -n "Character1_LeftHand_rotateZ";
	rename -uid "5B353409-4AA0-B390-607D-BD9584E3EBF9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 119 ".ktv[0:118]"  0 -0.00016078895714599639 1 -0.24318999052047729
		 2 -0.59284985065460205 3 -0.70209312438964844 4 -0.22025339305400848 5 1.5005905628204346
		 6 4.0790424346923828 7 6.3184685707092285 8 7.0320730209350586 9 5.3232254981994629
		 10 2.0793397426605225 11 -1.2466187477111816 12 -3.3421475887298584 13 -3.8524632453918461
		 14 -3.5154345035552979 15 -2.7377810478210449 16 -1.9544451236724851 17 -0.97663593292236328
		 18 0.33587640523910522 19 1.4406615495681763 20 1.791369557380676 21 0.80556130409240723
		 22 -1.1070092916488647 23 -2.9810025691986084 24 -3.825987815856934 25 -3.825987815856934
		 27 -3.825987815856934 28 -3.825987815856934 29 -3.3737900257110596 30 -2.2491891384124756
		 31 -1.1592069864273071 32 -0.6046978235244751 33 -0.64083957672119141 34 -1.3405318260192871
		 35 -2.1372318267822266 36 -1.8345432281494141 37 -0.02940564788877964 38 3.0408108234405518
		 39 6.6818256378173828 40 10.109306335449219 41 12.044341087341309 42 11.600178718566895
		 43 9.5353031158447266 44 6.9997220039367676 45 5.1041555404663086 46 4.3501930236816406
		 47 4.2434916496276855 48 4.4030508995056152 49 4.4639244079589844 50 4.6522092819213867
		 51 5.0866045951843262 52 5.2702579498291016 53 4.7049336433410645 54 2.8678169250488281
		 55 0.24675922095775604 56 -2.2041037082672119 57 -3.6380062103271484 58 -4.2506232261657715
		 59 -4.7699260711669922 60 -5.1763663291931152 61 -5.4849448204040527 62 -4.5935220718383789
		 63 -2.1851441860198975 64 -0.30437323451042175 65 -0.24829502403736117 66 -0.93787354230880737
		 67 -1.5968198776245117 68 -1.5254323482513428 69 -0.1275857537984848 70 2.0560612678527832
		 71 3.9971890449523926 72 4.6480312347412109 73 3.2135546207427979 74 0.29867053031921387
		 75 -2.9996016025543213 76 -5.4324355125427246 77 -6.4712233543395996 78 -6.6624822616577148
		 79 -6.2924928665161133 80 -5.7335939407348633 81 -4.7863707542419434 82 -3.3680727481842041
		 83 -2.105553150177002 84 -1.5882217884063721 85 -2.3882596492767334 86 -4.0517234802246094
		 87 -5.6537566184997559 88 -6.2036824226379395 89 -5.8943533897399902 90 -5.6914815902709961
		 91 -5.5991902351379395 92 -5.5224266052246094 93 -5.436974048614502 94 -5.2408347129821777
		 95 -4.9080839157104492 96 -4.5702900886535645 97 -4.3374538421630859 98 -4.1253232955932617
		 99 -3.8095765113830566 100 -3.2964489459991455 101 -2.312380313873291 102 -0.9933430552482605
		 103 0.11154498904943466 104 0.455266684293747 105 -0.54471904039382935 106 -2.4736568927764893
		 107 -4.3592910766601562 108 -5.2083520889282227 109 -5.2083520889282227 111 -5.2083520889282227
		 112 -5.2083520889282227 113 -5.2814197540283203 114 -5.3134074211120605 115 -5.1003079414367676
		 116 -4.5865044593811035 117 -3.7202236652374272 118 -2.5597772598266602 119 -1.2577875852584839
		 120 -0.00016078943735919893;
createNode animCurveTU -n "Character1_LeftHand_visibility";
	rename -uid "B2A5B4C4-49AA-DB3F-3145-5782C44EBD47";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateX";
	rename -uid "00D9604A-455F-B87E-3837-83BB7BDE947F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.1041760444641113;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateY";
	rename -uid "06EBD199-487B-15CD-A501-6693123BD7BC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.7491040229797363;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateZ";
	rename -uid "3BD5F02F-46D5-8A05-8593-7290A86F358C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.7276492118835449;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleX";
	rename -uid "C01DBCF9-4B90-C989-1252-D5B8F119DF21";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleY";
	rename -uid "97694797-450E-7840-0441-C389B50265A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleZ";
	rename -uid "E5783393-4518-8297-0C5D-26A09794EDDD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateX";
	rename -uid "C957F8A1-4506-6A2D-97DF-858C93C80522";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 7.3663825988769522 1 7.8447823524475089
		 2 8.3296327590942383 3 8.7971086502075195 4 9.2236270904541016 5 9.7982559204101562
		 6 10.46800422668457 7 10.84896183013916 8 10.558210372924805 9 9.0705499649047852
		 10 6.7074723243713379 11 4.3660163879394531 12 3.0097115039825439 13 3.1248350143432617
		 14 4.1361570358276367 15 5.4683694839477539 16 6.5695343017578125 17 7.4565763473510733
		 18 8.3701381683349609 19 9.1159524917602539 20 9.5022678375244141 21 9.4308614730834961
		 22 9.0214738845825195 23 8.4086265563964844 24 7.7272067070007324 25 6.8183584213256836
		 26 5.6673917770385742 27 4.6345481872558594 28 4.094123363494873 29 4.1883559226989746
		 30 4.6635746955871582 31 5.306643009185791 32 5.9082064628601074 33 6.6739730834960938
		 34 7.8077883720397949 35 9.2937965393066406 36 11.138010025024414 37 13.344700813293457
		 38 15.835777282714844 39 18.354162216186523 40 20.649208068847656 41 22.487813949584961
		 42 23.570343017578125 43 23.927722930908203 44 23.836875915527344 45 23.924928665161133
		 46 24.842733383178711 47 26.329448699951172 48 27.871004104614258 49 28.938137054443359
		 50 29.405454635620117 51 29.542476654052734 52 29.398534774780273 53 29.025177001953121
		 54 28.188156127929688 55 26.952136993408203 56 25.806118011474609 57 25.226728439331055
		 58 25.499813079833984 59 26.316314697265625 60 27.257890701293945 61 27.899713516235352
		 62 27.987331390380859 63 27.785364151000977 64 27.641395568847656 65 27.672000885009766
		 66 27.744318008422852 67 27.830711364746094 68 27.903484344482422 69 27.962089538574219
		 70 28.019012451171875 71 28.066055297851563 72 28.095010757446289 73 28.085212707519531
		 74 28.047191619873047 75 28.019521713256836 76 27.825651168823242 77 27.319190979003906
		 78 26.514728546142578 79 25.394634246826172 80 23.944686889648438 81 22.272579193115234
		 82 20.44511604309082 83 18.405750274658203 84 16.114755630493164 85 13.480064392089844
		 86 10.716039657592773 87 8.2348928451538086 88 6.4399266242980957 89 5.4592499732971191
		 90 5.2008037567138672 91 5.449732780456543 92 5.9406156539916992 93 6.9523124694824219
		 94 8.3797178268432617 95 9.6616678237915039 96 10.251957893371582 97 9.7983169555664062
		 98 8.6619701385498047 99 7.368553638458252 100 6.4554686546325684 101 6.0180892944335937
		 102 5.7897500991821289 103 5.7771353721618652 104 5.985710620880127 105 6.5845847129821777
		 106 7.5094151496887198 107 8.4204826354980469 108 8.9861164093017578 109 9.0988397598266602
		 110 8.9447660446166992 111 8.6348552703857422 112 8.2801179885864258 113 7.7950358390808105
		 114 7.1566500663757324 115 6.5723810195922852 116 6.2532534599304199 117 6.3105177879333496
		 118 6.6142430305480957 119 7.0154147148132324 120 7.3663825988769522;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateY";
	rename -uid "125840DF-4E5B-21E9-95D1-1987CC990B01";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 11.432723045349121 1 12.198748588562012
		 2 12.979418754577637 3 13.735960960388184 4 14.429286956787109 5 15.36762523651123
		 6 16.466793060302734 7 17.094337463378906 8 16.615245819091797 9 14.180130004882813
		 10 10.385064125061035 11 6.740354061126709 12 4.6917715072631836 13 4.8637313842773437
		 14 6.3897485733032227 15 8.4402055740356445 16 10.166882514953613 17 11.57680606842041
		 18 13.04482364654541 19 14.25399112701416 20 14.883708000183107 21 14.767148017883301
		 22 14.100324630737305 23 13.106998443603516 24 12.010079383850098 25 10.560746192932129
		 26 8.7502355575561523 27 7.1516585350036621 28 6.3257842063903809 29 6.4692502021789551
		 30 7.1962275505065927 31 8.18896484375 32 9.1266012191772461 33 9.7512845993041992
		 34 9.9038076400756836 35 9.6950531005859375 36 9.2805757522583008 37 8.8463230133056641
		 38 8.4891214370727539 39 8.0632095336914062 40 7.4591045379638663 41 6.6051478385925293
		 42 5.3248815536499023 43 3.9300420284271236 44 3.0913715362548828 45 3.2250826358795166
		 46 4.6168251037597656 47 6.8597931861877441 48 9.1629066467285156 49 10.739235877990723
		 50 11.424077987670898 51 11.624200820922852 52 11.413961410522461 53 10.867057800292969
		 54 9.6330986022949219 55 7.7933607101440439 56 6.0722746849060059 57 5.1977396011352539
		 58 5.6102542877197266 59 6.8400611877441406 60 8.250213623046875 61 9.2055282592773437
		 62 9.3355264663696289 63 9.0357131958007812 64 8.8216667175292969 65 8.8671894073486328
		 66 8.9747142791748047 67 9.1030759811401367 68 9.2111244201660156 69 9.2980861663818359
		 70 9.38250732421875 71 9.4522438049316406 72 9.4951515197753906 73 9.4806327819824219
		 74 9.4242830276489258 75 9.3832635879516602 76 9.5458450317382812 77 10.061737060546875
		 78 10.827101707458496 79 11.686784744262695 80 12.483628273010254 81 13.236850738525391
		 82 13.900736808776855 83 14.236326217651367 84 14.027650833129883 85 12.98444652557373
		 86 11.334447860717773 87 9.6590738296508789 88 8.5263290405273437 89 8.0564432144165039
		 90 8.0248813629150391 91 8.4112234115600586 92 9.1773529052734375 93 10.773321151733398
		 94 13.060297012329102 95 15.144166946411135 96 16.111627578735352 97 15.367725372314453
		 98 13.516885757446289 99 11.436190605163574 100 9.9867715835571289 101 9.2987747192382812
		 102 8.9413013458251953 103 8.9215869903564453 104 9.248011589050293 105 10.190670013427734
		 106 11.66129207611084 107 13.12615966796875 108 14.042854309082031 109 14.226150512695313
		 110 13.975666046142578 111 13.472966194152832 112 12.899501800537109 113 12.118892669677734
		 114 11.098296165466309 115 10.171382904052734 116 9.6681613922119141 117 9.758296012878418
		 118 10.237556457519531 119 10.873586654663086 120 11.432723045349121;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateZ";
	rename -uid "89B70327-41EA-C05A-2AE8-85B482C51902";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 119 ".ktv[0:118]"  0 36.344264984130859 1 36.127742767333984
		 2 35.913658142089844 3 35.712455749511719 4 35.533435821533203 5 35.29925537109375
		 6 35.036689758300781 7 34.892433166503906 8 35.002193450927734 9 35.597179412841797
		 10 36.65087890625 11 37.815174102783203 12 38.539459228515625 13 38.476642608642578
		 14 37.935451507568359 15 37.252918243408203 16 36.716278076171875 17 36.303047180175781
		 18 35.896018981933594 19 35.578216552734375 20 35.418865203857422 21 35.448047637939453
		 22 35.61773681640625 23 35.879295349121094 24 36.180473327636719 25 36.598609924316406
		 26 37.154037475585938 27 37.675971984863281 28 37.957557678222656 29 37.908046722412109
		 30 37.661006927490234 31 37.333869934082031 32 37.035495758056641 33 37.210433959960938
		 34 38.171756744384766 35 39.707309722900391 36 41.589305877685547 37 43.611831665039063
		 38 45.631092071533203 39 47.563804626464844 40 49.316787719726563 41 50.793540954589844
		 42 51.905132293701172 43 52.588832855224609 44 52.820381164550781 45 52.822273254394531
		 46 52.854251861572266 47 52.953273773193359 48 53.117103576660156 49 53.266513824462891
		 50 53.341106414794922 51 53.364028930664063 52 53.339962005615234 53 53.279987335205078
		 54 53.158451080322266 55 53.011997222900391 56 52.911777496337891 57 52.874244689941406
		 58 52.890830993652344 59 52.952144622802734 60 53.044536590576172 61 53.120738983154297
		 62 53.131969451904297 63 53.106380462646484 64 53.088787078857422 65 53.092479705810547
		 66 53.101310729980469 67 53.112033843994141 68 53.1212158203125 69 53.128711700439453
		 70 53.136074066162109 71 53.142223358154297 74 53.139751434326172 75 53.136142730712891
		 76 52.960971832275391 77 52.463993072509766 78 51.666934967041016 79 50.586097717285156
		 80 49.247905731201172 81 47.696144104003906 82 45.981136322021484 83 44.182113647460937
		 84 42.4195556640625 85 40.871952056884766 86 39.664634704589844 87 38.782558441162109
		 88 38.107879638671875 89 37.60772705078125 90 37.387134552001953 91 37.262218475341797
		 92 37.019634246826172 93 36.535823822021484 94 35.891853332519531 95 35.354179382324219
		 96 35.120147705078125 97 35.299228668212891 98 35.770088195800781 99 36.343269348144531
		 100 36.770668029785156 101 36.9818115234375 102 37.093654632568359 103 37.099864959716797
		 104 36.997604370117188 105 36.709121704101562 106 36.278984069824219 107 35.874153137207031
		 108 35.632583618164062 109 35.585357666015625 110 35.649986267089844 111 35.781703948974609
		 112 35.935268402099609 113 36.150012969970703 114 36.440814971923828 115 36.714923858642578
		 116 36.867778778076172 117 36.840187072753906 118 36.695034027099609 119 36.506385803222656
		 120 36.344264984130859;
createNode animCurveTU -n "Character1_LeftHandThumb1_visibility";
	rename -uid "37A5E954-47CC-5EE3-1E06-15A4DC4343F7";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateX";
	rename -uid "DB5D0295-4F78-8F1C-775D-AD97A430FF0F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.3594832420349121;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateY";
	rename -uid "3606AC28-4D8B-9B3A-4141-8393E1D87CEE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.8193249702453613;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateZ";
	rename -uid "60F9481E-4852-690A-B138-24AA5CBB2CD6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3286550045013428;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleX";
	rename -uid "16E0BB57-4206-F0AE-A3E2-9E97D92552B2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleY";
	rename -uid "C655F26B-4B31-6584-2018-00B7E77C32E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleZ";
	rename -uid "DE60D3CC-4433-7CF7-8641-0D859DCDBB88";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateX";
	rename -uid "CC27A6FD-4504-5255-6DEC-3FA4E3ABE87B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 22.477453231811523 1 23.442905426025391
		 2 24.424890518188477 3 25.374717712402344 4 26.243648529052734 5 27.417373657226563
		 6 28.789028167724606 7 29.570606231689457 8 28.974016189575195 9 25.931550979614258
		 10 21.153938293457031 11 16.519668579101562 12 13.892660140991211 13 14.113837242126465
		 14 16.071264266967773 15 18.687028884887695 16 20.877845764160156 17 22.65919303894043
		 18 24.507074356079102 19 26.024089813232422 20 26.812385559082031 21 26.666563034057617
		 22 25.831548690795898 23 24.585189819335938 24 23.205293655395508 25 21.376134872436523
		 26 19.081172943115234 27 17.045093536376953 28 15.98940372467041 29 16.172983169555664
		 30 17.101987838745117 31 18.367364883422852 32 19.559186935424805 33 20.266506195068359
		 34 20.264839172363281 35 19.823278427124023 36 19.270881652832031 37 18.936460494995117
		 38 18.978977203369141 39 19.199289321899414 40 19.38880729675293 41 19.338884353637695
		 42 18.682846069335938 43 17.636133193969727 44 16.862438201904297 45 17.024387359619141
		 46 18.711288452148437 47 21.435125350952148 48 24.239572525024414 49 26.164024353027344
		 50 27.001489639282227 51 27.246376037597656 52 26.989110946655273 53 26.32026481628418
		 54 24.813154220581055 55 22.5709228515625 56 20.477994918823242 57 19.416103363037109
		 58 19.916866302490234 59 21.4111328125 60 23.127220153808594 61 24.291549682617188
		 62 24.450107574462891 63 24.08447265625 64 23.823524475097656 65 23.879016876220703
		 66 24.010099411010742 67 24.166610717773438 68 24.298376083374023 69 24.404438018798828
		 70 24.507413864135742 71 24.592485427856445 72 24.644834518432617 73 24.627120971679688
		 74 24.558376312255859 75 24.508337020874023 76 24.54673957824707 77 24.737764358520508
		 78 25.029045104980469 79 25.315469741821289 80 25.491897583007813 81 25.662282943725586
		 82 25.83869743347168 83 25.778142929077148 84 25.237665176391602 85 23.848117828369141
		 86 21.841587066650391 87 19.876373291015625 88 18.608772277832031 89 18.145545959472656
		 90 18.158472061157227 91 18.650163650512695 92 19.623607635498047 93 21.64484977722168
		 94 24.526515960693359 95 27.138092041015625 96 28.34619140625 97 27.417497634887695
		 98 25.099851608276367 99 22.481826782226562 100 20.649803161621094 101 19.777694702148438
		 102 19.323904037475586 103 19.298866271972656 104 19.713281631469727 105 20.907953262329102
		 106 22.765726089477539 107 24.609256744384766 108 25.759517669677734 109 25.98921012878418
		 110 25.675296783447266 111 25.044729232788086 112 24.324451446533203 113 23.34234619140625
		 114 22.055362701416016 115 20.883539199829102 116 20.246135711669922 117 20.360366821289063
		 118 20.967292785644531 119 21.77154541015625 120 22.477453231811523;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateY";
	rename -uid "9A830AF7-4E4A-0BF7-007E-5B8E84837537";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 115 ".ktv[0:114]"  0 -11.664257049560547 1 -11.703047752380371
		 2 -11.737519264221191 3 -11.76607608795166 4 -11.78807258605957 5 -11.811520576477051
		 6 -11.829792022705078 7 -11.835801124572754 8 -11.831502914428711 9 -11.780625343322754
		 10 -11.603195190429688 11 -11.317804336547852 12 -11.106864929199219 13 -11.125988006591797
		 14 -11.284306526184082 15 -11.465106010437012 16 -11.589308738708496 17 -11.671930313110352
		 18 -11.740176200866699 19 -11.782886505126953 20 -11.800333976745605 21 -11.797350883483887
		 22 -11.778131484985352 23 -11.742669105529785 24 -11.693951606750488 25 -11.614082336425781
		 26 -11.489285469055176 27 -11.355739593505859 28 -11.278079986572266 29 -11.291996002197266
		 30 -11.359761238098145 31 -11.444905281066895 32 -11.517531394958496 33 -11.557153701782227
		 34 -11.557063102722168 35 -11.532628059387207 36 -11.500637054443359 37 -11.480501174926758
		 38 -11.48309326171875 39 -11.49637508392334 40 -11.507598876953125 41 -11.504660606384277
		 42 -11.464844703674316 43 -11.396710395812988 44 -11.342713356018066 45 -11.354270935058594
		 46 -11.466617584228516 47 -11.61693000793457 48 -11.731399536132812 49 -11.786221504211426
		 50 -11.804036140441895 51 -11.808552742004395 52 -11.803799629211426 53 -11.789822578430176
		 54 -11.749761581420898 55 -11.668224334716797 56 -11.568496704101563 57 -11.509200096130371
		 58 -11.537891387939453 59 -11.615774154663086 60 -11.690897941589355 61 -11.733134269714355
		 62 -11.738338470458984 63 -11.726139068603516 64 -11.717005729675293 65 -11.718977928161621
		 66 -11.723572731018066 67 -11.728940010070801 68 -11.733360290527344 69 -11.736852645874023
		 76 -11.741446495056152 77 -11.747446060180664 78 -11.756228446960449 79 -11.764431953430176
		 80 -11.769271850585937 81 -11.773792266845703 82 -11.778311729431152 83 -11.776778221130371
		 84 -11.762246131896973 85 -11.71788215637207 86 -11.636056900024414 87 -11.535619735717773
		 88 -11.460209846496582 89 -11.430577278137207 90 -11.431419372558594 91 -11.462802886962891
		 92 -11.521246910095215 93 -11.626906394958496 94 -11.740799903869629 95 -11.806593894958496
		 96 -11.824969291687012 97 -11.811522483825684 98 -11.758296012878418 99 -11.664443016052246
		 100 -11.577541351318359 101 -11.530048370361328 102 -11.503775596618652 103 -11.50229549407959
		 104 -11.526384353637695 105 -11.590842247009277 106 -11.676347732543945 107 -11.743431091308594
		 108 -11.776303291320801 109 -11.782039642333984 110 -11.774130821228027 111 -11.756688117980957
		 112 -11.734224319458008 113 -11.699234008789063 114 -11.645772933959961 115 -11.589599609375
		 116 -11.556049346923828 117 -11.562216758728027 118 -11.593851089477539 119 -11.63282299041748
		 120 -11.664257049560547;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateZ";
	rename -uid "BA0F178C-440F-0E5B-D07F-D0A1B7E37E7F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -2.6664080619812012 1 -2.9598288536071777
		 2 -3.2588565349578857 3 -3.5485754013061523 4 -3.8139734268188481 5 -4.1729006767272949
		 6 -4.5928225517272949 7 -4.8322434425354004 8 -4.6494832038879395 9 -3.7186138629913335
		 10 -2.2652254104614258 11 -0.87285363674163818 12 -0.094406008720397949 13 -0.15959148108959198
		 14 -0.73935610055923462 15 -1.5213611125946045 16 -2.1817076206207275 17 -2.7215960025787354
		 18 -3.283907413482666 19 -3.7468850612640381 20 -3.9878392219543461 21 -3.9432494640350342
		 22 -3.6880669593811035 23 -3.3077201843261719 24 -2.8875584602355957 25 -2.3324847221374512
		 26 -1.6398302316665649 27 -1.0295870304107666 28 -0.71501123905181885 29 -0.76961791515350342
		 30 -1.046578049659729 31 -1.4253937005996704 32 -1.7837114334106445 33 -1.9970036745071413
		 34 -1.996500492095947 35 -1.8632949590682986 36 -1.6969063282012939 37 -1.596315860748291
		 38 -1.6090984344482422 39 -1.6753625869750977 40 -1.732403039932251 41 -1.7173738479614258
		 42 -1.5201050043106079 43 -1.2062715291976929 44 -0.97506475448608387 45 -1.0234044790267944
		 46 -1.5286486148834229 47 -2.3503479957580566 48 -3.202383279800415 49 -3.7896411418914799
		 50 -4.0456724166870117 51 -4.1205825805664062 52 -4.0418863296508789 53 -3.8373885154724121
		 54 -3.3772327899932861 55 -2.6947882175445557 56 -2.0608644485473633 57 -1.7406207323074341
		 58 -1.8915131092071533 59 -2.3430824279785156 60 -2.8638198375701904 61 -3.2182216644287109
		 62 -3.2665426731109619 63 -3.1551332473754883 64 -3.0756673812866211 65 -3.0925631523132324
		 66 -3.1324810981750488 67 -3.18015456199646 68 -3.2203011512756348 69 -3.2526230812072754
		 70 -3.2840101718902588 71 -3.3099446296691895 72 -3.3259048461914062 73 -3.3205039501190186
		 74 -3.2995457649230957 75 -3.2842919826507568 76 -3.2959985733032227 77 -3.3542418479919434
		 78 -3.4430873394012451 79 -3.5304913520812988 80 -3.5843477249145512 81 -3.6363723278045659
		 82 -3.6902508735656738 83 -3.6717557907104492 84 -3.5067451000213623 85 -3.0831553936004639
		 86 -2.4735019207000732 87 -1.8793027400970457 88 -1.4978578090667725 89 -1.3588626384735107
		 90 -1.3627383708953857 91 -1.5102883577346802 92 -1.8031185865402222 93 -2.4138767719268799
		 94 -3.2898330688476563 95 -4.0874567031860352 96 -4.4572081565856934 97 -4.172938346862793
		 98 -3.4646904468536377 99 -2.6677360534667969 100 -2.1127719879150391 101 -1.8495540618896487
		 102 -1.712864875793457 103 -1.7053287029266357 104 -1.8301405906677244 105 -2.190812349319458
		 106 -2.7539560794830322 107 -3.3150577545166016 108 -3.6660666465759273 109 -3.7362284660339355
		 110 -3.6403462886810307 111 -3.4478724002838135 112 -3.2282471656799316 113 -2.9292392730712891
		 114 -2.5383238792419434 115 -2.1834292411804199 116 -1.9908543825149536 117 -2.0253410339355469
		 118 -2.2087588310241699 119 -2.4522709846496582 120 -2.6664080619812012;
createNode animCurveTU -n "Character1_LeftHandThumb2_visibility";
	rename -uid "65F1E110-4EC6-3FB2-3BA0-92950EB85F0D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateX";
	rename -uid "541075C4-40CE-B9F8-D5C5-5E960120F64E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1733376979827881;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateY";
	rename -uid "A4BF3ED5-4229-551C-0198-0EB82C677EDD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.34823575615882874;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateZ";
	rename -uid "3E0BC9A5-432B-F502-9CDF-CF89037EE8A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.468076229095459;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleX";
	rename -uid "46477E37-42E5-30F4-4985-3E91B75E28CA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleY";
	rename -uid "38EA5447-43C3-6761-6C5F-9B8D4350EB32";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleZ";
	rename -uid "1E45A822-46C4-953F-3A0F-DA90A209A500";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateX";
	rename -uid "C9B5CD3A-46B4-F0AC-4294-FDB187191B2A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.0533958528831135e-007;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateY";
	rename -uid "789994C2-4610-927C-105F-E0921E57D2A8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.1877225827374787e-008;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateZ";
	rename -uid "84B8657F-416B-B66E-AC32-D1A5BA3F3876";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.8289945791802893e-007;
createNode animCurveTU -n "Character1_LeftHandThumb3_visibility";
	rename -uid "CCCE040E-4D71-8B48-604C-9F80001A9DC5";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateX";
	rename -uid "432DB907-4C0A-BD61-66AA-C58724DA6312";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.4336724281311035;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateY";
	rename -uid "BF910031-42C7-1D35-E357-CBA8051E1415";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.6582565307617187;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateZ";
	rename -uid "F24CCC20-4DE1-2CB7-E5C1-BA826386955B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.338188648223877;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleX";
	rename -uid "3187AFC6-45A4-DBEF-7A92-F0B8CB63A77E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleY";
	rename -uid "17C14910-4437-E84A-7E81-BEBEF185A632";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleZ";
	rename -uid "2AD1C453-4388-197A-3DE2-BDA307B0863D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateX";
	rename -uid "F18BDD3C-4D24-05EA-B2F3-49B52C2BE13F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -5.4912371635437012 1 -5.9940404891967773
		 2 -6.5104598999023437 3 -7.0149350166320801 4 -7.4808635711669913 5 -8.117156982421875
		 6 -8.8712339401245117 7 -9.3061513900756836 8 -8.9738235473632812 9 -7.3130197525024414
		 10 -4.8095784187316895 11 -2.4865272045135498 12 -1.2089618444442749 13 -1.3155345916748047
		 14 -2.2665896415710449 15 -3.5612237453460693 16 -4.6684579849243164 17 -5.5855212211608887
		 18 -6.5539150238037109 19 -7.3627285957336417 20 -7.7881741523742667 21 -7.7091999053955087
		 22 -7.2593569755554199 23 -6.5952515602111816 24 -5.8698477745056152 25 -4.9234175682067871
		 26 -3.758820533752441 27 -2.7452688217163086 28 -2.2265238761901855 29 -2.3164119720458984
		 30 -2.7733542919158936 31 -3.4014675617218018 32 -3.9993910789489751 33 -3.8995287418365479
		 34 -3.1836800575256348 35 -2.9258737564086914 36 -3.0883500576019287 37 -3.9995768070220947
		 38 -6.9510922431945801 39 -12.91733455657959 40 -21.744197845458984 41 -32.791786193847656
		 42 -44.1546630859375 43 -52.926078796386719 44 -56.157375335693359 45 -56.313175201416016
		 46 -57.943016052246094 47 -60.598690032958991 48 -63.357486724853509 49 -65.260917663574219
		 50 -66.091056823730469 51 -66.333953857421875 52 -66.078781127929687 53 -65.415725708007813
		 54 -63.924091339111328 55 -61.713432312011712 56 -59.662460327148445 57 -58.627540588378913
		 58 -59.115066528320313 59 -60.575183868408203 60 -62.260757446289055 61 -63.408805847167969
		 62 -63.56538009643554 63 -63.204395294189453 64 -62.946945190429688 65 -63.001678466796875
		 66 -63.131000518798828 67 -63.28546142578125 68 -63.41554260253907 69 -63.520275115966804
		 70 -63.621986389160156 71 -63.706027984619141 72 -63.757751464843743 73 -63.740245819091804
		 74 -63.672328948974609 75 -63.622898101806641 76 -60.612823486328118 77 -52.842491149902344
		 78 -42.280338287353516 79 -30.878530502319336 80 -20.146812438964844 81 -11.043912887573242
		 82 -3.953147411346436 83 1.0221072435379028 84 -1.5764858722686768 85 -6.2065181732177734
		 86 -4.4354395866394043 87 -1.4878807067871094 88 1.5977435111999512 89 4.0486774444580078
		 90 5.038790225982666 91 4.7078437805175781 92 3.9479265213012695 93 2.5543076992034912
		 94 0.62105387449264526 95 -1.3472481966018677 96 -2.7873578071594238 97 -3.2673053741455078
		 98 -3.0651156902313232 99 -2.6579668521881104 100 -2.537445068359375 101 -2.7740755081176758
		 102 -3.0987792015075684 103 -3.5043046474456787 104 -3.9812695980072017 105 -4.6838293075561523
		 106 -5.6408672332763672 107 -6.6079950332641602 108 -7.2207398414611816 109 -7.3439869880676278
		 110 -7.1756248474121085 111 -6.8391070365905762 112 -6.4574036598205566 113 -5.9414458274841309
		 114 -5.2729043960571289 115 -4.6713657379150391 116 -4.3469300270080566 117 -4.4049334526062012
		 118 -4.7141389846801758 119 -5.126594066619873 120 -5.4912371635437012;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateY";
	rename -uid "6EB1AA27-49F9-9B16-0CEF-4E93F85DFFA6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -2.1310844421386719 1 -2.8611328601837158
		 2 -3.6000854969024663 3 -4.3113312721252441 4 -4.9589319229125977 5 -5.8289527893066406
		 6 -6.8386445045471191 7 -7.4105100631713867 8 -6.9742274284362793 9 -4.7266707420349121
		 10 -1.1246734857559204 11 2.4486074447631836 12 4.5069537162780762 13 4.332768440246582
		 14 2.798304557800293 15 0.76806735992431641 16 -0.91392457485198975 17 -2.2687764167785645
		 18 -3.66176438331604 19 -4.7955780029296875 20 -5.3811936378479004 21 -5.2730484008789062
		 22 -4.6521687507629395 23 -3.720364093780518 24 -2.681781530380249 25 -1.2940813302993774
		 26 0.46420764923095703 27 2.0397143363952637 28 2.8622183799743652 29 2.7189173698425293
		 30 1.9954935312271116 31 1.0149054527282715 32 0.09642522782087326 33 0.81940257549285889
		 34 3.4006361961364746 35 5.763756275177002 36 8.0156936645507812 37 9.5086288452148438
		 38 6.9505701065063477 39 -0.43242612481117249 40 -9.7359457015991211 41 -18.421321868896484
		 42 -24.587059020996094 43 -27.877445220947266 44 -28.842994689941406 45 -28.873432159423828
		 46 -29.168539047241207 47 -29.559717178344727 48 -29.851003646850586 49 -29.984830856323242
		 50 -30.026191711425781 51 -30.036348342895508 52 -30.025653839111328 53 -29.993324279785156
		 54 -29.896533966064457 55 -29.691389083862301 56 -29.434347152709957 57 -29.279912948608398
		 58 -29.354745864868161 59 -29.556734085083011 60 -29.749086380004879 61 -29.855327606201175
		 62 -29.86827278137207 63 -29.837871551513672 64 -29.814985275268555 65 -29.8199348449707
		 66 -29.831449508666996 67 -29.844869613647461 68 -29.855892181396484 69 -29.864580154418945
		 70 -29.872859954833984 71 -29.879585266113281 72 -29.883670806884766 73 -29.882291793823242
		 74 -29.876901626586914 75 -29.872934341430668 76 -29.326631546020511 77 -27.444269180297852
		 78 -23.703422546386719 79 -17.878894805908203 80 -10.348579406738281 81 -2.1616826057434082
		 82 5.3827323913574219 83 11.125036239624023 84 4.3450183868408203 85 -3.1665024757385254
		 86 0.9108893871307373 87 7.4530291557312021 88 14.176973342895508 89 19.21746826171875
		 90 21.131374359130859 91 20.485509872436523 92 19.004343032836914 93 16.304990768432617
		 94 12.62773609161377 95 8.9549674987792969 96 6.2257881164550781 97 5.0392251014709473
		 98 4.8686819076538086 99 4.9477095603942871 100 4.5094056129455566 101 3.4925315380096436
		 102 2.3951311111450195 103 1.2870200872421265 104 0.23760569095611572 105 -0.93692034482955933
		 106 -2.3494324684143066 107 -3.7384150028228755 108 -4.5984821319580078 109 -4.7696104049682617
		 110 -4.5356836318969727 111 -4.0646262168884277 112 -3.5246737003326416 113 -2.7852563858032227
		 114 -1.8108233213424685 115 -0.91827481985092152 116 -0.4306892454624176 117 -0.51817917823791504
		 118 -0.98223406076431286 119 -1.5951063632965088 120 -2.1310844421386719;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateZ";
	rename -uid "9B4AE3ED-447D-557F-FDE7-208C3AF44DD7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 4.2169618606567383 1 4.5847835540771484
		 2 4.9656186103820801 3 5.3405361175537109 4 5.6892647743225098 5 6.1691899299621582
		 6 6.7432489395141602 7 7.0768532752990723 8 6.8217782974243164 9 5.5633730888366699
		 10 3.7231571674346924 11 2.0856204032897949 12 1.2178248167037964 13 1.2892574071884155
		 14 1.9344773292541502 15 2.8341045379638672 16 3.6216456890106197 17 4.2857069969177246
		 18 4.997802734375 19 5.6006259918212891 20 5.9205317497253418 21 5.8610057830810547
		 22 5.5231866836547852 23 5.0284371376037598 24 4.493654727935791 25 3.8052260875701909
		 26 2.9734625816345215 27 2.2643308639526367 28 1.9070203304290774 29 1.9686534404754641
		 30 2.2837870121002197 31 2.7218213081359863 32 3.1438343524932861 33 2.8397867679595947
		 34 1.706523060798645 35 0.55522269010543823 36 -0.98461610078811634 37 -2.4107179641723633
		 38 -1.8055282831192014 39 1.7694439888000488 40 7.7954702377319336 41 15.84568405151367
		 42 24.342998504638672 43 30.907516479492184 44 33.260517120361328 45 33.41363525390625
		 46 35.015579223632813 47 37.626426696777344 48 40.339290618896484 49 42.211292266845703
		 50 43.027763366699219 51 43.266670227050781 52 43.015689849853516 53 42.363548278808594
		 54 40.896518707275391 55 38.722545623779297 56 36.705924987792969 57 35.688484191894531
		 58 36.167758941650391 59 37.603317260742188 60 39.260757446289063 61 40.389755249023438
		 62 40.543743133544922 63 40.188732147216797 64 39.935550689697266 65 39.989376068115234
		 66 40.116558074951172 67 40.268459320068359 68 40.396385192871094 69 40.499382019042969
		 70 40.599411010742187 71 40.682060241699219 72 40.732929229736328 73 40.715713500976562
		 74 40.648918151855469 75 40.600307464599609 76 38.416069030761719 77 32.829257965087891
		 78 25.354976654052734 79 17.527437210083008 80 10.510767936706543 81 4.976837158203125
		 82 1.0410110950469971 83 -1.4554206132888794 84 0.75209546089172363 85 4.7411069869995117
		 86 4.3143477439880371 87 3.49359130859375 88 3.0818734169006348 89 3.1813514232635498
		 90 3.3516056537628174 91 3.4324471950531006 92 3.571305513381958 93 3.9692928791046138
		 94 4.6588253974914551 95 5.3662590980529785 96 5.7103314399719238 97 5.3761796951293945
		 98 4.5897440910339355 99 3.731397151947021 100 3.1694667339324951 101 2.952404260635376
		 102 2.8865876197814941 103 2.9635899066925049 104 3.1745665073394775 105 3.6326906681060787
		 106 4.3261103630065918 107 5.0378851890563965 108 5.494286060333252 109 5.5865778923034668
		 110 5.460543155670166 111 5.2095456123352051 112 4.9263525009155273 113 4.5461692810058594
		 114 4.0581798553466797 115 3.6237344741821294 116 3.3913061618804932 117 3.4327614307403564
		 118 3.654477596282959 119 3.9520998001098637 120 4.2169618606567383;
createNode animCurveTU -n "Character1_LeftHandIndex1_visibility";
	rename -uid "D100A875-4A73-894C-EEFD-A98262A2F60E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateX";
	rename -uid "12C2CA21-4060-ED8F-86A7-2995D7A73FF2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.5846562385559082;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateY";
	rename -uid "8673EB34-41BD-925B-2AE7-6CBF7E5BA028";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.7001519203186035;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateZ";
	rename -uid "01EA898C-474D-A256-281E-84926B230B3B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.35993427038192749;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleX";
	rename -uid "2AB57B5D-4341-30B0-9F8F-C3980B6FEBB1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleY";
	rename -uid "AE133260-4810-62BB-5AC3-F8AE257BFBC3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleZ";
	rename -uid "26369EDB-4327-88B0-9A1D-F794C2B0A025";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateX";
	rename -uid "0C98B1DD-482D-38F3-8B4D-A5A5C100CAE5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -15.816792488098145 1 -16.362293243408203
		 2 -16.915300369262695 3 -17.448579788208008 4 -17.935163497924805 5 -18.59068489074707
		 6 -19.354549407958984 7 -19.788902282714844 8 -19.457408905029297 9 -17.760526657104492
		 10 -15.065806388854979 11 -12.40241527557373 12 -10.865118980407715 13 -10.99540901184082
		 14 -12.141547203063965 15 -13.655101776123047 16 -14.908655166625975 17 -15.919622421264648
		 18 -16.961505889892578 19 -17.812324523925781 20 -18.25303840637207 21 -18.171579360961914
		 22 -17.704540252685547 23 -17.005409240722656 24 -16.22821044921875 25 -15.192153930664062
		 26 -13.88152027130127 27 -12.707328796386719 28 -12.093857765197754 29 -12.200777053833008
		 30 -12.740298271179199 31 -13.471165657043457 32 -14.15557861328125 33 -15.415550231933592
		 34 -17.758260726928711 35 -21.011163711547852 36 -25.0672607421875 37 -29.887775421142582
		 38 -35.451179504394531 39 -41.651969909667969 40 -48.415245056152344 41 -55.476474761962891
		 42 -61.742290496826179 43 -65.833145141601563 44 -66.872100830078125 45 -67.108436584472656
		 46 -69.675247192382813 47 -74.273529052734375 48 -79.703964233398438 49 -83.912483215332031
		 50 -85.880714416503906 51 -86.472770690917969 52 -85.850990295410156 53 -84.273147583007813
		 54 -80.914344787597656 55 -76.380027770996094 56 -72.588394165039063 57 -70.808204650878906
		 58 -71.636405944824219 59 -74.230308532714844 60 -77.456710815429688 61 -79.812171936035156
		 62 -80.144050598144531 63 -79.382827758789063 64 -78.848304748535156 65 -78.961372375488281
		 66 -79.229743957519531 67 -79.552574157714844 68 -79.826400756835938 69 -80.048171997070313
		 70 -80.264671325683594 71 -80.444404602050781 72 -80.555404663085938 73 -80.517807006835938
		 74 -80.372245788574219 75 -80.266624450683594 76 -78.792198181152344 77 -75.112640380859375
		 78 -70.140823364257813 79 -64.560508728027344 80 -58.777744293212884 81 -53.158084869384766
		 82 -47.772281646728516 83 -42.474662780761719 84 -30.787254333496094 85 -19.77519416809082
		 86 -15.555674552917482 87 -12.682574272155762 88 -11.176342964172363 89 -10.687896728515625
		 90 -10.605693817138672 91 -10.776293754577637 92 -11.312973976135254 93 -12.50810432434082
		 94 -14.239800453186035 95 -15.884802818298342 96 -16.827104568481445 97 -16.651630401611328
		 98 -15.724601745605471 99 -14.608099937438965 100 -13.893427848815918 101 -13.682087898254395
		 102 -13.66511344909668 103 -13.83940601348877 104 -14.199664115905762 105 -14.925801277160643
		 106 -15.97986888885498 107 -17.01893424987793 108 -17.664203643798828 109 -17.792802810668945
		 110 -17.617031097412109 111 -17.263481140136719 112 -16.858819961547852 113 -16.305562973022461
		 114 -15.577704429626463 115 -14.911899566650391 116 -14.548417091369627 117 -14.613629341125487
		 118 -14.959589004516602 119 -15.416723251342772 120 -15.816792488098145;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateY";
	rename -uid "A5E9201F-4CC0-D624-AB36-20A2138583BF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -16.401388168334961 1 -17.053604125976563
		 2 -17.719516754150391 3 -18.366006851196289 4 -18.959461212158203 5 -19.764095306396484
		 6 -20.708736419677734 7 -21.249042510986328 8 -20.83648681640625 9 -18.746086120605469
		 10 -15.511360168457031 11 -12.433786392211914 12 -10.717601776123047 13 -10.861262321472168
		 14 -12.139347076416016 15 -13.865406036376953 16 -15.326300621032713 17 -16.52397346496582
		 18 -17.775363922119141 19 -18.809328079223633 20 -19.34892463684082 21 -19.248989105224609
		 22 -18.677768707275391 23 -17.828458786010742 24 -16.892854690551758 25 -15.66044807434082
		 26 -14.127226829528809 27 -12.779571533203125 28 -12.085659980773926 29 -12.206085205078125
		 30 -12.817065238952637 31 -13.653390884399414 32 -14.445358276367186 33 -15.924700736999513
		 34 -18.743320465087891 35 -22.78065299987793 36 -27.950832366943359 37 -34.146533966064453
		 38 -41.101848602294922 39 -48.239208221435547 40 -54.935462951660156 41 -60.571895599365234
		 42 -64.495323181152344 43 -66.588302612304688 44 -67.068153381347656 45 -67.174568176269531
		 46 -68.2681884765625 47 -69.968551635742187 48 -71.614234924316406 49 -72.666313171386719
		 50 -73.100936889648438 51 -73.225112915039063 52 -73.094619750976563 53 -72.74853515625
		 54 -71.935073852539063 55 -70.6494140625 56 -69.381248474121094 57 -68.716476440429687
		 58 -69.0316162109375 59 -69.953971862792969 60 -70.976005554199219 61 -71.643547058105469
		 62 -71.732681274414063 63 -71.526481628417969 64 -71.377922058105469 65 -71.40960693359375
		 66 -71.484260559082031 67 -71.573005676269531 68 -71.647392272949219 69 -71.707054138183594
		 70 -71.764785766601562 71 -71.812332153320313 72 -71.841529846191406 73 -71.831657409667969
		 74 -71.793281555175781 75 -71.765296936035156 76 -71.174285888671875 77 -69.533309936523438
		 78 -66.940940856933594 79 -63.480159759521484 80 -59.255638122558601 81 -54.534130096435547
		 82 -49.526355743408203 83 -44.241718292236328 84 -31.223869323730465 85 -19.116355895996094
		 86 -16.005083084106445 87 -14.822556495666506 88 -15.100262641906738 89 -16.045526504516602
		 90 -16.597869873046875 91 -16.794916152954102 92 -17.251703262329102 93 -18.355192184448242
		 94 -20.013593673706055 95 -21.498821258544922 96 -22.043848037719727 97 -21.140415191650391
		 98 -19.304485321044922 99 -17.287998199462891 100 -15.824265480041506 101 -15.028622627258301
		 102 -14.546203613281248 103 -14.391234397888185 104 -14.578962326049803 105 -15.346471786499023
		 106 -16.595870971679687 107 -17.844821929931641 108 -18.628578186035156 109 -18.785490036010742
		 110 -18.571079254150391 111 -18.141141891479492 112 -17.651290893554688 113 -16.985555648803711
		 114 -16.117027282714844 115 -15.330117225646973 116 -14.903673171997072 117 -14.980014801025392
		 118 -15.386234283447266 119 -15.926091194152832 120 -16.401388168334961;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateZ";
	rename -uid "DD83ED75-488C-21C4-3F0A-C8851BA19384";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -10.206292152404785 1 -10.50388240814209
		 2 -10.800863265991211 3 -11.082655906677246 4 -11.335753440856934 5 -11.670510292053223
		 6 -12.051357269287109 7 -12.263367652893066 8 -12.101864814758301 9 -11.245363235473633
		 10 -9.7892608642578125 11 -8.2455196380615234 12 -7.311774730682373 13 -7.3920516967773446
		 14 -8.0891761779785156 15 -8.9837751388549805 16 -9.7009391784667969 17 -10.262737274169922
		 18 -10.825457572937012 19 -11.272225379943848 20 -11.498984336853027 21 -11.457314491271973
		 22 -11.216278076171875 23 -10.848796844482422 24 -10.431157112121582 25 -9.8600082397460937
		 26 -9.1149454116821289 27 -8.4271392822265625 28 -8.0604991912841797 29 -8.124751091003418
		 30 -8.4467048645019531 31 -8.8766984939575195 32 -9.2727603912353516 33 -9.9845209121704102
		 34 -11.244187355041504 35 -12.841679573059082 36 -14.552622795104979 37 -16.117446899414063
		 38 -17.198734283447266 39 -17.398658752441406 40 -16.391855239868164 41 -14.099001884460449
		 42 -11.185146331787109 43 -8.924901008605957 44 -8.3126678466796875 45 -8.1714086532592773
		 46 -6.5923151969909668 47 -3.5796175003051758 48 0.22865566611289978 49 3.3291192054748535
		 50 4.8162651062011719 51 5.2677669525146484 52 4.7936453819274902 53 3.5999846458435059
		 54 1.1083829402923584 55 -2.1312532424926758 56 -4.7088174819946289 57 -5.8706903457641602
		 58 -5.3342776298522949 59 -3.6089177131652828 60 -1.376322865486145 61 0.30688071250915527
		 62 0.54732966423034668 63 -0.0030159046873450279 64 -0.38694310188293457 65 -0.30590885877609253
		 66 -0.11318260431289673 67 0.11934769153594971 68 0.31717312335968018 69 0.4777887761592865
		 70 0.63491678237915039 71 0.76561152935028076 72 0.84643459320068359 73 0.81905090808868408
		 74 0.71311217546463013 75 0.63633120059967041 76 -0.27375078201293945 77 -2.40110182762146
		 78 -4.9324092864990234 79 -7.2421622276306152 80 -8.9808387756347656 81 -9.9685001373291016
		 82 -10.284486770629883 83 -10.08846378326416 84 -8.4120244979858398 85 -4.2176532745361328
		 86 -2.5362646579742432 87 -1.5962231159210205 88 -1.4234621524810791 89 -1.7758051156997681
		 90 -2.2323675155639648 91 -2.7737958431243896 92 -3.5636982917785645 93 -4.7785897254943848
		 94 -6.2969927787780762 95 -7.693943977355957 96 -8.6187267303466797 97 -8.8772554397583008
		 98 -8.69305419921875 99 -8.3772640228271484 100 -8.2695074081420898 101 -8.4256601333618164
		 102 -8.6521520614624023 103 -8.9348249435424805 104 -9.2578811645507812 105 -9.7105932235717773
		 106 -10.295732498168945 107 -10.85598087310791 108 -11.195291519165039 109 -11.262106895446777
		 110 -11.17071533203125 111 -10.98536491394043 112 -10.770751953125 113 -10.473146438598633
		 114 -10.074435234069824 115 -9.7027664184570312 116 -9.4971294403076172 117 -9.5341625213623047
		 118 -9.7296047210693359 119 -9.985173225402832 120 -10.206292152404785;
createNode animCurveTU -n "Character1_LeftHandIndex2_visibility";
	rename -uid "C7719A4A-4A03-BB0C-444E-07B53AAD3536";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateX";
	rename -uid "E0DA8F58-47C8-DD1E-ED84-C88A56190405";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.2315034866333008;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateY";
	rename -uid "DA3C51D4-4866-843B-0534-91B971C95C20";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.1625156402587891;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateZ";
	rename -uid "BA406A24-4D42-1696-4D3C-69914035B8E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.733155369758606;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleX";
	rename -uid "2AD76FD2-4EBD-01D4-AB1B-F3AABCFFACF3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleY";
	rename -uid "585312DB-4900-9CD8-98AA-6BAC1BEB1246";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleZ";
	rename -uid "9F8691C7-4B60-2BEF-4CD8-10932F9702C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateX";
	rename -uid "0B39F466-4533-999E-97E7-2BAB54FD39F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.3830229401464754e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateY";
	rename -uid "517D2DB3-41E3-CC7F-7757-6EBD5A87D970";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.9617649488500319e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateZ";
	rename -uid "FF3F3F2B-4215-10C9-70AB-EC877E04BA61";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.1309740456417785e-008;
createNode animCurveTU -n "Character1_LeftHandIndex3_visibility";
	rename -uid "2F6E4FCB-4C1D-1E2D-62C9-2DA15791F1E8";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing1_translateX";
	rename -uid "4465BC6A-4130-4EA6-8F82-158F53AECD92";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.3586184978485107;
createNode animCurveTL -n "Character1_LeftHandRing1_translateY";
	rename -uid "B661DB98-4574-32A8-00A3-42A35B27879D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4706459045410156;
createNode animCurveTL -n "Character1_LeftHandRing1_translateZ";
	rename -uid "747CADB2-436B-8B07-071E-C988D86FF1AF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8014267683029175;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleX";
	rename -uid "78B03A4E-4C60-7B00-80C2-EB8080D27CE3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleY";
	rename -uid "F92F9551-471D-708F-4AEF-D1B4F126CC1F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleZ";
	rename -uid "148C3733-4047-5630-AF0C-92BC92AD5A55";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateX";
	rename -uid "64ED1F0E-431A-DA47-F478-B6B32B6D9AEE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 7.0871744155883789 1 7.7390518188476571
		 2 8.4110870361328125 3 9.0700654983520508 4 9.6809015274047852 5 10.518559455871582
		 6 11.516534805297852 7 12.094743728637695 8 11.652751922607422 9 9.4606113433837891
		 10 6.2071924209594727 11 3.2396781444549561 12 1.6272954940795898 13 1.7612919807434082
		 14 2.9611427783966064 15 4.6066184043884277 16 6.0255494117736816 17 7.209230899810791
		 18 8.4677543640136719 19 9.5258235931396484 20 10.084961891174316 21 9.9810352325439453
		 22 9.3902387619018555 23 8.5216751098632812 24 7.5778150558471689 25 6.3538522720336914
		 26 4.859039306640625 27 3.567875862121582 28 2.9104459285736084 29 3.0242035388946533
		 30 3.603534460067749 31 4.4027881622314453 32 3.9805722236633296 33 1.6500344276428223
		 34 -1.5699162483215332 35 -4.5368804931640625 36 -8.9035615921020508 37 -12.998927116394043
		 38 -11.095595359802246 39 -0.32887208461761475 40 18.297788619995117 41 48.317756652832031
		 42 82.668807983398438 43 95.762474060058594 44 94.465667724609375 45 94.7386474609375
		 46 97.533195495605469 47 101.85095977783203 48 106.03919982910156 49 108.76240539550781
		 50 109.90972900390625 51 110.24095916748047 52 109.89293670654297 53 108.97817993164062
		 54 106.86359405517578 55 103.57877349853516 56 100.36161041259766 57 98.673812866210938
		 58 99.474357604980469 59 101.81400299072266 60 104.40933990478516 61 106.11434936523437
		 62 106.34305572509766 63 105.81440734863281 64 105.43440246582031 65 105.51540374755859
		 66 105.70632934570312 67 105.93354797363281 68 106.12421417236328 69 106.27726745605469
		 70 106.42551422119141 71 106.54772186279297 72 106.622802734375 73 106.59740447998047
		 74 106.49874877929687 75 106.42684173583984 76 106.48203277587891 77 106.755859375
		 78 107.17107391357422 79 100.12636566162109 80 79.819831848144531 81 53.806602478027344
		 82 33.478927612304688 83 21.980192184448242 84 15.865280151367186 85 11.29703426361084
		 86 7.8823585510253906 87 5.6239891052246094 88 4.5566473007202148 89 4.2618083953857422
		 90 4.2700142860412598 91 4.5830726623535156 92 5.208442211151123 93 6.5317907333374023
		 94 8.4811677932739258 95 10.31790828704834 96 11.192039489746094 97 10.518650054931641
		 98 8.8784446716308594 99 7.0901093482971191 100 5.8760099411010742 101 5.3081278800964355
		 102 5.0151009559631348 103 4.9989809989929199 104 5.2664327621459961 105 6.0453262329101562
		 106 7.2809185981750497 107 8.5383014678955078 108 9.3396148681640625 109 9.5012340545654297
		 110 9.2804908752441406 111 8.8401079177856445 112 8.3419265747070312 113 7.6707520484924316
		 114 6.8048496246337891 115 6.0292901992797852 116 5.6123738288879395 117 5.6868410110473633
		 118 6.0843276977539063 119 6.6159048080444336 120 7.0871744155883789;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateY";
	rename -uid "C3D0A76B-4922-A789-C51D-1E926C1B195A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -22.507083892822266 1 -23.232044219970703
		 2 -23.964908599853516 3 -24.66932487487793 4 -25.309814453125 5 -26.168827056884766
		 6 -27.163448333740234 7 -27.725584030151367 8 -27.296806335449219 9 -25.080205917358398
		 10 -21.506307601928711 11 -17.9425048828125 12 -15.883936882019045 13 -16.058265686035156
		 14 -17.593013763427734 15 -19.620403289794922 16 -21.296550750732422 17 -22.643882751464844
		 18 -24.026033401489258 19 -25.148338317871094 20 -25.726949691772461 21 -25.620155334472656
		 22 -25.00653076171875 23 -24.084100723266602 24 -23.054025650024414 25 -21.674871444702148
		 26 -19.923463821411133 27 -18.35101318359375 28 -17.529125213623047 29 -17.67236328125
		 30 -18.395183563232422 31 -19.374135971069336 32 -18.922330856323242 33 -16.180805206298828
		 34 -12.205654144287109 35 -8.5228853225708008 36 -3.4407281875610352 37 1.018036961555481
		 38 -2.3182299137115479 39 -16.843517303466797 40 -36.686367034912109 41 -52.853572845458984
		 42 -57.248325347900391 43 -56.299251556396484 44 -56.564342498779297 45 -56.509956359863281
		 46 -55.909122467041016 47 -54.813312530517578 48 -53.536525726318359 49 -52.581222534179687
		 50 -52.146854400634766 51 -52.017787933349609 52 -52.153350830078125 53 -52.501018524169922
		 54 -53.258232116699219 55 -54.313491821289063 56 -55.215339660644531 57 -55.640041351318359
		 58 -55.442611694335938 59 -54.823604583740234 60 -54.059982299804688 61 -53.51153564453125
		 62 -53.435028076171875 63 -53.610809326171875 64 -53.734851837158203 65 -53.708572387695312
		 66 -53.646282196044922 67 -53.571521759033203 68 -53.508251190185547 69 -53.457107543945313
		 70 -53.407268524169922 71 -53.365959167480469 72 -53.340476989746094 73 -53.349105834960938
		 74 -53.382537841796875 75 -53.406822204589844 76 -53.388187408447266 77 -53.295131683349609
		 78 -53.152050018310547 79 -54.693244934082031 80 -56.510364532470703 81 -53.738651275634766
		 82 -46.583484649658203 83 -39.503097534179688 84 -34.133247375488281 85 -29.02596473693848
		 86 -24.505517959594727 87 -21.159540176391602 88 -19.560155868530273 89 -19.202999114990234
		 90 -19.212978363037109 91 -19.592025756835937 92 -20.339473724365234 93 -21.878433227539063
		 94 -24.040487289428711 95 -25.965082168579102 96 -26.843450546264648 97 -26.168918609619141
		 98 -24.465934753417969 99 -22.510377883911133 100 -21.123046875 101 -20.457420349121094
		 102 -20.109775543212891 103 -20.090568542480469 104 -20.408126831054688 105 -21.319440841674805
		 106 -22.724000930786133 107 -24.101985931396484 108 -24.953432083129883 109 -25.122663497924805
		 110 -24.891313552856445 111 -24.425102233886719 112 -23.890163421630859 113 -23.15673828125
		 114 -22.18878173828125 115 -21.300882339477539 116 -20.815362930297852 117 -20.902505874633789
		 118 -21.364545822143555 119 -21.974294662475586 120 -22.507083892822266;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateZ";
	rename -uid "95DF159F-4D18-B469-4959-729956DFBA1F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -6.542933464050293 1 -6.9327988624572754
		 2 -7.3406476974487314 3 -7.7462377548217773 4 -8.1270570755004883 5 -8.656641960144043
		 6 -9.2982673645019531 7 -9.6751289367675781 8 -9.38671875 9 -7.9891896247863778 10 -6.0259380340576172
		 11 -4.366851806640625 12 -3.5246021747589111 13 -3.5929031372070312 14 -4.2182331085205078
		 15 -5.1143093109130859 16 -5.920586109161377 17 -6.6154918670654297 18 -7.3753070831298819
		 19 -8.0299406051635742 20 -8.3814678192138672 21 -8.3158445358276367 22 -7.9452724456787118
		 23 -7.4083256721496573 24 -6.8358349800109863 25 -6.1113443374633789 26 -5.2555332183837891
		 27 -4.5435953140258789 28 -4.1913199424743652 29 -4.2517685890197754 30 -4.562903881072998
		 31 -5.0009856224060059 32 -4.6375703811645508 33 -3.1018989086151123 34 -1.2059427499771118
		 35 0.30444028973579407 36 1.8572684526443481 37 3.0027554035186768 38 2.6608791351318359
		 39 -1.170870304107666 40 -12.284969329833984 41 -36.849441528320313 42 -68.478302001953125
		 43 -81.040931701660156 44 -79.9671630859375 45 -80.1934814453125 46 -82.501350402832031
		 47 -86.030647277832031 48 -89.402702331542969 49 -91.562644958496094 50 -92.463920593261719
		 51 -92.72308349609375 52 -92.450767517089844 53 -91.732551574707031 54 -90.059516906738281
		 55 -87.428512573242188 56 -84.818778991699219 57 -83.438278198242188 58 -84.093978881835938
		 59 -86.000648498535156 60 -88.097175598144531 61 -89.462684631347656 62 -89.64508056640625
		 63 -89.223190307617188 64 -88.919319152832031 65 -88.984130859375 66 -89.136817932128906
		 67 -89.318359375 68 -89.470550537109375 69 -89.592628479003906 70 -89.710792541503906
		 71 -89.808143615722656 72 -89.867927551269531 73 -89.847702026367188 74 -89.769142150878906
		 75 -89.71185302734375 76 -89.755821228027344 77 -89.97381591796875 78 -90.303848266601563
		 79 -83.742599487304688 80 -65.0325927734375 81 -41.579730987548828 82 -24.294200897216797
		 83 -15.406173706054686 84 -11.185921669006348 85 -8.4218530654907227 86 -6.6285972595214844
		 87 -5.577949047088623 88 -5.0864677429199219 89 -4.9229822158813477 90 -4.9275140762329102
		 91 -5.1011857986450195 92 -5.452613353729248 93 -6.2153759002685547 94 -7.3835177421569824
		 95 -8.529026985168457 96 -9.0883960723876953 97 -8.6567001342773437 98 -7.6277303695678711
		 99 -6.544675350189209 100 -5.8342113494873047 101 -5.5091772079467773 102 -5.3433318138122559
		 103 -5.3342461585998535 104 -5.4855008125305176 105 -5.9320340156555176 106 -6.6582036018371582
		 107 -7.4185147285461426 108 -7.9137177467346191 109 -8.0145692825317383 110 -7.8769044876098633
		 111 -7.6040763854980478 112 -7.2984027862548819 113 -6.8916826248168945 114 -6.3758859634399414
		 115 -5.9227514266967773 116 -5.6827263832092285 117 -5.7254123687744141 118 -5.9546256065368652
		 119 -6.2647085189819336 120 -6.542933464050293;
createNode animCurveTU -n "Character1_LeftHandRing1_visibility";
	rename -uid "21FAAB1F-4FD8-91F9-735E-2A88DFE93C7D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing2_translateX";
	rename -uid "62101795-434E-A2E8-C53A-2FAFF7162A54";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7360267639160156;
createNode animCurveTL -n "Character1_LeftHandRing2_translateY";
	rename -uid "E19D455F-4D8E-15FC-DB51-C2874398B1D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8172166347503662;
createNode animCurveTL -n "Character1_LeftHandRing2_translateZ";
	rename -uid "D74F9A08-426A-FB76-0079-5980169E5AB8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.81390875577926636;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleX";
	rename -uid "2693BA01-4102-9D89-EFE8-36B8A728ED8E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleY";
	rename -uid "3B38C784-4B4D-7BA9-EE73-A4994CF75E01";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleZ";
	rename -uid "D228A0C6-47F5-0BAE-0634-8EB7DB860B0C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateX";
	rename -uid "9A977183-44FD-612F-2252-21B4F4226085";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 5.3052768707275391 1 5.6907415390014648
		 2 6.0916404724121094 3 6.4880304336547852 4 6.8582067489624023 5 7.369884490966796
		 6 7.9851722717285156 7 8.3442897796630859 8 8.0696067810058594 9 6.7244119644165039
		 10 4.7905869483947754 11 3.1089529991149902 12 2.23537278175354 13 2.3067746162414551
		 14 2.9558629989624023 15 3.8725464344024658 16 4.6851935386657715 17 5.3771862983703613
		 18 6.1256017684936523 19 6.7639846801757812 20 7.1044578552246094 21 7.041018009185791
		 22 6.6817398071289062 23 6.1579394340515137 24 5.5950798988342285 25 4.8758978843688965
		 26 4.0156822204589844 27 3.2904548645019531 28 2.9280936717987061 29 2.990445613861084
		 30 3.310246467590332 31 3.7574341297149658 32 2.8724045753479004 33 0.59226632118225098
		 34 -1.3764125108718872 35 -2.1122863292694092 36 -1.1683919429779053 37 1.6233359575271606
		 38 6.2257428169250488 39 13.943565368652344 40 27.191007614135742 41 44.172435760498047
		 42 57.989192962646477 43 62.606014251708984 44 61.781547546386719 45 61.954425811767585
		 46 63.74530029296875 47 66.593116760253906 48 69.457473754882813 49 71.377388000488281
		 50 72.200302124023437 51 72.439437866210937 52 72.188194274902344 53 71.531509399414063
		 54 70.033798217773438 55 67.762130737304688 56 65.599205017089844 57 64.487754821777344
		 58 65.012992858886719 59 66.56829833984375 60 68.330345153808594 61 69.509841918945313
		 62 69.669410705566406 63 69.301055908203125 64 69.037338256835938 65 69.093475341796875
		 66 69.225967407226563 67 69.383926391601562 68 69.516716003417969 69 69.62347412109375
		 70 69.727020263671875 71 69.812477111816406 72 69.865028381347656 73 69.847251892089844
		 74 69.778221130371094 75 69.727951049804688 76 69.766532897949219 77 69.958244323730469
		 78 70.249832153320313 79 68.378044128417969 80 62.511112213134773 81 53.483306884765625
		 82 42.620353698730469 83 31.558969497680661 84 21.798198699951172 85 13.995193481445313
		 86 8.4498662948608398 87 5.1236882209777832 88 3.844286203384399 89 3.6780662536621089
		 90 3.6826801300048833 91 3.8592274188995361 92 4.2148551940917969 93 4.9796590805053711
		 94 6.1336445808410645 95 7.2469072341918936 96 7.784461021423339 97 7.3699398040771484
		 98 6.3724417686462402 99 5.3070039749145508 100 4.5986509323120117 101 4.2718973159790039
		 102 4.1044955253601074 103 4.0953106880187988 104 4.2480273246765137 105 4.6966538429260254
		 106 5.4194788932800293 107 6.1679153442382813 108 6.651064395904541 109 6.7490596771240234
		 110 6.6152606010437012 111 6.3493480682373047 112 6.0502238273620605 113 5.6501936912536621
		 114 5.1394228935241699 115 4.6873607635498047 116 4.4465813636779785 117 4.4894700050354004
		 118 4.7192649841308594 119 5.0288047790527344 120 5.3052768707275391;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateY";
	rename -uid "48CA1B07-447E-8CB5-1C0D-B3A33B6D02B9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 12.256824493408203 1 12.951760292053223
		 2 13.654167175292969 3 14.329232215881348 4 14.942985534667967 5 15.766101837158205
		 6 16.719169616699219 7 17.257852554321289 8 16.846960067749023 9 14.722966194152834
		 10 11.297295570373535 11 7.8779029846191415 12 5.9006161689758301 13 6.0681295394897461
		 14 7.5423316955566397 15 9.4883432388305664 16 11.096151351928711 17 12.387967109680176
		 18 13.712748527526855 19 14.788252830505371 20 15.342693328857424 21 15.240361213684084
		 22 14.652365684509277 23 13.768398284912109 24 12.781124114990234 25 11.458930969238281
		 26 9.7791128158569336 27 8.2700843811035156 28 7.4809832572937003 29 7.6185250282287598
		 30 8.3124847412109375 31 9.2520408630371094 32 7.3575057983398437 33 1.7036381959915161
		 34 -4.497185230255127 35 -7.3239464759826669 36 -3.7594938278198242 37 4.4175195693969727
		 38 13.884665489196777 39 24.285177230834961 40 34.350795745849609 41 39.96807861328125
		 42 40.756015777587891 43 40.358726501464844 44 40.454383850097656 45 40.435226440429687
		 46 40.208488464355469 47 39.739166259765625 48 39.126476287841797 49 38.632556915283203
		 50 38.399452209472656 51 38.329242706298828 52 38.402976989746094 53 38.589893341064453
		 54 38.985420227050781 55 39.506595611572266 56 39.918422698974609 57 40.099166870117188
		 58 40.016323089599609 59 39.743846893310547 60 39.384914398193359 61 39.113914489746094
		 62 39.075313568115234 63 39.163715362548828 64 39.225486755371094 65 39.212444305419922
		 66 39.181430816650391 67 39.14404296875 68 39.112258911132813 69 39.086471557617188
		 70 39.061264038085937 71 39.040309906005859 72 39.027359008789062 73 39.031745910644531
		 74 39.048725128173828 75 39.061038970947266 76 39.051593780517578 77 39.004261016845703
		 78 38.930965423583984 79 39.374439239501953 80 40.370292663574219 81 40.824996948242187
		 82 39.688488006591797 83 36.394569396972656 84 31.073928833007809 85 24.339879989624023
		 86 17.413845062255859 87 11.92252254486084 88 9.4305343627929687 89 9.0878152847290039
		 90 9.0973911285400391 91 9.4611139297485352 92 10.178203582763672 93 11.65411376953125
		 94 13.726600646972656 95 15.570872306823729 96 16.412534713745117 97 15.766189575195311
		 98 14.134323120117188 99 12.259983062744141 100 10.929760932922363 101 10.291342735290527
		 102 9.957855224609375 103 9.9394292831420898 104 10.244060516357422 105 11.11810302734375
		 106 12.464771270751953 107 13.785539627075195 108 14.601484298706056 109 14.763650894165039
		 110 14.541958808898926 111 14.095192909240723 112 13.582533836364746 113 12.879578590393066
		 114 11.951669692993164 115 11.10030460357666 116 10.634670257568359 117 10.718249320983887
		 118 11.161355972290039 119 11.746027946472168 120 12.256824493408203;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateZ";
	rename -uid "205CFBA2-4DFD-0EF7-8ED0-428BF23D661F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 9.5605840682983398 1 10.174345016479492
		 2 10.805130004882813 3 11.421723365783691 4 11.991550445556641 5 12.770272254943848
		 6 13.693940162658691 7 14.227059364318848 8 13.819668769836426 9 11.786240577697754
		 10 8.7291173934936523 11 5.9007630348205566 12 4.348630428314209 13 4.478020191192627
		 14 5.6333942413330078 15 7.2082314491271982 16 8.5570707321166992 17 9.6756448745727539
		 18 10.858227729797363 19 11.847041130065918 20 12.367571830749512 21 12.270926475524902
		 22 11.720607757568359 23 10.90873908996582 24 10.022708892822266 25 8.867924690246582
		 26 7.448810577392579 27 6.21539306640625 28 5.5846953392028809 29 5.6939544677734375
		 30 6.2495508193969727 31 7.0137662887573242 32 5.4868450164794922 33 1.2177882194519043
		 34 -3.1000485420227051 35 -4.9818463325500488 36 -2.6014015674591064 37 3.2187955379486084
		 38 11.014496803283691 39 22.103364944458008 40 38.963493347167969 41 59.147384643554687
		 42 75.213935852050781 43 80.583427429199219 44 79.623359680175781 45 79.824615478515625
		 46 81.911231994628906 47 85.237335205078125 48 88.595733642578125 49 90.855987548828125
		 50 91.827415466308594 51 92.110023498535156 52 91.8131103515625 53 91.037788391113281
		 54 89.273368835449219 55 86.606201171875 56 84.075218200683594 57 82.777336120605469
		 58 83.390472412109375 59 85.208297729492188 60 87.272407531738281 61 88.657279968261719
		 62 88.844841003417969 63 88.411941528320312 64 88.1021728515625 65 88.168106079101563
		 66 88.323722839355469 67 88.509300231933594 68 88.665359497070313 69 88.790847778320313
		 70 88.912574768066406 71 89.0130615234375 72 89.074859619140625 73 89.053947448730469
		 74 88.9727783203125 75 88.913665771484375 76 88.959030151367188 77 89.184486389160156
		 78 89.527557373046875 79 87.328361511230469 80 80.472885131835938 81 69.981880187988281
		 82 57.332653045654297 83 44.246547698974609 84 32.292407989501953 85 22.172981262207031
		 86 14.382992744445801 87 9.2688446044921875 88 7.1605725288391113 89 6.8791618347167969
		 90 6.8869986534118652 91 7.1857767105102539 92 7.781376838684082 93 9.0362119674682617
		 94 10.870795249938965 95 12.584023475646973 96 13.394094467163086 97 12.770355224609375
		 98 11.24262523651123 99 9.5633506774902344 100 8.4153242111206055 101 7.8761625289916992
		 102 7.5974159240722656 103 7.5820708274841309 104 7.836522102355957 105 8.5758094787597656
		 106 9.743194580078125 107 10.924311637878418 108 11.673380851745605 109 11.824117660522461
		 110 11.618208885192871 111 11.206774711608887 112 10.74030590057373 113 10.110125541687012
		 114 9.2941913604736328 115 8.5606155395507812 116 8.1651935577392578 117 8.2358760833740234
		 118 8.6127586364746094 119 9.1157150268554687 120 9.5605840682983398;
createNode animCurveTU -n "Character1_LeftHandRing2_visibility";
	rename -uid "381B74D1-4C31-1234-80AD-7598AFDA6125";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing3_translateX";
	rename -uid "18A998AA-46D5-43B8-5F6C-91A69F0E1B7A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.4660000801086426;
createNode animCurveTL -n "Character1_LeftHandRing3_translateY";
	rename -uid "63BE3F71-4AAA-DC48-70F5-F9A704522FC1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9262526035308838;
createNode animCurveTL -n "Character1_LeftHandRing3_translateZ";
	rename -uid "0E159D84-403C-BB55-2C84-B686BB7F2D19";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.018019437789917;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleX";
	rename -uid "78D3ADE4-4C2D-ED04-FBF9-76B1E5BFF5CA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleY";
	rename -uid "080F75C1-471E-8A5B-0A79-818AFBD7E8D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleZ";
	rename -uid "404240B4-47D6-5D87-6BEC-CDB13512052F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateX";
	rename -uid "C95ECF34-4004-3E63-E473-C8BC3222CF5B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.2847135738857105e-008;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateY";
	rename -uid "2739E0C9-47F4-C2A9-4F67-20B3406F85F9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.9861338286282262e-007;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateZ";
	rename -uid "6CE7EA2E-4B94-DEE9-AC62-3CA07004CD28";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.6419796834461529e-008;
createNode animCurveTU -n "Character1_LeftHandRing3_visibility";
	rename -uid "6F072D38-4BD2-8B54-A396-AFBB261B1703";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightShoulder_translateX";
	rename -uid "72702898-458F-11B0-D2E9-CCB2EDA9DB70";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.151631355285645;
createNode animCurveTL -n "Character1_RightShoulder_translateY";
	rename -uid "232A6FC8-4EF4-EB6B-6218-55A3EF8172E9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.1017274856567383;
createNode animCurveTL -n "Character1_RightShoulder_translateZ";
	rename -uid "37431F5C-4AE8-D6D1-4D1B-7683797FC7E9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.81731081008911133;
createNode animCurveTU -n "Character1_RightShoulder_scaleX";
	rename -uid "D21FD8E0-43FD-06AC-1F68-FEAD0B1DA278";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightShoulder_scaleY";
	rename -uid "370385F9-4219-8E1F-7371-C388CD791C11";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightShoulder_scaleZ";
	rename -uid "B179900C-40D7-26DC-3F75-F6940A2EC364";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightShoulder_rotateX";
	rename -uid "141E97FE-4491-D935-1790-7D848A9EFA23";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 120 ".ktv[0:119]"  0 11.355966567993164 1 10.346405029296875
		 2 9.3221597671508789 3 8.3494491577148437 4 7.4930291175842294 5 6.6826949119567871
		 6 5.9116659164428711 7 5.3361654281616211 8 5.110112190246582 9 5.110112190246582
		 11 5.110112190246582 12 5.110112190246582 13 4.8863792419433594 14 4.3560547828674316
		 15 3.7294976711273189 16 3.0117771625518799 17 1.9998956918716433 18 0.68369096517562866
		 19 -0.72189205884933472 20 -2.0163133144378662 21 -3.0993671417236328 22 -4.0177812576293945
		 23 -4.797447681427002 24 -5.4571995735168457 25 -5.8245511054992676 26 -5.8640589714050293
		 27 -5.7807421684265137 28 -5.7665963172912598 29 -5.7387170791625977 30 -5.4686799049377441
		 31 -5.1532764434814453 32 -5.0263524055480957 33 -5.1112833023071289 34 -5.5896391868591309
		 35 -6.4472513198852539 36 -7.4152874946594238 37 -8.2246904373168945 38 -8.8893775939941406
		 39 -9.5203876495361328 40 -9.9933738708496094 41 -10.184179306030273 42 -10.188265800476074
		 43 -10.182348251342773 44 -10.164141654968262 45 -10.131365776062012 46 -10.297235488891602
		 47 -10.763935089111328 48 -11.363387107849121 49 -11.931012153625488 50 -12.497971534729004
		 51 -13.113921165466309 52 -13.645918846130371 53 -13.961712837219238 54 -14.007203102111816
		 55 -13.85044002532959 56 -13.542437553405762 57 -13.132487297058105 58 -12.484180450439453
		 59 -11.610493659973145 60 -10.77863883972168 61 -10.316015243530273 62 -10.535981178283691
		 63 -10.983586311340332 64 -11.595033645629883 65 -12.308405876159668 66 -13.064516067504883
		 67 -13.807092666625977 68 -14.482401847839357 69 -15.217045783996582 70 -16.025518417358398
		 71 -16.68769645690918 72 -16.980926513671875 73 -16.960189819335938 74 -16.819629669189453
		 75 -16.543283462524414 76 -16.107522964477539 77 -15.660244941711426 78 -15.284849166870117
		 79 -14.871217727661133 80 -14.30123233795166 81 -13.62800407409668 82 -12.921937942504883
		 83 -12.07647705078125 84 -10.976462364196777 85 -9.5771636962890625 86 -7.9588899612426767
		 87 -6.1924757957458496 88 -4.356898307800293 89 -2.3001348972320557 90 -0.062649384140968323
		 91 2.0331282615661621 92 3.6476197242736821 93 3.9035506248474121 94 3.3465917110443115
		 95 2.5321080684661865 96 2.019770622253418 97 1.9497784376144409 98 2.022998571395874
		 99 2.1398932933807373 100 2.199336051940918 101 2.102963924407959 102 1.9215201139450073
		 103 1.8071825504302981 104 1.9125767946243288 105 2.2990553379058838 106 2.9177680015563965
		 107 3.7379193305969243 108 4.6841578483581543 109 6.0325183868408203 110 7.7386465072631827
		 111 9.226557731628418 112 9.8947057723999023 113 9.1313238143920898 114 7.4162082672119141
		 115 5.7554111480712891 116 5.110112190246582 117 5.8801922798156738 118 7.5090913772583008
		 119 9.505218505859375 120 11.355966567993164;
createNode animCurveTA -n "Character1_RightShoulder_rotateY";
	rename -uid "9F35FE5B-4891-14C2-F066-44AE45471781";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 120 ".ktv[0:119]"  0 -21.784000396728516 1 -21.03101921081543
		 2 -20.253917694091797 3 -19.503442764282227 4 -18.831602096557617 5 -18.183685302734375
		 6 -17.555892944335938 7 -17.080394744873047 8 -16.891950607299805 9 -16.891950607299805
		 11 -16.891950607299805 12 -16.891950607299805 13 -16.7247314453125 14 -16.319740295410156
		 15 -15.823448181152344 16 -15.23989200592041 17 -14.390965461730959 18 -13.259976387023926
		 19 -12.047149658203125 20 -10.965340614318848 21 -10.144980430603027 22 -9.5452308654785156
		 23 -9.1270933151245117 24 -8.8446731567382812 25 -8.8664340972900391 26 -9.2437171936035156
		 27 -9.7342472076416016 28 -10.080718040466309 29 -10.32844066619873 30 -10.697997093200684
		 31 -11.041598320007324 32 -11.185534477233887 33 -11.09064769744873 34 -10.530403137207031
		 35 -9.5183897018432617 36 -8.376429557800293 37 -7.4252915382385263 38 -6.6338334083557129
		 39 -5.8669466972351074 40 -5.2884469032287598 41 -5.0625600814819336 42 -5.0651092529296875
		 43 -5.0614180564880371 44 -5.0500640869140625 45 -5.0296273231506348 46 -4.7497191429138184
		 47 -4.097498893737793 48 -3.2780485153198242 49 -2.499082088470459 50 -1.7093746662139893
		 51 -0.84656673669815063 52 -0.1047174260020256 53 0.32160010933876038 54 0.33972486853599548
		 55 0.050873838365077972 56 -0.4487183690071106 57 -1.0615342855453491 58 -1.9583152532577515
		 59 -3.139397144317627 60 -4.2475066184997559 61 -4.869478702545166 62 -4.5975079536437988
		 63 -4.030601978302002 64 -3.2401187419891357 65 -2.2987139225006104 66 -1.2810794115066528
		 67 -0.26413178443908691 68 0.67324817180633545 69 1.7224527597427368 70 2.90596604347229
		 71 3.8898179531097412 72 4.3413906097412109 73 4.3502945899963379 74 4.2153892517089844
		 75 3.9192388057708745 76 3.4275588989257812 77 2.9720554351806641 78 2.6669478416442871
		 79 2.3196089267730713 80 1.7447614669799805 81 1.0332367420196533 82 0.28717944025993347
		 83 -0.64928305149078369 84 -1.924812912940979 85 -3.5805351734161377 86 -5.4678492546081543
		 87 -7.4494271278381348 88 -9.3944244384765625 89 -11.438426971435547 90 -13.531373023986816
		 91 -15.361367225646974 92 -16.661556243896484 93 -16.768089294433594 94 -16.201288223266602
		 95 -15.427052497863771 96 -14.91017436981201 97 -14.769289016723635 98 -14.751176834106447
		 99 -14.765433311462402 100 -14.722963333129885 101 -14.526703834533691 102 -14.243894577026367
		 103 -14.032017707824707 104 -14.047732353210449 105 -14.356719017028809 106 -14.889164924621582
		 107 -15.58393669128418 108 -16.353067398071289 109 -17.391912460327148 110 -18.656833648681641
		 111 -19.726312637329102 112 -20.211441040039063 113 -19.710977554321289 114 -18.518955230712891
		 115 -17.328775405883789 116 -16.891950607299805 117 -17.551275253295898 118 -18.858053207397461
		 119 -20.399263381958008 120 -21.784000396728516;
createNode animCurveTA -n "Character1_RightShoulder_rotateZ";
	rename -uid "0562F4CC-488A-321D-9829-F9879BF8FAD0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 120 ".ktv[0:119]"  0 -5.0574803352355957 1 -4.1733670234680176
		 2 -3.2958385944366455 3 -2.4722776412963867 4 -1.7447295188903809 5 -1.0359040498733521
		 6 -0.348265141248703 7 0.16864892840385437 8 0.37261086702346802 9 0.37261086702346802
		 11 0.37261086702346802 12 0.37261086702346802 13 0.60738825798034668 14 1.1565057039260864
		 15 1.7891875505447388 16 2.5242006778717041 17 3.5803768634796143 18 4.9630980491638184
		 19 6.4749808311462402 20 7.9425792694091797 21 9.2650012969970703 22 10.464723587036133
		 23 11.563797950744629 24 12.577630996704102 25 13.324522972106934 26 13.746658325195313
		 27 14.036586761474609 28 14.388218879699707 29 14.751458168029785 30 14.868666648864746
		 31 14.894870758056641 32 15.03787899017334 33 15.260169982910158 34 15.666342735290526
		 35 16.272369384765625 36 16.914207458496094 37 17.450122833251953 38 17.923030853271484
		 39 18.385320663452148 40 18.73365592956543 41 18.870450973510742 42 18.869699478149414
		 43 18.870786666870117 44 18.874134063720703 45 18.880167007446289 46 19.083837509155273
		 47 19.558700561523438 48 20.123983383178711 49 20.607044219970703 50 21.018041610717773
		 51 21.419197082519531 52 21.730993270874023 53 21.881637573242188 54 21.833368301391602
		 55 21.627525329589844 56 21.311395645141602 57 20.9326171875 58 20.379154205322266
		 59 19.642127990722656 60 18.942070007324219 61 18.582565307617188 62 18.885622024536133
		 63 19.42633056640625 64 20.131866455078125 65 20.930080413818359 66 21.751691818237305
		 67 22.53141975402832 68 23.208145141601563 69 23.843629837036133 70 24.441221237182617
		 71 24.869037628173828 72 25.010393142700195 73 24.906269073486328 74 24.661544799804687
		 75 24.258295059204102 76 23.657726287841797 77 23.002727508544922 78 22.377845764160156
		 79 21.664995193481445 80 20.741199493408203 81 19.643186569213867 82 18.463983535766602
		 83 17.15434455871582 84 15.654793739318846 85 13.929381370544434 86 12.033099174499512
		 87 10.038963317871094 88 8.0342159271240234 89 5.8941702842712402 90 3.6415197849273682
		 91 1.5673329830169678 92 0.0010472750291228294 93 -0.16071429848670959 94 0.52421832084655762
		 95 1.4731974601745605 96 2.1355819702148438 97 2.3957147598266602 98 2.5351572036743164
		 99 2.6263306140899658 100 2.7408511638641357 101 2.9501364231109619 102 3.1895914077758789
		 103 3.3294110298156738 104 3.242783784866333 105 2.8614468574523926 106 2.2366921901702881
		 107 1.428178071975708 108 0.5142439603805542 109 -0.7659381628036499 110 -2.4020023345947266
		 111 -3.8483338356018066 112 -4.4889111518859863 113 -3.6987872123718262 114 -1.9799973964691162
		 115 -0.33109027147293091 116 0.37261086702346802 117 -0.20570465922355652 118 -1.5871845483779907
		 119 -3.3636987209320068 120 -5.0574803352355957;
createNode animCurveTU -n "Character1_RightShoulder_visibility";
	rename -uid "DD7ADF02-478D-1DF2-340A-83B94B13C11D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightArm_translateX";
	rename -uid "40C204F0-45E3-598D-FB7C-10888E4A06DD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.05986499786377;
createNode animCurveTL -n "Character1_RightArm_translateY";
	rename -uid "8853DDAD-4EE3-44B3-E16D-C08AE1E8B531";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.8638343811035156;
createNode animCurveTL -n "Character1_RightArm_translateZ";
	rename -uid "067A5DCB-4D39-FDE5-1B0C-6DBAC73DED15";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4631747007369995;
createNode animCurveTU -n "Character1_RightArm_scaleX";
	rename -uid "166187F3-4BC7-3774-BCC1-90B62E0AA14F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightArm_scaleY";
	rename -uid "AE5E4D79-4A5A-0954-CF8C-63828F98AE08";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightArm_scaleZ";
	rename -uid "7E0B55D1-4088-6C58-1E01-1C8BEEFA7B22";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightArm_rotateX";
	rename -uid "5709566A-4306-FF2C-2084-B09EC902B36B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -65.858718872070313 1 -69.566574096679688
		 2 -73.906463623046875 3 -77.60552978515625 4 -79.276268005371094 5 -77.435844421386719
		 6 -73.040771484375 7 -68.378486633300781 8 -65.782882690429687 9 -66.953208923339844
		 10 -70.385551452636719 11 -73.564254760742187 12 -73.888298034667969 13 -69.095451354980469
		 14 -60.854450225830078 15 -52.579906463623047 16 -48.042003631591797 17 -50.089752197265625
		 18 -56.76678466796875 19 -64.601791381835938 20 -69.460067749023438 21 -69.886390686035156
		 22 -67.909889221191406 23 -64.5701904296875 24 -60.801506042480469 25 -54.531318664550781
		 26 -45.646415710449219 27 -38.385261535644531 28 -37.425537109375 29 -44.054615020751953
		 30 -54.38079833984375 31 -64.997154235839844 32 -75.227325439453125 33 -83.316116333007813
		 34 -89.536293029785156 35 -95.004989624023438 36 -99.2122802734375 37 -101.51651000976562
		 38 -100.67420959472656 39 -97.335594177246094 40 -93.684288024902344 41 -91.882354736328125
		 42 -93.383415222167969 43 -96.606460571289062 44 -99.250907897949219 45 -99.117721557617188
		 46 -94.926528930664063 47 -88.127059936523438 48 -81.622940063476563 49 -78.700515747070313
		 50 -81.775772094726562 51 -88.602081298828125 52 -95.627952575683594 53 -99.628059387207031
		 54 -99.77734375 55 -97.746086120605469 56 -94.338905334472656 57 -90.363182067871094
		 58 -84.495353698730469 59 -76.365425109863281 60 -68.621192932128906 61 -64.660751342773437
		 62 -69.089912414550781 63 -76.209625244140625 64 -84.581520080566406 65 -93.092002868652344
		 66 -100.84984588623047 67 -107.02402496337891 68 -110.74407958984375 69 -109.69134521484375
		 70 -104.36100006103516 71 -98.065109252929688 72 -94.203895568847656 73 -94.561309814453125
		 74 -97.108291625976563 75 -99.653678894042969 76 -99.686355590820313 77 -94.48321533203125
		 78 -85.49896240234375 79 -75.607177734375 80 -68.168746948242187 81 -65.787727355957031
		 82 -66.722984313964844 83 -67.998046875 84 -66.298873901367187 85 -60.594112396240227
		 86 -53.092464447021484 87 -45.155101776123047 88 -37.888816833496094 89 -30.025306701660156
		 90 -22.087648391723633 91 -15.435985565185545 92 -13.031704902648926 93 -21.823379516601563
		 94 -38.076179504394531 95 -54.987583160400391 96 -64.99212646484375 97 -64.218963623046875
		 98 -56.931209564208984 99 -48.274803161621094 100 -43.709346771240234 101 -46.046104431152344
		 102 -52.429611206054687 103 -59.537776947021484 104 -63.269470214843743 105 -62.141017913818359
		 106 -57.9642333984375 107 -52.539508819580078 108 -46.836532592773438 109 -38.628646850585938
		 110 -28.4466552734375 111 -20.910804748535156 112 -19.952869415283203 113 -29.049013137817379
		 114 -45.455696105957031 115 -62.305301666259773 116 -72.660049438476563 117 -74.60650634765625
		 118 -72.205795288085938 119 -68.411376953125 120 -65.858718872070313;
createNode animCurveTA -n "Character1_RightArm_rotateY";
	rename -uid "A939FD2E-4C11-8C08-C893-7D8200021CAD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 27.430723190307617 1 28.835266113281254
		 2 30.097757339477536 3 31.104293823242188 4 31.909547805786133 5 32.663364410400391
		 6 33.427692413330078 7 34.166282653808594 8 34.908733367919922 9 35.596218109130859
		 10 36.098552703857422 11 36.523265838623047 12 37.241279602050781 13 38.580276489257812
		 14 40.152828216552734 15 41.519268035888672 16 42.68914794921875 17 44.313362121582031
		 18 45.960964202880859 19 46.878505706787109 20 47.183231353759766 21 47.213787078857422
		 22 47.050998687744141 23 46.912921905517578 24 47.090789794921875 25 48.117389678955078
		 26 49.425407409667969 27 50.028430938720703 28 49.418548583984375 29 47.223381042480469
		 30 43.659072875976562 31 39.693832397460938 32 36.087966918945313 33 33.621967315673828
		 34 32.650886535644531 35 32.427402496337891 36 32.211048126220703 37 31.776338577270508
		 38 31.625307083129883 39 32.035507202148437 40 32.630588531494141 41 32.954666137695313
		 42 32.526046752929688 43 31.614568710327145 44 30.998012542724609 45 31.643112182617191
		 46 33.461784362792969 47 36.347126007080078 48 39.239822387695313 49 41.216991424560547
		 50 41.817779541015625 51 41.206630706787109 52 39.844520568847656 53 38.943523406982422
		 54 39.115066528320312 55 39.864223480224609 56 41.006149291992188 57 42.429435729980469
		 58 44.723991394042969 59 47.584739685058594 60 49.750431060791016 61 50.415332794189453
		 62 48.325706481933594 63 45.127162933349609 64 41.269557952880859 65 37.261207580566406
		 66 33.589767456054687 67 30.635658264160156 68 28.646747589111328 69 28.201206207275391
		 70 29.133579254150387 71 30.552627563476559 72 31.427907943725582 73 31.148603439331055
		 74 30.286401748657227 75 29.55247688293457 76 29.697210311889648 77 31.593900680541989
		 78 34.508628845214844 79 37.19140625 80 38.860336303710937 81 39.793857574462891
		 82 40.617465972900391 83 41.429889678955078 84 42.307712554931641 85 42.80255126953125
		 86 42.4361572265625 87 41.296699523925781 88 39.885158538818359 89 38.364082336425781
		 90 36.384357452392578 91 34.711502075195313 92 33.748336791992188 93 34.924541473388672
		 94 36.049827575683594 95 35.731376647949219 96 35.112152099609375 97 35.725154876708984
		 98 37.227401733398437 99 38.909599304199219 100 40.550983428955078 101 42.618320465087891
		 102 44.732864379882812 103 46.065692901611328 104 46.598117828369141 105 46.498470306396484
		 106 46.101673126220703 107 45.328651428222656 108 44.574970245361328 109 44.006851196289063
		 110 42.969978332519531 111 41.571987152099609 112 40.701698303222656 113 41.039386749267578
		 114 40.864974975585938 115 38.856739044189453 116 36.322059631347656 117 34.447376251220703
		 118 32.594715118408203 119 30.225601196289066 120 27.430723190307617;
createNode animCurveTA -n "Character1_RightArm_rotateZ";
	rename -uid "31BF3BEC-4542-107C-0026-8D874BA32569";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -40.528045654296875 1 -44.228408813476563
		 2 -48.286907196044922 3 -51.960971832275391 4 -54.482818603515625 5 -55.029659271240234
		 6 -54.032661437988281 7 -52.705001831054688 8 -52.493541717529297 9 -54.614109039306641
		 10 -58.130661010742187 11 -61.366096496582031 12 -62.665546417236321 13 -60.511581420898445
		 14 -55.712230682373047 15 -50.45025634765625 16 -47.854461669921875 17 -50.489727020263672
		 18 -56.858402252197266 19 -64.166694641113281 20 -69.240509033203125 21 -70.837638854980469
		 22 -70.393196105957031 23 -68.848770141601563 24 -67.109039306640625 25 -64.122283935546875
		 26 -59.201358795166016 27 -54.558280944824219 28 -53.210811614990234 29 -55.562053680419922
		 30 -58.760005950927741 31 -61.258956909179688 32 -63.039608001708984 33 -64.377143859863281
		 34 -66.881690979003906 35 -70.849388122558594 36 -74.831680297851562 37 -77.207725524902344
		 38 -77.178985595703125 39 -75.793807983398438 40 -74.209159851074219 41 -73.761192321777344
		 42 -75.48004150390625 43 -78.473365783691406 44 -81.425689697265625 45 -83.220657348632813
		 46 -81.982711791992187 47 -79.009521484375 48 -75.925132751464844 49 -75.122421264648438
		 50 -78.645225524902344 51 -84.747825622558594 52 -90.556427001953125 53 -93.858558654785156
		 54 -94.088279724121094 55 -92.408447265625 56 -89.662071228027344 57 -86.749771118164062
		 58 -83.099075317382813 59 -77.865325927734375 60 -72.273475646972656 61 -68.810874938964844
		 62 -70.575164794921875 63 -73.652069091796875 64 -76.952720642089844 65 -79.818763732910156
		 66 -81.993980407714844 67 -83.473899841308594 68 -84.345802307128906 69 -84.532905578613281
		 70 -84.178741455078125 71 -83.695724487304688 72 -83.928779602050781 73 -85.920829772949219
		 74 -88.987686157226563 75 -91.785194396972656 76 -93.048973083496094 77 -91.196609497070313
		 78 -86.606681823730469 79 -80.709449768066406 80 -75.916786193847656 81 -74.483207702636719
		 82 -75.074012756347656 83 -75.269920349121094 84 -72.666984558105469 85 -66.558334350585938
		 86 -58.808551788330085 87 -50.853477478027344 88 -43.852592468261719 89 -37.014625549316406
		 90 -30.533740997314453 91 -25.44685173034668 92 -23.53619384765625 93 -30.243501663208004
		 94 -41.909427642822266 95 -52.983192443847656 96 -59.024089813232415 97 -58.705532073974602
		 98 -54.459648132324219 99 -49.017597198486328 100 -46.058753967285156 101 -47.976608276367188
		 102 -52.852821350097656 103 -58.086006164550774 104 -60.598079681396484 105 -59.195339202880866
		 106 -55.766654968261719 107 -51.353298187255859 108 -46.896877288818359 109 -40.831356048583984
		 110 -33.197219848632812 111 -27.358089447021484 112 -26.515363693237305 113 -33.750534057617188
		 114 -46.184268951416016 115 -57.327156066894531 116 -62.071170806884766 117 -59.750476837158196
		 118 -53.723903656005859 119 -46.560962677001953 120 -40.528045654296875;
createNode animCurveTU -n "Character1_RightArm_visibility";
	rename -uid "D3FABD3B-4D07-529C-99C1-BD9D8752B0A2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightForeArm_translateX";
	rename -uid "CB1F1573-4CDF-F966-F97E-289CF82CC5C4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -16.894504547119141;
createNode animCurveTL -n "Character1_RightForeArm_translateY";
	rename -uid "11866300-49EC-CE04-075E-329855071571";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.7289671897888184;
createNode animCurveTL -n "Character1_RightForeArm_translateZ";
	rename -uid "DA7CF187-4446-144C-7268-EB85A81C4483";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.2348899841308594;
createNode animCurveTU -n "Character1_RightForeArm_scaleX";
	rename -uid "8C6D562D-4F8B-B075-32BA-2C83D4EDA12C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightForeArm_scaleY";
	rename -uid "F2D08794-42DD-395B-C7AB-B28B152C5B06";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightForeArm_scaleZ";
	rename -uid "CE811906-46B9-AB89-1C99-B9A115BC0AA0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightForeArm_rotateX";
	rename -uid "907927E8-494E-19B0-9195-79BA8F48911A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -41.547431945800781 1 -52.693778991699219
		 2 -77.125083923339844 3 -117.65314483642578 4 -145.34407043457031 5 -155.60824584960937
		 6 -158.43147277832031 7 -157.23490905761719 8 -154.25132751464844 9 -149.25456237792969
		 10 -142.26385498046875 11 -135.82565307617187 12 -131.198486328125 13 -127.58349609374999
		 14 -123.97403717041016 15 -120.78211212158202 16 -117.37337493896484 17 -116.13691711425781
		 18 -116.33385467529297 19 -116.53826904296876 20 -115.64456176757812 21 -112.98683166503906
		 22 -108.98398590087891 23 -104.53213500976562 24 -101.29688262939453 25 -101.49138641357422
		 26 -103.8043212890625 27 -105.3702392578125 28 -103.81349182128906 29 -96.565025329589844
		 30 -83.94683837890625 31 -69.514404296875 32 -57.193935394287109 33 -50.636623382568359
		 34 -54.557476043701172 35 -76.498199462890625 36 -119.11241912841797 37 -144.02325439453125
		 38 -150.03770446777344 39 -148.70094299316406 40 -142.11280822753906 41 -133.20921325683594
		 42 -123.10002136230467 43 -112.27748107910156 44 -103.75481414794922 45 -97.780845642089844
		 46 -93.616661071777344 47 -89.289031982421875 48 -85.78436279296875 49 -84.105422973632812
		 50 -85.368766784667969 51 -88.400444030761719 52 -91.138938903808594 53 -91.986053466796875
		 54 -90.08062744140625 55 -86.5650634765625 56 -83.137435913085937 57 -81.810279846191406
		 58 -83.695777893066406 59 -87.520187377929688 60 -92.003471374511719 61 -95.208206176757813
		 62 -98.680137634277344 63 -103.75104522705078 64 -111.06854248046875 65 -121.56149291992187
		 66 -135.98619079589844 67 -153.3212890625 68 -169.52943420410156 69 -179.7681884765625
		 70 -183.85630798339844 71 -183.82405090332031 72 -181.53665161132812 73 -176.13139343261719
		 74 -167.14454650878906 75 -157.29364013671875 76 -147.00495910644531 77 -139.49771118164062
		 78 -133.30570983886719 79 -128.39349365234375 80 -125.06472778320311 81 -124.12499237060547
		 82 -124.83522033691408 83 -125.63878631591797 84 -125.43968963623047 85 -123.56634521484375
		 86 -120.28366851806641 87 -116.28005981445314 88 -113.20623016357422 89 -111.89881134033203
		 90 -111.75759124755859 91 -111.41490173339844 92 -111.26723480224609 93 -112.47177886962891
		 94 -116.02001190185547 95 -120.52767181396484 96 -121.22405242919922 97 -113.291259765625
		 98 -101.10079193115234 99 -90.762191772460938 100 -84.992729187011719 101 -83.95855712890625
		 102 -85.334075927734375 103 -86.829902648925781 104 -86.7713623046875 105 -84.297126770019531
		 106 -81.82373046875 107 -79.180877685546875 108 -78.324440002441406 109 -80.684700012207031
		 110 -84.809646606445313 111 -88.658821105957031 112 -90.613609313964844 113 -90.753814697265625
		 114 -89.860794067382813 115 -87.027900695800781 116 -80.911834716796875 117 -70.7861328125
		 118 -59.175056457519538 119 -49.190254211425781 120 -41.547431945800781;
createNode animCurveTA -n "Character1_RightForeArm_rotateY";
	rename -uid "736C9D33-404D-AFAE-E61A-B4BA27180ECD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -54.079616546630859 1 -64.234596252441406
		 2 -72.942718505859375 3 -75.502105712890625 4 -72.630203247070313 5 -70.362274169921875
		 6 -69.895332336425781 7 -70.13818359375 8 -69.690834045410156 9 -68.166778564453125
		 10 -66.066146850585938 11 -63.591354370117188 12 -61.303291320800781 13 -59.697345733642578
		 14 -58.499889373779304 15 -57.323871612548828 16 -56.047443389892578 17 -53.985958099365234
		 18 -51.524009704589844 19 -49.466384887695313 20 -48.612873077392578 21 -49.534454345703125
		 22 -51.449832916259766 23 -53.216293334960937 24 -53.976104736328125 25 -53.115280151367187
		 26 -51.308452606201172 27 -49.912307739257813 28 -50.418037414550781 29 -53.023048400878906
		 30 -55.634681701660156 31 -56.953094482421875 32 -57.462360382080078 33 -59.196548461914063
		 34 -65.214935302734375 35 -73.228736877441406 36 -75.387107849121094 37 -72.468391418457031
		 38 -71.709297180175781 39 -72.835395812988281 40 -74.475723266601563 41 -75.098678588867188
		 42 -74.106964111328125 43 -71.832252502441406 44 -68.904853820800781 45 -66.279869079589844
		 46 -64.434730529785156 47 -62.951972961425781 48 -61.552391052246094 49 -60.05870437622071
		 50 -58.391246795654297 51 -56.669868469238281 52 -55.112834930419922 53 -54.107898712158203
		 54 -54.012107849121094 55 -54.421226501464844 56 -54.767452239990234 57 -54.755649566650391
		 58 -54.030319213867188 59 -52.827899932861328 60 -51.878108978271484 61 -52.277835845947266
		 62 -55.241180419921875 63 -59.163478851318359 64 -63.538650512695312 65 -67.755966186523438
		 66 -71.084274291992188 67 -72.843238830566406 68 -72.893211364746094 69 -72.084129333496094
		 70 -71.337432861328125 71 -70.846366882324219 72 -70.49151611328125 73 -70.292793273925781
		 74 -70.0438232421875 75 -69.405570983886719 76 -68.857292175292969 77 -68.279273986816406
		 78 -67.721977233886719 79 -66.934646606445313 80 -65.631057739257813 81 -63.370780944824219
		 82 -60.531978607177734 83 -58.019577026367188 84 -56.773796081542969 85 -57.559787750244141
		 86 -59.648048400878899 87 -61.774536132812507 88 -62.818496704101555 89 -62.089492797851563
		 90 -60.387676239013679 91 -58.771747589111328 92 -58.552120208740234 93 -60.832294464111328
		 94 -64.696296691894531 95 -68.387382507324219 96 -70.479202270507813 97 -70.507774353027344
		 98 -68.714645385742187 99 -65.585372924804688 100 -62.54811096191407 101 -60.101280212402337
		 102 -57.803409576416023 103 -55.847545623779297 104 -54.480171203613281 105 -53.908870697021484
		 106 -54.213233947753906 107 -54.494602203369141 108 -54.501712799072266 109 -53.87548828125
		 110 -52.746181488037109 111 -51.813377380371094 112 -52.036968231201172 113 -54.33740234375
		 114 -58.084674835205078 115 -61.965847015380859 116 -64.45648193359375 117 -64.466262817382813
		 118 -62.172000885009766 119 -58.295215606689453 120 -54.079616546630859;
createNode animCurveTA -n "Character1_RightForeArm_rotateZ";
	rename -uid "DE5A9843-45A2-4440-3DEC-9C8D5547CF54";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -45.646244049072266 1 -38.804592132568359
		 2 -18.936155319213867 3 17.590633392333984 4 42.684696197509766 5 52.307304382324219
		 6 55.947711944580078 7 56.289333343505859 8 54.727035522460938 9 50.813320159912109
		 10 44.843215942382813 11 39.351802825927734 12 35.753833770751953 13 33.608146667480469
		 14 31.847213745117184 15 30.505628585815433 16 28.625665664672852 17 28.585575103759766
		 18 29.923433303833008 19 31.081527709960938 20 30.658308029174805 21 27.524894714355469
		 22 22.309688568115234 23 16.527145385742188 24 12.366668701171875 25 12.200235366821289
		 26 14.479020118713379 27 16.076181411743164 28 14.136199951171875 29 6.0168571472167969
		 30 -7.385794162750245 31 -22.329233169555664 32 -34.797153472900391 33 -41.867946624755859
		 34 -40.13433837890625 35 -21.765354156494141 36 17.498672485351563 37 40.691669464111328
		 38 47.157016754150391 39 47.685951232910156 40 43.648605346679688 41 37.188488006591797
		 42 29.261421203613278 43 20.651336669921875 44 14.288983345031738 45 10.445221900939941
		 46 8.4260129928588867 47 6.5250015258789063 48 5.366396427154541 49 5.5826787948608398
		 50 8.3322811126708984 51 12.671477317810059 52 16.366184234619141 53 17.506538391113281
		 54 14.759544372558594 55 9.5144128799438477 56 4.1531863212585449 57 1.2818546295166016
		 58 2.2547023296356201 59 5.442774772644043 60 9.198328971862793 61 11.930066108703613
		 62 14.022964477539063 63 17.285438537597656 64 22.567056655883789 65 31.009510040283203
		 66 43.597679138183594 67 59.560001373291016 68 75.129493713378906 69 86.270599365234375
		 70 92.846916198730469 71 95.970687866210938 72 96.572288513183594 73 93.571983337402344
		 74 86.802536010742188 75 78.949478149414063 76 70.282386779785156 77 64.799125671386719
		 78 60.935466766357422 79 58.211078643798828 80 56.476100921630859 81 56.464954376220703
		 82 57.716812133789063 83 58.679355621337891 84 58.175640106201172 85 55.298973083496094
		 86 50.477523803710937 87 44.829685211181641 88 40.281574249267578 89 37.453777313232422
		 90 35.626930236816406 91 33.661739349365234 92 32.503662109375 93 33.339298248291016
		 94 36.637126922607422 95 40.809402465820312 96 41.419284820556641 97 34.39764404296875
		 98 23.972936630249023 99 15.51674270629883 100 11.044800758361816 101 10.738024711608887
		 102 12.723875045776367 103 14.570498466491697 104 14.292049407958984 105 10.540743827819824
		 106 6.275545597076416 107 1.6579539775848389 108 -0.74241656064987183 109 0.65263944864273071
		 110 4.0914015769958496 111 7.3891763687133798 112 8.6849822998046875 113 7.9909095764160156
		 114 6.1559662818908691 115 2.2909941673278809 116 -4.7898859977722168 117 -15.65882396697998
		 118 -27.750267028808594 119 -37.970447540283203 120 -45.646244049072266;
createNode animCurveTU -n "Character1_RightForeArm_visibility";
	rename -uid "DED7BC3A-4B82-E0E2-D454-EDB63AFE4B79";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHand_translateX";
	rename -uid "C9079DDE-4E30-9E8B-566B-34BD54B82552";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.9317862987518311;
createNode animCurveTL -n "Character1_RightHand_translateY";
	rename -uid "143E79E5-4243-4DE1-7B5C-80A6E21A8B7B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 16.156974792480469;
createNode animCurveTL -n "Character1_RightHand_translateZ";
	rename -uid "C4D6C08D-4707-24B2-8CAD-C08F480372BB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.4309353828430176;
createNode animCurveTU -n "Character1_RightHand_scaleX";
	rename -uid "C80568DB-4C3E-E060-D11C-00ADB68B2D18";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightHand_scaleY";
	rename -uid "2017F178-4718-7677-79D8-B1869A119351";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHand_scaleZ";
	rename -uid "9C5D0AEC-4CFE-1277-DDF3-A8B6B5AF5FD4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightHand_rotateX";
	rename -uid "AE7D85F5-473D-D4D6-82A7-82ABF15BD3F4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -8.7037343978881836 1 -9.3129205703735352
		 2 -9.8433694839477539 3 -10.238625526428223 4 -10.556427955627441 5 -10.913878440856934
		 6 -11.358919143676758 7 -11.69486141204834 8 -11.557554244995117 9 -10.563432693481445
		 10 -9.0780296325683594 11 -7.7343063354492188 12 -6.7276630401611328 13 -5.925478458404541
		 14 -5.1969165802001953 15 -4.560300350189209 16 -3.9862666130065918 17 -3.5332794189453125
		 18 -3.3117070198059082 19 -3.2961766719818115 20 -3.3867180347442627 21 -3.7530801296234126
		 22 -4.3593401908874512 23 -4.835749626159668 24 -4.8231287002563477 25 -3.8613905906677246
		 26 -2.1169676780700684 27 -0.34644514322280884 28 0.29058173298835754 29 -0.54449403285980225
		 30 -1.9339816570281985 31 -3.2884924411773682 32 -4.2566113471984863 33 -4.770808219909668
		 34 -5.0800108909606934 35 -5.2856035232543945 36 -5.3397488594055176 37 -5.2535486221313477
		 38 -5.1576533317565918 39 -5.0797538757324219 40 -4.7101554870605469 41 -3.6543269157409668
		 42 -1.522991418838501 43 1.19264817237854 44 3.6506862640380859 45 5.5258593559265137
		 46 6.998748779296875 47 8.3538351058959961 48 9.5736188888549805 49 10.510231018066406
		 50 10.929802894592285 51 10.854290962219238 52 10.485633850097656 53 10.01930046081543
		 54 9.2124042510986328 55 8.0426731109619141 56 6.9875917434692383 57 6.58795166015625
		 58 7.5405464172363281 59 9.6422042846679687 60 11.898929595947266 61 12.804666519165039
		 62 10.648750305175781 63 7.6474571228027353 64 4.5416746139526367 65 1.8430519104003908
		 66 -0.22639112174510956 67 -1.6796931028366089 68 -2.6564700603485107 69 -3.3756716251373291
		 70 -3.8549306392669678 71 -3.6786077022552486 72 -2.4803378582000732 73 0.17606936395168304
		 74 3.7261056900024414 75 6.9496955871582031 76 9.306431770324707 77 11.03501033782959
		 78 12.636417388916016 79 14.158832550048828 80 15.369526863098146 81 15.870827674865724
		 82 15.729303359985353 83 15.267047882080078 84 14.740283966064455 85 13.78633975982666
		 86 12.327432632446289 87 11.014616012573242 88 10.653458595275879 89 12.311119079589844
		 90 15.756232261657715 91 18.97599983215332 92 19.140058517456055 93 15.014979362487793
		 94 9.5637779235839844 95 5.1736035346984863 96 2.3892674446105957 97 0.99698251485824596
		 98 0.38452437520027161 99 0.11309286952018738 100 -0.42171391844749451 101 -1.3777214288711548
		 102 -2.3952903747558594 103 -3.3724403381347656 104 -4.151402473449707 105 -4.8286738395690918
		 106 -5.5521039962768555 107 -6.1883058547973633 108 -6.4454851150512695 109 -6.0178980827331543
		 110 -5.0184645652770996 111 -3.8399946689605717 112 -3.1723263263702393 113 -3.3316123485565186
		 114 -3.9957048892974854 115 -4.9504890441894531 116 -5.8365564346313477 117 -6.514427661895752
		 118 -7.1848330497741699 119 -7.9377717971801749 120 -8.7037343978881836;
createNode animCurveTA -n "Character1_RightHand_rotateY";
	rename -uid "E56EC5B6-4DBA-E167-1969-66BC34ADF4A3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 0.5537753701210022 1 -2.6608180999755859
		 2 -6.3169960975646973 3 -9.1528711318969727 4 -9.8750982284545898 5 -7.2396225929260245
		 6 -2.2126948833465576 7 3.1681501865386963 8 6.8305387496948242 9 7.6173110008239755
		 10 6.7473893165588379 11 5.748112678527832 12 6.1524820327758789 13 9.0559520721435547
		 14 13.457313537597656 15 17.714643478393555 16 20.067476272583008 17 19.539297103881836
		 18 17.267705917358398 19 14.59592819213867 20 12.824755668640137 21 12.206306457519531
		 22 12.08668041229248 23 12.441316604614258 24 13.297651290893555 25 15.477416992187498
		 26 18.652544021606445 27 21.239507675170898 28 21.900444030761719 29 20.314065933227539
		 30 17.354190826416016 31 13.582793235778809 32 9.6668510437011719 33 6.3876509666442871
		 34 3.3154265880584717 35 0.16278156638145447 36 -2.0082380771636963 37 -2.1286978721618652
		 38 1.0039545297622681 39 6.4971857070922852 40 12.239944458007813 41 16.074905395507813
		 42 16.689596176147461 43 15.277864456176758 44 13.540547370910645 45 13.270538330078125
		 46 15.71296977996826 47 19.764205932617188 48 23.722324371337891 49 25.853338241577148
		 50 25.245149612426758 51 23.064493179321289 52 20.65478515625 53 19.330142974853516
		 54 19.439540863037109 55 20.254644393920898 56 21.535579681396484 57 23.124116897583008
		 58 25.606256484985352 59 28.803823471069336 60 31.545064926147461 61 32.975200653076172
		 62 31.787235260009762 63 29.320457458496094 64 25.87483024597168 65 21.914957046508789
		 66 18.059450149536133 67 15.022686004638672 68 13.55640697479248 69 15.752400398254393
		 70 21.374101638793945 71 27.616706848144531 72 31.621562957763675 73 31.943857192993164
		 74 30.077619552612305 75 27.726104736328125 76 26.87022590637207 77 28.890357971191406
		 78 32.625892639160156 79 36.384780883789063 80 38.466224670410156 81 37.991558074951172
		 82 36.081623077392578 83 34.010631561279297 84 33.034709930419922 85 33.551921844482422
		 86 34.821117401123047 87 36.463665008544922 88 38.189373016357422 89 40.686351776123047
		 90 43.682453155517578 91 45.435882568359375 92 44.759132385253906 93 40.309032440185547
		 94 32.642192840576172 95 24.327030181884766 96 18.532554626464844 97 17.276859283447266
		 98 18.804824829101563 99 20.738260269165039 100 20.740987777709961 101 17.880533218383789
		 102 13.732000350952148 103 9.7316818237304687 104 7.262073040008544 105 6.6464624404907227
		 106 6.9038505554199219 107 7.6215877532958984 108 8.8290948867797852 109 11.42119312286377
		 110 15.069717407226563 111 18.006261825561523 112 18.578851699829102 113 15.429372787475586
		 114 9.8151521682739258 115 4.0150146484375 116 0.17803622782230377 117 -0.97301250696182251
		 118 -0.70455622673034668 119 0.10437865555286407 120 0.5537753701210022;
createNode animCurveTA -n "Character1_RightHand_rotateZ";
	rename -uid "EACA6AF3-4E37-10AB-1C47-009211AB72F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -28.988409042358395 1 -30.801887512207031
		 2 -32.701522827148438 3 -34.428424835205078 4 -35.781639099121094 5 -36.996723175048828
		 6 -38.062885284423828 7 -38.452201843261719 8 -37.689022064208984 9 -35.016887664794922
		 10 -30.890085220336914 11 -26.655397415161133 12 -23.627262115478516 13 -22.261869430541992
		 14 -21.636211395263672 15 -21.095218658447266 16 -20.208911895751953 17 -18.92725944519043
		 18 -17.576557159423828 19 -16.413108825683594 20 -15.738076210021973 21 -16.108539581298828
		 22 -17.257204055786133 23 -18.288312911987305 24 -18.285856246948242 25 -16.432180404663086
		 26 -13.296200752258301 27 -10.213000297546387 28 -8.8231639862060547 29 -9.6871709823608398
		 30 -11.742710113525391 31 -14.246830940246582 32 -16.577676773071289 33 -18.300575256347656
		 34 -19.504495620727539 35 -20.441917419433594 36 -21.009971618652344 37 -21.118165969848633
		 38 -20.882627487182617 39 -20.220359802246094 40 -18.7742919921875 41 -16.310110092163086
		 42 -12.32069206237793 43 -7.254589080810546 44 -2.3130216598510742 45 1.5019766092300415
		 46 4.0628719329833984 47 6.1359000205993652 48 7.9793429374694824 49 9.4982233047485352
		 50 10.412901878356934 51 10.73487663269043 52 10.622111320495605 53 10.126063346862793
		 54 8.7388830184936523 55 6.5565352439880371 56 4.477750301361084 57 3.4659781455993652
		 58 4.4961905479431152 59 7.0166516304016113 60 9.5470695495605469 61 10.203950881958008
		 62 6.9566435813903809 63 2.4133388996124268 64 -2.6245970726013184 65 -7.5377616882324228
		 66 -11.92682933807373 67 -15.550414085388185 68 -18.233596801757813 69 -19.766899108886719
		 70 -20.138179779052734 71 -19.345579147338867 72 -17.477260589599609 73 -14.042183876037598
		 74 -9.3308620452880859 75 -4.6731600761413574 76 -1.0926320552825928 77 1.3482937812805176
		 78 3.4317374229431152 79 5.3435511589050293 80 6.8300480842590332 81 7.4363222122192383
		 82 7.2794528007507315 83 6.7027149200439453 84 5.9070134162902832 85 4.3422064781188965
		 86 2.0174989700317383 87 -0.075338259339332581 88 -0.80961549282073975 89 1.077790379524231
		 90 5.0777835845947266 91 8.7643003463745117 92 9.0548238754272461 93 4.6866793632507324
		 94 -1.0797107219696045 95 -5.574617862701416 96 -8.1888408660888672 97 -9.316411018371582
		 98 -9.7005271911621094 99 -9.7717609405517578 100 -10.131283760070801 101 -10.78679084777832
		 102 -11.300816535949707 103 -11.806278228759766 104 -12.599734306335449 105 -14.254714012145996
		 106 -16.623302459716797 107 -18.939809799194336 108 -20.281276702880859 109 -20.124807357788086
		 110 -18.942962646484375 111 -17.420183181762695 112 -16.473642349243164 113 -16.180938720703125
		 114 -15.957252502441408 115 -15.946979522705076 116 -16.684711456298828 117 -18.792411804199219
		 118 -21.970010757446289 119 -25.582656860351562 120 -28.988409042358395;
createNode animCurveTU -n "Character1_RightHand_visibility";
	rename -uid "8099C329-4E0F-7D23-B655-F18B6F890AA1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb1_translateX";
	rename -uid "E185AC0F-41B2-89EB-F155-80BFBDCEEB79";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2155418395996094;
createNode animCurveTL -n "Character1_RightHandThumb1_translateY";
	rename -uid "FFA436E4-4A1D-DB6A-D453-C4AE9AE01683";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8998895883560181;
createNode animCurveTL -n "Character1_RightHandThumb1_translateZ";
	rename -uid "72F43AB5-4EF4-37A3-D86F-66A85AD500BF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3098368644714355;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleX";
	rename -uid "8359F049-4F20-FD7E-AA6E-198427F3E131";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleY";
	rename -uid "D5A06EBD-448B-7B8E-A8CD-6C91E59671D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleZ";
	rename -uid "A0D17DDA-4178-E85C-326E-1599CD66093F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateX";
	rename -uid "A37DEBD3-47B2-4E55-6C29-BBAD888796D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -7.8129382133483887 1 -6.9239959716796875
		 2 -6.014437198638916 3 -5.1293091773986816 4 -4.3148021697998047 5 -3.2071175575256348
		 6 -1.9013518095016477 7 -1.1516845226287842 8 -1.724290132522583 9 -4.6078848838806152
		 10 -9.0233688354492187 11 -13.193452835083008 12 -15.515753746032715 13 -15.321266174316406
		 14 -13.59180736541748 15 -11.255703926086426 16 -9.274714469909668 17 -7.6459941864013681
		 18 -5.9380607604980469 19 -4.5210456848144531 20 -3.7791523933410649 21 -3.9166862964630131
		 22 -4.701667308807373 23 -5.8654308319091797 24 -7.1432571411132813 25 -8.8208026885986328
		 26 -10.901021003723145 27 -12.725590705871582 28 -13.664440155029297 29 -13.501516342163086
		 30 -12.674857139587402 31 -11.542828559875488 32 -10.469877243041992 33 -9.8298759460449219
		 34 -9.8313865661621094 35 -10.231207847595215 36 -10.730044364929199 37 -11.031329154968262
		 38 -10.993054389953613 39 -10.794588088989258 40 -10.623675346374512 41 -10.668715476989746
		 42 -11.259461402893066 43 -12.197873115539551 44 -12.888369560241699 45 -12.744050979614258
		 46 -11.233892440795898 47 -8.7669801712036133 48 -6.1865119934082031 49 -4.3896317481994629
		 50 -3.6006007194519043 51 -3.3690395355224609 52 -3.612295389175415 53 -4.242760181427002
		 54 -5.6532659530639648 55 -7.7271008491516122 56 -9.6380348205566406 57 -10.59904670715332
		 58 -10.146546363830566 59 -8.7888727188110352 60 -7.2152323722839364 61 -6.1382684707641602
		 62 -5.9910068511962891 63 -6.3303747177124023 64 -6.5721035003662109 65 -6.5207314491271973
		 66 -6.3993096351623535 67 -6.2542052268981934 68 -6.1319332122802734 69 -6.0334382057189941
		 70 -5.9377460479736328 71 -5.8586444854736328 72 -5.8099484443664551 73 -5.8264284133911133
		 74 -5.8903665542602539 75 -5.9368882179260254 76 -5.9011874198913574 77 -5.7234659194946289
		 78 -5.452056884765625 79 -5.1846790313720703 80 -5.0197372436523437 81 -4.8602662086486816
		 82 -4.6949658393859863 83 -4.7517256736755371 84 -5.2573575973510742 85 -6.5493388175964355
		 86 -8.395634651184082 87 -10.183181762695312 88 -11.32603645324707 89 -11.741792678833008
		 90 -11.730203628540039 91 -11.288839340209961 92 -10.411688804626465 93 -8.5754823684692383
		 94 -5.9199886322021484 95 -3.4714770317077637 96 -2.3242743015289307 97 -3.2069995403289795
		 98 -5.3860034942626953 99 -7.808922767639161 100 -9.4820222854614258 101 -10.272428512573242
		 102 -10.68222713470459 103 -10.704809188842773 104 -10.330657005310059 105 -9.2473239898681641
		 106 -7.5480499267578116 107 -5.843045711517334 108 -4.7691802978515625 109 -4.5537829399108887
		 110 -4.8480772972106934 111 -5.4374270439147949 112 -6.107722282409668 113 -7.0168266296386719
		 114 -8.1999750137329102 115 -9.2695331573486328 116 -9.8483419418334961 117 -9.7447614669799805
		 118 -9.1933250427246094 119 -8.459686279296875 120 -7.8129382133483887;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateY";
	rename -uid "742F0130-4764-ADDE-AB26-07B625810745";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 24.506990432739258 1 24.987037658691406
		 2 25.47187614440918 3 25.93743896484375 4 26.360326766967773 5 26.926809310913086
		 6 27.581649780273437 7 27.951200485229492 8 27.669355392456055 9 26.208774566650391
		 10 23.843679428100586 11 21.477258682250977 12 20.108642578125 13 20.224559783935547
		 14 21.244949340820313 15 22.592086791992188 16 23.704570770263672 17 24.597606658935547
		 18 25.512292861938477 19 26.253749847412109 20 26.63551139831543 21 26.56507682800293
		 22 26.160131454467773 23 25.550685882568359 24 24.869197845458984 25 23.955451965332031
		 26 22.7933349609375 27 21.748762130737305 28 21.202480316162109 29 21.297697067260742
		 30 21.778116226196289 31 22.428525924682617 32 23.036767959594727 33 23.395664215087891
		 34 23.394819259643555 35 23.170951843261719 36 22.890029907226563 37 22.719499588012695
		 38 22.741199493408203 39 22.853551864624023 40 22.950080871582031 41 22.924663543701172
		 42 22.589950561523438 43 22.05323600769043 44 21.65446662902832 45 21.738077163696289
		 46 22.604488372802734 47 23.985097885131836 48 25.380647659301758 49 26.321697235107422
		 50 26.726722717285156 51 26.844625473022461 52 26.720756530761719 53 26.397472381591797
		 54 25.662595748901367 55 24.553609848022461 56 23.502664566040039 57 22.963973999023438
		 58 23.218452453613281 59 23.973041534423828 60 24.830432891845703 61 25.40625 62 25.484279632568359
		 63 25.30419921875 64 25.175378799438477 65 25.202793121337891 66 25.267509460449219
		 67 25.344696044921875 68 25.409610748291016 69 25.461814880371094 70 25.512460708618164
		 71 25.554271697998047 72 25.579984664916992 73 25.571285247802734 74 25.537509918212891
		 75 25.512914657592773 76 25.531791687011719 77 25.625608444213867 78 25.768400192260742
		 79 25.908496856689453 80 25.994638442993164 81 26.077713012695313 82 26.163610458374023
		 83 26.134140014648438 84 25.870471954345703 85 25.187530517578125 86 24.189050674438477
		 87 23.197902679443359 88 22.552074432373047 89 22.314846038818359 90 22.321475982666016
		 91 22.573240280151367 92 23.069520950317383 93 24.090404510498047 94 25.5218505859375
		 95 26.792522430419922 96 27.371099472045898 97 26.926868438720703 98 25.803062438964844
		 99 24.509172439575195 100 23.589483261108398 101 23.147806167602539 102 22.917034149169922
		 103 22.90428352355957 104 23.115089416503906 105 23.719751358032227 106 24.650669097900391
		 107 25.562509536743164 108 26.125072479248047 109 26.236801147460938 110 26.084054946899414
		 111 25.776079177856445 112 25.422449111938477 113 24.937191009521484 114 24.296096801757813
		 115 23.707443237304687 116 23.385349273681641 117 23.443168640136719 118 23.749666213989258
		 119 24.153945922851563 120 24.506990432739258;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateZ";
	rename -uid "5F6E0D46-4C97-9E94-D0B6-38A2E809996B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 45.780338287353516 1 46.001533508300781
		 2 46.235771179199219 3 46.471286773681641 4 46.694522857666016 5 47.007968902587891
		 6 47.39178466796875 7 47.619014739990234 8 47.445003509521484 9 46.613483428955078
		 10 45.491615295410156 11 44.610313415527344 12 44.198223114013672 13 44.230522155761719
		 14 44.535552978515625 15 44.997642517089844 16 45.433486938476562 17 45.821292877197266
		 18 46.25579833984375 19 46.637413024902344 20 46.844688415527344 21 46.805881500244141
		 22 46.587722778320312 23 46.274898529052734 24 45.946262359619141 25 45.538921356201172
		 26 45.072742462158203 27 44.700248718261719 28 44.522102355957031 29 44.552352905273438
		 30 44.710140228271484 31 44.937789916992188 32 45.165763854980469 33 45.307334899902344
		 34 45.306995391845703 35 45.218074798583984 36 45.109405517578125 37 45.044998168945313
		 38 45.053131103515625 39 45.095527648925781 40 45.132362365722656 41 45.122627258300781
		 42 44.996852874755859 43 44.804439544677734 44 44.668697357177734 45 44.696659088134766
		 46 45.002223968505859 47 45.551559448242188 48 46.190849304199219 49 46.673755645751953
		 50 46.895332336425781 51 46.961444854736328 52 46.892005920410156 53 46.714565277099609
		 54 46.330974578857422 55 45.801361083984375 56 45.350578308105469 57 45.1376953125
		 58 45.236766815185547 59 45.546413421630859 60 45.928218841552734 61 46.203414916992188
		 62 46.241909027099609 63 46.153507232666016 64 46.091213226318359 65 46.104404449462891
		 66 46.135684967041016 67 46.173252105712891 68 46.205066680908203 69 46.230796813964844
		 70 46.255882263183594 71 46.276687622070313 72 46.289524078369141 73 46.285175323486328
		 74 46.268337249755859 75 46.256107330322266 76 46.265491485595703 77 46.312374114990234
		 78 46.384552001953125 79 46.456336975097656 80 46.500957489013672 81 46.544338226318359
		 82 46.589557647705078 83 46.574005126953125 84 46.436759948730469 85 46.097057342529297
		 86 45.639537811279297 87 45.228668212890625 88 44.982902526855469 89 44.896816253662109
		 90 44.899192810058594 91 44.990692138671875 92 45.178466796875 93 45.596759796142578
		 94 46.260547637939453 95 46.932140350341797 96 47.265792846679688 97 47.008003234863281
		 98 46.4022216796875 99 45.781318664550781 100 45.386013031005859 101 45.208995819091797
		 102 45.119709014892578 103 45.114841461181641 104 45.196205139160156 105 45.439788818359375
		 106 45.845443725585938 107 46.280792236328125 108 46.569225311279297 109 46.628383636474609
		 110 46.547664642333984 111 46.388462066650391 112 46.211383819580078 113 45.978076934814453
		 114 45.686443328857422 115 45.434677124023438 116 45.303192138671875 117 45.326473236083984
		 118 45.452236175537109 119 45.624263763427734 120 45.780338287353516;
createNode animCurveTU -n "Character1_RightHandThumb1_visibility";
	rename -uid "FA4BC66D-4408-27B5-8F31-C0BABDCA2CC1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb2_translateX";
	rename -uid "FCB70153-4D4B-22B2-06F1-D79498A84167";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9627087116241455;
createNode animCurveTL -n "Character1_RightHandThumb2_translateY";
	rename -uid "720C13E1-4E9B-4D5F-C94D-CF9BF7BB059D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5399179458618164;
createNode animCurveTL -n "Character1_RightHandThumb2_translateZ";
	rename -uid "3437DEB8-4A79-AF5E-C596-B08B2BCAA2A9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6609530448913574;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleX";
	rename -uid "033B07A2-4EAD-9B65-601A-00A0F244EACD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleY";
	rename -uid "03664C88-4BC8-FD32-A694-488CF53789E6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleZ";
	rename -uid "82312D1C-4D04-2F1B-D50C-93A5E1DC11A9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateX";
	rename -uid "4EE3AE43-478C-F8F0-8CDD-42B721815A22";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 5.4960494041442871 1 4.6379799842834473
		 2 3.7652850151062007 3 2.9211187362670898 4 2.1487078666687012 5 1.1049891710281372
		 6 -0.11553466320037842 7 -0.81150597333908081 8 -0.28022527694702148 9 2.4261593818664551
		 10 6.6726899147033691 11 10.799969673156738 12 13.148311614990234 13 12.950268745422363
		 14 11.200247764587402 15 8.8678512573242187 16 6.9182167053222656 17 5.3345131874084473
		 18 3.6922457218170166 19 2.343895435333252 20 1.6430286169052124 21 1.7726949453353882
		 22 2.51505446434021 23 3.62282395362854 24 4.8491511344909668 25 6.4751129150390625
		 26 8.5168857574462891 27 10.331191062927246 28 11.273344993591309 29 11.109427452087402
		 30 10.280445098876953 31 9.1525745391845703 32 8.0913753509521484 33 7.4619913101196289
		 34 7.4634742736816415 35 7.856351375579834 36 8.347996711730957 37 8.6457309722900391
		 38 8.6078739166259766 39 8.4117288589477539 40 8.2430238723754883 41 8.2874631881713867
		 42 8.8715753555297852 43 9.8041801452636719 44 10.494124412536621 45 10.349660873413086
		 46 8.8462457656860352 47 6.422661304473877 48 3.9299781322479248 49 2.2194952964782715
		 50 1.4748678207397461 51 1.2570803165435791 52 1.4858754873275757 53 2.0805912017822266
		 54 3.4202234745025635 55 5.4129700660705566 56 7.273857593536377 57 8.2187290191650391
		 58 7.773073673248291 59 6.4439949989318848 60 4.9185395240783691 61 3.8837852478027339
		 62 3.7428743839263912 63 4.0678153038024902 64 4.2997188568115234 65 4.2504034042358398
		 66 4.1339101791381836 67 3.9948194026947026 68 3.8777196407318111 69 3.7834615707397461
		 70 3.6919450759887695 71 3.6163392066955566 72 3.5698153972625732 73 3.5855584144592281
		 74 3.6466538906097412 75 3.6911244392395015 76 3.6569960117340088 77 3.4872262477874756
		 78 3.2283506393432617 79 2.9737789630889893 80 2.8169641494750977 81 2.6655149459838867
		 82 2.5086996555328369 83 2.5625269412994385 84 3.0429317951202393 85 4.277862548828125
		 86 6.0612878799438477 87 7.8091039657592773 88 8.9375448226928711 89 9.3501949310302734
		 90 9.3386764526367187 91 8.9006814956665039 92 8.0340414047241211 93 6.2361931800842285
		 94 3.6749682426452637 95 1.3533830642700195 96 0.27862119674682617 97 1.104878306388855
		 98 3.1654188632965088 99 5.492161750793457 100 7.1210365295410156 101 7.8969154357910156
		 102 8.3007974624633789 103 8.3230867385864258 104 7.9542360305786133 105 6.8914403915405273
		 106 5.2398257255554199 107 3.601434469223022 108 2.5790836811065674 109 2.3749020099639893
		 110 2.6539461612701416 111 3.2144098281860352 112 3.8545444011688228 113 4.7273478507995605
		 114 5.8712449073791504 115 6.913151741027832 116 7.4801149368286142 117 7.378492832183837
		 118 6.8386669158935547 119 6.1235561370849609 120 5.4960494041442871;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateY";
	rename -uid "A0DE54D2-492E-4560-FA61-B5AD9830219D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -2.6941189765930176 1 -2.293837308883667
		 2 -1.8878314495086668 3 -1.4962770938873291 4 -1.1391249895095825 5 -0.65838927030563354
		 6 -0.099210023880004883 7 0.21807590126991272 8 -0.024022448807954788 9 -1.2672857046127319
		 10 -3.2445163726806641 11 -5.1842856407165527 12 -6.2903165817260742 13 -6.1970682144165039
		 14 -5.3728127479553223 15 -4.2749342918395996 16 -3.3595590591430664 17 -2.618687629699707
		 18 -1.8539055585861208 19 -1.2292709350585937 20 -0.90593075752258301 21 -0.96567755937576283
		 22 -1.3083792924880981 23 -1.821668267250061 24 -2.3922522068023682 25 -3.1519861221313477
		 26 -4.1099429130554199 27 -4.9635348320007324 28 -5.4072427749633789 29 -5.3300352096557617
		 30 -4.9396414756774902 31 -4.4088377952575684 32 -3.9100177288055415 33 -3.614554882049561
		 34 -3.6152508258819585 35 -3.7996487617492671 36 -4.0305752754211426 37 -4.1705050468444824
		 38 -4.1527094841003418 39 -4.060523509979248 40 -3.9812545776367187 41 -4.0021328926086426
		 42 -4.2766852378845215 43 -4.7154369354248047 44 -5.0402555465698242 45 -4.972231388092041
		 46 -4.2647747993469238 47 -3.1274287700653076 48 -1.9643609523773191 49 -1.171809196472168
		 50 -0.82849806547164917 51 -0.72830164432525635 52 -0.83356499671936035 53 -1.107683539390564
		 54 -1.7276325225830078 55 -2.6553196907043457 56 -3.5263011455535889 57 -3.9698412418365483
		 58 -3.7605516910552983 59 -3.1374166011810303 60 -2.4246041774749756 61 -1.9428917169570923
		 62 -1.8774211406707766 63 -2.028444766998291 64 -2.1363286972045898 65 -2.1133797168731689
		 66 -2.0591840744018555 67 -1.994503378868103 68 -1.9400726556777956 69 -1.8962754011154175
		 70 -1.8537658452987669 71 -1.8186572790145874 72 -1.7970579862594602 73 -1.8043665885925295
		 74 -1.8327332735061646 75 -1.8533849716186521 76 -1.8375358581542969 77 -1.7587239742279053
		 78 -1.6386404037475586 79 -1.5206667184829712 80 -1.4480530023574829 81 -1.3779662847518921
		 82 -1.30544114112854 83 -1.330330491065979 84 -1.5527021884918213 85 -2.126157283782959
		 86 -2.9583199024200439 87 -3.7774662971496578 88 -4.307706356048584 89 -4.5018024444580078
		 90 -4.4963836669921875 91 -4.2903714179992676 92 -3.8830888271331787 93 -3.0401504039764404
		 94 -1.8458817005157471 95 -0.77259492874145508 96 -0.27941909432411194 97 -0.65833854675292969
		 98 -1.6094658374786377 99 -2.6923027038574219 100 -3.4546360969543457 101 -3.8186948299407959
		 102 -4.0083985328674316 103 -4.0188708305358887 104 -3.8456108570098873 105 -3.3470098972320557
		 106 -2.5744884014129639 107 -1.811737060546875 108 -1.3379871845245361 109 -1.2435978651046753
		 110 -1.3726143836975098 111 -1.6321768760681152 112 -1.9293030500411987 113 -2.3354787826538086
		 114 -2.8694477081298828 115 -3.3571853637695313 116 -3.6230583190917964 117 -3.5753817558288574
		 118 -3.3222792148590088 119 -2.9874489307403564 120 -2.6941189765930176;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateZ";
	rename -uid "9A646A4E-494D-49B5-57C0-DA84F9778255";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 118 ".ktv[0:117]"  0 0.59695249795913696 1 0.53505271673202515
		 2 0.46598741412162781 3 0.3933413028717041 4 0.32186219096183777 5 0.21771255135536194
		 6 0.084961988031864166 7 0.0040105343796312809 8 0.066149793565273285 9 0.3480873703956604
		 10 0.67210620641708374 11 0.84613567590713501 12 0.88253104686737061 13 0.88122087717056274
		 14 0.85555881261825562 15 0.78208231925964355 16 0.68636524677276611 17 0.58575540781021118
		 18 0.45992839336395258 19 0.34037581086158752 20 0.27248507738113403 21 0.28534075617790222
		 22 0.35635951161384583 23 0.4541297554969787 24 0.55083966255187988 25 0.6602744460105896
		 26 0.76715165376663208 27 0.83341962099075317 28 0.85713672637939453 29 0.85353660583496094
		 30 0.83193457126617432 31 0.79345160722732544 32 0.74769407510757446 33 0.71619242429733276
		 34 0.7162703275680542 35 0.73631000518798828 36 0.75960612297058105 37 0.77275007963180542
		 38 0.77111923694610596 39 0.7624809741973877 40 0.75479859113693237 41 0.75684499740600586
		 42 0.78223526477813721 43 0.81696164608001709 44 0.83804476261138916 45 0.83395493030548096
		 46 0.7811923623085022 47 0.65707981586456299 48 0.4794915616512298 49 0.32861155271530151
		 50 0.25561335682868958 51 0.23342862725257874 52 0.25672465562820435 53 0.31532987952232361
		 54 0.4369848370552063 55 0.59121996164321899 56 0.70614618062973022 57 0.75367313623428345
		 58 0.73216778039932251 59 0.65838181972503662 60 0.55594807863235474 61 0.47572594881057739
		 62 0.46413281559944158 63 0.49062600731849676 64 0.50901263952255249 65 0.50513893365859985
		 66 0.49591037631034851 67 0.4847482442855835 68 0.47522997856140137 69 0.46748816967010498
		 70 0.45990309119224543 71 0.45358586311340332 75 0.45983493328094477 76 0.45698878169059753
		 77 0.44269135594367981 78 0.42044341564178467 79 0.3980402946472168 80 0.38398122787475586
		 81 0.37021613121032715 82 0.35576978325843811 83 0.36075073480606079 84 0.40417730808258057
		 85 0.50729763507843018 86 0.63446122407913208 87 0.73396646976470947 88 0.78492635488510132
		 89 0.80095022916793823 90 0.80052191019058228 91 0.78342658281326294 92 0.74495786428451538
		 93 0.64554125070571899 94 0.45848846435546869 95 0.24328480660915372 96 0.12911863625049591
		 97 0.21770106256008148 98 0.41495370864868164 99 0.59668487310409546 100 0.6977725625038147
		 101 0.73830693960189819 102 0.75745576620101929 103 0.75847357511520386 104 0.74110621213912964
		 105 0.6848340630531311 106 0.57909387350082397 107 0.45233526825904846 108 0.36227864027023315
		 109 0.34328877925872803 110 0.36915755271911621 111 0.41923025250434875 112 0.47333347797393799
		 113 0.54177767038345337 114 0.62214028835296631 115 0.68607616424560547 116 0.7171446681022644
		 117 0.71176934242248535 118 0.68179947137832642 119 0.63843518495559692 120 0.59695249795913696;
createNode animCurveTU -n "Character1_RightHandThumb2_visibility";
	rename -uid "0F62748D-47D9-B3F9-E596-948E86E0D19F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb3_translateX";
	rename -uid "D5E95D84-4FEE-91B4-CDE5-23AB71673094";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.5867142677307129;
createNode animCurveTL -n "Character1_RightHandThumb3_translateY";
	rename -uid "F4E1916E-4DF3-284A-C157-BEA761F05D7B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.6152248382568359;
createNode animCurveTL -n "Character1_RightHandThumb3_translateZ";
	rename -uid "C6F0F4A5-45A9-17F3-345F-CB8E90493F55";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.4791715145111084;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleX";
	rename -uid "BC3FF973-45EC-2B63-FE7B-879385025DE5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleY";
	rename -uid "B212AF14-47F3-E0F8-A1DF-589532A639ED";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleZ";
	rename -uid "40B4D3B9-42BC-58BD-15C0-8A9C73290B81";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateX";
	rename -uid "0D71C6D1-4047-13C8-E1E8-889FAE0934A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4433742023811647e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateY";
	rename -uid "0900BC53-4974-0017-015E-5CBF1F91260C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.1235418747382937e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateZ";
	rename -uid "41A14BC7-439C-DC97-12EF-50A14BF392C6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.547976040683352e-007;
createNode animCurveTU -n "Character1_RightHandThumb3_visibility";
	rename -uid "4E4B0C6B-44D1-3917-9A1F-54927B6FF945";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex1_translateX";
	rename -uid "132DD00B-4FA9-E6E3-1428-808D204257D5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.615594863891602;
createNode animCurveTL -n "Character1_RightHandIndex1_translateY";
	rename -uid "BE72FF93-42AC-18EE-7D45-97AE5E0FE7E6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4688365459442139;
createNode animCurveTL -n "Character1_RightHandIndex1_translateZ";
	rename -uid "EA8697D2-4461-8994-31AA-689778C0F1EE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.3266587257385254;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleX";
	rename -uid "1EA58F6B-4DFD-AF2E-D120-B7BE6BF3FCBD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleY";
	rename -uid "ED0AB5A5-4745-7D25-2E99-58849F3313F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleZ";
	rename -uid "0C1EDECA-46AE-690B-0E62-9E8B1BF4E08E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateX";
	rename -uid "17449CB2-4590-DC58-94E8-A4B2C1FD4EBF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 3.6655156612396245 1 3.953294992446899
		 2 4.249424934387207 3 4.539330005645752 4 4.8076987266540527 5 5.175262451171875
		 6 5.6126542091369629 7 5.8658976554870605 8 5.6723232269287109 9 4.7109508514404297
		 10 3.2760848999023438 11 1.9526124000549316 12 1.2251400947570801 13 1.2858610153198242
		 14 1.8274191617965698 15 2.5644400119781494 16 3.1955499649047852 17 3.7194418907165523
		 18 4.2743716239929199 19 4.7395949363708496 20 4.9850592613220215 21 4.9394521713256836
		 22 4.6800360679626465 23 4.2981066703796387 24 3.8821666240692143 25 3.3410704135894775
		 26 2.6769874095916748 27 2.0998907089233398 28 1.8046115636825562 29 1.8557791709899905
		 30 2.1158773899078369 31 2.4734635353088379 32 2.8140518665313721 33 3.0180501937866211
		 34 3.0175678730010986 35 2.8900504112243652 36 2.7313086986541748 37 2.6356167793273926
		 38 2.6477658748626709 39 2.7107973098754883 40 2.7651255130767822 41 2.7508044242858887
		 42 2.5632486343383789 43 2.2663402557373047 44 2.0486195087432861 45 2.0940747261047363
		 46 2.5713558197021484 47 3.3583498001098633 48 4.1932668685913086 49 4.7829775810241699
		 50 5.0443391799926758 51 5.1213383674621582 52 5.0404539108276367 53 4.8315114974975586
		 54 4.3675069808959961 55 3.6932358741760254 56 3.0793282985687256 57 2.7729580402374268
		 58 2.9170303344726562 59 3.3513205051422119 60 3.8588385581970215 61 4.1943960189819336
		 62 4.2003517150878906 63 4.0234694480895996 64 3.857300758361816 65 3.7671456336975098
		 66 3.6832778453826909 67 3.5934045314788823 68 3.4854106903076172 69 3.361814022064209
		 70 3.231978178024292 71 3.094240665435791 72 2.9469761848449707 73 2.7821474075317383
		 74 2.6079685688018799 75 2.4468176364898682 76 2.3047084808349609 77 2.1873269081115723
		 78 2.084134578704834 79 1.9700692892074583 80 1.8204401731491087 81 1.6687741279602051
		 82 1.5227400064468384 83 1.3200739622116089 84 0.98819828033447266 85 0.45425152778625488
		 86 -0.21894881129264832 87 -0.88050985336303711 88 -1.3427715301513672 89 -1.5625406503677368
		 90 -1.6077200174331665 91 -1.4880387783050537 92 -1.2266669273376465 93 -0.67626744508743286
		 94 0.11806663125753404 95 0.86132502555847168 96 1.2412623167037964 97 1.0464018583297729
		 98 0.48225966095924372 99 -0.15375640988349915 100 -0.57662612199783325 101 -0.74034488201141357
		 102 -0.78213012218475342 103 -0.6926957368850708 104 -0.46200677752494812 105 0.0086544323712587357
		 106 0.68423348665237427 107 1.3741170167922974 108 1.8908785581588745 109 2.1628553867340088
		 110 2.2890465259552002 111 2.3307015895843506 112 2.3505485057830811 113 2.2990958690643311
		 114 2.1636648178100586 115 2.0617306232452393 116 2.1095201969146729 117 2.3683757781982422
		 118 2.7686717510223389 119 3.2294697761535645 120 3.6655156612396245;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateY";
	rename -uid "77892340-4D99-371B-57B1-AF9BC314DAE0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -12.402667999267578 1 -13.308293342590332
		 2 -14.228392601013184 3 -15.117377281188963 4 -15.929804801940918 5 -17.025934219360352
		 6 -18.305063247680664 7 -19.033035278320312 8 -18.477422714233398 9 -15.638093948364256
		 10 -11.159510612487793 11 -6.7911648750305176 12 -4.3038730621337891 13 -4.5136022567749023
		 14 -6.3671860694885254 15 -8.8371906280517578 16 -10.899937629699707 17 -12.573223114013672
		 18 -14.305352210998535 19 -15.724598884582521 20 -16.461126327514648 21 -16.324928283691406
		 22 -15.544602394104004 23 -14.37849235534668 24 -13.085500717163086 25 -11.368350982666016
		 26 -9.2086944580078125 27 -7.2876734733581543 28 -6.2897601127624512 29 -6.4633846282958984
		 30 -7.3414187431335458 31 -8.5357627868652344 32 -9.6590108871459961 33 -10.324877738952637
		 34 -10.323308944702148 35 -9.9076900482177734 36 -9.3874416351318359 37 -9.0723152160644531
		 38 -9.1123867034912109 39 -9.3199901580810547 40 -9.4985342025756836 41 -9.4515047073364258
		 42 -8.8332490921020508 43 -7.8458027839660645 44 -7.1151061058044434 45 -7.2681121826171866
		 46 -8.8600625991821289 47 -11.423787117004395 48 -14.054834365844727 49 -15.855392456054689
		 50 -16.637712478637695 51 -16.866331100463867 52 -16.626153945922852 53 -16.001401901245117
		 54 -14.591903686523439 55 -12.490388870239258 56 -10.523861885070801 57 -9.5242443084716797
		 58 -9.9957981109619141 59 -11.401240348815918 60 -13.01228141784668 61 -14.058329582214355
		 62 -14.076755523681641 63 -13.527420997619629 64 -13.007452011108398 65 -12.723770141601563
		 66 -12.458888053894043 67 -12.173989295959473 68 -11.830223083496094 69 -11.434896469116211
		 70 -11.017454147338867 71 -10.572213172912598 72 -10.093483924865723 73 -9.5543985366821289
		 74 -8.9810552597045898 75 -8.4472866058349609 76 -7.9739971160888672 77 -7.5812606811523446
		 78 -7.2346730232238778 79 -6.8501439094543457 80 -6.3434991836547852 81 -5.8274316787719727
		 82 -5.3281807899475098 83 -4.6316094398498535 84 -3.4820015430450439 85 -1.6107875108718872
		 86 0.78190416097640991 87 3.1629478931427002 88 4.8400816917419434 89 5.6403541564941406
		 90 5.8050637245178223 91 5.3688826560974121 92 4.4179940223693848 93 2.4251630306243896
		 94 -0.42020240426063538 95 -3.0397102832794189 96 -4.3595952987670898 97 -3.6843955516815181
		 98 -1.7095636129379272 99 0.54874140024185181 100 2.0660512447357178 101 2.6563913822174072
		 102 2.8072962760925293 103 2.4844233989715576 104 1.6536738872528076 105 -0.030835505574941635
		 106 -2.4198753833770752 107 -4.817772388458252 108 -6.5823159217834473 109 -7.4991817474365243
		 110 -7.9216899871826181 111 -8.0607461929321289 112 -8.1269292831420898 113 -7.9552564620971689
		 114 -7.5018973350524893 115 -7.159264087677002 116 -7.3200488090515137 117 -8.1863365173339844
		 118 -9.5101757049560547 119 -11.009366989135742 120 -12.402667999267578;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateZ";
	rename -uid "86B21A9F-4ABD-0F1A-5010-97BDA13DCBC3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -1.4665682315826416 1 -1.6086199283599854
		 2 -1.7584311962127686 3 -1.9085773229599001 4 -2.0505654811859131 5 -2.2495813369750977
		 6 -2.4930324554443359 7 -2.6371734142303467 8 -2.5267877578735352 9 -1.9990508556365965
		 10 -1.2800450325012207 11 -0.6979948878288269 12 -0.41443896293640137 13 -0.43707016110420227
		 14 -0.64729076623916626 15 -0.95681548118591309 16 -1.2423082590103149 17 -1.4929176568984985
		 18 -1.7712171077728271 19 -2.0142643451690674 20 -2.145949125289917 21 -2.1213057041168213
		 22 -1.9826672077178953 23 -1.7834056615829468 24 -1.5731824636459351 25 -1.3107061386108398
		 26 -1.0063751935958862 27 -0.75863355398178101 28 -0.6381344199180603 29 -0.65870547294616699
		 30 -0.76528078317642212 31 -0.91718894243240356 32 -1.067534327507019 33 -1.1601661443710327
		 34 -1.1599448919296265 35 -1.1018205881118774 36 -1.0305101871490479 37 -0.98808968067169189
		 38 -0.99345165491104126 39 -1.0213813781738281 40 -1.0456035137176514 41 -1.0392051935195923
		 42 -0.95629394054412853 43 -0.82844924926757813 44 -0.73740434646606445 45 -0.75622135400772095
		 46 -0.95984357595443726 47 -1.3188910484313965 48 -1.7297416925430298 49 -2.0373673439025879
		 50 -2.1780998706817627 51 -2.2200608253479004 52 -2.1759884357452393 53 -2.0633008480072021
		 54 -1.8191765546798708 55 -1.4800971746444702 56 -1.1883628368377686 57 -1.0491069555282593
		 58 -1.1140557527542114 59 -1.3155583143234253 60 -1.5616036653518677 61 -1.7303140163421631
		 62 -1.7333517074584961 63 -1.6437900066375732 64 -1.5608441829681396 65 -1.5163305997848511
		 66 -1.4752333164215088 67 -1.4315296411514282 68 -1.3794780969619751 69 -1.3205335140228271
		 70 -1.2593417167663574 71 -1.1952505111694336 72 -1.127676248550415 73 -1.0532208681106567
		 74 -0.97591298818588257 75 -0.90565854310989369 76 -0.84473228454589844 77 -0.79514116048812866
		 78 -0.75209802389144897 79 -0.70512712001800537 80 -0.64448761940002441 81 -0.58416497707366943
		 82 -0.52718108892440796 83 -0.44990655779838562 84 -0.32797679305076599 85 -0.14411810040473938
		 86 0.065324634313583374 87 0.24593180418014526 88 0.35681530833244324 89 0.40501773357391357
		 90 0.41456365585327148 91 0.38900852203369141 92 0.3301728367805481 93 0.19289879500865936
		 94 -0.0363503098487854 95 -0.28289970755577087 96 -0.42042985558509827 97 -0.34894198179244995
		 98 -0.15337793529033661 99 0.046158939599990845 100 0.16613432765007019 101 0.20979976654052734
		 102 0.22069267928600311 103 0.19725343585014343 104 0.13463464379310608 105 -0.0026379749178886414
		 106 -0.22142143547534943 107 -0.47030520439147949 108 -0.67289483547210693 109 -0.78488618135452271
		 110 -0.83807539939880371 111 -0.85580116510391235 112 -0.86427617073059082 113 -0.84234321117401123
		 114 -0.78522372245788574 115 -0.7428210973739624 116 -0.76263833045959473 117 -0.87190866470336914
		 118 -1.0471893548965454 119 -1.2581667900085449 120 -1.4665682315826416;
createNode animCurveTU -n "Character1_RightHandIndex1_visibility";
	rename -uid "FECAF1DB-4A17-ACEA-449F-9B99575D01C0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex2_translateX";
	rename -uid "9A7EA168-4E4D-5D36-54D8-6BB33B848571";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.10917675495147705;
createNode animCurveTL -n "Character1_RightHandIndex2_translateY";
	rename -uid "C1D1FC0E-4CC6-4985-BCEA-7D84A0EE08F1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.54082572460174561;
createNode animCurveTL -n "Character1_RightHandIndex2_translateZ";
	rename -uid "218BDA94-4F5D-807D-23A4-09A1238CA8BA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.6861534118652344;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleX";
	rename -uid "A99D5421-4583-5B2E-586A-F69F4581EB42";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleY";
	rename -uid "7D4E23D6-4524-4BB7-89EC-A789390914F2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleZ";
	rename -uid "D2A78BD7-4523-848E-55F8-0AB0B2128821";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateX";
	rename -uid "F3511D02-4657-4C02-DC09-38B424AB6D00";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 16.027473449707031 1 16.719005584716797
		 2 17.431041717529297 3 18.128244400024414 4 18.773517608642578 5 19.656661987304687
		 6 20.705898284912109 7 21.31220817565918 8 20.848844528198242 9 18.540924072265625
		 10 15.092861175537108 11 11.936137199401855 12 10.2213134765625 13 10.363731384277344
		 14 11.639776229858398 15 13.390721321105957 16 14.899812698364258 17 16.157012939453125
		 18 17.491037368774414 19 18.609792709350586 20 19.199783325195313 21 19.090190887451172
		 22 18.466594696044922 23 17.548116683959961 24 16.548032760620117 25 15.248702049255371
		 26 13.659284591674805 27 12.285374641418457 28 11.585840225219727 29 11.706871032714844
		 30 12.323320388793945 31 13.173837661743164 32 13.986702919006348 33 14.474668502807619
		 34 14.473513603210449 35 14.168404579162598 36 13.78900146484375 37 13.560532569885254
		 38 13.589529037475586 39 13.740013122558594 40 13.869785308837891 41 13.8355712890625
		 42 13.387879371643066 43 12.680794715881348 44 12.163723945617676 45 12.27156925201416
		 46 13.407215118408203 47 15.29014778137207 48 17.295991897583008 49 18.714088439941406
		 50 19.342206954956055 51 19.527162551879883 52 19.332874298095703 53 18.830759048461914
		 54 17.715023040771484 55 16.094057083129883 56 14.621383666992186 57 13.888498306274414
		 58 14.232934951782227 59 15.273288726806641 60 16.491968154907227 61 17.333841323852539
		 62 17.449445724487305 63 17.183204650878906 64 16.993946075439453 65 17.034139633178711
		 66 17.129199981689453 67 17.24290657043457 68 17.338809967041016 69 17.416122436523437
		 70 17.491283416748047 71 17.55345344543457 72 17.591741561889648 73 17.578781127929688
		 74 17.52851676940918 75 17.491958618164063 76 17.585897445678711 77 17.907529830932617
		 78 18.396276473999023 79 18.951774597167969 80 19.470273971557617 81 20.003694534301758
		 82 20.535120010375977 83 20.849262237548828 84 20.838985443115234 85 20.22291374206543
		 86 19.136367797851563 87 18.113834381103516 88 17.610157012939453 89 17.669055938720703
		 90 18.02734375 91 18.709789276123047 92 19.7606201171875 93 21.639226913452148 94 24.266006469726562
		 95 26.758901596069336 96 28.098249435424805 97 27.573032379150391 98 25.847488403320312
		 99 23.890966415405273 100 22.614917755126953 101 22.116348266601563 102 21.901849746704102
		 103 21.931081771850586 104 22.166957855224609 105 22.838003158569336 106 23.920488357543945
		 107 24.966634750366211 108 25.410219192504883 109 25.055030822753906 110 24.226968765258789
		 111 23.131427764892578 112 21.96617317199707 113 20.607416152954102 114 19.041072845458984
		 115 17.596004486083984 116 16.569929122924805 117 16.100154876708984 118 15.992120742797853
		 119 16.036748886108398 120 16.027473449707031;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateY";
	rename -uid "691A4397-49E7-90A7-BB5A-6F92DE2F8DA6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 18.286495208740234 1 18.891342163085938
		 2 19.499790191650391 3 20.081689834594727 4 20.608213424682617 5 21.310398101806641
		 6 22.117519378662109 7 22.570762634277344 8 22.225236892700195 9 20.419746398925781
		 10 17.446863174438477 11 14.415995597839357 12 12.638595581054687 13 12.789819717407227
		 14 14.115549087524414 15 15.85062885284424 16 17.270214080810547 17 18.400850296020508
		 18 19.550397872924805 19 20.475704193115234 20 20.949771881103516 21 20.862430572509766
		 22 20.359203338623047 23 19.598455429077148 24 18.743085861206055 25 17.588655471801758
		 26 16.108329772949219 27 14.766480445861816 28 14.060565948486328 29 14.183810234069824
		 30 14.804330825805662 31 15.640894889831543 32 16.419521331787109 33 16.877256393432617
		 34 16.876182556152344 35 16.590808868408203 36 16.232009887695313 37 16.013830184936523
		 38 16.041608810424805 39 16.185361862182617 40 16.308773040771484 41 16.276287078857422
		 42 15.84788990020752 43 15.158689498901367 44 14.644833564758299 45 14.752700805664061
		 46 15.866519927978516 47 17.626245498657227 48 19.385492324829102 49 20.560199737548828
		 50 21.062795639038086 51 21.20875358581543 52 21.055404663085938 53 20.654367446899414
		 54 19.738449096679688 55 18.3453369140625 56 17.013477325439453 57 16.326526641845703
		 58 16.651399612426758 59 17.610960006713867 60 18.694286346435547 61 19.417575836181641
		 62 19.515321731567383 63 19.289644241333008 64 19.127994537353516 65 19.162410736083984
		 66 19.243621826171875 67 19.340425491333008 68 19.421787261962891 69 19.487186431884766
		 70 19.550607681274414 71 19.602943420410156 72 19.635120391845703 73 19.624235153198242
		 74 19.581964492797852 75 19.551176071166992 76 19.630212783813477 77 19.898948669433594
		 78 20.301790237426758 79 20.751649856567383 80 21.163957595825195 81 21.58057975769043
		 82 21.988128662109375 83 22.225551605224609 84 22.217826843261719 85 21.749601364135742
		 86 20.899272918701172 87 20.069801330566406 88 19.650585174560547 89 19.699972152709961
		 90 19.998317718505859 91 20.556722640991211 92 21.391672134399414 93 22.811285018920898
		 94 24.646076202392578 95 26.234453201293945 96 27.029563903808594 97 26.722488403320312
		 98 25.670406341552734 99 24.39447021484375 100 23.512767791748047 101 23.157318115234375
		 102 23.002466201782227 103 23.023639678955078 104 23.193683624267578 105 23.669803619384766
		 106 24.414398193359375 107 25.107120513916016 108 25.393037796020508 109 25.164463043212891
		 110 24.620044708251953 111 23.874473571777344 112 23.049022674560547 113 22.042993545532227
		 114 20.823177337646484 115 19.638700485229492 116 18.762121200561523 117 18.350717544555664
		 118 18.255203247070313 119 18.294700622558594 120 18.286495208740234;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateZ";
	rename -uid "24F6D544-4693-DA05-9A3C-E085C3635873";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 11.694750785827637 1 12.246118545532227
		 2 12.81671142578125 3 13.378141403198242 4 13.900071144104004 5 14.617873191833494
		 6 15.475652694702147 7 15.973687171936035 8 15.59291934967041 9 13.711686134338379
		 10 10.954136848449707 11 8.4949493408203125 12 7.1894540786743173 13 7.2969894409179687
		 14 8.2677021026611328 15 9.6196155548095703 16 10.801834106445313 17 11.797820091247559
		 18 12.864917755126953 19 13.767436027526855 20 14.246039390563965 21 14.157003402709961
		 22 13.651542663574219 23 12.910801887512207 24 12.109539985656738 25 11.077254295349121
		 26 9.8288936614990234 27 8.7635774612426758 28 8.2264118194580078 29 8.3190879821777344
		 30 8.7928199768066406 31 9.4509687423706055 32 10.084700584411621 33 10.46726131439209
		 34 10.466354370117187 35 10.226969718933105 36 9.9301538467407227 37 9.7518854141235352
		 38 9.7744903564453125 39 9.8919000625610352 40 9.9932727813720703 41 9.9665346145629883
		 42 9.6174039840698242 43 9.0688142776489258 44 8.6699066162109375 45 8.7529458999633789
		 46 9.6324548721313477 47 11.110024452209473 48 12.708269119262695 49 13.851911544799805
		 50 14.361841201782225 51 14.512374877929687 52 14.354249000549316 53 13.946476936340332
		 54 13.045070648193359 55 11.74771785736084 56 10.582590103149414 57 10.007900238037109
		 58 10.277547836303711 59 11.096691131591797 60 12.064787864685059 61 12.7386474609375
		 62 12.831494331359863 63 12.617781639099121 64 12.466103553771973 65 12.498299598693848
		 66 12.574481010437012 67 12.665671348571777 68 12.742640495300293 69 12.804725646972656
		 70 12.865117073059082 71 12.915092468261719 72 12.945880889892578 73 12.935459136962891
		 74 12.895045280456543 75 12.865659713745117 76 12.941182136535645 77 13.20012378692627
		 78 13.594675064086914 79 14.044638633728027 80 14.466056823730471 81 14.900998115539549
		 82 15.33568286895752 83 15.593265533447266 84 15.584832191467283 85 15.080147743225096
		 86 14.194514274597168 87 13.366514205932617 88 12.960696220397949 89 13.008079528808594
		 90 13.296730041503906 91 13.848428726196289 92 14.702624320983887 93 16.242998123168945
		 94 18.422801971435547 95 20.516298294067383 96 21.64997673034668 97 21.204708099365234
		 98 19.74830436706543 99 18.109855651855469 100 17.049304962158203 101 16.636775970458984
		 102 16.459621429443359 103 16.483753204345703 104 16.678600311279297 105 17.234231948852539
		 106 18.134468078613281 107 19.008876800537109 108 19.380886077880859 109 19.082948684692383
		 110 18.39019775390625 111 17.477781295776367 112 16.512720108032227 113 15.394914627075197
		 114 14.11711597442627 115 12.949308395385742 116 12.127022743225098 117 11.752569198608398
		 118 11.666640281677246 119 11.702128410339355 120 11.694750785827637;
createNode animCurveTU -n "Character1_RightHandIndex2_visibility";
	rename -uid "869A7E6A-4ACC-6006-7306-4285C2DF9E69";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex3_translateX";
	rename -uid "6D797D59-4FEF-E9E3-9E6B-B8B4D8C87284";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.8903987407684326;
createNode animCurveTL -n "Character1_RightHandIndex3_translateY";
	rename -uid "76616D36-41D3-98F0-724C-0A82E24B29EB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3401000499725342;
createNode animCurveTL -n "Character1_RightHandIndex3_translateZ";
	rename -uid "D3D21EBF-45D2-85FC-2B9D-2EB9CFA0B035";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.8930935859680176;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleX";
	rename -uid "31F84364-4EAA-4666-4BCC-DA9A7F4491B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleY";
	rename -uid "A50AF5B2-401D-72FE-63CA-3BB7C283D3C7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleZ";
	rename -uid "62956A6B-4574-3F3F-AD60-0A9BDC499A88";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateX";
	rename -uid "8E4CB27B-4B7E-4B6F-883D-07ACDF7AA828";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.3585745263262652e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateY";
	rename -uid "0846A3B2-45A6-FB70-B585-00975F51D4C5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.0418900703589316e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateZ";
	rename -uid "374B8170-4211-7FA1-E415-77A1B5D2356B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.0227597019584209e-007;
createNode animCurveTU -n "Character1_RightHandIndex3_visibility";
	rename -uid "EFD12A07-439E-A249-339F-7283115ED87A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing1_translateX";
	rename -uid "9C195BDE-48B0-AAB4-7AE6-3AA0E72D21DE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.6450033187866211;
createNode animCurveTL -n "Character1_RightHandRing1_translateY";
	rename -uid "89C15BE9-4CBE-A7C5-642B-B6BD5747CDDD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.0317020416259766;
createNode animCurveTL -n "Character1_RightHandRing1_translateZ";
	rename -uid "35F686C1-4D8C-04AE-77E0-D89104EA7D51";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.2285447120666504;
createNode animCurveTU -n "Character1_RightHandRing1_scaleX";
	rename -uid "5A4038DD-4FC4-9701-4FC2-E392ACB30C4A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandRing1_scaleY";
	rename -uid "37DE8C2D-45B8-24DE-2742-64A5A4A8EE6B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandRing1_scaleZ";
	rename -uid "BF06F39B-4278-D958-6C19-6A983C6C67E3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightHandRing1_rotateX";
	rename -uid "F158F574-4DEF-CB0F-158F-BAA068EA0120";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 11.189376831054688 1 11.617251396179199
		 2 12.062569618225098 3 12.503439903259277 4 12.915866851806641 5 13.487410545349121
		 6 14.17750072479248 7 14.581960678100586 8 14.272477149963379 9 12.766711235046387
		 10 10.618081092834473 11 8.7435293197631836 12 7.7576718330383301 13 7.8387446403503409
		 14 8.5716085433959961 15 9.5971841812133789 16 10.501047134399414 17 11.269187927246094
		 18 12.100317001342773 19 12.810815811157227 20 13.190694808959961 21 13.119853019714355
		 22 12.719161987304687 23 12.136262893676758 24 11.51104736328125 25 10.712796211242676
		 26 9.7566442489624023 27 8.9469795227050781 28 8.5403900146484375 29 8.6104726791381836
		 30 8.9691410064697266 31 9.4688377380371094 32 9.9518547058105469 33 10.244455337524414
		 34 10.243760108947754 35 10.060573577880859 36 9.8338766098022461 37 9.697941780090332
		 38 9.7151699066162109 39 9.8046932220458984 40 9.882044792175293 41 9.8616371154785156
		 42 9.5954999923706055 43 9.1784706115722656 44 8.8760061264038086 45 8.9389200210571289
		 46 9.6069602966308594 47 10.738021850585938 48 11.977728843688965 49 12.877702713012695
		 50 13.282949447631836 51 13.403079986572266 52 13.276897430419922 53 12.952661514282227
		 54 12.241557121276855 55 11.230381965637207 56 10.332827568054199 57 9.8932104110717773
		 58 10.099250793457031 59 10.727757453918457 60 11.476280212402344 61 12.001486778259277
		 62 12.074143409729004 63 11.907010078430176 64 11.78862190246582 65 11.813735961914063
		 66 11.873193740844727 67 11.944429397583008 68 12.004610061645508 69 12.053189277648926
		 70 12.100473403930664 71 12.139625549316406 72 12.16375732421875 73 12.155588150024414
		 74 12.123917579650879 75 12.100897789001465 76 12.118560791015625 77 12.206671714782715
		 78 12.341813087463379 79 12.475639343261719 80 12.558542251586914 81 12.638947486877441
		 82 12.722556114196777 83 12.693817138671875 84 12.439194679260254 85 11.799747467041016
		 86 10.912752151489258 87 10.08250617980957 88 9.5656843185424805 89 9.3802833557128906
		 90 9.3854331970214844 91 9.5823392868041992 92 9.9783172607421875 93 10.827978134155273
		 94 12.109256744384766 95 13.349869728088379 96 13.952011108398438 97 13.487472534179688
		 98 12.374809265136719 99 11.191293716430664 100 10.404925346374512 101 10.041763305664062
		 102 9.8555183410644531 103 9.8452959060668945 104 10.015215873718262 105 10.513774871826172
		 106 11.316127777099609 107 12.14735221862793 108 12.684985160827637 109 12.79417896270752
		 110 12.645103454589844 111 12.349116325378418 112 12.01654052734375 113 11.572232246398926
		 114 11.005303382873535 115 10.503454208374023 116 10.235963821411133 117 10.283624649047852
		 118 10.538885116577148 119 10.882528305053711 120 11.189376831054688;
createNode animCurveTA -n "Character1_RightHandRing1_rotateY";
	rename -uid "3E87CBB3-4A61-EA61-E107-82BF93EB868A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -28.328363418579102 1 -29.187633514404297
		 2 -30.058811187744141 3 -30.898700714111328 4 -31.664619445800785 5 -32.695388793945313
		 6 -33.894237518310547 7 -34.574478149414063 8 -34.055435180664062 9 -31.389795303344727
		 10 -27.146078109741211 11 -22.968748092651367 12 -20.576112747192383 13 -20.778221130371094
		 14 -22.561578750610352 15 -24.92950439453125 16 -26.898828506469727 17 -28.490320205688477
		 18 -30.131593704223633 19 -31.471313476562496 20 -32.164642333984375 21 -32.036537170410156
		 22 -31.301670074462891 23 -30.200750350952148 24 -28.976406097412113 25 -27.34490966796875
		 26 -25.284753799438477 27 -23.445205688476562 28 -22.487192153930664 29 -22.653989791870117
		 30 -23.496757507324219 31 -24.641084671020508 32 -25.71504020690918 33 -26.350606918334961
		 34 -26.349111557006836 35 -25.952499389648437 36 -25.455595016479492 37 -25.15437126159668
		 38 -25.192684173583984 39 -25.391134262084961 40 -25.561744689941406 41 -25.516811370849609
		 42 -24.92573356628418 43 -23.980312347412109 44 -23.279653549194336 45 -23.426443099975586
		 46 -24.951381683349609 47 -27.397672653198242 48 -29.894626617431641 49 -31.594533920288089
		 50 -32.330665588378906 51 -32.545494079589844 52 -32.319801330566406 53 -31.732040405273437
		 54 -30.402469635009769 55 -28.41166877746582 56 -26.540374755859375 57 -25.586305618286133
		 58 -26.036603927612305 59 -27.376214981079102 60 -28.906965255737308 61 -29.94068717956543
		 62 -30.081146240234375 63 -29.757131576538089 64 -29.525644302368161 65 -29.574888229370117
		 66 -29.691177368164059 67 -29.8299560546875 68 -29.946737289428714 69 -30.04069900512695
		 70 -30.131895065307617 71 -30.207212448120114 72 -30.253545761108402 73 -30.23786735534668
		 74 -30.17701530456543 75 -30.132713317871097 76 -30.166713714599609 77 -30.335777282714844
		 78 -30.593358993530273 79 -30.846391677856449 80 -31.002126693725586 81 -31.152437210083011
		 82 -31.30797004699707 83 -31.254594802856445 84 -30.777683258056644 85 -29.547468185424805
		 86 -27.760965347290039 87 -26.000217437744141 88 -24.858922958374023 89 -24.440786361694336
		 90 -24.452463150024414 91 -24.896257400512695 92 -25.772983551025391 93 -27.585184097290039
		 94 -30.148807525634769 95 -32.450531005859375 96 -33.507884979248047 97 -32.695499420166016
		 98 -30.655935287475589 99 -28.33226203918457 100 -26.694450378417969 101 -25.911525726318359
		 102 -25.503326416015625 103 -25.48078727722168 104 -25.853618621826172 105 -26.925802230834961
		 106 -28.585212707519531 107 -30.222055435180664 108 -31.238174438476563 109 -31.440589904785156
		 110 -31.163913726806637 111 -30.607219696044922 112 -29.969839096069332 113 -29.098260879516605
		 114 -27.951850891113281 115 -26.9039306640625 116 -26.332321166992188 117 -26.434844970703125
		 118 -26.97895622253418 119 -27.698396682739258 120 -28.328363418579102;
createNode animCurveTA -n "Character1_RightHandRing1_rotateZ";
	rename -uid "B6B9A987-470D-0849-6C30-F18C6B79D460";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -12.764520645141602 1 -13.131381034851074
		 2 -13.515207290649414 3 -13.897127151489258 4 -14.256063461303711 5 -14.755995750427244
		 6 -15.36330032348633 7 -15.721004486083983 8 -15.447183609008789 9 -14.126071929931641
		 10 -12.277801513671875 11 -10.7086181640625 12 -9.9028863906860352 13 -9.9685811996459961
		 14 -10.567062377929688 15 -11.417595863342285 16 -12.178552627563477 17 -12.832802772521973
		 18 -13.547833442687988 19 -14.164488792419434 20 -14.496100425720215 21 -14.434163093566895
		 22 -14.084674835205078 23 -13.578916549682617 24 -13.040141105651855 25 -12.358240127563477
		 26 -11.551104545593262 27 -10.876679420471191 28 -10.541403770446777 29 -10.599024772644043
		 30 -10.895021438598633 31 -11.31037425994873 32 -11.714986801147461 33 -11.961513519287109
		 34 -11.96092700958252 35 -11.806464195251465 36 -11.615884780883789 37 -11.501917839050293
		 38 -11.516348838806152 39 -11.591398239135742 40 -11.656325340270996 41 -11.639188766479492
		 42 -11.416186332702637 43 -11.068606376647949 44 -10.817984580993652 45 -10.870011329650879
		 46 -11.42577075958252 47 -12.379681587219238 48 -13.441927909851074 49 -14.222784042358398
		 50 -14.576826095581056 51 -14.682053565979004 52 -14.571527481079103 53 -14.288161277770996
		 54 -13.670035362243652 55 -12.799592971801758 56 -12.036174774169922 57 -11.665703773498535
		 58 -11.839042663574219 59 -12.370956420898437 60 -13.010297775268555 61 -13.462441444396973
		 62 -13.525209426879883 63 -13.380901336669922 64 -13.278850555419922 65 -13.300487518310547
		 66 -13.351737022399902 67 -13.413186073303223 68 -13.46513843536377 69 -13.507102012634277
		 70 -13.547968864440918 71 -13.58182430267334 72 -13.602699279785156 73 -13.59563159942627
		 74 -13.568239212036133 75 -13.548335075378418 76 -13.563607215881348 77 -13.639834403991699
		 78 -13.756895065307617 79 -13.872988700866699 80 -13.944992065429688 81 -14.014886856079102
		 82 -14.087629318237305 83 -14.062618255615234 84 -13.84135627746582 85 -13.288434982299805
		 86 -12.528392791748047 87 -11.824935913085938 88 -11.391260147094727 89 -11.236522674560547
		 90 -11.240814208984375 91 -11.405182838439941 92 -11.737238883972168 93 -12.456199645996094
		 94 -13.555562973022461 95 -14.635429382324219 96 -15.164434432983398 97 -14.756051063537598
		 98 -13.785503387451172 99 -12.766160011291504 100 -12.097155570983887 101 -11.790626525878906
		 102 -11.634051322937012 103 -11.625469207763672 104 -11.768281936645508 105 -12.189337730407715
		 106 -12.872994422912598 107 -13.588507652282715 108 -14.054933547973633 109 -14.149995803833008
		 110 -14.020240783691406 111 -13.763226509094238 112 -13.475441932678223 113 -13.092690467834473
		 114 -12.607300758361816 115 -12.180591583251953 116 -11.954344749450684 117 -11.994594573974609
		 118 -12.210622787475586 119 -12.502644538879395 120 -12.764520645141602;
createNode animCurveTU -n "Character1_RightHandRing1_visibility";
	rename -uid "7035F282-4761-3DA5-D5A7-6C918F4B104F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing2_translateX";
	rename -uid "146AAFA3-43B3-5A8D-3401-888492717ECA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.046228401362895966;
createNode animCurveTL -n "Character1_RightHandRing2_translateY";
	rename -uid "B0925206-4DD8-47FF-9590-A49BDEA750D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.89510548114776611;
createNode animCurveTL -n "Character1_RightHandRing2_translateZ";
	rename -uid "2F1E8E7A-4187-34AA-0199-19BE979B80DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.4977946281433105;
createNode animCurveTU -n "Character1_RightHandRing2_scaleX";
	rename -uid "A82ABC14-4F1E-DD70-325B-C287323B27C6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandRing2_scaleY";
	rename -uid "04F43FA2-44B2-B8FF-B18E-A3B194627304";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandRing2_scaleZ";
	rename -uid "A02E35FD-456B-B964-8669-BB8C09D9F725";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightHandRing2_rotateX";
	rename -uid "CF9959BD-478C-4ED4-3EF9-DEB09554EE07";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 39.536983489990234 1 40.513782501220703
		 2 41.517082214355469 3 42.496742248535156 4 43.400760650634766 5 44.633434295654297
		 6 46.090320587158203 7 46.928089141845703 8 46.288120269775391 9 43.075210571289063
		 10 38.213630676269531 11 33.726692199707031 12 31.286245346069336 13 31.488826751708988
		 14 33.304828643798828 15 35.796443939208984 16 37.939888000488281 17 39.720123291015625
		 18 41.601493835449219 19 43.171642303466797 20 43.996425628662109 21 43.843402862548828
		 22 42.971099853515625 23 41.681785583496094 24 40.272491455078125 25 38.434516906738281
		 26 36.17828369140625 27 34.223789215087891 28 33.228054046630859 29 33.400337219238281
		 30 34.277797698974609 31 37.107547760009766 32 42.664382934570313 33 49.739780426025391
		 34 56.972770690917969 35 63.314495086669915 36 72.119194030761719 37 81.580245971679688
		 38 83.488800048828125 39 76.73583984375 40 66.182350158691406 41 53.367919921875
		 42 40.282295227050781 43 29.206768035888668 44 21.61039924621582 45 18.129844665527344
		 46 19.029237747192383 47 23.334535598754883 48 29.964235305786133 49 37.486686706542969
		 50 44.545330047607422 51 50.169170379638672 52 52.938728332519531 53 52.898525238037109
		 54 50.720390319824219 55 46.932415008544922 56 42.916522979736328 57 39.920738220214844
		 58 38.709201812744141 59 38.953731536865234 60 40.193332672119141 61 41.380275726318359
		 62 41.542972564697266 63 41.168170928955078 64 40.901504516601563 65 40.958152770996094
		 66 41.092098236083984 67 41.252250671386719 68 41.387275695800781 69 41.496086120605469
		 70 41.601840972900391 71 41.689289093017578 72 41.743133544921875 73 41.724910736083984
		 74 41.654216766357422 75 41.602787017822266 76 40.744247436523438 77 38.605560302734375
		 78 35.685482025146484 79 32.415714263916016 80 29.18361663818359 81 26.493650436401367
		 82 24.697332382202148 83 23.916339874267578 84 30.874937057495117 85 40.746437072753906
		 86 45.229633331298828 87 49.922397613525391 88 54.426063537597656 89 57.680191040039063
		 90 58.111885070800788 91 55.895309448242188 92 52.373355865478516 93 48.763084411621094
		 94 45.395072937011719 95 41.594039916992188 96 36.7867431640625 97 30.939315795898438
		 98 25.3677978515625 99 21.303504943847656 100 20.289648056030273 101 22.343357086181641
		 102 26.161699295043945 103 30.822521209716797 104 35.138435363769531 105 37.969699859619141
		 106 39.82763671875 107 41.706535339355469 108 42.896167755126953 109 43.135284423828125
		 110 42.808624267578125 111 42.155372619628906 112 41.414016723632813 113 40.411598205566406
		 114 39.112957000732422 115 37.945526123046875 116 37.31658935546875 117 37.428989410400391
		 118 38.028488159179688 119 38.828880310058594 120 39.536983489990234;
createNode animCurveTA -n "Character1_RightHandRing2_rotateY";
	rename -uid "BFC504D9-42F1-050A-9748-3BB1399D9E07";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 32.544033050537109 1 32.88909912109375
		 2 33.227188110351563 3 33.541645050048828 4 33.818347930908203 5 34.175201416015625
		 6 34.567192077636719 7 34.778289794921875 8 34.617965698242187 9 33.720180511474609
		 10 32.050941467285156 11 30.14871788024902 12 28.955068588256832 13 29.058647155761715
		 14 29.950698852539063 15 31.071622848510742 16 31.945199966430664 17 32.609939575195313
		 18 33.254886627197266 19 33.749427795410156 20 33.993705749511719 21 33.949180603027344
		 22 33.688434600830078 23 33.2811279296875 24 32.805332183837891 25 32.135326385498047
		 26 31.233253479003906 27 30.377693176269535 28 29.914291381835937 29 29.995828628540043
		 30 30.402288436889648 31 31.615655899047848 32 33.593925476074219 33 35.412441253662109
		 34 36.542800903320312 35 36.972301483154297 36 36.727954864501953 37 35.351001739501953
		 38 34.924869537353516 39 36.204994201660156 40 36.999774932861328 41 36.067264556884766
		 42 32.808753967285156 43 27.843055725097656 44 22.959287643432617 45 20.24015998840332
		 46 20.97430419921875 47 24.188678741455078 48 28.258541107177734 49 31.767271041870121
		 50 34.150466918945313 51 35.499404907226563 52 35.999156951904297 53 35.992652893066406
		 54 35.607276916503906 55 34.779354095458984 56 33.671726226806641 57 32.681491851806641
		 58 32.239089965820313 59 32.330379486083984 60 32.777641296386719 61 33.182048797607422
		 62 33.235694885253906 63 33.111469268798828 64 33.021694183349609 65 33.040863037109375
		 66 33.085975646972656 67 33.139533996582031 68 33.184364318847656 69 33.220279693603516
		 70 33.255001068115234 71 33.283573150634766 72 33.301109313964844 73 33.295181274414063
		 74 33.272129058837891 75 33.25531005859375 76 32.968212127685547 77 32.200088500976563
		 78 31.02415657043457 79 29.522069931030277 80 27.830163955688477 81 26.252845764160156
		 82 25.108257293701172 83 24.586788177490234 84 28.742206573486332 85 32.968959808349609
		 86 34.339469909667969 87 35.449741363525391 88 36.224643707275391 89 36.616172790527344
		 90 36.657772064208984 91 36.418563842773438 92 35.905647277832031 93 35.204963684082031
		 94 34.384101867675781 95 33.252445220947266 96 31.485382080078125 97 28.775753021240231
		 98 25.544296264648438 99 22.732505798339844 100 21.965789794921875 101 23.491107940673828
		 102 26.046977996826172 103 28.714834213256832 104 30.786849975585934 105 31.956779479980469
		 106 32.648368835449219 107 33.289196014404297 108 33.665481567382812 109 33.738418579101563
		 110 33.638557434082031 111 33.433807373046875 112 33.193210601806641 113 32.853740692138672
		 114 32.389278411865234 115 31.947391510009766 116 31.699563980102539 117 31.744359970092773
		 118 31.979566574096683 119 32.283897399902344 120 32.544033050537109;
createNode animCurveTA -n "Character1_RightHandRing2_rotateZ";
	rename -uid "A97CFC1B-471E-8F7F-5131-D7BD505E6364";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 31.752662658691406 1 32.613014221191406
		 2 33.498291015625 3 34.364177703857422 4 35.164424896240234 5 36.257373809814453
		 6 37.551567077636719 7 38.296894073486328 8 37.727466583251953 9 34.876117706298828
		 10 30.58966064453125 11 26.671520233154297 12 24.559316635131836 13 24.73408317565918
		 14 26.305366516113281 15 28.473747253417969 16 30.349477767944336 17 31.913852691650387
		 18 33.572845458984375 19 34.961502075195313 20 35.69232177734375 21 35.556663513183594
		 22 34.783946990966797 23 33.643768310546875 24 32.400341033935547 25 30.783569335937496
		 26 28.807229995727539 27 27.103504180908203 28 26.238771438598633 29 26.388225555419922
		 30 27.150470733642578 31 29.620035171508793 32 34.512489318847656 33 40.803672790527344
		 34 47.281379699707031 35 52.981410980224609 36 60.900043487548828 37 69.377273559570313
		 38 71.079170227050781 39 65.043525695800781 40 55.561416625976563 41 44.048591613769531
		 42 32.408981323242188 43 22.771797180175781 44 16.360889434814453 45 13.502068519592285
		 46 14.23518180847168 47 17.797199249267578 48 23.42152214050293 49 29.952140808105469
		 50 36.179187774658203 51 41.187152862548828 52 43.664218902587891 53 43.628219604492187
		 54 41.679672241210937 55 38.300743103027344 56 34.735633850097656 57 32.090480804443359
		 58 31.024824142456055 59 31.239707946777344 60 32.330593109130859 61 33.377487182617188
		 62 33.521160125732422 63 33.19024658203125 64 32.954940795898437 65 33.004920959472656
		 66 33.12310791015625 67 33.264461517333984 68 33.3836669921875 69 33.479751586914063
		 70 33.573150634765625 71 33.650398254394531 72 33.697967529296875 73 33.681865692138672
		 74 33.619415283203125 75 33.573989868164063 76 32.816226959228516 77 30.933778762817383
		 78 28.376893997192383 79 25.535043716430664 80 22.751964569091797 81 20.458841323852539
		 82 18.940898895263672 83 18.284595489501953 84 24.204814910888672 85 32.818161010742188
		 86 36.78668212890625 87 40.966739654541016 88 44.996749877929687 89 47.916561126708984
		 90 48.304279327392578 91 46.314380645751953 92 43.1580810546875 93 39.932018280029297
		 94 36.933639526367188 95 33.566261291503906 96 29.339242935180661 97 24.260272979736328
		 98 19.506126403808594 99 16.106550216674805 100 15.269309997558596 101 16.969974517822266
		 102 20.177484512329102 103 24.159671783447266 104 27.8997802734375 105 30.375631332397461
		 106 32.008502960205078 107 33.665634155273438 108 34.717617034912109 109 34.929306030273438
		 110 34.640132904052734 111 34.062297821044922 112 33.407279968261719 113 32.522937774658203
		 114 31.379684448242184 115 30.354423522949222 116 29.803112030029297 117 29.901582717895508
		 118 30.427200317382813 119 31.129981994628906 120 31.752662658691406;
createNode animCurveTU -n "Character1_RightHandRing2_visibility";
	rename -uid "B96FB759-40DD-0A7F-42B9-91BC5DEFC4B9";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing3_translateX";
	rename -uid "B82C6B11-4759-7667-523D-C4BEE3B9E11D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.8638609647750854;
createNode animCurveTL -n "Character1_RightHandRing3_translateY";
	rename -uid "BF288A7B-4C7C-9547-97D0-AE8FD6B1AF48";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0986917018890381;
createNode animCurveTL -n "Character1_RightHandRing3_translateZ";
	rename -uid "3882069F-47C8-EF51-A58E-33B8764B646D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.2819910049438477;
createNode animCurveTU -n "Character1_RightHandRing3_scaleX";
	rename -uid "D2AA8EE8-4747-FA0B-D96B-BCA3922627C3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing3_scaleY";
	rename -uid "87B66AC4-44B9-6295-C468-F2A80621ED68";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing3_scaleZ";
	rename -uid "FB9F2FE7-4ABC-433F-04FF-55A5C1990DE0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_RightHandRing3_rotateX";
	rename -uid "A21C3910-4AA7-B852-D606-38B394B06188";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2887743966748531e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateY";
	rename -uid "AF2DC902-449E-22DE-B0D3-D884CE778485";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3438902019279337e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateZ";
	rename -uid "39BB1542-487A-A98B-EDA8-9AA17A555D13";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.421513377650399e-009;
createNode animCurveTU -n "Character1_RightHandRing3_visibility";
	rename -uid "65246655-4AB7-A822-D179-87B19BFD008B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Neck_translateX";
	rename -uid "4E854A1F-47C5-133A-E8C3-47901CC4CB33";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.1971182823181152;
createNode animCurveTL -n "Character1_Neck_translateY";
	rename -uid "EECB11CE-4282-2C83-53A4-C996E3D2F870";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.621223449707031;
createNode animCurveTL -n "Character1_Neck_translateZ";
	rename -uid "9C18C7A6-4438-1DE8-3B8B-D493D2ACCE9A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.1444897651672363;
createNode animCurveTU -n "Character1_Neck_scaleX";
	rename -uid "12EE6B47-4810-7121-6CEE-C4824D190A78";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_Neck_scaleY";
	rename -uid "6F33C58D-42A4-C25B-7EE5-D4AD86F4B3D4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Neck_scaleZ";
	rename -uid "4F112925-4DED-34AD-AD58-EBB1782B3EEA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_Neck_rotateX";
	rename -uid "4279C0E0-4781-3A13-0EDA-A89993C4CD1B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 3.9813573360443115 1 4.649724006652832
		 2 5.4330015182495117 3 6.0587396621704102 4 6.2538008689880371 5 5.6176433563232422
		 6 4.3897266387939453 7 3.2597343921661377 8 2.9224483966827393 9 3.8691174983978271
		 10 5.6184186935424805 11 7.4081535339355469 12 8.4758119583129883 13 8.3383474349975586
		 14 7.4831504821777344 15 6.6031951904296875 16 6.2295517921447754 17 6.8376612663269043
		 18 7.9710187911987305 19 8.7778329849243164 20 8.4191684722900391 21 6.3028988838195801
		 22 3.1025748252868652 23 -0.12273000180721283 24 -2.3133928775787354 25 -2.7702505588531494
		 26 -2.1135933399200439 27 -1.2508481740951538 28 -1.0776339769363403 29 -1.8227807283401491
		 30 -2.858365535736084 31 -3.9238691329956055 32 -5.0888090133666992 33 -5.970184326171875
		 34 -6.2276167869567871 35 -6.0970544815063477 36 -6.0575618743896484 37 -6.5865159034729004
		 38 -8.093536376953125 39 -10.191656112670898 40 -12.153572082519531 41 -13.233397483825684
		 42 -12.889006614685059 43 -11.582430839538574 44 -10.062044143676758 45 -9.0612993240356445
		 46 -9.4642353057861328 47 -10.449324607849121 48 -11.329293251037598 49 -11.40938663482666
		 50 -10.135915756225586 51 -8.0373153686523437 52 -6.0253219604492188 53 -4.9839267730712891
		 54 -5.5620818138122559 55 -7.1706724166870126 56 -8.8172979354858398 57 -9.4877004623413086
		 58 -8.5422468185424805 59 -6.6722159385681152 60 -4.8395514488220215 61 -4.1990323066711426
		 62 -5.3293800354003906 63 -7.4538893699645996 64 -8.90087890625 65 -8.9740161895751953
		 66 -8.4806146621704102 67 -7.9741263389587402 68 -8.0048151016235352 69 -9.0230026245117187
		 70 -10.661190032958984 71 -12.233309745788574 72 -13.036802291870117 73 -12.572532653808594
		 74 -11.336873054504395 75 -10.109000205993652 76 -8.9953985214233398 77 -8.6221504211425781
		 78 -8.5601692199707031 79 -8.1805477142333984 80 -6.8515520095825195 81 -4.0820822715759277
		 82 -0.443643718957901 83 3.1152513027191162 84 5.6574244499206543 85 6.4623565673828125
		 86 6.0653114318847656 87 5.4221296310424805 88 5.5063896179199219 89 6.827479362487793
		 90 8.6878185272216797 91 10.40231990814209 92 11.45482063293457 93 11.055582046508789
		 94 9.9428071975708008 95 8.5692691802978516 96 7.386237144470214 97 6.3094792366027832
		 98 5.1799216270446777 99 4.3343520164489746 100 4.1080560684204102 101 5.0056004524230957
		 102 6.7158823013305664 103 8.3530702590942383 104 9.0354738235473633 105 8.1134061813354492
		 106 6.2456068992614746 107 4.3544759750366211 108 3.4627182483673096 109 4.1935567855834961
		 110 5.8869824409484863 111 7.6438732147216797 112 8.5676307678222656 113 8.2639427185058594
		 114 7.2670760154724121 115 6.0736160278320313 116 5.1800069808959961 117 4.719477653503418
		 118 4.4301238059997559 119 4.2160563468933105 120 3.9813573360443115;
createNode animCurveTA -n "Character1_Neck_rotateY";
	rename -uid "15424486-49EF-C7C5-0BE0-94BFCBA027B7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -11.86030101776123 1 -12.326610565185547
		 2 -12.880741119384766 3 -13.298061370849609 4 -13.352750778198242 5 -12.714856147766113
		 6 -11.593206405639648 7 -10.564715385437012 8 -10.196823120117187 9 -10.890399932861328
		 10 -12.256511688232422 11 -13.670248031616211 12 -14.49513530731201 13 -14.319960594177246
		 14 -13.557378768920898 15 -12.798549652099609 16 -12.457870483398438 17 -12.942803382873535
		 18 -13.883126258850098 19 -14.560564041137697 20 -14.241751670837402 21 -12.39350414276123
		 22 -9.6069068908691406 23 -6.8202300071716309 24 -4.9450626373291016 25 -4.5573430061340332
		 26 -5.1279616355895996 27 -5.8947925567626953 28 -6.0885839462280273 29 -5.5108656883239746
		 30 -4.7074308395385742 31 -3.6842541694641113 32 -2.6677505970001221 33 -1.9593218564987183
		 34 -1.8142781257629395 35 -2.0247981548309326 36 -2.1952083110809326 37 -1.9304512739181521
		 38 -0.89951431751251221 39 0.58210229873657227 40 1.9497591257095337 41 2.6583726406097412
		 42 2.3259706497192383 43 1.3113349676132202 44 0.18916128575801849 45 -0.45531743764877319
		 46 -0.21281854808330536 47 0.48504599928855896 48 1.0848488807678223 49 1.0401062965393066
		 50 -0.096828944981098175 51 -1.9254642724990845 52 -3.7071092128753667 53 -4.6797266006469727
		 54 -4.2677454948425293 55 -2.9812865257263184 56 -1.6694701910018921 57 -1.1631990671157837
		 58 -1.9753466844558716 59 -3.552354097366333 60 -5.1085381507873535 61 -5.8278164863586426
		 62 -5.0109786987304687 63 -3.3395819664001465 64 -2.2065873146057129 65 -2.1576693058013916
		 66 -2.5392439365386963 67 -2.9169583320617676 68 -2.8537654876708984 69 -1.9984650611877444
		 70 -0.66233640909194946 71 0.60602599382400513 72 1.2713829278945923 73 0.9591447114944458
		 74 0.040402337908744812 75 -0.8923947811126709 76 -1.6750448942184448 77 -1.7086143493652344
		 78 -1.3552207946777344 79 -1.1538693904876709 80 -1.6513606309890747 81 -3.3062088489532471
		 82 -5.7052521705627441 83 -8.0723247528076172 84 -9.5970392227172852 85 -9.6597080230712891
		 86 -8.7638635635375977 87 -7.7666101455688468 88 -7.5085663795471191 89 -8.4232959747314453
		 90 -9.9327716827392578 91 -11.433601379394531 92 -12.543031692504883 93 -12.552726745605469
		 94 -12.068068504333496 95 -11.449807167053223 96 -11.055286407470703 97 -10.782975196838379
		 98 -10.473274230957031 99 -10.390974044799805 100 -10.798954010009766 101 -12.106803894042969
		 102 -14.032197952270508 103 -15.792678833007813 104 -16.590768814086914 105 -15.827431678771973
		 106 -14.20091438293457 107 -12.552957534790039 108 -11.759756088256836 109 -12.348438262939453
		 110 -13.761936187744141 111 -15.237051963806152 112 -15.995834350585936 113 -15.689557075500488
		 114 -14.784294128417969 115 -13.715785980224609 116 -12.915165901184082 117 -12.497392654418945
		 118 -12.237261772155762 119 -12.052289962768555 120 -11.86030101776123;
createNode animCurveTA -n "Character1_Neck_rotateZ";
	rename -uid "66554B54-430F-D762-7D2C-98929DC9DC3F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 3.060516357421875 1 2.9056379795074463
		 2 2.7344050407409668 3 2.5329797267913818 4 2.2932393550872803 5 2.0035607814788818
		 6 1.6887502670288086 7 1.393769383430481 8 1.1580609083175659 9 1.0004500150680542
		 10 0.88667356967926025 11 0.78307801485061646 12 0.67675954103469849 13 0.57978165149688721
		 14 0.51089805364608765 15 0.47687509655952454 16 0.45862147212028509 17 0.41802632808685303
		 18 0.37777253985404968 19 0.35932785272598267 20 0.35033151507377625 21 0.27818924188613892
		 22 0.079071462154388428 23 -0.21963889896869659 24 -0.48218438029289246 25 -0.541756272315979
		 26 -0.41661983728408813 27 -0.23046983778476715 28 -0.15274512767791748 29 -0.26016071438789368
		 30 -0.46461552381515508 31 -0.64587908983230591 32 -0.80652403831481934 33 -0.88891476392745972
		 34 -0.82915490865707397 35 -0.67037588357925415 36 -0.51349556446075439 37 -0.47102025151252752
		 38 -0.66185760498046875 39 -1.0207862854003906 40 -1.374982476234436 41 -1.5245416164398193
		 42 -1.3116414546966553 43 -0.85881614685058594 44 -0.38363972306251526 45 -0.10463333874940872
		 46 -0.17170953750610352 47 -0.39937910437583923 48 -0.60463768243789673 49 -0.60025590658187866
		 50 -0.24859152734279633 51 0.29993599653244019 52 0.81158161163330078 53 1.0790525674819946
		 54 0.96573692560195923 55 0.61760598421096802 56 0.26278388500213623 57 0.131683349609375
		 58 0.36200758814811707 59 0.7862088680267334 60 1.1891790628433228 61 1.3671749830245972
		 62 1.1355195045471191 63 0.65349084138870239 64 0.29568237066268921 65 0.23834545910358429
		 66 0.30613204836845398 67 0.37000125646591187 68 0.30111619830131531 69 -0.012930701486766338
		 70 -0.47862446308135986 71 -0.91231346130371083 72 -1.1215951442718506 73 -0.96191066503524791
		 74 -0.55405300855636597 75 -0.099116116762161255 76 0.2008056640625 77 0.28226006031036377
		 78 0.29074281454086304 79 0.40866157412528992 80 0.77438735961914063 81 1.4231530427932739
		 82 2.1587119102478027 83 2.7962226867675781 84 3.2643234729766846 85 3.6026630401611324
		 86 3.9173703193664546 87 4.2416372299194336 88 4.5420069694519043 89 4.7471380233764648
		 90 4.8014898300170898 91 4.770118236541748 92 4.6923356056213379 93 4.6043553352355957
		 94 4.4893770217895508 95 4.3340911865234375 96 4.1369171142578125 97 3.9052484035491943
		 98 3.6512675285339355 99 3.3975772857666016 100 3.1738626956939697 101 3.011932373046875
		 102 2.9083943367004395 103 2.8434007167816162 104 2.8070623874664307 105 2.7952377796173096
		 106 2.7522983551025391 107 2.7044727802276611 108 2.6917369365692139 109 2.7450616359710693
		 110 2.8337917327880859 111 2.9209563732147217 112 2.9857645034790039 113 3.0266332626342773
		 114 3.0502481460571289 115 3.0593574047088623 116 3.0649576187133789 117 3.0725209712982178
		 118 3.0755550861358643 119 3.072312593460083 120 3.060516357421875;
createNode animCurveTU -n "Character1_Neck_visibility";
	rename -uid "A8B92283-42A0-5C39-C2EC-229D690E8652";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Head_translateX";
	rename -uid "683375C3-44C7-7DFD-D7F1-CD8A7AECBD88";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -16.985572814941406;
createNode animCurveTL -n "Character1_Head_translateY";
	rename -uid "482C2DD1-4281-7EBA-0326-3899F2E63AFD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.0131206512451172;
createNode animCurveTL -n "Character1_Head_translateZ";
	rename -uid "E720B6FA-4752-8DF1-752B-049AEA089C62";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.1724905967712402;
createNode animCurveTU -n "Character1_Head_scaleX";
	rename -uid "6F3A7D0A-43E0-D5A9-BA58-F0A2BD59725C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Head_scaleY";
	rename -uid "9CC237C7-454C-8C3D-4819-539148DC320A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_Head_scaleZ";
	rename -uid "1588ED95-4A78-F700-C066-3AB6B236F8AF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_Head_rotateX";
	rename -uid "39A1D325-445D-6FB4-1A50-AD9B3C0FCC6D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -12.699968338012695 1 -13.468565940856934
		 2 -14.412857055664061 3 -15.200570106506348 4 -15.498582839965822 5 -14.82485866546631
		 6 -13.458300590515137 7 -12.204587936401367 8 -11.866001129150391 9 -13.005529403686523
		 10 -15.049498558044434 11 -17.092947006225586 12 -18.224399566650391 13 -17.858638763427734
		 14 -16.564666748046875 15 -15.15594005584717 16 -14.422282218933105 17 -15.025186538696289
		 18 -16.402921676635742 19 -17.519685745239258 20 -17.335323333740234 21 -15.090955734252931
		 22 -11.560043334960938 23 -7.9896402359008798 24 -5.617851734161377 25 -5.2429723739624023
		 26 -6.0965065956115723 27 -7.0692505836486816 28 -7.0525312423706055 29 -5.7357258796691895
		 30 -3.8449854850769043 31 -1.7592235803604126 32 0.16346997022628784 33 1.5058071613311768
		 34 1.9041444063186646 35 1.6734009981155396 36 1.4125912189483643 37 1.7204576730728149
		 38 3.1134021282196045 39 5.1649031639099121 40 7.0548162460327148 41 7.9523591995239258
		 42 7.2664198875427246 43 5.5709919929504395 44 3.7733237743377686 45 2.7705092430114746
		 46 3.0873465538024902 47 4.1096816062927246 48 5.0201497077941895 49 4.9970669746398926
		 50 3.3795623779296875 51 0.78290075063705444 52 -1.7109432220458984 53 -3.0322365760803223
		 54 -2.3881068229675293 55 -0.49779909849166876 56 1.436156153678894 57 2.2023606300354004
		 58 1.0409319400787354 59 -1.2305101156234741 60 -3.4696049690246582 61 -4.5263614654541016
		 62 -3.2674438953399658 63 -0.66942900419235229 64 1.2355114221572876 65 1.5954422950744629
		 66 1.3352165222167969 67 1.0737974643707275 68 1.4296927452087402 69 2.9018087387084961
		 70 5.016273021697998 71 6.9265742301940918 72 7.7736964225769034 73 6.9396896362304687
		 74 4.9763259887695312 75 2.7685401439666748 76 1.1775397062301636 77 0.76316988468170166
		 78 0.96990245580673218 79 1.0003386735916138 80 0.057117443531751633 81 -2.4966144561767578
		 82 -6.0393829345703125 83 -9.4879550933837891 84 -11.756205558776855 85 -12.020874977111816
		 86 -10.984932899475098 87 -9.8436603546142578 88 -9.7917098999023437 89 -11.466188430786133
		 90 -14.080370903015137 91 -16.740022659301758 92 -18.563999176025391 93 -18.598041534423828
		 94 -17.727474212646484 95 -16.498144149780273 96 -15.451405525207518 97 -14.486047744750975
		 98 -13.415748596191406 99 -12.647095680236816 100 -12.584225654602051 101 -13.834230422973633
		 102 -16.033964157104492 103 -18.130172729492188 104 -19.062841415405273 105 -18.049108505249023
		 106 -15.81194496154785 107 -13.549917221069336 108 -12.480480194091797 109 -13.341496467590332
		 110 -15.349649429321287 111 -17.436592102050781 112 -18.525588989257813 113 -18.136922836303711
		 114 -16.911752700805664 115 -15.447694778442385 116 -14.339353561401367 117 -13.74494743347168
		 118 -13.350516319274902 119 -13.040629386901855 120 -12.699968338012695;
createNode animCurveTA -n "Character1_Head_rotateY";
	rename -uid "A02BF0D9-43F0-49AF-964F-E6A7F608D253";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 0.22921811044216159 1 -0.072624094784259796
		 2 -0.45283824205398554 3 -0.75718426704406738 4 -0.83344799280166626 5 -0.45997607707977295
		 6 0.23738649487495425 7 0.88497167825698853 8 1.1023582220077515 9 0.62284040451049805
		 10 -0.27647009491920471 11 -1.1668573617935181 12 -1.6339786052703857 13 -1.4209561347961426
		 14 -0.78490626811981201 15 -0.094047069549560547 16 0.28262126445770264 17 0.032379236072301865
		 18 -0.58064442873001099 19 -1.0756855010986328 20 -0.98113071918487549 21 0.056474007666110992
		 22 1.7063285112380981 23 3.3929746150970459 24 4.5138421058654785 25 4.6748557090759277
		 26 4.2443656921386719 27 3.7578351497650151 28 3.7484798431396484 29 4.3681230545043945
		 30 5.2736220359802246 31 6.2809076309204102 32 7.212426185607911 33 7.8670415878295898
		 34 8.0715703964233398 35 7.9779286384582528 36 7.8732151985168457 37 8.0445518493652344
		 38 8.7383489608764648 39 9.7462711334228516 40 10.674321174621582 41 11.133068084716797
		 42 10.849370956420898 43 10.089455604553223 44 9.2784690856933594 45 8.846074104309082
		 46 9.0505714416503906 47 9.6009931564331055 48 10.104665756225586 49 10.170370101928711
		 50 9.4811782836914062 51 8.3220510482788086 52 7.2088832855224609 53 6.6584382057189941
		 54 7.0521631240844727 55 8.0420236587524414 56 9.0462379455566406 57 9.4824514389038086
		 58 8.9870719909667969 59 7.9475102424621582 60 6.9110307693481445 61 6.4449615478515625
		 62 7.1047792434692383 63 8.4162921905517578 64 9.3953485488891602 65 9.6306886672973633
		 66 9.5633621215820312 67 9.486114501953125 68 9.6918516159057617 69 10.414794921875
		 70 11.419975280761719 71 12.297689437866211 72 12.642938613891602 73 12.166670799255371
		 74 11.118719100952148 75 9.9079608917236328 76 8.9219770431518555 77 8.4186038970947266
		 78 8.1441736221313477 79 7.7278561592102051 80 6.7993307113647461 81 5.0647110939025879
		 82 2.8403151035308838 83 0.6717870831489563 84 -0.908033847808838 85 -1.5111904144287109
		 86 -1.4590598344802856 87 -1.2961030006408691 88 -1.573138952255249 89 -2.5751500129699707
		 90 -3.9051191806793213 91 -5.1296210289001465 92 -5.8625683784484863 93 -5.7049026489257812
		 94 -5.0703949928283691 95 -4.2124795913696289 96 -3.3886857032775879 97 -2.5669436454772949
		 98 -1.6741708517074585 99 -0.91311311721801758 100 -0.48856112360954285 101 -0.69815188646316528
		 102 -1.3769444227218628 103 -2.043323278427124 104 -2.2342133522033691 105 -1.6171646118164063
		 106 -0.49538746476173406 107 0.61448162794113159 108 1.1405770778656006 109 0.7314879298210144
		 110 -0.23431915044784546 111 -1.2461862564086914 112 -1.8102573156356809 113 -1.7146903276443481
		 114 -1.244813084602356 115 -0.66450732946395874 116 -0.24433588981628415 117 -0.057252850383520126
		 118 0.045460168272256851 119 0.12156417965888977 120 0.22921811044216159;
createNode animCurveTA -n "Character1_Head_rotateZ";
	rename -uid "8D64D593-41D1-9BAE-27E6-1C9B12FE5448";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 4.6982064247131348 1 4.6060514450073242
		 2 4.5305275917053223 3 4.4352021217346191 4 4.2758870124816895 5 3.9909727573394775
		 6 3.629159688949585 7 3.2949943542480469 8 3.0643210411071777 9 2.9917473793029785
		 10 3.0477156639099121 11 3.148017406463623 12 3.1550254821777344 13 2.9597203731536865
		 14 2.6540853977203369 15 2.3687281608581543 16 2.1985423564910889 17 2.208768367767334
		 18 2.3401107788085937 19 2.4627945423126221 20 2.4156641960144043 21 2.1057310104370117
		 22 1.7150770425796509 23 1.4324641227722168 24 1.3143283128738403 25 1.32167649269104
		 26 1.3929543495178223 27 1.482702374458313 28 1.5224097967147827 29 1.4967440366744995
		 30 1.4698735475540161 31 1.4810941219329834 32 1.5452609062194824 33 1.6461915969848633
		 34 1.75548255443573 35 1.8653787374496462 36 1.9829214811325075 37 2.1132152080535889
		 38 2.2708568572998047 39 2.4724071025848389 40 2.6979527473449707 41 2.878901481628418
		 42 2.9529733657836914 43 2.9706552028656006 44 2.9982244968414307 45 3.0557467937469482
		 46 3.1388387680053711 47 3.2398903369903564 48 3.3396139144897461 49 3.3978610038757324
		 50 3.3944239616394043 51 3.3971843719482422 52 3.4503188133239746 53 3.5152850151062012
		 54 3.5343914031982422 55 3.5410776138305664 56 3.5750288963317871 57 3.6102044582366943
		 58 3.615390539169312 59 3.6368930339813228 60 3.7004406452178955 61 3.7560350894927979
		 62 3.7358348369598389 63 3.7147748470306392 64 3.7419388294219971 65 3.7682998180389404
		 66 3.7855074405670162 67 3.8004159927368164 68 3.8174369335174565 69 3.8517546653747554
		 70 3.9222929477691655 71 4.0090103149414062 72 4.0408806800842285 73 3.9562535285949707
		 74 3.8119442462921143 75 3.6819088459014897 76 3.5789945125579834 77 3.4804089069366455
		 78 3.3786113262176514 79 3.277801513671875 80 3.1840555667877197 81 3.1398963928222656
		 82 3.2211713790893555 83 3.426872730255127 84 3.6368649005889888 85 3.6979985237121582
		 86 3.6555523872375488 87 3.6286222934722905 88 3.7041468620300293 89 3.9313006401062012
		 90 4.2779579162597656 91 4.6742310523986816 92 4.9874391555786133 93 5.0330233573913574
		 94 4.935575008392334 95 4.7876195907592773 96 4.6691474914550781 97 4.5655150413513184
		 98 4.4558372497558594 99 4.3863654136657715 100 4.3942351341247559 101 4.5472555160522461
		 102 4.8365983963012695 103 5.1501460075378418 104 5.3116717338562012 105 5.1762642860412598
		 106 4.8818683624267578 107 4.6232819557189941 108 4.5234017372131348 109 4.631321907043457
		 110 4.8850865364074707 111 5.181450366973877 112 5.355532169342041 113 5.3095192909240723
		 114 5.1448235511779785 115 4.9608011245727539 116 4.8354182243347168 117 4.7774443626403809
		 118 4.7444839477539062 119 4.7222800254821777 120 4.6982064247131348;
createNode animCurveTU -n "Character1_Head_visibility";
	rename -uid "B6B4B7D9-4482-5D3D-712A-3183DC360A27";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "jaw_translateX";
	rename -uid "B10FDA58-4400-3EC3-C9B3-1989B0794C9A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 14.004427909851074;
createNode animCurveTL -n "jaw_translateY";
	rename -uid "A6B089C1-41AD-031F-3944-5A86266A0F67";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.9922315685958317e-015;
createNode animCurveTL -n "jaw_translateZ";
	rename -uid "9EEEE73D-4F79-70BF-64C8-54831956B3A1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9713712501773515e-015;
createNode animCurveTU -n "jaw_scaleX";
	rename -uid "F6565D84-48CA-A34B-866E-10AD76E4E626";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleY";
	rename -uid "0BF47212-49B6-080F-0135-A7A7FB832FB0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleZ";
	rename -uid "6CA83D9A-4280-3F2C-F79F-F9AC7A02A1C6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "jaw_rotateX";
	rename -uid "F22ED9F3-4CAC-1FE4-17F1-F9970C1E75E5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 -5.9752750396728516 1 -5.2495059967041016
		 2 -4.4447798728942871 3 -3.6810250282287598 4 -3.0770542621612549 5 -2.7510790824890137
		 6 -2.8058433532714844 7 -3.1634361743927002 8 -3.6704473495483398 9 -4.1705417633056641
		 10 -4.5048084259033203 11 -4.6347150802612305 12 -4.6679649353027344 13 -4.6636629104614258
		 14 -4.6768693923950195 15 -4.7582402229309082 16 -4.938021183013916 17 -5.1799788475036621
		 18 -5.4343233108520508 19 -5.6522922515869141 20 -5.7850308418273926 21 -5.7928905487060547
		 22 -5.7059307098388672 23 -5.5804915428161621 24 -5.477048397064209 25 -5.4604701995849609
		 26 -5.625007152557373 27 -5.9326291084289551 28 -6.2487034797668457 29 -6.4339084625244141
		 30 -6.3460602760314941 31 -5.5454063415527344 32 -3.9444985389709473 33 -1.9297107458114624
		 34 0.11686062812805174 35 1.8212629556655884 36 2.9377148151397705 37 3.4762208461761475
		 38 3.4506716728210449 39 3.1638345718383789 40 3.0215640068054199 41 3.0653507709503174
		 42 3.1783046722412109 43 3.2970271110534668 44 3.3660306930541992 45 3.3387463092803955
		 46 3.1891829967498779 47 2.9519126415252686 48 2.6732370853424072 49 2.3994064331054687
		 50 2.1784322261810303 51 2.0499393939971924 52 1.9850640296936035 53 1.9291220903396604
		 54 1.8231885433197024 55 1.6032299995422363 56 1.1739071607589722 57 0.56866002082824707
		 58 -0.13269810378551483 59 -0.93327438831329346 60 -1.6215074062347412 61 -1.9712719917297361
		 62 -2.0212273597717285 63 -1.9544938802719116 64 -1.9435561895370483 65 -2.1532547473907471
		 66 -2.6243302822113037 67 -3.1390073299407959 68 -3.6539313793182377 69 -4.1649775505065918
		 70 -4.5048084259033203 71 -4.6347150802612305 72 -4.6679649353027344 73 -4.6636629104614258
		 74 -4.6768693923950195 75 -4.7582402229309082 76 -4.938021183013916 77 -5.1799788475036621
		 78 -5.4343233108520508 79 -5.6522922515869141 80 -5.7850308418273926 81 -5.7928905487060547
		 82 -5.7059307098388672 83 -5.5804915428161621 84 -5.477048397064209 85 -5.4604701995849609
		 86 -5.5968413352966309 87 -5.8424429893493652 88 -6.1152892112731934 89 -6.3309812545776367
		 90 -6.3762803077697754 91 -6.1436705589294434 92 -5.6804442405700684 93 -5.0895624160766602
		 94 -4.4918231964111328 95 -4.0182280540466309 96 -3.682319164276123 97 -3.4100167751312256
		 98 -3.1961727142333984 99 -3.034909725189209 100 -2.9188511371612549 101 -2.9009497165679932
		 102 -2.9947845935821533 103 -3.1333620548248291 104 -3.2502827644348145 105 -3.2796587944030762
		 106 -3.1725771427154541 107 -2.9819245338439941 108 -2.7953119277954102 109 -2.8696103096008301
		 110 -3.3722677230834961 111 -4.206298828125 112 -5.157832145690918 113 -6.0150866508483887
		 114 -6.2254414558410645 115 -6.3460602760314941 116 -6.3541498184204102 117 -6.2897601127624512
		 118 -6.1844100952148437 119 -6.0694131851196289 120 -5.9752750396728516;
createNode animCurveTA -n "jaw_rotateY";
	rename -uid "A197A32B-4890-BEED-9BB3-FF817455FB40";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 3.8154958251039595e-013 1 -0.091849252581596375
		 2 -0.15005452930927277 3 -0.22506120800971982 4 -0.36733722686767578 5 -0.62736821174621582
		 6 -1.1196120977401733 7 -1.8103507757186887 8 -2.5279338359832764 9 -3.1007709503173828
		 10 -3.3570923805236816 11 -3.1411168575286865 12 -2.5674717426300049 13 -1.8696858882904055
		 14 -1.2810884714126587 15 -1.035241961479187 16 -1.3133059740066528 17 -1.9595122337341306
		 18 -2.7023143768310547 19 -3.2703344821929932 20 -3.39229416847229 21 -2.8320915699005127
		 22 -1.770554780960083 23 -0.56181854009628296 24 0.43983668088912964 25 0.87999182939529419
		 26 0.52915799617767334 27 -0.37623557448387146 28 -1.492118239402771 29 -2.4746053218841553
		 30 -2.9792404174804687 31 -2.8057398796081543 32 -2.1951005458831787 33 -1.4656586647033691
		 34 -0.90012377500534058 35 -0.72085988521575928 36 -1.0051711797714233 37 -1.5518234968185425
		 38 -2.1642248630523682 39 -2.6761512756347656 40 -2.9106807708740234 41 -2.7596540451049805
		 42 -2.3498578071594238 43 -1.8572938442230225 44 -1.4526534080505371 45 -1.3043231964111328
		 46 -1.5575889348983765 47 -2.0972819328308105 48 -2.7023444175720215 49 -3.1523196697235107
		 50 -3.2253220081329346 51 -2.7188107967376709 52 -1.777037501335144 53 -0.69791406393051147
		 54 0.21617799997329712 55 0.65627944469451904 56 0.42730283737182617 57 -0.27959698438644409
		 58 -1.1960983276367187 59 -2.0561580657958984 60 -2.56136155128479 61 -2.5193259716033936
		 62 -2.0790503025054932 63 -1.4473620653152466 64 -0.87147009372711182 65 -0.63440150022506714
		 66 -0.92978835105895996 67 -1.5958999395370483 68 -2.384962797164917 69 -3.053107738494873
		 70 -3.3570921421051025 71 -3.1411168575286865 72 -2.5674715042114258 73 -1.8696857690811157
		 74 -1.2810883522033691 75 -1.0352418422698975 76 -1.3133058547973633 77 -1.9595122337341306
		 78 -2.7023143768310547 79 -3.2703344821929932 80 -3.39229416847229 81 -2.8320913314819336
		 82 -1.770554780960083 83 -0.56181848049163818 84 0.43983668088912964 85 0.87999182939529419
		 86 0.57283967733383179 87 -0.090198129415512085 88 -0.73571807146072388 89 -1.1763412952423096
		 90 -1.2240079641342163 91 -0.95576637983322144 92 -0.64787620306015015 93 -0.41063714027404785
		 94 -0.19913053512573242 95 0.091263055801391602 96 0.57306218147277832 97 1.2517997026443481
		 98 1.9498306512832639 99 2.4957234859466553 100 2.7242043018341064 101 2.4684240818023682
		 102 1.8459948301315305 103 1.1179513931274414 104 0.54107755422592163 105 0.36934545636177063
		 106 0.82352769374847412 107 1.7289783954620361 108 2.7481956481933594 109 3.37986159324646
		 110 3.2026147842407227 111 2.0892186164855957 112 0.3557601273059845 113 -1.5372787714004517
		 114 -2.5147154331207275 115 -2.9792404174804687 116 -2.8210394382476807 117 -2.2797422409057617
		 118 -1.5197056531906128 119 -0.70511776208877563 120 6.9225465360034377e-009;
createNode animCurveTA -n "jaw_rotateZ";
	rename -uid "6C3DBB71-4674-1AEA-1E5C-77BEE2198710";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 121 ".ktv[0:120]"  0 4.09200966297249e-013 1 0.42252352833747864
		 2 0.94038456678390503 3 1.4105814695358276 4 1.6901174783706665 5 1.635987401008606
		 6 0.99899166822433483 7 -0.12563040852546692 8 -1.3640792369842529 9 -2.3424890041351318
		 10 -2.6871964931488037 11 -2.1146640777587891 12 -0.87383496761322021 13 0.60988795757293701
		 14 1.9108577966690063 15 2.6036427021026611 16 2.4678587913513184 17 1.7869548797607422
		 18 0.89156776666641235 19 0.11270870268344878 20 -0.21833653748035431 21 0.14135397970676422
		 22 0.97066998481750477 23 1.9051300287246702 24 2.5808660984039307 25 2.6342692375183105
		 26 1.7169314622879028 27 0.07120233029127121 28 -1.7800858020782471 29 -3.3138906955718994
		 30 -4.0077447891235352 31 -3.6512708663940434 32 -2.6781985759735107 33 -1.5323704481124878
		 34 -0.67626416683197021 35 -0.59989118576049805 36 -1.5849912166595459 37 -3.2112023830413818
		 38 -4.9147605895996094 39 -6.2606649398803711 40 -6.8655767440795898 41 -6.4231467247009277
		 42 -5.2606439590454102 43 -3.8285729885101318 44 -2.5797781944274902 45 -1.9679313898086548
		 46 -2.2362985610961914 47 -3.0809431076049805 48 -4.1338553428649902 49 -5.0266008377075195
		 50 -5.3916921615600586 51 -4.956383228302002 52 -3.9669075012207036 53 -2.8343038558959961
		 54 -1.965939402580261 55 -1.7641992568969727 56 -2.5953669548034668 57 -4.1806411743164062
		 58 -5.9232916831970215 59 -7.1542549133300781 60 -7.3638620376586923 61 -6.2435240745544434
		 62 -4.2245097160339355 63 -1.91449511051178 64 0.095152318477630615 65 1.2297873497009277
		 66 1.1458272933959961 67 0.16302827000617981 68 -1.1716326475143433 69 -2.2783434391021729
		 70 -2.6871967315673828 71 -2.1146643161773682 72 -0.87383502721786499 73 0.60988789796829224
		 74 1.910857677459717 75 2.603642463684082 76 2.4678587913513184 77 1.7869547605514526
		 78 0.89156776666641235 79 0.1127086877822876 80 -0.21833653748035431 81 0.14135397970676422
		 82 0.97067004442214977 83 1.9051300287246702 84 2.5808660984039307 85 2.6342692375183105
		 86 1.7860841751098633 87 0.71558964252471924 88 0.1014307513833046 89 0.097481779754161835
		 90 0.85637414455413818 91 2.1742544174194336 92 3.5052657127380371 93 4.4878015518188477
		 94 5.2077083587646484 95 5.9332642555236816 96 6.8675742149353027 97 8.0854616165161133
		 98 9.3044633865356445 99 10.237727165222168 100 10.5943603515625 101 10.150278091430664
		 102 9.1061897277832031 103 7.8092508316040048 104 6.609013557434082 105 5.8563227653503418
		 106 5.8138942718505859 107 6.2635540962219238 108 6.8323421478271484 109 6.9303750991821289
		 110 6.0504536628723145 111 4.0113534927368164 112 1.2274547815322876 113 -1.6623249053955078
		 114 -3.2092628479003906 115 -4.0077447891235352 116 -3.8877696990966801 117 -3.1715593338012695
		 118 -2.1144845485687256 119 -0.97206968069076549 120 8.0597306606478014e-009;
createNode animCurveTU -n "jaw_visibility";
	rename -uid "3BD5FC68-4323-D3D8-BC26-2FA956C7448D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "eyes_translateX";
	rename -uid "9B1EC02F-4FE1-7064-80F2-088A066F992F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.20706844329834;
createNode animCurveTL -n "eyes_translateY";
	rename -uid "C4EF5569-434D-8D53-A6D0-75829E7B4EC8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 22.342548370361328;
createNode animCurveTL -n "eyes_translateZ";
	rename -uid "0E875D97-4E90-81AF-8AC3-0E9748C6E6E4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.7847360800924434e-008;
createNode animCurveTU -n "eyes_scaleX";
	rename -uid "F84EE77E-4410-477E-2103-0C9AB8DC572A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleY";
	rename -uid "7B644A41-4755-FE30-AB0E-6C982572C423";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleZ";
	rename -uid "05784211-473D-4FCA-12E9-4B8D2B069735";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "eyes_rotateX";
	rename -uid "5B58803C-4A8B-8251-965C-E0AC807685C5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "eyes_rotateY";
	rename -uid "120061AB-420B-20FF-7322-9789A3C7676A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "eyes_rotateZ";
	rename -uid "13EFA3A1-4517-384E-B695-46BBFEB2526B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "eyes_visibility";
	rename -uid "B8BAD2E5-4FB5-E11C-60C4-B28678A1C48C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "6490D9EF-487A-EB62-4A28-34B602F3BF72";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "8C59899E-495F-45DE-865A-5C80795CA9D0";
createNode displayLayer -n "defaultLayer";
	rename -uid "DEE5F82A-4A68-6811-5B62-1E87395B6026";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "F3A2229E-47E9-A6F9-A942-25A6FB24FD8A";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "F6568948-4047-D6F4-E65E-72B48D73597D";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "328E41F9-4930-DEFF-C2E7-D19EAA822AD2";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 0\n                -polymeshes 1\n                -subdivSurfaces 0\n                -planes 0\n                -lights 0\n                -cameras 0\n                -controlVertices 0\n                -hulls 0\n                -grid 1\n                -imagePlane 0\n                -joints 1\n"
		+ "                -ikHandles 0\n                -deformers 0\n                -dynamics 0\n                -particleInstancers 0\n                -fluids 0\n                -hairSystems 0\n                -follicles 0\n                -nCloths 0\n                -nParticles 0\n                -nRigids 0\n                -dynamicConstraints 0\n                -locators 0\n                -manipulators 1\n                -pluginShapes 0\n                -dimensions 0\n                -handles 0\n                -pivots 0\n                -textures 0\n                -strokes 0\n                -motionTrails 0\n                -clipGhosts 0\n                -greasePencils 0\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1239\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 0 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 0\n            -polymeshes 1\n            -subdivSurfaces 0\n"
		+ "            -planes 0\n            -lights 0\n            -cameras 0\n            -controlVertices 0\n            -hulls 0\n            -grid 1\n            -imagePlane 0\n            -joints 1\n            -ikHandles 0\n            -deformers 0\n            -dynamics 0\n            -particleInstancers 0\n            -fluids 0\n            -hairSystems 0\n            -follicles 0\n            -nCloths 0\n            -nParticles 0\n            -nRigids 0\n            -dynamicConstraints 0\n            -locators 0\n            -manipulators 1\n            -pluginShapes 0\n            -dimensions 0\n            -handles 0\n            -pivots 0\n            -textures 0\n            -strokes 0\n            -motionTrails 0\n            -clipGhosts 0\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1239\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 0 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n"
		+ "                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n"
		+ "                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n"
		+ "                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n"
		+ "                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 24 100 -ps 2 76 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 0\\n    -polymeshes 1\\n    -subdivSurfaces 0\\n    -planes 0\\n    -lights 0\\n    -cameras 0\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 1\\n    -ikHandles 0\\n    -deformers 0\\n    -dynamics 0\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 0\\n    -handles 0\\n    -pivots 0\\n    -textures 0\\n    -strokes 0\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1239\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 0 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 0\\n    -polymeshes 1\\n    -subdivSurfaces 0\\n    -planes 0\\n    -lights 0\\n    -cameras 0\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 1\\n    -ikHandles 0\\n    -deformers 0\\n    -dynamics 0\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 0\\n    -handles 0\\n    -pivots 0\\n    -textures 0\\n    -strokes 0\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1239\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 0 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "28BCAFB3-4733-6A37-CA4C-03B831F99097";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 120 -ast 0 -aet 130 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 0;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
connectAttr "ref_frame.s" "Character1_Hips.is";
connectAttr "Character1_Hips_scaleX.o" "Character1_Hips.sx";
connectAttr "Character1_Hips_scaleY.o" "Character1_Hips.sy";
connectAttr "Character1_Hips_scaleZ.o" "Character1_Hips.sz";
connectAttr "Character1_Hips_translateX.o" "Character1_Hips.tx";
connectAttr "Character1_Hips_translateY.o" "Character1_Hips.ty";
connectAttr "Character1_Hips_translateZ.o" "Character1_Hips.tz";
connectAttr "Character1_Hips_rotateX.o" "Character1_Hips.rx";
connectAttr "Character1_Hips_rotateY.o" "Character1_Hips.ry";
connectAttr "Character1_Hips_rotateZ.o" "Character1_Hips.rz";
connectAttr "Character1_Hips_visibility.o" "Character1_Hips.v";
connectAttr "Character1_Hips.s" "Character1_LeftUpLeg.is";
connectAttr "Character1_LeftUpLeg_scaleX.o" "Character1_LeftUpLeg.sx";
connectAttr "Character1_LeftUpLeg_scaleY.o" "Character1_LeftUpLeg.sy";
connectAttr "Character1_LeftUpLeg_scaleZ.o" "Character1_LeftUpLeg.sz";
connectAttr "Character1_LeftUpLeg_translateX.o" "Character1_LeftUpLeg.tx";
connectAttr "Character1_LeftUpLeg_translateY.o" "Character1_LeftUpLeg.ty";
connectAttr "Character1_LeftUpLeg_translateZ.o" "Character1_LeftUpLeg.tz";
connectAttr "Character1_LeftUpLeg_rotateX.o" "Character1_LeftUpLeg.rx";
connectAttr "Character1_LeftUpLeg_rotateY.o" "Character1_LeftUpLeg.ry";
connectAttr "Character1_LeftUpLeg_rotateZ.o" "Character1_LeftUpLeg.rz";
connectAttr "Character1_LeftUpLeg_visibility.o" "Character1_LeftUpLeg.v";
connectAttr "Character1_LeftUpLeg.s" "Character1_LeftLeg.is";
connectAttr "Character1_LeftLeg_scaleX.o" "Character1_LeftLeg.sx";
connectAttr "Character1_LeftLeg_scaleY.o" "Character1_LeftLeg.sy";
connectAttr "Character1_LeftLeg_scaleZ.o" "Character1_LeftLeg.sz";
connectAttr "Character1_LeftLeg_translateX.o" "Character1_LeftLeg.tx";
connectAttr "Character1_LeftLeg_translateY.o" "Character1_LeftLeg.ty";
connectAttr "Character1_LeftLeg_translateZ.o" "Character1_LeftLeg.tz";
connectAttr "Character1_LeftLeg_rotateX.o" "Character1_LeftLeg.rx";
connectAttr "Character1_LeftLeg_rotateY.o" "Character1_LeftLeg.ry";
connectAttr "Character1_LeftLeg_rotateZ.o" "Character1_LeftLeg.rz";
connectAttr "Character1_LeftLeg_visibility.o" "Character1_LeftLeg.v";
connectAttr "Character1_LeftLeg.s" "Character1_LeftFoot.is";
connectAttr "Character1_LeftFoot_scaleX.o" "Character1_LeftFoot.sx";
connectAttr "Character1_LeftFoot_scaleY.o" "Character1_LeftFoot.sy";
connectAttr "Character1_LeftFoot_scaleZ.o" "Character1_LeftFoot.sz";
connectAttr "Character1_LeftFoot_translateX.o" "Character1_LeftFoot.tx";
connectAttr "Character1_LeftFoot_translateY.o" "Character1_LeftFoot.ty";
connectAttr "Character1_LeftFoot_translateZ.o" "Character1_LeftFoot.tz";
connectAttr "Character1_LeftFoot_rotateX.o" "Character1_LeftFoot.rx";
connectAttr "Character1_LeftFoot_rotateY.o" "Character1_LeftFoot.ry";
connectAttr "Character1_LeftFoot_rotateZ.o" "Character1_LeftFoot.rz";
connectAttr "Character1_LeftFoot_visibility.o" "Character1_LeftFoot.v";
connectAttr "Character1_LeftFoot.s" "Character1_LeftToeBase.is";
connectAttr "Character1_LeftToeBase_scaleX.o" "Character1_LeftToeBase.sx";
connectAttr "Character1_LeftToeBase_scaleY.o" "Character1_LeftToeBase.sy";
connectAttr "Character1_LeftToeBase_scaleZ.o" "Character1_LeftToeBase.sz";
connectAttr "Character1_LeftToeBase_translateX.o" "Character1_LeftToeBase.tx";
connectAttr "Character1_LeftToeBase_translateY.o" "Character1_LeftToeBase.ty";
connectAttr "Character1_LeftToeBase_translateZ.o" "Character1_LeftToeBase.tz";
connectAttr "Character1_LeftToeBase_rotateX.o" "Character1_LeftToeBase.rx";
connectAttr "Character1_LeftToeBase_rotateY.o" "Character1_LeftToeBase.ry";
connectAttr "Character1_LeftToeBase_rotateZ.o" "Character1_LeftToeBase.rz";
connectAttr "Character1_LeftToeBase_visibility.o" "Character1_LeftToeBase.v";
connectAttr "Character1_LeftToeBase.s" "Character1_LeftFootMiddle1.is";
connectAttr "Character1_LeftFootMiddle1_scaleX.o" "Character1_LeftFootMiddle1.sx"
		;
connectAttr "Character1_LeftFootMiddle1_scaleY.o" "Character1_LeftFootMiddle1.sy"
		;
connectAttr "Character1_LeftFootMiddle1_scaleZ.o" "Character1_LeftFootMiddle1.sz"
		;
connectAttr "Character1_LeftFootMiddle1_translateX.o" "Character1_LeftFootMiddle1.tx"
		;
connectAttr "Character1_LeftFootMiddle1_translateY.o" "Character1_LeftFootMiddle1.ty"
		;
connectAttr "Character1_LeftFootMiddle1_translateZ.o" "Character1_LeftFootMiddle1.tz"
		;
connectAttr "Character1_LeftFootMiddle1_rotateX.o" "Character1_LeftFootMiddle1.rx"
		;
connectAttr "Character1_LeftFootMiddle1_rotateY.o" "Character1_LeftFootMiddle1.ry"
		;
connectAttr "Character1_LeftFootMiddle1_rotateZ.o" "Character1_LeftFootMiddle1.rz"
		;
connectAttr "Character1_LeftFootMiddle1_visibility.o" "Character1_LeftFootMiddle1.v"
		;
connectAttr "Character1_LeftFootMiddle1.s" "Character1_LeftFootMiddle2.is";
connectAttr "Character1_LeftFootMiddle2_translateX.o" "Character1_LeftFootMiddle2.tx"
		;
connectAttr "Character1_LeftFootMiddle2_translateY.o" "Character1_LeftFootMiddle2.ty"
		;
connectAttr "Character1_LeftFootMiddle2_translateZ.o" "Character1_LeftFootMiddle2.tz"
		;
connectAttr "Character1_LeftFootMiddle2_scaleX.o" "Character1_LeftFootMiddle2.sx"
		;
connectAttr "Character1_LeftFootMiddle2_scaleY.o" "Character1_LeftFootMiddle2.sy"
		;
connectAttr "Character1_LeftFootMiddle2_scaleZ.o" "Character1_LeftFootMiddle2.sz"
		;
connectAttr "Character1_LeftFootMiddle2_rotateX.o" "Character1_LeftFootMiddle2.rx"
		;
connectAttr "Character1_LeftFootMiddle2_rotateY.o" "Character1_LeftFootMiddle2.ry"
		;
connectAttr "Character1_LeftFootMiddle2_rotateZ.o" "Character1_LeftFootMiddle2.rz"
		;
connectAttr "Character1_LeftFootMiddle2_visibility.o" "Character1_LeftFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_RightUpLeg.is";
connectAttr "Character1_RightUpLeg_scaleX.o" "Character1_RightUpLeg.sx";
connectAttr "Character1_RightUpLeg_scaleY.o" "Character1_RightUpLeg.sy";
connectAttr "Character1_RightUpLeg_scaleZ.o" "Character1_RightUpLeg.sz";
connectAttr "Character1_RightUpLeg_translateX.o" "Character1_RightUpLeg.tx";
connectAttr "Character1_RightUpLeg_translateY.o" "Character1_RightUpLeg.ty";
connectAttr "Character1_RightUpLeg_translateZ.o" "Character1_RightUpLeg.tz";
connectAttr "Character1_RightUpLeg_rotateX.o" "Character1_RightUpLeg.rx";
connectAttr "Character1_RightUpLeg_rotateY.o" "Character1_RightUpLeg.ry";
connectAttr "Character1_RightUpLeg_rotateZ.o" "Character1_RightUpLeg.rz";
connectAttr "Character1_RightUpLeg_visibility.o" "Character1_RightUpLeg.v";
connectAttr "Character1_RightUpLeg.s" "Character1_RightLeg.is";
connectAttr "Character1_RightLeg_scaleX.o" "Character1_RightLeg.sx";
connectAttr "Character1_RightLeg_scaleY.o" "Character1_RightLeg.sy";
connectAttr "Character1_RightLeg_scaleZ.o" "Character1_RightLeg.sz";
connectAttr "Character1_RightLeg_translateX.o" "Character1_RightLeg.tx";
connectAttr "Character1_RightLeg_translateY.o" "Character1_RightLeg.ty";
connectAttr "Character1_RightLeg_translateZ.o" "Character1_RightLeg.tz";
connectAttr "Character1_RightLeg_rotateX.o" "Character1_RightLeg.rx";
connectAttr "Character1_RightLeg_rotateY.o" "Character1_RightLeg.ry";
connectAttr "Character1_RightLeg_rotateZ.o" "Character1_RightLeg.rz";
connectAttr "Character1_RightLeg_visibility.o" "Character1_RightLeg.v";
connectAttr "Character1_RightLeg.s" "Character1_RightFoot.is";
connectAttr "Character1_RightFoot_scaleX.o" "Character1_RightFoot.sx";
connectAttr "Character1_RightFoot_scaleY.o" "Character1_RightFoot.sy";
connectAttr "Character1_RightFoot_scaleZ.o" "Character1_RightFoot.sz";
connectAttr "Character1_RightFoot_translateX.o" "Character1_RightFoot.tx";
connectAttr "Character1_RightFoot_translateY.o" "Character1_RightFoot.ty";
connectAttr "Character1_RightFoot_translateZ.o" "Character1_RightFoot.tz";
connectAttr "Character1_RightFoot_rotateX.o" "Character1_RightFoot.rx";
connectAttr "Character1_RightFoot_rotateY.o" "Character1_RightFoot.ry";
connectAttr "Character1_RightFoot_rotateZ.o" "Character1_RightFoot.rz";
connectAttr "Character1_RightFoot_visibility.o" "Character1_RightFoot.v";
connectAttr "Character1_RightFoot.s" "Character1_RightToeBase.is";
connectAttr "Character1_RightToeBase_scaleX.o" "Character1_RightToeBase.sx";
connectAttr "Character1_RightToeBase_scaleY.o" "Character1_RightToeBase.sy";
connectAttr "Character1_RightToeBase_scaleZ.o" "Character1_RightToeBase.sz";
connectAttr "Character1_RightToeBase_translateX.o" "Character1_RightToeBase.tx";
connectAttr "Character1_RightToeBase_translateY.o" "Character1_RightToeBase.ty";
connectAttr "Character1_RightToeBase_translateZ.o" "Character1_RightToeBase.tz";
connectAttr "Character1_RightToeBase_rotateX.o" "Character1_RightToeBase.rx";
connectAttr "Character1_RightToeBase_rotateY.o" "Character1_RightToeBase.ry";
connectAttr "Character1_RightToeBase_rotateZ.o" "Character1_RightToeBase.rz";
connectAttr "Character1_RightToeBase_visibility.o" "Character1_RightToeBase.v";
connectAttr "Character1_RightToeBase.s" "Character1_RightFootMiddle1.is";
connectAttr "Character1_RightFootMiddle1_scaleX.o" "Character1_RightFootMiddle1.sx"
		;
connectAttr "Character1_RightFootMiddle1_scaleY.o" "Character1_RightFootMiddle1.sy"
		;
connectAttr "Character1_RightFootMiddle1_scaleZ.o" "Character1_RightFootMiddle1.sz"
		;
connectAttr "Character1_RightFootMiddle1_translateX.o" "Character1_RightFootMiddle1.tx"
		;
connectAttr "Character1_RightFootMiddle1_translateY.o" "Character1_RightFootMiddle1.ty"
		;
connectAttr "Character1_RightFootMiddle1_translateZ.o" "Character1_RightFootMiddle1.tz"
		;
connectAttr "Character1_RightFootMiddle1_rotateX.o" "Character1_RightFootMiddle1.rx"
		;
connectAttr "Character1_RightFootMiddle1_rotateY.o" "Character1_RightFootMiddle1.ry"
		;
connectAttr "Character1_RightFootMiddle1_rotateZ.o" "Character1_RightFootMiddle1.rz"
		;
connectAttr "Character1_RightFootMiddle1_visibility.o" "Character1_RightFootMiddle1.v"
		;
connectAttr "Character1_RightFootMiddle1.s" "Character1_RightFootMiddle2.is";
connectAttr "Character1_RightFootMiddle2_translateX.o" "Character1_RightFootMiddle2.tx"
		;
connectAttr "Character1_RightFootMiddle2_translateY.o" "Character1_RightFootMiddle2.ty"
		;
connectAttr "Character1_RightFootMiddle2_translateZ.o" "Character1_RightFootMiddle2.tz"
		;
connectAttr "Character1_RightFootMiddle2_scaleX.o" "Character1_RightFootMiddle2.sx"
		;
connectAttr "Character1_RightFootMiddle2_scaleY.o" "Character1_RightFootMiddle2.sy"
		;
connectAttr "Character1_RightFootMiddle2_scaleZ.o" "Character1_RightFootMiddle2.sz"
		;
connectAttr "Character1_RightFootMiddle2_rotateX.o" "Character1_RightFootMiddle2.rx"
		;
connectAttr "Character1_RightFootMiddle2_rotateY.o" "Character1_RightFootMiddle2.ry"
		;
connectAttr "Character1_RightFootMiddle2_rotateZ.o" "Character1_RightFootMiddle2.rz"
		;
connectAttr "Character1_RightFootMiddle2_visibility.o" "Character1_RightFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_Spine.is";
connectAttr "Character1_Spine_scaleX.o" "Character1_Spine.sx";
connectAttr "Character1_Spine_scaleY.o" "Character1_Spine.sy";
connectAttr "Character1_Spine_scaleZ.o" "Character1_Spine.sz";
connectAttr "Character1_Spine_translateX.o" "Character1_Spine.tx";
connectAttr "Character1_Spine_translateY.o" "Character1_Spine.ty";
connectAttr "Character1_Spine_translateZ.o" "Character1_Spine.tz";
connectAttr "Character1_Spine_rotateX.o" "Character1_Spine.rx";
connectAttr "Character1_Spine_rotateY.o" "Character1_Spine.ry";
connectAttr "Character1_Spine_rotateZ.o" "Character1_Spine.rz";
connectAttr "Character1_Spine_visibility.o" "Character1_Spine.v";
connectAttr "Character1_Spine.s" "Character1_Spine1.is";
connectAttr "Character1_Spine1_scaleX.o" "Character1_Spine1.sx";
connectAttr "Character1_Spine1_scaleY.o" "Character1_Spine1.sy";
connectAttr "Character1_Spine1_scaleZ.o" "Character1_Spine1.sz";
connectAttr "Character1_Spine1_translateX.o" "Character1_Spine1.tx";
connectAttr "Character1_Spine1_translateY.o" "Character1_Spine1.ty";
connectAttr "Character1_Spine1_translateZ.o" "Character1_Spine1.tz";
connectAttr "Character1_Spine1_rotateX.o" "Character1_Spine1.rx";
connectAttr "Character1_Spine1_rotateY.o" "Character1_Spine1.ry";
connectAttr "Character1_Spine1_rotateZ.o" "Character1_Spine1.rz";
connectAttr "Character1_Spine1_visibility.o" "Character1_Spine1.v";
connectAttr "Character1_Spine1.s" "Character1_LeftShoulder.is";
connectAttr "Character1_LeftShoulder_scaleX.o" "Character1_LeftShoulder.sx";
connectAttr "Character1_LeftShoulder_scaleY.o" "Character1_LeftShoulder.sy";
connectAttr "Character1_LeftShoulder_scaleZ.o" "Character1_LeftShoulder.sz";
connectAttr "Character1_LeftShoulder_translateX.o" "Character1_LeftShoulder.tx";
connectAttr "Character1_LeftShoulder_translateY.o" "Character1_LeftShoulder.ty";
connectAttr "Character1_LeftShoulder_translateZ.o" "Character1_LeftShoulder.tz";
connectAttr "Character1_LeftShoulder_rotateX.o" "Character1_LeftShoulder.rx";
connectAttr "Character1_LeftShoulder_rotateY.o" "Character1_LeftShoulder.ry";
connectAttr "Character1_LeftShoulder_rotateZ.o" "Character1_LeftShoulder.rz";
connectAttr "Character1_LeftShoulder_visibility.o" "Character1_LeftShoulder.v";
connectAttr "Character1_LeftShoulder.s" "Character1_LeftArm.is";
connectAttr "Character1_LeftArm_scaleX.o" "Character1_LeftArm.sx";
connectAttr "Character1_LeftArm_scaleY.o" "Character1_LeftArm.sy";
connectAttr "Character1_LeftArm_scaleZ.o" "Character1_LeftArm.sz";
connectAttr "Character1_LeftArm_translateX.o" "Character1_LeftArm.tx";
connectAttr "Character1_LeftArm_translateY.o" "Character1_LeftArm.ty";
connectAttr "Character1_LeftArm_translateZ.o" "Character1_LeftArm.tz";
connectAttr "Character1_LeftArm_rotateX.o" "Character1_LeftArm.rx";
connectAttr "Character1_LeftArm_rotateY.o" "Character1_LeftArm.ry";
connectAttr "Character1_LeftArm_rotateZ.o" "Character1_LeftArm.rz";
connectAttr "Character1_LeftArm_visibility.o" "Character1_LeftArm.v";
connectAttr "Character1_LeftArm.s" "Character1_LeftForeArm.is";
connectAttr "Character1_LeftForeArm_scaleX.o" "Character1_LeftForeArm.sx";
connectAttr "Character1_LeftForeArm_scaleY.o" "Character1_LeftForeArm.sy";
connectAttr "Character1_LeftForeArm_scaleZ.o" "Character1_LeftForeArm.sz";
connectAttr "Character1_LeftForeArm_translateX.o" "Character1_LeftForeArm.tx";
connectAttr "Character1_LeftForeArm_translateY.o" "Character1_LeftForeArm.ty";
connectAttr "Character1_LeftForeArm_translateZ.o" "Character1_LeftForeArm.tz";
connectAttr "Character1_LeftForeArm_rotateX.o" "Character1_LeftForeArm.rx";
connectAttr "Character1_LeftForeArm_rotateY.o" "Character1_LeftForeArm.ry";
connectAttr "Character1_LeftForeArm_rotateZ.o" "Character1_LeftForeArm.rz";
connectAttr "Character1_LeftForeArm_visibility.o" "Character1_LeftForeArm.v";
connectAttr "Character1_LeftForeArm.s" "Character1_LeftHand.is";
connectAttr "Character1_LeftHand_scaleX.o" "Character1_LeftHand.sx";
connectAttr "Character1_LeftHand_scaleY.o" "Character1_LeftHand.sy";
connectAttr "Character1_LeftHand_scaleZ.o" "Character1_LeftHand.sz";
connectAttr "Character1_LeftHand_translateX.o" "Character1_LeftHand.tx";
connectAttr "Character1_LeftHand_translateY.o" "Character1_LeftHand.ty";
connectAttr "Character1_LeftHand_translateZ.o" "Character1_LeftHand.tz";
connectAttr "Character1_LeftHand_rotateX.o" "Character1_LeftHand.rx";
connectAttr "Character1_LeftHand_rotateY.o" "Character1_LeftHand.ry";
connectAttr "Character1_LeftHand_rotateZ.o" "Character1_LeftHand.rz";
connectAttr "Character1_LeftHand_visibility.o" "Character1_LeftHand.v";
connectAttr "Character1_LeftHand.s" "Character1_LeftHandThumb1.is";
connectAttr "Character1_LeftHandThumb1_scaleX.o" "Character1_LeftHandThumb1.sx";
connectAttr "Character1_LeftHandThumb1_scaleY.o" "Character1_LeftHandThumb1.sy";
connectAttr "Character1_LeftHandThumb1_scaleZ.o" "Character1_LeftHandThumb1.sz";
connectAttr "Character1_LeftHandThumb1_translateX.o" "Character1_LeftHandThumb1.tx"
		;
connectAttr "Character1_LeftHandThumb1_translateY.o" "Character1_LeftHandThumb1.ty"
		;
connectAttr "Character1_LeftHandThumb1_translateZ.o" "Character1_LeftHandThumb1.tz"
		;
connectAttr "Character1_LeftHandThumb1_rotateX.o" "Character1_LeftHandThumb1.rx"
		;
connectAttr "Character1_LeftHandThumb1_rotateY.o" "Character1_LeftHandThumb1.ry"
		;
connectAttr "Character1_LeftHandThumb1_rotateZ.o" "Character1_LeftHandThumb1.rz"
		;
connectAttr "Character1_LeftHandThumb1_visibility.o" "Character1_LeftHandThumb1.v"
		;
connectAttr "Character1_LeftHandThumb1.s" "Character1_LeftHandThumb2.is";
connectAttr "Character1_LeftHandThumb2_scaleX.o" "Character1_LeftHandThumb2.sx";
connectAttr "Character1_LeftHandThumb2_scaleY.o" "Character1_LeftHandThumb2.sy";
connectAttr "Character1_LeftHandThumb2_scaleZ.o" "Character1_LeftHandThumb2.sz";
connectAttr "Character1_LeftHandThumb2_translateX.o" "Character1_LeftHandThumb2.tx"
		;
connectAttr "Character1_LeftHandThumb2_translateY.o" "Character1_LeftHandThumb2.ty"
		;
connectAttr "Character1_LeftHandThumb2_translateZ.o" "Character1_LeftHandThumb2.tz"
		;
connectAttr "Character1_LeftHandThumb2_rotateX.o" "Character1_LeftHandThumb2.rx"
		;
connectAttr "Character1_LeftHandThumb2_rotateY.o" "Character1_LeftHandThumb2.ry"
		;
connectAttr "Character1_LeftHandThumb2_rotateZ.o" "Character1_LeftHandThumb2.rz"
		;
connectAttr "Character1_LeftHandThumb2_visibility.o" "Character1_LeftHandThumb2.v"
		;
connectAttr "Character1_LeftHandThumb2.s" "Character1_LeftHandThumb3.is";
connectAttr "Character1_LeftHandThumb3_translateX.o" "Character1_LeftHandThumb3.tx"
		;
connectAttr "Character1_LeftHandThumb3_translateY.o" "Character1_LeftHandThumb3.ty"
		;
connectAttr "Character1_LeftHandThumb3_translateZ.o" "Character1_LeftHandThumb3.tz"
		;
connectAttr "Character1_LeftHandThumb3_scaleX.o" "Character1_LeftHandThumb3.sx";
connectAttr "Character1_LeftHandThumb3_scaleY.o" "Character1_LeftHandThumb3.sy";
connectAttr "Character1_LeftHandThumb3_scaleZ.o" "Character1_LeftHandThumb3.sz";
connectAttr "Character1_LeftHandThumb3_rotateX.o" "Character1_LeftHandThumb3.rx"
		;
connectAttr "Character1_LeftHandThumb3_rotateY.o" "Character1_LeftHandThumb3.ry"
		;
connectAttr "Character1_LeftHandThumb3_rotateZ.o" "Character1_LeftHandThumb3.rz"
		;
connectAttr "Character1_LeftHandThumb3_visibility.o" "Character1_LeftHandThumb3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandIndex1.is";
connectAttr "Character1_LeftHandIndex1_scaleX.o" "Character1_LeftHandIndex1.sx";
connectAttr "Character1_LeftHandIndex1_scaleY.o" "Character1_LeftHandIndex1.sy";
connectAttr "Character1_LeftHandIndex1_scaleZ.o" "Character1_LeftHandIndex1.sz";
connectAttr "Character1_LeftHandIndex1_translateX.o" "Character1_LeftHandIndex1.tx"
		;
connectAttr "Character1_LeftHandIndex1_translateY.o" "Character1_LeftHandIndex1.ty"
		;
connectAttr "Character1_LeftHandIndex1_translateZ.o" "Character1_LeftHandIndex1.tz"
		;
connectAttr "Character1_LeftHandIndex1_rotateX.o" "Character1_LeftHandIndex1.rx"
		;
connectAttr "Character1_LeftHandIndex1_rotateY.o" "Character1_LeftHandIndex1.ry"
		;
connectAttr "Character1_LeftHandIndex1_rotateZ.o" "Character1_LeftHandIndex1.rz"
		;
connectAttr "Character1_LeftHandIndex1_visibility.o" "Character1_LeftHandIndex1.v"
		;
connectAttr "Character1_LeftHandIndex1.s" "Character1_LeftHandIndex2.is";
connectAttr "Character1_LeftHandIndex2_scaleX.o" "Character1_LeftHandIndex2.sx";
connectAttr "Character1_LeftHandIndex2_scaleY.o" "Character1_LeftHandIndex2.sy";
connectAttr "Character1_LeftHandIndex2_scaleZ.o" "Character1_LeftHandIndex2.sz";
connectAttr "Character1_LeftHandIndex2_translateX.o" "Character1_LeftHandIndex2.tx"
		;
connectAttr "Character1_LeftHandIndex2_translateY.o" "Character1_LeftHandIndex2.ty"
		;
connectAttr "Character1_LeftHandIndex2_translateZ.o" "Character1_LeftHandIndex2.tz"
		;
connectAttr "Character1_LeftHandIndex2_rotateX.o" "Character1_LeftHandIndex2.rx"
		;
connectAttr "Character1_LeftHandIndex2_rotateY.o" "Character1_LeftHandIndex2.ry"
		;
connectAttr "Character1_LeftHandIndex2_rotateZ.o" "Character1_LeftHandIndex2.rz"
		;
connectAttr "Character1_LeftHandIndex2_visibility.o" "Character1_LeftHandIndex2.v"
		;
connectAttr "Character1_LeftHandIndex2.s" "Character1_LeftHandIndex3.is";
connectAttr "Character1_LeftHandIndex3_translateX.o" "Character1_LeftHandIndex3.tx"
		;
connectAttr "Character1_LeftHandIndex3_translateY.o" "Character1_LeftHandIndex3.ty"
		;
connectAttr "Character1_LeftHandIndex3_translateZ.o" "Character1_LeftHandIndex3.tz"
		;
connectAttr "Character1_LeftHandIndex3_scaleX.o" "Character1_LeftHandIndex3.sx";
connectAttr "Character1_LeftHandIndex3_scaleY.o" "Character1_LeftHandIndex3.sy";
connectAttr "Character1_LeftHandIndex3_scaleZ.o" "Character1_LeftHandIndex3.sz";
connectAttr "Character1_LeftHandIndex3_rotateX.o" "Character1_LeftHandIndex3.rx"
		;
connectAttr "Character1_LeftHandIndex3_rotateY.o" "Character1_LeftHandIndex3.ry"
		;
connectAttr "Character1_LeftHandIndex3_rotateZ.o" "Character1_LeftHandIndex3.rz"
		;
connectAttr "Character1_LeftHandIndex3_visibility.o" "Character1_LeftHandIndex3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandRing1.is";
connectAttr "Character1_LeftHandRing1_scaleX.o" "Character1_LeftHandRing1.sx";
connectAttr "Character1_LeftHandRing1_scaleY.o" "Character1_LeftHandRing1.sy";
connectAttr "Character1_LeftHandRing1_scaleZ.o" "Character1_LeftHandRing1.sz";
connectAttr "Character1_LeftHandRing1_translateX.o" "Character1_LeftHandRing1.tx"
		;
connectAttr "Character1_LeftHandRing1_translateY.o" "Character1_LeftHandRing1.ty"
		;
connectAttr "Character1_LeftHandRing1_translateZ.o" "Character1_LeftHandRing1.tz"
		;
connectAttr "Character1_LeftHandRing1_rotateX.o" "Character1_LeftHandRing1.rx";
connectAttr "Character1_LeftHandRing1_rotateY.o" "Character1_LeftHandRing1.ry";
connectAttr "Character1_LeftHandRing1_rotateZ.o" "Character1_LeftHandRing1.rz";
connectAttr "Character1_LeftHandRing1_visibility.o" "Character1_LeftHandRing1.v"
		;
connectAttr "Character1_LeftHandRing1.s" "Character1_LeftHandRing2.is";
connectAttr "Character1_LeftHandRing2_scaleX.o" "Character1_LeftHandRing2.sx";
connectAttr "Character1_LeftHandRing2_scaleY.o" "Character1_LeftHandRing2.sy";
connectAttr "Character1_LeftHandRing2_scaleZ.o" "Character1_LeftHandRing2.sz";
connectAttr "Character1_LeftHandRing2_translateX.o" "Character1_LeftHandRing2.tx"
		;
connectAttr "Character1_LeftHandRing2_translateY.o" "Character1_LeftHandRing2.ty"
		;
connectAttr "Character1_LeftHandRing2_translateZ.o" "Character1_LeftHandRing2.tz"
		;
connectAttr "Character1_LeftHandRing2_rotateX.o" "Character1_LeftHandRing2.rx";
connectAttr "Character1_LeftHandRing2_rotateY.o" "Character1_LeftHandRing2.ry";
connectAttr "Character1_LeftHandRing2_rotateZ.o" "Character1_LeftHandRing2.rz";
connectAttr "Character1_LeftHandRing2_visibility.o" "Character1_LeftHandRing2.v"
		;
connectAttr "Character1_LeftHandRing2.s" "Character1_LeftHandRing3.is";
connectAttr "Character1_LeftHandRing3_translateX.o" "Character1_LeftHandRing3.tx"
		;
connectAttr "Character1_LeftHandRing3_translateY.o" "Character1_LeftHandRing3.ty"
		;
connectAttr "Character1_LeftHandRing3_translateZ.o" "Character1_LeftHandRing3.tz"
		;
connectAttr "Character1_LeftHandRing3_scaleX.o" "Character1_LeftHandRing3.sx";
connectAttr "Character1_LeftHandRing3_scaleY.o" "Character1_LeftHandRing3.sy";
connectAttr "Character1_LeftHandRing3_scaleZ.o" "Character1_LeftHandRing3.sz";
connectAttr "Character1_LeftHandRing3_rotateX.o" "Character1_LeftHandRing3.rx";
connectAttr "Character1_LeftHandRing3_rotateY.o" "Character1_LeftHandRing3.ry";
connectAttr "Character1_LeftHandRing3_rotateZ.o" "Character1_LeftHandRing3.rz";
connectAttr "Character1_LeftHandRing3_visibility.o" "Character1_LeftHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_RightShoulder.is";
connectAttr "Character1_RightShoulder_scaleX.o" "Character1_RightShoulder.sx";
connectAttr "Character1_RightShoulder_scaleY.o" "Character1_RightShoulder.sy";
connectAttr "Character1_RightShoulder_scaleZ.o" "Character1_RightShoulder.sz";
connectAttr "Character1_RightShoulder_translateX.o" "Character1_RightShoulder.tx"
		;
connectAttr "Character1_RightShoulder_translateY.o" "Character1_RightShoulder.ty"
		;
connectAttr "Character1_RightShoulder_translateZ.o" "Character1_RightShoulder.tz"
		;
connectAttr "Character1_RightShoulder_rotateX.o" "Character1_RightShoulder.rx";
connectAttr "Character1_RightShoulder_rotateY.o" "Character1_RightShoulder.ry";
connectAttr "Character1_RightShoulder_rotateZ.o" "Character1_RightShoulder.rz";
connectAttr "Character1_RightShoulder_visibility.o" "Character1_RightShoulder.v"
		;
connectAttr "Character1_RightShoulder.s" "Character1_RightArm.is";
connectAttr "Character1_RightArm_scaleX.o" "Character1_RightArm.sx";
connectAttr "Character1_RightArm_scaleY.o" "Character1_RightArm.sy";
connectAttr "Character1_RightArm_scaleZ.o" "Character1_RightArm.sz";
connectAttr "Character1_RightArm_translateX.o" "Character1_RightArm.tx";
connectAttr "Character1_RightArm_translateY.o" "Character1_RightArm.ty";
connectAttr "Character1_RightArm_translateZ.o" "Character1_RightArm.tz";
connectAttr "Character1_RightArm_rotateX.o" "Character1_RightArm.rx";
connectAttr "Character1_RightArm_rotateY.o" "Character1_RightArm.ry";
connectAttr "Character1_RightArm_rotateZ.o" "Character1_RightArm.rz";
connectAttr "Character1_RightArm_visibility.o" "Character1_RightArm.v";
connectAttr "Character1_RightArm.s" "Character1_RightForeArm.is";
connectAttr "Character1_RightForeArm_scaleX.o" "Character1_RightForeArm.sx";
connectAttr "Character1_RightForeArm_scaleY.o" "Character1_RightForeArm.sy";
connectAttr "Character1_RightForeArm_scaleZ.o" "Character1_RightForeArm.sz";
connectAttr "Character1_RightForeArm_translateX.o" "Character1_RightForeArm.tx";
connectAttr "Character1_RightForeArm_translateY.o" "Character1_RightForeArm.ty";
connectAttr "Character1_RightForeArm_translateZ.o" "Character1_RightForeArm.tz";
connectAttr "Character1_RightForeArm_rotateX.o" "Character1_RightForeArm.rx";
connectAttr "Character1_RightForeArm_rotateY.o" "Character1_RightForeArm.ry";
connectAttr "Character1_RightForeArm_rotateZ.o" "Character1_RightForeArm.rz";
connectAttr "Character1_RightForeArm_visibility.o" "Character1_RightForeArm.v";
connectAttr "Character1_RightForeArm.s" "Character1_RightHand.is";
connectAttr "Character1_RightHand_scaleX.o" "Character1_RightHand.sx";
connectAttr "Character1_RightHand_scaleY.o" "Character1_RightHand.sy";
connectAttr "Character1_RightHand_scaleZ.o" "Character1_RightHand.sz";
connectAttr "Character1_RightHand_translateX.o" "Character1_RightHand.tx";
connectAttr "Character1_RightHand_translateY.o" "Character1_RightHand.ty";
connectAttr "Character1_RightHand_translateZ.o" "Character1_RightHand.tz";
connectAttr "Character1_RightHand_rotateX.o" "Character1_RightHand.rx";
connectAttr "Character1_RightHand_rotateY.o" "Character1_RightHand.ry";
connectAttr "Character1_RightHand_rotateZ.o" "Character1_RightHand.rz";
connectAttr "Character1_RightHand_visibility.o" "Character1_RightHand.v";
connectAttr "Character1_RightHand.s" "Character1_RightHandThumb1.is";
connectAttr "Character1_RightHandThumb1_scaleX.o" "Character1_RightHandThumb1.sx"
		;
connectAttr "Character1_RightHandThumb1_scaleY.o" "Character1_RightHandThumb1.sy"
		;
connectAttr "Character1_RightHandThumb1_scaleZ.o" "Character1_RightHandThumb1.sz"
		;
connectAttr "Character1_RightHandThumb1_translateX.o" "Character1_RightHandThumb1.tx"
		;
connectAttr "Character1_RightHandThumb1_translateY.o" "Character1_RightHandThumb1.ty"
		;
connectAttr "Character1_RightHandThumb1_translateZ.o" "Character1_RightHandThumb1.tz"
		;
connectAttr "Character1_RightHandThumb1_rotateX.o" "Character1_RightHandThumb1.rx"
		;
connectAttr "Character1_RightHandThumb1_rotateY.o" "Character1_RightHandThumb1.ry"
		;
connectAttr "Character1_RightHandThumb1_rotateZ.o" "Character1_RightHandThumb1.rz"
		;
connectAttr "Character1_RightHandThumb1_visibility.o" "Character1_RightHandThumb1.v"
		;
connectAttr "Character1_RightHandThumb1.s" "Character1_RightHandThumb2.is";
connectAttr "Character1_RightHandThumb2_scaleX.o" "Character1_RightHandThumb2.sx"
		;
connectAttr "Character1_RightHandThumb2_scaleY.o" "Character1_RightHandThumb2.sy"
		;
connectAttr "Character1_RightHandThumb2_scaleZ.o" "Character1_RightHandThumb2.sz"
		;
connectAttr "Character1_RightHandThumb2_translateX.o" "Character1_RightHandThumb2.tx"
		;
connectAttr "Character1_RightHandThumb2_translateY.o" "Character1_RightHandThumb2.ty"
		;
connectAttr "Character1_RightHandThumb2_translateZ.o" "Character1_RightHandThumb2.tz"
		;
connectAttr "Character1_RightHandThumb2_rotateX.o" "Character1_RightHandThumb2.rx"
		;
connectAttr "Character1_RightHandThumb2_rotateY.o" "Character1_RightHandThumb2.ry"
		;
connectAttr "Character1_RightHandThumb2_rotateZ.o" "Character1_RightHandThumb2.rz"
		;
connectAttr "Character1_RightHandThumb2_visibility.o" "Character1_RightHandThumb2.v"
		;
connectAttr "Character1_RightHandThumb2.s" "Character1_RightHandThumb3.is";
connectAttr "Character1_RightHandThumb3_translateX.o" "Character1_RightHandThumb3.tx"
		;
connectAttr "Character1_RightHandThumb3_translateY.o" "Character1_RightHandThumb3.ty"
		;
connectAttr "Character1_RightHandThumb3_translateZ.o" "Character1_RightHandThumb3.tz"
		;
connectAttr "Character1_RightHandThumb3_scaleX.o" "Character1_RightHandThumb3.sx"
		;
connectAttr "Character1_RightHandThumb3_scaleY.o" "Character1_RightHandThumb3.sy"
		;
connectAttr "Character1_RightHandThumb3_scaleZ.o" "Character1_RightHandThumb3.sz"
		;
connectAttr "Character1_RightHandThumb3_rotateX.o" "Character1_RightHandThumb3.rx"
		;
connectAttr "Character1_RightHandThumb3_rotateY.o" "Character1_RightHandThumb3.ry"
		;
connectAttr "Character1_RightHandThumb3_rotateZ.o" "Character1_RightHandThumb3.rz"
		;
connectAttr "Character1_RightHandThumb3_visibility.o" "Character1_RightHandThumb3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandIndex1.is";
connectAttr "Character1_RightHandIndex1_scaleX.o" "Character1_RightHandIndex1.sx"
		;
connectAttr "Character1_RightHandIndex1_scaleY.o" "Character1_RightHandIndex1.sy"
		;
connectAttr "Character1_RightHandIndex1_scaleZ.o" "Character1_RightHandIndex1.sz"
		;
connectAttr "Character1_RightHandIndex1_translateX.o" "Character1_RightHandIndex1.tx"
		;
connectAttr "Character1_RightHandIndex1_translateY.o" "Character1_RightHandIndex1.ty"
		;
connectAttr "Character1_RightHandIndex1_translateZ.o" "Character1_RightHandIndex1.tz"
		;
connectAttr "Character1_RightHandIndex1_rotateX.o" "Character1_RightHandIndex1.rx"
		;
connectAttr "Character1_RightHandIndex1_rotateY.o" "Character1_RightHandIndex1.ry"
		;
connectAttr "Character1_RightHandIndex1_rotateZ.o" "Character1_RightHandIndex1.rz"
		;
connectAttr "Character1_RightHandIndex1_visibility.o" "Character1_RightHandIndex1.v"
		;
connectAttr "Character1_RightHandIndex1.s" "Character1_RightHandIndex2.is";
connectAttr "Character1_RightHandIndex2_scaleX.o" "Character1_RightHandIndex2.sx"
		;
connectAttr "Character1_RightHandIndex2_scaleY.o" "Character1_RightHandIndex2.sy"
		;
connectAttr "Character1_RightHandIndex2_scaleZ.o" "Character1_RightHandIndex2.sz"
		;
connectAttr "Character1_RightHandIndex2_translateX.o" "Character1_RightHandIndex2.tx"
		;
connectAttr "Character1_RightHandIndex2_translateY.o" "Character1_RightHandIndex2.ty"
		;
connectAttr "Character1_RightHandIndex2_translateZ.o" "Character1_RightHandIndex2.tz"
		;
connectAttr "Character1_RightHandIndex2_rotateX.o" "Character1_RightHandIndex2.rx"
		;
connectAttr "Character1_RightHandIndex2_rotateY.o" "Character1_RightHandIndex2.ry"
		;
connectAttr "Character1_RightHandIndex2_rotateZ.o" "Character1_RightHandIndex2.rz"
		;
connectAttr "Character1_RightHandIndex2_visibility.o" "Character1_RightHandIndex2.v"
		;
connectAttr "Character1_RightHandIndex2.s" "Character1_RightHandIndex3.is";
connectAttr "Character1_RightHandIndex3_translateX.o" "Character1_RightHandIndex3.tx"
		;
connectAttr "Character1_RightHandIndex3_translateY.o" "Character1_RightHandIndex3.ty"
		;
connectAttr "Character1_RightHandIndex3_translateZ.o" "Character1_RightHandIndex3.tz"
		;
connectAttr "Character1_RightHandIndex3_scaleX.o" "Character1_RightHandIndex3.sx"
		;
connectAttr "Character1_RightHandIndex3_scaleY.o" "Character1_RightHandIndex3.sy"
		;
connectAttr "Character1_RightHandIndex3_scaleZ.o" "Character1_RightHandIndex3.sz"
		;
connectAttr "Character1_RightHandIndex3_rotateX.o" "Character1_RightHandIndex3.rx"
		;
connectAttr "Character1_RightHandIndex3_rotateY.o" "Character1_RightHandIndex3.ry"
		;
connectAttr "Character1_RightHandIndex3_rotateZ.o" "Character1_RightHandIndex3.rz"
		;
connectAttr "Character1_RightHandIndex3_visibility.o" "Character1_RightHandIndex3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandRing1.is";
connectAttr "Character1_RightHandRing1_scaleX.o" "Character1_RightHandRing1.sx";
connectAttr "Character1_RightHandRing1_scaleY.o" "Character1_RightHandRing1.sy";
connectAttr "Character1_RightHandRing1_scaleZ.o" "Character1_RightHandRing1.sz";
connectAttr "Character1_RightHandRing1_translateX.o" "Character1_RightHandRing1.tx"
		;
connectAttr "Character1_RightHandRing1_translateY.o" "Character1_RightHandRing1.ty"
		;
connectAttr "Character1_RightHandRing1_translateZ.o" "Character1_RightHandRing1.tz"
		;
connectAttr "Character1_RightHandRing1_rotateX.o" "Character1_RightHandRing1.rx"
		;
connectAttr "Character1_RightHandRing1_rotateY.o" "Character1_RightHandRing1.ry"
		;
connectAttr "Character1_RightHandRing1_rotateZ.o" "Character1_RightHandRing1.rz"
		;
connectAttr "Character1_RightHandRing1_visibility.o" "Character1_RightHandRing1.v"
		;
connectAttr "Character1_RightHandRing1.s" "Character1_RightHandRing2.is";
connectAttr "Character1_RightHandRing2_scaleX.o" "Character1_RightHandRing2.sx";
connectAttr "Character1_RightHandRing2_scaleY.o" "Character1_RightHandRing2.sy";
connectAttr "Character1_RightHandRing2_scaleZ.o" "Character1_RightHandRing2.sz";
connectAttr "Character1_RightHandRing2_translateX.o" "Character1_RightHandRing2.tx"
		;
connectAttr "Character1_RightHandRing2_translateY.o" "Character1_RightHandRing2.ty"
		;
connectAttr "Character1_RightHandRing2_translateZ.o" "Character1_RightHandRing2.tz"
		;
connectAttr "Character1_RightHandRing2_rotateX.o" "Character1_RightHandRing2.rx"
		;
connectAttr "Character1_RightHandRing2_rotateY.o" "Character1_RightHandRing2.ry"
		;
connectAttr "Character1_RightHandRing2_rotateZ.o" "Character1_RightHandRing2.rz"
		;
connectAttr "Character1_RightHandRing2_visibility.o" "Character1_RightHandRing2.v"
		;
connectAttr "Character1_RightHandRing2.s" "Character1_RightHandRing3.is";
connectAttr "Character1_RightHandRing3_translateX.o" "Character1_RightHandRing3.tx"
		;
connectAttr "Character1_RightHandRing3_translateY.o" "Character1_RightHandRing3.ty"
		;
connectAttr "Character1_RightHandRing3_translateZ.o" "Character1_RightHandRing3.tz"
		;
connectAttr "Character1_RightHandRing3_scaleX.o" "Character1_RightHandRing3.sx";
connectAttr "Character1_RightHandRing3_scaleY.o" "Character1_RightHandRing3.sy";
connectAttr "Character1_RightHandRing3_scaleZ.o" "Character1_RightHandRing3.sz";
connectAttr "Character1_RightHandRing3_rotateX.o" "Character1_RightHandRing3.rx"
		;
connectAttr "Character1_RightHandRing3_rotateY.o" "Character1_RightHandRing3.ry"
		;
connectAttr "Character1_RightHandRing3_rotateZ.o" "Character1_RightHandRing3.rz"
		;
connectAttr "Character1_RightHandRing3_visibility.o" "Character1_RightHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_Neck.is";
connectAttr "Character1_Neck_scaleX.o" "Character1_Neck.sx";
connectAttr "Character1_Neck_scaleY.o" "Character1_Neck.sy";
connectAttr "Character1_Neck_scaleZ.o" "Character1_Neck.sz";
connectAttr "Character1_Neck_translateX.o" "Character1_Neck.tx";
connectAttr "Character1_Neck_translateY.o" "Character1_Neck.ty";
connectAttr "Character1_Neck_translateZ.o" "Character1_Neck.tz";
connectAttr "Character1_Neck_rotateX.o" "Character1_Neck.rx";
connectAttr "Character1_Neck_rotateY.o" "Character1_Neck.ry";
connectAttr "Character1_Neck_rotateZ.o" "Character1_Neck.rz";
connectAttr "Character1_Neck_visibility.o" "Character1_Neck.v";
connectAttr "Character1_Neck.s" "Character1_Head.is";
connectAttr "Character1_Head_scaleX.o" "Character1_Head.sx";
connectAttr "Character1_Head_scaleY.o" "Character1_Head.sy";
connectAttr "Character1_Head_scaleZ.o" "Character1_Head.sz";
connectAttr "Character1_Head_translateX.o" "Character1_Head.tx";
connectAttr "Character1_Head_translateY.o" "Character1_Head.ty";
connectAttr "Character1_Head_translateZ.o" "Character1_Head.tz";
connectAttr "Character1_Head_rotateX.o" "Character1_Head.rx";
connectAttr "Character1_Head_rotateY.o" "Character1_Head.ry";
connectAttr "Character1_Head_rotateZ.o" "Character1_Head.rz";
connectAttr "Character1_Head_visibility.o" "Character1_Head.v";
connectAttr "Character1_Head.s" "jaw.is";
connectAttr "jaw_lockInfluenceWeights.o" "jaw.liw";
connectAttr "jaw_translateX.o" "jaw.tx";
connectAttr "jaw_translateY.o" "jaw.ty";
connectAttr "jaw_translateZ.o" "jaw.tz";
connectAttr "jaw_scaleX.o" "jaw.sx";
connectAttr "jaw_scaleY.o" "jaw.sy";
connectAttr "jaw_scaleZ.o" "jaw.sz";
connectAttr "jaw_rotateX.o" "jaw.rx";
connectAttr "jaw_rotateY.o" "jaw.ry";
connectAttr "jaw_rotateZ.o" "jaw.rz";
connectAttr "jaw_visibility.o" "jaw.v";
connectAttr "Character1_Head.s" "eyes.is";
connectAttr "eyes_translateX.o" "eyes.tx";
connectAttr "eyes_translateY.o" "eyes.ty";
connectAttr "eyes_translateZ.o" "eyes.tz";
connectAttr "eyes_scaleX.o" "eyes.sx";
connectAttr "eyes_scaleY.o" "eyes.sy";
connectAttr "eyes_scaleZ.o" "eyes.sz";
connectAttr "eyes_rotateX.o" "eyes.rx";
connectAttr "eyes_rotateY.o" "eyes.ry";
connectAttr "eyes_rotateZ.o" "eyes.rz";
connectAttr "eyes_visibility.o" "eyes.v";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Enemy@Grunt_Idle_Shakes.ma
