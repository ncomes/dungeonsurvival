//Maya ASCII 2016 scene
//Name: skeleton_base@jump_into_pit_03.ma
//Last modified: Tue, Nov 24, 2015 12:45:00 PM
//Codeset: 1252
file -rdi 1 -dns -rpr "Grunt_Skeleton_Rig" -rfn "Grunt_Skeleton_RigRN" -typ "mayaAscii"
		 "C:/Users/Nathan/Documents/dungeonsurvival/SourceContent/Enemies/Grunt/Rig/Grunt_Skeleton_Rig.ma";
file -rdi 2 -dns -rpr "Grunt_Skeleton_skin" -rfn "Grunt_Skeleton_skinRN" -typ
		 "mayaAscii" "C:/Users/Nathan/Documents/dungeonsurvival/SourceContent/Enemies/Grunt/Rig/Grunt_Skeleton_skin.ma";
file -r -dns -rpr "Grunt_Skeleton_Rig" -dr 1 -rfn "Grunt_Skeleton_RigRN" -typ "mayaAscii"
		 "C:/Users/Nathan/Documents/dungeonsurvival/SourceContent/Enemies/Grunt/Rig/Grunt_Skeleton_Rig.ma";
requires maya "2016";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "0680981C-4AA9-B6A9-C9EE-668BE2CE4F56";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 552.67581599958737 91.50432542194433 488.32194442868854 ;
	setAttr ".r" -type "double3" -0.93835272966528838 46.999999999993044 -7.2868455128406485e-017 ;
	setAttr ".rp" -type "double3" -2.1316282072803006e-014 0 1.1368683772161603e-013 ;
	setAttr ".rpt" -type "double3" 1.5600113698891511e-014 6.6202443878007288e-015 1.6499679213149911e-015 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "6E9970E5-4A02-926D-91C6-07961410DDF4";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".ncp" 1;
	setAttr ".fcp" 1000000;
	setAttr ".coi" 740.52892523985906;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "F7C760DD-45F2-D7A2-3B23-4EB3D4D791D1";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "9BBCADAE-4BC7-36BF-515F-8F8A04D28E5D";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "F343F310-4714-0654-9DBD-D5AFD8E1D134";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "4D47A342-41FC-D473-B48E-809E8EC1C5B7";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "5221BE81-458A-7576-DC84-A8A9F3934666";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "F7B0777A-432A-989C-7B4D-06B7518E61BC";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "grunt_rig";
	rename -uid "362DC587-4356-1433-E411-70891819FC45";
	setAttr ".rp" -type "double3" -0.056989816302802865 94.467932384644911 -1.0564472310214796 ;
	setAttr ".sp" -type "double3" -0.056989816302802865 94.467932384644911 -1.0564472310214796 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "A2CDA282-46E5-38F4-D5A7-A3B897480D2D";
	setAttr -s 4 ".lnk";
	setAttr -s 4 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "9B7A4FE4-4BF8-AE70-7247-1681EFF8B0F2";
createNode displayLayer -n "defaultLayer";
	rename -uid "6487A5CF-4E3B-899B-F6D7-E5B7E7799413";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "DA2EA192-4093-BCA0-E373-C5B16646FBB1";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "951F278A-42EF-514A-4D93-14A8B5E6DA7D";
	setAttr ".g" yes;
createNode reference -n "Grunt_Skeleton_RigRN";
	rename -uid "6F0CDD0B-4A07-579C-9D45-CEA6B9FFF823";
	setAttr -s 258 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".phl[89]" 0;
	setAttr ".phl[90]" 0;
	setAttr ".phl[91]" 0;
	setAttr ".phl[92]" 0;
	setAttr ".phl[93]" 0;
	setAttr ".phl[94]" 0;
	setAttr ".phl[95]" 0;
	setAttr ".phl[96]" 0;
	setAttr ".phl[97]" 0;
	setAttr ".phl[98]" 0;
	setAttr ".phl[99]" 0;
	setAttr ".phl[100]" 0;
	setAttr ".phl[101]" 0;
	setAttr ".phl[102]" 0;
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".phl[111]" 0;
	setAttr ".phl[112]" 0;
	setAttr ".phl[113]" 0;
	setAttr ".phl[114]" 0;
	setAttr ".phl[115]" 0;
	setAttr ".phl[116]" 0;
	setAttr ".phl[117]" 0;
	setAttr ".phl[118]" 0;
	setAttr ".phl[119]" 0;
	setAttr ".phl[120]" 0;
	setAttr ".phl[121]" 0;
	setAttr ".phl[122]" 0;
	setAttr ".phl[123]" 0;
	setAttr ".phl[124]" 0;
	setAttr ".phl[125]" 0;
	setAttr ".phl[126]" 0;
	setAttr ".phl[127]" 0;
	setAttr ".phl[128]" 0;
	setAttr ".phl[129]" 0;
	setAttr ".phl[130]" 0;
	setAttr ".phl[131]" 0;
	setAttr ".phl[132]" 0;
	setAttr ".phl[133]" 0;
	setAttr ".phl[134]" 0;
	setAttr ".phl[135]" 0;
	setAttr ".phl[136]" 0;
	setAttr ".phl[137]" 0;
	setAttr ".phl[138]" 0;
	setAttr ".phl[139]" 0;
	setAttr ".phl[140]" 0;
	setAttr ".phl[141]" 0;
	setAttr ".phl[142]" 0;
	setAttr ".phl[143]" 0;
	setAttr ".phl[144]" 0;
	setAttr ".phl[145]" 0;
	setAttr ".phl[146]" 0;
	setAttr ".phl[147]" 0;
	setAttr ".phl[148]" 0;
	setAttr ".phl[149]" 0;
	setAttr ".phl[150]" 0;
	setAttr ".phl[151]" 0;
	setAttr ".phl[152]" 0;
	setAttr ".phl[153]" 0;
	setAttr ".phl[154]" 0;
	setAttr ".phl[155]" 0;
	setAttr ".phl[156]" 0;
	setAttr ".phl[157]" 0;
	setAttr ".phl[158]" 0;
	setAttr ".phl[159]" 0;
	setAttr ".phl[160]" 0;
	setAttr ".phl[161]" 0;
	setAttr ".phl[162]" 0;
	setAttr ".phl[163]" 0;
	setAttr ".phl[164]" 0;
	setAttr ".phl[165]" 0;
	setAttr ".phl[166]" 0;
	setAttr ".phl[167]" 0;
	setAttr ".phl[168]" 0;
	setAttr ".phl[169]" 0;
	setAttr ".phl[170]" 0;
	setAttr ".phl[171]" 0;
	setAttr ".phl[172]" 0;
	setAttr ".phl[173]" 0;
	setAttr ".phl[174]" 0;
	setAttr ".phl[175]" 0;
	setAttr ".phl[176]" 0;
	setAttr ".phl[177]" 0;
	setAttr ".phl[178]" 0;
	setAttr ".phl[179]" 0;
	setAttr ".phl[180]" 0;
	setAttr ".phl[181]" 0;
	setAttr ".phl[182]" 0;
	setAttr ".phl[183]" 0;
	setAttr ".phl[184]" 0;
	setAttr ".phl[185]" 0;
	setAttr ".phl[186]" 0;
	setAttr ".phl[187]" 0;
	setAttr ".phl[188]" 0;
	setAttr ".phl[189]" 0;
	setAttr ".phl[190]" 0;
	setAttr ".phl[191]" 0;
	setAttr ".phl[192]" 0;
	setAttr ".phl[193]" 0;
	setAttr ".phl[194]" 0;
	setAttr ".phl[195]" 0;
	setAttr ".phl[196]" 0;
	setAttr ".phl[197]" 0;
	setAttr ".phl[198]" 0;
	setAttr ".phl[199]" 0;
	setAttr ".phl[200]" 0;
	setAttr ".phl[201]" 0;
	setAttr ".phl[202]" 0;
	setAttr ".phl[203]" 0;
	setAttr ".phl[204]" 0;
	setAttr ".phl[205]" 0;
	setAttr ".phl[206]" 0;
	setAttr ".phl[207]" 0;
	setAttr ".phl[208]" 0;
	setAttr ".phl[209]" 0;
	setAttr ".phl[210]" 0;
	setAttr ".phl[211]" 0;
	setAttr ".phl[212]" 0;
	setAttr ".phl[213]" 0;
	setAttr ".phl[214]" 0;
	setAttr ".phl[215]" 0;
	setAttr ".phl[216]" 0;
	setAttr ".phl[217]" 0;
	setAttr ".phl[218]" 0;
	setAttr ".phl[219]" 0;
	setAttr ".phl[220]" 0;
	setAttr ".phl[221]" 0;
	setAttr ".phl[222]" 0;
	setAttr ".phl[223]" 0;
	setAttr ".phl[224]" 0;
	setAttr ".phl[225]" 0;
	setAttr ".phl[226]" 0;
	setAttr ".phl[227]" 0;
	setAttr ".phl[228]" 0;
	setAttr ".phl[229]" 0;
	setAttr ".phl[230]" 0;
	setAttr ".phl[231]" 0;
	setAttr ".phl[232]" 0;
	setAttr ".phl[233]" 0;
	setAttr ".phl[234]" 0;
	setAttr ".phl[235]" 0;
	setAttr ".phl[236]" 0;
	setAttr ".phl[237]" 0;
	setAttr ".phl[238]" 0;
	setAttr ".phl[239]" 0;
	setAttr ".phl[240]" 0;
	setAttr ".phl[241]" 0;
	setAttr ".phl[242]" 0;
	setAttr ".phl[243]" 0;
	setAttr ".phl[244]" 0;
	setAttr ".phl[245]" 0;
	setAttr ".phl[246]" 0;
	setAttr ".phl[247]" 0;
	setAttr ".phl[248]" 0;
	setAttr ".phl[249]" 0;
	setAttr ".phl[250]" 0;
	setAttr ".phl[251]" 0;
	setAttr ".phl[252]" 0;
	setAttr ".phl[253]" 0;
	setAttr ".phl[254]" 0;
	setAttr ".phl[255]" 0;
	setAttr ".phl[256]" 0;
	setAttr ".phl[257]" 0;
	setAttr ".phl[258]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"Grunt_Skeleton_RigRN"
		"Grunt_Skeleton_RigRN" 0
		"Grunt_Skeleton_skinRN" 0
		"Grunt_Skeleton_RigRN" 496
		2 "|grunt_rig|DoNotDeleteRigNodes|spineExtra|ikSplineSpine" "translate" " -type \"double3\" -9.4895678087103662e-009 75.824756678231836 -9.4321852715667376"
		
		2 "|grunt_rig|DoNotDeleteRigNodes|spineExtra|ikSplineSpine" "rotate" " -type \"double3\" 89.999999999999972 -9.6543514239209909 89.999999999999986"
		
		2 "|grunt_rig|worldPlacement" "translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement" "translateX" " -av"
		2 "|grunt_rig|worldPlacement" "translateY" " -av"
		2 "|grunt_rig|worldPlacement" "translateZ" " -av"
		2 "|grunt_rig|worldPlacement" "rotate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement" "rotateX" " -av"
		2 "|grunt_rig|worldPlacement" "rotateY" " -av"
		2 "|grunt_rig|worldPlacement" "rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig" "visibility" " -av 1"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig" "translate" " -type \"double3\" 0 44.244975198244305 -9.3133430480957031"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig" "translateX" " -av"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig" "translateY" " -av"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig" "translateZ" " -av"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig" "rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig" "rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig" "rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig" "scale" " -type \"double3\" 1 1 1"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig" "scaleX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig" "scaleY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig" "scaleZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig|hips_rig|l_hip_rig|l_knee_rig|l_ankle_rig|l_leg_ik_switch" 
		"IkFkSwitch" " -av -k 1 1"
		2 "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig|hips_rig|r_hip_rig|r_knee_rig|r_ankle_rig|r_leg_ik_switch" 
		"IkFkSwitch" " -av -k 1 1"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig" "visibility" " -av 1"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig" "translate" " -type \"double3\" -9.4895616142039899e-009 75.824488615540474 -9.4322473036750516"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig" "translateX" " -av"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig" "translateY" " -av"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig" "translateZ" " -av"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig" "rotateX" " -av"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig" "rotateY" " -av"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig" "rotateZ" " -av"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig" "scale" " -type \"double3\" 1 1 1"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig" "scaleX" " -av"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig" "scaleY" " -av"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig" "scaleZ" " -av"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl" 
		"translate" " -type \"double3\" 1.1095978640233767e-007 -114.76858529551514 0.68072052735330368"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl" 
		"index" " -av -k 1 -73.7"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl" 
		"index1" " -av -k 1 -73.7"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl" 
		"index2" " -av -k 1 -73.7"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl" 
		"_____" " -cb 1 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl" 
		"pinky" " -av -k 1 -73.7"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl" 
		"pinky1" " -av -k 1 -73.7"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl" 
		"pinky2" " -av -k 1 -73.7"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl" 
		"______" " -cb 1 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl" 
		"thumb" " -av -k 1 -17"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl" 
		"thumb1" " -av -k 1 -17"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl" 
		"thumb2" " -av -k 1 -17"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl" 
		"thumbReach" " -av -k 1 -32.3"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"index" " -av -k 1 -71.5"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"index1" " -av -k 1 -71.5"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"index2" " -av -k 1 -71.5"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"_____" " -cb 1 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"pinky" " -av -k 1 -71.5"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"pinky1" " -av -k 1 -71.5"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"pinky2" " -av -k 1 -71.5"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"______" " -cb 1 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"thumb" " -av -k 1 -20.3"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"thumb1" " -av -k 1 -20.3"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"thumb2" " -av -k 1 -20.3"
		2 "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl" 
		"thumbReach" " -av -k 1 -35"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl" "translate" 
		" -type \"double3\" 0 -4.2578705659158516 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl" "translateX" 
		" -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl" "translateY" 
		" -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl" "translateZ" 
		" -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl" "rotate" 
		" -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl" "rotateX" 
		" -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl" "rotateY" 
		" -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl" "rotateZ" 
		" -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl" 
		"rotate" " -type \"double3\" 28.398711562433068 123.7290704964697 -9.7293426155055176"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl" 
		"rotate" " -type \"double3\" 64.394657779900456 -1.554273623493839 1.0042729284252756"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl|l_ball_grp|l_ball_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl|l_ball_grp|l_ball_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl" 
		"rotate" " -type \"double3\" 26.561925601000986 117.58638970831244 -21.378234794508444"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl" 
		"rotate" " -type \"double3\" 0 18.093992333414722 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl" 
		"rotate" " -type \"double3\" 61.650310502745249 -6.4859785732104474 5.6284054310141434"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl|r_ball_grp|r_ball_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl|r_ball_grp|r_ball_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01" 
		"rotate" " -type \"double3\" 0 0 6.3161851613978088"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01" 
		"rotate" " -type \"double3\" 23.622811169644283 11.833807174056558 -48.544825630570429"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01" 
		"rotate" " -type \"double3\" 0 -60.773635412093434 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01" 
		"rotate" " -type \"double3\" 0 0 6.3161851613978088"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01" 
		"rotate" " -type \"double3\" 32.183823749716375 20.474867211594614 -46.910748509822803"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01" 
		"rotate" " -type \"double3\" 0 -66.930794899470769 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01" 
		"rotate" " -type \"double3\" -12.051431388619157 -23.597914958351705 -10.394458944167162"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl" 
		"rotate" " -type \"double3\" 20.400609952411415 -8.2799782120664389 -3.5361276145662019"
		
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"translate" " -type \"double3\" 4.9302857975058743 0 0"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"translate" " -type \"double3\" -6.6011894368414374 0 0"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01" 
		"rotateZ" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01" 
		"translateX" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01" 
		"translateY" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01" 
		"translateZ" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01" 
		"rotateX" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01" 
		"rotateY" " -av"
		2 "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01" 
		"rotateZ" " -av"
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.midIkCtrl" "Grunt_Skeleton_RigRN.placeHolderList[1]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.lookAt" "Grunt_Skeleton_RigRN.placeHolderList[2]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.rotateX" "Grunt_Skeleton_RigRN.placeHolderList[3]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.rotateY" "Grunt_Skeleton_RigRN.placeHolderList[4]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.rotateZ" "Grunt_Skeleton_RigRN.placeHolderList[5]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.translateX" "Grunt_Skeleton_RigRN.placeHolderList[6]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.translateY" "Grunt_Skeleton_RigRN.placeHolderList[7]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.translateZ" "Grunt_Skeleton_RigRN.placeHolderList[8]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.scaleY" "Grunt_Skeleton_RigRN.placeHolderList[9]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.scaleX" "Grunt_Skeleton_RigRN.placeHolderList[10]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.scaleZ" "Grunt_Skeleton_RigRN.placeHolderList[11]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement.visibility" "Grunt_Skeleton_RigRN.placeHolderList[12]" 
		""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig|hips_rig|l_hip_rig|l_knee_rig|l_ankle_rig|l_leg_ik_switch.IkFkSwitch" 
		"Grunt_Skeleton_RigRN.placeHolderList[13]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig|hips_rig|l_hip_rig|l_knee_rig|l_ankle_rig|l_leg_ik_switch.autoStretch" 
		"Grunt_Skeleton_RigRN.placeHolderList[14]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig|hips_rig|r_hip_rig|r_knee_rig|r_ankle_rig|r_leg_ik_switch.IkFkSwitch" 
		"Grunt_Skeleton_RigRN.placeHolderList[15]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|pelvis_rig|hips_rig|r_hip_rig|r_knee_rig|r_ankle_rig|r_leg_ik_switch.autoStretch" 
		"Grunt_Skeleton_RigRN.placeHolderList[16]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[17]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[18]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[19]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.visibility" 
		"Grunt_Skeleton_RigRN.placeHolderList[20]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[21]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[22]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[23]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.scaleX" 
		"Grunt_Skeleton_RigRN.placeHolderList[24]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.scaleY" 
		"Grunt_Skeleton_RigRN.placeHolderList[25]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|neckBase_rig|neck_mid_rig|neck_rig|JawCtrl.scaleZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[26]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_arm_ik_switch.IkFkSwitch" 
		"Grunt_Skeleton_RigRN.placeHolderList[27]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_arm_ik_switch.elbowLock" 
		"Grunt_Skeleton_RigRN.placeHolderList[28]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_arm_ik_switch.autoStretch" 
		"Grunt_Skeleton_RigRN.placeHolderList[29]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.index" 
		"Grunt_Skeleton_RigRN.placeHolderList[30]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.index1" 
		"Grunt_Skeleton_RigRN.placeHolderList[31]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.index2" 
		"Grunt_Skeleton_RigRN.placeHolderList[32]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.pinky" 
		"Grunt_Skeleton_RigRN.placeHolderList[33]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.pinky1" 
		"Grunt_Skeleton_RigRN.placeHolderList[34]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.pinky2" 
		"Grunt_Skeleton_RigRN.placeHolderList[35]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.thumb" 
		"Grunt_Skeleton_RigRN.placeHolderList[36]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.thumb1" 
		"Grunt_Skeleton_RigRN.placeHolderList[37]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.thumb2" 
		"Grunt_Skeleton_RigRN.placeHolderList[38]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.thumbReach" 
		"Grunt_Skeleton_RigRN.placeHolderList[39]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.thumbTwist" 
		"Grunt_Skeleton_RigRN.placeHolderList[40]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.indexSpread" 
		"Grunt_Skeleton_RigRN.placeHolderList[41]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.pinkySpread" 
		"Grunt_Skeleton_RigRN.placeHolderList[42]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.indexTwist" 
		"Grunt_Skeleton_RigRN.placeHolderList[43]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.pinkyTwist" 
		"Grunt_Skeleton_RigRN.placeHolderList[44]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|l_clav_rig|l_shoulder_rig|l_elbow_rig|l_wrist_rig|l_hand_grp|l_hand_ctrl.visibility" 
		"Grunt_Skeleton_RigRN.placeHolderList[45]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_arm_ik_switch.IkFkSwitch" 
		"Grunt_Skeleton_RigRN.placeHolderList[46]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_arm_ik_switch.elbowLock" 
		"Grunt_Skeleton_RigRN.placeHolderList[47]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_arm_ik_switch.autoStretch" 
		"Grunt_Skeleton_RigRN.placeHolderList[48]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl.index" 
		"Grunt_Skeleton_RigRN.placeHolderList[49]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl.index1" 
		"Grunt_Skeleton_RigRN.placeHolderList[50]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl.index2" 
		"Grunt_Skeleton_RigRN.placeHolderList[51]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl.pinky" 
		"Grunt_Skeleton_RigRN.placeHolderList[52]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl.pinky1" 
		"Grunt_Skeleton_RigRN.placeHolderList[53]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl.pinky2" 
		"Grunt_Skeleton_RigRN.placeHolderList[54]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl.thumb" 
		"Grunt_Skeleton_RigRN.placeHolderList[55]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl.thumb1" 
		"Grunt_Skeleton_RigRN.placeHolderList[56]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl.thumb2" 
		"Grunt_Skeleton_RigRN.placeHolderList[57]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl.thumbReach" 
		"Grunt_Skeleton_RigRN.placeHolderList[58]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl.thumbTwist" 
		"Grunt_Skeleton_RigRN.placeHolderList[59]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl.indexSpread" 
		"Grunt_Skeleton_RigRN.placeHolderList[60]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl.pinkySpread" 
		"Grunt_Skeleton_RigRN.placeHolderList[61]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl.indexTwist" 
		"Grunt_Skeleton_RigRN.placeHolderList[62]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl.pinkyTwist" 
		"Grunt_Skeleton_RigRN.placeHolderList[63]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|spineParent_rig|r_clav_rig|r_shoulder_rig|r_elbow_rig|r_wrist_rig|r_hand_grp|r_hand_ctrl.visibility" 
		"Grunt_Skeleton_RigRN.placeHolderList[64]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[65]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[66]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[67]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[68]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[69]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[70]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[71]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[72]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[73]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[74]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[75]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[76]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl.rotateWith" 
		"Grunt_Skeleton_RigRN.placeHolderList[77]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[78]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[79]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[80]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[81]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[82]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[83]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[84]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[85]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[86]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[87]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[88]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[89]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[90]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[91]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[92]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[93]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[94]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[95]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl|l_ball_grp|l_ball_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[96]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl|l_ball_grp|l_ball_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[97]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl|l_ball_grp|l_ball_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[98]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl|l_ball_grp|l_ball_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[99]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl|l_ball_grp|l_ball_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[100]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|l_hip_grp|l_hip_ctrl|l_knee_grp|l_knee_ctrl|l_ankle_grp|l_ankle_ctrl|l_ball_grp|l_ball_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[101]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl.rotateWith" 
		"Grunt_Skeleton_RigRN.placeHolderList[102]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[103]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[104]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[105]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[106]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[107]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[108]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[109]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[110]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[111]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[112]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[113]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[114]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[115]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[116]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[117]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[118]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[119]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[120]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl|r_ball_grp|r_ball_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[121]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl|r_ball_grp|r_ball_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[122]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl|r_ball_grp|r_ball_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[123]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl|r_ball_grp|r_ball_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[124]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl|r_ball_grp|r_ball_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[125]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|hips_ctrl|r_hip_grp|r_hip_ctrl|r_knee_grp|r_knee_ctrl|r_ankle_grp|r_ankle_ctrl|r_ball_grp|r_ball_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[126]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[127]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[128]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[129]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[130]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[131]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[132]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[133]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[134]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[135]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl.spineScale" 
		"Grunt_Skeleton_RigRN.placeHolderList[136]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[137]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[138]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[139]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[140]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[141]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_clav_grp|l_clav_rig_ctrl_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[142]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01.rotateWith" 
		"Grunt_Skeleton_RigRN.placeHolderList[143]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[144]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[145]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[146]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[147]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[148]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[149]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[150]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[151]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[152]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[153]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[154]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[155]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[156]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[157]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[158]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[159]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[160]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|l_shoulder_grp|l_shoulder_fk_ctrl_01|l_elbow_grp|l_elbow_fk_ctrl_01|l_wrist_grp|l_wrist_fk_ctrl_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[161]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[162]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[163]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[164]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[165]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[166]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_clav_grp|r_clav_rig_ctrl_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[167]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01.rotateWith" 
		"Grunt_Skeleton_RigRN.placeHolderList[168]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[169]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[170]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[171]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[172]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[173]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[174]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[175]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[176]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[177]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[178]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[179]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[180]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[181]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[182]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[183]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[184]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[185]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|r_shoulder_grp|r_shoulder_fk_ctrl_01|r_elbow_grp|r_elbow_fk_ctrl_01|r_wrist_grp|r_wrist_fk_ctrl_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[186]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl.rotateWith" 
		"Grunt_Skeleton_RigRN.placeHolderList[187]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[188]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[189]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[190]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[191]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[192]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[193]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.scaleX" 
		"Grunt_Skeleton_RigRN.placeHolderList[194]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.scaleY" 
		"Grunt_Skeleton_RigRN.placeHolderList[195]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.rotateWith" 
		"Grunt_Skeleton_RigRN.placeHolderList[196]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[197]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[198]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[199]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[200]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[201]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|rig_skeleton|rig_controls|pelvis_ctrl|spine_01_ctrl|spine_02_ctrl|spine_03_ctrl|topSpine_ctrl|neckBase_grp|neckBase_ctrl|neck_grp|neck_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[202]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.xOffset" 
		"Grunt_Skeleton_RigRN.placeHolderList[203]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.yOffset" 
		"Grunt_Skeleton_RigRN.placeHolderList[204]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.zOffset" 
		"Grunt_Skeleton_RigRN.placeHolderList[205]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.stretch" 
		"Grunt_Skeleton_RigRN.placeHolderList[206]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.heelRoll" 
		"Grunt_Skeleton_RigRN.placeHolderList[207]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.ballRoll" 
		"Grunt_Skeleton_RigRN.placeHolderList[208]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.toeRoll" 
		"Grunt_Skeleton_RigRN.placeHolderList[209]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.toeTap" 
		"Grunt_Skeleton_RigRN.placeHolderList[210]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.heelPivot" 
		"Grunt_Skeleton_RigRN.placeHolderList[211]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.ballPivot" 
		"Grunt_Skeleton_RigRN.placeHolderList[212]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.toePivot" 
		"Grunt_Skeleton_RigRN.placeHolderList[213]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.heelSide" 
		"Grunt_Skeleton_RigRN.placeHolderList[214]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.ballSide" 
		"Grunt_Skeleton_RigRN.placeHolderList[215]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.toeSide" 
		"Grunt_Skeleton_RigRN.placeHolderList[216]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.space" 
		"Grunt_Skeleton_RigRN.placeHolderList[217]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[218]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[219]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[220]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[221]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[222]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_foot_ik_ctrl_switch|l_foot_ik_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[223]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.xOffset" 
		"Grunt_Skeleton_RigRN.placeHolderList[224]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.yOffset" 
		"Grunt_Skeleton_RigRN.placeHolderList[225]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.zOffset" 
		"Grunt_Skeleton_RigRN.placeHolderList[226]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.stretch" 
		"Grunt_Skeleton_RigRN.placeHolderList[227]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.heelRoll" 
		"Grunt_Skeleton_RigRN.placeHolderList[228]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.ballRoll" 
		"Grunt_Skeleton_RigRN.placeHolderList[229]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.toeRoll" 
		"Grunt_Skeleton_RigRN.placeHolderList[230]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.toeTap" 
		"Grunt_Skeleton_RigRN.placeHolderList[231]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.heelPivot" 
		"Grunt_Skeleton_RigRN.placeHolderList[232]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.ballPivot" 
		"Grunt_Skeleton_RigRN.placeHolderList[233]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.toePivot" 
		"Grunt_Skeleton_RigRN.placeHolderList[234]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.heelSide" 
		"Grunt_Skeleton_RigRN.placeHolderList[235]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.ballSide" 
		"Grunt_Skeleton_RigRN.placeHolderList[236]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.toeSide" 
		"Grunt_Skeleton_RigRN.placeHolderList[237]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.space" 
		"Grunt_Skeleton_RigRN.placeHolderList[238]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[239]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[240]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[241]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[242]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[243]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_foot_ik_ctrl_switch|r_foot_ik_ctrl.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[244]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01.space" 
		"Grunt_Skeleton_RigRN.placeHolderList[245]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[246]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[247]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[248]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[249]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[250]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_r_leg_pv_01_switch|r_leg_pv_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[251]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01.space" 
		"Grunt_Skeleton_RigRN.placeHolderList[252]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01.translateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[253]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01.translateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[254]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01.translateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[255]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01.rotateX" 
		"Grunt_Skeleton_RigRN.placeHolderList[256]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01.rotateY" 
		"Grunt_Skeleton_RigRN.placeHolderList[257]" ""
		5 4 "Grunt_Skeleton_RigRN" "|grunt_rig|worldPlacement|njc_space_switch_grp|njc_spaces_children|njc_l_leg_pv_01_switch|l_leg_pv_01.rotateZ" 
		"Grunt_Skeleton_RigRN.placeHolderList[258]" ""
		"Grunt_Skeleton_skinRN" 14
		2 "|grunt_rig|ref_frame|Character1_Hips" "visibility" " -av 1"
		2 "|grunt_rig|ref_frame|Character1_Hips" "translate" " -type \"double3\" 0 44.244975198244298 -9.3133430480957031"
		
		2 "|grunt_rig|ref_frame|Character1_Hips" "translateX" " -av"
		2 "|grunt_rig|ref_frame|Character1_Hips" "translateY" " -av"
		2 "|grunt_rig|ref_frame|Character1_Hips" "translateZ" " -av"
		2 "|grunt_rig|ref_frame|Character1_Hips" "rotate" " -type \"double3\" 180 -180.00000000000003 180.00000000000006"
		
		2 "|grunt_rig|ref_frame|Character1_Hips" "rotateX" " -av"
		2 "|grunt_rig|ref_frame|Character1_Hips" "rotateY" " -av"
		2 "|grunt_rig|ref_frame|Character1_Hips" "rotateZ" " -av"
		2 "|grunt_rig|ref_frame|Character1_Hips" "scale" " -type \"double3\" 0.9999997896161138 0.99999977606968649 0.99999981473457988"
		
		2 "|grunt_rig|ref_frame|Character1_Hips" "scaleX" " -av"
		2 "|grunt_rig|ref_frame|Character1_Hips" "scaleY" " -av"
		2 "|grunt_rig|ref_frame|Character1_Hips" "scaleZ" " -av"
		2 "|grunt_rig|ref_frame|Character1_Hips" "lockInfluenceWeights" " -av -k 1 0";
lockNode -l 1 ;
createNode script -n "uiConfigurationScriptNode2";
	rename -uid "D0B90BA9-4637-C2ED-17FC-1487C6D0612F";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1636\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1636\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 815\n                -height 345\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 815\n            -height 345\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 0\n                -polymeshes 1\n                -subdivSurfaces 0\n                -planes 0\n                -lights 0\n                -cameras 0\n                -controlVertices 0\n                -hulls 0\n                -grid 1\n                -imagePlane 0\n                -joints 0\n"
		+ "                -ikHandles 0\n                -deformers 0\n                -dynamics 0\n                -particleInstancers 0\n                -fluids 0\n                -hairSystems 0\n                -follicles 0\n                -nCloths 0\n                -nParticles 0\n                -nRigids 0\n                -dynamicConstraints 0\n                -locators 0\n                -manipulators 1\n                -pluginShapes 0\n                -dimensions 0\n                -handles 0\n                -pivots 0\n                -textures 0\n                -strokes 0\n                -motionTrails 0\n                -clipGhosts 0\n                -greasePencils 0\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1226\n                -height 639\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 0 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 0\n            -polymeshes 1\n            -subdivSurfaces 0\n"
		+ "            -planes 0\n            -lights 0\n            -cameras 0\n            -controlVertices 0\n            -hulls 0\n            -grid 1\n            -imagePlane 0\n            -joints 0\n            -ikHandles 0\n            -deformers 0\n            -dynamics 0\n            -particleInstancers 0\n            -fluids 0\n            -hairSystems 0\n            -follicles 0\n            -nCloths 0\n            -nParticles 0\n            -nRigids 0\n            -dynamicConstraints 0\n            -locators 0\n            -manipulators 1\n            -pluginShapes 0\n            -dimensions 0\n            -handles 0\n            -pivots 0\n            -textures 0\n            -strokes 0\n            -motionTrails 0\n            -clipGhosts 0\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1226\n            -height 639\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 0 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n"
		+ "                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n"
		+ "                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n"
		+ "                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n"
		+ "                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 19 100 -ps 2 81 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 0\\n    -polymeshes 1\\n    -subdivSurfaces 0\\n    -planes 0\\n    -lights 0\\n    -cameras 0\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 0\\n    -ikHandles 0\\n    -deformers 0\\n    -dynamics 0\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 0\\n    -handles 0\\n    -pivots 0\\n    -textures 0\\n    -strokes 0\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1226\\n    -height 639\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 0 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 0\\n    -polymeshes 1\\n    -subdivSurfaces 0\\n    -planes 0\\n    -lights 0\\n    -cameras 0\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 0\\n    -ikHandles 0\\n    -deformers 0\\n    -dynamics 0\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 0\\n    -handles 0\\n    -pivots 0\\n    -textures 0\\n    -strokes 0\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1226\\n    -height 639\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 0 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode2";
	rename -uid "B467B63E-4C2A-14B0-7680-8AA0B0108729";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 35 -ast 0 -aet 250 ";
	setAttr ".st" 6;
createNode animCurveTU -n "worldPlacement_visibility";
	rename -uid "31104F68-42FF-DC26-5544-62B3061F5BC8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 5 1 10 1 14 1 21 1 25 1;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTL -n "worldPlacement_translateX";
	rename -uid "7791C6F2-4526-423F-41B1-50BA4C9BBEA7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 10 0 14 0 21 0 25 0;
createNode animCurveTL -n "worldPlacement_translateY";
	rename -uid "0558FF2E-4FDF-F767-7E3A-E3BF1A33E0E1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 10 0 14 0 21 0 25 0;
createNode animCurveTL -n "worldPlacement_translateZ";
	rename -uid "14553E2D-498B-0733-5445-3FAD71397C73";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 10 0 14 0 21 0 25 0;
createNode animCurveTA -n "worldPlacement_rotateX";
	rename -uid "E141D382-49C3-6B6C-A8D3-FCB11EC39B64";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 10 0 14 0 21 0 25 0;
createNode animCurveTA -n "worldPlacement_rotateY";
	rename -uid "688CEB50-49F6-FA2E-DCBE-668F9CC515C8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 10 0 14 0 21 0 25 0;
createNode animCurveTA -n "worldPlacement_rotateZ";
	rename -uid "9F8860AB-4095-66BC-0997-A8AD9C08CB36";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 10 0 14 0 21 0 25 0;
createNode animCurveTU -n "worldPlacement_scaleX";
	rename -uid "218E0061-4B72-6AB6-E934-8385D4BF0576";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 5 1 10 1 14 1 21 1 25 1;
createNode animCurveTU -n "worldPlacement_scaleY";
	rename -uid "BFD3ED0C-47C8-7AD2-584F-18B2DBE8B0FC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 5 1 10 1 14 1 21 1 25 1;
createNode animCurveTU -n "worldPlacement_scaleZ";
	rename -uid "29B1F660-4286-4E93-5F08-1F9940500770";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 5 1 10 1 14 1 21 1 25 1;
createNode animCurveTU -n "worldPlacement_midIkCtrl";
	rename -uid "DF8C19B4-4E9B-444F-DB2B-39BD997E367F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 10 0 14 0 21 0 25 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTU -n "worldPlacement_lookAt";
	rename -uid "896A0EFB-4D82-0C89-86E7-D6B5BD29DB58";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 10 0 14 0 21 0 25 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTU -n "l_leg_ik_switch_IkFkSwitch";
	rename -uid "2134C675-4D55-CBAF-AE12-90B1EF699A07";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  0 1 5 1 10 1 14 1 15 0 17 0 21 0 25 0 35 0;
createNode animCurveTU -n "l_leg_ik_switch_autoStretch";
	rename -uid "3167FB0D-4A99-EDBF-68FF-33A9CA05FF8C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 1 5 1 10 1 14 1 17 1 21 1 25 1 35 1;
createNode animCurveTU -n "r_leg_ik_switch_IkFkSwitch";
	rename -uid "AE482E3C-4CAE-D892-AA72-A1958E1137CF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  0 1 5 1 10 1 14 1 15 0 17 0 21 0 25 0 35 0;
createNode animCurveTU -n "r_leg_ik_switch_autoStretch";
	rename -uid "C03CD9C7-4877-C71C-9499-58868D308118";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 1 5 1 10 1 14 1 17 1 21 1 25 1 35 1;
createNode animCurveTU -n "JawCtrl_visibility";
	rename -uid "FB696C06-428E-9444-B2E0-97884AA43DCF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 1 5 1 10 1 14 1 21 1 25 1 30 1 35 1;
	setAttr -s 8 ".kot[0:7]"  5 5 5 5 5 5 5 5;
createNode animCurveTL -n "JawCtrl_translateX";
	rename -uid "1E2F6135-4519-2C20-D235-C8A8238DE934";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 1.1095978640233767e-007 5 1.1095978640233767e-007
		 10 1.1095978640233767e-007 14 1.1095978640233767e-007 21 1.1095978640233767e-007
		 25 1.1095978640233767e-007 30 1.1095978640233767e-007 35 1.1095978640233767e-007;
createNode animCurveTL -n "JawCtrl_translateY";
	rename -uid "480E9DA2-44A1-51CC-3D79-E591D0D413EC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 -114.76858529551514 5 -114.76858529551514
		 10 -114.76858529551514 14 -114.76858529551514 21 -114.76858529551514 25 -114.76858529551514
		 30 -114.76858529551514 35 -114.76858529551514;
createNode animCurveTL -n "JawCtrl_translateZ";
	rename -uid "0FB5FC8C-41D0-7ACB-3BCF-CE8D06EDC906";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0.68072052735330368 5 0.68072052735330368
		 10 0.68072052735330368 14 0.68072052735330368 21 0.68072052735330368 25 0.68072052735330368
		 30 0.68072052735330368 35 0.68072052735330368;
createNode animCurveTA -n "JawCtrl_rotateX";
	rename -uid "6676F62E-4576-AC96-5C51-FEBFAFB5A9DF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 6.3611093629265792e-015 5 0 10 0 14 0
		 21 0 25 27.511503307522993 30 69.764203303527523 35 72.431861789232272;
createNode animCurveTA -n "JawCtrl_rotateY";
	rename -uid "830B99A4-42C2-5871-132D-43BE8CDB90CC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 2.2504626351166665e-013 5 0 10 0 14 0
		 21 0 25 0 30 0 35 0;
createNode animCurveTA -n "JawCtrl_rotateZ";
	rename -uid "46FB6386-4703-A087-D8C5-F4822A5EE9E6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 -2.2479385798591407e-013 5 0 10 0 14 0
		 21 0 25 0 30 0 35 0;
createNode animCurveTU -n "JawCtrl_scaleX";
	rename -uid "EF3FA15F-424D-C375-A204-4CA1D5CD7161";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 1.0000000000000002 5 1.0000000000000002
		 10 1.0000000000000002 14 1.0000000000000002 21 1.0000000000000002 25 1.0000000000000002
		 30 1.0000000000000002 35 1.0000000000000002;
createNode animCurveTU -n "JawCtrl_scaleY";
	rename -uid "979A126C-4D64-2A64-E7A2-4594D4312E9F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0.99999999999999978 5 0.99999999999999978
		 10 0.99999999999999978 14 0.99999999999999978 21 0.99999999999999978 25 0.99999999999999978
		 30 0.99999999999999978 35 0.99999999999999978;
createNode animCurveTU -n "JawCtrl_scaleZ";
	rename -uid "AAE22919-4169-14D0-C336-FCBDC9908BD5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0.99999999999999978 5 0.99999999999999978
		 10 0.99999999999999978 14 0.99999999999999978 21 0.99999999999999978 25 0.99999999999999978
		 30 0.99999999999999978 35 0.99999999999999978;
createNode animCurveTU -n "l_arm_ik_switch_IkFkSwitch";
	rename -uid "D2ABE0D8-498A-F7E1-1C0B-428229AA7BE6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTU -n "l_arm_ik_switch_elbowLock";
	rename -uid "9933D9F7-4DA9-4F19-5A7F-13B172830CE3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTU -n "l_arm_ik_switch_autoStretch";
	rename -uid "E957C1D3-427A-FAB8-3808-E7B858A3E128";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 5 1 10 1 14 1 21 1 25 1 35 1;
createNode animCurveTU -n "l_hand_ctrl_visibility";
	rename -uid "840CF54C-4CEA-1225-7408-7D9FC6BED1F5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 5 1 10 1 14 1 21 1 25 1 35 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "l_hand_ctrl_index";
	rename -uid "0FFF73DA-46B3-1490-0922-FAAB82164F98";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -73.7 5 -73.7 10 -73.7 14 -73.7 21 0 25 19.452283523933197
		 35 41.6;
createNode animCurveTU -n "l_hand_ctrl_index1";
	rename -uid "753027F1-4877-8F95-D3CE-2694A1ED2CF0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -73.7 5 -73.7 10 -73.7 14 -73.7 21 0 25 19.452283523933197
		 35 41.6;
createNode animCurveTU -n "l_hand_ctrl_index2";
	rename -uid "55721B1D-41F5-618C-C234-1ABFAF972F20";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -73.7 5 -73.7 10 -73.7 14 -73.7 21 0 25 19.452283523933197
		 35 41.6;
createNode animCurveTU -n "l_hand_ctrl_pinky";
	rename -uid "A623AA48-4D70-7723-EBAB-BBBD68C794E3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -73.7 5 -73.7 10 -73.7 14 -73.7 21 0 25 19.452283523933197
		 35 41.6;
createNode animCurveTU -n "l_hand_ctrl_pinky1";
	rename -uid "D7CB64B2-4596-ABEA-F4CA-B9AC270A4917";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -73.7 5 -73.7 10 -73.7 14 -73.7 21 0 25 19.452283523933197
		 35 41.6;
createNode animCurveTU -n "l_hand_ctrl_pinky2";
	rename -uid "93132E87-4D78-7E37-F680-BBBC440064EC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -73.7 5 -73.7 10 -73.7 14 -73.7 21 0 25 19.452283523933197
		 35 41.6;
createNode animCurveTU -n "l_hand_ctrl_thumb";
	rename -uid "D881DFFA-4DAD-0D4D-F338-43899C4B6156";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -17 5 -17 10 -17 14 -17 21 0 25 0 35 0;
createNode animCurveTU -n "l_hand_ctrl_thumb1";
	rename -uid "60BC7C79-438C-F30D-27E2-C4A5819D83E6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -17 5 -17 10 -17 14 -17 21 0 25 0 35 0;
createNode animCurveTU -n "l_hand_ctrl_thumb2";
	rename -uid "427340BD-45D6-37A1-E5F4-2AAB07E8F0A0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -17 5 -17 10 -17 14 -17 21 0 25 0 35 0;
createNode animCurveTU -n "l_hand_ctrl_thumbReach";
	rename -uid "C5F89AB3-468D-7A3B-C0C2-97925625772F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -32.3 5 -32.3 10 -32.3 14 -32.3 21 0 25 0
		 35 0;
createNode animCurveTU -n "l_hand_ctrl_thumbTwist";
	rename -uid "8C462223-4F67-962F-B9B4-DA9CB8342E3F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTU -n "l_hand_ctrl_indexSpread";
	rename -uid "03089F9D-44CA-20C2-94D8-C9B5FF8AC3AF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTU -n "l_hand_ctrl_pinkySpread";
	rename -uid "6F533384-4601-84E5-E2D9-EBADA3B48AA6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTU -n "l_hand_ctrl_indexTwist";
	rename -uid "8E9C249D-49BB-901B-B756-F38E274C25E3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTU -n "l_hand_ctrl_pinkyTwist";
	rename -uid "E9CE8437-4BF1-AEEE-294B-EC8FB0F62C06";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTU -n "r_arm_ik_switch_IkFkSwitch";
	rename -uid "5B5B75AF-4882-1B83-B19A-078C18BD0D22";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTU -n "r_arm_ik_switch_elbowLock";
	rename -uid "20F46578-4E26-71A5-0627-23BCD16D4627";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTU -n "r_arm_ik_switch_autoStretch";
	rename -uid "57EBE4D9-4575-DDB0-FD21-04AC2823AA8F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 5 1 10 1 14 1 21 1 25 1 35 1;
createNode animCurveTU -n "r_hand_ctrl_visibility";
	rename -uid "388046CE-4EF5-33F9-3603-F7A0C1CECC11";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 5 1 10 1 14 1 21 1 25 1 35 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "r_hand_ctrl_index";
	rename -uid "365B052F-404B-82CF-ACAF-A68471440325";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -71.5 5 -71.5 10 -71.5 14 -71.5 21 0 25 19.23848380051054
		 35 41.6;
createNode animCurveTU -n "r_hand_ctrl_index1";
	rename -uid "40D49C3B-4253-F255-DA11-919E90A56E57";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -71.5 5 -71.5 10 -71.5 14 -71.5 21 0 25 19.23848380051054
		 35 41.6;
createNode animCurveTU -n "r_hand_ctrl_index2";
	rename -uid "E2E1313A-4902-7567-1460-C38CCF592B28";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -71.5 5 -71.5 10 -71.5 14 -71.5 21 0 25 19.23848380051054
		 35 41.6;
createNode animCurveTU -n "r_hand_ctrl_pinky";
	rename -uid "2FABCA97-4660-DD69-EB21-188BB8CA0FE9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -71.5 5 -71.5 10 -71.5 14 -71.5 21 0 25 19.23848380051054
		 35 41.6;
createNode animCurveTU -n "r_hand_ctrl_pinky1";
	rename -uid "A8125EF8-45AF-7AF4-1C3A-97B227ABE1EE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -71.5 5 -71.5 10 -71.5 14 -71.5 21 0 25 19.23848380051054
		 35 41.6;
createNode animCurveTU -n "r_hand_ctrl_pinky2";
	rename -uid "9C6D495C-4CF9-D665-6DA1-75BFEAE237A3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -71.5 5 -71.5 10 -71.5 14 -71.5 21 0 25 19.23848380051054
		 35 41.6;
createNode animCurveTU -n "r_hand_ctrl_thumb";
	rename -uid "FFF11B42-4EE3-F950-70B9-4BBFBB5CDA12";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -20.3 5 -20.3 10 -20.3 14 -20.3 21 0 25 0
		 35 0;
createNode animCurveTU -n "r_hand_ctrl_thumb1";
	rename -uid "13BC1757-4882-30E6-4393-D390E13D2BDF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -20.3 5 -20.3 10 -20.3 14 -20.3 21 0 25 0
		 35 0;
createNode animCurveTU -n "r_hand_ctrl_thumb2";
	rename -uid "6452F79E-4621-9629-7874-8FB34F133662";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -20.3 5 -20.3 10 -20.3 14 -20.3 21 0 25 0
		 35 0;
createNode animCurveTU -n "r_hand_ctrl_thumbReach";
	rename -uid "33FF51E8-4F3F-A36D-431C-26B186AEC296";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -35 5 -35 10 -35 14 -35 21 0 25 0 35 0;
createNode animCurveTU -n "r_hand_ctrl_thumbTwist";
	rename -uid "F7E5B40F-496F-276E-7AB1-5EB8928EA56C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTU -n "r_hand_ctrl_indexSpread";
	rename -uid "8A1D05BB-4B4D-0537-E26A-C9AB2DF5E6BF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTU -n "r_hand_ctrl_pinkySpread";
	rename -uid "D89371C9-424F-AC2A-1457-5ABB9EF006E5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTU -n "r_hand_ctrl_indexTwist";
	rename -uid "93FC4D8A-4628-CEFD-6465-D28DC61930B0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTU -n "r_hand_ctrl_pinkyTwist";
	rename -uid "7F1DD53B-4077-38A2-3784-EC9B38A98A4D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "pelvis_ctrl_translateX";
	rename -uid "179F400F-4B71-D411-28CB-B89CED20F1B1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "pelvis_ctrl_translateY";
	rename -uid "7C3C4A88-42AA-1A44-FFBD-E9A7B7AA1806";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -4.2578705659158516 5 -17.613530801917001
		 10 -22.065417547250718 14 23.426015110551702 21 274.16258055677434 25 261.04080018404312
		 35 -445.495027590262;
	setAttr -s 7 ".kit[3:6]"  1 18 18 1;
	setAttr -s 7 ".kot[3:6]"  1 18 18 1;
	setAttr -s 7 ".kix[3:6]"  0.00059062178479507565 1 0.0033870546612888575 
		0.00021004014706704766;
	setAttr -s 7 ".kiy[3:6]"  0.99999988079071045 0 -0.99999427795410156 
		-1;
	setAttr -s 7 ".kox[3:6]"  0.00059062114451080561 1 0.0033870546612888575 
		0.00021003969595767558;
	setAttr -s 7 ".koy[3:6]"  0.99999988079071045 0 -0.99999427795410156 
		-1;
createNode animCurveTL -n "pelvis_ctrl_translateZ";
	rename -uid "6645B60C-44A3-A3C8-C1AA-CDBC93D82729";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 -5.5748150145116835 10 -7.433086686015578
		 14 35.086765666743048 25 233.76300052601022 35 450.62544157825744;
	setAttr -s 6 ".kit[5]"  1;
	setAttr -s 6 ".kot[5]"  1;
	setAttr -s 6 ".kix[5]"  0.0013135990593582392;
	setAttr -s 6 ".kiy[5]"  0.99999922513961792;
	setAttr -s 6 ".kox[5]"  0.0013135992921888828;
	setAttr -s 6 ".koy[5]"  0.99999922513961792;
createNode animCurveTA -n "pelvis_ctrl_rotateX";
	rename -uid "9065F57E-4B12-957A-2624-BC8B4DEA0CA2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 10.749868431849778 10 14.333157909133037
		 14 11.990739405097619 21 66.172366044268415 25 73.354524953346314 35 77.472735636579984;
createNode animCurveTA -n "pelvis_ctrl_rotateY";
	rename -uid "0B1DE9BF-42DC-E872-126D-B79A81F5354B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 2.0847433404795508 21 0
		 25 0 35 0;
createNode animCurveTA -n "pelvis_ctrl_rotateZ";
	rename -uid "A2C26619-44EC-3AE8-114D-9290A9CC7ED3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 -15.337963187713102 21 0
		 25 0 35 0;
createNode animCurveTL -n "hips_ctrl_translateX";
	rename -uid "99B07725-434A-C5C5-5345-83A28B6945B9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "hips_ctrl_translateY";
	rename -uid "D6DD42C6-4FAE-3C78-1BFB-0AA0E874CC0F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "hips_ctrl_translateZ";
	rename -uid "43D7165A-4C0B-3ABC-6FBF-8CB8AF5FDF61";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "hips_ctrl_rotateX";
	rename -uid "5686D12E-4EFC-49FF-31BA-5B89C0BA92F0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "hips_ctrl_rotateY";
	rename -uid "7AE3020B-486A-F590-F1B3-0FA888E456CC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "hips_ctrl_rotateZ";
	rename -uid "8CAA4454-44EF-3B14-51E2-CFBF351785D9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "spine_02_ctrl_rotateX";
	rename -uid "E4205EF0-420B-39ED-A371-28B0E3236FA3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 13.227704408726945 10 17.636939211635926
		 14 -2.0437798157909257 21 -7.172167155326199 25 -16.471648833121272 35 -36.962719098978326;
createNode animCurveTA -n "spine_02_ctrl_rotateY";
	rename -uid "56B093E5-4C46-D1E2-2087-6C8785978B3C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 -11.006074339004806 21 0
		 25 0 35 0;
createNode animCurveTA -n "spine_02_ctrl_rotateZ";
	rename -uid "2CC732D9-4014-478C-7659-17ADCBBD2512";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 9.8154057127829404 21 0
		 25 0 35 0;
createNode animCurveTL -n "topSpine_ctrl_translateX";
	rename -uid "62384AD5-4387-2813-240F-A6B72B4B50A9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "topSpine_ctrl_translateY";
	rename -uid "918D76F1-43C6-D88C-3FA0-63B8BC5CE225";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "topSpine_ctrl_translateZ";
	rename -uid "C3C2236B-46AC-A3D4-8B8C-458BD78AEE07";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "topSpine_ctrl_rotateX";
	rename -uid "7187E696-4E8D-8D16-F49A-98B120959A03";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "topSpine_ctrl_rotateY";
	rename -uid "4AD6E1D3-4415-013C-28EE-25BC6D3FD115";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "topSpine_ctrl_rotateZ";
	rename -uid "AE419E62-4A10-5754-E16C-AEB91AAE7266";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTU -n "topSpine_ctrl_spineScale";
	rename -uid "DD9E4024-4851-4BB1-F920-C2A811ACBA05";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "l_clav_rig_ctrl_01_translateX";
	rename -uid "E8BBC5CF-4739-91C1-CA09-4AA15EB4A96E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_clav_rig_ctrl_01_translateY";
	rename -uid "77F74BAB-40F8-09F5-2654-199664C1109A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_clav_rig_ctrl_01_translateZ";
	rename -uid "E0114D50-4C1C-FB17-5B70-36B8E7FA78E3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "l_clav_rig_ctrl_01_rotateX";
	rename -uid "D2D2F0B2-402E-2F13-9F82-76806448EB30";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "l_clav_rig_ctrl_01_rotateY";
	rename -uid "470DFE73-4237-5595-256B-3495F39754D3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 4.4395367113025319
		 35 22.3935454702466;
createNode animCurveTA -n "l_clav_rig_ctrl_01_rotateZ";
	rename -uid "E4317CD7-4B15-A361-FC3B-328FDCE41EA6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 6.3161851613978088 5 6.3161851613978088
		 10 6.3161851613978088 14 9.8351957255877327 21 19.889513356916183 25 19.889513356916183
		 35 19.889513356916183;
createNode animCurveTL -n "l_shoulder_fk_ctrl_01_translateX";
	rename -uid "7E889D04-4FBD-6961-1197-1697B010F7C2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_shoulder_fk_ctrl_01_translateY";
	rename -uid "FC19C0EB-4F33-3D1C-DFF2-69A94878C3E6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_shoulder_fk_ctrl_01_translateZ";
	rename -uid "06A44FDB-4818-09E7-13B4-9B9633112AB6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "l_shoulder_fk_ctrl_01_rotateX";
	rename -uid "B756F271-4C3A-3B41-9CDF-7F9641D66148";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 23.622811169644283 5 24.364818585126301
		 10 24.612154390286971 14 32.682899415394296 21 0 25 0 35 0;
createNode animCurveTA -n "l_shoulder_fk_ctrl_01_rotateY";
	rename -uid "33DF0AB1-44D1-FAF2-BD13-D7B2E223BA70";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 11.833807174056558 5 45.7172393724816
		 10 57.011716771956614 14 49.436446626473348 21 27.989466900825963 25 27.989466900825963
		 35 27.989466900825963;
createNode animCurveTA -n "l_shoulder_fk_ctrl_01_rotateZ";
	rename -uid "D3D56125-43BC-AD6F-10D1-349487834F8D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -48.544825630570429 5 -33.681065117219291
		 10 -28.726478279435575 14 20.63260339596372 21 0 25 0 35 0;
createNode animCurveTU -n "l_shoulder_fk_ctrl_01_rotateWith";
	rename -uid "94B8A32E-4605-521C-28C2-ECBB3038AB5A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "l_elbow_fk_ctrl_01_translateX";
	rename -uid "88667AFD-488F-EE86-BBF5-58B66940D1B7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_elbow_fk_ctrl_01_translateY";
	rename -uid "E56CC57D-4165-9F41-5701-56ABDFD3D1FF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_elbow_fk_ctrl_01_translateZ";
	rename -uid "FC81D810-462D-1D6E-4B60-F6A4CF84A765";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "l_elbow_fk_ctrl_01_rotateX";
	rename -uid "DEDA18A5-4D5F-4F6F-B592-37A7F2AA844A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0.25053381628736632
		 35 1.263722043920098;
createNode animCurveTA -n "l_elbow_fk_ctrl_01_rotateY";
	rename -uid "9836B2BD-4E0F-BA63-EA8C-239E74062FD7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -60.773635412093434 5 -60.773635412093434
		 10 -60.773635412093434 14 -20.447038025476122 21 0 25 6.8907215470105792 35 16.598185242254434;
createNode animCurveTA -n "l_elbow_fk_ctrl_01_rotateZ";
	rename -uid "D726FD5C-4FBE-54B8-7C63-22B9E012E2ED";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0.074736690838359376
		 35 0.37698066114054801;
createNode animCurveTL -n "l_wrist_fk_ctrl_01_translateX";
	rename -uid "FC38672E-497E-3C0B-296F-9AB0A2FB591D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_wrist_fk_ctrl_01_translateY";
	rename -uid "0F4C4F08-4C8B-0056-5557-9E8426EDB868";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_wrist_fk_ctrl_01_translateZ";
	rename -uid "8D18CF4E-422C-78F1-5566-A19F34C03932";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "l_wrist_fk_ctrl_01_rotateX";
	rename -uid "6660B6D7-4AFD-60E2-1DE5-8EA2247F88DC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 -31.28115610961969 21 -57.231243822248274
		 25 -57.231243822248274 35 -57.231243822248274;
createNode animCurveTA -n "l_wrist_fk_ctrl_01_rotateY";
	rename -uid "FBC5EE31-4388-EF15-3193-C9876153A54B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 14.287310337915406 21 0
		 25 0 35 0;
createNode animCurveTA -n "l_wrist_fk_ctrl_01_rotateZ";
	rename -uid "7DCDB681-4575-7971-592B-1E98E1B67FAB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 22.838185769239544 21 0
		 25 0 35 0;
createNode animCurveTL -n "r_clav_rig_ctrl_01_translateX";
	rename -uid "8457BC53-4800-1ACB-3609-7EAF5949C51A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_clav_rig_ctrl_01_translateY";
	rename -uid "19DCA5E2-4387-5465-FE8E-9E948773B52E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_clav_rig_ctrl_01_translateZ";
	rename -uid "ACC11D10-4184-2849-35A3-1F8341AF9D0E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "r_clav_rig_ctrl_01_rotateX";
	rename -uid "51C7719A-44F9-EB7F-D5FC-C9874C37C70A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "r_clav_rig_ctrl_01_rotateY";
	rename -uid "FF2787C1-413D-91AE-881F-48BB88C7FD32";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 4.4395367113025319
		 35 22.3935454702466;
createNode animCurveTA -n "r_clav_rig_ctrl_01_rotateZ";
	rename -uid "3E75E66C-4982-8B43-951E-64A2794DCB08";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 6.3161851613978088 5 6.3161851613978088
		 10 6.3161851613978088 14 9.8351957255877327 21 19.889513356916183 25 19.889513356916183
		 35 19.889513356916183;
createNode animCurveTL -n "r_shoulder_fk_ctrl_01_translateX";
	rename -uid "70470D95-4500-B5DC-AD9C-19BABE7899E6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_shoulder_fk_ctrl_01_translateY";
	rename -uid "23C141E6-4D5E-83D4-62C0-CCA126B91782";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_shoulder_fk_ctrl_01_translateZ";
	rename -uid "0FB1779A-48D2-9656-0E50-4BB758379FEF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "r_shoulder_fk_ctrl_01_rotateX";
	rename -uid "16FB2D6E-4126-28EE-47E3-7AAC501E8737";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 32.183823749716375 5 41.747467076173677
		 10 44.935348184992783 14 11.259576478771262 21 0 25 1.0890319682630687 35 5.4932053693269491;
createNode animCurveTA -n "r_shoulder_fk_ctrl_01_rotateY";
	rename -uid "4B763B28-49B7-AC2D-2E12-77BE31B66129";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 20.474867211594614 5 40.53429393545801
		 10 47.220769510079137 14 33.074470040492713 21 27.989466900825963 25 30.458487940496582
		 35 40.443499497988071;
createNode animCurveTA -n "r_shoulder_fk_ctrl_01_rotateZ";
	rename -uid "2C5C2615-44D4-1B8E-96CC-AB80D5BD9B86";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -46.910748509822803 5 -19.043488289796109
		 10 -9.754401549787211 14 39.51364426093906 21 0 25 0.90614590185077626 35 4.5707065343355335;
createNode animCurveTU -n "r_shoulder_fk_ctrl_01_rotateWith";
	rename -uid "C62BA008-44C2-177C-08B7-C9BB572414B2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "r_elbow_fk_ctrl_01_translateX";
	rename -uid "B3E2CBA7-4146-BB12-2594-34A8AD92CE3B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_elbow_fk_ctrl_01_translateY";
	rename -uid "8C34D017-4F32-4A47-8D49-64A0378F6127";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_elbow_fk_ctrl_01_translateZ";
	rename -uid "58B27735-4DD7-99CB-BA3A-1C8C09A81414";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "r_elbow_fk_ctrl_01_rotateX";
	rename -uid "DE2181E4-44EA-6DA4-2D7F-93B30EF01D31";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "r_elbow_fk_ctrl_01_rotateY";
	rename -uid "C2087E2A-41B4-9B57-BE39-F8B115DDA37A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -66.930794899470769 5 -66.930794899470769
		 10 -66.930794899470769 14 -36.659527525209235 21 0 25 0 35 0;
createNode animCurveTA -n "r_elbow_fk_ctrl_01_rotateZ";
	rename -uid "FCDD34D9-4AC5-2136-F02E-3F935DD2CFC8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_wrist_fk_ctrl_01_translateX";
	rename -uid "6785E1F0-4274-62D9-4908-4CA327AFA364";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_wrist_fk_ctrl_01_translateY";
	rename -uid "C03025A8-47D8-BCB6-B96F-7B965591A980";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_wrist_fk_ctrl_01_translateZ";
	rename -uid "1FAF2947-4402-2F1F-4CE2-15A265213F19";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "r_wrist_fk_ctrl_01_rotateX";
	rename -uid "BA0B4F8E-4966-C0E2-4EBB-35A44403500F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -12.051431388619157 5 -12.051431388619157
		 10 -12.051431388619157 14 -12.051431388619157 21 -57.231243822248274 25 -57.231243822248274
		 35 -57.231243822248274;
createNode animCurveTA -n "r_wrist_fk_ctrl_01_rotateY";
	rename -uid "0599036E-45A6-3234-291C-AF832EADC856";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -23.597914958351705 5 -23.597914958351705
		 10 -23.597914958351705 14 -23.597914958351705 21 0 25 0 35 0;
createNode animCurveTA -n "r_wrist_fk_ctrl_01_rotateZ";
	rename -uid "006A8E22-41B2-9282-0855-239608CB4C86";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -10.394458944167162 5 -10.394458944167162
		 10 -10.394458944167162 14 -10.394458944167162 21 0 25 0 35 0;
createNode animCurveTL -n "neckBase_ctrl_translateX";
	rename -uid "C7A0C150-48F9-A416-B393-62A4898FF84B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "neckBase_ctrl_translateY";
	rename -uid "2DE7F19A-4459-F3D8-3840-58BBFFFBE14F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "neckBase_ctrl_translateZ";
	rename -uid "665BCA5A-41A7-6496-F78F-4EB140AFC663";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "neckBase_ctrl_rotateX";
	rename -uid "4928F4A0-481F-CA0C-D828-5DBADF661EB2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "neckBase_ctrl_rotateY";
	rename -uid "80F2D9F8-473C-D2DA-2502-BCAF9A378BA3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "neckBase_ctrl_rotateZ";
	rename -uid "AF6D68F4-4D7A-0A33-7442-16B37CF1C06D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTU -n "neckBase_ctrl_rotateWith";
	rename -uid "85E12152-4AA1-0E90-B22F-A580BDE2ADAB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "neck_ctrl_translateX";
	rename -uid "809DD012-42C9-2A4D-9CC6-5AA7D91DB2A4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "neck_ctrl_translateY";
	rename -uid "65907B7D-47D3-A4E6-5031-B7A7987912E4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTL -n "neck_ctrl_translateZ";
	rename -uid "BC9F3A0E-4419-2011-F7AA-4AAE947CB615";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
createNode animCurveTA -n "neck_ctrl_rotateX";
	rename -uid "D3B13736-4614-99F0-23C8-55A7F96DB268";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 20.400609952411415 5 -17.438392338788688
		 10 -30.051393102522052 14 18.29390148149054 21 -55.574973880347819 25 -62.586239572303093
		 35 -66.60646064962603;
createNode animCurveTA -n "neck_ctrl_rotateY";
	rename -uid "FACFE5F8-406F-35A2-5C71-B4BD74F022F7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -8.2799782120664389 5 -0.80231898433687565
		 10 1.6902340915729783 14 -4.6241274128843397 21 1.6405981440144055 25 1.7147801029264209
		 35 1.7573156252617266;
createNode animCurveTA -n "neck_ctrl_rotateZ";
	rename -uid "4C289809-4B30-46F7-6B98-048A20478063";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -3.5361276145662019 5 -1.3730559333332382
		 10 -0.6520320395889172 14 12.413604070638764 21 -0.76844834239129023 25 -0.70338193460218801
		 35 -0.44024572663155387;
createNode animCurveTU -n "neck_ctrl_scaleX";
	rename -uid "876AFC48-48A8-F82D-22C9-C3AC482E847B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 5 1 10 1 14 1 21 1 25 1 35 1;
createNode animCurveTU -n "neck_ctrl_scaleY";
	rename -uid "E8EE42AD-4373-E552-31E1-748CE61B367D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 5 1 10 1 14 1 21 1 25 1 35 1;
createNode animCurveTU -n "neck_ctrl_rotateWith";
	rename -uid "31D733F8-4B6C-1956-3FFF-2E9783E0A478";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 10 0 14 0 21 0 25 0 35 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "l_foot_ik_ctrl_translateX";
	rename -uid "632C1733-4C03-FA3E-FDD4-67BE1DDBDB23";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 4.9302857975058743 5 4.9302857975058743
		 10 4.9302857975058743 13 4.9302857975058743 17 4.4520557429178673 23 -0.50418465820191294
		 27 -0.50418465820191294 35 -0.50418465820191294;
createNode animCurveTL -n "l_foot_ik_ctrl_translateY";
	rename -uid "16750F47-4F43-FA1A-7223-1EB0267ED5AA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  0 6.2172489379008766e-015 5 0 10 0 13 0
		 14 20.013889526832052 17 51.534138840596995 23 197.65031607519452 27 194.42270708284175
		 35 -383.37855090891617;
	setAttr -s 9 ".kit[7:8]"  1 1;
	setAttr -s 9 ".kot[7:8]"  1 1;
	setAttr -s 9 ".kix[7:8]"  0.0067478567361831665 0.00022964809613768011;
	setAttr -s 9 ".kiy[7:8]"  -0.9999772310256958 -1;
	setAttr -s 9 ".kox[7:8]"  0.0067478385753929615 0.00022964811068959534;
	setAttr -s 9 ".koy[7:8]"  -0.9999772310256958 -1;
createNode animCurveTL -n "l_foot_ik_ctrl_translateZ";
	rename -uid "6382011C-4245-F857-0B9E-F68289543EC5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  0 0 5 0 10 0 13 0 14 15.868508258436076
		 17 46.078658869365995 23 183.66546122060555 27 243.37928325623261 35 469.16355305949827;
	setAttr -s 9 ".kit[8]"  1;
	setAttr -s 9 ".kot[8]"  1;
	setAttr -s 9 ".kix[8]"  0.0012429171474650502;
	setAttr -s 9 ".kiy[8]"  0.9999992847442627;
	setAttr -s 9 ".kox[8]"  0.0012429177295416594;
	setAttr -s 9 ".koy[8]"  0.9999992847442627;
createNode animCurveTA -n "l_foot_ik_ctrl_rotateX";
	rename -uid "864897E4-4326-7CF3-FF24-89B46E503163";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  0 0 5 0 10 0 13 0 14 56.78719528811088 17 -6.0404725270083395
		 23 -81.881105399952062 27 -81.881105399952062 35 -81.881105399952062;
createNode animCurveTA -n "l_foot_ik_ctrl_rotateY";
	rename -uid "FC71485C-447B-FF7B-12BE-449B2A2AF305";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTA -n "l_foot_ik_ctrl_rotateZ";
	rename -uid "1C89BFCE-4B38-0CF6-2E7C-5EB088119474";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "l_foot_ik_ctrl_xOffset";
	rename -uid "F93996EC-409A-BF58-4036-F59ACC164D75";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "l_foot_ik_ctrl_yOffset";
	rename -uid "AD5325F8-48E4-7310-B349-2986831C4D1E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "l_foot_ik_ctrl_zOffset";
	rename -uid "449E0F13-475C-417C-5A88-828A9B39BFBA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "l_foot_ik_ctrl_stretch";
	rename -uid "D7833794-4A6F-C441-E446-0A9B386D54D0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "l_foot_ik_ctrl_heelRoll";
	rename -uid "A3E87E1C-466D-55BF-01E7-779C973CC7F3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "l_foot_ik_ctrl_ballRoll";
	rename -uid "CB30BDAF-4A26-378E-EADB-D992E6BBFB89";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "l_foot_ik_ctrl_toeRoll";
	rename -uid "6B335003-40BF-8812-E381-10A6228F8A2E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "l_foot_ik_ctrl_toeTap";
	rename -uid "7FC358AD-40A4-2E9F-4887-F29290C1E04F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "l_foot_ik_ctrl_heelPivot";
	rename -uid "5F955EBD-4861-61A2-BF53-5E91C622F450";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "l_foot_ik_ctrl_ballPivot";
	rename -uid "73885495-48D8-0BCF-C83D-F4BFE6167859";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "l_foot_ik_ctrl_toePivot";
	rename -uid "6472F4FD-4899-CB41-1801-828DDAAD973E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "l_foot_ik_ctrl_heelSide";
	rename -uid "4D2E1BAE-4040-5B64-5673-388C799B5F72";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "l_foot_ik_ctrl_ballSide";
	rename -uid "2CE40019-452B-E81A-7911-30BDB1C493C6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "l_foot_ik_ctrl_toeSide";
	rename -uid "DA07CC3C-4A61-E804-C89F-70912B442BCA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "l_foot_ik_ctrl_space";
	rename -uid "D95E0994-454C-AB81-697B-9A8BCA88FFF4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
	setAttr -s 8 ".kot[0:7]"  5 5 5 5 5 5 5 5;
createNode animCurveTL -n "r_foot_ik_ctrl_translateX";
	rename -uid "10C5BA18-4CF2-DB18-062B-9FBB71A4F0D2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 -6.6011894368414374 5 -6.6011894368414374
		 10 -6.6011894368414374 13 -6.6011894368414374 17 -5.813375638943727 23 0.97394323525193727
		 27 0.97394323525193727 35 0.97394323525193727;
createNode animCurveTL -n "r_foot_ik_ctrl_translateY";
	rename -uid "4BF7AA39-463B-37C8-75A0-16B204048144";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  0 -8.8817841970012523e-016 5 0 10 0 13 0
		 14 29.772963793357924 17 53.527714218406786 23 195.74448909671662 27 192.51688010436385
		 35 -385.28437788739404;
	setAttr -s 9 ".kit[7:8]"  1 1;
	setAttr -s 9 ".kot[7:8]"  1 1;
	setAttr -s 9 ".kix[7:8]"  0.007016476709395647 0.00023001084628049284;
	setAttr -s 9 ".kiy[7:8]"  -0.99997538328170776 -1;
	setAttr -s 9 ".kox[7:8]"  0.0070164748467504978 0.0002300108753843233;
	setAttr -s 9 ".koy[7:8]"  -0.99997538328170776 -1;
createNode animCurveTL -n "r_foot_ik_ctrl_translateZ";
	rename -uid "7DEA73B5-433C-435D-EEFC-A6A6F50DBE31";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  0 0 5 0 10 0 13 0 14 14.516665029626612
		 17 45.934922298528697 23 184.08112150604759 27 243.79494354167466 35 469.57921334494029;
	setAttr -s 9 ".kit[8]"  1;
	setAttr -s 9 ".kot[8]"  1;
	setAttr -s 9 ".kix[8]"  0.0012441447470337152;
	setAttr -s 9 ".kiy[8]"  0.9999992847442627;
	setAttr -s 9 ".kox[8]"  0.0012441442813724279;
	setAttr -s 9 ".koy[8]"  0.9999992847442627;
createNode animCurveTA -n "r_foot_ik_ctrl_rotateX";
	rename -uid "4F304CE7-4D28-9524-E57E-96A94E68C3CA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  0 0 5 0 10 0 13 0 14 77.124664619677972
		 17 34.612977127602164 23 -87.801746521538007 27 -87.801746521538007 35 -87.801746521538007;
createNode animCurveTA -n "r_foot_ik_ctrl_rotateY";
	rename -uid "F478B5EC-4BF8-0D47-F17A-C3833735F706";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  0 0 5 0 10 0 13 0 14 -0.10810675772682023
		 17 -3.3408026317520103 23 -10.543974530118616 27 -10.543974530118616 35 -10.543974530118616;
createNode animCurveTA -n "r_foot_ik_ctrl_rotateZ";
	rename -uid "B5000117-434F-E340-3AF5-0B93C95631D5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  0 0 5 0 10 0 13 0 14 0.30582524314895676
		 17 -0.42917886822745854 23 -15.856393702363333 27 -15.856393702363333 35 -15.856393702363333;
createNode animCurveTU -n "r_foot_ik_ctrl_xOffset";
	rename -uid "AB769D54-4B0E-1BE4-1E00-FBAE6F0F8412";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "r_foot_ik_ctrl_yOffset";
	rename -uid "A9B3D502-47F6-4BF4-92DF-ECB78D98BD4F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "r_foot_ik_ctrl_zOffset";
	rename -uid "F3C70EF1-42A2-3315-0F5A-9586B073518D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "r_foot_ik_ctrl_stretch";
	rename -uid "C1CB53EA-4D20-3B5F-C57A-41A17A691853";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "r_foot_ik_ctrl_heelRoll";
	rename -uid "6C157ED0-463E-EE0B-2EB9-1FA38B168231";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "r_foot_ik_ctrl_ballRoll";
	rename -uid "F8A6E432-4382-F6DB-9E51-268EEE8B07F1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "r_foot_ik_ctrl_toeRoll";
	rename -uid "F71D3674-4771-556C-5480-55B24D421D7C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "r_foot_ik_ctrl_toeTap";
	rename -uid "BBC56CBF-4034-442D-30A1-F6823A87AE9E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "r_foot_ik_ctrl_heelPivot";
	rename -uid "8ACF1012-4399-4FFF-6133-199F7045F91F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "r_foot_ik_ctrl_ballPivot";
	rename -uid "8C7C6B69-4CC2-72CC-4766-36B856092D81";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "r_foot_ik_ctrl_toePivot";
	rename -uid "3E4E4A36-4A51-6400-95B8-33A8485DBFF9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "r_foot_ik_ctrl_heelSide";
	rename -uid "6C9679A4-4553-0DD5-A5B7-499C0F774814";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "r_foot_ik_ctrl_ballSide";
	rename -uid "086DC2B9-477D-1697-EBE6-319CC1BD37BD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "r_foot_ik_ctrl_toeSide";
	rename -uid "8147CAE5-4526-DB48-E399-DAB490E7BAD4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
createNode animCurveTU -n "r_foot_ik_ctrl_space";
	rename -uid "45ED7DC3-4102-C47A-0129-768E2A839239";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 0 10 0 13 0 17 0 23 0 27 0 35 0;
	setAttr -s 8 ".kot[0:7]"  5 5 5 5 5 5 5 5;
createNode animCurveTL -n "r_leg_pv_01_translateX";
	rename -uid "615C1995-4599-A68E-1026-2B8F1731D5BB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 18 0 23 0 27 0 35 0;
createNode animCurveTL -n "r_leg_pv_01_translateY";
	rename -uid "40F9613B-4767-AEC4-EB2B-48948179C548";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 15 79.157468223049136 18 0 23 324.40146038219092
		 27 411.15146392802518 35 -145.6851951516943;
createNode animCurveTL -n "r_leg_pv_01_translateZ";
	rename -uid "838A4D67-4615-2D5B-EAC6-80BC9D1DCA1B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 15 170.87842641326438 18 0 23 139.812579072625
		 27 117.14975066969339 35 375.08194341813947;
createNode animCurveTA -n "r_leg_pv_01_rotateX";
	rename -uid "B7198A94-4D42-5152-0466-E5B34697AE36";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 18 0 23 0 27 0 35 0;
createNode animCurveTA -n "r_leg_pv_01_rotateY";
	rename -uid "2E84FC07-4619-F6D1-E7E1-3BAFB119B4A1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 18 0 23 0 27 0 35 0;
createNode animCurveTA -n "r_leg_pv_01_rotateZ";
	rename -uid "276D48F4-49A1-D8E4-C043-C8AD67361BDB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 18 0 23 0 27 0 35 0;
createNode animCurveTU -n "r_leg_pv_01_space";
	rename -uid "F8B2F109-4E9D-0671-EE41-D293B9AB49C9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 18 0 23 0 27 0 35 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTL -n "l_leg_pv_01_translateX";
	rename -uid "CED1BF81-4991-9D86-22A5-AAA020756AE9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 18 0 23 0 27 0 35 0;
createNode animCurveTL -n "l_leg_pv_01_translateY";
	rename -uid "C9AEBDBB-419C-0168-EEDD-899F9E0242D7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 15 79.157468223049136 18 0 23 324.40146038219092
		 27 411.15146392802518 35 -145.6851951516943;
createNode animCurveTL -n "l_leg_pv_01_translateZ";
	rename -uid "253DA2E7-4F86-83AD-C049-E6A2BF84A42E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 0 15 170.87842641326438 18 0 23 139.812579072625
		 27 117.14975066969339 35 375.08194341813947;
createNode animCurveTA -n "l_leg_pv_01_rotateX";
	rename -uid "CD119B17-45C1-0651-C186-DAA5034E85B6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 18 0 23 0 27 0 35 0;
createNode animCurveTA -n "l_leg_pv_01_rotateY";
	rename -uid "2C942D79-4A01-093A-8F19-1FB07E8F2B58";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 18 0 23 0 27 0 35 0;
createNode animCurveTA -n "l_leg_pv_01_rotateZ";
	rename -uid "BFCB8A40-4612-FEBA-F49D-D79E0A8BDE69";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 18 0 23 0 27 0 35 0;
createNode animCurveTU -n "l_leg_pv_01_space";
	rename -uid "1D2C70F0-49C1-ABD0-64E1-86A8B609A4C2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 5 0 18 0 23 0 27 0 35 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTL -n "l_hip_ctrl_translateX";
	rename -uid "D78459CA-4960-0522-1768-22BAAFFB60EA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  10 0 14 0 15 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_hip_ctrl_translateY";
	rename -uid "1E06B770-4090-6F52-66A4-6E89DBAEE914";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  10 0 14 0 15 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_hip_ctrl_translateZ";
	rename -uid "EAEDE5F3-44ED-AB56-57EF-D485112243FC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  10 0 14 0 15 0 21 0 25 0 35 0;
createNode animCurveTA -n "l_hip_ctrl_rotateX";
	rename -uid "A7FA80DD-40B3-05A1-B00F-C7A10D9D0CB2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  10 28.398711562433068 14 28.398711562433068
		 15 0 21 28.398711562433068 25 28.398711562433068 35 28.398711562433068;
createNode animCurveTA -n "l_hip_ctrl_rotateY";
	rename -uid "BC307CC2-4489-312F-7C52-6CAD36762D6B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  10 123.7290704964697 14 123.7290704964697
		 15 20.174471459291219 21 123.7290704964697 25 123.7290704964697 35 123.7290704964697;
createNode animCurveTA -n "l_hip_ctrl_rotateZ";
	rename -uid "36DD9BA1-4598-DFEA-01A8-DABFE09B0579";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  10 -9.7293426155055176 14 -9.7293426155055176
		 15 0 21 -9.7293426155055176 25 -9.7293426155055176 35 -9.7293426155055176;
createNode animCurveTU -n "l_hip_ctrl_rotateWith";
	rename -uid "53673AD1-485C-C874-1155-F59881A2484C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  10 0 14 0 15 0 21 0 25 0 35 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTL -n "l_knee_ctrl_translateX";
	rename -uid "9CE624F9-40C9-1090-0E3B-75865CA6CA34";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_knee_ctrl_translateY";
	rename -uid "A51D8A29-48AF-4AF7-6C8F-9EB2306C93E0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_knee_ctrl_translateZ";
	rename -uid "22EB8E7F-4FC6-50DB-9E0E-1BBDA3A6D764";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTA -n "l_knee_ctrl_rotateX";
	rename -uid "6910FEF0-4C31-926D-CD9B-E5AF5EBB7A5D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTA -n "l_knee_ctrl_rotateY";
	rename -uid "8EB0C8AE-4425-52E2-2531-5E9A0E1069A5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 4.7950287022565705 35 24.186688895205936;
createNode animCurveTA -n "l_knee_ctrl_rotateZ";
	rename -uid "1698B38E-400E-F23E-75C0-9E95B8E5C290";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_ankle_ctrl_translateX";
	rename -uid "479FD089-411F-4AC2-83B0-E8A50204192A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_ankle_ctrl_translateY";
	rename -uid "84D452D2-400B-CF98-8A10-0D9F0C8DF3DA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_ankle_ctrl_translateZ";
	rename -uid "405D9622-418E-7150-F4A9-18A71253C488";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTA -n "l_ankle_ctrl_rotateX";
	rename -uid "6EBC50AF-4A64-E327-B710-CEAD8A2AD0DE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 64.394657779900456 21 64.394657779900456
		 25 64.394657779900456 35 64.394657779900456;
createNode animCurveTA -n "l_ankle_ctrl_rotateY";
	rename -uid "F7886C04-4491-4561-B4A9-669EBA298B3F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 -1.554273623493839 21 -1.554273623493839
		 25 -1.554273623493839 35 -1.554273623493839;
createNode animCurveTA -n "l_ankle_ctrl_rotateZ";
	rename -uid "FBF45A9B-4492-8338-98AC-17A0AB5D6A02";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 1.0042729284252756 21 1.0042729284252756
		 25 1.0042729284252756 35 1.0042729284252756;
createNode animCurveTL -n "l_ball_ctrl_translateX";
	rename -uid "03E79115-4604-0752-6981-35A1905AFAE9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_ball_ctrl_translateY";
	rename -uid "4A65168D-4F34-9FC6-11DF-85A675C2E26E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTL -n "l_ball_ctrl_translateZ";
	rename -uid "EB9EE32B-425B-92AF-4D99-3C9B483648F6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTA -n "l_ball_ctrl_rotateX";
	rename -uid "62F08B70-4AF8-7EF8-6921-008F065921F8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTA -n "l_ball_ctrl_rotateY";
	rename -uid "883E914F-453A-4C34-6DC1-689D81EF4161";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTA -n "l_ball_ctrl_rotateZ";
	rename -uid "4BCC06FC-415F-A582-F75D-D0883D1864EF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_hip_ctrl_translateX";
	rename -uid "B2911843-46B4-FAA1-7EF9-B9B831CC7D7A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  10 0 14 0 15 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_hip_ctrl_translateY";
	rename -uid "F8FC50ED-4F9A-4341-A8CE-CE83878CC909";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  10 0 14 0 15 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_hip_ctrl_translateZ";
	rename -uid "8F0EF11D-4848-2DFC-3A22-00AAE0F70478";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  10 0 14 0 15 0 21 0 25 0 35 0;
createNode animCurveTA -n "r_hip_ctrl_rotateX";
	rename -uid "0297D5C7-4B87-EC01-B7D4-C686A4636ECB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  10 26.561925601000986 14 26.561925601000986
		 15 0 21 26.561925601000986 25 26.561925601000986 35 26.561925601000986;
createNode animCurveTA -n "r_hip_ctrl_rotateY";
	rename -uid "16D6EC08-4C5B-D97B-C4F4-B3A501272262";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  10 117.58638970831244 14 117.58638970831244
		 15 20.174471459291219 21 117.58638970831244 25 117.58638970831244 35 117.58638970831244;
createNode animCurveTA -n "r_hip_ctrl_rotateZ";
	rename -uid "FF736118-40EC-2EFF-16BE-67BCA32DC121";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  10 -21.378234794508444 14 -21.378234794508444
		 15 0 21 -21.378234794508444 25 -21.378234794508444 35 -21.378234794508444;
createNode animCurveTU -n "r_hip_ctrl_rotateWith";
	rename -uid "03132C02-4541-228C-5BAD-89AFFF9D39BA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  10 0 14 0 15 0 21 0 25 0 35 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTL -n "r_knee_ctrl_translateX";
	rename -uid "A725A9E1-4244-4EED-9921-08A65A4CDD8B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_knee_ctrl_translateY";
	rename -uid "36C25A20-4F33-6EB9-9887-37B2310EC9EF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_knee_ctrl_translateZ";
	rename -uid "DEA610B2-4FEC-84EF-219E-5680F2A19FDA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTA -n "r_knee_ctrl_rotateX";
	rename -uid "FED3EC52-4EA5-F64A-0F92-86B812AED836";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTA -n "r_knee_ctrl_rotateY";
	rename -uid "BD7B3BDA-4AD1-EBCA-D92E-47B17ADDD275";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  10 18.093992333414722 15 0 21 18.093992333414722
		 25 23.115695547742678 35 43.42405413509838;
createNode animCurveTA -n "r_knee_ctrl_rotateZ";
	rename -uid "68E500E1-4734-9289-77DF-46BEF351D3CA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_ankle_ctrl_translateX";
	rename -uid "4D391268-44FF-F16C-57E8-01B5FEA2BF2E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_ankle_ctrl_translateY";
	rename -uid "5DA4A98C-4D6D-C90E-AFE8-8BB80F15A178";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_ankle_ctrl_translateZ";
	rename -uid "DE7DD572-48E9-74A8-1F99-BF97AC2D66A1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTA -n "r_ankle_ctrl_rotateX";
	rename -uid "63752FF8-48BC-1AE0-6224-9C9A2F053598";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 61.650310502745249 21 61.650310502745249
		 25 61.650310502745249 35 61.650310502745249;
createNode animCurveTA -n "r_ankle_ctrl_rotateY";
	rename -uid "E219DC93-4C1C-8F30-6762-00AF1C36F7CF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 -6.4859785732104474 21 -6.4859785732104474
		 25 -6.4859785732104474 35 -6.4859785732104474;
createNode animCurveTA -n "r_ankle_ctrl_rotateZ";
	rename -uid "BEA5DBEF-4C7A-D341-E44C-BBBEACF62B49";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 5.6284054310141434 21 5.6284054310141434
		 25 5.6284054310141434 35 5.6284054310141434;
createNode animCurveTL -n "r_ball_ctrl_translateX";
	rename -uid "31D44661-4AF3-E0DA-3960-268C8C82FFC5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_ball_ctrl_translateY";
	rename -uid "A778617D-4D2D-199A-1660-6A83A5027DA2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTL -n "r_ball_ctrl_translateZ";
	rename -uid "1822DE55-4DA6-184D-A3CD-80ABB9B4B7A9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTA -n "r_ball_ctrl_rotateX";
	rename -uid "97ADB35D-445B-945E-8BB1-4D8528719D5B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTA -n "r_ball_ctrl_rotateY";
	rename -uid "9FE33493-4EF0-65C6-5BA0-0BB6D865611A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
createNode animCurveTA -n "r_ball_ctrl_rotateZ";
	rename -uid "B546FCF2-4C43-281D-D359-EBBBABD25662";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  10 0 21 0 25 0 35 0;
select -ne :time1;
	setAttr ".o" 0;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 6 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 114 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 3 ".r";
select -ne :defaultTextureList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 3 ".sol";
connectAttr "worldPlacement_midIkCtrl.o" "Grunt_Skeleton_RigRN.phl[1]";
connectAttr "worldPlacement_lookAt.o" "Grunt_Skeleton_RigRN.phl[2]";
connectAttr "worldPlacement_rotateX.o" "Grunt_Skeleton_RigRN.phl[3]";
connectAttr "worldPlacement_rotateY.o" "Grunt_Skeleton_RigRN.phl[4]";
connectAttr "worldPlacement_rotateZ.o" "Grunt_Skeleton_RigRN.phl[5]";
connectAttr "worldPlacement_translateX.o" "Grunt_Skeleton_RigRN.phl[6]";
connectAttr "worldPlacement_translateY.o" "Grunt_Skeleton_RigRN.phl[7]";
connectAttr "worldPlacement_translateZ.o" "Grunt_Skeleton_RigRN.phl[8]";
connectAttr "worldPlacement_scaleY.o" "Grunt_Skeleton_RigRN.phl[9]";
connectAttr "worldPlacement_scaleX.o" "Grunt_Skeleton_RigRN.phl[10]";
connectAttr "worldPlacement_scaleZ.o" "Grunt_Skeleton_RigRN.phl[11]";
connectAttr "worldPlacement_visibility.o" "Grunt_Skeleton_RigRN.phl[12]";
connectAttr "l_leg_ik_switch_IkFkSwitch.o" "Grunt_Skeleton_RigRN.phl[13]";
connectAttr "l_leg_ik_switch_autoStretch.o" "Grunt_Skeleton_RigRN.phl[14]";
connectAttr "r_leg_ik_switch_IkFkSwitch.o" "Grunt_Skeleton_RigRN.phl[15]";
connectAttr "r_leg_ik_switch_autoStretch.o" "Grunt_Skeleton_RigRN.phl[16]";
connectAttr "JawCtrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[17]";
connectAttr "JawCtrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[18]";
connectAttr "JawCtrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[19]";
connectAttr "JawCtrl_visibility.o" "Grunt_Skeleton_RigRN.phl[20]";
connectAttr "JawCtrl_translateX.o" "Grunt_Skeleton_RigRN.phl[21]";
connectAttr "JawCtrl_translateY.o" "Grunt_Skeleton_RigRN.phl[22]";
connectAttr "JawCtrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[23]";
connectAttr "JawCtrl_scaleX.o" "Grunt_Skeleton_RigRN.phl[24]";
connectAttr "JawCtrl_scaleY.o" "Grunt_Skeleton_RigRN.phl[25]";
connectAttr "JawCtrl_scaleZ.o" "Grunt_Skeleton_RigRN.phl[26]";
connectAttr "l_arm_ik_switch_IkFkSwitch.o" "Grunt_Skeleton_RigRN.phl[27]";
connectAttr "l_arm_ik_switch_elbowLock.o" "Grunt_Skeleton_RigRN.phl[28]";
connectAttr "l_arm_ik_switch_autoStretch.o" "Grunt_Skeleton_RigRN.phl[29]";
connectAttr "l_hand_ctrl_index.o" "Grunt_Skeleton_RigRN.phl[30]";
connectAttr "l_hand_ctrl_index1.o" "Grunt_Skeleton_RigRN.phl[31]";
connectAttr "l_hand_ctrl_index2.o" "Grunt_Skeleton_RigRN.phl[32]";
connectAttr "l_hand_ctrl_pinky.o" "Grunt_Skeleton_RigRN.phl[33]";
connectAttr "l_hand_ctrl_pinky1.o" "Grunt_Skeleton_RigRN.phl[34]";
connectAttr "l_hand_ctrl_pinky2.o" "Grunt_Skeleton_RigRN.phl[35]";
connectAttr "l_hand_ctrl_thumb.o" "Grunt_Skeleton_RigRN.phl[36]";
connectAttr "l_hand_ctrl_thumb1.o" "Grunt_Skeleton_RigRN.phl[37]";
connectAttr "l_hand_ctrl_thumb2.o" "Grunt_Skeleton_RigRN.phl[38]";
connectAttr "l_hand_ctrl_thumbReach.o" "Grunt_Skeleton_RigRN.phl[39]";
connectAttr "l_hand_ctrl_thumbTwist.o" "Grunt_Skeleton_RigRN.phl[40]";
connectAttr "l_hand_ctrl_indexSpread.o" "Grunt_Skeleton_RigRN.phl[41]";
connectAttr "l_hand_ctrl_pinkySpread.o" "Grunt_Skeleton_RigRN.phl[42]";
connectAttr "l_hand_ctrl_indexTwist.o" "Grunt_Skeleton_RigRN.phl[43]";
connectAttr "l_hand_ctrl_pinkyTwist.o" "Grunt_Skeleton_RigRN.phl[44]";
connectAttr "l_hand_ctrl_visibility.o" "Grunt_Skeleton_RigRN.phl[45]";
connectAttr "r_arm_ik_switch_IkFkSwitch.o" "Grunt_Skeleton_RigRN.phl[46]";
connectAttr "r_arm_ik_switch_elbowLock.o" "Grunt_Skeleton_RigRN.phl[47]";
connectAttr "r_arm_ik_switch_autoStretch.o" "Grunt_Skeleton_RigRN.phl[48]";
connectAttr "r_hand_ctrl_index.o" "Grunt_Skeleton_RigRN.phl[49]";
connectAttr "r_hand_ctrl_index1.o" "Grunt_Skeleton_RigRN.phl[50]";
connectAttr "r_hand_ctrl_index2.o" "Grunt_Skeleton_RigRN.phl[51]";
connectAttr "r_hand_ctrl_pinky.o" "Grunt_Skeleton_RigRN.phl[52]";
connectAttr "r_hand_ctrl_pinky1.o" "Grunt_Skeleton_RigRN.phl[53]";
connectAttr "r_hand_ctrl_pinky2.o" "Grunt_Skeleton_RigRN.phl[54]";
connectAttr "r_hand_ctrl_thumb.o" "Grunt_Skeleton_RigRN.phl[55]";
connectAttr "r_hand_ctrl_thumb1.o" "Grunt_Skeleton_RigRN.phl[56]";
connectAttr "r_hand_ctrl_thumb2.o" "Grunt_Skeleton_RigRN.phl[57]";
connectAttr "r_hand_ctrl_thumbReach.o" "Grunt_Skeleton_RigRN.phl[58]";
connectAttr "r_hand_ctrl_thumbTwist.o" "Grunt_Skeleton_RigRN.phl[59]";
connectAttr "r_hand_ctrl_indexSpread.o" "Grunt_Skeleton_RigRN.phl[60]";
connectAttr "r_hand_ctrl_pinkySpread.o" "Grunt_Skeleton_RigRN.phl[61]";
connectAttr "r_hand_ctrl_indexTwist.o" "Grunt_Skeleton_RigRN.phl[62]";
connectAttr "r_hand_ctrl_pinkyTwist.o" "Grunt_Skeleton_RigRN.phl[63]";
connectAttr "r_hand_ctrl_visibility.o" "Grunt_Skeleton_RigRN.phl[64]";
connectAttr "pelvis_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[65]";
connectAttr "pelvis_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[66]";
connectAttr "pelvis_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[67]";
connectAttr "pelvis_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[68]";
connectAttr "pelvis_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[69]";
connectAttr "pelvis_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[70]";
connectAttr "hips_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[71]";
connectAttr "hips_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[72]";
connectAttr "hips_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[73]";
connectAttr "hips_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[74]";
connectAttr "hips_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[75]";
connectAttr "hips_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[76]";
connectAttr "l_hip_ctrl_rotateWith.o" "Grunt_Skeleton_RigRN.phl[77]";
connectAttr "l_hip_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[78]";
connectAttr "l_hip_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[79]";
connectAttr "l_hip_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[80]";
connectAttr "l_hip_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[81]";
connectAttr "l_hip_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[82]";
connectAttr "l_hip_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[83]";
connectAttr "l_knee_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[84]";
connectAttr "l_knee_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[85]";
connectAttr "l_knee_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[86]";
connectAttr "l_knee_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[87]";
connectAttr "l_knee_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[88]";
connectAttr "l_knee_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[89]";
connectAttr "l_ankle_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[90]";
connectAttr "l_ankle_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[91]";
connectAttr "l_ankle_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[92]";
connectAttr "l_ankle_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[93]";
connectAttr "l_ankle_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[94]";
connectAttr "l_ankle_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[95]";
connectAttr "l_ball_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[96]";
connectAttr "l_ball_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[97]";
connectAttr "l_ball_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[98]";
connectAttr "l_ball_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[99]";
connectAttr "l_ball_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[100]";
connectAttr "l_ball_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[101]";
connectAttr "r_hip_ctrl_rotateWith.o" "Grunt_Skeleton_RigRN.phl[102]";
connectAttr "r_hip_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[103]";
connectAttr "r_hip_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[104]";
connectAttr "r_hip_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[105]";
connectAttr "r_hip_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[106]";
connectAttr "r_hip_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[107]";
connectAttr "r_hip_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[108]";
connectAttr "r_knee_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[109]";
connectAttr "r_knee_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[110]";
connectAttr "r_knee_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[111]";
connectAttr "r_knee_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[112]";
connectAttr "r_knee_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[113]";
connectAttr "r_knee_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[114]";
connectAttr "r_ankle_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[115]";
connectAttr "r_ankle_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[116]";
connectAttr "r_ankle_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[117]";
connectAttr "r_ankle_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[118]";
connectAttr "r_ankle_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[119]";
connectAttr "r_ankle_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[120]";
connectAttr "r_ball_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[121]";
connectAttr "r_ball_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[122]";
connectAttr "r_ball_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[123]";
connectAttr "r_ball_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[124]";
connectAttr "r_ball_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[125]";
connectAttr "r_ball_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[126]";
connectAttr "spine_02_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[127]";
connectAttr "spine_02_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[128]";
connectAttr "spine_02_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[129]";
connectAttr "topSpine_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[130]";
connectAttr "topSpine_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[131]";
connectAttr "topSpine_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[132]";
connectAttr "topSpine_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[133]";
connectAttr "topSpine_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[134]";
connectAttr "topSpine_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[135]";
connectAttr "topSpine_ctrl_spineScale.o" "Grunt_Skeleton_RigRN.phl[136]";
connectAttr "l_clav_rig_ctrl_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[137]";
connectAttr "l_clav_rig_ctrl_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[138]";
connectAttr "l_clav_rig_ctrl_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[139]";
connectAttr "l_clav_rig_ctrl_01_translateX.o" "Grunt_Skeleton_RigRN.phl[140]";
connectAttr "l_clav_rig_ctrl_01_translateY.o" "Grunt_Skeleton_RigRN.phl[141]";
connectAttr "l_clav_rig_ctrl_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[142]";
connectAttr "l_shoulder_fk_ctrl_01_rotateWith.o" "Grunt_Skeleton_RigRN.phl[143]"
		;
connectAttr "l_shoulder_fk_ctrl_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[144]";
connectAttr "l_shoulder_fk_ctrl_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[145]";
connectAttr "l_shoulder_fk_ctrl_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[146]";
connectAttr "l_shoulder_fk_ctrl_01_translateX.o" "Grunt_Skeleton_RigRN.phl[147]"
		;
connectAttr "l_shoulder_fk_ctrl_01_translateY.o" "Grunt_Skeleton_RigRN.phl[148]"
		;
connectAttr "l_shoulder_fk_ctrl_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[149]"
		;
connectAttr "l_elbow_fk_ctrl_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[150]";
connectAttr "l_elbow_fk_ctrl_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[151]";
connectAttr "l_elbow_fk_ctrl_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[152]";
connectAttr "l_elbow_fk_ctrl_01_translateX.o" "Grunt_Skeleton_RigRN.phl[153]";
connectAttr "l_elbow_fk_ctrl_01_translateY.o" "Grunt_Skeleton_RigRN.phl[154]";
connectAttr "l_elbow_fk_ctrl_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[155]";
connectAttr "l_wrist_fk_ctrl_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[156]";
connectAttr "l_wrist_fk_ctrl_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[157]";
connectAttr "l_wrist_fk_ctrl_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[158]";
connectAttr "l_wrist_fk_ctrl_01_translateX.o" "Grunt_Skeleton_RigRN.phl[159]";
connectAttr "l_wrist_fk_ctrl_01_translateY.o" "Grunt_Skeleton_RigRN.phl[160]";
connectAttr "l_wrist_fk_ctrl_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[161]";
connectAttr "r_clav_rig_ctrl_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[162]";
connectAttr "r_clav_rig_ctrl_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[163]";
connectAttr "r_clav_rig_ctrl_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[164]";
connectAttr "r_clav_rig_ctrl_01_translateX.o" "Grunt_Skeleton_RigRN.phl[165]";
connectAttr "r_clav_rig_ctrl_01_translateY.o" "Grunt_Skeleton_RigRN.phl[166]";
connectAttr "r_clav_rig_ctrl_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[167]";
connectAttr "r_shoulder_fk_ctrl_01_rotateWith.o" "Grunt_Skeleton_RigRN.phl[168]"
		;
connectAttr "r_shoulder_fk_ctrl_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[169]";
connectAttr "r_shoulder_fk_ctrl_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[170]";
connectAttr "r_shoulder_fk_ctrl_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[171]";
connectAttr "r_shoulder_fk_ctrl_01_translateX.o" "Grunt_Skeleton_RigRN.phl[172]"
		;
connectAttr "r_shoulder_fk_ctrl_01_translateY.o" "Grunt_Skeleton_RigRN.phl[173]"
		;
connectAttr "r_shoulder_fk_ctrl_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[174]"
		;
connectAttr "r_elbow_fk_ctrl_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[175]";
connectAttr "r_elbow_fk_ctrl_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[176]";
connectAttr "r_elbow_fk_ctrl_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[177]";
connectAttr "r_elbow_fk_ctrl_01_translateX.o" "Grunt_Skeleton_RigRN.phl[178]";
connectAttr "r_elbow_fk_ctrl_01_translateY.o" "Grunt_Skeleton_RigRN.phl[179]";
connectAttr "r_elbow_fk_ctrl_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[180]";
connectAttr "r_wrist_fk_ctrl_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[181]";
connectAttr "r_wrist_fk_ctrl_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[182]";
connectAttr "r_wrist_fk_ctrl_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[183]";
connectAttr "r_wrist_fk_ctrl_01_translateX.o" "Grunt_Skeleton_RigRN.phl[184]";
connectAttr "r_wrist_fk_ctrl_01_translateY.o" "Grunt_Skeleton_RigRN.phl[185]";
connectAttr "r_wrist_fk_ctrl_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[186]";
connectAttr "neckBase_ctrl_rotateWith.o" "Grunt_Skeleton_RigRN.phl[187]";
connectAttr "neckBase_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[188]";
connectAttr "neckBase_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[189]";
connectAttr "neckBase_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[190]";
connectAttr "neckBase_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[191]";
connectAttr "neckBase_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[192]";
connectAttr "neckBase_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[193]";
connectAttr "neck_ctrl_scaleX.o" "Grunt_Skeleton_RigRN.phl[194]";
connectAttr "neck_ctrl_scaleY.o" "Grunt_Skeleton_RigRN.phl[195]";
connectAttr "neck_ctrl_rotateWith.o" "Grunt_Skeleton_RigRN.phl[196]";
connectAttr "neck_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[197]";
connectAttr "neck_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[198]";
connectAttr "neck_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[199]";
connectAttr "neck_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[200]";
connectAttr "neck_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[201]";
connectAttr "neck_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[202]";
connectAttr "l_foot_ik_ctrl_xOffset.o" "Grunt_Skeleton_RigRN.phl[203]";
connectAttr "l_foot_ik_ctrl_yOffset.o" "Grunt_Skeleton_RigRN.phl[204]";
connectAttr "l_foot_ik_ctrl_zOffset.o" "Grunt_Skeleton_RigRN.phl[205]";
connectAttr "l_foot_ik_ctrl_stretch.o" "Grunt_Skeleton_RigRN.phl[206]";
connectAttr "l_foot_ik_ctrl_heelRoll.o" "Grunt_Skeleton_RigRN.phl[207]";
connectAttr "l_foot_ik_ctrl_ballRoll.o" "Grunt_Skeleton_RigRN.phl[208]";
connectAttr "l_foot_ik_ctrl_toeRoll.o" "Grunt_Skeleton_RigRN.phl[209]";
connectAttr "l_foot_ik_ctrl_toeTap.o" "Grunt_Skeleton_RigRN.phl[210]";
connectAttr "l_foot_ik_ctrl_heelPivot.o" "Grunt_Skeleton_RigRN.phl[211]";
connectAttr "l_foot_ik_ctrl_ballPivot.o" "Grunt_Skeleton_RigRN.phl[212]";
connectAttr "l_foot_ik_ctrl_toePivot.o" "Grunt_Skeleton_RigRN.phl[213]";
connectAttr "l_foot_ik_ctrl_heelSide.o" "Grunt_Skeleton_RigRN.phl[214]";
connectAttr "l_foot_ik_ctrl_ballSide.o" "Grunt_Skeleton_RigRN.phl[215]";
connectAttr "l_foot_ik_ctrl_toeSide.o" "Grunt_Skeleton_RigRN.phl[216]";
connectAttr "l_foot_ik_ctrl_space.o" "Grunt_Skeleton_RigRN.phl[217]";
connectAttr "l_foot_ik_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[218]";
connectAttr "l_foot_ik_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[219]";
connectAttr "l_foot_ik_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[220]";
connectAttr "l_foot_ik_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[221]";
connectAttr "l_foot_ik_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[222]";
connectAttr "l_foot_ik_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[223]";
connectAttr "r_foot_ik_ctrl_xOffset.o" "Grunt_Skeleton_RigRN.phl[224]";
connectAttr "r_foot_ik_ctrl_yOffset.o" "Grunt_Skeleton_RigRN.phl[225]";
connectAttr "r_foot_ik_ctrl_zOffset.o" "Grunt_Skeleton_RigRN.phl[226]";
connectAttr "r_foot_ik_ctrl_stretch.o" "Grunt_Skeleton_RigRN.phl[227]";
connectAttr "r_foot_ik_ctrl_heelRoll.o" "Grunt_Skeleton_RigRN.phl[228]";
connectAttr "r_foot_ik_ctrl_ballRoll.o" "Grunt_Skeleton_RigRN.phl[229]";
connectAttr "r_foot_ik_ctrl_toeRoll.o" "Grunt_Skeleton_RigRN.phl[230]";
connectAttr "r_foot_ik_ctrl_toeTap.o" "Grunt_Skeleton_RigRN.phl[231]";
connectAttr "r_foot_ik_ctrl_heelPivot.o" "Grunt_Skeleton_RigRN.phl[232]";
connectAttr "r_foot_ik_ctrl_ballPivot.o" "Grunt_Skeleton_RigRN.phl[233]";
connectAttr "r_foot_ik_ctrl_toePivot.o" "Grunt_Skeleton_RigRN.phl[234]";
connectAttr "r_foot_ik_ctrl_heelSide.o" "Grunt_Skeleton_RigRN.phl[235]";
connectAttr "r_foot_ik_ctrl_ballSide.o" "Grunt_Skeleton_RigRN.phl[236]";
connectAttr "r_foot_ik_ctrl_toeSide.o" "Grunt_Skeleton_RigRN.phl[237]";
connectAttr "r_foot_ik_ctrl_space.o" "Grunt_Skeleton_RigRN.phl[238]";
connectAttr "r_foot_ik_ctrl_rotateX.o" "Grunt_Skeleton_RigRN.phl[239]";
connectAttr "r_foot_ik_ctrl_rotateY.o" "Grunt_Skeleton_RigRN.phl[240]";
connectAttr "r_foot_ik_ctrl_rotateZ.o" "Grunt_Skeleton_RigRN.phl[241]";
connectAttr "r_foot_ik_ctrl_translateX.o" "Grunt_Skeleton_RigRN.phl[242]";
connectAttr "r_foot_ik_ctrl_translateY.o" "Grunt_Skeleton_RigRN.phl[243]";
connectAttr "r_foot_ik_ctrl_translateZ.o" "Grunt_Skeleton_RigRN.phl[244]";
connectAttr "r_leg_pv_01_space.o" "Grunt_Skeleton_RigRN.phl[245]";
connectAttr "r_leg_pv_01_translateX.o" "Grunt_Skeleton_RigRN.phl[246]";
connectAttr "r_leg_pv_01_translateY.o" "Grunt_Skeleton_RigRN.phl[247]";
connectAttr "r_leg_pv_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[248]";
connectAttr "r_leg_pv_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[249]";
connectAttr "r_leg_pv_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[250]";
connectAttr "r_leg_pv_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[251]";
connectAttr "l_leg_pv_01_space.o" "Grunt_Skeleton_RigRN.phl[252]";
connectAttr "l_leg_pv_01_translateX.o" "Grunt_Skeleton_RigRN.phl[253]";
connectAttr "l_leg_pv_01_translateY.o" "Grunt_Skeleton_RigRN.phl[254]";
connectAttr "l_leg_pv_01_translateZ.o" "Grunt_Skeleton_RigRN.phl[255]";
connectAttr "l_leg_pv_01_rotateX.o" "Grunt_Skeleton_RigRN.phl[256]";
connectAttr "l_leg_pv_01_rotateY.o" "Grunt_Skeleton_RigRN.phl[257]";
connectAttr "l_leg_pv_01_rotateZ.o" "Grunt_Skeleton_RigRN.phl[258]";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "grunt_rig.msg" "Grunt_Skeleton_RigRN.asn[0]";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of skeleton_base@jump_into_pit_03.ma
