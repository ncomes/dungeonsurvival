//Maya ASCII 2016 scene
//Name: Enemy@Grunt_Death_01.ma
//Last modified: Sat, Nov 21, 2015 11:04:57 PM
//Codeset: 1252
requires maya "2016";
requires -nodeType "ikSpringSolver" "ikSpringSolver" "1.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "7A31353C-43E4-9F0A-9A8B-04850881A605";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 335.79129140481803 184.95221725540287 208.04446016833666 ;
	setAttr ".r" -type "double3" -20.138352729603433 54.600000000000641 -2.7452598438518579e-015 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "4BA5B63F-4C81-2D89-9CFB-B880E7F1E751";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 425.80736812007638;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "13373D7C-420C-7A77-FD30-E3BDC9F12FA6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "D9F11792-420E-4B7A-9193-488C666E0E9B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "B53C4094-4E3D-B041-4F04-038DFC4F9299";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "6A0FF29E-414B-C16A-2734-9CB5B775FB32";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "9A6D1506-492B-396E-2975-F097EDD969FE";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "86A843ED-494D-E505-1C95-018A91D72FD0";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "ref_frame";
	rename -uid "322798C6-40C8-385B-700B-59B76C5A03B6";
	addAttr -ci true -sn "refFrame" -ln "refFrame" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "Character1_Hips" -p "ref_frame";
	rename -uid "E47D206B-450A-F701-1740-21AEBD93617F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 1.406920211983633 -3.3458391704689405 -22.822435086355146 ;
	setAttr ".ssc" no;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftUpLeg" -p "Character1_Hips";
	rename -uid "90E72E40-4B32-54DC-8925-E1BFA5E8F087";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 96.630252250634001 6.5132649289235918 97.599644355322027 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftLeg" -p "Character1_LeftUpLeg";
	rename -uid "4CB79ADA-4214-55F5-3995-72986A63017E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -129.48089468354581 -7.9067044581664252 177.65996829599328 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFoot" -p "Character1_LeftLeg";
	rename -uid "566B71B3-45C9-B4BB-79F2-21BFE2DC9CB6";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -82.075120143962664 29.940914059061775 128.00062508119765 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftToeBase" -p "Character1_LeftFoot";
	rename -uid "D39CC7B0-4039-1321-FD44-BE86FBBA3F50";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -126.16837928120003 -7.059491002168353 35.858557690282048 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle1" -p "Character1_LeftToeBase";
	rename -uid "018550BA-4FE6-A54E-2FA7-669982270CA7";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -100.08944018296057 24.928076457701248 56.143888543356006 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle2" -p "Character1_LeftFootMiddle1";
	rename -uid "CDC5B2E1-44C7-01B4-5013-DCADD1359C75";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -39.508027973091373 -2.0582897454817326 31.259679900637106 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightUpLeg" -p "Character1_Hips";
	rename -uid "46B165AA-4E0A-95C2-8D36-00841D2B72AA";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 11.416489881294433 -11.43673382189878 -152.25229525481123 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightLeg" -p "Character1_RightUpLeg";
	rename -uid "2971A3C1-4BA1-C4CF-C727-E7B1975D362A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -22.942928776539453 -35.956391389429932 101.2794675694641 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFoot" -p "Character1_RightLeg";
	rename -uid "E11255C8-4364-457F-F282-0281F848DC08";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 66.643786034585347 17.03863148951929 124.91120862192221 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightToeBase" -p "Character1_RightFoot";
	rename -uid "019F59FA-4313-81D3-97D3-29BD746D0BC9";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -155.84366405206632 -5.8738653577349114 -93.73595023516998 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle1" -p "Character1_RightToeBase";
	rename -uid "ED5B7B11-4ECD-7F07-EAC9-A09C6621ED39";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -166.06091029301223 54.415170601070251 -127.18425880983375 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle2" -p "Character1_RightFootMiddle1";
	rename -uid "C6D9B394-4CFD-7552-2308-47AB1B328D04";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -129.2483692316568 10.731788640430997 6.9892235317174372 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine" -p "Character1_Hips";
	rename -uid "7953EBB5-43EC-2766-0E34-00B4236B6D8C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 11.416489881294433 -11.43673382189878 -152.25229525481123 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine1" -p "Character1_Spine";
	rename -uid "89BAD351-4180-BB8E-D05E-E9BE15E49AA7";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 24.933471933369759 -37.782249520228419 -20.761597110625811 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftShoulder" -p "Character1_Spine1";
	rename -uid "54E4F197-4D97-8D17-41CE-64BDEE99E63D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 144.67442469508543 70.793071067570196 19.807411116001759 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftArm" -p "Character1_LeftShoulder";
	rename -uid "238513CC-4627-773E-05E4-B58BAE76619B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 99.384574925726014 -60.006523708111558 170.15034313799799 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftForeArm" -p "Character1_LeftArm";
	rename -uid "B834BD28-4C91-AF7B-193A-03AD242B8D71";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 70.061799635480924 -4.8580147780287897 21.798459685601706 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHand" -p "Character1_LeftForeArm";
	rename -uid "C4B49476-4CC5-59C6-5D13-4DA101A737EF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.9708494230587 -13.075378107872792 150.36023365917421 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb1" -p "Character1_LeftHand";
	rename -uid "8D180CED-4798-A229-321A-7D96D85536FE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -65.748924167512314 39.787402356574312 166.7746235430387 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb2" -p "Character1_LeftHandThumb1";
	rename -uid "9276A69F-407C-4C2F-41D6-A78EC97904BB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -39.861597330550808 6.7221908644292059 68.670730027774553 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb3" -p "Character1_LeftHandThumb2";
	rename -uid "F1907D38-4B85-8428-18AA-97BD113CAF0B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -120.21671274765816 -26.239331955300646 57.413978285788289 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex1" -p "Character1_LeftHand";
	rename -uid "E22D0724-44D1-5C93-0A51-77BC3F55B3D4";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -152.38220651039205 4.806304023778381 48.842756939944849 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex2" -p "Character1_LeftHandIndex1";
	rename -uid "11D681B7-4859-D272-73C7-449FC0E59D48";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 176.30914278667802 -13.434888179376443 97.633542306389202 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex3" -p "Character1_LeftHandIndex2";
	rename -uid "105DFF03-40F7-0D8F-8DCB-029960321473";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -102.43860404365057 -60.659303181068822 -152.9472923666747 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing1" -p "Character1_LeftHand";
	rename -uid "380B299A-4FEC-50F2-0C9C-569EE23601FA";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -31.516946849722316 2.5758733401430476 -137.43155166436202 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing2" -p "Character1_LeftHandRing1";
	rename -uid "78330DDB-4304-2AE6-9F35-009271C59699";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -37.632109434888541 57.372964012610872 -137.42548313576771 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing3" -p "Character1_LeftHandRing2";
	rename -uid "C5E92D23-467F-0E6A-4EA5-AB8D3A6B892B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -16.443988862208798 8.3290143241887424 -59.438981507600595 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightShoulder" -p "Character1_Spine1";
	rename -uid "67CD2E91-4EC0-C54F-67C8-7483A21726C7";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -174.37324535156188 -81.21351031321781 -130.38505903740904 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightArm" -p "Character1_RightShoulder";
	rename -uid "FD8E85EF-4199-D698-B5A4-32AA8BCC9816";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -104.33239449525594 -37.101615257968433 20.137114731244228 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightForeArm" -p "Character1_RightArm";
	rename -uid "4CE6DAEC-4B46-15BD-D043-FCB6074570AB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -43.743657547705759 -9.6726323149773243 103.83730983340543 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHand" -p "Character1_RightForeArm";
	rename -uid "E55691FC-4F6C-C495-B981-C2813224EBE9";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 140.03960701773187 13.516449256889247 -135.99675148159383 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb1" -p "Character1_RightHand";
	rename -uid "F11E0749-4AB3-EA76-8C1A-369F3673A93A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.10974980768816 71.94515848234029 -178.78334614629264 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb2" -p "Character1_RightHandThumb1";
	rename -uid "4198B160-46D7-A24C-DA0F-94A9B892728D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.2056241799594538 33.542113270229827 -130.48989956049317 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb3" -p "Character1_RightHandThumb2";
	rename -uid "1E7A32CF-494A-3649-758F-72930C6A36AC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 134.7539820196863 -70.074251222974951 75.428204415317296 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex1" -p "Character1_RightHand";
	rename -uid "E807E83E-43AF-4BA0-D2F9-318551DDF62C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.10974980768816 71.94515848234029 -178.78334614629264 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex2" -p "Character1_RightHandIndex1";
	rename -uid "0E9142BD-4163-FD5D-7018-0893875B1DFF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.2057394778445882 33.54331930430493 -130.49010822234442 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex3" -p "Character1_RightHandIndex2";
	rename -uid "6FE878FA-4474-8F3A-6729-C291607ADA19";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 134.79643182941854 -70.056599207814415 75.383915829770686 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing1" -p "Character1_RightHand";
	rename -uid "E86ED999-4114-38F1-8F03-5CA3C502BD21";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.10974980768816 71.94515848234029 -178.78334614629264 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing2" -p "Character1_RightHandRing1";
	rename -uid "BEBDF1C2-4FE9-5866-D952-CDACD5DFEFBE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.2056241799594538 33.542113270229827 -130.48989956049317 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing3" -p "Character1_RightHandRing2";
	rename -uid "4F8EA0E8-458C-8D0B-D82D-D085D1D3728D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 134.7539820196863 -70.074251222974951 75.428204415317296 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Neck" -p "Character1_Spine1";
	rename -uid "A310B9B4-40EF-C731-763F-C6A025F73DD2";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 161.13402532275541 31.934232202611735 171.90006021381112 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Head" -p "Character1_Neck";
	rename -uid "8EF30CA3-4A10-2156-94BE-CBA65102CC1C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -18.098521216998183 -9.9246993917445305 116.26360516393567 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "jaw" -p "Character1_Head";
	rename -uid "D6B95628-4B1F-C934-6C15-4EB3FC44EEFD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.667422224424381 90 0 ;
	setAttr ".radi" 6;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "eyes" -p "Character1_Head";
	rename -uid "FA2998AF-4D7D-AAC6-67F9-BE81D6829162";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.667422224424381 90 0 ;
	setAttr ".radi" 4.5;
	setAttr ".fbxID" 5;
createNode animCurveTU -n "jaw_lockInfluenceWeights";
	rename -uid "772DBBA7-44F4-B951-0337-A1BCEEDA083E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "Character1_Hips_translateX";
	rename -uid "558B747B-492C-C258-F136-54AD8F7A12D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -1.2373414941843457e-016 1 -0.032118361443281174
		 2 -0.064236722886562347 3 -0.094519935548305511 4 -0.12446771562099457 5 -0.15452319383621216
		 6 -0.1812664270401001 7 -0.20127743482589722 8 -0.21671582758426666 9 -0.23029817640781403
		 10 -0.23943999409675598 11 -0.24155677855014801 12 -0.23406404256820679 13 -0.20721590518951416
		 14 -0.15846075117588043 15 -0.096005409955978394 16 -0.015076342970132828 17 0.080563127994537354
		 18 0.16990083456039429 19 0.23192457854747772 20 0.26158881187438965 21 0.27305534482002258
		 22 0.27412322163581848 23 0.27259144186973572 24 0.27625903487205505 25 0.27625903487205505;
createNode animCurveTL -n "Character1_Hips_translateY";
	rename -uid "91164AF4-46BA-62A8-5C6D-6ABD97464374";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 35.110595703125 1 43.724258422851562 2 52.337913513183594
		 3 58.566890716552734 4 62.656940460205078 5 65.751609802246094 6 66.812881469726563
		 7 64.802757263183594 8 59.195468902587891 9 51.000606536865234 10 41.483142852783203
		 11 31.908069610595703 12 23.540374755859375 13 16.248195648193359 14 9.9672718048095703
		 15 6.063971996307373 16 5.0589084625244141 17 5.7814316749572754 18 7.0610246658325195
		 19 7.7271695137023926 20 7.5678110122680664 21 7.2083978652954102 22 6.7346673011779785
		 23 6.2323575019836426 24 5.7872061729431152 25 5.7872061729431152;
createNode animCurveTL -n "Character1_Hips_translateZ";
	rename -uid "1685FD3B-42E9-3966-1A4A-57954150C30D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -5.8778152465820313 1 -5.6434340476989746
		 2 -5.4090523719787598 3 -6.6958222389221191 4 -10.197181701660156 5 -14.900283813476562
		 6 -19.766843795776367 7 -23.758583068847656 8 -26.706640243530273 9 -29.225326538085938
		 10 -31.451118469238281 11 -33.520500183105469 12 -35.569942474365234 13 -37.600086212158203
		 14 -39.322113037109375 15 -40.438327789306641 16 -40.868484497070313 17 -40.882179260253906
		 18 -40.70648193359375 19 -40.568450927734375 20 -40.503036499023438 21 -40.403553009033203
		 22 -40.284603118896484 23 -40.160789489746094 24 -40.046703338623047 25 -40.046703338623047;
createNode animCurveTU -n "Character1_Hips_scaleX";
	rename -uid "F67322FB-43ED-C094-EBE3-3182C01A6322";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_Hips_scaleY";
	rename -uid "F3033C5D-4115-06D2-825B-B9BF44F19BA2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_Hips_scaleZ";
	rename -uid "7337A2F8-4B7C-4600-674A-A299091A7673";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Hips_rotateX";
	rename -uid "4F48B47C-41ED-35BD-055B-5FBC2C757C87";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -38.452392578125 1 -40.107803344726563
		 2 -41.764472961425781 3 -45.838592529296875 4 -53.979732513427734 5 -64.927726745605469
		 6 -76.658966064453125 7 -87.0947265625 8 -97.69895172119142 9 -109.51312255859375
		 10 -119.94016265869139 11 -126.55616760253906 12 -127.13166809082031 13 -116.71913146972658
		 14 -98.316642761230469 15 -82.936965942382812 16 -73.956893920898437 17 -67.198692321777344
		 18 -62.545700073242195 19 -60.026844024658203 20 -60.153190612792969 21 -62.698726654052734
		 22 -66.634811401367188 23 -70.920974731445312 24 -74.514701843261719 25 -74.514701843261719;
createNode animCurveTA -n "Character1_Hips_rotateY";
	rename -uid "896AD448-4E76-147A-FEDD-6E9FCF43736A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -13.724538803100586 1 -14.120668411254883
		 2 -14.501588821411133 3 -15.418374061584473 4 -16.984977722167969 5 -18.450000762939453
		 6 -19.16876220703125 7 -19.052871704101563 8 -18.242420196533203 9 -16.574943542480469
		 10 -14.41900062561035 11 -12.628852844238281 12 -12.148236274719238 13 -14.176655769348145
		 14 -16.919412612915039 15 -18.126007080078125 16 -18.602182388305664 17 -19.10523796081543
		 18 -19.682954788208008 19 -20.193258285522461 20 -20.552167892456055 21 -20.813405990600586
		 22 -20.965919494628906 23 -21.029108047485352 24 -21.070991516113281 25 -21.070991516113281;
createNode animCurveTA -n "Character1_Hips_rotateZ";
	rename -uid "2F8919D1-4B00-28DA-B0DB-629A52B30480";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 7.4860110282897949 1 7.980703353881835
		 2 8.4862985610961914 3 9.7888660430908203 4 12.571805953979494 5 16.589427947998047
		 6 21.095714569091797 7 25.138820648193359 8 29.008935928344723 9 32.954277038574219
		 10 36.1341552734375 11 38.102401733398437 12 38.71417236328125 13 36.774761199951172
		 14 32.289993286132812 15 28.568737030029297 16 27.497402191162109 17 27.614252090454102
		 18 28.29931640625 19 28.950708389282227 20 29.546657562255863 21 30.411550521850586
		 22 31.442146301269535 23 32.519474029541016 24 33.525314331054688 25 33.525314331054688;
createNode animCurveTU -n "Character1_Hips_visibility";
	rename -uid "E3286335-4741-775D-FCEC-5987EB3E7D1F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftUpLeg_translateX";
	rename -uid "E94985BD-4940-8CEB-1CA6-83907CA3940A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  6 10.596480369567871 7 10.596480369567871
		 8 12.376348495483398 9 15.967294692993164 10 18.746330261230469 11 20.059017181396484
		 12 21.067239761352539 13 21.872482299804687 14 22.576234817504883 15 23.279989242553711
		 16 24.085233688354492 17 24.085233688354492;
createNode animCurveTL -n "Character1_LeftUpLeg_translateY";
	rename -uid "D689E826-43C2-77F0-4BF6-33BA760E5121";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  6 3.315338134765625 7 3.315338134765625
		 8 2.1462357044219971 9 -0.21703460812568665 10 -2.0584185123443604 11 -2.9472417831420898
		 12 -3.640357494354248 13 -4.2029995918273926 14 -4.7004079818725586 15 -5.1978158950805664
		 16 -5.7604594230651855 17 -5.7604594230651855;
createNode animCurveTL -n "Character1_LeftUpLeg_translateZ";
	rename -uid "DE82A303-4C74-94E9-F2AC-D981CD5D5E5C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  6 2.0558223724365234 7 2.0558223724365234
		 8 6.6133041381835937 9 14.992478370666504 10 19.253438949584961 11 17.864904403686523
		 12 14.932037353515625 13 10.96961784362793 14 6.4924221038818359 15 2.0152273178100586
		 16 -1.9471917152404785 17 -1.9471917152404785;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleX";
	rename -uid "59E139DB-4A0E-D893-5C99-01B9B5B7E7CC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleY";
	rename -uid "B982D90B-4F04-9987-2CF7-6BAF9C43658D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleZ";
	rename -uid "1120C0F9-4A91-2AAD-4820-A9B12DD4B6E0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateX";
	rename -uid "3734087C-480B-F325-F73F-D2AD5F6FEEBF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 5.5763673782348633 1 4.9189586639404297
		 2 4.1172757148742676 3 3.3401703834533691 4 2.0822329521179199 5 0.028975039720535278
		 6 -1.9841734170913696 7 -3.1840338706970215 8 -14.009194374084473 9 -16.243389129638672
		 10 -16.534685134887695 11 -10.803008079528809 12 -17.839298248291016 13 -23.081916809082035
		 14 -26.438793182373047 15 -26.064107894897461 16 -24.876897811889648 17 -24.057989120483398
		 18 -22.490753173828125 19 -28.726520538330082 20 -22.191911697387695 21 39.620796203613281
		 22 41.647026062011719 23 44.480648040771484 24 47.4117431640625 25 47.4117431640625;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateY";
	rename -uid "91E2DA83-49B3-23A3-3F35-86A14817B3A8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -13.410167694091797 1 -13.203137397766113
		 2 -13.357522010803223 3 -14.523715972900391 4 -16.177780151367188 5 -17.159574508666992
		 6 -17.470970153808594 7 -17.44904899597168 8 -5.8325943946838379 9 6.008275032043457
		 10 16.285984039306641 11 24.177221298217773 12 24.972476959228516 13 22.97389030456543
		 14 21.238479614257813 15 22.731233596801758 16 27.228221893310547 17 31.445341110229492
		 18 36.949012756347656 19 43.330997467041016 20 56.9530029296875 21 56.552219390869141
		 22 58.858032226562493 23 60.828590393066413 24 62.310073852539055 25 62.310073852539055;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateZ";
	rename -uid "8377965D-4CBF-82E4-3803-7CAF7A1BECD5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -5.9287300109863281 1 -3.6906459331512456
		 2 -1.8598453998565672 3 -4.8964834213256836 4 -14.534616470336912 5 -27.319402694702148
		 6 -40.339515686035156 7 -52.910228729248047 8 -66.024795532226562 9 -64.074386596679688
		 10 -60.586524963378913 11 -50.041717529296875 12 -63.412517547607422 13 -82.550712585449219
		 14 -106.10117340087891 15 -119.12023162841798 16 -115.49559020996094 17 -103.58232116699219
		 18 -92.612823486328125 19 -92.577293395996094 20 -79.932327270507812 21 -17.702844619750977
		 22 -14.432252883911133 23 -10.225610733032227 24 -6.0740351676940918 25 -6.0740351676940918;
createNode animCurveTU -n "Character1_LeftUpLeg_visibility";
	rename -uid "7D29CB4F-4EC4-91A2-6B88-818515148D72";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftLeg_translateX";
	rename -uid "AE700F62-431E-7B9D-8B8A-3F9A8D6A18A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -21.532840728759766;
createNode animCurveTL -n "Character1_LeftLeg_translateY";
	rename -uid "F8EC2D62-4155-B4C8-FFAB-14B29E1916D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4219808578491211;
createNode animCurveTL -n "Character1_LeftLeg_translateZ";
	rename -uid "7750872B-4EF1-70FB-0ACC-62AE43384E83";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8661990165710449;
createNode animCurveTU -n "Character1_LeftLeg_scaleX";
	rename -uid "E4B2C3C6-46B3-F9A9-33F2-2CA1CA2F6046";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftLeg_scaleY";
	rename -uid "66599657-4379-9F1F-7E52-E79BACFCA009";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftLeg_scaleZ";
	rename -uid "2533299A-42E0-95C3-FFAA-D5B0957248AA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftLeg_rotateX";
	rename -uid "A1552B4C-42A5-72DD-9D37-70A9E8812D57";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 58.691905975341804 1 56.216499328613281
		 2 53.890644073486328 3 57.376861572265625 4 62.981773376464844 5 64.054496765136719
		 6 59.184444427490234 7 46.166431427001953 8 42.667926788330078 9 35.957050323486328
		 10 22.105226516723633 11 -4.1137104034423828 12 -22.644580841064457 13 -29.723054885864258
		 14 -31.668807983398434 15 -29.277877807617188 16 -28.993894577026371 17 -31.715688705444336
		 18 -33.194648742675781 19 -29.863998413085938 20 -20.840452194213867 21 -9.6474714279174805
		 22 -7.3655853271484375 23 -4.81365966796875 24 -2.742844820022583 25 -2.742844820022583;
createNode animCurveTA -n "Character1_LeftLeg_rotateY";
	rename -uid "9764C5B7-419E-6280-EDB2-D8AB1167BC01";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -44.524219512939453 1 -45.698566436767578
		 2 -46.553241729736328 3 -45.723167419433594 4 -43.842922210693359 5 -43.748699188232422
		 6 -45.467910766601563 7 -47.297794342041016 8 -51.954216003417969 9 -46.968925476074219
		 10 -32.462043762207031 11 7.3885965347290039 12 9.8699216842651367 13 15.515125274658201
		 14 16.997259140014648 15 16.630399703979492 16 16.545738220214844 17 16.840873718261719
		 18 16.205905914306641 19 -6.0157723426818848 20 -24.531356811523438 21 0.79067623615264893
		 22 -2.1006717681884766 23 -4.1514406204223633 24 -5.4407410621643066 25 -5.4407410621643066;
createNode animCurveTA -n "Character1_LeftLeg_rotateZ";
	rename -uid "B8A120D4-49BE-A760-448B-74AE71FD8B5F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -128.64268493652344 1 -123.86884307861327
		 2 -119.53004455566406 3 -124.77641296386719 4 -133.37432861328125 5 -134.3863525390625
		 6 -127.38565063476562 7 -111.60922241210937 8 -83.497390747070313 9 -53.685672760009766
		 10 -26.855716705322269 11 7.6959238052368173 12 6.1423177719116211 13 4.7441558837890625
		 14 5.2017850875854501 15 5.6979880332946777 16 5.7235283851623535 17 5.0406804084777832
		 18 3.9456892013549809 19 -10.885451316833496 20 -13.29387378692627 21 4.3881015777587891
		 22 3.8383493423461919 23 3.6197104454040527 24 3.5552210807800293 25 3.5552210807800293;
createNode animCurveTU -n "Character1_LeftLeg_visibility";
	rename -uid "404DD466-4122-A25E-835A-D3986EF20058";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFoot_translateX";
	rename -uid "5BA994BB-4A65-83CB-8C5E-BB80FBEB95AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 18.368806838989258;
createNode animCurveTL -n "Character1_LeftFoot_translateY";
	rename -uid "F3B0AE1F-40A5-D551-06C0-9E90093E7A30";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.7176852226257324;
createNode animCurveTL -n "Character1_LeftFoot_translateZ";
	rename -uid "7323E67B-4A67-86CF-F34D-02A055E10774";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3830562829971313;
createNode animCurveTU -n "Character1_LeftFoot_scaleX";
	rename -uid "ECF56EF1-40CD-D7FE-D821-2B9BB7F8D5B4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleY";
	rename -uid "33525357-4C45-D446-BC63-18BDE8FA2372";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleZ";
	rename -uid "FF3BD26A-46A1-D1F0-0302-058DB30A2E22";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftFoot_rotateX";
	rename -uid "BC99F61C-4C57-F762-F4F0-6AA0EF53503E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -6.8538055419921875 1 -7.6742706298828116
		 2 -8.7739419937133789 3 -10.015390396118164 4 -10.576190948486328 5 -6.8994121551513672
		 6 8.2604827880859375 7 48.051906585693359 8 61.923137664794922 9 20.79808235168457
		 10 -4.5948324203491211 11 -17.666728973388672 12 -27.535821914672852 13 -31.658718109130859
		 14 -33.377933502197266 15 -35.280113220214844 16 -34.726692199707031 17 -30.963174819946293
		 18 -29.148641586303711 19 -34.884517669677734 20 -36.925865173339844 21 -38.736583709716797
		 22 -38.621582031250007 23 -38.589000701904297 24 -38.628025054931641 25 -38.628025054931641;
createNode animCurveTA -n "Character1_LeftFoot_rotateY";
	rename -uid "5A94F578-4BC8-C339-A9E7-B8B9D6574F41";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -19.690277099609375 1 -15.023370742797852
		 2 -7.8241996765136728 3 0.11430432647466661 4 14.185551643371582 5 35.9344482421875
		 6 57.502182006835937 7 68.756988525390625 8 69.188499450683594 9 63.257228851318359
		 10 50.916694641113281 11 45.322128295898438 12 29.110254287719727 13 36.911853790283203
		 14 49.935443878173835 15 55.642456054687507 16 51.410541534423828 17 45.539188385009766
		 18 41.910919189453125 19 23.046464920043945 20 5.0372505187988281 21 12.947819709777832
		 22 11.937713623046875 23 11.410824775695801 24 11.158931732177734 25 11.158931732177734;
createNode animCurveTA -n "Character1_LeftFoot_rotateZ";
	rename -uid "AA6ED937-4A34-41F8-6C23-B6A4F780A16C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 1.0927051305770874 1 3.3355729579925537
		 2 6.8152227401733398 3 10.786896705627441 4 17.059200286865234 5 27.466693878173832
		 6 46.896156311035156 7 86.627281188964844 8 103.22145080566406 9 65.16339111328125
		 10 42.188228607177734 11 27.235761642456055 12 12.045984268188478 13 8.4072866439819336
		 14 5.5319695472717285 15 4.0390644073486328 16 3.4301826953887939 17 3.58934473991394
		 18 4.9075708389282227 19 6.8677926063537598 20 5.4289836883544922 21 7.2292332649230957
		 22 6.9984049797058105 23 6.9092373847961426 24 6.926830768585206 25 6.926830768585206;
createNode animCurveTU -n "Character1_LeftFoot_visibility";
	rename -uid "16F2E7B8-4893-8284-D5F7-E187FBEFB01B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftToeBase_translateX";
	rename -uid "CEB405B9-4073-E19E-CF27-C6A1610DD353";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3813290596008301;
createNode animCurveTL -n "Character1_LeftToeBase_translateY";
	rename -uid "86A3E638-4599-A462-6F20-2CAD3371361F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.3898367881774902;
createNode animCurveTL -n "Character1_LeftToeBase_translateZ";
	rename -uid "1B9D97EB-40F5-0953-CB62-71A1D064CA92";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2284431457519531;
createNode animCurveTU -n "Character1_LeftToeBase_scaleX";
	rename -uid "4B0DA55E-43D7-8136-BE2C-5AA9B2488049";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftToeBase_scaleY";
	rename -uid "59AE1C2B-4D8A-5AA6-9F74-259DC3987BC9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftToeBase_scaleZ";
	rename -uid "E67A4E79-49B4-6587-BB8B-738283E3C5DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftToeBase_rotateX";
	rename -uid "2A60BB01-4512-4982-FAEC-E79CAD89771E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8327609702394341e-010;
createNode animCurveTA -n "Character1_LeftToeBase_rotateY";
	rename -uid "E517ADEC-470F-B56A-18F9-51923C449B8F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.6956253068300953e-010;
createNode animCurveTA -n "Character1_LeftToeBase_rotateZ";
	rename -uid "347F23A5-4333-9358-382A-979136139B2C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.5336494254638637e-010;
createNode animCurveTU -n "Character1_LeftToeBase_visibility";
	rename -uid "AC8CB79C-452A-A080-51DD-5EA77EB01514";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateX";
	rename -uid "1C81B090-46FA-BAC7-39C2-9C9C1E4EAFDB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.1971817016601563;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateY";
	rename -uid "26435C7E-429E-B4F8-4E14-91905E6754A8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.7569773197174072;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateZ";
	rename -uid "1D3C6C69-4DC4-9455-152F-8F9C204A2A13";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4053930044174194;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleX";
	rename -uid "2C720D40-482B-2B57-26D6-AA9CC6ECA4B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleY";
	rename -uid "C622B878-48A5-4180-8723-42AFB06187AB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999958276748657;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleZ";
	rename -uid "F354B546-43BA-2221-5D66-D5ACD8CFB043";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateX";
	rename -uid "46AFA31A-473F-2BBE-3C70-068E981D4842";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.8674978471203758e-010;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateY";
	rename -uid "76869A12-4D67-E438-C622-2DAC32FCD58A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.2053043835512653e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateZ";
	rename -uid "E8A1DAB8-4F8E-9094-B532-A6B089F150DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.9006003982322568e-010;
createNode animCurveTU -n "Character1_LeftFootMiddle1_visibility";
	rename -uid "44E58B87-4AD5-CA8B-8F87-5FB9A34A2389";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateX";
	rename -uid "B83DE0D9-460B-90E5-8235-63802492F30A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8763875961303711;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateY";
	rename -uid "1BFE5A81-4D3B-47A2-DFAB-7DB73B9E163D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.1604456901550293;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateZ";
	rename -uid "8A27082B-4CB9-4991-D676-6480EB574ABE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.313550591468811;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleX";
	rename -uid "98CD98B5-45DC-AA3A-A53B-B9A3F2756E13";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleY";
	rename -uid "3A59BFA5-4081-5561-B8DF-C19C1A151443";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleZ";
	rename -uid "95C36FF8-4F82-8809-4458-68AFDE2F11C3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateX";
	rename -uid "48FF4C4A-41A7-44CE-B567-2187FAE01439";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.6406251346932099e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateY";
	rename -uid "40E6076B-415C-A2CD-B4A3-0A957C88B990";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.7736597463245971e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateZ";
	rename -uid "CE498FAA-4D54-A73A-9FC7-988496125F9D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 9.0398677432546027e-010;
createNode animCurveTU -n "Character1_LeftFootMiddle2_visibility";
	rename -uid "02A3CAC1-4C81-0E67-6267-D381444A25CE";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightUpLeg_translateX";
	rename -uid "0CF00348-46C9-86B9-345E-CC817ECC8403";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  7 -9.4649467468261719 8 -9.4649467468261719
		 9 -5.5646395683288574 10 -1.3372197151184082 11 2.7266433238983154 12 6.1362814903259277
		 13 8.4010276794433594 14 9.1830425262451172 15 8.8094425201416016 16 7.7869753837585449
		 17 6.6224040985107422 18 5.8224773406982422 19 5.4104490280151367 20 5.0507845878601074
		 21 4.7120661735534668 22 4.3628721237182617 23 4.3628721237182617;
createNode animCurveTL -n "Character1_RightUpLeg_translateY";
	rename -uid "6561D2AB-40BD-E31F-4E32-9CB99E1149A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  7 -5.110023021697998 8 -5.110023021697998
		 9 -15.797911643981934 10 -27.270162582397461 11 -38.350231170654297 12 -47.861572265625
		 13 -54.627655029296875 14 -57.822376251220703 15 -58.230110168457031 16 -57.089969635009766
		 17 -55.641105651855469 18 -55.122631072998047 19 -55.625007629394531 20 -56.331108093261719
		 21 -57.118698120117188 22 -57.86553955078125 23 -57.86553955078125;
createNode animCurveTL -n "Character1_RightUpLeg_translateZ";
	rename -uid "39454B45-4395-95E2-9F6E-60A85A45308E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  7 3.4359440803527832 8 3.4359440803527832
		 9 7.3794689178466797 10 11.542235374450684 11 15.595381736755371 12 19.210042953491211
		 13 22.057361602783203 14 24.126611709594727 15 25.637039184570313 16 26.604721069335938
		 17 27.045743942260742 18 26.976186752319336 19 25.943988800048828 20 23.89362907409668
		 21 21.436000823974609 22 19.182008743286133 23 19.182008743286133;
createNode animCurveTU -n "Character1_RightUpLeg_scaleX";
	rename -uid "6161B8A6-4C7D-0A8F-5889-B0A448D2F265";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightUpLeg_scaleY";
	rename -uid "1E1D2DCF-45F5-6F4E-688A-2584E21EF4A0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightUpLeg_scaleZ";
	rename -uid "782878CB-4DC2-440B-F0E4-418759B25AA6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightUpLeg_rotateX";
	rename -uid "824FCE39-4083-C7E4-33D2-29ADF4112AC5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -4.6803326606750488 1 -14.402938842773436
		 2 -27.671672821044922 3 -40.222030639648437 4 -54.437309265136719 5 -57.655677795410156
		 6 -60.277847290039055 7 -49.133407592773438 8 -21.059577941894531 9 -2.0165159702301025
		 10 14.840254783630373 11 29.380832672119141 12 43.603610992431641 13 77.861236572265639
		 14 91.1033935546875 15 109.82417297363283 16 117.53154754638673 17 121.42364501953124
		 18 122.33375549316408 19 139.33253479003906 20 145.49664306640625 21 137.51060485839844
		 22 111.23926544189453 23 74.859443664550781 24 44.389804840087891 25 29.384622573852536
		 26 29.384622573852536;
createNode animCurveTA -n "Character1_RightUpLeg_rotateY";
	rename -uid "70E9B786-4353-A308-D84D-5DB43FC6CF59";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -0.82046854496002197 1 -0.56153136491775513
		 2 0.094945505261421204 3 1.2965024709701538 4 3.4182491302490234 5 4.440272331237793
		 6 6.0182285308837891 7 5.6933212280273438 8 8.2216196060180664 9 18.386590957641602
		 10 16.227527618408207 11 8.6702890396118164 12 5.052300453186036 13 8.4057674407958984
		 14 -3.4259862899780273 15 -12.529477119445801 16 -14.286032676696779 17 -12.489004135131836
		 18 -10.410787582397461 19 -6.8128328323364267 20 -7.2951536178588858 21 -10.20842456817627
		 22 -9.0497961044311523 23 5.01580810546875 24 33.468715667724609 25 59.389907836914055
		 26 59.389907836914055;
createNode animCurveTA -n "Character1_RightUpLeg_rotateZ";
	rename -uid "130D51D8-4FA3-25C3-2EF1-7D8198E3CEDE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 16.957609176635742 1 16.667112350463871
		 2 17.831914901733398 3 19.201925277709961 4 20.62648963928223 5 22.07318115234375
		 6 22.226129531860352 7 19.525690078735352 8 4.7436075210571289 9 -4.1045393943786621
		 10 -1.8351165056228638 11 0.36474642157554626 12 0.1964761167764664 13 5.9202065467834473
		 14 17.498907089233402 15 15.529624938964844 16 6.0168190002441406 17 -5.7213449478149414
		 18 -15.444446563720705 19 -20.921018600463867 20 -19.099369049072266 21 -12.279953002929687
		 22 -0.32111898064613342 23 8.5449275970458984 24 9.684962272644043 25 11.039042472839355
		 26 11.039042472839355;
createNode animCurveTU -n "Character1_RightUpLeg_visibility";
	rename -uid "92E24E3B-48BA-4391-8DCC-32B3E26399AE";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightLeg_translateX";
	rename -uid "2733EDE1-4CA9-1728-8188-12872700F56A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3108952045440674;
createNode animCurveTL -n "Character1_RightLeg_translateY";
	rename -uid "8699AE17-4CF0-9158-39CB-2691DBA05545";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 22.181583404541016;
createNode animCurveTL -n "Character1_RightLeg_translateZ";
	rename -uid "43E1C410-40C2-8E08-E4BB-3EA18A0D46E6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.6536058187484741;
createNode animCurveTU -n "Character1_RightLeg_scaleX";
	rename -uid "0EFFAC20-4791-543D-73C3-149D95D57340";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightLeg_scaleY";
	rename -uid "3E704178-46FC-3F5D-7E03-238824DD1D6F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightLeg_scaleZ";
	rename -uid "E4F32157-4EB2-E222-6D5D-44ACD9DC6B63";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightLeg_rotateX";
	rename -uid "DC64D85F-413D-54CF-DF94-599D6E440BA6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 91.156593322753906 1 45.9168701171875
		 2 19.091926574707031 3 11.085421562194826 4 9.9504146575927734 5 11.097352027893066
		 6 11.42660427093506 7 10.905328750610352 8 18.50068473815918 9 18.456638336181641
		 10 23.201803207397461 11 23.828239440917969 12 22.243301391601566 13 22.619977951049805
		 14 -12.09940242767334 15 -15.037403106689453 16 -11.426488876342773 17 -6.103053092956543
		 18 -3.6055619716644287 19 12.549180030822754 20 32.740203857421875 21 63.774158477783196
		 22 148.18106079101562 23 186.92091369628906 24 202.42881774902344 25 191.40843200683594
		 26 191.40843200683594;
createNode animCurveTA -n "Character1_RightLeg_rotateY";
	rename -uid "DC95C234-4D9C-F120-6BA4-7D88EC280437";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 65.604209899902344 1 60.015464782714851
		 2 39.642654418945313 3 17.238813400268555 4 -7.2347803115844727 5 -6.6203489303588867
		 6 -6.7453899383544922 7 13.158978462219238 8 37.771232604980469 9 39.179176330566406
		 10 45.595405578613281 11 43.068840026855469 12 39.291488647460945 13 40.187999725341797
		 14 -12.118395805358887 15 -11.624686241149902 16 -12.207547187805176 17 -12.56916332244873
		 18 -12.268611907958984 19 26.3128776550293 20 52.868453979492188 21 74.588699340820312
		 22 84.711204528808608 23 92.055450439453125 24 123.28137207031249 25 158.40287780761719
		 26 158.40287780761719;
createNode animCurveTA -n "Character1_RightLeg_rotateZ";
	rename -uid "6E4BC5DD-4774-0823-D6F0-71AD486B83DD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 82.2694091796875 1 35.956443786621094
		 2 5.487858772277832 3 -8.0890350341796875 4 -18.463903427124023 5 -19.795221328735352
		 6 -20.266599655151367 7 -11.091896057128906 8 6.7786335945129395 9 3.3476665019989014
		 10 10.306709289550781 11 15.873572349548338 12 18.273521423339844 13 17.589117050170898
		 14 5.6909041404724121 15 9.0405178070068359 16 4.9243941307067871 17 -1.1126868724822998
		 18 -3.7831246852874756 19 7.0829391479492188 20 22.581546783447266 21 46.697536468505859
		 22 125.66676330566408 23 160.2895202636719 24 179.22288513183594 25 176.95635986328125
		 26 176.95635986328125;
createNode animCurveTU -n "Character1_RightLeg_visibility";
	rename -uid "92BBB6C3-4DF0-962E-05F7-C7B6A9D88B64";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFoot_translateX";
	rename -uid "19DD8BB1-4211-AE22-943E-52AA672F4FB3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.668137550354004;
createNode animCurveTL -n "Character1_RightFoot_translateY";
	rename -uid "77A8BE2E-4505-9A20-7B3C-1C9856B4DD2E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3660926818847656;
createNode animCurveTL -n "Character1_RightFoot_translateZ";
	rename -uid "B2672329-4DA9-E2AD-376F-ED90D0B1FDA7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -15.681424140930176;
createNode animCurveTU -n "Character1_RightFoot_scaleX";
	rename -uid "8A7067DE-4A03-96D7-EB61-208493E04D3B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightFoot_scaleY";
	rename -uid "C5100703-426B-68A8-389A-B2A5021A26FB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999958276748657;
createNode animCurveTU -n "Character1_RightFoot_scaleZ";
	rename -uid "993629A8-4CF4-5569-CAB8-4EB723FA3BB4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFoot_rotateX";
	rename -uid "5A437CE5-4811-DDCD-13AA-4A8CC0D2D154";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -26.836753845214844 1 -19.253946304321289
		 2 -10.286612510681152 3 -0.16914741694927216 4 16.140850067138672 5 24.549673080444336
		 6 29.144342422485348 7 18.848211288452148 8 -8.9291172027587891 9 -18.878078460693359
		 10 -13.353745460510254 11 1.1594983339309692 12 14.175136566162109 13 39.182651519775391
		 14 72.038337707519531 15 80.350143432617188 16 74.713470458984375 17 62.444919586181641
		 18 49.116302490234375 19 33.495967864990234 20 27.815773010253906 21 20.245515823364258
		 22 2.447932243347168 23 -14.403616905212402 24 -7.6593747138977051 25 9.6884441375732422
		 26 9.6884441375732422;
createNode animCurveTA -n "Character1_RightFoot_rotateY";
	rename -uid "A85474FC-4F1E-793E-A3F3-96BFB2E29D1C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -8.1331624984741211 1 -5.2337188720703125
		 2 -1.8080545663833616 3 0.68561208248138428 4 -0.3915688693523407 5 -3.5561909675598145
		 6 -7.2695212364196777 7 -2.6695659160614014 8 4.0851616859436035 9 3.9279482364654541
		 10 -0.8011818528175354 11 -2.255457878112793 12 -0.18241347372531891 13 -0.22209565341472626
		 14 -27.425745010375977 15 -32.775775909423828 16 -28.15545654296875 17 -19.680221557617188
		 18 -13.356968879699707 19 -4.5976619720458993 20 -1.5500919818878176 21 1.0137193202972412
		 22 -3.7647235393524165 23 -12.035550117492676 24 -9.1741476058959961 25 -1.1205325126647949
		 26 -1.1205325126647949;
createNode animCurveTA -n "Character1_RightFoot_rotateZ";
	rename -uid "E8E38F1A-419C-FDD4-F536-168CF2690AF4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -22.650707244873047 1 -14.985782623291014
		 2 -4.7545084953308105 3 7.4110145568847656 4 27.315317153930668 5 40.854835510253906
		 6 52.179740905761719 7 49.460391998291016 8 26.517887115478516 9 9.9138727188110352
		 10 0.97606188058853149 11 2.8178088665008545 12 8.5973110198974609 13 17.442811965942383
		 14 50.659065246582031 15 48.192279815673828 16 49.382331848144531 17 50.761539459228516
		 18 48.943088531494141 19 24.962860107421875 20 4.0578374862670898 21 -17.970121383666992
		 22 -37.487747192382812 23 -44.299022674560547 24 -43.438133239746094 25 -30.810724258422852
		 26 -30.810724258422852;
createNode animCurveTU -n "Character1_RightFoot_visibility";
	rename -uid "64E13F6C-4000-85CC-F7AD-458A5E165A4A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightToeBase_translateX";
	rename -uid "F19DABFD-4609-7EDC-4FF5-1A8FF5E00253";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.103175163269043;
createNode animCurveTL -n "Character1_RightToeBase_translateY";
	rename -uid "87B3EDFB-4AD7-8AAA-4C94-A086EA3429EB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.7509346008300781;
createNode animCurveTL -n "Character1_RightToeBase_translateZ";
	rename -uid "5D1AE7C6-48A4-BCD3-4542-6A92297C17B6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.6768350601196289;
createNode animCurveTU -n "Character1_RightToeBase_scaleX";
	rename -uid "25E6ECD9-4953-DB4A-9055-86B96AF4EEE3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightToeBase_scaleY";
	rename -uid "FF57D2D7-4F70-5415-BC4B-6395179BF221";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightToeBase_scaleZ";
	rename -uid "961DC87C-40F5-AB51-6ECB-95923D85DF81";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightToeBase_rotateX";
	rename -uid "F54DADBB-49B6-1E13-8FF0-3BB8F91B042E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 9.8169405937653664e-010;
createNode animCurveTA -n "Character1_RightToeBase_rotateY";
	rename -uid "1767E7E9-4F16-6AAB-4484-4DBAF3DA998D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.4550408634668202e-010;
createNode animCurveTA -n "Character1_RightToeBase_rotateZ";
	rename -uid "3EAFACA8-4A17-4637-4EF9-669B5983E2BE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.2934347743717183e-010;
createNode animCurveTU -n "Character1_RightToeBase_visibility";
	rename -uid "A18300AA-4400-9CBD-603A-09B3837813F8";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateX";
	rename -uid "DFD24E22-458A-D9B9-ECD9-D48CAACE4EC5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.31789970397949219;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateY";
	rename -uid "E9605A12-46DD-E299-F21B-7C92926F791C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.6937241554260254;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateZ";
	rename -uid "3751E602-45AD-6B4A-4BF9-F8BC8D4E0059";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.4678492546081543;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleX";
	rename -uid "68DCAF3D-48DD-B015-0ABB-7CB4A4D886FD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleY";
	rename -uid "B956F9B5-4D4D-8378-9881-EA8ECF3891A8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleZ";
	rename -uid "84CE17AA-49FC-6670-A62C-B9B0A2020314";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateX";
	rename -uid "EA0023B1-47AE-44AB-F7BB-7A8C686C6DB8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.014258232956422e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateY";
	rename -uid "BE4C82DF-407A-1D4E-76BF-7192773FF906";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.7046165307353931e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateZ";
	rename -uid "925A9A12-4B04-7E3C-FD82-DCB9D72D71CC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8631777211104463e-010;
createNode animCurveTU -n "Character1_RightFootMiddle1_visibility";
	rename -uid "792D45A2-4F98-5BE3-33E5-898C3290F8FB";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateX";
	rename -uid "C6E5A36D-422C-9C22-5E1C-9DADBC72C75C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3449039459228516;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateY";
	rename -uid "8DCA7D69-4BDA-F529-DAC1-279C4C2EC95A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3579239845275879;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateZ";
	rename -uid "795C3B0E-445E-879D-555B-5B91CF591B2C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.7014765739440918;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleX";
	rename -uid "1E0933CE-4136-C846-4405-139536CE4583";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleY";
	rename -uid "ED4B6122-4099-9154-F076-F19AEE04F2B2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleZ";
	rename -uid "3FF8682A-4571-1E6B-7A41-099121A5F75D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateX";
	rename -uid "594B9D67-4797-3A5F-315C-0EAB6A237A43";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.7610618236195705e-009;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateY";
	rename -uid "B57D078E-445C-2D66-A200-ACBC9F70DD67";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.4609239090978008e-009;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateZ";
	rename -uid "E818A699-422B-9E9F-AC87-EC80E2938477";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.2805128330769548e-009;
createNode animCurveTU -n "Character1_RightFootMiddle2_visibility";
	rename -uid "52E78EBB-4E5B-5B3C-F1CD-C9AD4C1F33A6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Spine_translateX";
	rename -uid "CBAC8684-4FDF-2CB0-EFE2-41BA92EBA20A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.9174466133117676;
createNode animCurveTL -n "Character1_Spine_translateY";
	rename -uid "5BC3517F-45EF-4B8D-D553-3DB0B9145F2A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.894325256347656;
createNode animCurveTL -n "Character1_Spine_translateZ";
	rename -uid "F16B8CB1-40C1-C189-E546-26897E125387";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1938900947570801;
createNode animCurveTU -n "Character1_Spine_scaleX";
	rename -uid "1C6DFA9F-436D-4528-3B4C-2DA9FFA8AE2E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine_scaleY";
	rename -uid "CFB4AE32-4FDB-7428-9C86-F28FE4CC94B3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine_scaleZ";
	rename -uid "6B5CBE25-41A7-80C5-FA9D-63AADC048CE7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_Spine_rotateX";
	rename -uid "FD2D9980-40FC-DBB8-D32E-D0803A6F194D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -38.819557189941406 1 -23.933712005615234
		 2 -8.8393096923828125 3 2.6818463802337646 4 10.911376953125002 5 17.653415679931641
		 6 21.345159530639652 7 20.305431365966797 8 12.62926197052002 9 -0.062889210879802704
		 10 -14.21611213684082 11 -26.375543594360352 12 -33.0736083984375 13 -28.620639801025394
		 14 -15.70712471008301 15 -2.6821982860565186 16 9.545438766479494 17 23.045900344848633
		 18 34.604183197021484 19 41.068134307861328 20 41.642757415771484 21 38.616600036621094
		 22 33.492336273193359 23 27.803482055664066 24 23.121421813964847 25 23.121421813964847;
createNode animCurveTA -n "Character1_Spine_rotateY";
	rename -uid "AA2FD553-4CE1-60B9-6C1F-4DA46CDDE75E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 5.5474224090576172 1 3.8677947521209712
		 2 3.8975694179534912 3 5.5639452934265146 4 8.1556901931762695 5 11.089896202087402
		 6 13.51085376739502 7 14.779559135437012 8 15.127033233642578 9 15.790016174316406
		 10 17.435367584228519 11 19.700164794921875 12 21.41602897644043 13 20.212255477905273
		 14 16.421787261962891 15 13.283907890319824 16 11.528860092163086 17 9.9043750762939453
		 18 8.4769496917724609 19 7.6999650001525888 20 7.8358311653137216 21 8.4951295852661133
		 22 9.304896354675293 23 9.9773359298706055 24 10.375760078430176 25 10.375760078430176;
createNode animCurveTA -n "Character1_Spine_rotateZ";
	rename -uid "6802B5E9-4C8C-A2B7-C4BB-E8AE8D392E3B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 2.7765178680419922 1 -0.77412337064743042
		 2 -4.3564367294311523 3 -6.6253824234008789 4 -7.6207060813903809 5 -7.7200098037719735
		 6 -6.987856388092041 7 -5.8535656929016113 8 -4.4788312911987305 9 -2.8904886245727539
		 10 -1.4766976833343506 11 -0.64424264430999756 12 -0.085546277463436127 13 1.4695650339126587
		 14 3.0241315364837646 15 3.5565519332885747 16 4.2750473022460937 17 5.1591649055480957
		 18 5.8449068069458008 19 5.929962158203125 20 5.3921098709106445 21 4.5221991539001465
		 22 3.3983979225158691 23 2.1604907512664795 24 1.0063120126724245 25 1.0063120126724245;
createNode animCurveTU -n "Character1_Spine_visibility";
	rename -uid "A5E4F0A3-4195-0D52-29E2-B5824E733283";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Spine1_translateX";
	rename -uid "E04C45F3-4B86-8415-40C9-859DA8B9C834";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2515636682510376;
createNode animCurveTL -n "Character1_Spine1_translateY";
	rename -uid "7E242D35-42B4-0371-53EE-DFA246DF6D8B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -15.818062782287598;
createNode animCurveTL -n "Character1_Spine1_translateZ";
	rename -uid "CA2CEBBD-4BC9-C792-DB54-A8B4FF21C853";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.5716743469238281;
createNode animCurveTU -n "Character1_Spine1_scaleX";
	rename -uid "6BB2D227-484D-7670-C5DD-C5A0FF91D709";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine1_scaleY";
	rename -uid "3FB8E840-4AD4-B77F-9913-39A2A5C1C639";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine1_scaleZ";
	rename -uid "B8695E64-4087-860A-ABC8-0688F810C5FA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_Spine1_rotateX";
	rename -uid "F39C3CE8-418E-DF76-8668-0DBD48B8A01E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -15.990665435791016 1 -5.3386974334716797
		 2 5.7472476959228525 3 12.914242744445801 4 15.245910644531248 5 15.119235038757324
		 6 13.456348419189453 7 11.173237800598145 8 7.7509679794311523 9 2.9111788272857666
		 10 -2.0732066631317139 11 -5.9614534378051758 12 -7.5139822959899902 13 -2.5583341121673588
		 14 6.671933650970459 15 11.735238075256348 16 8.8921384811401367 17 2.2623598575592045
		 18 -4.5099687576293945 19 -7.5851492881774902 20 -7.5851492881774902;
createNode animCurveTA -n "Character1_Spine1_rotateY";
	rename -uid "571AE7F2-4714-622A-037B-77A5C0310E2A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 3.9192204475402832 1 0.8648226261138916
		 2 0.33858820796012878 3 1.5147712230682373 4 2.6156291961669922 5 3.3124516010284424
		 6 3.3986670970916748 7 2.7359359264373784 8 1.1927376985549927 9 -1.0133557319641113
		 10 -3.5468642711639404 11 -6.0881853103637695 12 -8.3969841003417969 13 -10.619672775268555
		 14 -12.610629081726074 15 -14.426620483398438 16 -16.83647346496582 17 -18.955680847167969
		 18 -19.812507629394531 19 -19.863803863525391 20 -19.863803863525391;
createNode animCurveTA -n "Character1_Spine1_rotateZ";
	rename -uid "B6302F74-4266-DE05-60B4-349A70A3DB06";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 16.748464584350586 1 4.6421499252319336
		 2 -7.7747840881347647 3 -15.993885040283205 4 -18.903999328613281 5 -19.100591659545898
		 6 -18.010869979858398 7 -16.959827423095703 8 -15.755875587463379 9 -13.623470306396484
		 10 -11.028295516967773 11 -8.6424789428710937 12 -7.392871379852294 13 -10.410857200622559
		 14 -16.661169052124023 15 -19.774286270141605 16 -15.734110832214354 17 -7.77248239517212
		 18 0.19386056065559387 19 3.7986054420471191 20 3.7986054420471191;
createNode animCurveTU -n "Character1_Spine1_visibility";
	rename -uid "6D160AAB-44FA-CA1E-E586-6E81D0FEC770";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftShoulder_translateX";
	rename -uid "F7C40224-40E5-E728-50C3-22A740A5C8E4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.49134865403175354;
createNode animCurveTL -n "Character1_LeftShoulder_translateY";
	rename -uid "E4F613BB-458A-D779-0EEF-CBBE5E0DCE11";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.20281982421875;
createNode animCurveTL -n "Character1_LeftShoulder_translateZ";
	rename -uid "47206181-4B94-9909-800C-37A4E418F084";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.910428047180176;
createNode animCurveTU -n "Character1_LeftShoulder_scaleX";
	rename -uid "A624E3D9-41FD-BABD-2F12-75B6279FC1EA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftShoulder_scaleY";
	rename -uid "E19145A3-4D17-0C56-17F7-8C8083B8625F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999934434890747;
createNode animCurveTU -n "Character1_LeftShoulder_scaleZ";
	rename -uid "0C85FD11-4CC0-838A-4167-2D91B355D18E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftShoulder_rotateX";
	rename -uid "614D2277-4E85-EF29-D554-19A68A2B43AC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -11.818500518798828 1 -13.289626121520996
		 2 -14.847229957580566 3 -16.417606353759766 4 -17.934358596801758 5 -19.328468322753906
		 6 -20.51708984375 7 -21.392826080322266 8 -20.943933486938477 9 -19.098196029663086
		 10 -17.183294296264648 11 -15.521765708923338 12 -13.77010440826416 13 -12.038908004760742
		 14 -10.314883232116699 15 -8.3569612503051758 16 -6.1750774383544922 17 -4.166750431060791
		 18 -2.7002279758453374 19 -2.1314156055450439 20 -2.1314156055450439;
createNode animCurveTA -n "Character1_LeftShoulder_rotateY";
	rename -uid "BF399719-4FA5-F282-8643-0F8424BC1D93";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 8.5785551071166992 1 13.692377090454102
		 2 19.054679870605469 3 24.497814178466797 4 29.848310470581051 5 34.929153442382813
		 6 39.561759948730469 7 43.567447662353516 8 45.509136199951172 9 45.247562408447266
		 10 44.577384948730469 11 43.693264007568366 12 42.326812744140625 13 41.021053314208984
		 14 39.642406463623047 15 38.027309417724609 16 36.163051605224609 17 34.391208648681641
		 18 33.065395355224609 19 32.544162750244141 20 32.544162750244141;
createNode animCurveTA -n "Character1_LeftShoulder_rotateZ";
	rename -uid "006DF087-4874-BFA4-B335-BBA11766565E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -17.762969970703125 1 -16.608592987060547
		 2 -15.642147064208984 3 -14.846786499023438 4 -14.183245658874512 5 -13.588985443115234
		 6 -12.975634574890137 7 -12.226037979125977 8 -10.544594764709473 9 -7.8857049942016602
		 10 -5.3067479133605957 11 -3.2131016254425049 12 -1.2380369901657104 13 0.75269490480422974
		 14 2.7004134654998779 15 4.8952469825744629 16 7.3153715133666983 17 9.5187845230102539
		 18 11.112712860107422 19 11.727431297302246 20 11.727431297302246;
createNode animCurveTU -n "Character1_LeftShoulder_visibility";
	rename -uid "6B8D7C2E-4FE6-4E7F-08C0-7B83710E85F7";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftArm_translateX";
	rename -uid "61F72D69-4FDF-DA31-BBCA-6E9B9D4682DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -10.39077091217041 1 -15.122026443481445
		 2 -20.075899124145508 3 -24.918462753295898 4 -29.315792083740234 5 -32.933956146240234
		 6 -35.826488494873047 7 -38.216007232666016 8 -40.022212982177734 9 -41.164817810058594
		 10 -41.563529968261719 11 -41.563529968261719 18 -41.563529968261719 19 -41.563529968261719
		 20 -41.614501953125 21 -41.748878479003906 22 -41.938861846923828 23 -42.156646728515625
		 24 -42.374431610107422 25 -42.564414978027344 26 -42.564414978027344;
createNode animCurveTL -n "Character1_LeftArm_translateY";
	rename -uid "4EB61370-427C-918F-C8AA-949F82F4B997";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 -5.8805270195007324 1 -5.0811600685119629
		 2 -4.1700682640075684 3 -3.3148388862609863 4 -2.6830599308013916 5 -2.4423191547393799
		 6 -2.8177695274353027 7 -3.6976885795593262 8 -4.7443432807922363 9 -5.6200037002563477
		 10 -5.9869422912597656 11 -5.9869422912597656 18 -5.9869422912597656 19 -5.9869422912597656
		 20 -6.0294175148010254 21 -6.1413969993591309 22 -6.2997112274169922 23 -6.4811935424804687
		 24 -6.6626772880554199 25 -6.8209934234619141 26 -6.8209934234619141;
createNode animCurveTL -n "Character1_LeftArm_translateZ";
	rename -uid "6B9A494C-4B1A-B89B-CBB9-A48E8109E3C4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  0 2.8116312026977539 1 -1.7051790952682495
		 2 -6.7419261932373047 3 -11.518704414367676 4 -15.255609512329102 5 -17.172735214233398
		 6 -16.349769592285156 7 -13.306646347045898 8 -9.4238405227661133 9 -6.0818142890930176
		 10 -4.661034107208252 11 -4.661034107208252 18 -4.661034107208252 19 -4.661034107208252
		 20 -4.5661506652832031 21 -4.3160057067871094 22 -3.9623532295227051 23 -3.5569448471069336
		 24 -3.1515367031097412 25 -2.7978837490081787 26 -2.7978837490081787;
createNode animCurveTU -n "Character1_LeftArm_scaleX";
	rename -uid "D634E4EE-40CD-A1DD-1F63-F69135D586C2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999964237213135;
createNode animCurveTU -n "Character1_LeftArm_scaleY";
	rename -uid "D657939D-49B1-CF9D-AFED-DB9A55B9DA95";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleZ";
	rename -uid "A23B1B9B-433B-4DC8-6501-DBA305905EFF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_LeftArm_rotateX";
	rename -uid "CBE18676-41C4-6FD6-C204-B99BACA8410C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -13.350896835327148 1 -8.2570743560791033
		 2 -0.72806501388549805 3 7.7624735832214355 4 15.380401611328127 5 20.979085922241211
		 6 24.313102722167972 7 25.597314834594727 8 22.899745941162109 9 17.060602188110352
		 10 12.059726715087891 11 9.6840381622314453 12 3.0919375419616699 13 -11.389572143554688
		 14 -21.72900390625 15 -22.183343887329102 16 -20.10017204284668 17 -16.68359375 18 -13.428012847900391
		 19 -11.749860763549805 20 -12.441424369812013 21 -14.562108039855957 22 -16.741828918457031
		 23 -18.552055358886719 24 -20.433769226074219 25 -20.433769226074219;
createNode animCurveTA -n "Character1_LeftArm_rotateY";
	rename -uid "5316AA81-41C9-5743-D1ED-A68DB0D861D2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 24.852851867675781 1 28.500165939331055
		 2 29.918481826782227 3 28.279581069946293 4 23.639848709106445 5 16.853734970092773
		 6 9.0857524871826172 7 1.436156153678894 8 -3.4886889457702641 9 -4.0651559829711914
		 10 -1.7512416839599609 11 4.6854963302612305 12 10.194238662719727 13 11.766140937805178
		 14 10.662257194519043 15 11.08910083770752 16 11.706197738647461 17 11.552229881286621
		 18 10.460141181945801 19 9.2966318130493164 20 9.3591699600219727 21 10.547019004821777
		 22 12.011205673217773 23 13.893708229064941 24 16.057090759277344 25 16.057090759277344;
createNode animCurveTA -n "Character1_LeftArm_rotateZ";
	rename -uid "E0F2DF41-471C-EB22-0851-419990868F26";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -62.170162200927727 1 -45.688007354736328
		 2 -27.35948371887207 3 -8.7541532516479492 4 8.2890520095825195 5 22.650703430175781
		 6 34.09709548950196 7 42.801422119140625 8 46.976882934570313 9 45.705738067626953
		 10 39.988491058349609 11 23.498958587646484 12 0.82770705223083496 13 -21.049959182739258
		 14 -33.314785003662109 15 -29.700700759887695 16 -21.586156845092773 17 -11.468683242797853
		 18 -1.9597014188766482 19 4.4992966651916504 20 5.9600715637207031 21 3.9889512062072754
		 22 1.6081582307815552 23 -0.56109076738357544 24 -3.2291786670684814 25 -3.2291786670684814;
createNode animCurveTU -n "Character1_LeftArm_visibility";
	rename -uid "3F9EBEEE-444C-C61C-C746-60B70E757F3F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftForeArm_translateX";
	rename -uid "A958C42F-4304-2EFC-D702-789FD80E0CC5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.9387187957763672;
createNode animCurveTL -n "Character1_LeftForeArm_translateY";
	rename -uid "51E85FD7-495A-E2A3-76FF-65B26ED8693B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -14.273021697998047;
createNode animCurveTL -n "Character1_LeftForeArm_translateZ";
	rename -uid "7A79C77D-49C5-CB9C-C633-7B9E6E97435D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.7470760345458984;
createNode animCurveTU -n "Character1_LeftForeArm_scaleX";
	rename -uid "0D620B6F-49CB-1E81-B631-5E85412C7346";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftForeArm_scaleY";
	rename -uid "2D53A0F4-4B17-81D4-B849-CA8AB474FF66";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftForeArm_scaleZ";
	rename -uid "5BCFF4C9-406A-F0AA-C241-5CBBD74F88B4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftForeArm_rotateX";
	rename -uid "DC15E030-4069-D112-9B7E-6E81D26340FD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 24 ".ktv[0:23]"  0 -23.619070053100589 1 -23.290826797485352
		 2 -22.869325637817383 3 -22.422082901000977 4 -22.018268585205078 5 -21.728725433349609
		 6 -21.625177383422852 7 -21.778543472290039 8 -17.364013671875 9 -8.1420478820800781
		 10 -0.29692533612251282 11 7.5797090530395499 12 11.263251304626465 13 -1.4754116535186768
		 14 -12.415701866149902 15 -14.267888069152832 16 -14.087010383605957 17 -12.907717704772949
		 18 -11.630435943603516 19 -10.798605918884277 20 -10.594394683837891 21 -10.734895706176758
		 22 -10.862906455993652 23 -10.862906455993652;
createNode animCurveTA -n "Character1_LeftForeArm_rotateY";
	rename -uid "93737EBC-4003-6601-0BA5-52921C41352A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 24 ".ktv[0:23]"  0 -35.682445526123047 1 -35.016727447509773
		 2 -34.168552398681641 3 -33.27691650390625 4 -32.479331970214844 5 -31.911869049072269
		 6 -31.709831237792969 7 -32.009243011474609 8 -30.887475967407227 9 -26.408153533935547
		 10 -21.111392974853516 11 -22.06439208984375 12 -18.84181022644043 13 2.8116283416748047
		 14 23.362503051757813 15 27.798263549804688 16 28.97269439697266 17 28.005464553833008
		 18 26.127191543579102 19 24.691179275512695 20 24.383325576782227 21 24.59596061706543
		 22 24.786539077758793 23 24.786539077758793;
createNode animCurveTA -n "Character1_LeftForeArm_rotateZ";
	rename -uid "C27C8014-44C4-E17B-9291-05904117F5C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 24 ".ktv[0:23]"  0 -22.501960754394531 1 -22.203737258911136
		 2 -21.819614410400391 3 -21.410667419433597 4 -21.040277481079102 5 -20.77406120300293
		 6 -20.678730010986328 7 -20.819904327392578 8 -30.837402343750004 9 -50.987869262695313
		 10 -68.45928955078125 11 -82.99359130859375 12 -89.71575927734375 13 -77.781021118164077
		 14 -68.602142333984375 15 -64.784225463867188 16 -60.997604370117195 17 -57.571537017822266
		 18 -54.86279296875 19 -53.023998260498047 20 -52.451343536376953 21 -52.846225738525391
		 22 -53.202625274658203 23 -53.202625274658203;
createNode animCurveTU -n "Character1_LeftForeArm_visibility";
	rename -uid "95F67F7E-4B6D-E443-E05D-F7A756ECC191";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHand_translateX";
	rename -uid "FC40FF2C-49E9-0975-0853-55AAC30C93F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.95125424861907959;
createNode animCurveTL -n "Character1_LeftHand_translateY";
	rename -uid "6E82B3B2-4103-73FC-5CBA-6F91F50AD34C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.299518585205078;
createNode animCurveTL -n "Character1_LeftHand_translateZ";
	rename -uid "96B709B7-4F7A-F96F-BBAE-A89D6EB89FA5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 12.872089385986328;
createNode animCurveTU -n "Character1_LeftHand_scaleX";
	rename -uid "012CC4EE-459B-9600-CAFF-3F98F2FECA1B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHand_scaleY";
	rename -uid "5065409A-4CF3-7334-6C38-1F974D649C73";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHand_scaleZ";
	rename -uid "FC1D3C95-4CA5-A13E-D624-23B92F6F9871";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHand_rotateX";
	rename -uid "D74BFFE5-44B7-FD10-42E8-ACA4D7EAB500";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 18 ".ktv[0:17]"  6 1.0124522553667248e-008 7 1.1745709294075368e-008
		 8 -7.5484843254089347 9 -20.210903167724609 10 -22.78782844543457 11 9.4131317138671875
		 12 40.849754333496094 13 22.625852584838867 14 -3.3224534988403325 15 -14.709053039550783
		 16 -25.308387756347656 17 -33.722991943359382 18 -38.606571197509766 19 -38.619380950927734
		 20 -22.312856674194336 21 5.5799808502197266 22 20.456565856933594 23 20.456565856933594;
createNode animCurveTA -n "Character1_LeftHand_rotateY";
	rename -uid "B1077C58-4CBD-059A-9416-87892EA66048";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 18 ".ktv[0:17]"  6 6.0401511291274808e-005 7 6.0401158407330506e-005
		 8 1.0718307495117187 9 2.5410583019256592 10 2.5182671546936035 11 1.8589189052581787
		 12 2.63570237159729 13 1.3080048561096191 14 1.3079110383987427 15 1.8620655536651614
		 16 2.6708500385284424 17 3.5345377922058105 18 4.1142358779907227 19 4.0509476661682129
		 20 1.8952285051345825 21 0.023118194192647934 22 -0.78973889350891113 23 -0.78973889350891113;
createNode animCurveTA -n "Character1_LeftHand_rotateZ";
	rename -uid "54A2B149-4B1B-9FE7-0C22-89B9CAF4845B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 18 ".ktv[0:17]"  6 -0.00016080286877695471 7 -0.00016080308705568314
		 8 0.056255068629980087 9 -0.24625207483768463 10 -2.0721335411071777 11 -8.5314865112304687
		 12 -10.10462474822998 13 -7.8046960830688477 14 -2.8366603851318359 15 -0.54816567897796631
		 16 1.6016758680343628 17 3.4136040210723877 18 4.5717401504516602 19 4.7613472938537598
		 20 1.6894741058349609 21 -1.9865037202835083 22 -2.9668135643005371 23 -2.9668135643005371;
createNode animCurveTU -n "Character1_LeftHand_visibility";
	rename -uid "954AA104-4C6F-AE95-7A62-5285EA261955";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateX";
	rename -uid "C4F23D34-45D6-5823-10CE-37A4A101F828";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.1041760444641113;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateY";
	rename -uid "618614AC-4F3A-6AC6-AF73-D4AC4C99849B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.7491040229797363;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateZ";
	rename -uid "A5EC26FE-4D10-16D1-CF11-BDA4F961235F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.7276492118835449;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleX";
	rename -uid "D95B6D1F-481A-ED9A-E42B-9B9BACD2DF67";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleY";
	rename -uid "A5198C24-40E9-78AF-338B-9993847793F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleZ";
	rename -uid "CB60952E-4C64-388D-14E8-8F98ECC2AAA1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateX";
	rename -uid "3C454555-4F59-1269-6C8C-A19E02E31AD5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.1981080833863871e-009;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateY";
	rename -uid "1B9B6701-4265-DF4C-CF9C-5B939013A280";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3960885225650088e-010;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateZ";
	rename -uid "B368E103-4AC6-AE98-ECCC-31A1AD34A2EC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.9419526814677965e-009;
createNode animCurveTU -n "Character1_LeftHandThumb1_visibility";
	rename -uid "A2B423C2-4A3E-894C-2B6E-288F3E7704B7";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateX";
	rename -uid "B202D31D-48C1-6BDE-4949-AA86B1019024";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.3594832420349121;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateY";
	rename -uid "0C0F0A36-4B06-82FA-45FF-AF917E1DBDEA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.8193249702453613;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateZ";
	rename -uid "4BEED7D2-44EC-CA2F-FA5F-A995D01AE633";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3286550045013428;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleX";
	rename -uid "05E6A409-4537-E6AA-9966-2AB9E7CAEE7D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleY";
	rename -uid "0B7FBFC9-4845-088D-7CD4-B0841B9E6812";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleZ";
	rename -uid "60C0F474-4F1A-7655-3400-B2A860135DFB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateX";
	rename -uid "F582D238-4113-1BB4-4C13-5DB3F8E30796";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.5216219395597363e-009;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateY";
	rename -uid "98A33518-426B-18A1-DFD6-B99D182CCB81";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5660007452188438e-009;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateZ";
	rename -uid "0C58404A-48E4-1AF9-9371-9DB9287FDBC9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2345189759344066e-008;
createNode animCurveTU -n "Character1_LeftHandThumb2_visibility";
	rename -uid "08A8F91B-4911-1C41-41CA-E3AFEC8E6367";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateX";
	rename -uid "DA95D575-444B-FED3-93C4-3E8554D7C9F1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1733376979827881;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateY";
	rename -uid "42322EA7-485F-4B2F-9D0B-AFBC3CB19E69";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.34823575615882874;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateZ";
	rename -uid "FC6774E6-45A4-F46B-626D-E38C4CDFE145";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.468076229095459;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleX";
	rename -uid "B4005586-4879-0B42-734F-E7BCA2B47939";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleY";
	rename -uid "4C37F556-4AEC-DF13-11ED-6C93E8279672";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleZ";
	rename -uid "04F065E7-49FB-4F9A-12B6-779945A5249B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateX";
	rename -uid "767BEC89-4635-81D8-5ED8-F2BFE5CFAAC8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3105144525127344e-008;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateY";
	rename -uid "C02D4F59-4042-FB94-A341-618E3C531E71";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.463138765700478e-008;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateZ";
	rename -uid "ACA3CED0-4F66-7281-BA74-228A65C274FA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.6830177775377706e-009;
createNode animCurveTU -n "Character1_LeftHandThumb3_visibility";
	rename -uid "7DADF9A6-4388-8CC1-4BB5-2D9C26350C43";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateX";
	rename -uid "7D9295D2-4A4F-9331-25CE-86B694734F1E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.4336724281311035;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateY";
	rename -uid "7B9FFB61-4E91-47DA-2582-9082340CEC0C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.6582565307617187;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateZ";
	rename -uid "A630C011-45ED-8E60-DF57-8AB9C7160344";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.338188648223877;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleX";
	rename -uid "94BB4579-4BDB-58F1-38F6-78B78B73C4FB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleY";
	rename -uid "EA3E3E2E-4CED-C458-09FB-D3BA77B71763";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleZ";
	rename -uid "6A9F2407-4DC4-24EC-D064-D1907E3BE8D1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateX";
	rename -uid "A7670A52-46EF-76C6-ACFE-BB842CA82566";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.5141093934190053e-009;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateY";
	rename -uid "8E7C8B6B-4AA6-B245-6C8D-63B6C7F2F182";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.8072161502926747e-010;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateZ";
	rename -uid "6F70FC61-491F-54B9-6839-1CB6EDE6AC5B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.9771990973113134e-009;
createNode animCurveTU -n "Character1_LeftHandIndex1_visibility";
	rename -uid "A0857019-4063-39FA-24BC-B4961A6F77FE";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateX";
	rename -uid "E9FD425D-47BF-1EB6-F2AA-BAA2FF807C30";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.5846562385559082;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateY";
	rename -uid "D174E0C6-4311-3E83-436E-1DB77082ED00";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.7001519203186035;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateZ";
	rename -uid "5FD079C5-45C3-2966-296E-2DA67A52EA60";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.35993427038192749;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleX";
	rename -uid "A0B67C1A-4A49-8B2B-C7F4-22BDC0FD9869";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleY";
	rename -uid "338F651F-4087-C6CB-5155-F4819CB2E7F1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleZ";
	rename -uid "02F06AE8-43BD-8C64-E58E-A297C16233DF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateX";
	rename -uid "7B2599D5-49B2-DF14-53F1-42A6B66E6AB2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.3576076080853454e-009;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateY";
	rename -uid "DD3910F6-4041-0D3C-4477-3685A0B1A0CC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.776038675657901e-009;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateZ";
	rename -uid "BD0938B3-4822-DF1F-5C20-258F5C6E5603";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.0156121277304919e-008;
createNode animCurveTU -n "Character1_LeftHandIndex2_visibility";
	rename -uid "8A33A695-44F6-9B48-9045-6082C8AAABF4";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateX";
	rename -uid "54AF2C4C-4CE4-C84B-2E66-06A3225B4F6B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.2315034866333008;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateY";
	rename -uid "D16B201C-486A-5F3C-3690-D09500FC4745";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.1625156402587891;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateZ";
	rename -uid "45227A02-4482-5904-D792-60953FA84C25";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.733155369758606;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleX";
	rename -uid "C9DBF900-4E27-314E-047A-E282A500946A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleY";
	rename -uid "9BFB12C1-447F-3122-F0A8-A7974E113C60";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleZ";
	rename -uid "BB28CB64-4474-D863-6230-8F837CDE197C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateX";
	rename -uid "2A20920A-4FDF-ACFD-A3EB-AABE85206AD9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2426471407422923e-008;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateY";
	rename -uid "5F6CF045-4B1E-E423-C52C-6EA427940187";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.6730982821400175e-008;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateZ";
	rename -uid "24605B1D-415E-D967-E5BC-D5B28E524C19";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.0515380201641165e-009;
createNode animCurveTU -n "Character1_LeftHandIndex3_visibility";
	rename -uid "E8B76BEF-446A-1980-FEC1-7AAD6864A3E9";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing1_translateX";
	rename -uid "2BAE70B4-4719-855A-658E-A3BDBDB700F4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.3586184978485107;
createNode animCurveTL -n "Character1_LeftHandRing1_translateY";
	rename -uid "A6F6B1A4-4E63-6138-865C-3CB82DEA441B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4706459045410156;
createNode animCurveTL -n "Character1_LeftHandRing1_translateZ";
	rename -uid "FC7EE145-4F8C-F46E-071E-4E8AFA1FE81D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8014267683029175;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleX";
	rename -uid "86CA1BC8-46A4-C510-62DC-479FF2AC75A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleY";
	rename -uid "C038C146-4EAE-F7FD-A8A1-74BB5AF19C8D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleZ";
	rename -uid "44AB79CD-4D80-09AF-A38B-21B6090DA988";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateX";
	rename -uid "6383B452-4D03-DFC6-D049-1EA3CF4CC487";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.395554641436263e-010;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateY";
	rename -uid "98DD089B-4715-3C46-D3D4-A2B4122D571B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.5990862774256698e-010;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateZ";
	rename -uid "2804D096-4D5F-80DC-2557-1B845355D2FB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.1762563124668759e-009;
createNode animCurveTU -n "Character1_LeftHandRing1_visibility";
	rename -uid "1FDB4C94-48D9-A5AF-6056-1AAB2C1C0D6B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing2_translateX";
	rename -uid "D1552DC8-4099-6A7A-0A32-AB88D6D65B16";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7360267639160156;
createNode animCurveTL -n "Character1_LeftHandRing2_translateY";
	rename -uid "ED525E46-4641-4C3C-8CF2-048ECC9EE05E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8172166347503662;
createNode animCurveTL -n "Character1_LeftHandRing2_translateZ";
	rename -uid "A7A626B0-4454-8DA9-674C-D0910E0D690B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.81390875577926636;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleX";
	rename -uid "4C8ED497-4C0D-5068-4A0D-33BBFF1D4A3F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleY";
	rename -uid "D71DE640-41F6-A00C-F7E2-049BC75A39A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleZ";
	rename -uid "EC7B6BA7-4A0C-B3F7-8C19-078A220CF445";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateX";
	rename -uid "9AAB1839-47E7-E765-45EA-8793621B5EE6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1503001218216014e-008;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateY";
	rename -uid "EAD733FA-4741-601A-C67A-7B92778F77A3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3978583563177835e-009;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateZ";
	rename -uid "FC57FD95-4E82-A585-8633-A78149CB1A3B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.0914338583775134e-010;
createNode animCurveTU -n "Character1_LeftHandRing2_visibility";
	rename -uid "E4A4AF44-41BD-A6F3-5DF0-3488D731C45E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing3_translateX";
	rename -uid "9FA36B93-429C-8FD2-FEA6-CF8EDD2473CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.4660000801086426;
createNode animCurveTL -n "Character1_LeftHandRing3_translateY";
	rename -uid "37332AED-4A89-DC1D-50F5-1D86C9340114";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9262526035308838;
createNode animCurveTL -n "Character1_LeftHandRing3_translateZ";
	rename -uid "4CF310BD-4E37-A55A-B1A1-9F9C87BE8B3C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.018019437789917;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleX";
	rename -uid "D31DC70B-44F5-46F0-013B-AB810954184F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleY";
	rename -uid "9C7AF54B-4580-132C-F67A-0ABA2BFB69FA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleZ";
	rename -uid "EF2901D2-4994-EB07-08AC-7D80F376AE82";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateX";
	rename -uid "C530FD23-4BC0-70D0-FB03-C189DBF63721";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.5833347662191963e-009;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateY";
	rename -uid "4F2D6D1C-4F0D-D12C-EA22-35A44554E9D5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.7699244736490982e-008;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateZ";
	rename -uid "4BE5A074-463B-9B5E-BA58-4FA9F5449AA5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0753570478527763e-008;
createNode animCurveTU -n "Character1_LeftHandRing3_visibility";
	rename -uid "603B7E23-43B2-7D53-455F-9487BB09AAA6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightShoulder_translateX";
	rename -uid "9F487A19-432E-EB08-E895-F7B76356A3AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.151631355285645;
createNode animCurveTL -n "Character1_RightShoulder_translateY";
	rename -uid "08548DFB-479F-4820-8D09-769AE9A450CD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.1017274856567383;
createNode animCurveTL -n "Character1_RightShoulder_translateZ";
	rename -uid "7317BE3F-4A7E-BBD7-6046-EE9DA1D4F81E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.81731081008911133;
createNode animCurveTU -n "Character1_RightShoulder_scaleX";
	rename -uid "19D1DDFE-424D-C3F2-6665-3A812CBB6D74";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightShoulder_scaleY";
	rename -uid "6DF1122E-401B-924A-AFA0-5FB06EF8C1D2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightShoulder_scaleZ";
	rename -uid "172BD5C0-4013-2940-2C6F-B8830EB13ACB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightShoulder_rotateX";
	rename -uid "39AB8256-4DCF-FB75-8D99-80B5A86C2647";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  0 -7.8278732299804687 1 -8.3010978698730469
		 2 -8.7583303451538086 3 -9.1087198257446289 4 -9.2710371017456055 5 -9.4340782165527344
		 6 -9.3634700775146484 7 -8.3971729278564453 8 -6.0692758560180664 9 -2.8093998432159424
		 10 0.64947271347045898 11 4.0632467269897461 12 7.6626467704772958 13 11.348409652709961
		 14 15.004973411560057 15 18.188465118408203 16 21.038381576538086 17 23.813972473144531
		 18 26.492424011230469 19 28.895988464355465 20 30.232856750488281 21 30.232856750488281;
createNode animCurveTA -n "Character1_RightShoulder_rotateY";
	rename -uid "3C8481BF-4134-EC01-EFB8-8480E8886526";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  0 11.290409088134766 1 12.280182838439941
		 2 13.314906120300293 3 14.346176147460936 4 15.320396423339846 5 16.338199615478516
		 6 17.253486633300781 7 17.619356155395508 8 17.144044876098633 9 16.115617752075195
		 10 14.944989204406737 11 13.76312255859375 12 12.49041748046875 13 11.203136444091797
		 14 9.9695796966552752 15 8.9148902893066406 16 7.9787945747375497 17 7.0798144340515146
		 18 6.2236037254333496 19 5.4636998176574707 20 5.0441608428955078 21 5.0441608428955078;
createNode animCurveTA -n "Character1_RightShoulder_rotateZ";
	rename -uid "75327B40-4802-59E1-1F9B-D9970DA35221";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  0 13.417060852050781 1 15.856416702270508
		 2 18.335508346557617 3 20.733497619628906 4 22.929550170898437 5 25.189847946166992
		 6 27.164546966552734 7 27.809274673461914 8 26.474111557006836 9 23.881410598754883
		 10 21.015451431274414 11 18.170330047607422 12 15.134501457214355 13 12.076470375061035
		 14 9.1598501205444336 15 6.6712660789489746 16 4.4585986137390137 17 2.3317644596099854
		 18 0.30359047651290894 19 -1.4996875524520874 20 -2.4970386028289795 21 -2.4970386028289795;
createNode animCurveTU -n "Character1_RightShoulder_visibility";
	rename -uid "13B76334-4C53-A077-B4C8-D298FC4E5DE6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightArm_translateX";
	rename -uid "B8E23D17-478E-51A6-106D-08B8B109D6CE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -10.05986499786377 1 -4.8925971984863281
		 2 1.2916668653488159 3 6.9674348831176758 4 10.609206199645996 5 10.691492080688477
		 6 5.205345630645752 7 -4.8322324752807617 8 -16.407833099365234 9 -26.508037567138672
		 10 -32.119438171386719 11 -34.215606689453125 12 -35.815807342529297 13 -36.975139617919922
		 14 -37.748710632324219 15 -38.191635131835938 16 -38.359024047851562 17 -38.305976867675781
		 18 -38.087604522705078 19 -37.759017944335938 20 -37.375320434570313 21 -36.991626739501953
		 22 -36.663040161132813 23 -36.444671630859375 24 -36.391624450683594 25 -36.559005737304688
		 26 -36.559005737304688;
createNode animCurveTL -n "Character1_RightArm_translateY";
	rename -uid "009015CF-4EA7-EA3C-5EEC-CABB741B59DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -6.8638343811035156 1 -4.8066611289978027
		 2 -1.9368981122970581 3 0.52656877040863037 4 1.3648583889007568 5 -0.64091908931732178
		 6 -7.3501319885253906 7 -17.950191497802734 8 -29.652040481567383 9 -39.666622161865234
		 10 -45.204875946044922 11 -47.294200897216797 12 -48.860141754150391 13 -49.960842132568359
		 14 -50.654460906982422 15 -50.999153137207031 16 -51.053073883056641 17 -50.874374389648438
		 18 -50.521213531494141 19 -50.051742553710938 20 -49.524112701416016 21 -48.996490478515625
		 22 -48.527019500732422 23 -48.173858642578125 24 -47.995159149169922 25 -48.049076080322266
		 26 -48.049076080322266;
createNode animCurveTL -n "Character1_RightArm_translateZ";
	rename -uid "35E69546-4552-5609-A42D-2B86A0AEF46D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -1.4631747007369995 1 -13.308080673217773
		 2 -26.798925399780273 3 -39.466800689697266 4 -48.842800140380859 5 -52.458023071289063
		 6 -47.442161560058594 7 -35.441165924072266 8 -20.760471343994141 9 -7.7055258750915527
		 10 -0.58177155256271362 11 1.8124768733978271 12 3.5945451259613037 13 4.8324556350708008
		 14 5.5942277908325195 15 5.9478788375854492 16 5.9614300727844238 17 5.7029023170471191
		 18 5.2403159141540527 19 4.6416888236999512 20 3.9750421047210693 21 3.3083946704864502
		 22 2.7097675800323486 23 2.2471804618835449 24 1.9886524677276611 25 2.0022051334381104
		 26 2.0022051334381104;
createNode animCurveTU -n "Character1_RightArm_scaleX";
	rename -uid "858997BE-48E6-EF6C-F1B8-0793C9DD8DEC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightArm_scaleY";
	rename -uid "DB96A46C-460D-FD89-B14F-AB9FA958E233";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightArm_scaleZ";
	rename -uid "841A60AD-45E7-59A7-8EE8-06A509FA636A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightArm_rotateX";
	rename -uid "820ED088-4A2C-5C00-FC36-828DF9FA6A29";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -32.020454406738281 1 -35.528709411621094
		 2 -37.569229125976563 3 -37.096611022949219 4 -33.506599426269531 5 -26.060049057006836
		 6 -9.3025856018066424 7 17.803609848022461 8 44.59515380859375 9 60.434936523437507
		 10 63.478649139404297 11 57.738212585449219 12 48.345199584960938 13 37.216781616210937
		 14 26.047595977783207 15 17.781490325927734 16 11.400032997131348 17 -3.0255362987518311
		 18 -9.5136003494262695 19 -13.204404830932617 20 -16.718461990356445 21 -20.42896842956543
		 22 -23.918861389160156 23 -28.699920654296875 24 -33.447799682617187 25 -37.782962799072266
		 26 -37.782962799072266;
createNode animCurveTA -n "Character1_RightArm_rotateY";
	rename -uid "04ACAA3D-4EDB-27D9-8103-0DA829026BAB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 32.294548034667969 1 35.936275482177734
		 2 36.469486236572266 3 37.404048919677734 4 41.641532897949219 5 50.216999053955078
		 6 58.515594482421875 7 59.142154693603509 8 51.595676422119141 9 39.133487701416016
		 10 26.586524963378906 11 15.016547203063965 12 3.7655093669891357 13 -7.0133371353149414
		 14 -16.651533126831055 15 -22.688718795776367 16 -26.498758316040039 17 -30.448051452636719
		 18 -31.432411193847653 19 -31.020957946777344 20 -28.894609451293945 21 -25.413436889648438
		 22 -21.85114860534668 23 -18.54304313659668 24 -15.479522705078125 25 -12.676001548767092
		 26 -12.676001548767092;
createNode animCurveTA -n "Character1_RightArm_rotateZ";
	rename -uid "6576CBCC-4B63-D860-C105-9A851F467C35";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -74.225425720214844 1 -94.589134216308594
		 2 -110.1903076171875 3 -118.37224578857422 4 -119.58780670166016 5 -114.7064208984375
		 6 -99.433807373046875 7 -72.454742431640625 8 -41.202949523925781 9 -15.807241439819336
		 10 -3.112452507019043 11 -1.2613502740859985 12 -1.2603013515472412 13 -0.11473879218101501
		 14 3.4430277347564697 15 4.3083877563476562 16 0.43316465616226196 17 4.623786449432373
		 18 0.076773226261138916 19 -9.0850734710693359 20 -14.931052207946777 21 -13.510705947875977
		 22 -12.181083679199219 23 -9.9776105880737305 24 -7.8526878356933603 25 -6.4495325088500977
		 26 -6.4495325088500977;
createNode animCurveTU -n "Character1_RightArm_visibility";
	rename -uid "B6411194-498B-8489-23DA-89B1F07E8C30";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightForeArm_translateX";
	rename -uid "425B4284-4537-F387-7007-45963DC2B09A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -16.894504547119141;
createNode animCurveTL -n "Character1_RightForeArm_translateY";
	rename -uid "13EB7F7D-440A-BE10-580F-4296BBF8BE77";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.7289671897888184;
createNode animCurveTL -n "Character1_RightForeArm_translateZ";
	rename -uid "3C46E5BC-4836-39D7-D303-79B516209823";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.2348899841308594;
createNode animCurveTU -n "Character1_RightForeArm_scaleX";
	rename -uid "6B961E6C-4AAB-1CDD-55A6-C288DF6EFD1B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightForeArm_scaleY";
	rename -uid "5AE7F6F7-4624-EB01-D048-31B01AD6BB74";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightForeArm_scaleZ";
	rename -uid "310B42FF-43E1-3BBA-F312-9AB256733307";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightForeArm_rotateX";
	rename -uid "DE6CA863-46D1-59E0-CE81-9A9EDA8AAC99";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -10.021683692932129 1 -8.0743799209594727
		 2 -5.4139599800109863 3 -2.0312004089355469 4 1.6210237741470339 5 3.6532006263732915
		 6 3.7698061466217041 7 4.064012050628663 8 5.7814254760742187 9 10.060647010803223
		 10 22.123208999633789 11 35.770427703857422 12 39.137226104736328 13 37.960475921630859
		 14 39.021526336669922 15 47.174121856689453 16 47.793498992919922 17 31.841796875
		 18 26.472835540771484 19 25.07023811340332 20 21.721714019775391 21 24.933500289916992
		 22 27.488761901855469 23 24.363161087036133 24 27.052608489990234 25 27.600339889526367
		 26 27.600339889526367;
createNode animCurveTA -n "Character1_RightForeArm_rotateY";
	rename -uid "D7B0A79F-4F58-2BE9-8F1C-E99F6B58BCD7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 7.7267794609069824 1 18.562545776367188
		 2 29.781539916992187 3 39.903789520263672 4 47.514701843261719 5 49.588809967041016
		 6 48.431289672851563 7 50.624095916748047 8 60.839614868164063 9 73.817291259765625
		 10 81.928138732910156 11 83.199195861816406 12 81.652778625488281 13 79.018875122070313
		 14 76.810310363769531 15 74.970817565917969 16 71.963821411132827 17 66.259429931640625
		 18 60.224395751953132 19 55.804615020751953 20 51.343212127685547 21 53.768730163574219
		 22 55.410991668701172 23 53.369625091552734 24 55.146530151367188 25 55.4776611328125
		 26 55.4776611328125;
createNode animCurveTA -n "Character1_RightForeArm_rotateZ";
	rename -uid "D2AEB714-4DDE-D276-87BA-50946A0ED511";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -27.592550277709961 1 -25.077793121337891
		 2 -21.779590606689453 3 -17.75236701965332 4 -13.524674415588379 5 -10.399709701538086
		 6 -9.3529758453369141 7 -10.226638793945312 8 -14.507988929748535 9 -19.160078048706055
		 10 -12.755027770996096 11 1.2588436603546143 12 7.8199672698974609 13 11.065827369689941
		 14 16.25377082824707 15 29.829545974731449 16 36.665817260742188 17 24.46613883972168
		 18 25.232860565185547 19 29.854751586914063 20 29.629707336425781 21 33.208663940429687
		 22 36.021640777587891 23 32.576953887939453 24 35.543354034423828 25 36.143882751464844
		 26 36.143882751464844;
createNode animCurveTU -n "Character1_RightForeArm_visibility";
	rename -uid "67D69A72-42C1-559E-F596-DF8A174E306D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHand_translateX";
	rename -uid "A4A2C02F-4850-63EA-24D6-0F8C4ADFCD30";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.9317862987518311;
createNode animCurveTL -n "Character1_RightHand_translateY";
	rename -uid "0A0AA1FB-4AB9-F136-AE30-9BA6DE53135B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 16.156974792480469;
createNode animCurveTL -n "Character1_RightHand_translateZ";
	rename -uid "511FC9E8-48F9-014A-877E-BDBBECE19E31";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.4309353828430176;
createNode animCurveTU -n "Character1_RightHand_scaleX";
	rename -uid "6B0761AA-4E03-CBB1-A8DF-9AA031865B57";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightHand_scaleY";
	rename -uid "6D64C79E-4AC4-2A98-CF26-81B581E0E9CE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHand_scaleZ";
	rename -uid "EA2FCCD2-4839-8431-6187-FB94A09D1C9B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightHand_rotateX";
	rename -uid "51E0D77E-4FF5-87AE-9470-099A3DE88DD8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 1.8879169338958945e-009 1 -5.0397334098815918
		 2 -9.6895160675048828 3 -13.294621467590332 4 -15.86902904510498 5 -18.250726699829102
		 6 -20.229341506958008 7 -18.722801208496094 8 -10.04081916809082 9 3.3817470073699951
		 10 13.252968788146973 11 16.505123138427734 12 15.356594085693358 13 10.39073657989502
		 14 4.5602593421936035 15 2.3708875179290771 16 0.058090165257453918 17 -3.3270819187164307
		 18 -1.9633704423904419 19 0.93802165985107422 20 -0.10784203559160233 21 -0.24583710730075836
		 22 2.1705160140991211 23 5.7109508514404297 24 2.7178304195404053 25 4.7713541984558105
		 26 4.7713541984558105;
createNode animCurveTA -n "Character1_RightHand_rotateY";
	rename -uid "A0542E6A-4BA2-4C7E-30A7-9499E2277DE8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 2.3886028710506939e-009 1 -4.8510427474975586
		 2 -10.6480712890625 3 -16.747961044311523 4 -21.960527420043945 5 -26.562723159790039
		 6 -28.53605842590332 7 -25.029060363769531 8 -14.405190467834473 9 -5.8886599540710449
		 10 -2.3794000148773193 11 3.5319719314575195 12 11.805062294006349 13 19.076030731201175
		 14 23.762884140014648 15 21.11848258972168 16 18.965263366699219 17 14.206570625305176
		 18 14.288629531860352 19 14.163350105285645 20 10.179699897766113 21 5.5878510475158691
		 22 1.0051072835922241 23 4.4208211898803711 24 -0.95766705274581909 25 -0.025033198297023773
		 26 -0.025033198297023773;
createNode animCurveTA -n "Character1_RightHand_rotateZ";
	rename -uid "615D91FE-48B3-339D-4C26-2BA9A5CB495D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -0.00013989154831506312 1 -4.2155165672302255
		 2 -8.4968729019165039 3 -11.309413909912109 4 -11.193106651306152 5 -5.7619123458862305
		 6 3.5416440963745122 7 11.399292945861816 8 19.592914581298828 9 30.84906005859375
		 10 35.707443237304688 11 29.724853515624996 12 17.819400787353516 13 2.7930355072021484
		 14 -10.798884391784668 15 -12.868281364440918 16 -15.196170806884766 17 -17.274087905883793
		 18 -13.597000122070313 19 -6.1840071678161621 20 -1.9920024871826172 21 1.4904735088348389
		 22 3.9204013347625732 23 6.9952678680419931 24 9.2530612945556641 25 12.41526985168457
		 26 12.41526985168457;
createNode animCurveTU -n "Character1_RightHand_visibility";
	rename -uid "9A689FD5-4E21-C35F-0478-93B12B89EE0F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb1_translateX";
	rename -uid "770A22EC-4EDA-80BF-12C2-44826EB7F429";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2155418395996094;
createNode animCurveTL -n "Character1_RightHandThumb1_translateY";
	rename -uid "D3B9D3D7-4385-5FDC-4B33-C893F2173F4D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8998895883560181;
createNode animCurveTL -n "Character1_RightHandThumb1_translateZ";
	rename -uid "93841E99-4B77-BA5E-29A4-3DAAAC5C0260";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3098368644714355;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleX";
	rename -uid "955B9E9C-438D-E022-14DC-DF858F6EDC3D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleY";
	rename -uid "1D130196-4EC0-1208-4E52-EC927895378C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleZ";
	rename -uid "03768B53-4258-3D54-B671-F1B3B6D50FD7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateX";
	rename -uid "1ED624E6-4785-E0AA-B83C-85B95919CF02";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.4526640980157026e-009;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateY";
	rename -uid "27D51882-464C-D5BF-686A-09931B44F98C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.1090772440763885e-009;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateZ";
	rename -uid "4F23EDBD-4E11-6167-ECCD-9592A5CC6C04";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.4103339596747162e-010;
createNode animCurveTU -n "Character1_RightHandThumb1_visibility";
	rename -uid "8BBBE368-4743-549A-7553-B9A606DE46F5";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb2_translateX";
	rename -uid "7E5519E1-4530-DDC9-8EFE-3AA02F381EA1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9627087116241455;
createNode animCurveTL -n "Character1_RightHandThumb2_translateY";
	rename -uid "80149995-4EC2-B5D9-B389-0FB8B3CAD5B8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5399179458618164;
createNode animCurveTL -n "Character1_RightHandThumb2_translateZ";
	rename -uid "BE8A2044-4B89-BAA8-FBB7-C7A40D89F46B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6609530448913574;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleX";
	rename -uid "E87B320A-4C6A-9259-AD90-5E81C5BA1989";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleY";
	rename -uid "BDB5512D-44F7-A252-2FDF-09AC4028C42F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleZ";
	rename -uid "76B6DD3F-4E41-8EC4-C9F9-7CB1DD5BFE94";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateX";
	rename -uid "66BB02EF-4A83-3516-6E52-4293E64A5227";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.6092016608126869e-009;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateY";
	rename -uid "170138C4-48F1-9723-AB27-16A0C9000C87";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4465986630796124e-008;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateZ";
	rename -uid "DF15B616-470B-A2D0-C93E-0A9B19831B9A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.1848610415607936e-009;
createNode animCurveTU -n "Character1_RightHandThumb2_visibility";
	rename -uid "97D1B525-4738-8EEF-9362-839CBC9E444B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb3_translateX";
	rename -uid "41ECD452-49DF-A10B-BE32-30ADABC1C78F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.5867142677307129;
createNode animCurveTL -n "Character1_RightHandThumb3_translateY";
	rename -uid "D159EE34-4177-664F-855A-C9A4599C7643";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.6152248382568359;
createNode animCurveTL -n "Character1_RightHandThumb3_translateZ";
	rename -uid "73C9AE8D-442F-8D4C-8D34-7CBC7D24E827";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.4791715145111084;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleX";
	rename -uid "CE105C1B-47B0-8ACC-0453-94A8D791B076";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleY";
	rename -uid "68B0A84F-4202-14CB-C5EE-3B81EB3AC945";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleZ";
	rename -uid "CD15CFC8-4C41-21B7-8075-25BB926FC35F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateX";
	rename -uid "31F06758-456B-BBD1-2799-F88EFB974F3D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1517275133599014e-008;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateY";
	rename -uid "1351C6BD-4E10-EFB7-F535-5D895A1D637F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.6789428514130122e-008;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateZ";
	rename -uid "F38FB51E-4F74-8C51-639C-52B164A857DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1666732468995633e-008;
createNode animCurveTU -n "Character1_RightHandThumb3_visibility";
	rename -uid "149851DA-4D62-4A1A-94C1-C2AC8736FC0D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex1_translateX";
	rename -uid "960C15A9-4A5C-FCBC-C5DE-A583624B446B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.615594863891602;
createNode animCurveTL -n "Character1_RightHandIndex1_translateY";
	rename -uid "336308E9-4F78-A60B-CAE2-17AC063CC841";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4688365459442139;
createNode animCurveTL -n "Character1_RightHandIndex1_translateZ";
	rename -uid "1ADC1BAA-4F89-5653-5816-A88B4A20D830";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.3266587257385254;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleX";
	rename -uid "FB7CD183-410C-22A8-B661-A0BC40C6256A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleY";
	rename -uid "296BCDF1-4097-0BAD-C55A-70A8BBD7EF6C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleZ";
	rename -uid "553CEB09-4017-23C7-749F-EB8EE2FD295B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateX";
	rename -uid "493531CE-4233-8687-485A-4F9E0D874139";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.4524096348984585e-009;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateY";
	rename -uid "EA3A44B6-497C-B0C1-0FE5-C3ACBE423AEF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.10939876466432e-009;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateZ";
	rename -uid "840F9D6E-48C4-B9A7-7420-84B07D1CF760";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.4096342416134462e-010;
createNode animCurveTU -n "Character1_RightHandIndex1_visibility";
	rename -uid "3025EE1E-40FA-F071-8E29-97A12F479C28";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex2_translateX";
	rename -uid "E6F924FA-4D72-5412-9621-D3B11CA2647F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.10917675495147705;
createNode animCurveTL -n "Character1_RightHandIndex2_translateY";
	rename -uid "12C820DF-4E41-73B8-F998-89995D1C1830";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.54082572460174561;
createNode animCurveTL -n "Character1_RightHandIndex2_translateZ";
	rename -uid "565FF7A1-482E-93A5-EF55-2DB51581F644";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.6861534118652344;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleX";
	rename -uid "EC020129-4920-3AEF-76C2-B2908B489B13";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleY";
	rename -uid "9D2EDCAC-4269-A89C-B476-6F8264A5E431";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleZ";
	rename -uid "3D6A6DDC-4BCE-07F1-B472-01AC48CD4EB6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateX";
	rename -uid "64EF808E-44BF-FD5B-82C5-D48EFBAA899A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.6122748691671518e-009;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateY";
	rename -uid "1CA5D63A-43B8-A4D6-4283-AF98F492DAB9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4464510478262582e-008;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateZ";
	rename -uid "5E590551-4F56-A2E2-3690-C0935C161EA5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.1846336678853504e-009;
createNode animCurveTU -n "Character1_RightHandIndex2_visibility";
	rename -uid "0AA11498-49B0-E41E-C1BA-41B8A6110991";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex3_translateX";
	rename -uid "5A0FFFEA-4D0A-D3EF-7A2D-8D83B9408A97";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.8903987407684326;
createNode animCurveTL -n "Character1_RightHandIndex3_translateY";
	rename -uid "3B2E1BEB-4720-C2D9-B779-4D9411797FFE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3401000499725342;
createNode animCurveTL -n "Character1_RightHandIndex3_translateZ";
	rename -uid "6BD757AE-4681-9389-2FF6-7FA92CE5F321";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.8930935859680176;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleX";
	rename -uid "8D1759F2-4B8C-7D69-4BE4-FE8A8F99CE98";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleY";
	rename -uid "A6041F0F-46AC-75CD-29FA-B69C5EDF40B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleZ";
	rename -uid "1C7B8940-4E60-8C6C-CD77-65AFD7577F4C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateX";
	rename -uid "D3300AF3-43AB-A43E-EE9D-3D8007A9E7EE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.151974693414104e-008;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateY";
	rename -uid "C4A9C203-4DF5-8302-B6B5-099F9F9A36B6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.6779862832549952e-008;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateZ";
	rename -uid "B0419A2C-464C-C6F4-2495-038F53D00A0B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1668437771561457e-008;
createNode animCurveTU -n "Character1_RightHandIndex3_visibility";
	rename -uid "1C5D3FA4-4033-30F9-05AA-A1A6BEE2743B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing1_translateX";
	rename -uid "2F5282B7-4157-E40B-FD98-9FB4C7CC9920";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.6450033187866211;
createNode animCurveTL -n "Character1_RightHandRing1_translateY";
	rename -uid "C484EE6B-4403-B706-DC3F-6791A9AE1509";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.0317020416259766;
createNode animCurveTL -n "Character1_RightHandRing1_translateZ";
	rename -uid "7F5A106A-481D-4A98-75A1-A1A1DA1D953F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.2285447120666504;
createNode animCurveTU -n "Character1_RightHandRing1_scaleX";
	rename -uid "E60616E6-46C2-D528-2D57-12AD7AB42055";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandRing1_scaleY";
	rename -uid "AA831B58-489A-2469-E3EF-14A5B979F9CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandRing1_scaleZ";
	rename -uid "29CDD545-4538-CCB2-81E9-28ACDB1C62FB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightHandRing1_rotateX";
	rename -uid "9FCAA650-4F5B-2A81-0E54-A1A957BA0282";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.4522528714073815e-009;
createNode animCurveTA -n "Character1_RightHandRing1_rotateY";
	rename -uid "06C8FC37-4784-5783-061C-DD9483825678";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.109289074629487e-009;
createNode animCurveTA -n "Character1_RightHandRing1_rotateZ";
	rename -uid "AD482970-4EBC-C19D-6F78-55B4041AF65E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.4071215293529633e-010;
createNode animCurveTU -n "Character1_RightHandRing1_visibility";
	rename -uid "8183F03E-406B-64EE-A85E-19ACFD964CF7";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing2_translateX";
	rename -uid "72B9FE14-4F29-0363-4591-74BA93169F48";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.046228401362895966;
createNode animCurveTL -n "Character1_RightHandRing2_translateY";
	rename -uid "BE87F0CE-437A-D2CC-F909-499D6D7356D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.89510548114776611;
createNode animCurveTL -n "Character1_RightHandRing2_translateZ";
	rename -uid "993D5434-42DE-F7A6-7DDC-2E9D3110A8A4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.4977946281433105;
createNode animCurveTU -n "Character1_RightHandRing2_scaleX";
	rename -uid "21B5B043-4842-C1EF-FEB8-AEA40A66CDAA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandRing2_scaleY";
	rename -uid "0344A593-4253-F090-C771-D99AA056016E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandRing2_scaleZ";
	rename -uid "52511541-4A4D-9E15-1FE5-7E8524F29879";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightHandRing2_rotateX";
	rename -uid "45E87D69-4A22-9C06-27FC-999D8ACE481D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.6115846435127423e-009;
createNode animCurveTA -n "Character1_RightHandRing2_rotateY";
	rename -uid "1175B56D-4D9C-9083-8892-56ACB80805C6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.446445629937898e-008;
createNode animCurveTA -n "Character1_RightHandRing2_rotateZ";
	rename -uid "CE20A722-4DB3-43FE-5662-9EA2B404FB2C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.1846933978840752e-009;
createNode animCurveTU -n "Character1_RightHandRing2_visibility";
	rename -uid "983DEF9E-40DB-9A4F-F6C5-0FB1E7366661";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing3_translateX";
	rename -uid "AE487969-4F74-F678-4C7F-3A83607F31DE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.8638609647750854;
createNode animCurveTL -n "Character1_RightHandRing3_translateY";
	rename -uid "6C14BEE1-4763-0DAD-6EB6-9EAC0487F4F1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0986917018890381;
createNode animCurveTL -n "Character1_RightHandRing3_translateZ";
	rename -uid "A2DF12ED-4FE9-622A-503E-BBB43E01DF74";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.2819910049438477;
createNode animCurveTU -n "Character1_RightHandRing3_scaleX";
	rename -uid "339F980C-4402-C526-648B-DEBA1F734150";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing3_scaleY";
	rename -uid "6575C7DA-4926-4523-9A6C-5EBE96655F4F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing3_scaleZ";
	rename -uid "BD497447-4C1C-A974-D514-89BDFD75D606";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_RightHandRing3_rotateX";
	rename -uid "883DC3D1-4EA4-BB12-6588-1DA7A8AA9755";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1514452502581209e-008;
createNode animCurveTA -n "Character1_RightHandRing3_rotateY";
	rename -uid "47556D26-4950-DC4E-90AA-0CB0482680C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.678063910048877e-008;
createNode animCurveTA -n "Character1_RightHandRing3_rotateZ";
	rename -uid "3D3222F9-40C1-5342-2FB4-AEB9F9B20496";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1671337674101778e-008;
createNode animCurveTU -n "Character1_RightHandRing3_visibility";
	rename -uid "0522B78F-48DB-387F-AD93-C99181F99D5C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Neck_translateX";
	rename -uid "0B6ABAE7-4C47-F74D-85CF-2180492EB934";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.1971182823181152;
createNode animCurveTL -n "Character1_Neck_translateY";
	rename -uid "617077E4-49AF-A987-9289-F983301D58C3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.621223449707031;
createNode animCurveTL -n "Character1_Neck_translateZ";
	rename -uid "6BF7214B-4411-EA44-A879-EF913B0E6378";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.1444897651672363;
createNode animCurveTU -n "Character1_Neck_scaleX";
	rename -uid "98E9D2E6-47DE-A82C-8D0D-3BA25278CA67";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_Neck_scaleY";
	rename -uid "5BC8F733-452E-7503-D25A-83A9F3FEE824";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Neck_scaleZ";
	rename -uid "053AA4A2-488F-7174-118B-B3AFAA5D933E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_Neck_rotateX";
	rename -uid "3CDA1102-49E4-C1F9-7B3E-D2BAAFC466CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 5.302431583404541 1 1.7160037755966187
		 2 -2.8058156967163086 3 -5.794497013092041 4 -5.9397592544555664 5 -4.5295839309692383
		 6 -2.5370209217071533 7 -0.84528839588165294 8 0.94772517681121837 9 1.6719475984573364
		 10 1.9813481569290159 11 1.5839204788208008 12 1.3483327627182007 13 2.1737017631530762
		 14 3.2861754894256592 15 4.0239419937133789 16 4.1675930023193359 17 4.045989990234375
		 18 3.8955104351043701 19 3.9649562835693355 20 4.4925127029418945 21 5.2666401863098145
		 22 5.8861546516418457 23 6.173668384552002 24 6.3023552894592294 25 6.4214210510253906
		 26 6.4214210510253906;
createNode animCurveTA -n "Character1_Neck_rotateY";
	rename -uid "353A3A1C-4FEC-7025-F7DD-5A86D81AF764";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 3.3555116653442383 1 -0.7046658992767334
		 2 -4.1078176498413086 3 -5.9025678634643555 4 -6.4392166137695313 5 -6.3702254295349121
		 6 -5.9417252540588379 7 -5.5380845069885254 8 -5.1899266242980957 9 -4.9993147850036621
		 10 -4.9032025337219238 11 -5.0045809745788574 12 -5.0504255294799805 13 -4.7903165817260742
		 14 -4.4025721549987793 15 -4.1234092712402344 16 -4.1268215179443359 17 -4.2703452110290527
		 18 -4.3644471168518066 19 -4.2273850440979004 20 -3.6030435562133785 21 -2.6778016090393066
		 22 -1.9490652084350584 23 -1.6521369218826294 24 -1.5476680994033813 25 -1.4354389905929565
		 26 -1.4354389905929565;
createNode animCurveTA -n "Character1_Neck_rotateZ";
	rename -uid "51BE9889-4508-8FCD-B7BC-31A8F53E98BE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -13.590253829956055 1 -0.41689622402191162
		 2 12.94334602355957 3 20.836694717407227 4 21.301742553710938 5 17.791233062744141
		 6 12.589135169982912 7 7.980170726776123 8 3.1061673164367676 9 1.1292484998703003
		 10 0.30970373749732971 11 1.4356774091720581 12 2.1306774616241455 13 -0.10978812724351883
		 14 -3.2462270259857178 15 -5.2144699096679687 16 -5.2195034027099609 17 -4.3256950378417969
		 18 -3.2579188346862793 19 -2.7420649528503418 20 -3.4416580200195313 21 -4.6834979057312012
		 22 -5.2000675201416016 23 -4.3390908241271973 24 -2.7636466026306152 25 -1.1905319690704346
		 26 -1.1905319690704346;
createNode animCurveTU -n "Character1_Neck_visibility";
	rename -uid "07817026-491D-C98A-099D-B6A4F29DABE0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Head_translateX";
	rename -uid "E4D5474F-4669-DE01-3332-4D9E39C9AA33";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -16.985572814941406;
createNode animCurveTL -n "Character1_Head_translateY";
	rename -uid "5E9012DB-4B9F-5510-CE7B-F9A4AF930B5D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.0131206512451172;
createNode animCurveTL -n "Character1_Head_translateZ";
	rename -uid "5EB472D4-40A7-9C38-9D17-3B93C2E7392C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.1724905967712402;
createNode animCurveTU -n "Character1_Head_scaleX";
	rename -uid "E181B11C-4FA8-3FDC-D38A-01BAE2914B62";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Head_scaleY";
	rename -uid "74B86511-462C-C419-FF08-4AB81D3C8F4D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_Head_scaleZ";
	rename -uid "55530AFD-433A-0BD9-A73E-4F9764BA60CA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_Head_rotateX";
	rename -uid "3D26CB5F-43A1-5F78-0537-0887078F8BEE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -2.311011791229248 1 -1.7469916343688965
		 2 -1.1862635612487793 3 -1.0975919961929321 4 -1.6289196014404297 5 -2.4928956031799316
		 6 -3.5107896327972412 7 -4.5007343292236328 8 -5.6131243705749512 9 -6.0243968963623047
		 10 -6.1719746589660645 11 -5.9100332260131836 12 -5.7357106208801278 13 -6.1875333786010742
		 14 -6.8262596130371094 15 -7.1956791877746591 16 -7.166738986968995 17 -6.9625825881958008
		 18 -6.6570620536804199 19 -6.3241653442382821 20 -5.9593625068664551 21 -5.5028533935546875
		 22 -4.9455976486206055 23 -4.2506799697875977 24 -3.4553968906402588 25 -2.6614065170288086
		 26 -2.6614065170288086;
createNode animCurveTA -n "Character1_Head_rotateY";
	rename -uid "953C1021-4727-33CF-893E-80AB88CAC87F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -2.5246729850769047 1 0.21367762982845306
		 2 2.9541075229644775 3 5.3164758682250977 4 7.234860897064209 5 8.9251184463500977
		 6 10.431310653686523 7 11.797981262207031 8 11.887468338012695 9 11.635170936584473
		 10 11.37474250793457 11 11.324750900268555 12 11.255575180053711 13 10.985806465148928
		 14 10.654254913330078 15 10.392148017883301 16 10.261565208435059 17 10.192299842834473
		 18 10.118992805480957 19 9.9764251708984375 20 9.7059850692749023 21 9.3507575988769531
		 22 8.9982223510742187 23 8.678288459777832 24 8.3600616455078125 25 8.0403804779052752
		 26 8.0403804779052752;
createNode animCurveTA -n "Character1_Head_rotateZ";
	rename -uid "DEE749C8-4CC0-1458-D2ED-E6968189BAEA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -14.661340713500977 1 -6.4924483299255371
		 2 1.7691183090209961 3 4.5456132888793945 4 -0.11453549563884734 5 -8.7664756774902344
		 6 -18.940849304199219 7 -28.144844055175781 8 -34.660385131835938 9 -36.603683471679688
		 10 -37.021865844726563 11 -35.395149230957031 12 -34.319293975830078 13 -36.614162445068359
		 14 -39.956008911132812 15 -42.078022003173828 16 -42.102531433105469 17 -41.18280029296875
		 18 -40.102790832519531 19 -39.646587371826172 20 -40.556903839111328 21 -42.115104675292969
		 22 -42.912979125976562 23 -42.227584838867188 24 -40.787494659423828 25 -39.35821533203125
		 26 -39.35821533203125;
createNode animCurveTU -n "Character1_Head_visibility";
	rename -uid "419AEC0B-4467-312D-E9EC-A79EA3EA1CEF";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "jaw_translateX";
	rename -uid "53EEBF5D-4632-3D37-D88F-5884CD726DD1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 14.004427909851074;
createNode animCurveTL -n "jaw_translateY";
	rename -uid "20E4323A-493A-9C14-CFA3-41A4B3F1B788";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.9922315685958317e-015;
createNode animCurveTL -n "jaw_translateZ";
	rename -uid "E78FD3E4-4C8A-BB02-78AA-1CA6FEB9FACA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9713712501773515e-015;
createNode animCurveTU -n "jaw_scaleX";
	rename -uid "98B5C945-438B-0B3A-B89F-F18D54765588";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleY";
	rename -uid "934D954C-4912-0E35-23A5-52ACFF20C2C3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleZ";
	rename -uid "C8025E6A-4714-1D54-2200-6D8B5BFAB195";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "jaw_rotateX";
	rename -uid "3988414D-40BA-A32B-3FD1-DC8CEFB51153";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -9.4167051315307617 1 30.536813735961914
		 2 57.071315765380859 3 62.186305999755859 4 64.849693298339844 5 65.257026672363281
		 6 63.603878021240234 7 60.085800170898437 8 54.898365020751953 9 48.237133026123047
		 10 21.329544067382813 11 -8.9252195358276367 12 -12.168392181396484 13 -7.8720960617065439
		 14 -6.4043469429016113 15 -4.7032341957092285 16 -3.6670713424682617 17 -3.5063071250915527
		 18 -3.7087206840515137 19 -4.0886178016662598 20 -4.4603066444396973 21 -4.6380949020385751
		 22 -4.5611872673034668 23 -4.3473539352416992 24 -4.0787506103515625 25 -3.8375320434570308
		 26 -3.8375320434570308;
createNode animCurveTA -n "jaw_rotateY";
	rename -uid "5213AB70-4477-5883-6B8A-C29637CE15EA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 6.6601108308916857e-013 1 6.6601108308916857e-013
		 2 6.6601108308916857e-013 3 6.6601108308916857e-013 4 6.6601108308916857e-013 5 6.6601108308916857e-013
		 6 6.6601108308916857e-013 7 6.6601108308916857e-013 8 6.6601108308916857e-013 9 6.6601108308916857e-013
		 10 6.6601108308916857e-013 11 6.6601108308916857e-013 12 6.6601108308916857e-013
		 13 6.6601108308916857e-013 14 6.6601108308916857e-013 15 6.6601108308916857e-013
		 16 6.5209795029053108e-013 17 -0.83351492881774902 18 -2.9110972881317139 19 -5.5983028411865234
		 20 -8.260685920715332 21 -10.263805389404297 22 -11.402740478515625 23 -12.080147743225098
		 24 -12.572944641113281 25 -13.158045768737793 26 -13.158045768737793;
createNode animCurveTA -n "jaw_rotateZ";
	rename -uid "89B10F99-436D-65B5-7425-B29C0037ACA7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 27 ".ktv[0:26]"  0 -3.204068412497596e-013 1 -3.204068412497596e-013
		 2 -3.204068412497596e-013 3 -3.204068412497596e-013 4 -3.204068412497596e-013 5 -3.204068412497596e-013
		 6 -3.204068412497596e-013 7 -3.204068412497596e-013 8 -3.204068412497596e-013 9 -3.204068412497596e-013
		 10 -3.204068412497596e-013 11 -3.204068412497596e-013 12 -3.204068412497596e-013
		 13 -3.204068412497596e-013 14 -3.204068412497596e-013 15 -3.204068412497596e-013
		 16 -3.6855596157424347e-013 17 0.19717121124267581 18 0.6926497220993042 19 1.3423826694488525
		 20 2.0023176670074463 21 2.5284018516540527 22 2.8878417015075684 23 3.1734232902526855
		 24 3.4294612407684326 25 3.7002706527709961 26 3.7002706527709961;
createNode animCurveTU -n "jaw_visibility";
	rename -uid "AD7847C6-429C-7665-4F5E-3893ACADAFB6";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "eyes_translateX";
	rename -uid "C367DCC8-4495-D835-80B7-08B4FA17963C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.515233039855957;
createNode animCurveTL -n "eyes_translateY";
	rename -uid "CE3304FC-4A6B-9A01-3D8B-2FBE4DC4ADC9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.655519485473633;
createNode animCurveTL -n "eyes_translateZ";
	rename -uid "57CDA72A-4846-EBA0-688D-E59FD973C31C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.16235609352588654;
createNode animCurveTU -n "eyes_scaleX";
	rename -uid "2994B7DA-44C7-5282-5A43-15BF8FB84526";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleY";
	rename -uid "8666FFFD-4094-F246-89DB-B3A7CBD3362F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleZ";
	rename -uid "42CE34DC-4D9A-5FE2-F095-E19D4A37A54B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "eyes_rotateX";
	rename -uid "8AA63844-4F65-A494-8AD4-2FABFD6E8F00";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "eyes_rotateY";
	rename -uid "EA78BDA2-40F1-254B-6C24-FC8CA1648609";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTA -n "eyes_rotateZ";
	rename -uid "8B3F274E-4262-29F3-19EC-90B45D50C3F4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTU -n "eyes_visibility";
	rename -uid "6775663C-4DD7-E2A0-EA39-C9B82615DC16";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "AB333DE9-41EF-5B97-7D52-CA887D554CE4";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "861E7B48-469B-553A-B8F1-E4A6FFCD9F7D";
createNode displayLayer -n "defaultLayer";
	rename -uid "737880F0-4589-A94A-14A8-A18CE9ECF24B";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "1973BBF6-4999-BBFB-C8FC-C99EA9FA27D6";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "8C9F0E13-4F7C-78A3-33D8-73AB1DC1BFA0";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "450E379F-486C-554C-5B19-45A07B3A6CAA";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 0\n                -polymeshes 1\n                -subdivSurfaces 0\n                -planes 0\n                -lights 0\n                -cameras 0\n                -controlVertices 0\n                -hulls 0\n                -grid 1\n                -imagePlane 0\n                -joints 1\n"
		+ "                -ikHandles 0\n                -deformers 0\n                -dynamics 0\n                -particleInstancers 0\n                -fluids 0\n                -hairSystems 0\n                -follicles 0\n                -nCloths 0\n                -nParticles 0\n                -nRigids 0\n                -dynamicConstraints 0\n                -locators 0\n                -manipulators 1\n                -pluginShapes 0\n                -dimensions 0\n                -handles 0\n                -pivots 0\n                -textures 0\n                -strokes 0\n                -motionTrails 0\n                -clipGhosts 0\n                -greasePencils 0\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1287\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 0 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 0\n            -polymeshes 1\n            -subdivSurfaces 0\n"
		+ "            -planes 0\n            -lights 0\n            -cameras 0\n            -controlVertices 0\n            -hulls 0\n            -grid 1\n            -imagePlane 0\n            -joints 1\n            -ikHandles 0\n            -deformers 0\n            -dynamics 0\n            -particleInstancers 0\n            -fluids 0\n            -hairSystems 0\n            -follicles 0\n            -nCloths 0\n            -nParticles 0\n            -nRigids 0\n            -dynamicConstraints 0\n            -locators 0\n            -manipulators 1\n            -pluginShapes 0\n            -dimensions 0\n            -handles 0\n            -pivots 0\n            -textures 0\n            -strokes 0\n            -motionTrails 0\n            -clipGhosts 0\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1287\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 0 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n"
		+ "                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n"
		+ "                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n"
		+ "                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n"
		+ "                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n"
		+ "                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 15 100 -ps 2 85 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 0\\n    -polymeshes 1\\n    -subdivSurfaces 0\\n    -planes 0\\n    -lights 0\\n    -cameras 0\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 1\\n    -ikHandles 0\\n    -deformers 0\\n    -dynamics 0\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 0\\n    -handles 0\\n    -pivots 0\\n    -textures 0\\n    -strokes 0\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1287\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 0 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 0\\n    -polymeshes 1\\n    -subdivSurfaces 0\\n    -planes 0\\n    -lights 0\\n    -cameras 0\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 1\\n    -ikHandles 0\\n    -deformers 0\\n    -dynamics 0\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 0\\n    -handles 0\\n    -pivots 0\\n    -textures 0\\n    -strokes 0\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1287\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 0 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "EF5AD209-4596-B5BE-7198-819AE63D3011";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 40 -ast 0 -aet 50 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 48;
	setAttr ".unw" 48;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
connectAttr "ref_frame.s" "Character1_Hips.is";
connectAttr "Character1_Hips_scaleX.o" "Character1_Hips.sx";
connectAttr "Character1_Hips_scaleY.o" "Character1_Hips.sy";
connectAttr "Character1_Hips_scaleZ.o" "Character1_Hips.sz";
connectAttr "Character1_Hips_translateX.o" "Character1_Hips.tx";
connectAttr "Character1_Hips_translateY.o" "Character1_Hips.ty";
connectAttr "Character1_Hips_translateZ.o" "Character1_Hips.tz";
connectAttr "Character1_Hips_rotateX.o" "Character1_Hips.rx";
connectAttr "Character1_Hips_rotateY.o" "Character1_Hips.ry";
connectAttr "Character1_Hips_rotateZ.o" "Character1_Hips.rz";
connectAttr "Character1_Hips_visibility.o" "Character1_Hips.v";
connectAttr "Character1_Hips.s" "Character1_LeftUpLeg.is";
connectAttr "Character1_LeftUpLeg_scaleX.o" "Character1_LeftUpLeg.sx";
connectAttr "Character1_LeftUpLeg_scaleY.o" "Character1_LeftUpLeg.sy";
connectAttr "Character1_LeftUpLeg_scaleZ.o" "Character1_LeftUpLeg.sz";
connectAttr "Character1_LeftUpLeg_translateX.o" "Character1_LeftUpLeg.tx";
connectAttr "Character1_LeftUpLeg_translateY.o" "Character1_LeftUpLeg.ty";
connectAttr "Character1_LeftUpLeg_translateZ.o" "Character1_LeftUpLeg.tz";
connectAttr "Character1_LeftUpLeg_rotateX.o" "Character1_LeftUpLeg.rx";
connectAttr "Character1_LeftUpLeg_rotateY.o" "Character1_LeftUpLeg.ry";
connectAttr "Character1_LeftUpLeg_rotateZ.o" "Character1_LeftUpLeg.rz";
connectAttr "Character1_LeftUpLeg_visibility.o" "Character1_LeftUpLeg.v";
connectAttr "Character1_LeftUpLeg.s" "Character1_LeftLeg.is";
connectAttr "Character1_LeftLeg_scaleX.o" "Character1_LeftLeg.sx";
connectAttr "Character1_LeftLeg_scaleY.o" "Character1_LeftLeg.sy";
connectAttr "Character1_LeftLeg_scaleZ.o" "Character1_LeftLeg.sz";
connectAttr "Character1_LeftLeg_translateX.o" "Character1_LeftLeg.tx";
connectAttr "Character1_LeftLeg_translateY.o" "Character1_LeftLeg.ty";
connectAttr "Character1_LeftLeg_translateZ.o" "Character1_LeftLeg.tz";
connectAttr "Character1_LeftLeg_rotateX.o" "Character1_LeftLeg.rx";
connectAttr "Character1_LeftLeg_rotateY.o" "Character1_LeftLeg.ry";
connectAttr "Character1_LeftLeg_rotateZ.o" "Character1_LeftLeg.rz";
connectAttr "Character1_LeftLeg_visibility.o" "Character1_LeftLeg.v";
connectAttr "Character1_LeftLeg.s" "Character1_LeftFoot.is";
connectAttr "Character1_LeftFoot_scaleX.o" "Character1_LeftFoot.sx";
connectAttr "Character1_LeftFoot_scaleY.o" "Character1_LeftFoot.sy";
connectAttr "Character1_LeftFoot_scaleZ.o" "Character1_LeftFoot.sz";
connectAttr "Character1_LeftFoot_translateX.o" "Character1_LeftFoot.tx";
connectAttr "Character1_LeftFoot_translateY.o" "Character1_LeftFoot.ty";
connectAttr "Character1_LeftFoot_translateZ.o" "Character1_LeftFoot.tz";
connectAttr "Character1_LeftFoot_rotateX.o" "Character1_LeftFoot.rx";
connectAttr "Character1_LeftFoot_rotateY.o" "Character1_LeftFoot.ry";
connectAttr "Character1_LeftFoot_rotateZ.o" "Character1_LeftFoot.rz";
connectAttr "Character1_LeftFoot_visibility.o" "Character1_LeftFoot.v";
connectAttr "Character1_LeftFoot.s" "Character1_LeftToeBase.is";
connectAttr "Character1_LeftToeBase_scaleX.o" "Character1_LeftToeBase.sx";
connectAttr "Character1_LeftToeBase_scaleY.o" "Character1_LeftToeBase.sy";
connectAttr "Character1_LeftToeBase_scaleZ.o" "Character1_LeftToeBase.sz";
connectAttr "Character1_LeftToeBase_translateX.o" "Character1_LeftToeBase.tx";
connectAttr "Character1_LeftToeBase_translateY.o" "Character1_LeftToeBase.ty";
connectAttr "Character1_LeftToeBase_translateZ.o" "Character1_LeftToeBase.tz";
connectAttr "Character1_LeftToeBase_rotateX.o" "Character1_LeftToeBase.rx";
connectAttr "Character1_LeftToeBase_rotateY.o" "Character1_LeftToeBase.ry";
connectAttr "Character1_LeftToeBase_rotateZ.o" "Character1_LeftToeBase.rz";
connectAttr "Character1_LeftToeBase_visibility.o" "Character1_LeftToeBase.v";
connectAttr "Character1_LeftToeBase.s" "Character1_LeftFootMiddle1.is";
connectAttr "Character1_LeftFootMiddle1_scaleX.o" "Character1_LeftFootMiddle1.sx"
		;
connectAttr "Character1_LeftFootMiddle1_scaleY.o" "Character1_LeftFootMiddle1.sy"
		;
connectAttr "Character1_LeftFootMiddle1_scaleZ.o" "Character1_LeftFootMiddle1.sz"
		;
connectAttr "Character1_LeftFootMiddle1_translateX.o" "Character1_LeftFootMiddle1.tx"
		;
connectAttr "Character1_LeftFootMiddle1_translateY.o" "Character1_LeftFootMiddle1.ty"
		;
connectAttr "Character1_LeftFootMiddle1_translateZ.o" "Character1_LeftFootMiddle1.tz"
		;
connectAttr "Character1_LeftFootMiddle1_rotateX.o" "Character1_LeftFootMiddle1.rx"
		;
connectAttr "Character1_LeftFootMiddle1_rotateY.o" "Character1_LeftFootMiddle1.ry"
		;
connectAttr "Character1_LeftFootMiddle1_rotateZ.o" "Character1_LeftFootMiddle1.rz"
		;
connectAttr "Character1_LeftFootMiddle1_visibility.o" "Character1_LeftFootMiddle1.v"
		;
connectAttr "Character1_LeftFootMiddle1.s" "Character1_LeftFootMiddle2.is";
connectAttr "Character1_LeftFootMiddle2_translateX.o" "Character1_LeftFootMiddle2.tx"
		;
connectAttr "Character1_LeftFootMiddle2_translateY.o" "Character1_LeftFootMiddle2.ty"
		;
connectAttr "Character1_LeftFootMiddle2_translateZ.o" "Character1_LeftFootMiddle2.tz"
		;
connectAttr "Character1_LeftFootMiddle2_scaleX.o" "Character1_LeftFootMiddle2.sx"
		;
connectAttr "Character1_LeftFootMiddle2_scaleY.o" "Character1_LeftFootMiddle2.sy"
		;
connectAttr "Character1_LeftFootMiddle2_scaleZ.o" "Character1_LeftFootMiddle2.sz"
		;
connectAttr "Character1_LeftFootMiddle2_rotateX.o" "Character1_LeftFootMiddle2.rx"
		;
connectAttr "Character1_LeftFootMiddle2_rotateY.o" "Character1_LeftFootMiddle2.ry"
		;
connectAttr "Character1_LeftFootMiddle2_rotateZ.o" "Character1_LeftFootMiddle2.rz"
		;
connectAttr "Character1_LeftFootMiddle2_visibility.o" "Character1_LeftFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_RightUpLeg.is";
connectAttr "Character1_RightUpLeg_scaleX.o" "Character1_RightUpLeg.sx";
connectAttr "Character1_RightUpLeg_scaleY.o" "Character1_RightUpLeg.sy";
connectAttr "Character1_RightUpLeg_scaleZ.o" "Character1_RightUpLeg.sz";
connectAttr "Character1_RightUpLeg_translateX.o" "Character1_RightUpLeg.tx";
connectAttr "Character1_RightUpLeg_translateY.o" "Character1_RightUpLeg.ty";
connectAttr "Character1_RightUpLeg_translateZ.o" "Character1_RightUpLeg.tz";
connectAttr "Character1_RightUpLeg_rotateX.o" "Character1_RightUpLeg.rx";
connectAttr "Character1_RightUpLeg_rotateY.o" "Character1_RightUpLeg.ry";
connectAttr "Character1_RightUpLeg_rotateZ.o" "Character1_RightUpLeg.rz";
connectAttr "Character1_RightUpLeg_visibility.o" "Character1_RightUpLeg.v";
connectAttr "Character1_RightUpLeg.s" "Character1_RightLeg.is";
connectAttr "Character1_RightLeg_scaleX.o" "Character1_RightLeg.sx";
connectAttr "Character1_RightLeg_scaleY.o" "Character1_RightLeg.sy";
connectAttr "Character1_RightLeg_scaleZ.o" "Character1_RightLeg.sz";
connectAttr "Character1_RightLeg_translateX.o" "Character1_RightLeg.tx";
connectAttr "Character1_RightLeg_translateY.o" "Character1_RightLeg.ty";
connectAttr "Character1_RightLeg_translateZ.o" "Character1_RightLeg.tz";
connectAttr "Character1_RightLeg_rotateX.o" "Character1_RightLeg.rx";
connectAttr "Character1_RightLeg_rotateY.o" "Character1_RightLeg.ry";
connectAttr "Character1_RightLeg_rotateZ.o" "Character1_RightLeg.rz";
connectAttr "Character1_RightLeg_visibility.o" "Character1_RightLeg.v";
connectAttr "Character1_RightLeg.s" "Character1_RightFoot.is";
connectAttr "Character1_RightFoot_scaleX.o" "Character1_RightFoot.sx";
connectAttr "Character1_RightFoot_scaleY.o" "Character1_RightFoot.sy";
connectAttr "Character1_RightFoot_scaleZ.o" "Character1_RightFoot.sz";
connectAttr "Character1_RightFoot_translateX.o" "Character1_RightFoot.tx";
connectAttr "Character1_RightFoot_translateY.o" "Character1_RightFoot.ty";
connectAttr "Character1_RightFoot_translateZ.o" "Character1_RightFoot.tz";
connectAttr "Character1_RightFoot_rotateX.o" "Character1_RightFoot.rx";
connectAttr "Character1_RightFoot_rotateY.o" "Character1_RightFoot.ry";
connectAttr "Character1_RightFoot_rotateZ.o" "Character1_RightFoot.rz";
connectAttr "Character1_RightFoot_visibility.o" "Character1_RightFoot.v";
connectAttr "Character1_RightFoot.s" "Character1_RightToeBase.is";
connectAttr "Character1_RightToeBase_scaleX.o" "Character1_RightToeBase.sx";
connectAttr "Character1_RightToeBase_scaleY.o" "Character1_RightToeBase.sy";
connectAttr "Character1_RightToeBase_scaleZ.o" "Character1_RightToeBase.sz";
connectAttr "Character1_RightToeBase_translateX.o" "Character1_RightToeBase.tx";
connectAttr "Character1_RightToeBase_translateY.o" "Character1_RightToeBase.ty";
connectAttr "Character1_RightToeBase_translateZ.o" "Character1_RightToeBase.tz";
connectAttr "Character1_RightToeBase_rotateX.o" "Character1_RightToeBase.rx";
connectAttr "Character1_RightToeBase_rotateY.o" "Character1_RightToeBase.ry";
connectAttr "Character1_RightToeBase_rotateZ.o" "Character1_RightToeBase.rz";
connectAttr "Character1_RightToeBase_visibility.o" "Character1_RightToeBase.v";
connectAttr "Character1_RightToeBase.s" "Character1_RightFootMiddle1.is";
connectAttr "Character1_RightFootMiddle1_scaleX.o" "Character1_RightFootMiddle1.sx"
		;
connectAttr "Character1_RightFootMiddle1_scaleY.o" "Character1_RightFootMiddle1.sy"
		;
connectAttr "Character1_RightFootMiddle1_scaleZ.o" "Character1_RightFootMiddle1.sz"
		;
connectAttr "Character1_RightFootMiddle1_translateX.o" "Character1_RightFootMiddle1.tx"
		;
connectAttr "Character1_RightFootMiddle1_translateY.o" "Character1_RightFootMiddle1.ty"
		;
connectAttr "Character1_RightFootMiddle1_translateZ.o" "Character1_RightFootMiddle1.tz"
		;
connectAttr "Character1_RightFootMiddle1_rotateX.o" "Character1_RightFootMiddle1.rx"
		;
connectAttr "Character1_RightFootMiddle1_rotateY.o" "Character1_RightFootMiddle1.ry"
		;
connectAttr "Character1_RightFootMiddle1_rotateZ.o" "Character1_RightFootMiddle1.rz"
		;
connectAttr "Character1_RightFootMiddle1_visibility.o" "Character1_RightFootMiddle1.v"
		;
connectAttr "Character1_RightFootMiddle1.s" "Character1_RightFootMiddle2.is";
connectAttr "Character1_RightFootMiddle2_translateX.o" "Character1_RightFootMiddle2.tx"
		;
connectAttr "Character1_RightFootMiddle2_translateY.o" "Character1_RightFootMiddle2.ty"
		;
connectAttr "Character1_RightFootMiddle2_translateZ.o" "Character1_RightFootMiddle2.tz"
		;
connectAttr "Character1_RightFootMiddle2_scaleX.o" "Character1_RightFootMiddle2.sx"
		;
connectAttr "Character1_RightFootMiddle2_scaleY.o" "Character1_RightFootMiddle2.sy"
		;
connectAttr "Character1_RightFootMiddle2_scaleZ.o" "Character1_RightFootMiddle2.sz"
		;
connectAttr "Character1_RightFootMiddle2_rotateX.o" "Character1_RightFootMiddle2.rx"
		;
connectAttr "Character1_RightFootMiddle2_rotateY.o" "Character1_RightFootMiddle2.ry"
		;
connectAttr "Character1_RightFootMiddle2_rotateZ.o" "Character1_RightFootMiddle2.rz"
		;
connectAttr "Character1_RightFootMiddle2_visibility.o" "Character1_RightFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_Spine.is";
connectAttr "Character1_Spine_scaleX.o" "Character1_Spine.sx";
connectAttr "Character1_Spine_scaleY.o" "Character1_Spine.sy";
connectAttr "Character1_Spine_scaleZ.o" "Character1_Spine.sz";
connectAttr "Character1_Spine_translateX.o" "Character1_Spine.tx";
connectAttr "Character1_Spine_translateY.o" "Character1_Spine.ty";
connectAttr "Character1_Spine_translateZ.o" "Character1_Spine.tz";
connectAttr "Character1_Spine_rotateX.o" "Character1_Spine.rx";
connectAttr "Character1_Spine_rotateY.o" "Character1_Spine.ry";
connectAttr "Character1_Spine_rotateZ.o" "Character1_Spine.rz";
connectAttr "Character1_Spine_visibility.o" "Character1_Spine.v";
connectAttr "Character1_Spine.s" "Character1_Spine1.is";
connectAttr "Character1_Spine1_scaleX.o" "Character1_Spine1.sx";
connectAttr "Character1_Spine1_scaleY.o" "Character1_Spine1.sy";
connectAttr "Character1_Spine1_scaleZ.o" "Character1_Spine1.sz";
connectAttr "Character1_Spine1_translateX.o" "Character1_Spine1.tx";
connectAttr "Character1_Spine1_translateY.o" "Character1_Spine1.ty";
connectAttr "Character1_Spine1_translateZ.o" "Character1_Spine1.tz";
connectAttr "Character1_Spine1_rotateX.o" "Character1_Spine1.rx";
connectAttr "Character1_Spine1_rotateY.o" "Character1_Spine1.ry";
connectAttr "Character1_Spine1_rotateZ.o" "Character1_Spine1.rz";
connectAttr "Character1_Spine1_visibility.o" "Character1_Spine1.v";
connectAttr "Character1_Spine1.s" "Character1_LeftShoulder.is";
connectAttr "Character1_LeftShoulder_scaleX.o" "Character1_LeftShoulder.sx";
connectAttr "Character1_LeftShoulder_scaleY.o" "Character1_LeftShoulder.sy";
connectAttr "Character1_LeftShoulder_scaleZ.o" "Character1_LeftShoulder.sz";
connectAttr "Character1_LeftShoulder_translateX.o" "Character1_LeftShoulder.tx";
connectAttr "Character1_LeftShoulder_translateY.o" "Character1_LeftShoulder.ty";
connectAttr "Character1_LeftShoulder_translateZ.o" "Character1_LeftShoulder.tz";
connectAttr "Character1_LeftShoulder_rotateX.o" "Character1_LeftShoulder.rx";
connectAttr "Character1_LeftShoulder_rotateY.o" "Character1_LeftShoulder.ry";
connectAttr "Character1_LeftShoulder_rotateZ.o" "Character1_LeftShoulder.rz";
connectAttr "Character1_LeftShoulder_visibility.o" "Character1_LeftShoulder.v";
connectAttr "Character1_LeftShoulder.s" "Character1_LeftArm.is";
connectAttr "Character1_LeftArm_scaleX.o" "Character1_LeftArm.sx";
connectAttr "Character1_LeftArm_scaleY.o" "Character1_LeftArm.sy";
connectAttr "Character1_LeftArm_scaleZ.o" "Character1_LeftArm.sz";
connectAttr "Character1_LeftArm_translateX.o" "Character1_LeftArm.tx";
connectAttr "Character1_LeftArm_translateY.o" "Character1_LeftArm.ty";
connectAttr "Character1_LeftArm_translateZ.o" "Character1_LeftArm.tz";
connectAttr "Character1_LeftArm_rotateX.o" "Character1_LeftArm.rx";
connectAttr "Character1_LeftArm_rotateY.o" "Character1_LeftArm.ry";
connectAttr "Character1_LeftArm_rotateZ.o" "Character1_LeftArm.rz";
connectAttr "Character1_LeftArm_visibility.o" "Character1_LeftArm.v";
connectAttr "Character1_LeftArm.s" "Character1_LeftForeArm.is";
connectAttr "Character1_LeftForeArm_scaleX.o" "Character1_LeftForeArm.sx";
connectAttr "Character1_LeftForeArm_scaleY.o" "Character1_LeftForeArm.sy";
connectAttr "Character1_LeftForeArm_scaleZ.o" "Character1_LeftForeArm.sz";
connectAttr "Character1_LeftForeArm_translateX.o" "Character1_LeftForeArm.tx";
connectAttr "Character1_LeftForeArm_translateY.o" "Character1_LeftForeArm.ty";
connectAttr "Character1_LeftForeArm_translateZ.o" "Character1_LeftForeArm.tz";
connectAttr "Character1_LeftForeArm_rotateX.o" "Character1_LeftForeArm.rx";
connectAttr "Character1_LeftForeArm_rotateY.o" "Character1_LeftForeArm.ry";
connectAttr "Character1_LeftForeArm_rotateZ.o" "Character1_LeftForeArm.rz";
connectAttr "Character1_LeftForeArm_visibility.o" "Character1_LeftForeArm.v";
connectAttr "Character1_LeftForeArm.s" "Character1_LeftHand.is";
connectAttr "Character1_LeftHand_scaleX.o" "Character1_LeftHand.sx";
connectAttr "Character1_LeftHand_scaleY.o" "Character1_LeftHand.sy";
connectAttr "Character1_LeftHand_scaleZ.o" "Character1_LeftHand.sz";
connectAttr "Character1_LeftHand_translateX.o" "Character1_LeftHand.tx";
connectAttr "Character1_LeftHand_translateY.o" "Character1_LeftHand.ty";
connectAttr "Character1_LeftHand_translateZ.o" "Character1_LeftHand.tz";
connectAttr "Character1_LeftHand_rotateX.o" "Character1_LeftHand.rx";
connectAttr "Character1_LeftHand_rotateY.o" "Character1_LeftHand.ry";
connectAttr "Character1_LeftHand_rotateZ.o" "Character1_LeftHand.rz";
connectAttr "Character1_LeftHand_visibility.o" "Character1_LeftHand.v";
connectAttr "Character1_LeftHand.s" "Character1_LeftHandThumb1.is";
connectAttr "Character1_LeftHandThumb1_scaleX.o" "Character1_LeftHandThumb1.sx";
connectAttr "Character1_LeftHandThumb1_scaleY.o" "Character1_LeftHandThumb1.sy";
connectAttr "Character1_LeftHandThumb1_scaleZ.o" "Character1_LeftHandThumb1.sz";
connectAttr "Character1_LeftHandThumb1_translateX.o" "Character1_LeftHandThumb1.tx"
		;
connectAttr "Character1_LeftHandThumb1_translateY.o" "Character1_LeftHandThumb1.ty"
		;
connectAttr "Character1_LeftHandThumb1_translateZ.o" "Character1_LeftHandThumb1.tz"
		;
connectAttr "Character1_LeftHandThumb1_rotateX.o" "Character1_LeftHandThumb1.rx"
		;
connectAttr "Character1_LeftHandThumb1_rotateY.o" "Character1_LeftHandThumb1.ry"
		;
connectAttr "Character1_LeftHandThumb1_rotateZ.o" "Character1_LeftHandThumb1.rz"
		;
connectAttr "Character1_LeftHandThumb1_visibility.o" "Character1_LeftHandThumb1.v"
		;
connectAttr "Character1_LeftHandThumb1.s" "Character1_LeftHandThumb2.is";
connectAttr "Character1_LeftHandThumb2_scaleX.o" "Character1_LeftHandThumb2.sx";
connectAttr "Character1_LeftHandThumb2_scaleY.o" "Character1_LeftHandThumb2.sy";
connectAttr "Character1_LeftHandThumb2_scaleZ.o" "Character1_LeftHandThumb2.sz";
connectAttr "Character1_LeftHandThumb2_translateX.o" "Character1_LeftHandThumb2.tx"
		;
connectAttr "Character1_LeftHandThumb2_translateY.o" "Character1_LeftHandThumb2.ty"
		;
connectAttr "Character1_LeftHandThumb2_translateZ.o" "Character1_LeftHandThumb2.tz"
		;
connectAttr "Character1_LeftHandThumb2_rotateX.o" "Character1_LeftHandThumb2.rx"
		;
connectAttr "Character1_LeftHandThumb2_rotateY.o" "Character1_LeftHandThumb2.ry"
		;
connectAttr "Character1_LeftHandThumb2_rotateZ.o" "Character1_LeftHandThumb2.rz"
		;
connectAttr "Character1_LeftHandThumb2_visibility.o" "Character1_LeftHandThumb2.v"
		;
connectAttr "Character1_LeftHandThumb2.s" "Character1_LeftHandThumb3.is";
connectAttr "Character1_LeftHandThumb3_translateX.o" "Character1_LeftHandThumb3.tx"
		;
connectAttr "Character1_LeftHandThumb3_translateY.o" "Character1_LeftHandThumb3.ty"
		;
connectAttr "Character1_LeftHandThumb3_translateZ.o" "Character1_LeftHandThumb3.tz"
		;
connectAttr "Character1_LeftHandThumb3_scaleX.o" "Character1_LeftHandThumb3.sx";
connectAttr "Character1_LeftHandThumb3_scaleY.o" "Character1_LeftHandThumb3.sy";
connectAttr "Character1_LeftHandThumb3_scaleZ.o" "Character1_LeftHandThumb3.sz";
connectAttr "Character1_LeftHandThumb3_rotateX.o" "Character1_LeftHandThumb3.rx"
		;
connectAttr "Character1_LeftHandThumb3_rotateY.o" "Character1_LeftHandThumb3.ry"
		;
connectAttr "Character1_LeftHandThumb3_rotateZ.o" "Character1_LeftHandThumb3.rz"
		;
connectAttr "Character1_LeftHandThumb3_visibility.o" "Character1_LeftHandThumb3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandIndex1.is";
connectAttr "Character1_LeftHandIndex1_scaleX.o" "Character1_LeftHandIndex1.sx";
connectAttr "Character1_LeftHandIndex1_scaleY.o" "Character1_LeftHandIndex1.sy";
connectAttr "Character1_LeftHandIndex1_scaleZ.o" "Character1_LeftHandIndex1.sz";
connectAttr "Character1_LeftHandIndex1_translateX.o" "Character1_LeftHandIndex1.tx"
		;
connectAttr "Character1_LeftHandIndex1_translateY.o" "Character1_LeftHandIndex1.ty"
		;
connectAttr "Character1_LeftHandIndex1_translateZ.o" "Character1_LeftHandIndex1.tz"
		;
connectAttr "Character1_LeftHandIndex1_rotateX.o" "Character1_LeftHandIndex1.rx"
		;
connectAttr "Character1_LeftHandIndex1_rotateY.o" "Character1_LeftHandIndex1.ry"
		;
connectAttr "Character1_LeftHandIndex1_rotateZ.o" "Character1_LeftHandIndex1.rz"
		;
connectAttr "Character1_LeftHandIndex1_visibility.o" "Character1_LeftHandIndex1.v"
		;
connectAttr "Character1_LeftHandIndex1.s" "Character1_LeftHandIndex2.is";
connectAttr "Character1_LeftHandIndex2_scaleX.o" "Character1_LeftHandIndex2.sx";
connectAttr "Character1_LeftHandIndex2_scaleY.o" "Character1_LeftHandIndex2.sy";
connectAttr "Character1_LeftHandIndex2_scaleZ.o" "Character1_LeftHandIndex2.sz";
connectAttr "Character1_LeftHandIndex2_translateX.o" "Character1_LeftHandIndex2.tx"
		;
connectAttr "Character1_LeftHandIndex2_translateY.o" "Character1_LeftHandIndex2.ty"
		;
connectAttr "Character1_LeftHandIndex2_translateZ.o" "Character1_LeftHandIndex2.tz"
		;
connectAttr "Character1_LeftHandIndex2_rotateX.o" "Character1_LeftHandIndex2.rx"
		;
connectAttr "Character1_LeftHandIndex2_rotateY.o" "Character1_LeftHandIndex2.ry"
		;
connectAttr "Character1_LeftHandIndex2_rotateZ.o" "Character1_LeftHandIndex2.rz"
		;
connectAttr "Character1_LeftHandIndex2_visibility.o" "Character1_LeftHandIndex2.v"
		;
connectAttr "Character1_LeftHandIndex2.s" "Character1_LeftHandIndex3.is";
connectAttr "Character1_LeftHandIndex3_translateX.o" "Character1_LeftHandIndex3.tx"
		;
connectAttr "Character1_LeftHandIndex3_translateY.o" "Character1_LeftHandIndex3.ty"
		;
connectAttr "Character1_LeftHandIndex3_translateZ.o" "Character1_LeftHandIndex3.tz"
		;
connectAttr "Character1_LeftHandIndex3_scaleX.o" "Character1_LeftHandIndex3.sx";
connectAttr "Character1_LeftHandIndex3_scaleY.o" "Character1_LeftHandIndex3.sy";
connectAttr "Character1_LeftHandIndex3_scaleZ.o" "Character1_LeftHandIndex3.sz";
connectAttr "Character1_LeftHandIndex3_rotateX.o" "Character1_LeftHandIndex3.rx"
		;
connectAttr "Character1_LeftHandIndex3_rotateY.o" "Character1_LeftHandIndex3.ry"
		;
connectAttr "Character1_LeftHandIndex3_rotateZ.o" "Character1_LeftHandIndex3.rz"
		;
connectAttr "Character1_LeftHandIndex3_visibility.o" "Character1_LeftHandIndex3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandRing1.is";
connectAttr "Character1_LeftHandRing1_scaleX.o" "Character1_LeftHandRing1.sx";
connectAttr "Character1_LeftHandRing1_scaleY.o" "Character1_LeftHandRing1.sy";
connectAttr "Character1_LeftHandRing1_scaleZ.o" "Character1_LeftHandRing1.sz";
connectAttr "Character1_LeftHandRing1_translateX.o" "Character1_LeftHandRing1.tx"
		;
connectAttr "Character1_LeftHandRing1_translateY.o" "Character1_LeftHandRing1.ty"
		;
connectAttr "Character1_LeftHandRing1_translateZ.o" "Character1_LeftHandRing1.tz"
		;
connectAttr "Character1_LeftHandRing1_rotateX.o" "Character1_LeftHandRing1.rx";
connectAttr "Character1_LeftHandRing1_rotateY.o" "Character1_LeftHandRing1.ry";
connectAttr "Character1_LeftHandRing1_rotateZ.o" "Character1_LeftHandRing1.rz";
connectAttr "Character1_LeftHandRing1_visibility.o" "Character1_LeftHandRing1.v"
		;
connectAttr "Character1_LeftHandRing1.s" "Character1_LeftHandRing2.is";
connectAttr "Character1_LeftHandRing2_scaleX.o" "Character1_LeftHandRing2.sx";
connectAttr "Character1_LeftHandRing2_scaleY.o" "Character1_LeftHandRing2.sy";
connectAttr "Character1_LeftHandRing2_scaleZ.o" "Character1_LeftHandRing2.sz";
connectAttr "Character1_LeftHandRing2_translateX.o" "Character1_LeftHandRing2.tx"
		;
connectAttr "Character1_LeftHandRing2_translateY.o" "Character1_LeftHandRing2.ty"
		;
connectAttr "Character1_LeftHandRing2_translateZ.o" "Character1_LeftHandRing2.tz"
		;
connectAttr "Character1_LeftHandRing2_rotateX.o" "Character1_LeftHandRing2.rx";
connectAttr "Character1_LeftHandRing2_rotateY.o" "Character1_LeftHandRing2.ry";
connectAttr "Character1_LeftHandRing2_rotateZ.o" "Character1_LeftHandRing2.rz";
connectAttr "Character1_LeftHandRing2_visibility.o" "Character1_LeftHandRing2.v"
		;
connectAttr "Character1_LeftHandRing2.s" "Character1_LeftHandRing3.is";
connectAttr "Character1_LeftHandRing3_translateX.o" "Character1_LeftHandRing3.tx"
		;
connectAttr "Character1_LeftHandRing3_translateY.o" "Character1_LeftHandRing3.ty"
		;
connectAttr "Character1_LeftHandRing3_translateZ.o" "Character1_LeftHandRing3.tz"
		;
connectAttr "Character1_LeftHandRing3_scaleX.o" "Character1_LeftHandRing3.sx";
connectAttr "Character1_LeftHandRing3_scaleY.o" "Character1_LeftHandRing3.sy";
connectAttr "Character1_LeftHandRing3_scaleZ.o" "Character1_LeftHandRing3.sz";
connectAttr "Character1_LeftHandRing3_rotateX.o" "Character1_LeftHandRing3.rx";
connectAttr "Character1_LeftHandRing3_rotateY.o" "Character1_LeftHandRing3.ry";
connectAttr "Character1_LeftHandRing3_rotateZ.o" "Character1_LeftHandRing3.rz";
connectAttr "Character1_LeftHandRing3_visibility.o" "Character1_LeftHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_RightShoulder.is";
connectAttr "Character1_RightShoulder_scaleX.o" "Character1_RightShoulder.sx";
connectAttr "Character1_RightShoulder_scaleY.o" "Character1_RightShoulder.sy";
connectAttr "Character1_RightShoulder_scaleZ.o" "Character1_RightShoulder.sz";
connectAttr "Character1_RightShoulder_translateX.o" "Character1_RightShoulder.tx"
		;
connectAttr "Character1_RightShoulder_translateY.o" "Character1_RightShoulder.ty"
		;
connectAttr "Character1_RightShoulder_translateZ.o" "Character1_RightShoulder.tz"
		;
connectAttr "Character1_RightShoulder_rotateX.o" "Character1_RightShoulder.rx";
connectAttr "Character1_RightShoulder_rotateY.o" "Character1_RightShoulder.ry";
connectAttr "Character1_RightShoulder_rotateZ.o" "Character1_RightShoulder.rz";
connectAttr "Character1_RightShoulder_visibility.o" "Character1_RightShoulder.v"
		;
connectAttr "Character1_RightShoulder.s" "Character1_RightArm.is";
connectAttr "Character1_RightArm_scaleX.o" "Character1_RightArm.sx";
connectAttr "Character1_RightArm_scaleY.o" "Character1_RightArm.sy";
connectAttr "Character1_RightArm_scaleZ.o" "Character1_RightArm.sz";
connectAttr "Character1_RightArm_translateX.o" "Character1_RightArm.tx";
connectAttr "Character1_RightArm_translateY.o" "Character1_RightArm.ty";
connectAttr "Character1_RightArm_translateZ.o" "Character1_RightArm.tz";
connectAttr "Character1_RightArm_rotateX.o" "Character1_RightArm.rx";
connectAttr "Character1_RightArm_rotateY.o" "Character1_RightArm.ry";
connectAttr "Character1_RightArm_rotateZ.o" "Character1_RightArm.rz";
connectAttr "Character1_RightArm_visibility.o" "Character1_RightArm.v";
connectAttr "Character1_RightArm.s" "Character1_RightForeArm.is";
connectAttr "Character1_RightForeArm_scaleX.o" "Character1_RightForeArm.sx";
connectAttr "Character1_RightForeArm_scaleY.o" "Character1_RightForeArm.sy";
connectAttr "Character1_RightForeArm_scaleZ.o" "Character1_RightForeArm.sz";
connectAttr "Character1_RightForeArm_translateX.o" "Character1_RightForeArm.tx";
connectAttr "Character1_RightForeArm_translateY.o" "Character1_RightForeArm.ty";
connectAttr "Character1_RightForeArm_translateZ.o" "Character1_RightForeArm.tz";
connectAttr "Character1_RightForeArm_rotateX.o" "Character1_RightForeArm.rx";
connectAttr "Character1_RightForeArm_rotateY.o" "Character1_RightForeArm.ry";
connectAttr "Character1_RightForeArm_rotateZ.o" "Character1_RightForeArm.rz";
connectAttr "Character1_RightForeArm_visibility.o" "Character1_RightForeArm.v";
connectAttr "Character1_RightForeArm.s" "Character1_RightHand.is";
connectAttr "Character1_RightHand_scaleX.o" "Character1_RightHand.sx";
connectAttr "Character1_RightHand_scaleY.o" "Character1_RightHand.sy";
connectAttr "Character1_RightHand_scaleZ.o" "Character1_RightHand.sz";
connectAttr "Character1_RightHand_translateX.o" "Character1_RightHand.tx";
connectAttr "Character1_RightHand_translateY.o" "Character1_RightHand.ty";
connectAttr "Character1_RightHand_translateZ.o" "Character1_RightHand.tz";
connectAttr "Character1_RightHand_rotateX.o" "Character1_RightHand.rx";
connectAttr "Character1_RightHand_rotateY.o" "Character1_RightHand.ry";
connectAttr "Character1_RightHand_rotateZ.o" "Character1_RightHand.rz";
connectAttr "Character1_RightHand_visibility.o" "Character1_RightHand.v";
connectAttr "Character1_RightHand.s" "Character1_RightHandThumb1.is";
connectAttr "Character1_RightHandThumb1_scaleX.o" "Character1_RightHandThumb1.sx"
		;
connectAttr "Character1_RightHandThumb1_scaleY.o" "Character1_RightHandThumb1.sy"
		;
connectAttr "Character1_RightHandThumb1_scaleZ.o" "Character1_RightHandThumb1.sz"
		;
connectAttr "Character1_RightHandThumb1_translateX.o" "Character1_RightHandThumb1.tx"
		;
connectAttr "Character1_RightHandThumb1_translateY.o" "Character1_RightHandThumb1.ty"
		;
connectAttr "Character1_RightHandThumb1_translateZ.o" "Character1_RightHandThumb1.tz"
		;
connectAttr "Character1_RightHandThumb1_rotateX.o" "Character1_RightHandThumb1.rx"
		;
connectAttr "Character1_RightHandThumb1_rotateY.o" "Character1_RightHandThumb1.ry"
		;
connectAttr "Character1_RightHandThumb1_rotateZ.o" "Character1_RightHandThumb1.rz"
		;
connectAttr "Character1_RightHandThumb1_visibility.o" "Character1_RightHandThumb1.v"
		;
connectAttr "Character1_RightHandThumb1.s" "Character1_RightHandThumb2.is";
connectAttr "Character1_RightHandThumb2_scaleX.o" "Character1_RightHandThumb2.sx"
		;
connectAttr "Character1_RightHandThumb2_scaleY.o" "Character1_RightHandThumb2.sy"
		;
connectAttr "Character1_RightHandThumb2_scaleZ.o" "Character1_RightHandThumb2.sz"
		;
connectAttr "Character1_RightHandThumb2_translateX.o" "Character1_RightHandThumb2.tx"
		;
connectAttr "Character1_RightHandThumb2_translateY.o" "Character1_RightHandThumb2.ty"
		;
connectAttr "Character1_RightHandThumb2_translateZ.o" "Character1_RightHandThumb2.tz"
		;
connectAttr "Character1_RightHandThumb2_rotateX.o" "Character1_RightHandThumb2.rx"
		;
connectAttr "Character1_RightHandThumb2_rotateY.o" "Character1_RightHandThumb2.ry"
		;
connectAttr "Character1_RightHandThumb2_rotateZ.o" "Character1_RightHandThumb2.rz"
		;
connectAttr "Character1_RightHandThumb2_visibility.o" "Character1_RightHandThumb2.v"
		;
connectAttr "Character1_RightHandThumb2.s" "Character1_RightHandThumb3.is";
connectAttr "Character1_RightHandThumb3_translateX.o" "Character1_RightHandThumb3.tx"
		;
connectAttr "Character1_RightHandThumb3_translateY.o" "Character1_RightHandThumb3.ty"
		;
connectAttr "Character1_RightHandThumb3_translateZ.o" "Character1_RightHandThumb3.tz"
		;
connectAttr "Character1_RightHandThumb3_scaleX.o" "Character1_RightHandThumb3.sx"
		;
connectAttr "Character1_RightHandThumb3_scaleY.o" "Character1_RightHandThumb3.sy"
		;
connectAttr "Character1_RightHandThumb3_scaleZ.o" "Character1_RightHandThumb3.sz"
		;
connectAttr "Character1_RightHandThumb3_rotateX.o" "Character1_RightHandThumb3.rx"
		;
connectAttr "Character1_RightHandThumb3_rotateY.o" "Character1_RightHandThumb3.ry"
		;
connectAttr "Character1_RightHandThumb3_rotateZ.o" "Character1_RightHandThumb3.rz"
		;
connectAttr "Character1_RightHandThumb3_visibility.o" "Character1_RightHandThumb3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandIndex1.is";
connectAttr "Character1_RightHandIndex1_scaleX.o" "Character1_RightHandIndex1.sx"
		;
connectAttr "Character1_RightHandIndex1_scaleY.o" "Character1_RightHandIndex1.sy"
		;
connectAttr "Character1_RightHandIndex1_scaleZ.o" "Character1_RightHandIndex1.sz"
		;
connectAttr "Character1_RightHandIndex1_translateX.o" "Character1_RightHandIndex1.tx"
		;
connectAttr "Character1_RightHandIndex1_translateY.o" "Character1_RightHandIndex1.ty"
		;
connectAttr "Character1_RightHandIndex1_translateZ.o" "Character1_RightHandIndex1.tz"
		;
connectAttr "Character1_RightHandIndex1_rotateX.o" "Character1_RightHandIndex1.rx"
		;
connectAttr "Character1_RightHandIndex1_rotateY.o" "Character1_RightHandIndex1.ry"
		;
connectAttr "Character1_RightHandIndex1_rotateZ.o" "Character1_RightHandIndex1.rz"
		;
connectAttr "Character1_RightHandIndex1_visibility.o" "Character1_RightHandIndex1.v"
		;
connectAttr "Character1_RightHandIndex1.s" "Character1_RightHandIndex2.is";
connectAttr "Character1_RightHandIndex2_scaleX.o" "Character1_RightHandIndex2.sx"
		;
connectAttr "Character1_RightHandIndex2_scaleY.o" "Character1_RightHandIndex2.sy"
		;
connectAttr "Character1_RightHandIndex2_scaleZ.o" "Character1_RightHandIndex2.sz"
		;
connectAttr "Character1_RightHandIndex2_translateX.o" "Character1_RightHandIndex2.tx"
		;
connectAttr "Character1_RightHandIndex2_translateY.o" "Character1_RightHandIndex2.ty"
		;
connectAttr "Character1_RightHandIndex2_translateZ.o" "Character1_RightHandIndex2.tz"
		;
connectAttr "Character1_RightHandIndex2_rotateX.o" "Character1_RightHandIndex2.rx"
		;
connectAttr "Character1_RightHandIndex2_rotateY.o" "Character1_RightHandIndex2.ry"
		;
connectAttr "Character1_RightHandIndex2_rotateZ.o" "Character1_RightHandIndex2.rz"
		;
connectAttr "Character1_RightHandIndex2_visibility.o" "Character1_RightHandIndex2.v"
		;
connectAttr "Character1_RightHandIndex2.s" "Character1_RightHandIndex3.is";
connectAttr "Character1_RightHandIndex3_translateX.o" "Character1_RightHandIndex3.tx"
		;
connectAttr "Character1_RightHandIndex3_translateY.o" "Character1_RightHandIndex3.ty"
		;
connectAttr "Character1_RightHandIndex3_translateZ.o" "Character1_RightHandIndex3.tz"
		;
connectAttr "Character1_RightHandIndex3_scaleX.o" "Character1_RightHandIndex3.sx"
		;
connectAttr "Character1_RightHandIndex3_scaleY.o" "Character1_RightHandIndex3.sy"
		;
connectAttr "Character1_RightHandIndex3_scaleZ.o" "Character1_RightHandIndex3.sz"
		;
connectAttr "Character1_RightHandIndex3_rotateX.o" "Character1_RightHandIndex3.rx"
		;
connectAttr "Character1_RightHandIndex3_rotateY.o" "Character1_RightHandIndex3.ry"
		;
connectAttr "Character1_RightHandIndex3_rotateZ.o" "Character1_RightHandIndex3.rz"
		;
connectAttr "Character1_RightHandIndex3_visibility.o" "Character1_RightHandIndex3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandRing1.is";
connectAttr "Character1_RightHandRing1_scaleX.o" "Character1_RightHandRing1.sx";
connectAttr "Character1_RightHandRing1_scaleY.o" "Character1_RightHandRing1.sy";
connectAttr "Character1_RightHandRing1_scaleZ.o" "Character1_RightHandRing1.sz";
connectAttr "Character1_RightHandRing1_translateX.o" "Character1_RightHandRing1.tx"
		;
connectAttr "Character1_RightHandRing1_translateY.o" "Character1_RightHandRing1.ty"
		;
connectAttr "Character1_RightHandRing1_translateZ.o" "Character1_RightHandRing1.tz"
		;
connectAttr "Character1_RightHandRing1_rotateX.o" "Character1_RightHandRing1.rx"
		;
connectAttr "Character1_RightHandRing1_rotateY.o" "Character1_RightHandRing1.ry"
		;
connectAttr "Character1_RightHandRing1_rotateZ.o" "Character1_RightHandRing1.rz"
		;
connectAttr "Character1_RightHandRing1_visibility.o" "Character1_RightHandRing1.v"
		;
connectAttr "Character1_RightHandRing1.s" "Character1_RightHandRing2.is";
connectAttr "Character1_RightHandRing2_scaleX.o" "Character1_RightHandRing2.sx";
connectAttr "Character1_RightHandRing2_scaleY.o" "Character1_RightHandRing2.sy";
connectAttr "Character1_RightHandRing2_scaleZ.o" "Character1_RightHandRing2.sz";
connectAttr "Character1_RightHandRing2_translateX.o" "Character1_RightHandRing2.tx"
		;
connectAttr "Character1_RightHandRing2_translateY.o" "Character1_RightHandRing2.ty"
		;
connectAttr "Character1_RightHandRing2_translateZ.o" "Character1_RightHandRing2.tz"
		;
connectAttr "Character1_RightHandRing2_rotateX.o" "Character1_RightHandRing2.rx"
		;
connectAttr "Character1_RightHandRing2_rotateY.o" "Character1_RightHandRing2.ry"
		;
connectAttr "Character1_RightHandRing2_rotateZ.o" "Character1_RightHandRing2.rz"
		;
connectAttr "Character1_RightHandRing2_visibility.o" "Character1_RightHandRing2.v"
		;
connectAttr "Character1_RightHandRing2.s" "Character1_RightHandRing3.is";
connectAttr "Character1_RightHandRing3_translateX.o" "Character1_RightHandRing3.tx"
		;
connectAttr "Character1_RightHandRing3_translateY.o" "Character1_RightHandRing3.ty"
		;
connectAttr "Character1_RightHandRing3_translateZ.o" "Character1_RightHandRing3.tz"
		;
connectAttr "Character1_RightHandRing3_scaleX.o" "Character1_RightHandRing3.sx";
connectAttr "Character1_RightHandRing3_scaleY.o" "Character1_RightHandRing3.sy";
connectAttr "Character1_RightHandRing3_scaleZ.o" "Character1_RightHandRing3.sz";
connectAttr "Character1_RightHandRing3_rotateX.o" "Character1_RightHandRing3.rx"
		;
connectAttr "Character1_RightHandRing3_rotateY.o" "Character1_RightHandRing3.ry"
		;
connectAttr "Character1_RightHandRing3_rotateZ.o" "Character1_RightHandRing3.rz"
		;
connectAttr "Character1_RightHandRing3_visibility.o" "Character1_RightHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_Neck.is";
connectAttr "Character1_Neck_scaleX.o" "Character1_Neck.sx";
connectAttr "Character1_Neck_scaleY.o" "Character1_Neck.sy";
connectAttr "Character1_Neck_scaleZ.o" "Character1_Neck.sz";
connectAttr "Character1_Neck_translateX.o" "Character1_Neck.tx";
connectAttr "Character1_Neck_translateY.o" "Character1_Neck.ty";
connectAttr "Character1_Neck_translateZ.o" "Character1_Neck.tz";
connectAttr "Character1_Neck_rotateX.o" "Character1_Neck.rx";
connectAttr "Character1_Neck_rotateY.o" "Character1_Neck.ry";
connectAttr "Character1_Neck_rotateZ.o" "Character1_Neck.rz";
connectAttr "Character1_Neck_visibility.o" "Character1_Neck.v";
connectAttr "Character1_Neck.s" "Character1_Head.is";
connectAttr "Character1_Head_scaleX.o" "Character1_Head.sx";
connectAttr "Character1_Head_scaleY.o" "Character1_Head.sy";
connectAttr "Character1_Head_scaleZ.o" "Character1_Head.sz";
connectAttr "Character1_Head_translateX.o" "Character1_Head.tx";
connectAttr "Character1_Head_translateY.o" "Character1_Head.ty";
connectAttr "Character1_Head_translateZ.o" "Character1_Head.tz";
connectAttr "Character1_Head_rotateX.o" "Character1_Head.rx";
connectAttr "Character1_Head_rotateY.o" "Character1_Head.ry";
connectAttr "Character1_Head_rotateZ.o" "Character1_Head.rz";
connectAttr "Character1_Head_visibility.o" "Character1_Head.v";
connectAttr "Character1_Head.s" "jaw.is";
connectAttr "jaw_lockInfluenceWeights.o" "jaw.liw";
connectAttr "jaw_translateX.o" "jaw.tx";
connectAttr "jaw_translateY.o" "jaw.ty";
connectAttr "jaw_translateZ.o" "jaw.tz";
connectAttr "jaw_scaleX.o" "jaw.sx";
connectAttr "jaw_scaleY.o" "jaw.sy";
connectAttr "jaw_scaleZ.o" "jaw.sz";
connectAttr "jaw_rotateX.o" "jaw.rx";
connectAttr "jaw_rotateY.o" "jaw.ry";
connectAttr "jaw_rotateZ.o" "jaw.rz";
connectAttr "jaw_visibility.o" "jaw.v";
connectAttr "Character1_Head.s" "eyes.is";
connectAttr "eyes_translateX.o" "eyes.tx";
connectAttr "eyes_translateY.o" "eyes.ty";
connectAttr "eyes_translateZ.o" "eyes.tz";
connectAttr "eyes_scaleX.o" "eyes.sx";
connectAttr "eyes_scaleY.o" "eyes.sy";
connectAttr "eyes_scaleZ.o" "eyes.sz";
connectAttr "eyes_rotateX.o" "eyes.rx";
connectAttr "eyes_rotateY.o" "eyes.ry";
connectAttr "eyes_rotateZ.o" "eyes.rz";
connectAttr "eyes_visibility.o" "eyes.v";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Enemy@Grunt_Death_01.ma
