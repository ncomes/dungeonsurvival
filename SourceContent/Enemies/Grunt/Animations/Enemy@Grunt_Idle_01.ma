//Maya ASCII 2016 scene
//Name: Enemy@Grunt_Idle_01.ma
//Last modified: Sun, Nov 22, 2015 10:33:42 AM
//Codeset: 1252
requires maya "2016";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "C729D16C-459D-1A8B-611F-609027D405F9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 171.37316086678911 126.24820751877886 182.25246783540877 ;
	setAttr ".r" -type "double3" -17.138352729603177 42.200000000000024 1.073344677329861e-015 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "8BA885AC-4D74-5C7E-C028-408534BB5CB1";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 290.92655869886818;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "5901370E-4C9A-1A3C-1B0C-EA8770F8480D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "B35D9CE1-4967-B569-9E65-6998150694D6";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "E8902417-424E-B77E-6AB3-22BB1AC279C7";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "CD1F9CA7-489A-3C36-22FC-9DB5250E1C33";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "D4005F3D-4FFC-4A3F-B0D3-059861255EA7";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "60918C6D-4730-9580-2F91-05AA2E97F7A8";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "ref_frame";
	rename -uid "CD67C9F2-40D5-1A7E-6868-89B446AF7575";
	addAttr -ci true -sn "refFrame" -ln "refFrame" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "Character1_Hips" -p "ref_frame";
	rename -uid "7F1D7C2C-4656-80B4-E639-C9854ED3C8A6";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 1.406920211983633 -3.3458391704689405 -22.822435086355146 ;
	setAttr ".ssc" no;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftUpLeg" -p "Character1_Hips";
	rename -uid "27ACC7C0-48CD-CCC5-3EA0-E59A7350B850";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 96.630252250634001 6.5132649289235918 97.599644355322027 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftLeg" -p "Character1_LeftUpLeg";
	rename -uid "4C3E183A-4ADB-0229-DDC1-8B89F9C64D8E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -129.48089468354581 -7.9067044581664252 177.65996829599328 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFoot" -p "Character1_LeftLeg";
	rename -uid "92844C04-43BE-AD5E-AD9B-0280C66504DA";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -82.075120143962664 29.940914059061775 128.00062508119765 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftToeBase" -p "Character1_LeftFoot";
	rename -uid "E8C7ECA6-4A7D-76D8-B66F-E7A0BBAFAC5B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -126.16837928120003 -7.059491002168353 35.858557690282048 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle1" -p "Character1_LeftToeBase";
	rename -uid "5AB731FF-47F8-B855-BF06-4381173453B6";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -100.08944018296057 24.928076457701248 56.143888543356006 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle2" -p "Character1_LeftFootMiddle1";
	rename -uid "04E6D403-41F1-57B2-808F-8A87B4A9AF7D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -39.508027973091373 -2.0582897454817326 31.259679900637106 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightUpLeg" -p "Character1_Hips";
	rename -uid "E366E78D-4C76-7928-6E85-C789FE8FBD39";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 11.416489881294433 -11.43673382189878 -152.25229525481123 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightLeg" -p "Character1_RightUpLeg";
	rename -uid "80034D64-4BB5-D214-896C-3E8F4A76A90B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -22.942928776539453 -35.956391389429932 101.2794675694641 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFoot" -p "Character1_RightLeg";
	rename -uid "5495907E-4AE2-129D-505F-23B1BCFC3E75";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 66.643786034585347 17.03863148951929 124.91120862192221 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightToeBase" -p "Character1_RightFoot";
	rename -uid "894BEDF0-4BD2-8017-541D-A2B149EFD2EC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -155.84366405206632 -5.8738653577349114 -93.73595023516998 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle1" -p "Character1_RightToeBase";
	rename -uid "68719E77-427A-AE6F-8631-73A924265900";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -166.06091029301223 54.415170601070251 -127.18425880983375 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle2" -p "Character1_RightFootMiddle1";
	rename -uid "DEEF23D4-4998-FCCA-C3D3-2EB7036EA2BC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -129.2483692316568 10.731788640430997 6.9892235317174372 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine" -p "Character1_Hips";
	rename -uid "3C4C8B09-416B-F518-DCA0-1EB970AE8B51";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 11.416489881294433 -11.43673382189878 -152.25229525481123 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine1" -p "Character1_Spine";
	rename -uid "CBA4C411-43EC-2E5C-6CBF-C982612356CF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 24.933471933369759 -37.782249520228419 -20.761597110625811 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftShoulder" -p "Character1_Spine1";
	rename -uid "D169340D-4E6B-06AE-B843-5A9BE17D30A0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 144.67442469508543 70.793071067570196 19.807411116001759 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftArm" -p "Character1_LeftShoulder";
	rename -uid "81B70C2A-43AA-3129-8EE4-B69185D01451";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 99.384574925726014 -60.006523708111558 170.15034313799799 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftForeArm" -p "Character1_LeftArm";
	rename -uid "78862590-441D-AB03-D1F2-20B50AA21917";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 70.061799635480924 -4.8580147780287897 21.798459685601706 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHand" -p "Character1_LeftForeArm";
	rename -uid "CAC03EC7-4DA8-08D0-5BB2-7C8A48DE359F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.9708494230587 -13.075378107872792 150.36023365917421 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb1" -p "Character1_LeftHand";
	rename -uid "1414717A-4F33-4BF8-CB4E-069EAF3BD406";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -65.748924167512314 39.787402356574312 166.7746235430387 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb2" -p "Character1_LeftHandThumb1";
	rename -uid "C79F0316-4141-D229-D6FA-49B26165A28B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -39.861597330550808 6.7221908644292059 68.670730027774553 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb3" -p "Character1_LeftHandThumb2";
	rename -uid "B3D5EA16-4AE4-CF5C-DE1B-47AB95BCB7B0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -120.21671274765816 -26.239331955300646 57.413978285788289 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex1" -p "Character1_LeftHand";
	rename -uid "F7C047FD-47D7-6A65-25E5-2397F6F5FB2A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -152.38220651039205 4.806304023778381 48.842756939944849 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex2" -p "Character1_LeftHandIndex1";
	rename -uid "12B51C82-4818-44D0-A4DA-539854E7505C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 176.30914278667802 -13.434888179376443 97.633542306389202 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex3" -p "Character1_LeftHandIndex2";
	rename -uid "BB2A70C9-4E0E-A37E-E5AA-56AF37C90F65";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -102.43860404365057 -60.659303181068822 -152.9472923666747 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing1" -p "Character1_LeftHand";
	rename -uid "C6D4D9EF-47E6-DBF5-3C0F-6EAFCF7E33C1";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -31.516946849722316 2.5758733401430476 -137.43155166436202 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing2" -p "Character1_LeftHandRing1";
	rename -uid "BCAF144F-4297-23E5-7372-FD9FBDF832B8";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -37.632109434888541 57.372964012610872 -137.42548313576771 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing3" -p "Character1_LeftHandRing2";
	rename -uid "0EFD6D14-4AB0-B218-814F-BB8127B98AB5";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -16.443988862208798 8.3290143241887424 -59.438981507600595 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightShoulder" -p "Character1_Spine1";
	rename -uid "EFF2DA0A-4F2F-4A47-C1E5-BCAF4465A109";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -174.37324535156188 -81.21351031321781 -130.38505903740904 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightArm" -p "Character1_RightShoulder";
	rename -uid "9FBE441F-4C88-A791-6644-94A26AF841FD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -104.33239449525594 -37.101615257968433 20.137114731244228 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightForeArm" -p "Character1_RightArm";
	rename -uid "AFDF3344-41E7-C291-AC93-3ABB377D196C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -43.743657547705759 -9.6726323149773243 103.83730983340543 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHand" -p "Character1_RightForeArm";
	rename -uid "7BFAA445-40AB-F50F-3BAF-79B30CDFFDC9";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 140.03960701773187 13.516449256889247 -135.99675148159383 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb1" -p "Character1_RightHand";
	rename -uid "69E3B035-4B40-4033-CD41-64A20BA2CE8D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.10974980768816 71.94515848234029 -178.78334614629264 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb2" -p "Character1_RightHandThumb1";
	rename -uid "80CB6E54-4609-FE7F-3A9F-98B4CC6EAA7E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.2056241799594538 33.542113270229827 -130.48989956049317 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb3" -p "Character1_RightHandThumb2";
	rename -uid "E94B00BA-493A-8EAD-E9D3-D08D0ABA3CCB";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 134.7539820196863 -70.074251222974951 75.428204415317296 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex1" -p "Character1_RightHand";
	rename -uid "60A80227-42A3-853C-FD5E-D3B4954CB70F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.10974980768816 71.94515848234029 -178.78334614629264 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex2" -p "Character1_RightHandIndex1";
	rename -uid "C3AA9B7E-4ABF-FF22-A8FB-548B43889D55";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.2057394778445882 33.54331930430493 -130.49010822234442 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex3" -p "Character1_RightHandIndex2";
	rename -uid "52A4EE87-42FA-D26B-FA7D-E1ABA9305F3A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 134.79643182941854 -70.056599207814415 75.383915829770686 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing1" -p "Character1_RightHand";
	rename -uid "803505C1-4427-D6B5-5CAD-358E6118279C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.10974980768816 71.94515848234029 -178.78334614629264 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing2" -p "Character1_RightHandRing1";
	rename -uid "87C6C8E1-45AE-C43C-63AD-3FB1C8C2AA25";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.2056241799594538 33.542113270229827 -130.48989956049317 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing3" -p "Character1_RightHandRing2";
	rename -uid "47AA4849-4790-73C3-DE73-D282CB6FB214";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 134.7539820196863 -70.074251222974951 75.428204415317296 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Neck" -p "Character1_Spine1";
	rename -uid "475DFD77-4CAA-FC09-1C57-FAAD779D2467";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 161.13402532275541 31.934232202611735 171.90006021381112 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Head" -p "Character1_Neck";
	rename -uid "D70993EF-457B-56D0-869E-209FD4A55257";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -18.098521216998183 -9.9246993917445305 116.26360516393567 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "jaw" -p "Character1_Head";
	rename -uid "0C676306-4972-24D5-6E7B-6EB4F2D5264F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.667422224424381 90 0 ;
	setAttr ".radi" 6;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "eyes" -p "Character1_Head";
	rename -uid "1C39F13C-442A-5F57-4E66-2DBE329CA326";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.667422224424381 90 0 ;
	setAttr ".radi" 4.5;
	setAttr ".fbxID" 5;
createNode animCurveTU -n "jaw_lockInfluenceWeights";
	rename -uid "A211E4AB-4FE0-8594-82D8-5F99F31E2489";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "Character1_Hips_translateX";
	rename -uid "8F517569-4E3A-258E-AC64-878DD460E47E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -1.9489617347717285 1 -2.0362780094146729
		 2 -2.1136906147003174 3 -2.1816720962524414 4 -2.2406935691833496 5 -2.2912282943725586
		 6 -2.3337481021881104 7 -2.3687248229980469 8 -2.3966312408447266 9 -2.4179391860961914
		 10 -2.4331207275390625 11 -2.4426484107971191 12 -2.4469940662384033 13 -2.4466297626495361
		 14 -2.4420280456542969 15 -2.4321751594543457 16 -2.4158358573913574 17 -2.3931519985198975
		 18 -2.3642642498016357 19 -2.3293135166168213 20 -2.2884407043457031 21 -2.2417874336242676
		 22 -2.1894938945770264 23 -2.1317019462585449 24 -2.0685522556304932 25 -2.000185489654541
		 26 -1.9267431497573853 27 -1.8483662605285645 28 -1.7651954889297485 29 -1.6756386756896973
		 30 -1.578162670135498 31 -1.472997784614563 32 -1.3603742122650146 33 -1.2405221462249756
		 34 -1.1136716604232788 35 -0.98005294799804688 36 -0.83989626169204712 37 -0.69343191385269165
		 38 -0.54088985919952393 39 -0.38250038027763367 40 -0.21849370002746582 41 -0.074431523680686951
		 42 0.026998557150363922 43 0.089877553284168243 44 0.11828650534152985 45 0.11630645394325256
		 46 0.088018432259559631 47 0.037503473460674286 48 -0.031157389283180237 49 -0.1138831228017807
		 50 -0.20659270882606506 51 -0.30520510673522949 52 -0.40563926100730896 53 -0.50381416082382202
		 54 -0.59564876556396484 55 -0.67706209421157837 56 -0.7439730167388916 57 -0.80633926391601563
		 58 -0.87725603580474854 59 -0.95549750328063965 60 -1.0398375988006592 61 -1.129050612449646
		 62 -1.2219103574752808 63 -1.3171911239624023 64 -1.4136667251586914 65 -1.5101113319396973
		 66 -1.6052989959716797 67 -1.6980037689208984 68 -1.7869998216629028 69 -1.8710612058639526
		 70 -1.9489617347717285;
createNode animCurveTL -n "Character1_Hips_translateY";
	rename -uid "ACFBDC9E-422D-83A0-7624-0592487412E4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 42.193660736083984 1 42.194084167480469
		 2 42.162769317626953 3 42.103546142578125 4 42.020267486572266 5 41.916763305664062
		 6 41.796878814697266 7 41.664451599121094 8 41.523323059082031 9 41.377330780029297
		 10 41.230319976806641 11 41.086116790771484 12 40.948581695556641 13 40.821540832519531
		 14 40.708839416503906 15 40.609970092773438 16 40.521419525146484 17 40.4425048828125
		 18 40.372554779052734 19 40.310886383056641 20 40.256820678710938 21 40.209678649902344
		 22 40.168785095214844 23 40.133460998535156 24 40.10302734375 25 40.076808929443359
		 26 40.054122924804688 27 40.034290313720703 28 40.016635894775391 29 40.031082153320313
		 30 40.101097106933594 31 40.216354370117187 32 40.366508483886719 33 40.541213989257813
		 34 40.730133056640625 35 40.922927856445313 36 41.1092529296875 37 41.278770446777344
		 38 41.421142578125 39 41.526020050048828 40 41.58306884765625 41 41.595748901367188
		 42 41.577346801757813 43 41.5322265625 44 41.464778900146484 45 41.379371643066406
		 46 41.280380249023438 47 41.172195434570313 48 41.059181213378906 49 40.945720672607422
		 50 40.836189270019531 51 40.734966278076172 52 40.646430969238281 53 40.574951171875
		 54 40.524913787841797 55 40.500698089599609 56 40.506668090820312 57 40.549713134765625
		 58 40.633377075195313 59 40.750473022460938 60 40.893829345703125 61 41.056278228759766
		 62 41.230640411376953 63 41.409744262695312 64 41.586418151855469 65 41.753486633300781
		 66 41.903774261474609 67 42.030109405517578 68 42.125316619873047 69 42.182224273681641
		 70 42.193660736083984;
createNode animCurveTL -n "Character1_Hips_translateZ";
	rename -uid "7B009E05-463E-D79A-CE9F-E0A09AD0E806";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -9.694854736328125 1 -9.7079677581787109
		 2 -9.7183551788330078 3 -9.7262392044067383 4 -9.7318477630615234 5 -9.7354059219360352
		 6 -9.7371387481689453 7 -9.7372703552246094 8 -9.7360296249389648 9 -9.7336397171020508
		 10 -9.7303256988525391 11 -9.726313591003418 12 -9.7218303680419922 13 -9.7170991897583008
		 14 -9.7123479843139648 15 -9.7121343612670898 16 -9.7200593948364258 17 -9.7349147796630859
		 18 -9.7554893493652344 19 -9.780573844909668 20 -9.8089570999145508 21 -9.8394298553466797
		 22 -9.8707809448242187 23 -9.9018001556396484 24 -9.931279182434082 25 -9.9580059051513672
		 26 -9.9807701110839844 27 -9.9983634948730469 28 -10.009573936462402 29 -9.9954490661621094
		 30 -9.9427480697631836 31 -9.8588294982910156 32 -9.7510528564453125 33 -9.626774787902832
		 34 -9.4933547973632812 35 -9.3581523895263672 36 -9.2285242080688477 37 -9.1118297576904297
		 38 -9.0154266357421875 39 -8.9466753005981445 40 -8.9129323959350586 41 -8.8951740264892578
		 42 -8.8706502914428711 43 -8.8411331176757813 44 -8.8083944320678711 45 -8.7742071151733398
		 46 -8.7403411865234375 47 -8.7085685729980469 48 -8.6806631088256836 49 -8.6583967208862305
		 50 -8.6435384750366211 51 -8.6378612518310547 52 -8.6431388854980469 53 -8.6611404418945313
		 54 -8.6936407089233398 55 -8.7424087524414062 56 -8.8092174530029297 57 -8.8878297805786133
		 58 -8.9698610305786133 59 -9.0538663864135742 60 -9.1384010314941406 61 -9.2220191955566406
		 62 -9.3032760620117187 63 -9.3807249069213867 64 -9.4529209136962891 65 -9.5184202194213867
		 66 -9.575775146484375 67 -9.6235418319702148 68 -9.6602745056152344 69 -9.6845264434814453
		 70 -9.694854736328125;
createNode animCurveTU -n "Character1_Hips_scaleX";
	rename -uid "685EF5F2-441B-3869-EF4E-8F99D2817B63";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_Hips_scaleY";
	rename -uid "BD365A6B-4A76-0F0B-ACD5-A4944E4A5872";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_Hips_scaleZ";
	rename -uid "49D0F1C3-49FA-48CE-C169-3E8DBC965DC6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Hips_rotateX";
	rename -uid "1203F095-41FE-94FC-83E0-62B33BF8B9A8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -11.560624122619629 1 -11.467764854431152
		 2 -11.337780952453613 3 -11.176394462585449 4 -10.989344596862793 5 -10.782368659973145
		 6 -10.561189651489258 7 -10.331504821777344 8 -10.098983764648437 9 -9.8692636489868164
		 10 -9.6479549407958984 11 -9.4406414031982422 12 -9.2528953552246094 13 -9.0902824401855469
		 14 -8.9583835601806641 15 -8.8567781448364258 16 -8.7805919647216797 17 -8.7275123596191406
		 18 -8.6952447891235352 19 -8.6814966201782227 20 -8.6839628219604492 21 -8.7003145217895508
		 22 -8.7281894683837891 23 -8.7651853561401367 24 -8.8088521957397461 25 -8.8566932678222656
		 26 -8.906163215637207 27 -8.9546718597412109 28 -8.9995889663696289 29 -9.0405082702636719
		 30 -9.0811185836791992 31 -9.1211681365966797 32 -9.1604061126708984 33 -9.1985816955566406
		 34 -9.235447883605957 35 -9.2707614898681641 36 -9.3042821884155273 37 -9.335780143737793
		 38 -9.3650321960449219 39 -9.3918247222900391 40 -9.415959358215332 41 -9.4648237228393555
		 42 -9.5621356964111328 43 -9.7023181915283203 44 -9.8798723220825195 45 -10.08931827545166
		 46 -10.325148582458496 47 -10.581784248352051 48 -10.853545188903809 49 -11.134623527526855
		 50 -11.419069290161133 51 -11.700788497924805 52 -11.973543167114258 53 -12.230972290039063
		 54 -12.46661376953125 55 -12.673942565917969 56 -12.846417427062988 57 -12.963170051574707
		 58 -13.006402969360352 59 -12.986312866210938 60 -12.91317081451416 61 -12.797313690185547
		 62 -12.649136543273926 63 -12.479083061218262 64 -12.297639846801758 65 -12.11533260345459
		 66 -11.942717552185059 67 -11.790369033813477 68 -11.668869018554688 69 -11.588781356811523
		 70 -11.560624122619629;
createNode animCurveTA -n "Character1_Hips_rotateY";
	rename -uid "41F5351B-4156-C690-3104-B4A145136598";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 12.123907089233398 1 12.139401435852051
		 2 12.112446784973145 3 12.050169944763184 4 11.959694862365723 5 11.848151206970215
		 6 11.722677230834961 7 11.590429306030273 8 11.45858097076416 9 11.334324836730957
		 10 11.22486686706543 11 11.137431144714355 12 11.079246520996094 13 11.057549476623535
		 14 11.079573631286621 15 11.139253616333008 16 11.224742889404297 17 11.33269214630127
		 18 11.459718704223633 19 11.602416038513184 20 11.757363319396973 21 11.921138763427734
		 22 12.090322494506836 23 12.261505126953125 24 12.431293487548828 25 12.596311569213867
		 26 12.753207206726074 27 12.898650169372559 28 13.029332160949707 29 13.146825790405273
		 30 13.252926826477051 31 13.347114562988281 32 13.428865432739258 33 13.497662544250488
		 34 13.552986145019531 35 13.594321250915527 36 13.621150970458984 37 13.632960319519043
		 38 13.629233360290527 39 13.609454154968262 40 13.573107719421387 41 13.483432769775391
		 42 13.311813354492188 43 13.069804191589355 44 12.769015312194824 45 12.421086311340332
		 46 12.03765869140625 47 11.630350112915039 48 11.210729598999023 49 10.790297508239746
		 50 10.380464553833008 51 9.9925365447998047 52 9.6377058029174805 53 9.327052116394043
		 54 9.0715484619140625 55 8.8820829391479492 56 8.7694816589355469 57 8.7631969451904297
		 58 8.8621578216552734 59 9.0493793487548828 60 9.3079643249511719 61 9.6210794448852539
		 62 9.9719257354736328 63 10.343723297119141 64 10.71970272064209 65 11.083090782165527
		 66 11.417115211486816 67 11.704998016357422 68 11.929957389831543 69 12.075197219848633
		 70 12.123907089233398;
createNode animCurveTA -n "Character1_Hips_rotateZ";
	rename -uid "D9F52F63-44FC-3CF6-32E2-B79F4629561F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -4.8187236785888672 1 -4.8297281265258789
		 2 -4.8299741744995117 3 -4.8214154243469238 4 -4.806023120880127 5 -4.7857470512390137
		 6 -4.7624897956848145 7 -4.7380824089050293 8 -4.7142744064331055 9 -4.6927280426025391
		 10 -4.6750192642211914 11 -4.6626482009887695 12 -4.6570572853088379 13 -4.6596565246582031
		 14 -4.6718549728393555 15 -4.7038559913635254 16 -4.7669529914855957 17 -4.8576803207397461
		 18 -4.9726004600524902 19 -5.1082944869995117 20 -5.261347770690918 21 -5.4283394813537598
		 22 -5.6058335304260254 23 -5.7903742790222168 24 -5.9784770011901855 25 -6.1666297912597656
		 26 -6.351287841796875 27 -6.5288786888122559 28 -6.69580078125 29 -6.8521862030029297
		 30 -7.0008821487426758 31 -7.1411318778991699 32 -7.2721762657165527 33 -7.393256664276123
		 34 -7.5036139488220215 35 -7.6024894714355478 36 -7.68912649154663 37 -7.7627692222595215
		 38 -7.8226656913757315 39 -7.8680677413940421 40 -7.8982305526733398 41 -7.8950982093811035
		 42 -7.8447890281677246 43 -7.7524886131286612 44 -7.6233258247375488 45 -7.4624361991882324
		 46 -7.2750096321105948 47 -7.0663352012634277 48 -6.8418340682983398 49 -6.6070799827575684
		 50 -6.3678154945373535 51 -6.1299524307250977 52 -5.8995676040649414 53 -5.6828808784484863
		 54 -5.4862303733825684 55 -5.3160309791564941 56 -5.1787247657775879 57 -5.0714540481567383
		 58 -4.9829506874084473 59 -4.9117040634155273 60 -4.8559679985046387 61 -4.8139181137084961
		 62 -4.7837719917297363 63 -4.7638864517211914 64 -4.7528204917907715 65 -4.7493696212768555
		 66 -4.7525715827941895 67 -4.7616786956787109 68 -4.776099681854248 69 -4.7953085899353027
		 70 -4.8187236785888672;
createNode animCurveTU -n "Character1_Hips_visibility";
	rename -uid "35214870-44A4-8243-7279-0C829C182DED";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftUpLeg_translateX";
	rename -uid "07E0CD4B-44DB-BE29-1CA2-B6BD875A9524";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.596480369567871;
createNode animCurveTL -n "Character1_LeftUpLeg_translateY";
	rename -uid "25F86D65-442E-AB2A-0508-45A700373870";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.315338134765625;
createNode animCurveTL -n "Character1_LeftUpLeg_translateZ";
	rename -uid "888AE0B6-4449-864F-A46E-13941E090463";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.0558223724365234;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleX";
	rename -uid "6A253B08-423A-E770-260A-E1A5105C4867";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleY";
	rename -uid "CEEE9EDF-4FC7-B386-59FC-C6B7339C6A6E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleZ";
	rename -uid "6708C7D5-47E8-B90F-FCE4-C78108F497B4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateX";
	rename -uid "37EB8729-4FCA-5283-17BD-1189621ED658";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -9.9912195205688477 1 -10.111742973327637
		 2 -10.244756698608398 3 -10.389334678649902 4 -10.54411792755127 5 -10.707550048828125
		 6 -10.877992630004883 7 -11.053872108459473 8 -11.23377799987793 9 -11.416522979736328
		 10 -11.601173400878906 11 -11.787091255187988 12 -11.973861694335937 13 -12.161323547363281
		 14 -12.349468231201172 15 -12.565659523010254 16 -12.832965850830078 17 -13.138911247253418
		 18 -13.471078872680664 19 -13.817192077636719 20 -14.165115356445313 21 -14.502890586853029
		 22 -14.818723678588869 23 -15.100996017456055 24 -15.338241577148438 25 -15.519110679626465
		 26 -15.632335662841797 27 -15.666683197021486 28 -15.610869407653809 29 -15.375177383422853
		 30 -14.903895378112793 31 -14.236796379089355 32 -13.412956237792969 33 -12.471593856811523
		 34 -11.452766418457031 35 -10.397899627685547 36 -9.3501291275024414 37 -8.3543672561645508
		 38 -7.4570770263671875 39 -6.7057070732116699 40 -6.1477012634277344 41 -5.7012958526611328
		 42 -5.2575054168701172 43 -4.8213529586791992 44 -4.3973212242126465 45 -3.9896643161773682
		 46 -3.6025512218475342 47 -3.2402191162109375 48 -2.9071483612060547 49 -2.6080689430236816
		 50 -2.3480415344238281 51 -2.1324386596679687 52 -1.9669215679168701 53 -1.8573735952377319
		 54 -1.8097696304321289 55 -1.8300776481628418 56 -1.9241274595260618 57 -2.1192066669464111
		 58 -2.4279742240905762 59 -2.8362107276916504 60 -3.3297810554504395 61 -3.8947577476501465
		 62 -4.5174832344055176 63 -5.1845455169677734 64 -5.882746696472168 65 -6.5990405082702637
		 66 -7.3204269409179687 67 -8.0338335037231445 68 -8.7259864807128906 69 -9.3832101821899414
		 70 -9.9912195205688477;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateY";
	rename -uid "73CCCD89-4F26-9137-A899-138B28970B02";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 16.004459381103516 1 16.092960357666016
		 2 16.160383224487305 3 16.208599090576172 4 16.239486694335938 5 16.254905700683594
		 6 16.256677627563477 7 16.246561050415039 8 16.226242065429688 9 16.197320938110352
		 10 16.161293029785156 11 16.119550704956055 12 16.073379516601563 13 16.023942947387695
		 14 15.972298622131348 15 15.913500785827638 16 15.846766471862795 17 15.774563789367676
		 18 15.69953727722168 19 15.624467849731447 20 15.552271842956543 21 15.485980987548828
		 22 15.428750991821287 23 15.383868217468263 24 15.354757308959961 25 15.345016479492189
		 26 15.358449935913084 27 15.399101257324219 28 15.471321105957033 29 15.620550155639648
		 30 15.876499176025391 31 16.216316223144531 32 16.616228103637695 33 17.051982879638672
		 34 17.499240875244141 35 17.934001922607422 36 18.332952499389648 37 18.673709869384766
		 38 18.934909820556641 39 19.09599494934082 40 19.136724472045898 41 19.138650894165039
		 42 19.190641403198242 43 19.283124923706055 44 19.406639099121094 45 19.551923751831055
		 46 19.709991455078125 47 19.872186660766602 48 20.030258178710938 49 20.176387786865234
		 50 20.303220748901367 51 20.403861999511719 52 20.471832275390625 53 20.501031875610352
		 54 20.485620498657227 55 20.419927597045898 56 20.298290252685547 57 20.112592697143555
		 58 19.859325408935547 59 19.551340103149414 60 19.201391220092773 61 18.822149276733398
		 62 18.42616081237793 63 18.025733947753906 64 17.632776260375977 65 17.258602142333984
		 66 16.913742065429688 67 16.607793807983398 68 16.349336624145508 69 16.145975112915039
		 70 16.004459381103516;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateZ";
	rename -uid "24827207-4DDD-6953-114B-C18BB04C96DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -7.5735960006713858 1 -7.6900935173034668
		 2 -7.8403301239013663 3 -8.0209007263183594 4 -8.2280101776123047 5 -8.4577779769897461
		 6 -8.7063713073730469 7 -8.9702091217041016 8 -9.2460613250732422 9 -9.5311183929443359
		 10 -9.8230199813842773 11 -10.119903564453125 12 -10.420266151428223 13 -10.723031997680664
		 14 -11.027376174926758 15 -11.336507797241211 16 -11.65708065032959 17 -11.987914085388184
		 18 -12.327796936035156 19 -12.675552368164062 20 -13.030027389526367 21 -13.390113830566406
		 22 -13.754730224609375 23 -14.122840881347656 24 -14.493431091308594 25 -14.865470886230467
		 26 -15.237924575805664 27 -15.60969066619873 28 -15.979552268981935 29 -16.287332534790039
		 30 -16.480487823486328 31 -16.571496963500977 32 -16.572149276733398 33 -16.494691848754883
		 34 -16.352725982666016 35 -16.161911010742188 36 -15.940396308898926 37 -15.708909034729002
		 38 -15.490519523620605 39 -15.310009956359863 40 -15.192727088928223 41 -15.056358337402342
		 42 -14.811753273010254 43 -14.472631454467772 44 -14.052175521850586 45 -13.563486099243164
		 46 -13.019806861877441 47 -12.434738159179688 48 -11.822447776794434 49 -11.197640419006348
		 50 -10.575596809387207 51 -9.9720954895019531 52 -9.4032764434814453 53 -8.8854637145996094
		 54 -8.4349021911621094 55 -8.0675010681152344 56 -7.7985401153564453 57 -7.6393961906433105
		 58 -7.5591988563537607 59 -7.5409517288207999 60 -7.5677876472473136 61 -7.6236519813537598
		 62 -7.6938443183898917 63 -7.7653284072875977 64 -7.826953887939454 65 -7.8694915771484375
		 66 -7.8854823112487793 67 -7.8689236640930185 68 -7.8147559165954581 69 -7.7181425094604492
		 70 -7.5735960006713858;
createNode animCurveTU -n "Character1_LeftUpLeg_visibility";
	rename -uid "5301AA89-45E2-D440-72FE-4AB57F4EA3AA";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftLeg_translateX";
	rename -uid "6381B311-431A-650A-8FE5-C48EA0B8DDDC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -21.532840728759766;
createNode animCurveTL -n "Character1_LeftLeg_translateY";
	rename -uid "310728D8-4032-B90C-599C-4EAE5F809F08";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4219808578491211;
createNode animCurveTL -n "Character1_LeftLeg_translateZ";
	rename -uid "DF000EE8-4E84-EF64-B838-1482E678690E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8661990165710449;
createNode animCurveTU -n "Character1_LeftLeg_scaleX";
	rename -uid "B63A21E0-4A5A-1166-C88F-F4B13D29992B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftLeg_scaleY";
	rename -uid "9C7C0006-47CF-0600-678D-4BB4AD042560";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftLeg_scaleZ";
	rename -uid "1A188978-494A-69B5-C735-A2AE9780E2E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftLeg_rotateX";
	rename -uid "74497398-4012-6C41-304E-E1AC228F7231";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -12.467771530151367 1 -12.512673377990723
		 2 -12.530227661132813 3 -12.521101951599121 4 -12.485676765441895 5 -12.424117088317871
		 6 -12.336620330810547 7 -12.223484039306641 8 -12.085237503051758 9 -11.922691345214844
		 10 -11.737009048461914 11 -11.529664993286133 12 -11.302578926086426 13 -11.05792236328125
		 14 -10.798222541809082 15 -10.542689323425293 16 -10.301788330078125 17 -10.069319725036621
		 18 -9.8391647338867187 19 -9.6052675247192383 20 -9.3616876602172852 21 -9.1026191711425781
		 22 -8.8223848342895508 23 -8.5154094696044922 24 -8.1762104034423828 25 -7.7993774414062509
		 26 -7.3794808387756348 27 -6.9110627174377441 28 -6.3885622024536133 29 -5.8569245338439941
		 30 -5.3626413345336914 31 -4.9019150733947754 32 -4.4701170921325684 33 -4.0628771781921387
		 34 -3.6769065856933594 35 -3.3105249404907227 36 -2.9639639854431152 37 -2.6394658088684082
		 38 -2.3410310745239258 39 -2.0738821029663086 40 -1.8437373638153076 41 -1.6352792978286743
		 42 -1.4323610067367554 43 -1.2395123243331909 44 -1.061510443687439 45 -0.90310478210449219
		 46 -0.76898443698883057 47 -0.66370034217834473 48 -0.59149634838104248 49 -0.55637657642364502
		 50 -0.56202667951583862 51 -0.61183851957321167 52 -0.7089417576789856 53 -0.85622972249984741
		 54 -1.0565279722213745 55 -1.3126406669616699 56 -1.6274659633636475 57 -2.031444787979126
		 58 -2.5788125991821289 59 -3.2472963333129883 60 -4.0148148536682129 61 -4.8595175743103027
		 62 -5.7597908973693848 63 -6.6943635940551758 64 -7.6422820091247559 65 -8.5829057693481445
		 66 -9.4958658218383789 67 -10.36098575592041 68 -11.158164024353027 69 -11.867232322692871
		 70 -12.467771530151367;
createNode animCurveTA -n "Character1_LeftLeg_rotateY";
	rename -uid "0E94C173-4C95-453C-1E37-6C9FA7A69E6B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -29.1739501953125 1 -29.129472732543949
		 2 -29.176750183105472 3 -29.305490493774418 4 -29.504579544067379 5 -29.762615203857418
		 6 -30.068124771118168 7 -30.409921646118168 8 -30.777267456054687 9 -31.160032272338871
		 10 -31.548746109008793 11 -31.934700012207035 12 -32.309761047363281 13 -32.666500091552734
		 14 -32.997955322265625 15 -33.308311462402344 16 -33.609378814697266 17 -33.902023315429688
		 18 -34.187095642089844 19 -34.465526580810547 20 -34.738265991210937 21 -35.006301879882813
		 22 -35.270675659179688 23 -35.532470703125 24 -35.792839050292969 25 -36.052970886230469
		 26 -36.314136505126953 27 -36.577663421630859 28 -36.844928741455078 29 -37.046340942382812
		 30 -37.126182556152344 31 -37.103652954101563 32 -36.996139526367188 33 -36.820816040039063
		 34 -36.595916748046875 35 -36.341678619384766 36 -36.080890655517578 37 -35.839035034179687
		 38 -35.643932342529297 39 -35.524955749511719 40 -35.511489868164063 41 -35.571945190429688
		 42 -35.653011322021484 43 -35.749713897705078 44 -35.856475830078125 45 -35.967555999755859
		 46 -36.077213287353516 47 -36.179851531982422 48 -36.270282745361328 49 -36.343669891357422
		 50 -36.395610809326172 51 -36.422077178955078 52 -36.419368743896484 53 -36.384017944335937
		 54 -36.312564849853516 55 -36.201435089111328 56 -36.046737670898438 57 -35.819496154785156
		 58 -35.480976104736328 59 -35.046611785888672 60 -34.531757354736328 61 -33.95233154296875
		 62 -33.325386047363281 63 -32.669406890869141 64 -32.004631042480469 65 -31.353181838989254
		 66 -30.738981246948239 67 -30.18748664855957 68 -29.725177764892578 69 -29.378732681274414
		 70 -29.1739501953125;
createNode animCurveTA -n "Character1_LeftLeg_rotateZ";
	rename -uid "8BFE44E8-4C58-37F0-592E-8E9EAB40DA3E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -37.912395477294922 1 -37.861728668212891
		 2 -37.954799652099609 3 -38.175186157226563 4 -38.506141662597656 5 -38.930934906005859
		 6 -39.432769775390625 7 -39.9949951171875 8 -40.601100921630859 9 -41.234764099121094
		 10 -41.879787445068359 11 -42.520221710205078 12 -43.140052795410156 13 -43.723480224609375
		 14 -44.254604339599609 15 -44.753597259521484 16 -45.253078460693359 17 -45.750167846679688
		 18 -46.241870880126953 19 -46.725212097167969 20 -47.197219848632813 21 -47.654979705810547
		 22 -48.095623016357422 23 -48.516414642333984 24 -48.914726257324219 25 -49.288047790527344
		 26 -49.634021759033203 27 -49.950412750244141 28 -50.235076904296875 29 -50.328758239746094
		 30 -50.111610412597656 31 -49.640193939208984 32 -48.971225738525391 33 -48.161540985107422
		 34 -47.268051147460938 35 -46.347820281982422 36 -45.458091735839844 37 -44.656291961669922
		 38 -44.000091552734375 39 -43.547508239746094 40 -43.356773376464844 41 -43.343055725097656
		 42 -43.378322601318359 43 -43.455120086669922 44 -43.564914703369141 45 -43.698760986328125
		 46 -43.847511291503906 47 -44.0020751953125 48 -44.153823852539063 49 -44.294532775878906
		 50 -44.416587829589844 51 -44.512943267822266 52 -44.577110290527344 53 -44.603065490722656
		 54 -44.584957122802734 55 -44.516971588134766 56 -44.393096923828125 57 -44.171279907226563
		 58 -43.807689666748047 59 -43.327949523925781 60 -42.757572174072266 61 -42.122173309326172
		 62 -41.44769287109375 63 -40.76043701171875 64 -40.087215423583984 65 -39.455432891845703
		 66 -38.893104553222656 67 -38.428756713867188 68 -38.091304779052734 69 -37.909683227539063
		 70 -37.912395477294922;
createNode animCurveTU -n "Character1_LeftLeg_visibility";
	rename -uid "ECE7917B-40FC-B6C9-7A04-F0BDA8EEB648";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftFoot_translateX";
	rename -uid "5FA1E0B5-48E9-554B-81BD-C28721E9A6CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 18.368806838989258;
createNode animCurveTL -n "Character1_LeftFoot_translateY";
	rename -uid "25FB48F5-420D-EA95-B8E2-A7A9EFE63BF5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.7176852226257324;
createNode animCurveTL -n "Character1_LeftFoot_translateZ";
	rename -uid "F47144E7-4717-9733-E12E-CB8921AC03AC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3830562829971313;
createNode animCurveTU -n "Character1_LeftFoot_scaleX";
	rename -uid "86DB3A35-45A3-2356-9C90-48AC09E6DFDD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleY";
	rename -uid "7BD78769-4CAD-114E-6E35-62B532815688";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleZ";
	rename -uid "58E05B9A-43F2-F887-99EB-8488FBA0196C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftFoot_rotateX";
	rename -uid "5939BDA1-429F-1E36-6DBB-2E822CCF10FA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -21.993980407714844 1 -22.10845947265625
		 2 -22.294580459594727 3 -22.543201446533203 4 -22.8453369140625 5 -23.191959381103516
		 6 -23.573699951171875 7 -23.980703353881836 8 -24.402475357055664 9 -24.827798843383789
		 10 -25.244644165039063 11 -25.640232086181641 12 -26.000961303710938 13 -26.312631607055664
		 14 -26.560497283935547 15 -26.763648986816406 16 -26.94969367980957 17 -27.114847183227539
		 18 -27.255146026611328 19 -27.366533279418945 20 -27.444873809814453 21 -27.485996246337891
		 22 -27.485681533813477 23 -27.439670562744141 24 -27.343622207641602 25 -27.193061828613281
		 26 -26.983314514160156 27 -26.70939826965332 28 -26.36591911315918 29 -25.873231887817383
		 30 -25.182382583618164 31 -24.335390090942383 32 -23.375705718994141 33 -22.346763610839844
		 34 -21.290447235107422 35 -20.245649337768555 36 -19.247156143188477 37 -18.325057983398438
		 38 -17.504932403564453 39 -16.808895111083984 40 -16.257511138916016 41 -15.853169441223145
		 42 -15.576233863830566 43 -15.415177345275879 44 -15.358842849731444 45 -15.396230697631836
		 46 -15.516224861145018 47 -15.707365989685059 48 -15.957703590393065 49 -16.254642486572266
		 50 -16.584917068481445 51 -16.934579849243164 52 -17.289102554321289 53 -17.633514404296875
		 54 -17.95257568359375 55 -18.23101806640625 56 -18.453823089599609 57 -18.637399673461914
		 58 -18.829452514648438 59 -19.027996063232422 60 -19.231454849243164 61 -19.438976287841797
		 62 -19.650693893432617 63 -19.867952346801758 64 -20.093507766723633 65 -20.331727981567383
		 66 -20.588739395141602 67 -20.872533798217773 68 -21.193065643310547 69 -21.562257766723633
		 70 -21.993980407714844;
createNode animCurveTA -n "Character1_LeftFoot_rotateY";
	rename -uid "B01D1C5D-4971-8CFB-87D1-0FB18555939D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -33.848003387451172 1 -33.733768463134766
		 2 -33.726234436035156 3 -33.812580108642578 4 -33.979560852050781 5 -34.213951110839844
		 6 -34.502655029296875 7 -34.832977294921875 8 -35.192703247070313 9 -35.570247650146484
		 10 -35.954624176025391 11 -36.335521697998047 12 -36.703094482421875 13 -37.048027038574219
		 14 -37.361221313476563 15 -37.642406463623047 16 -37.901317596435547 17 -38.141250610351563
		 18 -38.365608215332031 19 -38.577945709228516 20 -38.781917572021484 21 -38.981254577636719
		 22 -39.179729461669922 23 -39.381122589111328 24 -39.589138031005859 25 -39.807338714599609
		 26 -40.039085388183594 27 -40.287399291992188 28 -40.554885864257813 29 -40.796627044677734
		 30 -40.968822479248047 31 -41.07318115234375 32 -41.111892700195313 33 -41.089839935302734
		 34 -41.016101837158203 35 -40.904808044433594 36 -40.775295257568359 37 -40.651622772216797
		 38 -40.561634063720703 39 -40.535591125488281 40 -40.604377746582031 41 -40.748756408691406
		 42 -40.925701141357422 43 -41.128520965576172 44 -41.349220275878906 45 -41.579059600830078
		 46 -41.808822631835938 47 -42.029098510742187 48 -42.23065185546875 49 -42.404476165771484
		 50 -42.542034149169922 51 -42.635292053222656 52 -42.676784515380859 53 -42.659584045410156
		 54 -42.577156066894531 55 -42.423240661621094 56 -42.191665649414063 57 -41.852542877197266
		 58 -41.38153076171875 59 -40.801017761230469 60 -40.133174896240234 61 -39.400291442871094
		 62 -38.625087738037109 63 -37.830844879150391 64 -37.041606903076172 65 -36.282241821289063
		 66 -35.57843017578125 67 -34.956489562988281 68 -34.443126678466797 69 -34.064975738525391
		 70 -33.848003387451172;
createNode animCurveTA -n "Character1_LeftFoot_rotateZ";
	rename -uid "E2B212A2-43A9-86CE-06CC-C8A3CAF7F5EE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 12.97510814666748 1 13.126035690307617
		 2 13.321613311767578 3 13.555458068847656 4 13.821564674377441 5 14.113986968994141
		 6 14.426508903503418 7 14.752426147460936 8 15.084355354309082 9 15.414121627807617
		 10 15.732680320739746 11 16.030136108398438 12 16.295740127563477 13 16.518068313598633
		 14 16.685140609741211 15 16.816900253295898 16 16.938751220703125 17 17.045803070068359
		 18 17.133028030395508 19 17.195350646972656 20 17.227655410766602 21 17.224817276000977
		 22 17.181695938110352 23 17.093128204345703 24 16.953887939453125 25 16.758628845214844
		 26 16.501804351806641 27 16.177577972412109 28 15.77969169616699 29 15.239551544189455
		 30 14.518348693847658 31 13.656136512756348 32 12.69431209564209 33 11.673951148986816
		 34 10.634146690368652 35 9.6104574203491211 36 8.633763313293457 37 7.7296996116638175
		 38 6.9189414978027344 39 6.2183990478515625 40 5.6433906555175781 41 5.2017960548400879
		 42 4.8836941719055176 43 4.6787948608398437 44 4.5772261619567871 45 4.5692806243896484
		 46 4.645164966583252 47 4.794776439666748 48 5.0075263977050781 49 5.2722172737121582
		 50 5.5769977569580078 51 5.9093608856201172 52 6.2562370300292969 53 6.6041297912597656
		 54 6.9392986297607422 55 7.2479929924011222 56 7.516699790954589 57 7.7687277793884286
		 58 8.0578823089599609 59 8.3781728744506836 60 8.724024772644043 61 9.0904655456542969
		 62 9.4732742309570312 63 9.8691263198852539 64 10.275718688964844 65 10.691902160644531
		 66 11.117823600769043 67 11.555057525634766 68 12.00676441192627 69 12.47784423828125
		 70 12.97510814666748;
createNode animCurveTU -n "Character1_LeftFoot_visibility";
	rename -uid "4A86DD4D-4E62-40A8-F442-199B25455B3C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftToeBase_translateX";
	rename -uid "AEA2B34A-467B-433A-EF0C-CBAC680B28FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3813290596008301;
createNode animCurveTL -n "Character1_LeftToeBase_translateY";
	rename -uid "F56CF178-402C-C9F4-BEAF-E8BE629A0426";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.3898367881774902;
createNode animCurveTL -n "Character1_LeftToeBase_translateZ";
	rename -uid "6FEF1B3B-41CC-F0B9-8CE8-849E30C27FE2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2284431457519531;
createNode animCurveTU -n "Character1_LeftToeBase_scaleX";
	rename -uid "B4969819-4822-5613-404A-9897D6E8AC6E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftToeBase_scaleY";
	rename -uid "FACD4786-4563-48AF-56E8-F6A8E93A7F03";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftToeBase_scaleZ";
	rename -uid "6443BC5F-4CAB-DAEA-A3C3-2590C14EF809";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftToeBase_rotateX";
	rename -uid "F9B5955E-418B-190A-71B1-E481E203FB15";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.9252780459928545e-010;
createNode animCurveTA -n "Character1_LeftToeBase_rotateY";
	rename -uid "88165587-4788-5E4A-3C53-47B1F541FCAC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.8224403097732986e-009;
createNode animCurveTA -n "Character1_LeftToeBase_rotateZ";
	rename -uid "62CCCEFE-4B26-754D-8F98-17893214914D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.5253352176644057e-009;
createNode animCurveTU -n "Character1_LeftToeBase_visibility";
	rename -uid "EC10AB91-4B41-8D4D-5E0A-39B537327416";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateX";
	rename -uid "0C156226-4AA0-F073-A7D5-478250F65390";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.1971817016601563;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateY";
	rename -uid "B203715A-4070-6319-E6CD-8B97A8246E95";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.7569773197174072;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateZ";
	rename -uid "448EDC1B-487B-C82C-6803-86B09FBFB0C3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4053930044174194;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleX";
	rename -uid "54A18900-4006-338B-3059-0D974F8C95B7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleY";
	rename -uid "BFB9AB76-4EDC-A71D-7C94-DEB2C07F9F31";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999958276748657;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleZ";
	rename -uid "5BB030A1-41D5-512F-41BA-A389A22DC2FA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateX";
	rename -uid "ADA994B7-472C-EBC0-BC9B-D9B74DC07FAB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.4335376764005328e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateY";
	rename -uid "F1A615D3-4C82-1B85-D394-9BAE77E0965E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.269026039807386e-008;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateZ";
	rename -uid "093C19C9-4F4A-2D1E-B04F-F5A3ECA478DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.5307118905738548e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle1_visibility";
	rename -uid "A7454F11-41C6-122B-0809-6DAE3C9F9EE8";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateX";
	rename -uid "EE137751-4442-C2F0-F42C-6CA4B90D1CDC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8763875961303711;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateY";
	rename -uid "23F3C35E-441B-E095-8E02-9993DC4FBE03";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.1604456901550293;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateZ";
	rename -uid "275DF6B1-4243-5EE9-1A7A-F0BE05477749";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.313550591468811;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleX";
	rename -uid "25490B55-44FA-1681-F54A-F9B204DF85C1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleY";
	rename -uid "2B46EE78-4C8A-36B3-A3D5-548AC1B26936";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleZ";
	rename -uid "F94964B5-4010-8EFE-A423-8D8893CC54E0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateX";
	rename -uid "9823EDC8-4114-388D-643A-37ABB0DFA49F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.2131183531377587e-010;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateY";
	rename -uid "7BFD2C03-42AD-2CCA-507B-55A325536E5C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.7268947988545736e-008;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateZ";
	rename -uid "CBAC1406-41B0-6AD4-10AA-EC84AB00AEBD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2344000488440088e-008;
createNode animCurveTU -n "Character1_LeftFootMiddle2_visibility";
	rename -uid "FA1122CA-4330-C89C-54F2-12BDDAA0BEF7";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightUpLeg_translateX";
	rename -uid "676278DD-4FB7-013E-18C1-56A45D473ED3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.4649467468261719;
createNode animCurveTL -n "Character1_RightUpLeg_translateY";
	rename -uid "FA685F67-45EF-0F71-2A66-B4AA2398349D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.110023021697998;
createNode animCurveTL -n "Character1_RightUpLeg_translateZ";
	rename -uid "6B975CD4-4136-2770-6FB6-19AAEAD60AAF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.4359440803527832;
createNode animCurveTU -n "Character1_RightUpLeg_scaleX";
	rename -uid "92F19B4A-438E-3796-0CCE-98929CB85F30";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightUpLeg_scaleY";
	rename -uid "B77B227A-4DE4-951C-42E1-98A2087D3918";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightUpLeg_scaleZ";
	rename -uid "2B67FC1A-4DE1-C71E-00DE-57AA409D0A68";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightUpLeg_rotateX";
	rename -uid "FE7DA77E-4243-BE44-FF7A-D69FF83E37CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 22.308263778686523 1 22.54545783996582
		 2 22.910732269287109 3 23.384902954101563 4 23.948451995849609 5 24.582025527954102
		 6 25.266578674316406 7 25.983615875244141 8 26.715242385864258 9 27.444189071655273
		 10 28.153739929199219 11 28.827737808227539 12 29.450326919555664 13 30.006040573120114
		 14 30.479539871215824 15 30.875169754028324 16 31.209577560424801 17 31.487480163574219
		 18 31.713306427001953 19 31.891336441040043 20 32.025699615478516 21 32.120433807373047
		 22 32.179489135742188 23 32.206798553466797 24 32.206279754638672 25 32.181819915771484
		 26 32.137348175048828 27 32.076797485351562 28 32.004104614257813 29 31.825965881347653
		 30 31.463851928710938 31 30.948442459106445 32 30.309976577758786 33 29.578746795654297
		 34 28.78559494018555 35 27.962404251098633 36 27.142518997192383 37 26.361028671264648
		 38 25.654882431030273 39 25.062751770019531 40 24.624338150024414 41 24.305141448974609
		 42 24.041654586791992 43 23.828758239746094 44 23.659721374511719 45 23.527065277099609
		 46 23.42308235168457 47 23.340305328369141 48 23.271944046020508 49 23.211919784545898
		 50 23.154977798461914 51 23.096609115600586 52 23.032899856567383 53 22.960361480712891
		 54 22.875583648681641 55 22.77496337890625 56 22.654342651367188 57 22.53215217590332
		 58 22.429225921630859 59 22.343015670776367 60 22.271059036254883 61 22.211286544799805
		 62 22.16230583190918 63 22.123563766479492 64 22.095531463623047 65 22.079813003540039
		 66 22.079130172729492 67 22.097194671630859 68 22.138416290283203 69 22.207393646240234
		 70 22.308263778686523;
createNode animCurveTA -n "Character1_RightUpLeg_rotateY";
	rename -uid "90962B7F-46A4-5697-A993-E1B94461AA6B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 29.134269714355469 1 29.126358032226566
		 2 29.109869003295898 3 29.086072921752933 4 29.056346893310547 5 29.022090911865234
		 6 28.984682083129883 7 28.94542121887207 8 28.905521392822262 9 28.866086959838864
		 10 28.828121185302734 11 28.792507171630859 12 28.760044097900394 13 28.731416702270508
		 14 28.707231521606445 15 28.676376342773434 16 28.630594253540039 17 28.574577331542969
		 18 28.513021469116211 19 28.450630187988281 20 28.392105102539063 21 28.342145919799805
		 22 28.305458068847656 23 28.2867431640625 24 28.290725708007813 25 28.322149276733398
		 26 28.385770797729492 27 28.486392974853516 28 28.628870010375977 29 28.843402862548825
		 30 29.14625358581543 31 29.521421432495117 32 29.952741622924808 33 30.423444747924808
		 34 30.915908813476559 35 31.411531448364261 36 31.890750885009769 37 32.333236694335937
		 38 32.718162536621094 39 33.024616241455078 40 33.232128143310547 41 33.369022369384766
		 42 33.477901458740234 43 33.560726165771484 44 33.619716644287109 45 33.657138824462891
		 46 33.675148010253906 47 33.675655364990234 48 33.660232543945313 49 33.630035400390625
		 50 33.5858154296875 51 33.527896881103516 52 33.456226348876953 53 33.370456695556641
		 54 33.270069122314453 55 33.154518127441406 56 33.023414611816406 57 32.86712646484375
		 58 32.672939300537109 59 32.445579528808594 60 32.18994140625 61 31.910989761352539
		 62 31.613729476928707 63 31.303125381469727 64 30.98403358459473 65 30.661125183105472
		 66 30.338827133178711 67 30.021280288696289 68 29.71232795715332 69 29.415540695190426
		 70 29.134269714355469;
createNode animCurveTA -n "Character1_RightUpLeg_rotateZ";
	rename -uid "62DA263B-46C2-F72B-9874-C2B8F8873114";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  0 -14.250053405761719 1 -14.05159854888916
		 2 -13.864622116088867 3 -13.689326286315918 4 -13.525876045227051 5 -13.374540328979492
		 6 -13.23576545715332 7 -13.110238075256348 8 -12.998921394348145 9 -12.903078079223633
		 10 -12.824250221252441 11 -12.764250755310059 12 -12.725092887878418 13 -12.708966255187988
		 14 -12.718121528625488 15 -12.737654685974121 16 -12.748658180236816 22 -12.745248794555664
		 23 -12.748388290405273 24 -12.756476402282715 25 -12.770922660827637 26 -12.793169975280762
		 27 -12.824710845947266 28 -12.86711597442627 29 -12.910015106201172 30 -12.944332122802734
		 31 -12.974856376647949 32 -13.006691932678223 33 -13.044994354248047 34 -13.094754219055176
		 35 -13.160665512084961 36 -13.247061729431152 37 -13.357897758483887 38 -13.496819496154785
		 39 -13.667282104492188 40 -13.872666358947754 41 -14.10008716583252 42 -14.333902359008789
		 43 -14.572910308837892 44 -14.81505870819092 45 -15.057705879211424 46 -15.297793388366699
		 47 -15.531999588012694 48 -15.756891250610352 49 -15.968986511230467 50 -16.164836883544922
		 51 -16.341073989868164 52 -16.494457244873047 53 -16.62193489074707 54 -16.720664978027344
		 55 -16.788095474243164 56 -16.822013854980469 57 -16.811508178710938 58 -16.740196228027344
		 59 -16.617574691772461 60 -16.453157424926758 61 -16.256210327148438 62 -16.035551071166992
		 63 -15.799374580383301 64 -15.555171966552736 65 -15.309682846069336 66 -15.068902015686035
		 67 -14.838123321533201 68 -14.622020721435547 69 -14.424746513366699 70 -14.250053405761719;
createNode animCurveTU -n "Character1_RightUpLeg_visibility";
	rename -uid "FF791F96-4E53-7741-E3EB-6FA6E34DCA3C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightLeg_translateX";
	rename -uid "900CAB95-4E70-3A52-0B58-ED8914A26AED";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3108952045440674;
createNode animCurveTL -n "Character1_RightLeg_translateY";
	rename -uid "687D5EDC-4D8F-CB56-469A-41801D4438D1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 22.181583404541016;
createNode animCurveTL -n "Character1_RightLeg_translateZ";
	rename -uid "4E10F69F-45BC-8BC1-CE86-2684A46BDA6D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.6536058187484741;
createNode animCurveTU -n "Character1_RightLeg_scaleX";
	rename -uid "E6E3C0E7-40D1-C8C2-EEBF-FE9415FC94AF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightLeg_scaleY";
	rename -uid "11BF09AF-484B-197B-B41E-12B58207EBF2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightLeg_scaleZ";
	rename -uid "CBDE0DC2-4BCE-7D9A-B67D-69BA3A422278";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightLeg_rotateX";
	rename -uid "58B892B0-4A47-7B82-12F9-5A98B7BE39DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 11.33759880065918 1 11.424591064453125
		 2 11.659102439880371 3 12.02265453338623 4 12.496707916259766 5 13.062972068786621
		 6 13.703228950500488 7 14.399462699890137 8 15.133775711059569 9 15.8884220123291
		 10 16.645721435546875 11 17.388172149658203 12 18.098169326782227 13 18.758335113525391
		 14 19.351213455200195 15 19.86119270324707 16 20.285482406616211 17 20.630533218383789
		 18 20.902803421020508 19 21.108831405639648 20 21.255149841308594 21 21.348293304443359
		 22 21.394756317138672 23 21.401037216186523 24 21.373586654663086 25 21.318792343139648
		 26 21.243015289306641 27 21.152523040771484 28 21.053487777709961 29 20.825132369995117
		 30 20.366321563720703 31 19.714193344116211 32 18.90544319152832 33 17.977209091186523
		 34 16.967887878417969 35 15.917930603027346 36 14.870508193969727 37 13.871820449829102
		 38 12.971186637878418 39 12.220609664916992 40 11.673456192016602 41 11.3612060546875
		 42 11.263957023620605 43 11.356949806213379 44 11.61375904083252 45 12.00688648223877
		 46 12.507939338684082 47 13.087888717651367 48 13.717544555664063 49 14.367513656616211
		 50 15.008499145507811 51 15.611340522766112 52 16.147113800048828 53 16.587236404418945
		 54 16.903280258178711 55 17.067089080810547 56 17.050716400146484 57 16.864456176757813
		 58 16.533744812011719 59 16.086179733276367 60 15.549507141113281 61 14.951642990112305
		 62 14.32062816619873 63 13.684375762939453 64 13.070508003234863 65 12.506209373474121
		 66 12.018082618713379 67 11.632059097290039 68 11.373473167419434 69 11.267143249511719
		 70 11.33759880065918;
createNode animCurveTA -n "Character1_RightLeg_rotateY";
	rename -uid "52B2D98D-460F-FFE9-E457-06A53F99495E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 24.406715393066406 1 24.481784820556641
		 2 24.671627044677734 3 24.958715438842773 4 25.324073791503906 5 25.748680114746094
		 6 26.214237213134766 7 26.703939437866211 8 27.202823638916016 9 27.698007583618164
		 10 28.178655624389648 11 28.635955810546875 12 29.062705993652344 13 29.453226089477543
		 14 29.80280685424805 15 30.108816146850582 16 30.373779296874996 17 30.601991653442386
		 18 30.796598434448242 19 30.95992469787598 20 31.093673706054688 21 31.199110031127926
		 22 31.277229309082035 23 31.328884124755856 24 31.354887008666992 25 31.356079101562504
		 26 31.333400726318356 27 31.287904739379883 28 31.220743179321293 29 31.054468154907227
		 30 30.726442337036133 31 30.260740280151367 32 29.681127548217773 33 29.012163162231449
		 34 28.280082702636719 35 27.513500213623047 36 26.743892669677734 37 26.005744934082031
		 38 25.33642578125 39 24.775606155395508 40 24.363872528076172 41 24.120643615722656
		 42 24.027690887451172 43 24.065332412719727 44 24.2105712890625 45 24.439126968383789
		 46 24.726926803588867 47 25.05131721496582 48 25.392024993896484 49 25.73143196105957
		 50 26.054729461669922 51 26.349639892578125 52 26.605945587158203 53 26.814817428588867
		 54 26.967824935913086 55 27.055936813354492 56 27.068241119384766 57 27.015453338623047
		 58 26.908525466918945 59 26.751068115234375 60 26.546405792236328 61 26.298999786376953
		 62 26.015604019165039 63 25.706100463867188 64 25.384103775024414 65 25.067253112792969
		 66 24.777107238769531 67 24.53857421875 68 24.378904342651367 69 24.326078414916992
		 70 24.406715393066406;
createNode animCurveTA -n "Character1_RightLeg_rotateZ";
	rename -uid "61452430-43BB-50A7-0E6E-4C9A7311A715";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 29.375934600830075 1 29.374591827392578
		 2 29.541223526000977 3 29.853939056396484 4 30.290744781494144 5 30.829822540283203
		 6 31.449419021606445 7 32.127952575683594 8 32.843982696533203 9 33.576229095458984
		 10 34.303524017333984 11 35.004890441894531 12 35.6593017578125 13 36.245929718017578
		 14 36.743839263916016 15 37.131999969482422 16 37.407550811767578 17 37.583751678466797
		 18 37.673686981201172 19 37.690414428710937 20 37.646930694580078 21 37.556251525878906
		 22 37.431407928466797 23 37.285495758056641 24 37.1317138671875 25 36.983310699462891
		 26 36.853668212890625 27 36.756214141845703 28 36.704429626464844 29 36.621532440185547
		 30 36.434711456298828 31 36.165000915527344 32 35.833206176757813 33 35.459632873535156
		 34 35.063938140869141 35 34.665206909179687 36 34.282176971435547 37 33.933448791503906
		 38 33.637886047363281 39 33.414981842041016 40 33.284980773925781 41 33.311439514160156
		 42 33.527458190917969 43 33.907737731933594 44 34.425632476806641 45 35.053562164306641
		 46 35.763072967529297 47 36.525001525878906 48 37.309852600097656 49 38.087730407714844
		 50 38.828651428222656 51 39.502601623535156 52 40.079681396484375 53 40.530235290527344
		 54 40.824790954589844 55 40.934188842773437 56 40.829605102539063 57 40.488910675048828
		 58 39.920078277587891 59 39.161396026611328 60 38.251399993896484 61 37.228904724121094
		 62 36.133045196533203 63 35.003021240234375 64 33.877983093261719 65 32.796829223632812
		 66 31.798036575317383 67 30.919515609741214 68 30.198596954345703 69 29.672004699707035
		 70 29.375934600830075;
createNode animCurveTU -n "Character1_RightLeg_visibility";
	rename -uid "3E1D4644-4135-7B8F-DA9C-168F92963DD1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightFoot_translateX";
	rename -uid "454BD213-411F-5DC3-7BF5-289B0B45FBCA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.668137550354004;
createNode animCurveTL -n "Character1_RightFoot_translateY";
	rename -uid "D757742C-48BC-BF68-448A-5AA3C51A1558";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3660926818847656;
createNode animCurveTL -n "Character1_RightFoot_translateZ";
	rename -uid "64F021DC-40CA-0B4D-4A91-93985CB1383A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -15.681424140930176;
createNode animCurveTU -n "Character1_RightFoot_scaleX";
	rename -uid "8EB75970-430B-54FB-A1C6-6EA48B46F897";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightFoot_scaleY";
	rename -uid "23B747F1-4180-DE2E-0807-AFA8BC1DAB57";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999958276748657;
createNode animCurveTU -n "Character1_RightFoot_scaleZ";
	rename -uid "7CFDAB56-4216-374C-C3A3-799DB075D0F6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFoot_rotateX";
	rename -uid "03C5BF41-4670-F635-43DB-8CA9C80A6EAF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -2.8193857669830322 1 -2.7091412544250488
		 2 -2.5967707633972168 3 -2.4839804172515869 4 -2.3721497058868408 5 -2.2626678943634033
		 6 -2.1571648120880127 7 -2.0576934814453125 8 -1.9668210744857788 9 -1.8876876831054687
		 10 -1.8239812850952151 11 -1.7799128293991089 12 -1.7601054906845093 13 -1.7695320844650269
		 14 -1.8133553266525269 15 -1.8930990695953367 16 -2.0057039260864258 17 -2.1470193862915039
		 18 -2.3125338554382324 19 -2.497485876083374 20 -2.696955680847168 21 -2.9059367179870605
		 22 -3.1193976402282715 23 -3.3323307037353516 24 -3.539776086807251 25 -3.7368476390838623
		 26 -3.9187407493591304 27 -4.0807161331176758 28 -4.2180795669555664 29 -4.3195710182189941
		 30 -4.3811054229736328 31 -4.4099035263061523 32 -4.4136528968811035 33 -4.4004273414611816
		 34 -4.3785219192504883 35 -4.3562283515930176 36 -4.3415732383728027 37 -4.342038631439209
		 38 -4.3643617630004883 39 -4.4144434928894043 40 -4.4973955154418945 41 -4.5637598037719727
		 42 -4.5652651786804199 43 -4.5075979232788086 44 -4.3960666656494141 45 -4.2361021041870117
		 46 -4.0336470603942871 47 -3.7954409122467041 48 -3.5292043685913086 49 -3.243708610534668
		 50 -2.948742151260376 51 -2.6550004482269287 52 -2.3738939762115479 53 -2.117293119430542
		 54 -1.8971801996231077 55 -1.7252660989761353 56 -1.6125117540359497 57 -1.5693528652191162
		 58 -1.5843329429626465 59 -1.6463351249694824 60 -1.743888258934021 61 -1.8657864332199094
		 62 -2.0016012191772461 63 -2.1421096324920654 64 -2.2796101570129395 65 -2.4081299304962158
		 66 -2.5234818458557129 67 -2.623136043548584 68 -2.7059102058410645 69 -2.7714300155639648
		 70 -2.8193857669830322;
createNode animCurveTA -n "Character1_RightFoot_rotateY";
	rename -uid "B775AD33-4CB9-77F5-15A8-A5970D64EBB5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -0.21458061039447784 1 -0.21937325596809387
		 2 -0.23624652624130252 3 -0.26355934143066406 4 -0.29956376552581787 5 -0.34251245856285095
		 6 -0.3907051682472229 7 -0.44254636764526367 8 -0.49656957387924194 9 -0.55146044492721558
		 10 -0.60605376958847046 11 -0.65933686494827271 12 -0.71041226387023926 13 -0.75849747657775879
		 14 -0.8028673529624939 15 -0.84166985750198364 16 -0.87374883890151978 17 -0.89956837892532349
		 18 -0.91949838399887085 19 -0.93387305736541737 20 -0.94303131103515625 21 -0.94735127687454224
		 22 -0.94727647304534912 23 -0.94333446025848378 24 -0.93614292144775391 25 -0.92640888690948486
		 26 -0.91492116451263428 27 -0.90252655744552612 28 -0.89010471105575562 29 -0.87293177843093872
		 30 -0.84654152393341064 31 -0.81222224235534668 32 -0.77108246088027954 33 -0.72413212060928345
		 34 -0.6723589301109314 35 -0.61681067943572998 36 -0.55867302417755127 37 -0.49933770298957819
		 38 -0.44047471880912781 39 -0.38408339023590088 40 -0.33248633146286011 41 -0.29602980613708496
		 42 -0.28219613432884216 43 -0.28863427042961121 44 -0.3123336136341095 45 -0.34987500309944153
		 46 -0.39763271808624268 47 -0.45195412635803223 48 -0.509327232837677 49 -0.56646806001663208
		 50 -0.62039679288864136 51 -0.66845375299453735 52 -0.70828592777252197 53 -0.73779922723770142
		 54 -0.75505536794662476 55 -0.75816500186920166 56 -0.7451358437538147 57 -0.71944421529769897
		 58 -0.68475037813186646 59 -0.64237397909164429 60 -0.59366005659103394 61 -0.54011023044586182
		 62 -0.48347571492195124 63 -0.42579987645149231 64 -0.36942926049232483 65 -0.31698697805404663
		 66 -0.27130985260009766 67 -0.23535141348838803 68 -0.21207094192504883 69 -0.20429623126983643
		 70 -0.21458061039447784;
createNode animCurveTA -n "Character1_RightFoot_rotateZ";
	rename -uid "8453B65D-4206-B121-12BB-AB95B50D5CB4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -1.1549981832504272 1 -1.2335577011108398
		 2 -1.3953069448471069 3 -1.6280632019042969 4 -1.9189765453338621 5 -2.2552585601806641
		 6 -2.6244950294494629 7 -3.0150156021118164 8 -3.4160258769989014 9 -3.817718505859375
		 10 -4.2112150192260742 11 -4.5885605812072754 12 -4.9424595832824707 13 -5.2663016319274902
		 14 -5.5538129806518555 15 -5.7892608642578125 16 -5.963994026184082 17 -6.08453369140625
		 18 -6.1570267677307129 19 -6.1873927116394043 20 -6.1813735961914063 21 -6.144594669342041
		 22 -6.0826082229614258 23 -6.0009379386901855 24 -5.9050869941711426 25 -5.8005499839782715
		 26 -5.6928262710571289 27 -5.5873818397521973 28 -5.4896435737609863 29 -5.3741998672485352
		 30 -5.2130331993103027 31 -5.007481575012207 32 -4.7583355903625488 33 -4.4670801162719727
		 34 -4.1369137763977051 35 -3.7736132144927979 36 -3.3861517906188965 37 -2.9869394302368164
		 38 -2.5917432308197021 39 -2.2190766334533691 40 -1.8888489007949831 41 -1.6698811054229736
		 42 -1.6057934761047363 43 -1.676932692527771 44 -1.8617483377456665 45 -2.1377813816070557
		 46 -2.4823269844055176 47 -2.8729820251464844 48 -3.2881488800048828 49 -3.7071151733398442
		 50 -4.1101789474487305 51 -4.4785261154174805 52 -4.7940869331359863 53 -5.0393404960632324
		 54 -5.1969246864318848 55 -5.2493829727172852 56 -5.1787562370300293 57 -5.0064654350280762
		 58 -4.7625350952148437 59 -4.458740234375 60 -4.1066646575927734 61 -3.7183020114898686
		 62 -3.3065500259399414 63 -2.8855075836181641 64 -2.4707131385803223 65 -2.0792529582977295
		 66 -1.7297054529190063 67 -1.441880464553833 68 -1.2364161014556885 69 -1.1340980529785156
		 70 -1.1549981832504272;
createNode animCurveTU -n "Character1_RightFoot_visibility";
	rename -uid "850EB92F-4D40-F3CA-CD19-91AB47AAD300";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightToeBase_translateX";
	rename -uid "7CCB4B21-4658-267F-E00F-8488208FAD90";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.103175163269043;
createNode animCurveTL -n "Character1_RightToeBase_translateY";
	rename -uid "E7188948-43A5-8642-7831-D4AA02D4FCC0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.7509346008300781;
createNode animCurveTL -n "Character1_RightToeBase_translateZ";
	rename -uid "8E1C1F98-45C9-FCE8-26E7-3782A3941B30";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.6768350601196289;
createNode animCurveTU -n "Character1_RightToeBase_scaleX";
	rename -uid "A53E085B-446F-1182-5083-A7BB85B00286";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightToeBase_scaleY";
	rename -uid "0F5245AF-436B-8DB9-A34D-278E110A9B0F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightToeBase_scaleZ";
	rename -uid "2D05433A-49CA-E4B9-83BC-9B83BF864D07";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightToeBase_rotateX";
	rename -uid "8CA435A0-475C-6C7F-A09F-879D7FB68AA5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.50311493544109e-009;
createNode animCurveTA -n "Character1_RightToeBase_rotateY";
	rename -uid "E241FBDE-4F5D-07EA-6142-A7B2C1E22050";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.4775487217354453e-010;
createNode animCurveTA -n "Character1_RightToeBase_rotateZ";
	rename -uid "D23396F1-48DE-583C-6105-93981A84ECD9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.4231712192346322e-009;
createNode animCurveTU -n "Character1_RightToeBase_visibility";
	rename -uid "32AB4854-4918-3869-D2EB-20AB581D5CFF";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateX";
	rename -uid "A9B6CC57-4F36-E7D7-1B22-BBA7556C9BBB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.31789970397949219;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateY";
	rename -uid "45A37C9C-4B26-5048-D8F0-24AD6B2E7249";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.6937241554260254;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateZ";
	rename -uid "87B2874F-44EB-9386-350A-DAAA77108E05";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.4678492546081543;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleX";
	rename -uid "A6ABE1F7-4AD9-0C4F-73AD-5A983D88BD84";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleY";
	rename -uid "8E42D95B-4B90-7520-6A25-CB987BC7921C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleZ";
	rename -uid "B3CAD622-451C-34E4-7206-DFA9AF5AFABF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateX";
	rename -uid "6F6FD536-437A-3883-4281-529036AD6E15";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2730262357676452e-010;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateY";
	rename -uid "9F5E60F7-472E-4C64-0F85-248E8F7489BF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.0482777312859071e-008;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateZ";
	rename -uid "AE36EBB9-47D8-147C-07BA-049C7213E76E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4553973137765295e-008;
createNode animCurveTU -n "Character1_RightFootMiddle1_visibility";
	rename -uid "94AF75F6-4E58-A8B9-537A-ACBF573A9E4D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateX";
	rename -uid "85394FED-4F8B-BC10-BE3F-8B8617140264";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3449039459228516;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateY";
	rename -uid "365B1986-4C8E-DAE1-DDC8-E3B86B9EC5C4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3579239845275879;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateZ";
	rename -uid "9EB586F9-40F1-D4EA-DD0C-A48ED3F3D922";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.7014765739440918;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleX";
	rename -uid "1D2332D6-42F6-CDF2-B2FD-4FB148E0B8C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleY";
	rename -uid "04DA9ADF-4FF4-9739-5678-10990C518582";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleZ";
	rename -uid "51F0ABB5-403C-D08D-22BC-8F8CC7BB593E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateX";
	rename -uid "711CECC6-4DBC-8954-7D28-959E3B016926";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.074161382064176e-008;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateY";
	rename -uid "634E35D6-4E5D-9F08-85F9-58BC22359B47";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.5282341188985811e-009;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateZ";
	rename -uid "32E66EC6-4A3B-07EF-CCFC-C99F5B694F62";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.3327182791254018e-008;
createNode animCurveTU -n "Character1_RightFootMiddle2_visibility";
	rename -uid "A7C8229A-4EDF-59BC-1C8D-89BF8272FF93";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_Spine_translateX";
	rename -uid "DC23228C-4D87-07B7-47DD-6FB1FA9DFC01";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.9174466133117676;
createNode animCurveTL -n "Character1_Spine_translateY";
	rename -uid "2E2B2ECD-4290-EC74-5613-7394A2FF5327";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.894325256347656;
createNode animCurveTL -n "Character1_Spine_translateZ";
	rename -uid "25B4C57E-4537-1EBE-755F-54A1666DEA01";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1938900947570801;
createNode animCurveTU -n "Character1_Spine_scaleX";
	rename -uid "C1E67226-4DD8-4551-9E2D-7D90EECC7958";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine_scaleY";
	rename -uid "75D2ADF3-4C85-1239-01C5-DEA496459FBF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine_scaleZ";
	rename -uid "B1066CBB-4E06-DC05-5093-EB89D2D4FA22";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_Spine_rotateX";
	rename -uid "9DF7727D-4D3B-8518-CB9A-B5966505BEFB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -11.865646362304688 1 -11.884803771972656
		 2 -11.938641548156738 3 -12.021718978881836 4 -12.12858772277832 5 -12.253804206848145
		 6 -12.391924858093262 7 -12.537501335144043 8 -12.685086250305176 9 -12.829230308532715
		 10 -12.964478492736816 11 -13.085378646850586 12 -13.18647289276123 13 -13.262301445007324
		 14 -13.30741024017334 15 -13.340834617614746 16 -13.383296966552734 17 -13.431872367858887
		 18 -13.483634948730469 19 -13.535656929016113 20 -13.585014343261719 21 -13.628783226013184
		 22 -13.664034843444824 23 -13.687847137451172 24 -13.697291374206543 25 -13.689437866210938
		 26 -13.661354064941406 27 -13.610102653503418 28 -13.53273868560791 29 -13.39549732208252
		 30 -13.178498268127441 31 -12.899588584899902 32 -12.57658576965332 33 -12.227268218994141
		 34 -11.869377136230469 35 -11.520620346069336 36 -11.198675155639648 37 -10.921196937561035
		 38 -10.705824851989746 39 -10.570196151733398 40 -10.531952857971191 41 -10.574318885803223
		 42 -10.664247512817383 43 -10.794997215270996 44 -10.959833145141602 45 -11.152020454406738
		 46 -11.364828109741211 47 -11.591535568237305 48 -11.825424194335938 49 -12.059778213500977
		 50 -12.28788948059082 51 -12.503046035766602 52 -12.698542594909668 53 -12.867666244506836
		 54 -13.003703117370605 55 -13.099939346313477 56 -13.149652481079102 57 -13.153828620910645
		 58 -13.118011474609375 59 -13.048420906066895 60 -12.951286315917969 61 -12.832864761352539
		 62 -12.699461936950684 63 -12.557433128356934 64 -12.413187980651855 65 -12.273190498352051
		 66 -12.143952369689941 67 -12.032014846801758 68 -11.943944931030273 69 -11.886306762695313
		 70 -11.865646362304688;
createNode animCurveTA -n "Character1_Spine_rotateY";
	rename -uid "E6147C97-414A-D20E-4917-F295FFDDEC33";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 3.2039728164672852 1 3.1869349479675293
		 2 3.1389949321746826 3 3.064877986907959 4 2.9692835807800293 5 2.8569200038909912
		 6 2.7325282096862793 7 2.6008968353271484 8 2.4668748378753662 9 2.3353769779205322
		 10 2.2113795280456543 11 2.0999164581298828 12 2.00606369972229 13 1.9349215030670166
		 14 1.8915916681289675 15 1.8703997135162354 16 1.8614054918289187 17 1.8633010387420654
		 18 1.8747811317443848 19 1.8945428133010866 20 1.9212845563888548 21 1.9537036418914795
		 22 1.9904990196228027 23 2.0303716659545898 24 2.0720274448394775 25 2.1141781806945801
		 26 2.1555473804473877 27 2.1948745250701904 28 2.2309191226959229 29 2.2883737087249756
		 30 2.3867480754852295 31 2.5168988704681396 32 2.6696398258209229 33 2.8359003067016602
		 34 3.0068409442901611 35 3.173931360244751 36 3.3289759159088135 37 3.4640946388244629
		 38 3.5716471672058105 39 3.6441044807434078 40 3.6738653182983398 41 3.6547303199768066
		 42 3.5903308391571045 43 3.4875414371490479 44 3.3531959056854248 45 3.1941449642181396
		 46 3.0173103809356689 47 2.8297207355499268 48 2.6385350227355957 49 2.4510571956634521
		 50 2.2747359275817871 51 2.1171553134918213 52 1.9860163927078247 53 1.88910448551178
		 54 1.8342510461807251 55 1.8292871713638306 56 1.8819818496704102 57 1.9746941328048704
		 58 2.0815122127532959 59 2.1988568305969238 60 2.3230900764465332 61 2.4505906105041504
		 62 2.577812671661377 63 2.7013287544250488 64 2.8178563117980957 65 2.9242696762084961
		 66 3.0175929069519043 67 3.0949776172637939 68 3.1536624431610107 69 3.1909189224243164
		 70 3.2039728164672852;
createNode animCurveTA -n "Character1_Spine_rotateZ";
	rename -uid "75E54FE5-43CF-4A3B-3525-6EBD57B34A00";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 7.2477531433105469 1 7.2336263656616202
		 2 7.1939520835876465 3 7.1328492164611816 4 7.0544710159301758 5 6.9629592895507812
		 6 6.8624124526977539 7 6.7568531036376953 8 6.6502118110656738 9 6.5463204383850098
		 10 6.4489107131958008 11 6.3616247177124023 12 6.2880358695983887 13 6.2316765785217285
		 14 6.1960792541503906 15 6.1743097305297852 16 6.1564435958862305 17 6.141547679901123
		 18 6.1286821365356445 19 6.1169071197509766 20 6.1052885055541992 21 6.0928983688354492
		 22 6.0788235664367676 23 6.0621623992919922 24 6.042027473449707 25 6.0175471305847168
		 26 5.9878597259521484 27 5.9521188735961914 28 5.909482479095459 29 5.886993408203125
		 30 5.9075045585632324 31 5.9639616012573242 32 6.0492029190063477 33 6.1557793617248535
		 34 6.275825023651123 35 6.4010047912597656 36 6.5225133895874023 37 6.6311397552490234
		 38 6.7173833847045898 39 6.7716274261474609 40 6.784367561340332 41 6.7511448860168457
		 42 6.6779670715332031 43 6.571385383605957 44 6.438025951385498 45 6.2844977378845215
		 46 6.1173253059387207 47 5.9428939819335938 48 5.7674107551574707 49 5.5968804359436035
		 50 5.4371018409729004 51 5.2936773300170898 52 5.1720414161682129 53 5.0775055885314941
		 54 5.0153188705444336 55 4.9907522201538086 56 5.0091924667358398 57 5.0776047706604004
		 58 5.1934375762939453 59 5.3484892845153809 60 5.5345454216003418 61 5.743349552154541
		 62 5.9665799140930176 63 6.1958365440368652 64 6.4226326942443848 65 6.6384010314941406
		 66 6.8344969749450684 67 7.0022182464599609 68 7.1328258514404297 69 7.2175765037536621
		 70 7.2477531433105469;
createNode animCurveTU -n "Character1_Spine_visibility";
	rename -uid "52058EE8-403A-4DC3-5BD5-E686109F6C7F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_Spine1_translateX";
	rename -uid "73586EA2-4105-A902-7FB1-279C7AA0D19A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2515636682510376;
createNode animCurveTL -n "Character1_Spine1_translateY";
	rename -uid "0C03F259-4EE9-37A1-E5D5-1EB8CBE82384";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -15.818062782287598;
createNode animCurveTL -n "Character1_Spine1_translateZ";
	rename -uid "229AA06B-4BB6-5FA1-D6D7-838D5D460D79";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.5716743469238281;
createNode animCurveTU -n "Character1_Spine1_scaleX";
	rename -uid "5BC2361D-4F5D-4595-7BE6-048F9159551A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine1_scaleY";
	rename -uid "6C2519C8-460A-FF2A-93D2-1C9F0319623C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine1_scaleZ";
	rename -uid "24388820-473B-FD2D-B450-B091AC8C8E18";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_Spine1_rotateX";
	rename -uid "AC438CE1-42FC-FA03-0F72-4CB570F10E68";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -17.41119384765625 1 -17.415069580078125
		 2 -17.470781326293945 3 -17.575273513793945 4 -17.725534439086914 5 -17.918594360351563
		 6 -18.151517868041992 7 -18.421413421630859 8 -18.725421905517578 9 -19.06072998046875
		 10 -19.424552917480469 11 -19.81414794921875 12 -20.226804733276367 13 -20.659833908081055
		 14 -21.110597610473633 15 -21.573818206787109 16 -22.042388916015625 17 -22.510828018188477
		 18 -22.973546981811523 19 -23.424777984619141 20 -23.858583450317383 21 -24.26884651184082
		 22 -24.649271011352539 23 -24.993389129638672 24 -25.294595718383789 25 -25.546165466308594
		 26 -25.741304397583008 27 -25.873195648193359 28 -25.935054779052734 29 -25.973926544189453
		 30 -26.035825729370117 31 -26.112567901611328 32 -26.196075439453125 33 -26.278373718261719
		 34 -26.351570129394531 35 -26.40782356262207 36 -26.439268112182617 37 -26.437965393066406
		 38 -26.395822525024414 39 -26.304529190063477 40 -26.155464172363281 41 -25.958538055419922
		 42 -25.731937408447266 43 -25.479259490966797 44 -25.20421028137207 45 -24.910604476928711
		 46 -24.60240364074707 47 -24.283721923828125 48 -23.958885192871094 49 -23.63245964050293
		 50 -23.309305191040039 51 -22.994592666625977 52 -22.693824768066406 53 -22.412790298461914
		 54 -22.157535552978516 55 -21.934234619140625 56 -21.749092102050781 57 -21.547382354736328
		 58 -21.271926879882812 59 -20.937864303588867 60 -20.560291290283203 61 -20.154014587402344
		 62 -19.733434677124023 63 -19.312423706054688 64 -18.904293060302734 65 -18.521799087524414
		 66 -18.177194595336914 67 -17.882320404052734 68 -17.648735046386719 69 -17.487859725952148
		 70 -17.41119384765625;
createNode animCurveTA -n "Character1_Spine1_rotateY";
	rename -uid "8D59BE76-4BF9-46DD-3AB3-C18CD9FC397F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 33.526878356933594 1 33.387458801269531
		 2 33.256923675537109 3 33.135536193847656 4 33.023555755615234 5 32.921241760253906
		 6 32.828853607177734 7 32.74664306640625 8 32.67486572265625 9 32.613761901855469
		 10 32.563560485839844 11 32.524486541748047 12 32.496742248535156 13 32.480518341064453
		 14 32.475994110107422 15 32.495414733886719 16 32.5467529296875 17 32.623996734619141
		 18 32.721420288085938 19 32.833583831787109 20 32.955303192138672 21 33.081642150878906
		 22 33.207881927490234 23 33.329479217529297 24 33.442035675048828 25 33.541248321533203
		 26 33.622852325439453 27 33.682563781738281 28 33.716014862060547 29 33.677127838134766
		 30 33.537216186523438 31 33.317897796630859 32 33.040802001953125 33 32.727550506591797
		 34 32.399742126464844 35 32.078933715820313 36 31.786615371704105 37 31.544229507446286
		 38 31.373142242431637 39 31.294696807861328 40 31.330196380615231 41 31.489246368408207
		 42 31.755826950073239 43 32.112819671630859 44 32.543125152587891 45 33.029640197753906
		 46 33.555294036865234 47 34.103015899658203 48 34.655773162841797 49 35.196575164794922
		 50 35.708488464355469 51 36.174640655517578 52 36.578227996826172 53 36.902523040771484
		 54 37.130828857421875 55 37.246479034423828 56 37.232791900634766 57 37.114707946777344
		 58 36.930034637451172 59 36.689598083496094 60 36.404209136962891 61 36.084716796875
		 62 35.742080688476563 63 35.387386322021484 64 35.031875610351563 65 34.686927795410156
		 66 34.364059448242188 67 34.074901580810547 68 33.831153869628906 69 33.644561767578125
		 70 33.526878356933594;
createNode animCurveTA -n "Character1_Spine1_rotateZ";
	rename -uid "2E197CB9-460B-5DC3-3649-85AA3804614E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  0 3.2983624935150146 1 3.3130137920379639
		 2 3.336970329284668 3 3.3698711395263672 4 3.4112772941589355 5 3.4606714248657227
		 6 3.5174741744995117 7 3.581031322479248 8 3.6506175994873042 9 3.7254445552825923
		 10 3.8046596050262451 11 3.8873486518859868 12 3.9725453853607182 13 4.0592288970947266
		 14 4.1463232040405273 15 4.2011990547180176 16 4.1969928741455078 17 4.1414651870727539
		 18 4.0426545143127441 19 3.9088695049285889 20 3.7486975193023677 21 3.5709774494171143
		 22 3.384782075881958 23 3.1993937492370605 24 3.0242602825164795 25 2.8689465522766113
		 26 2.7430925369262695 27 2.6563386917114258 28 2.6182601451873779 29 2.6204676628112793
		 30 2.6462206840515137 31 2.6926367282867432 32 2.756770133972168 33 2.8353545665740967
		 34 2.9246451854705811 35 3.0203533172607422 36 3.1177082061767578 37 3.2115864753723145
		 38 3.2967569828033447 39 3.3682057857513428 40 3.421572208404541 41 3.457890510559082
		 42 3.4822320938110352 43 3.4971816539764404 44 3.5052666664123535 45 3.5087320804595947
		 48 3.5058469772338867 49 3.5016984939575195 50 3.4944841861724854 51 3.4820215702056885
		 52 3.4613132476806641 53 3.4286761283874512 54 3.3799276351928711 55 3.3106656074523926
		 56 3.2165772914886475 57 3.1228148937225342 58 3.0617849826812744 59 3.0280556678771973
		 60 3.0163192749023438 61 3.0216293334960937 62 3.0395541191101074 63 3.0662617683410645
		 64 3.0985662937164307 65 3.1339051723480225 66 3.1702759265899658 67 3.2061276435852051
		 68 3.2402081489562988 69 3.2713727951049805 70 3.2983624935150146;
createNode animCurveTU -n "Character1_Spine1_visibility";
	rename -uid "4C3A98BD-4FB4-FE17-8B18-89A0329B20B1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftShoulder_translateX";
	rename -uid "AD6AF948-4AD2-5E29-B833-9680DFD916B4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.49134865403175354;
createNode animCurveTL -n "Character1_LeftShoulder_translateY";
	rename -uid "0EED63F1-4A4E-0AAF-586D-EC99BEEE9A58";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.20281982421875;
createNode animCurveTL -n "Character1_LeftShoulder_translateZ";
	rename -uid "12F6D7EF-44D1-021D-4CB3-98B5B7AE131D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.910428047180176;
createNode animCurveTU -n "Character1_LeftShoulder_scaleX";
	rename -uid "AFC266C5-4E11-9419-0127-D7A47EA4F2B7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftShoulder_scaleY";
	rename -uid "8FFC3DBD-4E9E-83D5-9B35-1E92C887A37C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999934434890747;
createNode animCurveTU -n "Character1_LeftShoulder_scaleZ";
	rename -uid "004FEAB8-4692-ADD2-8E00-0A8F18F6F1FD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftShoulder_rotateX";
	rename -uid "EAEEE0B1-43DC-F9AE-96CA-3383680BFE17";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 0.96115404367446899 1 1.2423583269119263
		 2 1.6063193082809448 3 2.0352640151977539 4 2.5110306739807129 5 3.0154509544372559
		 6 3.5306429862976074 7 4.0392465591430664 8 4.5245804786682129 9 4.970731258392334
		 10 5.3625631332397461 11 5.6856374740600586 12 5.9260425567626953 13 6.070127010345459
		 14 6.1041278839111328 15 6.0651469230651855 16 5.9983859062194824 17 5.9019203186035156
		 18 5.7738676071166992 19 5.6123695373535156 20 5.4155788421630859 21 5.181633472442627
		 22 4.9086356163024902 23 4.5946226119995117 24 4.2375373840332031 25 3.8351974487304683
		 26 3.3852593898773193 27 2.8851816654205322 28 2.332186222076416 29 1.6914577484130859
		 30 0.9459107518196106 31 0.11957027018070221 32 -0.76283299922943115 33 -1.6757601499557495
		 34 -2.5928523540496826 35 -3.4870121479034424 36 -4.33056640625 37 -5.0954980850219727
		 38 -5.7537569999694824 39 -6.2776427268981934 40 -6.6402812004089355 41 -6.8648595809936523
		 42 -6.9990658760070801 43 -7.0534453392028809 44 -7.0381708145141602 45 -6.9630770683288574
		 46 -6.8377056121826172 47 -6.6713361740112305 48 -6.4730205535888672 49 -6.2516164779663086
		 50 -6.0158143043518066 51 -5.7741661071777344 52 -5.5351157188415527 53 -5.3070225715637207
		 54 -5.0981926918029785 55 -4.9169011116027832 56 -4.7714262008666992 57 -4.5983304977416992
		 58 -4.3368325233459473 59 -3.9984803199768066 60 -3.5944201946258545 61 -3.1360104084014893
		 62 -2.6353368759155273 63 -2.1056489944458008 64 -1.5616880655288696 65 -1.019920825958252
		 66 -0.49863731861114502 67 -0.017892727628350258 68 0.40075165033340454 69 0.73471802473068237
		 70 0.96115404367446899;
createNode animCurveTA -n "Character1_LeftShoulder_rotateY";
	rename -uid "3C75D915-443E-C1AD-946A-BAA3BCD11446";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 4.7950630187988281 1 4.2767438888549805
		 2 3.7436373233795166 3 3.2039244174957275 4 2.6653280258178711 5 2.1355369091033936
		 6 1.6225467920303345 7 1.1348869800567627 8 0.68176138401031494 9 0.27309611439704895
		 10 -0.080495022237300873 11 -0.36781901121139526 12 -0.57727766036987305 13 -0.69710791110992432
		 14 -0.71568244695663452 15 -0.65402710437774658 16 -0.54115766286849976 17 -0.37772610783576965
		 18 -0.16436381638050079 19 0.098313160240650177 20 0.40968507528305054 21 0.76912194490432739
		 22 1.1759614944458008 23 1.6294903755187988 24 2.1289267539978027 25 2.6733889579772949
		 26 3.2618720531463623 27 3.893214225769043 28 4.566065788269043 29 5.2866311073303223
		 30 6.0561513900756836 31 6.8659958839416504 32 7.7081241607666025 33 8.5752220153808594
		 34 9.4607334136962891 35 10.358817100524902 36 11.26419734954834 37 12.171915054321289
		 38 13.076967239379883 39 13.973837852478027 40 14.855904579162598 41 15.689186096191404
		 42 16.445426940917969 43 17.125581741333008 44 17.730854034423828 45 18.262641906738281
		 46 18.722522735595703 47 19.112226486206055 48 19.433609008789063 49 19.688634872436523
		 50 19.879358291625977 51 20.007902145385742 52 20.076450347900391 53 20.087221145629883
		 54 20.04246711730957 55 19.944448471069336 56 19.795434951782227 57 19.446340560913086
		 58 18.781137466430664 59 17.851816177368164 60 16.710390090942383 61 15.408734321594238
		 62 13.998400688171387 63 12.530492782592773 64 11.055511474609375 65 9.6232614517211914
		 66 8.2827816009521484 67 7.0823464393615723 68 6.0695476531982422 69 5.2914838790893555
		 70 4.7950630187988281;
createNode animCurveTA -n "Character1_LeftShoulder_rotateZ";
	rename -uid "76294E26-430C-2085-EBEB-35B442078AD9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 9.8116216659545898 1 9.7645092010498047
		 2 9.9512853622436523 3 10.33360767364502 4 10.873672485351563 5 11.534225463867188
		 6 12.278508186340332 7 13.070205688476562 8 13.873364448547363 9 14.652304649353029
		 10 15.371520042419434 11 15.995577812194822 12 16.489009857177734 13 16.816194534301758
		 14 16.941259384155273 15 16.953447341918945 16 16.961856842041016 17 16.960037231445313
		 18 16.941459655761719 19 16.899497985839844 20 16.827455520629883 21 16.718540191650391
		 22 16.565876007080078 23 16.362499237060547 24 16.101358413696289 25 15.775308609008789
		 26 15.377109527587892 27 14.899404525756836 28 14.334733963012697 29 13.655157089233398
		 30 12.862736701965332 31 11.995145797729492 32 11.090432167053223 33 10.187104225158691
		 34 9.3242206573486328 35 8.5414829254150391 36 7.8793435096740732 37 7.3790884017944327
		 38 7.0829167366027832 39 7.0339803695678711 40 7.2763671875 41 7.6933431625366211
		 42 8.1355152130126953 43 8.5984182357788086 44 9.0773143768310547 45 9.5672130584716797
		 46 10.062906265258789 47 10.558988571166992 48 11.049888610839844 49 11.529892921447754
		 50 11.993170738220215 51 12.43380069732666 52 12.845791816711426 53 13.223114967346191
		 54 13.559719085693359 55 13.849560737609863 56 14.086629867553711 57 14.211702346801758
		 58 14.183354377746582 59 14.022102355957031 60 13.748409271240234 61 13.382952690124512
		 62 12.946907043457031 63 12.46222972869873 64 11.951935768127441 65 11.440337181091309
		 66 10.953197479248047 67 10.517757415771484 68 10.162590026855469 69 9.9172391891479492
		 70 9.8116216659545898;
createNode animCurveTU -n "Character1_LeftShoulder_visibility";
	rename -uid "A6EC5146-4F1B-D84F-C68E-B3BBF66E784C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftArm_translateX";
	rename -uid "ED05CB0D-4CC5-D3C6-8BF1-EDB2B98B8DF9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.39077091217041;
createNode animCurveTL -n "Character1_LeftArm_translateY";
	rename -uid "BF3B1B4E-445A-FB72-B850-6AAB180B01A0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.8805270195007324;
createNode animCurveTL -n "Character1_LeftArm_translateZ";
	rename -uid "D51F04CC-44C5-FB6B-E045-AABD48011C8E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8116312026977539;
createNode animCurveTU -n "Character1_LeftArm_scaleX";
	rename -uid "F6B79083-461F-F734-6841-998BD2B186AF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999964237213135;
createNode animCurveTU -n "Character1_LeftArm_scaleY";
	rename -uid "8E73BD07-4DCC-CC3C-F3CE-4B8F8381EBC8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleZ";
	rename -uid "C10D34F4-43BB-A73A-795B-52A97512C07A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_LeftArm_rotateX";
	rename -uid "86A225C1-4782-F991-EC64-E49976E1B8F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 42.705905914306641 1 43.067817687988281
		 2 43.501052856445313 3 43.998744964599609 4 44.551937103271484 5 45.150600433349609
		 6 45.784324645996094 7 46.442592620849609 8 47.114852905273437 9 47.790382385253906
		 10 48.458164215087891 11 49.106777191162109 12 49.72442626953125 13 50.299175262451172
		 14 50.819366455078125 15 51.230476379394531 16 51.485607147216797 17 51.585346221923828
		 18 51.53057861328125 19 51.322471618652344 20 50.962486267089844 21 50.452350616455078
		 22 49.794044494628906 23 48.989837646484375 24 48.042289733886719 25 46.954319000244141
		 26 45.729270935058594 27 44.370994567871094 28 42.883945465087891 29 41.218284606933594
		 30 39.393112182617188 31 37.516338348388672 32 35.688938140869141 33 33.998401641845703
		 34 32.513362884521484 35 31.279308319091793 36 30.315498352050785 37 29.613945007324219
		 38 29.142005920410156 39 28.850206375122067 40 28.686710357666016 41 28.72584342956543
		 42 29.043281555175785 43 29.601739883422852 44 30.366016387939453 45 31.301532745361332
		 46 32.373126983642578 47 33.544212341308594 48 34.776351928710938 49 36.029281616210938
		 50 37.261283874511719 51 38.429801940917969 52 39.492168426513672 53 40.406261444091797
		 54 41.131072998046875 55 41.627056121826172 56 41.856525421142578 57 41.913734436035156
		 58 41.926315307617188 59 41.907630920410156 60 41.870151519775391 61 41.825504302978516
		 62 41.784511566162109 63 41.757293701171875 64 41.75335693359375 65 41.781719207763672
		 66 41.851036071777344 67 41.969738006591797 68 42.146167755126953 69 42.388710021972656
		 70 42.705905914306641;
createNode animCurveTA -n "Character1_LeftArm_rotateY";
	rename -uid "02154886-496D-5173-7A50-3DBC69E29487";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 43.850231170654297 1 43.063034057617188
		 2 41.974044799804688 3 40.636489868164063 4 39.104404449462891 5 37.433067321777344
		 6 35.679256439208984 7 33.901325225830078 8 32.159259796142578 9 30.514493942260746
		 10 29.029722213745114 11 27.768669128417969 12 26.79571533203125 13 26.175674438476563
		 14 25.973442077636719 15 26.070236206054688 16 26.298177719116211 17 26.647586822509766
		 18 27.107805252075195 19 27.667354583740234 20 28.314033508300781 21 29.035013198852543
		 22 29.816902160644528 23 30.645795822143555 24 31.507366180419918 25 32.386913299560547
		 26 33.269474029541016 27 34.139907836914063 28 34.983078002929687 29 35.962314605712891
		 30 37.221637725830078 31 38.713119506835938 32 40.379802703857422 33 42.154411315917969
		 34 43.961200714111328 35 45.719875335693359 36 47.350090026855469 37 48.775363922119141
		 38 49.925525665283203 39 50.736957550048828 40 51.150596618652344 41 51.349948883056641
		 42 51.555679321289063 43 51.762355804443359 44 51.961612701416016 45 52.143611907958984
		 46 52.298328399658203 47 52.416614532470703 48 52.49102783203125 49 52.516395568847656
		 50 52.490161895751953 51 52.412307739257813 52 52.285041809082031 53 52.112106323242187
		 54 51.897769927978516 55 51.645481109619141 56 51.356330871582031 57 51.035713195800781
		 58 50.687690734863281 59 50.309238433837891 60 49.898212432861328 61 49.453170776367188
		 62 48.973236083984375 63 48.457965850830078 64 47.90716552734375 65 47.320835113525391
		 66 46.698963165283203 67 46.041450500488281 68 45.347988128662109 69 44.617946624755859
		 70 43.850231170654297;
createNode animCurveTA -n "Character1_LeftArm_rotateZ";
	rename -uid "51BEEB78-4FE6-0371-C3E4-16919B45FE02";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -25.906341552734375 1 -25.362447738647461
		 2 -24.605632781982422 3 -23.680376052856445 4 -22.630970001220703 5 -21.499719619750977
		 6 -20.325681686401367 7 -19.143856048583984 8 -17.984859466552734 9 -16.874845504760742
		 10 -15.835768699645996 11 -14.885817527770994 12 -14.040116310119629 13 -13.311545372009277
		 14 -12.711846351623535 15 -12.279024124145508 16 -12.037960052490234 17 -11.98214054107666
		 18 -12.106809616088867 19 -12.408734321594238 20 -12.885836601257324 21 -13.536967277526855
		 22 -14.361545562744139 23 -15.359298706054689 24 -16.529914855957031 25 -17.872766494750977
		 26 -19.386554718017578 27 -21.069005966186523 28 -22.916555404663086 29 -24.774866104125977
		 30 -26.511774063110352 31 -28.14836311340332 32 -29.694248199462891 33 -31.147182464599606
		 34 -32.495338439941406 35 -33.722328186035156 36 -34.814334869384766 37 -35.767440795898438
		 38 -36.592586517333984 39 -37.315673828125 40 -37.97100830078125 41 -38.352161407470703
		 42 -38.272148132324219 43 -37.792819976806641 44 -36.975543975830078 45 -35.882129669189453
		 46 -34.575553894042969 47 -33.120471954345703 48 -31.583330154418945 49 -30.032127380371094
		 50 -28.535831451416016 51 -27.163656234741211 52 -25.984220504760742 53 -25.064836502075195
		 54 -24.470935821533203 55 -24.265691757202148 56 -24.509737014770508 57 -24.996932983398438
		 58 -25.472391128540039 59 -25.922084808349609 60 -26.332763671875 61 -26.691951751708984
		 62 -26.987930297851563 63 -27.209728240966797 64 -27.34709358215332 65 -27.39045524597168
		 66 -27.330898284912109 67 -27.160116195678711 68 -26.87040901184082 69 -26.454660415649414
		 70 -25.906341552734375;
createNode animCurveTU -n "Character1_LeftArm_visibility";
	rename -uid "2EA544FE-4132-1279-1038-838B263B4900";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftForeArm_translateX";
	rename -uid "798348EE-4D01-E741-2D09-CDB34CE59B71";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.9387187957763672;
createNode animCurveTL -n "Character1_LeftForeArm_translateY";
	rename -uid "A20CC59A-4669-C496-1BC3-04A1FA760E2A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -14.273021697998047;
createNode animCurveTL -n "Character1_LeftForeArm_translateZ";
	rename -uid "00B8230D-4133-42DE-0655-DAB7ED3106AB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.7470760345458984;
createNode animCurveTU -n "Character1_LeftForeArm_scaleX";
	rename -uid "F8C7AF66-4472-62B3-D13D-2593C00095FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftForeArm_scaleY";
	rename -uid "3867E38E-4352-FC3C-12B5-89A6B12B2941";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftForeArm_scaleZ";
	rename -uid "CFB2D0CA-49FB-8DB0-8DCC-5EB21973BB52";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftForeArm_rotateX";
	rename -uid "9C7723D5-4E08-B5A0-B907-9D98BD908791";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -18.374488830566406 1 -18.449102401733398
		 2 -18.664695739746094 3 -18.996749877929688 4 -19.420608520507813 5 -19.9122314453125
		 6 -20.448705673217773 7 -21.008573532104492 8 -21.571950912475586 9 -22.120429992675781
		 10 -22.636795043945313 11 -23.104513168334961 12 -23.507020950317383 13 -23.826831817626953
		 14 -24.044437408447266 15 -24.178756713867188 16 -24.261716842651367 17 -24.289009094238281
		 18 -24.255956649780273 19 -24.159927368164063 20 -24.002042770385742 21 -23.788190841674805
		 22 -23.529407501220703 23 -23.241689682006836 24 -22.945390701293945 25 -22.664180755615234
		 26 -22.423732757568359 27 -22.250085830688477 28 -22.167724609375 29 -22.143404006958008
		 30 -22.129899978637695 31 -22.128227233886719 32 -22.13939094543457 33 -22.164403915405273
		 34 -22.204273223876953 35 -22.260011672973633 36 -22.332632064819336 37 -22.42315673828125
		 38 -22.532619476318359 39 -22.662071228027344 40 -22.812570571899414 41 -23.017282485961914
		 42 -23.300069808959961 43 -23.648006439208984 44 -24.048175811767578 45 -24.487766265869141
		 46 -24.954137802124023 47 -25.434864044189453 48 -25.917728424072266 49 -26.390737533569336
		 50 -26.842069625854492 51 -27.260019302368164 52 -27.632925033569336 53 -27.949069976806641
		 54 -28.196569442749023 55 -28.363241195678711 56 -28.436460494995117 57 -28.285661697387695
		 58 -27.811923980712891 59 -27.080923080444336 60 -26.161273956298828 61 -25.120414733886719
		 62 -24.021396636962891 63 -22.920673370361328 64 -21.866838455200195 65 -20.900394439697266
		 66 -20.054380416870117 67 -19.355817794799805 68 -18.827865600585938 69 -18.492607116699219
		 70 -18.374488830566406;
createNode animCurveTA -n "Character1_LeftForeArm_rotateY";
	rename -uid "5EBB3BA8-4E51-1B18-FD8D-CBA0259B0E0A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -13.094388961791992 1 -13.223658561706543
		 2 -13.597091674804688 3 -14.176775932312012 4 -14.924798011779785 5 -15.802929878234863
		 6 -16.77241325378418 7 -17.793865203857422 8 -18.827266693115234 9 -19.832048416137695
		 10 -20.767257690429688 11 -21.591745376586914 12 -22.26441764831543 13 -22.74458122253418
		 14 -22.992284774780273 15 -23.007301330566406 16 -22.836484909057617 17 -22.512407302856445
		 18 -22.068410873413086 19 -21.537837982177734 20 -20.953250885009766 21 -20.345685958862305
		 22 -19.744009017944336 23 -19.174455642700195 24 -18.660501480102539 25 -18.223045349121094
		 26 -17.881072998046875 27 -17.652713775634766 28 -17.556745529174805 29 -17.537883758544922
		 30 -17.528289794921875 31 -17.529481887817383 32 -17.542972564697266 33 -17.570281982421875
		 34 -17.612926483154297 35 -17.672426223754883 36 -17.750289916992187 37 -17.848024368286133
		 38 -17.967130661010742 39 -18.109109878540039 40 -18.275436401367188 41 -18.505487442016602
		 42 -18.827913284301758 43 -19.228158950805664 44 -19.691652297973633 45 -20.203742980957031
		 46 -20.749645233154297 47 -21.314399719238281 48 -21.88286018371582 49 -22.439714431762695
		 50 -22.969493865966797 51 -23.456615447998047 52 -23.88543701171875 53 -24.240324020385742
		 54 -24.5057373046875 55 -24.666322708129883 56 -24.707023620605469 57 -24.547674179077148
		 58 -24.122817993164063 59 -23.46760368347168 60 -22.615100860595703 61 -21.599756240844727
		 62 -20.459831237792969 63 -19.238807678222656 64 -17.985836029052734 65 -16.755456924438477
		 66 -15.606757164001463 67 -14.602129936218262 68 -13.805822372436523 69 -13.282303810119629
		 70 -13.094388961791992;
createNode animCurveTA -n "Character1_LeftForeArm_rotateZ";
	rename -uid "7FA545C8-439A-5935-9CD8-7A9C817FF570";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -22.465702056884766 1 -22.561973571777344
		 2 -22.804790496826172 3 -23.165035247802734 4 -23.61332893371582 5 -24.120367050170898
		 6 -24.65716552734375 7 -25.195169448852539 8 -25.706319808959961 9 -26.163005828857422
		 10 -26.538064956665039 11 -26.804750442504883 12 -26.936763763427734 13 -26.908363342285156
		 14 -26.694482803344727 15 -26.210813522338867 16 -25.42724609375 17 -24.401050567626953
		 18 -23.189332962036133 19 -21.848260879516602 20 -20.432621002197266 21 -18.995649337768555
		 22 -17.589096069335937 23 -16.263439178466797 24 -15.068174362182619 25 -14.052136421203613
		 26 -13.263843536376953 27 -12.75185489654541 28 -12.565174102783203 29 -12.560600280761719
		 30 -12.561793327331543 31 -12.568661689758301 32 -12.581114768981934 33 -12.599054336547852
		 34 -12.622373580932617 35 -12.650961875915527 36 -12.684685707092285 37 -12.723396301269531
		 38 -12.766923904418945 39 -12.815073013305664 40 -12.867611885070801 41 -12.95380973815918
		 42 -13.095937728881836 43 -13.283220291137695 44 -13.504921913146973 45 -13.75042724609375
		 46 -14.009300231933594 47 -14.271327972412109 48 -14.526534080505369 49 -14.765206336975098
		 50 -14.977878570556641 51 -15.155326843261719 52 -15.288538932800291 53 -15.368681907653809
		 54 -15.387055397033691 55 -15.335039138793944 56 -15.204013824462891 57 -15.186628341674805
		 58 -15.437385559082031 59 -15.904789924621582 60 -16.535472869873047 61 -17.277120590209961
		 62 -18.080715179443359 63 -18.902050018310547 64 -19.702428817749023 65 -20.448644638061523
		 66 -21.11229133605957 67 -21.668571472167969 68 -22.094779968261719 69 -22.368520736694336
		 70 -22.465702056884766;
createNode animCurveTU -n "Character1_LeftForeArm_visibility";
	rename -uid "65B3094B-4736-D38A-6395-FDA168A0C241";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHand_translateX";
	rename -uid "7E12E25D-4464-07C8-DE31-BB81E854E46F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.95125424861907959;
createNode animCurveTL -n "Character1_LeftHand_translateY";
	rename -uid "8D87F1C7-4B6C-D515-D5A7-748FAD1493F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.299518585205078;
createNode animCurveTL -n "Character1_LeftHand_translateZ";
	rename -uid "A05EFBB7-43EE-BE80-EA60-43853CF29330";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 12.872089385986328;
createNode animCurveTU -n "Character1_LeftHand_scaleX";
	rename -uid "7C8CFC19-4809-1529-C08C-F99B617E5918";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHand_scaleY";
	rename -uid "236D71C0-4136-176B-F406-278ADA4B7A7D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHand_scaleZ";
	rename -uid "4C1B9FDE-4119-DC4A-B543-678C010D23CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHand_rotateX";
	rename -uid "F0FE35EF-44A5-E54B-892B-04941B1A210F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 30 ".ktv[0:29]"  14 -14.319267272949219 15 -14.313579559326172
		 16 -14.297652244567871 17 -14.273184776306152 18 -14.241879463195801 19 -14.205437660217285
		 20 -14.165562629699707 21 -14.123957633972168 22 -14.082334518432617 23 -14.042403221130371
		 24 -14.005879402160645 25 -13.974479675292969 26 -13.949922561645508 27 -13.933929443359375
		 28 -13.928216934204102 56 -13.928216934204102 57 -13.932072639465332 58 -13.943069458007813
		 59 -13.960346221923828 60 -13.983043670654297 61 -14.010302543640137 62 -14.041261672973633
		 63 -14.07506275177002 64 -14.110845565795898 65 -14.147753715515137 66 -14.184932708740234
		 67 -14.221525192260742 68 -14.256679534912109 69 -14.289544105529785 70 -14.319267272949219;
createNode animCurveTA -n "Character1_LeftHand_rotateY";
	rename -uid "83BCA34C-437B-B7C7-01B5-16A2C3F3EF57";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  17 2.1249265670776367 18 2.1298472881317139
		 19 2.1356265544891357 20 2.1420121192932129 21 2.1487436294555664 22 2.1555490493774414
		 23 2.1621437072753906 24 2.1682331562042236 25 2.1735107898712158 60 2.172067403793335
		 61 2.167492151260376 62 2.1623334884643555 63 2.1567449569702148 64 2.1508796215057373
		 65 2.1448845863342285 66 2.1389017105102539 67 2.1330685615539551 68 2.1275157928466797
		 69 2.1223704814910889 70 2.1177551746368408;
createNode animCurveTA -n "Character1_LeftHand_rotateZ";
	rename -uid "FCFFD437-4733-FDE6-A540-7AAE12E17CF6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 30 ".ktv[0:29]"  14 -3.3737902641296387 15 -3.3814487457275391
		 16 -3.4028909206390381 17 -3.4358158111572266 18 -3.4779195785522461 19 -3.5269019603729248
		 20 -3.5804612636566162 21 -3.6362993717193604 22 -3.6921198368072514 23 -3.7456309795379643
		 24 -3.7945415973663335 25 -3.8365628719329838 26 -3.8694097995758061 27 -3.8907947540283203
		 28 -3.8984317779541016 56 -3.8984317779541016 57 -3.8932769298553467 58 -3.8785753250122075
		 59 -3.8554694652557377 60 -3.8251035213470459 61 -3.7886195182800293 62 -3.7471597194671631
		 63 -3.7018685340881348 64 -3.6538884639739995 65 -3.6043667793273921 66 -3.5544486045837402
		 67 -3.5052828788757324 68 -3.4580175876617432 69 -3.4138028621673584 70 -3.3737902641296387;
createNode animCurveTU -n "Character1_LeftHand_visibility";
	rename -uid "E23EF12C-4115-005E-8E81-1AA2B999BB88";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateX";
	rename -uid "47C98AB1-48B1-4290-3524-888184BE5A82";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.1041760444641113;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateY";
	rename -uid "8D23866D-4D32-40E2-6C1E-5FBC764C738E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.7491040229797363;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateZ";
	rename -uid "6A00F093-4A1F-5866-7254-09BABD98E6CE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.7276492118835449;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleX";
	rename -uid "D6B58E7C-41E4-1169-9EE0-1DB20093B671";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleY";
	rename -uid "506D37BD-4E4C-A8FE-E4E3-27A3A1CB0150";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleZ";
	rename -uid "26E8DDAA-4AD9-B02A-0807-BC9BB6FFB324";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateX";
	rename -uid "DF395A4E-4AD8-67DE-2E99-269EA77A7F0F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 30.875671386718754;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateY";
	rename -uid "E1EB9A4C-4676-5DBF-A047-B49CB3B817F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 51.291091918945312;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateZ";
	rename -uid "0DA0C2B3-4909-7E25-1F44-DC823F6EEFA7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 63.124191284179687;
createNode animCurveTU -n "Character1_LeftHandThumb1_visibility";
	rename -uid "A49F1916-443A-A2CD-CB09-EFACB6213F4F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateX";
	rename -uid "3B7CF606-4A31-1847-7E1B-CEB05B5572BE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.3594832420349121;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateY";
	rename -uid "018171FB-4868-A2C4-8ADF-B6A60FB2E025";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.8193249702453613;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateZ";
	rename -uid "63886BA8-4C3E-2135-721A-4CB10DD18C06";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3286550045013428;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleX";
	rename -uid "51120804-44D7-1EA0-1A68-7A9FC09DFDFD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleY";
	rename -uid "308175EF-4FAE-3C99-7DC7-1CB113E8C4E3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleZ";
	rename -uid "BE62466C-49D1-9F1C-7508-C5BB8200D86B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateX";
	rename -uid "FA8F57C1-4FEA-9EC8-40E5-FEABA7255E9D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 22.477453231811523;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateY";
	rename -uid "B7E3F898-48E1-A238-2932-43AE4B495457";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.664257049560547;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateZ";
	rename -uid "486D672F-4182-5485-B6C1-9BAFE566767C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.6664080619812012;
createNode animCurveTU -n "Character1_LeftHandThumb2_visibility";
	rename -uid "C20C41E6-428D-6565-96F3-689B9A94F6EC";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateX";
	rename -uid "666A8D14-4E0B-2691-15DE-20889370CC9F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1733376979827881;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateY";
	rename -uid "E51D452C-49A4-D0EF-F9E3-CE9D342BA9A9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.34823575615882874;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateZ";
	rename -uid "4E580F2F-4171-8E1B-AD2B-1FA1AEAF4830";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.468076229095459;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleX";
	rename -uid "2E2B6071-4EAB-951D-0A35-3DBAC23B0331";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleY";
	rename -uid "8499271C-4906-DA8F-C141-728D4ECA5F0F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleZ";
	rename -uid "47012323-4287-233E-27C5-8793DFB7199E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateX";
	rename -uid "136251C5-49D4-ED5F-0D4A-FCAD8EBA2262";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.9553311732306611e-007;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateY";
	rename -uid "7F4E16B0-4988-A210-F7DE-4AAB6BBFDEFF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.4734514637621032e-008;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateZ";
	rename -uid "646EA835-4F42-D3C0-0328-BFB01BC02EB1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.4853049985249527e-007;
createNode animCurveTU -n "Character1_LeftHandThumb3_visibility";
	rename -uid "A559F56C-49F9-87FA-7D01-3097E8C0A50B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateX";
	rename -uid "AE413A92-4044-2D90-7BF5-049DADC7BE08";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.4336724281311035;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateY";
	rename -uid "659BCABB-4FE6-D05D-9FBF-BEA08073BCF5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.6582565307617187;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateZ";
	rename -uid "F27CE314-4FC1-CFFD-C015-839A4246A572";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.338188648223877;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleX";
	rename -uid "777ED580-49A4-5071-0939-FFB608C117BD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleY";
	rename -uid "87E3626A-4604-3C83-5FB3-2BB256E4BF24";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleZ";
	rename -uid "A01D76CF-479D-A4C6-4F0C-6FAA7237A565";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateX";
	rename -uid "13A3DC69-4BDC-7ECD-79D0-68946CA63891";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.6426725387573242;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateY";
	rename -uid "CDD986B1-49AD-742E-4FB5-8997F06F5B81";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.726017951965332;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateZ";
	rename -uid "E9F0D341-4620-AD92-F498-D18FEF5AFBFB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 18.884702682495117;
createNode animCurveTU -n "Character1_LeftHandIndex1_visibility";
	rename -uid "EAE99BCB-459F-1F4A-8920-FAAD5295CB51";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateX";
	rename -uid "142DF1E2-4098-D9ED-03A5-C8B252553834";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.5846562385559082;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateY";
	rename -uid "BE339147-48C2-A946-05B0-028435F567D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.7001519203186035;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateZ";
	rename -uid "87C96534-4D0D-B093-9627-488A634BE9A3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.35993427038192749;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleX";
	rename -uid "7441A016-4CD1-C0D9-33E4-7F97CE3ED62E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleY";
	rename -uid "C711AE08-4323-1986-1B3C-EEA2D4031C0B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleZ";
	rename -uid "00A3B582-40CD-2628-99B6-8089F7608D96";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateX";
	rename -uid "3A3FBB1B-4902-21FC-F91F-378B3752BB98";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -59.34446716308593;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateY";
	rename -uid "56A367DE-40B0-1E70-A1D8-538787F74F91";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -63.10359191894532;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateZ";
	rename -uid "43748914-4C5D-9923-89D0-29AA2E623EEA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -12.385616302490234;
createNode animCurveTU -n "Character1_LeftHandIndex2_visibility";
	rename -uid "5E710C7F-4737-6902-B378-8AB0F1834E10";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateX";
	rename -uid "C68637C4-475E-6D21-C3F6-3DB62B03BA62";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.2315034866333008;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateY";
	rename -uid "12CDC112-4F54-DD56-55B4-21A33130BA93";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.1625156402587891;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateZ";
	rename -uid "62785695-432B-F8F4-57BF-D98C675C977F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.733155369758606;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleX";
	rename -uid "43DA2762-4E9B-5A22-5D8F-F9B098CC6E34";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleY";
	rename -uid "355ACDB6-480E-1393-15E8-3981C437CAD3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleZ";
	rename -uid "D5A2604D-48F3-A691-A074-1787B7BCE00F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateX";
	rename -uid "44C94CF2-40CF-5339-F87E-D08D997FDB01";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.6074998749645607e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateY";
	rename -uid "4220C30B-4A04-E631-482D-9A95CB7EF48B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.3296873880317435e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateZ";
	rename -uid "721CB0AD-4851-A1D7-ED7E-579541A414CD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.7508214966710511e-007;
createNode animCurveTU -n "Character1_LeftHandIndex3_visibility";
	rename -uid "277BF7BE-4E04-5014-C7E3-B8B5FADAEF92";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandRing1_translateX";
	rename -uid "6194168A-4311-B41E-E365-45846639BCB6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.3586184978485107;
createNode animCurveTL -n "Character1_LeftHandRing1_translateY";
	rename -uid "F265F152-4161-3140-2FB1-45834955D0ED";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4706459045410156;
createNode animCurveTL -n "Character1_LeftHandRing1_translateZ";
	rename -uid "C621C46F-40D0-EF8B-9273-9A83D8251E45";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8014267683029175;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleX";
	rename -uid "5410D97F-4119-838C-8EF1-2DB0140BB517";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleY";
	rename -uid "AD9A53D6-447D-BB0D-B780-5796C660C364";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleZ";
	rename -uid "2690AD23-40E0-2C4F-99CB-5B92C7FB24B7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateX";
	rename -uid "ABF3A414-49BD-1ABC-E894-7C99DB761252";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.9235119819641113;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateY";
	rename -uid "B5561791-4CDE-D4AD-CDFA-9C8800E7C3F2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.7192277908325195;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateZ";
	rename -uid "449E7924-423E-1943-427E-8BB03D8C7EDB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.9382824897766113;
createNode animCurveTU -n "Character1_LeftHandRing1_visibility";
	rename -uid "975196E7-4E4A-7D8D-8EA4-74AB4A02093F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandRing2_translateX";
	rename -uid "36F272AA-48E1-9A5E-D211-1D8910774AD2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7360267639160156;
createNode animCurveTL -n "Character1_LeftHandRing2_translateY";
	rename -uid "7016688A-4BB9-5B83-30B9-6F86E585910B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8172166347503662;
createNode animCurveTL -n "Character1_LeftHandRing2_translateZ";
	rename -uid "FBDD6466-4727-F9D5-A2A8-CD94F90C0B41";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.81390875577926636;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleX";
	rename -uid "82E9533D-47B6-9CCB-3692-0E92F9CB7870";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleY";
	rename -uid "61B5D7A2-4F51-39B7-BB9B-8091A7F91936";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleZ";
	rename -uid "CB4FE427-45EB-53E1-1998-D6AFD5BA8023";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateX";
	rename -uid "4035528B-462D-D3C1-D679-AF99ECABDC62";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 68.480361938476563;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateY";
	rename -uid "59F8CDC5-448C-CF81-6CC6-78A4190AE04B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 39.351833343505859;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateZ";
	rename -uid "A8D4B262-41B2-4B7C-5B28-CE841E256AA2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 87.448394775390625;
createNode animCurveTU -n "Character1_LeftHandRing2_visibility";
	rename -uid "97524D29-4CE0-A460-928D-78AB58A13C87";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_LeftHandRing3_translateX";
	rename -uid "C258935E-4C8B-6876-C809-67BB775C7718";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.4660000801086426;
createNode animCurveTL -n "Character1_LeftHandRing3_translateY";
	rename -uid "82CA102B-4754-5B4E-4A5E-82A197C77EFC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9262526035308838;
createNode animCurveTL -n "Character1_LeftHandRing3_translateZ";
	rename -uid "E7281F02-44B9-C89F-C17B-248D0302BC01";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.018019437789917;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleX";
	rename -uid "5B24FEA1-4689-1CBD-4053-909E4B78709B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleY";
	rename -uid "0EF4528C-44B5-2499-C50C-AA8A2C6E95BC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleZ";
	rename -uid "7F5FB59B-45FF-AAD4-B87C-569C6AD20CA7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateX";
	rename -uid "4EB9E675-49B8-6715-5388-1C8087A71079";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.8659176122309873e-009;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateY";
	rename -uid "90CF04D2-48B4-75BF-09FB-3BB69ECEAC73";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.7733799369307235e-007;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateZ";
	rename -uid "48F09F6F-45A8-B5B2-3B98-80B55A2CC75A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.2666637871916464e-007;
createNode animCurveTU -n "Character1_LeftHandRing3_visibility";
	rename -uid "2E6EDCEC-41C0-EA3E-F105-9D9539A490D9";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightShoulder_translateX";
	rename -uid "B7365C8F-4CD9-DB57-9B00-8BB5E40DBC18";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.151631355285645;
createNode animCurveTL -n "Character1_RightShoulder_translateY";
	rename -uid "B4AB5F49-4C53-B651-510D-589759B93B82";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.1017274856567383;
createNode animCurveTL -n "Character1_RightShoulder_translateZ";
	rename -uid "973185B6-4964-0B5F-2EDA-14978A1DDB01";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.81731081008911133;
createNode animCurveTU -n "Character1_RightShoulder_scaleX";
	rename -uid "6FFEB3E3-4B5E-AEA0-44E6-F2B9FDB17119";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightShoulder_scaleY";
	rename -uid "038E8316-4045-A6BD-9209-FA8A4AC308D8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightShoulder_scaleZ";
	rename -uid "80BBF616-4091-4B35-B4A2-F1A91A6D61BE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightShoulder_rotateX";
	rename -uid "7D0E62C3-4BE1-7D1F-2531-069AB4047A8D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -5.7387170791625977 1 -5.5218958854675293
		 2 -4.9110255241394043 3 -3.962212085723877 4 -2.7298686504364014 5 -1.2695491313934326
		 6 0.35990068316459656 7 2.0948731899261475 8 3.8662204742431645 9 5.5991387367248535
		 10 7.2136750221252441 11 8.6258020401000977 12 9.7489385604858398 13 10.495827674865723
		 14 10.780729293823242 15 10.76682186126709 16 10.676370620727539 17 10.514973640441895
		 18 10.288347244262695 19 10.002315521240234 20 9.6627922058105469 21 9.2757692337036133
		 22 8.8473052978515625 23 8.383509635925293 24 7.8905220031738281 25 7.3745064735412589
		 26 6.8416271209716797 27 6.2980327606201172 28 5.7498335838317871 29 5.2233915328979492
		 30 4.7407855987548828 31 4.3016409873962402 32 3.9055726528167725 33 3.552201509475708
		 34 3.2411773204803467 35 2.9722142219543457 36 2.745112419128418 37 2.5597951412200928
		 38 2.4163408279418945 39 2.3150196075439453 40 2.2563292980194092 41 2.1869237422943115
		 42 2.0592386722564697 43 1.882916331291199 44 1.6678353548049927 45 1.4239641427993774
		 46 1.161249041557312 47 0.88953489065170288 48 0.61851364374160767 49 0.35770794749259949
		 50 0.11647246032953261 51 -0.095966830849647522 52 -0.27046611905097961 53 -0.39788404107093811
		 54 -0.46898570656776434 55 -0.47433385252952581 56 -0.40413764119148254 57 -0.38979396224021912
		 58 -0.55125582218170166 59 -0.85975724458694458 60 -1.2853600978851318 61 -1.7977715730667114
		 62 -2.367020845413208 63 -2.9639770984649658 64 -3.5607190132141113 65 -4.1307334899902344
		 66 -4.6489481925964355 67 -5.0915718078613281 68 -5.4357562065124512 69 -5.6590571403503418
		 70 -5.7387170791625977;
createNode animCurveTA -n "Character1_RightShoulder_rotateY";
	rename -uid "14D0E236-45AF-7875-AAB3-0D852171ECEC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -10.32844066619873 1 -10.486442565917969
		 2 -10.926873207092285 3 -11.597184181213379 4 -12.443273544311523 5 -13.41103458404541
		 6 -14.447784423828123 7 -15.503571510314943 8 -16.532323837280273 9 -17.49266242980957
		 10 -18.348264694213867 11 -19.067571640014648 12 -19.622735977172852 13 -19.987701416015625
		 14 -20.135454177856445 15 -20.153873443603516 16 -20.150226593017578 17 -20.123659133911133
		 18 -20.073165893554688 19 -19.997623443603516 20 -19.895809173583984 21 -19.766437530517578
		 22 -19.608180999755859 23 -19.419698715209961 24 -19.199657440185547 25 -18.946758270263672
		 26 -18.659753799438477 27 -18.337472915649414 28 -17.97882080078125 29 -17.620113372802734
		 30 -17.298685073852539 31 -17.015073776245117 32 -16.769817352294922 33 -16.563442230224609
		 34 -16.396427154541016 35 -16.269197463989258 36 -16.182098388671875 37 -16.13536262512207
		 38 -16.129098892211914 39 -16.163244247436523 40 -16.237556457519531 41 -16.276178359985352
		 42 -16.213979721069336 43 -16.066444396972656 44 -15.848902702331541 45 -15.576622009277344
		 46 -15.264878273010254 47 -14.929003715515137 48 -14.58442497253418 49 -14.246671676635742
		 50 -13.931368827819824 51 -13.654221534729004 52 -13.430972099304199 53 -13.277363777160645
		 54 -13.209071159362793 55 -13.241625785827637 56 -13.39033317565918 57 -13.531311988830566
		 58 -13.543563842773437 59 -13.445310592651367 60 -13.253864288330078 61 -12.986349105834961
		 62 -12.660247802734375 63 -12.293807029724121 64 -11.906242370605469 65 -11.517823219299316
		 66 -11.149800300598145 67 -10.824225425720215 68 -10.563679695129395 69 -10.390918731689453
		 70 -10.32844066619873;
createNode animCurveTA -n "Character1_RightShoulder_rotateZ";
	rename -uid "D82640EC-47D5-DF97-C69C-E4B752DE29A9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 14.751459121704102 1 14.434908866882326
		 2 13.546457290649414 3 12.176257133483887 4 10.413213729858398 5 8.346099853515625
		 6 6.0646810531616211 7 3.6607987880706787 8 1.2293307781219482 9 -1.131158709526062
		 10 -3.3182137012481689 11 -5.2258386611938477 12 -6.7454113960266113 13 -7.7671732902526864
		 14 -8.1821737289428711 15 -8.2053050994873047 16 -8.1262874603271484 17 -7.9504647254943857
		 18 -7.6832437515258789 19 -7.33011817932129 20 -6.8966712951660156 21 -6.3885836601257324
		 22 -5.8116350173950195 23 -5.1717042922973633 24 -4.4747676849365234 25 -3.7268922328948975
		 26 -2.9342236518859863 27 -2.102982759475708 28 -1.2394425868988037 29 -0.39268192648887634
		 30 0.38948547840118408 31 1.1025782823562622 32 1.7420133352279663 33 2.3031048774719238
		 34 2.7810561656951904 35 3.170961856842041 36 3.4677984714508057 37 3.6664183139801021
		 38 3.7615456581115723 39 3.7477653026580815 40 3.619515180587769 41 3.4988522529602051
		 42 3.4992282390594482 43 3.6016948223114014 44 3.787020206451416 45 4.0359044075012207
		 46 4.3291459083557129 47 4.6477689743041992 48 4.9731011390686035 49 5.2868070602416992
		 50 5.5708813667297363 51 5.8076043128967285 52 5.9794564247131348 53 6.0690016746520996
		 54 6.0587358474731445 55 5.9308896064758301 56 5.6672039031982422 57 5.5094709396362305
		 58 5.6790075302124023 59 6.1253886222839355 60 6.7973928451538086 61 7.6434717178344718
		 62 8.6121320724487305 63 9.6522245407104492 64 10.713129043579102 65 11.744843482971191
		 66 12.697977066040039 67 13.523661613464355 68 14.173391342163086 69 14.598815917968752
		 70 14.751459121704102;
createNode animCurveTU -n "Character1_RightShoulder_visibility";
	rename -uid "62DF3DF9-4D40-EDAF-3E64-849E5AFE081D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightArm_translateX";
	rename -uid "2EE0DA96-40CE-08BA-CFDF-2C9D9A3AD72B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.05986499786377;
createNode animCurveTL -n "Character1_RightArm_translateY";
	rename -uid "8D6F15FE-435D-06A1-62EB-CE933D4F1DD9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.8638343811035156;
createNode animCurveTL -n "Character1_RightArm_translateZ";
	rename -uid "40B17B32-4C5E-25E0-ACE6-81AB34CB6471";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4631747007369995;
createNode animCurveTU -n "Character1_RightArm_scaleX";
	rename -uid "6C018BC5-4D04-A7DF-4173-558E6BE598CD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightArm_scaleY";
	rename -uid "2104ED08-4B20-692E-8EC8-2A91CC0EE8E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightArm_scaleZ";
	rename -uid "077FF0D3-4626-D8F8-D348-5FB40956BE84";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightArm_rotateX";
	rename -uid "8DA8C0B8-4B33-F20D-5C9B-61894CA07A79";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -49.127330780029297 1 -48.087970733642578
		 2 -45.681751251220703 3 -42.357181549072266 4 -38.614086151123047 5 -34.909305572509766
		 6 -31.57769775390625 7 -28.804891586303707 8 -26.649761199951172 9 -25.089714050292969
		 10 -24.064048767089844 11 -23.505531311035156 12 -23.361169815063477 13 -23.599775314331055
		 14 -24.231870651245117 15 -25.039571762084961 16 -25.786214828491211 17 -26.499244689941406
		 18 -27.204498291015625 19 -27.924737930297852 20 -28.677993774414062 21 -29.475942611694336
		 22 -30.322433471679688 23 -31.212558746337891 24 -32.132545471191406 25 -33.060989379882812
		 26 -33.971492767333984 27 -34.836944580078125 28 -35.634868621826172 29 -36.729995727539063
		 30 -38.105754852294922 31 -39.675708770751953 32 -41.36322021484375 33 -43.099208831787109
		 34 -44.819244384765625 35 -46.460643768310547 36 -47.958305358886719 37 -49.247203826904297
		 38 -50.258190155029297 39 -50.913776397705078 40 -51.128421783447266 41 -50.908359527587891
		 42 -50.456348419189453 43 -49.78851318359375 44 -48.920269012451172 45 -47.867229461669922
		 46 -46.646247863769531 47 -45.276473999023438 48 -43.780536651611328 49 -42.185752868652344
		 50 -40.525314331054687 51 -38.839424133300781 52 -37.165863037109375 53 -35.541622161865234
		 54 -34.01708984375 55 -32.650981903076172 56 -31.511884689331055 57 -30.780860900878906
		 58 -30.580570220947266 59 -30.898643493652344 60 -31.715986251831058 61 -33.000659942626953
		 62 -34.700950622558594 63 -36.739330291748047 64 -39.009117126464844 65 -41.3758544921875
		 66 -43.684135437011719 67 -45.768753051757813 68 -47.467182159423828 69 -48.629936218261719
		 70 -49.127330780029297;
createNode animCurveTA -n "Character1_RightArm_rotateY";
	rename -uid "9D807239-405F-4BD0-091C-739E2CE3A609";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 65.350975036621094 1 65.383621215820313
		 2 65.038772583007813 3 64.280982971191406 4 63.08501052856446 5 61.466598510742188
		 6 59.491752624511719 7 57.268646240234375 8 54.932022094726563 9 52.628700256347656
		 10 50.507537841796875 11 48.713638305664062 12 47.385356903076172 13 46.661693572998047
		 14 46.675827026367188 15 47.189212799072266 16 47.858249664306641 17 48.643764495849609
		 18 49.507041931152344 19 50.409774780273438 20 51.314113616943359 21 52.182910919189453
		 22 52.980037689208984 23 53.670772552490234 24 54.222061157226563 25 54.602664947509766
		 26 54.782871246337891 27 54.733921051025391 28 54.426956176757813 29 53.938381195068359
		 30 53.197368621826172 31 52.267910003662109 32 51.214817047119141 33 50.102611541748047
		 34 48.994453430175781 35 47.951198577880859 36 47.047286987304687 37 46.347091674804688
		 38 45.890773773193359 39 45.714683532714844 40 45.852798461914063 41 46.169483184814453
		 42 46.614238739013672 43 47.166969299316406 44 47.807510375976562 45 48.515804290771484
		 46 49.272148132324219 47 50.05755615234375 48 50.854156494140625 49 51.645671844482422
		 50 52.417922973632813 51 53.159324645996094 52 53.877788543701172 53 54.588459014892578
		 54 55.294673919677734 55 56.002407073974609 56 56.720279693603516 57 57.524173736572266
		 58 58.453224182128906 59 59.460758209228516 60 60.498573303222656 61 61.519042968750007
		 62 62.477539062499993 63 63.335296630859375 64 64.062263488769531 65 64.639411926269531
		 66 65.059783935546875 67 65.327651977539062 68 65.455482482910156 69 65.459014892578125
		 70 65.350975036621094;
createNode animCurveTA -n "Character1_RightArm_rotateZ";
	rename -uid "24DC4565-41AF-D7B3-11EA-01A6BB1D0EA8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -46.935062408447266 1 -45.595863342285156
		 2 -42.874675750732422 3 -39.233345031738281 4 -35.188858032226563 5 -31.216087341308594
		 6 -27.666440963745117 7 -24.738780975341797 8 -22.500518798828125 9 -20.931919097900391
		 10 -19.968929290771484 11 -19.534749984741211 12 -19.561262130737305 13 -19.958578109741211
		 14 -20.664443969726563 15 -21.47089958190918 16 -22.183727264404297 17 -22.841964721679688
		 18 -23.482378005981445 19 -24.137483596801758 20 -24.833600997924805 21 -25.589078903198242
		 22 -26.412866592407227 23 -27.303768157958984 24 -28.250696182250977 25 -29.234422683715817
		 26 -30.230907440185543 27 -31.216409683227536 28 -32.173824310302734 29 -33.417160034179688
		 30 -34.909633636474609 31 -36.568805694580078 32 -38.322139739990234 33 -40.104881286621094
		 34 -41.857620239257813 35 -43.524101257324219 36 -45.033504486083984 37 -46.325996398925781
		 38 -47.358249664306641 39 -48.079692840576172 40 -48.431118011474609 41 -48.538619995117188
		 42 -48.635543823242187 43 -48.697681427001953 44 -48.700038909912109 45 -48.618038177490234
		 46 -48.428672790527344 47 -48.111728668212891 48 -47.6510009765625 49 -47.035499572753906
		 50 -46.260658264160156 51 -45.329429626464844 52 -44.221797943115234 53 -42.909828186035156
		 54 -41.396865844726562 55 -39.695579528808594 56 -37.82989501953125 57 -36.140312194824219
		 58 -34.961219787597656 59 -34.310359954833984 60 -34.192764282226562 61 -34.595470428466797
		 62 -35.481525421142578 63 -36.784919738769531 64 -38.408164978027344 65 -40.224544525146484
		 66 -42.085777282714844 67 -43.833904266357422 68 -45.314468383789063 69 -46.387538909912109
		 70 -46.935062408447266;
createNode animCurveTU -n "Character1_RightArm_visibility";
	rename -uid "707AAE46-4363-E3D5-E4E7-1BA11874E8D4";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightForeArm_translateX";
	rename -uid "10785D25-4532-186D-D9E7-7E9DBDB64545";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -16.894504547119141;
createNode animCurveTL -n "Character1_RightForeArm_translateY";
	rename -uid "07A27E59-4AD4-7F0D-CE0D-D78824B4AC9F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.7289671897888184;
createNode animCurveTL -n "Character1_RightForeArm_translateZ";
	rename -uid "6BD33EE8-41A9-196A-D27F-27BFE3B749B4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.2348899841308594;
createNode animCurveTU -n "Character1_RightForeArm_scaleX";
	rename -uid "1DC686EB-4F12-74DA-7EBA-1EBB53F005BD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightForeArm_scaleY";
	rename -uid "2DA22C5B-4F90-917B-6902-798B9E7A1AEB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightForeArm_scaleZ";
	rename -uid "055BB6BA-4CB4-38EA-C33E-C7A998FE963E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightForeArm_rotateX";
	rename -uid "2C305BDC-4934-4772-B5D8-B58415CBF390";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -29.593856811523434 1 -30.643978118896481
		 2 -31.935998916625973 3 -33.430225372314453 4 -35.087123870849609 5 -36.863384246826172
		 6 -38.709575653076172 7 -40.569522857666016 8 -42.381195068359375 9 -44.078792572021484
		 10 -45.595417022705078 11 -46.865447998046875 12 -47.825992584228516 13 -48.452205657958984
		 14 -48.732860565185547 15 -48.679267883300781 16 -48.353336334228516 17 -47.788784027099609
		 18 -47.018440246582031 19 -46.075180053710938 20 -44.992706298828125 21 -43.806072235107422
		 22 -42.551895141601563 23 -41.268226623535156 24 -39.994190216064453 25 -38.769424438476563
		 26 -37.633525848388672 27 -36.625495910644531 28 -35.783477783203125 29 -34.156208038330078
		 30 -32.593509674072266 31 -31.107706069946286 32 -29.712007522583008 33 -28.420085906982422
		 34 -27.245628356933594 35 -26.20195198059082 36 -25.328668594360352 37 -24.651912689208984
		 38 -24.163387298583984 39 -23.853836059570313 40 -23.713441848754883 41 -23.641050338745117
		 42 -23.573921203613281 43 -23.519598007202148 44 -23.48484992980957 45 -23.47491455078125
		 46 -23.49285888671875 47 -23.539169311523438 48 -23.611505508422852 49 -23.704700469970703
		 50 -23.810958862304688 51 -23.920286178588867 52 -24.059946060180664 53 -24.274585723876953
		 54 -24.579746246337891 55 -24.991918563842773 56 -25.528430938720703 57 -26.076839447021484
		 58 -26.522127151489258 59 -26.880208969116211 60 -27.168661117553711 61 -27.405616760253906
		 62 -27.608827590942383 63 -27.794881820678711 64 -27.978673934936523 65 -28.173030853271484
		 66 -28.388519287109375 67 -28.633388519287109 68 -28.913623809814453 69 -29.233110427856442
		 70 -29.593856811523434;
createNode animCurveTA -n "Character1_RightForeArm_rotateY";
	rename -uid "669B1FC9-4723-E241-0A21-F98719449C09";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 11.746877670288086 1 11.224470138549805
		 2 11.097238540649414 3 11.278937339782715 4 11.682782173156738 5 12.226696968078613
		 6 12.837921142578125 7 13.456841468811035 8 14.039572715759277 9 14.559001922607422
		 10 15.003922462463381 11 15.376232147216799 12 15.686212539672852 13 15.944416999816893
		 14 16.150476455688477 15 16.347742080688477 16 16.58253288269043 17 16.846698760986328
		 18 17.128423690795898 19 17.414325714111328 20 17.691244125366211 21 17.947784423828125
		 22 18.17567253112793 23 18.370777130126953 24 18.533796310424805 25 18.670698165893555
		 26 18.792600631713867 27 18.915422439575195 28 19.05908203125 29 18.9534912109375
		 30 18.887563705444336 31 18.845615386962891 32 18.814170837402344 33 18.782182693481445
		 34 18.741033554077148 35 18.684352874755859 36 18.596206665039063 37 18.467380523681641
		 38 18.301258087158203 39 18.098781585693359 40 17.858270645141602 41 17.763250350952148
		 42 17.834564208984375 43 18.056262969970703 44 18.410612106323242 45 18.877988815307617
		 46 19.437158584594727 47 20.065540313720703 48 20.739654541015625 49 21.43562126159668
		 50 22.129701614379883 51 22.798799514770508 52 23.41510009765625 53 23.95103645324707
		 54 24.385465621948242 55 24.698186874389648 56 24.870077133178711 57 24.830312728881836
		 58 24.540555953979492 59 24.030384063720703 60 23.328414916992188 61 22.462612152099609
		 62 21.460399627685547 63 20.348899841308594 64 19.155046463012695 65 17.905584335327148
		 66 16.627120971679688 67 15.346054077148437 68 14.088525772094727 69 12.880349159240723
		 70 11.746877670288086;
createNode animCurveTA -n "Character1_RightForeArm_rotateZ";
	rename -uid "F826161D-4AE2-B806-2966-54839AB2A978";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -35.848541259765625 1 -36.927825927734375
		 2 -38.776355743408203 3 -41.271839141845703 4 -44.294464111328125 5 -47.722412109375
		 6 -51.428539276123047 7 -55.278400421142578 8 -59.129577636718743 9 -62.832321166992188
		 10 -66.231086730957031 11 -69.166526794433594 12 -71.477302551269531 13 -73.054550170898437
		 14 -73.807914733886719 15 -73.809188842773438 16 -73.260871887207031 17 -72.225372314453125
		 18 -70.765380859375 19 -68.944137573242188 20 -66.825645446777344 21 -64.474800109863281
		 22 -61.957332611083991 23 -59.339702606201172 24 -56.688785552978516 25 -54.071495056152344
		 26 -51.554569244384766 27 -49.204326629638672 28 -47.0867919921875 29 -45.253658294677734
		 30 -43.322822570800781 31 -41.339916229248047 32 -39.352207183837891 33 -37.40838623046875
		 34 -35.558197021484375 35 -33.852188110351563 36 -32.395229339599609 37 -31.270761489868161
		 38 -30.497016906738285 39 -30.091552734375004 40 -30.071369171142582 41 -30.340662002563477
		 42 -30.751508712768555 43 -31.2763671875 44 -31.88836669921875 45 -32.561416625976562
		 46 -33.270221710205078 47 -33.990196228027344 48 -34.697353363037109 49 -35.368061065673828
		 50 -35.978828430175781 51 -36.506069183349609 52 -36.987625122070313 53 -37.485836029052734
		 54 -38.013031005859375 55 -38.580127716064453 56 -39.195888519287109 57 -39.736789703369141
		 58 -40.083072662353516 59 -40.249835968017578 60 -40.2550048828125 61 -40.118301391601563
		 62 -39.860328674316406 63 -39.501899719238281 64 -39.063606262207031 65 -38.565547943115234
		 66 -38.027256011962891 67 -37.467765808105469 68 -36.905754089355469 69 -36.359783172607422
		 70 -35.848541259765625;
createNode animCurveTU -n "Character1_RightForeArm_visibility";
	rename -uid "348BABBF-4A01-7889-DEE2-A69DC23F9755";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHand_translateX";
	rename -uid "73A0B6DE-476A-382E-ED38-369896B4A443";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.9317862987518311;
createNode animCurveTL -n "Character1_RightHand_translateY";
	rename -uid "D529A4C8-4DE4-C580-F294-7D831073586B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 16.156974792480469;
createNode animCurveTL -n "Character1_RightHand_translateZ";
	rename -uid "4B7354B8-47CD-9BBE-2574-B8BFC74BB693";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.4309353828430176;
createNode animCurveTU -n "Character1_RightHand_scaleX";
	rename -uid "A9F59A67-4515-D885-1603-3488D9E0971C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightHand_scaleY";
	rename -uid "DB4F0F0E-43D1-FB62-DF9E-E089C7987487";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHand_scaleZ";
	rename -uid "1E51B2C4-409A-0CE1-F60E-ACBA7206123D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightHand_rotateX";
	rename -uid "031CEDB1-46D1-D9B1-3738-1488F6400DA2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -7.3513579368591309 1 -7.8681941032409668
		 2 -8.7737159729003906 3 -9.9349098205566406 4 -11.206786155700684 5 -12.447157859802246
		 6 -13.530994415283203 7 -14.362853050231934 8 -14.88572311401367 9 -15.08521556854248
		 10 -14.988707542419434 11 -14.659344673156738 12 -14.184868812561035 13 -13.6768798828125
		 14 -13.237239837646484 15 -12.88215160369873 16 -12.574258804321289 17 -12.293990135192871
		 18 -12.01347541809082 19 -11.702827453613281 20 -11.335261344909668 21 -10.890961647033691
		 22 -10.359746932983398 23 -9.7425355911254883 24 -9.05169677734375 25 -8.3103513717651367
		 26 -7.5506911277770996 27 -6.8114137649536133 28 -6.1343593597412109 29 -5.5685577392578125
		 30 -5.1319494247436523 31 -4.8154425621032715 32 -4.6059632301330566 33 -4.4882669448852539
		 34 -4.4463295936584473 35 -4.4643697738647461 36 -4.5324687957763672 37 -4.6326479911804199
		 38 -4.739509105682373 39 -4.8299932479858398 40 -4.8826274871826172 41 -4.9172968864440918
		 42 -4.9649982452392578 43 -5.0208215713500977 44 -5.0793824195861816 45 -5.1350398063659668
		 46 -5.1821293830871582 47 -5.2151885032653809 48 -5.2291712760925293 49 -5.21966552734375
		 50 -5.1830878257751465 51 -5.1168656349182129 52 -5.0339803695678711 53 -4.9480128288269043
		 54 -4.8585872650146484 55 -4.7659697532653809 56 -4.6711382865905762 57 -4.6381988525390625
		 58 -4.7214350700378418 59 -4.8996052742004395 60 -5.1445298194885254 61 -5.4255428314208984
		 62 -5.7136044502258301 63 -5.9849667549133301 64 -6.2241897583007812 65 -6.4262018203735352
		 66 -6.5969867706298828 67 -6.7525091171264648 68 -6.9154386520385742 69 -7.109473705291748
		 70 -7.3513579368591309;
createNode animCurveTA -n "Character1_RightHand_rotateY";
	rename -uid "AB7C4F2D-4046-7111-CC2F-8996881B804C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 7.8076076507568368 1 7.924065113067627
		 2 7.6431560516357422 3 6.9747543334960938 4 5.9315400123596191 5 4.5432333946228027
		 6 2.8642656803131104 7 0.97464126348495495 8 -1.0249660015106201 9 -3.0218055248260498
		 10 -4.9007081985473633 11 -6.5522441864013672 12 -7.877211093902587 13 -8.7981700897216797
		 14 -9.2472372055053711 15 -9.2577142715454102 16 -8.9456405639648437 17 -8.3575553894042969
		 18 -7.5403041839599609 19 -6.5420370101928711 20 -5.4126887321472168 21 -4.2037615776062012
		 22 -2.9674456119537354 23 -1.7552698850631714 24 -0.61659377813339233 25 0.40272679924964905
		 26 1.261156439781189 27 1.9214986562728882 28 2.3498842716217041 29 2.5480015277862549
		 30 2.5615606307983398 31 2.4213135242462158 32 2.1551401615142822 33 1.7890863418579102
		 34 1.3483161926269531 35 0.85789811611175537 36 0.34777095913887024 37 -0.15057823061943054
		 38 -0.6085243821144104 39 -0.99627435207366954 40 -1.2833306789398193 41 -1.5121464729309082
		 42 -1.7469738721847534 43 -1.9901249408721924 44 -2.2440085411071777 45 -2.510974645614624
		 46 -2.7931993007659912 47 -3.0926251411437988 48 -3.4109437465667725 49 -3.7496182918548588
		 50 -4.1099514961242676 51 -4.4931769371032715 52 -4.9054551124572754 53 -5.3483028411865234
		 54 -5.8157811164855957 55 -6.3017516136169434 56 -6.8000321388244629 57 -6.9584293365478516
		 58 -6.5033683776855469 59 -5.5467720031738281 60 -4.1999988555908203 61 -2.5731747150421143
		 62 -0.77459490299224854 63 1.0898888111114502 64 2.9174926280975342 65 4.6090950965881348
		 66 6.0695676803588867 67 7.2076449394226074 68 7.9350309371948233 69 8.1645851135253906
		 70 7.8076076507568368;
createNode animCurveTA -n "Character1_RightHand_rotateZ";
	rename -uid "03BFF3F1-4AA6-94C5-5395-1D8295577C25";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -28.309108734130859 1 -29.390817642211911
		 2 -31.240541458129883 3 -33.711944580078125 4 -36.655200958251953 5 -39.920581817626953
		 6 -43.362400054931641 7 -46.841381072998047 8 -50.224170684814453 9 -53.379779815673828
		 10 -56.174549102783203 11 -58.467670440673828 12 -60.109439849853523 13 -61.061851501464851
		 14 -61.326259613037109 15 -60.942161560058587 16 -60.057262420654304 17 -58.741783142089844
		 18 -57.067237854003906 19 -55.104518890380859 20 -52.922687530517578 21 -50.5885009765625
		 22 -48.166648864746094 23 -45.720489501953125 24 -43.3131103515625 25 -41.008220672607422
		 26 -38.870841979980469 27 -36.967487335205078 28 -35.365833282470703 29 -34.090984344482422
		 30 -33.104362487792969 31 -32.377590179443359 32 -31.881160736083984 33 -31.58477783203125
		 34 -31.457693099975586 35 -31.469051361083988 36 -31.615921020507813 37 -31.873136520385739
		 38 -32.176944732666016 39 -32.464073181152344 40 -32.671649932861328 41 -32.789264678955078
		 42 -32.850051879882813 43 -32.856800079345703 44 -32.812397003173828 45 -32.719772338867188
		 46 -32.581912994384766 47 -32.401817321777344 48 -32.182460784912109 49 -31.926797866821293
		 50 -31.637712478637695 51 -31.317998886108402 52 -31.029483795166019 53 -30.848636627197262
		 54 -30.800039291381836 55 -30.908073425292969 56 -31.196882247924801 57 -31.490928649902344
		 58 -31.610481262207031 59 -31.571191787719727 60 -31.388582229614258 61 -31.080337524414063
		 62 -30.668083190917969 63 -30.178842544555661 64 -29.646278381347656 65 -29.111616134643551
		 66 -28.623811721801758 67 -28.238803863525391 68 -28.017337799072266 69 -28.021341323852539
		 70 -28.309108734130859;
createNode animCurveTU -n "Character1_RightHand_visibility";
	rename -uid "27E00F91-4830-44D1-382A-C1B0FC964B31";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandThumb1_translateX";
	rename -uid "D6FE5BA1-490C-ED60-74DD-6BB292BDBCE1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2155418395996094;
createNode animCurveTL -n "Character1_RightHandThumb1_translateY";
	rename -uid "D37F657E-4714-4539-2CAF-B796B356A451";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8998895883560181;
createNode animCurveTL -n "Character1_RightHandThumb1_translateZ";
	rename -uid "E2AA5E13-496D-763C-BE9A-B7964282E481";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3098368644714355;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleX";
	rename -uid "3A3493DB-4F3C-C24E-BC3C-29B75A47F172";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleY";
	rename -uid "5494BDBB-429F-B94C-3180-F09D0715999F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleZ";
	rename -uid "910C6A9D-441F-7DDB-EC3B-08B147D0C16A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateX";
	rename -uid "140CEE8B-467C-07AE-CCE3-D79DAE4839CD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.8673629760742187;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateY";
	rename -uid "882BE012-49A8-F0F6-362F-F59626370848";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 15.858498573303224;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateZ";
	rename -uid "468125CC-4B3F-5168-0C0E-C5B74B3999BD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 73.536689758300781;
createNode animCurveTU -n "Character1_RightHandThumb1_visibility";
	rename -uid "D3B22DD6-4155-14E2-6C26-3BB163B6187D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandThumb2_translateX";
	rename -uid "DC983C31-4996-8C1F-1397-518ABBAC09BE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9627087116241455;
createNode animCurveTL -n "Character1_RightHandThumb2_translateY";
	rename -uid "AE7B1EFD-402F-74AE-A714-B28A9F2E0BC3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5399179458618164;
createNode animCurveTL -n "Character1_RightHandThumb2_translateZ";
	rename -uid "9FE707F1-47D1-D919-58D7-0B828256FE24";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6609530448913574;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleX";
	rename -uid "EAB6CA77-4E58-3796-8614-5D80F7D9B509";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleY";
	rename -uid "C5AA6583-45C0-C293-F229-4BBAD26282DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleZ";
	rename -uid "EFDFBB42-4CD4-2580-CE73-2C9FBA56D34D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateX";
	rename -uid "B6419418-4728-5CFC-1801-E5BD349C45F5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.4960494041442871;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateY";
	rename -uid "0709BF77-4D3E-937A-903C-4B90391ACA2B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.6941189765930176;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateZ";
	rename -uid "68EBB58D-4DAE-6657-AD12-DE804EB03C1E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.59695267677307129;
createNode animCurveTU -n "Character1_RightHandThumb2_visibility";
	rename -uid "F6904B67-4A1E-8653-F5EA-F5B85A50DEB5";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandThumb3_translateX";
	rename -uid "B203889C-4895-09F9-B72D-688095BACB8C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.5867142677307129;
createNode animCurveTL -n "Character1_RightHandThumb3_translateY";
	rename -uid "931DF1A6-41BD-163F-B73F-C4B1F7577775";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.6152248382568359;
createNode animCurveTL -n "Character1_RightHandThumb3_translateZ";
	rename -uid "A1E6CC2A-4092-6812-A7E9-6792E39F3484";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.4791715145111084;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleX";
	rename -uid "93562DB8-4882-F5EB-B94E-499EFE629482";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleY";
	rename -uid "392DEC8C-4C70-50D9-342E-309B9669EC90";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleZ";
	rename -uid "9E4831B3-4E0D-CE91-DF76-388A0CD40D13";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateX";
	rename -uid "4B489971-4753-7796-D54D-728EBB2CCA41";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.261275173192189e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateY";
	rename -uid "C6605C72-49EA-F82A-1F3F-EBB58EB28CBE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.9508761296456214e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateZ";
	rename -uid "BB7F884A-45B3-85B7-7AB0-AA9CCE775C42";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.1564645180660591e-007;
createNode animCurveTU -n "Character1_RightHandThumb3_visibility";
	rename -uid "65F88D23-449C-7878-24F9-F6A3659DFA81";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandIndex1_translateX";
	rename -uid "AB1CD07B-4A38-0E0A-BF1B-EF8E0180B610";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.615594863891602;
createNode animCurveTL -n "Character1_RightHandIndex1_translateY";
	rename -uid "9D302B39-47DA-DC8D-07CA-6290B68C880E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4688365459442139;
createNode animCurveTL -n "Character1_RightHandIndex1_translateZ";
	rename -uid "10703F34-4C17-0900-FC48-40AD3F282F5C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.3266587257385254;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleX";
	rename -uid "2C4DEB2F-4107-EDE1-CC65-C2A27AB7104E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleY";
	rename -uid "3CED503A-4D80-545C-DA3B-818FB4306BB0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleZ";
	rename -uid "69C9E9A7-4282-ECD9-6FBC-EB84E6EF99EF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateX";
	rename -uid "F759AA77-42DB-DB8D-05A2-F888D95ED6C9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.895548820495605;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateY";
	rename -uid "5726DDFA-46E9-3D53-B94A-A489E91BD2FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -25.372457504272461;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateZ";
	rename -uid "8C357E25-4338-59C5-91BD-5BBC6E0CCFC5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.0900802612304687;
createNode animCurveTU -n "Character1_RightHandIndex1_visibility";
	rename -uid "F3A9AA31-4CCF-17C9-6FB3-00BD2F1381C0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandIndex2_translateX";
	rename -uid "BE4A0A0F-4A53-04BD-DB56-31B34BE93A24";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.10917675495147705;
createNode animCurveTL -n "Character1_RightHandIndex2_translateY";
	rename -uid "1AA0C051-4CAF-3881-FB62-DC9970DAFAF6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.54082572460174561;
createNode animCurveTL -n "Character1_RightHandIndex2_translateZ";
	rename -uid "4224FF9E-4D6F-7A9E-F81C-15AB4BBEC4B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.6861534118652344;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleX";
	rename -uid "DC8AA6EA-4677-4045-200C-018B0BF5F8C8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleY";
	rename -uid "8530627F-4412-30D9-4956-0A9685D886CE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleZ";
	rename -uid "0EF3D5DF-4434-833F-0E0D-2FB250305C43";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateX";
	rename -uid "624DFAFF-4EE3-B3CB-E3AB-A4ADCE97C59B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 71.57855224609375;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateY";
	rename -uid "63701285-426D-262A-A0D1-8CBEB382D085";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 36.584194183349609;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateZ";
	rename -uid "7D087E5E-4C4D-88A4-B49D-278F3AAF8C03";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 60.075927734375007;
createNode animCurveTU -n "Character1_RightHandIndex2_visibility";
	rename -uid "092054EA-4AF2-BEC9-C81E-B58EDBBF189C";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandIndex3_translateX";
	rename -uid "8D97995E-49E1-F1BF-9E52-319EEEE143C6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.8903987407684326;
createNode animCurveTL -n "Character1_RightHandIndex3_translateY";
	rename -uid "E64FE791-4ECE-31F6-A9CF-C589AE9CBBF2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3401000499725342;
createNode animCurveTL -n "Character1_RightHandIndex3_translateZ";
	rename -uid "B4554E99-423F-6744-835C-2E84AED10095";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.8930935859680176;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleX";
	rename -uid "6DD10D57-4CEF-F487-B41C-40B89240AFA6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleY";
	rename -uid "3A6060CF-4C41-247A-0CB9-6E9B20B0D2D2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleZ";
	rename -uid "8BD16372-4D2A-AAA2-4D4E-569606230875";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateX";
	rename -uid "9B374AF6-4881-1882-26DD-4EABB3E32C36";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.8837520100496477e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateY";
	rename -uid "80E65F04-452F-FDD5-7594-1EA6DBF058DE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.7038109351451567e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateZ";
	rename -uid "609B979A-4526-2A7F-BAB6-7DAFAE5F2694";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.5036792433420487e-007;
createNode animCurveTU -n "Character1_RightHandIndex3_visibility";
	rename -uid "584E57DE-4BC1-492A-3BD2-A3BBD67D3E09";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandRing1_translateX";
	rename -uid "1381B60B-4770-31EC-0EC9-67B8D40736E5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.6450033187866211;
createNode animCurveTL -n "Character1_RightHandRing1_translateY";
	rename -uid "C3499BB6-464E-503D-6AE1-F997F2ABDF2E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.0317020416259766;
createNode animCurveTL -n "Character1_RightHandRing1_translateZ";
	rename -uid "5EDB1970-4E6C-75CF-32E6-778633D8F975";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.2285447120666504;
createNode animCurveTU -n "Character1_RightHandRing1_scaleX";
	rename -uid "38C17451-4070-C71B-095D-6A91138C6CB8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandRing1_scaleY";
	rename -uid "CC849EB3-4876-D792-EAF1-33B1DCA5EED6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandRing1_scaleZ";
	rename -uid "38F4D6B3-4A65-2A92-68FC-B58D6AA6CF73";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightHandRing1_rotateX";
	rename -uid "FEB188C4-4BEF-FFEE-CA1F-2E9A18D1AFA6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.009989738464355;
createNode animCurveTA -n "Character1_RightHandRing1_rotateY";
	rename -uid "129865D7-4289-5EF1-204F-EC8E5E3A447F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -14.141476631164551;
createNode animCurveTA -n "Character1_RightHandRing1_rotateZ";
	rename -uid "191DAE55-4B0C-8B9E-3E30-74B1CC5FAA69";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.602083683013916;
createNode animCurveTU -n "Character1_RightHandRing1_visibility";
	rename -uid "775035CA-4EA1-00BF-A770-DFBD7044F5F3";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandRing2_translateX";
	rename -uid "A410F3F7-419C-6943-7F41-62AE0BF5B6AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.046228401362895966;
createNode animCurveTL -n "Character1_RightHandRing2_translateY";
	rename -uid "894FD79D-40E4-32E6-A45E-F4BDFC719D1A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.89510548114776611;
createNode animCurveTL -n "Character1_RightHandRing2_translateZ";
	rename -uid "BBFA97B4-466A-5BF5-DE77-26AEE04FA4B6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.4977946281433105;
createNode animCurveTU -n "Character1_RightHandRing2_scaleX";
	rename -uid "510C9701-42ED-E063-C273-FC8A2A948FC7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandRing2_scaleY";
	rename -uid "6CC09DDD-4DA4-6621-689F-D29B0ECE9950";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandRing2_scaleZ";
	rename -uid "53D1B2F4-4219-1B9C-BC37-25A8442C4263";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightHandRing2_rotateX";
	rename -uid "54DC300B-4D00-0DE2-52BA-5A8F017F96C5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 107.08145141601562;
createNode animCurveTA -n "Character1_RightHandRing2_rotateY";
	rename -uid "3391578E-4E86-E0BF-1DFE-CAB9F97175D1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 24.604604721069336;
createNode animCurveTA -n "Character1_RightHandRing2_rotateZ";
	rename -uid "03290CE4-4E1E-2A70-2F39-35BEA9120894";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 91.610084533691406;
createNode animCurveTU -n "Character1_RightHandRing2_visibility";
	rename -uid "C2E5A98E-4F4B-03A1-12E1-80B550F08D1E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_RightHandRing3_translateX";
	rename -uid "904D06EA-4EC5-8838-8AA1-B4937E37AA43";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.8638609647750854;
createNode animCurveTL -n "Character1_RightHandRing3_translateY";
	rename -uid "9F7FEA30-4333-3ABF-4D7A-B4AA83B4A9B7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0986917018890381;
createNode animCurveTL -n "Character1_RightHandRing3_translateZ";
	rename -uid "D11140B7-469B-EFAD-59E8-AF914D906F87";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.2819910049438477;
createNode animCurveTU -n "Character1_RightHandRing3_scaleX";
	rename -uid "DA447624-4923-3650-4061-BB8411B7CFC5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing3_scaleY";
	rename -uid "BBF40D51-4A20-4C04-FC7D-3FA151498BFE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing3_scaleZ";
	rename -uid "C565DAC6-49B9-4589-99C9-B397FD5AF28B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_RightHandRing3_rotateX";
	rename -uid "49EBFA93-4518-4386-8EA5-918F15B0862C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.313534477911162e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateY";
	rename -uid "A99038C5-4198-9B40-96E6-1A8EC4CC9D07";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.8691288516947679e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateZ";
	rename -uid "B961C3A4-4973-B29D-A696-818C96B346AF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.0538312141034112e-008;
createNode animCurveTU -n "Character1_RightHandRing3_visibility";
	rename -uid "A32668BB-45FD-FCE3-2C14-4EA3B2355986";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_Neck_translateX";
	rename -uid "C7F5D2DB-4A85-A920-F3A8-00966DCAFE31";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.1971182823181152;
createNode animCurveTL -n "Character1_Neck_translateY";
	rename -uid "2738AD32-432F-DB53-8974-93B96A159480";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.621223449707031;
createNode animCurveTL -n "Character1_Neck_translateZ";
	rename -uid "7422954A-455B-4233-A125-C5BE55475EAA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.1444897651672363;
createNode animCurveTU -n "Character1_Neck_scaleX";
	rename -uid "5BC724F0-45F1-C51A-53CC-E0B881FA76BB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_Neck_scaleY";
	rename -uid "3FA15F16-42AB-DB09-CA98-A1A001692BDF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Neck_scaleZ";
	rename -uid "E6F0E332-4B58-53E1-57CB-E6A113A5BB32";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_Neck_rotateX";
	rename -uid "461F4F63-4B17-5E5B-252D-DAAEE6EFA2FC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -1.8227808475494385 1 -1.9275969266891482
		 2 -2.1070070266723633 3 -2.3533234596252441 4 -2.6588366031646729 5 -3.0158319473266602
		 6 -3.4165987968444824 7 -3.8534386157989506 8 -4.3186712265014648 9 -4.8046360015869141
		 10 -5.3036904335021973 11 -5.8082079887390137 12 -6.3105697631835937 13 -6.8031558990478516
		 14 -7.2783389091491708 15 -7.72129249572754 16 -8.1237821578979492 17 -8.4881267547607422
		 18 -8.8167333602905273 19 -9.1120901107788086 20 -9.3767528533935547 21 -9.6133298873901367
		 22 -9.8244810104370117 23 -10.012895584106445 24 -10.18129825592041 25 -10.33243465423584
		 26 -10.469069480895996 27 -10.59398078918457 28 -10.709958076477051 29 -10.781085014343262
		 30 -10.777975082397461 31 -10.713265419006348 32 -10.599641799926758 33 -10.449832916259766
		 34 -10.276601791381836 35 -10.092726707458496 36 -9.911005973815918 37 -9.7442340850830078
		 38 -9.6051969528198242 39 -9.5066556930541992 40 -9.461334228515625 41 -9.4067878723144531
		 42 -9.2788534164428711 43 -9.0875768661499023 44 -8.8429412841796875 45 -8.5548973083496094
		 46 -8.2333850860595703 47 -7.8883605003356934 48 -7.5298247337341309 49 -7.1678404808044434
		 50 -6.8125629425048828 51 -6.4742498397827148 52 -6.1632742881774902 53 -5.8901257514953613
		 54 -5.6653957366943359 55 -5.4997601509094238 56 -5.4039368629455566 57 -5.2987070083618164
		 58 -5.1082186698913574 59 -4.8480887413024902 60 -4.5337834358215332 61 -4.1806893348693848
		 62 -3.8041794300079346 63 -3.4196696281433105 64 -3.0426621437072754 65 -2.6887798309326172
		 66 -2.3737871646881104 67 -2.1135964393615723 68 -1.9242537021636963 69 -1.8219116926193237
		 70 -1.8227808475494385;
createNode animCurveTA -n "Character1_Neck_rotateY";
	rename -uid "A4A485DF-4A53-7054-8D45-48824DD7B75A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -5.5108656883239746 1 -5.4145379066467285
		 2 -5.363919734954834 3 -5.354032039642334 4 -5.379908561706543 5 -5.4365682601928711
		 6 -5.5189905166625977 7 -5.6220989227294922 8 -5.7407431602478027 9 -5.86968994140625
		 10 -6.0036168098449707 11 -6.1371092796325684 12 -6.2646622657775879 13 -6.3806862831115723
		 14 -6.4795150756835937 15 -6.5489192008972168 16 -6.5827898979187012 17 -6.5845613479614258
		 18 -6.5576214790344238 19 -6.5053243637084961 20 -6.4309968948364258 21 -6.3379406929016113
		 22 -6.2294445037841797 23 -6.1087794303894043 24 -5.9792141914367676 25 -5.8440108299255371
		 26 -5.7064318656921387 27 -5.5697402954101562 28 -5.437201976776123 29 -5.2906436920166016
		 30 -5.1148171424865723 31 -4.9174008369445801 32 -4.7060914039611816 33 -4.4885859489440918
		 34 -4.2725744247436523 35 -4.0657258033752441 36 -3.8756928443908691 37 -3.7101070880889888
		 38 -3.5765905380249023 39 -3.4827666282653809 40 -3.4362778663635254 41 -3.4678609371185303
		 42 -3.5948781967163086 43 -3.8049273490905757 44 -4.0856060981750488 45 -4.4245367050170898
		 46 -4.8093791007995605 47 -5.2278547286987305 48 -5.6677584648132324 49 -6.1169619560241699
		 50 -6.5634298324584961 51 -6.9952092170715332 52 -7.4004325866699228 53 -7.7672996520996094
		 54 -8.0840559005737305 55 -8.3389644622802734 56 -8.5202627182006836 57 -8.6035919189453125
		 58 -8.5823488235473633 59 -8.4713859558105469 60 -8.2855386734008789 61 -8.0395879745483398
		 62 -7.7482304573059091 63 -7.4260525703430185 64 -7.0875258445739746 65 -6.7470006942749023
		 66 -6.4187192916870117 67 -6.1168408393859863 68 -5.8554797172546387 69 -5.6487560272216797
		 70 -5.5108656883239746;
createNode animCurveTA -n "Character1_Neck_rotateZ";
	rename -uid "E966AD69-4BCA-ED0B-D6FD-B3B8B1F613B8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -0.26016071438789368 1 -0.23349943757057187
		 2 -0.17713466286659241 3 -0.090341053903102875 4 0.027669172734022141 5 0.17770282924175262
		 6 0.36054858565330505 7 0.57694327831268311 8 0.82754296064376831 9 1.1128991842269897
		 10 1.4334384202957153 11 1.7894469499588013 12 2.1810612678527832 13 2.6082615852355957
		 14 3.0708730220794678 15 3.5188100337982178 16 3.9052569866180415 17 4.2349028587341309
		 18 4.5125322341918945 19 4.7430095672607422 20 4.9312620162963867 21 5.0822744369506836
		 22 5.2010722160339355 23 5.2927150726318359 24 5.3622832298278809 25 5.4148764610290527
		 26 5.4556012153625488 27 5.4895710945129395 28 5.5219020843505859 29 5.5321240425109863
		 30 5.4997782707214355 31 5.4300799369812012 32 5.3282914161682129 33 5.1996631622314453
		 34 5.0493793487548828 35 4.8825340270996094 36 4.7041130065917969 37 4.5189914703369141
		 38 4.3319554328918457 39 4.1477293968200684 40 3.9710226058959965 41 3.7786824703216557
		 42 3.548750638961792 43 3.2875924110412598 44 3.0014855861663818 45 2.6967535018920898
		 46 2.379875659942627 47 2.0575766563415527 48 1.7368848323822021 49 1.4251806735992432
		 50 1.1302070617675781 51 0.86006975173950195 52 0.62320524454116821 53 0.42833027243614197
		 54 0.28436100482940674 55 0.20030997693538666 56 0.18515565991401672 57 0.19532163441181183
		 58 0.18400910496711731 59 0.15529726445674896 60 0.11325157433748245 61 0.061840035021305084
		 62 0.0048673930577933788 63 -0.05406496673822403 64 -0.11157554388046265 65 -0.1645025908946991
		 66 -0.20987755060195923 67 -0.24488240480422974 68 -0.26678028702735901 69 -0.27282756567001343
		 70 -0.26016071438789368;
createNode animCurveTU -n "Character1_Neck_visibility";
	rename -uid "82DED24C-4C45-C0E3-EBFE-169ABA02CB79";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "Character1_Head_translateX";
	rename -uid "FED1A820-47EA-B85C-F611-E1BD74729730";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -16.985572814941406;
createNode animCurveTL -n "Character1_Head_translateY";
	rename -uid "7A94ACB0-47B2-F310-91E7-B59EB6EE584E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.0131206512451172;
createNode animCurveTL -n "Character1_Head_translateZ";
	rename -uid "D5573065-4A28-C2AA-3224-45B58089E75D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.1724905967712402;
createNode animCurveTU -n "Character1_Head_scaleX";
	rename -uid "63EE2829-492A-9B72-7CBB-1FA39F1900D4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Head_scaleY";
	rename -uid "44CFF36A-47CB-DE00-6E4D-5F91F83B25CC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_Head_scaleZ";
	rename -uid "F2244E7B-4C20-A318-EA7F-74B98522A3B3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_Head_rotateX";
	rename -uid "85701F72-466F-F0BF-0CE3-75AE2EB2FF94";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 -5.7357258796691895 1 -5.5430631637573242
		 2 -5.3488788604736328 3 -5.1535482406616211 4 -4.9574441909790039 5 -4.7609434127807617
		 6 -4.564422607421875 7 -4.3682608604431152 8 -4.1728425025939941 9 -3.9785549640655518
		 10 -3.7857933044433589 11 -3.5949575901031499 12 -3.4064552783966064 13 -3.2207012176513672
		 14 -3.038118839263916 15 -2.8565933704376221 16 -2.6742031574249268 17 -2.4916660785675049
		 18 -2.3097026348114014 19 -2.1290378570556641 20 -1.9503984451293948 21 -1.7745159864425659
		 22 -1.6021242141723633 23 -1.4339570999145508 24 -1.2707529067993164 25 -1.1132494211196899
		 26 -0.9621848464012146 27 -0.81829673051834106 28 -0.68232160806655884 29 -0.53618025779724121
		 30 -0.36788460612297058 31 -0.18729804456233978 32 -0.0042577614076435566 33 0.17143093049526215
		 34 0.32999500632286072 35 0.46169030666351318 36 0.55679386854171753 37 0.60559701919555664
		 38 0.59838801622390747 39 0.52544152736663818 40 0.37700438499450684 41 0.15079483389854431
		 42 -0.142648845911026 43 -0.49388951063156128 44 -0.89349311590194702 45 -1.3320153951644897
		 46 -1.7999941110610962 47 -2.2879419326782227 48 -2.7863399982452393 49 -3.2856349945068359
		 50 -3.7762360572814941 51 -4.2485179901123047 52 -4.6928238868713379 53 -5.0994749069213867
		 54 -5.4587817192077637 55 -5.7610569000244141 56 -5.9966373443603516 57 -6.1697278022766113
		 58 -6.2858295440673828 59 -6.3518204689025879 60 -6.3745975494384766 61 -6.3610639572143555
		 62 -6.3181138038635254 63 -6.2526249885559082 64 -6.1714539527893066 65 -6.0814299583435059
		 66 -5.9893546104431152 67 -5.9020037651062012 68 -5.8261303901672363 69 -5.768465518951416
		 70 -5.7357258796691895;
createNode animCurveTA -n "Character1_Head_rotateY";
	rename -uid "388A35CA-4650-7711-459C-CF96FE1F438D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 4.3681230545043945 1 4.6204619407653809
		 2 4.8815746307373047 3 5.1490654945373535 4 5.4205350875854492 5 5.6935830116271973
		 6 5.9658069610595703 7 6.2348051071166992 8 6.4981746673583984 9 6.7535123825073242
		 10 6.9984188079833984 11 7.2304940223693839 12 7.4473390579223633 13 7.6465606689453125
		 14 7.8257665634155273 15 7.9903068542480469 16 8.1465797424316406 17 8.2937726974487305
		 18 8.4310703277587891 19 8.5576639175415039 20 8.6727437973022461 21 8.7755031585693359
		 22 8.8651399612426758 23 8.9408483505249023 24 9.0018301010131836 25 9.0472869873046875
		 26 9.0764198303222656 27 9.0884313583374023 28 9.0825271606445313 29 9.0398902893066406
		 30 8.9485435485839844 31 8.8179502487182617 32 8.6575851440429687 33 8.476923942565918
		 34 8.2854452133178711 35 8.0926275253295898 36 7.9079418182373047 37 7.7408537864685059
		 38 7.600821018218995 39 7.49729347229004 40 7.4397096633911124 41 7.4267115592956552
		 42 7.4469995498657227 43 7.4950838088989258 44 7.5654668807983407 45 7.6526455879211426
		 46 7.7511181831359863 47 7.8553915023803702 48 7.9599928855895996 49 8.0594778060913086
		 50 8.1484394073486328 51 8.221522331237793 52 8.2734184265136719 53 8.2988748550415039
		 54 8.2926845550537109 55 8.2496757507324219 56 8.1646957397460937 57 8.0375795364379883
		 58 7.8677949905395508 59 7.6605119705200195 60 7.4208664894104004 61 7.1539745330810547
		 62 6.8649349212646484 63 6.5588431358337402 64 6.2408003807067871 65 5.9159102439880371
		 66 5.589287281036377 67 5.2660574913024902 68 4.9513568878173828 69 4.6503286361694336
		 70 4.3681230545043945;
createNode animCurveTA -n "Character1_Head_rotateZ";
	rename -uid "1BB1EBD4-4FC9-14D3-A80C-FC99632C4086";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 1.4967440366744995 1 1.8416440486907957
		 2 2.1950662136077881 3 2.5552301406860352 4 2.9203095436096191 5 3.28843092918396
		 6 3.6576731204986568 7 4.026066780090332 8 4.391594409942627 9 4.7521915435791016
		 10 5.105748176574707 11 5.450110912322998 12 5.7830867767333984 13 6.1024484634399414
		 14 6.4059319496154785 15 6.7033562660217285 16 7.0038533210754395 17 7.3041019439697257
		 18 7.6007676124572745 19 7.8905029296875 20 8.1699533462524414 21 8.4357538223266602
		 22 8.6845331192016602 23 8.9129180908203125 24 9.1175317764282227 25 9.2950010299682617
		 26 9.4419546127319336 27 9.5550298690795898 28 9.6308727264404297 29 9.6596403121948242
		 30 9.6371135711669922 31 9.5684709548950195 32 9.4590435028076172 33 9.3143701553344727
		 34 9.1401968002319336 35 8.942479133605957 36 8.727325439453125 37 8.5009441375732422
		 38 8.2695388793945313 39 8.0392007827758789 40 7.8157496452331543 41 7.5884823799133301
		 42 7.344144344329834 43 7.0843682289123535 44 6.8107900619506836 45 6.5251526832580566
		 46 6.2293844223022461 47 5.9256610870361328 48 5.6164536476135254 49 5.3045468330383301
		 50 4.993049144744873 51 4.6853790283203125 52 4.385230541229248 53 4.0965204238891602
		 54 3.823319673538208 55 3.5697629451751709 56 3.3399393558502197 57 3.1333606243133545
		 58 2.9490001201629639 59 2.7843465805053711 60 2.6367092132568359 61 2.5032954216003418
		 62 2.3812768459320068 63 2.2678442001342773 64 2.1602499485015869 65 2.0558397769927979
		 66 1.9520727396011353 67 1.846529483795166 68 1.7369097471237183 69 1.6210192441940308
		 70 1.4967440366744995;
createNode animCurveTU -n "Character1_Head_visibility";
	rename -uid "068D1A6B-4366-D376-4FD9-10A007D6506E";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "jaw_translateX";
	rename -uid "7FB6078A-4812-C0CC-3F12-CD907C7F89AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 14.004427909851074;
createNode animCurveTL -n "jaw_translateY";
	rename -uid "5E5E73B1-4873-9676-40F8-EAAC4F211B43";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.9922315685958317e-015;
createNode animCurveTL -n "jaw_translateZ";
	rename -uid "35C0916F-4DE5-2BE1-104F-4CB35591EE05";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9713712501773515e-015;
createNode animCurveTU -n "jaw_scaleX";
	rename -uid "7F291E3D-402B-D543-0DB0-C5A9D71960B2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleY";
	rename -uid "AD22B823-4660-C398-95AB-979C0BA0AFED";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleZ";
	rename -uid "F74C3DDF-4BC4-EF0D-102A-95BE1DFF0456";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "jaw_rotateX";
	rename -uid "810EC85E-4145-F694-6BAB-A5A1CFD83831";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 60 ".ktv[0:59]"  11 -6.4339084625244141 12 -6.4339084625244141
		 13 -6.1904711723327637 14 -5.608396053314209 15 -4.9100370407104492 16 -4.3177480697631836
		 17 -4.0538835525512695 18 -4.1770343780517578 19 -4.5286602973937988 20 -5.0054168701171875
		 21 -5.503960132598877 22 -5.9209456443786621 23 -6.1530294418334961 24 -6.2393612861633301
		 25 -6.2911367416381836 26 -6.3130779266357422 27 -6.3099069595336914 28 -6.2863469123840332
		 29 -6.2471194267272949 30 -6.1969480514526367 31 -6.1405544281005859 32 -6.0826611518859863
		 33 -6.0279908180236816 34 -5.9812664985656738 35 -5.947209358215332 36 -5.9232559204101562
		 37 -5.9026217460632324 38 -5.8837008476257324 39 -5.8648867607116699 40 -5.8445734977722168
		 41 -5.8211541175842285 42 -5.793022632598877 43 -5.7585725784301758 44 -5.7161984443664551
		 45 -5.6642923355102539 46 -5.6012492179870605 47 -5.5254626274108887 48 -5.4213089942932129
		 49 -5.2568564414978027 50 -5.0763249397277832 51 -4.9239335060119629 52 -4.8439016342163086
		 53 -4.8804492950439453 54 -5.0364899635314941 55 -5.270266056060791 56 -5.5589900016784668
		 57 -5.8798737525939941 58 -6.2101297378540039 59 -6.5269694328308105 60 -6.8076062202453613
		 61 -7.0292510986328125 62 -7.1691164970397958 63 -7.2175402641296396 64 -7.1915054321289063
		 65 -7.1081962585449219 66 -6.9847936630249023 67 -6.8384809494018555 68 -6.6864409446716309
		 69 -6.5458559989929199 70 -6.4339084625244141;
createNode animCurveTA -n "jaw_rotateY";
	rename -uid "B7B64971-4740-4F48-407D-7591FA8913AC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 59 ".ktv[0:58]"  11 -2.4746053218841553 12 -2.4746053218841553
		 13 -2.5263521671295166 14 -2.6463708877563477 15 -2.7818286418914795 16 -2.8798928260803223
		 17 -2.8877308368682861 18 -2.7808747291564941 19 -2.5963764190673828 20 -2.3736803531646729
		 21 -2.1522312164306641 22 -1.9714747667312622 23 -1.8708555698394775 24 -1.8306869268417356
		 25 -1.8042043447494505 26 -1.7902370691299438 27 -1.7876139879226685 28 -1.7951639890670776
		 29 -1.811715841293335 30 -1.8360989093780518 31 -1.8671417236328125 32 -1.9036732912063599
		 33 -1.9445226192474365 34 -1.9885187149047852 35 -2.0344903469085693 36 -2.0934514999389648
		 37 -2.1736881732940674 38 -2.2699384689331055 39 -2.3769397735595703 40 -2.4894297122955322
		 41 -2.6021466255187988 42 -2.709827184677124 43 -2.8072099685668945 44 -2.8890321254730225
		 45 -2.9500315189361572 46 -2.9849460124969482 47 -2.9885132312774658 48 -2.9872934818267822
		 49 -2.933408260345459 50 -2.8479511737823486 51 -2.7520155906677246 52 -2.6666948795318604
		 53 -2.6130826473236084 54 -2.5866782665252686 55 -2.5679364204406738 56 -2.5553851127624512
		 57 -2.5475516319274902 58 -2.5429632663726807 60 -2.5376322269439697 61 -2.5339446067810059
		 62 -2.5276122093200684 63 -2.5198225975036621 64 -2.512650728225708 65 -2.5059535503387451
		 66 -2.4995887279510498 67 -2.4934141635894775 68 -2.4872870445251465 69 -2.481065034866333
		 70 -2.4746053218841553;
createNode animCurveTA -n "jaw_rotateZ";
	rename -uid "770F7B39-4001-98B8-E6BC-93912F488741";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 58 ".ktv[0:57]"  11 -3.3138904571533203 12 -3.3138904571533203
		 13 -3.3416574001312256 14 -3.4092576503753662 15 -3.4931392669677734 16 -3.5697517395019531
		 17 -3.6155431270599365 18 -3.6288034915924072 19 -3.6267392635345459 20 -3.6141753196716309
		 21 -3.5959355831146236 22 -3.5768444538116455 23 -3.5617263317108154 24 -3.5487484931945801
		 25 -3.5333526134490967 26 -3.5162739753723145 27 -3.4982469081878662 28 -3.4800071716308594
		 29 -3.462289571762085 30 -3.4458296298980713 31 -3.4313619136810303 32 -3.4196219444274902
		 33 -3.4113447666168213 34 -3.4072651863098145 35 -3.4081184864044189 36 -3.4169809818267822
		 37 -3.4350082874298096 38 -3.4600546360015869 39 -3.4899744987487793 40 -3.5226218700408936
		 41 -3.5558507442474365 42 -3.587516307830811 43 -3.6154718399047852 44 -3.6375722885131836
		 45 -3.6516716480255127 46 -3.6556243896484375 49 -3.6548748016357426 50 -3.6459364891052246
		 51 -3.6149435043334965 52 -3.5517220497131348 53 -3.4460978507995605 54 -3.2838940620422363
		 55 -3.0701286792755127 56 -2.8234207630157471 57 -2.5623888969421387 58 -2.3056521415710449
		 59 -2.0718293190002441 60 -1.8795388936996462 61 -1.7474004030227661 62 -1.6940319538116455
		 63 -1.7355345487594604 64 -1.8624548912048338 65 -2.0550816059112549 66 -2.2937021255493164
		 67 -2.5586049556732178 68 -2.8300788402557373 69 -3.0884113311767578 70 -3.3138904571533203;
createNode animCurveTU -n "jaw_visibility";
	rename -uid "2DDA3794-4B31-9E7A-D60B-2683310E430F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 28 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[0:1]"  17 5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
createNode animCurveTL -n "eyes_translateX";
	rename -uid "089ED21B-4A41-68FB-FE81-26BDF740E9C3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.20706844329834;
createNode animCurveTL -n "eyes_translateY";
	rename -uid "5628A9B9-4F62-3E9F-79AE-969D143EEC49";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 22.342548370361328;
createNode animCurveTL -n "eyes_translateZ";
	rename -uid "6DDA7D5D-479E-D84F-27F4-E987AEF44C7B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.7847360800924434e-008;
createNode animCurveTU -n "eyes_scaleX";
	rename -uid "334E54F2-4D78-A0DC-6047-58A5E443B490";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleY";
	rename -uid "BE6E3837-4010-229B-C3FB-0C82B2F35FCC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleZ";
	rename -uid "3C0D8325-463D-35DA-88F3-47B2B00643F9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "eyes_rotateX";
	rename -uid "72597E87-44E0-1312-4EA8-9282BFA65027";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 61 ".ktv[0:60]"  0 0 1 -0.12333540618419646 2 -0.25292837619781494
		 3 -0.38707235455513 4 -0.52406066656112671 5 -0.66218668222427368 6 -0.79974383115768433
		 7 -0.93502557277679432 8 -1.0663251876831055 9 -1.1919360160827637 10 -1.3101515769958496
		 11 -1.4192653894424438 12 -1.5175703763961792 13 -1.6033602952957153 14 -1.6749285459518433
		 15 -1.7349668741226196 16 -1.7867341041564941 17 -1.829373836517334 18 -1.8620296716690061
		 19 -1.8838455677032471 20 -1.8939647674560547 21 -1.891531229019165 22 -1.8756886720657351
		 23 -1.8455804586410525 24 -1.800350546836853 25 -1.7391424179077148 26 -1.6610996723175049
		 27 -1.5402944087982178 28 -1.3620588779449463 29 -1.142427921295166 30 -0.89743602275848389
		 31 -0.64311802387237549 32 -0.39550849795341492 33 -0.17064207792282104 34 0.015446434728801249
		 35 0.14672243595123291 36 0.22358366847038269 37 0.26226365566253662 38 0.27048137784004211
		 39 0.25595599412918091 40 0.22640642523765561 41 0.18955178558826447 42 0.15311107039451599
		 43 0.12480333447456361 44 0.11234760284423828 45 0.11074742674827576 56 0.10563154518604279
		 57 0.10086635500192642 58 0.095278702676296234 59 0.088967286050319672 60 0.082030795514583588
		 61 0.074567936360836029 62 0.066677391529083252 63 0.058457862585783005 64 0.050008043646812439
		 65 0.041426632553339005 66 0.032812319695949554 67 0.024263806641101837 68 0.015879785642027855
		 69 0.0077589503489434719 70 0;
createNode animCurveTA -n "eyes_rotateY";
	rename -uid "39E09411-4161-4D8C-B0BA-248B694926D1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 71 ".ktv[0:70]"  0 0 1 0.02271217480301857 2 0.04819144681096077
		 3 0.075683154165744781 4 0.10443264245986938 5 0.13368524610996246 6 0.16268627345561981
		 7 0.1906810998916626 8 0.21691504120826721 9 0.24063342809677124 10 0.26108160614967346
		 11 0.27750492095947266 12 0.2891487181186676 13 0.2952582836151123 14 0.29507896304130554
		 15 0.28830555081367493 16 0.27542731165885925 17 0.25688135623931885 18 0.23310476541519165
		 19 0.20453464984893799 20 0.17160815000534058 21 0.13476233184337616 22 0.094434335827827454
		 23 0.051061265170574188 24 0.0050802356563508511 25 -0.043071683496236801 26 -0.092957347631454468
		 27 -0.15410339832305908 28 -0.23241160809993747 29 -0.32200765609741211 30 -0.41701716184616089
		 31 -0.51156586408615112 32 -0.59977924823760986 33 -0.67578309774398804 34 -0.73370295763015747
		 35 -0.76766461133956909 36 -0.78572982549667358 37 -0.79780417680740356 38 -0.80077815055847168
		 39 -0.79154253005981445 40 -0.76698803901672363 41 -0.72400546073913574 42 -0.65948539972305298
		 43 -0.57031857967376709 44 -0.45339584350585932 45 -0.30331334471702576 46 -0.12117212265729904
		 47 0.086379826068878174 48 0.31269454956054688 49 0.55112409591674805 50 0.79502034187316895
		 51 1.0377355813980103 52 1.2726218700408936 53 1.4930310249328613 54 1.6923151016235352
		 55 1.8638263940811155 56 2.0009167194366455 57 1.9962645769119263 58 1.9507037401199341
		 59 1.8691438436508179 60 1.7564934492111206 61 1.617661714553833 62 1.4575574398040771
		 63 1.2810900211334229 64 1.0931681394577026 65 0.89870089292526245 66 0.70259732007980347
		 67 0.50976651906967163 68 0.32511726021766663 69 0.15355880558490753 70 0;
createNode animCurveTA -n "eyes_rotateZ";
	rename -uid "F3D742CC-44FB-ABF7-A2AB-E6BC50D3D94B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 70 ".ktv[0:69]"  0 0 1 -0.079606711864471436 2 -0.16448521614074707
		 3 -0.25319775938987732 4 -0.34430655837059021 5 -0.43637388944625854 6 -0.52796196937561035
		 7 -0.61763298511505127 8 -0.70394927263259888 9 -0.78547298908233643 10 -0.86076641082763672
		 11 -0.92839175462722767 12 -0.98691129684448242 13 -1.0348873138427734 14 -1.0708818435668945
		 15 -1.0885957479476929 16 -1.0853227376937866 17 -1.0650142431259155 18 -1.0316216945648193
		 19 -0.98909670114517212 20 -0.94139075279235851 21 -0.89245539903640747 22 -0.84624224901199341
		 23 -0.80670267343521118 24 -0.77778828144073486 25 -0.76345056295394897 26 -0.76764106750488281
		 27 -0.79497212171554565 28 -0.84410828351974487 29 -0.9100790023803712 30 -0.98791390657424916
		 31 -1.0726428031921387 32 -1.1592952013015747 33 -1.2429006099700928 34 -1.3184889554977417
		 35 -1.381089448928833 36 -1.4181628227233887 37 -1.4284096956253052 39 -1.4159830808639526
		 40 -1.4170893430709839 41 -1.4389283657073975 42 -1.4933899641036987 43 -1.5923643112182617
		 44 -1.7477408647537231 45 -1.9658079147338869 46 -2.2371630668640137 47 -2.5501601696014404
		 48 -2.8931536674499512 49 -3.2544970512390137 50 -3.6225452423095703 51 -3.9856514930725093
		 52 -4.3321704864501953 53 -4.6504554748535156 54 -4.9288616180419922 55 -5.1557421684265137
		 56 -5.3194513320922852 57 -5.2091488838195801 58 -5.019066333770752 59 -4.7587766647338867
		 60 -4.4378533363342285 61 -4.0658702850341797 62 -3.6524016857147217 63 -3.2070207595825195
		 64 -2.7393014430999756 65 -2.2588169574737549 66 -1.7751414775848389 67 -1.2978482246398926
		 68 -0.83651119470596313 69 -0.4007037878036499 70 0;
createNode animCurveTU -n "eyes_visibility";
	rename -uid "97208DF2-47F4-AA62-8D90-4B8A5732C492";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "DDA1AACC-4A1D-2FE9-E69F-1088746435E6";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "9A7C61D9-48C2-4BF3-F9DF-2D8ECA98EC8B";
createNode displayLayer -n "defaultLayer";
	rename -uid "59F05AA1-4715-7BD3-08F8-348AADD33932";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "DDD8DB5F-4240-8EE8-FE2F-2D80FB0A1453";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "C400A86F-442A-FB48-B8EE-1EB4450CC692";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "CFA15C2D-4F92-3210-CA68-4EBCF9554CE1";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 0\n                -polymeshes 1\n                -subdivSurfaces 0\n                -planes 0\n                -lights 0\n                -cameras 0\n                -controlVertices 0\n                -hulls 0\n                -grid 1\n                -imagePlane 0\n                -joints 1\n"
		+ "                -ikHandles 0\n                -deformers 0\n                -dynamics 0\n                -particleInstancers 0\n                -fluids 0\n                -hairSystems 0\n                -follicles 0\n                -nCloths 0\n                -nParticles 0\n                -nRigids 0\n                -dynamicConstraints 0\n                -locators 0\n                -manipulators 1\n                -pluginShapes 0\n                -dimensions 0\n                -handles 0\n                -pivots 0\n                -textures 0\n                -strokes 0\n                -motionTrails 0\n                -clipGhosts 0\n                -greasePencils 0\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1240\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 0 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 0\n            -polymeshes 1\n            -subdivSurfaces 0\n"
		+ "            -planes 0\n            -lights 0\n            -cameras 0\n            -controlVertices 0\n            -hulls 0\n            -grid 1\n            -imagePlane 0\n            -joints 1\n            -ikHandles 0\n            -deformers 0\n            -dynamics 0\n            -particleInstancers 0\n            -fluids 0\n            -hairSystems 0\n            -follicles 0\n            -nCloths 0\n            -nParticles 0\n            -nRigids 0\n            -dynamicConstraints 0\n            -locators 0\n            -manipulators 1\n            -pluginShapes 0\n            -dimensions 0\n            -handles 0\n            -pivots 0\n            -textures 0\n            -strokes 0\n            -motionTrails 0\n            -clipGhosts 0\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1240\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 0 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n"
		+ "                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n"
		+ "                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n"
		+ "                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n"
		+ "                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 24 100 -ps 2 76 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 0\\n    -polymeshes 1\\n    -subdivSurfaces 0\\n    -planes 0\\n    -lights 0\\n    -cameras 0\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 1\\n    -ikHandles 0\\n    -deformers 0\\n    -dynamics 0\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 0\\n    -handles 0\\n    -pivots 0\\n    -textures 0\\n    -strokes 0\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1240\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 0 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 0\\n    -polymeshes 1\\n    -subdivSurfaces 0\\n    -planes 0\\n    -lights 0\\n    -cameras 0\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 1\\n    -ikHandles 0\\n    -deformers 0\\n    -dynamics 0\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 0\\n    -handles 0\\n    -pivots 0\\n    -textures 0\\n    -strokes 0\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1240\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 0 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "4476B8F9-4C7C-74A4-6431-E3AEC1716678";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 70 -ast 0 -aet 80 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 0;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
connectAttr "ref_frame.s" "Character1_Hips.is";
connectAttr "Character1_Hips_scaleX.o" "Character1_Hips.sx";
connectAttr "Character1_Hips_scaleY.o" "Character1_Hips.sy";
connectAttr "Character1_Hips_scaleZ.o" "Character1_Hips.sz";
connectAttr "Character1_Hips_translateX.o" "Character1_Hips.tx";
connectAttr "Character1_Hips_translateY.o" "Character1_Hips.ty";
connectAttr "Character1_Hips_translateZ.o" "Character1_Hips.tz";
connectAttr "Character1_Hips_rotateX.o" "Character1_Hips.rx";
connectAttr "Character1_Hips_rotateY.o" "Character1_Hips.ry";
connectAttr "Character1_Hips_rotateZ.o" "Character1_Hips.rz";
connectAttr "Character1_Hips_visibility.o" "Character1_Hips.v";
connectAttr "Character1_Hips.s" "Character1_LeftUpLeg.is";
connectAttr "Character1_LeftUpLeg_scaleX.o" "Character1_LeftUpLeg.sx";
connectAttr "Character1_LeftUpLeg_scaleY.o" "Character1_LeftUpLeg.sy";
connectAttr "Character1_LeftUpLeg_scaleZ.o" "Character1_LeftUpLeg.sz";
connectAttr "Character1_LeftUpLeg_translateX.o" "Character1_LeftUpLeg.tx";
connectAttr "Character1_LeftUpLeg_translateY.o" "Character1_LeftUpLeg.ty";
connectAttr "Character1_LeftUpLeg_translateZ.o" "Character1_LeftUpLeg.tz";
connectAttr "Character1_LeftUpLeg_rotateX.o" "Character1_LeftUpLeg.rx";
connectAttr "Character1_LeftUpLeg_rotateY.o" "Character1_LeftUpLeg.ry";
connectAttr "Character1_LeftUpLeg_rotateZ.o" "Character1_LeftUpLeg.rz";
connectAttr "Character1_LeftUpLeg_visibility.o" "Character1_LeftUpLeg.v";
connectAttr "Character1_LeftUpLeg.s" "Character1_LeftLeg.is";
connectAttr "Character1_LeftLeg_scaleX.o" "Character1_LeftLeg.sx";
connectAttr "Character1_LeftLeg_scaleY.o" "Character1_LeftLeg.sy";
connectAttr "Character1_LeftLeg_scaleZ.o" "Character1_LeftLeg.sz";
connectAttr "Character1_LeftLeg_translateX.o" "Character1_LeftLeg.tx";
connectAttr "Character1_LeftLeg_translateY.o" "Character1_LeftLeg.ty";
connectAttr "Character1_LeftLeg_translateZ.o" "Character1_LeftLeg.tz";
connectAttr "Character1_LeftLeg_rotateX.o" "Character1_LeftLeg.rx";
connectAttr "Character1_LeftLeg_rotateY.o" "Character1_LeftLeg.ry";
connectAttr "Character1_LeftLeg_rotateZ.o" "Character1_LeftLeg.rz";
connectAttr "Character1_LeftLeg_visibility.o" "Character1_LeftLeg.v";
connectAttr "Character1_LeftLeg.s" "Character1_LeftFoot.is";
connectAttr "Character1_LeftFoot_scaleX.o" "Character1_LeftFoot.sx";
connectAttr "Character1_LeftFoot_scaleY.o" "Character1_LeftFoot.sy";
connectAttr "Character1_LeftFoot_scaleZ.o" "Character1_LeftFoot.sz";
connectAttr "Character1_LeftFoot_translateX.o" "Character1_LeftFoot.tx";
connectAttr "Character1_LeftFoot_translateY.o" "Character1_LeftFoot.ty";
connectAttr "Character1_LeftFoot_translateZ.o" "Character1_LeftFoot.tz";
connectAttr "Character1_LeftFoot_rotateX.o" "Character1_LeftFoot.rx";
connectAttr "Character1_LeftFoot_rotateY.o" "Character1_LeftFoot.ry";
connectAttr "Character1_LeftFoot_rotateZ.o" "Character1_LeftFoot.rz";
connectAttr "Character1_LeftFoot_visibility.o" "Character1_LeftFoot.v";
connectAttr "Character1_LeftFoot.s" "Character1_LeftToeBase.is";
connectAttr "Character1_LeftToeBase_scaleX.o" "Character1_LeftToeBase.sx";
connectAttr "Character1_LeftToeBase_scaleY.o" "Character1_LeftToeBase.sy";
connectAttr "Character1_LeftToeBase_scaleZ.o" "Character1_LeftToeBase.sz";
connectAttr "Character1_LeftToeBase_translateX.o" "Character1_LeftToeBase.tx";
connectAttr "Character1_LeftToeBase_translateY.o" "Character1_LeftToeBase.ty";
connectAttr "Character1_LeftToeBase_translateZ.o" "Character1_LeftToeBase.tz";
connectAttr "Character1_LeftToeBase_rotateX.o" "Character1_LeftToeBase.rx";
connectAttr "Character1_LeftToeBase_rotateY.o" "Character1_LeftToeBase.ry";
connectAttr "Character1_LeftToeBase_rotateZ.o" "Character1_LeftToeBase.rz";
connectAttr "Character1_LeftToeBase_visibility.o" "Character1_LeftToeBase.v";
connectAttr "Character1_LeftToeBase.s" "Character1_LeftFootMiddle1.is";
connectAttr "Character1_LeftFootMiddle1_scaleX.o" "Character1_LeftFootMiddle1.sx"
		;
connectAttr "Character1_LeftFootMiddle1_scaleY.o" "Character1_LeftFootMiddle1.sy"
		;
connectAttr "Character1_LeftFootMiddle1_scaleZ.o" "Character1_LeftFootMiddle1.sz"
		;
connectAttr "Character1_LeftFootMiddle1_translateX.o" "Character1_LeftFootMiddle1.tx"
		;
connectAttr "Character1_LeftFootMiddle1_translateY.o" "Character1_LeftFootMiddle1.ty"
		;
connectAttr "Character1_LeftFootMiddle1_translateZ.o" "Character1_LeftFootMiddle1.tz"
		;
connectAttr "Character1_LeftFootMiddle1_rotateX.o" "Character1_LeftFootMiddle1.rx"
		;
connectAttr "Character1_LeftFootMiddle1_rotateY.o" "Character1_LeftFootMiddle1.ry"
		;
connectAttr "Character1_LeftFootMiddle1_rotateZ.o" "Character1_LeftFootMiddle1.rz"
		;
connectAttr "Character1_LeftFootMiddle1_visibility.o" "Character1_LeftFootMiddle1.v"
		;
connectAttr "Character1_LeftFootMiddle1.s" "Character1_LeftFootMiddle2.is";
connectAttr "Character1_LeftFootMiddle2_translateX.o" "Character1_LeftFootMiddle2.tx"
		;
connectAttr "Character1_LeftFootMiddle2_translateY.o" "Character1_LeftFootMiddle2.ty"
		;
connectAttr "Character1_LeftFootMiddle2_translateZ.o" "Character1_LeftFootMiddle2.tz"
		;
connectAttr "Character1_LeftFootMiddle2_scaleX.o" "Character1_LeftFootMiddle2.sx"
		;
connectAttr "Character1_LeftFootMiddle2_scaleY.o" "Character1_LeftFootMiddle2.sy"
		;
connectAttr "Character1_LeftFootMiddle2_scaleZ.o" "Character1_LeftFootMiddle2.sz"
		;
connectAttr "Character1_LeftFootMiddle2_rotateX.o" "Character1_LeftFootMiddle2.rx"
		;
connectAttr "Character1_LeftFootMiddle2_rotateY.o" "Character1_LeftFootMiddle2.ry"
		;
connectAttr "Character1_LeftFootMiddle2_rotateZ.o" "Character1_LeftFootMiddle2.rz"
		;
connectAttr "Character1_LeftFootMiddle2_visibility.o" "Character1_LeftFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_RightUpLeg.is";
connectAttr "Character1_RightUpLeg_scaleX.o" "Character1_RightUpLeg.sx";
connectAttr "Character1_RightUpLeg_scaleY.o" "Character1_RightUpLeg.sy";
connectAttr "Character1_RightUpLeg_scaleZ.o" "Character1_RightUpLeg.sz";
connectAttr "Character1_RightUpLeg_translateX.o" "Character1_RightUpLeg.tx";
connectAttr "Character1_RightUpLeg_translateY.o" "Character1_RightUpLeg.ty";
connectAttr "Character1_RightUpLeg_translateZ.o" "Character1_RightUpLeg.tz";
connectAttr "Character1_RightUpLeg_rotateX.o" "Character1_RightUpLeg.rx";
connectAttr "Character1_RightUpLeg_rotateY.o" "Character1_RightUpLeg.ry";
connectAttr "Character1_RightUpLeg_rotateZ.o" "Character1_RightUpLeg.rz";
connectAttr "Character1_RightUpLeg_visibility.o" "Character1_RightUpLeg.v";
connectAttr "Character1_RightUpLeg.s" "Character1_RightLeg.is";
connectAttr "Character1_RightLeg_scaleX.o" "Character1_RightLeg.sx";
connectAttr "Character1_RightLeg_scaleY.o" "Character1_RightLeg.sy";
connectAttr "Character1_RightLeg_scaleZ.o" "Character1_RightLeg.sz";
connectAttr "Character1_RightLeg_translateX.o" "Character1_RightLeg.tx";
connectAttr "Character1_RightLeg_translateY.o" "Character1_RightLeg.ty";
connectAttr "Character1_RightLeg_translateZ.o" "Character1_RightLeg.tz";
connectAttr "Character1_RightLeg_rotateX.o" "Character1_RightLeg.rx";
connectAttr "Character1_RightLeg_rotateY.o" "Character1_RightLeg.ry";
connectAttr "Character1_RightLeg_rotateZ.o" "Character1_RightLeg.rz";
connectAttr "Character1_RightLeg_visibility.o" "Character1_RightLeg.v";
connectAttr "Character1_RightLeg.s" "Character1_RightFoot.is";
connectAttr "Character1_RightFoot_scaleX.o" "Character1_RightFoot.sx";
connectAttr "Character1_RightFoot_scaleY.o" "Character1_RightFoot.sy";
connectAttr "Character1_RightFoot_scaleZ.o" "Character1_RightFoot.sz";
connectAttr "Character1_RightFoot_translateX.o" "Character1_RightFoot.tx";
connectAttr "Character1_RightFoot_translateY.o" "Character1_RightFoot.ty";
connectAttr "Character1_RightFoot_translateZ.o" "Character1_RightFoot.tz";
connectAttr "Character1_RightFoot_rotateX.o" "Character1_RightFoot.rx";
connectAttr "Character1_RightFoot_rotateY.o" "Character1_RightFoot.ry";
connectAttr "Character1_RightFoot_rotateZ.o" "Character1_RightFoot.rz";
connectAttr "Character1_RightFoot_visibility.o" "Character1_RightFoot.v";
connectAttr "Character1_RightFoot.s" "Character1_RightToeBase.is";
connectAttr "Character1_RightToeBase_scaleX.o" "Character1_RightToeBase.sx";
connectAttr "Character1_RightToeBase_scaleY.o" "Character1_RightToeBase.sy";
connectAttr "Character1_RightToeBase_scaleZ.o" "Character1_RightToeBase.sz";
connectAttr "Character1_RightToeBase_translateX.o" "Character1_RightToeBase.tx";
connectAttr "Character1_RightToeBase_translateY.o" "Character1_RightToeBase.ty";
connectAttr "Character1_RightToeBase_translateZ.o" "Character1_RightToeBase.tz";
connectAttr "Character1_RightToeBase_rotateX.o" "Character1_RightToeBase.rx";
connectAttr "Character1_RightToeBase_rotateY.o" "Character1_RightToeBase.ry";
connectAttr "Character1_RightToeBase_rotateZ.o" "Character1_RightToeBase.rz";
connectAttr "Character1_RightToeBase_visibility.o" "Character1_RightToeBase.v";
connectAttr "Character1_RightToeBase.s" "Character1_RightFootMiddle1.is";
connectAttr "Character1_RightFootMiddle1_scaleX.o" "Character1_RightFootMiddle1.sx"
		;
connectAttr "Character1_RightFootMiddle1_scaleY.o" "Character1_RightFootMiddle1.sy"
		;
connectAttr "Character1_RightFootMiddle1_scaleZ.o" "Character1_RightFootMiddle1.sz"
		;
connectAttr "Character1_RightFootMiddle1_translateX.o" "Character1_RightFootMiddle1.tx"
		;
connectAttr "Character1_RightFootMiddle1_translateY.o" "Character1_RightFootMiddle1.ty"
		;
connectAttr "Character1_RightFootMiddle1_translateZ.o" "Character1_RightFootMiddle1.tz"
		;
connectAttr "Character1_RightFootMiddle1_rotateX.o" "Character1_RightFootMiddle1.rx"
		;
connectAttr "Character1_RightFootMiddle1_rotateY.o" "Character1_RightFootMiddle1.ry"
		;
connectAttr "Character1_RightFootMiddle1_rotateZ.o" "Character1_RightFootMiddle1.rz"
		;
connectAttr "Character1_RightFootMiddle1_visibility.o" "Character1_RightFootMiddle1.v"
		;
connectAttr "Character1_RightFootMiddle1.s" "Character1_RightFootMiddle2.is";
connectAttr "Character1_RightFootMiddle2_translateX.o" "Character1_RightFootMiddle2.tx"
		;
connectAttr "Character1_RightFootMiddle2_translateY.o" "Character1_RightFootMiddle2.ty"
		;
connectAttr "Character1_RightFootMiddle2_translateZ.o" "Character1_RightFootMiddle2.tz"
		;
connectAttr "Character1_RightFootMiddle2_scaleX.o" "Character1_RightFootMiddle2.sx"
		;
connectAttr "Character1_RightFootMiddle2_scaleY.o" "Character1_RightFootMiddle2.sy"
		;
connectAttr "Character1_RightFootMiddle2_scaleZ.o" "Character1_RightFootMiddle2.sz"
		;
connectAttr "Character1_RightFootMiddle2_rotateX.o" "Character1_RightFootMiddle2.rx"
		;
connectAttr "Character1_RightFootMiddle2_rotateY.o" "Character1_RightFootMiddle2.ry"
		;
connectAttr "Character1_RightFootMiddle2_rotateZ.o" "Character1_RightFootMiddle2.rz"
		;
connectAttr "Character1_RightFootMiddle2_visibility.o" "Character1_RightFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_Spine.is";
connectAttr "Character1_Spine_scaleX.o" "Character1_Spine.sx";
connectAttr "Character1_Spine_scaleY.o" "Character1_Spine.sy";
connectAttr "Character1_Spine_scaleZ.o" "Character1_Spine.sz";
connectAttr "Character1_Spine_translateX.o" "Character1_Spine.tx";
connectAttr "Character1_Spine_translateY.o" "Character1_Spine.ty";
connectAttr "Character1_Spine_translateZ.o" "Character1_Spine.tz";
connectAttr "Character1_Spine_rotateX.o" "Character1_Spine.rx";
connectAttr "Character1_Spine_rotateY.o" "Character1_Spine.ry";
connectAttr "Character1_Spine_rotateZ.o" "Character1_Spine.rz";
connectAttr "Character1_Spine_visibility.o" "Character1_Spine.v";
connectAttr "Character1_Spine.s" "Character1_Spine1.is";
connectAttr "Character1_Spine1_scaleX.o" "Character1_Spine1.sx";
connectAttr "Character1_Spine1_scaleY.o" "Character1_Spine1.sy";
connectAttr "Character1_Spine1_scaleZ.o" "Character1_Spine1.sz";
connectAttr "Character1_Spine1_translateX.o" "Character1_Spine1.tx";
connectAttr "Character1_Spine1_translateY.o" "Character1_Spine1.ty";
connectAttr "Character1_Spine1_translateZ.o" "Character1_Spine1.tz";
connectAttr "Character1_Spine1_rotateX.o" "Character1_Spine1.rx";
connectAttr "Character1_Spine1_rotateY.o" "Character1_Spine1.ry";
connectAttr "Character1_Spine1_rotateZ.o" "Character1_Spine1.rz";
connectAttr "Character1_Spine1_visibility.o" "Character1_Spine1.v";
connectAttr "Character1_Spine1.s" "Character1_LeftShoulder.is";
connectAttr "Character1_LeftShoulder_scaleX.o" "Character1_LeftShoulder.sx";
connectAttr "Character1_LeftShoulder_scaleY.o" "Character1_LeftShoulder.sy";
connectAttr "Character1_LeftShoulder_scaleZ.o" "Character1_LeftShoulder.sz";
connectAttr "Character1_LeftShoulder_translateX.o" "Character1_LeftShoulder.tx";
connectAttr "Character1_LeftShoulder_translateY.o" "Character1_LeftShoulder.ty";
connectAttr "Character1_LeftShoulder_translateZ.o" "Character1_LeftShoulder.tz";
connectAttr "Character1_LeftShoulder_rotateX.o" "Character1_LeftShoulder.rx";
connectAttr "Character1_LeftShoulder_rotateY.o" "Character1_LeftShoulder.ry";
connectAttr "Character1_LeftShoulder_rotateZ.o" "Character1_LeftShoulder.rz";
connectAttr "Character1_LeftShoulder_visibility.o" "Character1_LeftShoulder.v";
connectAttr "Character1_LeftShoulder.s" "Character1_LeftArm.is";
connectAttr "Character1_LeftArm_scaleX.o" "Character1_LeftArm.sx";
connectAttr "Character1_LeftArm_scaleY.o" "Character1_LeftArm.sy";
connectAttr "Character1_LeftArm_scaleZ.o" "Character1_LeftArm.sz";
connectAttr "Character1_LeftArm_translateX.o" "Character1_LeftArm.tx";
connectAttr "Character1_LeftArm_translateY.o" "Character1_LeftArm.ty";
connectAttr "Character1_LeftArm_translateZ.o" "Character1_LeftArm.tz";
connectAttr "Character1_LeftArm_rotateX.o" "Character1_LeftArm.rx";
connectAttr "Character1_LeftArm_rotateY.o" "Character1_LeftArm.ry";
connectAttr "Character1_LeftArm_rotateZ.o" "Character1_LeftArm.rz";
connectAttr "Character1_LeftArm_visibility.o" "Character1_LeftArm.v";
connectAttr "Character1_LeftArm.s" "Character1_LeftForeArm.is";
connectAttr "Character1_LeftForeArm_scaleX.o" "Character1_LeftForeArm.sx";
connectAttr "Character1_LeftForeArm_scaleY.o" "Character1_LeftForeArm.sy";
connectAttr "Character1_LeftForeArm_scaleZ.o" "Character1_LeftForeArm.sz";
connectAttr "Character1_LeftForeArm_translateX.o" "Character1_LeftForeArm.tx";
connectAttr "Character1_LeftForeArm_translateY.o" "Character1_LeftForeArm.ty";
connectAttr "Character1_LeftForeArm_translateZ.o" "Character1_LeftForeArm.tz";
connectAttr "Character1_LeftForeArm_rotateX.o" "Character1_LeftForeArm.rx";
connectAttr "Character1_LeftForeArm_rotateY.o" "Character1_LeftForeArm.ry";
connectAttr "Character1_LeftForeArm_rotateZ.o" "Character1_LeftForeArm.rz";
connectAttr "Character1_LeftForeArm_visibility.o" "Character1_LeftForeArm.v";
connectAttr "Character1_LeftForeArm.s" "Character1_LeftHand.is";
connectAttr "Character1_LeftHand_scaleX.o" "Character1_LeftHand.sx";
connectAttr "Character1_LeftHand_scaleY.o" "Character1_LeftHand.sy";
connectAttr "Character1_LeftHand_scaleZ.o" "Character1_LeftHand.sz";
connectAttr "Character1_LeftHand_translateX.o" "Character1_LeftHand.tx";
connectAttr "Character1_LeftHand_translateY.o" "Character1_LeftHand.ty";
connectAttr "Character1_LeftHand_translateZ.o" "Character1_LeftHand.tz";
connectAttr "Character1_LeftHand_rotateX.o" "Character1_LeftHand.rx";
connectAttr "Character1_LeftHand_rotateY.o" "Character1_LeftHand.ry";
connectAttr "Character1_LeftHand_rotateZ.o" "Character1_LeftHand.rz";
connectAttr "Character1_LeftHand_visibility.o" "Character1_LeftHand.v";
connectAttr "Character1_LeftHand.s" "Character1_LeftHandThumb1.is";
connectAttr "Character1_LeftHandThumb1_scaleX.o" "Character1_LeftHandThumb1.sx";
connectAttr "Character1_LeftHandThumb1_scaleY.o" "Character1_LeftHandThumb1.sy";
connectAttr "Character1_LeftHandThumb1_scaleZ.o" "Character1_LeftHandThumb1.sz";
connectAttr "Character1_LeftHandThumb1_translateX.o" "Character1_LeftHandThumb1.tx"
		;
connectAttr "Character1_LeftHandThumb1_translateY.o" "Character1_LeftHandThumb1.ty"
		;
connectAttr "Character1_LeftHandThumb1_translateZ.o" "Character1_LeftHandThumb1.tz"
		;
connectAttr "Character1_LeftHandThumb1_rotateX.o" "Character1_LeftHandThumb1.rx"
		;
connectAttr "Character1_LeftHandThumb1_rotateY.o" "Character1_LeftHandThumb1.ry"
		;
connectAttr "Character1_LeftHandThumb1_rotateZ.o" "Character1_LeftHandThumb1.rz"
		;
connectAttr "Character1_LeftHandThumb1_visibility.o" "Character1_LeftHandThumb1.v"
		;
connectAttr "Character1_LeftHandThumb1.s" "Character1_LeftHandThumb2.is";
connectAttr "Character1_LeftHandThumb2_scaleX.o" "Character1_LeftHandThumb2.sx";
connectAttr "Character1_LeftHandThumb2_scaleY.o" "Character1_LeftHandThumb2.sy";
connectAttr "Character1_LeftHandThumb2_scaleZ.o" "Character1_LeftHandThumb2.sz";
connectAttr "Character1_LeftHandThumb2_translateX.o" "Character1_LeftHandThumb2.tx"
		;
connectAttr "Character1_LeftHandThumb2_translateY.o" "Character1_LeftHandThumb2.ty"
		;
connectAttr "Character1_LeftHandThumb2_translateZ.o" "Character1_LeftHandThumb2.tz"
		;
connectAttr "Character1_LeftHandThumb2_rotateX.o" "Character1_LeftHandThumb2.rx"
		;
connectAttr "Character1_LeftHandThumb2_rotateY.o" "Character1_LeftHandThumb2.ry"
		;
connectAttr "Character1_LeftHandThumb2_rotateZ.o" "Character1_LeftHandThumb2.rz"
		;
connectAttr "Character1_LeftHandThumb2_visibility.o" "Character1_LeftHandThumb2.v"
		;
connectAttr "Character1_LeftHandThumb2.s" "Character1_LeftHandThumb3.is";
connectAttr "Character1_LeftHandThumb3_translateX.o" "Character1_LeftHandThumb3.tx"
		;
connectAttr "Character1_LeftHandThumb3_translateY.o" "Character1_LeftHandThumb3.ty"
		;
connectAttr "Character1_LeftHandThumb3_translateZ.o" "Character1_LeftHandThumb3.tz"
		;
connectAttr "Character1_LeftHandThumb3_scaleX.o" "Character1_LeftHandThumb3.sx";
connectAttr "Character1_LeftHandThumb3_scaleY.o" "Character1_LeftHandThumb3.sy";
connectAttr "Character1_LeftHandThumb3_scaleZ.o" "Character1_LeftHandThumb3.sz";
connectAttr "Character1_LeftHandThumb3_rotateX.o" "Character1_LeftHandThumb3.rx"
		;
connectAttr "Character1_LeftHandThumb3_rotateY.o" "Character1_LeftHandThumb3.ry"
		;
connectAttr "Character1_LeftHandThumb3_rotateZ.o" "Character1_LeftHandThumb3.rz"
		;
connectAttr "Character1_LeftHandThumb3_visibility.o" "Character1_LeftHandThumb3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandIndex1.is";
connectAttr "Character1_LeftHandIndex1_scaleX.o" "Character1_LeftHandIndex1.sx";
connectAttr "Character1_LeftHandIndex1_scaleY.o" "Character1_LeftHandIndex1.sy";
connectAttr "Character1_LeftHandIndex1_scaleZ.o" "Character1_LeftHandIndex1.sz";
connectAttr "Character1_LeftHandIndex1_translateX.o" "Character1_LeftHandIndex1.tx"
		;
connectAttr "Character1_LeftHandIndex1_translateY.o" "Character1_LeftHandIndex1.ty"
		;
connectAttr "Character1_LeftHandIndex1_translateZ.o" "Character1_LeftHandIndex1.tz"
		;
connectAttr "Character1_LeftHandIndex1_rotateX.o" "Character1_LeftHandIndex1.rx"
		;
connectAttr "Character1_LeftHandIndex1_rotateY.o" "Character1_LeftHandIndex1.ry"
		;
connectAttr "Character1_LeftHandIndex1_rotateZ.o" "Character1_LeftHandIndex1.rz"
		;
connectAttr "Character1_LeftHandIndex1_visibility.o" "Character1_LeftHandIndex1.v"
		;
connectAttr "Character1_LeftHandIndex1.s" "Character1_LeftHandIndex2.is";
connectAttr "Character1_LeftHandIndex2_scaleX.o" "Character1_LeftHandIndex2.sx";
connectAttr "Character1_LeftHandIndex2_scaleY.o" "Character1_LeftHandIndex2.sy";
connectAttr "Character1_LeftHandIndex2_scaleZ.o" "Character1_LeftHandIndex2.sz";
connectAttr "Character1_LeftHandIndex2_translateX.o" "Character1_LeftHandIndex2.tx"
		;
connectAttr "Character1_LeftHandIndex2_translateY.o" "Character1_LeftHandIndex2.ty"
		;
connectAttr "Character1_LeftHandIndex2_translateZ.o" "Character1_LeftHandIndex2.tz"
		;
connectAttr "Character1_LeftHandIndex2_rotateX.o" "Character1_LeftHandIndex2.rx"
		;
connectAttr "Character1_LeftHandIndex2_rotateY.o" "Character1_LeftHandIndex2.ry"
		;
connectAttr "Character1_LeftHandIndex2_rotateZ.o" "Character1_LeftHandIndex2.rz"
		;
connectAttr "Character1_LeftHandIndex2_visibility.o" "Character1_LeftHandIndex2.v"
		;
connectAttr "Character1_LeftHandIndex2.s" "Character1_LeftHandIndex3.is";
connectAttr "Character1_LeftHandIndex3_translateX.o" "Character1_LeftHandIndex3.tx"
		;
connectAttr "Character1_LeftHandIndex3_translateY.o" "Character1_LeftHandIndex3.ty"
		;
connectAttr "Character1_LeftHandIndex3_translateZ.o" "Character1_LeftHandIndex3.tz"
		;
connectAttr "Character1_LeftHandIndex3_scaleX.o" "Character1_LeftHandIndex3.sx";
connectAttr "Character1_LeftHandIndex3_scaleY.o" "Character1_LeftHandIndex3.sy";
connectAttr "Character1_LeftHandIndex3_scaleZ.o" "Character1_LeftHandIndex3.sz";
connectAttr "Character1_LeftHandIndex3_rotateX.o" "Character1_LeftHandIndex3.rx"
		;
connectAttr "Character1_LeftHandIndex3_rotateY.o" "Character1_LeftHandIndex3.ry"
		;
connectAttr "Character1_LeftHandIndex3_rotateZ.o" "Character1_LeftHandIndex3.rz"
		;
connectAttr "Character1_LeftHandIndex3_visibility.o" "Character1_LeftHandIndex3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandRing1.is";
connectAttr "Character1_LeftHandRing1_scaleX.o" "Character1_LeftHandRing1.sx";
connectAttr "Character1_LeftHandRing1_scaleY.o" "Character1_LeftHandRing1.sy";
connectAttr "Character1_LeftHandRing1_scaleZ.o" "Character1_LeftHandRing1.sz";
connectAttr "Character1_LeftHandRing1_translateX.o" "Character1_LeftHandRing1.tx"
		;
connectAttr "Character1_LeftHandRing1_translateY.o" "Character1_LeftHandRing1.ty"
		;
connectAttr "Character1_LeftHandRing1_translateZ.o" "Character1_LeftHandRing1.tz"
		;
connectAttr "Character1_LeftHandRing1_rotateX.o" "Character1_LeftHandRing1.rx";
connectAttr "Character1_LeftHandRing1_rotateY.o" "Character1_LeftHandRing1.ry";
connectAttr "Character1_LeftHandRing1_rotateZ.o" "Character1_LeftHandRing1.rz";
connectAttr "Character1_LeftHandRing1_visibility.o" "Character1_LeftHandRing1.v"
		;
connectAttr "Character1_LeftHandRing1.s" "Character1_LeftHandRing2.is";
connectAttr "Character1_LeftHandRing2_scaleX.o" "Character1_LeftHandRing2.sx";
connectAttr "Character1_LeftHandRing2_scaleY.o" "Character1_LeftHandRing2.sy";
connectAttr "Character1_LeftHandRing2_scaleZ.o" "Character1_LeftHandRing2.sz";
connectAttr "Character1_LeftHandRing2_translateX.o" "Character1_LeftHandRing2.tx"
		;
connectAttr "Character1_LeftHandRing2_translateY.o" "Character1_LeftHandRing2.ty"
		;
connectAttr "Character1_LeftHandRing2_translateZ.o" "Character1_LeftHandRing2.tz"
		;
connectAttr "Character1_LeftHandRing2_rotateX.o" "Character1_LeftHandRing2.rx";
connectAttr "Character1_LeftHandRing2_rotateY.o" "Character1_LeftHandRing2.ry";
connectAttr "Character1_LeftHandRing2_rotateZ.o" "Character1_LeftHandRing2.rz";
connectAttr "Character1_LeftHandRing2_visibility.o" "Character1_LeftHandRing2.v"
		;
connectAttr "Character1_LeftHandRing2.s" "Character1_LeftHandRing3.is";
connectAttr "Character1_LeftHandRing3_translateX.o" "Character1_LeftHandRing3.tx"
		;
connectAttr "Character1_LeftHandRing3_translateY.o" "Character1_LeftHandRing3.ty"
		;
connectAttr "Character1_LeftHandRing3_translateZ.o" "Character1_LeftHandRing3.tz"
		;
connectAttr "Character1_LeftHandRing3_scaleX.o" "Character1_LeftHandRing3.sx";
connectAttr "Character1_LeftHandRing3_scaleY.o" "Character1_LeftHandRing3.sy";
connectAttr "Character1_LeftHandRing3_scaleZ.o" "Character1_LeftHandRing3.sz";
connectAttr "Character1_LeftHandRing3_rotateX.o" "Character1_LeftHandRing3.rx";
connectAttr "Character1_LeftHandRing3_rotateY.o" "Character1_LeftHandRing3.ry";
connectAttr "Character1_LeftHandRing3_rotateZ.o" "Character1_LeftHandRing3.rz";
connectAttr "Character1_LeftHandRing3_visibility.o" "Character1_LeftHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_RightShoulder.is";
connectAttr "Character1_RightShoulder_scaleX.o" "Character1_RightShoulder.sx";
connectAttr "Character1_RightShoulder_scaleY.o" "Character1_RightShoulder.sy";
connectAttr "Character1_RightShoulder_scaleZ.o" "Character1_RightShoulder.sz";
connectAttr "Character1_RightShoulder_translateX.o" "Character1_RightShoulder.tx"
		;
connectAttr "Character1_RightShoulder_translateY.o" "Character1_RightShoulder.ty"
		;
connectAttr "Character1_RightShoulder_translateZ.o" "Character1_RightShoulder.tz"
		;
connectAttr "Character1_RightShoulder_rotateX.o" "Character1_RightShoulder.rx";
connectAttr "Character1_RightShoulder_rotateY.o" "Character1_RightShoulder.ry";
connectAttr "Character1_RightShoulder_rotateZ.o" "Character1_RightShoulder.rz";
connectAttr "Character1_RightShoulder_visibility.o" "Character1_RightShoulder.v"
		;
connectAttr "Character1_RightShoulder.s" "Character1_RightArm.is";
connectAttr "Character1_RightArm_scaleX.o" "Character1_RightArm.sx";
connectAttr "Character1_RightArm_scaleY.o" "Character1_RightArm.sy";
connectAttr "Character1_RightArm_scaleZ.o" "Character1_RightArm.sz";
connectAttr "Character1_RightArm_translateX.o" "Character1_RightArm.tx";
connectAttr "Character1_RightArm_translateY.o" "Character1_RightArm.ty";
connectAttr "Character1_RightArm_translateZ.o" "Character1_RightArm.tz";
connectAttr "Character1_RightArm_rotateX.o" "Character1_RightArm.rx";
connectAttr "Character1_RightArm_rotateY.o" "Character1_RightArm.ry";
connectAttr "Character1_RightArm_rotateZ.o" "Character1_RightArm.rz";
connectAttr "Character1_RightArm_visibility.o" "Character1_RightArm.v";
connectAttr "Character1_RightArm.s" "Character1_RightForeArm.is";
connectAttr "Character1_RightForeArm_scaleX.o" "Character1_RightForeArm.sx";
connectAttr "Character1_RightForeArm_scaleY.o" "Character1_RightForeArm.sy";
connectAttr "Character1_RightForeArm_scaleZ.o" "Character1_RightForeArm.sz";
connectAttr "Character1_RightForeArm_translateX.o" "Character1_RightForeArm.tx";
connectAttr "Character1_RightForeArm_translateY.o" "Character1_RightForeArm.ty";
connectAttr "Character1_RightForeArm_translateZ.o" "Character1_RightForeArm.tz";
connectAttr "Character1_RightForeArm_rotateX.o" "Character1_RightForeArm.rx";
connectAttr "Character1_RightForeArm_rotateY.o" "Character1_RightForeArm.ry";
connectAttr "Character1_RightForeArm_rotateZ.o" "Character1_RightForeArm.rz";
connectAttr "Character1_RightForeArm_visibility.o" "Character1_RightForeArm.v";
connectAttr "Character1_RightForeArm.s" "Character1_RightHand.is";
connectAttr "Character1_RightHand_scaleX.o" "Character1_RightHand.sx";
connectAttr "Character1_RightHand_scaleY.o" "Character1_RightHand.sy";
connectAttr "Character1_RightHand_scaleZ.o" "Character1_RightHand.sz";
connectAttr "Character1_RightHand_translateX.o" "Character1_RightHand.tx";
connectAttr "Character1_RightHand_translateY.o" "Character1_RightHand.ty";
connectAttr "Character1_RightHand_translateZ.o" "Character1_RightHand.tz";
connectAttr "Character1_RightHand_rotateX.o" "Character1_RightHand.rx";
connectAttr "Character1_RightHand_rotateY.o" "Character1_RightHand.ry";
connectAttr "Character1_RightHand_rotateZ.o" "Character1_RightHand.rz";
connectAttr "Character1_RightHand_visibility.o" "Character1_RightHand.v";
connectAttr "Character1_RightHand.s" "Character1_RightHandThumb1.is";
connectAttr "Character1_RightHandThumb1_scaleX.o" "Character1_RightHandThumb1.sx"
		;
connectAttr "Character1_RightHandThumb1_scaleY.o" "Character1_RightHandThumb1.sy"
		;
connectAttr "Character1_RightHandThumb1_scaleZ.o" "Character1_RightHandThumb1.sz"
		;
connectAttr "Character1_RightHandThumb1_translateX.o" "Character1_RightHandThumb1.tx"
		;
connectAttr "Character1_RightHandThumb1_translateY.o" "Character1_RightHandThumb1.ty"
		;
connectAttr "Character1_RightHandThumb1_translateZ.o" "Character1_RightHandThumb1.tz"
		;
connectAttr "Character1_RightHandThumb1_rotateX.o" "Character1_RightHandThumb1.rx"
		;
connectAttr "Character1_RightHandThumb1_rotateY.o" "Character1_RightHandThumb1.ry"
		;
connectAttr "Character1_RightHandThumb1_rotateZ.o" "Character1_RightHandThumb1.rz"
		;
connectAttr "Character1_RightHandThumb1_visibility.o" "Character1_RightHandThumb1.v"
		;
connectAttr "Character1_RightHandThumb1.s" "Character1_RightHandThumb2.is";
connectAttr "Character1_RightHandThumb2_scaleX.o" "Character1_RightHandThumb2.sx"
		;
connectAttr "Character1_RightHandThumb2_scaleY.o" "Character1_RightHandThumb2.sy"
		;
connectAttr "Character1_RightHandThumb2_scaleZ.o" "Character1_RightHandThumb2.sz"
		;
connectAttr "Character1_RightHandThumb2_translateX.o" "Character1_RightHandThumb2.tx"
		;
connectAttr "Character1_RightHandThumb2_translateY.o" "Character1_RightHandThumb2.ty"
		;
connectAttr "Character1_RightHandThumb2_translateZ.o" "Character1_RightHandThumb2.tz"
		;
connectAttr "Character1_RightHandThumb2_rotateX.o" "Character1_RightHandThumb2.rx"
		;
connectAttr "Character1_RightHandThumb2_rotateY.o" "Character1_RightHandThumb2.ry"
		;
connectAttr "Character1_RightHandThumb2_rotateZ.o" "Character1_RightHandThumb2.rz"
		;
connectAttr "Character1_RightHandThumb2_visibility.o" "Character1_RightHandThumb2.v"
		;
connectAttr "Character1_RightHandThumb2.s" "Character1_RightHandThumb3.is";
connectAttr "Character1_RightHandThumb3_translateX.o" "Character1_RightHandThumb3.tx"
		;
connectAttr "Character1_RightHandThumb3_translateY.o" "Character1_RightHandThumb3.ty"
		;
connectAttr "Character1_RightHandThumb3_translateZ.o" "Character1_RightHandThumb3.tz"
		;
connectAttr "Character1_RightHandThumb3_scaleX.o" "Character1_RightHandThumb3.sx"
		;
connectAttr "Character1_RightHandThumb3_scaleY.o" "Character1_RightHandThumb3.sy"
		;
connectAttr "Character1_RightHandThumb3_scaleZ.o" "Character1_RightHandThumb3.sz"
		;
connectAttr "Character1_RightHandThumb3_rotateX.o" "Character1_RightHandThumb3.rx"
		;
connectAttr "Character1_RightHandThumb3_rotateY.o" "Character1_RightHandThumb3.ry"
		;
connectAttr "Character1_RightHandThumb3_rotateZ.o" "Character1_RightHandThumb3.rz"
		;
connectAttr "Character1_RightHandThumb3_visibility.o" "Character1_RightHandThumb3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandIndex1.is";
connectAttr "Character1_RightHandIndex1_scaleX.o" "Character1_RightHandIndex1.sx"
		;
connectAttr "Character1_RightHandIndex1_scaleY.o" "Character1_RightHandIndex1.sy"
		;
connectAttr "Character1_RightHandIndex1_scaleZ.o" "Character1_RightHandIndex1.sz"
		;
connectAttr "Character1_RightHandIndex1_translateX.o" "Character1_RightHandIndex1.tx"
		;
connectAttr "Character1_RightHandIndex1_translateY.o" "Character1_RightHandIndex1.ty"
		;
connectAttr "Character1_RightHandIndex1_translateZ.o" "Character1_RightHandIndex1.tz"
		;
connectAttr "Character1_RightHandIndex1_rotateX.o" "Character1_RightHandIndex1.rx"
		;
connectAttr "Character1_RightHandIndex1_rotateY.o" "Character1_RightHandIndex1.ry"
		;
connectAttr "Character1_RightHandIndex1_rotateZ.o" "Character1_RightHandIndex1.rz"
		;
connectAttr "Character1_RightHandIndex1_visibility.o" "Character1_RightHandIndex1.v"
		;
connectAttr "Character1_RightHandIndex1.s" "Character1_RightHandIndex2.is";
connectAttr "Character1_RightHandIndex2_scaleX.o" "Character1_RightHandIndex2.sx"
		;
connectAttr "Character1_RightHandIndex2_scaleY.o" "Character1_RightHandIndex2.sy"
		;
connectAttr "Character1_RightHandIndex2_scaleZ.o" "Character1_RightHandIndex2.sz"
		;
connectAttr "Character1_RightHandIndex2_translateX.o" "Character1_RightHandIndex2.tx"
		;
connectAttr "Character1_RightHandIndex2_translateY.o" "Character1_RightHandIndex2.ty"
		;
connectAttr "Character1_RightHandIndex2_translateZ.o" "Character1_RightHandIndex2.tz"
		;
connectAttr "Character1_RightHandIndex2_rotateX.o" "Character1_RightHandIndex2.rx"
		;
connectAttr "Character1_RightHandIndex2_rotateY.o" "Character1_RightHandIndex2.ry"
		;
connectAttr "Character1_RightHandIndex2_rotateZ.o" "Character1_RightHandIndex2.rz"
		;
connectAttr "Character1_RightHandIndex2_visibility.o" "Character1_RightHandIndex2.v"
		;
connectAttr "Character1_RightHandIndex2.s" "Character1_RightHandIndex3.is";
connectAttr "Character1_RightHandIndex3_translateX.o" "Character1_RightHandIndex3.tx"
		;
connectAttr "Character1_RightHandIndex3_translateY.o" "Character1_RightHandIndex3.ty"
		;
connectAttr "Character1_RightHandIndex3_translateZ.o" "Character1_RightHandIndex3.tz"
		;
connectAttr "Character1_RightHandIndex3_scaleX.o" "Character1_RightHandIndex3.sx"
		;
connectAttr "Character1_RightHandIndex3_scaleY.o" "Character1_RightHandIndex3.sy"
		;
connectAttr "Character1_RightHandIndex3_scaleZ.o" "Character1_RightHandIndex3.sz"
		;
connectAttr "Character1_RightHandIndex3_rotateX.o" "Character1_RightHandIndex3.rx"
		;
connectAttr "Character1_RightHandIndex3_rotateY.o" "Character1_RightHandIndex3.ry"
		;
connectAttr "Character1_RightHandIndex3_rotateZ.o" "Character1_RightHandIndex3.rz"
		;
connectAttr "Character1_RightHandIndex3_visibility.o" "Character1_RightHandIndex3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandRing1.is";
connectAttr "Character1_RightHandRing1_scaleX.o" "Character1_RightHandRing1.sx";
connectAttr "Character1_RightHandRing1_scaleY.o" "Character1_RightHandRing1.sy";
connectAttr "Character1_RightHandRing1_scaleZ.o" "Character1_RightHandRing1.sz";
connectAttr "Character1_RightHandRing1_translateX.o" "Character1_RightHandRing1.tx"
		;
connectAttr "Character1_RightHandRing1_translateY.o" "Character1_RightHandRing1.ty"
		;
connectAttr "Character1_RightHandRing1_translateZ.o" "Character1_RightHandRing1.tz"
		;
connectAttr "Character1_RightHandRing1_rotateX.o" "Character1_RightHandRing1.rx"
		;
connectAttr "Character1_RightHandRing1_rotateY.o" "Character1_RightHandRing1.ry"
		;
connectAttr "Character1_RightHandRing1_rotateZ.o" "Character1_RightHandRing1.rz"
		;
connectAttr "Character1_RightHandRing1_visibility.o" "Character1_RightHandRing1.v"
		;
connectAttr "Character1_RightHandRing1.s" "Character1_RightHandRing2.is";
connectAttr "Character1_RightHandRing2_scaleX.o" "Character1_RightHandRing2.sx";
connectAttr "Character1_RightHandRing2_scaleY.o" "Character1_RightHandRing2.sy";
connectAttr "Character1_RightHandRing2_scaleZ.o" "Character1_RightHandRing2.sz";
connectAttr "Character1_RightHandRing2_translateX.o" "Character1_RightHandRing2.tx"
		;
connectAttr "Character1_RightHandRing2_translateY.o" "Character1_RightHandRing2.ty"
		;
connectAttr "Character1_RightHandRing2_translateZ.o" "Character1_RightHandRing2.tz"
		;
connectAttr "Character1_RightHandRing2_rotateX.o" "Character1_RightHandRing2.rx"
		;
connectAttr "Character1_RightHandRing2_rotateY.o" "Character1_RightHandRing2.ry"
		;
connectAttr "Character1_RightHandRing2_rotateZ.o" "Character1_RightHandRing2.rz"
		;
connectAttr "Character1_RightHandRing2_visibility.o" "Character1_RightHandRing2.v"
		;
connectAttr "Character1_RightHandRing2.s" "Character1_RightHandRing3.is";
connectAttr "Character1_RightHandRing3_translateX.o" "Character1_RightHandRing3.tx"
		;
connectAttr "Character1_RightHandRing3_translateY.o" "Character1_RightHandRing3.ty"
		;
connectAttr "Character1_RightHandRing3_translateZ.o" "Character1_RightHandRing3.tz"
		;
connectAttr "Character1_RightHandRing3_scaleX.o" "Character1_RightHandRing3.sx";
connectAttr "Character1_RightHandRing3_scaleY.o" "Character1_RightHandRing3.sy";
connectAttr "Character1_RightHandRing3_scaleZ.o" "Character1_RightHandRing3.sz";
connectAttr "Character1_RightHandRing3_rotateX.o" "Character1_RightHandRing3.rx"
		;
connectAttr "Character1_RightHandRing3_rotateY.o" "Character1_RightHandRing3.ry"
		;
connectAttr "Character1_RightHandRing3_rotateZ.o" "Character1_RightHandRing3.rz"
		;
connectAttr "Character1_RightHandRing3_visibility.o" "Character1_RightHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_Neck.is";
connectAttr "Character1_Neck_scaleX.o" "Character1_Neck.sx";
connectAttr "Character1_Neck_scaleY.o" "Character1_Neck.sy";
connectAttr "Character1_Neck_scaleZ.o" "Character1_Neck.sz";
connectAttr "Character1_Neck_translateX.o" "Character1_Neck.tx";
connectAttr "Character1_Neck_translateY.o" "Character1_Neck.ty";
connectAttr "Character1_Neck_translateZ.o" "Character1_Neck.tz";
connectAttr "Character1_Neck_rotateX.o" "Character1_Neck.rx";
connectAttr "Character1_Neck_rotateY.o" "Character1_Neck.ry";
connectAttr "Character1_Neck_rotateZ.o" "Character1_Neck.rz";
connectAttr "Character1_Neck_visibility.o" "Character1_Neck.v";
connectAttr "Character1_Neck.s" "Character1_Head.is";
connectAttr "Character1_Head_scaleX.o" "Character1_Head.sx";
connectAttr "Character1_Head_scaleY.o" "Character1_Head.sy";
connectAttr "Character1_Head_scaleZ.o" "Character1_Head.sz";
connectAttr "Character1_Head_translateX.o" "Character1_Head.tx";
connectAttr "Character1_Head_translateY.o" "Character1_Head.ty";
connectAttr "Character1_Head_translateZ.o" "Character1_Head.tz";
connectAttr "Character1_Head_rotateX.o" "Character1_Head.rx";
connectAttr "Character1_Head_rotateY.o" "Character1_Head.ry";
connectAttr "Character1_Head_rotateZ.o" "Character1_Head.rz";
connectAttr "Character1_Head_visibility.o" "Character1_Head.v";
connectAttr "Character1_Head.s" "jaw.is";
connectAttr "jaw_lockInfluenceWeights.o" "jaw.liw";
connectAttr "jaw_translateX.o" "jaw.tx";
connectAttr "jaw_translateY.o" "jaw.ty";
connectAttr "jaw_translateZ.o" "jaw.tz";
connectAttr "jaw_scaleX.o" "jaw.sx";
connectAttr "jaw_scaleY.o" "jaw.sy";
connectAttr "jaw_scaleZ.o" "jaw.sz";
connectAttr "jaw_rotateX.o" "jaw.rx";
connectAttr "jaw_rotateY.o" "jaw.ry";
connectAttr "jaw_rotateZ.o" "jaw.rz";
connectAttr "jaw_visibility.o" "jaw.v";
connectAttr "Character1_Head.s" "eyes.is";
connectAttr "eyes_translateX.o" "eyes.tx";
connectAttr "eyes_translateY.o" "eyes.ty";
connectAttr "eyes_translateZ.o" "eyes.tz";
connectAttr "eyes_scaleX.o" "eyes.sx";
connectAttr "eyes_scaleY.o" "eyes.sy";
connectAttr "eyes_scaleZ.o" "eyes.sz";
connectAttr "eyes_rotateX.o" "eyes.rx";
connectAttr "eyes_rotateY.o" "eyes.ry";
connectAttr "eyes_rotateZ.o" "eyes.rz";
connectAttr "eyes_visibility.o" "eyes.v";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Enemy@Grunt_Idle_01.ma
