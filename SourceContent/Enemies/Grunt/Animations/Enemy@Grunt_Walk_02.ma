//Maya ASCII 2016 scene
//Name: Enemy@Grunt_Walk_02.ma
//Last modified: Sun, Nov 22, 2015 03:13:34 PM
//Codeset: 1252
requires maya "2016";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "717E1B42-4B32-8409-CF1D-ACA446AC0DA3";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 136.12381390983325 123.68337786070539 311.57694124299979 ;
	setAttr ".r" -type "double3" -14.138352729602905 23.000000000000345 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "3449A2C1-490F-734D-6507-04A868D53A4C";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 351.57454569522139;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "2D8E8848-429F-33C1-33C0-DDB3380FACFB";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "2C637693-4C24-8E18-1EB7-B7B3502B8626";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "E5D70567-44B4-7408-337C-418C9DD268F3";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "81B71F9B-465C-A967-488E-9687E06B982B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "61143844-4431-F129-6A7E-479E389DE302";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "0EC61F90-452F-FE54-A452-E4908ADF29E8";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "ref_frame";
	rename -uid "60B6AEDA-4C9E-F6C6-20C6-25B1CDE1249F";
	addAttr -ci true -sn "refFrame" -ln "refFrame" -at "double";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.5;
createNode joint -n "Character1_Hips" -p "ref_frame";
	rename -uid "34FD21C9-4072-C506-F84C-919626E38053";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 1.406920211983633 -3.3458391704689405 -22.822435086355146 ;
	setAttr ".ssc" no;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftUpLeg" -p "Character1_Hips";
	rename -uid "EC318352-491D-F9ED-158E-BA8487C1E2BF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 96.630252250634001 6.5132649289235918 97.599644355322027 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftLeg" -p "Character1_LeftUpLeg";
	rename -uid "7B9376EA-4226-9C8A-F57E-6A862628664D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -129.48089468354581 -7.9067044581664252 177.65996829599328 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFoot" -p "Character1_LeftLeg";
	rename -uid "A18F2D90-430C-C20B-6764-A8BBDAFB75B8";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -82.075120143962664 29.940914059061775 128.00062508119765 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftToeBase" -p "Character1_LeftFoot";
	rename -uid "42BE96D5-4C0C-9C1E-4B2B-CABE5F924815";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -126.16837928120003 -7.059491002168353 35.858557690282048 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle1" -p "Character1_LeftToeBase";
	rename -uid "8E13E922-4EDE-BF75-8372-199666BE4F95";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -100.08944018296057 24.928076457701248 56.143888543356006 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftFootMiddle2" -p "Character1_LeftFootMiddle1";
	rename -uid "DD4EE85C-4EFF-FEFC-6702-CA852F7D6F3E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -39.508027973091373 -2.0582897454817326 31.259679900637106 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightUpLeg" -p "Character1_Hips";
	rename -uid "A2EF308E-45C5-F27B-2281-3782DB4C29DE";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 11.416489881294433 -11.43673382189878 -152.25229525481123 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightLeg" -p "Character1_RightUpLeg";
	rename -uid "B1F8564A-4BB2-241D-08D5-839C5DCE491E";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -22.942928776539453 -35.956391389429932 101.2794675694641 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFoot" -p "Character1_RightLeg";
	rename -uid "72CB3E52-4F80-BC1D-36A0-F4B1CE2CF70F";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 66.643786034585347 17.03863148951929 124.91120862192221 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightToeBase" -p "Character1_RightFoot";
	rename -uid "16C7D96F-4E83-0B91-8EFF-3FADC26C237C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -155.84366405206632 -5.8738653577349114 -93.73595023516998 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle1" -p "Character1_RightToeBase";
	rename -uid "83CEA1C4-40E5-4AFF-379E-15A256900F19";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -166.06091029301223 54.415170601070251 -127.18425880983375 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightFootMiddle2" -p "Character1_RightFootMiddle1";
	rename -uid "E9B0CA4B-4DAD-B142-03BD-0E8933548A19";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -129.2483692316568 10.731788640430997 6.9892235317174372 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine" -p "Character1_Hips";
	rename -uid "CCC7425F-4F08-CAD0-6C3C-DE96B5827D82";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 11.416489881294433 -11.43673382189878 -152.25229525481123 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Spine1" -p "Character1_Spine";
	rename -uid "450867C5-4E82-0814-D52D-A0937914A2F6";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 24.933471933369759 -37.782249520228419 -20.761597110625811 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftShoulder" -p "Character1_Spine1";
	rename -uid "604091A5-4484-ADEE-8480-489DA65753F6";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 144.67442469508543 70.793071067570196 19.807411116001759 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftArm" -p "Character1_LeftShoulder";
	rename -uid "06DDB4F9-4063-F0F9-191A-758B4B0F4696";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 99.384574925726014 -60.006523708111558 170.15034313799799 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftForeArm" -p "Character1_LeftArm";
	rename -uid "43CC1AA9-4AF4-2BDF-896C-21A9375D1F23";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 70.061799635480924 -4.8580147780287897 21.798459685601706 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHand" -p "Character1_LeftForeArm";
	rename -uid "38FD50C9-4048-0036-9975-0FA1C64E5B3A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.9708494230587 -13.075378107872792 150.36023365917421 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb1" -p "Character1_LeftHand";
	rename -uid "7E4C3BAF-4724-0707-B732-D7B7F18407D7";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -65.748924167512314 39.787402356574312 166.7746235430387 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb2" -p "Character1_LeftHandThumb1";
	rename -uid "0C323ADF-445E-6E32-AA35-F1AF92EC921B";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -39.861597330550808 6.7221908644292059 68.670730027774553 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandThumb3" -p "Character1_LeftHandThumb2";
	rename -uid "C7B6D069-4442-2ABC-B8EC-5283753CEE99";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -120.21671274765816 -26.239331955300646 57.413978285788289 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex1" -p "Character1_LeftHand";
	rename -uid "4DAF35CC-40C3-E897-69DB-3DAE2D193C9A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -152.38220651039205 4.806304023778381 48.842756939944849 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex2" -p "Character1_LeftHandIndex1";
	rename -uid "2A1C4BF1-4DB1-637C-4FE4-59830DAAD910";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 176.30914278667802 -13.434888179376443 97.633542306389202 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandIndex3" -p "Character1_LeftHandIndex2";
	rename -uid "377FD4D2-46C1-3A06-C018-3992FDB54ABF";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -102.43860404365057 -60.659303181068822 -152.9472923666747 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing1" -p "Character1_LeftHand";
	rename -uid "E974B0D9-4F10-E19B-C9DC-50BF4329C97A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -31.516946849722316 2.5758733401430476 -137.43155166436202 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing2" -p "Character1_LeftHandRing1";
	rename -uid "8FDD3B2F-4A7A-2007-272F-63B2908F644A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -37.632109434888541 57.372964012610872 -137.42548313576771 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_LeftHandRing3" -p "Character1_LeftHandRing2";
	rename -uid "C796B10F-4BD0-488D-89AA-08AE34A8A9BD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -16.443988862208798 8.3290143241887424 -59.438981507600595 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightShoulder" -p "Character1_Spine1";
	rename -uid "62023433-433B-E394-AA76-A0A1CAED9341";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -174.37324535156188 -81.21351031321781 -130.38505903740904 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightArm" -p "Character1_RightShoulder";
	rename -uid "0CE18830-4B8C-0C0D-DA20-5E9B1B7DBA41";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -104.33239449525594 -37.101615257968433 20.137114731244228 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightForeArm" -p "Character1_RightArm";
	rename -uid "084FBB8B-40E3-AB34-8CFB-B5AE5CA3F899";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -43.743657547705759 -9.6726323149773243 103.83730983340543 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHand" -p "Character1_RightForeArm";
	rename -uid "7C222FCC-4D93-082F-B076-0A9CBD9E8CC6";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 140.03960701773187 13.516449256889247 -135.99675148159383 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb1" -p "Character1_RightHand";
	rename -uid "D3FC1B6E-4078-0A44-B02B-3E94380170D0";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.10974980768816 71.94515848234029 -178.78334614629264 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb2" -p "Character1_RightHandThumb1";
	rename -uid "00DD4152-4645-59C7-6263-56AF240181DD";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.2056241799594538 33.542113270229827 -130.48989956049317 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandThumb3" -p "Character1_RightHandThumb2";
	rename -uid "960D9F96-47B3-9999-7B80-D18105B82B95";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 134.7539820196863 -70.074251222974951 75.428204415317296 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex1" -p "Character1_RightHand";
	rename -uid "8176A505-4069-5D82-8B68-E3804701599A";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.10974980768816 71.94515848234029 -178.78334614629264 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex2" -p "Character1_RightHandIndex1";
	rename -uid "D6EEECD7-4B2E-00AC-F179-64B23BFDFFBC";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.2057394778445882 33.54331930430493 -130.49010822234442 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandIndex3" -p "Character1_RightHandIndex2";
	rename -uid "781C5589-46EF-4C8A-55C5-9489C38FC926";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 134.79643182941854 -70.056599207814415 75.383915829770686 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing1" -p "Character1_RightHand";
	rename -uid "FABC0D93-40AF-749D-712C-DE8C75AA6575";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -143.10974980768816 71.94515848234029 -178.78334614629264 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing2" -p "Character1_RightHandRing1";
	rename -uid "7A40EF3C-43BA-D7D8-F8BA-D2858C661E39";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -8.2056241799594538 33.542113270229827 -130.48989956049317 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_RightHandRing3" -p "Character1_RightHandRing2";
	rename -uid "2AFF7A1C-40BF-5180-C617-918B45B29128";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 134.7539820196863 -70.074251222974951 75.428204415317296 ;
	setAttr ".radi" 0.96499850933368381;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Neck" -p "Character1_Spine1";
	rename -uid "AE1C9DC6-4E06-F554-E31D-9F802B3FBDE6";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" 161.13402532275541 31.934232202611735 171.90006021381112 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "Character1_Head" -p "Character1_Neck";
	rename -uid "3F2893D7-4874-1A8B-AB2C-6082F2F0134D";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -18.098521216998183 -9.9246993917445305 116.26360516393567 ;
	setAttr ".radi" 2.8949955280010511;
	setAttr ".fbxID" 5;
createNode joint -n "jaw" -p "Character1_Head";
	rename -uid "45747254-496D-9553-9411-69A25B5BA10C";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.667422224424381 90 0 ;
	setAttr ".radi" 6;
	setAttr -k on ".liw";
	setAttr ".fbxID" 5;
createNode joint -n "eyes" -p "Character1_Head";
	rename -uid "C9CD438C-4D4A-C62C-3EC3-1DAAB12DEBFA";
	addAttr -is true -ci true -k true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 
		1 -at "bool";
	addAttr -ci true -h true -sn "fbxID" -ln "filmboxTypeID" -at "short";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	setAttr ".jo" -type "double3" -25.667422224424381 90 0 ;
	setAttr ".radi" 4.5;
	setAttr ".fbxID" 5;
createNode animCurveTU -n "jaw_lockInfluenceWeights";
	rename -uid "ED003ACC-4A9C-369D-E984-69AB2358C29C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0;
createNode animCurveTL -n "Character1_Hips_translateX";
	rename -uid "73B32F4F-4265-7A84-313B-05A3CA2B7F04";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 0.31991457939147949 1 0.45354592800140381
		 2 0.57799309492111206 3 0.69488638639450073 4 0.81915080547332764 5 0.94138950109481812
		 6 1.0522059202194214 7 1.1422032117843628 8 1.2019848823547363 9 1.1809945106506348
		 10 1.0767279863357544 11 0.95186835527420044 12 0.82288235425949097 13 0.67594367265701294
		 14 0.52829611301422119 15 0.39718317985534668 16 0.30833733081817627 17 0.25624126195907593
		 18 0.21063858270645142 19 0.1635480523109436 20 0.1187516450881958 21 0.08003079891204834
		 22 0.051167130470275879 23 0.035942554473876953 24 0.041903316974639893 25 0.077460289001464844
		 26 0.12093156576156616 27 0.16511958837509155 28 0.21514704823493958 29 0.26781252026557922
		 30 0.31991457939147949;
createNode animCurveTL -n "Character1_Hips_translateY";
	rename -uid "A735BBE8-4843-225F-0468-5DBE0C0946F6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 45.982681274414063 1 44.565296173095703
		 2 43.831405639648438 3 43.970378875732422 4 44.53173828125 5 45.363945007324219 6 46.315456390380859
		 7 47.234729766845703 8 47.970230102539063 9 48.54632568359375 10 48.993465423583984
		 11 49.169235229492188 12 48.977287292480469 13 48.409523010253906 14 47.455860137939453
		 15 46.106231689453125 16 44.211158752441406 17 43.208671569824219 18 43.642719268798828
		 19 44.390937805175781 20 45.325046539306641 21 46.316768646240234 22 47.237823486328125
		 23 47.959941864013672 24 48.52020263671875 25 48.880859375 26 48.980815887451172
		 27 48.729244232177734 28 48.110877990722656 29 47.177947998046875 30 45.982681274414063;
createNode animCurveTL -n "Character1_Hips_translateZ";
	rename -uid "F6628A8D-4E79-5607-9541-6BA04872F75F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -1.5415304899215698 1 -1.6600425243377686
		 2 -1.7107394933700562 3 -1.6981834173202515 4 -1.5652090311050415 5 -1.3650516271591187
		 6 -1.1509468555450439 7 -0.97613018751144409 8 -0.8938373327255249 9 -0.89079666137695313
		 10 -0.9254487156867981 11 -1.0085971355438232 12 -1.1315734386444092 13 -1.2551320791244507
		 14 -1.3442116975784302 15 -1.3637504577636719 16 -1.0958577394485474 17 -0.83107089996337891
		 18 -0.81567615270614624 19 -0.81114155054092407 20 -0.81781864166259766 21 -0.83605927228927612
		 22 -0.86621570587158203 23 -0.90863913297653198 24 -0.96422886848449707 25 -1.0297724008560181
		 26 -1.0994552373886108 27 -1.1771683692932129 28 -1.2737159729003906 29 -1.393652081489563
		 30 -1.5415304899215698;
createNode animCurveTU -n "Character1_Hips_scaleX";
	rename -uid "80FBECD4-4454-823F-68EC-F2B1B3D12BCF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_Hips_scaleY";
	rename -uid "BA656516-4CF9-7DC0-D905-7EAF14565826";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_Hips_scaleZ";
	rename -uid "F2F49207-49EA-A6F5-13C9-0EB1C4FAC8A7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_Hips_rotateX";
	rename -uid "232A99E9-4924-EEB4-4F5C-E6A8674AC7AF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 19.890151977539063 1 20.290704727172852
		 2 20.740489959716797 3 21.125158309936523 4 21.355182647705078 5 21.48857307434082
		 6 21.621402740478516 7 21.889379501342773 8 22.462953567504883 9 23.755317687988281
		 10 25.649808883666992 11 27.598911285400391 12 29.755678176879886 13 32.187492370605469
		 14 34.149765014648438 15 34.869575500488281 16 34.119022369384766 17 32.661571502685547
		 18 30.785737991333004 19 28.471857070922852 20 25.948337554931641 21 23.449239730834961
		 22 21.212638854980469 23 19.481157302856445 24 18.384086608886719 25 17.94365119934082
		 26 17.803943634033203 27 18.069026947021484 28 18.802654266357422 29 19.569219589233398
		 30 19.890151977539063;
createNode animCurveTA -n "Character1_Hips_rotateY";
	rename -uid "9B4963AA-4C66-0EE5-5996-3A886EC2BE82";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 4.339287281036377 1 3.0652413368225098
		 2 1.0875967741012573 3 -1.1372976303100586 4 -3.6415736675262451 5 -6.2736444473266602
		 6 -8.8911209106445313 7 -11.365921974182129 8 -13.580889701843262 9 -15.547990798950197
		 10 -17.295703887939453 11 -18.753463745117188 12 -19.991870880126953 13 -20.982015609741211
		 14 -21.574640274047852 15 -21.695295333862305 16 -21.339656829833984 17 -20.474285125732422
		 18 -19.217931747436523 19 -17.781509399414063 20 -16.205593109130859 21 -14.505496978759767
		 22 -12.668839454650879 23 -10.660514831542969 24 -8.3177461624145508 25 -5.5906715393066406
		 26 -2.9366500377655029 27 -0.40375080704689026 28 1.969251871109009 29 3.7014856338500977
		 30 4.339287281036377;
createNode animCurveTA -n "Character1_Hips_rotateZ";
	rename -uid "B71CA6D8-4A0C-A7AE-4195-30AB97FD0384";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -5.0620083808898926 1 -3.034782886505127
		 2 -0.77037054300308228 3 1.7573837041854858 4 4.6388072967529297 5 7.5794987678527832
		 6 10.271600723266602 7 12.389933586120605 8 13.590790748596191 9 13.625195503234863
		 10 12.79707145690918 11 11.512495994567871 12 9.8128271102905273 13 7.6473984718322754
		 14 5.370002269744873 15 3.3654160499572754 16 1.6854548454284668 17 0.12716805934906006
		 18 -1.5238821506500244 19 -3.3741638660430908 20 -5.2559123039245605 21 -6.9883155822753906
		 22 -8.3771295547485352 23 -9.2133817672729492 24 -9.6247177124023437 25 -9.8410634994506836
		 26 -9.7434463500976562 27 -9.1091127395629883 28 -7.9615588188171387 29 -6.5209593772888184
		 30 -5.0620083808898926;
createNode animCurveTU -n "Character1_Hips_visibility";
	rename -uid "DEF214BE-4C67-DE18-207B-38BA3CA2698A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftUpLeg_translateX";
	rename -uid "9D9D66F7-49DD-FABD-4AB9-27AE24FAA3E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.596480369567871;
createNode animCurveTL -n "Character1_LeftUpLeg_translateY";
	rename -uid "1E216ACA-401B-2EC7-FB58-D99A5433595A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.315338134765625;
createNode animCurveTL -n "Character1_LeftUpLeg_translateZ";
	rename -uid "644BF866-4095-DDAE-7E45-59985F2BF3F2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.0558223724365234;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleX";
	rename -uid "2B587E9A-49CF-E58F-1EFE-B28618966BAF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleY";
	rename -uid "AB3B8A79-4DA4-B2EF-81CA-AFAD7AC705BE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftUpLeg_scaleZ";
	rename -uid "9B3C27AB-4797-075F-45F6-BBAD880F5CC3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateX";
	rename -uid "B8E4E20F-425C-4641-F5CA-55AF59F0223B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 26.427566528320312 1 28.218149185180664
		 2 32.491043090820313 3 36.523265838623047 4 37.463901519775391 5 35.171379089355469
		 6 30.950719833374023 7 26.469701766967773 8 23.408750534057617 9 22.400684356689453
		 10 23.047168731689453 11 24.840463638305664 12 26.913778305053711 13 28.6278076171875
		 14 29.786342620849609 15 29.74940299987793 16 33.167140960693359 17 38.045955657958984
		 18 40.334865570068359 19 42.88287353515625 20 45.5076904296875 21 47.981834411621094
		 22 49.976406097412109 23 51.032985687255859 24 50.275287628173828 25 47.682952880859375
		 26 43.942108154296875 27 39.297233581542969 28 34.236976623535156 29 29.729368209838867
		 30 26.427566528320312;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateY";
	rename -uid "10CA53E1-43F9-1A7C-2C23-70B1DF6844DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 8.334019660949707 1 9.2362842559814453
		 2 7.1946187019348145 3 4.4895725250244141 4 6.0713095664978027 5 9.9168510437011719
		 6 13.075976371765137 7 15.324209213256838 8 17.225242614746094 9 18.404581069946289
		 10 18.346450805664063 11 17.155384063720703 12 15.35860538482666 13 13.812535285949707
		 14 13.430387496948242 15 15.641360282897951 16 21.528097152709961 17 26.248653411865234
		 18 27.272668838500977 19 28.107912063598633 20 28.571399688720703 21 28.389093399047852
		 22 27.28187370300293 23 25.089057922363281 24 22.084692001342773 25 19.467060089111328
		 26 17.384309768676758 27 15.66190242767334 28 13.851540565490723 29 11.443411827087402
		 30 8.334019660949707;
createNode animCurveTA -n "Character1_LeftUpLeg_rotateZ";
	rename -uid "81EE743F-4DCC-D5A5-F97C-55A089F720DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 2.6358358860015869 1 5.3510246276855469
		 2 12.950237274169922 3 18.48661994934082 4 14.875378608703613 5 5.0179901123046875
		 6 -7.6694755554199219 7 -21.515167236328125 8 -33.681900024414063 9 -42.948551177978516
		 10 -49.368667602539063 11 -52.131061553955078 12 -52.181167602539062 13 -50.995536804199219
		 14 -50.124984741210938 15 -52.043052673339844 16 -57.032566070556641 17 -58.390228271484382
		 18 -54.580005645751953 19 -48.950653076171875 20 -42.047454833984375 21 -34.495841979980469
		 22 -27.006155014038086 23 -20.352910995483398 24 -14.833191871643068 25 -10.767780303955078
		 26 -7.4754109382629395 27 -5.1678814888000488 28 -3.6228961944580078 29 -1.4203797578811646
		 30 2.6358358860015869;
createNode animCurveTU -n "Character1_LeftUpLeg_visibility";
	rename -uid "34B3C277-4BF5-0430-5629-20999920D39A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftLeg_translateX";
	rename -uid "0D4F363A-4EC8-658F-DBDB-0598F48D92FD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -21.532840728759766;
createNode animCurveTL -n "Character1_LeftLeg_translateY";
	rename -uid "130DADF4-4120-1568-A88A-4B82C20194F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4219808578491211;
createNode animCurveTL -n "Character1_LeftLeg_translateZ";
	rename -uid "2B876031-4F70-8591-2D77-F988CC781A5C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.8661990165710449;
createNode animCurveTU -n "Character1_LeftLeg_scaleX";
	rename -uid "FCFFBCAF-4F9A-46CC-494B-FD91AC8B76E5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftLeg_scaleY";
	rename -uid "384E3BB8-4C1C-B1DF-3A34-2A9DA3B3F379";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftLeg_scaleZ";
	rename -uid "4BA2B0BE-4C93-0267-C37F-8BB84748F79E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftLeg_rotateX";
	rename -uid "6486B5FF-4098-1631-6C03-A6BB68DB941A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 11.270273208618164 1 14.282470703125 2 17.661659240722656
		 3 21.83428955078125 4 27.751609802246094 5 38.176891326904297 6 54.470226287841797
		 7 71.767837524414063 8 79.952583312988281 9 74.949310302734375 10 59.302349090576172
		 11 40.422035217285156 12 25.24571418762207 13 15.69028854370117 14 11.40187931060791
		 15 11.750368118286133 16 24.764318466186523 17 36.120048522949219 18 38.645076751708984
		 19 40.534141540527344 20 41.74566650390625 21 42.244850158691406 22 42.0579833984375
		 23 41.283481597900391 24 39.234947204589844 25 36.038105010986328 26 31.806947708129886
		 27 26.782321929931641 28 21.454351425170898 29 16.24040412902832 30 11.270273208618164;
createNode animCurveTA -n "Character1_LeftLeg_rotateY";
	rename -uid "0C437F4F-4A36-711A-B397-AAA308DED59A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -11.852363586425781 1 -20.664712905883789
		 2 -20.229364395141602 3 -18.745038986206055 4 -29.385879516601563 5 -44.870708465576172
		 6 -55.976085662841797 7 -60.453720092773445 8 -61.013923645019531 9 -60.639137268066399
		 10 -58.913227081298828 11 -53.717887878417969 12 -44.85601806640625 13 -34.573696136474609
		 14 -26.914741516113281 15 -27.464954376220703 16 -35.688484191894531 17 -38.834964752197266
		 18 -37.734146118164062 19 -36.261955261230469 20 -34.327827453613281 21 -31.733678817749023
		 22 -28.296693801879883 23 -24.065549850463867 24 -19.322490692138672 25 -15.616414070129395
		 26 -13.33946704864502 27 -12.91681957244873 28 -13.599429130554199 29 -13.619974136352539
		 30 -11.852363586425781;
createNode animCurveTA -n "Character1_LeftLeg_rotateZ";
	rename -uid "EA491291-4744-A173-E7D8-05821E460BF5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -7.730980396270752 1 -14.30218505859375
		 2 -12.375164031982422 3 -10.03801441192627 4 -19.378679275512695 5 -37.858371734619141
		 6 -62.041740417480469 7 -85.593948364257813 8 -97.861366271972656 9 -94.980308532714844
		 10 -79.5625 11 -59.15571594238282 12 -41.051353454589844 13 -27.693458557128906 14 -19.852817535400391
		 15 -20.304134368896484 16 -30.619924545288086 17 -38.690681457519531 18 -37.917049407958984
		 19 -35.784805297851563 20 -32.214710235595703 21 -27.308967590332031 22 -21.413970947265625
		 23 -15.153669357299805 24 -9.2264804840087891 25 -5.2008552551269531 26 -3.1409304141998291
		 27 -3.2140128612518311 28 -4.8041043281555176 29 -6.634345531463623 30 -7.730980396270752;
createNode animCurveTU -n "Character1_LeftLeg_visibility";
	rename -uid "EA8E77FB-44C3-3E32-46BA-1E96EC8D0197";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFoot_translateX";
	rename -uid "C0C1D85C-4BAA-D7BD-4EC4-23B56212DF95";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 18.368806838989258;
createNode animCurveTL -n "Character1_LeftFoot_translateY";
	rename -uid "C875DE48-46BD-F667-19F1-F09873ACE17D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.7176852226257324;
createNode animCurveTL -n "Character1_LeftFoot_translateZ";
	rename -uid "28A93CA8-44C3-6ED2-6E8A-6486635D9505";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3830562829971313;
createNode animCurveTU -n "Character1_LeftFoot_scaleX";
	rename -uid "6D420738-4E2D-978D-CCC4-6A93C0A9AE3F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleY";
	rename -uid "2BF98D67-4F62-494B-A06A-B8AB3F2BB59E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftFoot_scaleZ";
	rename -uid "075262FD-4309-F38A-A8FE-178412E0151F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftFoot_rotateX";
	rename -uid "DE194CB0-4B6A-9469-F572-D89690DAB3B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 0.54025465250015259 1 -1.2603027820587158
		 2 -5.2335400581359863 3 -8.1712465286254883 4 -7.5667037963867187 5 -5.3574929237365723
		 6 -3.9369909763336182 7 -3.8677830696105961 8 -4.3593697547912598 9 -5.6665396690368652
		 10 -7.9923977851867676 11 -9.2450666427612305 12 -7.1465344429016113 13 -1.9983438253402708
		 14 3.0858292579650879 15 6.3316788673400879 16 8.539794921875 17 9.3514022827148437
		 18 9.961482048034668 19 10.329251289367676 20 10.391613006591797 21 10.064774513244629
		 22 9.3079376220703125 23 8.2156744003295898 24 6.6727862358093262 25 5.2301959991455078
		 26 4.1600260734558105 27 3.6367459297180171 28 3.2613024711608887 29 2.3217589855194092
		 30 0.54025465250015259;
createNode animCurveTA -n "Character1_LeftFoot_rotateY";
	rename -uid "F290549D-4F86-6137-0727-56806EA2E6F9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -16.980564117431641 1 -17.914054870605469
		 2 -12.931707382202148 3 -4.7788815498352051 4 -0.27744483947753906 5 3.1511783599853516
		 6 7.4123778343200684 7 12.623355865478516 8 18.313322067260742 9 20.761825561523437
		 10 16.545162200927734 11 4.3069453239440918 12 -12.729456901550293 13 -26.374233245849609
		 14 -32.077735900878906 15 -31.252264022827152 16 -21.041145324707031 17 -9.5928325653076172
		 18 -9.6393756866455078 19 -10.044858932495117 20 -10.696529388427734 21 -11.528142929077148
		 22 -12.49014949798584 23 -13.520936965942383 24 -14.060853958129883 25 -13.862766265869141
		 26 -13.416073799133301 27 -13.37312126159668 28 -14.089595794677734 29 -15.409241676330568
		 30 -16.980564117431641;
createNode animCurveTA -n "Character1_LeftFoot_rotateZ";
	rename -uid "314A4263-4A3E-509B-75B2-6DBB839D67EC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -6.1165485382080078 1 -5.1377077102661133
		 2 0.19128991663455963 3 5.9451498985290527 4 7.5991206169128427 5 8.2709083557128906
		 6 11.118075370788574 7 16.042606353759766 8 20.57257080078125 9 21.72645378112793
		 10 18.549411773681641 11 11.144848823547363 12 1.2001473903656006 13 -8.5897922515869141
		 14 -15.113916397094727 15 -17.549720764160156 16 -15.055090904235838 17 -11.448938369750977
		 18 -12.859878540039063 19 -14.338001251220703 20 -15.46335506439209 21 -15.93088436126709
		 22 -15.617679595947264 23 -14.64466667175293 24 -12.816308975219727 25 -10.888404846191406
		 26 -9.3371686935424805 27 -8.5602560043334961 28 -8.2315645217895508 29 -7.5372476577758798
		 30 -6.1165485382080078;
createNode animCurveTU -n "Character1_LeftFoot_visibility";
	rename -uid "497CFC09-4B24-5BEA-3FE8-9A83BBB9B997";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftToeBase_translateX";
	rename -uid "A9006BDC-40B2-65BA-D565-FC9E06A35ED7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3813290596008301;
createNode animCurveTL -n "Character1_LeftToeBase_translateY";
	rename -uid "6C709BEC-4D34-B204-BB53-459F63091F55";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.3898367881774902;
createNode animCurveTL -n "Character1_LeftToeBase_translateZ";
	rename -uid "7BEC4AF1-42CF-43EF-68EF-338AB6CD759F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2284431457519531;
createNode animCurveTU -n "Character1_LeftToeBase_scaleX";
	rename -uid "BCB4C8C6-44C7-F995-AE98-A1B29C4F8669";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftToeBase_scaleY";
	rename -uid "83E4B74E-4933-A750-8A82-2E930EE8FA5F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftToeBase_scaleZ";
	rename -uid "E7D57B64-4077-AFAD-9FCD-1EA3C2D4FE9F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftToeBase_rotateX";
	rename -uid "1015D905-4DC8-7860-A5F6-A4A72B12206F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 28 ".ktv[0:27]"  0 -13.545751571655273 1 -10.414163589477539
		 2 -7.4625349044799805 3 -4.2923116683959961 4 -0.27214702963829041 5 4.1952929496765137
		 6 8.1557064056396484 7 10.893276214599609 8 11.728524208068848 9 10.542023658752441
		 10 7.9755287170410147 11 4.3868169784545898 12 0.12754438817501068 13 -4.3577103614807129
		 14 -8.4259710311889648 15 -11.205355644226074 16 -5.6935501098632813 17 3.7473388836417598e-009
		 18 3.4673452997679988e-009 22 2.3265089854618282e-009 23 2.264669340945602e-009 24 -0.45214906334877014
		 25 -1.6898306608200073 26 -3.5495784282684326 27 -5.8689656257629395 28 -8.4650602340698242
		 29 -11.115349769592285 30 -13.545751571655273;
createNode animCurveTA -n "Character1_LeftToeBase_rotateY";
	rename -uid "A6C7CC63-42CD-EEE1-A4AD-E7AD4E9BFFFF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 28 ".ktv[0:27]"  0 15.544464111328125 1 12.542376518249512
		 2 9.4046621322631836 3 5.6770009994506836 4 0.38219419121742249 5 -6.2804303169250488
		 6 -12.867890357971191 7 -17.764858245849609 8 -19.308061599731445 9 -17.122343063354492
		 10 -12.554934501647949 11 -6.5845160484313965 12 -0.18016925454139709 13 5.7578072547912598
		 14 10.463079452514648 15 13.331663131713867 16 7.3718218803405771 17 2.7401234614643499e-009
		 18 2.5913442502911721e-009 22 1.8634758180979816e-009 23 1.7742907143514233e-009
		 24 0.63331907987594604 25 2.3239805698394775 26 4.7475957870483398 27 7.5786619186401367
		 28 10.505304336547852 29 13.242941856384277 30 15.544464111328125;
createNode animCurveTA -n "Character1_LeftToeBase_rotateZ";
	rename -uid "C90B0525-4C6D-3A52-66A1-75A3F9D66B8B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 28 ".ktv[0:27]"  0 -11.365211486816406 1 -8.6472988128662109
		 2 -6.1275391578674316 3 -3.4761319160461426 4 -0.21583074331283569 5 3.233330249786377
		 6 6.0869050025939941 7 7.9137763977050781 8 8.4429235458374023 9 7.6870808601379395
		 10 5.9621076583862305 11 3.3762118816375732 12 0.10094132274389267 13 -3.5302019119262695
		 14 -6.9450774192810059 15 -9.3300046920776367 16 -4.6403436660766602 17 -3.4111322655405729e-009
		 18 -3.4508176316450086e-009 22 -3.5357772265598446e-009 23 -3.5699840861269654e-009
		 24 -0.35900145769119263 25 -1.3508505821228027 26 -2.8644771575927734 27 -4.7869563102722168
		 28 -6.9783267974853516 29 -9.252171516418457 30 -11.365211486816406;
createNode animCurveTU -n "Character1_LeftToeBase_visibility";
	rename -uid "E8B6BD12-44A4-25A1-78AE-A4BA23E27019";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateX";
	rename -uid "3C3BBC31-4F48-0A9A-58AB-088CB774011B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.1971817016601563;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateY";
	rename -uid "5422258D-4A85-6724-A76A-7AB09FCA4704";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.7569773197174072;
createNode animCurveTL -n "Character1_LeftFootMiddle1_translateZ";
	rename -uid "44D2AB9D-4005-E5DA-BD5D-FAA6E29A6945";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4053930044174194;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleX";
	rename -uid "4720CA7F-4C69-CDE4-CEAE-83B822115425";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleY";
	rename -uid "7A1B18D2-4357-2A25-7345-DFB618FB5650";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999958276748657;
createNode animCurveTU -n "Character1_LeftFootMiddle1_scaleZ";
	rename -uid "38926AEB-4E2A-AE5C-2697-DEAC995BA984";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateX";
	rename -uid "C8A104F4-4EE0-3A57-DBDA-BFB2A4616235";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.3482278040160054e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateY";
	rename -uid "D149FA3A-43A8-B324-860C-889DAD591BC1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3084017747597727e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle1_rotateZ";
	rename -uid "FC46F881-48B1-A21D-8B4D-BA923A3D89DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.8756071119603348e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle1_visibility";
	rename -uid "CE02C793-41C7-86B0-DDAF-F8896BCECE9D";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateX";
	rename -uid "E0985E46-4B45-C0B9-3CE6-E8B203AB2A26";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8763875961303711;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateY";
	rename -uid "80DDB081-45CB-7CD7-EBAF-43BF0A8723EA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.1604456901550293;
createNode animCurveTL -n "Character1_LeftFootMiddle2_translateZ";
	rename -uid "6F99383C-47E3-97A8-788F-6484BF7FD0A3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.313550591468811;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleX";
	rename -uid "C895373D-4182-8BC4-CBF8-6587D1E52AD5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleY";
	rename -uid "FDF0E40F-48CE-1894-0607-4FB6450EBD37";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftFootMiddle2_scaleZ";
	rename -uid "8EA1AA78-4670-EB54-FA3E-F78B9E289FEE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateX";
	rename -uid "14091EFE-4F6C-355C-9A22-0DBA3A1D8B16";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0487360313504723e-008;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateY";
	rename -uid "0E4FCE8E-4951-C3FF-A04F-51A00CF82C6A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.4944857290876143e-009;
createNode animCurveTA -n "Character1_LeftFootMiddle2_rotateZ";
	rename -uid "8662C321-4348-9073-475E-9190E8F988A3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.4572278896976059e-009;
createNode animCurveTU -n "Character1_LeftFootMiddle2_visibility";
	rename -uid "F1005FC5-4493-84FD-BD77-AEB28C8E04CA";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightUpLeg_translateX";
	rename -uid "1FA85595-48F7-58C2-916F-468D4CA823FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.4649467468261719;
createNode animCurveTL -n "Character1_RightUpLeg_translateY";
	rename -uid "F2AB6E1A-47DA-3DF0-9E19-46A9B0414121";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.110023021697998;
createNode animCurveTL -n "Character1_RightUpLeg_translateZ";
	rename -uid "D9762581-4507-A60E-781B-B9B23E175B5B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.4359440803527832;
createNode animCurveTU -n "Character1_RightUpLeg_scaleX";
	rename -uid "4E309CC6-4948-9BE2-5EE6-9CBDE38FA06B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightUpLeg_scaleY";
	rename -uid "100839EA-4330-ED09-59C0-8B8E07CBBC3F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightUpLeg_scaleZ";
	rename -uid "7525452E-4CD3-DCA5-37A0-8B9BD2E50B16";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_RightUpLeg_rotateX";
	rename -uid "3657203D-4120-EEB7-977F-8D9EF841509A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 50.392391204833984 1 57.397510528564453
		 2 60.132240295410149 3 58.226894378662109 4 54.625553131103516 5 50.051670074462891
		 6 45.104324340820312 7 40.241352081298828 8 35.827007293701172 9 31.818590164184567
		 10 28.010322570800781 11 24.71131706237793 12 22.7645263671875 13 22.215852737426758
		 14 22.095380783081055 15 21.136091232299805 16 17.893880844116211 17 11.786968231201172
		 18 6.6117024421691895 19 7.2645149230957031 20 13.673269271850586 21 23.240079879760742
		 22 34.150688171386719 23 43.287681579589844 24 48.403041839599609 25 51.925102233886719
		 26 53.606990814208984 27 54.184108734130859 28 54.237636566162109 29 53.304840087890625
		 30 50.392391204833984;
createNode animCurveTA -n "Character1_RightUpLeg_rotateY";
	rename -uid "2DAD8505-490F-77B5-96DB-7DA997D7CC97";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 22.855892181396484 1 28.815505981445309
		 2 32.667819976806641 3 31.84055328369141 4 29.620750427246097 5 26.213836669921875
		 6 22.116586685180664 7 18.053409576416016 8 14.812981605529787 9 12.337652206420898
		 10 10.152641296386719 11 8.2789974212646484 12 6.5464587211608887 13 4.8317513465881348
		 14 3.1105549335479736 15 1.2367689609527588 16 -0.72401636838912964 17 -1.508159875869751
		 18 0.29532021284103394 19 3.8393318653106694 20 7.857537269592286 21 11.936176300048828
		 22 15.459860801696779 23 17.678424835205078 24 19.075038909912109 25 19.979297637939453
		 26 20.577316284179687 27 21.063205718994141 28 21.564363479614258 29 22.160346984863281
		 30 22.855892181396484;
createNode animCurveTA -n "Character1_RightUpLeg_rotateZ";
	rename -uid "29406CC9-4614-D5C1-B507-6A80DAF35B7B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 15.725654602050781 1 16.179832458496094
		 2 15.369746208190918 3 11.109720230102539 4 5.7278537750244141 5 -0.023629821836948395
		 6 -5.4284505844116211 7 -9.9422636032104492 8 -13.197273254394531 9 -15.086696624755858
		 10 -16.188760757446289 11 -17.050060272216797 12 -17.704185485839844 13 -18.133817672729492
		 14 -18.721343994140625 15 -19.692316055297852 16 -21.068464279174805 17 -21.839759826660156
		 18 -22.200353622436523 19 -22.537746429443359 20 -21.794918060302734 21 -19.129665374755859
		 22 -14.345494270324707 23 -8.9963541030883789 24 -3.8906674385070801 25 0.82413452863693237
		 26 4.8123884201049805 27 8.0078029632568359 28 10.662038803100586 29 13.086943626403809
		 30 15.725654602050781;
createNode animCurveTU -n "Character1_RightUpLeg_visibility";
	rename -uid "D5B0B741-4DAD-3D5C-53FD-AD821919F6AD";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightLeg_translateX";
	rename -uid "136336C9-4A30-01B8-3499-86B6FF605D55";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3108952045440674;
createNode animCurveTL -n "Character1_RightLeg_translateY";
	rename -uid "54A5C755-438D-DFD5-AC26-DDA1F58BCAB5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 22.181583404541016;
createNode animCurveTL -n "Character1_RightLeg_translateZ";
	rename -uid "BD2AA569-4462-B166-AB27-9090CEA3F6FB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.6536058187484741;
createNode animCurveTU -n "Character1_RightLeg_scaleX";
	rename -uid "E8FE4619-4A10-E875-7F96-86A54EB7BEBB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightLeg_scaleY";
	rename -uid "A588EFDB-41A9-4AE3-DB43-5597AD40EF03";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightLeg_scaleZ";
	rename -uid "29EC7AB8-4933-13B6-35F7-CE87707F6528";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightLeg_rotateX";
	rename -uid "3C120B24-4C9F-3FE2-1CD1-409D9894E764";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -6.0223665237426758 1 5.3855314254760742
		 2 12.286943435668945 3 14.678658485412596 4 14.792125701904297 5 13.18431282043457
		 6 10.377182960510254 7 7.0127310752868652 8 3.7945845127105713 9 0.44946968555450434
		 10 -2.7996768951416016 11 -4.6267905235290527 12 -4.1996712684631348 13 -1.5002434253692627
		 14 3.4503867626190186 15 10.328817367553711 16 19.182256698608398 17 23.524988174438477
		 18 23.744487762451172 19 31.570032119750977 20 47.677494049072266 21 63.895668029785156
		 22 75.334220886230469 23 79.980819702148438 24 76.229248046875 25 66.81793212890625
		 26 53.079158782958984 27 38.011211395263672 28 23.622106552124023 29 9.6190280914306641
		 30 -6.0223665237426758;
createNode animCurveTA -n "Character1_RightLeg_rotateY";
	rename -uid "7C4DA17D-4E60-3F1B-D870-F9927511AF03";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 13.063023567199707 1 20.632251739501953
		 2 23.689708709716797 3 25.743297576904297 4 27.461143493652344 5 28.805021286010739
		 6 29.478010177612305 7 29.217071533203125 8 28.092329025268555 9 25.985013961791992
		 10 23.10661506652832 11 20.7918701171875 12 20.297628402709961 13 21.876144409179688
		 14 25.344705581665039 15 30.170450210571286 16 36.202495574951172 17 38.549205780029297
		 18 37.809028625488281 19 38.438953399658203 20 38.540035247802734 21 36.831378936767578
		 22 35.794475555419922 23 37.008445739746094 24 40.647354125976563 25 43.137409210205078
		 26 42.940475463867188 27 39.296699523925781 28 32.657215118408203 29 23.805431365966797
		 30 13.063023567199707;
createNode animCurveTA -n "Character1_RightLeg_rotateZ";
	rename -uid "D16BC39D-478C-FD85-CE36-E3BC1BF0D908";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 35.508026123046875 1 48.694232940673828
		 2 56.285728454589844 3 58.164531707763665 4 57.566322326660156 5 54.841068267822266
		 6 50.603424072265625 7 45.876956939697266 8 41.953018188476563 9 38.619277954101563
		 10 35.390129089355469 11 33.013797760009766 12 32.000198364257813 13 32.354175567626953
		 14 33.978240966796875 15 36.679096221923828 16 42.728240966796875 17 48.35760498046875
		 18 51.459255218505859 19 60.390659332275391 20 75.252700805664063 21 88.244209289550781
		 22 95.9388427734375 23 97.701126098632813 24 92.09844970703125 25 82.107505798339844
		 26 69.19708251953125 27 56.592727661132813 28 46.711101531982422 29 39.977108001708984
		 30 35.508026123046875;
createNode animCurveTU -n "Character1_RightLeg_visibility";
	rename -uid "3215465D-46D0-019C-DB76-0DAE05139E55";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFoot_translateX";
	rename -uid "DB2F1309-4DE1-C026-AB66-D2B41AE24507";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.668137550354004;
createNode animCurveTL -n "Character1_RightFoot_translateY";
	rename -uid "915B6148-4259-2A32-175E-AE83E155BBFB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3660926818847656;
createNode animCurveTL -n "Character1_RightFoot_translateZ";
	rename -uid "8A1C440F-4B24-BB60-46BA-0C95634D7AB1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -15.681424140930176;
createNode animCurveTU -n "Character1_RightFoot_scaleX";
	rename -uid "E8C04FD7-4250-D169-D7F6-2F8373E137FA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightFoot_scaleY";
	rename -uid "F5D878C4-461C-ACAF-AEFB-C08A9A6C9F57";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999958276748657;
createNode animCurveTU -n "Character1_RightFoot_scaleZ";
	rename -uid "0BE7D9C5-487D-9CC3-8F87-2B8C7DB259F0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFoot_rotateX";
	rename -uid "FDE58DEA-4ED1-8B61-1BEF-D384C16BCE51";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 2.457329273223877 1 13.301854133605957
		 2 19.951501846313477 3 18.210594177246094 4 15.194838523864744 5 11.360392570495605
		 6 7.3084497451782227 7 3.706806898117065 8 1.1488531827926636 9 -0.48363879323005676
		 10 -1.7005172967910767 11 -2.680499792098999 12 -3.5044665336608887 13 -4.2606077194213867
		 14 -5.2338948249816895 15 -6.8255443572998047 16 -10.269380569458008 17 -12.466938972473145
		 18 -9.0036211013793945 19 -2.4952683448791504 20 4.2537245750427246 21 10.060159683227539
		 22 14.119125366210937 23 16.776193618774414 24 18.384391784667969 25 17.064496994018555
		 26 13.393070220947266 27 8.5809392929077148 28 4.6342020034790039 29 2.8061411380767822
		 30 2.457329273223877;
createNode animCurveTA -n "Character1_RightFoot_rotateY";
	rename -uid "37F7D20A-4607-DED3-B57A-9B8BB7A77A89";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -1.9004402160644533 1 -0.98211187124252319
		 2 -0.38280531764030457 3 0.058158922940492623 4 0.1917445957660675 5 0.060824926942586899
		 6 -0.19795913994312286 7 -0.42094323039054871 8 -0.5127752423286438 9 -0.50434613227844238
		 10 -0.45267295837402349 11 -0.45361730456352228 12 -0.58047634363174438 13 -0.82490521669387817
		 14 -1.1914836168289185 15 -1.6718676090240479 16 -2.6212098598480225 17 -2.325108528137207
		 18 0.28337466716766357 19 2.2378406524658203 20 2.5750045776367187 21 1.9622898101806643
		 22 0.97541803121566772 23 0.20858512818813324 24 0.3662382960319519 25 1.7082935571670532
		 26 3.256138801574707 27 3.6196553707122798 28 2.2906475067138672 29 0.12111923098564148
		 30 -1.9004402160644533;
createNode animCurveTA -n "Character1_RightFoot_rotateZ";
	rename -uid "8372537F-4BA3-DEA7-E4D0-AD8C17EE493F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -19.36988639831543 1 -12.272727012634277
		 2 -8.272369384765625 3 -12.385130882263184 4 -15.717833518981932 5 -18.288631439208984
		 6 -20.008445739746094 7 -20.853218078613281 8 -20.960046768188477 9 -20.29401969909668
		 10 -19.021800994873047 11 -17.907672882080078 12 -17.534368515014648 13 -17.899723052978516
		 14 -18.903314590454102 15 -20.195047378540039 16 -22.662010192871094 17 -20.788076400756836
		 18 -11.906046867370605 19 -2.3471775054931641 20 4.7468600273132324 21 11.950068473815918
		 22 20.291488647460938 23 28.28803825378418 24 31.687711715698246 25 28.270462036132813
		 26 18.950403213500977 27 5.9582180976867676 28 -6.8826875686645508 29 -16.050064086914063
		 30 -19.36988639831543;
createNode animCurveTU -n "Character1_RightFoot_visibility";
	rename -uid "7D238A1D-47A6-1132-D4B5-749748762B67";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightToeBase_translateX";
	rename -uid "0F840FF3-4E78-4AC6-7BBA-1CAB0CCE9C4E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.103175163269043;
createNode animCurveTL -n "Character1_RightToeBase_translateY";
	rename -uid "183EC239-4F18-C3E3-4DB3-A0BDB4C56C85";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.7509346008300781;
createNode animCurveTL -n "Character1_RightToeBase_translateZ";
	rename -uid "E7E72C4E-4804-BF31-B10A-3BBBF454DF6F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.6768350601196289;
createNode animCurveTU -n "Character1_RightToeBase_scaleX";
	rename -uid "DA3B1714-48EA-48D7-512D-31AAF11DBC5F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightToeBase_scaleY";
	rename -uid "32BE92A0-4600-518F-E7D3-29A29DB20DBF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightToeBase_scaleZ";
	rename -uid "B4968802-42E1-4698-2676-0CA3641E908C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightToeBase_rotateX";
	rename -uid "6F8321E2-423A-82A0-E21D-0C8397759B04";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 28 ".ktv[0:27]"  0 1.2876406908035278 1 0.29954695701599121
		 2 4.9177608651973514e-009 3 4.8079775716303175e-009 7 3.7538687713833951e-009 8 3.5987943736159873e-009
		 9 0.047495793551206589 10 0.22089585661888123 11 0.58285963535308838 12 1.1597820520401001
		 13 1.8953704833984373 14 2.6331086158752441 15 3.1341214179992676 16 2.9131808280944824
		 17 2.0308549404144287 18 1.0722237825393677 19 0.22911827266216275 20 -0.077116012573242188
		 21 0.45935177803039556 22 1.4078867435455322 23 1.7879172563552856 24 1.3752535581588745
		 25 0.76042670011520386 26 0.20837569236755371 27 -0.073020152747631073 28 0.027900300920009613
		 29 0.51581168174743652 30 1.2876406908035278;
createNode animCurveTA -n "Character1_RightToeBase_rotateY";
	rename -uid "2C399323-43F6-DCA4-83BB-F5B8AB5DE093";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 28 ".ktv[0:27]"  0 10.245872497558594 1 3.8635747432708736
		 2 6.7988570329191589e-010 3 7.6050959973983367e-010 7 1.50458046022095e-009 8 1.5414541865155229e-009
		 9 0.85299175977706909 10 3.0771279335021973 11 6.1655230522155762 12 9.6084203720092773
		 13 12.903133392333984 14 15.566475868225098 15 17.139213562011719 16 16.465152740478516
		 17 13.430422782897949 18 9.1512460708618164 19 3.1640348434448242 20 -4.551018238067627
		 21 -12.078838348388672 22 -17.523527145385742 23 -19.126211166381836 24 -17.375608444213867
		 25 -14.147603034973145 26 -9.8175210952758789 27 -4.7856183052062988 28 0.52343255281448364
		 29 5.6752839088439941 30 10.245872497558594;
createNode animCurveTA -n "Character1_RightToeBase_rotateZ";
	rename -uid "38C72570-40E2-279B-F2E3-BD9B7908F8BD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 28 ".ktv[0:27]"  0 8.615880012512207 1 3.1903414726257324
		 2 2.5806265457894995e-010 3 2.3055006515004806e-010 7 4.3636813606973135e-010 8 4.5584583330260392e-010
		 9 0.70112621784210205 10 2.5372722148895264 11 5.1177816390991211 12 8.0608510971069336
		 13 10.972080230712891 14 13.416102409362793 15 14.906269073486328 16 14.262908935546875
		 17 11.448795318603516 18 7.6649494171142578 19 2.6093306541442871 20 -3.733089923858643
		 21 -10.012874603271484 22 -14.789046287536621 23 -16.25616455078125 24 -14.655269622802734
		 25 -11.795317649841309 26 -8.0990333557128906 27 -3.925883531570435 28 0.43009436130523682
		 29 4.7050380706787109 30 8.615880012512207;
createNode animCurveTU -n "Character1_RightToeBase_visibility";
	rename -uid "0F2770DE-45B6-FC3F-AC4A-1695E4285BBC";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateX";
	rename -uid "7FB22E80-4C6B-528D-608C-7CB9ADB838B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.31789970397949219;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateY";
	rename -uid "EE4BEC19-473A-6D28-56F1-51AA8B167183";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.6937241554260254;
createNode animCurveTL -n "Character1_RightFootMiddle1_translateZ";
	rename -uid "0E63C78D-45B3-96BE-04D6-A7B6649D4436";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.4678492546081543;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleX";
	rename -uid "8AAB44D8-4614-839D-6D18-4E95218D75AD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleY";
	rename -uid "7D68870B-4B2F-BC8B-DB9C-8AAFC8D0C120";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightFootMiddle1_scaleZ";
	rename -uid "AD9533CC-41A1-DA7C-DB1F-5A921AEA1FDF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateX";
	rename -uid "067220C9-4628-C5CF-F005-A18B54114658";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.4056152321588789e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateY";
	rename -uid "53CC39A7-4D7E-F2B0-4182-238D2E05E5CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.2029434921839766e-009;
createNode animCurveTA -n "Character1_RightFootMiddle1_rotateZ";
	rename -uid "8FD3570E-4554-25E5-18AE-838A1F0B0AE0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.9063706692750202e-009;
createNode animCurveTU -n "Character1_RightFootMiddle1_visibility";
	rename -uid "3D7065CD-478B-AEA0-CF47-F5BCE7DF9B8B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateX";
	rename -uid "FE114390-4D70-0B5E-5A80-6DB5069CBB71";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3449039459228516;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateY";
	rename -uid "A80BCDD0-4CA1-6ACA-F88C-C4849E07C884";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.3579239845275879;
createNode animCurveTL -n "Character1_RightFootMiddle2_translateZ";
	rename -uid "19F2EB7F-4700-D639-6B34-44A6725D2474";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.7014765739440918;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleX";
	rename -uid "9635E4D8-4E36-0765-726C-55ABC40226E1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleY";
	rename -uid "B355783D-47FB-96B4-0951-3D85B2B73BC4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightFootMiddle2_scaleZ";
	rename -uid "A868F540-49FA-1657-6806-9B959D31B8D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateX";
	rename -uid "EAFBE388-4E24-44E1-BCA0-04A7DC850C11";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.7003165098117279e-008;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateY";
	rename -uid "542E4903-400F-A463-4B52-E6BD7BAE3563";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.6009248088598724e-009;
createNode animCurveTA -n "Character1_RightFootMiddle2_rotateZ";
	rename -uid "0A1D6EC5-4D5B-9247-9832-F0898AAB3F3D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.5239896455909729e-008;
createNode animCurveTU -n "Character1_RightFootMiddle2_visibility";
	rename -uid "5ADB00E9-4AD0-D56B-A475-B7B2B4583A24";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Spine_translateX";
	rename -uid "2C0ABD4A-4045-8CC1-30C6-059EB25785CC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.9174466133117676;
createNode animCurveTL -n "Character1_Spine_translateY";
	rename -uid "1EFEAD9C-448A-749E-1794-6E9BAD29D0CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 13.894325256347656;
createNode animCurveTL -n "Character1_Spine_translateZ";
	rename -uid "67E9EE35-4C6D-2BB0-A616-7DA576C36E57";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.1938900947570801;
createNode animCurveTU -n "Character1_Spine_scaleX";
	rename -uid "1FBFB23B-4B47-F4A5-D9AD-71B5B6791524";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine_scaleY";
	rename -uid "BC9C75E8-4AFC-D35D-20BB-1BA19251F828";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_Spine_scaleZ";
	rename -uid "D904779E-4B5F-98FE-AE4F-CD82D63AB133";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_Spine_rotateX";
	rename -uid "ACE0352A-44D2-9F22-0306-239E2495F583";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -3.1129300594329834 1 -3.5954182147979736
		 2 -4.2233777046203613 3 -4.9235754013061523 4 -5.6310148239135742 5 -6.2897272109985352
		 6 -6.851468563079834 7 -7.2717823982238761 8 -7.5034871101379386 9 -7.4629116058349609
		 10 -7.1662211418151855 11 -6.7040262222290039 12 -6.1634111404418945 13 -5.6310763359069824
		 14 -5.195192813873291 15 -4.9460158348083496 16 -5.0029706954956055 17 -5.3487277030944824
		 18 -5.8751459121704102 19 -6.4830985069274902 20 -7.0833611488342285 21 -7.5945568084716797
		 22 -7.9380202293396005 23 -8.0301971435546875 24 -7.6746578216552725 25 -6.869657039642334
		 26 -5.8115344047546387 27 -4.6981730461120605 28 -3.7340693473815914 29 -3.1323025226593018
		 30 -3.1129300594329834;
createNode animCurveTA -n "Character1_Spine_rotateY";
	rename -uid "A87E5B38-4FF2-E697-96BF-9E8ECE6A89DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 9.5085439682006836 1 8.653071403503418
		 2 7.6318321228027344 3 6.4559288024902344 4 5.1523523330688477 5 3.7632060050964351
		 6 2.3421506881713867 7 0.94856166839599598 8 -0.36039832234382629 9 -1.6315542459487915
		 10 -2.883054256439209 11 -4.0232701301574707 12 -4.9533567428588867 13 -5.5590949058532715
		 14 -5.7054047584533691 15 -5.2332763671875 16 -4.3114180564880371 17 -3.2164771556854248
		 18 -1.952678918838501 19 -0.55038130283355713 20 0.93743932247161865 21 2.4416990280151367
		 22 3.8860793113708501 23 5.1979947090148926 24 6.3932900428771973 25 7.4992237091064453
		 26 8.470947265625 27 9.25225830078125 28 9.7646703720092773 29 9.8990592956542969
		 30 9.5085439682006836;
createNode animCurveTA -n "Character1_Spine_rotateZ";
	rename -uid "9AF958F0-430C-47CD-31D8-BBA56FC1EAB5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 0.23594455420970917 1 -2.2239885330200195
		 2 -4.7396445274353027 3 -7.1588578224182129 4 -9.3350200653076172 5 -11.128539085388184
		 6 -12.407183647155762 7 -13.045411109924316 8 -12.923111915588379 9 -12.123854637145996
		 10 -10.870988845825195 11 -9.2489681243896484 12 -7.3294224739074707 13 -5.1715521812438965
		 14 -2.8259193897247314 15 -0.34257954359054565 16 2.1788959503173828 17 4.6003937721252441
		 18 6.8108329772949219 19 8.7066593170166016 20 10.194140434265137 21 11.19035816192627
		 22 11.622751235961914 23 11.427518844604492 24 10.68403434753418 25 9.5706872940063477
		 26 8.1606483459472656 27 6.5023636817932129 28 4.6211495399475098 29 2.5276730060577393
		 30 0.23594455420970917;
createNode animCurveTU -n "Character1_Spine_visibility";
	rename -uid "0530BC9A-4AD2-3403-F841-328324496FD1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Spine1_translateX";
	rename -uid "38C34681-4CBC-B534-8BC8-248F5454A60D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.2515636682510376;
createNode animCurveTL -n "Character1_Spine1_translateY";
	rename -uid "A88C13BE-475C-B113-5979-A4A66B34EA7F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -15.818062782287598;
createNode animCurveTL -n "Character1_Spine1_translateZ";
	rename -uid "30AA5EF8-4712-E2B9-184A-E58540A973AD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.5716743469238281;
createNode animCurveTU -n "Character1_Spine1_scaleX";
	rename -uid "8228ABCB-4C5E-374B-1BE6-A98504C1BD09";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine1_scaleY";
	rename -uid "A59B895B-4316-5C2A-DB1A-25B1F3E6BD01";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Spine1_scaleZ";
	rename -uid "5C302DB2-443A-6821-7F84-F688D3B5B467";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_Spine1_rotateX";
	rename -uid "FAA6E6F9-4078-854D-48AF-BCBB8063C7AD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -24.86555290222168 1 -24.918699264526367
		 2 -24.732780456542969 3 -24.269046783447266 4 -23.51573371887207 5 -22.49110221862793
		 6 -21.243404388427734 7 -19.848747253417969 8 -18.409177780151367 9 -17.202064514160156
		 10 -16.476236343383789 11 -16.253955841064453 12 -16.481632232666016 13 -17.022981643676758
		 14 -17.659936904907227 15 -18.127157211303711 16 -18.35498046875 17 -18.423931121826172
		 18 -18.327108383178711 19 -18.093246459960938 20 -17.791912078857422 21 -17.535341262817383
		 22 -17.479364395141602 23 -17.828947067260742 24 -18.569814682006836 25 -19.566795349121094
		 26 -20.814258575439453 27 -22.206628799438477 28 -23.538688659667969 29 -24.524482727050781
		 30 -24.86555290222168;
createNode animCurveTA -n "Character1_Spine1_rotateY";
	rename -uid "04C3793B-4FFB-C3A0-AF37-2D8DE27919FE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 35.0308837890625 1 34.711536407470703
		 2 33.810813903808594 3 32.280139923095703 4 30.065439224243164 5 27.106441497802734
		 6 23.337499618530273 7 18.690158843994141 8 13.097606658935547 9 6.7669529914855957
		 10 0.22401848435401914 11 -6.1139616966247559 12 -11.841236114501953 13 -16.578456878662109
		 14 -19.981424331665039 15 -21.734947204589844 16 -22.193937301635742 17 -21.884065628051758
		 18 -20.748964309692383 19 -18.724555969238281 20 -15.738460540771484 21 -11.712135314941406
		 22 -6.5662698745727539 23 -0.22986501455307004 24 6.8316478729248047 25 13.862629890441895
		 26 20.471220016479492 27 26.263616561889648 28 30.859867095947269 29 33.899337768554688
		 30 35.0308837890625;
createNode animCurveTA -n "Character1_Spine1_rotateZ";
	rename -uid "B6A0C329-45FC-6976-E300-13821018F016";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -5.1863360404968262 1 -5.1567630767822266
		 2 -4.9884185791015625 3 -4.6097064018249512 4 -3.9649200439453121 5 -3.0136008262634277
		 6 -1.7254987955093384 7 -0.070982880890369415 8 1.9929535388946531 9 4.3047876358032227
		 10 6.6638736724853516 11 9.0690126419067383 12 11.471778869628906 13 13.737029075622559
		 14 15.626635551452637 15 16.826963424682617 16 17.466287612915039 17 17.813682556152344
		 18 17.771173477172852 19 17.266878128051758 20 16.256204605102539 21 14.715578079223635
		 22 12.627677917480469 23 9.9569568634033203 24 6.9609932899475098 25 4.0406365394592285
		 26 1.3052053451538086 27 -1.1456356048583984 28 -3.1780385971069336 29 -4.6006908416748047
		 30 -5.1863360404968262;
createNode animCurveTU -n "Character1_Spine1_visibility";
	rename -uid "D86B7749-45BB-E8E8-1A60-0981627A38EB";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftShoulder_translateX";
	rename -uid "7C6D5CB2-4925-AA80-9EEB-1D9466368DAB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.49134865403175354;
createNode animCurveTL -n "Character1_LeftShoulder_translateY";
	rename -uid "2FA9152E-4F3D-6B67-2F31-31BE9ECB0949";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.20281982421875;
createNode animCurveTL -n "Character1_LeftShoulder_translateZ";
	rename -uid "7641E823-44E9-0BCC-1D72-7E9FFC02C638";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.910428047180176;
createNode animCurveTU -n "Character1_LeftShoulder_scaleX";
	rename -uid "C08B4869-4CF4-E230-67D6-999DD3A8DB8E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftShoulder_scaleY";
	rename -uid "F72FAB77-46BA-FAB6-34AE-DEAD1CD81F69";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999934434890747;
createNode animCurveTU -n "Character1_LeftShoulder_scaleZ";
	rename -uid "B8CBCDD3-49CE-3A34-CAB9-17B0F59C2F69";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftShoulder_rotateX";
	rename -uid "ECDD49D5-472C-9386-560F-1CA3AFF792CC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 5.0149154663085938 1 3.906521081924438
		 2 2.6777975559234619 3 1.4408601522445679 4 0.29055386781692505 5 -0.69270777702331543
		 6 -1.438287615776062 7 -1.8801637887954712 8 -1.9516421556472776 9 -1.4918512105941772
		 10 -0.49428352713584905 11 0.8920937180519104 12 2.5172019004821777 13 4.208777904510498
		 14 5.7551636695861816 15 6.9009785652160645 16 7.6425113677978525 17 8.1852102279663086
		 18 8.5682191848754883 19 8.8355493545532227 20 9.0341911315917969 21 9.2127799987792969
		 22 9.4209728240966797 23 9.7096805572509766 24 9.4908523559570312 25 9.0338897705078125
		 26 8.3845529556274414 27 7.5935506820678711 28 6.7202577590942383 29 5.8342666625976562
		 30 5.0149154663085938;
createNode animCurveTA -n "Character1_LeftShoulder_rotateY";
	rename -uid "7FDDA4CA-4C35-438D-23AB-07A8CE389A1C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -7.2927737236022949 1 -5.2183084487915039
		 2 -2.8089802265167236 3 -0.24558226764202118 4 2.2817597389221191 5 4.5751223564147949
		 6 6.4312748908996582 7 7.6445565223693857 8 8.0100088119506836 9 7.1236939430236816
		 10 5.0028386116027832 11 2.0822303295135498 12 -1.2056392431259155 13 -4.4412541389465332
		 14 -7.2264342308044434 15 -9.1863422393798828 16 -10.416280746459961 17 -11.30926513671875
		 18 -11.947244644165039 19 -12.409433364868164 20 -12.773354530334473 21 -13.115585327148438
		 22 -13.512117385864258 23 -14.03826904296875 24 -13.630917549133301 25 -12.836365699768066
		 26 -11.781609535217285 27 -10.590549468994141 28 -9.3788080215454102 29 -8.2498102188110352
		 30 -7.2927737236022949;
createNode animCurveTA -n "Character1_LeftShoulder_rotateZ";
	rename -uid "2B53F0D0-40DC-856B-FB78-17BCB21AD1D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 0.32157677412033081 1 1.3456026315689087
		 2 2.3667800426483154 3 3.3444912433624268 4 4.2581567764282227 5 5.10491943359375
		 6 5.8942427635192871 7 6.6403279304504395 8 7.3525872230529785 9 8.0047521591186523
		 10 8.5563993453979492 11 8.979736328125 12 9.2652587890625 13 9.4454889297485352
		 14 9.6086311340332031 15 9.8971643447875977 16 10.356910705566406 17 10.914295196533203
		 18 11.554841995239258 19 12.257543563842773 20 12.997077941894531 21 13.745363235473633
		 22 14.472291946411135 23 15.14561176300049 24 14.131260871887207 25 12.463044166564941
		 26 10.310218811035156 27 7.8425097465515128 28 5.2336492538452148 29 2.6639885902404785
		 30 0.32157677412033081;
createNode animCurveTU -n "Character1_LeftShoulder_visibility";
	rename -uid "EEE26159-4FE5-CF30-EEE3-7CB14C3F9E12";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftArm_translateX";
	rename -uid "291BB8D9-418D-E74B-9A67-EDB78991E439";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.39077091217041;
createNode animCurveTL -n "Character1_LeftArm_translateY";
	rename -uid "011C12A0-4480-F640-8B8C-328DB07210B6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.8805270195007324;
createNode animCurveTL -n "Character1_LeftArm_translateZ";
	rename -uid "A276C38D-456E-5B0F-EF7C-9A9AE55AEE6E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8116312026977539;
createNode animCurveTU -n "Character1_LeftArm_scaleX";
	rename -uid "AAE91212-4119-F943-BDF8-5DBBF58744F6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999964237213135;
createNode animCurveTU -n "Character1_LeftArm_scaleY";
	rename -uid "93F71349-462D-5B7E-A476-049D3A2A7A71";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftArm_scaleZ";
	rename -uid "5216B5F4-414C-8329-12D1-9494279F5534";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTA -n "Character1_LeftArm_rotateX";
	rename -uid "345B77B2-44FE-4052-1DF0-78BA4DAFC7DE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 48.250389099121094 1 48.502517700195313
		 2 48.892635345458984 3 49.434410095214844 4 50.102584838867188 5 50.834518432617188
		 6 51.541023254394531 7 52.125518798828125 8 52.511432647705078 9 52.635196685791016
		 10 52.65277099609375 11 52.848125457763672 12 53.386642456054688 13 54.227367401123047
		 14 55.10986328125 15 55.620357513427734 16 55.576751708984375 17 55.119422912597656
		 18 54.305526733398438 19 53.212902069091797 20 51.924945831298828 21 50.519634246826172
		 22 49.062702178955078 23 47.605663299560547 24 47.033016204833984 25 46.803230285644531
		 26 46.848609924316406 27 47.094783782958984 28 47.463233947753906 29 47.8743896484375
		 30 48.250389099121094;
createNode animCurveTA -n "Character1_LeftArm_rotateY";
	rename -uid "DDF08A75-4759-773B-9202-CD88AA1C3DBC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 24.659021377563477 1 23.405101776123047
		 2 21.889141082763672 3 20.228294372558594 4 18.54823112487793 5 16.977725982666016
		 6 15.645389556884766 7 14.681547164916992 8 14.225810050964355 9 14.625885009765623
		 10 15.961675643920897 11 17.942232131958008 12 20.2144775390625 13 22.40643310546875
		 14 24.193264007568359 15 25.327653884887695 16 25.912683486938477 17 26.242481231689453
		 18 26.379680633544922 19 26.377141952514648 20 26.288637161254883 21 26.176284790039063
		 22 26.114101409912109 23 26.188034057617188 24 26.046728134155273 25 25.804197311401367
		 26 25.519855499267578 27 25.241907119750977 28 25.001369476318359 29 24.809572219848633
		 30 24.659021377563477;
createNode animCurveTA -n "Character1_LeftArm_rotateZ";
	rename -uid "970E8083-48D4-A72E-8944-D39249EFC07A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 17.74812126159668 1 16.25341796875 2 14.589195251464844
		 3 12.913667678833008 4 11.37325382232666 5 10.107773780822754 6 9.2564496994018555
		 7 8.9619235992431641 8 9.3709173202514648 9 10.81137752532959 10 13.255594253540039
		 11 16.328592300415039 12 19.667015075683594 13 22.895673751831055 14 25.595338821411133
		 15 27.294567108154297 16 27.999135971069336 17 28.109487533569336 18 27.730112075805664
		 19 26.971561431884766 20 25.946645736694336 21 24.767158508300781 22 23.541379928588867
		 23 22.372859954833984 24 21.470781326293945 25 20.644540786743164 26 19.899435043334961
		 27 19.238021850585937 28 18.660707473754883 29 18.165615081787109 30 17.74812126159668;
createNode animCurveTU -n "Character1_LeftArm_visibility";
	rename -uid "95866BA0-4075-77C9-D000-F68AFC0D51A8";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftForeArm_translateX";
	rename -uid "CB881DE5-4C6E-693B-A8F1-A6A41ED36066";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 8.9387187957763672;
createNode animCurveTL -n "Character1_LeftForeArm_translateY";
	rename -uid "A258CCBF-42D2-6FA3-E1CC-809DA2249E7F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -14.273021697998047;
createNode animCurveTL -n "Character1_LeftForeArm_translateZ";
	rename -uid "C8654E76-4E6E-4EBB-50CA-F3ACBDE089DA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.7470760345458984;
createNode animCurveTU -n "Character1_LeftForeArm_scaleX";
	rename -uid "744BC414-48AD-C085-60DD-389CC9331082";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftForeArm_scaleY";
	rename -uid "739B6EFD-471E-ECC0-9197-B1ABC69F9D57";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftForeArm_scaleZ";
	rename -uid "9C09E68C-4C82-40BB-0FBE-59AC8BEB0D91";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftForeArm_rotateX";
	rename -uid "670CB765-4702-F1DF-2E2C-7E9B046FF9B6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -11.949672698974609 1 -12.427806854248047
		 2 -12.882322311401367 3 -13.296604156494141 4 -13.674842834472656 5 -14.044057846069336
		 6 -14.450444221496584 7 -14.949185371398924 8 -15.587180137634277 9 -16.59034538269043
		 10 -17.951536178588867 11 -19.327888488769531 12 -20.411184310913086 13 -20.977180480957031
		 14 -20.894327163696289 15 -20.106401443481445 16 -18.69096565246582 17 -16.880527496337891
		 18 -14.889277458190918 19 -12.906167984008789 20 -11.07892894744873 21 -9.5042142868041992
		 22 -8.2251014709472656 23 -7.2345476150512695 24 -7.3112139701843271 25 -7.7312054634094229
		 26 -8.4030008316040039 27 -9.2552862167358398 28 -10.207841873168945 29 -11.153066635131836
		 30 -11.949672698974609;
createNode animCurveTA -n "Character1_LeftForeArm_rotateY";
	rename -uid "A4C37BA6-406B-4707-760A-23BD74B1DF8F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -15.424473762512207 1 -16.579776763916016
		 2 -17.817974090576172 3 -19.10051155090332 4 -20.381059646606445 5 -21.606462478637695
		 6 -22.718585968017578 7 -23.655916213989258 8 -24.354755401611328 9 -24.87199592590332
		 10 -25.244180679321289 11 -25.415945053100586 12 -25.385507583618164 13 -25.169153213500977
		 14 -24.745073318481445 15 -24.009967803955078 16 -22.828208923339844 17 -21.178556442260742
		 18 -19.075145721435547 19 -16.567184448242187 20 -13.744924545288086 21 -10.735747337341309
		 22 -7.6943740844726563 23 -4.7913236618041992 24 -4.6238341331481934 25 -5.4938616752624512
		 26 -7.1173357963562012 27 -9.2038679122924805 28 -11.466529846191406 29 -13.628293037414551
		 30 -15.424473762512207;
createNode animCurveTA -n "Character1_LeftForeArm_rotateZ";
	rename -uid "5AAEA05E-41A6-3F13-B160-D384E24B4E59";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -21.810270309448242 1 -22.625480651855469
		 2 -23.782268524169922 3 -25.078456878662109 4 -26.307628631591797 5 -27.257474899291992
		 6 -27.709579467773438 7 -27.441390991210938 8 -26.231245040893555 9 -23.302326202392578
		 10 -18.559707641601563 11 -12.830386161804199 12 -6.948946475982666 13 -1.7321760654449463
		 14 2.036207914352417 15 3.6112656593322749 16 3.0706586837768555 17 1.2283755540847778
		 18 -1.5317560434341431 19 -4.8489995002746582 20 -8.3970279693603516 21 -11.886406898498535
		 22 -15.058186531066895 23 -17.671808242797852 24 -18.955112457275391 25 -19.854808807373047
		 26 -20.458551406860352 27 -20.850839614868164 28 -21.126476287841797 29 -21.40007209777832
		 30 -21.810270309448242;
createNode animCurveTU -n "Character1_LeftForeArm_visibility";
	rename -uid "4467A5F5-427D-EC89-7F23-F390363B2796";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHand_translateX";
	rename -uid "7E683E39-40FE-5592-C934-0A92A6992551";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.95125424861907959;
createNode animCurveTL -n "Character1_LeftHand_translateY";
	rename -uid "30532F94-4645-D273-A884-1F8FA1B487E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -11.299518585205078;
createNode animCurveTL -n "Character1_LeftHand_translateZ";
	rename -uid "6CDB6B88-472A-F675-49CF-0D81B3854BC1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 12.872089385986328;
createNode animCurveTU -n "Character1_LeftHand_scaleX";
	rename -uid "5DE7F66A-4D1C-7944-2E3E-2984099FC5B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHand_scaleY";
	rename -uid "B5037EA0-40D2-A1A4-67E2-3BA86E3E5A85";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHand_scaleZ";
	rename -uid "BAF24DA8-402B-DCC5-B060-9B9559C1B82C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHand_rotateX";
	rename -uid "D47F0726-4925-1F9C-732A-FC95D899BBAE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -9.8823070526123047 1 -9.9819927215576172
		 2 -10.120012283325195 3 -10.276420593261719 4 -10.429781913757324 5 -10.557207107543945
		 6 -10.634706497192383 7 -10.637796401977539 8 -10.542301177978516 9 -10.275487899780273
		 10 -9.8338394165039062 11 -9.2984962463378906 12 -8.7478408813476563 13 -8.2541866302490234
		 14 -7.8824896812438965 15 -7.6921129226684579 16 -7.6735239028930664 17 -7.7616543769836435
		 18 -7.9322628974914551 19 -8.1616706848144531 20 -8.4259881973266602 21 -8.7006378173828125
		 22 -8.9601707458496094 23 -9.1783390045166016 24 -9.3139047622680664 25 -9.4283428192138672
		 26 -9.5272789001464844 27 -9.6164321899414062 28 -9.701533317565918 29 -9.7882728576660156
		 30 -9.8823070526123047;
createNode animCurveTA -n "Character1_LeftHand_rotateY";
	rename -uid "925FAB20-498C-DDC7-AE54-3D8AFD68EFCB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 0.083652190864086151 1 0.32367447018623352
		 2 0.59634989500045776 3 0.87872982025146484 4 1.149783730506897 5 1.3905680179595947
		 6 1.5838471651077271 7 1.7131251096725464 8 1.7610629796981812 9 1.68067467212677
		 10 1.4606777429580688 11 1.1360411643981934 12 0.74958586692810059 13 0.35647079348564148
		 14 0.023794254288077354 15 -0.17409045994281769 16 -0.23543573915958407 17 -0.21988584101200104
		 18 -0.14601832628250122 19 -0.033248506486415863 20 0.099378667771816254 21 0.23398110270500183
		 22 0.35422638058662415 23 0.44527861475944519 24 0.44002968072891235 25 0.40520146489143372
		 26 0.34950023889541626 27 0.28133180737495422 28 0.20892064273357391 29 0.14036525785923004
		 30 0.083652190864086151;
createNode animCurveTA -n "Character1_LeftHand_rotateZ";
	rename -uid "7A034606-43AB-E4AB-8EFF-FDADABA3667C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 4.4257907867431641 1 3.3573300838470459
		 2 2.1396389007568359 3 0.86134254932403564 4 -0.38846293091773987 5 -1.5201715230941772
		 6 -2.443751335144043 7 -3.0689518451690674 8 -3.3056387901306152 9 -2.9422357082366943
		 10 -1.9752200841903687 11 -0.62706965208053589 12 0.88102710247039795 13 2.3304150104522705
		 14 3.5052096843719482 15 4.1914029121398926 16 4.4203987121582031 17 4.4039707183837891
		 18 4.198615550994873 19 3.8606350421905513 20 3.4464099407196045 21 3.0125656127929687
		 22 2.6160423755645752 23 2.3140618801116943 24 2.4047904014587402 25 2.6241898536682129
		 26 2.9372870922088623 27 3.3090543746948242 28 3.7044382095336914 29 4.0883750915527344
		 30 4.4257907867431641;
createNode animCurveTU -n "Character1_LeftHand_visibility";
	rename -uid "AD84B67E-476C-5CE7-1772-15AAA88DCC28";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateX";
	rename -uid "D8DBB949-4808-1575-492E-86A224003E05";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.1041760444641113;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateY";
	rename -uid "3CACD3C1-434A-F215-2E75-B58B1A437ABE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.7491040229797363;
createNode animCurveTL -n "Character1_LeftHandThumb1_translateZ";
	rename -uid "5FCE2E29-4CC3-20A6-AFEB-69B9E67E99BA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.7276492118835449;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleX";
	rename -uid "8879B676-4AD8-F34F-F006-D5A3E8AA9F4B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleY";
	rename -uid "C54CA1F8-4BBE-428B-97F0-C39AF4991614";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandThumb1_scaleZ";
	rename -uid "2633161A-48DA-5DFF-6E02-A3830ADF8D05";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateX";
	rename -uid "D624BC99-493B-68E0-0638-1C802B1FE72F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 18.860912322998047 8 18.860912322998047
		 9 18.955039978027344 10 19.222812652587891 11 19.669767379760742 12 20.312005996704102
		 13 21.105663299560547 14 21.853622436523438 15 22.190587997436523 16 22.190587997436523
		 22 22.190587997436523 23 22.190587997436523 24 21.956947326660156 25 21.391433715820313
		 26 20.709609985351563 27 20.069820404052734 28 19.54554557800293 29 19.147739410400391
		 30 18.860912322998047;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateY";
	rename -uid "C52BEB4D-40B8-C23F-97CE-B786DF078575";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -5.9013795852661133 8 -5.9013795852661133
		 9 -4.4185523986816406 10 -0.51467406749725342 11 4.9952182769775391 12 11.197641372680664
		 13 17.043960571289063 14 21.394962310791016 15 23.095975875854492 16 23.095975875854492
		 22 23.095975875854492 23 23.095975875854492 24 21.931373596191406 25 18.813102722167969
		 26 14.322598457336426 27 9.0521469116210937 28 3.5795366764068604 29 -1.5612558126449585
		 30 -5.9013795852661133;
createNode animCurveTA -n "Character1_LeftHandThumb1_rotateZ";
	rename -uid "99124BC6-44A2-9BDF-7A43-2A973CDCBB49";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 62.457702636718743 8 62.457702636718743
		 9 61.750255584716804 10 60.061691284179688 11 58.077629089355469 12 56.365074157714844
		 13 55.241020202636719 14 54.720352172851563 15 54.593734741210938 16 54.593734741210938
		 22 54.593734741210938 23 54.593734741210938 24 54.675624847412109 25 54.995944976806641
		 26 55.704872131347656 27 56.896709442138672 28 58.544734954833977 29 60.490428924560547
		 30 62.457702636718743;
createNode animCurveTU -n "Character1_LeftHandThumb1_visibility";
	rename -uid "9A44B65A-4EFD-8F90-4B17-598E4BE97BDC";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateX";
	rename -uid "E3BF297A-4746-CA86-C9EE-F080F65B87F6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.3594832420349121;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateY";
	rename -uid "EFF89AF2-46F8-8D70-ED71-8791E73D12DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.8193249702453613;
createNode animCurveTL -n "Character1_LeftHandThumb2_translateZ";
	rename -uid "EF64211D-41D1-C6BC-FB84-0B970A43615C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.3286550045013428;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleX";
	rename -uid "E2DC014A-4EFD-954D-B8FB-F0A7966DB6BC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleY";
	rename -uid "13845CA2-455C-6773-408A-CF8C0C37B334";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandThumb2_scaleZ";
	rename -uid "CB5E660E-4A98-6ED3-2DAC-D7B36290B7BB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateX";
	rename -uid "C1FEDCC1-4B24-2C74-F71A-A7BA8CB2D39B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 53.687442779541016 8 53.687442779541016
		 9 51.292129516601563 10 44.954128265380859 11 35.877296447753906 12 25.436880111694336
		 13 15.42853260040283 14 7.9512825012207031 15 5.0391688346862793 16 5.0391688346862793
		 22 5.0391688346862793 23 5.0391688346862793 24 7.0317964553833008 25 12.386384010314941
		 26 20.100553512573242 27 29.073961257934567 28 38.226947784423828 29 46.659305572509766
		 30 53.687442779541016;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateY";
	rename -uid "C6DBA010-45C3-48E3-AB62-DAB66F111A16";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -14.673751831054688 8 -14.673751831054688
		 9 -15.907823562622072 10 -18.902496337890625 11 -22.462739944458008 12 -25.45640754699707
		 13 -27.214698791503906 14 -27.81873893737793 15 -27.888177871704102 16 -27.888177871704102
		 22 -27.888177871704102 23 -27.888177871704102 24 -27.850790023803711 25 -27.533954620361328
		 26 -26.529008865356445 27 -24.548130035400391 28 -21.625770568847656 29 -18.136699676513672
		 30 -14.673751831054688;
createNode animCurveTA -n "Character1_LeftHandThumb2_rotateZ";
	rename -uid "93776523-4607-18F9-D90A-288D27B485ED";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -32.373161315917969 8 -32.373161315917969
		 9 -31.257539749145508 10 -28.057371139526367 11 -22.92869758605957 12 -16.40003776550293
		 13 -9.6770315170288086 14 -4.4436459541320801 15 -2.3693301677703857 16 -2.3693301677703857
		 22 -2.3693301677703857 23 -2.3693301677703857 24 -3.7905852794647221 25 -7.5658235549926758
		 26 -12.862202644348145 27 -18.739324569702148 28 -24.311273574829102 29 -28.951940536499023
		 30 -32.373161315917969;
createNode animCurveTU -n "Character1_LeftHandThumb2_visibility";
	rename -uid "EE5AFC56-4A1B-5BFC-9C0A-8195C27D5D0F";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateX";
	rename -uid "E6BE4AF3-4BA3-452D-F074-BEB5EBC9117C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1733376979827881;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateY";
	rename -uid "AD4B14F4-4E59-D99D-AB3F-68AEFD0E927C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.34823575615882874;
createNode animCurveTL -n "Character1_LeftHandThumb3_translateZ";
	rename -uid "0A63ACCD-45E5-4E78-41CB-B4859E6935DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.468076229095459;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleX";
	rename -uid "9487B94D-4E3F-E431-B579-AE9DAC89E157";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleY";
	rename -uid "05A44DF1-4417-F8A4-8504-038D313E74D7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandThumb3_scaleZ";
	rename -uid "F0C95F85-405E-5E00-8661-21B2F66C1CE2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateX";
	rename -uid "DCFC4098-47B7-85AF-ACCC-07B1EDA188CF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3349671235118876e-007;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateY";
	rename -uid "81CBB2F1-44A7-5666-3129-8483E972A249";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.1616723339975579e-008;
createNode animCurveTA -n "Character1_LeftHandThumb3_rotateZ";
	rename -uid "71D3116A-4E72-9F4D-5986-0588E6659AE4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.5252379625962931e-007;
createNode animCurveTU -n "Character1_LeftHandThumb3_visibility";
	rename -uid "2974DF3C-4AEE-81ED-6401-86A2D4601D60";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateX";
	rename -uid "8D286EAD-4B67-398A-0A7B-F28FD21B09C9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.4336724281311035;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateY";
	rename -uid "8072DE38-416F-B69E-33D3-0B9B05AEE8D1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.6582565307617187;
createNode animCurveTL -n "Character1_LeftHandIndex1_translateZ";
	rename -uid "042730DA-4E67-0932-C2BB-7AB325458B85";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.338188648223877;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleX";
	rename -uid "4DB61F44-4CCF-5219-B22B-85A94179CC38";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleY";
	rename -uid "30C3330C-493B-D404-9E2A-558BB6F93499";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_LeftHandIndex1_scaleZ";
	rename -uid "5E59B690-47FD-67E6-98F5-BEA9F70F9507";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateX";
	rename -uid "888A5BEF-4CC7-D695-0D9B-788AF1F98904";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -81.639427185058594 8 -81.639427185058594
		 9 -80.477119445800781 10 -77.391731262207031 11 -72.969940185546875 12 -67.900962829589844
		 13 -63.051296234130866 14 -59.413566589355469 15 -57.988296508789055 16 -57.988296508789055
		 22 -57.988296508789055 23 -57.988296508789055 24 -58.964210510253906 25 -61.574142456054688
		 26 -65.315765380859375 27 -69.664512634277344 28 -74.113655090332031 29 -78.222663879394531
		 30 -81.639427185058594;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateY";
	rename -uid "57C32D47-4A70-5E39-35D6-6B8C7BA8235B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -33.114273071289063 8 -33.114273071289063
		 9 -33.726043701171875 10 -35.213218688964844 11 -37.015872955322266 12 -38.640621185302734
		 13 -39.786434173583984 14 -40.39862060546875 15 -40.582439422607422 16 -40.582439422607422
		 22 -40.582439422607422 23 -40.582439422607422 24 -40.459949493408203 25 -40.060028076171875
		 26 -39.299350738525391 27 -38.126537322998047 28 -36.585304260253906 29 -34.831859588623047
		 30 -33.114273071289063;
createNode animCurveTA -n "Character1_LeftHandIndex1_rotateZ";
	rename -uid "E5EA2AD0-4C74-103E-F130-15BD47766989";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 70.20220947265625 8 70.20220947265625
		 9 69.168319702148438 10 66.371322631835938 11 62.246921539306641 12 57.379123687744134
		 13 52.605209350585938 14 48.9586181640625 15 47.515529632568359 16 47.515529632568359
		 22 47.515529632568359 23 47.515529632568359 24 48.504497528076172 25 51.130928039550781
		 26 54.847377777099609 27 59.087913513183601 28 63.325565338134766 29 67.131675720214844
		 30 70.20220947265625;
createNode animCurveTU -n "Character1_LeftHandIndex1_visibility";
	rename -uid "1590995B-465E-5102-286B-AA9D622C8CE0";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateX";
	rename -uid "1D1ABDF2-4D8C-3AFD-5F78-D5939D7EEDD2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.5846562385559082;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateY";
	rename -uid "93C32B2B-41CB-3B17-50B7-3288AD72B3C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.7001519203186035;
createNode animCurveTL -n "Character1_LeftHandIndex2_translateZ";
	rename -uid "DB349672-44C2-2372-99D1-C291CAAC63A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.35993427038192749;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleX";
	rename -uid "2175FC27-4152-BCC4-6C42-07ADA6E1F98C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleY";
	rename -uid "D30509D7-439F-102D-8BEA-69A2111695EC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandIndex2_scaleZ";
	rename -uid "8A928056-478F-9901-A44F-C6A9F155FF31";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateX";
	rename -uid "0DCA689F-47E5-3A16-08C8-BE993CA8F75C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -57.842250823974602 8 -57.842250823974602
		 9 -58.837909698486328 10 -61.563125610351562 11 -65.719635009765625 12 -70.954704284667969
		 13 -76.583389282226562 14 -81.314903259277344 15 -83.309844970703125 16 -83.309844970703125
		 22 -83.309844970703125 23 -83.309844970703125 24 -81.934700012207031 25 -78.445999145507813
		 26 -73.868515014648438 27 -69.067359924316406 28 -64.612327575683594 29 -60.816555023193352
		 30 -57.842250823974602;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateY";
	rename -uid "12DF76A8-4505-0DE0-37F1-9E98A9B1051A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -62.163970947265632 8 -62.163970947265632
		 9 -62.792747497558594 10 -64.395751953125 11 -66.534675598144531 12 -68.772979736328125
		 13 -70.712181091308594 14 -72.037895202636719 15 -72.526260375976563 16 -72.526260375976563
		 22 -72.526260375976563 23 -72.526260375976563 24 -72.193794250488281 25 -71.264007568359375
		 26 -69.830970764160156 27 -68.019195556640625 28 -65.998634338378906 29 -63.973285675048828
		 30 -62.163970947265632;
createNode animCurveTA -n "Character1_LeftHandIndex2_rotateZ";
	rename -uid "115ED14E-4063-8184-A290-C9BAA82278EF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -13.085443496704102 8 -13.085443496704102
		 9 -12.626277923583984 10 -11.278216361999512 11 -8.9909048080444336 12 -5.7763452529907227
		 13 -1.9893929958343508 14 1.4017314910888672 15 2.8782413005828857 16 2.8782410621643066
		 22 2.8782410621643066 23 2.8782410621643066 24 1.8577386140823364 25 -0.67449855804443359
		 26 -3.8535029888153076 27 -6.9734721183776855 28 -9.625361442565918 29 -11.660289764404297
		 30 -13.085443496704102;
createNode animCurveTU -n "Character1_LeftHandIndex2_visibility";
	rename -uid "A63C0517-40B6-4844-E85A-0BA8E4F9105B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateX";
	rename -uid "8A72B157-47C3-6E01-AFBF-3EBD273C3A7C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.2315034866333008;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateY";
	rename -uid "8E5F43D0-4644-7880-7632-FFBA37C5D235";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.1625156402587891;
createNode animCurveTL -n "Character1_LeftHandIndex3_translateZ";
	rename -uid "0DE63FE9-492E-5591-00C5-ADABB82EE34E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.733155369758606;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleX";
	rename -uid "EA9B9CCA-4059-580E-5035-BA9352106B4B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleY";
	rename -uid "B923B238-4D02-D14D-EA0B-C6B8297794E0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_LeftHandIndex3_scaleZ";
	rename -uid "3AA5CB01-4B95-937C-EAD0-D4A208419557";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateX";
	rename -uid "FFF2BFC4-407C-0CDC-DAD6-CB8EDC7671ED";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1071227845604881e-007;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateY";
	rename -uid "59406A0E-4097-024B-91C7-B298995E2B69";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.3041576707450986e-008;
createNode animCurveTA -n "Character1_LeftHandIndex3_rotateZ";
	rename -uid "6C7E949D-44F2-7A71-53C4-0FAA7805D01D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.3055714848396747e-007;
createNode animCurveTU -n "Character1_LeftHandIndex3_visibility";
	rename -uid "77EF775D-4377-DD1C-34E7-C6A79F8BBCE9";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing1_translateX";
	rename -uid "596ABF60-41E0-33C0-1B57-8B9B3447DEA6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.3586184978485107;
createNode animCurveTL -n "Character1_LeftHandRing1_translateY";
	rename -uid "CF01655B-46AF-116E-0348-DD8EE2AC6240";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.4706459045410156;
createNode animCurveTL -n "Character1_LeftHandRing1_translateZ";
	rename -uid "37463CE7-4C14-9C62-8C16-2EBA210DC8AE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8014267683029175;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleX";
	rename -uid "FDA0D4D3-4FE4-561B-AC03-D4A4375EF0B0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleY";
	rename -uid "F0494189-4052-C02B-61C9-DC85F979704F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandRing1_scaleZ";
	rename -uid "8D4DBF68-4824-1314-9911-61B6326B867E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateX";
	rename -uid "E7391284-416C-320D-69F6-90A003341245";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 87.599990844726563 8 87.599990844726563
		 9 86.0758056640625 10 82.145172119140625 11 76.794403076171875 12 71.0352783203125
		 13 65.848487854003906 14 62.13017654418946 15 60.707347869873047 16 60.707347869873047
		 22 60.707347869873047 23 60.707347869873047 24 61.679683685302727 25 64.32257080078125
		 26 68.234733581542969 27 72.996681213378906 28 78.147659301757813 29 83.187385559082031
		 30 87.599990844726563;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateY";
	rename -uid "65720889-4BE2-9057-A320-C3AF4E35FB8A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -48.969036102294922 8 -48.969036102294922
		 9 -48.959110260009766 10 -48.856796264648438 11 -48.541217803955078 12 -47.974765777587891
		 13 -47.261528015136719 14 -46.629898071289063 15 -46.361240386962891 16 -46.361240386962891
		 22 -46.361240386962891 23 -46.361240386962891 24 -46.546459197998047 25 -47.014583587646484
		 26 -47.613758087158203 27 -48.194171905517578 28 -48.640178680419922 29 -48.894649505615234
		 30 -48.969036102294922;
createNode animCurveTA -n "Character1_LeftHandRing1_rotateZ";
	rename -uid "646443E7-4CB3-E30A-6FF4-B48902EE3ABA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -69.716575622558594 8 -69.716575622558594
		 9 -68.520698547363281 10 -65.454978942871094 11 -61.329189300537116 12 -56.960052490234375
		 13 -53.0994873046875 14 -50.381553649902344 15 -49.353527069091797 16 -49.353527069091797
		 22 -49.353527069091797 23 -49.353527069091797 24 -50.055316925048828 25 -51.978782653808594
		 26 -54.866115570068359 27 -58.438961029052727 28 -62.367015838623054 29 -66.265167236328125
		 30 -69.716575622558594;
createNode animCurveTU -n "Character1_LeftHandRing1_visibility";
	rename -uid "D00EF511-4127-591A-58C5-E79487BD7CA1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing2_translateX";
	rename -uid "21FEC000-4BA4-84D4-4280-AEA06D9C5429";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.7360267639160156;
createNode animCurveTL -n "Character1_LeftHandRing2_translateY";
	rename -uid "B7EC6A65-4981-104D-5753-8EA448228CEF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.8172166347503662;
createNode animCurveTL -n "Character1_LeftHandRing2_translateZ";
	rename -uid "94292F66-46EB-DB3F-6F63-8F8C3216E7D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.81390875577926636;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleX";
	rename -uid "5941B612-43AE-E52B-3237-BE90E1DF2DAA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleY";
	rename -uid "4777E79B-472A-4A6B-4865-C0B20D6F7A12";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_LeftHandRing2_scaleZ";
	rename -uid "64D58CAD-45B2-1DBB-A159-4E8FD56EE8C3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateX";
	rename -uid "72688500-498A-C1DA-8ACF-8490B805EC08";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 64.615447998046875 8 64.615447998046875
		 9 65.556098937988281 10 67.9522705078125 11 71.148277282714844 12 74.50732421875
		 13 77.463310241699219 14 79.542549133300781 15 80.329338073730469 16 80.329338073730469
		 22 80.329338073730469 23 80.329338073730469 24 79.792198181152344 25 78.320625305175781
		 26 76.111427307128906 27 73.372512817382813 28 70.346961975097656 29 67.320999145507813
		 30 64.615447998046875;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateY";
	rename -uid "0049EB3C-4C80-666E-072E-CE9B373E9FE1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 40.079448699951172 8 40.079448699951172
		 9 39.925819396972656 10 39.466510772705078 11 38.695133209228516 12 37.674278259277344
		 13 36.580863952636719 14 35.691791534423828 15 35.327632904052734 16 35.327632904052734
		 22 35.327632904052734 23 35.327632904052734 24 35.57794189453125 25 36.226837158203125
		 26 37.104789733886719 27 38.044441223144531 28 38.906196594238281 29 39.597145080566406
		 30 40.079448699951172;
createNode animCurveTA -n "Character1_LeftHandRing2_rotateZ";
	rename -uid "F317AB0E-4B5D-6EFD-F018-19A1F9124D3E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 82.926361083984375 8 82.926361083984375
		 9 84.024848937988281 10 86.829063415527344 11 90.585823059082031 12 94.560440063476562
		 13 98.086570739746094 14 100.58642578125 15 101.53723907470703 16 101.53723907470703
		 22 101.53723907470703 23 101.53723907470703 24 100.8878173828125 25 99.115165710449219
		 26 96.47021484375 27 93.214210510253906 28 89.641883850097656 29 86.089385986328125
		 30 82.926361083984375;
createNode animCurveTU -n "Character1_LeftHandRing2_visibility";
	rename -uid "4BC6472A-4CEA-94DF-F040-18A321ED4863";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_LeftHandRing3_translateX";
	rename -uid "AB496F9F-4A2A-A17F-221D-918CAD1DA4E0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.4660000801086426;
createNode animCurveTL -n "Character1_LeftHandRing3_translateY";
	rename -uid "F713892A-41E7-0D23-1B98-6DAB3C69B3A0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9262526035308838;
createNode animCurveTL -n "Character1_LeftHandRing3_translateZ";
	rename -uid "CE61619D-4604-91D9-49C9-BCAF1027867B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.018019437789917;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleX";
	rename -uid "6BDEB0D5-4AD6-14B4-2508-93B8803CE7C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleY";
	rename -uid "DB707ADE-4CCA-94A2-BBA9-DF8FB6ABB18A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_LeftHandRing3_scaleZ";
	rename -uid "25A086D4-4F18-4C83-8059-0DBC4B4F52C3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateX";
	rename -uid "8A7B3F64-4C98-F708-0AAD-4699EEBADAF3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.4964757017187367e-007;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateY";
	rename -uid "FE2E30DE-4867-D881-81C9-B4B6AA62BFCE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.00672701566873e-007;
createNode animCurveTA -n "Character1_LeftHandRing3_rotateZ";
	rename -uid "4345783F-42A0-7436-CF5B-24AC52967599";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.5026604671675159e-007;
createNode animCurveTU -n "Character1_LeftHandRing3_visibility";
	rename -uid "9AA4A97A-4166-783B-FD77-1DAD97552B8A";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightShoulder_translateX";
	rename -uid "B44E73FF-46E0-ED69-E513-269948E6AC90";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 10.151631355285645;
createNode animCurveTL -n "Character1_RightShoulder_translateY";
	rename -uid "AFB1AFCC-4FCB-27C2-ED86-A9824E266EEF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.1017274856567383;
createNode animCurveTL -n "Character1_RightShoulder_translateZ";
	rename -uid "C0BD661F-4922-FC00-3B34-2F9665E2B3C7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.81731081008911133;
createNode animCurveTU -n "Character1_RightShoulder_scaleX";
	rename -uid "D79BEFC7-43E8-E818-CAC8-D8B163EF8D1F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightShoulder_scaleY";
	rename -uid "B24E3A69-4DA4-7C31-7D04-43B379AB470A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightShoulder_scaleZ";
	rename -uid "E5B4CE94-4FBF-A977-C865-83B879A8E2A5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightShoulder_rotateX";
	rename -uid "6EA23038-4355-DD38-6766-8BB8882356D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 8.3699455261230469 1 9.8897991180419922
		 2 11.562458992004395 3 13.045605659484863 4 13.973348617553711 5 14.19244384765625
		 6 13.896092414855957 7 13.237539291381836 8 12.37324333190918 9 11.241121292114258
		 10 9.8127765655517578 11 8.2895269393920898 12 6.8557844161987305 13 5.6708669662475586
		 14 4.86932373046875 15 4.5737066268920898 16 4.5737066268920898 22 4.5737066268920898
		 23 4.5737066268920898 24 4.7084431648254395 25 5.0750308036804199 26 5.6189956665039062
		 27 6.2843685150146484 28 7.0103001594543457 29 7.7297472953796396 30 8.3699455261230469;
createNode animCurveTA -n "Character1_RightShoulder_rotateY";
	rename -uid "8B8C40DF-48B9-9B2A-50C9-C4AF48AAD106";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -8.1564884185791016 1 -8.6001625061035156
		 2 -8.9976701736450195 3 -9.2668666839599609 4 -9.3820552825927734 5 -9.3352985382080078
		 6 -9.1860160827636719 7 -9.0190505981445313 8 -8.9107656478881836 9 -8.8440027236938477
		 10 -8.7193460464477539 11 -8.5105342864990234 12 -8.229365348815918 13 -7.9269194602966309
		 14 -7.6825923919677725 15 -7.5838861465454093 16 -7.5838861465454093 22 -7.5838861465454093
		 23 -7.5838861465454093 24 -7.6175179481506357 25 -7.7035703659057626 26 -7.8170590400695801
		 27 -7.9338989257812491 28 -8.0352449417114258 29 -8.1101770401000977 30 -8.1564884185791016;
createNode animCurveTA -n "Character1_RightShoulder_rotateZ";
	rename -uid "9B269E3B-44E4-57DF-880A-FFA60B4D774C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 26 ".ktv[0:25]"  0 -10.832074165344238 1 -13.983783721923828
		 2 -17.364156723022461 3 -20.309627532958984 4 -22.152790069580078 5 -22.680845260620117
		 6 -22.259069442749023 7 -21.088075637817383 8 -19.365379333496094 9 -16.918937683105469
		 10 -13.719696044921875 11 -10.17877197265625 12 -6.7096848487854004 13 -3.7253077030181885
		 14 -1.6356750726699829 15 -0.84852004051208496 16 -0.84852004051208496 22 -0.84852004051208496
		 23 -0.84852004051208496 24 -1.2268197536468506 25 -2.2454860210418701 26 -3.7301559448242183
		 27 -5.5061817169189453 28 -7.3985576629638681 29 -9.2321443557739258 30 -10.832074165344238;
createNode animCurveTU -n "Character1_RightShoulder_visibility";
	rename -uid "26F3E901-4F66-4C28-21C2-B481D830DE51";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightArm_translateX";
	rename -uid "8157BF50-4C85-718F-5933-EC8C56053F9E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.05986499786377;
createNode animCurveTL -n "Character1_RightArm_translateY";
	rename -uid "0160D5D2-4706-044E-A2BC-50B0D3974D8B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.8638343811035156;
createNode animCurveTL -n "Character1_RightArm_translateZ";
	rename -uid "42E81F20-47D8-770D-C142-888D8537B76D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.4631747007369995;
createNode animCurveTU -n "Character1_RightArm_scaleX";
	rename -uid "3565E714-4BF2-D58C-A273-2B9BD485B838";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightArm_scaleY";
	rename -uid "268E954B-4F50-D0EA-0377-CD9D375555F7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "Character1_RightArm_scaleZ";
	rename -uid "B069AA3F-4B1E-E19A-91F5-7897EEDB811E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "Character1_RightArm_rotateX";
	rename -uid "1B7C99AB-4A8D-2967-E4CA-448AF6FD243E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -7.7432284355163565 1 -5.2033267021179199
		 2 -2.5587368011474609 3 -0.3808097243309021 4 0.84520417451858521 5 0.65277153253555298
		 6 -0.540976881980896 7 -1.93816077709198 8 -2.7246980667114258 9 -2.9182369709014893
		 10 -3.1111760139465332 11 -3.3248369693756104 12 -3.5652520656585693 13 -3.8220207691192627
		 14 -4.069453239440918 15 -4.2699289321899414 16 -4.4432392120361328 17 -4.6375436782836914
		 18 -4.8505845069885254 19 -5.0769915580749512 20 -5.306952953338623 21 -5.5254497528076172
		 22 -5.7117619514465332 23 -5.839012622833252 24 -6.3430428504943848 25 -6.8385148048400879
		 26 -7.283452033996582 27 -7.6359539031982431 28 -7.8531885147094735 29 -7.896745204925538
		 30 -7.7432284355163565;
createNode animCurveTA -n "Character1_RightArm_rotateY";
	rename -uid "6EC25C91-47AF-6D69-05F6-9F83D00EE7B3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 47.553615570068359 1 42.935043334960937
		 2 37.98211669921875 3 33.328231811523438 4 29.586948394775394 5 27.080905914306641
		 6 25.436487197875977 7 24.194379806518555 8 22.904678344726563 9 21.574714660644531
		 10 20.472038269042969 11 19.593479156494141 12 18.937625885009766 13 18.502410888671875
		 14 18.283935546875 15 18.275369644165039 16 18.530895233154297 17 19.109477996826172
		 18 20.00341796875 19 21.200710296630859 20 22.687772750854492 21 24.451940536499023
		 22 26.483671188354492 23 28.778352737426758 24 31.060327529907227 25 33.553482055664063
		 26 36.233020782470703 27 39.057514190673828 28 41.958011627197266 29 44.833610534667969
		 30 47.553615570068359;
createNode animCurveTA -n "Character1_RightArm_rotateZ";
	rename -uid "2ABFF77D-49A1-974B-FBAA-BFADE08C184F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -10.792771339416504 1 -10.197103500366211
		 2 -9.8033409118652344 3 -10.257053375244141 4 -12.017953872680664 5 -15.643624305725098
		 6 -20.363710403442383 7 -24.730657577514648 8 -27.516572952270508 9 -28.78386116027832
		 10 -29.44675254821777 11 -29.668066024780273 12 -29.594997406005856 13 -29.359216690063477
		 14 -29.078020095825195 15 -28.856382369995114 16 -28.623756408691406 17 -28.220649719238281
		 18 -27.608924865722656 19 -26.749080657958984 20 -25.602195739746094 21 -24.131582260131836
		 22 -22.304008483886719 23 -20.090448379516602 24 -18.74946403503418 25 -17.43571662902832
		 26 -16.144973754882812 27 -14.862323760986326 28 -13.562261581420898 29 -12.214046478271484
		 30 -10.792771339416504;
createNode animCurveTU -n "Character1_RightArm_visibility";
	rename -uid "5BDAC0C6-420B-8B8C-4AB9-D7B3CF4F8655";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightForeArm_translateX";
	rename -uid "93CB279A-43A7-01D1-0210-5983D22CA75F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -16.894504547119141;
createNode animCurveTL -n "Character1_RightForeArm_translateY";
	rename -uid "70407ABC-4D96-88AA-64D1-6D88E6915381";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.7289671897888184;
createNode animCurveTL -n "Character1_RightForeArm_translateZ";
	rename -uid "0CE127FD-4B1A-ECC9-9A2B-DB8FA8C796CB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.2348899841308594;
createNode animCurveTU -n "Character1_RightForeArm_scaleX";
	rename -uid "D16B911C-4BAC-811A-A768-77BB6BC9BC9C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightForeArm_scaleY";
	rename -uid "FED1BFF3-43B9-E03E-36EB-2C82E9D9F148";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightForeArm_scaleZ";
	rename -uid "C101ACE0-4A33-74EF-FBF5-F5979C8EEB8F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightForeArm_rotateX";
	rename -uid "51AC0BD8-4272-8D36-7705-CFA91FFB075A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -7.7566051483154288 1 -4.6782169342041016
		 2 -1.4790117740631104 3 1.3036991357803345 4 3.2359943389892578 5 4.0841121673583984
		 6 4.2125215530395508 7 4.1449222564697266 8 4.3945508003234863 9 5.0620269775390625
		 10 5.8836636543273926 11 6.8104734420776367 12 7.7647614479064933 13 8.6396856307983398
		 14 9.3023529052734375 15 9.6033811569213867 16 9.197535514831543 17 7.8908209800720215
		 18 5.8188190460205078 19 3.2036838531494141 20 0.33227351307868958 21 -2.4973800182342529
		 22 -5.0336093902587891 23 -7.1014194488525391 24 -7.9488244056701669 25 -8.3215808868408203
		 26 -8.3720407485961914 27 -8.235753059387207 28 -8.0263528823852539 29 -7.8388290405273429
		 30 -7.7566051483154288;
createNode animCurveTA -n "Character1_RightForeArm_rotateY";
	rename -uid "DD642DEA-41F8-815A-2F9F-50A61DA1D03C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 16.15064811706543 1 16.936164855957031
		 2 17.451269149780273 3 17.939054489135742 4 18.781023025512695 5 20.224964141845703
		 6 22.187540054321289 7 24.560415267944336 8 27.153005599975586 9 29.904478073120121
		 10 32.815444946289063 11 35.740608215332031 12 38.546070098876953 13 41.110446929931641
		 14 43.323211669921875 15 45.079547882080078 16 46.358928680419922 17 47.182548522949219
		 18 47.486564636230469 19 47.211601257324219 20 46.338920593261719 21 44.913379669189453
		 22 43.045886993408203 23 40.896614074707031 24 38.358692169189453 25 35.231132507324219
		 26 31.670192718505863 27 27.821941375732422 28 23.83909797668457 29 19.888490676879883
		 30 16.15064811706543;
createNode animCurveTA -n "Character1_RightForeArm_rotateZ";
	rename -uid "648F022C-46E6-2096-4D85-C6875D05D8B1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -23.415355682373047 1 -17.559677124023438
		 2 -11.189048767089844 3 -5.1196699142456055 4 -0.16820831596851349 5 3.2880957126617432
		 6 5.6091408729553223 7 7.1154971122741699 8 8.1838750839233398 9 8.9557886123657227
		 10 9.3375186920166016 11 9.3943281173706055 12 9.1714029312133789 13 8.6901969909667969
		 14 7.9488825798034668 15 6.9295268058776855 16 4.8794856071472168 17 1.2564657926559448
		 18 -3.5847136974334717 19 -9.205388069152832 20 -15.107716560363768 21 -20.789424896240234
		 22 -25.801328659057617 23 -29.777359008789063 24 -30.866424560546875 25 -30.646247863769528
		 26 -29.533266067504883 27 -27.919221878051758 28 -26.158075332641602 29 -24.56390380859375
		 30 -23.415355682373047;
createNode animCurveTU -n "Character1_RightForeArm_visibility";
	rename -uid "E9FA360B-4169-1519-5DE2-D7891DADDB05";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHand_translateX";
	rename -uid "E0B86AC2-4194-6443-2DAF-0F9BA1DC4753";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.9317862987518311;
createNode animCurveTL -n "Character1_RightHand_translateY";
	rename -uid "0C78E1D5-4242-E865-8363-0C9A3ABA453F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 16.156974792480469;
createNode animCurveTL -n "Character1_RightHand_translateZ";
	rename -uid "0FDD90F4-4274-4103-58F6-9096B7ABFBB3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 5.4309353828430176;
createNode animCurveTU -n "Character1_RightHand_scaleX";
	rename -uid "7D874CDD-4584-481F-E0B1-12BB3B10FBFB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_RightHand_scaleY";
	rename -uid "32D935B7-4C3B-A641-1E7B-C5B2289DB8A3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHand_scaleZ";
	rename -uid "733EA1D1-4297-6188-5C12-C69ABF9B46AC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightHand_rotateX";
	rename -uid "7B7F4DD9-45D9-6DA2-0683-E1B4B7BA2B7B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -1.6178616285324097 1 -1.655461311340332
		 2 -1.7045764923095703 3 -1.7638169527053833 4 -1.8319368362426758 5 -1.9078582525253296
		 6 -1.9906932115554807 7 -2.0797615051269531 8 -2.1746056079864502 9 -1.9180001020431519
		 10 -1.1261625289916992 11 -0.014589382335543633 12 1.2175744771957397 13 2.3465526103973389
		 14 3.1085927486419678 15 3.2222771644592285 16 2.660973072052002 17 1.6969449520111084
		 18 0.52036422491073608 19 -0.69772070646286011 20 -1.8262591361999512 21 -2.7862889766693115
		 22 -3.5530967712402344 23 -4.1490769386291504 24 -4.2008681297302246 25 -4.0147857666015625
		 26 -3.6540966033935542 27 -3.1694736480712891 28 -2.6149184703826904 29 -2.0633823871612549
		 30 -1.6178616285324097;
createNode animCurveTA -n "Character1_RightHand_rotateY";
	rename -uid "BA1BE928-4618-5AAF-513D-8B8AF14DC29F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -2.9114444255828857 1 -2.6866128444671631
		 2 -2.4842665195465088 3 -2.3002123832702637 4 -2.1299481391906738 5 -1.9686625003814697
		 6 -1.8112400770187378 7 -1.6522712707519531 8 -1.4860649108886719 9 -0.62692153453826904
		 10 1.3053805828094482 11 3.8331458568572994 12 6.482017993927002 13 8.8121871948242187
		 14 10.428611755371094 15 10.963011741638184 16 10.652910232543945 17 9.9792594909667969
		 18 8.9318923950195313 19 7.5131931304931641 20 5.7444300651550293 21 3.6636645793914795
		 22 1.3199745416641235 23 -1.2313523292541504 24 -2.0285608768463135 25 -2.5252742767333984
		 26 -2.7849066257476807 27 -2.8734662532806396 28 -2.8657815456390381 29 -2.8473706245422363
		 30 -2.9114444255828857;
createNode animCurveTA -n "Character1_RightHand_rotateZ";
	rename -uid "3AD38EE1-469E-A0E9-2172-DC9AAE22C96C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 1.2924915552139282 1 0.012491647154092789
		 2 -1.2801327705383301 3 -2.5823006629943848 4 -3.8908672332763672 5 -5.2026376724243164
		 6 -6.5143814086914062 7 -7.8228363990783691 8 -9.1247243881225586 9 -10.064059257507324
		 10 -10.380757331848145 11 -10.214971542358398 12 -9.7421464920043945 13 -9.1966409683227539
		 14 -8.8697595596313477 15 -9.0774431228637695 16 -9.9792261123657227 17 -11.360348701477051
		 18 -12.95400333404541 19 -14.495832443237303 20 -15.732172012329102 21 -16.425168991088867
		 22 -16.353237152099609 23 -15.306888580322267 24 -14.115049362182617 25 -12.22850227355957
		 26 -9.8274564743041992 27 -7.088165283203125 28 -4.1917037963867188 29 -1.3299225568771362
		 30 1.2924915552139282;
createNode animCurveTU -n "Character1_RightHand_visibility";
	rename -uid "38F3BA7F-4911-2FFE-F9C2-369C6521F44B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb1_translateX";
	rename -uid "45325FCF-4A05-7D5C-E05B-0AB6FD603A80";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -9.2155418395996094;
createNode animCurveTL -n "Character1_RightHandThumb1_translateY";
	rename -uid "92BA8C53-4381-E266-3B79-53B0875F8514";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -1.8998895883560181;
createNode animCurveTL -n "Character1_RightHandThumb1_translateZ";
	rename -uid "DEBE8E02-40E7-78CF-5DDB-2E9038420F0F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3098368644714355;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleX";
	rename -uid "2C9CF295-4820-65F1-BA1D-1F95D052ABB6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleY";
	rename -uid "BAA8D7DE-45B5-1B92-8C3A-5FA03E3BEF6E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandThumb1_scaleZ";
	rename -uid "AC3D3D0D-4BDA-D820-8D73-E0938D79C823";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateX";
	rename -uid "2BE46B27-444C-0DC6-91D3-6B991BC2DB20";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -2.7197971343994141 8 -2.7197971343994141
		 9 -3.7511916160583496 10 -6.3727173805236816 11 -9.918701171875 12 -13.833234786987305
		 13 -17.584272384643555 14 -20.489433288574219 15 -21.663774490356445 16 -21.663774490356445
		 22 -21.663774490356445 23 -21.663774490356445 24 -20.857112884521484 25 -18.750341415405273
		 26 -15.822546005249025 27 -12.478958129882813 28 -9.0189027786254883 29 -5.6813931465148926
		 30 -2.7197971343994141;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateY";
	rename -uid "0BC739AC-49A0-80DE-2DC2-56BF9AF2329B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -6.4185709953308105 8 -6.4185709953308105
		 9 -7.7572722434997559 10 -11.259660720825195 11 -16.117866516113281 12 -21.414793014526367
		 13 -26.188945770263672 14 -29.574012756347656 15 -30.853075027465817 16 -30.853075027465817
		 22 -30.853075027465817 23 -30.853075027465817 24 -29.980195999145511 25 -27.584117889404297
		 26 -23.996316909790039 27 -19.606687545776367 28 -14.881405830383303 29 -10.324602127075195
		 30 -6.4185709953308105;
createNode animCurveTA -n "Character1_RightHandThumb1_rotateZ";
	rename -uid "B4B5DEDD-445E-A3A7-4F50-299DED5804C9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 58.378379821777337 8 58.378379821777337
		 9 58.041236877441406 10 57.440635681152344 11 57.258934020996094 12 57.936058044433587
		 13 59.410678863525398 14 61.04234695434571 15 61.807601928710938 16 61.807601928710938
		 22 61.807601928710938 23 61.807601928710938 24 61.275825500488281 25 60.018287658691399
		 26 58.623065948486328 27 57.598278045654297 28 57.233837127685547 29 57.561965942382813
		 30 58.378379821777337;
createNode animCurveTU -n "Character1_RightHandThumb1_visibility";
	rename -uid "C1F2F51F-4481-597D-334D-3C97D30E86FE";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb2_translateX";
	rename -uid "F4972EBB-493E-AB15-DFF0-259472BBABDC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9627087116241455;
createNode animCurveTL -n "Character1_RightHandThumb2_translateY";
	rename -uid "4AFEFD29-4854-7503-5B86-B1822AF09A48";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.5399179458618164;
createNode animCurveTL -n "Character1_RightHandThumb2_translateZ";
	rename -uid "A84D82FB-403A-FC6C-F957-33A751B56083";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -4.6609530448913574;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleX";
	rename -uid "B7EDF348-4400-3B3D-8BB2-0A95D9C1A960";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleY";
	rename -uid "2E07E00B-4D37-86C6-6163-67B279A91DD4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandThumb2_scaleZ";
	rename -uid "89336704-4B67-CF83-FC7C-8FBD855F6F8B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateX";
	rename -uid "091CCA8E-4939-0DB8-6498-8BA030D15308";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 1.6604856252670288 8 1.6604856252670288
		 9 -0.5866352915763855 10 -6.7416625022888184 11 -16.043783187866211 12 -27.336799621582031
		 13 -38.594383239746094 14 -47.173858642578125 15 -50.537124633789062 16 -50.537124633789062
		 22 -50.537124633789062 23 -50.537124633789062 24 -48.234855651855469 25 -42.072601318359375
		 26 -33.298126220703125 27 -23.340738296508789 28 -13.58484935760498 29 -5.0566549301147461
		 30 1.6604856252670288;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateY";
	rename -uid "E1B65F81-499B-F4A5-23D3-7398AD40FBCF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 21.524660110473633 8 21.524660110473633
		 9 23.125791549682617 10 26.974010467529297 11 31.477664947509766 12 35.190643310546875
		 13 37.321338653564453 14 38.036617279052734 15 38.117893218994141 16 38.117893218994141
		 22 38.117893218994141 23 38.117893218994141 24 38.074111938476563 25 37.701133728027344
		 26 36.496852874755859 27 34.073085784912109 28 30.426437377929688 29 25.994962692260742
		 30 21.524660110473633;
createNode animCurveTA -n "Character1_RightHandThumb2_rotateZ";
	rename -uid "CBB4DC96-438D-1B08-4505-828C45094F9A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 18.76649284362793 8 18.76649284362793
		 9 17.404018402099609 10 13.530234336853027 11 7.3732862472534189 12 -0.41392096877098083
		 13 -8.338963508605957 14 -14.388689041137694 15 -16.746269226074219 16 -16.746269226074219
		 22 -16.746269226074219 23 -16.746269226074219 24 -15.133691787719727 25 -10.795147895812988
		 26 -4.6008925437927246 27 2.369189977645874 28 9.0293455123901367 29 14.609372138977051
		 30 18.76649284362793;
createNode animCurveTU -n "Character1_RightHandThumb2_visibility";
	rename -uid "70B8FEE0-4223-D8CA-DC40-E9A1FD79A01B";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandThumb3_translateX";
	rename -uid "54DC071F-461D-D96C-A360-F98D695B51DE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.5867142677307129;
createNode animCurveTL -n "Character1_RightHandThumb3_translateY";
	rename -uid "C1D82125-4FC3-9BB1-2CDD-539511660D5F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.6152248382568359;
createNode animCurveTL -n "Character1_RightHandThumb3_translateZ";
	rename -uid "B894625D-4B9A-9BEC-9D6F-65BA1312B8EC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.4791715145111084;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleX";
	rename -uid "9D632E70-4D4B-CBE9-9B78-368A98505F83";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleY";
	rename -uid "84F6C485-4A1A-5EC9-E414-2B8289D66447";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandThumb3_scaleZ";
	rename -uid "4BA7293F-49A1-045F-9EB4-6BB4BC765C38";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateX";
	rename -uid "12B7B6BD-406A-7004-A78C-578987D94F15";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.3022869173655636e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateY";
	rename -uid "9D2D1AF8-4114-B17E-3A0E-06BD2FE56B3D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.8784427159916959e-007;
createNode animCurveTA -n "Character1_RightHandThumb3_rotateZ";
	rename -uid "1E902A2B-494C-9FE3-80D5-34B03DFC9D91";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.5911729178696987e-007;
createNode animCurveTU -n "Character1_RightHandThumb3_visibility";
	rename -uid "1541E539-42E7-EC50-6A9E-F6A3416767CE";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex1_translateX";
	rename -uid "D85379FF-480D-B37D-EA8B-F79B48C83F77";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.615594863891602;
createNode animCurveTL -n "Character1_RightHandIndex1_translateY";
	rename -uid "ADE3AECC-4545-0BD5-21C1-189B6A990080";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.4688365459442139;
createNode animCurveTL -n "Character1_RightHandIndex1_translateZ";
	rename -uid "B3D19E94-416A-1CDB-8288-13A8D7F27BCE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 4.3266587257385254;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleX";
	rename -uid "D5B5027B-4C15-2E82-2DF4-098749A9001D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleY";
	rename -uid "6307FB3B-4FB4-44BA-FC49-9FB46B7221E7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandIndex1_scaleZ";
	rename -uid "054CB118-48C0-8D8E-B55B-E8B32777D8F2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateX";
	rename -uid "4925AE6A-41BA-B6A1-8DF6-77A895CD8D1E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 41.524635314941406 8 41.524635314941406
		 9 43.690662384033203 10 49.658794403076172 11 58.760158538818366 12 69.906036376953125
		 13 81.068443298339844 14 89.5650634765625 15 92.884078979492187 16 92.884078979492187
		 22 92.884078979492187 23 92.884078979492187 24 90.613067626953125 25 84.516693115234375
		 26 75.8148193359375 27 65.952850341796875 28 56.345962524414062 29 48.020160675048828
		 30 41.524635314941406;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateY";
	rename -uid "F587D167-4F77-C0CE-3981-D0BA0D852A3C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  7 -62.593696594238281 8 -62.593696594238281
		 9 -63.25080871582032 10 -64.831619262695313 11 -66.680976867675781 12 -68.192184448242188
		 13 -69.025428771972656 14 -69.26068115234375 15 -69.263580322265625 23 -69.263580322265625
		 24 -69.266960144042969 25 -69.16082763671875 26 -68.710823059082031 27 -67.740592956542969
		 28 -66.249855041503906 29 -64.429290771484375 30 -62.593696594238281;
createNode animCurveTA -n "Character1_RightHandIndex1_rotateZ";
	rename -uid "29815B42-4CAE-3876-0189-BE9676447C51";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -32.544338226318359 8 -32.544338226318359
		 9 -34.203094482421875 10 -38.860874176025391 11 -46.165756225585937 12 -55.348369598388672
		 13 -64.703849792480469 14 -71.876152038574219 15 -74.681053161621094 16 -74.681053161621094
		 22 -74.681053161621094 23 -74.681053161621094 24 -72.761833190917969 25 -67.611839294433594
		 26 -60.286838531494141 27 -52.068206787109375 28 -44.207889556884766 29 -37.570178985595703
		 30 -32.544338226318359;
createNode animCurveTU -n "Character1_RightHandIndex1_visibility";
	rename -uid "3B22688A-4C8A-2979-548A-D99A702BFF27";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex2_translateX";
	rename -uid "113BB0F8-4CF6-15DF-E176-91A471D6623F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -0.10917675495147705;
createNode animCurveTL -n "Character1_RightHandIndex2_translateY";
	rename -uid "9472ECAE-4F40-CC1A-107F-04984157338F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.54082572460174561;
createNode animCurveTL -n "Character1_RightHandIndex2_translateZ";
	rename -uid "2D1479C8-48D7-0A63-4221-9AA8096E2226";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.6861534118652344;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleX";
	rename -uid "D7859CC4-4904-9D80-A82C-ABA911DA07B2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleY";
	rename -uid "2839C6FF-46AE-66AB-CBDA-3080F33F214E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandIndex2_scaleZ";
	rename -uid "D2E94D8F-4C84-9E58-A362-39B93FEDC935";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateX";
	rename -uid "220F4C03-45C9-1CD0-A6D1-79B84FB2578D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 96.6031494140625 8 96.6031494140625 9 95.744590759277344
		 10 93.488807678222656 11 90.309455871582031 12 86.726470947265625 13 83.33746337890625
		 14 80.804275512695312 15 79.810813903808594 16 79.810813903808594 22 79.810813903808594
		 23 79.810813903808594 24 80.491195678710938 25 82.308738708496094 26 84.916824340820313
		 27 87.966941833496094 28 91.126312255859375 29 94.093086242675781 30 96.6031494140625;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateY";
	rename -uid "06424E92-43AC-36FC-F982-5596F930AD7F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 30.306468963623047 8 30.306468963623047
		 9 30.686162948608398 10 31.620145797729492 11 32.786983489990234 12 33.904819488525391
		 13 34.781703948974609 14 35.328475952148438 15 35.518379211425781 16 35.518379211425781
		 22 35.518379211425781 23 35.518379211425781 24 35.389793395996094 25 35.014705657958984
		 26 34.394203186035156 27 33.540618896484375 28 32.503353118896484 29 31.378820419311523
		 30 30.306468963623047;
createNode animCurveTA -n "Character1_RightHandIndex2_rotateZ";
	rename -uid "F4CE4D53-40C1-BAED-9E60-2F8D904FFECF";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 82.211105346679688 8 82.211105346679688
		 9 81.466865539550781 10 79.503990173339844 11 76.721046447753906 12 73.565292358398438
		 13 70.564765930175781 14 68.313873291015625 15 67.429512023925781 16 67.429512023925781
		 22 67.429512023925781 23 67.429512023925781 24 68.035263061523438 25 69.651443481445312
		 26 71.964767456054687 27 74.659950256347656 28 77.437736511230469 29 80.030815124511719
		 30 82.211105346679688;
createNode animCurveTU -n "Character1_RightHandIndex2_visibility";
	rename -uid "C7381C3B-40AC-D9D2-B418-428CF1251CBA";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandIndex3_translateX";
	rename -uid "AE7101B6-4A4A-CA26-7DA7-8283D357F82A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.8903987407684326;
createNode animCurveTL -n "Character1_RightHandIndex3_translateY";
	rename -uid "F3201EA4-41D9-FA48-258F-CD856342B4CD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.3401000499725342;
createNode animCurveTL -n "Character1_RightHandIndex3_translateZ";
	rename -uid "3F45AFB9-4AB3-11F1-FE8C-73A7EE00BCF9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.8930935859680176;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleX";
	rename -uid "16C4DFD7-47B2-8AA5-E243-558502620625";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleY";
	rename -uid "CA9BDA38-4642-895F-881F-2788B62F64BA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTU -n "Character1_RightHandIndex3_scaleZ";
	rename -uid "BA45213A-47F7-9121-60D5-81A661922FE6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999982118606567;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateX";
	rename -uid "865E8516-429E-7516-0DE1-69A61A90608B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.3036107538464421e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateY";
	rename -uid "3F4367E3-47E4-470F-8B93-82994D5A96EE";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.9643598509210278e-007;
createNode animCurveTA -n "Character1_RightHandIndex3_rotateZ";
	rename -uid "6FAB62B3-488A-AC43-4453-E0A7A924BFF4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.2750032258045391e-007;
createNode animCurveTU -n "Character1_RightHandIndex3_visibility";
	rename -uid "CF14CD81-4146-E25E-8621-EEB0DB3C7918";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing1_translateX";
	rename -uid "64F46FD8-49F7-9F49-920E-0BA48B8B2F0A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.6450033187866211;
createNode animCurveTL -n "Character1_RightHandRing1_translateY";
	rename -uid "82A94F11-47F1-A601-F243-1DB21A145F7D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.0317020416259766;
createNode animCurveTL -n "Character1_RightHandRing1_translateZ";
	rename -uid "2AD4D4CB-4456-DC32-A669-BE9F60202F45";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.2285447120666504;
createNode animCurveTU -n "Character1_RightHandRing1_scaleX";
	rename -uid "ACA0BB52-48CD-9201-43C1-6D9EE8C25750";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandRing1_scaleY";
	rename -uid "3FBE454E-4C5A-6FF8-0B47-519F90985145";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandRing1_scaleZ";
	rename -uid "6E287477-4EE7-BAF8-EF4F-168770B37A98";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_RightHandRing1_rotateX";
	rename -uid "0EA93AFF-42F4-C83C-381E-608A58D0904B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 48.429409027099609 8 48.429409027099609
		 9 49.893062591552734 10 53.990463256835937 11 60.391765594482429 12 68.427833557128906
		 13 76.647087097167969 14 82.99835205078125 15 85.498344421386719 16 85.498344421386719
		 22 85.498344421386719 23 85.498344421386719 24 83.78668212890625 25 79.215652465820313
		 26 72.759536743164062 27 65.556060791015625 28 58.677982330322266 29 52.856681823730469
		 30 48.429409027099609;
createNode animCurveTA -n "Character1_RightHandRing1_rotateY";
	rename -uid "30277BD0-4F3F-8C96-6419-C8A552471E85";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -60.605064392089844 8 -60.605064392089844
		 9 -61.280483245849609 10 -62.914867401123047 11 -64.857566833496094 12 -66.507339477539063
		 13 -67.510086059570313 14 -67.896018981933594 15 -67.962852478027344 16 -67.962852478027344
		 22 -67.962852478027344 23 -67.962852478027344 24 -67.922172546386719 25 -67.704360961914063
		 26 -67.111717224121094 27 -66.003250122070312 28 -64.400108337402344 29 -62.497322082519531
		 30 -60.605064392089844;
createNode animCurveTA -n "Character1_RightHandRing1_rotateZ";
	rename -uid "BA8751CE-4E75-3AC5-7C43-94A270C08A65";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 -47.688526153564453 8 -47.688526153564453
		 9 -49.232791900634766 10 -53.560005187988281 11 -60.325935363769524 12 -68.824363708496094
		 13 -77.526748657226563 14 -84.266532897949219 15 -86.925132751464844 16 -86.925132751464844
		 22 -86.925132751464844 23 -86.925132751464844 24 -85.104469299316406 25 -80.250312805175781
		 26 -73.408714294433594 27 -65.78668212890625 28 -58.514179229736335 29 -52.362190246582031
		 30 -47.688526153564453;
createNode animCurveTU -n "Character1_RightHandRing1_visibility";
	rename -uid "4F138433-4B63-82CF-FF3A-5A95325F6862";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing2_translateX";
	rename -uid "513E059B-4508-B2BF-0F34-78A7BCA64BC7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.046228401362895966;
createNode animCurveTL -n "Character1_RightHandRing2_translateY";
	rename -uid "F9CECD21-4C0A-70D1-8F26-559E5AA838F8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.89510548114776611;
createNode animCurveTL -n "Character1_RightHandRing2_translateZ";
	rename -uid "967AAAD5-44A8-282B-A3A6-E1AD67246939";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.4977946281433105;
createNode animCurveTU -n "Character1_RightHandRing2_scaleX";
	rename -uid "1C066E1D-47E1-5B72-B04C-2E9D94B42410";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_RightHandRing2_scaleY";
	rename -uid "3432AB8B-477D-FD5F-819A-BB811D0209C9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_RightHandRing2_scaleZ";
	rename -uid "636D55CA-4AF7-5A27-2DB5-6D8863A0087E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTA -n "Character1_RightHandRing2_rotateX";
	rename -uid "DC2AAAFF-48A7-D328-2E82-47A69ADD3EF4";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 96.525802612304688 8 96.525802612304688
		 9 95.666595458984375 10 93.408744812011719 11 90.225616455078125 12 86.637367248535156
		 13 83.242584228515625 14 80.704704284667969 15 79.709327697753906 16 79.709327697753906
		 22 79.709327697753906 23 79.709327697753906 24 80.391021728515625 25 82.211990356445312
		 26 84.824714660644531 27 87.879768371582031 28 91.043533325195313 29 94.013633728027344
		 30 96.525802612304688;
createNode animCurveTA -n "Character1_RightHandRing2_rotateY";
	rename -uid "EB9C9819-402A-B6F8-F0B6-B08278F1217B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 30.502138137817383 8 30.502138137817383
		 9 30.882543563842773 10 31.818155288696289 11 32.986705780029297 12 34.105648040771484
		 13 34.982776641845703 14 35.529224395751953 15 35.718883514404297 16 35.718883514404297
		 22 35.718883514404297 23 35.718883514404297 24 35.590473175048828 25 35.2156982421875
		 26 34.595260620117187 27 33.741161346435547 28 32.702705383300781 29 31.576429367065426
		 30 30.502138137817383;
createNode animCurveTA -n "Character1_RightHandRing2_rotateZ";
	rename -uid "0514EBDE-458E-451C-5035-B182001D6671";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  7 82.574462890625 8 82.574462890625 9 81.82598876953125
		 10 79.851837158203125 11 77.052688598632813 12 73.878280639648438 13 70.859817504882812
		 14 68.59539794921875 15 67.705718994140625 16 67.705718994140625 22 67.705718994140625
		 23 67.705718994140625 24 68.315116882324219 25 69.941009521484375 26 72.268203735351562
		 27 74.979438781738281 28 77.773582458496094 29 80.381706237792969 30 82.574462890625;
createNode animCurveTU -n "Character1_RightHandRing2_visibility";
	rename -uid "D6973407-4B22-6B28-5A6D-63A5E48EB0EA";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_RightHandRing3_translateX";
	rename -uid "8BDF3145-461A-ABEA-B4C4-27AC5B992505";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.8638609647750854;
createNode animCurveTL -n "Character1_RightHandRing3_translateY";
	rename -uid "5F7E96F7-40AA-1913-D447-CF856F417294";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0986917018890381;
createNode animCurveTL -n "Character1_RightHandRing3_translateZ";
	rename -uid "83202640-44E8-2F91-D481-BB8E9546D86C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -5.2819910049438477;
createNode animCurveTU -n "Character1_RightHandRing3_scaleX";
	rename -uid "954EF998-4391-F3FD-036E-7FBA641054C0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing3_scaleY";
	rename -uid "181114C1-46E1-09AB-4179-C19748EEE0DC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTU -n "Character1_RightHandRing3_scaleZ";
	rename -uid "A2FA659D-4D1A-9519-D33C-0CA630291A15";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.0000001192092896;
createNode animCurveTA -n "Character1_RightHandRing3_rotateX";
	rename -uid "DBFCCD6A-4B98-3BB8-AE0E-C8B7B34F030C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.0837162512398208e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateY";
	rename -uid "C6C08B66-4E91-DCDA-1641-CE80AC44F27A";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -3.3234505281143356e-007;
createNode animCurveTA -n "Character1_RightHandRing3_rotateZ";
	rename -uid "C38749B3-4BDD-A1D4-CC66-E5A63B49C0DB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -2.9096332809785963e-007;
createNode animCurveTU -n "Character1_RightHandRing3_visibility";
	rename -uid "A1E4B877-4B8C-85A8-8A2A-14A7BEB996C3";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Neck_translateX";
	rename -uid "8F896043-4854-9964-2E90-BA883D163F6E";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.1971182823181152;
createNode animCurveTL -n "Character1_Neck_translateY";
	rename -uid "E8484FAC-4CE1-650C-EA5C-2A81E06526E0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -13.621223449707031;
createNode animCurveTL -n "Character1_Neck_translateZ";
	rename -uid "9418D8C4-405D-2F1A-F320-039344805F75";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 7.1444897651672363;
createNode animCurveTU -n "Character1_Neck_scaleX";
	rename -uid "F9BCAA17-4099-B38C-9CD2-7AB21DE63327";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999970197677612;
createNode animCurveTU -n "Character1_Neck_scaleY";
	rename -uid "57A440F9-485B-A391-C334-2E83899A7EBD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Neck_scaleZ";
	rename -uid "486F4C80-45FF-203D-D47D-D9A051FC296C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.9999997615814209;
createNode animCurveTA -n "Character1_Neck_rotateX";
	rename -uid "8772AEFD-4F55-7843-0709-A3A482A54A29";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -12.113608360290527 1 -12.256378173828125
		 2 -12.447529792785645 3 -12.613641738891602 4 -12.681133270263672 5 -12.578037261962891
		 6 -12.235085487365723 7 -11.586178779602051 8 -10.568342208862305 9 -9.1477746963500977
		 10 -7.445258140563964 11 -5.6330342292785645 12 -3.886343240737915 13 -2.3852159976959229
		 14 -1.3148082494735718 15 -0.86301302909851074 16 -0.87279075384140015 17 -1.0786309242248535
		 18 -1.5042432546615601 19 -2.1711781024932861 20 -3.0975806713104248 21 -4.2976884841918945
		 22 -5.7822155952453613 23 -7.5598258972167969 24 -9.1749105453491211 25 -10.284521102905273
		 26 -11.03233814239502 27 -11.563311576843262 28 -12.022279739379883 29 -12.553716659545898
		 30 -12.113608360290527;
createNode animCurveTA -n "Character1_Neck_rotateY";
	rename -uid "FAF49D11-45CC-DD4C-DC4D-B5AB1E8B3F6C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -24.177896499633789 1 -23.962600708007813
		 2 -23.387510299682617 3 -22.505376815795898 4 -21.368865966796875 5 -20.031108856201172
		 6 -18.546045303344727 7 -16.968696594238281 8 -15.355415344238279 9 -13.559041023254395
		 10 -11.518758773803711 11 -9.4222640991210937 12 -7.4579267501831055 13 -5.813941478729248
		 14 -4.6774544715881348 15 -4.234309196472168 16 -4.2975807189941406 17 -4.5743751525878906
		 18 -5.0910272598266602 19 -5.8755698204040527 20 -6.9588394165039062 21 -8.3747539520263672
		 22 -10.1597900390625 23 -12.351827621459961 24 -14.691095352172852 25 -16.918058395385742
		 26 -19.025768280029297 27 -21.00953483581543 28 -22.866830825805664 29 -24.597043991088867
		 30 -24.177896499633789;
createNode animCurveTA -n "Character1_Neck_rotateZ";
	rename -uid "09EC3E78-43F4-9F43-A639-3AAD36649E9D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 1.5711681842803955 1 1.5636390447616577
		 2 1.6058650016784668 3 1.7028061151504517 4 1.8594050407409668 5 2.0830323696136475
		 6 2.3855338096618652 7 2.7848854064941406 8 3.3065776824951172 9 4.1423673629760742
		 10 5.3423371315002441 11 6.7225289344787598 12 8.0941238403320312 13 9.2635955810546875
		 14 10.033736228942871 15 10.205685615539551 16 9.7469730377197266 17 8.8381748199462891
		 18 7.6009235382080078 19 6.1579170227050781 20 4.6336517333984375 21 3.1551096439361572
		 22 1.8523983955383303 23 0.85948747396469116 24 0.31181856989860535 25 0.2427998483181
		 26 0.48288890719413757 27 0.8717724084854126 28 1.2537517547607422 29 1.47554612159729
		 30 1.5711681842803955;
createNode animCurveTU -n "Character1_Neck_visibility";
	rename -uid "EAA0F122-4941-6A05-229C-B0B8BC4289FB";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "Character1_Head_translateX";
	rename -uid "99A1BE95-411B-BBE1-B090-0EBC1D7D6A70";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -16.985572814941406;
createNode animCurveTL -n "Character1_Head_translateY";
	rename -uid "65247B3E-4A4E-7BD9-177C-0392B9EC7593";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -8.0131206512451172;
createNode animCurveTL -n "Character1_Head_translateZ";
	rename -uid "BA257D93-4C57-6970-320B-93A26B8A4E95";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -6.1724905967712402;
createNode animCurveTU -n "Character1_Head_scaleX";
	rename -uid "BBA76A7D-41C1-972E-422C-3D902831DAE5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTU -n "Character1_Head_scaleY";
	rename -uid "1A762927-48D9-8F20-DC5E-DCBE8605E90F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999988079071045;
createNode animCurveTU -n "Character1_Head_scaleZ";
	rename -uid "640EA0D4-4D02-245C-A235-1FB322986D43";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 0.99999994039535522;
createNode animCurveTA -n "Character1_Head_rotateX";
	rename -uid "89D1C25F-4051-D5CB-EA32-5D9BAD500B31";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -14.023781776428223 1 -13.223540306091309
		 2 -12.427944183349609 3 -11.633321762084961 4 -10.837213516235352 5 -10.038167953491211
		 6 -9.2355422973632812 7 -8.4292583465576172 8 -7.6194872856140128 9 -6.7252464294433594
		 10 -5.7265510559082031 11 -4.7103853225708008 12 -3.7591586112976074 13 -2.9539299011230469
		 14 -2.3772754669189453 15 -2.1142802238464355 16 -2.1506607532501221 17 -2.3947386741638184
		 18 -2.8164389133453369 19 -3.38551926612854 20 -4.0719232559204102 21 -4.8460550308227539
		 22 -5.6788835525512695 23 -6.5418491363525391 24 -7.5139837265014648 25 -8.6871242523193359
		 26 -9.9883441925048828 27 -11.343860626220703 28 -12.674691200256348 29 -13.89212703704834
		 30 -14.023781776428223;
createNode animCurveTA -n "Character1_Head_rotateY";
	rename -uid "659F8FA4-4CC5-77D0-698E-C2A7A784B1E9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 21.922052383422852 1 21.168807983398437
		 2 20.52003288269043 3 19.909893035888672 4 19.27252197265625 5 18.542049407958984
		 6 17.652612686157227 7 16.538366317749023 8 15.133449554443359 9 13.148990631103516
		 10 10.538307189941406 11 7.5993165969848624 12 4.6304965019226074 13 1.930513381958008
		 14 -0.20213660597801208 15 -1.4692484140396118 16 -1.9694349765777586 17 -2.0312738418579102
		 18 -1.7015863656997681 19 -1.0272053480148315 20 -0.055061966180801392 21 1.1677777767181396
		 22 2.5941274166107178 23 4.1767358779907227 24 6.1099710464477539 25 8.6265478134155273
		 26 11.51256275177002 27 14.554420471191406 28 17.53962516784668 29 20.257564544677734
		 30 21.922052383422852;
createNode animCurveTA -n "Character1_Head_rotateZ";
	rename -uid "A30C54EA-44C1-1450-655F-D298603DE781";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 4.3367552757263184 1 4.6480693817138672
		 2 4.9219379425048828 3 5.1730432510375977 4 5.4142179489135742 5 5.656531810760498
		 6 5.9096155166625977 7 6.1822371482849121 8 6.4831504821777344 9 6.9132928848266602
		 10 7.4928064346313477 11 8.1182003021240234 12 8.6956005096435547 13 9.1379594802856445
		 14 9.3616619110107422 15 9.2822628021240234 16 8.8313360214233398 17 8.0693349838256836
		 18 7.0989222526550293 19 6.0210304260253906 20 4.9364056587219238 21 3.9468657970428467
		 22 3.1562511920928955 23 2.6710939407348633 24 2.5242214202880859 25 2.6788907051086426
		 26 3.0080163478851318 27 3.3946590423583984 28 3.7347865104675293 29 3.9390354156494136
		 30 4.3367552757263184;
createNode animCurveTU -n "Character1_Head_visibility";
	rename -uid "AD0B6415-42A6-91C9-CCA2-FF90297A67AF";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "jaw_translateX";
	rename -uid "951F6252-4121-13D3-F660-8097FA1D94F9";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 14.004427909851074;
createNode animCurveTL -n "jaw_translateY";
	rename -uid "54631FB1-4FDD-F673-3BE4-2EAF36A590D3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 6.9922315685958317e-015;
createNode animCurveTL -n "jaw_translateZ";
	rename -uid "ABBCFE66-4007-93AC-665B-B8ADE5238CF8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9713712501773515e-015;
createNode animCurveTU -n "jaw_scaleX";
	rename -uid "EE8B23FB-408D-6F71-5A74-0E832B4EDD3B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleY";
	rename -uid "6B808218-4212-DE35-A910-6CAD16FA8499";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "jaw_scaleZ";
	rename -uid "561A162F-4AAC-34FB-1F03-0D894D07C2D2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "jaw_rotateX";
	rename -uid "7CB90BEE-4B6E-7780-A230-2DBF97CCA4E8";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -7.3103938102722177;
createNode animCurveTA -n "jaw_rotateY";
	rename -uid "465E469D-4BCA-4AA2-B579-BC928E8740B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 2.9145002383712537e-013;
createNode animCurveTA -n "jaw_rotateZ";
	rename -uid "6092BFE5-412F-1236-8905-A7B43A114DBB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1.1480868881454109e-013;
createNode animCurveTU -n "jaw_visibility";
	rename -uid "A33F7C02-45D7-7ADA-A7E3-839C40F5CD74";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "eyes_translateX";
	rename -uid "D23B3F9A-4A7D-4FF8-99AB-0191C80D0FDD";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 -10.20706844329834;
createNode animCurveTL -n "eyes_translateY";
	rename -uid "F483CFC6-48EB-850F-AB65-D6A4136C7DBB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 22.342548370361328;
createNode animCurveTL -n "eyes_translateZ";
	rename -uid "B094A28E-4678-9544-F07E-838C6BC1BE5F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 3.7847360800924434e-008;
createNode animCurveTU -n "eyes_scaleX";
	rename -uid "CD6FBFBF-4BD4-5114-6285-A7A71F318ABA";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleY";
	rename -uid "20094CDC-4365-254C-7F53-ED9DE263DEED";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTU -n "eyes_scaleZ";
	rename -uid "13B66064-48A4-D89C-9247-A099635B274F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
createNode animCurveTA -n "eyes_rotateX";
	rename -uid "F948D66B-4AD8-3753-D27E-9A9F340FC8F3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -7.6373577117919931 1 -7.5049901008605966
		 2 -7.3672623634338379 3 -7.2281951904296884 4 -7.0918078422546387 5 -6.9621200561523437
		 6 -6.8431510925292969 7 -6.7389206886291504 8 -6.6383757591247559 9 -6.5369091033935547
		 10 -6.4482083320617676 11 -6.3859620094299316 12 -6.3638577461242676 13 -6.3955850601196289
		 14 -6.4976105690002441 15 -6.6612272262573242 16 -6.8623614311218262 17 -7.0769386291503906
		 18 -7.2808842658996582 19 -7.4501247406005859 20 -7.5605845451354972 21 -7.6212096214294434
		 22 -7.6596102714538583 23 -7.6797075271606445 24 -7.6854243278503418 25 -7.6806812286376953
		 26 -7.6694021224975577 27 -7.655508041381835 28 -7.6429214477539062 29 -7.6355638504028329
		 30 -7.6355638504028329;
createNode animCurveTA -n "eyes_rotateY";
	rename -uid "E1757F0D-4CFC-3582-B5BD-54A5FE652776";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 -0.17832925915718079 1 -0.03190237283706665
		 2 0.14132377505302429 3 0.32124972343444824 4 0.48777610063552862 5 0.6208033561706543
		 6 0.70023208856582642 7 0.7059628963470459 8 0.59759330749511719 9 0.37902787327766418
		 10 0.09662821888923645 11 -0.20324438810348511 12 -0.47422850131988525 13 -0.66996270418167114
		 14 -0.80482304096221924 15 -0.92221528291702271 16 -1.0193222761154175 17 -1.0933270454406738
		 18 -1.1414127349853516 19 -1.1607619524002075 20 -1.1485580205917358 21 -1.1066988706588745
		 22 -1.0416843891143799 23 -0.95760124921798717 24 -0.8585352897644043 25 -0.7485727071762085
		 26 -0.63179993629455566 27 -0.51230299472808838 28 -0.39416807889938354 29 -0.28148144483566284
		 30 -0.17832925915718079;
createNode animCurveTA -n "eyes_rotateZ";
	rename -uid "7E11824F-4020-8C4C-4D1B-90B862287EC5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 31 ".ktv[0:30]"  0 0.024373672902584076 1 -0.40484324097633362
		 2 -0.88595765829086304 3 -1.3800464868545532 4 -1.8481864929199217 5 -2.2514545917510986
		 6 -2.5509278774261475 7 -2.7076833248138428 8 -2.6467368602752686 9 -2.3763020038604736
		 10 -1.9822499752044678 11 -1.5504531860351562 12 -1.166783332824707 13 -0.91711229085922241
		 14 -0.79896974563598633 15 -0.74048680067062378 16 -0.72343772649765015 17 -0.72959667444229126
		 18 -0.74073785543441772 19 -0.7386355996131897 20 -0.70506393909454346 21 -0.64634561538696289
		 22 -0.58165609836578369 23 -0.51204913854598999 24 -0.43857851624488831 25 -0.36229795217514038
		 26 -0.28426113724708557 27 -0.2055218368768692 28 -0.12713377177715302 29 -0.05015069991350174
		 30 0.024373672902584076;
createNode animCurveTU -n "eyes_visibility";
	rename -uid "3078E54A-477D-07E4-A1D6-B69344B0D992";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  0 1;
	setAttr ".kot[0]"  17;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "FC55B11F-46F4-466C-7A16-5AB9BE1BD0D4";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "BF6A2643-4EEB-429E-4308-1E805F625FFD";
createNode displayLayer -n "defaultLayer";
	rename -uid "E5C15C40-4044-8D8A-1076-A6AC3281A76E";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "F4805540-4603-2DD4-37BF-E3ADF59DC349";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "DB45E644-4DDC-0C6C-3D5E-6A802ED6A6FB";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "8823F20C-40F1-4DAE-D64A-419ACC4A2539";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 0\n                -polymeshes 1\n                -subdivSurfaces 0\n                -planes 0\n                -lights 0\n                -cameras 0\n                -controlVertices 0\n                -hulls 0\n                -grid 1\n                -imagePlane 0\n                -joints 1\n"
		+ "                -ikHandles 0\n                -deformers 0\n                -dynamics 0\n                -particleInstancers 0\n                -fluids 0\n                -hairSystems 0\n                -follicles 0\n                -nCloths 0\n                -nParticles 0\n                -nRigids 0\n                -dynamicConstraints 0\n                -locators 0\n                -manipulators 1\n                -pluginShapes 0\n                -dimensions 0\n                -handles 0\n                -pivots 0\n                -textures 0\n                -strokes 0\n                -motionTrails 0\n                -clipGhosts 0\n                -greasePencils 0\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1239\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 0 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 0\n            -polymeshes 1\n            -subdivSurfaces 0\n"
		+ "            -planes 0\n            -lights 0\n            -cameras 0\n            -controlVertices 0\n            -hulls 0\n            -grid 1\n            -imagePlane 0\n            -joints 1\n            -ikHandles 0\n            -deformers 0\n            -dynamics 0\n            -particleInstancers 0\n            -fluids 0\n            -hairSystems 0\n            -follicles 0\n            -nCloths 0\n            -nParticles 0\n            -nRigids 0\n            -dynamicConstraints 0\n            -locators 0\n            -manipulators 1\n            -pluginShapes 0\n            -dimensions 0\n            -handles 0\n            -pivots 0\n            -textures 0\n            -strokes 0\n            -motionTrails 0\n            -clipGhosts 0\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1239\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 0 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n"
		+ "                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n"
		+ "                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n"
		+ "                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n"
		+ "                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 24 100 -ps 2 76 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 0\\n    -polymeshes 1\\n    -subdivSurfaces 0\\n    -planes 0\\n    -lights 0\\n    -cameras 0\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 1\\n    -ikHandles 0\\n    -deformers 0\\n    -dynamics 0\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 0\\n    -handles 0\\n    -pivots 0\\n    -textures 0\\n    -strokes 0\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1239\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 0 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 0\\n    -polymeshes 1\\n    -subdivSurfaces 0\\n    -planes 0\\n    -lights 0\\n    -cameras 0\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 1\\n    -ikHandles 0\\n    -deformers 0\\n    -dynamics 0\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 0\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 0\\n    -handles 0\\n    -pivots 0\\n    -textures 0\\n    -strokes 0\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1239\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 0 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "DC11053B-4456-0B8F-3671-8F977F2D277D";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 30 -ast 0 -aet 30 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 0;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
connectAttr "ref_frame.s" "Character1_Hips.is";
connectAttr "Character1_Hips_scaleX.o" "Character1_Hips.sx";
connectAttr "Character1_Hips_scaleY.o" "Character1_Hips.sy";
connectAttr "Character1_Hips_scaleZ.o" "Character1_Hips.sz";
connectAttr "Character1_Hips_translateX.o" "Character1_Hips.tx";
connectAttr "Character1_Hips_translateY.o" "Character1_Hips.ty";
connectAttr "Character1_Hips_translateZ.o" "Character1_Hips.tz";
connectAttr "Character1_Hips_rotateX.o" "Character1_Hips.rx";
connectAttr "Character1_Hips_rotateY.o" "Character1_Hips.ry";
connectAttr "Character1_Hips_rotateZ.o" "Character1_Hips.rz";
connectAttr "Character1_Hips_visibility.o" "Character1_Hips.v";
connectAttr "Character1_Hips.s" "Character1_LeftUpLeg.is";
connectAttr "Character1_LeftUpLeg_scaleX.o" "Character1_LeftUpLeg.sx";
connectAttr "Character1_LeftUpLeg_scaleY.o" "Character1_LeftUpLeg.sy";
connectAttr "Character1_LeftUpLeg_scaleZ.o" "Character1_LeftUpLeg.sz";
connectAttr "Character1_LeftUpLeg_translateX.o" "Character1_LeftUpLeg.tx";
connectAttr "Character1_LeftUpLeg_translateY.o" "Character1_LeftUpLeg.ty";
connectAttr "Character1_LeftUpLeg_translateZ.o" "Character1_LeftUpLeg.tz";
connectAttr "Character1_LeftUpLeg_rotateX.o" "Character1_LeftUpLeg.rx";
connectAttr "Character1_LeftUpLeg_rotateY.o" "Character1_LeftUpLeg.ry";
connectAttr "Character1_LeftUpLeg_rotateZ.o" "Character1_LeftUpLeg.rz";
connectAttr "Character1_LeftUpLeg_visibility.o" "Character1_LeftUpLeg.v";
connectAttr "Character1_LeftUpLeg.s" "Character1_LeftLeg.is";
connectAttr "Character1_LeftLeg_scaleX.o" "Character1_LeftLeg.sx";
connectAttr "Character1_LeftLeg_scaleY.o" "Character1_LeftLeg.sy";
connectAttr "Character1_LeftLeg_scaleZ.o" "Character1_LeftLeg.sz";
connectAttr "Character1_LeftLeg_translateX.o" "Character1_LeftLeg.tx";
connectAttr "Character1_LeftLeg_translateY.o" "Character1_LeftLeg.ty";
connectAttr "Character1_LeftLeg_translateZ.o" "Character1_LeftLeg.tz";
connectAttr "Character1_LeftLeg_rotateX.o" "Character1_LeftLeg.rx";
connectAttr "Character1_LeftLeg_rotateY.o" "Character1_LeftLeg.ry";
connectAttr "Character1_LeftLeg_rotateZ.o" "Character1_LeftLeg.rz";
connectAttr "Character1_LeftLeg_visibility.o" "Character1_LeftLeg.v";
connectAttr "Character1_LeftLeg.s" "Character1_LeftFoot.is";
connectAttr "Character1_LeftFoot_scaleX.o" "Character1_LeftFoot.sx";
connectAttr "Character1_LeftFoot_scaleY.o" "Character1_LeftFoot.sy";
connectAttr "Character1_LeftFoot_scaleZ.o" "Character1_LeftFoot.sz";
connectAttr "Character1_LeftFoot_translateX.o" "Character1_LeftFoot.tx";
connectAttr "Character1_LeftFoot_translateY.o" "Character1_LeftFoot.ty";
connectAttr "Character1_LeftFoot_translateZ.o" "Character1_LeftFoot.tz";
connectAttr "Character1_LeftFoot_rotateX.o" "Character1_LeftFoot.rx";
connectAttr "Character1_LeftFoot_rotateY.o" "Character1_LeftFoot.ry";
connectAttr "Character1_LeftFoot_rotateZ.o" "Character1_LeftFoot.rz";
connectAttr "Character1_LeftFoot_visibility.o" "Character1_LeftFoot.v";
connectAttr "Character1_LeftFoot.s" "Character1_LeftToeBase.is";
connectAttr "Character1_LeftToeBase_scaleX.o" "Character1_LeftToeBase.sx";
connectAttr "Character1_LeftToeBase_scaleY.o" "Character1_LeftToeBase.sy";
connectAttr "Character1_LeftToeBase_scaleZ.o" "Character1_LeftToeBase.sz";
connectAttr "Character1_LeftToeBase_translateX.o" "Character1_LeftToeBase.tx";
connectAttr "Character1_LeftToeBase_translateY.o" "Character1_LeftToeBase.ty";
connectAttr "Character1_LeftToeBase_translateZ.o" "Character1_LeftToeBase.tz";
connectAttr "Character1_LeftToeBase_rotateX.o" "Character1_LeftToeBase.rx";
connectAttr "Character1_LeftToeBase_rotateY.o" "Character1_LeftToeBase.ry";
connectAttr "Character1_LeftToeBase_rotateZ.o" "Character1_LeftToeBase.rz";
connectAttr "Character1_LeftToeBase_visibility.o" "Character1_LeftToeBase.v";
connectAttr "Character1_LeftToeBase.s" "Character1_LeftFootMiddle1.is";
connectAttr "Character1_LeftFootMiddle1_scaleX.o" "Character1_LeftFootMiddle1.sx"
		;
connectAttr "Character1_LeftFootMiddle1_scaleY.o" "Character1_LeftFootMiddle1.sy"
		;
connectAttr "Character1_LeftFootMiddle1_scaleZ.o" "Character1_LeftFootMiddle1.sz"
		;
connectAttr "Character1_LeftFootMiddle1_translateX.o" "Character1_LeftFootMiddle1.tx"
		;
connectAttr "Character1_LeftFootMiddle1_translateY.o" "Character1_LeftFootMiddle1.ty"
		;
connectAttr "Character1_LeftFootMiddle1_translateZ.o" "Character1_LeftFootMiddle1.tz"
		;
connectAttr "Character1_LeftFootMiddle1_rotateX.o" "Character1_LeftFootMiddle1.rx"
		;
connectAttr "Character1_LeftFootMiddle1_rotateY.o" "Character1_LeftFootMiddle1.ry"
		;
connectAttr "Character1_LeftFootMiddle1_rotateZ.o" "Character1_LeftFootMiddle1.rz"
		;
connectAttr "Character1_LeftFootMiddle1_visibility.o" "Character1_LeftFootMiddle1.v"
		;
connectAttr "Character1_LeftFootMiddle1.s" "Character1_LeftFootMiddle2.is";
connectAttr "Character1_LeftFootMiddle2_translateX.o" "Character1_LeftFootMiddle2.tx"
		;
connectAttr "Character1_LeftFootMiddle2_translateY.o" "Character1_LeftFootMiddle2.ty"
		;
connectAttr "Character1_LeftFootMiddle2_translateZ.o" "Character1_LeftFootMiddle2.tz"
		;
connectAttr "Character1_LeftFootMiddle2_scaleX.o" "Character1_LeftFootMiddle2.sx"
		;
connectAttr "Character1_LeftFootMiddle2_scaleY.o" "Character1_LeftFootMiddle2.sy"
		;
connectAttr "Character1_LeftFootMiddle2_scaleZ.o" "Character1_LeftFootMiddle2.sz"
		;
connectAttr "Character1_LeftFootMiddle2_rotateX.o" "Character1_LeftFootMiddle2.rx"
		;
connectAttr "Character1_LeftFootMiddle2_rotateY.o" "Character1_LeftFootMiddle2.ry"
		;
connectAttr "Character1_LeftFootMiddle2_rotateZ.o" "Character1_LeftFootMiddle2.rz"
		;
connectAttr "Character1_LeftFootMiddle2_visibility.o" "Character1_LeftFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_RightUpLeg.is";
connectAttr "Character1_RightUpLeg_scaleX.o" "Character1_RightUpLeg.sx";
connectAttr "Character1_RightUpLeg_scaleY.o" "Character1_RightUpLeg.sy";
connectAttr "Character1_RightUpLeg_scaleZ.o" "Character1_RightUpLeg.sz";
connectAttr "Character1_RightUpLeg_translateX.o" "Character1_RightUpLeg.tx";
connectAttr "Character1_RightUpLeg_translateY.o" "Character1_RightUpLeg.ty";
connectAttr "Character1_RightUpLeg_translateZ.o" "Character1_RightUpLeg.tz";
connectAttr "Character1_RightUpLeg_rotateX.o" "Character1_RightUpLeg.rx";
connectAttr "Character1_RightUpLeg_rotateY.o" "Character1_RightUpLeg.ry";
connectAttr "Character1_RightUpLeg_rotateZ.o" "Character1_RightUpLeg.rz";
connectAttr "Character1_RightUpLeg_visibility.o" "Character1_RightUpLeg.v";
connectAttr "Character1_RightUpLeg.s" "Character1_RightLeg.is";
connectAttr "Character1_RightLeg_scaleX.o" "Character1_RightLeg.sx";
connectAttr "Character1_RightLeg_scaleY.o" "Character1_RightLeg.sy";
connectAttr "Character1_RightLeg_scaleZ.o" "Character1_RightLeg.sz";
connectAttr "Character1_RightLeg_translateX.o" "Character1_RightLeg.tx";
connectAttr "Character1_RightLeg_translateY.o" "Character1_RightLeg.ty";
connectAttr "Character1_RightLeg_translateZ.o" "Character1_RightLeg.tz";
connectAttr "Character1_RightLeg_rotateX.o" "Character1_RightLeg.rx";
connectAttr "Character1_RightLeg_rotateY.o" "Character1_RightLeg.ry";
connectAttr "Character1_RightLeg_rotateZ.o" "Character1_RightLeg.rz";
connectAttr "Character1_RightLeg_visibility.o" "Character1_RightLeg.v";
connectAttr "Character1_RightLeg.s" "Character1_RightFoot.is";
connectAttr "Character1_RightFoot_scaleX.o" "Character1_RightFoot.sx";
connectAttr "Character1_RightFoot_scaleY.o" "Character1_RightFoot.sy";
connectAttr "Character1_RightFoot_scaleZ.o" "Character1_RightFoot.sz";
connectAttr "Character1_RightFoot_translateX.o" "Character1_RightFoot.tx";
connectAttr "Character1_RightFoot_translateY.o" "Character1_RightFoot.ty";
connectAttr "Character1_RightFoot_translateZ.o" "Character1_RightFoot.tz";
connectAttr "Character1_RightFoot_rotateX.o" "Character1_RightFoot.rx";
connectAttr "Character1_RightFoot_rotateY.o" "Character1_RightFoot.ry";
connectAttr "Character1_RightFoot_rotateZ.o" "Character1_RightFoot.rz";
connectAttr "Character1_RightFoot_visibility.o" "Character1_RightFoot.v";
connectAttr "Character1_RightFoot.s" "Character1_RightToeBase.is";
connectAttr "Character1_RightToeBase_scaleX.o" "Character1_RightToeBase.sx";
connectAttr "Character1_RightToeBase_scaleY.o" "Character1_RightToeBase.sy";
connectAttr "Character1_RightToeBase_scaleZ.o" "Character1_RightToeBase.sz";
connectAttr "Character1_RightToeBase_translateX.o" "Character1_RightToeBase.tx";
connectAttr "Character1_RightToeBase_translateY.o" "Character1_RightToeBase.ty";
connectAttr "Character1_RightToeBase_translateZ.o" "Character1_RightToeBase.tz";
connectAttr "Character1_RightToeBase_rotateX.o" "Character1_RightToeBase.rx";
connectAttr "Character1_RightToeBase_rotateY.o" "Character1_RightToeBase.ry";
connectAttr "Character1_RightToeBase_rotateZ.o" "Character1_RightToeBase.rz";
connectAttr "Character1_RightToeBase_visibility.o" "Character1_RightToeBase.v";
connectAttr "Character1_RightToeBase.s" "Character1_RightFootMiddle1.is";
connectAttr "Character1_RightFootMiddle1_scaleX.o" "Character1_RightFootMiddle1.sx"
		;
connectAttr "Character1_RightFootMiddle1_scaleY.o" "Character1_RightFootMiddle1.sy"
		;
connectAttr "Character1_RightFootMiddle1_scaleZ.o" "Character1_RightFootMiddle1.sz"
		;
connectAttr "Character1_RightFootMiddle1_translateX.o" "Character1_RightFootMiddle1.tx"
		;
connectAttr "Character1_RightFootMiddle1_translateY.o" "Character1_RightFootMiddle1.ty"
		;
connectAttr "Character1_RightFootMiddle1_translateZ.o" "Character1_RightFootMiddle1.tz"
		;
connectAttr "Character1_RightFootMiddle1_rotateX.o" "Character1_RightFootMiddle1.rx"
		;
connectAttr "Character1_RightFootMiddle1_rotateY.o" "Character1_RightFootMiddle1.ry"
		;
connectAttr "Character1_RightFootMiddle1_rotateZ.o" "Character1_RightFootMiddle1.rz"
		;
connectAttr "Character1_RightFootMiddle1_visibility.o" "Character1_RightFootMiddle1.v"
		;
connectAttr "Character1_RightFootMiddle1.s" "Character1_RightFootMiddle2.is";
connectAttr "Character1_RightFootMiddle2_translateX.o" "Character1_RightFootMiddle2.tx"
		;
connectAttr "Character1_RightFootMiddle2_translateY.o" "Character1_RightFootMiddle2.ty"
		;
connectAttr "Character1_RightFootMiddle2_translateZ.o" "Character1_RightFootMiddle2.tz"
		;
connectAttr "Character1_RightFootMiddle2_scaleX.o" "Character1_RightFootMiddle2.sx"
		;
connectAttr "Character1_RightFootMiddle2_scaleY.o" "Character1_RightFootMiddle2.sy"
		;
connectAttr "Character1_RightFootMiddle2_scaleZ.o" "Character1_RightFootMiddle2.sz"
		;
connectAttr "Character1_RightFootMiddle2_rotateX.o" "Character1_RightFootMiddle2.rx"
		;
connectAttr "Character1_RightFootMiddle2_rotateY.o" "Character1_RightFootMiddle2.ry"
		;
connectAttr "Character1_RightFootMiddle2_rotateZ.o" "Character1_RightFootMiddle2.rz"
		;
connectAttr "Character1_RightFootMiddle2_visibility.o" "Character1_RightFootMiddle2.v"
		;
connectAttr "Character1_Hips.s" "Character1_Spine.is";
connectAttr "Character1_Spine_scaleX.o" "Character1_Spine.sx";
connectAttr "Character1_Spine_scaleY.o" "Character1_Spine.sy";
connectAttr "Character1_Spine_scaleZ.o" "Character1_Spine.sz";
connectAttr "Character1_Spine_translateX.o" "Character1_Spine.tx";
connectAttr "Character1_Spine_translateY.o" "Character1_Spine.ty";
connectAttr "Character1_Spine_translateZ.o" "Character1_Spine.tz";
connectAttr "Character1_Spine_rotateX.o" "Character1_Spine.rx";
connectAttr "Character1_Spine_rotateY.o" "Character1_Spine.ry";
connectAttr "Character1_Spine_rotateZ.o" "Character1_Spine.rz";
connectAttr "Character1_Spine_visibility.o" "Character1_Spine.v";
connectAttr "Character1_Spine.s" "Character1_Spine1.is";
connectAttr "Character1_Spine1_scaleX.o" "Character1_Spine1.sx";
connectAttr "Character1_Spine1_scaleY.o" "Character1_Spine1.sy";
connectAttr "Character1_Spine1_scaleZ.o" "Character1_Spine1.sz";
connectAttr "Character1_Spine1_translateX.o" "Character1_Spine1.tx";
connectAttr "Character1_Spine1_translateY.o" "Character1_Spine1.ty";
connectAttr "Character1_Spine1_translateZ.o" "Character1_Spine1.tz";
connectAttr "Character1_Spine1_rotateX.o" "Character1_Spine1.rx";
connectAttr "Character1_Spine1_rotateY.o" "Character1_Spine1.ry";
connectAttr "Character1_Spine1_rotateZ.o" "Character1_Spine1.rz";
connectAttr "Character1_Spine1_visibility.o" "Character1_Spine1.v";
connectAttr "Character1_Spine1.s" "Character1_LeftShoulder.is";
connectAttr "Character1_LeftShoulder_scaleX.o" "Character1_LeftShoulder.sx";
connectAttr "Character1_LeftShoulder_scaleY.o" "Character1_LeftShoulder.sy";
connectAttr "Character1_LeftShoulder_scaleZ.o" "Character1_LeftShoulder.sz";
connectAttr "Character1_LeftShoulder_translateX.o" "Character1_LeftShoulder.tx";
connectAttr "Character1_LeftShoulder_translateY.o" "Character1_LeftShoulder.ty";
connectAttr "Character1_LeftShoulder_translateZ.o" "Character1_LeftShoulder.tz";
connectAttr "Character1_LeftShoulder_rotateX.o" "Character1_LeftShoulder.rx";
connectAttr "Character1_LeftShoulder_rotateY.o" "Character1_LeftShoulder.ry";
connectAttr "Character1_LeftShoulder_rotateZ.o" "Character1_LeftShoulder.rz";
connectAttr "Character1_LeftShoulder_visibility.o" "Character1_LeftShoulder.v";
connectAttr "Character1_LeftShoulder.s" "Character1_LeftArm.is";
connectAttr "Character1_LeftArm_scaleX.o" "Character1_LeftArm.sx";
connectAttr "Character1_LeftArm_scaleY.o" "Character1_LeftArm.sy";
connectAttr "Character1_LeftArm_scaleZ.o" "Character1_LeftArm.sz";
connectAttr "Character1_LeftArm_translateX.o" "Character1_LeftArm.tx";
connectAttr "Character1_LeftArm_translateY.o" "Character1_LeftArm.ty";
connectAttr "Character1_LeftArm_translateZ.o" "Character1_LeftArm.tz";
connectAttr "Character1_LeftArm_rotateX.o" "Character1_LeftArm.rx";
connectAttr "Character1_LeftArm_rotateY.o" "Character1_LeftArm.ry";
connectAttr "Character1_LeftArm_rotateZ.o" "Character1_LeftArm.rz";
connectAttr "Character1_LeftArm_visibility.o" "Character1_LeftArm.v";
connectAttr "Character1_LeftArm.s" "Character1_LeftForeArm.is";
connectAttr "Character1_LeftForeArm_scaleX.o" "Character1_LeftForeArm.sx";
connectAttr "Character1_LeftForeArm_scaleY.o" "Character1_LeftForeArm.sy";
connectAttr "Character1_LeftForeArm_scaleZ.o" "Character1_LeftForeArm.sz";
connectAttr "Character1_LeftForeArm_translateX.o" "Character1_LeftForeArm.tx";
connectAttr "Character1_LeftForeArm_translateY.o" "Character1_LeftForeArm.ty";
connectAttr "Character1_LeftForeArm_translateZ.o" "Character1_LeftForeArm.tz";
connectAttr "Character1_LeftForeArm_rotateX.o" "Character1_LeftForeArm.rx";
connectAttr "Character1_LeftForeArm_rotateY.o" "Character1_LeftForeArm.ry";
connectAttr "Character1_LeftForeArm_rotateZ.o" "Character1_LeftForeArm.rz";
connectAttr "Character1_LeftForeArm_visibility.o" "Character1_LeftForeArm.v";
connectAttr "Character1_LeftForeArm.s" "Character1_LeftHand.is";
connectAttr "Character1_LeftHand_scaleX.o" "Character1_LeftHand.sx";
connectAttr "Character1_LeftHand_scaleY.o" "Character1_LeftHand.sy";
connectAttr "Character1_LeftHand_scaleZ.o" "Character1_LeftHand.sz";
connectAttr "Character1_LeftHand_translateX.o" "Character1_LeftHand.tx";
connectAttr "Character1_LeftHand_translateY.o" "Character1_LeftHand.ty";
connectAttr "Character1_LeftHand_translateZ.o" "Character1_LeftHand.tz";
connectAttr "Character1_LeftHand_rotateX.o" "Character1_LeftHand.rx";
connectAttr "Character1_LeftHand_rotateY.o" "Character1_LeftHand.ry";
connectAttr "Character1_LeftHand_rotateZ.o" "Character1_LeftHand.rz";
connectAttr "Character1_LeftHand_visibility.o" "Character1_LeftHand.v";
connectAttr "Character1_LeftHand.s" "Character1_LeftHandThumb1.is";
connectAttr "Character1_LeftHandThumb1_scaleX.o" "Character1_LeftHandThumb1.sx";
connectAttr "Character1_LeftHandThumb1_scaleY.o" "Character1_LeftHandThumb1.sy";
connectAttr "Character1_LeftHandThumb1_scaleZ.o" "Character1_LeftHandThumb1.sz";
connectAttr "Character1_LeftHandThumb1_translateX.o" "Character1_LeftHandThumb1.tx"
		;
connectAttr "Character1_LeftHandThumb1_translateY.o" "Character1_LeftHandThumb1.ty"
		;
connectAttr "Character1_LeftHandThumb1_translateZ.o" "Character1_LeftHandThumb1.tz"
		;
connectAttr "Character1_LeftHandThumb1_rotateX.o" "Character1_LeftHandThumb1.rx"
		;
connectAttr "Character1_LeftHandThumb1_rotateY.o" "Character1_LeftHandThumb1.ry"
		;
connectAttr "Character1_LeftHandThumb1_rotateZ.o" "Character1_LeftHandThumb1.rz"
		;
connectAttr "Character1_LeftHandThumb1_visibility.o" "Character1_LeftHandThumb1.v"
		;
connectAttr "Character1_LeftHandThumb1.s" "Character1_LeftHandThumb2.is";
connectAttr "Character1_LeftHandThumb2_scaleX.o" "Character1_LeftHandThumb2.sx";
connectAttr "Character1_LeftHandThumb2_scaleY.o" "Character1_LeftHandThumb2.sy";
connectAttr "Character1_LeftHandThumb2_scaleZ.o" "Character1_LeftHandThumb2.sz";
connectAttr "Character1_LeftHandThumb2_translateX.o" "Character1_LeftHandThumb2.tx"
		;
connectAttr "Character1_LeftHandThumb2_translateY.o" "Character1_LeftHandThumb2.ty"
		;
connectAttr "Character1_LeftHandThumb2_translateZ.o" "Character1_LeftHandThumb2.tz"
		;
connectAttr "Character1_LeftHandThumb2_rotateX.o" "Character1_LeftHandThumb2.rx"
		;
connectAttr "Character1_LeftHandThumb2_rotateY.o" "Character1_LeftHandThumb2.ry"
		;
connectAttr "Character1_LeftHandThumb2_rotateZ.o" "Character1_LeftHandThumb2.rz"
		;
connectAttr "Character1_LeftHandThumb2_visibility.o" "Character1_LeftHandThumb2.v"
		;
connectAttr "Character1_LeftHandThumb2.s" "Character1_LeftHandThumb3.is";
connectAttr "Character1_LeftHandThumb3_translateX.o" "Character1_LeftHandThumb3.tx"
		;
connectAttr "Character1_LeftHandThumb3_translateY.o" "Character1_LeftHandThumb3.ty"
		;
connectAttr "Character1_LeftHandThumb3_translateZ.o" "Character1_LeftHandThumb3.tz"
		;
connectAttr "Character1_LeftHandThumb3_scaleX.o" "Character1_LeftHandThumb3.sx";
connectAttr "Character1_LeftHandThumb3_scaleY.o" "Character1_LeftHandThumb3.sy";
connectAttr "Character1_LeftHandThumb3_scaleZ.o" "Character1_LeftHandThumb3.sz";
connectAttr "Character1_LeftHandThumb3_rotateX.o" "Character1_LeftHandThumb3.rx"
		;
connectAttr "Character1_LeftHandThumb3_rotateY.o" "Character1_LeftHandThumb3.ry"
		;
connectAttr "Character1_LeftHandThumb3_rotateZ.o" "Character1_LeftHandThumb3.rz"
		;
connectAttr "Character1_LeftHandThumb3_visibility.o" "Character1_LeftHandThumb3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandIndex1.is";
connectAttr "Character1_LeftHandIndex1_scaleX.o" "Character1_LeftHandIndex1.sx";
connectAttr "Character1_LeftHandIndex1_scaleY.o" "Character1_LeftHandIndex1.sy";
connectAttr "Character1_LeftHandIndex1_scaleZ.o" "Character1_LeftHandIndex1.sz";
connectAttr "Character1_LeftHandIndex1_translateX.o" "Character1_LeftHandIndex1.tx"
		;
connectAttr "Character1_LeftHandIndex1_translateY.o" "Character1_LeftHandIndex1.ty"
		;
connectAttr "Character1_LeftHandIndex1_translateZ.o" "Character1_LeftHandIndex1.tz"
		;
connectAttr "Character1_LeftHandIndex1_rotateX.o" "Character1_LeftHandIndex1.rx"
		;
connectAttr "Character1_LeftHandIndex1_rotateY.o" "Character1_LeftHandIndex1.ry"
		;
connectAttr "Character1_LeftHandIndex1_rotateZ.o" "Character1_LeftHandIndex1.rz"
		;
connectAttr "Character1_LeftHandIndex1_visibility.o" "Character1_LeftHandIndex1.v"
		;
connectAttr "Character1_LeftHandIndex1.s" "Character1_LeftHandIndex2.is";
connectAttr "Character1_LeftHandIndex2_scaleX.o" "Character1_LeftHandIndex2.sx";
connectAttr "Character1_LeftHandIndex2_scaleY.o" "Character1_LeftHandIndex2.sy";
connectAttr "Character1_LeftHandIndex2_scaleZ.o" "Character1_LeftHandIndex2.sz";
connectAttr "Character1_LeftHandIndex2_translateX.o" "Character1_LeftHandIndex2.tx"
		;
connectAttr "Character1_LeftHandIndex2_translateY.o" "Character1_LeftHandIndex2.ty"
		;
connectAttr "Character1_LeftHandIndex2_translateZ.o" "Character1_LeftHandIndex2.tz"
		;
connectAttr "Character1_LeftHandIndex2_rotateX.o" "Character1_LeftHandIndex2.rx"
		;
connectAttr "Character1_LeftHandIndex2_rotateY.o" "Character1_LeftHandIndex2.ry"
		;
connectAttr "Character1_LeftHandIndex2_rotateZ.o" "Character1_LeftHandIndex2.rz"
		;
connectAttr "Character1_LeftHandIndex2_visibility.o" "Character1_LeftHandIndex2.v"
		;
connectAttr "Character1_LeftHandIndex2.s" "Character1_LeftHandIndex3.is";
connectAttr "Character1_LeftHandIndex3_translateX.o" "Character1_LeftHandIndex3.tx"
		;
connectAttr "Character1_LeftHandIndex3_translateY.o" "Character1_LeftHandIndex3.ty"
		;
connectAttr "Character1_LeftHandIndex3_translateZ.o" "Character1_LeftHandIndex3.tz"
		;
connectAttr "Character1_LeftHandIndex3_scaleX.o" "Character1_LeftHandIndex3.sx";
connectAttr "Character1_LeftHandIndex3_scaleY.o" "Character1_LeftHandIndex3.sy";
connectAttr "Character1_LeftHandIndex3_scaleZ.o" "Character1_LeftHandIndex3.sz";
connectAttr "Character1_LeftHandIndex3_rotateX.o" "Character1_LeftHandIndex3.rx"
		;
connectAttr "Character1_LeftHandIndex3_rotateY.o" "Character1_LeftHandIndex3.ry"
		;
connectAttr "Character1_LeftHandIndex3_rotateZ.o" "Character1_LeftHandIndex3.rz"
		;
connectAttr "Character1_LeftHandIndex3_visibility.o" "Character1_LeftHandIndex3.v"
		;
connectAttr "Character1_LeftHand.s" "Character1_LeftHandRing1.is";
connectAttr "Character1_LeftHandRing1_scaleX.o" "Character1_LeftHandRing1.sx";
connectAttr "Character1_LeftHandRing1_scaleY.o" "Character1_LeftHandRing1.sy";
connectAttr "Character1_LeftHandRing1_scaleZ.o" "Character1_LeftHandRing1.sz";
connectAttr "Character1_LeftHandRing1_translateX.o" "Character1_LeftHandRing1.tx"
		;
connectAttr "Character1_LeftHandRing1_translateY.o" "Character1_LeftHandRing1.ty"
		;
connectAttr "Character1_LeftHandRing1_translateZ.o" "Character1_LeftHandRing1.tz"
		;
connectAttr "Character1_LeftHandRing1_rotateX.o" "Character1_LeftHandRing1.rx";
connectAttr "Character1_LeftHandRing1_rotateY.o" "Character1_LeftHandRing1.ry";
connectAttr "Character1_LeftHandRing1_rotateZ.o" "Character1_LeftHandRing1.rz";
connectAttr "Character1_LeftHandRing1_visibility.o" "Character1_LeftHandRing1.v"
		;
connectAttr "Character1_LeftHandRing1.s" "Character1_LeftHandRing2.is";
connectAttr "Character1_LeftHandRing2_scaleX.o" "Character1_LeftHandRing2.sx";
connectAttr "Character1_LeftHandRing2_scaleY.o" "Character1_LeftHandRing2.sy";
connectAttr "Character1_LeftHandRing2_scaleZ.o" "Character1_LeftHandRing2.sz";
connectAttr "Character1_LeftHandRing2_translateX.o" "Character1_LeftHandRing2.tx"
		;
connectAttr "Character1_LeftHandRing2_translateY.o" "Character1_LeftHandRing2.ty"
		;
connectAttr "Character1_LeftHandRing2_translateZ.o" "Character1_LeftHandRing2.tz"
		;
connectAttr "Character1_LeftHandRing2_rotateX.o" "Character1_LeftHandRing2.rx";
connectAttr "Character1_LeftHandRing2_rotateY.o" "Character1_LeftHandRing2.ry";
connectAttr "Character1_LeftHandRing2_rotateZ.o" "Character1_LeftHandRing2.rz";
connectAttr "Character1_LeftHandRing2_visibility.o" "Character1_LeftHandRing2.v"
		;
connectAttr "Character1_LeftHandRing2.s" "Character1_LeftHandRing3.is";
connectAttr "Character1_LeftHandRing3_translateX.o" "Character1_LeftHandRing3.tx"
		;
connectAttr "Character1_LeftHandRing3_translateY.o" "Character1_LeftHandRing3.ty"
		;
connectAttr "Character1_LeftHandRing3_translateZ.o" "Character1_LeftHandRing3.tz"
		;
connectAttr "Character1_LeftHandRing3_scaleX.o" "Character1_LeftHandRing3.sx";
connectAttr "Character1_LeftHandRing3_scaleY.o" "Character1_LeftHandRing3.sy";
connectAttr "Character1_LeftHandRing3_scaleZ.o" "Character1_LeftHandRing3.sz";
connectAttr "Character1_LeftHandRing3_rotateX.o" "Character1_LeftHandRing3.rx";
connectAttr "Character1_LeftHandRing3_rotateY.o" "Character1_LeftHandRing3.ry";
connectAttr "Character1_LeftHandRing3_rotateZ.o" "Character1_LeftHandRing3.rz";
connectAttr "Character1_LeftHandRing3_visibility.o" "Character1_LeftHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_RightShoulder.is";
connectAttr "Character1_RightShoulder_scaleX.o" "Character1_RightShoulder.sx";
connectAttr "Character1_RightShoulder_scaleY.o" "Character1_RightShoulder.sy";
connectAttr "Character1_RightShoulder_scaleZ.o" "Character1_RightShoulder.sz";
connectAttr "Character1_RightShoulder_translateX.o" "Character1_RightShoulder.tx"
		;
connectAttr "Character1_RightShoulder_translateY.o" "Character1_RightShoulder.ty"
		;
connectAttr "Character1_RightShoulder_translateZ.o" "Character1_RightShoulder.tz"
		;
connectAttr "Character1_RightShoulder_rotateX.o" "Character1_RightShoulder.rx";
connectAttr "Character1_RightShoulder_rotateY.o" "Character1_RightShoulder.ry";
connectAttr "Character1_RightShoulder_rotateZ.o" "Character1_RightShoulder.rz";
connectAttr "Character1_RightShoulder_visibility.o" "Character1_RightShoulder.v"
		;
connectAttr "Character1_RightShoulder.s" "Character1_RightArm.is";
connectAttr "Character1_RightArm_scaleX.o" "Character1_RightArm.sx";
connectAttr "Character1_RightArm_scaleY.o" "Character1_RightArm.sy";
connectAttr "Character1_RightArm_scaleZ.o" "Character1_RightArm.sz";
connectAttr "Character1_RightArm_translateX.o" "Character1_RightArm.tx";
connectAttr "Character1_RightArm_translateY.o" "Character1_RightArm.ty";
connectAttr "Character1_RightArm_translateZ.o" "Character1_RightArm.tz";
connectAttr "Character1_RightArm_rotateX.o" "Character1_RightArm.rx";
connectAttr "Character1_RightArm_rotateY.o" "Character1_RightArm.ry";
connectAttr "Character1_RightArm_rotateZ.o" "Character1_RightArm.rz";
connectAttr "Character1_RightArm_visibility.o" "Character1_RightArm.v";
connectAttr "Character1_RightArm.s" "Character1_RightForeArm.is";
connectAttr "Character1_RightForeArm_scaleX.o" "Character1_RightForeArm.sx";
connectAttr "Character1_RightForeArm_scaleY.o" "Character1_RightForeArm.sy";
connectAttr "Character1_RightForeArm_scaleZ.o" "Character1_RightForeArm.sz";
connectAttr "Character1_RightForeArm_translateX.o" "Character1_RightForeArm.tx";
connectAttr "Character1_RightForeArm_translateY.o" "Character1_RightForeArm.ty";
connectAttr "Character1_RightForeArm_translateZ.o" "Character1_RightForeArm.tz";
connectAttr "Character1_RightForeArm_rotateX.o" "Character1_RightForeArm.rx";
connectAttr "Character1_RightForeArm_rotateY.o" "Character1_RightForeArm.ry";
connectAttr "Character1_RightForeArm_rotateZ.o" "Character1_RightForeArm.rz";
connectAttr "Character1_RightForeArm_visibility.o" "Character1_RightForeArm.v";
connectAttr "Character1_RightForeArm.s" "Character1_RightHand.is";
connectAttr "Character1_RightHand_scaleX.o" "Character1_RightHand.sx";
connectAttr "Character1_RightHand_scaleY.o" "Character1_RightHand.sy";
connectAttr "Character1_RightHand_scaleZ.o" "Character1_RightHand.sz";
connectAttr "Character1_RightHand_translateX.o" "Character1_RightHand.tx";
connectAttr "Character1_RightHand_translateY.o" "Character1_RightHand.ty";
connectAttr "Character1_RightHand_translateZ.o" "Character1_RightHand.tz";
connectAttr "Character1_RightHand_rotateX.o" "Character1_RightHand.rx";
connectAttr "Character1_RightHand_rotateY.o" "Character1_RightHand.ry";
connectAttr "Character1_RightHand_rotateZ.o" "Character1_RightHand.rz";
connectAttr "Character1_RightHand_visibility.o" "Character1_RightHand.v";
connectAttr "Character1_RightHand.s" "Character1_RightHandThumb1.is";
connectAttr "Character1_RightHandThumb1_scaleX.o" "Character1_RightHandThumb1.sx"
		;
connectAttr "Character1_RightHandThumb1_scaleY.o" "Character1_RightHandThumb1.sy"
		;
connectAttr "Character1_RightHandThumb1_scaleZ.o" "Character1_RightHandThumb1.sz"
		;
connectAttr "Character1_RightHandThumb1_translateX.o" "Character1_RightHandThumb1.tx"
		;
connectAttr "Character1_RightHandThumb1_translateY.o" "Character1_RightHandThumb1.ty"
		;
connectAttr "Character1_RightHandThumb1_translateZ.o" "Character1_RightHandThumb1.tz"
		;
connectAttr "Character1_RightHandThumb1_rotateX.o" "Character1_RightHandThumb1.rx"
		;
connectAttr "Character1_RightHandThumb1_rotateY.o" "Character1_RightHandThumb1.ry"
		;
connectAttr "Character1_RightHandThumb1_rotateZ.o" "Character1_RightHandThumb1.rz"
		;
connectAttr "Character1_RightHandThumb1_visibility.o" "Character1_RightHandThumb1.v"
		;
connectAttr "Character1_RightHandThumb1.s" "Character1_RightHandThumb2.is";
connectAttr "Character1_RightHandThumb2_scaleX.o" "Character1_RightHandThumb2.sx"
		;
connectAttr "Character1_RightHandThumb2_scaleY.o" "Character1_RightHandThumb2.sy"
		;
connectAttr "Character1_RightHandThumb2_scaleZ.o" "Character1_RightHandThumb2.sz"
		;
connectAttr "Character1_RightHandThumb2_translateX.o" "Character1_RightHandThumb2.tx"
		;
connectAttr "Character1_RightHandThumb2_translateY.o" "Character1_RightHandThumb2.ty"
		;
connectAttr "Character1_RightHandThumb2_translateZ.o" "Character1_RightHandThumb2.tz"
		;
connectAttr "Character1_RightHandThumb2_rotateX.o" "Character1_RightHandThumb2.rx"
		;
connectAttr "Character1_RightHandThumb2_rotateY.o" "Character1_RightHandThumb2.ry"
		;
connectAttr "Character1_RightHandThumb2_rotateZ.o" "Character1_RightHandThumb2.rz"
		;
connectAttr "Character1_RightHandThumb2_visibility.o" "Character1_RightHandThumb2.v"
		;
connectAttr "Character1_RightHandThumb2.s" "Character1_RightHandThumb3.is";
connectAttr "Character1_RightHandThumb3_translateX.o" "Character1_RightHandThumb3.tx"
		;
connectAttr "Character1_RightHandThumb3_translateY.o" "Character1_RightHandThumb3.ty"
		;
connectAttr "Character1_RightHandThumb3_translateZ.o" "Character1_RightHandThumb3.tz"
		;
connectAttr "Character1_RightHandThumb3_scaleX.o" "Character1_RightHandThumb3.sx"
		;
connectAttr "Character1_RightHandThumb3_scaleY.o" "Character1_RightHandThumb3.sy"
		;
connectAttr "Character1_RightHandThumb3_scaleZ.o" "Character1_RightHandThumb3.sz"
		;
connectAttr "Character1_RightHandThumb3_rotateX.o" "Character1_RightHandThumb3.rx"
		;
connectAttr "Character1_RightHandThumb3_rotateY.o" "Character1_RightHandThumb3.ry"
		;
connectAttr "Character1_RightHandThumb3_rotateZ.o" "Character1_RightHandThumb3.rz"
		;
connectAttr "Character1_RightHandThumb3_visibility.o" "Character1_RightHandThumb3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandIndex1.is";
connectAttr "Character1_RightHandIndex1_scaleX.o" "Character1_RightHandIndex1.sx"
		;
connectAttr "Character1_RightHandIndex1_scaleY.o" "Character1_RightHandIndex1.sy"
		;
connectAttr "Character1_RightHandIndex1_scaleZ.o" "Character1_RightHandIndex1.sz"
		;
connectAttr "Character1_RightHandIndex1_translateX.o" "Character1_RightHandIndex1.tx"
		;
connectAttr "Character1_RightHandIndex1_translateY.o" "Character1_RightHandIndex1.ty"
		;
connectAttr "Character1_RightHandIndex1_translateZ.o" "Character1_RightHandIndex1.tz"
		;
connectAttr "Character1_RightHandIndex1_rotateX.o" "Character1_RightHandIndex1.rx"
		;
connectAttr "Character1_RightHandIndex1_rotateY.o" "Character1_RightHandIndex1.ry"
		;
connectAttr "Character1_RightHandIndex1_rotateZ.o" "Character1_RightHandIndex1.rz"
		;
connectAttr "Character1_RightHandIndex1_visibility.o" "Character1_RightHandIndex1.v"
		;
connectAttr "Character1_RightHandIndex1.s" "Character1_RightHandIndex2.is";
connectAttr "Character1_RightHandIndex2_scaleX.o" "Character1_RightHandIndex2.sx"
		;
connectAttr "Character1_RightHandIndex2_scaleY.o" "Character1_RightHandIndex2.sy"
		;
connectAttr "Character1_RightHandIndex2_scaleZ.o" "Character1_RightHandIndex2.sz"
		;
connectAttr "Character1_RightHandIndex2_translateX.o" "Character1_RightHandIndex2.tx"
		;
connectAttr "Character1_RightHandIndex2_translateY.o" "Character1_RightHandIndex2.ty"
		;
connectAttr "Character1_RightHandIndex2_translateZ.o" "Character1_RightHandIndex2.tz"
		;
connectAttr "Character1_RightHandIndex2_rotateX.o" "Character1_RightHandIndex2.rx"
		;
connectAttr "Character1_RightHandIndex2_rotateY.o" "Character1_RightHandIndex2.ry"
		;
connectAttr "Character1_RightHandIndex2_rotateZ.o" "Character1_RightHandIndex2.rz"
		;
connectAttr "Character1_RightHandIndex2_visibility.o" "Character1_RightHandIndex2.v"
		;
connectAttr "Character1_RightHandIndex2.s" "Character1_RightHandIndex3.is";
connectAttr "Character1_RightHandIndex3_translateX.o" "Character1_RightHandIndex3.tx"
		;
connectAttr "Character1_RightHandIndex3_translateY.o" "Character1_RightHandIndex3.ty"
		;
connectAttr "Character1_RightHandIndex3_translateZ.o" "Character1_RightHandIndex3.tz"
		;
connectAttr "Character1_RightHandIndex3_scaleX.o" "Character1_RightHandIndex3.sx"
		;
connectAttr "Character1_RightHandIndex3_scaleY.o" "Character1_RightHandIndex3.sy"
		;
connectAttr "Character1_RightHandIndex3_scaleZ.o" "Character1_RightHandIndex3.sz"
		;
connectAttr "Character1_RightHandIndex3_rotateX.o" "Character1_RightHandIndex3.rx"
		;
connectAttr "Character1_RightHandIndex3_rotateY.o" "Character1_RightHandIndex3.ry"
		;
connectAttr "Character1_RightHandIndex3_rotateZ.o" "Character1_RightHandIndex3.rz"
		;
connectAttr "Character1_RightHandIndex3_visibility.o" "Character1_RightHandIndex3.v"
		;
connectAttr "Character1_RightHand.s" "Character1_RightHandRing1.is";
connectAttr "Character1_RightHandRing1_scaleX.o" "Character1_RightHandRing1.sx";
connectAttr "Character1_RightHandRing1_scaleY.o" "Character1_RightHandRing1.sy";
connectAttr "Character1_RightHandRing1_scaleZ.o" "Character1_RightHandRing1.sz";
connectAttr "Character1_RightHandRing1_translateX.o" "Character1_RightHandRing1.tx"
		;
connectAttr "Character1_RightHandRing1_translateY.o" "Character1_RightHandRing1.ty"
		;
connectAttr "Character1_RightHandRing1_translateZ.o" "Character1_RightHandRing1.tz"
		;
connectAttr "Character1_RightHandRing1_rotateX.o" "Character1_RightHandRing1.rx"
		;
connectAttr "Character1_RightHandRing1_rotateY.o" "Character1_RightHandRing1.ry"
		;
connectAttr "Character1_RightHandRing1_rotateZ.o" "Character1_RightHandRing1.rz"
		;
connectAttr "Character1_RightHandRing1_visibility.o" "Character1_RightHandRing1.v"
		;
connectAttr "Character1_RightHandRing1.s" "Character1_RightHandRing2.is";
connectAttr "Character1_RightHandRing2_scaleX.o" "Character1_RightHandRing2.sx";
connectAttr "Character1_RightHandRing2_scaleY.o" "Character1_RightHandRing2.sy";
connectAttr "Character1_RightHandRing2_scaleZ.o" "Character1_RightHandRing2.sz";
connectAttr "Character1_RightHandRing2_translateX.o" "Character1_RightHandRing2.tx"
		;
connectAttr "Character1_RightHandRing2_translateY.o" "Character1_RightHandRing2.ty"
		;
connectAttr "Character1_RightHandRing2_translateZ.o" "Character1_RightHandRing2.tz"
		;
connectAttr "Character1_RightHandRing2_rotateX.o" "Character1_RightHandRing2.rx"
		;
connectAttr "Character1_RightHandRing2_rotateY.o" "Character1_RightHandRing2.ry"
		;
connectAttr "Character1_RightHandRing2_rotateZ.o" "Character1_RightHandRing2.rz"
		;
connectAttr "Character1_RightHandRing2_visibility.o" "Character1_RightHandRing2.v"
		;
connectAttr "Character1_RightHandRing2.s" "Character1_RightHandRing3.is";
connectAttr "Character1_RightHandRing3_translateX.o" "Character1_RightHandRing3.tx"
		;
connectAttr "Character1_RightHandRing3_translateY.o" "Character1_RightHandRing3.ty"
		;
connectAttr "Character1_RightHandRing3_translateZ.o" "Character1_RightHandRing3.tz"
		;
connectAttr "Character1_RightHandRing3_scaleX.o" "Character1_RightHandRing3.sx";
connectAttr "Character1_RightHandRing3_scaleY.o" "Character1_RightHandRing3.sy";
connectAttr "Character1_RightHandRing3_scaleZ.o" "Character1_RightHandRing3.sz";
connectAttr "Character1_RightHandRing3_rotateX.o" "Character1_RightHandRing3.rx"
		;
connectAttr "Character1_RightHandRing3_rotateY.o" "Character1_RightHandRing3.ry"
		;
connectAttr "Character1_RightHandRing3_rotateZ.o" "Character1_RightHandRing3.rz"
		;
connectAttr "Character1_RightHandRing3_visibility.o" "Character1_RightHandRing3.v"
		;
connectAttr "Character1_Spine1.s" "Character1_Neck.is";
connectAttr "Character1_Neck_scaleX.o" "Character1_Neck.sx";
connectAttr "Character1_Neck_scaleY.o" "Character1_Neck.sy";
connectAttr "Character1_Neck_scaleZ.o" "Character1_Neck.sz";
connectAttr "Character1_Neck_translateX.o" "Character1_Neck.tx";
connectAttr "Character1_Neck_translateY.o" "Character1_Neck.ty";
connectAttr "Character1_Neck_translateZ.o" "Character1_Neck.tz";
connectAttr "Character1_Neck_rotateX.o" "Character1_Neck.rx";
connectAttr "Character1_Neck_rotateY.o" "Character1_Neck.ry";
connectAttr "Character1_Neck_rotateZ.o" "Character1_Neck.rz";
connectAttr "Character1_Neck_visibility.o" "Character1_Neck.v";
connectAttr "Character1_Neck.s" "Character1_Head.is";
connectAttr "Character1_Head_scaleX.o" "Character1_Head.sx";
connectAttr "Character1_Head_scaleY.o" "Character1_Head.sy";
connectAttr "Character1_Head_scaleZ.o" "Character1_Head.sz";
connectAttr "Character1_Head_translateX.o" "Character1_Head.tx";
connectAttr "Character1_Head_translateY.o" "Character1_Head.ty";
connectAttr "Character1_Head_translateZ.o" "Character1_Head.tz";
connectAttr "Character1_Head_rotateX.o" "Character1_Head.rx";
connectAttr "Character1_Head_rotateY.o" "Character1_Head.ry";
connectAttr "Character1_Head_rotateZ.o" "Character1_Head.rz";
connectAttr "Character1_Head_visibility.o" "Character1_Head.v";
connectAttr "Character1_Head.s" "jaw.is";
connectAttr "jaw_lockInfluenceWeights.o" "jaw.liw";
connectAttr "jaw_translateX.o" "jaw.tx";
connectAttr "jaw_translateY.o" "jaw.ty";
connectAttr "jaw_translateZ.o" "jaw.tz";
connectAttr "jaw_scaleX.o" "jaw.sx";
connectAttr "jaw_scaleY.o" "jaw.sy";
connectAttr "jaw_scaleZ.o" "jaw.sz";
connectAttr "jaw_rotateX.o" "jaw.rx";
connectAttr "jaw_rotateY.o" "jaw.ry";
connectAttr "jaw_rotateZ.o" "jaw.rz";
connectAttr "jaw_visibility.o" "jaw.v";
connectAttr "Character1_Head.s" "eyes.is";
connectAttr "eyes_translateX.o" "eyes.tx";
connectAttr "eyes_translateY.o" "eyes.ty";
connectAttr "eyes_translateZ.o" "eyes.tz";
connectAttr "eyes_scaleX.o" "eyes.sx";
connectAttr "eyes_scaleY.o" "eyes.sy";
connectAttr "eyes_scaleZ.o" "eyes.sz";
connectAttr "eyes_rotateX.o" "eyes.rx";
connectAttr "eyes_rotateY.o" "eyes.ry";
connectAttr "eyes_rotateZ.o" "eyes.rz";
connectAttr "eyes_visibility.o" "eyes.v";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Enemy@Grunt_Walk_02.ma
