﻿using UnityEngine;
using System.Collections;

public class MeleeWeapon : MonoBehaviour {

    private MeleeAttackRange mInRange;
    private bool targetInRange;
    public float AttackTimer;
    public Animator anim;
    //public GameObject Projectile;
    //public Transform projectileSpawnPoint;
    //public float projectileSpeed;

    private float _nextSpawnTime;

    // Use this for initialization
    void Start()
    {
        mInRange = GetComponentInParent<MeleeAttackRange>();
    }

    // Update is called once per frame
    void Update()
    {
        targetInRange = mInRange.targetInRange;
        if (targetInRange)
        {
            AttackInterval();
        }
    }

    //----------------------------------------------------------------------------
    // sets the timer for the enemy to attack
    //----------------------------------------------------------------------------
    void EnemyRangeAttack()
    {
        _nextSpawnTime = Time.time + AttackTimer;
        anim.SetInteger("AttackRand", Random.Range(0, 3));
        anim.SetBool("IsAttacking", true);
    }

    //----------------------------------------------------------------------------
    // Attacks at set time
    //----------------------------------------------------------------------------
    public void AttackInterval()
    {
        if (Time.time >= _nextSpawnTime)
        {
            EnemyRangeAttack();
        }
        else
        {
            anim.SetBool("IsAttacking", false);
        }
    }
}
