﻿using UnityEngine;
using System.Collections;

public class MeleeAlertAttack : MonoBehaviour {

    public GameObject weapon;
    private MeleeHitBox soureScript;
    private GameObject target;

    void Start()
    {
        soureScript = weapon.GetComponent<MeleeHitBox>();
    }
    void Update()
    {
        target = weapon.GetComponent<MeleeHitBox>().target;
    }

    public void changeAlert()
    {
        if (target != null)
        {
            soureScript.SetAlert(true);
        }
    }
}
