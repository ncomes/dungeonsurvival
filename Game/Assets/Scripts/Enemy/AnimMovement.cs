﻿using UnityEngine;
using System.Collections;

public class AnimMovement : MonoBehaviour
{
    Animator anim;
    NavMeshAgent navAgent;

	void Start ()
    {
        anim = GetComponentInChildren<Animator>();
        if (!anim)
            Debug.LogError(gameObject + " has an AnimMovement component but no animator!");

        navAgent = GetComponent<NavMeshAgent>();
        if (!navAgent)
            Debug.LogError(gameObject + " has an AnimMovement but can't find a NavMeshAgent.");
	}
	
	void Update ()
    {
        anim.SetFloat("Speed", navAgent.velocity.magnitude);
	}
}
