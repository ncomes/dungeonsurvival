﻿using UnityEngine;
using System.Collections;

public class MageRadiusScript : MonoBehaviour {

    public GameObject player;
    public float radius;
    public GameObject mage;
    
	// Use this for initialization
	void Start () {
        GetComponent<SphereCollider>().radius = 0f;
    }
	
	// Update is called once per frame
	void Update () {
        GrowRadius();
        if (player != null) 
        {
            mage.transform.LookAt(player.transform.position);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            player = (other.gameObject);
            EnemyAI ai = GetComponentInParent<EnemyAI>();
            if (ai)
                ai.ChangeEnemyAIState(eEnemyAIState.AIS_Attacking);

            return;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            player = null;
            EnemyAI ai = GetComponentInParent<EnemyAI>();
            if (ai)
                ai.ChangeEnemyAIState(eEnemyAIState.AIS_WalkingToGoal);

            return;
        }
    }
    
    void GrowRadius() {
        if (GetComponent<SphereCollider>().radius <= radius) {
            GetComponent<SphereCollider>().radius += 1f;
        }
    }


}