﻿using UnityEngine;
using System.Collections;

public class WeaponScript : MonoBehaviour
{
    public GameObject target;
    public float radius;
    public GameObject EnemyRange;
    public float AttackTimer;
    //public GameObject mageProjectile;
    //public Transform projectileSpawnPoint;
    //public float projectileSpeed;

    private float _nextSpawnTime;
    private Animator anim;

    bool bPaused = false;

    // Use this for initialization
    void Start()
    {
        GetComponent<SphereCollider>().radius = 0f;
        anim = EnemyRange.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (bPaused)
            return;

        GrowRadius();
        if (target != null)
        {
            EnemyRange.transform.LookAt(target.transform.position);
            AttackInterval();
            //anim.SetBool("IsAttacking", false);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            target = (other.gameObject);
            EnemyAI ai = GetComponentInParent<EnemyAI>();
            if (ai)
                ai.ChangeEnemyAIState(eEnemyAIState.AIS_Attacking);

            return;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            target = null;
            EnemyAI ai = GetComponentInParent<EnemyAI>();
            if (ai)
                ai.ChangeEnemyAIState(eEnemyAIState.AIS_WalkingToGoal);

            return;
        }
    }

    void GrowRadius()
    {
        if (GetComponent<SphereCollider>().radius <= radius)
        {
            GetComponent<SphereCollider>().radius += 1f;
        }
    }

    public bool HasTarget()
    {
        return target;
    }

    public void SetPaused(bool set)
    {
        bPaused = set;
    }

    //----------------------------------------------------------------------------
    // Sets the mage attack bool
    //----------------------------------------------------------------------------
    void EnemyProjectileAttack()
    {
        _nextSpawnTime = Time.time + AttackTimer;
        anim.SetBool("IsAttacking", true);
    }

    //----------------------------------------------------------------------------
    // Attacks at set time
    //----------------------------------------------------------------------------
    public void AttackInterval()
    {
        if (Time.time >= _nextSpawnTime)
        {
            EnemyProjectileAttack();
        }
        else
        {
            anim.SetBool("IsAttacking", false);
        }
    }

}
