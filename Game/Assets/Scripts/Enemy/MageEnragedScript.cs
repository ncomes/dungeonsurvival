﻿using UnityEngine;
using System.Collections;

public class MageEnragedScript : MonoBehaviour {

    public bool enemyNotSpawned = true;
    public GameObject player;
    public float EnragedTime = 2.0f;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && enemyNotSpawned == true)
        {
            player = other.gameObject;
            if (player != null && enemyNotSpawned == true)
            {
                EnemyAI ai = GetComponentInParent<EnemyAI>();
                if (ai)
                    ai.ChangeEnemyAIState(eEnemyAIState.AIS_Enraged);

                Invoke("EndEnrage", EnragedTime);
                return;

            }
        }
    }

    void EndEnrage()
    {
        EnemyAI ai = GetComponentInParent<EnemyAI>();
        if (ai)
        {
            if (GetComponentInParent<TargetInRange>().targetInRange)
                ai.ChangeEnemyAIState(eEnemyAIState.AIS_Attacking);
            else
                ai.ChangeEnemyAIState(eEnemyAIState.AIS_WalkingToGoal);
        }
            
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player" && enemyNotSpawned == false)
        {
            player = other.gameObject;
            if (player != null)
            {
                EnemyAI ai = GetComponentInParent<EnemyAI>();
                if (ai)
                    ai.ChangeEnemyAIState(eEnemyAIState.AIS_Attacking);

                return;

            }
        }
    }
}
