﻿using UnityEngine;
using System.Collections;

public class AlertAttack : MonoBehaviour 
{

    public GameObject weapon;
    private ProjectileScript soureScript;

    void Start()
    {
        soureScript = weapon.GetComponent<ProjectileScript>();
    }
    
    public void changeAlert()
    {
        soureScript.SetAlert(true);
    }
    
}
