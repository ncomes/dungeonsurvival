﻿using UnityEngine;
using System.Collections;

public class ProjectileScript : MonoBehaviour {

    public Rigidbody projectile;
    public float projectileSpeed;
    public bool Alert = false;

    void Update()
    {
        if (Alert)
        {
            SpawnProjectile();
            //Debug.Log("Fire!!!");
        }
            
    }

    public void SpawnProjectile()
    {
        Rigidbody _proj = Instantiate(projectile, transform.position, transform.rotation) as Rigidbody;
        _proj.velocity = transform.transform.TransformDirection(new Vector3(0f, 0f, projectileSpeed));
        SetAlert(false);
    }

    public void SetAlert(bool set)
    {
        Alert = set;
    }
}

