﻿using UnityEngine;
using System.Collections;

public class ArcherArrowScript : MonoBehaviour {

    public float lifetime = 3.0f;
    public float damage = 2.0f;

    void Awake()
    {
        Destroy(gameObject, lifetime);
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            DestroyObject(gameObject);
            other.gameObject.GetComponent<Damageable>().TakeDamage(damage);

        }
    }
}
