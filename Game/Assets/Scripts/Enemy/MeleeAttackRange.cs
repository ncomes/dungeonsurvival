﻿using UnityEngine;
using System.Collections;

public class MeleeAttackRange : MonoBehaviour {

    public float radius;
    public bool targetInRange = false;
    public GameObject rootNode;
    public GameObject target;
    [Tooltip("Speed in which the enemy turns to aim at target.")]
    public float turnToTargetSpeed = 15.0f;

    bool bPaused = false;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

        if (bPaused)
            return;

        GrowRadius();
        if (targetInRange)
        {
            //rootNode.transform.LookAt(target.transform.position);
            Quaternion newRotation = Quaternion.LookRotation(target.transform.position - rootNode.transform.position);
            newRotation.x = 0f;
            newRotation.z = 0f;

            rootNode.transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * turnToTargetSpeed);
        }
    }
    //----------------------------------------------------------------------------
    // Grows the Sphere Collider on start to ensure it hits everything around it
    //----------------------------------------------------------------------------
    void GrowRadius()
    {
        if (GetComponent<SphereCollider>().radius <= radius)
        {
            GetComponent<SphereCollider>().radius += 1f;
        }
    }
    //----------------------------------------------------------------------------
    // Checks to see if the player has entered the collider
    //----------------------------------------------------------------------------
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
			GetComponentInParent<NavMeshAgent>().destination = rootNode.transform.position;
			target = (other.gameObject);
            targetInRange = true;
            EnemyAI ai = GetComponentInParent<EnemyAI>();
            if (ai)
                ai.ChangeEnemyAIState(eEnemyAIState.AIS_MeleeAttack);

            return;
        }
    }
    //----------------------------------------------------------------------------
    // Checks to see if the player has exited the collider
    //----------------------------------------------------------------------------
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            target = null;
            targetInRange = false;
            EnemyAI ai = GetComponentInParent<EnemyAI>();
            if (ai)
                ai.ChangeEnemyAIState(eEnemyAIState.AIS_MoveToPlayer);

            return;
        }
    }

    public void SetPaused(bool set)
    {
        bPaused = set;
    }
}
