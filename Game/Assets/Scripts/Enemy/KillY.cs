﻿using UnityEngine;
using System.Collections;

public class KillY : MonoBehaviour
{
    public bool KillEnemies = true;
    public bool DebugReportKills = false;

	void OnTriggerEnter(Collider other)
    {
        if (KillEnemies && other.gameObject.tag == "Enemy")
        {
            if (DebugReportKills)
                Debug.Log("KillY Killed: " + gameObject);

            other.gameObject.BroadcastMessage("Damage", 10000);
            return;
        }
    }
}
