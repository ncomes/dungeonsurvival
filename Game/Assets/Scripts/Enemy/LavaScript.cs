﻿using UnityEngine;
using System.Collections;

public class LavaScript : MonoBehaviour
{

    //public gruntMobScript pitJump;
    //public ArcherMobScript archerJump;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            EnemyAI ai = other.gameObject.GetComponent<EnemyAI>();
            if (ai)
                ai.ChangeEnemyAIState(eEnemyAIState.AIS_Jumping);

            return;
        }
    }
}