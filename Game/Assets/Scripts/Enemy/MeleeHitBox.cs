﻿using UnityEngine;
using System.Collections;

public class MeleeHitBox : MonoBehaviour 
{
    public float damage = 1.0f;
    public bool Alert = false;
    public GameObject target;

    //private bool mar;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (target != null && Alert)
        {
            target.GetComponent<Damageable>().TakeDamage(damage);
            SetAlert(false);
        }
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            target = (other.gameObject);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            target = null;
        }
    }

    public void SetAlert(bool set)
    {
        Alert = set;
    }
}
