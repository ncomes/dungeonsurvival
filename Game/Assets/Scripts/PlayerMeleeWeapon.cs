﻿using UnityEngine;
using System.Collections;

public class PlayerMeleeWeapon : MonoBehaviour {

    private Animator anim;
	private float nextSpawnTime;
	private float PauseTimer;

    
	// Use this for initialization
	void Start () {
        anim = GetComponentInParent<Animator>();
	}

    public void AttackTarget()
    {
        anim.SetInteger("AttackRand", Random.Range(0, 3));
        anim.SetBool("IsAttacking", true);
    }
		
}
