﻿using UnityEngine;
using System.Collections;

public class CameraFollowScript : MonoBehaviour {


	public GameObject player;
	private bool ready;
	private Vector3 cameraPos;
	public float heightV;
	public float distanceAway = 20.0f;
	public float cameraMoveSpeed = 3;


	// Use this for initialization
	void Start () {
		//heightV = 15f;
		transform.position = new Vector3(player.transform.position.x, player.transform.position.y + heightV, player.transform.position.z) + ((distanceAway*-1))*player.transform.forward;
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (!player)
            return;

		cameraPos = new Vector3(player.transform.position.x, player.transform.position.y + heightV, player.transform.position.z + distanceAway);
		if (!ready) {
			StartCoroutine("cam_rotate");
		}
		transform.LookAt (player.transform.position);
//		Quaternion newRotation = Quaternion.LookRotation(transform.position);
//		newRotation.y = 0f;
//		newRotation.z = 0f;
	}
	IEnumerator cam_rotate(){
		ready = true;
		transform.position = Vector3.Slerp (transform.position, cameraPos, cameraMoveSpeed * Time.deltaTime);
		ready = false;
		yield return new WaitForSeconds (0.005f);
	}
}
