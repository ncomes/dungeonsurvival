﻿using UnityEngine;
using System.Collections;

public class GameState : MonoBehaviour 
{
    protected virtual eGameState mGameState { get {return eGameState.GS_None; } }
    protected eGameState mCurrentState = eGameState.GS_None;

    bool bStartState = false;

    void Awake()
    {
        enabled = false;
    }

    protected virtual void Update()
    {
        if (bStartState)
        {
            StartState();
            bStartState = false;
        }
    }

    protected virtual void ChangeState(eGameState newState)
    {
        if (mCurrentState != mGameState && mGameState == newState)
        {
            enabled = true;
            bStartState = true;
        }
        else if (mCurrentState == mGameState)
        {
            EndState();
            enabled = false;
        }
        mCurrentState = newState;
    }

    protected virtual void StartState() {}
    protected virtual void EndState() {}
}
