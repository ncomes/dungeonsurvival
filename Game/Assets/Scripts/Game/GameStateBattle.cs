﻿using UnityEngine;
using System.Collections;

public class GameStateBattle : GameState
{
    protected override eGameState mGameState { get { return eGameState.GS_Battle; } }


    protected override void StartState()
    {
        UIHUD.Show();
    }


    protected override void EndState()
    {
        UIHUD.Hide();
    }
}
