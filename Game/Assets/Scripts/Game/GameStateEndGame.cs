﻿using UnityEngine;
using System.Collections;

public class GameStateEndGame : GameState
{
    protected override eGameState mGameState { get { return eGameState.GS_EndGame; } }

    protected override void StartState()
    {
        UIEndGame.Show();
    }

    protected override void EndState()
    {
        UIEndGame.Hide();
    }
}
