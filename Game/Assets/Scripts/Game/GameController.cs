﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum eGameState
{
    GS_None,
    GS_MainMenu,
    GS_Battle,
    GS_EndGame
}

public class GameController : MonoBehaviour 
{
    public static GameController instance;

    public eGameState CurrentState = eGameState.GS_None;

    public eGameState DebugChangeState = eGameState.GS_None;
    
    void Awake()
    {
        if (!instance)
            instance = this;
    }

    void Start()
    {
        StartMainMenu();
    }

    void Update()
    {
        if (DebugChangeState != eGameState.GS_None)
        {
            SetNewState(DebugChangeState);
            DebugChangeState = eGameState.GS_None;
        }
    }

    public static void StartMainMenu()      { GameController.SetNewState(eGameState.GS_MainMenu); }
    public static void StartBattle()        { GameController.SetNewState(eGameState.GS_Battle); }
    public static void StartEndGame()       { GameController.SetNewState(eGameState.GS_EndGame); }

    public static void SetNewState(eGameState state) 
    {
        //Debug.Log("GameState-State: " + instance.CurrentState + " New: " + state);
        instance.CurrentState = state; GameController.ChangeGameState(); 
    }
    public static void ChangeGameState() { instance.BroadcastMessage("ChangeState", instance.CurrentState); }

}
