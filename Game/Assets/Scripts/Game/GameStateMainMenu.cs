﻿using UnityEngine;
using System.Collections;

public class GameStateMainMenu : GameState
{
    protected override eGameState mGameState { get { return eGameState.GS_MainMenu; } }

    protected override void StartState()
    {
        UIMainMenu.Show();
    }

    protected override void EndState()
    {
        UIMainMenu.Hide();
    }
}
