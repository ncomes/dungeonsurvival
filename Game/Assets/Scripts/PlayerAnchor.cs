﻿using UnityEngine;
using System.Collections;

public class PlayerAnchor : MonoBehaviour
{
    public static PlayerAnchor instance;

    void Awake()
    {
        if (!instance)
            instance = this;
        else
            Debug.LogError("More than one PlayerAnchor Detected. This should not be. Object: " + gameObject.name);
    }

}
