﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyAIStateSpawning : EnemyAIState 
{
    protected override eEnemyAIState mAIState { get { return eEnemyAIState.AIS_Spawning; } }

    protected override void StartState()
    {
    }

    protected override void EndState()
    {
    }

}
