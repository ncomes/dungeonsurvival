﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyAIStateJumping : EnemyAIState 
{
    protected override eEnemyAIState mAIState { get { return eEnemyAIState.AIS_Jumping; } }

    /*
    public float JumpVelocity = 100.0f;
    public float JumpYAngle = 0.7f;
    public float JumpGravity = 10.0f;
    */

    //private Vector3 vel = Vector3.zero;

    protected override void StartState()
    {
        // Set the animation parameters when we go to jump
        anim.SetInteger("PitJumpRand", Random.Range(0, 3));
        anim.SetBool("PitJump", true);
    }
    /*
        
        // Turn off the colliders so he can't be killed mid jump.
        //GetComponent<CapsuleCollider>().enabled = false;

        // Initial Jump velocity
        CharacterController cc = GetComponent<CharacterController>();
        vel = Vector3.Normalize(cc.velocity);
        vel.y = JumpYAngle;
        vel *= JumpVelocity;
    }

    protected override void StateUpdate()
    {
        vel.y -= JumpGravity * Time.deltaTime;
        GetComponent<CharacterController>().Move(vel * Time.deltaTime);
    }
    */

    protected override void EndState()
    {
    }

}
