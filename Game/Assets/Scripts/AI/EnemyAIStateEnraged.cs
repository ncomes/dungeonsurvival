﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyAIStateEnraged : EnemyAIState
{
    protected override eEnemyAIState mAIState{get { return eEnemyAIState.AIS_Enraged; } }

    public List<GameObject> gruntspawners;
    public GameObject grunt;

    protected override void StartState()
    {

        anim.SetBool("Enraged", true);
        Invoke("SpawnGrunts", .5f);
        GetComponentInChildren<MageEnragedScript>().enemyNotSpawned = false;
        GetComponentInChildren<MageEnragedScript>().player = null;
        Invoke("MageOffEnraged", 1f);

    }

    protected override void EndState()
    {
        GetComponentInChildren<MageEnragedScript>().gameObject.SetActive(false);
        anim.SetBool("Enraged", false);
    }

    public void SpawnGrunts()
    {
        foreach (GameObject gruntSpawn in gruntspawners)
        {
            GameObject _spawnedGrunt = Instantiate(grunt, new Vector3(gruntSpawn.transform.position.x, gruntSpawn.transform.position.y, gruntSpawn.transform.position.z), Quaternion.identity) as GameObject;
            GameObject _mobParent = GameObject.FindGameObjectWithTag("EnemyGroup");
            _spawnedGrunt.transform.parent = _mobParent.transform;
        }
    }

    public void MageOffEnraged()
    {
        anim.SetBool("Enraged", false);
    }
    
}
