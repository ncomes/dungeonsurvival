﻿using UnityEngine;
using System.Collections;

public class EnemyAIStateMeleeAttack : EnemyAIState
{

    protected override eEnemyAIState mAIState { get { return eEnemyAIState.AIS_MeleeAttack; } }

    private MeleeAttackRange mInRange;

    void Start()
    {
        mInRange = GetComponentInChildren<MeleeAttackRange>();
    }

    protected override void StartState()
    {

    }

    protected override void EndState()
    {
        anim.SetBool("IsAttacking", false);
    }
}
