﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyAIState : MonoBehaviour 
{
    protected virtual eEnemyAIState mAIState { get { return eEnemyAIState.AIS_None; } }
    protected eEnemyAIState mCurrentState = eEnemyAIState.AIS_None;

    bool bStartState = false;

    protected EnemyAI mAI;
    protected Animator anim;

    public bool DebugAIState = false;

    void Awake()
    {
        mAI = GetComponent<EnemyAI>();
        if (!mAI)
            Debug.LogError("EnemyAIState: Could not find an EnemyAI component on the enemy ship. " + gameObject);

        anim = GetComponentInChildren<Animator>();
    }

    protected virtual void Update()
    {
        if (bStartState)
        {
            StartState();
            bStartState = false;
        }
        StateUpdate();
    }

    protected virtual void ChangeAIState(eEnemyAIState newState)
    {
        // We're trying to enter a state we're already in
        if (mCurrentState == newState)
            return;

        if (DebugAIState)
            Debug.Log("ChangeAIState: Object: " + gameObject.name + " ComponentState: " + mAIState + " CurrentState: " + mCurrentState + " newState: " + newState);

        if (mCurrentState != mAIState && mAIState == newState)
        {
            if (DebugAIState)
                Debug.Log(gameObject.name + " has started AI state of: " + mAIState);
            
            enabled = true;
            bStartState = true;
        }
        else if (mCurrentState == mAIState)
        {
            if (DebugAIState)
                Debug.Log(gameObject.name + " has ended AI state of: " + mAIState);
        
            EndState();
            enabled = false;
        }
        mCurrentState = newState;
    }
    
    protected virtual void StartState() { }
    protected virtual void EndState() { }
    protected virtual void StateUpdate() { }
	
}
