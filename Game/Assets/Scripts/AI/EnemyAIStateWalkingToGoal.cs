﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyAIStateWalkingToGoal : EnemyAIState 
{
    protected override eEnemyAIState mAIState { get { return eEnemyAIState.AIS_WalkingToGoal; } }

    [Tooltip("Distance AI needs to get to the goal to consider it successful.")]
    public float GoalDistance = 15.0f;
    private Vector3 goalLoc;
    private NavMeshAgent navAgent;

    protected override void StartState()
    {
        navAgent = GetComponent<NavMeshAgent>();
        navAgent.ResetPath();
        goalLoc = GameObject.FindGameObjectWithTag("Finish").GetComponent<Transform>().position;
        goalLoc += Vector3.MoveTowards(goalLoc, transform.position, GoalDistance);
        NavMeshHit hit;
        if (NavMesh.SamplePosition(goalLoc, out hit, 100.0f, NavMesh.AllAreas))
        {
            navAgent.SetDestination(goalLoc);
            //transform.LookAt(navAgent.velocity);
            Quaternion newRotation = Quaternion.LookRotation(goalLoc - transform.position);
            newRotation.x = 0f;
            newRotation.z = 0f;

            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * 3.0f);
        }
    }

    protected override void EndState()
    {
        if (navAgent.isActiveAndEnabled)
            navAgent.Stop();
    }

}
