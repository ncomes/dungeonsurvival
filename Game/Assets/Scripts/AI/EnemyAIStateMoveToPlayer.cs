﻿using UnityEngine;
using System.Collections;

public class EnemyAIStateMoveToPlayer : EnemyAIState
{
    protected override eEnemyAIState mAIState { get { return eEnemyAIState.AIS_MoveToPlayer; } }

    [Tooltip("Distance AI needs to get to the goal to consider it successful.")]
    public float GoalDistance;
    private Vector3 goalLoc;
    private NavMeshAgent navAgent;

    protected override void StateUpdate()
    {
        moveToPlayer();
    }
    
    protected override void StartState() 
    {
        navAgent = GetComponent<NavMeshAgent>();
        navAgent.ResetPath();
    }
    
    protected override void EndState()
    {
        navAgent.Stop();
    }
    
    void moveToPlayer()
    {
        goalLoc = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position;
        if (Vector3.Distance(transform.position, goalLoc) > GoalDistance)
        {
            
            GetComponent<NavMeshAgent>().destination = goalLoc;
        }
        if (Vector3.Distance(transform.position, goalLoc) <= GoalDistance)
        {
            GetComponent<NavMeshAgent>().destination = transform.position;
        }
    }
}
