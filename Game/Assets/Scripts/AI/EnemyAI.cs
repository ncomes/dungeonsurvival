﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum eEnemyAIState
{
    AIS_None,
    AIS_Idle,
    AIS_Spawning,
    AIS_Attacking,
    AIS_MeleeAttack,
    AIS_WalkingToGoal,
    AIS_MoveToPlayer,
    AIS_Jumping,
    AIS_Enraged,
    AIS_Paused
}

public class EnemyAI : MonoBehaviour
{
    [Tooltip("What state this AI starts off in.")]
    public eEnemyAIState StartingState = eEnemyAIState.AIS_Paused;
    eEnemyAIState CurrentState = eEnemyAIState.AIS_None;

    [Tooltip("Walk Speed of this mob.")]
    public float WalkSpeed = 20.0f;

    [Tooltip("Set this to something other than AIS_None to immediately switch to a different AI State.")]
    public eEnemyAIState DebugStartState = eEnemyAIState.AIS_None;

    void Start()
    {
        ChangeEnemyAIState(StartingState);
    }

    void Update()
    {
        if (DebugStartState != eEnemyAIState.AIS_None)
        {
            ChangeEnemyAIState(DebugStartState);
            DebugStartState = eEnemyAIState.AIS_None;
        }
    }

    public eEnemyAIState GetCurrentState()
    {
        return CurrentState;
    }

    public void ChangeEnemyAIState(eEnemyAIState newState)
    {
        CurrentState = newState;
        BroadcastMessage("ChangeAIState", CurrentState);
    }

    void OnKilled()
    {
        ChangeEnemyAIState(eEnemyAIState.AIS_Paused);
    }

    
}
