﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyAIStateAttacking : EnemyAIState 
{
    protected override eEnemyAIState mAIState { get { return eEnemyAIState.AIS_Attacking; } }

    private TargetInRange mInRange;
    
    void Start()
    {
        mInRange = GetComponentInChildren<TargetInRange>();
    }

    protected override void StartState()
    {
        mInRange.SetPaused(false);
    }
    
    protected override void EndState()
    {
        anim.SetBool("IsAttacking", false);
        mInRange.SetPaused(true);
    }

}
