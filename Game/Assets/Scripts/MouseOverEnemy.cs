﻿using UnityEngine;
using System.Collections;

public class MouseOverEnemy : MonoBehaviour {

    public Transform player;

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    //----------------------------------------------------------------
    // when you click on a mob the player gets his info
    //----------------------------------------------------------------
    void OnMouseOver()
    {
        if (Input.GetMouseButton(0))
        {
            player.GetComponent<PlayerAIStateWalkingtoGoal>().opponent = gameObject;
        }
    }
}
