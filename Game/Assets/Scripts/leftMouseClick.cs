﻿using UnityEngine;
using System.Collections;

public class leftMouseClick : MonoBehaviour {

    public bool bHasClicked = false;
    public bool bHasReleased = true;
    public PlayerAIStateWalkingtoGoal opponent;

    // Use this for initialization
    void Start()
    {
        opponent = GetComponent<PlayerAIStateWalkingtoGoal>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            opponent.opponent = null;
            bHasClicked = true;
            bHasReleased = false;
            PlayerAI ai = GetComponent<PlayerAI>();
            if (ai)
                ai.ChangePlayerAIState(ePlayerAIState.AIS_WalkingToGoal);

            return;
        }
        if (Input.GetMouseButtonUp(0))
        {
            bHasReleased = true;
            bHasClicked = false;
        }
    }
}
