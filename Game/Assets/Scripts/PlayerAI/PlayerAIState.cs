﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAIState : MonoBehaviour 
{
    protected virtual ePlayerAIState mAIState { get { return ePlayerAIState.AIS_None; } }
    protected ePlayerAIState mCurrentState = ePlayerAIState.AIS_None;

    bool bStartState = false;

    protected PlayerAI mAI;
    protected Animator anim;

    public bool DebugAIState = false;

    void Awake()
    {
        mAI = GetComponent<PlayerAI>();
        if (!mAI)
            Debug.LogError("PlayerAIState: Could not find an EnemyAI component on the enemy ship. " + gameObject);

        anim = GetComponentInChildren<Animator>();
    }

    protected virtual void Update()
    {
        if (bStartState)
        {
            StartState();
            bStartState = false;
        }
        StateUpdate();
    }

    protected virtual void ChangeAIState(ePlayerAIState newState)
    {
        // We're trying to enter a state we're already in
        if (mCurrentState == newState)
            return;

        if (DebugAIState)
            Debug.Log("ChangeAIState: Object: " + gameObject.name + " ComponentState: " + mAIState + " CurrentState: " + mCurrentState + " newState: " + newState);

        if (mCurrentState != mAIState && mAIState == newState)
        {
            if (DebugAIState)
                Debug.Log(gameObject.name + " has started AI state of: " + mAIState);

            enabled = true;
            bStartState = true;
        }
        else if (mCurrentState == mAIState)
        {
            if (DebugAIState)
                Debug.Log(gameObject.name + " has ended AI state of: " + mAIState);

            EndState();
            enabled = false;
        }
        mCurrentState = newState;
    }

    protected virtual void StartState() { }
    protected virtual void EndState() { }
    protected virtual void StateUpdate() { }

}
