﻿using UnityEngine;
using System.Collections;

public class PlayerAIStateAttacking : PlayerAIState
{
    protected override ePlayerAIState mAIState { get { return ePlayerAIState.AIS_Attacking; } }

    public PlayerMeleeWeapon pmw;

	private Vector3 target;
    
    protected override void StartState()
    {
        //pmw = GetComponentInChildren<PlayerMeleeWeapon>();
		target = GetComponent<PlayerAIStateWalkingtoGoal>().opponent.transform.position;
		Quaternion newRotation = Quaternion.LookRotation(target - transform.position);
		newRotation.x = 0f;
		newRotation.z = 0f;

		transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * 100.0f);
        pmw.AttackTarget();
		Invoke("EndAttack", 0.3f);
    }

    protected override void EndState()
    {
        anim.SetBool("IsAttacking", false);
    }

	//----------------------------------------------------------------------------
	// EndsAttack
	//----------------------------------------------------------------------------
	public void EndAttack()
	{
		anim.SetBool("IsAttacking", false);
		PlayerAI ai = GetComponentInParent<PlayerAI>();
		if (ai)
			ai.ChangePlayerAIState(ePlayerAIState.AIS_Idle);

		return;
	}
}
