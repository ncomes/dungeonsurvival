﻿using UnityEngine;
using System.Collections;

public class PlayerAIStateWalkingtoGoal : PlayerAIState
{
    protected override ePlayerAIState mAIState { get { return ePlayerAIState.AIS_WalkingToGoal; } }
    [Tooltip("Speed the player moves.")]
    public float speed = 20;
    [Tooltip("Speed the player turns/rotates.")]
    public float rotationSpeed = 10;
    [Tooltip("Enemy that is clicked on")]
    public GameObject opponent;
    [Tooltip("Checks to see in an Enemy has been clicked on")]
    public bool bSelectEnemy;
    [Tooltip("Has the left mouse button been click?")]
    public bool bHasClicked;
    [Tooltip("Has the left mouse button been released?")]
    public bool bHasReleased;
    [Tooltip("Has the player reached his goal?")]
    public bool bReachedGoal = true;

    private Vector3 desiredPosition;
    private int range = 8;
    private Vector3 enemyPosition;
    private CharacterController controller;

    protected override void StartState()
    {
        controller = GetComponent<CharacterController>();
    }
    
    protected override void StateUpdate()
    {
        if (opponent != null)
        {
            enemyPosition = opponent.transform.position;
        }
        bHasClicked = GetComponent<leftMouseClick>().bHasClicked;
        bHasReleased = GetComponent<leftMouseClick>().bHasReleased;
        if (bHasClicked)
        {
            locatePosition();
        }
        if (bHasReleased == false || bReachedGoal == false)
        {
            moveToPosition();
        }
    }

    protected override void EndState()
    {
        
    }
    //------------------------------------------------------------------
    // updates the location where the user has left mouse clicked
    //------------------------------------------------------------------
    void locatePosition()
    {
        bReachedGoal = false;
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 1000, 1 << 8 | 1 << 9 | 1 << 10))
        {
            if (hit.collider.tag != "Player" && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
                desiredPosition = new Vector3(hit.point.x, hit.point.y, hit.point.z);
                bSelectEnemy = false;
                range = 3;
                //Debug.Log(desiredPosition);
            }
            if (opponent != null && hit.collider.tag == "Enemy" && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
                bSelectEnemy = true;
                range = 8;
            }
        }

    }
    //------------------------------------------------------------------
    // Move and rotates the tagrget towards the selected location
    //------------------------------------------------------------------
    void moveToPosition()
    {
        // see if the player is close to the clicked position
        if (opponent != null && bSelectEnemy == true)
        {
            desiredPosition = enemyPosition;
        }
        if (Vector3.Distance(transform.position, desiredPosition) > range)
        {
            Quaternion newRotation = Quaternion.LookRotation(desiredPosition - transform.position);
            newRotation.x = 0f;
            newRotation.z = 0f;

            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * rotationSpeed);
            controller.SimpleMove(transform.forward * speed);

            float moveSpeed = Vector3.Distance(desiredPosition, transform.position);
            anim.SetFloat("Speed", (moveSpeed));

        }
        else
        {
            bReachedGoal = true;
            //bHasClicked = false;
            anim.SetFloat("Speed", (0));
            if (opponent == null)
            {
                PlayerAI ai = GetComponent<PlayerAI>();
                if (ai)
                    ai.ChangePlayerAIState(ePlayerAIState.AIS_Idle);

                return;
            }
            else
            {
                PlayerAI ai = GetComponent<PlayerAI>();
                if (ai)
                    ai.ChangePlayerAIState(ePlayerAIState.AIS_Attacking);

                return;
            }
        }
    }
}
