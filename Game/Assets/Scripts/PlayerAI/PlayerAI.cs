﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ePlayerAIState
{
    AIS_None,
    AIS_Idle,
    AIS_Spawning,
    AIS_Attacking,
    AIS_WalkingToGoal,
    AIS_Paused
}


public class PlayerAI : MonoBehaviour 
{

    [Tooltip("What state this AI starts off in.")]
    public ePlayerAIState StartingState = ePlayerAIState.AIS_Paused;
    ePlayerAIState CurrentState = ePlayerAIState.AIS_None;

    [Tooltip("Walk Speed of this mob.")]
    public float WalkSpeed = 20.0f;

    [Tooltip("Set this to something other than AIS_None to immediately switch to a different AI State.")]
    public ePlayerAIState DebugStartState = ePlayerAIState.AIS_None;

    void Start()
    {
        ChangePlayerAIState(StartingState);
    }

    void Update()
    {
        if (DebugStartState != ePlayerAIState.AIS_None)
        {
            ChangePlayerAIState(DebugStartState);
            DebugStartState = ePlayerAIState.AIS_None;
        }
    }

    public ePlayerAIState GetCurrentState()
    {
        return CurrentState;
    }

    public void ChangePlayerAIState(ePlayerAIState newState)
    {
        CurrentState = newState;
        BroadcastMessage("ChangeAIState", CurrentState);
    }

    void OnKilled()
    {
        ChangePlayerAIState(ePlayerAIState.AIS_Paused);
    }
}
