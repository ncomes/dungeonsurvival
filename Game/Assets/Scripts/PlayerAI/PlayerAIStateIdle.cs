﻿using UnityEngine;
using System.Collections;

public class PlayerAIStateIdle : PlayerAIState
{
    protected override ePlayerAIState mAIState { get { return ePlayerAIState.AIS_Idle; } }

    protected override void StartState()
    {
		anim.SetBool("IsIdle", true);
    }

    protected override void EndState()
    {
		anim.SetBool("IsIdle", false);
    }

}
