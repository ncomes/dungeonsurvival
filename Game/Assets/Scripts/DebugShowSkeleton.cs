﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class DebugShowSkeleton : MonoBehaviour 
{
    public Transform RootNode;


    // Use this for initialization
    void Start () 
    {
    }
    
    // Update is called once per frame
    void DrawLinesToChildren(Transform p)
    {
        foreach (Transform child in p.transform)
        {
            Debug.DrawLine(p.transform.position, child.position, Color.yellow, .1f);
            DrawLinesToChildren(child);
        }
    }
    void Update () 
    {
        DrawLinesToChildren(RootNode);
    }

    void OnRenderObject()
    {
        DrawLinesToChildren(RootNode);
    }
}
