﻿using UnityEngine;
using System.Collections;

public class Damageable : MonoBehaviour 
{
    public float Health    = 1.0f;
    public float MaxHealth = 1.0f;
    public float TimeToRemove = 0.1f;
    public GameObject Owner;

    public float DebugDealDamage = 0.0f;

    void Update()
    {
        if (DebugDealDamage > 0)
        {
            TakeDamage(DebugDealDamage);
            DebugDealDamage = 0.0f;
        }
    }

    public void TakeDamage(float amt)
    {
        if (amt < 0)
            Heal(amt);
        else
            Damage(amt);
    }

    public void Heal(float amt)
    {
        Health = Mathf.Min(MaxHealth, Health + amt);
    }

    public void Damage(float amt)
    {
        if (amt > Health)
            amt = Health;
        Health = Health - amt;
        //Debug.Log("DAMAGE: amt: " + amt + " Health: " + Health);
        if (Health <= 0)
            Kill();
    }

    public void Kill()
    {
        BroadcastMessage("OnKilled", SendMessageOptions.DontRequireReceiver);
        Invoke("RemoveFromWorld", TimeToRemove);
        GameController.StartEndGame();
    }

    void RemoveFromWorld()
    {
        Destroy(Owner);
    }

}
