﻿using UnityEngine;
using System.Collections;

public class TurretAttackScript : MonoBehaviour {

    public float speed;
    public Rigidbody fireBall;
    public float spawnTimer = 5.0f;
    public Transform spawner;

    private float _nextSpawnTime;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            FireInterval();
        }
    }

    //----------------------------------------------------------------------------
    // Spawns the projectile and launches it forward
    //----------------------------------------------------------------------------
    void launchFireBall()
    {
        Rigidbody _proj = Instantiate(fireBall, spawner.transform.position, spawner.transform.rotation) as Rigidbody;
        _nextSpawnTime = Time.time + spawnTimer;
        _proj.velocity = spawner.transform.TransformDirection(new Vector3(0f, 0f, speed));
    }

    //----------------------------------------------------------------------------
    // Spawns the projectile and launches it forward
    //----------------------------------------------------------------------------
    void FireInterval()
    {
        if (Time.time > _nextSpawnTime)
        {
            launchFireBall();
        }
    }
}
