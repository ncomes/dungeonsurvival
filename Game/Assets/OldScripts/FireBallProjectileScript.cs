﻿using UnityEngine;
using System.Collections;

public class FireBallProjectileScript : MonoBehaviour {

	public float lifetime = 3.0f;

    void Awake()
	{
		Destroy(gameObject, lifetime);
    }

	void Update ()
	{
        
	}


	void OnTriggerEnter(Collider other) {
		if(other.gameObject.tag == "Enemy")
		{
			DestroyObject(gameObject);
            if (other.gameObject.GetComponent<MobScript>()){
                other.gameObject.GetComponent<MobScript>().health -= 1f;
            }
            if (other.gameObject.GetComponent<gruntMobScript>())
            {
                other.gameObject.GetComponent<gruntMobScript>().health -= 1f;
            }
            if (other.gameObject.GetComponent<KingMobScript>())
            {
                other.gameObject.GetComponent<KingMobScript>().health -= 1f;
            }
            if (other.gameObject.GetComponent<ArcherMobScript>())
            {
                other.gameObject.GetComponent<ArcherMobScript>().health -= 1f;
            }
            if (other.gameObject.GetComponent<MageMobScript>())
            {
                other.gameObject.GetComponent<MageMobScript>().health -= 1f;
            }

        }
	}
}
