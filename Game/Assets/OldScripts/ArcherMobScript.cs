﻿using UnityEngine;
using System.Collections;

public class ArcherMobScript : MonoBehaviour {
    public float speed;
    public CharacterController controller;
    public Animator anim;
    public float health;
    public Transform player;
    public bool gruntJump = false;
    public float range;
    public Rigidbody arrow;
    public float arrowSpeed; 
    public Transform spawner;

    private bool _archerHitReact = false;
    private Transform _lavaPoint;
    private bool _randJump = false;
    private Transform _lava;
    private Transform _playerPosition;


    // Use this for initialization
    void Start()
    {
        _lavaPoint = GameObject.FindGameObjectWithTag("Finish").GetComponent<Transform>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        _lava = GameObject.FindGameObjectWithTag("Lava").GetComponent<Transform>();
        _archerHitReact = false;
        // get the animator component of the mob
        anim = GetComponent<Animator>();
        _archerHitReact = false;
        _playerPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        spawner = spawner.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GruntJumpPit())
        {
            if (!IsGruntDead())
            {
                if (!_archerHitReact)
                {
                    if (IsPlayerInRange())
                        {
                            ArcherAttack();
                        }
                    else 
                        {
                            // Move the Grunt towards the lava pit
                            MoveToLava();
                        }
                }
                else
                {
                    hitReact();
                }
            }
            else
            {
                anim.Play("Death");
            }
        }
        else
        {
            RandPitJump();
        }

    }

    //----------------------------------------------------------------
    // if player hits grunt, add damage and set the hit react to true
    //----------------------------------------------------------------
    public void getHit(float damage)
    {
        _archerHitReact = true;
        health -= damage;

    }

    //----------------------------------------------------------------
    // returns whether the mob is dead or not
    //----------------------------------------------------------------
    public bool IsGruntDead()
    {
        if (health <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //--------------------------------------------------------------
    // Move the Grunt towards the lava pit
    //--------------------------------------------------------------
    void MoveToLava()
    {
        if (Vector3.Distance(transform.position, _lavaPoint.transform.position) > 20)
        {
            transform.LookAt(_lavaPoint.position);
            controller.SimpleMove(transform.forward * speed);
            anim.Play("Walk");
        }
        else
        {
            anim.Play("Idle");
        }
    }
    //--------------------------------------------------------------
    // Plays the hit react animation
    //--------------------------------------------------------------
    void hitReact()
    {
        // if player hit the mob play hit react
        anim.Play("Damage_Left");
        // wait for the animtion to stop playing before updating
        if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !anim.IsInTransition(0))
        {
            //  turn off hit react again
            _archerHitReact = false;
        }
    }

    //----------------------------------------------------------------
    // when you click on a mob the player gets his info
    //----------------------------------------------------------------
    void OnMouseOver()
    {
        if (Input.GetMouseButton(0))
        {
            player.GetComponent<CombatScript>().opponent = gameObject;
            player.GetComponent<MoveToScript>().opponent = gameObject;
        }
    }

    //----------------------------------------------------------------
    //  Add to the score when the grunt dies
    //----------------------------------------------------------------
    void AddToScore()
    {
        GUIScript.score += 1;
    }

    //----------------------------------------------------------------
    //  if the mob is dead, destroy him.  executes on the animation
    //----------------------------------------------------------------
    void destroyMob()
    {
        Destroy(gameObject);
    }

    bool GruntJumpPit()
    {
        if (gruntJump)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    void RandPitJump()
    {
        if (!_randJump)
        {
            float rand = Random.Range(0, 100);
            if (rand <= 33)
            {
                anim.CrossFade("Pit_Jump_01", .25f, 0, 0f);
                _randJump = true;
            }
            if (rand > 33 && rand <= 66)
            {
                anim.CrossFade("Pit_Jump_02", .25f, 0, 0f);
                _randJump = true;
            }
            if (rand > 66)
            {
                anim.CrossFade("Pit_Jump_03", .25f, 0, 0f);
                _randJump = true;
            }
        }
    }
    public void DestroyCharController()
    {
        controller = GetComponent<CharacterController>();
        Destroy(controller);
    }

    public void Lava()
    {
        float _newLavaPos = _lava.transform.position.y;
        if (_newLavaPos <= 0f)
        {
            _lava.transform.position = new Vector3(0, _newLavaPos + 1f, 0);
        }
        if (_newLavaPos >= 0f)
        {
            print("GameOver");
        }
        print(_newLavaPos);

    }
    
    bool IsPlayerInRange()
        {
        if (Vector3.Distance(transform.position, _playerPosition.position) < range)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void ArcherAttack()
        {
        
            transform.LookAt(_playerPosition.position);
            anim.Play("Attack_01");

        }
    public void LaunchArrow()
    {
        Rigidbody _proj = Instantiate(arrow, spawner.transform.position, spawner.transform.rotation) as Rigidbody;
        _proj.velocity = spawner.transform.TransformDirection(new Vector3(0f, 0f, arrowSpeed));
    }
}
