﻿using UnityEngine;
using System.Collections;

public class KingMobScript : MonoBehaviour {

    public float speed;
    // range - checks to see if the mob is close enough to attack
    public float range;
    public Transform player;
    public Transform MoveToObj;
    public CharacterController controller;
    public float health;
    public float damage;
    public GameObject playerHealth;
    public bool mobHitreact;
    public float StopFromTarget;
    // 0 = grunt, 1 = warrior

    private Animator anim;
    private float _randNum;

    // Use this for initialization
    void Start()
    {
        // get the animator component of the mob
        anim = GetComponent<Animator>();
        // set the mob to play hit react to false
        mobHitreact = false;
        // find the player
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        playerHealth = GameObject.FindGameObjectWithTag("Player");
        FindPlayer();
        // set the mobs tag
        gameObject.tag = "Enemy";



    }

    // Update is called once per frame
    void Update()
    {
        RandomNumberGenerator();
        // check to see if the mob is dead
        if (!isDead())
        {
            // check to see if the mob has been struck
            if (!mobHitreact)
            {
                // see if the paler is in range.  if not than run after him.
                if (!inRange() && playerHealth.GetComponent<MoveToScript>().health >= 1f)
                {
                    chase();
                }
                else
                {
                    // if player is in range.  play attack animation
                    if (playerHealth.GetComponent<MoveToScript>().health <= 0f)
                    {
                        anim.Play("Idle");
                    }
                    else
                    {
                        if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !anim.IsInTransition(0))
                        {
                            randomAttack(_randNum);
                        }
                    }

                }
            }
            else
            {
                anim.Play("Damage_Right");
                // wait for the animtion to stop playing before updating
                if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !anim.IsInTransition(0))
                {
                    //  turn off hit react again
                    mobHitreact = false;
                }
            }
        }
        else
        {
            //  player is dead
            //Destroy(gameObject.GetComponent<CharacterController>());
            anim.Play("Death");
        }
    }
    //----------------------------------------------------------------
    // if player is hit add damage and set the hit react to true
    //----------------------------------------------------------------
    public void getHit(float damage)
    {
        mobHitreact = true;
        health -= damage;

    }
    //----------------------------------------------------------------
    //  returns true/false if the player is in range
    //----------------------------------------------------------------
    bool inRange()
    {
        if (Vector3.Distance(transform.position, MoveToObj.position) < range)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    //----------------------------------------------------------------
    //  if the mob is dead, destroy him.  executes on the animation
    //----------------------------------------------------------------
    void destroyMob()
    {
        Destroy(gameObject);
    }
    //----------------------------------------------------------------
    // checks distance between player and mob.  If the mob is not too
    // close, mob run after the player.
    //----------------------------------------------------------------
    void chase()
    {
        if (Vector3.Distance(transform.position, MoveToObj.transform.position) > StopFromTarget)
        {
            transform.LookAt(MoveToObj.position);
            controller.SimpleMove(transform.forward * speed);
            anim.Play("Run");
        }
    }
    //----------------------------------------------------------------
    // when you click on a mob the player gets his info
    //----------------------------------------------------------------
    void OnMouseOver()
    {
        if (Input.GetMouseButton(0))
        {
            player.GetComponent<CombatScript>().opponent = gameObject;
            player.GetComponent<MoveToScript>().opponent = gameObject;
        }

    }
    //----------------------------------------------------------------
    // returns whether the mob is dead or not
    //----------------------------------------------------------------
    public bool isDead()
    {
        if (health <= 0 || gameObject.transform.position.y < -3)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    //----------------------------------------------------------------
    // if the player is close enough the mob plays attack animation
    //----------------------------------------------------------------
    void attack()
    {
        if (inRange())
        {
            playerHealth.GetComponent<MoveToScript>().health -= damage;
            //print ("Hit!!!");
        }
    }

    void FindPlayer()
    {
        // Get the Lava point
        MoveToObj = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }
    
    void RandomNumberGenerator()
        {
            _randNum = Random.Range(0, 10);

        }
    
    void AddToScore()
    {
        if (gameObject.transform.position.y >= 0)
        {
            GUIScript.score += 1;
        }
    }
    void randomAttack(float Ran)
    {
        if (Ran <= 3f)
        {
            anim.CrossFade("Attack_01", .25f, 0, 0);
        }
        if (Ran > 3f && Ran <= 6f)
        {
            anim.CrossFade("Attack_02", .25f, 0, 0);
        }
        if (Ran > 6f)
        {
            anim.CrossFade("Attack_03", .25f, 0, 0);
        }
    }
}

