﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TurretFBScript : MonoBehaviour {

    public float speed;
    public Rigidbody fireBall;
    public float spawnTimer = 5.0f;
    public Transform spawner;

    private float _nextSpawnTime;
    private float _turretRotationSpeed = 10.0f;
    private List<GameObject> targetedEnemies;
    private SphereCollider _turret;
    private GameObject _targeted;

    void Awake()
    {
        targetedEnemies = new List<GameObject>();
        _turret = GetComponent<SphereCollider>();
    }

    // Use this for initialization
    void Start () {
        //enemy = GameObject.FindGameObjectWithTag("Enemy").GetComponent<Transform>();
        spawner = spawner.transform;
        _turret.radius = 0f;
    }
	
	// Update is called once per frame
	void Update () {
        GrowRadius();
        tagetEnemy();

    }
    
    void tagetEnemy() {
        if (_targeted == null)
        {
            foreach (GameObject target in targetedEnemies)
            {
                _targeted = target;
                if (_targeted != null)
                {
                    break;
                }

            }
        }
        if (_targeted != null)
        {
            lookAtEnemys(_targeted.GetComponent<Transform>());
            FireInterval();
            if (ISEnemyDead(_targeted)){
                targetedEnemies.Remove(_targeted);
                _targeted = null;
            }

        }
    }
    
    //----------------------------------------------------------------------------
    // turn the turret so if faces the enemy
    //----------------------------------------------------------------------------
    void lookAtEnemys(Transform enemy )
    {
        // turn and look at the player
        Vector3 _enemyPosition = enemy.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(_enemyPosition);
        rotation.x = 0f;
        rotation.z = 0f;

        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * _turretRotationSpeed);

    }
    //----------------------------------------------------------------------------
    // Spawns the projectile and launches it forward
    //----------------------------------------------------------------------------
    void launchFireBall()
    {
        Rigidbody _proj = Instantiate(fireBall, spawner.transform.position, spawner.transform.rotation) as Rigidbody;
        _nextSpawnTime = Time.time + spawnTimer;
        _proj.velocity = spawner.transform.TransformDirection(new Vector3(0f, 0f, speed));
    }

    //----------------------------------------------------------------------------
    // Spawns the projectile and launches it forward
    //----------------------------------------------------------------------------
    void FireInterval()
    {
        if (Time.time > _nextSpawnTime)
        {
            launchFireBall();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            targetedEnemies.Add(other.gameObject);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            targetedEnemies.Remove(other.gameObject);
        }
    }
    
    void GrowRadius()
    {
        if (_turret.radius <= 40){
            _turret.radius += 30f * Time.deltaTime;
        }
    }
    
    bool ISEnemyDead(GameObject enemy) 
    {
        if (enemy.GetComponent<MobScript>()) {
            if (enemy.GetComponent<MobScript>().health <= 0)
            {
                return true;
            }
        }
        if (enemy.GetComponent<gruntMobScript>())
        {
            if (enemy.GetComponent<gruntMobScript>().health <= 0)
            {
                return true;
            }
        }
            if (enemy.GetComponent<KingMobScript>())
            {
                if (enemy.GetComponent<KingMobScript>().health <= 0)
                {
                    return true;
                }
            }
            if (enemy.GetComponent<ArcherMobScript>())
            {
                if (enemy.GetComponent<ArcherMobScript>().health <= 0)
                {
                    return true;
                }
            }
            if (enemy.GetComponent<MageMobScript>())
            {
                if (enemy.GetComponent<MageMobScript>().health <= 0)
                {
                    return true;
                }
            }
        return false;
    }

}
