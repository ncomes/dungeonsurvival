﻿using UnityEngine;
using System.Collections;

public class FireBallTurretDestroyScript : MonoBehaviour {

    public float lifetime;

    void Awake()
    {
        Destroy(gameObject, lifetime);
    }

}
