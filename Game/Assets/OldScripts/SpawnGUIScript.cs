﻿using UnityEngine;
using System.Collections;

public class SpawnGUIScript : MonoBehaviour {

    public GameObject FBTurret;
    public float spawnTimer = 5.0f;

    private Transform _player;
    private float _nextSpawnTime;
    private float _turretButtonNumber;

    // Use this for initialization
    void Start () {
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        _turretButtonNumber = GUIScript.turretsCount;

    }
	
	// Update is called once per frame
	void Update () {
        RecoverTurret();
    }

//    void OnGUI()
 //   {
        /*
        if(GUI.Button(new Rect(10, 10, 100, 80), Mathf.Floor(GUIScript.turretsCount).ToString()) && GUIScript.turretsCount >=1)
        {
            SpawnTurret();
        }
        */
   // }


    //----------------------------------------------------------------------------
    // Spawns the projectile and launches it forward
    //----------------------------------------------------------------------------
    public void SpawnTurret()
    {
        if (GUIScript.turretsCount >= 1) {
            Vector3 _position = new Vector3(_player.transform.position.x, 0f, _player.transform.position.z);
            GameObject.Instantiate(FBTurret, new Vector3(_position.x, _position.y, _position.z), Quaternion.identity);
            GUIScript.turretsCount -= 1;
        }
    }

    //----------------------------------------------------------------------------
    // Spawns the projectile and launches it forward
    //----------------------------------------------------------------------------
    public void RecoverTurret()
    {
        if (GUIScript.turretsCount <= (_turretButtonNumber))
        {
            GUIScript.turretsCount += 1f * Time.deltaTime / 10f;
        }
    }
}
