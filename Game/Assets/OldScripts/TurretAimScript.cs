﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TurretAimScript : MonoBehaviour {

    public List<GameObject> targetedEnemies;

    private SphereCollider _turret;
    private float _turretRotationSpeed = 10.0f;
    private GameObject _targeted;


    // Use this for initialization
    void Start () {
        targetedEnemies = new List<GameObject>();
        _turret = GetComponent<SphereCollider>();
    }
	
	// Update is called once per frame
	void Update () {
        tagetEnemy();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            targetedEnemies.Add(other.gameObject);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            targetedEnemies.Remove(other.gameObject);
        }
    }

    void GrowRadius()
    {
        if (_turret.radius <= 40)
        {
            _turret.radius += 30f * Time.deltaTime;
        }
    }
    bool ISEnemyDead(GameObject enemy)
    {
        if (enemy.GetComponent<MageMobScript>())
        {
            if (enemy.GetComponent<MageMobScript>().health <= 0)
            {
                return true;
            }
        }
        return false;
    }
    //----------------------------------------------------------------------------
    // turn the turret so if faces the enemy
    //----------------------------------------------------------------------------
    void lookAtEnemys(Transform enemy)
    {
        // turn and look at the player
        Vector3 _enemyPosition = enemy.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(_enemyPosition);
        rotation.x = 0f;
        rotation.z = 0f;

        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * _turretRotationSpeed);

    }

    void tagetEnemy()
    {
        if (_targeted == null)
        {
            foreach (GameObject target in targetedEnemies)
            {
                _targeted = target;
                if (_targeted != null)
                {
                    break;
                }

            }
        }
        if (_targeted != null)
        {
            lookAtEnemys(_targeted.GetComponent<Transform>());
            if (ISEnemyDead(_targeted))
            {
                targetedEnemies.Remove(_targeted);
                _targeted = null;
            }

        }
    }
}
