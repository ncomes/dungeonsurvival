﻿using UnityEngine;
using System.Collections;

public class CombatScript : MonoBehaviour {

	public GameObject opponent;
	public float damage;
	public float range;

	private int RanNum;
	private Animator anim;
	private MoveToScript _player;
	private float P_health;
	// Use this for initialization
	void Start () {
		// get the players animator to access his animations
		anim = GetComponent<Animator> ();
		// starting a random number for the random attacks
		RanNum = 1;
		// get the MoveTo script
		_player = gameObject.GetComponent<MoveToScript>();
	}
	
	// Update is called once per frame
	void Update () {
		// update the player's health info for attacking
		P_health = _player.health;
		// on the left click of the mouse
		if (Input.GetMouseButton (0) && inRange() && hitClick() && P_health >= 1f) {
			// chooses a random attack
			randomAttack(RanNum);

			// turns the player towards the opponent
			if (opponent != null){
				transform.LookAt(opponent.transform.position);
			}

		}
		//if (!this.anim.GetCurrentAnimatorStateInfo(0).IsName ("1h_Attack_02")) {
		if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !anim.IsInTransition(0)){
			anim.SetBool("IsAttacking", false);
			RanNum = Random.Range (0, 2);
		}

		//impact ();
	}

	void impact(){
		if (opponent != null && inRange()) {
            if (opponent.GetComponent<MobScript>())
            {
                opponent.GetComponent<MobScript>().getHit(damage);
            }
            if (opponent.GetComponent<gruntMobScript>())
            {
                opponent.GetComponent<gruntMobScript>().getHit(damage);
            }
            if (opponent.GetComponent<KingMobScript>())
            {
                opponent.GetComponent<KingMobScript>().getHit(damage);
            }
            if (opponent.GetComponent<ArcherMobScript>())
            {
                opponent.GetComponent<ArcherMobScript>().getHit(damage);
            }
            if (opponent.GetComponent<MageMobScript>())
            {
                opponent.GetComponent<MageMobScript>().getHit(damage);
            }
        }

	}

	bool inRange(){
		if (opponent != null) {
			return (Vector3.Distance (opponent.transform.position, transform.position) < range);
		} else {
			return false;
		}
	}
	bool hitClick()
	{
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		
		if (Physics.Raycast(ray, out hit, 1000, 1 << 8 | 1 << 9 | 1 << 10) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
			if (hit.collider.tag == "Enemy"){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	void randomAttack(int Ran){
			if (Ran == 0) {
				anim.Play ("1h_Attack_01");
			}
			if (Ran == 1) {
				anim.Play ("1h_Attack_02");
			}
			if (Ran == 2) {
				anim.Play ("1h_Attack_03");
			}
	}

	public void PlayerDestroy(){
			Destroy(gameObject);

			}

}
