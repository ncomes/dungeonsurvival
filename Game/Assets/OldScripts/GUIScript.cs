﻿using UnityEngine;
using System.Collections;

public class GUIScript : MonoBehaviour {

    public static int score = 0;
    public static float turretsCount = 2;

    private MoveToScript player;
    private float _health;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<MoveToScript>();
    }
	
	// Update is called once per frame
	void Update () {
        _health = player.health;
    }

    void OnGUI()
    {
        if (_health <= 0)
        {
            _health = 0;
        }
        GUI.Label(new Rect(50, 100, 60, 60), "Score");
        GUI.Label(new Rect(50, 130, 60, 60), score.ToString());

        GUI.Label(new Rect(50, 180, 60, 60), "Player Health");
        GUI.Label(new Rect(50, 210, 60, 60), Mathf.Ceil(_health).ToString());
    }
}
