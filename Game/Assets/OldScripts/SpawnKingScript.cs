﻿using UnityEngine;
using System.Collections;

public class SpawnKingScript : MonoBehaviour {

    public GameObject kingMob;
    public int spawnAtScore = 15;
    public GameObject _mobSpawn;
    public float powerUp = 0f;

    private Vector3 _spawner;
    private GameObject _mobParent;
    private int _spawnScore = 0;
    private int _score;

    // Use this for initialization
    void Start()
    {
        _spawner = transform.position;
        _mobParent = GameObject.FindGameObjectWithTag("EnemyGroup");
        _score = GUIScript.score;
        //StartCoroutine("SpawnWait");
    }

    // Update is called once per frame
    void Update()
    {
        SpawnAtScore();
    }

    // Spawns a mob character
    void spawnMob()
    {
        _mobSpawn = Instantiate(kingMob, new Vector3(_spawner.x, _spawner.y, _spawner.z), Quaternion.identity) as GameObject;
        _mobSpawn.transform.parent = _mobParent.transform;
    }

    void SpawnAtScore()
    {
        if (GUIScript.score > _score)
            {
                _score += 1;
                _spawnScore += 1;
            }
        if (_spawnScore >= spawnAtScore)
            {
                _spawnScore = 0;
                spawnMob();
            if (_mobSpawn.GetComponent<KingMobScript>().health <= 10)
            {
                _mobSpawn.GetComponent<KingMobScript>().health += (powerUp += 0.2f);
                _mobSpawn.GetComponent<KingMobScript>().damage += (powerUp);
            }
            }
    }

}