﻿using UnityEngine;
using System.Collections;

public class Turret : MonoBehaviour {

    private float _turretRotationSpeed = 10.0f;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //----------------------------------------------------------------------------
    // turn the turret so if faces the enemy
    //----------------------------------------------------------------------------
    void lookAtEnemys(Transform enemy)
    {
        // turn and look at the player
        Vector3 _enemyPosition = enemy.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(_enemyPosition);
        rotation.x = 0f;
        rotation.z = 0f;

        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * _turretRotationSpeed);

    }

}
