﻿using UnityEngine;
using System.Collections;

public class FireBallSpawnerScript : MonoBehaviour {
	public float speed;
	public Rigidbody fireBall;
	public float spawnTimer = 5.0f;
	
	private float _nextSpawnTime;
	
	void Awake()
	{
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		FireInterval ();
	}
	
	//----------------------------------------------------------------------------
	// Spawns the projectile and launches it forward
	//----------------------------------------------------------------------------
	void launchFireBall()
	{
		Rigidbody _proj = Instantiate(fireBall, transform.position, transform.rotation) as Rigidbody;
		_nextSpawnTime = Time.time + spawnTimer;
		_proj.velocity = transform.TransformDirection (new Vector3 (0f, 0f, speed));

	}
	
	//----------------------------------------------------------------------------
	// Spawns the projectile and launches it forward
	//----------------------------------------------------------------------------
	void FireInterval()
	{
		if (Time.time > _nextSpawnTime)
		{
			launchFireBall();
		}
	}
}
