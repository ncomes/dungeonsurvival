﻿using UnityEngine;
using System.Collections;

public class SpawnMobScript : MonoBehaviour {

	public GameObject warriorMob;
    public GameObject gruntMob;
    public GameObject slowGruntMob;
    public GameObject archerMob;
    public GameObject mageMob;
    public float minSpawnTimer = 15.0f;
    public float maxSpawnTimer = 25.0f;

    private Vector3 _spawner;
    private GameObject _mobParent;
    private float _nextSpawnTime;
    private GameObject _mob;

    // Use this for initialization
    void Start () {
        _spawner = transform.position;
        _mobParent = GameObject.FindGameObjectWithTag("EnemyGroup");
    }
	
	// Update is called once per frame
	void Update () {
        SpawnAtTime();
    }

    // Spawns a mob character
    void spawnMob(){
        RanddomMobObj();
        GameObject _mobSpawn = Instantiate(_mob, new Vector3(_spawner.x, _spawner.y, _spawner.z), Quaternion.identity) as GameObject;
        _mobSpawn.transform.parent = _mobParent.transform;
        _nextSpawnTime = Time.time + (Random.Range(minSpawnTimer, maxSpawnTimer));
    }

    void SpawnAtTime()
    {
        if (Time.time > _nextSpawnTime)
        {
            spawnMob();
        }
    }

    void RanddomMobObj()
    {
        float rand = Random.Range(0, 100);
        if (rand <= 10)
        {
            _mob = warriorMob;
        }
        if (rand > 10 && rand <= 20)
        {
            _mob = archerMob;
        }
        if (rand > 20 && rand <= 28)
        {
            _mob = mageMob;
        }
        if (rand > 28 && rand <= 60)
        {
            _mob = gruntMob;
        }
        if (rand > 60)
        {
            _mob = slowGruntMob;
        }
    }
    
}


/*
// create a pause before spawning
IEnumerator SpawnWait()
{
    print("waiting");
    yield return new WaitForSeconds(5);
    print("Stop waiting!");
    spawnMob();
}


    */