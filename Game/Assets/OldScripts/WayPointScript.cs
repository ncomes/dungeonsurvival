﻿using UnityEngine;
using System.Collections;

public class WayPointScript : MonoBehaviour
{
	public Transform [] Waypoints;
	public float Speed = 3f;
	public int curWayPoint;
	public bool doPatrol = true;
	public Vector3 Target;
	public Vector3 MoveDirection;
	public Vector3 Velocity;
	private Transform myTransform;
	public Transform LookTagret;
	public float rotationSpeed = 1f;

	void Awake()
	{
		myTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {
		//print (curWayPoint);
		//print (Waypoints.Length);
		if (curWayPoint < Waypoints.Length)
		{
			Target = Waypoints[curWayPoint].position;
			LookTagret = Waypoints[curWayPoint];
			MoveDirection = Target - transform.position;
			Velocity = GetComponent<Rigidbody>().velocity;
			myTransform.rotation = Quaternion.Slerp(myTransform.rotation, Quaternion.LookRotation(LookTagret.position - myTransform.position), rotationSpeed * Time.deltaTime);


			if(MoveDirection.magnitude < 1)
			{
				curWayPoint++;
			}
			else
			{
				Velocity = MoveDirection.normalized * Speed;
				//print (Velocity);
			}

		}
		else
		{
			if (doPatrol)
			{
				curWayPoint=0;
			}
			else
			{
				Velocity = Vector3.zero;
			}
		}
		GetComponent<Rigidbody>().velocity = Velocity;
		//transform.LookAt(Target);
	}
}
