﻿using UnityEngine;
using System.Collections;

public class MoveToScript : MonoBehaviour
	{
	public float speed = 10;
	public float rotationSpeed = 5;
	public CharacterController controller;
	public float health;
	public GameObject opponent;

	private Animator _anim;
	private Vector3 _desiredPosition;
	private Vector3 _enemyPosition;
	private bool _attack;
	private bool _selectEnemy;
	private int _range = 8;



		void Start()
		{
			// get the current position of the player
			_desiredPosition = transform.position;
			// get the animator so we can access the animations
			_anim = GetComponent<Animator> ();
			_attack = _anim.GetBool("IsAttacking");
			_selectEnemy = false;
		}
		
		// Update is called once per frame
		// Checking each state the player is in every frame
		void Update () {
			if (opponent != null) {
				_enemyPosition = opponent.transform.position;
			}
			if (!IsDead ()) {
				_anim.SetBool("IsDead", false);
				if (!_attack) {
					if (Input.GetMouseButton (0)) {
						// Locate where the player clicked on the terrain
						locatePosition ();
					}
					moveToPosition();
					//Debug.DrawLine (desiredPosition, transform.position);
				}
			} else {
				_anim.SetBool("IsDead", true);
				_anim.SetBool("IsAttacking", false);
			}
		}
		// check to see where the player clicked and update where the player needs to move to.
		void locatePosition()
		{
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if (Physics.Raycast(ray, out hit, 1000, 1 << 8 | 1 << 9 | 1 << 10 ))
			{
			    if (hit.collider.tag != "Player" && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() ){
					    _desiredPosition = new Vector3(hit.point.x, hit.point.y, hit.point.z);
					    _selectEnemy = false;
					    _range = 3;
				    }
			    if (opponent != null && hit.collider.tag == "Enemy" && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                {
                    _selectEnemy = true;
				    _range = 8;
			    }
			}

		}
		// move the player character to the clicked position
		void moveToPosition()
			{
				// see if the player is close to the clicked position
				if (opponent != null &&_selectEnemy == true) {
					_desiredPosition = _enemyPosition;
				}
				if (Vector3.Distance (transform.position, _desiredPosition) > _range) {
					Quaternion newRotation = Quaternion.LookRotation (_desiredPosition - transform.position);
					newRotation.x = 0f;
					newRotation.z = 0f;

					transform.rotation = Quaternion.Slerp (transform.rotation, newRotation, Time.deltaTime * rotationSpeed);
					controller.SimpleMove (transform.forward * speed);
					
					float _speed = Vector3.Distance(_desiredPosition, transform.position);
					_anim.SetFloat("Speed", (_speed));
		
		
				 }else{
					_anim.SetFloat("Speed", (0));
				}
		}
		// Check to see if the player is dead
		bool IsDead(){

		if (health <= 0f || gameObject.transform.position.y < -3) {
			return true;
		} else {
			return false;
		}
	}


}