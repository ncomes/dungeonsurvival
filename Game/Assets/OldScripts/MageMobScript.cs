﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MageMobScript : MonoBehaviour {

    public float speed;
    public float health;
    public float range;
    public Rigidbody arrow;
    public float arrowSpeed;
    public Transform spawner;
    public GameObject grunt;
    public List<GameObject> _gruntspawners;
    public GameObject mageRadius;
    public GameObject mageSpawnRadius;

    private bool _archerHitReact = false;
    private Transform _lavaPoint;
    private bool _randJump = false;
    private Transform _lava;
    private Transform _playerPosition;
    private MageRadiusScript _mageRadius;
    private MageRadiusScript _mageSpawnRadius;
    private Transform player;
    private Animator anim;
    private CharacterController controller;
    private bool gruntJump = false;
    private bool _spawnGrunts;
    private GameObject _mobParent;


    // Use this for initialization
    void Start()
    {
        _lavaPoint = GameObject.FindGameObjectWithTag("Finish").GetComponent<Transform>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        _lava = GameObject.FindGameObjectWithTag("Lava").GetComponent<Transform>();
        _mageRadius = mageRadius.GetComponent<MageRadiusScript>();
        _mageSpawnRadius = mageSpawnRadius.GetComponent<MageRadiusScript>();
        _archerHitReact = false;
        // get the animator component of the mob
        anim = GetComponent<Animator>();
        _archerHitReact = false;
        _playerPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        spawner = spawner.transform;
        controller = GetComponent<CharacterController>();
        _spawnGrunts = true;
        _mobParent = GameObject.FindGameObjectWithTag("EnemyGroup");
    }

    // Update is called once per frame
    void Update()
    {
        if (!GruntJumpPit())
        {
            if (!IsGruntDead())
            {
                if (!_archerHitReact)
                {
                    if (SpawnGruntsInRange() && _spawnGrunts == true) {
                        anim.Play("Enraged");
                        //if (anim.GetCurrentAnimatorStateInfo(0).IsName("Enraged"))
                        //   print("wait");
                        
                    } else {
                        if (IsPlayerInRange())
                        {
                            ArcherAttack();
                        }
                        else
                        {
                            // Move the Grunt towards the lava pit
                            MoveToLava();
                        }
                    }
                }
                else
                {
                    hitReact();
                }
            }
            else
            {
                anim.Play("Death");
            }
        }
        else
        {
            RandPitJump();
        }

    }

    //----------------------------------------------------------------
    // if player hits grunt, add damage and set the hit react to true
    //----------------------------------------------------------------
    public void getHit(float damage)
    {
        _archerHitReact = true;
        health -= damage;

    }

    //----------------------------------------------------------------
    // returns whether the mob is dead or not
    //----------------------------------------------------------------
    public bool IsGruntDead()
    {
        if (health <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //--------------------------------------------------------------
    // Move the Grunt towards the lava pit
    //--------------------------------------------------------------
    void MoveToLava()
    {
        if (Vector3.Distance(transform.position, _lavaPoint.transform.position) > 20)
        {
            transform.LookAt(_lavaPoint.position);
            controller.SimpleMove(transform.forward * speed);
            anim.Play("Walk");
        }
        else
        {
            anim.Play("Idle");
        }
    }
    //--------------------------------------------------------------
    // Plays the hit react animation
    //--------------------------------------------------------------
    void hitReact()
    {
        // if player hit the mob play hit react
        anim.Play("Damage_Left");
        // wait for the animtion to stop playing before updating
        if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !anim.IsInTransition(0))
        {
            //  turn off hit react again
            _archerHitReact = false;
        }
    }

    //----------------------------------------------------------------
    // when you click on a mob the player gets his info
    //----------------------------------------------------------------
    void OnMouseOver()
    {
        if (Input.GetMouseButton(0))
        {
            player.GetComponent<CombatScript>().opponent = gameObject;
            player.GetComponent<MoveToScript>().opponent = gameObject;
        }
    }

    //----------------------------------------------------------------
    //  Add to the score when the grunt dies
    //----------------------------------------------------------------
    void AddToScore()
    {
        GUIScript.score += 1;
    }

    //----------------------------------------------------------------
    //  if the mob is dead, destroy him.  executes on the animation
    //----------------------------------------------------------------
    void destroyMob()
    {
        Destroy(gameObject);
    }

    bool GruntJumpPit()
    {
        if (gruntJump)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //----------------------------------------------------------------
    //  pick a random pit jump animation
    //---------------------------------------------------------------- 
    void RandPitJump()
    {
        if (!_randJump)
        {
            float rand = Random.Range(0, 100);
            if (rand <= 33)
            {
                anim.CrossFade("Pit_Jump_01", .25f, 0, 0f);
                _randJump = true;
            }
            if (rand > 33 && rand <= 66)
            {
                anim.CrossFade("Pit_Jump_02", .25f, 0, 0f);
                _randJump = true;
            }
            if (rand > 66)
            {
                anim.CrossFade("Pit_Jump_03", .25f, 0, 0f);
                _randJump = true;
            }
        }
    }

    //----------------------------------------------------------------------
    //  deletes the character controller so the player can run through him.
    //---------------------------------------------------------------------- 
    public void DestroyCharController()
    {
        Destroy(controller);
    }

    public void Lava()
    {
        float _newLavaPos = _lava.transform.position.y;
        if (_newLavaPos <= 0f)
        {
            _lava.transform.position = new Vector3(0, _newLavaPos + 1f, 0);
        }
        if (_newLavaPos >= 0f)
        {
            print("GameOver");
        }
        print(_newLavaPos);

    }

    bool IsPlayerInRange()
    {
        if  (_mageRadius.player != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool SpawnGruntsInRange()
    {
        if (_mageSpawnRadius.player != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }



    void ArcherAttack()
    {

        transform.LookAt(_playerPosition.position);
        anim.Play("Attack_01");

    }
    public void LaunchArrow()
    {
        Rigidbody _proj = Instantiate(arrow, spawner.transform.position, spawner.transform.rotation) as Rigidbody;
        _proj.velocity = spawner.transform.TransformDirection(new Vector3(0f, 0f, arrowSpeed));
    }
    
    public void SpawnGrunts()
    {
        foreach (GameObject gruntSpawn in _gruntspawners)
        {
            GameObject _spawnedGrunt = Instantiate(grunt, new Vector3(gruntSpawn.transform.position.x, gruntSpawn.transform.position.y, gruntSpawn.transform.position.z), Quaternion.identity) as GameObject;
            _spawnedGrunt.transform.parent = _mobParent.transform;
        }
        _spawnGrunts = false;
    }
}

