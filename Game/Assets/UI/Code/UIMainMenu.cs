﻿using UnityEngine;
using System.Collections;

public class UIMainMenu : MonoBehaviour
{
    public static UIMainMenu instance;
    public GameObject ParentObject;

    void Awake()
    {
        if (!instance)
            instance = this;
    }

    public static void Show() { UIMainMenu.instance.ShowUI(); }
    public void ShowUI() { ParentObject.SetActive(true); }

    public static void Hide() { UIMainMenu.instance.HideUI(); }
    public void HideUI() { ParentObject.SetActive(false); }

    public static void StartBattleFromUI() { UIMainMenu.instance.StartBattleClicked(); }
    public void StartBattleClicked() { GameController.StartBattle(); }
}
