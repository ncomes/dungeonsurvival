﻿using UnityEngine;
using System.Collections;

public class UIEndGame : MonoBehaviour
{

    public static UIEndGame instance;
    public GameObject ParentObject;

    void Awake()
    {
        if (!instance)
            instance = this;
    }

    public static void Show() { UIEndGame.instance.ShowUI(); }
    public void ShowUI() { ParentObject.SetActive(true); }

    public static void Hide() { UIEndGame.instance.HideUI(); }
    public void HideUI() { ParentObject.SetActive(false); }

    public static void GotoMainMenu() { UIEndGame.instance.ToMainMenu(); }
    public void ToMainMenu() { GameController.StartMainMenu(); }
}
