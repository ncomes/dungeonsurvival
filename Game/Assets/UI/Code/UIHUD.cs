﻿using UnityEngine;
using System.Collections;

public class UIHUD : MonoBehaviour
{
    public static UIHUD instance;
    public GameObject ParentObject;
    public UISlider HealthSlider;

    public Color GoodHealthColor = Color.green;
    public Color HurtHealthColor = Color.yellow;
    public Color NearDeathHealthColor = Color.red;

    void Awake()
    {
        if (!instance)
            instance = this;
    }

    void Update()
    {
        UpdateHealthBar();
    }

    void UpdateHealthBar()
    {
        if (!ParentObject.activeInHierarchy)
            return;

        Damageable playerHealth = PlayerAnchor.instance.GetComponent<Damageable>();
        if (!playerHealth)
            return;

        float pct = playerHealth.Health / playerHealth.MaxHealth;
        HealthSlider.value = pct;
        if (pct < 0.333)
            HealthSlider.foregroundWidget.color = NearDeathHealthColor;
        else if (pct < 0.667)
            HealthSlider.foregroundWidget.color = HurtHealthColor;
        else
            HealthSlider.foregroundWidget.color = GoodHealthColor;

    }

    public static void Show() { UIHUD.instance.ShowUI(); }
    public void ShowUI() { ParentObject.SetActive(true); }

    public static void Hide() { UIHUD.instance.HideUI(); }
    public void HideUI() { ParentObject.SetActive(false); }

    public static void GotoMainMenu() { UIHUD.instance.ToMainMenu(); }
    public void ToMainMenu() { GameController.StartMainMenu(); }
}
