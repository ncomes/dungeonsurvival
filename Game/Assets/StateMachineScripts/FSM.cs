﻿using UnityEngine;
using System.Collections;

public class FSM : MonoBehaviour {

    public enum State {
        Idle,
        Initialize,
        Setup,
        IsAttacking,
        IsDead
    }

    private State _state;  // local var that represents our state
    
	// Use this for initialization
	IEnumerator Start () {
        _state = State.Initialize;
        while (true)
        {
            switch (_state)
            {
                case State.Initialize:
                    InitMe();
                    break;
                case State.Setup:
                    SetMeUp();
                    break;
                case State.IsAttacking:
                    Actions();
                    break;
                case State.IsDead:
                    DeathState();
                    break;
            }
            yield return 0;
        }
        
	}
	
    private void InitMe() {
        print("This is the InitMe function");
        _state = State.Setup;
    }
    private void SetMeUp() {
        print("This is the SetMeUp function");
        _state = State.IsAttacking;
    }
    private void Actions()
    {
        print("This is the Actions function");
        _state = State.IsDead;
    }
    private void DeathState()
    {
        print("This is the DeathState function");
    }
}
